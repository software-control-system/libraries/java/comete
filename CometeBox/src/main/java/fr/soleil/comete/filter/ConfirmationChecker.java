/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.filter;

import java.awt.Component;
import java.util.Arrays;

import javax.swing.JOptionPane;

import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IDialog;
import fr.soleil.data.filter.AbstractChecker;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;

public class ConfirmationChecker extends AbstractChecker {

    private static final String CONFIRMATION = "Confirmation";
    private static final String SET_S = "Set %s";
    private static final String STRING_FORMAT = "%s";
    private static final String AND_SO_ON = "(...)";
    private static final String QUESTION_MARK = " ?";

    private String confirmationMessage;
    private String confirmationTitle;
    private boolean confirmation;

    public ConfirmationChecker() {
        confirmationMessage = ObjectUtils.EMPTY_STRING;
        confirmationTitle = CONFIRMATION;
        confirmation = false;
    }

    @Override
    public boolean isInputConfirmed(Object toSource, Object data) {
        return true;
    }

    @Override
    public boolean isOutputConfirmed(Object source, Object data) {
        boolean result = true;
        if (confirmation) {
            int confResult = JOptionPane.YES_OPTION;
            IComponent parent = (source instanceof IComponent) ? (IComponent) source : null;
            confResult = executeConfirmation(parent, data, confirmationMessage, confirmationTitle);
            result = (confResult == JOptionPane.YES_OPTION);
        }
        return result;
    }

    public String getConfirmationMessage() {
        return confirmationMessage;
    }

    public void setConfirmationMessage(String confirmationMessage) {
        this.confirmationMessage = confirmationMessage;
    }

    public boolean isConfirmation() {
        return confirmation;
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
    }

    public String getConfirmationTitle() {
        return confirmationTitle;
    }

    public void setConfirmationTitle(String message) {
        this.confirmationTitle = message;
    }

    protected static Component searchForVisibleComponent(final Component comp) {
        Component result = comp;
        while ((result != null) && (!result.isVisible())) {
            result = result.getParent();
        }
        if (result == null) {
            result = WindowSwingUtils.getWindowForComponent(comp);
        }
        return result;
    }

    public static int executeConfirmation(IComponent parent, Object data, String confirmationMessage,
            String confirmationTitle) {
        int result = JOptionPane.NO_OPTION;
        StringBuilder messageBuilder = new StringBuilder();
        String confirmationFormat;
        boolean addQuestionMark;
        if ((confirmationMessage != null) && (!confirmationMessage.trim().isEmpty())) {
            confirmationFormat = confirmationMessage;
            addQuestionMark = false;
        } else {
            confirmationFormat = SET_S;
            addQuestionMark = true;
        }
        if (confirmationFormat.indexOf(STRING_FORMAT) > -1) {
            String strValue = ObjectUtils.EMPTY_STRING;
            if ((data == null) || (!data.getClass().isArray())) {
                strValue = String.valueOf(data);
            } else {
                if (data instanceof Object[]) {
                    strValue = Arrays.deepToString((Object[]) data);
                } else if (data instanceof boolean[]) {
                    strValue = Arrays.toString((boolean[]) data);
                } else if (data instanceof char[]) {
                    strValue = Arrays.toString((char[]) data);
                } else if (data instanceof byte[]) {
                    strValue = Arrays.toString((byte[]) data);
                } else if (data instanceof short[]) {
                    strValue = Arrays.toString((short[]) data);
                } else if (data instanceof int[]) {
                    strValue = Arrays.toString((int[]) data);
                } else if (data instanceof long[]) {
                    strValue = Arrays.toString((long[]) data);
                } else if (data instanceof float[]) {
                    strValue = Arrays.toString((float[]) data);
                } else if (data instanceof double[]) {
                    strValue = Arrays.toString((double[]) data);
                }
                if (strValue.length() > 100) {
                    messageBuilder.append(strValue.substring(0, 100)).append(AND_SO_ON);
                } else {
                    messageBuilder.append(strValue);
                }
            }
            messageBuilder.append(String.format(confirmationFormat, strValue));
        } else {
            messageBuilder.append(confirmationFormat);
        }
        if (addQuestionMark) {
            messageBuilder.append(QUESTION_MARK);
        }
        if ((parent == null) || (parent instanceof Component)) {
            result = JOptionPane.showConfirmDialog(searchForVisibleComponent((Component) parent), messageBuilder,
                    confirmationTitle, IDialog.YES_NO_OPTION);
        }
        return result;
    }
}
