/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.controller;

import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.definition.data.target.scalar.IScalarComponent;
import fr.soleil.data.adapter.NumberToStringAdapter;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.adapter.source.NumberToStringDataAdapter;
import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.controller.ScalarTargetController;
import fr.soleil.data.exception.UnhandledTargetSignatureException;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;
import fr.soleil.lib.project.ObjectUtils;

/**
 * This class allows you to connect a {@link ITarget} which can read a scalar T with a {@link AbstractDataSource}
 * sending a scalar U. This version provide a {@link AbstractCaps} handling
 * 
 * @author huriez
 * 
 * @param <T> the type of the {@link ITarget}
 * @param <U> the type of the {@link IDataContainer}
 */
public class ScalarCometeController<U, T> extends ScalarTargetController<U, T> {

    public ScalarCometeController(Class<U> sourceType, Class<T> targetType) {
        this(null, sourceType, targetType);
    }

    public ScalarCometeController(Class<?> classTarget, Class<U> typeSource, Class<T> typeTarget) {
        super(classTarget, typeSource, typeTarget);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean isTargetAssignable(ITarget target) {
        boolean result = false;
        if (target != null && target instanceof CometeCaps) {
            GenericDescriptor targetDesc = ((CometeCaps) target).getDataType();
            result = getTargetType().isAssignableFrom(targetDesc);
        }
        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void sendDataToTargetUsingSignature(ITarget target, T targetValue, AbstractDataSource<U> source) {
        if (target instanceof CometeCaps) {
            ((CometeCaps<T, IScalarComponent>) target).setData(targetValue, source);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void sendDataToTarget(ITarget target, T targetValue, AbstractDataSource<U> source) {
        if (target instanceof CometeCaps) {
            ((CometeCaps<T, IScalarComponent>) target).setData(targetValue, source);
        } else {
            super.sendDataToTarget(target, targetValue, source);
        }
    }

    @Override
    protected TargetSignature<T> initSignature() throws UnhandledTargetSignatureException {
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public DataSourceAdapter<U, T> createAdapter(AbstractDataSource<U> source) {
        DataSourceAdapter<U, T> adapter;
        if (targetType.equals(String.class) && ObjectUtils.isNumberClass(sourceType)
                && ObjectUtils.isNumberClass(source.getDataType().getConcernedClass())) {
            adapter = new NumberToStringDataAdapter(source, new NumberToStringAdapter<Number>(Number.class));
        } else {
            adapter = super.createAdapter(source);
        }
        return adapter;
    }

    /**
     * Returns the {@link NumberToStringDataAdapter} associated with the a particular {@link AbstractDataSource}. This
     * method is available because a {@link NumberToStringDataAdapter} is also an {@link ITarget}
     * 
     * @param source The {@link AbstractDataSource}
     * @return The corresponding {@link NumberToStringDataAdapter}, or <code>null</code> is there is
     *         no such adapter
     */
    @SuppressWarnings("unchecked")
    public NumberToStringDataAdapter<Number> getAdapter(AbstractDataSource<U> source) {
        NumberToStringDataAdapter<Number> result = null;
        DataSourceAdapter<U, T> adapter = adapters.get(source);
        if (adapter instanceof NumberToStringDataAdapter) {
            result = (NumberToStringDataAdapter<Number>) adapter;
        }
        return result;
    }

}