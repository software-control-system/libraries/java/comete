/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.controller;

import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.definition.data.target.matrix.IMatrixComponent;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.controller.NumberMatrixController;
import fr.soleil.data.exception.UnhandledTargetSignatureException;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;
import fr.soleil.data.target.matrix.INumberMatrixTarget;

public class NumberMatrixCometeController extends NumberMatrixController {

    public NumberMatrixCometeController() {
        super(null);
    }

    @Override
    protected TargetSignature<Object[]> initSignature() throws UnhandledTargetSignatureException {
        return null;
    }

    @Override
    public boolean isTargetAssignable(ITarget target) {
        return target instanceof INumberMatrixTarget;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void sendDataToTargetUsingSignature(ITarget target, Object[] targetValue,
            AbstractDataSource<AbstractNumberMatrix<?>> source) {
        if (target instanceof CometeCaps) {
            ((CometeCaps<Object[], IMatrixComponent>) target).setData(targetValue, source);
        } else if (target instanceof INumberMatrixTarget) {
            ((INumberMatrixTarget) target).setNumberMatrix(targetValue);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void sendDataToTarget(ITarget target, Object[] targetValue,
            AbstractDataSource<AbstractNumberMatrix<?>> source) {
        if (target instanceof CometeCaps) {
            ((CometeCaps<Object[], IMatrixComponent>) target).setData(targetValue, source);
        } else if (target instanceof INumberMatrixTarget) {
            ((INumberMatrixTarget) target).setNumberMatrix(targetValue);
        } else {
            super.sendDataToTarget(target, targetValue, source);
        }
    }
}
