/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.controller;

import fr.soleil.comete.caps.complexcaps.TreeNodeCaps;
import fr.soleil.comete.definition.data.target.complex.ITreeNodeTarget;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;

public class TreeNodeController<U> extends ScalarCometeController<U, ITreeNode> {

    public TreeNodeController(Class<U> sourceType) {
        super(ITreeNodeTarget.class, sourceType, ITreeNode.class);
    }

    @Override
    protected void sendDataToTargetUsingSignature(ITarget target, ITreeNode targetValue, AbstractDataSource<U> source) {
        if (target instanceof TreeNodeCaps) {
            ((TreeNodeCaps) target).setData(targetValue, source);
        }
    }

    @Override
    protected void sendDataToTarget(ITarget target, ITreeNode targetValue, AbstractDataSource<U> source) {
        if (target instanceof TreeNodeCaps) {
            ((TreeNodeCaps) target).setData(targetValue, source);
        } else {
            super.sendDataToTarget(target, targetValue, source);
        }
    }

}
