/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.controller;

import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.definition.data.target.matrix.IMatrixComponent;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.controller.MatrixTargetController;
import fr.soleil.data.exception.UnhandledTargetSignatureException;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;

/**
 * This class allows you to connect a {@link ITarget} which can read an array of {@link Object} of
 * rank 2 with a {@link AbstractDataSource} sending a {@link Matrix} of type U. This version can
 * handle {@link AbstractCaps}.
 * 
 * @author huriez
 * 
 * @param <U> The type of data the {@link AbstractDataSource} can manipulate
 * @param <T> The type of data the {@link ITarget} can manipulate
 * 
 */
public class MatrixCometeController<U, T> extends MatrixTargetController<U, T> {

    /**
     * Constructor
     * 
     * @param typeSource the primitive type of the {@link AbstractDataSource}
     * @param typeTarget the primitive type of the {@link ITarget}
     */
    public MatrixCometeController(Class<U> source, Class<T> typeTarget) {
        super(null, source, typeTarget);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean isTargetAssignable(ITarget target) {
        boolean result = false;
        if (target != null && target instanceof CometeCaps) {
            GenericDescriptor targetDesc = ((CometeCaps) target).getDataType();
            result = getTargetType().isAssignableFrom(targetDesc);
        }
        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void sendDataToTargetUsingSignature(ITarget target, Object[] targetValue,
            AbstractDataSource<AbstractMatrix<U>> source) {
        if (target instanceof CometeCaps) {
            ((CometeCaps<Object[], IMatrixComponent>) target).setData(targetValue, source);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void sendDataToTarget(ITarget target, Object[] targetValue,
            AbstractDataSource<AbstractMatrix<U>> source) {
        if (target instanceof CometeCaps) {
            ((CometeCaps<Object[], IMatrixComponent>) target).setData(targetValue, source);
        } else {
            super.sendDataToTarget(target, targetValue, source);
        }
    }

    @Override
    protected TargetSignature<Object[]> initSignature() throws UnhandledTargetSignatureException {
        return null;
    }

}
