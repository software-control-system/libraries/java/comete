/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.util;

import java.util.List;

/**
 * Tool class to interact with {@link List}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ListUtils {

    /**
     * Adds all elements not already present from a {@link List} in another one
     * 
     * @param from The {@link List} from which to get the elements to add
     * @param to The {@link List} to which to add the elements, if not already present
     */
    public static <T> void addAll(List<T> from, List<? super T> to) {
        if ((from != null) && (to != null)) {
            for (T data : from) {
                if (!to.contains(data)) {
                    to.add(data);
                }
            }
        }
    }

}
