/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Tool class to interact with {@link Map}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MapUtils {

    /**
     * Extracts the {@link Map} keys, using synchronization
     * 
     * @param map The {@link Map} from which to extract the keys
     * @return A {@link List}
     */
    public static <U, V> List<U> extractKeys(Map<U, V> map) {
        List<U> keys = new ArrayList<U>();
        if (map != null) {
            synchronized (map) {
                keys.addAll(map.keySet());
            }
        }
        return keys;
    }

}
