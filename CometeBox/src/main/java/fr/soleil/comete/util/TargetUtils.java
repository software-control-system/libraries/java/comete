/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.util;

import fr.soleil.comete.definition.data.target.scalar.IEditableComponent;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.service.thread.NoDrawingThreadManager;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IDrawableTarget;
import fr.soleil.data.target.ITarget;

/**
 * A class used to interact with {@link ITarget}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TargetUtils {

    /**
     * Update the "editable" state of the target in function of the settable capability of the
     * source.
     * 
     * @param source The source
     * @param target The target
     * @param userEditable Whether target is considered as editable by user
     * @param editableComponentManagementEnabled Whether this functionality is enabled
     */
    public static void updateTargetAvailabilities(AbstractDataSource<?> source, ITarget target, boolean userEditable,
            boolean editableComponentManagementEnabled) {
        if (editableComponentManagementEnabled && (target instanceof IEditableComponent)) {
            final IEditableComponent editableComponent = (IEditableComponent) target;
            if (userEditable) {
                if (source != null) {
                    editableComponent.setEditable(source.isSettable());
                }
            } else {
                editableComponent.setEditable(false);
            }
        }

    }

    /**
     * Recovers the best possible {@link IDrawingThreadManager} for an {@link IDrawableTarget}.
     * 
     * @param target The {@link IDrawableTarget}.
     * @return An {@link IDrawingThreadManager}, never <code>null</code>
     */
    public static IDrawingThreadManager getNotNullDrawingThreadManager(IDrawableTarget target) {
        IDrawingThreadManager drawingThreadManager;
        if (target == null) {
            drawingThreadManager = NoDrawingThreadManager.INSTANCE;
        } else {
            drawingThreadManager = target.getDrawingThreadManager();
            if (drawingThreadManager == null) {
                drawingThreadManager = NoDrawingThreadManager.INSTANCE;
            }
        }
        return drawingThreadManager;
    }

    /**
     * Recovers the best possible {@link IDrawingThreadManager} for an {@link ITarget}.
     * 
     * @param target The {@link ITarget}.
     * @return An {@link IDrawingThreadManager}, never <code>null</code>
     */
    public static IDrawingThreadManager getNotNullDrawingThreadManager(ITarget target) {
        IDrawingThreadManager drawingThreadManager;
        if (target instanceof IDrawableTarget) {
            drawingThreadManager = getNotNullDrawingThreadManager((IDrawableTarget) target);
        } else {
            drawingThreadManager = NoDrawingThreadManager.INSTANCE;
        }
        return drawingThreadManager;
    }

}
