/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.target;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.service.ISourceListener;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.IGenericTarget;
import fr.soleil.data.target.TargetDelegate;

public class ListenableTarget<T> implements IGenericTarget<T> {

    protected T data;
    protected TargetDelegate delegate = new TargetDelegate();
    protected List<ISourceListener<T>> listeners = new ArrayList<ISourceListener<T>>();

    @Override
    public T getTargetData() {
        return data;
    }

    @Override
    public void setTargetData(T data) {
        synchronized (listeners) {
            for (ISourceListener<T> listener : listeners) {
                listener.onSourceChange(data);
            }
        }
    }

    public void addSourceListener(ISourceListener<T> listener) {
        if (listener != null) {
            synchronized (listeners) {
                if (!listeners.contains(listener)) {
                    listeners.add(listener);
                }
            }
        }
    }

    public void removeSourceListener(ISourceListener<T> listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    @Override
    public GenericDescriptor getTargetType() {
        GenericDescriptor result = null;
        if (data != null) {
            result = new GenericDescriptor(data.getClass());
        }
        return result;
    }

    public boolean hasListeners() {
        boolean result = false;
        synchronized (listeners) {
            result = !listeners.isEmpty();
        }
        return result;
    }
}
