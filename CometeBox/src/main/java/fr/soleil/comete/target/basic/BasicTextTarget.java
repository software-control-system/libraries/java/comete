/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.target.basic;

import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.data.target.scalar.ITextTarget;

public class BasicTextTarget implements ITextTarget, IExecutableTarget {

    protected String value;
    protected final TargetDelegate delegate = new TargetDelegate();

    @Override
    public String getText() {
        return value;
    }

    @Override
    public void setText(String text) {
        value = text;
        execute();
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);

    }

    public <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public void execute() {
        warnMediators(new TextInformation(this, value));
    }

    @Override
    public String toString() {
        return (value == null) ? super.toString() : value;
    }

}
