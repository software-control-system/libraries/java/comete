/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.target.basic;

import java.lang.reflect.Array;

import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.MatrixInformation;
import fr.soleil.data.target.matrix.ITextMatrixTarget;

public class BasicStringMatrixTarget implements ITextMatrixTarget, IExecutableTarget {

    protected StringMatrix matrix;
    protected final TargetDelegate delegate = new TargetDelegate();

    @Override
    public String[] getFlatStringMatrix() {
        String[] result = null;
        if (matrix != null) {
            result = matrix.getFlatValue();
        }
        return result;
    }

    @Override
    public String[][] getStringMatrix() {
        String[][] result = null;
        if (matrix != null) {
            result = matrix.getValue();
        }
        return result;
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        if (matrix != null) {
            matrix.setFlatValue(value, width, height);
        }
    }

    @Override
    public void setStringMatrix(String[][] value) {
        if (matrix != null) {
            matrix.setValue(value);
        }
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        int height = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if (value != null) {
                height = value.length;
            }
        }
        return height;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int width = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if ((value != null) && (value.length > 0)) {
                width = Array.getLength(value[0]);
            }
        }
        return width;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);

    }

    @Override
    public void execute() {
        delegate.warnMediators(new MatrixInformation(this, matrix.getFlatValue()));
    }
}
