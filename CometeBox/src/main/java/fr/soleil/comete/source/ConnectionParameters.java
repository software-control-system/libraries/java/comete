/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.source;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.ITarget;
import fr.soleil.lib.project.ObjectUtils;

public class ConnectionParameters {

    private AbstractCometeBox<?> box;
    private ITarget widget;
    private IKey key;
    private final Collection<AbstractDataFilter<?>> sourceFilters;
    private final Collection<AbstractDataFilter<?>> targetFilters;
    private final boolean plugins;

    public ConnectionParameters(AbstractCometeBox<?> box, ITarget widget, IKey key, boolean plugins) {
        super();
        this.box = box;
        this.widget = widget;
        this.key = key;
        this.plugins = plugins;
        sourceFilters = Collections.newSetFromMap(new LinkedHashMap<AbstractDataFilter<?>, Boolean>());
        targetFilters = Collections.newSetFromMap(new LinkedHashMap<AbstractDataFilter<?>, Boolean>());
    }

    public AbstractCometeBox<?> getBox() {
        return box;
    }

    public void setBox(AbstractCometeBox<IComponent> box) {
        this.box = box;
    }

    public ITarget getWidget() {
        return widget;
    }

    public void setWidget(IComponent widget) {
        this.widget = widget;
    }

    public IKey getKey() {
        return key;
    }

    public void setKey(IKey key) {
        this.key = key;
    }

    public boolean isPlugins() {
        return plugins;
    }

    public void addSourceFilters(AbstractDataFilter<?>... sourceSourceFilters) {
        if (sourceSourceFilters != null) {
            synchronized (this.sourceFilters) {
                for (AbstractDataFilter<?> sourceSourceFilter : sourceSourceFilters) {
                    if (sourceSourceFilter != null) {
                        this.sourceFilters.add(sourceSourceFilter);
                    }
                }
            }
        }
    }

    public void removeSourceFilters(AbstractDataFilter<?>... sourceSourceFilters) {
        if (sourceSourceFilters != null) {
            synchronized (this.sourceFilters) {
                for (AbstractDataFilter<?> sourceSourceFilter : sourceSourceFilters) {
                    if (sourceSourceFilter != null) {
                        this.sourceFilters.remove(sourceSourceFilter);
                    }
                }
            }
        }
    }

    public Collection<AbstractDataFilter<?>> getSourceFilters() {
        Collection<AbstractDataFilter<?>> copy = new ArrayList<AbstractDataFilter<?>>();
        synchronized (sourceFilters) {
            copy.addAll(sourceFilters);
        }
        return copy;
    }

    public void clearSourceFilters() {
        synchronized (sourceFilters) {
            sourceFilters.clear();
        }
    }

    public void addTargetFilters(AbstractDataFilter<?>... targetFilters) {
        if (targetFilters != null) {
            synchronized (this.targetFilters) {
                for (AbstractDataFilter<?> targetTargetFilter : targetFilters) {
                    if (targetTargetFilter != null) {
                        this.targetFilters.add(targetTargetFilter);
                    }
                }
            }
        }
    }

    public void removeTargetFilters(AbstractDataFilter<?>... targetFilters) {
        if (targetFilters != null) {
            synchronized (this.targetFilters) {
                for (AbstractDataFilter<?> targetTargetFilter : targetFilters) {
                    if (targetTargetFilter != null) {
                        this.targetFilters.remove(targetTargetFilter);
                    }
                }
            }
        }
    }

    public Collection<AbstractDataFilter<?>> getTargetFilters() {
        Collection<AbstractDataFilter<?>> copy = new ArrayList<AbstractDataFilter<?>>();
        synchronized (targetFilters) {
            copy.addAll(targetFilters);
        }
        return copy;
    }

    public void clearTargetFilters() {
        synchronized (targetFilters) {
            targetFilters.clear();
        }
    }

    @Override
    public int hashCode() {
        int code = 0xC022;
        int mult = 0xEC7;
        code = code * mult + ObjectUtils.getHashCode(getBox());
        code = code * mult + ObjectUtils.getHashCode(getKey());
        code = code * mult + ObjectUtils.getHashCode(getWidget());
        return code;
    }

    @Override
    public boolean equals(Object object) {
        boolean result = false;
        // For equality test, we don't care about "plugins", to avoid dual reconnections (with and
        // without plugins)
        if (object instanceof ConnectionParameters) {
            ConnectionParameters connectionParameters = (ConnectionParameters) object;
            result = ObjectUtils.sameObject(getBox(), connectionParameters.getBox());
            result = result && ObjectUtils.sameObject(getKey(), connectionParameters.getKey());
            result = result && ObjectUtils.sameObject(getWidget(), connectionParameters.getWidget());
        }
        return result;
    }

    @Override
    public String toString() {
        return "Widget :" + widget.getClass() + " Key : " + key.getInformationKey() + " Box : " + box.getClass();
    }

}
