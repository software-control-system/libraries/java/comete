/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.source;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.service.ITargetListener;
import fr.soleil.data.source.BasicDataSource;

/**
 * A {@link BasicDataSource} that can be listened.
 */
public class ListenableDataSource<U> extends BasicDataSource<U> {

    protected List<ITargetListener<U>> listeners = new ArrayList<ITargetListener<U>>();

    public ListenableDataSource() {
        super(null);
    }

    @Override
    public void setData(U data) {
        this.data = data;
        synchronized (listeners) {
            for (ITargetListener<U> listener : listeners) {
                listener.onTargetOutput(data);
            }
        }
    }

    public void addTargetListener(ITargetListener<U> listener) {
        if (listener != null) {
            synchronized (listeners) {
                if (!listeners.contains(listener)) {
                    listeners.add(listener);
                }
            }
        }
    }

    public void removeTargetListener(ITargetListener<U> listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    public boolean hasListeners() {
        boolean result = false;
        synchronized (listeners) {
            result = !listeners.isEmpty();
        }
        return result;
    }
}
