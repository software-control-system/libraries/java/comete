/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.source;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.ITarget;
import fr.soleil.lib.project.ObjectUtils;

public class DataSourceReconnectionManager {

    private static final Collection<ConnectionParameters> badConnectionParameters = Collections
            .newSetFromMap(new ConcurrentHashMap<ConnectionParameters, Boolean>());
    private static volatile boolean started = false;
    private static volatile boolean interruptedReconnection = false;
    private static ConnectionThread thread;
    private static Semaphore semaphore = new Semaphore(1);

    public static void forgetBadConnection(ITarget widget, AbstractCometeBox<? extends ITarget> box) {
        interruptedReconnection = true;
        List<ConnectionParameters> toRemove = new ArrayList<ConnectionParameters>();
        for (ConnectionParameters parameter : badConnectionParameters) {
            if (ObjectUtils.sameObject(parameter.getBox(), box)
                    && (ObjectUtils.sameObject(parameter.getWidget(), widget))) {
                toRemove.add(parameter);
            }
        }
        badConnectionParameters.removeAll(toRemove);
        toRemove.clear();
    }

    public static void registerBadConnection(ITarget widget, IKey key, AbstractCometeBox<? extends ITarget> box,
            boolean withPlugins) {
        ConnectionParameters params = new ConnectionParameters(box, widget, key, withPlugins);
        badConnectionParameters.add(params);

        // start thread if needed
        if (!started) {
            try {
                // Synchronized Access starts here
                semaphore.acquire();
                if (thread == null) {
                    thread = new ConnectionThread();
                    thread.start();
                    started = true;
                }
                semaphore.release();
                // End of Synchronized Access
            } catch (InterruptedException e) {
                // No need to log this interruption
            }
        }
    }

    public static <V> void addSourceFilter(ITarget widget, AbstractDataFilter<V> filter,
            AbstractCometeBox<? extends ITarget> box) {
        for (ConnectionParameters params : badConnectionParameters) {
            if ((box == params.getBox()) && (widget == params.getWidget())) {
                params.addSourceFilters(filter);
            }
        }
    }

    public static <V> void removeSourceFilter(ITarget widget, AbstractDataFilter<V> filter,
            AbstractCometeBox<? extends ITarget> box) {
        for (ConnectionParameters params : badConnectionParameters) {
            if ((box == params.getBox()) && (widget == params.getWidget())) {
                params.removeSourceFilters(filter);
            }
        }
    }

    public static <V> void addTargetFilter(ITarget widget, AbstractDataFilter<V> filter,
            AbstractCometeBox<? extends ITarget> box) {
        for (ConnectionParameters params : badConnectionParameters) {
            if ((box == params.getBox()) && (widget == params.getWidget())) {
                params.addTargetFilters(filter);
            }
        }
    }

    public static <V> void removeTargetFilter(ITarget widget, AbstractDataFilter<V> filter,
            AbstractCometeBox<? extends ITarget> box) {
        for (ConnectionParameters params : badConnectionParameters) {
            if ((box == params.getBox()) && (widget == params.getWidget())) {
                params.removeTargetFilters(filter);
            }
        }
    }

//    public synchronized void removeBadConnection(IComponent widget, IKey key, AbstractCometeBox<IComponent> box) {
//        // This is not a mistake
//        // Please see ConnectionParameters.equals()
//        badConnectionParameters.remove(new ConnectionParameters(box, widget, key, true));
//        badConnectionParameters.remove(new ConnectionParameters(box, widget, key, false));
//    }

    protected static class ConnectionThread extends Thread {

        public ConnectionThread() {
            super(DataSourceReconnectionManager.class.getSimpleName() + " ConnectionThread");
        }

        @Override
        public void run() {
            while (true) {
                List<ConnectionParameters> copy = new ArrayList<ConnectionParameters>(badConnectionParameters.size());
                copy.addAll(badConnectionParameters);
                for (ConnectionParameters params : copy) {
                    @SuppressWarnings("unchecked")
                    AbstractCometeBox<ITarget> box = (AbstractCometeBox<ITarget>) params.getBox();
                    ITarget widget = params.getWidget();
                    IKey key = params.getKey();
                    if (interruptedReconnection) {
                        break;
                    }
                    if (box.connectWidget(widget, key, false, params.isPlugins())) {
                        Collection<AbstractDataFilter<?>> sourceFilters = params.getSourceFilters();
                        Collection<AbstractDataFilter<?>> targetFilters = params.getTargetFilters();
                        for (AbstractDataFilter<?> filter : sourceFilters) {
                            box.addFilterToSourceFromTarget(filter, widget, false);
                        }
                        params.clearSourceFilters();
                        for (AbstractDataFilter<?> filter : targetFilters) {
                            box.addFilterToTarget(filter, widget, false);
                        }
                        params.clearTargetFilters();
                        badConnectionParameters.remove(params);
                    }
                }
                copy.clear();
                interruptedReconnection = false;
                try {
                    // WAIT 5 seconds for a new connection attempt
                    sleep(5000);
                } catch (InterruptedException e) {
                    // No need to log this interruption
                }
            }
        }
    }
}
