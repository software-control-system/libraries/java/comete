/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box.matrixbox;

import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.caps.matrixcaps.BooleanMatrixCaps;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.matrix.IBooleanMatrixTarget;

public class BooleanMatrixBox extends AbstractMatrixBox<IBooleanMatrixTarget> {

    @Override
    protected Class<IBooleanMatrixTarget> getTargetClassId() {
        return IBooleanMatrixTarget.class;
    }

    @Override
    protected GenericDescriptor getTargetType() {
        return new GenericDescriptor(boolean[][].class);
    }

    @Override
    protected CometeCaps<?, IBooleanMatrixTarget> initTargetCaps(IBooleanMatrixTarget target) {
        return new BooleanMatrixCaps(target, this);
    }

    @Override
    protected Class<?> getPrimitiveTargetType() {
        return Boolean.TYPE;
    }

    @Override
    protected void cleanWidget(IBooleanMatrixTarget target) {
        try {
            target.setBooleanMatrix(null);
        } catch (Exception e) {
            // ignore exception
        }
    }

}
