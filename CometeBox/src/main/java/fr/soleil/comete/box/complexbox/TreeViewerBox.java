/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box.complexbox;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.caps.complexcaps.TreeNodeCaps;
import fr.soleil.comete.controller.TreeNodeController;
import fr.soleil.comete.definition.data.target.complex.ITreeNodeTarget;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.service.GenericDescriptor;

public class TreeViewerBox extends AbstractCometeBox<ITreeNodeTarget> {

    @Override
    protected void cleanWidget(ITreeNodeTarget target) {
        // target.cleanTree();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <U> AbstractController<U> initController(Class<?> targetClass, GenericDescriptor sourceType) {
        return new TreeNodeController(extractSourceClass(sourceType));
    }

    @Override
    protected CometeCaps<?, ITreeNodeTarget> initTargetCaps(ITreeNodeTarget target) {
        return new TreeNodeCaps(target, this);
    }

    @Override
    protected Class<ITreeNodeTarget> getTargetClassId() {
        return ITreeNodeTarget.class;
    }

    @Override
    protected GenericDescriptor getTargetType() {
        return new GenericDescriptor(ITreeNode.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <U> AbstractController<U> initBasicController(GenericDescriptor sourceType) {
        return new TreeNodeController(extractSourceClass(sourceType));
    }

    @SuppressWarnings("unchecked")
    protected <U> Class<U> extractSourceClass(GenericDescriptor sourceType) {
        return (sourceType == null ? null : (Class<U>) sourceType.getConcernedClass());
    }

    public void setUseSelectionToWrite(ITreeNodeTarget target, boolean value) {
        CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if ((targetCaps != null) && targetCaps instanceof TreeNodeCaps) {
            ((TreeNodeCaps) targetCaps).setUseSelectionToWrite(value);
        }
    }

    public boolean isUseSelectionToWrite(ITreeNodeTarget target) {
        boolean result = true;
        CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if ((targetCaps != null) && targetCaps instanceof TreeNodeCaps) {
            result = ((TreeNodeCaps) targetCaps).isUseSelectionToWrite();
        }
        return result;
    }

    public void setUseChildren(ITreeNodeTarget target, boolean value) {
        CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if ((targetCaps != null) && targetCaps instanceof TreeNodeCaps) {
            ((TreeNodeCaps) targetCaps).setUseChildren(value);
        }
    }

    public boolean isUseChildren(ITreeNodeTarget target) {
        boolean result = false;
        CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if ((targetCaps != null) && targetCaps instanceof TreeNodeCaps) {
            result = ((TreeNodeCaps) targetCaps).isUseChildren();
        }
        return result;
    }
}
