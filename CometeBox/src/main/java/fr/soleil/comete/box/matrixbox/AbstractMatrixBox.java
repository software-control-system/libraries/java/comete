/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box.matrixbox;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.controller.MatrixCometeController;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.controller.DataTargetController;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.matrix.IMatrixTarget;
import fr.soleil.lib.project.math.ArrayUtils;

public abstract class AbstractMatrixBox<T extends IMatrixTarget> extends AbstractCometeBox<T> {

    @SuppressWarnings("unchecked")
    @Override
    public <U> AbstractController<U> initController(Class<?> targetClass, GenericDescriptor sourceType) {
        AbstractController<U> result = (AbstractController<U>) initMatrixController(targetClass,
                ArrayUtils.recoverDeepComponentType(sourceType.getPrimitiveType()), getPrimitiveTargetType());

        return result;
    }

    protected <U, T2> DataTargetController<AbstractMatrix<U>, Object[]> initMatrixController(Class<?> targetClass,
            Class<U> sourceType, Class<T2> targetType) {

        DataTargetController<AbstractMatrix<U>, Object[]> result = new MatrixCometeController<U, T2>(sourceType,
                targetType);

        return result;
    }

    @Override
    protected <U> AbstractController<U> initBasicController(GenericDescriptor sourceType) {
        return null;
    }

    protected abstract Class<?> getPrimitiveTargetType();
}
