/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.ThrowableProxy;
import ch.qos.logback.core.AppenderBase;
import fr.soleil.comete.box.scalarbox.ScalarCometeBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.definition.data.controller.ScalarComponentController;
import fr.soleil.comete.definition.data.plugin.ComponentColorPlugin;
import fr.soleil.comete.definition.data.target.scalar.IEditableComponent;
import fr.soleil.comete.definition.data.target.scalar.ITextComponent;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IFileBrowserTextFieldButton;
import fr.soleil.comete.definition.widget.ITextFieldButton;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.source.DataSourceReconnectionManager;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.util.ListUtils;
import fr.soleil.comete.util.MapUtils;
import fr.soleil.comete.util.TargetUtils;
import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.controller.DataTargetController;
import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.data.filter.AbstractChecker;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.plugin.AbstractPlugin;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.DataControllerProvider;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IConstantSourceProducer;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.ILoggingSystem;
import fr.soleil.data.service.LoggingSystemDelegate;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.source.IPluggableDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.swing.dialog.StaticErrorDialog;

/**
 * <p>
 * Abstraction for all the Comete boxes. It mainly contains the process to create a complete {@link IComponent}, already
 * linked with his source, integrated into the method {@link AbstractCometeBox#createAdvancedWidget(Class, IKey)}.
 * </p>
 * 
 * <p>
 * A common "box" define the controller and the source operations with the {@link IComponent} you want to create. For
 * example, to create an {@link ITextComponent}, you'll need a {@link ScalarComponentController} and an
 * {@link AbstractDataSource} with a {@link String} parameter for both. The {@link StringScalarBox} provide theses
 * object, so you can create it.
 * </p>
 * 
 * <table border="1">
 * <thead>
 * <tr>
 * <td><big>Example of using :</big></td>
 * </tr>
 * </thead> <tbody>
 * <tr>
 * <td><code>
 *      <p>// first you need to create the right box</br>
 *      StringScalarBox stringBox = new StringScalarBox();</p>
 * 
 *      <p> // then, you have to create the IKey you want to use for your source</br>
 *      TangoKey key = new TangoKey();</br>
 *      TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "string_scalar");</p>
 * 
 *      <p> // at last, you can create your IComponent with </br>
 *      // the class of your wished widget and the donate IKey</br>
 *      TextField textfield = stringBox.createAdvancedWidget(TextField.class, key);</br>
 *      TextArea textArea = stringBox.createAdvancedWidget(TextArea.class, key);</br>
 *      FileBrowser fileBrowser = stringBox.createAdvancedWidget(FileBrowser.class, key);</p>
 * </code></td>
 * </tr>
 * </tbody>
 * </table>
 * 
 * <p>
 * The method {@link AbstractCometeBox#createAdvancedWidget(Class, IKey)} uses the target type to create a source, so
 * you have no need for data adaptation.</br>
 * If you want to use a different kind of source (for example, a String instead of a Number), you can use the method
 * {@link AbstractCometeBox#createAdvancedWidget(Class, IKey, Class)}
 * </p>
 * 
 * @author huriez
 * 
 * @param <T> the type of {@link IComponent} this {@link AbstractCometeBox} can create
 */
public abstract class AbstractCometeBox<T extends ITarget> implements ILoggingSystem {

    private static final String CONNECTION_ERROR = "Connection error: ";
    private static final String WITH = " With:";
    private static final String CONNECTION_FOR = "connection for";
    private static final String AN_ERROR_OCCURRED_FOR_THE_CONNECTION_OF = "An error occurred for the connection of:\n";
    private static final String ERROR_TYPE = "\nError type: ";
    private static final String ERROR_MESSAGE = "\nError message: ";
    private static final String DISCONNECT_ALL_REFERENCIES_FROM = "disconnect all referencies from";
    private static final String DISCONNECT_FROM = "disconnect from";
    private static final String EQUIVALENT = "<=>";
    private static final String HYPHEN = "-";
    private static final String TAB = "\t";
    private static final String PLUGINS = "\tPlugins: ";
    private static final String COMMA = ", ";
    private static final String NEW_LINE_TAB = "\n\t";
    private static final String GLOBAL_SEPARATOR = "***********************************************\n";
    private static final String TOTAL_NUMBER_OF_CONNECTED_TARGETS = ", total number of connected targets: ";
    private static final String NEW_LINE_NEW_LINE = "\n\n";
    private static final String LIST_OF_ALL_REGISTERED_CONNECTIONS = "LIST OF ALL REGISTERED CONNECTIONS: \n\n";
    private static final String TARGET_SEPARATOR = "-----------------------------------------\n\n";
    private static final String AT = "At ";
    private static final String COLON = ": ";
    private static final String NULL = "null";

    public static final int AUTO_RECONNECT_BAD_CONNECTION = 0;
    public static final int CONNECTION_WITH_PLUGINS = 1;
    public static final int READ_IMMEDIATELY = 2;
    public static final String LOG_HEADER = "CONNECTION LOG:\n\n";
    protected static final CometeHandler COMETE_HANDLER = new CometeHandler();
    protected static final Logger LOGGER = LoggerFactory.getLogger(Mediator.LOGGER_ACCESS);
    protected static LoggingMode loggingMode = LoggingMode.NOTHING;

    static {
        if (LOGGER instanceof ch.qos.logback.classic.Logger) {
            ((ch.qos.logback.classic.Logger) LOGGER).addAppender(COMETE_HANDLER);
        }
    }

    private final long startTime;
    private final StringBuilder connectionsLog;

    private final List<AbstractPlugin<?>> listPluginController;

    protected final Map<T, List<IKey>> mainConnections;
    protected final Map<T, List<AbstractPlugin<?>>> linkedPlugins;
    protected final Map<T, CometeCaps<?, T>> capsMap;

    protected boolean sourcePluginAllowed;
    protected boolean firstInitAllowed;
    protected boolean singleConnectionOnly;
    protected boolean defaultToolTip;

    protected final WeakHashMap<ITarget, Boolean> cleanWidgetMap;
    protected final WeakHashMap<IComponent, String> tooltipMap;

    protected final LoggingSystemDelegate loggingSystemDelegate;

    public AbstractCometeBox() {
        listPluginController = new ArrayList<>();
        mainConnections = new HashMap<>();
        linkedPlugins = new HashMap<>();
        capsMap = new HashMap<>();
        startTime = System.currentTimeMillis();
        sourcePluginAllowed = true;
        firstInitAllowed = true;
        singleConnectionOnly = false;
        defaultToolTip = true;
        connectionsLog = new StringBuilder(LOG_HEADER);
        cleanWidgetMap = new WeakHashMap<ITarget, Boolean>();
        tooltipMap = new WeakHashMap<IComponent, String>();
        loggingSystemDelegate = new LoggingSystemDelegate();
    }

    /**
     * <p>
     * Connect the widget parameter to an {@link AbstractDataSource} designed from the {@link IKey}, with the
     * {@link AbstractController} specified by this box.
     * </p>
     * 
     * @param <TI> the type of the widget you want to create
     * @param <U> the type of the source
     * @param widget the widget you want to connect
     * @param key the {@link IKey} that define the source you want to connect with the widget
     * @return true if the {@link ITarget} connection is successful, false otherwise
     */

    public <TI extends T, U> boolean connectWidget(final TI widget, final IKey key) {
        return connectWidget(widget, key, true, true);
    }

    public <TI extends T, U> boolean connectWidgetNoMetaData(final TI widget, final IKey key) {
        return connectWidget(widget, key, true, false);
    }

    protected boolean checkParam(boolean[] advancedParams, int index, boolean defaultValue) {
        boolean result = defaultValue;
        if ((advancedParams != null) && (index > -1) && (index < advancedParams.length)) {
            result = advancedParams[index];
        }
        return result;
    }

    /**
     * Connect the target parameter to an {@link AbstractDataSource} designed from the {@link IKey},
     * with the {@link AbstractController} specified by this box.
     * 
     * @param <TI> the type of the widget you want to create
     * @param <U> the type of the source
     * @param target the target you want to connect
     * @param key the {@link IKey} that define the source you want to connect with the widget
     * @param advancedConfiguration The advancedConfiguration for this connection.
     *            <p>
     *            Here, we expect following order:
     *            <ol start="0">
     *            <li><code>autoReconnectBadConnections</code>: Whether to try reconnecting on connection failed</li>
     *            <li><code>withPlugins</code>: Whether to connect target to source plugins</li>
     *            <li><code>immediately</code>: Whether to force source to read data immediately on creation</li>
     *            </ol>
     *            </p>
     * @return <code>true</code> if the {@link ITarget} connection is successful, <code>false</code> otherwise
     */
    public <TI extends T, U> boolean connectWidget(final TI target, final IKey key, boolean... advancedConfiguration) {
        boolean autoReconnectBadConnections = checkParam(advancedConfiguration, AUTO_RECONNECT_BAD_CONNECTION, true);
        boolean withPlugins = checkParam(advancedConfiguration, CONNECTION_WITH_PLUGINS, true);
        boolean immediately = checkParam(advancedConfiguration, READ_IMMEDIATELY, false);

        boolean result = false;
        if ((target != null) && (key != null)) {
            synchronized (mainConnections) {
                if (singleConnectionOnly && mainConnections.containsKey(target)) {
                    disconnectWidgetFromAll(target);
                }
            }
            try {
                // we encapsulate the target into an CometeCaps to add some sort of intelligence to it
                final CometeCaps<?, ?> connectedTarget = getTargetCaps(target);
                // we ask for the producer to give us the associated source
                final AbstractDataSource<U> source = getSource(key, immediately, false);
                if (source == null) {
                    // Deactivate widget if connected to no source
                    if ((connectedTarget != null) && (singleConnectionOnly || !isTargetConnected(target))) {
                        connectedTarget.setData(null, null);
                        connectedTarget.setInError();
                    }
                    computeDefaultToolTip(target, key);
                    if (autoReconnectBadConnections) {
                        DataSourceReconnectionManager.registerBadConnection(target, key, this, withPlugins);
                    }
                    final StringBuilder builder = new StringBuilder(CONNECTION_ERROR);
                    builder.append(target.getClass().getSimpleName());
                    builder.append(WITH);
                    builder.append(key.getInformationKey());
                    loggingSystemDelegate.log(java.util.logging.Level.SEVERE, builder.toString());
                } else {
                    boolean userEditable, editableManagementEnabled;
                    if (connectedTarget == null) {
                        userEditable = true;
                        editableManagementEnabled = true;
                    } else {
                        userEditable = connectedTarget.isEditable();
                        editableManagementEnabled = connectedTarget.isEditableComponentManagementEnabled();
                    }
                    TargetUtils.updateTargetAvailabilities(source, target, userEditable, editableManagementEnabled);

                    if (target instanceof IComponent) {
                        ((IComponent) target).setEnabled(isUserEnabled(target));
                    }

                    // We check first if a similar controller already exist, otherwise we create it
                    final AbstractController<U> controller = getController(source.getDataType(), getTargetType());
                    checkController(source.getDataType(), getTargetType(), controller);

                    if (controller != null) {
                        controller.setFirstInitAllowed(firstInitAllowed);
                        if (controller instanceof DataTargetController<?, ?>) {
                            ((DataTargetController<U, ?>) controller).registerAdapter(source);
                        }

                        // First, Connect plugins
                        if (withPlugins) {
                            initPlugins(source, target);
                        }

                        computeDefaultToolTip(target, source);

                        // registration of all usefull information about the connection
                        synchronized (mainConnections) {
                            List<IKey> connections = mainConnections.get(target);
                            if (connections == null) {
                                connections = new ArrayList<IKey>();
                                mainConnections.put(target, connections);
                            }
                            if (!connections.contains(key)) {
                                connections.add(key);
                            }
                        }

                        // Then, connect source (and check whether this connection is made)
                        if (connectedTarget.isActLikeCommand()) {
                            connectedTarget.setIgnoreData(true);
                        }

                        prepareController(controller, connectedTarget);
                        result = controller.addLink(source, connectedTarget);

                        // connection trace
                        printLog(CONNECTION_FOR, target, controller, source);
                    }
                }

            } catch (final Exception e) {
                final StringBuilder builder = new StringBuilder();
                builder.append(AN_ERROR_OCCURRED_FOR_THE_CONNECTION_OF);
                builder.append(target.getClass().getSimpleName());
                builder.append(WITH);
                builder.append(key.getInformationKey());
                builder.append(ERROR_TYPE);
                builder.append(e.getClass().getName());
                builder.append(ERROR_MESSAGE);
                builder.append(e.getMessage());
                loggingSystemDelegate.log(java.util.logging.Level.SEVERE, builder.toString(), e);
            }
        }
        return result;
    }

    protected void prepareController(AbstractController<?> controller, CometeCaps<?, ?> connectedTarget) {
        // nothing to do by default
    }

    /**
     * 
     * @param target the {@link ITarget} to encapsulate
     * @return the registered {@link CometeCaps}, the new one if no encapsulation exists
     */
    protected CometeCaps<?, T> getTargetCaps(final T target) {
        CometeCaps<?, T> result = null;
        if (target != null) {
            synchronized (capsMap) {
                result = capsMap.get(target);
                if (result == null) {
                    result = initTargetCaps(target);
                    if (result != null) {
                        capsMap.put(target, result);
                        prepareTargetOnFirstAccess(target, result);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Prepares an {@link ITarget} when it is accessed for the first time, through {@link #getTargetCaps(ITarget)}
     * 
     * @param target The {@link ITarget}
     * @param targetCaps The {@link ITarget}'s associated {@link CometeCaps}
     */
    protected void prepareTargetOnFirstAccess(final T target, final CometeCaps<?, ?> targetCaps) {
        // ITextFieldButtons are not settable by default (but IFileBrowserTextFieldButtons are settable)
        if ((target instanceof ITextFieldButton) && (!(target instanceof IFileBrowserTextFieldButton))) {
            targetCaps.setSettable(false);
        }
    }

    /**
     * Give a default tool tip to the {@link ITarget} that corresponding to a parameter
     * 
     * @param target The {@link ITarget}
     * @param param The parameter. If it is <code>null</code>, so is the tooltip. If it is an {@link IKey}, the tooltip
     *            will be {@link IKey#getInformationKey()}. Otherwise,
     *            it will be <code>parameter.toString()</code>
     */
    public void computeDefaultToolTip(final T target, final Object param) {
        if (defaultToolTip && (target instanceof IComponent)) {
            String tooltip;
            if (param == null) {
                tooltip = null;
            } else if (param instanceof IKey) {
                tooltip = ((IKey) param).getInformationKey();
            } else {
                tooltip = param.toString();
            }
            IComponent component = (IComponent) target;
            synchronized (tooltipMap) {
                // Once connected, store previous tooltip if not already stored (i.e. if not already connected)
                if (!tooltipMap.containsKey(component)) {
                    tooltipMap.put(component, component.getToolTipText());
                }
            }
            // Must be done in EDT (SCAN-976, PROBLEM-2582)
            IDrawingThreadManager manager = TargetUtils.getNotNullDrawingThreadManager(component);
            manager.runInDrawingThread(() -> {
                component.setToolTipText(tooltip);
            });
        }
    }

    /**
     * 
     * @param target the {@link ITarget} to check
     * @return true if the {@link ITarget} is connected to at least one {@link AbstractDataSource} using this
     *         {@link AbstractCometeBox}, false otherwise
     */
    public boolean isTargetConnected(final T target) {
        boolean result = false;
        if (target != null) {
            final CometeCaps<?, ?> caps;
            synchronized (capsMap) {
                caps = capsMap.get(target);
            }
            if (caps != null) {
                for (final AbstractController<?> controller : getUsedControllers()) {
                    if (controller.contains(caps)) {
                        result = true;
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * 
     * @param target the {@link ITarget} to check
     * @param key the {@link IKey} to check
     * @return true if the {@link ITarget} is connected to the {@link AbstractDataSource} related to
     *         the donate {@link IKey}, using this {@link AbstractCometeBox}. It returns false
     *         otherwise.
     */
    @SuppressWarnings("unchecked")
    public <U> boolean isTargetConnected(final T target, final IKey key) {
        boolean result = false;
        if (target != null) {
            final CometeCaps<?, ?> caps;
            synchronized (capsMap) {
                caps = capsMap.get(target);
            }
            if (caps != null) {
                for (final AbstractController<?> controller : getUsedControllers()) {
                    final List<AbstractDataSource<U>> listSources = ((AbstractController<U>) controller)
                            .getAllRelatedSources(caps);
                    for (final AbstractDataSource<U> source : listSources) {
                        if (ObjectUtils.sameObject(key, source.getOriginDescriptor())) {
                            result = true;
                            break;
                        }
                    }
                    if (result == true) {
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Return the plugins linked to the donate {@link ITarget} that extend the donate plugin class.
     * 
     * @param <P> the class of the {@link AbstractPlugin}
     * @param target The {@link ITarget}
     * @param pluginClass The {@link AbstractPlugin} {@link Class}
     * @return A not <code>null</code> {@link List}
     */
    protected final <P extends AbstractPlugin<?>> List<P> getPlugins(final T target, final Class<P> pluginClass) {
        List<AbstractPlugin<?>> listPlugin;
        synchronized (linkedPlugins) {
            listPlugin = linkedPlugins.get(target);
            if (listPlugin != null) {
                List<AbstractPlugin<?>> tmp = new ArrayList<AbstractPlugin<?>>();
                tmp.addAll(listPlugin);
                listPlugin = tmp;
            }
        }
        return getPlugins(listPlugin, pluginClass);
    }

    /**
     * Extracts the plugins of a given {@link Class} from a {@link List}
     * 
     * @param plugins The {@link List}
     * @param pluginClass The plugin {@link Class}
     * @return A not <code>null</code> {@link List}
     */
    @SuppressWarnings("unchecked")
    protected final <P extends AbstractPlugin<?>> List<P> getPlugins(final List<AbstractPlugin<?>> plugins,
            final Class<P> pluginClass) {
        List<P> result = new ArrayList<P>();
        if ((plugins != null) && (pluginClass != null)) {
            for (final AbstractPlugin<?> plugin : plugins) {
                if (pluginClass.isAssignableFrom(plugin.getClass())) {
                    result.add((P) plugin);
                }
            }
        }
        return result;
    }

    /**
     * Initialize the list of plugin the component has to be linked.
     * 
     * @param source the source we have to link to the component
     * @return the {@link List} of all the plugin to link
     */
    protected void initPlugins(final AbstractDataSource<?> source, final T target) {
        final List<AbstractPlugin<?>> result = new ArrayList<AbstractPlugin<?>>();

        synchronized (linkedPlugins) {
            List<AbstractPlugin<?>> tmp = linkedPlugins.get(target);
            if (tmp != null) {
                result.addAll(tmp);
            }
        }

        if ((source != null) && (target != null)) {
            // we retrieve all the plugin attached to the source if allowed
            if ((source != null) && sourcePluginAllowed && (source instanceof IPluggableDataSource)) {
                List<AbstractPlugin<?>> plugins = ((IPluggableDataSource) source).getListPlugin();
                ListUtils.addAll(plugins, result);
            }
            // then we add our own plugin
            synchronized (listPluginController) {
                ListUtils.addAll(listPluginController, result);
            }

            // We register all useful plugins by type
            searchForKnownPlugins(target, result);

            filterPlugins(result, source, target);

            synchronized (linkedPlugins) {
                linkedPlugins.put(target, result);
            }
        }
    }

    /**
     * Filters the plugins that can be connected to a given {@link ITarget}
     * 
     * @param plugins The plugin list to filter
     * @param source The {@link AbstractDataSource} from which the plugins came
     * @param target The {@link ITarget} for which to filter the plugins
     */
    protected void filterPlugins(final List<AbstractPlugin<?>> plugins, final AbstractDataSource<?> source,
            final T target) {
        CometeCaps<?, T> caps = getTargetCaps(target);
        final List<AbstractPlugin<?>> toRemove = new ArrayList<AbstractPlugin<?>>();
        for (final AbstractPlugin<?> plugin : plugins) {
            boolean connectionState;
            plugin.setFirstInitAllowed(firstInitAllowed);
            if (plugin.addPlugin(caps)) {
                connectionState = true;
            } else {
                connectionState = plugin.addPlugin(target);
            }
            if (!connectionState) {
                toRemove.add(plugin);
            }
        }
        plugins.removeAll(toRemove);
        toRemove.clear();
    }

    /**
     * Search for plugins that must be referenced somewhere, in order to save his configuration.
     * This method must be redefine by derived class that need special plugins (like {@link ScalarCometeBox}).
     * 
     * @param target
     * @param listPlugin the list of plugins to check
     */
    protected void searchForKnownPlugins(final T target, final List<AbstractPlugin<?>> listPlugin) {
        CometeCaps<?, T> caps = getTargetCaps(target);
        for (AbstractPlugin<?> plugin : listPlugin) {
            if (plugin instanceof ComponentColorPlugin) {
                ComponentColorPlugin<?> colorPlugin = (ComponentColorPlugin<?>) plugin;
                colorPlugin.setColorAsForeground(caps.isColorAsForeground(), target);
                colorPlugin.setColorAsForeground(caps.isColorAsForeground(), caps);
                colorPlugin.setColorEnabled(target, caps.isColorEnabled());
                colorPlugin.setColorEnabled(caps, caps.isColorEnabled());
            }
        }
    }

    protected void addPluginController(final AbstractPlugin<?> pluginController) {
        if (pluginController != null) {
            synchronized (listPluginController) {
                if (!listPluginController.contains(pluginController)) {
                    listPluginController.add(pluginController);
                }
            }
        }
    }

    public void removePluginController(final AbstractPlugin<?> pluginController) {
        if (pluginController != null) {
            synchronized (listPluginController) {
                listPluginController.remove(pluginController);
            }
        }
    }

    protected final void cleanToolTip(final T target) {
        if (target instanceof IComponent) {
            IComponent component = (IComponent) target;
            synchronized (tooltipMap) {
                // We do want to check whether tooltipMap contains the key, as associated value might be null
                if (tooltipMap.containsKey(component)) {
                    component.setToolTipText(tooltipMap.remove(component));
                }
            }
        }
    }

    /**
     * Cleans an {@link ITarget}. This method is called by {@link #disconnectWidgetFromAll(ITarget)}
     * 
     * @param target The {@link ITarget} to clean
     */
    protected abstract void cleanWidget(T target);

    protected void cleanWidgetConnection(T target, AbstractDataSource<?> source) {
        // by default: clean widget only when no more connected
        if (!isTargetConnected(target)) {
            cleanWidget(target);
        }
    }

    /**
     * Disconnect the {@link ITarget} parameter from all sources and plugins.<br>
     * <b>WARNING</b> : this method will also disconnect from sources that has not been connected
     * with this {@link AbstractCometeBox}.
     * 
     * @param target the {@link ITarget} to disconnect
     */
    public <U> void disconnectWidgetFromAll(final T target) {
        if (target != null) {
            DataSourceReconnectionManager.forgetBadConnection(target, this);
            disconnectPluginsFromAll(target);
            final Set<AbstractController<?>> controllerSet = getUsedControllers();

            final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            for (final AbstractController<?> controller : controllerSet) {
                controller.removeAllReferencesTo(targetCaps);
                printLog(DISCONNECT_ALL_REFERENCIES_FROM, target, controller, null);
            }
            if (targetCaps.isActLikeCommand()) {
                targetCaps.setIgnoreData(true);
            }
            synchronized (mainConnections) {
                mainConnections.remove(target);
            }
            if (isSettable(target) && isCleanWidgetOnDisconnect(target)) {
                cleanWidget(target);
            }
            cleanToolTip(target);
        }
    }

    /**
     * Disconnect the donate {@link ITarget} from the source defined by the {@link IKey} parameter.
     * If you set <code>null</code> instead of a key, you will disconnect the {@link ITarget} from
     * his last connected source
     * 
     * @param target the {@link ITarget} to disconnect
     * @param key the key that define the {@link AbstractDataSource} to disconnect
     */
    @SuppressWarnings("unchecked")
    public <U> void disconnectWidget(final T target, final IKey key) {
        final List<T> usedTarget = MapUtils.extractKeys(mainConnections);
        if ((target != null) && (key != null) && usedTarget.contains(target)) {
            disconnectPlugins(target, key);
            final Set<AbstractController<?>> controllerSet = getUsedControllers();
            final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            final AbstractDataSource<?> source = getExistingSource(key);
            if (source != null) {
                boolean successfulRemoval = false;
                for (final AbstractController<?> controller : controllerSet) {
                    if (controller != null) {
                        GenericDescriptor controllerDesc = controller.getSourceType();
                        if (controllerDesc.isAssignableFrom(source.getDataType())) {
                            if (((AbstractController<U>) controller).hasLink((AbstractDataSource<U>) source, target)) {
                                ((AbstractController<U>) controller).removeLink((AbstractDataSource<U>) source, target);
                                successfulRemoval = true;
                                printLog(DISCONNECT_FROM, target, controller, source);
                            } else if (((AbstractController<U>) controller).hasLink((AbstractDataSource<U>) source,
                                    targetCaps)) {
                                ((AbstractController<U>) controller).removeLink((AbstractDataSource<U>) source,
                                        targetCaps);
                                successfulRemoval = true;
                                printLog(DISCONNECT_FROM, target, controller, source);
                            }
                        }
                    }
                }
                if (successfulRemoval) {
                    synchronized (mainConnections) {
                        List<IKey> connections = mainConnections.get(target);
                        if (connections != null) {
                            connections.remove(key);
                            if (connections.isEmpty()) {
                                mainConnections.remove(target);
                            }
                        }
                    }
                    if (isSettable(target) && isCleanWidgetOnDisconnect(target)) {
                        cleanWidgetConnection(target, source);
                    }
                    if (!isTargetConnected(target)) {
                        cleanToolTip(target);
                    }
                }
            }
        }
    }

    public boolean isCommandWidget(final T target) {
        boolean command = false;
        if (target != null) {
            CometeCaps<?, ?> caps = getTargetCaps(target);
            command = caps.isActLikeCommand();
        }
        return command;
    }

    /**
     * Asks this {@link AbstractCometeBox} to consider or not an {@link ITarget} as a command
     * widget. A command widget is a widget that is supposed to receive data only after asking for
     * modifications (i.e. after notifying its mediators for some changes)
     * 
     * @param target The concerned {@link ITarget}
     * @param command Whether to consider the {@link ITarget} as a command widget
     */
    public void setCommandWidget(final T target, boolean command) {
        if (target != null) {
            CometeCaps<?, ?> caps = getTargetCaps(target);
            caps.setActLikeCommand(command);
        }
    }

    /**
     * Returns whether data transmission is done synchronously for a given {@link ITarget}
     * 
     * @param target The concerned {@link ITarget}
     * @return A <code>boolean</code> value
     * @see #setSynchronTransmission(ITarget, boolean)
     */
    public boolean isSynchronTransmission(T target) {
        boolean synchron = false;
        if (target != null) {
            CometeCaps<?, ?> caps = getTargetCaps(target);
            synchron = caps.isSynchronTransmission();
        }
        return synchron;
    }

    /**
     * Asks this {@link AbstractCometeBox} to transmit data synchronously or asynchronously to the
     * given {@link ITarget}
     * 
     * @param target The concerned {@link ITarget}
     * @param synchron Whether to transmit data synchronously. If <code>true</code>, the target will
     *            receive data immediately on linked sources changes. Otherwise, data transmission
     *            will be done in a separated thread to lesser EDT's work (default case)
     */
    public void setSynchronTransmission(T target, boolean synchron) {
        if (target != null) {
            CometeCaps<?, ?> caps = getTargetCaps(target);
            caps.setSynchronTransmission(synchron);
        }
    }

    /**
     * Disconnects form an {@link ITarget} the plugins that come from a give {@link AbstractDataSource}
     * 
     * @param target The {@link ITarget}
     * @param key The {@link IKey} that identifies the {@link AbstractDataSource}
     */
    protected <U> void disconnectPlugins(final T target, final IKey key) {
        if (target != null && key != null) {
            List<AbstractPlugin<?>> sourcePlugins = null;
            synchronized (linkedPlugins) {
                final List<AbstractPlugin<?>> plugins = linkedPlugins.get(target);
                if (plugins != null) {
                    AbstractDataSource<?> source = getExistingSource(key);
                    if (source instanceof IPluggableDataSource) {
                        sourcePlugins = ((IPluggableDataSource) source).getListPlugin();
                        if (sourcePlugins != null) {
                            plugins.removeAll(sourcePlugins);
                            if (plugins.isEmpty()) {
                                linkedPlugins.remove(target);
                            }
                        }
                    }
                }
            }
            CometeCaps<?, T> caps = getTargetCaps(target);
            // Clean plugin connections out of synchronized block for better performances
            if (sourcePlugins != null) {
                for (final AbstractPlugin<?> plugin : sourcePlugins) {
                    plugin.removePlugin(target);
                    plugin.removePlugin(caps);
                }
                for (final AbstractPlugin<?> plugin : sourcePlugins) {
                    plugin.removePluginFromAllIfLast(DataSourceAdapter.class, AbstractAdapter.class);
                }
            }
        }
    }

    /**
     * Disconnects all plugins from an {@link ITarget}
     * 
     * @param target The {@link ITarget}
     */
    protected <U> void disconnectPluginsFromAll(final T target) {
        if (target != null) {
            List<AbstractPlugin<?>> plugins;
            synchronized (linkedPlugins) {
                plugins = linkedPlugins.remove(target);
            }
            // Clean plugin connections out of synchronized block for better performances
            if (plugins != null) {
                CometeCaps<?, T> caps = getTargetCaps(target);
                for (final AbstractPlugin<?> plugin : plugins) {
                    plugin.removePluginFromAll(target);
                    plugin.removePluginFromAll(caps);
                }
                for (AbstractPlugin<?> plugin : plugins) {
                    plugin.removePluginFromAllIfLast(DataSourceAdapter.class, AbstractAdapter.class);
                }
            }
        }
    }

    /**
     * Reconnect the donate {@link ITarget} with another source defined by the {@link IKey} parameter. This method first
     * disconnect the {@link ITarget} from all his source then connect
     * it to the new one.
     * 
     * @param target the {@link ITarget} to reconnect
     * @param key the key that define the {@link AbstractDataSource} to connect with
     * @return true if the {@link ITarget} connection is successful, false otherwise
     */
    public <TI extends T> boolean reconnectWidget(final TI target, final IKey key) {
        boolean result = false;
        if (target != null && key != null) {
            disconnectWidgetFromAll(target);
            result = connectWidget(target, key);
        }
        return result;
    }

    /**
     * Plugs a filter to a target
     * 
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     */
    public <V> void addFilterToTarget(final AbstractDataFilter<V> filter, final T target) {
        addFilterToTarget(filter, target, true);
    }

    /**
     * Plugs a filter to a target
     * 
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     * @param autoReconnect Whether to check for pending automatic reconnections
     */
    public <V> void addFilterToTarget(final AbstractDataFilter<V> filter, final T target, boolean autoReconnect) {
        for (final AbstractController<?> tempController : getUsedControllers()) {
            tempController.addFilterToTarget(filter, target);
            tempController.addFilterToTarget(filter, getTargetCaps(target));
        }
        if (autoReconnect) {
            DataSourceReconnectionManager.addTargetFilter(target, filter, this);
        }
    }

    /**
     * Unplugs a filter from a target
     * 
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     */
    public <V> void removeFilterFromTarget(final AbstractDataFilter<V> filter, final T target) {
        removeFilterFromTarget(filter, target, true);
    }

    /**
     * Unplugs a filter from a target
     * 
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     */
    public <V> void removeFilterFromTarget(final AbstractDataFilter<V> filter, final T target, boolean autoReconnect) {
        for (final AbstractController<?> tempController : getUsedControllers()) {
            tempController.removeFilterFromTarget(filter, target);
            tempController.removeFilterFromTarget(filter, getTargetCaps(target));
        }
        if (autoReconnect) {
            DataSourceReconnectionManager.removeTargetFilter(target, filter, this);
        }
    }

    /**
     * Adds a filter to all compatible sources linked with a given target
     * 
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     */
    public <V> void addFilterToSourceFromTarget(final AbstractDataFilter<V> filter, final T target) {
        addFilterToSourceFromTarget(filter, target, true);
    }

    /**
     * Adds a filter to all compatible sources linked with a given target
     * 
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     * @param autoReconnect Whether to check for pending automatic reconnections
     */
    @SuppressWarnings("unchecked")
    public <V> void addFilterToSourceFromTarget(final AbstractDataFilter<V> filter, final T target,
            boolean autoReconnect) {
        for (final AbstractController<?> tempController : getUsedControllers()) {
            if (filter == null || ObjectUtils.sameObject(filter.getDataType(), tempController.getSourceType())) {
                ((AbstractController<V>) tempController).addFilterToSourceFromTarget(filter, target);
                ((AbstractController<V>) tempController).addFilterToSourceFromTarget(filter, getTargetCaps(target));
            }
        }
        if (autoReconnect) {
            DataSourceReconnectionManager.addSourceFilter(target, filter, this);
        }
    }

    /**
     * Removes a filter from all compatible sources linked with a given target
     * 
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     */
    public <V> void removeFilterToSourceFromTarget(final AbstractDataFilter<V> filter, final T target) {
        removeFilterToSourceFromTarget(filter, target, true);
    }

    /**
     * Removes a filter from all compatible sources linked with a given target
     * 
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     * @param autoReconnect Whether to check for pending automatic reconnections
     */
    @SuppressWarnings("unchecked")
    public <V> void removeFilterToSourceFromTarget(final AbstractDataFilter<V> filter, final T target,
            boolean autoReconnect) {
        for (final AbstractController<?> tempController : getUsedControllers()) {
            if (filter == null || ObjectUtils.sameObject(filter.getDataType(), tempController.getSourceType())) {
                ((AbstractController<V>) tempController).removeFilterFromSourceFromTarget(filter, target);
                ((AbstractController<V>) tempController).removeFilterFromSourceFromTarget(filter,
                        getTargetCaps(target));
            }
        }
        if (autoReconnect) {
            DataSourceReconnectionManager.removeSourceFilter(target, filter, this);
        }
    }

    public void addCheckerToTarget(final AbstractChecker checker, final T target) {
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (checker != null && targetCaps != null) {
            targetCaps.addCheckerToTarget(checker);
        }
    }

    @SuppressWarnings("unchecked")
    public <U> void addCheckerToSource(final AbstractChecker checker, final IKey key) {
        if (checker != null && key != null) {
            final AbstractDataSource<U> source = getSource(key, false, false);
            for (final AbstractController<?> controller : getUsedControllers()) {
                final GenericDescriptor controllerDesc = controller.getSourceType();
                if (controllerDesc != null && controllerDesc.isAssignableFrom(source.getDataType())) {
                    ((AbstractController<U>) controller).addCheckerToSource(checker, source);
                }
            }
        }
    }

    /**
     * Search for an existing {@link AbstractController} related to this {@link AbstractCometeBox} 's target type and
     * the {@link GenericDescriptor} parameter
     * 
     * @param <U> the data type of the source
     * @param <DT> the data type of the target
     * @param sourceType the {@link GenericDescriptor} that describe the source data type
     * @return the {@link AbstractController} associated
     */
    protected <U> AbstractController<U> getController(final GenericDescriptor sourceType,
            final GenericDescriptor targetType) {
        AbstractController<U> result = null;
        if (sourceType != null && targetType != null) {
            result = DataControllerProvider.getController(sourceType, targetType);
            if (result == null) {
                if (sourceType.equals(targetType) && sourceType.isScalar()) {
                    result = initBasicController(sourceType);
                }
                if (result == null) {
                    result = initController(getTargetClassId(), sourceType);
                }
                DataControllerProvider.pushNewController(result, sourceType, targetType);
            }
        }
        return result;
    }

    protected <U> void checkController(final GenericDescriptor sourceType, final GenericDescriptor targetType,
            AbstractController<U> controller) {
        // nothing to do by default
    }

    protected Set<AbstractController<?>> getUsedControllers() {
        final Set<AbstractController<?>> result = new HashSet<AbstractController<?>>();
        final List<T> targets = MapUtils.extractKeys(mainConnections);
        for (final T target : targets) {
            final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            result.addAll(DataControllerProvider.getAllAssociatedController(targetCaps));
        }
        return result;
    }

    public <U> String printSelfDescription() {
        final StringBuilder result = new StringBuilder();

        final String globalSeparator = GLOBAL_SEPARATOR;
        final List<T> targets = MapUtils.extractKeys(mainConnections);

        // description of this object

        result.append(globalSeparator);
        result.append(ObjectUtils.NEW_LINE);
        result.append(getClass().getSimpleName());
        result.append(TOTAL_NUMBER_OF_CONNECTED_TARGETS);
        result.append((targets != null) ? targets.size() : 0);
        result.append(NEW_LINE_NEW_LINE);

        // main connections

        result.append(LIST_OF_ALL_REGISTERED_CONNECTIONS);
        for (final T target : targets) {
            result.append(printTargetDescription(target));
            result.append(TARGET_SEPARATOR);
        }

        synchronized (connectionsLog) {
            result.append(connectionsLog.toString());
        }

        result.append(globalSeparator);

        return result.toString();
    }

    @SuppressWarnings("unchecked")
    protected <U> String printTargetDescription(final T target) {

        final StringBuilder result = new StringBuilder();
        final String connectionSeparator = EQUIVALENT;

        // target name
        result.append(TAB);
        final String targetName = target.getClass().getSimpleName() + HYPHEN + target.hashCode();
        result.append(targetName);

        // target plugins
        result.append(TAB);
        result.append(connectionSeparator);
        result.append(PLUGINS);
        boolean first = true;
        synchronized (linkedPlugins) {
            for (final AbstractPlugin<?> plugin : linkedPlugins.get(target)) {
                if (!first) {
                    result.append(COMMA);
                } else {
                    first = false;
                }
                result.append(plugin.getClass().getSimpleName() + HYPHEN + plugin.hashCode());
            }
        }
        final List<AbstractDataSource<?>> datasources = new ArrayList<AbstractDataSource<?>>();
        final int indentation = (targetName.length() / 2) - (connectionSeparator.length() / 2);
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);

        // target controllers
        result.append(NEW_LINE_TAB);
        for (int i = 0; i < indentation; ++i) {
            result.append(CometeConstants.SPACE);
        }
        result.append(connectionSeparator);
        result.append(NEW_LINE_TAB);
        for (final AbstractController<?> controller : getUsedControllers()) {
            if (controller.contains(targetCaps)) {
                result.append(controller.getClass().getSimpleName() + HYPHEN + controller.hashCode());

                final List<AbstractDataSource<U>> sources = ((AbstractController<U>) controller)
                        .getAllRelatedSources(targetCaps);
                if (sources != null) {
                    datasources.addAll(sources);
                }
            }
            result.append(NEW_LINE_TAB);
        }

        // target sources
        for (int i = 0; i < indentation; ++i) {
            result.append(CometeConstants.SPACE);
        }
        result.append(connectionSeparator);
        result.append(NEW_LINE_TAB);
        for (final AbstractDataSource<?> source : datasources) {
            result.append(printDataSource(source));
        }
        result.append(ObjectUtils.NEW_LINE);
        result.append(printAdditional(target));

        return result.toString();
    }

    protected String printDataSource(final AbstractDataSource<?> source) {
        final StringBuilder result = new StringBuilder();
        result.append(source.getClass().getSimpleName() + HYPHEN + source.hashCode());
        final IKey key = source.getOriginDescriptor();
        if (key != null) {
            result.append(CometeConstants.OPEN_PAR);
            result.append(key.getInformationKey());
            result.append(CometeConstants.CLOSE_PAR);
            result.append(NEW_LINE_TAB);
        }
        return result.toString();
    }

    protected String printAdditional(final T target) {
        return ObjectUtils.EMPTY_STRING;
    }

    protected void printLog(final String logMessage, final ITarget target, final AbstractController<?> controller,
            final AbstractDataSource<?> source) {
        synchronized (connectionsLog) {
            connectionsLog.append(AT);
            connectionsLog.append(System.currentTimeMillis() - startTime);
            connectionsLog.append(COMMA);
            connectionsLog.append(logMessage);
            connectionsLog.append(COLON);
            if (target != null) {
                connectionsLog.append(target.getClass().getSimpleName() + HYPHEN + target.hashCode());
            } else {
                connectionsLog.append(NULL);
            }
            connectionsLog.append(CometeConstants.SPACE).append(EQUIVALENT).append(CometeConstants.SPACE);
            if (controller != null) {
                connectionsLog.append(controller.getClass().getSimpleName() + HYPHEN + controller.hashCode());
            } else {
                connectionsLog.append(NULL);
            }
            connectionsLog.append(CometeConstants.SPACE).append(EQUIVALENT).append(CometeConstants.SPACE);
            if (source != null) {
                connectionsLog.append(source.getClass().getSimpleName() + HYPHEN + source.hashCode());
                if (source.getOriginDescriptor() != null) {
                    connectionsLog.append(CometeConstants.OPEN_PAR + source.getOriginDescriptor().getInformationKey()
                            + CometeConstants.CLOSE_PAR);
                }
            } else {
                connectionsLog.append(NULL);
            }
            connectionsLog.append(ObjectUtils.NEW_LINE);
        }
    }

    // /////////////// //
    // Getters/setters //
    // /////////////// //

    /**
     * @return the firstInitAllowed value
     */
    public boolean isFirstInitAllowed() {
        return firstInitAllowed;
    }

    /**
     * Allow user to choose whether or not the controller must be initialized after connection.
     * </br>
     * <B>Warning</B> : you must set this value BEFORE connecting your component.
     * 
     * @param firstInitAllowed the firstInitAllowed to set
     */
    public void setFirstInitAllowed(final boolean firstInitAllowed) {
        this.firstInitAllowed = firstInitAllowed;
    }

    /**
     * @return the sourcePluginAllowed
     */
    public boolean isSourcePluginAllowed() {
        return sourcePluginAllowed;
    }

    /**
     * @param sourcePluginAllowed the sourcePluginAllowed to set
     */
    public void setSourcePluginAllowed(final boolean sourcePluginAllowed) {
        this.sourcePluginAllowed = sourcePluginAllowed;
    }

    public void setConfirmation(final T target, final boolean confirmation) {
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            targetCaps.setConfirmation(confirmation);
        }
    }

    public boolean isConfirmation(final T target) {
        boolean result = false;
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            result = targetCaps.isConfirmation();
        }
        return result;
    }

    public void setConfirmationMessage(final T target, final String message) {
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            targetCaps.setConfirmationMessage(message);
        }
    }

    public String getConfirmationMessage(final T target) {
        String result = null;
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            result = targetCaps.getConfirmationMessage();
        }
        return result;
    }

    public void setConfirmationTitle(final T target, final String message) {
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            targetCaps.setConfirmationTitle(message);
        }
    }

    public String getConfirmationTitle(final T target) {
        String result = null;
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            result = targetCaps.getConfirmationTitle();
        }
        return result;
    }

    public void setReadOnly(final T target, final boolean state) {
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            targetCaps.setReadOnly(state);
        }
    }

    public boolean isReadOnly(final T target) {
        boolean result = false;
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            result = targetCaps.isReadOnly();
        }
        return result;
    }

    public void setSettable(final T target, final boolean state) {
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            targetCaps.setSettable(state);
        }
    }

    public boolean isSettable(final T target) {
        boolean result = false;
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            result = targetCaps.isSettable();
        }
        return result;
    }

    /**
     * Forces a widget to become enabled/disabled.
     * 
     * @param target The concerned widget
     * @param state Whether widget should be enabled. If <code>true</code>, the widget is made
     *            enabled/settable depending on its associated sources. If <code>false</code>, the
     *            widget is kept disabled
     */
    public void setUserEnabled(final T target, final boolean state) {
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            targetCaps.setEnabled(state);
        }
    }

    /**
     * Returns whether a widget was previously set enabled by users
     * 
     * @param target The concerned widget
     * @return A <code>boolean</code>
     * @see #setUserEnabled(ITarget, boolean)
     */
    public boolean isUserEnabled(final T target) {
        boolean result;
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps == null) {
            // by default, a target should be considered as enabled
            result = true;
        } else {
            result = targetCaps.isEnabled();
        }
        return result;
    }

    /**
     * Forces a widget to become editable or not.
     * 
     * @param target The concerned widget
     * @param state Whether widget should be editable. If <code>true</code>, the widget is made
     *            editable depending on its associated sources. If <code>false</code>, the widget is
     *            kept not editable. This will only work if the widget is an {@link IEditableComponent}
     */
    public void setUserEditable(final T target, final boolean state) {
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            targetCaps.setEditable(state);
        }
    }

    /**
     * Returns whether a widget was previously set editable by users
     * 
     * @param target The concerned widget
     * @return A <code>boolean</code>
     * @see #setUserEditable(ITarget, boolean)
     */
    public boolean isUserEditable(final T target) {
        boolean result = false;
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            result = targetCaps.isEditable();
        }
        return result;
    }

    /**
     * Returns whether the management of the "editable" property of an {@link IEditableComponent} is enabled
     * (<code>true</code> by default)
     * 
     * @param target The {@link IEditableComponent}
     * @return A <code>boolean</code>
     */
    public boolean isEditableComponentManagementEnabled(final T target) {
        boolean result = true;
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            result = targetCaps.isEditableComponentManagementEnabled();
        }
        return result;
    }

    /**
     * Enables or disables the management of the "editable" property of an {@link IEditableComponent}
     * 
     * @param target The concerned target
     * @param editableComponentManagementEnabled Whether to enable the management of the "editable" property
     */
    public void setEditableComponentManagementEnabled(final T target, boolean editableComponentManagementEnabled) {
        final CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            targetCaps.setEditableComponentManagementEnabled(editableComponentManagementEnabled);
        }
    }

    /**
     * @param state true if you want the coloration to appear, false otherwise
     */
    public void setColorEnabled(T target, boolean state) {

        final CometeCaps<?, ?> caps = getTargetCaps(target);
        if (caps != null) {
            caps.setColorEnabled(state);
        }

        for (ComponentColorPlugin<?> colorPlugin : getPlugins(target, ComponentColorPlugin.class)) {
            if (colorPlugin != null) {
                colorPlugin.setColorEnabled(target, state);
            }
        }
    }

    /**
     * @return true if the coloration appears, false otherwise
     */
    public boolean isColorEnabled(T target) {
        boolean result = false;
        final CometeCaps<?, ?> caps = getTargetCaps(target);
        if (caps != null) {
            result = caps.isColorEnabled();
        }
        return result;
    }

    public void setColorAsForeground(T target, boolean state) {
        final CometeCaps<?, ?> caps = getTargetCaps(target);
        if (caps != null) {
            caps.setColorAsForeground(state);
        }

        for (ComponentColorPlugin<?> colorPlugin : getPlugins(target, ComponentColorPlugin.class)) {
            if (colorPlugin != null) {
                colorPlugin.setColorAsForeground(state, target);
            }
        }
    }

    public boolean isColorAsForeground(T target) {
        boolean result = false;
        final CometeCaps<?, ?> caps = getTargetCaps(target);
        if (caps != null) {
            result = caps.isColorAsForeground();
        }
        return result;
    }

    public void setDefaultToolTipEnabled(boolean enableDefaultToolTip) {
        defaultToolTip = enableDefaultToolTip;
    }

    public boolean isDefaultToolTipEnabled() {
        return defaultToolTip;
    }

    /**
     * Returns whether this {@link AbstractCometeBox} will clean the {@link ITarget} on
     * {@link #disconnectWidgetFromAll(ITarget)}
     * 
     * @param widget The concerned {@link ITarget}
     * @return A <code>boolean</code> value
     */
    public boolean isCleanWidgetOnDisconnect(ITarget widget) {
        Boolean clean;
        synchronized (cleanWidgetMap) {
            clean = cleanWidgetMap.get(widget);
        }
        if (clean == null) {
            clean = Boolean.valueOf(true);
        }
        return clean.booleanValue();
    }

    /**
     * Sets whether this {@link AbstractCometeBox} should clean the {@link ITarget} on
     * {@link #disconnectWidgetFromAll(ITarget)}
     * 
     * @param widget The concerned {@link ITarget}
     * @param clean Whether this {@link AbstractCometeBox} should clean the {@link ITarget} on
     *            {@link #disconnectWidgetFromAll(ITarget)}
     */
    public void setCleanWidgetOnDisconnect(ITarget widget, boolean clean) {
        synchronized (cleanWidgetMap) {
            cleanWidgetMap.put(widget, Boolean.valueOf(clean));
        }
    }

    @Override
    public void addLogger(String loggerId) {
        loggingSystemDelegate.addLogger(loggerId);
        Set<AbstractController<?>> constrollers = getUsedControllers();
        for (AbstractController<?> controller : constrollers) {
            controller.addLogger(loggerId);
        }
    }

    @Override
    public void removeLogger(String loggerId) {
        loggingSystemDelegate.removeLogger(loggerId);
        Set<AbstractController<?>> constrollers = getUsedControllers();
        for (AbstractController<?> controller : constrollers) {
            controller.removeLogger(loggerId);
        }
    }

    public CometeColor getErrorColor(T target) {
        CometeColor color = null;
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps != null) {
                targetCaps.getErrorColor();
            }
        }
        return color;
    }

    public void setErrorColor(T target, CometeColor errorColor) {
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps != null) {
                targetCaps.setErrorColor(errorColor);
            }
        }
    }

    public void putInError(T target) {
        CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null) {
            targetCaps.setTargetInErrorMode();
        }
    }

    // ////////////// //
    // Static methods //
    // ////////////// //

    /**
     * Add to the {@link DataSourceProducerProvider} map a new {@link IDataSourceProducer} he can
     * handle.
     */
    public static void addDataSourceProvider(final Class<? extends IDataSourceProducer> newProducer) {
        DataSourceProducerProvider.pushNewProducer(newProducer);
    }

    /**
     * Check if an {@link AbstractDataSource} can be created with the donate {@link IKey}.
     * 
     * @param key the {@link IKey} that define the source you want to verify
     * @return true if the source is creatable, false otherwise
     */
    public static boolean isSourceCreatable(final IKey key) {
        boolean result = false;
        if (key != null) {
            AbstractDataSource<?> source = getSource(key, false, true);
            result = (source != null);
        }
        return result;
    }

    /**
     * Check if an {@link AbstractDataSource} which is donated by {@link IKey} is read only.
     * 
     * @param key the {@link IKey} that define the source you want to verify
     * @return true if the source is read only, false otherwise
     */
    public static boolean isSourceReadOnly(final IKey key) {
        boolean result = false;
        if (key != null) {
            AbstractDataSource<?> source = getSource(key, false, true);
            result = (source != null) && (!source.isSettable());
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    protected static <U> AbstractDataSource<U> getExistingSource(final IKey key) {
        AbstractDataSource<U> result = null;
        if (key != null) {
            final IDataSourceProducer producer = DataSourceProducerProvider.getProducer(key.getSourceProduction());
            if (producer instanceof IConstantSourceProducer) {
                result = (AbstractDataSource<U>) ((IConstantSourceProducer) producer).getExistingSource(key);
            }
        }
        return result;
    }

    /**
     * This method search for an existing source created with the donate {@link IKey} and return it,
     * otherwise it will initialize the source with the IKey corresponding {@link DataSourceProducerProvider}. The job
     * is done by the producer.
     * 
     * @param <U> the type of the source
     * @param key the key that create the source
     * @param readImmediately Whether to force the source to read its data immediately
     * @param forgetImmediately Whether to immediately remove the source, if it did not exist, once created
     * @return the desired source
     */
    @SuppressWarnings("unchecked")
    protected static <U> AbstractDataSource<U> getSource(final IKey key, boolean readImmediately,
            boolean forgetImmediately) {
        AbstractDataSource<U> result = null;
        if (key != null) {
            final IDataSourceProducer producer = DataSourceProducerProvider.getProducer(key.getSourceProduction());
            try {
                if (producer == null) {
                    throw new KeyCompatibilityException(key, producer);
                } else if (producer instanceof IConstantSourceProducer) {
                    IConstantSourceProducer constantSourceProducer = (IConstantSourceProducer) producer;
                    AbstractDataSource<U> former = (AbstractDataSource<U>) constantSourceProducer
                            .getExistingSource(key);
                    if (producer instanceof AbstractRefreshingManager<?>) {
                        result = (AbstractDataSource<U>) ((AbstractRefreshingManager<?>) producer).createDataSource(key,
                                false, readImmediately);
                    } else {
                        result = (AbstractDataSource<U>) producer.createDataSource(key);
                    }
                    if (forgetImmediately && (former == null)) {
                        constantSourceProducer.removeDataSource(key);
                    }
                } else {
                    result = (AbstractDataSource<U>) producer.createDataSource(key);
                }
            } catch (final Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .error("Failed to create source for key: " + (key == null ? null : key.getInformationKey()), e);
            }
        }
        return result;
    }

    /**
     * Returns the logging mode used
     * 
     * @return A {@link LoggingMode}
     */
    public static LoggingMode getLoggingMode() {
        return loggingMode;
    }

    /**
     * Sets the logging mode to use
     * 
     * @param loggingMode the logging mode to use
     */
    public static void setLoggingMode(LoggingMode loggingMode) {
        AbstractCometeBox.loggingMode = loggingMode;
    }

    // //////////////// //
    // Abstract methods //
    // //////////////// //

    /**
     * Create the controller used to connect the {@link IComponent} with it {@link AbstractDataSource}.
     * 
     * @param <U> the type of the source
     * @param targetClass the {@link Class} object of the widget
     * @param sourceType the {@link Class} object for the type of the source
     * @return the controller
     */
    protected abstract <U> AbstractController<U> initController(Class<?> targetClass, GenericDescriptor sourceType);

    /**
     * Create an adapter encapsulation for the {@link ITarget} parameter
     * 
     * @param target the target to encapsulate
     * @return the {@link CometeCaps} that contains the target
     */
    protected abstract CometeCaps<?, T> initTargetCaps(T target);

    /**
     * @return the box type of target which can be handle
     */
    protected abstract Class<T> getTargetClassId();

    /**
     * @return the target data type
     */
    protected abstract GenericDescriptor getTargetType();

    protected abstract <U> AbstractController<U> initBasicController(GenericDescriptor sourceType);

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * {@link Enum} used to know how to log information
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    public static enum LoggingMode {
        /**
         * Log information in pop-up, if its {@link Level} is {@link Level#WARNING} or {@link Level#SEVERE} (in console
         * otherwise)
         */
        POP_UP,
        /**
         * Log information in console
         */
        CONSOLE,
        /**
         * Do not log information
         */
        NOTHING
    }

    /**
     * A very basic {@link AppenderBase} that redirects {@link ILoggingEvent}s to {@link StaticErrorDialog}
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected static class CometeHandler extends AppenderBase<ILoggingEvent> {

        private static final String LEVEL_SEPARATOR = ":";
        private final SimpleDateFormat format;

        public CometeHandler() {
            super();
            format = new SimpleDateFormat(IDateConstants.US_DATE_FORMAT);
            setContext(new LoggerContext());
        }

        @Override
        public synchronized void doAppend(ILoggingEvent event) {
            LoggingMode mode = loggingMode;
            if ((mode != null) && (mode != LoggingMode.NOTHING) && (event != null)) {
                Level level = event.getLevel();
                StringBuilder sb = new StringBuilder(format.format(new Date(event.getTimeStamp())));
                sb.append(' ').append(event.getLevel());
                sb.append(LEVEL_SEPARATOR);
                String message = event.getMessage();
                // We don't want to keep trace of the full error. We just want to get a minimum message (JAVAAPI-498).
                if (message == null) {
                    IThrowableProxy proxy = event.getThrowableProxy();
                    if (proxy instanceof ThrowableProxy) {
                        Throwable t = ((ThrowableProxy) proxy).getThrowable();
                        if (t != null) {
                            sb.append(t.getMessage());
                        }
                    }
                } else {
                    sb.append(message);
                }
                // Log according to LoggingMode (Mantis #23616)
                if ((mode == LoggingMode.POP_UP) && (level.toInt() >= Level.WARN_INT)) {
                    StaticErrorDialog.showMessageDialog(null, sb.toString());
                } else {
                    System.out.println(sb);
                }
            }
        }

        @Override
        protected void append(ILoggingEvent eventObject) {
            // not managed
        }

    }

}
