/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box.scalarbox;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.caps.scalarcaps.ScalarCaps;
import fr.soleil.comete.controller.ScalarCometeController;
import fr.soleil.comete.service.ISourceListener;
import fr.soleil.comete.service.ITargetListener;
import fr.soleil.comete.target.ListenableTarget;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.plugin.AbstractPlugin;
import fr.soleil.data.plugin.FormatPlugin;
import fr.soleil.data.plugin.UnitPlugin;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.scalar.IScalarTarget;

public abstract class ScalarCometeBox<T extends IScalarTarget, V> extends AbstractCometeBox<T> {

    protected HashMap<IKey, ListenableTarget<?>> listenableTargetMap = new HashMap<IKey, ListenableTarget<?>>();

    // protected HashMap<T, IKey> cometeListenerRelations = new HashMap<T, IKey>();

    protected abstract Class<V> getScalarClass();

    @SuppressWarnings("unchecked")
    @Override
    public <U> AbstractController<U> initController(Class<?> targetClass, GenericDescriptor sourceType) {
        AbstractController<U> controller = null;
        controller = new ScalarCometeController<U, V>((Class<U>) sourceType.getConcernedClass(), getScalarClass());
        return controller;
    }

    public ScalarCometeBox() {
        super();
        singleConnectionOnly = true;
    }

    /**
     * 
     * @param <U>
     * @param <DT>
     * @param listener
     * @param key
     */
    @SuppressWarnings("unchecked")
    public <U> void addSourceListener(ISourceListener<U> listener, IKey key) {
        if (listener != null && key != null) {
            AbstractDataSource<U> source = getSource(key, false, false);
            if (source != null) {
                AbstractController<U> controller = getController(source.getDataType(), source.getDataType());
                synchronized (listenableTargetMap) {
                    ListenableTarget<?> listenableTarget = listenableTargetMap.get(key);
                    if (listenableTarget == null && controller != null) {
                        listenableTarget = new ListenableTarget<U>();
                        controller.addLink(source, listenableTarget);
                        listenableTargetMap.put(key, listenableTarget);
                    }
                    if (listenableTarget != null) {
                        ((ListenableTarget<U>) listenableTarget).addSourceListener(listener);
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public <DT> void addTargetListener(ITargetListener<DT> listener, T target) {
        if (listener != null && target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps.getDataType().equals(getTargetType())) {
                ((ScalarCaps<DT, ?>) targetCaps).addTargetListener(listener);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public <U> void removeSourceListener(ISourceListener<U> listener, IKey key) {
        if (listener != null && key != null) {
            ListenableTarget<?> listenableTarget;
            synchronized (listenableTargetMap) {
                listenableTarget = listenableTargetMap.get(key);
            }
            if (listenableTarget != null) {
                ((ListenableTarget<U>) listenableTarget).removeSourceListener(listener);
                if (!listenableTarget.hasListeners()) {
                    synchronized (listenableTargetMap) {
                        listenableTargetMap.remove(key);
                    }
                    Set<AbstractController<?>> controllerSet = getUsedControllers();
                    for (AbstractController<?> controller : controllerSet) {
                        AbstractDataSource<?> source = getExistingSource(key);
                        ((AbstractController<U>) controller).removeLink((AbstractDataSource<U>) source,
                                listenableTarget);
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public <DT> void removeTargetListener(ITargetListener<DT> listener, T target) {
        if (listener != null && target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps != null && targetCaps instanceof ScalarCaps) {
                if (targetCaps.getDataType().equals(getTargetType())) {
                    ((ScalarCaps<DT, ?>) targetCaps).removeTargetListener(listener);
                }
            }
        }
    }

    @Override
    protected void searchForKnownPlugins(T target, List<AbstractPlugin<?>> result) {
        super.searchForKnownPlugins(target, result);
        Object tempCaps = getTargetCaps(target);

        if (tempCaps instanceof ScalarCaps) {
            ScalarCaps<?, ?> targetCaps = (ScalarCaps<?, ?>) tempCaps;
            for (AbstractPlugin<?> plugin : result) {
                if (plugin instanceof FormatPlugin) {
                    ((FormatPlugin) plugin).setFormatEnabled(target, targetCaps.isFormatEnabled());
                } else if (plugin instanceof UnitPlugin) {
                    ((UnitPlugin) plugin).setUnitEnabled(target, targetCaps.isUnitEnabled());
                }
            }
        }
    }

    /**
     * @param state true if you want the format to appear, false otherwise
     */
    public void setFormatEnabled(T target, boolean state) {

        Object tempCaps = getTargetCaps(target);
        if (tempCaps instanceof ScalarCaps) {
            ScalarCaps<?, ?> targetCaps = (ScalarCaps<?, ?>) tempCaps;
            targetCaps.setFormatEnabled(state);
        }

        for (FormatPlugin plugin : getPlugins(target, FormatPlugin.class)) {
            if (plugin != null) {
                plugin.setFormatEnabled(target, state);
            }
        }
    }

    /**
     * @return true if the format appears, false otherwise
     */
    public boolean isFormatEnabled(T target) {
        boolean result = false;
        Object tempCaps = getTargetCaps(target);
        if (tempCaps instanceof ScalarCaps) {
            ScalarCaps<?, ?> targetCaps = (ScalarCaps<?, ?>) tempCaps;
            result = targetCaps.isFormatEnabled();
        }
        return result;
    }

    /**
     * @param state true if you want the coloration to appear, false otherwise
     */
    public void setUnitEnabled(T target, boolean state) {
        Object tempCaps = getTargetCaps(target);
        if (tempCaps instanceof ScalarCaps) {
            ScalarCaps<?, ?> targetCaps = (ScalarCaps<?, ?>) tempCaps;
            targetCaps.setUnitEnabled(state);
        }

        for (UnitPlugin unitPlugin : getPlugins(target, UnitPlugin.class)) {
            if (unitPlugin != null) {
                unitPlugin.setUnitEnabled(target, state);
            }
        }
    }

    /**
     * @return true if the coloration appears, false otherwise
     */
    public boolean isUnitEnabled(T target) {
        boolean result = false;
        Object tempCaps = getTargetCaps(target);
        if (tempCaps instanceof ScalarCaps) {
            ScalarCaps<?, ?> targetCaps = (ScalarCaps<?, ?>) tempCaps;
            result = targetCaps.isUnitEnabled();
        }
        return result;
    }

    @SuppressWarnings({ "unchecked" })
    @Override
    protected String printAdditional(T target) {
        StringBuilder result = new StringBuilder();
        result.append(super.printAdditional(target));
        CometeCaps<?, ?> targetCaps = getTargetCaps(target);
        if (targetCaps != null && targetCaps instanceof ScalarCaps) {
            List<ITargetListener<?>> targetListeners = ((ScalarCaps) targetCaps).getListeners();
            if (targetListeners != null && !targetListeners.isEmpty()) {
                result.append("\n\tListener relation : ");
                for (ITargetListener<?> targetListener : targetListeners) {
                    result.append(targetListener.getClass().getSimpleName() + "-" + targetListener.hashCode());
                }

                // IKey key = cometeListenerRelations.get(target);
                // synchronized (listenableTargetMap) {
                // if (key != null && !listenableTargetMap.isEmpty()) {
                // result.append(", ");
                // ListenableTarget<?> sourceListener = listenableTargetMap.get(key);
                // if (sourceListener != null) {
                // result.append(sourceListener.getClass().getSimpleName() + "-"
                // + sourceListener.hashCode());
                // }
                //
                // result.append("\n\tbetween : ");
                // result.append(target.getClass().getSimpleName() + "-" + target.hashCode());
                // result.append(" <=> ");
                // result.append(key.getInformationKey());
                // }
                // }
                result.append("\n");
            }
        }
        return result.toString();
    }
}
