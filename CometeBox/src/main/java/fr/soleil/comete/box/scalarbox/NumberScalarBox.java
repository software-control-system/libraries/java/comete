/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box.scalarbox;

import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.caps.scalarcaps.NumberScalarCaps;
import fr.soleil.data.controller.BasicNumberTargetController;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.scalar.INumberTarget;

public class NumberScalarBox extends ScalarCometeBox<INumberTarget, Number> {

    @Override
    protected Class<Number> getScalarClass() {
        return Number.class;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <U> AbstractController<U> initBasicController(GenericDescriptor sourceType) {

        AbstractController<U> controller = null;
        controller = (AbstractController<U>) new BasicNumberTargetController() {
            @Override
            protected void setDataToTarget(ITarget target, Number data, AbstractDataSource<Number> source) {
                if (target instanceof NumberScalarCaps) {
                    ((NumberScalarCaps) target).setData(data, source);
                } else {
                    super.setDataToTarget(target, data, source);
                }
            }

            @Override
            protected void setDataToGenericTarget(ITarget target, Number data, AbstractDataSource<Number> source) {
                if (target instanceof NumberScalarCaps) {
                    ((NumberScalarCaps) target).setData(data, source);
                } else {
                    super.setDataToGenericTarget(target, data, source);
                }
            }
        };
        return controller;
    }

    @Override
    protected Class<INumberTarget> getTargetClassId() {
        return INumberTarget.class;
    }

    @Override
    protected GenericDescriptor getTargetType() {
        return new GenericDescriptor(Number.class);
    }

    @Override
    protected CometeCaps<?, INumberTarget> initTargetCaps(INumberTarget target) {
        return new NumberScalarCaps(target, this);
    }

    @Override
    protected void cleanWidget(INumberTarget target) {
        if (target != null) {
            try {
                target.setNumberValue(null);
            } catch (Exception e) {
                // ignore exception
            }
        }
    }

}