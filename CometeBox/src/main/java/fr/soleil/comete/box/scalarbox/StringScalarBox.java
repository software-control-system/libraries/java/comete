/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box.scalarbox;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.caps.scalarcaps.StringScalarCaps;
import fr.soleil.comete.controller.ScalarCometeController;
import fr.soleil.comete.definition.widget.IButton;
import fr.soleil.data.adapter.NumberToStringAdapter;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.adapter.source.NumberToStringDataAdapter;
import fr.soleil.data.controller.BasicTextTargetController;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.plugin.AbstractPlugin;
import fr.soleil.data.plugin.FormatPlugin;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.source.IPluggableDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;

public class StringScalarBox extends ScalarCometeBox<ITextTarget, String> {

    public final static String DEFAULT_ERROR_STRING = "-----";

    protected ScalarCometeController<Number, String> numberController;

    public StringScalarBox() {
        super();
    }

    @Override
    protected Class<String> getScalarClass() {
        return String.class;
    }

    @Override
    protected void prepareTargetOnFirstAccess(ITextTarget target, CometeCaps<?, ?> targetCaps) {
        super.prepareTargetOnFirstAccess(target, targetCaps);
        if (target instanceof IButton) {
            setOutputInPopup(target, true);
            setCommandWidget(target, true);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <U> void checkController(GenericDescriptor sourceType, GenericDescriptor targetType,
            AbstractController<U> controller) {
        if ((numberController == null) && ObjectUtils.isNumberClass(sourceType.getConcernedClass())) {
            numberController = (ScalarCometeController<Number, String>) controller;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <U> AbstractController<U> initBasicController(GenericDescriptor sourceType) {
        AbstractController<U> controller = null;
        controller = (AbstractController<U>) new BasicTextTargetController() {
            @Override
            protected void setDataToTarget(ITarget target, String data, AbstractDataSource<String> source) {
                if (target instanceof StringScalarCaps) {
                    ((StringScalarCaps) target).setData(data, source);
                } else {
                    super.setDataToTarget(target, data, source);
                }
            }

            @Override
            protected void setDataToGenericTarget(ITarget target, String data, AbstractDataSource<String> source) {
                if (target instanceof StringScalarCaps) {
                    ((StringScalarCaps) target).setData(data, source);
                } else {
                    super.setDataToGenericTarget(target, data, source);
                }
            }
        };
        return controller;
    }

    @Override
    protected void initPlugins(AbstractDataSource<?> source, ITextTarget target) {
        super.initPlugins(source, target);
        NumberToStringDataAdapter<?> numberAdapter = getNumberAdapter(source);
        if (numberAdapter != null) {
            if (source instanceof IPluggableDataSource) {
                List<AbstractPlugin<?>> plugins = ((IPluggableDataSource) source).getListPlugin();
                for (FormatPlugin foundedPlugin : getPlugins(plugins, FormatPlugin.class)) {
                    if (foundedPlugin != null) {
                        foundedPlugin.addPlugin(numberAdapter);
                    }
                }
            }
        }
    }

    @Override
    protected void filterPlugins(final List<AbstractPlugin<?>> plugins, final AbstractDataSource<?> source,
            final ITextTarget target) {
        final List<AbstractPlugin<?>> toRemove = new ArrayList<AbstractPlugin<?>>();
        NumberToStringDataAdapter<?> numberAdapter = getNumberAdapter(source);
        CometeCaps<?, ITextTarget> caps = getTargetCaps(target);
        for (final AbstractPlugin<?> plugin : plugins) {
            boolean connectionState;
            plugin.setFirstInitAllowed(firstInitAllowed);
            connectionState = (plugin.addPlugin(target) || plugin.addPlugin(caps) || plugin.addPlugin(numberAdapter));
            if (!connectionState) {
                toRemove.add(plugin);
            }
        }
        plugins.removeAll(toRemove);
        toRemove.clear();
    }

    @SuppressWarnings("unchecked")
    protected NumberToStringDataAdapter<?> getNumberAdapter(AbstractDataSource<?> source) {
        NumberToStringDataAdapter<?> result = null;
        if ((numberController != null) && (source.getDataType() != null)
                && ObjectUtils.isNumberClass(source.getDataType().getConcernedClass())) {
            result = numberController.getAdapter((AbstractDataSource<Number>) source);
        }
        return result;
    }

    @Override
    public void setFormatEnabled(ITextTarget target, boolean enabled) {
        super.setFormatEnabled(target, enabled);
        DataSourceAdapter<Number, String> adapter = (enabled ? null
                : new DataSourceAdapter<Number, String>(null, new NumberToStringAdapter<Number>(Number.class)));
        if (numberController != null) {
            numberController.setCustomTargetAdapter(getTargetCaps(target), adapter);
        }
    }

    @Override
    protected Class<ITextTarget> getTargetClassId() {
        return ITextTarget.class;
    }

    @Override
    protected GenericDescriptor getTargetType() {
        return new GenericDescriptor(String.class);
    }

    protected String getErrorText(ITextTarget target, String defaultValue) {
        String result = defaultValue;
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps instanceof StringScalarCaps) {
                result = ((StringScalarCaps) targetCaps).getErrorText();
            }
        }
        return result;
    }

    public String getErrorText(ITextTarget target) {
        return getErrorText(target, ObjectUtils.EMPTY_STRING);
    }

    public void setErrorText(ITextTarget target, String message) {
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps instanceof StringScalarCaps) {
                ((StringScalarCaps) targetCaps).setErrorText(message);
            }
        }
    }

    public boolean isUseErrorText(ITextTarget target) {
        boolean useErrorText = false;
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps instanceof StringScalarCaps) {
                useErrorText = ((StringScalarCaps) targetCaps).isErrorTextActivated();
            }
        }
        return useErrorText;
    }

    public void setUseErrorText(ITextTarget target, boolean useErrorText) {
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps instanceof StringScalarCaps) {
                ((StringScalarCaps) targetCaps).setErrorTextActivated(useErrorText);
            }
        }
    }

    public boolean isOnError(ITextTarget target) {
        return target.getText().equalsIgnoreCase(getErrorText(target));
    }

    public void setOutputInPopup(ITextTarget target, boolean isPopup) {
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps instanceof StringScalarCaps) {
                ((StringScalarCaps) targetCaps).setOutputInPopup(isPopup);
                for (AbstractController<?> controller : getUsedControllers()) {
                    if (!controller.getAllRelatedSources(targetCaps).isEmpty()) {
                        prepareController(controller, targetCaps);
                    }
                }
            }
            if (isPopup) {
                setUseErrorText(target, false);
            }
        }
    }

    @Override
    protected void prepareController(AbstractController<?> controller, CometeCaps<?, ?> connectedTarget) {
        if ((controller != null) && (connectedTarget instanceof StringScalarCaps)) {
            controller.setCancelable(connectedTarget, !((StringScalarCaps) connectedTarget).isOutputInPopup());
        }
    }

    public boolean isOutputInPopup(ITextTarget target) {
        boolean result = false;
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps instanceof StringScalarCaps) {
                result = ((StringScalarCaps) targetCaps).isOutputInPopup();
            }
        }
        return result;
    }

    @Override
    protected CometeCaps<?, ITextTarget> initTargetCaps(ITextTarget target) {
        return new StringScalarCaps(target, this);
    }

    @Override
    protected void cleanWidget(ITextTarget target) {
        try {
            putInError(target);
        } catch (Exception e) {
            // ignore exception
        }
    }

}
