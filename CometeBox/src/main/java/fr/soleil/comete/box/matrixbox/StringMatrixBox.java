/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box.matrixbox;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.caps.matrixcaps.StringMatrixCaps;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.matrix.ITextMatrixTarget;
import fr.soleil.lib.project.ObjectUtils;

public class StringMatrixBox extends AbstractMatrixBox<ITextMatrixTarget> {

    public static final String[][] DEFAULT_ERROR_STRING_MATRIX = new String[][] {
            { StringScalarBox.DEFAULT_ERROR_STRING } };

    @Override
    protected Class<ITextMatrixTarget> getTargetClassId() {
        return ITextMatrixTarget.class;
    }

    @Override
    protected GenericDescriptor getTargetType() {
        return new GenericDescriptor(String[][].class);
    }

    @Override
    protected CometeCaps<?, ITextMatrixTarget> initTargetCaps(ITextMatrixTarget target) {
        return new StringMatrixCaps(target, this);
    }

    @Override
    protected Class<?> getPrimitiveTargetType() {
        return String.class;
    }

    protected String[][] getErrorText(ITextMatrixTarget target, String[][] defaultValue) {
        String[][] result = defaultValue;
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps instanceof StringMatrixCaps) {
                result = ((StringMatrixCaps) targetCaps).getErrorTextMatrix();
            }
        }
        return result;
    }

    public String[][] getErrorTextMatrix(ITextMatrixTarget target) {
        return getErrorText(target, null);
    }

    public void setErrorTextMatrix(ITextMatrixTarget target, String[][] message) {
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps instanceof StringMatrixCaps) {
                ((StringMatrixCaps) targetCaps).setErrorTextMatrix(message);
            }
        }
    }

    public boolean isUseErrorTextMatrix(ITextMatrixTarget target) {
        boolean useErrorTextMatrix = false;
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps instanceof StringMatrixCaps) {
                useErrorTextMatrix = ((StringMatrixCaps) targetCaps).isErrorTextMatrixActivated();
            }
        }
        return useErrorTextMatrix;
    }

    public void setUseErrorTextMatrix(ITextMatrixTarget target, boolean useErrorTextMatrix) {
        if (target != null) {
            CometeCaps<?, ?> targetCaps = getTargetCaps(target);
            if (targetCaps instanceof StringMatrixCaps) {
                ((StringMatrixCaps) targetCaps).setErrorTextMatrixActivated(useErrorTextMatrix);
            }
        }
    }

    public boolean isOnError(ITextMatrixTarget target) {
        return ObjectUtils.sameObject(getErrorTextMatrix(target), target.getStringMatrix());
    }

    @Override
    protected void cleanWidget(ITextMatrixTarget target) {
        try {
            if (isUseErrorTextMatrix(target)) {
                putInError(target);
            } else {
                target.setStringMatrix(null);
            }
        } catch (Exception e) {
            // ignore exception
        }
    }

}
