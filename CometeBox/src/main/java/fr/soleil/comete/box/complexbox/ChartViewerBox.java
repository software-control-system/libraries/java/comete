/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box.complexbox;

import java.util.Map;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.caps.complexcaps.ChartViewerCaps;
import fr.soleil.comete.definition.data.adapter.DataArrayAdapter;
import fr.soleil.comete.definition.data.controller.ChartViewerController;
import fr.soleil.comete.definition.data.target.complex.IDataArrayTarget;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.service.CompositeKey;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;

public class ChartViewerBox extends AbstractCometeBox<IDataArrayTarget> {

    protected boolean multiDataViewEnabled;

    public ChartViewerBox() {
        super();
        singleConnectionOnly = false;
        multiDataViewEnabled = true;
    }

    /**
     * @return the splitMatrixThroughColumns
     */
    public boolean isSplitMatrixThroughColumns(IKey key) {
        boolean splitMatrixThroughColumns = false;
        DataArrayAdapter<?> adapter = getDataArrayAdapter(key);
        if (adapter != null) {
            splitMatrixThroughColumns = adapter.isSplitMatrixThroughColumns();
        }
        return splitMatrixThroughColumns;
    }

    /**
     * @param splitMatrixThroughColumns the splitMatrixThroughColumns to set
     */
    public <U> void setSplitMatrixThroughColumns(IKey key, boolean splitMatrixThroughColumns) {
        DataArrayAdapter<U> adapter = getDataArrayAdapter(key);
        if ((adapter != null) && (splitMatrixThroughColumns != adapter.isSplitMatrixThroughColumns())) {
            adapter.setSplitMatrixThroughColumns(splitMatrixThroughColumns);
            AbstractDataSource<U> source = getSource(key, false, false);
            if (source != null) {
                AbstractController<U> controller = getController(source.getDataType(), getTargetType());
                if (controller != null) {
                    controller.transmitSourceChange(source);
                }
            }
        }
    }

    protected <U> ChartViewerController<U> getPreparedChartViewerController(AbstractDataSource<U> source) {
        ChartViewerController<U> chartViewerController = null;
        if (source != null) {
            AbstractController<U> controller = getController(source.getDataType(), getTargetType());
            if (controller instanceof ChartViewerController) {
                chartViewerController = (ChartViewerController<U>) controller;
                chartViewerController.registerAdapter(source);
            }
        }
        return chartViewerController;
    }

    /**
     * <big>EXPERT USAGE ONLY</big>. Returns the adapter used with a given source
     * 
     * @param key The {@link IKey} that identifies the source
     * @return A {@link DataArrayAdapter}
     */
    public <U> DataArrayAdapter<U> getDataArrayAdapter(IKey key) {
        DataArrayAdapter<U> adapter = null;
        if (key != null) {
            AbstractDataSource<U> source = getSource(key, false, false);
            ChartViewerController<U> chartViewerController = getPreparedChartViewerController(source);
            if (chartViewerController != null) {
                DataSourceAdapter<U, Map<String, Object>> tmp = chartViewerController.getSourceAdapter(source);
                if (tmp instanceof DataArrayAdapter) {
                    adapter = (DataArrayAdapter<U>) tmp;
                }
            }
        }
        return adapter;
    }

    /**
     * <big>EXPERT USAGE ONLY</big>. Changes the adapter for a given source
     * 
     * @param key The {@link IKey} that identifies the source
     * @param adapter The new adapter to use
     */
    public <U> void setDataArrayAdapter(IKey key, DataArrayAdapter<U> adapter) {
        if ((key != null) && (adapter != null)) {
            AbstractDataSource<U> source = getSource(key, false, false);
            ChartViewerController<U> chartViewerController = getPreparedChartViewerController(source);
            if (chartViewerController != null) {
                chartViewerController.setSourceAdapter(source, adapter);
            }
        }
    }

    @Override
    protected Class<IDataArrayTarget> getTargetClassId() {
        return IDataArrayTarget.class;
    }

    @Override
    protected GenericDescriptor getTargetType() {
        GenericDescriptor[] desc = new GenericDescriptor[] { new GenericDescriptor(String.class),
                new GenericDescriptor(Object.class) };
        return new GenericDescriptor(String.class, desc);
    }

    @Override
    public <U> AbstractController<U> initController(Class<?> targetClass, GenericDescriptor sourceType) {
        ChartViewerController<U> controller = new ChartViewerController<U>(sourceType);
        controller.setMultiDataViewEnabled(multiDataViewEnabled);
        return controller;
    }

    /**
     * Connects an {@link IChartViewer} to a list of data sources
     * 
     * @param widget The {@link IChartViewer} to connect
     * @param keys The keys that identify the data sources to which to connect the {@link IChartViewer}
     * @return Whether the widget was successfully connected to all sources
     */
    public <TI extends IDataArrayTarget, U> boolean connectWidget(TI widget, IKey... keys) {
        boolean result = false;
        if ((widget != null) && (keys != null) && (keys.length > 0)) {
            result = true;
            for (IKey key : keys) {
                result = result && connectWidget(widget, key);
            }
        }
        return result;
    }

    /**
     * Connects an {@link IChartViewer} to 2 sources:
     * <ul>
     * <li>one that represents the x values,</li>
     * <li>one that represents the y values</li>
     * </ul>
     * 
     * @param widget The {@link IChartViewer} to connect
     * @param xKey The {@link IKey} that identifies the X data source
     * @param yKey The {@link IKey} that identifies the Y data source
     * @return Whether the widget was successfully connected
     */
    public <TI extends IDataArrayTarget, U> boolean connectWidgetDual(TI widget, IKey xKey, IKey yKey) {
        return connectWidget(widget, createDualKey(xKey, yKey));
    }

    public static IKey createDualKey(IKey xKey, IKey yKey) {
        CompositeKey compKey = new CompositeKey();
        compKey.addKey(CompositeKey.X_INDEX, xKey);
        compKey.addKey(CompositeKey.Y_INDEX, yKey);
        return compKey;
    }

    @Override
    protected <U> AbstractController<U> initBasicController(GenericDescriptor sourceType) {
        return null;
    }

    @Override
    protected CometeCaps<?, IDataArrayTarget> initTargetCaps(IDataArrayTarget target) {
        return new ChartViewerCaps(target, this);
    }

    /**
     * @return the multiDataViewEnabled
     */
    public boolean isMultiDataViewEnabled() {
        return multiDataViewEnabled;
    }

    /**
     * @param multiDataViewEnabled the multiDataViewEnabled to set
     */
    public void setMultiDataViewEnabled(boolean multiDataViewEnabled) {
        this.multiDataViewEnabled = multiDataViewEnabled;
    }

    @Override
    protected void cleanWidget(IDataArrayTarget target) {
        target.setData(null);
        // try {
        // target.resetAll(false);
        // }
        // catch (Exception e) {
        // // ignore exception
        // }
    }

}
