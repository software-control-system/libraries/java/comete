/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box.matrixbox;

import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.caps.matrixcaps.NumberMatrixCaps;
import fr.soleil.comete.controller.NumberMatrixCometeController;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.swing.ImagePlayer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.RotaryImageViewer;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.matrix.INumberMatrixTarget;

public class NumberMatrixBox extends AbstractMatrixBox<INumberMatrixTarget> {

    protected static int applicationIdNumber = 0;

    @Override
    protected Class<INumberMatrixTarget> getTargetClassId() {
        return INumberMatrixTarget.class;
    }

    @Override
    protected GenericDescriptor getTargetType() {
        return new GenericDescriptor(Object[].class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <U> AbstractController<U> initController(Class<?> targetClass, GenericDescriptor sourceType) {
        return (AbstractController<U>) new NumberMatrixCometeController();
    }

    @Override
    protected <U> AbstractController<U> initBasicController(GenericDescriptor sourceType) {
        return null;
    }

    @Override
    public <TI extends INumberMatrixTarget, U> boolean connectWidget(TI widget, IKey key) {
        if (widget instanceof ImageViewer) {
            ImageViewer viewer = (ImageViewer) widget;
            if (viewer.getApplicationId() == null) {
                viewer.setApplicationId("ImageViewerBox_Viewer_" + applicationIdNumber++);
            }
        } else if (widget instanceof ImagePlayer) {
            ImagePlayer player = (ImagePlayer) widget;
            player.setApplicationId("ImageViewerBox_Player_" + applicationIdNumber++);
        } else if (widget instanceof RotaryImageViewer) {
            RotaryImageViewer viewer = (RotaryImageViewer) widget;
            if (viewer.getApplicationId() == null) {
                viewer.setApplicationId("ImageViewerBox_Viewer_" + applicationIdNumber++);
            }
        }
        return super.connectWidget(widget, key);
    }

    @Override
    protected CometeCaps<?, INumberMatrixTarget> initTargetCaps(INumberMatrixTarget target) {
        return new NumberMatrixCaps(target, this);
    }

    @Override
    protected Class<?> getPrimitiveTargetType() {
        return Number.class;
    }

    @Override
    protected void cleanWidget(INumberMatrixTarget target) {
        try {
            target.setNumberMatrix(null);
            if (target instanceof IImageViewer) {
                ((IImageViewer) target).setText(null);
            }
        } catch (Exception e) {
            // ignore exception
        }
    }

}
