/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box.matrixbox;

import java.util.List;

import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.caps.matrixcaps.TreeNodeMatrixCaps;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.service.GenericDescriptor;

public class TreeNodeMatrixBox extends AbstractMatrixBox<ITreeNode> {

    protected static final GenericDescriptor TARGET_TYPE = new GenericDescriptor(ITreeNode[].class);

    @Override
    protected Class<ITreeNode> getTargetClassId() {
        return ITreeNode.class;
    }

    @Override
    protected GenericDescriptor getTargetType() {
        return TARGET_TYPE;
    }

    @Override
    protected CometeCaps<?, ITreeNode> initTargetCaps(ITreeNode target) {
        return new TreeNodeMatrixCaps(target, this);
    }

    @Override
    protected Class<?> getPrimitiveTargetType() {
        return ITreeNode.class;
    }

    @Override
    protected void cleanWidget(ITreeNode target) {
        if (target != null) {
            List<ITreeNode> children = target.getChildren();
            if (children != null) {
                for (ITreeNode child : children) {
                    target.removeNode(child);
                }
            }
        }
    }

}