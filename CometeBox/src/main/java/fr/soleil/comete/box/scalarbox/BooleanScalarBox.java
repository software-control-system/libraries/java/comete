/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.box.scalarbox;

import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.caps.scalarcaps.BooleanScalarCaps;
import fr.soleil.comete.controller.ScalarCometeController;
import fr.soleil.data.controller.BasicBooleanTargetController;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.scalar.IBooleanTarget;

public class BooleanScalarBox extends ScalarCometeBox<IBooleanTarget, Boolean> {

    @Override
    protected Class<Boolean> getScalarClass() {
        return Boolean.class;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <U> AbstractController<U> initController(Class<?> targetClass, GenericDescriptor sourceType) {

        AbstractController<U> controller = null;

        controller = new ScalarCometeController<U, Boolean>((Class<U>) sourceType.getPrimitiveType(), Boolean.class);

        return controller;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <U> AbstractController<U> initBasicController(GenericDescriptor sourceType) {

        AbstractController<U> controller = null;

        controller = (AbstractController<U>) new BasicBooleanTargetController() {
            @Override
            protected void setDataToTarget(ITarget target, Boolean data, AbstractDataSource<Boolean> source) {
                if (target instanceof BooleanScalarCaps) {
                    ((BooleanScalarCaps) target).setData(data, source);
                } else {
                    super.setDataToTarget(target, data, source);
                }
            }

            @Override
            protected void setDataToGenericTarget(ITarget target, Boolean data, AbstractDataSource<Boolean> source) {
                if (target instanceof BooleanScalarCaps) {
                    ((BooleanScalarCaps) target).setData(data, source);
                } else {
                    super.setDataToGenericTarget(target, data, source);
                }
            }
        };

        return controller;
    }

    @Override
    protected Class<IBooleanTarget> getTargetClassId() {
        return IBooleanTarget.class;
    }

    @Override
    protected GenericDescriptor getTargetType() {
        return new GenericDescriptor(Boolean.class);
    }

    @Override
    protected CometeCaps<?, IBooleanTarget> initTargetCaps(IBooleanTarget target) {
        return new BooleanScalarCaps(target, this);
    }

    @Override
    protected void cleanWidget(IBooleanTarget target) {
        if (target != null) {
            try {
                target.setSelected(false);
            } catch (Exception e) {
                // ignore exception
            }
        }
    }

}