/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.ITarget;

public final class CometeBoxProvider {

    private static final Map<Class<? extends AbstractCometeBox<?>>, AbstractCometeBox<?>> COMETE_BOX_MAP = new HashMap<>();

    private static <T extends ITarget> AbstractCometeBox<T> pushNewCometeBox(
            Class<? extends AbstractCometeBox<T>> boxClass) {
        AbstractCometeBox<T> result = null;
        if (boxClass != null) {
            try {
                result = boxClass.newInstance();
                synchronized (COMETE_BOX_MAP) {
                    COMETE_BOX_MAP.put(boxClass, result);
                }
            } catch (Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .error("Failed to create CometeBox of class " + boxClass.getName(), e);
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public static <T extends ITarget> AbstractCometeBox<T> getCometeBox(
            Class<? extends AbstractCometeBox<T>> boxClass) {
        AbstractCometeBox<T> result = null;
        if (boxClass != null) {
            synchronized (COMETE_BOX_MAP) {
                result = (AbstractCometeBox<T>) COMETE_BOX_MAP.get(boxClass);
            }
            if (result == null) {
                result = pushNewCometeBox(boxClass);
            }
        }
        return result;
    }

}
