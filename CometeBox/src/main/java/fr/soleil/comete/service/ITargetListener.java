/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.service;

import fr.soleil.data.target.ITarget;

public interface ITargetListener<T> {

    /**
     * Is called when a {@link ITarget} <b>receive</b> an information
     * 
     * @param data the information
     */
    public void onTargetInput(T data);

    /**
     * Is called when a {@link ITarget} <b>send</b> an information
     * 
     * @param data the information
     */
    public void onTargetOutput(T data);
}
