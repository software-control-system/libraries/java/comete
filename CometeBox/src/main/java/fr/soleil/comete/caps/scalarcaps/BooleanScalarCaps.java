/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps.scalarcaps;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.scalar.IBooleanTarget;

public class BooleanScalarCaps extends ScalarCaps<Boolean, IBooleanTarget> implements IBooleanTarget {

    public BooleanScalarCaps(IBooleanTarget target, AbstractCometeBox<IBooleanTarget> box) {
        super(target, box);
    }

    @Override
    public void writeData(Boolean data) {
        if (data != null) {
            target.setSelected(data);
        }
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Boolean.class);
    }

    @Override
    public <I> boolean canCastInfo(I info) {
        boolean result = false;
        result = (info != null) && (info instanceof Boolean);
        return result;
    }

    @Override
    public boolean isSelected() {
        return target.isSelected();
    }

    @Override
    public void setSelected(boolean bool) {
        setData(bool, null);
    }
}
