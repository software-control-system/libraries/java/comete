/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps.complexcaps;

import java.util.List;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.definition.data.target.complex.ITreeNodeTarget;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.service.GenericDescriptor;

public class TreeNodeCaps extends CometeCaps<ITreeNode, ITreeNodeTarget> implements ITreeNodeTarget {

    private boolean useSelectionToWrite;
    private boolean useChildren;

    public TreeNodeCaps(ITreeNodeTarget target, AbstractCometeBox<ITreeNodeTarget> box) {
        super(target, box);
        useSelectionToWrite = true;
        useChildren = false;
    }

    @Override
    public void writeData(ITreeNode data) {
        boolean isModified = false;
        if (useSelectionToWrite) {
            ITreeNode[] currentNodes = getSelectedNodes();
            if (currentNodes == null) {
                isModified = updateRootNode(data);
            } else if (currentNodes.length == 1) {
                if (useChildren) {
                    isModified = addChildrenNodes(currentNodes[0], data);
                } else {
                    currentNodes[0].addNodes(data);
                    isModified = true;
                }
            } else {
                ITreeNode rootNode = getRootNode();
                if (rootNode != null) {
                    if (useChildren) {
                        isModified = addChildrenNodes(rootNode, data);
                    } else {
                        rootNode.addNodes(data);
                        isModified = true;
                    }
                }
            }
        } else {
            isModified = updateRootNode(data);
        }
        if (isModified && (data != null) && ((data.getName() == null) || (data.getName().trim().isEmpty()))
                && (!useChildren)) {
            Object key = data.getData();
            data.setName(key.toString());
        }
    }

    private ITreeNode[] extractChildren(ITreeNode node) {
        ITreeNode[] children = null;
        if (node != null) {
            List<ITreeNode> childrenList = node.getChildren();
            if (childrenList != null) {
                children = childrenList.toArray(new ITreeNode[0]);
            }
        }
        return children;
    }

    protected boolean addChildrenNodes(ITreeNode target, ITreeNode childrenToExtract) {
        boolean isModified = false;
        if ((target != null) && (childrenToExtract != null)) {
            ITreeNode[] children = extractChildren(childrenToExtract);
            if ((children != null) && (children.length > 0)) {
                target.addNodes(children);
                isModified = true;
            }
        }
        return isModified;
    }

    protected boolean updateRootNode(ITreeNode data) {
        boolean isModified = false;
        if (useChildren) {
            isModified = addChildrenNodes(getRootNode(), data);
        } else {
            setRootNode(data);
            isModified = true;
        }
        return isModified;
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(ITreeNode.class);
    }

    @Override
    public <I> boolean canCastInfo(I info) {
        boolean result = false;
        result = (info != null) && (info instanceof ITreeNode);
        return result;
    }

    @Override
    public ITreeNode getRootNode() {
        return target.getRootNode();
    }

    @Override
    public void setRootNode(ITreeNode rootNode) {
        target.setRootNode(rootNode);
    }

    @Override
    public ITreeNode[] getSelectedNodes() {
        return target.getSelectedNodes();
    }

    public boolean isUseSelectionToWrite() {
        return useSelectionToWrite;
    }

    public void setUseSelectionToWrite(boolean useSelectionToWrite) {
        this.useSelectionToWrite = useSelectionToWrite;
    }

    /**
     * @return the useChildren
     */
    public boolean isUseChildren() {
        return useChildren;
    }

    /**
     * @param useChildren the useChildren to set
     */
    public void setUseChildren(boolean useChildren) {
        this.useChildren = useChildren;
    }

    @Override
    protected boolean isInErrorWhenNullData() {
        return !isUseSelectionToWrite();
    }

}
