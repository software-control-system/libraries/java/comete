/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps.complexcaps;

import java.util.Collection;
import java.util.Map;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.definition.data.target.complex.IDataArrayTarget;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.IMultiFormatableTarget;

public class ChartViewerCaps extends CometeCaps<Map<String, Object>, IDataArrayTarget>
        implements IDataArrayTarget, IMultiFormatableTarget {

    protected final IMultiFormatableTarget multiFormatableTarget;
    protected final boolean isMultiFormatable;

    public ChartViewerCaps(IDataArrayTarget target, AbstractCometeBox<IDataArrayTarget> box) {
        super(target, box);
        if (target instanceof IMultiFormatableTarget) {
            multiFormatableTarget = (IMultiFormatableTarget) target;
            isMultiFormatable = true;
        } else {
            multiFormatableTarget = null;
            isMultiFormatable = false;
        }
    }

    @Override
    public void writeData(Map<String, Object> data) {
        target.setData(data);
    }

    @Override
    public GenericDescriptor getDataType() {
        GenericDescriptor[] desc = new GenericDescriptor[] { new GenericDescriptor(String.class),
                new GenericDescriptor(Object.class) };
        return new GenericDescriptor(String.class, desc);
    }

    @Override
    public <I> boolean canCastInfo(I info) {
        boolean result = false;
        result = (info != null) && (info instanceof String);
        return result;
    }

    @Override
    public Map<String, Object> getData() {
        return target.getData();
    }

    @Override
    public void addData(Map<String, Object> dataToAdd) {
        if (isSynchronTransmission()) {
            target.addData(dataToAdd);
        } else {
            Runnable dataTransmitter = () -> {
                target.addData(dataToAdd);
            };
            if (shouldReceiveDataInDrawingThread()) {
                getDrawingThreadManager().runInDrawingThread(dataTransmitter);
            } else {
                DATA_SCHEDULER.submit(dataTransmitter);
            }
        }
    }

    @Override
    public void removeData(Collection<String> idsToRemove) {
        if (isSynchronTransmission()) {
            target.removeData(idsToRemove);
        } else {
            Runnable dataTransmitter = () -> {
                target.removeData(idsToRemove);
            };
            if (shouldReceiveDataInDrawingThread()) {
                getDrawingThreadManager().runInDrawingThread(dataTransmitter);
            } else {
                DATA_SCHEDULER.submit(dataTransmitter);
            }
        }
    }

    @Override
    public void setData(Map<String, Object> data) {
        setData(data, null);
    }

    @Override
    public String getFormat(String id) {
        return isMultiFormatable ? multiFormatableTarget.getFormat(id) : getFormat();
    }

    @Override
    public void setFormat(String id, String format) {
        if (isMultiFormatable) {
            multiFormatableTarget.setFormat(id, format);
        } else {
            setFormat(format);
        }
    }
}
