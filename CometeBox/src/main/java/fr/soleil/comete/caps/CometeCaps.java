/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.filter.ConfirmationChecker;
import fr.soleil.comete.filter.FocusChecker;
import fr.soleil.comete.util.TargetUtils;
import fr.soleil.data.filter.AbstractChecker;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.manager.SimpleRandomTaskManager;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IAvailabilityTarget;
import fr.soleil.data.target.IDrawableTarget;
import fr.soleil.data.target.IErrorNotifiableTarget;
import fr.soleil.data.target.IFormatableTarget;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.IUnitTarget;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.data.util.Availability;
import fr.soleil.lib.project.ObjectUtils;

public abstract class CometeCaps<T, DT extends ITarget> implements IComponent, IFormatableTarget, IUnitTarget,
        IAvailabilityTarget, IErrorNotifiableTarget, Mediator<T> {

    protected static final SimpleRandomTaskManager DATA_SCHEDULER = new SimpleRandomTaskManager(
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS), "CometeCaps data transmitter");

    protected final DT target;
    protected final IComponent component;
    protected final boolean isComponent;
    protected final IFormatableTarget formatableTarget;
    protected final boolean isFormat;
    protected final IUnitTarget unitTarget;
    protected final boolean isUnit;
    protected final List<AbstractChecker> checkers;

    protected boolean settable;
    protected boolean ignoreData;
    protected boolean actLikeCommand;
    protected boolean enabled;
    protected boolean editable;
    protected boolean editableComponentManagementEnabled;
    protected boolean readOnly;
    protected volatile boolean isInError, isAvailabilityError;
    protected T lastData;
    protected AbstractDataSource<?> lastOrigin;
    // A lock used to limit concurrent modifications of associated target
    protected final Object targetSettingLock;
    protected boolean synchronTransmission;
    protected boolean colorEnabled;
    protected boolean colorAsForeground;
    protected CometeColor errorColor;
    protected boolean confirmation;

    protected WeakReference<AbstractCometeBox<DT>> boxReference;
    protected TargetDelegate targetDelegate;

    public CometeCaps(DT target, AbstractCometeBox<DT> box) {
        checkers = new ArrayList<>();
        synchronTransmission = false;
        this.target = target;
        if (target instanceof IComponent) {
            component = (IComponent) target;
            isComponent = true;
        } else {
            component = null;
            isComponent = false;
        }
        if (target instanceof IFormatableTarget) {
            formatableTarget = (IFormatableTarget) target;
            isFormat = true;
        } else {
            formatableTarget = null;
            isFormat = false;
        }
        if (target instanceof IUnitTarget) {
            unitTarget = (IUnitTarget) target;
            isUnit = true;
        } else {
            unitTarget = null;
            isUnit = false;
        }
        settable = true;
        ignoreData = false;
        actLikeCommand = false;
        enabled = true;
        editable = true;
        editableComponentManagementEnabled = true;
        readOnly = false;
        isInError = false;
        isAvailabilityError = false;
        // colors deactivated by default (for behavior background compatibility)
        colorEnabled = false;
        colorAsForeground = false;
        errorColor = null;
        targetDelegate = new TargetDelegate();
        lastData = null;
        lastOrigin = null;
        targetSettingLock = new Object();
        confirmation = false;
        this.boxReference = (box == null ? null : new WeakReference<>(box));
        target.addMediator(this);
        connectCheckers();
    }

    public DT getTarget() {
        return target;
    }

    public void addCheckerToTarget(AbstractChecker checker) {
        if (checker != null) {
            synchronized (checkers) {
                if (!checkers.contains(checker)) {
                    checkers.add(checker);
                }
            }
        }
    }

    public void removeCheckerToTarget(AbstractChecker checker) {
        if (checker != null) {
            synchronized (checkers) {
                checkers.remove(checker);
            }
        }
    }

    public List<AbstractChecker> getTargetCheckers() {
        List<AbstractChecker> result = new ArrayList<>();
        synchronized (checkers) {
            result.addAll(checkers);
        }
        return result;
    }

    /***
     * Connect standard checkers to the donate {@link ITarget} and {@link AbstractController}
     * 
     * @param widget
     */
    protected void connectCheckers() {
        List<AbstractChecker> checkers = getTargetCheckers();

        boolean hasNoConfChecker = true;
        boolean hasNoFocusChecker = true;
        for (AbstractChecker checker : checkers) {
            hasNoConfChecker &= !(checker instanceof ConfirmationChecker);
            hasNoFocusChecker &= !(checker instanceof FocusChecker);
        }

        if (hasNoConfChecker) {
            addCheckerToTarget(new ConfirmationChecker());
        }

        if (hasNoFocusChecker) {
            addCheckerToTarget(new FocusChecker());
        }
    }

    @SuppressWarnings("unchecked")
    protected <C> C getLinkedChecker(Class<C> checkerClass) {
        C result = null;
        List<AbstractChecker> checkers = getTargetCheckers();
        for (AbstractChecker checker : checkers) {
            if (checkerClass.isAssignableFrom(checker.getClass())) {
                result = (C) checker;
            }
        }
        return result;
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
        ConfirmationChecker checker = getLinkedChecker(ConfirmationChecker.class);
        if (checker != null) {
            checker.setConfirmation(confirmation);
        }
    }

    public boolean isConfirmation() {
        boolean result = false;
        ConfirmationChecker checker = getLinkedChecker(ConfirmationChecker.class);
        if (checker != null) {
            result = checker.isConfirmation();
        }
        return result;
    }

    public void setConfirmationMessage(String message) {
        ConfirmationChecker checker = getLinkedChecker(ConfirmationChecker.class);
        if (checker != null) {
            checker.setConfirmationMessage(message);
        }
    }

    public String getConfirmationMessage() {
        String result = null;
        ConfirmationChecker checker = getLinkedChecker(ConfirmationChecker.class);
        if (checker != null) {
            result = checker.getConfirmationMessage();
        }
        return result;
    }

    public void setConfirmationTitle(String message) {
        ConfirmationChecker checker = getLinkedChecker(ConfirmationChecker.class);
        if (checker != null) {
            checker.setConfirmationTitle(message);
        }
    }

    public String getConfirmationTitle() {
        String result = null;
        ConfirmationChecker checker = getLinkedChecker(ConfirmationChecker.class);
        if (checker != null) {
            result = checker.getConfirmationTitle();
        }
        return result;
    }

    public boolean isSettable() {
        return settable;
    }

    public void setSettable(boolean state) {
        settable = state;
    }

    public boolean isIgnoreData() {
        return ignoreData;
    }

    public void setIgnoreData(boolean ignoreData) {
        this.ignoreData = ignoreData;
    }

    public boolean isActLikeCommand() {
        return actLikeCommand;
    }

    public void setActLikeCommand(boolean actLikeCommand) {
        this.actLikeCommand = actLikeCommand;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        if (isComponent) {
            component.setEnabled(enabled);
        }
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        TargetUtils.updateTargetAvailabilities(lastOrigin, target, editable, isEditableComponentManagementEnabled());
    }

    public boolean isEditableComponentManagementEnabled() {
        return editableComponentManagementEnabled;
    }

    public void setEditableComponentManagementEnabled(boolean editableComponentManagementEnabled) {
        this.editableComponentManagementEnabled = editableComponentManagementEnabled;
    }

    public void setReadOnly(boolean state) {
        readOnly = state;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        targetDelegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        targetDelegate.removeMediator(mediator);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <I, V extends TargetInformation<I>> void transmitTargetChange(V targetInformation) {
        if (targetInformation != null) {
            if (canCastInfo(targetInformation.getInformationData())) {
                T data = (T) targetInformation.getInformationData();
                AbstractCometeBox<DT> box = ObjectUtils.recoverObject(boxReference);
                // No need to filter output when target is not connected
                if ((box != null) && (box.isTargetConnected(target))) {
                    if (isTargetOutputChecked(data)) {
                        sendInformation((TargetInformation<T>) targetInformation);
                    } else if (canCancelTargetOutput()) {
                        writeData(lastData);
                    }
                }
            }
        }
    }

    protected boolean canCancelTargetOutput() {
        // A target output can be canceled by default
        return true;
    }

    protected <V extends TargetInformation<T>> void sendInformation(V targetInformation) {
        setIgnoreData(false);
        targetInformation.setConcernedTarget(this);
        targetDelegate.warnMediators(targetInformation);
    }

    /**
     * @return the synchronTransmission
     */
    public boolean isSynchronTransmission() {
        return synchronTransmission;
    }

    /**
     * @param synchronTransmission the synchronTransmission to set
     */
    public void setSynchronTransmission(boolean synchronTransmission) {
        this.synchronTransmission = synchronTransmission;
    }

    protected void checkForError(Object data, boolean availability) {
        if (data == null) {
            if (availability || isInErrorWhenNullData()) {
                // If it was not already in error
                setInError(availability);
            }
        } else {
            // ReEnable a target only when it was on error
            if (isComponent) {
                if (availability) {
                    if (!isInError) {
                        TargetUtils.getNotNullDrawingThreadManager(component).runInDrawingThread(() -> {
                            component.setEnabled(enabled);
                        });
                    }
                } else if (!isAvailabilityError) {
                    TargetUtils.getNotNullDrawingThreadManager(component).runInDrawingThread(() -> {
                        component.setEnabled(enabled);
                    });
                }
            }
            if (availability) {
                isAvailabilityError = false;
            } else {
                isInError = false;
            }
        }
    }

    public void setData(final T data, final AbstractDataSource<?> origin) {
        Runnable dataTransmitter = () -> {
            // lock targetSettingLock to limit concurrent modifications of associated target
            boolean dataWritten = false;
            synchronized (targetSettingLock) {
                if (origin != null) {
                    lastOrigin = origin;
                }
                TargetUtils.updateTargetAvailabilities(origin, target, editable,
                        isEditableComponentManagementEnabled());
                if (isReadyForInput()) {
                    if (isInputValidated(data)) {
                        checkForError(data, false);
                        lastData = data;
                        writeData(data);
                        dataWritten = true;
                    }
                } else {
                    // JAVAAPI-524: put in error when connected to null data and not settable
                    checkForError(data, false);
                }
            }
            // Do this outside of synchronization block to avoid some dead locks
            if (dataWritten) {
                fireDataChanged(data);
            }
        };
        if (isSynchronTransmission()) {
            dataTransmitter.run();
        } else if (shouldReceiveDataInDrawingThread()) {
            getDrawingThreadManager().runInDrawingThread(dataTransmitter);
        } else {
            DATA_SCHEDULER.submit(dataTransmitter);
        }
    }

    protected void setInError(boolean availability) {
        if (availability) {
            if (!isAvailabilityError) {
                isAvailabilityError = true;
            }
        } else {
            if (!isInError) {
                isInError = true;
            }
        }
        setTargetInErrorMode();
    }

    public void setInError() {
        setInError(false);
    }

    /**
     * Returns whether this {@link CometeCaps} should be considered in error when receiving <code>null</code> data
     * 
     * @return A <code>boolean</code> value
     */
    protected boolean isInErrorWhenNullData() {
        return true;
    }

    protected void fireDataChanged(T data) {
        // nothing to do here
    }

    protected boolean isReadyForInput() {
        return isSettable() && (!isIgnoreData());
    }

    protected boolean isInputValidated(T data) {
        boolean result = true;
        synchronized (checkers) {
            for (AbstractChecker checker : checkers) {
                result &= checker.getInputDataCheck(target, data);
            }
        }
        return result;
    }

    protected boolean isTargetInputChecked(T data) {
        boolean result = isReadyForInput();
        if (result) {
            result = isInputValidated(data);
        }
        return result;
    }

    protected boolean isTargetOutputChecked(T data) {
        boolean result = !readOnly;
        if (result) {
            synchronized (checkers) {
                for (AbstractChecker checker : checkers) {
                    result &= checker.getOutputDataCheck(target, data);
                }
            }
        }
        return result;
    }

    public abstract GenericDescriptor getDataType();

    public abstract <I> boolean canCastInfo(I info);

    public abstract void writeData(T data);

    public void setTargetInErrorMode() {
        if (isComponent) {
            component.setEnabled(false);
            if (colorEnabled && (errorColor != null)) {
                if (colorAsForeground) {
                    component.setCometeForeground(errorColor);
                } else {
                    component.setCometeBackground(errorColor);
                }
            }
        }
    }

    /**
     * @return the colorEnabled
     */
    public boolean isColorEnabled() {
        return colorEnabled;
    }

    /**
     * @param colorEnabled the colorEnabled to set
     */
    public void setColorEnabled(boolean colorEnabled) {
        this.colorEnabled = colorEnabled;
    }

    /**
     * @return the colorAsForeground
     */
    public boolean isColorAsForeground() {
        return colorAsForeground;
    }

    /**
     * @param colorAsForeground the colorAsForeground to set
     */
    public void setColorAsForeground(boolean colorAsForeground) {
        this.colorAsForeground = colorAsForeground;
    }

    public CometeColor getErrorColor() {
        return errorColor;
    }

    public void setErrorColor(CometeColor errorColor) {
        this.errorColor = errorColor;
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        if (target instanceof IErrorNotifiableTarget) {
            ((IErrorNotifiableTarget) target).notifyForError(message, error);
        }
    }

    @Override
    public void setAvailable(Availability availability) {
        checkForError(availability, true);
    }

    // nothing to do
    @Override
    public void removeFilterFromSource(AbstractDataFilter<T> filter, AbstractDataSource<T> source) {
    }

    @Override
    public <V> void removeFilterFromTarget(AbstractDataFilter<V> filter, ITarget target) {
    }

    @Override
    public void addFilterToSource(AbstractDataFilter<T> filter, AbstractDataSource<T> source) {
    }

    @Override
    public <V> void addFilterToTarget(AbstractDataFilter<V> filter, ITarget target) {
    }

    @Override
    public boolean addLink(AbstractDataSource<T> source, ITarget target) {
        return false;
    }

    @Override
    public void removeAllReferencesTo(AbstractDataSource<T> source) {
    }

    @Override
    public void removeAllReferencesTo(ITarget target) {
    }

    @Override
    public void removeLink(AbstractDataSource<T> source, ITarget target) {
    }

    @Override
    public void transmitSourceChange(AbstractDataSource<T> source) {
    }

    @Override
    public boolean hasFocus() {
        return isComponent ? component.hasFocus() : false;
    }

    @Override
    public void setOpaque(boolean opaque) {
        if (isComponent) {
            component.setOpaque(true);
        }
    }

    @Override
    public boolean isOpaque() {
        return isComponent ? component.isOpaque() : false;
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        if (isComponent) {
            component.setCometeBackground(color);
        }
    }

    @Override
    public CometeColor getCometeBackground() {
        return isComponent ? component.getCometeBackground() : null;
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        if (isComponent) {
            component.setCometeForeground(color);
        }
    }

    @Override
    public CometeColor getCometeForeground() {
        return isComponent ? component.getCometeForeground() : null;
    }

    @Override
    public void setCometeFont(CometeFont font) {
        if (isComponent) {
            component.setCometeFont(font);
        }
    }

    @Override
    public CometeFont getCometeFont() {
        return isComponent ? component.getCometeFont() : null;
    }

    @Override
    public void setToolTipText(String text) {
        if (isComponent) {
            component.setToolTipText(text);
        }
    }

    @Override
    public String getToolTipText() {
        return isComponent ? component.getToolTipText() : null;
    }

    @Override
    public void setVisible(boolean visible) {
        if (isComponent) {
            component.setVisible(visible);
        }
    }

    @Override
    public boolean isVisible() {
        return isComponent ? component.isVisible() : false;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        if (isComponent) {
            component.setHorizontalAlignment(halign);
        }
    }

    @Override
    public int getHorizontalAlignment() {
        return isComponent ? component.getHorizontalAlignment() : 0;
    }

    @Override
    public void setSize(int width, int height) {
        if (isComponent) {
            component.setSize(width, height);
        }
    }

    @Override
    public void setPreferredSize(int width, int height) {
        if (isComponent) {
            component.setPreferredSize(width, height);
        }
    }

    @Override
    public int getWidth() {
        return isComponent ? component.getWidth() : 0;
    }

    @Override
    public int getHeight() {
        return isComponent ? component.getHeight() : 0;
    }

    @Override
    public void setLocation(int x, int y) {
        if (isComponent) {
            component.setLocation(x, y);
        }
    }

    @Override
    public int getX() {
        return isComponent ? component.getX() : 0;
    }

    @Override
    public int getY() {
        return isComponent ? component.getY() : 0;
    }

    @Override
    public boolean isEditingData() {
        return isComponent ? component.isEditingData() : false;
    }

    @Override
    public void setTitledBorder(String title) {
        if (isComponent) {
            component.setTitledBorder(title);
        }
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        if (isComponent) {
            component.addMouseListener(listener);
        }
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        if (isComponent) {
            component.removeMouseListener(listener);
        }
    }

    @Override
    public void removeAllMouseListeners() {
        if (isComponent) {
            component.removeAllMouseListeners();
        }
    }

    @Override
    public String getFormat() {
        return isFormat ? formatableTarget.getFormat() : null;
    }

    @Override
    public void setFormat(String format) {
        if (isFormat) {
            formatableTarget.setFormat(format);
        }
    }

    @Override
    public String getUnit() {
        return isUnit ? unitTarget.getUnit() : null;
    }

    @Override
    public void setUnit(String unit) {
        if (isUnit) {
            unitTarget.setUnit(unit);
        }
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return component == null ? TargetUtils.getNotNullDrawingThreadManager(target)
                : TargetUtils.getNotNullDrawingThreadManager(component);
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        boolean shouldReceiveDataInDrawingThread;
        if (component == null) {
            if (target instanceof IDrawableTarget) {
                shouldReceiveDataInDrawingThread = true;
            } else {
                shouldReceiveDataInDrawingThread = false;
            }
        } else {
            try {
                shouldReceiveDataInDrawingThread = component.shouldReceiveDataInDrawingThread();
            } catch (AbstractMethodError e) {
                // Associated project is not up to date: use previous behavior
                shouldReceiveDataInDrawingThread = true;
            }
        }
        return shouldReceiveDataInDrawingThread;
    }

}
