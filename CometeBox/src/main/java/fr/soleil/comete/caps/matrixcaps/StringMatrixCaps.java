/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps.matrixcaps;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.matrix.ITextMatrixTarget;

public class StringMatrixCaps extends MatrixCaps<String[][], ITextMatrixTarget> implements ITextMatrixTarget {

    protected String[][] errorTextMatrix;
    protected boolean errorTextMatrixActivated;

    public StringMatrixCaps(ITextMatrixTarget target, AbstractCometeBox<ITextMatrixTarget> box) {
        super(target, box);
        errorTextMatrixActivated = true;
        errorTextMatrix = StringMatrixBox.DEFAULT_ERROR_STRING_MATRIX;
    }

    @Override
    public void writeData(String[][] data) {
        if (data == null) {
            setTargetInErrorMode();
        } else {
            target.setStringMatrix(data);
        }
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(String[][].class);
    }

    @Override
    public <I> boolean canCastInfo(I info) {
        return (info instanceof String[][]);
    }

    @Override
    public String[] getFlatStringMatrix() {
        return target.getFlatStringMatrix();
    }

    @Override
    public String[][] getStringMatrix() {
        return target.getStringMatrix();
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        if (value == null) {
            setTargetInErrorMode();
        } else {
            target.setFlatStringMatrix(value, width, height);
        }
    }

    @Override
    public void setStringMatrix(String[][] value) {
        setData(value, null);
    }

    @Override
    public void setTargetInErrorMode() {
        if (isErrorTextMatrixActivated() && isReadyForInput()) {
            target.setStringMatrix(getErrorTextMatrix());
        }
        super.setTargetInErrorMode();
    }

    /**
     * @return the errorTextActivated
     */
    public boolean isErrorTextMatrixActivated() {
        return errorTextMatrixActivated;
    }

    /**
     * @param errorTextActivated the errorTextActivated to set
     */
    public void setErrorTextMatrixActivated(boolean errorTextActivated) {
        this.errorTextMatrixActivated = errorTextActivated;
    }

    public String[][] getErrorTextMatrix() {
        return errorTextMatrix;
    }

    public void setErrorTextMatrix(String[][] errorTextMatrix) {
        this.errorTextMatrix = errorTextMatrix;
    }
}
