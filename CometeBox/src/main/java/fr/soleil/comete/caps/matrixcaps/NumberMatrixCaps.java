/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps.matrixcaps;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.matrix.INumberMatrixTarget;

public class NumberMatrixCaps extends MatrixCaps<Object[], INumberMatrixTarget> implements INumberMatrixTarget {

    public NumberMatrixCaps(INumberMatrixTarget target, AbstractCometeBox<INumberMatrixTarget> box) {
        super(target, box);
    }

    @Override
    public void writeData(Object[] data) {
        target.setNumberMatrix(data);
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Object[].class);
    }

    @Override
    public <I> boolean canCastInfo(I info) {
        return (info instanceof Object[]);
    }

    @Override
    public Object getFlatNumberMatrix() {
        return target.getFlatNumberMatrix();
    }

    @Override
    public Object[] getNumberMatrix() {
        return target.getNumberMatrix();
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        target.setFlatNumberMatrix(value, width, height);
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        setData(value, null);
    }

}
