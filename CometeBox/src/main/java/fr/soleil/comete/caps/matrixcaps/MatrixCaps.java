/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps.matrixcaps;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.data.target.matrix.IMatrixTarget;

public abstract class MatrixCaps<T, DT extends IMatrixTarget> extends CometeCaps<T, DT> implements IMatrixTarget {

    public MatrixCaps(DT target, AbstractCometeBox<DT> box) {
        super(target, box);
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return target.getMatrixDataHeight(concernedDataClass);
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        return target.getMatrixDataWidth(concernedDataClass);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return target.isPreferFlatValues(concernedDataClass);
    }

}
