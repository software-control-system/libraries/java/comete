/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps.scalarcaps;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.lib.project.math.MathConst;

public class NumberScalarCaps extends ScalarCaps<Number, INumberTarget> implements INumberTarget {

    public NumberScalarCaps(INumberTarget target, AbstractCometeBox<INumberTarget> box) {
        super(target, box);
    }

    @Override
    public void writeData(Number data) {
        if (data == null) {
            target.setNumberValue(MathConst.NAN_FOR_NULL);
        } else {
            target.setNumberValue(data);
        }
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Number.class);
    }

    @Override
    public <I> boolean canCastInfo(I info) {
        boolean result = false;
        result = (info != null) && (info instanceof Number);
        return result;
    }

    @Override
    public Number getNumberValue() {
        return target.getNumberValue();
    }

    @Override
    public void setNumberValue(Number value) {
        setData(value, null);
    }
}
