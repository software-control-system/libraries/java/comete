/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps.matrixcaps;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.matrix.IBooleanMatrixTarget;

public class BooleanMatrixCaps extends MatrixCaps<boolean[][], IBooleanMatrixTarget> implements IBooleanMatrixTarget {

    public BooleanMatrixCaps(IBooleanMatrixTarget target, AbstractCometeBox<IBooleanMatrixTarget> box) {
        super(target, box);
    }

    @Override
    public void writeData(boolean[][] data) {
        target.setBooleanMatrix(data);
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(boolean[][].class);
    }

    @Override
    public <I> boolean canCastInfo(I info) {
        boolean result = false;
        result = (info != null) && (info instanceof boolean[][]);
        return result;
    }

    @Override
    public boolean[] getFlatBooleanMatrix() {
        return target.getFlatBooleanMatrix();
    }

    @Override
    public boolean[][] getBooleanMatrix() {
        return target.getBooleanMatrix();
    }

    @Override
    public void setFlatBooleanMatrix(boolean[] value, int width, int height) {
        target.setFlatBooleanMatrix(value, width, height);
    }

    @Override
    public void setBooleanMatrix(boolean[][] value) {
        setData(value, null);
    }

}
