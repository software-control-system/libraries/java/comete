/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps.scalarcaps;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.scalar.ITextTarget;

public class StringScalarCaps extends ScalarCaps<String, ITextTarget> implements ITextTarget {

    private static final String DIALOG_TITLE = "Message";
    private static final String VALUE = "value";

    protected String errorText;
    protected boolean errorTextActivated;
    protected boolean outputInPopup;

    public StringScalarCaps(ITextTarget target, AbstractCometeBox<ITextTarget> box) {
        super(target, box);
        errorTextActivated = true;
        errorText = StringScalarBox.DEFAULT_ERROR_STRING;
        outputInPopup = false;
    }

    @Override
    public boolean isSettable() {
        return super.isSettable() || isOutputInPopup();
    }

    @Override
    public void writeData(String data) {
        if (data == null) {
            if (super.isSettable()) {
                setTargetInErrorMode();
            }
        } else {
            if (isOutputInPopup()) {
                if (!data.trim().isEmpty()) {
                    Component owner = target instanceof Component ? (Component) target : null;
                    showConfirmationDialog(owner, data);
                }
            } else {
                target.setText(data);
            }
        }
    }

    @Override
    public void setTargetInErrorMode() {
        if (isErrorTextActivated() && isReadyForInput()) {
            target.setText(getErrorText());
        }
        super.setTargetInErrorMode();
    }

    @Override
    protected boolean canCancelTargetOutput() {
        // A target output can't be canceled if output is in popup
        return !isOutputInPopup();
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(String.class);
    }

    @Override
    public <I> boolean canCastInfo(I info) {
        boolean result = false;
        result = (info == null) || (info instanceof String);
        return result;
    }

    @Override
    public String getText() {
        return target.getText();
    }

    @Override
    public void setText(String text) {
        setData(text, null);
    }

    /**
     * @return the errorTextActivated
     */
    public boolean isErrorTextActivated() {
        return errorTextActivated;
    }

    /**
     * @param errorTextActivated the errorTextActivated to set
     */
    public void setErrorTextActivated(boolean errorTextActivated) {
        this.errorTextActivated = errorTextActivated;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        if (errorText == null) {
            errorText = StringScalarBox.DEFAULT_ERROR_STRING;
        }
        this.errorText = errorText;
    }

    public boolean isOutputInPopup() {
        return outputInPopup;
    }

    public void setOutputInPopup(boolean isPopup) {
        outputInPopup = isPopup;
    }

    private void showConfirmationDialog(Component owner, Object data) {
        // We prefer not using JOptionPane.showMessageDialog, in order to have a resizable JDialog
        final JDialog dialog = new JDialog(SwingUtilities.getWindowAncestor(owner), DIALOG_TITLE);
        JOptionPane contentPane = new JOptionPane(data, JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION,
                null);
        dialog.setContentPane(contentPane);
        dialog.pack();
        dialog.setLocationRelativeTo(owner);
        dialog.setVisible(true);

        contentPane.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (VALUE.equals(evt.getPropertyName())) {
                    dialog.dispose();
                }
            }
        });
    }
}
