/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps.matrixcaps;

import java.util.List;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.definition.listener.ITreeNodeListener;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.lib.project.math.ArrayUtils;

public class TreeNodeMatrixCaps extends MatrixCaps<ITreeNode[][], fr.soleil.comete.definition.widget.util.ITreeNode>
        implements ITreeNode {
    protected static final GenericDescriptor DATA_TYPE = new GenericDescriptor(ITreeNode[][].class);

    public TreeNodeMatrixCaps(ITreeNode target,
            AbstractCometeBox<fr.soleil.comete.definition.widget.util.ITreeNode> box) {
        super(target, box);
    }

    @Override
    public ITreeNode getParent() {
        return target.getParent();
    }

    @Override
    public void setParent(ITreeNode parent) {
        target.setParent(parent);
    }

    @Override
    public List<ITreeNode> getChildren() {
        return target.getChildren();
    }

    @Override
    public boolean isAncestorOf(ITreeNode node) {
        return target.isAncestorOf(node);
    }

    @Override
    public void addNodes(ITreeNode... nodes) {
        setData(new ITreeNode[][] { nodes }, null);
    }

    @Override
    public void addNode(ITreeNode node, int index) {
        target.addNode(node, index);
    }

    @Override
    public void removeNode(ITreeNode node) {
        target.removeNode(node);
    }

    @Override
    public void removeNodeTemporary(ITreeNode node) {
        target.removeNodeTemporary(node);
    }

    @Override
    public void removeNode(int index) {
        target.removeNode(index);
    }

    @Override
    public ITreeNode getNodeAt(int index) {
        return target.getNodeAt(index);
    }

    @Override
    public String getName() {
        return target.getName();
    }

    @Override
    public void setName(String name) {
        target.setName(name);
    }

    @Override
    public Object getData() {
        return target.getData();
    }

    @Override
    public void setData(Object data) {
        target.setData(data);
    }

    @Override
    public CometeImage getImage() {
        return target.getImage();
    }

    @Override
    public void setImage(CometeImage image) {
        target.setImage(image);
    }

    @Override
    public void setToolTip(String toolTip) {
        target.setToolTip(toolTip);
    }

    @Override
    public String getToolTip() {
        return target.getToolTip();
    }

    @Override
    public void addTreeNodeListener(ITreeNodeListener listener) {
        target.addTreeNodeListener(listener);
    }

    @Override
    public void removeTreeNodeListener(ITreeNodeListener listener) {
        target.removeTreeNodeListener(listener);
    }

    @Override
    public void removeAllTreeNodeListeners() {
        target.removeAllTreeNodeListeners();
    }

    @Override
    public boolean isTreeNodeListenerRegistered(ITreeNodeListener listener) {
        return target.isTreeNodeListenerRegistered(listener);
    }

    @Override
    public GenericDescriptor getDataType() {
        return DATA_TYPE;
    }

    @Override
    public <I> boolean canCastInfo(I info) {
        return (info instanceof ITreeNode[]);
    }

    @Override
    public void writeData(ITreeNode[][] data) {
        List<ITreeNode> children = target.getChildren();
        ITreeNode[] transformed = (ITreeNode[]) ArrayUtils.convertArrayDimensionFromNTo1(data);
        try {
            if ((children != null) && (transformed != null)) {
                for (ITreeNode node : transformed) {
                    children.remove(node);
                }
                for (ITreeNode node : children) {
                    target.removeNode(node);
                }
            }
            target.addNodes(transformed);
        } catch (Exception e) {
            LoggerFactory.getLogger(LOGGER_ACCESS).error("Failed to transmit nodes", e);
        }
    }

}
