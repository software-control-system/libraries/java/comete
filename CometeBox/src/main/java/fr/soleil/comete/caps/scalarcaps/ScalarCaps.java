/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.caps.scalarcaps;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.caps.CometeCaps;
import fr.soleil.comete.service.ITargetListener;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.data.target.scalar.IScalarTarget;

public abstract class ScalarCaps<T, DT extends IScalarTarget> extends CometeCaps<T, DT> {

    protected final List<ITargetListener<T>> listeners = new ArrayList<ITargetListener<T>>();

    protected boolean unitEnabled;
    protected boolean formatEnabled;

    public ScalarCaps(DT target, AbstractCometeBox<DT> box) {
        super(target, box);
        unitEnabled = true;
        formatEnabled = true;
        colorEnabled = true;
        colorAsForeground = false;
    }

    public void addTargetListener(ITargetListener<T> targetListener) {
        if (targetListener != null) {
            synchronized (listeners) {
                if (!listeners.contains(targetListener)) {
                    listeners.add(targetListener);
                }
            }
        }
    }

    public void removeTargetListener(ITargetListener<T> targetListener) {
        if (targetListener != null) {
            synchronized (listeners) {
                listeners.remove(targetListener);
            }
        }
    }

    public List<ITargetListener<T>> getListeners() {
        List<ITargetListener<T>> result = new ArrayList<ITargetListener<T>>();
        synchronized (listeners) {
            result.addAll(listeners);
        }
        return result;
    }

    @Override
    protected <V extends TargetInformation<T>> void sendInformation(V targetInformation) {
        T data = targetInformation.getInformationData();
        synchronized (listeners) {
            for (ITargetListener<T> listener : listeners) {
                listener.onTargetOutput(data);
            }
        }
        super.sendInformation(targetInformation);
    }

    @Override
    protected void fireDataChanged(T data) {
        synchronized (listeners) {
            for (ITargetListener<T> listener : listeners) {
                listener.onTargetInput(data);
            }
        }
    }

    /**
     * @return the unitEnabled
     */
    public boolean isUnitEnabled() {
        return unitEnabled;
    }

    /**
     * @param unitEnabled the unitEnabled to set
     */
    public void setUnitEnabled(boolean unitEnabled) {
        this.unitEnabled = unitEnabled;
    }

    /**
     * @return the formatEnabled
     */
    public boolean isFormatEnabled() {
        return formatEnabled;
    }

    /**
     * @param formatEnabled the formatEnabled to set
     */
    public void setFormatEnabled(boolean formatEnabled) {
        this.formatEnabled = formatEnabled;
    }

}
