/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.component;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.ILabel;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.comete.swing.util.IIconComponent;
import fr.soleil.comete.swing.util.ImageIconCometeImage;
import fr.soleil.comete.swing.util.ImageTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.text.DynamicForegroundLabel;

public class StateLEDLabel extends DynamicForegroundLabel implements ILabel, IIconComponent {

    private static final long serialVersionUID = -7147231304560594045L;

    protected static final String ON = "ON";
    protected static final String OFF = "OFF";
    protected static final String CLOSE = "CLOSE";
    protected static final String OPEN = "OPEN";
    protected static final String INSERT = "INSERT";
    protected static final String EXTRACT = "EXTRACT";
    protected static final String MOVING = "MOVING";
    protected static final String STANDBY = "STANDBY";
    protected static final String FAULT = "FAULT";
    protected static final String INIT = "INIT";
    protected static final String RUNNING = "RUNNING";
    protected static final String ALARM = "ALARM";
    protected static final String DISABLE = "DISABLE";
    protected static final String UNKNOWN = "UNKNOWN";

    protected static final String TEXT_FORMAT = "%s: %s";

    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;
    private final Object bgLock;

    private volatile String lastToolTip, lastText, lastState;
    private volatile boolean stateInText, stateInToolTip;

    public StateLEDLabel() {
        super();
        bgLock = new Object();
        setAntiAliasingEnabled(true);
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        stateInText = stateInToolTip = false;
        lastToolTip = lastText = lastState = null;
        setIconId(-1);
    }

    /**
     * Returns the text displayed by this {@link StateLEDLabel}, without state.
     * 
     * @return The text displayed by this {@link StateLEDLabel}, without state.
     */
    protected String getDisplayedText() {
        return lastText;
    }

    /**
     * Sets the text displayed by this {@link StateLEDLabel}.
     * 
     * @param text The text to set.
     */
    public void setDisplayedText(String text) {
        this.lastText = text;
        updateText();
    }

    /**
     * Calls {@link DynamicForegroundLabel#setText(String)}.
     * <p>
     * Only used for component's extension.
     * </p>
     * 
     * @param text The text to set.
     */
    protected final void setForcedText(String text) {
        super.setText(text);
    }

    /**
     * Calls {@link DynamicForegroundLabel#setToolTipText(String)}.
     * <p>
     * Only used for component's extension.
     * </p>
     * 
     * @param text The tooltip text to set.
     */
    protected final void setForcedTooltipText(String text) {
        super.setToolTipText(text);
    }

    /**
     * Returns whether the state will be displayed in text.
     * 
     * @return Whether the state will be displayed in text.
     * @see #setStateInText(boolean)
     */
    public boolean isStateInText() {
        return stateInText;
    }

    /**
     * Sets whether the state should be displayed in text.
     * 
     * @param stateInText Whether the state should be displayed in text.
     *            <p>
     *            If <code>true</code>, the state received in {@link #setText(String)} will be added to the text
     *            received in {@link #setDisplayedText(String)}.
     *            So, the displayed text will be <code>"text: state"</code>.
     *            </p>
     *            <p>
     *            If <code>false</code>, the state won't be displayed in text, which means the text will
     *            be the one set in {@link #setDisplayedText(String)}.
     *            </p>
     * @see #setDisplayedText(String)
     */
    public void setStateInText(boolean stateInText) {
        if (this.stateInText != stateInText) {
            this.stateInText = stateInText;
            updateText();
        }
    }

    /**
     * Returns whether the state will be displayed in tooltip.
     * 
     * @return Whether the state will be displayed in tooltip.
     * @see #setStateInToolTip(boolean)
     */
    public boolean isStateInToolTip() {
        return stateInToolTip;
    }

    /**
     * Sets whether the state should be displayed in tooltip.
     * 
     * @param stateInToolTip Whether the state should be displayed in tooltip.
     *            <p>
     *            If <code>true</code>, the state received in {@link #setText(String)} will be added to the tooltip text
     *            received in {@link #setToolTipText(String)}.
     *            So, the displayed tooltip text will be <code>"tooltip: state"</code>.
     *            </p>
     *            <p>
     *            If <code>false</code>, the state won't be displayed in tooltip text, which means the tooltip text will
     *            be the one set in {@link #setToolTipText(String)}.
     *            </p>
     */
    public void setStateInToolTip(boolean stateInToolTip) {
        if (this.stateInToolTip != stateInToolTip) {
            this.stateInToolTip = stateInToolTip;
            updateToolTip();
        }
    }

    /**
     * Returns the text that must be set (either in text or in tooltip), according to the previously set one, the state
     * and the <code>boolean</code> that tells whether to display sate or not.
     * 
     * @param lastText The previously set text.
     * @param lastState The state.
     * @param displayState The <code>boolean</code> that tells whether to display sate or not.
     * @return A {@link String}: the adapted text.
     */
    protected String getNewText(String lastText, String lastState, boolean displayState) {
        String newText;
        if (displayState) {
            if (lastText == null) {
                newText = lastState;
            } else if (lastState == null) {
                newText = lastText;
            } else {
                newText = String.format(TEXT_FORMAT, lastText, lastState);
            }
        } else {
            newText = lastText;
        }
        return newText;
    }

    /**
     * Updates this {@link StateLEDLabel}'s text, according to last text and state.
     */
    protected void updateText() {
        String newText = getNewText(lastText, lastState, isStateInText());
        if (newText == null) {
            newText = ObjectUtils.EMPTY_STRING;
        }
        if (!ObjectUtils.sameObject(newText, getText())) {
            setForcedText(newText);
        }
    }

    /**
     * Updates this {@link StateLEDLabel}'s tooltip text, according to last tooltip text and state.
     */
    protected void updateToolTip() {
        String newTooltip = getNewText(lastToolTip, lastState, isStateInToolTip());
        if (!ObjectUtils.sameObject(getToolTipText(), newTooltip)) {
            setForcedTooltipText(newTooltip);
        }
    }

    @Override
    public void setToolTipText(String text) {
        this.lastToolTip = text;
        updateToolTip();
    }

    /**
     * Updates this {@link StateLEDLabel} icon, text and tooltip according to given state.
     * 
     * @param state The state.
     * @see #setDisplayedText(String)
     * @see #isStateInText()
     * @see #setStateInText(boolean)
     * @see #isStateInToolTip()
     * @see #setStateInToolTip(boolean)
     */
    @Override
    public void setText(String state) {
        String toConvert = state == null ? ObjectUtils.EMPTY_STRING : state.trim().toUpperCase();
        String toDisplay = toConvert;
        int iconId;
        switch (toConvert) {
            case ON:
                iconId = LED_GREEN;
                break;
            case OFF:
                iconId = LED_WHITE;
                break;
            case CLOSE:
                iconId = LED_WHITE;
                break;
            case OPEN:
                iconId = LED_GREEN;
                break;
            case INSERT:
                iconId = LED_WHITE;
                break;
            case EXTRACT:
                iconId = LED_GREEN;
                break;
            case MOVING:
                iconId = LED_BLUE;
                break;
            case STANDBY:
                iconId = LED_YELLOW;
                break;
            case FAULT:
                iconId = LED_RED;
                break;
            case INIT:
                iconId = LED_BROWNGRAY;
                break;
            case RUNNING:
                iconId = LED_DARKGREEN;
                break;
            case ALARM:
                iconId = LED_ORANGE;
                break;
            case DISABLE:
                iconId = LED_PINK;
                break;
            case UNKNOWN:
                iconId = LED_GRAY;
                break;
            default:
                iconId = -1;
                toDisplay = toConvert.isEmpty() || StringScalarBox.DEFAULT_ERROR_STRING.equals(toConvert) ? null
                        : toConvert;
                break;
        }
        lastState = toDisplay;
        setIconId(iconId);
        updateText();
        updateToolTip();
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        synchronized (bgLock) {
            if (color == null) {
                setOpaque(false);
            } else {
                setOpaque(true);
                setBackground(ColorTool.getColor(color));
            }
        }
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        setForeground(ColorTool.getColor(color));
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeFont(CometeFont font) {
        super.setFont(FontTool.getFont(font));
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public void setCometeImage(CometeImage image) {
        setIcon(ImageTool.getImage(image));
        revalidate();
    }

    @Override
    public CometeImage getCometeImage() {
        CometeImage cometeImage;
        Icon icon = getIcon();
        if (icon == null) {
            cometeImage = null;
        } else if (icon instanceof ImageIcon) {
            cometeImage = ImageTool.getCometeImage((ImageIcon) icon);
        } else {
            BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(),
                    BufferedImage.TYPE_INT_ARGB);
            icon.paintIcon(this, image.getGraphics(), 0, 0);
            cometeImage = new ImageIconCometeImage(new ImageIcon(image));
        }
        return cometeImage;
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

    @Override
    public void setIconId(int iconId) {
        ImageIcon icon = IIconComponent.getIconForId(iconId);
        if (icon == null) {
            icon = ICON_LED_KO;
        }
        setIcon(icon);
    }

}
