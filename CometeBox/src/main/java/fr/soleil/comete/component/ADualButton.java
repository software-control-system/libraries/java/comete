/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.IButtonListener;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IButton;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.comete.swing.util.ImageTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link JButton} that can be connected to 2 different {@link IKey}s, through 2 different inner {@link ITarget}s
 *
 * @param <P> The type of parameter the {@link ITarget}s can store
 * @param <TI> The type of {@link TargetInformation} the {@link ITarget}s can return
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class ADualButton<P, TI extends TargetInformation<P>> extends JButton implements ActionListener {

    private static final long serialVersionUID = 6044052347824264892L;

    private static final String HTML_START = "<html><body>";
    private static final String HTML_END = "</body></html>";
    protected static final String DIV_START = "<div>";
    protected static final String DIV_END = "</div>";
    protected static final String BOLD_START = "<b>";
    protected static final String BOLD_END = "</b>";
    protected final ErrorNotificationDelegate errorNotificationDelegate;
    protected final ButtonTarget firstTarget, secondTarget;
    protected boolean forcedTooltip;
    protected boolean autoSwitch;
    protected boolean executeOnClick;

    protected volatile boolean isSecond;

    public ADualButton() {
        super();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        firstTarget = generateFirstTarget();
        secondTarget = generateSecondTarget();
        autoSwitch = true;
        executeOnClick = true;
        addActionListener(this);
    }

    public ADualButton(String text) {
        super(text);
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        firstTarget = generateFirstTarget();
        secondTarget = generateSecondTarget();
        autoSwitch = true;
        executeOnClick = true;
        addActionListener(this);
    }

    public ADualButton(Icon icon) {
        super(icon);
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        firstTarget = generateFirstTarget();
        secondTarget = generateSecondTarget();
        autoSwitch = true;
        executeOnClick = true;
        addActionListener(this);
    }

    public ADualButton(String text, Icon icon) {
        super(text, icon);
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        firstTarget = generateFirstTarget();
        secondTarget = generateSecondTarget();
        autoSwitch = true;
        executeOnClick = true;
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (isExecuteOnClick()) {
            execute();
        }
    }

    public final void execute() {
        ButtonTarget target = getCurrentTarget();
        if (target != null) {
            target.execute();
        }
    }

    protected ButtonTarget generateFirstTarget() {
        return new ButtonTarget(false);
    }

    protected ButtonTarget generateSecondTarget() {
        return new ButtonTarget(true);
    }

    public IButton getFirstTarget() {
        return firstTarget;
    }

    public IButton getSecondTarget() {
        return secondTarget;
    }

    protected boolean isAnyTargetEnabled() {
        boolean enabled;
        if (firstTarget == null) {
            if (secondTarget == null) {
                enabled = false;
            } else {
                enabled = secondTarget.isEnabled();
            }
        } else if (secondTarget == null) {
            enabled = firstTarget.isEnabled();
        } else {
            enabled = firstTarget.isEnabled() && secondTarget.isEnabled();
        }
        return enabled;
    }

    protected final ButtonTarget getCurrentTarget() {
        return isSecond ? secondTarget : firstTarget;
    }

    public boolean isAutoSwitch() {
        return autoSwitch;
    }

    public void setAutoSwitch(boolean autoSwitch) {
        this.autoSwitch = autoSwitch;
    }

    public boolean isExecuteOnClick() {
        return executeOnClick;
    }

    public void setExecuteOnClick(boolean executeOnClick) {
        this.executeOnClick = executeOnClick;
    }

    public final boolean isSecondTargetActive() {
        return isSecond;
    }

    public final void switchTarget() {
        isSecond = !isSecond;
        ButtonTarget target = getCurrentTarget();
        if (target != null) {
            doSetForeground(target.getForeground());
            doSetBackground(target.getBackground());
            doSetFont(target.getFont());
            doSetIcon(target.getIcon());
            doSetOpaque(target.isOpaque());
            doSetText(target.getText());
            doSetToolTipText(computeTooltipText());
            doSetActionCommand(target.actionName.isEmpty() ? null : target.actionName);
        }
    }

    protected void doSetOpaque(boolean opaque) {
        super.setOpaque(opaque);
    }

    @Override
    public void setOpaque(boolean opaque) {
        doSetOpaque(opaque);
        if (firstTarget != null) {
            firstTarget.opaque = opaque;
        }
        if (secondTarget != null) {
            secondTarget.opaque = opaque;
        }
    }

    protected void doSetFont(Font font) {
        super.setFont(font);
    }

    @Override
    public void setFont(Font font) {
        doSetFont(font);
        if (firstTarget != null) {
            firstTarget.font = font;
        }
        if (secondTarget != null) {
            secondTarget.font = font;
        }
    }

    protected void doSetBackground(Color background) {
        super.setBackground(background);
    }

    @Override
    public void setBackground(Color background) {
        doSetBackground(background);
        if (firstTarget != null) {
            firstTarget.background = background;
        }
        if (secondTarget != null) {
            secondTarget.background = background;
        }
    }

    protected void doSetForeground(Color foreground) {
        super.setForeground(foreground);
    }

    @Override
    public void setForeground(Color foreground) {
        doSetForeground(foreground);
        if (firstTarget != null) {
            firstTarget.foreground = foreground;
        }
        if (secondTarget != null) {
            secondTarget.foreground = foreground;
        }
    }

    protected void doSetText(String text) {
        super.setText(text);
    }

    @Override
    public void setText(String text) {
        doSetText(text);
        if (firstTarget != null) {
            firstTarget.text = text;
        }
        if (secondTarget != null) {
            secondTarget.text = text;
        }
    }

    protected String computeTooltipText() {
        String text;
        if (forcedTooltip || (firstTarget == null) || (secondTarget == null)) {
            text = getToolTipText();
        } else {
            String tooltip1 = firstTarget.getToolTipText(), tooltip2 = secondTarget.getToolTipText();
            if ((tooltip1 == null) && (tooltip2 == null)) {
                text = null;
            } else {
                StringBuilder builder = new StringBuilder(HTML_START);
                boolean second = isSecondTargetActive();
                if (tooltip1 != null) {
                    builder.append(DIV_START);
                    if (second) {
                        builder.append(tooltip1);
                    } else {
                        builder.append(BOLD_START).append(tooltip1).append(BOLD_END);
                    }
                    builder.append(DIV_END);
                }
                if (tooltip2 != null) {
                    builder.append(DIV_START);
                    if (second) {
                        builder.append(BOLD_START).append(tooltip2).append(BOLD_END);
                    } else {
                        builder.append(tooltip2);
                    }
                    builder.append(DIV_END);
                }
                builder.append(HTML_END);
                text = builder.toString();
            }
        }
        return text;
    }

    protected void doSetToolTipText(String tooltip) {
        super.setToolTipText(tooltip);
    }

    @Override
    public void setToolTipText(String tooltip) {
        forcedTooltip = (tooltip != null);
        doSetToolTipText(tooltip);
    }

    protected void doSetIcon(Icon icon) {
        super.setIcon(icon);
    }

    @Override
    public void setIcon(Icon icon) {
        doSetIcon(icon);
        if (firstTarget != null) {
            firstTarget.icon = icon;
        }
        if (secondTarget != null) {
            secondTarget.icon = icon;
        }
    }

    protected void doSetActionCommand(String actionCommand) {
        if (!ObjectUtils.sameObject(actionCommand, getActionCommand())) {
            super.setActionCommand(actionCommand);
        }
    }

    @Override
    public void setActionCommand(String actionCommand) {
        doSetActionCommand(actionCommand);
        if ((firstTarget != null) && (secondTarget != null)) {
            if (actionCommand == null) {
                actionCommand = ObjectUtils.EMPTY_STRING;
            }
            firstTarget.actionName = ObjectUtils.EMPTY_STRING;
            secondTarget.actionName = ObjectUtils.EMPTY_STRING;
        }
    }

    public P getFirstParameter() {
        return firstTarget == null ? null : firstTarget.getParameter();
    }

    public void setFirstParameter(P parameter) {
        if (firstTarget != null) {
            firstTarget.setParameter(parameter);
        }
    }

    public P getSecondParameter() {
        return secondTarget == null ? null : secondTarget.getParameter();
    }

    public void setSecondParameter(P parameter) {
        if (secondTarget != null) {
            secondTarget.setParameter(parameter);
        }
    }

    protected abstract TI buildTargetInformation(ButtonTarget target);

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class ButtonTarget extends Component implements IButton {

        private static final long serialVersionUID = -5399850127487051713L;

        private boolean enabled;
        private String tooltip, text;
        private Font font;
        private Color foreground, background;
        private Icon icon;
        private boolean opaque;
        private P parameter;
        private String actionName;
        private final boolean second;
        private final TargetDelegate delegate;
        private final Collection<IButtonListener> buttonListeners;

        public ButtonTarget(boolean second) {
            this.second = second;
            font = ADualButton.this.getFont();
            background = ADualButton.this.getBackground();
            icon = ADualButton.this.getIcon();
            tooltip = ADualButton.this.getToolTipText();
            text = ADualButton.this.getText();
            opaque = ADualButton.this.isOpaque();
            enabled = true;
            parameter = null;
            actionName = ObjectUtils.EMPTY_STRING;
            delegate = new TargetDelegate();
            buttonListeners = Collections.newSetFromMap(new ConcurrentHashMap<IButtonListener, Boolean>());
        }

        @Override
        public Container getParent() {
            return ADualButton.this;
        }

        public void setParameter(P parameter) {
            this.parameter = parameter;
        }

        public P getParameter() {
            return parameter;
        }

        protected boolean isActive() {
            return second == isSecond;
        }

        @Override
        public boolean isSelected() {
            return isActive() ? ADualButton.this.isSelected() : false;
        }

        @Override
        public void setSelected(boolean bool) {
            if (isActive()) {
                ADualButton.this.setSelected(bool);
            }
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            delegate.addMediator(mediator);
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            delegate.removeMediator(mediator);
        }

        /**
         * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
         * 
         * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
         * @param <V> The type of {@link TargetInformation} to transmit
         * @param targetInformation The {@link TargetInformation} that should be transmitted to
         *            {@link Mediator}s
         */
        public <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
            delegate.warnMediators(targetInformation);
        }

        @Override
        public boolean hasFocus() {
            return false;
        }

        @Override
        public void setOpaque(boolean opaque) {
            this.opaque = opaque;
            if (isActive()) {
                ADualButton.this.doSetOpaque(opaque);
            }
        }

        @Override
        public boolean isOpaque() {
            return opaque;
        }

        @Override
        public void setCometeBackground(CometeColor color) {
            this.background = ColorTool.getColor(color);
            if (isActive()) {
                ADualButton.this.doSetBackground(background);
            }
        }

        @Override
        public CometeColor getCometeBackground() {
            return ColorTool.getCometeColor(background);
        }

        @Override
        public Color getBackground() {
            return background;
        }

        @Override
        public void setCometeForeground(CometeColor color) {
            this.foreground = ColorTool.getColor(color);
            if (isActive()) {
                ADualButton.this.doSetForeground(foreground);
            }
        }

        @Override
        public CometeColor getCometeForeground() {
            return ColorTool.getCometeColor(foreground);
        }

        @Override
        public Color getForeground() {
            return foreground;
        }

        @Override
        public void setEnabled(boolean enabled) {
            boolean oldEnabled = ADualButton.this.isAnyTargetEnabled();
            if (this.enabled != enabled) {
                this.enabled = enabled;
                boolean newEnabled = ADualButton.this.isAnyTargetEnabled();
                if (newEnabled != oldEnabled) {
                    ADualButton.this.setEnabled(newEnabled);
                }
            }
        }

        @Override
        public boolean isEnabled() {
            return enabled;
        }

        @Override
        public void setCometeFont(CometeFont font) {
            this.font = FontTool.getFont(font);
            if (isActive()) {
                ADualButton.this.doSetFont(getFont());
            }
        }

        @Override
        public CometeFont getCometeFont() {
            return FontTool.getCometeFont(font);
        }

        @Override
        public Font getFont() {
            return font;
        }

        @Override
        public void setToolTipText(String text) {
            this.tooltip = text;
            ADualButton.this.doSetToolTipText(computeTooltipText());
        }

        @Override
        public String getToolTipText() {
            return tooltip;
        }

        @Override
        public void setVisible(boolean visible) {
            // not managed
        }

        @Override
        public boolean isVisible() {
            return false;
        }

        @Override
        public void setHorizontalAlignment(int halign) {
            // not managed
        }

        @Override
        public int getHorizontalAlignment() {
            // not managed
            return 0;
        }

        @Override
        public void setSize(int width, int height) {
            // not managed
        }

        @Override
        public void setPreferredSize(int width, int height) {
            // not managed
        }

        @Override
        public int getWidth() {
            // not managed
            return 0;
        }

        @Override
        public int getHeight() {
            // not managed
            return 0;
        }

        @Override
        public void setLocation(int x, int y) {
            // not managed
        }

        @Override
        public int getX() {
            // not managed
            return 0;
        }

        @Override
        public int getY() {
            // not managed
            return 0;
        }

        @Override
        public boolean isEditingData() {
            // not managed
            return false;
        }

        @Override
        public void setTitledBorder(String title) {
            // not managed
        }

        @Override
        public void addMouseListener(IMouseListener listener) {
            // not managed
        }

        @Override
        public void removeMouseListener(IMouseListener listener) {
            // not managed
        }

        @Override
        public void removeAllMouseListeners() {
            // not managed
        }

        @Override
        public String getText() {
            return text;
        }

        @Override
        public void setText(String text) {
            this.text = text;
            if (isActive()) {
                ADualButton.this.doSetText(text);
            }
        }

        @Override
        public void setEditable(boolean b) {
            // Nothing to do: a button is not editable
        }

        @Override
        public boolean isEditable() {
            return false;
        }

        @Override
        public void setCometeImage(CometeImage image) {
            this.icon = ImageTool.getImage(image);
            if (isActive()) {
                ADualButton.this.doSetIcon(icon);
            }
        }

        @Override
        public CometeImage getCometeImage() {
            return ImageTool.getCometeImage(icon instanceof ImageIcon ? (ImageIcon) icon : null);
        }

        public Icon getIcon() {
            return icon;
        }

        @Override
        public void setBorderPainted(boolean painted) {
            // not managed
        }

        @Override
        public boolean isBorderPainted() {
            // not managed
            return false;
        }

        @Override
        public void setButtonLook(boolean look) {
            // not managed
        }

        @Override
        public boolean isButtonLook() {
            // not managed
            return true;
        }

        @Override
        public void addButtonListener(IButtonListener listener) {
            if (listener != null) {
                buttonListeners.add(listener);
            }
        }

        @Override
        public void removeButtonListener(IButtonListener listener) {
            if (listener != null) {
                buttonListeners.remove(listener);
            }
        }

        @Override
        public void fireActionPerformed(EventObject event) {
            warnMediators(buildTargetInformation(this));
            for (IButtonListener listener : buttonListeners) {
                if (listener != null) {
                    listener.actionPerformed(event);
                }
            }
            if (isAutoSwitch()) {
                switchTarget();
            }
        }

        @Override
        public void execute() {
            ActionEvent event = new ActionEvent(this, this.hashCode(), actionName, System.currentTimeMillis(), 0);
            fireActionPerformed(event);
        }

        @Override
        public String getActionName() {
            return actionName;
        }

        @Override
        public void setActionName(String actionName) {
            this.actionName = actionName == null ? ObjectUtils.EMPTY_STRING : actionName;
            if (isActive()) {
                doSetActionCommand(actionName);
            }
        }

        @Override
        public void notifyForError(String message, Throwable error) {
            errorNotificationDelegate.notifyForError(message, error);
        }

        @Override
        public IDrawingThreadManager getDrawingThreadManager() {
            return EDTManager.INSTANCE;
        }

        @Override
        public boolean shouldReceiveDataInDrawingThread() {
            return true;
        }

    }
}
