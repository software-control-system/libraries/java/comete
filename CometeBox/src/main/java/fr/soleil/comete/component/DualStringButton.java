/*
 * This file is part of CometeBox.
 * 
 * CometeBox is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeBox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeBox. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.component;

import javax.swing.Icon;

import fr.soleil.comete.definition.data.information.TextInformation;

/**
 * An {@link ADualButton} that manages {@link TextInformation}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DualStringButton extends ADualButton<String, TextInformation> {

    private static final long serialVersionUID = 9062611532821236960L;

    public DualStringButton() {
        super();
    }

    public DualStringButton(String text) {
        super(text);
    }

    public DualStringButton(Icon icon) {
        super(icon);
    }

    public DualStringButton(String text, Icon icon) {
        super(text, icon);
    }

    @Override
    protected TextInformation buildTargetInformation(ADualButton<String, TextInformation>.ButtonTarget target) {
        return new TextInformation(target, target == null ? null : target.getParameter());
    }

}
