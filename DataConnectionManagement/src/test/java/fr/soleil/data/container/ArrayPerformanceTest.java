/*******************************************************************************
 * Copyright (c) 2008-2023 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.data.container;

import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.util.Locale;

import fr.soleil.data.container.matrix.BooleanMatrix;
import fr.soleil.lib.project.math.ArraySet;

public class ArrayPerformanceTest {

    /**
     * Default matrix width (4000)
     */
    private final static int DEFAULT_WIDTH = 4000;
    /**
     * Default matrix height (4000)
     */
    private final static int DEFAULT_HEIGHT = 4000;

    /**
     * Tests java performances with boolean matrix (i.e. <code>boolean[][]</code>), using different
     * methods:
     * <ul>
     * <li>Copy a matrix to another using Array.set and Array.get (which uses autoboxing), traced as
     * "Array Object copy"</li>
     * <li>Copy a matrix to another using classic for (case where you know exactly the matrix
     * content type), traced as "Direct copy"</li>
     * <li>Copy a matrix to another using Array.setBoolean and Array.getBoolean (no autoboxing),
     * traced as "Array boolean copy"</li>
     * <li>Copy a matrix to another using System.arraycopy, traced as "System array copy"
     * </ul>
     * 
     * @param args Program arguments. If length == 2, the programs tests performances with boolean
     *            matrix of width = <code>args[0]</code> and <code>height = args[1]</code>. By
     *            default, this programs uses {@link #DEFAULT_WIDTH}&times;{@link #DEFAULT_HEIGHT}
     *            matrix
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        int width = -1, height = -1;
        if ((args != null)) {
            if (args.length > 0) {
                try {
                    width = Integer.parseInt(args[0]);
                }
                catch (Exception e) {
                    width = -1;
                }
                if (args.length > 1) {
                    try {
                        height = Integer.parseInt(args[1]);
                    }
                    catch (Exception e) {
                        height = -1;
                    }
                }
                else {
                    height = width;
                }
            }
        }
        if (width < 0) {
            width = DEFAULT_WIDTH;
        }
        if (height < 0) {
            height = DEFAULT_HEIGHT;
        }
        System.out.println("Initializing arrays (" + width + "\u00d7" + height + ")...");
        long timeInit = System.currentTimeMillis();

        boolean[][] destination = (boolean[][]) Array.newInstance(Boolean.TYPE, height, width);
        Object destinationToObject = destination.clone();
        Object[] destinationToObjectArray = destination.clone();

        boolean[][] source = (boolean[][]) Array.newInstance(Boolean.TYPE, height, width);
        for (int y = 0; y < source.length; y++) {
            for (int x = 0; x < source[y].length; x++) {
                source[y][x] = (Math.random() < 0.5);
            }
        }
        Object sourceToObject = source.clone();
        Object[] sourceToObjectArray = source.clone();

        timeInit = System.currentTimeMillis() - timeInit;

        System.out.println("Arrays initialized");
        System.out.println("Starting copy...");

        long timeObject = System.currentTimeMillis();
        for (int y = 0; y < source.length; y++) {
            for (int x = 0; x < source[y].length; x++) {
                Array.set(Array.get(destinationToObject, y), x, Array.get(Array.get(sourceToObject,
                        y), x));
            }
        }
        timeObject = System.currentTimeMillis() - timeObject;

        long timeDirect = System.currentTimeMillis();
        for (int y = 0; y < source.length; y++) {
            for (int x = 0; x < source[y].length; x++) {
                destination[y][x] = source[y][x];
            }
        }

        timeDirect = System.currentTimeMillis() - timeDirect;

        long timeBoolean = System.currentTimeMillis();
        for (int y = 0; y < source.length; y++) {
            for (int x = 0; x < source[y].length; x++) {
                Array.setBoolean(Array.get(destinationToObject, y), x, Array.getBoolean(Array.get(
                        sourceToObject, y), x));
            }
        }
        timeBoolean = System.currentTimeMillis() - timeBoolean;

        long timeSystemArray = System.currentTimeMillis();
        for (int y = 0; y < source.length; y++) {
            System.arraycopy(sourceToObjectArray[y], 0, destinationToObjectArray[y], 0, width);
        }
        timeSystemArray = System.currentTimeMillis() - timeSystemArray;

        // Format milliseconds, using ' ' as group separator
        NumberFormat format = NumberFormat.getInstance(Locale.FRENCH);
        format.setGroupingUsed(true);

        System.out.println("Copy done. Ellapsed times:");
        System.out.println(" - Initializations: " + format.format(timeInit) + " ms");
        System.out.println(" - Array Object copy: " + format.format(timeObject) + " ms");
        System.out.println(" - Direct copy: " + format.format(timeDirect) + " ms");
        System.out.println(" - Array boolean copy: " + format.format(timeBoolean) + " ms");
        System.out.println(" - System array copy: " + format.format(timeSystemArray) + " ms");
        System.out
                .println("Now, let's compare AbstractMatrix vs ArraySet (flat value recovering): ");
        ArraySet<Boolean> booleanSet = new ArraySet<Boolean>(source, Boolean.TYPE);
        BooleanMatrix matrix = new BooleanMatrix(Boolean.TYPE);
        matrix.setValue(source);
        long timeSet = System.currentTimeMillis();
        booleanSet.getFlatValues();
        timeSet = System.currentTimeMillis() - timeSet;
        long timeMatrix = System.currentTimeMillis();
        matrix.getFlatValue();
        timeMatrix = System.currentTimeMillis() - timeMatrix;
        System.out.println(" - ArraySet to Flat: " + timeSet + " ms");
        System.out.println(" - AbstractMatrix to Flat: " + timeMatrix + " ms");
    }
}
