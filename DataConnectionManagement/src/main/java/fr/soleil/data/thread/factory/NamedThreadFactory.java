/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.thread.factory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A named thread factory. Based on {@link Executors}' DefaultThreadFactory
 */
public class NamedThreadFactory implements ThreadFactory {

    protected static final ConcurrentHashMap<String, AtomicInteger> POOL_NUMBER_MAP = new ConcurrentHashMap<String, AtomicInteger>();
    protected final ThreadGroup group;
    protected final AtomicInteger threadNumber = new AtomicInteger(1);
    protected final String namePrefix;

    public NamedThreadFactory(String name) {
        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
        AtomicInteger poolNumber = POOL_NUMBER_MAP.get(name);
        if (poolNumber == null) {
            poolNumber = POOL_NUMBER_MAP.putIfAbsent(name, new AtomicInteger(1));
            if (poolNumber == null) {
                poolNumber = POOL_NUMBER_MAP.get(name);
            }
        }
        namePrefix = name + "-" + poolNumber.getAndIncrement() + "-thread-";
    }

    protected Thread generateThread(Runnable r) {
        return new Thread(group, r, namePrefix + threadNumber.getAndIncrement(), 0);
    }

    protected void configureThread(Thread t) {
        if (t.isDaemon())
            t.setDaemon(false);
        if (t.getPriority() != Thread.NORM_PRIORITY)
            t.setPriority(Thread.NORM_PRIORITY);
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = generateThread(r);
        configureThread(t);
        return t;
    }
}