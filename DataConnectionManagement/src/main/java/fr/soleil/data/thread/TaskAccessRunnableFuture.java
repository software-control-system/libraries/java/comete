/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * A {@link RunnableFuture} that manages a timeout, delegates the work to another {@link RunnableFuture}, and remembers
 * its {@link Callable}, giving access to it.
 *
 * @param <T> The result type returned by this Future's <tt>get</tt> method.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TaskAccessRunnableFuture<T> implements RunnableFuture<T> {

    private final Callable<T> callable;
    private final RunnableFuture<T> delegate;
    /*
     * Time at which the task was submitted to the executor.
     */
    private final long futureCreationTime;
    // Time at which the task is about to be executed.
    private long taskExecutionStartTime;
    // Timeout value
    private final long timeout;
    // Timeout unit
    private final TimeUnit unit;

    /**
     * Constructs a new {@link TaskAccessRunnableFuture}.
     * 
     * @param delegate The delegate, that really manages the associated task.
     * @param callable The associated task.
     */
    public TaskAccessRunnableFuture(final RunnableFuture<T> delegate, final Callable<T> callable) {
        this(delegate, callable, -1, null);
    }

    /**
     * Constructs a new {@link TaskAccessRunnableFuture}, setting its associated task timeout.
     * 
     * @param delegate The delegate, that really manages the associated task.
     * @param callable The associated task.
     * @param timeout The task timeout value. If <code>&le; 0</code>, task time out is not managed.
     * @param unit The task timeout unit. If <code>null</code>, task time out is not managed.
     */
    public TaskAccessRunnableFuture(final RunnableFuture<T> delegate, final Callable<T> callable, final long timeout,
            final TimeUnit unit) {
        this.callable = callable;
        this.delegate = delegate;
        this.timeout = timeout;
        this.unit = unit;
        this.futureCreationTime = System.currentTimeMillis();
    }

    @Override
    public void run() {
        this.taskExecutionStartTime = System.currentTimeMillis();
        delegate.run();
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return delegate.cancel(mayInterruptIfRunning);
    }

    @Override
    public boolean isCancelled() {
        return delegate.isCancelled();
    }

    @Override
    public boolean isDone() {
        return delegate.isDone();
    }

    @Override
    public T get() throws InterruptedException, ExecutionException {
        T result;
        if ((timeout > 0) && (unit != null)) {
            try {
                result = delegate.get(timeout, unit);
            } catch (TimeoutException e) {
                throw new ExecutionException("Task took too much time", e);
            }
        } else {
            result = delegate.get();
        }
        return result;
    }

    @Override
    public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return delegate.get(timeout, unit);
    }

    /**
     * Returns the date (in milliseconds) at which this {@link TaskAccessRunnableFuture} was created
     * 
     * @return A <code>long</code>.
     */
    public long getFutureCreationTime() {
        return futureCreationTime;
    }

    /**
     * Returns the date (in milliseconds) at which the associated task was about to be executed
     * 
     * @return A <code>long</code>.
     */
    public long getTaskExecutionStartTime() {
        return taskExecutionStartTime;
    }

    /**
     * Returns the associated {@link Callable}.
     * 
     * @return A {@link Callable}.
     */
    public Callable<T> getCallableTask() {
        return callable;
    }

}
