/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.thread;

public interface IThreadConfiguration extends Cloneable {

    /**
     * Default value by which to multiply core pool size in order to calculate maximum pool size.
     */
    public static final double DEFAULT_POOL_SIZE_MULTIPLIER = 1.5d;

    /**
     * Default value to use in {@link #setCorePoolSize(double)}
     */
    public static final int DEFAULT_CORE_POOL_SIZE = Runtime.getRuntime().availableProcessors();

    /**
     * Default value to use in {@link #setUseSystemCores(boolean)}
     */
    public static final boolean DEFAULT_USE_SYSTEM_CORES = true;

    /**
     * Default value to use in {@link #setMaxPoolSize(double)}
     */
    public static final int DEFAULT_MAX_POOL_SIZE = (int) Math
            .round(DEFAULT_CORE_POOL_SIZE * DEFAULT_POOL_SIZE_MULTIPLIER);

    /**
     * Default value to use in {@link #setTaskTimeout(long)}
     */
    public static final long DEFAULT_TASK_TIMEOUT = 30;

    /**
     * Default value to use in {@link #setKeepAliveTime(long)}
     */
    public static final long DEFAULT_KEEP_ALIVE_TIME = 100;

    /**
     * Default value to use in {@link #setMultiThreadAllowed(boolean)}
     */
    public static final boolean DEFAULT_MULTI_THREAD_ALLOWED = true;

    /**
     * Returns the number of threads to keep in the pool, even if they are idle.
     * 
     * @return An <code>int</code>
     */
    public int getCorePoolSize();

    /**
     * Sets the number of threads to keep in the pool, even if they are idle.
     * 
     * @param nbCorePoolSize The number of threads to keep in the pool, even if they are idle.
     */
    public void setCorePoolSize(int nbCorePoolSize);

    /**
     * Returns whether to use as many threads as there are available cores, instead of using {@link #getCorePoolSize()}
     * 
     * @return A <code>boolean</code>
     */
    public boolean isUseSystemCores();

    /**
     * Sets whether to use as many threads as there are available cores, instead of using {@link #getCorePoolSize()}
     * 
     * @param useSystemCores whether to use as many threads as there are available cores, instead of using
     *            {@link #getCorePoolSize()}
     */
    public void setUseSystemCores(boolean useSystemCores);

    /**
     * Returns the maximum number of threads to allow in the pool.
     * 
     * @return An <code>int</code>
     */
    public int getMaximumPoolSize();

    /**
     * Sets the maximum number of threads to allow in the pool.
     * 
     * @param maxPoolSize The maximum number of threads to allow in the pool.
     */
    public void setMaximumPoolSize(int maxPoolSize);

    /**
     * Returns the maximum time that excess idle threads will wait (in seconds) for new tasks before terminating, when
     * the number of threads is greater than the core.
     * 
     * @return A <code>long</code>
     */
    public long getKeepAliveTime();

    /**
     * Sets the maximum time that excess idle threads will wait (in seconds) for new tasks before terminating, when
     * the number of threads is greater than the core.
     * 
     * @param keepAliveTime The time to wait (in seconds)
     */
    public void setKeepAliveTime(long keepAliveTime);

    /**
     * Returns the time to wait (in seconds) before considering a task is blocked and should be killed.
     * 
     * @return A <code>long</code>
     */
    public long getTaskTimeout();

    /**
     * Sets the time to wait (in seconds) before considering a task is blocked and should be killed.
     * 
     * @param timeout The task timeout (in seconds)
     */
    public void setTaskTimeout(long timeout);

    public boolean isMultiThreadAllowed();

    public void setMultiThreadAllowed(boolean allowed);

    /**
     * Returns a clone of this {@link IThreadConfiguration}.
     * 
     * @return An {@link IThreadConfiguration}.
     */
    public IThreadConfiguration clone();
}
