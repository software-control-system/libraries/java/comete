/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.thread;

import static fr.soleil.data.metric.TaskMetric.REJECTED_TASK_COUNT;
import static fr.soleil.data.metric.TaskMetric.TASK_COUNT;
import static fr.soleil.data.metric.TaskMetric.TASK_END_TO_END_TIME;
import static fr.soleil.data.metric.TaskMetric.TASK_EXECUTION_TIME;
import static fr.soleil.data.metric.TaskMetric.TASK_QUEUE_WAIT_TIME;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import fr.soleil.data.event.TaskEvent;
import fr.soleil.data.iterator.CalculationIterator;
import fr.soleil.data.listener.TaskFinishListener;
import fr.soleil.data.metric.TaskMetric;
import fr.soleil.data.service.thread.AccessibleFutureTask;
import fr.soleil.lib.project.ICancelable;

/**
 * A {@link ThreadPoolExecutor} that is able to self submit children tasks, when a {@link CalculationIterator} task is
 * finished.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AutoSubmitThreadPoolExecutor extends ThreadPoolExecutor {

    private final long taskTimeout;
    private final TimeUnit timeUnit;
    private final ICancelable cancelable;
    private long startFisrtTime = 0l;
    private volatile boolean interrupted;
    private final Collection<TaskFinishListener> listeners;
    private final AtomicInteger taskCount;
    // boolean used to avoid finishing while still submitting tasks
    private volatile boolean duringTaskSubmission;

    /**
     * Constructs a new {@link AutoSubmitThreadPoolExecutor}
     * 
     * @param corePoolSize The number of threads to keep in the pool, even if they are idle, unless
     *            {@code allowCoreThreadTimeOut} is set.
     * @param maximumPoolSize The maximum number of threads to allow in the pool.
     * @param keepAliveTime When the number of threads is greater than the core, this is the maximum time that excess
     *            idle threads will wait for new tasks before terminating.
     * @param taskTimeout The maximum time to wait for a task to finish, before considering it timed out.
     * @param unit The time unit for the {@code keepAliveTime} argument.
     * @param workQueue The queue to use for holding tasks before they are executed. This queue will hold only the
     *            {@code Runnable} tasks submitted by the {@code execute} method.
     * @param handler The handler to use when execution is blocked because the thread bounds and queue capacities are
     *            reached.
     * @param cancelable The {@link ICancelable} that knows when computation is canceled.
     */
    public AutoSubmitThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, long taskTimeout,
            TimeUnit unit, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler,
            final ICancelable cancelable) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
        this.taskTimeout = taskTimeout;
        this.timeUnit = unit;
        this.cancelable = cancelable;
        this.listeners = Collections.newSetFromMap(new ConcurrentHashMap<TaskFinishListener, Boolean>());
        this.taskCount = new AtomicInteger();
        this.duringTaskSubmission = false;
    }

    /**
     * Constructs a new {@link AutoSubmitThreadPoolExecutor}
     * 
     * @param corePoolSize The number of threads to keep in the pool, even if they are idle, unless
     *            {@code allowCoreThreadTimeOut} is set.
     * @param maximumPoolSize The maximum number of threads to allow in the pool.
     * @param keepAliveTime When the number of threads is greater than the core, this is the maximum time that excess
     *            idle threads will wait for new tasks before terminating.
     * @param taskTimeout The maximum time to wait for a task to finish, before considering it timed out.
     * @param unit The time unit for the {@code keepAliveTime} argument.
     * @param workQueue The queue to use for holding tasks before they are executed. This queue will hold only the
     *            {@code Runnable} tasks submitted by the {@code execute} method.
     * @param threadFactory The factory to use when the executor creates a new thread
     * @param handler The handler to use when execution is blocked because the thread bounds and queue capacities are
     *            reached.
     * @param cancelable The {@link ICancelable} that knows when computation is canceled.
     */
    public AutoSubmitThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, long taskTimeout,
            TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory,
            RejectedExecutionHandler handler, final ICancelable cancelable) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
        this.taskTimeout = taskTimeout;
        this.timeUnit = unit;
        this.cancelable = cancelable;
        this.listeners = Collections.newSetFromMap(new ConcurrentHashMap<TaskFinishListener, Boolean>());
        this.taskCount = new AtomicInteger();
        this.duringTaskSubmission = false;
    }

    /**
     * Tests whether associated {@link ICancelable} is canceled.
     * 
     * @return A <code>boolean</code>.
     */
    protected boolean isCanceled() {
        ICancelable cancelable = this.cancelable;
        return ((cancelable != null) && cancelable.isCanceled());
    }

    /**
     * Returns whether this {@link AutoSubmitThreadPoolExecutor} was interrupted.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isInterrupted() {
        return interrupted;
    }

    /**
     * Returns whether some new tasks may arrived.
     * <p>
     * This is dependant of {@link #setDuringTaskSubmission(boolean)}
     * </p>
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isDuringTaskSubmission() {
        return duringTaskSubmission;
    }

    /**
     * Notifies this {@link AutoSubmitThreadPoolExecutor} that some new tasks may arrive
     * 
     * @param duringTaskSubmission Whether some new tasks may arrive.
     */
    public void setDuringTaskSubmission(boolean duringTaskSubmission) {
        this.duringTaskSubmission = duringTaskSubmission;
    }

    /**
     * Adds a {@link TaskFinishListener} to this {@link AutoSubmitThreadPoolExecutor}.
     * 
     * @param listener The {@link TaskFinishListener} to add.
     */
    public void addTaskFinishListener(TaskFinishListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
    }

    /**
     * Removes a {@link TaskFinishListener} from this {@link AutoSubmitThreadPoolExecutor}.
     * 
     * @param listener The {@link TaskFinishListener} to remove.
     */
    public void removeTaskFinishListener(TaskFinishListener listener) {
        if (listener != null) {
            listeners.remove(listener);
        }
    }

    protected void notifyListenersForFinishedTasks() {
        TaskEvent event = new TaskEvent(this);
        for (TaskFinishListener listener : listeners) {
            if (listener != null) {
                listener.allTasksFinished(event);
            }
        }
    }

    @Override
    protected void afterExecute(Runnable task, Throwable t) {
        try {
            if (t == null) {
                interrupted = interrupted || isCanceled();
            } else {
                interrupted = true;
            }
            TaskAccessRunnableFuture<?> future = getTaskAccessRunnableFuture(task);
            if ((!interrupted) && (future != null) && (!future.isCancelled())) {
                TASK_EXECUTION_TIME.update(System.currentTimeMillis() - future.getTaskExecutionStartTime());
                Callable<?> callable = future.getCallableTask();
                if (callable instanceof CalculationIterator<?, ?, ?>) {
                    CalculationIterator<?, ?, ?> taskIterator = (CalculationIterator<?, ?, ?>) callable;
                    while (taskIterator.hasNext()) {
                        if (isCanceled()) {
                            interrupted = true;
                            break;
                        }
                        this.submit(taskIterator.next());
                    }
                    if (isCanceled()) {
                        interrupted = true;
                    }
                } // end if (callable instanceof CalculationIterator<?, ?, ?>)
            } // end if ((!interrupted) && (future != null) && (!future.isCancelled()))
            if ((!interrupted) && (taskCount.decrementAndGet() == 0) && (!duringTaskSubmission)) {
                // shutdown when there is no more task left
                shutdown();
            }
        } catch (Throwable e) {
            interrupted = true;
        } finally {
            maybeShutDown();
        }
    }

    /**
     * Shuts down this {@link AutoSubmitThreadPoolExecutor} if it was interrupted.
     */
    protected void maybeShutDown() {
//        contextManager.traceMetrics(getAllResultsTasksMetrics(), imagesPerBlock > 1);
        if (interrupted) {
            shutdownNow();
        }
    }

    @Override
    protected void terminated() {
        TASK_END_TO_END_TIME.update(System.currentTimeMillis() - startFisrtTime);
        notifyListenersForFinishedTasks();
        if (interrupted) {
            shutdownNow();
        }
    }

    @Override
    public <T> RunnableFuture<T> newTaskFor(Runnable runnable, T value) {
        RunnableFuture<T> result;
        if (runnable instanceof TaskAccessRunnableFuture<?>) {
            @SuppressWarnings("unchecked")
            TaskAccessRunnableFuture<T> future = (TaskAccessRunnableFuture<T>) runnable;
            result = future;
        } else {
            result = super.newTaskFor(runnable, value);
        }
        return result;
    }

    @Override
    public <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        RunnableFuture<T> runnableFuture = null;
        try {
            // save the time for the first task submit
            if (startFisrtTime == 0l) {
                startFisrtTime = System.currentTimeMillis();
            }
            runnableFuture = new TaskAccessRunnableFuture<T>(super.newTaskFor(callable), callable, taskTimeout,
                    timeUnit);
            taskCount.incrementAndGet();
        } catch (Exception e) {
            interrupted = true;
        }
        return runnableFuture;
    }

    private TaskAccessRunnableFuture<?> getTaskAccessRunnableFuture(Runnable task) {
        TaskAccessRunnableFuture<?> instrumentedFutur = null;
        if (task instanceof TaskAccessRunnableFuture) {
            instrumentedFutur = (TaskAccessRunnableFuture<?>) task;
        } else if (task instanceof AccessibleFutureTask<?>) {
            Future<?> future = ((AccessibleFutureTask<?>) task).getTask();
            if (future instanceof TaskAccessRunnableFuture<?>) {
                instrumentedFutur = (TaskAccessRunnableFuture<?>) future;
            }
        }
        return instrumentedFutur;
    }

    public List<TaskMetric> getAllResultsTasksMetrics() {
        List<TaskMetric> listTasks = new ArrayList<TaskMetric>();
        listTasks.add(REJECTED_TASK_COUNT);
        listTasks.add(TASK_QUEUE_WAIT_TIME);
        listTasks.add(TASK_EXECUTION_TIME);
        listTasks.add(TASK_COUNT);
        listTasks.add(TASK_END_TO_END_TIME);
        return listTasks;
    }
}
