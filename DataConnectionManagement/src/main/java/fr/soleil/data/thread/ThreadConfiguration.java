/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.thread;

import fr.soleil.lib.project.ObjectUtils;

public class ThreadConfiguration implements IThreadConfiguration {
    private int corePoolSize, maximumPoolSize;
    private long keepAliveTime, taskTimeout;
    private boolean multiThreadAllowed;
    private boolean useSystemCores;

    public ThreadConfiguration() {
        this(DEFAULT_CORE_POOL_SIZE, DEFAULT_USE_SYSTEM_CORES, DEFAULT_MAX_POOL_SIZE, DEFAULT_KEEP_ALIVE_TIME,
                DEFAULT_TASK_TIMEOUT, DEFAULT_MULTI_THREAD_ALLOWED);
    }

    public ThreadConfiguration(int corePoolSize, boolean useSystemCores, int maximumPoolSize, long keepAliveTime,
            long taskTimeout, boolean multiThreadAllowed) {
        this.corePoolSize = corePoolSize;
        this.useSystemCores = useSystemCores;
        this.maximumPoolSize = maximumPoolSize;
        this.keepAliveTime = keepAliveTime;
        this.taskTimeout = taskTimeout;
        this.multiThreadAllowed = multiThreadAllowed;
    }

    @Override
    public int getCorePoolSize() {
        return corePoolSize;
    }

    @Override
    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize < 1 ? DEFAULT_CORE_POOL_SIZE : corePoolSize;
    }

    @Override
    public boolean isUseSystemCores() {
        return useSystemCores;
    }

    @Override
    public void setUseSystemCores(boolean useSystemCores) {
        this.useSystemCores = useSystemCores;
    }

    @Override
    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    @Override
    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize < 1 ? DEFAULT_MAX_POOL_SIZE : maximumPoolSize;
    }

    @Override
    public long getKeepAliveTime() {
        return keepAliveTime;
    }

    @Override
    public void setKeepAliveTime(long keepAliveTime) {
        this.keepAliveTime = keepAliveTime < 1 ? DEFAULT_KEEP_ALIVE_TIME : keepAliveTime;
    }

    @Override
    public long getTaskTimeout() {
        return taskTimeout;
    }

    @Override
    public void setTaskTimeout(long taskTimeout) {
        this.taskTimeout = taskTimeout < 1 ? DEFAULT_TASK_TIMEOUT : taskTimeout;
    }

    @Override
    public boolean isMultiThreadAllowed() {
        return multiThreadAllowed;
    }

    @Override
    public void setMultiThreadAllowed(boolean multiThreadAllowed) {
        this.multiThreadAllowed = multiThreadAllowed;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj.getClass().equals(getClass())) {
            ThreadConfiguration manager = (ThreadConfiguration) obj;
            equals = (corePoolSize == manager.corePoolSize) && (maximumPoolSize == manager.maximumPoolSize)
                    && (keepAliveTime == manager.keepAliveTime) && (taskTimeout == manager.taskTimeout)
                    && (multiThreadAllowed == manager.multiThreadAllowed);
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0x137EAD;
        int mult = 0x367;
        code = code * mult + getClass().hashCode();
        code = code * mult + corePoolSize;
        code = code * mult + maximumPoolSize;
        code = code * mult + (int) keepAliveTime;
        code = code * mult + (int) taskTimeout;
        code = code * mult + ObjectUtils.generateHashCode(multiThreadAllowed);
        return code;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getClass().getName());
        builder.append("{corePoolSize = ").append(corePoolSize);
        builder.append(", maximumPoolSize = ").append(maximumPoolSize);
        builder.append(", taskTimeout = ").append(taskTimeout).append('s');
        builder.append(", keepAliveTime = ").append(keepAliveTime).append('s');
        builder.append(", multiThreadAllowed = ").append(multiThreadAllowed).append('}');
        return builder.toString();
    }

    @Override
    public ThreadConfiguration clone() {
        ThreadConfiguration clone;
        try {
            clone = (ThreadConfiguration) super.clone();
        } catch (CloneNotSupportedException e) {
            // Should not happen as IThreadConfiguration extends Cloneable
            clone = new ThreadConfiguration(corePoolSize, useSystemCores, maximumPoolSize, keepAliveTime, taskTimeout,
                    multiThreadAllowed);
        }
        return clone;
    }
}
