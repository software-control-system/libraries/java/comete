/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.listener;

import java.util.EventListener;

import fr.soleil.data.event.GroupEvent;

/**
 * An {@link EventListener} that listens to the refreshment of a group
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IRefreshingGroupListener extends EventListener {

    /**
     * Notifies this {@link IRefreshingGroupListener} that the listened group was refreshed
     * 
     * @param event The {@link GroupEvent} that describes the refreshed group
     */
    public void groupRefreshed(GroupEvent event);

}
