/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.listener;

import java.util.EventListener;

import fr.soleil.data.event.TaskEvent;
import fr.soleil.data.thread.AutoSubmitThreadPoolExecutor;

/**
 * An {@link EventListener} that listens for an {@link AutoSubmitThreadPoolExecutor}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface TaskFinishListener extends EventListener {

    /**
     * Notifies this {@link TaskFinishListener} that all tasks of an {@link AutoSubmitThreadPoolExecutor} finished
     * 
     * @param event The {@link TaskEvent} that knows the {@link AutoSubmitThreadPoolExecutor}
     */
    public void allTasksFinished(TaskEvent event);

}
