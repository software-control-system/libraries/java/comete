/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.manager;

/**
 * An interface for something that must or can treat some data in a single thread
 *
 * @param <E> The type of {@link Exception} returned by this {@link ISingleThreadManager} in case of treatment error.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ISingleThreadManager<E extends Exception> {

    /**
     * Treats everything in a single thread, instead of using separated tasks for multi threads.
     * In case multi threading is not allowed, this method will be called.
     * 
     * @throws E If a problem occurred
     */
    public void treatDataInASingleThread() throws E;

}
