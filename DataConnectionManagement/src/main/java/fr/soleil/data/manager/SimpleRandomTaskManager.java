/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.manager;

import java.util.Collection;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

import org.slf4j.Logger;

import fr.soleil.data.thread.AutoSubmitThreadPoolExecutor;
import fr.soleil.data.thread.factory.NamedThreadFactory;
import fr.soleil.data.util.DataConnectionUtils;

/**
 * An {@link ATaskManager} that may run different tasks, in form of {@link Runnable}s, in a thread pool.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class SimpleRandomTaskManager extends ATaskManager {

    protected static final String TIMER_KEY = "fr.soleil.data.tasks.check.scheduled";

    protected final AutoSubmitThreadPoolExecutor executor;
    protected final CompletionService<Void> completionService;
    protected final Collection<Future<Void>> futures;
    protected final Timer timer;

    public SimpleRandomTaskManager(Logger logger, String threadName) {
        this(logger, threadName, null);
    }

    public SimpleRandomTaskManager(Logger logger, String threadName, Class<? extends NamedThreadFactory> factoryClass) {
        super(logger, factoryClass);
        executor = generateThreadPoolExecutor(threadName, null);
        completionService = generateCompletionService(executor);
        futures = Collections.newSetFromMap(new ConcurrentHashMap<>());
        timer = new Timer(
                String.format(DataConnectionUtils.MESSAGE_MANAGER.getMessage(TIMER_KEY), getClass().getSimpleName()));
        long taskTimeOut = threadConfiguration.getTaskTimeout() * 1000;
        timer.scheduleAtFixedRate(new TaskChecker(), taskTimeOut / 10, taskTimeOut / 3);
    }

    @Override
    protected boolean mayInterruptCanceledFutures() {
        return true;
    }

    public void submit(Runnable task) {
        if (task != null) {
            futures.add(completionService.submit(task, null));
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class TaskChecker extends TimerTask {

        protected static final String WARN_KEY = "fr.soleil.data.tasks.check.interrupted";

        @Override
        public void run() {
            try {
                waitForAllTasks(futures, true, threadConfiguration.getTaskTimeout(), null, completionService);
            } catch (InterruptedException e) {
                logger.warn(DataConnectionUtils.MESSAGE_MANAGER.getMessage(WARN_KEY), e);
            }
        }

    }

}
