/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.manager;

import org.slf4j.Logger;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.IThreadConfigurationTarget;
import fr.soleil.data.thread.IThreadConfiguration;
import fr.soleil.data.thread.factory.NamedThreadFactory;

/**
 * An {@link ATaskManager} that is also an {@link IThreadConfigurationTarget}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AConfigurableTaskManager extends ATaskManager implements IThreadConfigurationTarget {

    public AConfigurableTaskManager(Logger logger) {
        super(logger);
    }

    public AConfigurableTaskManager(Logger logger, Class<? extends NamedThreadFactory> factoryClass) {
        super(logger, factoryClass);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public IThreadConfiguration getThreadConfiguration() {
        return threadConfiguration;
    }

    @Override
    public void setThreadConfiguration(IThreadConfiguration threadConfiguration) {
        this.threadConfiguration = threadConfiguration == null ? generateDefaultThreadConfiguration()
                : threadConfiguration;
    }

}
