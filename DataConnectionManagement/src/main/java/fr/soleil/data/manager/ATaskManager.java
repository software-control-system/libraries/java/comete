/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.TaskManagerCompletionService;
import fr.soleil.data.thread.AutoSubmitThreadPoolExecutor;
import fr.soleil.data.thread.IThreadConfiguration;
import fr.soleil.data.thread.ThreadConfiguration;
import fr.soleil.data.thread.factory.NamedThreadFactory;
import fr.soleil.data.util.DataConnectionUtils;
import fr.soleil.data.util.LoggedRejectedExecutionHandler;
import fr.soleil.lib.project.ICancelable;

/**
 * Abstract class delegated to tasks management. This is a common class to treat tasks in threads.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class ATaskManager {

    protected static final String POOL_SHUTDOWN_ERROR_KEY = "fr.soleil.data.threads.pool.shutdown.error";
    protected static final String ELLAPSED_TIME_MULTI_KEY = "fr.soleil.data.tasks.time.ellapsed.thead.multi";
    protected static final String ELLAPSED_TIME_SINGLE_KEY = "fr.soleil.data.tasks.time.ellapsed.thead.single";
    protected static final String TASKS_INTERRUPTED_KEY = "fr.soleil.data.tasks.interrupted";
    protected static final String TASK_FINALIZER_KEY = "Finalize %s%s task iterator and launch next one";
    protected static final String ST_KEY = "fr.soleil.data.tasks.order.1";
    protected static final String ND_KEY = "fr.soleil.data.tasks.order.2";
    protected static final String RD_KEY = "fr.soleil.data.tasks.order.3";
    protected static final String TH_KEY = "fr.soleil.data.tasks.order.any";

    protected final Logger logger;
    protected volatile IThreadConfiguration threadConfiguration;
    protected final Class<? extends NamedThreadFactory> factoryClass;

    public ATaskManager(Logger logger) {
        this(logger, null);
    }

    public ATaskManager(Logger logger, Class<? extends NamedThreadFactory> factoryClass) {
        this.logger = (logger == null ? LoggerFactory.getLogger(Mediator.LOGGER_ACCESS) : logger);
        this.factoryClass = factoryClass;
        threadConfiguration = generateDefaultThreadConfiguration();
    }

    /**
     * Generates a default {@link ThreadConfiguration}.
     * 
     * @return A {@link ThreadConfiguration}. Never <code>null</code>.
     */
    protected ThreadConfiguration generateDefaultThreadConfiguration() {
        return new ThreadConfiguration();
    }

    /**
     * Returns whether, while canceling a {@link Future}, that {@link Future} might be interrupted if running.
     * 
     * @return A <code>boolean</code>.
     */
    protected abstract boolean mayInterruptCanceledFutures();

    /**
     * Generates a new {@link TaskManagerCompletionService}.
     * 
     * @param executor The {@link AbstractExecutorService} with which the {@link CompletionService} should be
     *            initialized. Can not be <code>null</code>.
     * @return A {@link TaskManagerCompletionService}.
     */
    public <V> TaskManagerCompletionService<V> generateCompletionService(AbstractExecutorService executor) {
        return new TaskManagerCompletionService<>(executor);
    }

    /**
     * Cancels a {@link Future} is not yet done or cancelled.
     * 
     * @param future The {@link Future} to cancel.
     */
    protected <V> void cancelFutureIfNecessary(Future<V> future) {
        if ((!future.isDone()) && (!future.isCancelled())) {
            future.cancel(mayInterruptCanceledFutures());
        }
    }

    /**
     * Traces in logs a task cancel.
     * 
     * @param taskTitle The task title.
     * @param t The potential reason why the task is cancelled.
     */
    protected void logCanceled(String taskTitle, Throwable t) {
        String message = String.format(DataConnectionUtils.MESSAGE_MANAGER.getMessage(TASKS_INTERRUPTED_KEY),
                taskTitle);
        if (t == null) {
            logger.warn(message);
        } else {
            logger.warn(message, t);
        }
    }

    /**
     * Cancels all tasks prepared in a {@link Future} {@link Collection}.
     * 
     * @param futureList The {@link Future} {@link Collection}. Can not be <code>null</code>.
     * @param taskTitle A text that represents the current global treatment. Can not be <code>null</code>.
     * @param t The potential reason why the tasks are cancelled.
     */
    protected <V> void shutdownTasks(Collection<Future<V>> futureList, String taskTitle, Throwable t) {
        if ((futureList != null) && (!futureList.isEmpty())) {
            boolean noneCanceled = true;
            for (Future<?> futur : futureList) {
                if (futur.cancel(mayInterruptCanceledFutures())) {
                    noneCanceled = false;
                }
            }
            if ((!noneCanceled) || (t != null)) {
                logCanceled(taskTitle, t);
            }
            futureList.clear();
        }
    }

    /**
     * Creates a new {@link AutoSubmitThreadPoolExecutor}.
     * 
     * @param threadTitle A way to name generated threads.
     * @param cancelable The {@link ICancelable} that can be used by the {@link AutoSubmitThreadPoolExecutor} to know
     *            when computation is canceled.
     * 
     * @return A {@link AutoSubmitThreadPoolExecutor}.
     */
    public AutoSubmitThreadPoolExecutor generateThreadPoolExecutor(String threadTitle, final ICancelable cancelable) {
        IThreadConfiguration threadConfiguration = this.threadConfiguration;
        int corePoolSize, maximumPoolSize;
        long keepAliveTime, taskTimeOut;
        if (threadConfiguration == null) {
            corePoolSize = IThreadConfiguration.DEFAULT_CORE_POOL_SIZE;
            maximumPoolSize = IThreadConfiguration.DEFAULT_MAX_POOL_SIZE;
            keepAliveTime = IThreadConfiguration.DEFAULT_KEEP_ALIVE_TIME;
            taskTimeOut = IThreadConfiguration.DEFAULT_TASK_TIMEOUT;
        } else {
            corePoolSize = threadConfiguration.isUseSystemCores() ? Runtime.getRuntime().availableProcessors()
                    : threadConfiguration.getCorePoolSize();
            maximumPoolSize = Math.max(corePoolSize, threadConfiguration.getMaximumPoolSize());
            keepAliveTime = threadConfiguration.getKeepAliveTime();
            taskTimeOut = threadConfiguration.getTaskTimeout();
        }
        AutoSubmitThreadPoolExecutor executor;
        if ((threadTitle == null) || threadTitle.trim().isEmpty()) {
            executor = new AutoSubmitThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, taskTimeOut,
                    TimeUnit.SECONDS, new LinkedBlockingQueue<>(), new LoggedRejectedExecutionHandler(logger),
                    cancelable);
        } else {
            NamedThreadFactory factory;
            if (factoryClass == null) {
                factory = new NamedThreadFactory(threadTitle);
            } else {
                try {
                    factory = factoryClass.getConstructor(String.class).newInstance(threadTitle);
                } catch (Exception e) {
                    factory = new NamedThreadFactory(threadTitle);
                }
            }
            executor = new AutoSubmitThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, taskTimeOut,
                    TimeUnit.SECONDS, new LinkedBlockingQueue<>(), factory, new LoggedRejectedExecutionHandler(logger),
                    cancelable);
        }
        return executor;
    }

    /**
     * Waits for executing {@link Future}s, removing them from a {@link Collection} until its size is under a given
     * limit.
     * 
     * @param futures The {@link Future} {@link Collection}.
     * @param concurrentFutureCollection Whether <code>futures</code> is a {@link Collection} that supports concurrent
     *            access/modifications.
     * @param taskTimeout The time to wait, in seconds, before considering tasks timed out.
     * @param cancelable The {@link ICancelable} to check before considering waiting for tasks.
     * @param completionService The {@link CompletionService} that manages the {@link Future}s.
     * @param maxSize The size limit, included.
     * @throws InterruptedException if interrupted while waiting.
     */
    protected <V> void waitForTasks(Collection<Future<V>> futures, boolean concurrentFutureCollection, long taskTimeout,
            ICancelable cancelable, CompletionService<V> completionService, int maxSize) throws InterruptedException {
        while ((futures.size() > maxSize) && ((cancelable == null) || (!cancelable.isCanceled()))) {
            Collection<Future<V>> futureList;
            if (concurrentFutureCollection) {
                futureList = new ArrayList<>(futures);
            } else {
                futureList = null;
            }
            try {
                Future<V> future = completionService.poll(taskTimeout, TimeUnit.SECONDS);
                if (future == null) {
                    // All futures timed out!
                    // 1- Notify for cancellation
                    if (cancelable != null) {
                        cancelable.setCanceled(true);
                    }
                    // 2- Cancel all futures
                    for (Future<V> tmp : futures) {
                        cancelFutureIfNecessary(tmp);
                    }
                    // 3- Empty future collection
                    if (futureList == null) {
                        futures.clear();
                    } else {
                        futures.removeAll(futureList);
                    }
                } else {
                    cancelFutureIfNecessary(future);
                    futures.remove(future);
                }
            } finally {
                if (futureList != null) {
                    futureList.clear();
                }
            }
        }
    }

    /**
     * Waits for all the tasks in a {@link Future} {@link Collection} to complete.
     * 
     * @param futures The {@link Future} {@link Collection}. Can not be <code>null</code>.
     * @param concurrentFutureCollection Whether <code>futures</code> is a {@link Collection} that supports concurrent
     *            access/modifications.
     * @param taskTimeout The time to wait, in seconds, before considering a task timed out.
     * @param cancelable An {@link ICancelable}, to know whether user asked for interruption.
     * @param completionService The {@link CompletionService} that manages the tasks threads. Can not be
     *            <code>null</code>.
     * @throws InterruptedException If interrupted while waiting.
     */
    protected <V> void waitForAllTasks(Collection<Future<V>> futures, boolean concurrentFutureCollection,
            long taskTimeout, ICancelable cancelable, CompletionService<V> completionService)
            throws InterruptedException {
        waitForTasks(futures, concurrentFutureCollection, taskTimeout, cancelable, completionService, 0);
    }

}
