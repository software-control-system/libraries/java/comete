/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.manager;

import java.util.concurrent.Callable;

import fr.soleil.data.listener.IStepListener;
import fr.soleil.data.util.StepDelegate;
import fr.soleil.lib.project.BasicCancelable;
import fr.soleil.lib.project.ICancelable;

/**
 * An {@link ITreatmentManager} that also is an {@link IStepManager}
 * 
 * @param <V> The type of data returned by the {@link Callable} for task treatment
 * @param <C> The type of {@link Callable} managed by this {@link ATreatmentManager}
 * @param <E> The type of {@link Exception} returned by this {@link ATreatmentManager} in case of treatment error.
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class ATreatmentManager<V, C extends Callable<V>, E extends Exception>
        implements ITreatmentManager<V, C, E>, IStepManager {

    protected final ICancelable cancelable;
    protected final StepDelegate stepDelegate;
    private boolean prepared;

    public ATreatmentManager(ICancelable cancelable) {
        this.cancelable = (cancelable == null ? new BasicCancelable() : cancelable);
        stepDelegate = new StepDelegate();
        prepared = false;
    }

    @Override
    public boolean areTaskIteratorsPrepared() {
        return prepared;
    }

    /**
     * Returns whether task iterators are ok.
     * This method is called in {@link #prepareTaskIterators()}, just after {@link #setupTaskIterators()}.
     * 
     * @return A <code>boolean</code>.
     */
    protected boolean areTaskIteratorsOk() {
        // by default, task iterators are always ok.
        return true;
    }

    @Override
    public final void prepareTaskIterators() {
        setupTaskIterators();
        prepared = areTaskIteratorsOk();
    }

    /**
     * Method called during {@link #prepareTaskIterators()}.
     * This method effectively prepares the task iterators.
     */
    protected abstract void setupTaskIterators();

    @Override
    public ICancelable getCancelable() {
        return cancelable;
    }

    @Override
    public void addStepListener(IStepListener listener) {
        stepDelegate.addStepListener(listener);
    }

    @Override
    public void removeStepListener(IStepListener listener) {
        stepDelegate.removeStepListener(listener);
    }

    @Override
    public boolean isSingleThreadOnly() {
        // by default, an ATreatmentManager is compatible with multi thread
        return false;
    }
}
