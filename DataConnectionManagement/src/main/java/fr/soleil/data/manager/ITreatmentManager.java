/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.manager;

import java.util.concurrent.Callable;

import fr.soleil.data.iterator.CalculationIterator;
import fr.soleil.lib.project.ICancelable;

/**
 * An interface for something able to do some treatments
 * 
 * @param <V> The type of data returned by the {@link Callable} for task treatment
 * @param <C> The type of {@link Callable} managed by this {@link ITreatmentManager}
 * @author Rapha&euml;l GIRARDOT
 */
public interface ITreatmentManager<V, C extends Callable<V>, E extends Exception> {

    /**
     * Prepares everything that has to be prepared before tasks treatment.
     * <p>
     * This method should be called before {@link #getTaskIterators()}
     * </p>
     * 
     * @see #getTaskIterators()
     */
    public void prepareTaskIterators();

    /**
     * Returns whether task iterators were prepared using {@link #prepareTaskIterators()}.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean areTaskIteratorsPrepared();

    /**
     * Returns a {@link CalculationIterator} array to treat tasks in parallel. The array is useful if you have to wait
     * for some tasks to finish before treating other tasks, in case of multiple threads.
     * <p>
     * This method should be called after {@link #prepareTaskIterators()}
     * </p>
     * 
     * @return A {@link CalculationIterator} array.
     * 
     * @see #prepareTaskIterators()
     */
    public CalculationIterator<V, C, E>[] getTaskIterators();

    /**
     * Returns the title describing the tasks managed by this {@link ITreatmentManager}.
     * This title will be visible in Threads if you monitor your application.
     * 
     * @return A {@link String}.
     */
    public String getTaskTitle();

    /**
     * Returns whether this {@link ITreatmentManager} is only compatible with single thread treatments
     * 
     * @return A <code>boolean</code>
     */
    public boolean isSingleThreadOnly();

    /**
     * Returns the {@link ICancelable} that can be used to cancel tasks treatment
     * 
     * @return An {@link ICancelable}
     */
    public ICancelable getCancelable();

}
