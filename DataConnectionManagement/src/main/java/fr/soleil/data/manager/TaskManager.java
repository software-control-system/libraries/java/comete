/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;

import fr.soleil.data.event.TaskEvent;
import fr.soleil.data.iterator.CalculationIterator;
import fr.soleil.data.listener.TaskFinishListener;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.source.BasicDataSource;
import fr.soleil.data.thread.AutoSubmitThreadPoolExecutor;
import fr.soleil.data.thread.IThreadConfiguration;
import fr.soleil.data.thread.factory.NamedThreadFactory;
import fr.soleil.data.util.DataConnectionUtils;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;

/**
 * A class delegated to tasks management. A {@link TaskManager} will decide whether or not to use multiple threads
 * depending on previously set {@link IThreadConfiguration}.
 * 
 * @see #treatAllTasks(ITreatmentManager)
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TaskManager extends AConfigurableTaskManager {

    private static final boolean MAY_INTERRUPT_IF_RUNNING = false;
    private static final String TITLE_SEPARATOR = " - ";

    private final BasicDataSource<Boolean> workingSource;

    public TaskManager(Logger logger) {
        this(logger, null);
    }

    public TaskManager(Logger logger, Class<? extends NamedThreadFactory> factoryClass) {
        super(logger, factoryClass);
        this.workingSource = new BasicDataSource<>(null, Boolean.FALSE);
        workingSource.setIgnoreDuplicationTest(true);
    }

    @Override
    protected boolean mayInterruptCanceledFutures() {
        return MAY_INTERRUPT_IF_RUNNING;
    }

    /**
     * Returns the {@link AbstractDataSource} indicating when this {@link TaskManager} is working
     * 
     * @return An {@link AbstractDataSource}
     */
    public AbstractDataSource<Boolean> getWorkingSource() {
        return workingSource;
    }

    /**
     * Generates a new {@link MultiThreadTreatmentDelegate}.
     * 
     * @param taskLimit The maximum number of tasks.
     * @param taskTimeout The task timeout (in seconds)
     * @param taskTitle The task title to use.
     * @param taskIterators The task iterators.
     * @param cancelable The {@link ICancelable} that indicates when treatment is cancelled.
     * @return A {@link MultiThreadTreatmentDelegate}.
     */
    protected <V, C extends Callable<V>, E extends Exception> MultiThreadTreatmentDelegate<V, C, E> generateMultiThreadTreatmentDelegate(
            int taskLimit, long taskTimeout, String taskTitle, CalculationIterator<V, C, E>[] taskIterators,
            ICancelable cancelable) {
        return new MultiThreadTreatmentDelegate<>(taskLimit, taskTimeout, taskTitle, taskIterators, cancelable);
    }

    /**
     * Treats all tasks managed by an {@link ITreatmentManager}, either in a single thread or in multiple threads,
     * depending on current configuration
     * 
     * @param treatmentManager The {@link ITreatmentManager}.
     */
    public <V, C extends Callable<V>, E extends Exception> void treatAllTasks(
            final ITreatmentManager<V, C, E> treatmentManager) {
        if (treatmentManager != null) {
            workingSource.setData(Boolean.TRUE);
            IThreadConfiguration threadManager = this.threadConfiguration;
            int taskLimit;
            long taskTimeout;
            boolean multiThread;
            if (threadManager == null) {
                taskLimit = IThreadConfiguration.DEFAULT_MAX_POOL_SIZE;
                taskTimeout = IThreadConfiguration.DEFAULT_TASK_TIMEOUT;
                multiThread = IThreadConfiguration.DEFAULT_MULTI_THREAD_ALLOWED;
            } else {
                taskLimit = threadManager.getMaximumPoolSize();
                taskTimeout = threadManager.getTaskTimeout();
                multiThread = threadManager.isMultiThreadAllowed();
            }
            if (treatmentManager.isSingleThreadOnly()) {
                multiThread = false;
            }
            String taskTitle = treatmentManager.getTaskTitle();
            if (!treatmentManager.areTaskIteratorsPrepared()) {
                treatmentManager.prepareTaskIterators();
            }
            CalculationIterator<V, C, E>[] taskIterators = treatmentManager.getTaskIterators();
            if (taskIterators == null) {
                workingSource.setData(Boolean.FALSE);
            } else {
                ICancelable cancelable = treatmentManager.getCancelable();
                if (multiThread) {
                    generateMultiThreadTreatmentDelegate(taskLimit, taskTimeout, taskTitle, taskIterators, cancelable)
                            .treatAllTasks();
                } else {
                    long time = System.currentTimeMillis();
                    for (CalculationIterator<V, C, E> taskIterator : taskIterators) {
                        if ((cancelable != null) && cancelable.isCanceled()) {
                            break;
                        }
                        if (taskIterator != null) {
                            try {
                                while (taskIterator.hasNext()) {
                                    if ((cancelable != null) && cancelable.isCanceled()) {
                                        break;
                                    }
                                    taskIterator.treatDataInASingleThread();
                                }
                            } catch (Exception e) {
                                if (cancelable != null) {
                                    cancelable.setCanceled(true);
                                }
                                logCanceled(taskTitle, e);
                            } finally {
                                taskIterator.close();
                            }
                        }
                    }
                    logger.trace(DateUtil.elapsedTimeToStringBuilder(
                            new StringBuilder(DataConnectionUtils.MESSAGE_MANAGER.getMessage(ELLAPSED_TIME_SINGLE_KEY)),
                            System.currentTimeMillis() - time).toString());
                    workingSource.setData(Boolean.FALSE);
                } // end if (multiThread) ... else
            } // end if (taskIterators == null) ... else
        } // end if (treatmentManager != null)
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class MultiThreadTreatmentDelegate<V, C extends Callable<V>, E extends Exception>
            implements TaskFinishListener {
        protected int taskLimit;
        protected long taskTimeout;
        protected String taskTitle;
        protected CalculationIterator<V, C, E>[] taskIterators;
        protected ICancelable cancelable;
        protected AtomicInteger iteratorIndex;
        protected StringBuilder builder;
        protected long time;
        protected List<Future<V>> futureList;
        // boolean used to check for interruption/cancellation
        protected volatile boolean interrupted;

        public MultiThreadTreatmentDelegate(int taskLimit, long taskTimeout, String taskTitle,
                CalculationIterator<V, C, E>[] taskIterators, ICancelable cancelable) {
            super();
            this.taskLimit = taskLimit;
            this.taskTimeout = taskTimeout;
            this.taskTitle = taskTitle;
            this.taskIterators = taskIterators;
            this.cancelable = cancelable;
            this.iteratorIndex = new AtomicInteger(-1);
            this.builder = new StringBuilder();
            this.interrupted = false;
        }

        protected StringBuilder generateStringBuilder() {
            return taskTitle == null ? new StringBuilder() : new StringBuilder(taskTitle).append(TITLE_SEPARATOR);
        }

        public void treatAllTasks() {
            time = System.currentTimeMillis();
            futureList = new ArrayList<>(taskLimit);
            treatNextThreadGroup();
        }

        private boolean isCanceled(AutoSubmitThreadPoolExecutor executor) {
            boolean canceled = interrupted;
            if (!canceled) {
                if (executor != null) {
                    if (executor.isInterrupted()) {
                        canceled = true;
                        interrupted = true;
                    }
                }
                if (!canceled) {
                    ICancelable cancelable = this.cancelable;
                    canceled = (cancelable != null) && cancelable.isCanceled();
                }
            }
            return canceled;
        }

        protected void traceEllapsedTime() {
            logger.trace(DateUtil.elapsedTimeToStringBuilder(
                    builder.append(DataConnectionUtils.MESSAGE_MANAGER.getMessage(ELLAPSED_TIME_MULTI_KEY)),
                    System.currentTimeMillis() - time).toString());
        }

        protected void treatNextThreadGroup() {
            AtomicInteger iteratorIndex = this.iteratorIndex;
            if ((iteratorIndex != null) && (!isCanceled(null))) {
                builder = generateStringBuilder();
                int index = iteratorIndex.incrementAndGet();
                int formerIndex = index - 1;
                if ((formerIndex > -1) && (formerIndex < taskIterators.length)) {
                    CalculationIterator<V, C, E> toClose = taskIterators[formerIndex];
                    if (toClose != null) {
                        toClose.close();
                    }
                }
                if (index < taskIterators.length) {
                    time = System.currentTimeMillis();
                    CalculationIterator<V, C, E> taskIterator = taskIterators[index];
                    if ((taskIterator != null) && (!isCanceled(null))) {
                        if (taskIterator.isThreadSafe()) {
                            // taskIterator is thread safe: use multi threads
                            int maxSize = taskLimit - 1;
                            try {
                                AutoSubmitThreadPoolExecutor executor = generateThreadPoolExecutor(taskTitle,
                                        cancelable);
                                executor.addTaskFinishListener(this);
                                CompletionService<V> completionService = generateCompletionService(executor);
                                while (taskIterator.hasNext()) {
                                    if (isCanceled(executor)) {
                                        break;
                                    }
                                    C task = taskIterator.next();
                                    // Do not exceed maximum number of parallel tasks
                                    waitForTasks(futureList, false, taskTimeout, cancelable, completionService,
                                            maxSize);
                                    futureList.add(completionService.submit(task));
                                }
                                waitForAllTasks(futureList, false, taskTimeout, cancelable, completionService);
                            } catch (Exception e) {
                                interrupted = true;
                                if (cancelable != null) {
                                    cancelable.setCanceled(true);
                                }
                                shutdownTasks(futureList, taskTitle, e);
                                taskIterator.close();
                            }
                        } else {
                            // taskIterator is not thread safe: treat data in a single thread
                            try {
                                taskIterator.treatDataInASingleThread();
                            } catch (Exception e) {
                                interrupted = true;
                                if (cancelable != null) {
                                    cancelable.setCanceled(true);
                                }
                                logCanceled(taskTitle, e);
                            }
                            traceEllapsedTime();
                            treatNextThreadGroup();
                        } // end if (taskIterator.isThreadSafe()) ... else
                    } // end if ((taskIterator != null) && (!isCanceled(null)))
                } // end if (index < taskIterators.length)
            } // end if ((iteratorIndex != null) && (!isCanceled(null)))
        }

        @Override
        public void allTasksFinished(final TaskEvent event) {
            int index = iteratorIndex.get() + 1;
            String addKey;
            switch (index) {
                case 1:
                    addKey = DataConnectionUtils.MESSAGE_MANAGER.getMessage(ST_KEY);
                    break;
                case 2:
                    addKey = DataConnectionUtils.MESSAGE_MANAGER.getMessage(ND_KEY);
                    break;
                case 3:
                    addKey = DataConnectionUtils.MESSAGE_MANAGER.getMessage(RD_KEY);
                    break;
                default:
                    addKey = DataConnectionUtils.MESSAGE_MANAGER.getMessage(TH_KEY);
                    break;
            }

            Thread nextTreatmentThread = new Thread(
                    String.format(DataConnectionUtils.MESSAGE_MANAGER.getMessage(TASK_FINALIZER_KEY), index, addKey)) {
                @Override
                public void run() {
                    AutoSubmitThreadPoolExecutor executor = event.getSource();
                    if (executor != null) {
                        executor.removeTaskFinishListener(MultiThreadTreatmentDelegate.this);
                    }
                    if (isCanceled(executor)) {
                        shutdownTasks(futureList, taskTitle, null);
                        executor.purge();
                    } else {
                        futureList.clear();
                        if (executor != null) {
                            try {
                                executor.shutdown();
                                executor.purge();
                            } catch (Exception e) {
                                String tmpTitle = taskTitle;
                                if (tmpTitle == null) {
                                    tmpTitle = ObjectUtils.EMPTY_STRING;
                                }
                                logger.warn(String.format(
                                        DataConnectionUtils.MESSAGE_MANAGER.getMessage(POOL_SHUTDOWN_ERROR_KEY),
                                        tmpTitle), e);
                            }
                        }
                        traceEllapsedTime();
                        treatNextThreadGroup();
                    }
                }
            };
            nextTreatmentThread.start();
        }

        @Override
        protected void finalize() throws Throwable {
            // clean references
            taskTitle = null;
            taskIterators = null;
            cancelable = null;
            iteratorIndex = null;
            builder = null;
            if (futureList != null) {
                futureList.clear();
                futureList = null;
            }
            super.finalize();
        }
    }
}
