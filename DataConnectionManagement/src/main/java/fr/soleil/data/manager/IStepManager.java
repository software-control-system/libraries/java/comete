/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.manager;

import fr.soleil.data.listener.IStepListener;

/**
 * An interface for something able to manage steps
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IStepManager {

    /**
     * Adds an {@link IStepListener} to this {@link IStepManager}
     * 
     * @param listener The {@link IStepListener} to add
     */
    public void addStepListener(IStepListener listener);

    /**
     * Removes an {@link IStepListener} from this {@link IStepManager}
     * 
     * @param listener The {@link IStepListener} to remove
     */
    public void removeStepListener(IStepListener listener);

}
