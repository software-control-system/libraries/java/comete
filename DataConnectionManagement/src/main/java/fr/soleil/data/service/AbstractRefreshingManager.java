/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.LoggerFactory;

import fr.soleil.data.container.IUpdatableDataContainer;
import fr.soleil.data.event.GroupEvent;
import fr.soleil.data.event.RefreshEvent;
import fr.soleil.data.listener.IRefreshingGroupListener;
import fr.soleil.data.listener.IRefreshingThreadListener;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.plugin.AbstractPlugin;
import fr.soleil.data.service.thread.AbstractRefreshingThread;
import fr.soleil.data.service.thread.DataSourceRefreshingThread;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.source.BufferedDataSource;
import fr.soleil.data.source.IPluggableDataSource;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;

/**
 * Class that gives some basic methods to implement refreshing strategies.
 * <P>
 * An {@link AbstractRefreshingManager} creates some {@link AbstractDataSource}s, knows the {@link AbstractDataSource}s
 * it created, and is able to refresh them.
 * </P>
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <ID> The type of key used to recover known {@link AbstractDataSource}s
 */
public abstract class AbstractRefreshingManager<ID> implements IConstantSourceProducer, IRefreshingThreadListener {

    private static final String CANCELING_REFRESHMENT_OF_SOURCE = "Canceling refreshment of source \"%s\" (%s)";
    private static final String TO_KEEP_TRACE_OF_NULL_STRATEGY = "To keep trace of null strategy";
    private static final String REFRESHING_PAUSED = "Refreshing paused, which means \"%s\" won't be applied now to source \"%s\" (%s)";
    private static final String APPLYING_REFRESHING_STRATEGY_TO_SOURCE = "Applying refreshing strategy \"%s\" to source \"%s\" (%s)";
    private static final String FAILED_TO_APPLY_STRATEGY_TO_SOURCE = "Failed to apply \"%s\" strategy to source \"%s\" (%s)";

    protected static String DEFAULT_META_INFORMATION = "DataSource";

    /**
     * A map of known sources
     */
    protected final HashMap<ID, AbstractDataSource<?>> knownSources;

    protected final Map<AbstractDataSource<?>, IRefreshingStrategy> strategyMap;

    protected IRefreshingStrategy defaultRefreshingStrategy;

    /**
     * Maps that stores the {@link AbstractRefreshingThread}s which manage the {@link BufferedDataSource}'s
     * {@link PolledRefreshingStrategy}. This {@link Map} uses refreshing period as key
     */
    protected final Map<Integer, DataSourceRefreshingThread> bufferedSourcesTimeThreads;

    /**
     * Maps that stores the {@link AbstractRefreshingThread}s which manage the {@link BufferedDataSource}'s
     * {@link GroupRefreshingStrategy}. This {@link Map} uses refreshing name as key
     */
    protected final Map<String, DataSourceRefreshingThread> bufferedSourcesGroupThreads;

    protected final Object strategyLock;

    /**
     * Map that stores to which group a thread belongs
     */
    protected final Map<AbstractRefreshingThread<?>, String> threadGroupMap;

    protected final Collection<IRefreshingGroupListener> refreshingGroupListeners;

    protected volatile boolean pauseRefreshing;
    protected Map<AbstractDataSource<?>, IRefreshingStrategy> savedStrategyMap;

    /**
     * Default constructor
     */
    protected AbstractRefreshingManager() {
        super();
        pauseRefreshing = false;
        knownSources = new HashMap<ID, AbstractDataSource<?>>();
        bufferedSourcesTimeThreads = new HashMap<Integer, DataSourceRefreshingThread>();
        bufferedSourcesGroupThreads = new HashMap<String, DataSourceRefreshingThread>();
        strategyMap = new ConcurrentHashMap<AbstractDataSource<?>, IRefreshingStrategy>();
        threadGroupMap = new ConcurrentHashMap<AbstractRefreshingThread<?>, String>();
        strategyLock = new Object();
        refreshingGroupListeners = Collections.newSetFromMap(new WeakHashMap<IRefreshingGroupListener, Boolean>());
    }

    @Override
    public AbstractDataSource<?> createDataSource(IKey key) {
        return createDataSource(key, false, false);
    }

    @Override
    public AbstractDataSource<?> createDataSource(IKey key, boolean forceCreation) {
        return createDataSource(key, forceCreation, false);
    }

    protected String getSourceKeyErrorMessage(IKey key) {
        String keyMessage;
        if (key == null) {
            keyMessage = "null key";
        } else {
            try {
                keyMessage = "desired source was " + key.getInformationKey();
            } catch (Exception e) {
                keyMessage = "Failed to extract information for key " + key.getClass().getName() + '@' + key.hashCode();
            }
        }
        return keyMessage;
    }

    /**
     * Creates an {@link AbstractDataSource} that manages a particular type of data
     * 
     * @param key The {@link IKey} that contains the necessary information to identify the way to
     *            create the {@link AbstractDataSource}
     * @param forceCreation Whether to force source creation even if this source is already cached
     * @param readImmediately Whether to force source to read data immediately, without waiting for
     *            any kind of refreshing strategy
     * @return An {@link AbstractDataSource}, or <code>null</null> if there was no way creating it.
     */
    public final AbstractDataSource<?> createDataSource(IKey key, boolean forceCreation, boolean readImmediately) {
        AbstractDataSource<?> result = null;
        IKey parsedKey = null;
        try {
            parsedKey = parseKey(key);
            if (parsedKey != null) {
                result = getExistingSource(parsedKey);
                if (forceCreation) {
                    removeDataSource(parsedKey);
                    result = null;
                }
                if (result == null) {
                    result = constructDataSource(parsedKey);
                    if (result != null) {
                        if (readImmediately) {
                            if (result instanceof IUpdatableDataContainer<?>) {
                                try {
                                    ((IUpdatableDataContainer<?>) result).updateData();
                                } catch (Exception e) {
                                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(
                                            "Failed to update content of data source '" + result + "' on first read",
                                            e);
                                }
                            }
                        }
                        updateKnownSources(parsedKey, result);
                        if (!pauseRefreshing) {
                            initDefaultRefreshingStrategy(result);
                        }
                    }
                }
            }
        } catch (Exception e) {
            String keyMessage = getSourceKeyErrorMessage(parsedKey == null ? key : parsedKey);
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Error during datasource creation: " + keyMessage, e);
        }
        return result;
    }

    /**
     * Registers an {@link AbstractDataSource} in the {@link Map} of known {@link AbstractDataSource}s
     * 
     * @param key The {@link IKey} with which the {@link AbstractDataSource} was created
     * @param source The {@link AbstractDataSource}
     */
    protected void updateKnownSources(IKey key, AbstractDataSource<?> source) {
        ID mapKey = createIdentifier(parseKey(key));
        if ((mapKey != null) && (source != null)) {
            synchronized (knownSources) {
                knownSources.put(mapKey, source);
            }
        }
    }

    /**
     * Returns the {@link AbstractDataSource} previously created with a given {@link IKey}
     * 
     * @param key The {@link IKey}
     * @return An {@link AbstractDataSource}. <code>null</code> if no such data source was found
     */
    @Override
    public AbstractDataSource<?> getExistingSource(IKey key) {
        AbstractDataSource<?> result = null;
        if (key != null) {
            ID mapKey = createIdentifier(parseKey(key));
            if (mapKey != null) {
                synchronized (knownSources) {
                    if (knownSources.containsKey(mapKey)) {
                        result = knownSources.get(mapKey);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Puts all refreshing strategies on pause, by stopping and saving them in order to restart them later.
     * 
     * @see #resumeRefreshingStrategies()
     */
    public final void pauseRefreshingStrategies() {
        pauseRefreshing = true;
        synchronized (strategyLock) {
            if (savedStrategyMap == null) {
                savedStrategyMap = new HashMap<>(strategyMap);
                for (AbstractDataSource<?> source : savedStrategyMap.keySet()) {
                    setRefreshingStrategy(source, null);
                }
            }
        }
    }

    /**
     * Resumes refreshing strategies.
     * 
     * @see #pauseRefreshing
     */
    public final void resumeRefreshingStrategies() {
        pauseRefreshing = false;
        synchronized (strategyLock) {
            if (savedStrategyMap != null) {
                for (Entry<AbstractDataSource<?>, IRefreshingStrategy> entry : savedStrategyMap.entrySet()) {
                    setRefreshingStrategy(entry.getKey(), entry.getValue());
                }
                Map<AbstractDataSource<?>, IRefreshingStrategy> tmp = savedStrategyMap;
                savedStrategyMap = null;
                tmp.clear();
            }
        }
    }

    /**
     * Returns whether all refreshing strategies were paused by {@link #pauseRefreshingStrategies()}
     * 
     * @return A <code>boolean</code>.
     * @see #pauseRefreshingStrategies()
     * @see #resumeRefreshingStrategies()
     */
    public boolean areRefreshingStrategiesPaused() {
        return pauseRefreshing;
    }

    /**
     * Initialize the default refreshing strategy for the given {@link AbstractDataSource}
     * 
     * @param source The source for which to initialize the strategy
     */
    protected void initDefaultRefreshingStrategy(AbstractDataSource<?> source) {
        setRefreshingStrategy(source, getDefaultRefreshingStrategy());
    }

    /**
     * Creates an identifier to recover an {@link AbstractDataSource} in the map of known sources
     * 
     * @param key The {@link IKey} that contains the necessary information to identify the way to
     *            create the {@link AbstractDataSource}, and what kind of {@link AbstractDataSource} is
     *            expected (i.e. its type).
     * @return The expected identifier
     */
    protected abstract ID createIdentifier(IKey key);

    /**
     * Constructs an {@link AbstractDataSource} that manages a particular type of data. The
     * constructed {@link AbstractDataSource} is likely to be a buffered one, if compatible with
     * data refreshing. This is called by {@link #createDataSource(IKey)} if the {@link AbstractDataSource} was not
     * already created
     * 
     * @param key The {@link IKey} that contains the necessary information to identify the way to
     *            create the {@link AbstractDataSource}
     * @return an {@link AbstractDataSource}
     * @see #createDataSource(IKey)
     */
    protected abstract AbstractDataSource<?> constructDataSource(IKey key);

    /**
     * Returns the {@link IRefreshingStrategy} previously set for a given source
     * 
     * @param key The {@link IKey} that identifies the source
     * @return An {@link IRefreshingStrategy}. Can be <code>null</code>
     */
    public IRefreshingStrategy getRefreshingStrategy(IKey key) {
        IRefreshingStrategy strategy = null;
        AbstractDataSource<?> source = getExistingSource(key);
        if (source != null) {
            strategy = strategyMap.get(source);
            if ((strategy == null) && pauseRefreshing) {
                synchronized (strategyLock) {
                    if (pauseRefreshing && (savedStrategyMap != null)) {
                        strategy = savedStrategyMap.get(source);
                    } else {
                        strategy = strategyMap.get(source);
                    }
                }
            }
        }
        return strategy;
    }

    /**
     * Sets the refreshing strategy for a previously produced source
     * 
     * @param key The key that identifies the previously produced source. If no such source was
     *            produced, nothing happens.
     * @param strategy The {@link IRefreshingStrategy} to use
     */
    public void setRefreshingStrategy(IKey key, IRefreshingStrategy strategy) {
        setRefreshingStrategy(getExistingSource(key), strategy);
    }

    /**
     * Sets the refreshing strategy for a given source
     * 
     * @param source The concerned source
     * @param strategy The {@link IRefreshingStrategy} to use
     */
    public final void setRefreshingStrategy(AbstractDataSource<?> source, IRefreshingStrategy strategy) {
        if (source != null) {
            try {
                synchronized (strategyLock) {
                    prepareDataSourceBeforeStrategyChange(source);
                    // stop refreshing before changing refreshing strategy
                    // in order to avoid duplicate threads (Jira JAVAAPI-125)
                    strategyMap.remove(source);
                    stopRefreshing(source);
                    if (strategy == null) {
                        // TODO remove that trace later (PROBLEM-2000)
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).debug(
                                String.format(CANCELING_REFRESHMENT_OF_SOURCE, source, source.getClass().getName()),
                                new Exception(TO_KEEP_TRACE_OF_NULL_STRATEGY));
                    } else {
                        if (pauseRefreshing) {
                            // TODO remove that trace later (PROBLEM-2000)
                            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).debug(
                                    String.format(REFRESHING_PAUSED, strategy, source, source.getClass().getName()));
                            if (savedStrategyMap != null) {
                                savedStrategyMap.put(source, strategy);
                            }
                        } else {
                            // TODO remove that trace later (PROBLEM-2000)
                            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                    .debug(String.format(APPLYING_REFRESHING_STRATEGY_TO_SOURCE, strategy, source,
                                            source.getClass().getName()));
                            strategyMap.put(source, strategy);
                            if (strategy instanceof GroupRefreshingStrategy) {
                                GroupRefreshingStrategy groupStrategy = (GroupRefreshingStrategy) strategy;
                                if (source instanceof BufferedDataSource<?>) {
                                    applyGroupRefreshingStrategyToBufferedSource((BufferedDataSource<?>) source,
                                            groupStrategy);
                                } else {
                                    applyGroupRefreshingStrategyToOtherSource(source, groupStrategy);
                                }
                            } else if (strategy instanceof PolledRefreshingStrategy) {
                                PolledRefreshingStrategy polledStrategy = (PolledRefreshingStrategy) strategy;
                                if (source instanceof BufferedDataSource<?>) {
                                    applyPolledRefreshingStrategyToBufferedSource((BufferedDataSource<?>) source,
                                            polledStrategy);
                                } else {
                                    applyPolledRefreshingStrategyToOtherSource(source, polledStrategy);
                                }
                            } else {
                                applyOtherRefreshingStrategyToSource(source, strategy);
                            }
                        } // end if (pauseRefreshing) ... else
                    } // end if (strategy != null)
                } // end synchronized (strategyLock)
            } catch (Throwable t) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(String.format(FAILED_TO_APPLY_STRATEGY_TO_SOURCE,
                        strategy, source, source.getClass().getName()), t);
            }
        } // end if (source != null)
    }

    /**
     * Applies a {@link PolledRefreshingStrategy} to a given {@link BufferedDataSource}
     * 
     * @param source The {@link BufferedDataSource}
     * @param strategy The {@link PolledRefreshingStrategy}
     */
    protected void applyPolledRefreshingStrategyToBufferedSource(BufferedDataSource<?> source,
            PolledRefreshingStrategy strategy) {
        applyPolledRefreshingStrategy(bufferedSourcesTimeThreads, source, strategy, DataSourceRefreshingThread.class);
    }

    /**
     * Applies a {@link GroupRefreshingStrategy} to a given {@link BufferedDataSource}
     * 
     * @param source The {@link BufferedDataSource}
     * @param strategy The {@link GroupRefreshingStrategy}
     */
    protected void applyGroupRefreshingStrategyToBufferedSource(BufferedDataSource<?> source,
            GroupRefreshingStrategy strategy) {
        applyGroupRefreshingStrategy(bufferedSourcesGroupThreads, source, strategy, DataSourceRefreshingThread.class);
    }

    /**
     * Applies a {@link PolledRefreshingStrategy} to an {@link AbstractDataSource} that is not a
     * {@link BufferedDataSource}
     * 
     * @param source The {@link AbstractDataSource}
     * @param strategy The {@link PolledRefreshingStrategy}
     */
    protected void applyPolledRefreshingStrategyToOtherSource(AbstractDataSource<?> source,
            PolledRefreshingStrategy strategy) {
        // nothing to by default
    }

    /**
     * Applies a {@link GroupRefreshingStrategy} to an {@link AbstractDataSource} that is not a
     * {@link BufferedDataSource}
     * 
     * @param source The {@link AbstractDataSource}
     * @param strategy The {@link GroupRefreshingStrategy}
     */
    protected void applyGroupRefreshingStrategyToOtherSource(AbstractDataSource<?> source,
            GroupRefreshingStrategy strategy) {
        // nothing to by default
    }

    /**
     * Applies an {@link IRefreshingStrategy}, that is not a {@link PolledRefreshingStrategy}, to a
     * given {@link AbstractDataSource}
     * 
     * @param source The {@link AbstractDataSource}
     * @param strategy The {@link IRefreshingStrategy}
     */
    protected void applyOtherRefreshingStrategyToSource(AbstractDataSource<?> source, IRefreshingStrategy strategy) {
        // nothing to do by default
    }

    protected <K, S extends AbstractDataSource<?>, T extends AbstractRefreshingThread<? super S>> void applyStrategy(
            K key, Map<K, T> threadMap, S dataSource, PolledRefreshingStrategy strategy, Class<T> threadClass) {
        T thread = threadMap.get(key);
        if ((thread == null) || thread.isStoped()) {
            thread = constructThread(threadClass, getThreadName(dataSource, threadClass, strategy));
            if (thread != null) {
                thread.start();
                threadMap.put(key, thread);
                if (strategy instanceof GroupRefreshingStrategy) {
                    threadGroupMap.put(thread, ((GroupRefreshingStrategy) strategy).getName());
                    thread.addRefreshingListener(this);
                }
            }
        }
        if (thread != null) {
            thread.setSleepingPeriod(strategy.getRefreshingPeriod());
            String threadName = getThreadName(dataSource, threadClass, strategy);
            if (!ObjectUtils.sameObject(threadName, thread.getName())) {
                thread.setName(threadName);
            }
            prepareDataSource(dataSource, strategy);
            thread.addData(dataSource);
        }
    }

    /**
     * Applies a {@link PolledRefreshingStrategy} to an {@link AbstractDataSource}, based on a {@link Map}
     * 
     * @param threadMap The {@link Map} that contains the {@link AbstractRefreshingThread}s that
     *            manage the {@link AbstractDataSource}s refreshing
     * @param dataSource The {@link AbstractDataSource}
     * @param strategy The {@link PolledRefreshingStrategy}
     * @param threadClass The {@link AbstractRefreshingThread} {@link Class}, used to generate a new
     *            {@link AbstractRefreshingThread} when necessary
     */
    protected <S extends AbstractDataSource<?>, T extends AbstractRefreshingThread<? super S>> void applyPolledRefreshingStrategy(
            Map<Integer, T> threadMap, S dataSource, PolledRefreshingStrategy strategy, Class<T> threadClass) {
        if ((threadMap != null) && (dataSource != null) && (strategy != null) && (threadClass != null)
                && (strategy.getRefreshingPeriod() > 0)) {
            stopRefreshing(threadMap, dataSource);
            Integer period = Integer.valueOf(strategy.getRefreshingPeriod());
            applyStrategy(period, threadMap, dataSource, strategy, threadClass);
        }
    }

    /**
     * Applies a {@link GroupRefreshingStrategy} to an {@link AbstractDataSource}, based on a {@link Map}
     * 
     * @param threadMap The {@link Map} that contains the {@link AbstractRefreshingThread}s that
     *            manage the {@link AbstractDataSource}s refreshing
     * @param dataSource The {@link AbstractDataSource}
     * @param strategy The {@link GroupRefreshingStrategy}
     * @param threadClass The {@link AbstractRefreshingThread} {@link Class}, used to generate a new
     *            {@link AbstractRefreshingThread} when necessary
     */
    protected <S extends AbstractDataSource<?>, T extends AbstractRefreshingThread<? super S>> void applyGroupRefreshingStrategy(
            Map<String, T> threadMap, S dataSource, GroupRefreshingStrategy strategy, Class<T> threadClass) {
        if ((threadMap != null) && (dataSource != null) && (strategy != null) && (threadClass != null)
                && (strategy.getRefreshingPeriod() > 0)) {
            stopRefreshing(threadMap, dataSource);
            String name = strategy.getName();
            if ((name != null) && (!name.trim().isEmpty())) {
                applyStrategy(name, threadMap, dataSource, strategy, threadClass);
            }
        }
    }

    /**
     * Returns the desired name for an {@link AbstractRefreshingThread} creation
     * 
     * @param dataSource The {@link AbstractDataSource} for which the {@link AbstractRefreshingThread} is created
     * @param threadClass The {@link AbstractRefreshingThread} {@link Class}
     * @param strategy The {@link IRefreshingStrategy} for which the {@link AbstractRefreshingThread} is created
     * @return A {@link String}
     */
    protected <S extends AbstractDataSource<?>, T extends AbstractRefreshingThread<? super S>> String getThreadName(
            S dataSource, Class<T> threadClass, IRefreshingStrategy strategy) {
        StringBuilder builder = new StringBuilder(threadClass.getSimpleName());
        if (strategy instanceof PolledRefreshingStrategy) {
            appendPolledRefreshingStrategyName(builder, (PolledRefreshingStrategy) strategy);
        }
        return builder.toString();
    }

    protected StringBuilder appendPolledRefreshingStrategyName(StringBuilder builder,
            PolledRefreshingStrategy strategy) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        boolean grouped;
        if (strategy instanceof GroupRefreshingStrategy) {
            grouped = true;
            builder.append('{').append(((GroupRefreshingStrategy) strategy).getName());
        } else {
            grouped = false;
        }
        DateUtil.elapsedTimeToStringBuilder(builder.append('('), strategy.getRefreshingPeriod()).append(')');
        if (grouped) {
            builder.append('}');
        }
        return builder;
    }

    /**
     * Stops the refreshing of a given {@link AbstractDataSource}
     * 
     * @param source The {@link AbstractDataSource}
     */
    protected void stopRefreshing(AbstractDataSource<?> source) {
        if (source instanceof BufferedDataSource<?>) {
            stopBufferedSourceRefreshing((BufferedDataSource<?>) source);
        } else {
            stopOtherSourceRefreshing(source);
        }
    }

    /**
     * Stops the refreshing of a given {@link BufferedDataSource}
     * 
     * @param source The {@link BufferedDataSource}
     */
    protected void stopBufferedSourceRefreshing(BufferedDataSource<?> source) {
        stopRefreshing(bufferedSourcesTimeThreads, source);
        stopRefreshing(bufferedSourcesGroupThreads, source);
    }

    /**
     * Stops the refreshing of a given {@link AbstractDataSource} that is not a {@link BufferedDataSource}
     * 
     * @param source The {@link AbstractDataSource}
     */
    protected void stopOtherSourceRefreshing(AbstractDataSource<?> source) {
        // nothing to do by default
    }

    /**
     * Stops some data refreshing from a given {@link AbstractRefreshingThread} {@link Map}
     * 
     * @param threadMap The {@link AbstractRefreshingThread} {@link Map}
     * @param data The concerned data
     */
    protected <K, S, T extends AbstractRefreshingThread<? super S>> void stopRefreshing(Map<K, T> threadMap, S data) {
        if ((threadMap != null) && (data != null)) {
            // Work on map copy to remove dead threads as soon as possible
            Map<K, T> copy = new LinkedHashMap<K, T>(threadMap.size());
            copy.putAll(threadMap);
            for (Entry<K, T> entry : copy.entrySet()) {
                T thread = entry.getValue();
                thread.removeData(data);
                if (thread.isEmpty()) {
                    // first, remove thread reference
                    threadMap.remove(entry.getKey());
                    threadGroupMap.remove(thread);
                    // then stop thread
                    thread.doStop();
                }
            }
            // clear copy
            copy.clear();
        }
    }

    /**
     * Prepares an {@link AbstractDataSource} before applying an {@link IRefreshingStrategy}
     * 
     * @param dataSource The {@link AbstractDataSource}
     * @param strategy The {@link IRefreshingStrategy}
     */
    protected <S extends AbstractDataSource<?>> void prepareDataSource(S dataSource, IRefreshingStrategy strategy) {
        // nothing to do by default
    }

    /**
     * Prepares an {@link AbstractDataSource} before changing its {@link IRefreshingStrategy}
     * 
     * @param dataSource The {@link AbstractDataSource}
     */
    protected <S extends AbstractDataSource<?>> void prepareDataSourceBeforeStrategyChange(S dataSource) {
        // nothing to do by default
    }

    /**
     * Creates and returns a new {@link AbstractRefreshingThread} of a given {@link Class}
     * 
     * @param threadClass The {@link AbstractRefreshingThread} {@link Class}
     * @param name The desired thread name
     * @return An {@link AbstractRefreshingThread}. <code>null</code> if creation failed.
     */
    protected <T extends AbstractRefreshingThread<?>> T constructThread(Class<T> threadClass, String name) {
        T result;
        if (threadClass == null) {
            result = null;
        } else {
            if ((name == null) || (name.trim().isEmpty())) {
                try {
                    result = threadClass.newInstance();
                } catch (Exception e2) {
                    result = null;
                }
            } else {
                try {
                    Constructor<T> constructor = threadClass.getConstructor(String.class);
                    result = constructor.newInstance(name);
                } catch (Exception e) {
                    try {
                        result = threadClass.newInstance();
                    } catch (Exception e2) {
                        result = null;
                    }
                }
            }
        }
        return result;
    }

    @Override
    public void removeDataSource(IKey key) {
        if (key != null) {
            AbstractDataSource<?> source = getExistingSource(key);
            if (source != null) {
                setRefreshingStrategy(source, null);
                synchronized (knownSources) {
                    knownSources.remove(createIdentifier(key));
                }
                if (source instanceof IPluggableDataSource) {
                    List<AbstractPlugin<?>> plugins = ((IPluggableDataSource) source).getListPlugin();
                    if (plugins != null) {
                        for (AbstractPlugin<?> plugin : plugins) {
                            IKey pluginKey = plugin.getPluginKey();
                            if (pluginKey != null) {
                                ID id = createIdentifier(pluginKey);
                                AbstractDataSource<?> pluginSource = null;
                                if (id != null) {
                                    synchronized (knownSources) {
                                        pluginSource = knownSources.get(id);
                                    }
                                }
                                // clean plugin when not linked
                                if ((pluginSource != null) && (!pluginSource.isLinked())) {
                                    removeDataSource(pluginKey);
                                }
                            }
                        }
                    }
                }
                cleanDataSourceAfterRemoving(source);
            }
        }
    }

    /**
     * Cleans an {@link AbstractDataSource}, once removed from known sources
     * 
     * @param source The {@link AbstractDataSource}
     */
    protected void cleanDataSourceAfterRemoving(AbstractDataSource<?> source) {
        // nothing to do by default
    }

    /**
     * Returns the default {@link IRefreshingStrategy} used by this {@link AbstractRefreshingManager}
     * 
     * @return An {@link IRefreshingStrategy}
     */
    public IRefreshingStrategy getDefaultRefreshingStrategy() {
        return defaultRefreshingStrategy;
    }

    /**
     * Sets the default {@link IRefreshingStrategy} this {@link AbstractRefreshingManager} should
     * use
     * 
     * @param defaultRefreshingStrategy The {@link IRefreshingStrategy} to use
     */
    public void setDefaultRefreshingStrategy(IRefreshingStrategy defaultRefreshingStrategy) {
        this.defaultRefreshingStrategy = defaultRefreshingStrategy;
    }

    public void addRefreshingGroupListener(IRefreshingGroupListener listener) {
        if (listener != null) {
            synchronized (refreshingGroupListeners) {
                refreshingGroupListeners.add(listener);
            }
        }
    }

    public void removeRefreshingGroupListener(IRefreshingGroupListener listener) {
        if (listener != null) {
            synchronized (refreshingGroupListeners) {
                refreshingGroupListeners.remove(listener);
            }
        }
    }

    protected void notifyGroupListeners(GroupEvent event) {
        List<IRefreshingGroupListener> copy = new ArrayList<IRefreshingGroupListener>();
        synchronized (refreshingGroupListeners) {
            copy.addAll(refreshingGroupListeners);
        }
        for (IRefreshingGroupListener listener : copy) {
            try {
                listener.groupRefreshed(event);
            } catch (Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .warn(getClass().getSimpleName() + " failed to notify group listener " + listener, e);
            }
        }
        copy.clear();
    }

    @Override
    public void refreshed(RefreshEvent event) {
        if (event != null) {
            AbstractRefreshingThread<?> thread = event.getSource();
            if (thread != null) {
                String groupName = threadGroupMap.get(thread);
                if (groupName != null) {
                    notifyGroupListeners(new GroupEvent(this, groupName, getGroupMetaInformation(groupName, thread)));
                }
            }
        }
    }

    protected String getGroupMetaInformation(String groupName, AbstractRefreshingThread<?> thread) {
        return DEFAULT_META_INFORMATION;
    }

    protected <K, V extends AbstractRefreshingThread<?>> StringBuilder appendMapToBuilder(StringBuilder builder,
            Map<K, V> map, String identifier) {
        StringBuilder result = builder;
        if (result == null) {
            result = new StringBuilder();
        }
        if ((map != null) && (identifier != null)) {
            result.append("\n").append(identifier).append(":");
            for (Entry<K, V> entry : map.entrySet()) {
                result.append("\n\t").append(entry.getKey()).append(" <-> ").append(entry.getValue().getDataCopy());
            }
        }
        return result;
    }

    /**
     * Appends the refreshing maps in a {@link StringBuilder}, creating one when necessary
     * 
     * @param builder The {@link StringBuilder}. Can be <code>null</code>, in which case a new one is created
     * @return A {@link StringBuilder}, never <code>null</code>, containing the refreshing maps string representation
     */
    protected StringBuilder appendRefreshingMapsToBuilder(StringBuilder builder) {
        StringBuilder result = builder;
        result = appendMapToBuilder(result, bufferedSourcesTimeThreads, "time bufferedDataSources");
        result = appendMapToBuilder(result, bufferedSourcesGroupThreads, "group bufferedDataSources");
        return result;
    }

    /**
     * Call this method to debug your program.
     * It prints the refreshed sources in logger and console
     */
    public final void printRefreshedSources() {
        StringBuilder builder = new StringBuilder("--------------------\n");
        builder.append(getClass().getName()).append(" refreshed sources:");
        appendRefreshingMapsToBuilder(builder);
        builder.append("\n--------------------");
        String debugMessage = builder.toString();
        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).trace(debugMessage);
        PrintStream out = System.out;
        if (out != null) {
            out.println(debugMessage);
        }
    }

}
