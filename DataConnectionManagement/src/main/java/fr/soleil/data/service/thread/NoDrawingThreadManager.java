/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service.thread;

/**
 * Default implementation of {@link IDrawingThreadManager}.
 * In that case, it is considered there is no drawing thread and the {@link Runnable} runs immediately in
 * {@link #runInDrawingThread(Runnable)}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class NoDrawingThreadManager implements IDrawingThreadManager {

    public static final NoDrawingThreadManager INSTANCE = new NoDrawingThreadManager();

    private NoDrawingThreadManager() {
        // nothing to do
    }

    @Override
    public void runInDrawingThread(Runnable runnable) {
        if (runnable != null) {
            runnable.run();
        }
    }

}
