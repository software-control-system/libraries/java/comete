/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service.thread;

import java.lang.ref.WeakReference;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

import fr.soleil.lib.project.ObjectUtils;

public class AccessibleFutureTask<V> extends FutureTask<V> {

    private final WeakReference<BlockingQueue<Future<V>>> completionQueueRef;
    private final Future<V> task;

    public AccessibleFutureTask(RunnableFuture<V> task, BlockingQueue<Future<V>> completionQueue) {
        super(task, null);
        this.task = task;
        this.completionQueueRef = (completionQueue == null ? null : new WeakReference<>(completionQueue));
    }

    public Future<V> getTask() {
        return task;
    }

    @Override
    protected void done() {
        BlockingQueue<Future<V>> completionQueue = ObjectUtils.recoverObject(completionQueueRef);
        if (completionQueue != null) {
            completionQueue.add(task);
        }
    }
}
