/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Node;

import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

/**
 * This class defined a abstraction of {@link IKey} that gives some basic
 * implementations
 * 
 * @author Katy Saintin
 * @see AbstractDAOFactory
 */
public abstract class AbstractKey implements IKey {

    protected Map<String, Object> propertiesTable;

    public static final String SOURCE_PRODUCTION_PROPERTY_NAME = "sourceProduction";
    public static final String SOURCE_TYPE = "sourceType";

    /**
     * Constructs a new {@link AbstractKey}, without knowing its compatible source producer
     */
    public AbstractKey() {
        propertiesTable = new HashMap<>();
    }

    /**
     * Constructs this {@link AbstractKey}, indicating the compatible source producer
     * 
     * @param sourceProduction The {@link String} that identifies the compatible source producer
     */
    public AbstractKey(String sourceProduction) {
        this();
        if (sourceProduction != null) {
            registerProperty(SOURCE_PRODUCTION_PROPERTY_NAME, sourceProduction);
        }
    }

    @Override
    public String getSourceProduction() {
        String result = null;
        Object prop = getPropertyValue(SOURCE_PRODUCTION_PROPERTY_NAME);
        if (prop instanceof String) {
            result = (String) prop;
        }
        return result;
    }

    /**
     * Register a property in the properties map
     * 
     * @param propertyName the property's name used as key of the map
     * @param propertyValue the property's value
     */
    public void registerProperty(String propertyName, String propertyValue) {
        String valueToInsert = (propertyValue == null) ? null : propertyValue.trim();
        propertiesTable.put(propertyName, valueToInsert);
    }

    /**
     * Register a property in the properties map
     * 
     * @param propertyName the property's name used as key of the map
     * @param propertyValue the property's value
     */
    @Override
    public void registerProperty(String propertyName, Object propertyValue) {
        propertiesTable.put(propertyName, propertyValue);
    }

    /**
     * Unregister a property in the properties map The removing is synchronized
     * 
     * @param propertyName the property's name used as key of the map
     */
    @Override
    public void unregisterProperty(String propertyName) {
        if (propertiesTable.containsKey(propertyName)) {
            synchronized (propertiesTable) {
                propertiesTable.remove(propertyName);
            }
        }
    }

    /**
     * Get the list of the existing registered properties
     * 
     * @return the properties name list
     */
    @Override
    public String[] getExistingProperties() {
        Set<String> keys = getExistingKeys();
        Iterator<String> iterator = keys.iterator();
        String[] keyList = new String[keys.size()];
        int index = 0;
        while (iterator.hasNext()) {
            keyList[index] = iterator.next();
            index++;
        }
        return keyList;
    }

    /**
     * Get an enumeration of the existing registered properties
     * 
     * @return the properties name enumeration
     */
    @Override
    public Set<String> getExistingKeys() {
        return propertiesTable.keySet();
    }

    /**
     * Check if the properties map contains a property value
     * 
     * @param propertyValue the property value
     * @return true if the properties map contains the given property value else false
     */
    @Override
    public boolean containsPropertyValue(Object propertyValue) {
        return propertiesTable.containsValue(propertyValue);
    }

    /**
     * Return the first property name found for a given value
     * 
     * @param the property value
     * @return the property name corresponding to the given value, null if not found
     */
    @Override
    public String getPropertyNameForValue(Object propertyValue) {
        if (propertyValue == null) {
            return null;
        }
        if (containsPropertyValue(propertyValue)) {
            String[] keys = getExistingProperties();
            String tmpPropertyName = null;
            Object tmpPropertyValue = null;
            for (String key : keys) {
                tmpPropertyName = key;
                tmpPropertyValue = propertiesTable.get(tmpPropertyName);
                if (propertyValue.equals(tmpPropertyValue)) {
                    return tmpPropertyName;
                }
            }
        }
        return null;
    }

    /**
     * Return the property value for a given property name
     * 
     * @param the property name
     * @return the property value of a property, null if property not found
     */
    @Override
    public Object getPropertyValue(String propertyName) {
        if (propertiesTable.containsKey(propertyName)) {
            return propertiesTable.get(propertyName);
        }
        return null;
    }

    /**
     * Check if a property is registered in the properties map
     * 
     * @param the property's name
     * @return true if the property is registered else false
     */
    @Override
    public boolean containProperty(String propertyName) {
        return propertiesTable.containsKey(propertyName);
    }

    /**
     * @return true if the property map is empty else true
     */
    @Override
    public boolean hasNoProperty() {
        return propertiesTable.isEmpty();
    }

    /**
     * @return the number of property
     */
    @Override
    public int getPropertiesCount() {
        return propertiesTable.size();
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj != null) {
            if (obj == this) {
                equals = true;
            } else if (getClass().equals(obj.getClass())) {
                AbstractKey toCompare = (AbstractKey) obj;
                if (ObjectUtils.sameMapContent(propertiesTable, toCompare.propertiesTable)) {
                    equals = ObjectUtils.sameObject(getInformationKey(), toCompare.getInformationKey());
                }
            }
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 9;
        final int multiplier = 42;
        code = multiplier * code + getClass().getName().hashCode();
        code = multiplier * code + propertiesTable.hashCode();
        code = multiplier * code + getInformationKey().hashCode();
        return code;
    }

    @Override
    public AbstractKey clone() {
        AbstractKey result = null;
        try {
            result = (AbstractKey) super.clone();
            Map<String, Object> newMap = new HashMap<>();
            newMap.putAll(propertiesTable);
            result.propertiesTable = newMap;
        } catch (CloneNotSupportedException e) {
            // Will not happen because IKey extends Cloneable
        }
        return result;
    }

    /**
     * Return if the key is valid, overridden by the implementation. This validity can be used in the AbstractDAOFactory
     * for the creation of a DAO
     * 
     * @return true if the Key is valid else false
     * @see AbstractDAOFactory
     */
    @Override
    public abstract boolean isValid();

    /**
     * Return information of the key, that will be displayed in widget as a tool type That can be the concatenation of
     * the PropertyValue
     * 
     * @return information message on the key
     */
    @Override
    public abstract String getInformationKey();

    @Override
    public String toXML() {
        String result;
        XMLLine[] lines = toXMLLines(XML_TAG);
        if (lines == null) {
            result = null;
        } else {
            StringBuilder builder = new StringBuilder();
            for (XMLLine line : lines) {
                line.toAppendable(builder).append('\n');
            }
            // remove last '\n'
            if (builder.length() > 0) {
                builder.deleteCharAt(builder.length() - 1);
            }
            result = builder.toString();
        }
        return result;
    }

    @Override
    public void parseXML(String xml) throws KeyCompatibilityException {
        if (xml == null) {
            throw new KeyCompatibilityException("Can't parse null xml code");
        } else {
            try {
                parseXML(XMLUtils.getRootNodeFromFileContent(xml));
            } catch (XMLWarning e) {
                throw new KeyCompatibilityException("Uncompatible xml code", e);
            }
        }
    }

    /**
     * Creates a new opening or empty {@link XMLLine}, containing the common attributes
     * 
     * @param tag The tag that identifies the {@link XMLLine}
     * @param empty Whether {@link XMLLine} should be an empty one
     * @return An {@link XMLLine}
     */
    protected XMLLine createOpeningOrEmptyLine(String tag, boolean empty) {
        XMLLine line = new XMLLine(tag, empty ? XMLLine.EMPTY_TAG_CATEGORY : XMLLine.OPENING_TAG_CATEGORY);
        line.setAttribute(SOURCE_PRODUCTION_PROPERTY_NAME, getSourceProduction());
        line.setAttribute(KEY_TYPE_XML_TAG, getClass().getName());
        return line;
    }

    /**
     * Extracts source production from some xml properties
     * 
     * @param properties The xml properties
     * @throws KeyCompatibilityException If <code>properties</code> is <code>null</code>
     */
    protected void readSourceProduction(Map<String, String> properties) throws KeyCompatibilityException {
        if (properties == null) {
            throw new KeyCompatibilityException("Can't recover source production from null properties");
        } else {
            propertiesTable.remove(SOURCE_PRODUCTION_PROPERTY_NAME);
            String sourceProduction = properties.get(SOURCE_PRODUCTION_PROPERTY_NAME);
            if (sourceProduction != null) {
                registerProperty(SOURCE_PRODUCTION_PROPERTY_NAME, sourceProduction);
            }
        }
    }

    /**
     * Tries to recover an {@link IKey} from an XML code
     * 
     * @param xml The XML code
     * @return An {@link IKey}
     * @throws KeyCompatibilityException If a problem occurred while trying to recover the {@link IKey}
     */
    public static IKey recoverKeyFromXML(String xml) throws KeyCompatibilityException {
        if (xml == null) {
            throw new KeyCompatibilityException("Can't parse null xml code");
        } else {
            try {
                return recoverKeyFromXMLNode(XMLUtils.getRootNodeFromFileContent(xml));
            } catch (XMLWarning e) {
                throw new KeyCompatibilityException("Uncompatible xml code", e);
            }
        }
    }

    /**
     * Tries to recover an {@link IKey} from an XML {@link Node}
     * 
     * @param node The XML {@link Node}
     * @return An {@link IKey}
     * @throws KeyCompatibilityException If a problem occurred while trying to recover the {@link IKey}
     */
    public static IKey recoverKeyFromXMLNode(Node node) throws KeyCompatibilityException {
        if (node == null) {
            throw new KeyCompatibilityException("Can't parse null xml code");
        } else {
            Map<String, String> propertyMap = XMLUtils.loadAttributes(node);
            String keyType = propertyMap.get(KEY_TYPE_XML_TAG);
            if (keyType == null) {
                throw new KeyCompatibilityException("Uncompatible xml code: can't find key type");
            } else {
                try {
                    Class<?> recoveredClass = Class.forName(keyType);
                    Class<? extends IKey> keyClass = recoveredClass.asSubclass(IKey.class);
                    IKey result = keyClass.newInstance();
                    result.parseXML(node);
                    return result;
                } catch (ClassNotFoundException e) {
                    throw new KeyCompatibilityException("Can't recover key type from xml code", e);
                } catch (ClassCastException e) {
                    throw new KeyCompatibilityException("Can't recover key type from xml code", e);
                } catch (InstantiationException e) {
                    throw new KeyCompatibilityException("Can't build key from xml code", e);
                } catch (IllegalAccessException e) {
                    throw new KeyCompatibilityException("Can't build key from xml code", e);
                }
            }
        }
    }

    /**
     * Checks whether an XML {@link Node} corresponds to a given {@link IKey} {@link Class}, by checking the properties.
     * <ul>
     * <li>If the {@link Node} matches the {@link IKey}, this method returns the loaded property map.</li>
     * <li>If not, this method throws a {@link KeyCompatibilityException}</li>
     * </ul>
     * 
     * @param node The XML {@link Node}
     * @param keyClass The {@link IKey} {@link Class}
     * @return A {@link Map} : the property map.
     * @throws KeyCompatibilityException If <code>node</code> or <code>keyClass</code> is <code>null</code>, or if
     *             <code>node</code> does not match the given <code>keyClass</code>
     */
    public static Map<String, String> checkKeyCompatibility(Node node, Class<? extends IKey> keyClass)
            throws KeyCompatibilityException {
        if (node == null) {
            throw new KeyCompatibilityException("Can't parse null xml code");
        } else if (keyClass == null) {
            throw new KeyCompatibilityException("Can't compare with null key class");
        } else {
            Map<String, String> propertyMap = XMLUtils.loadAttributes(node);
            String keyType = propertyMap.get(KEY_TYPE_XML_TAG);
            if (keyType == null) {
                throw new KeyCompatibilityException("Uncompatible xml code: can't find key type");
            } else {
                try {
                    Class<?> recoveredClass = Class.forName(keyType);
                    if (!ObjectUtils.sameObject(recoveredClass, keyClass)) {
                        throw new KeyCompatibilityException(
                                "Uncompatible xml code: this code does not correspond to key class " + keyClass);
                    }
                } catch (ClassNotFoundException e) {
                    throw new KeyCompatibilityException("Can't recover key type from xml code", e);
                }
            }
            return propertyMap;
        }
    }

}
