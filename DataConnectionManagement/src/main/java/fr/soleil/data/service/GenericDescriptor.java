/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import fr.soleil.lib.project.ObjectUtils;

/**
 * This class is used to describe a class that may have generic arguments.
 * <p>
 * The idea of this class came to answer a java problem: How to recover the parameters of a class,
 * when this class comes unsigned of any generic parameter ?
 * </p>
 * <p>
 * The answer we found to that question was: by describing it. And there comes the
 * {@link GenericDescriptor}
 * </p>
 * <p>
 * This class will mainly be used by an {@link IDataSourceProducer} to guess the type of source to
 * product from an {@link IKey}
 * </p>
 * <table border="1">
 * <thead>
 * <tr>
 * <td><big>Here is an example:</big></td>
 * </tr>
 * </thead> <tbody>
 * <tr>
 * <td><code>
        <p>// numberDescriptor describes Number <br/>
        GenericDescriptor numberDescriptor = new GenericDescriptor(Number.class, null);</p>
        <p>// listOfNumberDescriptor describes List&lt;Number&gt; <br/>
        GenericDescriptor listOfNumberDescriptor = new GenericDescriptor(List.class,
                new GenericDescriptor[] { numberDescriptor });</p>
        <p>// listOfListOfNumberDescriptor describes List&lt;List&lt;Number&gt;&gt; <br/>
        GenericDescriptor listOfListOfNumberDescriptor = new GenericDescriptor(List.class,
                new GenericDescriptor[] { listOfNumberDescriptor });</p>
        <p>// stringDescriptor describes String <br/>
        GenericDescriptor stringDescriptor = new GenericDescriptor(String.class, null);</p>
        <p>// mapOfKeyStringAndNumberValueDescriptor describes Map&lt;String,Number&gt; <br/>
        GenericDescriptor mapOfKeyStringAndNumberValueDescriptor = new GenericDescriptor(Map.class,
                new GenericDescriptor[] { stringDescriptor, numberDescriptor });</p>
 * </code></td>
 * </tr>
 * </tbody>
 * </table>
 * 
 * @see IDataSourceProducer#createDataSource(IKey)
 * 
 * @author Rapha&euml;l GIRARDOT, huriez
 */
public class GenericDescriptor implements Cloneable {

    private final Class<?> concernedClass;
    private final GenericDescriptor[] parameters;

    /**
     * Constructs this {@link GenericDescriptor}
     * 
     * @param concernedClass The {@link GenericDescriptor} concerned {@link Class}
     * @param parameters The {@link Class} parameters
     */
    public GenericDescriptor(Class<?> concernedClass, GenericDescriptor... parameters) {
        this.concernedClass = concernedClass;
        this.parameters = parameters;
    }

    /**
     * Constructs this {@link GenericDescriptor} with a {@link Class} that doesn't have any
     * parameter (example: {@link java.lang.Integer Integer})
     * 
     * @param concernedClass The {@link GenericDescriptor} concerned {@link Class}
     */
    public GenericDescriptor(Class<?> concernedClass) {
        this(concernedClass, (GenericDescriptor[]) null);
    }

    /**
     * Returns the class concerned by this {@link GenericDescriptor}. Example:
     * <code>{@link java.util.Map Map}.class</code>
     * 
     * @return A {@link Class}
     */
    public Class<?> getConcernedClass() {
        return concernedClass;
    }

    /**
     * Returns the {@link GenericDescriptor}s that describe the concerned {@link Class} expected
     * parameters
     * 
     * @return A {@link GenericDescriptor array. <code>null</code> if no parameter is expected
     */
    public GenericDescriptor[] getParameters() {
        return parameters;
    }

    /**
     * Search for the primitive type of this {@link GenericDescriptor}. For a scalar, it will be the
     * concerned class, for a generic object it will be the last generic type indicate in this
     * {@link GenericDescriptor} (for example, an object Map<String, Number> will return Number)
     * 
     * @return the primitive of this {@link GenericDescriptor}
     */
    public Class<?> getPrimitiveType() {
        Class<?> result = null;

        if (concernedClass != null) {
            if (parameters == null) {
                result = concernedClass;
            } else {
                // search for the last generic type primitive
                result = parameters[parameters.length - 1].getPrimitiveType();
            }
        }

        return result;
    }

    /**
     * Allow user to know if this {@link GenericDescriptor} is scalar.
     * 
     * @return true if the {@link GenericDescriptor} do not contains parameters or type parameters
     *         for the concerned class, false otherwise
     */
    public boolean isScalar() {
        boolean result = true;
        if ((concernedClass != null) && (concernedClass.getTypeParameters() != null)) {
            result &= (concernedClass.getTypeParameters().length == 0);
        }
        if (parameters != null) {
            result &= (parameters.length == 0);
        }
        return result;
    }

    /**
     * Check if this {@link GenericDescriptor} is assignable from the {@link GenericDescriptor}
     * parameter, using the {@link Class#isAssignableFrom(Class)} method.
     * 
     * @param descriptor the {@link GenericDescriptor} to check for
     * @return true if this {@link GenericDescriptor} is assignable from the descriptor parameter
     */
    public boolean isAssignableFrom(GenericDescriptor descriptor) {
        boolean result = false;
        if (descriptor != null) {
            result = concernedClass.isAssignableFrom(descriptor.concernedClass);
            if ((descriptor.parameters != null) && (parameters != null)
                    && (parameters.length == descriptor.parameters.length)) {
                for (int i = 0; i < parameters.length; ++i) {
                    result = result
                            && ((parameters[i] == null) || parameters[i].isAssignableFrom(descriptor.parameters[i]));
                }
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object object) {
        boolean result = false;
        if (object instanceof GenericDescriptor) {
            GenericDescriptor copy = (GenericDescriptor) object;
            boolean tempResult = true;
            if ((concernedClass != null) && (copy.getConcernedClass() != null)) {
                tempResult &= concernedClass.equals(copy.getConcernedClass());
            } else {
                tempResult &= (concernedClass == null) && (copy.getConcernedClass() == null);
            }
            if ((parameters != null) && (copy.getParameters() != null)
                    && (parameters.length == copy.getParameters().length)) {
                for (int i = 0; i < parameters.length; ++i) {
                    GenericDescriptor desc = parameters[i];
                    GenericDescriptor copyDesc = copy.getParameters()[i];
                    tempResult &= ObjectUtils.sameObject(desc, copyDesc);
                }
            } else {
                tempResult &= (parameters == null) && (copy.getParameters() == null);
            }
            result = tempResult;
        }
        return result;
    }

    @Override
    public int hashCode() {
        int result = 5;
        final int multiplier = 11;
        if (concernedClass != null) {
            result = multiplier * result + (concernedClass == null ? 0 : concernedClass.hashCode());
        }
        if (parameters != null) {
            for (GenericDescriptor desc : parameters) {
                result = multiplier * result + ObjectUtils.getHashCode(desc);
            }
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(concernedClass.getSimpleName());
        if ((parameters != null) && (parameters.length > 0)) {
            builder.append("<");
            for (int i = 0; i < parameters.length; ++i) {
                GenericDescriptor desc = parameters[i];
                if (i > 0) {
                    builder.append(",");
                }
                builder.append(desc);
            }
            builder.append(">");
        }
        return builder.toString();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
