/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import java.util.Set;

import org.w3c.dom.Node;

import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * An {@link IKey} gives all necessary information to an {@link IDataSourceProducer} to produce an
 * {@link AbstractDataSource}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IKey extends Cloneable {

    public static final String XML_TAG = "iKey";
    public static final String KEY_TYPE_XML_TAG = "iKeyClass";

    /**
     * Returns A {@link String} that identifies the type of {@link IDataSourceProducer} this
     * {@link IKey} should be compatible with. This id should refer to
     * {@link IDataSourceProducer#getId()}
     * 
     * @return A {@link String}
     */
    public String getSourceProduction();

    /**
     * Register a property in the properties map
     * 
     * @param propertyName , the property's name used as key of the map
     * @param the propertyValue, the property's value
     */
    public void registerProperty(String propertyName, Object propertyValue);

    /**
     * Unregister a property in the properties map The removing is synchronized
     * 
     * @param propertyName , the property's name used as key of the map
     */
    public void unregisterProperty(String propertyName);

    /**
     * Get the list of the existing registered properties
     * 
     * @return the properties name list
     */
    public String[] getExistingProperties();

    /**
     * Get an enumeration of the existing registered properties
     * 
     * @return the properties name enumeration
     */
    public Set<String> getExistingKeys();

    /**
     * Check if the properties map contains a property value
     * 
     * @param the property value
     * @return true if the properties map contains the given property value else false
     */
    public boolean containsPropertyValue(Object propertyValue);

    /**
     * Return the first property name found for a given value
     * 
     * @param the property value
     * @return the property name corresponding to the given value, null if not found
     */
    public String getPropertyNameForValue(Object propertyValue);

    /**
     * Return the property value for a given property name
     * 
     * @param the property name
     * @return the property value of a property, null if property not found
     */
    public Object getPropertyValue(String propertyName);

    /**
     * Check if a property is registered in the properties map
     * 
     * @param the property's name
     * @return true if the property is registered else false
     */
    public boolean containProperty(String propertyName);

    /**
     * @return true if the property map is empty else true
     */
    public boolean hasNoProperty();

    /**
     * @return the number of property
     */
    public int getPropertiesCount();

    /**
     * Return if the key is valid, overridden by the implementation This validity can be used in the
     * AbstractDAOFactory for the creation of a DAO
     * 
     * @return true if the Key is valid else false
     * @see AbstractDAOFactory
     */
    public abstract boolean isValid();

    /**
     * Return information of the key, that will be displayed in widget as a tool type That can be
     * the concatenation of the PropertyValue
     * 
     * @return information message on the key
     */
    public abstract String getInformationKey();

    /**
     * @return a clone of this {@link IKey}
     */
    public IKey clone();

    /**
     * Returns an XML representation of this {@link IKey}
     * 
     * @return a {@link String}
     */
    public String toXML();

    /**
     * Returns an XML representation of this {@link IKey}, as {@link XMLLine}s identified by a tag
     * 
     * @param tag The tag that identifies the {@link XMLLine}s
     * 
     * @return a {@link XMLLine} array (generally of 2 elements: opening line, closing line)
     */
    public XMLLine[] toXMLLines(String tag);

    /**
     * Parses an XML representation of a compatible IKey to recover the corresponding properties
     * 
     * @param xml The XML {@link String}
     * @throws KeyCompatibilityException If the XML {@link String} does not correspond to the XML
     *             representation of a compatible {@link IKey}
     */
    public void parseXML(String xml) throws KeyCompatibilityException;

    /**
     * Parses an XML node of a compatible IKey to recover the corresponding properties
     * 
     * @param xmlNode The xml node
     * @throws KeyCompatibilityException If the {@link Node} does not correspond to the XML
     *             representation of a compatible {@link IKey}
     */
    public void parseXML(Node xmlNode) throws KeyCompatibilityException;

}
