/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

public class BasicKey extends AbstractKey {

    protected static final String INFORMATION_XML_TAG = "informationKey";

    private String informationKey;

    public BasicKey() {
        informationKey = null;
    }

    public BasicKey(String sourceProduction) {
        super(sourceProduction);
        informationKey = null;
    }

    public void setInformationKey(String ainformationKey) {
        informationKey = ainformationKey;
    }

    @Override
    public boolean isValid() {
        if (informationKey == null || informationKey.isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public String getInformationKey() {
        return informationKey;
    }

    @Override
    public XMLLine[] toXMLLines(String tag) {
        XMLLine openingLine = createOpeningOrEmptyLine(tag, true);
        openingLine.setAttribute(INFORMATION_XML_TAG, getInformationKey());
        return new XMLLine[] { openingLine };
    }

    @Override
    public void parseXML(Node node) throws KeyCompatibilityException {
        if (node == null) {
            throw new KeyCompatibilityException("Can't parse a null XML node");
        } else {
            Map<String, String> propertyMap = checkKeyCompatibility(node, getClass());
            propertiesTable.clear();
            readSourceProduction(propertyMap);
            informationKey = propertyMap.get(INFORMATION_XML_TAG);
            extractProperties(node, propertyMap);
        }
    }

    /**
     * Extracts properties from a {@link Node} and its attributes
     * 
     * @param node The {@link Node}
     * @param propertyMap The node attributes
     */
    protected void extractProperties(Node node, Map<String, String> propertyMap) {
        // nothing to do by default
    }

}
