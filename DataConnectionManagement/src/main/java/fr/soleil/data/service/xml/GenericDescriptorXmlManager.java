/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service.xml;

import java.util.Map;

import javax.xml.bind.DataBindingException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

/**
 * A class that handles {@link GenericDescriptor} xml conversion
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class GenericDescriptorXmlManager {

    protected static String PARAMETERS_COUNT_XML_TAG = "parametersCount";
    protected static String PARAMETER_XML_TAG = "param_";
    protected static String CLASS_XML_TAG = "concernedClass";
    public static String DESCRIPTOR_XML_TAG = "GenericDescriptor";

    /**
     * Converts a {@link GenericDescriptor} into {@link XMLLine}s, identified by a tag
     * 
     * @param tag The tag
     * @param descriptor The {@link GenericDescriptor}
     * @return An {@link XMLLine} array. <code>null</code> if <code>tag</code> or
     *         <code>descriptor</code> is <code>null</code>
     */
    public static XMLLine[] toXMLLines(String tag, GenericDescriptor descriptor) {
        XMLLine[] result;
        if ((tag == null) || (descriptor == null)) {
            result = null;
        } else {
            XMLLine openingLine = new XMLLine(tag, XMLLine.OPENING_TAG_CATEGORY);
            openingLine.setAttribute(CLASS_XML_TAG,
                    descriptor.getConcernedClass() == null ? "null" : descriptor.getConcernedClass().getName());
            GenericDescriptor[] parameters = descriptor.getParameters();
            int count = (parameters == null ? 0 : parameters.length);
            openingLine.setAttribute(PARAMETERS_COUNT_XML_TAG, Integer.toString(count));
            XMLLine[][] parameterLines = new XMLLine[count][];
            if (count > 0) {
                for (int i = 0; i < count; i++) {
                    parameterLines[i] = toXMLLines(PARAMETER_XML_TAG + i, parameters[i]);
                }
            }
            count = 2;
            for (XMLLine[] line : parameterLines) {
                if (line != null) {
                    count += line.length;
                }
            }
            result = new XMLLine[count];
            result[0] = openingLine;
            result[result.length - 1] = new XMLLine(tag, XMLLine.CLOSING_TAG_CATEGORY);
            int index = 1;
            for (XMLLine[] line : parameterLines) {
                if (line != null) {
                    System.arraycopy(line, 0, result, index, line.length);
                    index += line.length;
                }
            }
        }
        return result;
    }

    /**
     * Converts a {@link GenericDescriptor} to an xml {@link String}
     * 
     * @param descriptor The {@link GenericDescriptor}
     * @return A {@link String}
     */
    public static String toXmlString(GenericDescriptor descriptor) {
        StringBuilder xmlBuilder = new StringBuilder();
        XMLLine[] lines = toXMLLines(DESCRIPTOR_XML_TAG, descriptor);
        if (lines != null) {
            for (XMLLine line : lines) {
                if (line != null) {
                    line.toAppendable(xmlBuilder).append('\n');
                }
            }
        }
        // delete last '\n'
        if (xmlBuilder.length() > 0) {
            xmlBuilder.deleteCharAt(xmlBuilder.length() - 1);
        }
        return xmlBuilder.toString();
    }

    /**
     * Parses an XML {@link Node} to recover its corresponding {@link GenericDescriptor}
     * 
     * @param node The XML {@link Node}
     * @return A {@link GenericDescriptor}
     * @throws DataAdaptationException If a problem occurred while trying to recover the
     *             {@link GenericDescriptor}
     */
    public static GenericDescriptor parseXML(Node node) throws DataAdaptationException {
        if (node == null) {
            throw new DataAdaptationException("Can't parse a null xml Node");
        } else {
            Map<String, String> propertyMap = XMLUtils.loadAttributes(node);
            String className = propertyMap.get(CLASS_XML_TAG);
            if (className == null) {
                throw new DataAdaptationException("Can't recover concerned class: null class name");
            } else {
                try {
                    Class<?> concernedClass = Class.forName(className);
                    String paramCountToString = propertyMap.get(PARAMETERS_COUNT_XML_TAG);
                    if (paramCountToString == null) {
                        throw new DataAdaptationException("Can't recover parameters: null count");
                    } else {
                        int count = Integer.parseInt(paramCountToString);
                        GenericDescriptor[] parameters;
                        if (count > 0) {
                            parameters = new GenericDescriptor[count];
                            // fill parameters
                            if (node.hasChildNodes()) {
                                NodeList subNodes = node.getChildNodes();
                                for (int i = 0; i < subNodes.getLength(); i++) {
                                    Node subNode = subNodes.item(i);
                                    if (!XMLUtils.isAFakeNode(subNode)) {
                                        String subNodeType = subNode.getNodeName().trim();
                                        if (subNodeType.startsWith(PARAMETER_XML_TAG)) {
                                            int index = Integer
                                                    .parseInt(subNodeType.substring(PARAMETER_XML_TAG.length()));
                                            // May throw an ArrayIndexOutOfBoundsException
                                            parameters[index] = parseXML(subNode);
                                        }
                                    }
                                }
                            }
                        } else {
                            parameters = null;
                        }
                        return new GenericDescriptor(concernedClass, parameters);
                    }
                } catch (ClassNotFoundException e) {
                    throw new DataAdaptationException("Can't recover concerned class", e);
                } catch (NumberFormatException e) {
                    throw new DataAdaptationException("Can't recover parameters", e);
                } catch (ArrayIndexOutOfBoundsException e) {
                    throw new DataAdaptationException("Can't recover parameters", e);
                }
            }
        }
    }

    /**
     * Parses an XML {@link String} to recover its corresponding {@link GenericDescriptor}
     * 
     * @param xml The XML {@link String}
     * @return A {@link GenericDescriptor}
     * @throws DataAdaptationException If a problem occurred while trying to recover the
     *             {@link GenericDescriptor}
     */
    public static GenericDescriptor parseXML(String xml) throws DataAdaptationException {
        if (xml == null) {
            throw new DataAdaptationException("Can't parse a null xml String");
        } else {
            try {
                return parseXML(XMLUtils.getRootNodeFromFileContent(xml));
            } catch (XMLWarning e) {
                throw new DataBindingException("Failed to parse xml String", e);
            }

        }
    }

}
