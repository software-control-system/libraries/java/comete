/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

/**
 * A class to define a common base to use events to refresh your data (i.e.: you don't poll your
 * data. Each time your data changes, some notification is supposed to be done)
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class EventRefreshingStrategy implements IRefreshingStrategy {

    private static final long serialVersionUID = -5631907460980403398L;

    /**
     * Default Constructor
     */
    public EventRefreshingStrategy() {
        super();
    }

}
