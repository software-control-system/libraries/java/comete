/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.IDrawableTarget;
import fr.soleil.data.target.IUnitTarget;

/**
 * A class that helps setting unit to a {@link IUnitTarget}, taking account of the case where that {@link IUnitTarget}
 * might be an {@link IDrawableTarget}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class UnitHelper {

    /**
     * Applies a unit to an {@link IUnitTarget}.
     * 
     * @param target The {@link IUnitTarget}.
     * @param unit The unit.
     */
    public static void setUnitToTarget(IUnitTarget target, String unit) {
        if (target instanceof IDrawableTarget) {
            IDrawingThreadManager manager = ((IDrawableTarget) target).getDrawingThreadManager();
            if (manager == null) {
                target.setUnit(unit);
            } else {
                manager.runInDrawingThread(() -> {
                    target.setUnit(unit);
                });
            }
        } else {
            target.setUnit(unit);
        }
    }

}
