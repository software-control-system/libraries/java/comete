/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import java.util.Arrays;
import java.util.logging.Level;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.data.mediator.Mediator;

public class LoggingSystemDelegate implements ILoggingSystem {

    private String[] loggerIds;
    private final Object loggerLock;

    public LoggingSystemDelegate() {
        loggerLock = new Object();
        loggerIds = new String[] { Mediator.LOGGER_ACCESS };
    }

    /**
     * Logs a message in a {@link Logger}, with potential associated Throwable information, according to a
     * {@link Level}.
     * <p>
     * This method mainly helps migrating from <code>java.util.logging</code> to <code>slf4j</code>.
     * </p>
     * 
     * @param logger The {@link Logger}
     * @param level The {@link Level}
     * @param msg The message
     * @param thrown The {@link Throwable}. Can be <code>null</code>.
     */
    public static void log(Logger logger, Level level, String msg, Throwable thrown) {
        if ((logger != null) && (level != null)) {
            switch (level.intValue()) {
                case 1000: // SEVERE
                    if (thrown == null) {
                        logger.error(msg);
                    } else {
                        logger.error(msg, thrown);
                    }
                    break;
                case 900: // WARNING
                    if (thrown == null) {
                        logger.warn(msg);
                    } else {
                        logger.warn(msg, thrown);
                    }
                    break;
                case 700: // CONFIG
                    if (thrown == null) {
                        logger.debug(msg);
                    } else {
                        logger.debug(msg, thrown);
                    }
                    break;
                case 500: // FINE
                case 400: // FINER
                case 300: // FINEST
                    if (thrown == null) {
                        logger.trace(msg);
                    } else {
                        logger.trace(msg, thrown);
                    }
                    break;
                case 800: // INFO
                default:
                    if (thrown == null) {
                        logger.info(msg);
                    } else {
                        logger.info(msg, thrown);
                    }
                    break;
            }
        }
    }

    /**
     * Logs a message, with associated Throwable information.
     * 
     * @param level Message level identifier
     * @param msg The {@link String} message
     * @param thrown {@link Throwable} associated with log message
     */
    public void log(Level level, String msg, Throwable thrown) {
        String[] loggers = loggerIds;
        if (level != null) {
            switch (level.intValue()) {
                case 1000: // SEVERE
                    for (String logger : loggers) {
                        LoggerFactory.getLogger(logger).error(msg, thrown);
                    }
                    break;
                case 900: // WARNING
                    for (String logger : loggers) {
                        LoggerFactory.getLogger(logger).warn(msg, thrown);
                    }
                    break;
                case 700: // CONFIG
                    for (String logger : loggers) {
                        LoggerFactory.getLogger(logger).debug(msg, thrown);
                    }
                    break;
                case 500: // FINE
                case 400: // FINER
                case 300: // FINEST
                    for (String logger : loggers) {
                        LoggerFactory.getLogger(logger).trace(msg, thrown);
                    }
                    break;
                case 800: // INFO
                default:
                    for (String logger : loggers) {
                        LoggerFactory.getLogger(logger).info(msg, thrown);
                    }
                    break;
            }
        }
    }

    /**
     * Logs a message, with no arguments.
     * 
     * @param level Message level identifier
     * @param msg The {@link String} message
     */
    public void log(Level level, String msg) {
        String[] loggers = loggerIds;
        if (level != null) {
            switch (level.intValue()) {
                case 1000: // SEVERE
                    for (String logger : loggers) {
                        LoggerFactory.getLogger(logger).error(msg);
                    }
                    break;
                case 900: // WARNING
                    for (String logger : loggers) {
                        LoggerFactory.getLogger(logger).warn(msg);
                    }
                    break;
                case 700: // CONFIG
                    for (String logger : loggers) {
                        LoggerFactory.getLogger(logger).debug(msg);
                    }
                    break;
                case 500: // FINE
                case 400: // FINER
                case 300: // FINEST
                    for (String logger : loggers) {
                        LoggerFactory.getLogger(logger).trace(msg);
                    }
                    break;
                case 800: // INFO
                default:
                    for (String logger : loggers) {
                        LoggerFactory.getLogger(logger).info(msg);
                    }
                    break;
            }
        }
    }

    /**
     * Returns whether a {@link Logger} id is considered as valid
     * 
     * @param loggerId The {@link Logger} id
     * @return A <code>boolean</code>
     */
    protected final boolean isValidLogger(String loggerId) {
        return ((loggerId != null) && (!loggerId.trim().isEmpty()));
    }

    @Override
    public void addLogger(String loggerId) {
        if (isValidLogger(loggerId)) {
            synchronized (loggerLock) {
                boolean canAdd = true;
                for (String logger : loggerIds) {
                    if (logger.equals(loggerId)) {
                        canAdd = false;
                        break;
                    }
                }
                if (canAdd) {
                    String[] loggers = Arrays.copyOf(loggerIds, loggerIds.length + 1);
                    loggers[loggerIds.length] = loggerId;
                    loggerIds = loggers;
                }
            }
        }
    }

    @Override
    public void removeLogger(String loggerId) {
        if (isValidLogger(loggerId)) {
            synchronized (loggerLock) {
                int index = -1;
                for (int i = 0; i < loggerIds.length; i++) {
                    if (loggerIds[i].equals(loggerId)) {
                        index = i;
                        break;
                    }
                }
                if (index > -1) {
                    String[] loggers = new String[loggerIds.length - 1];
                    if (index > 0) {
                        System.arraycopy(loggerIds, 0, loggers, 0, index);
                    }
                    if (index < loggerIds.length - 1) {
                        System.arraycopy(loggerIds, index + 1, loggers, index, loggerIds.length - (index + 1));
                    }
                    loggerIds = loggers;
                }
            }
        }
    }

}
