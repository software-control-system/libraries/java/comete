/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import org.slf4j.Logger;

/**
 * Interface for things that are able to log, and for which you can register and unregister
 * {@link Logger}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ILoggingSystem {
    /**
     * Adds a {@link Logger} id in the list of {@link Logger} ids to warn when logging information.
     * 
     * @param loggerId The {@link Logger} id
     * 
     */
    public void addLogger(String loggerId);

    /**
     * Removes a {@link Logger} id from the list of {@link Logger} ids to warn when logging
     * information.
     * 
     * @param loggerId The {@link Logger} id
     */
    public void removeLogger(String loggerId);

}
