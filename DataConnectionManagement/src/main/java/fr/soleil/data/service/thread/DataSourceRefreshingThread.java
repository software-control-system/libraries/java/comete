/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service.thread;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.soleil.data.source.BufferedDataSource;

/**
 * An {@link AbstractRefreshingThread} that manages {@link BufferedDataSource}s refreshing
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DataSourceRefreshingThread extends AbstractRefreshingThread<BufferedDataSource<?>> {
    protected final List<BufferedDataSource<?>> sources = new ArrayList<BufferedDataSource<?>>();

    public DataSourceRefreshingThread() {
        super("DataSourceRefreshingThread");
    }

    public DataSourceRefreshingThread(String name) {
        super(name);
    }

    public DataSourceRefreshingThread(ThreadGroup group, String name) {
        super(group, name);
    }

    @Override
    protected void refreshData() throws Exception {
        List<BufferedDataSource<?>> copy = new ArrayList<BufferedDataSource<?>>();
        synchronized (sources) {
            copy.addAll(sources);
        }
        if (!stopped) {
            for (BufferedDataSource<?> dataSource : copy) {
                if (stopped) {
                    break;
                }
                dataSource.updateData();
            }
        }
    }

    @Override
    public void addData(BufferedDataSource<?> source) {
        synchronized (sources) {
            if (!sources.contains(source) && !stopped) {
                sources.add(source);
            }
        }
    }

    @Override
    public void removeData(BufferedDataSource<?> source) {
        synchronized (sources) {
            sources.remove(source);
        }
    }

    @Override
    public boolean isEmpty() {
        boolean empty = false;
        synchronized (sources) {
            empty = sources.isEmpty();
        }
        return empty;
    }

    @Override
    public Collection<BufferedDataSource<?>> getDataCopy() {
        List<BufferedDataSource<?>> copy = new ArrayList<BufferedDataSource<?>>();
        synchronized (sources) {
            copy.addAll(sources);
        }
        return copy;
    }

}
