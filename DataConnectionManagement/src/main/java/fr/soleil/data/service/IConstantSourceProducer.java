/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import fr.soleil.data.exception.UnhandledDataTypeException;
import fr.soleil.data.source.AbstractDataSource;

public interface IConstantSourceProducer extends IDataSourceProducer {

    /**
     * Creates an {@link AbstractDataSource} that manages a particular type of data
     * 
     * @param key The {@link IKey} that contains the necessary information to identify the way to
     *            create the {@link AbstractDataSource}
     * @param forceCreation Force source creation even if this source is already cached
     * @return An {@link AbstractDataSource}, or <code>null</null> if there was no way creating it.
     */
    public AbstractDataSource<?> createDataSource(IKey key, boolean forceCreation) throws UnhandledDataTypeException;

    /**
     * Returns the previously created {@link AbstractDataSource}
     * 
     * @param key The {@link IKey} that identifies the {@link AbstractDataSource}
     * @return An {@link AbstractDataSource}. <code>null</code> If there is no such
     *         {@link AbstractDataSource} yet
     */
    public AbstractDataSource<?> getExistingSource(IKey key);

    /**
     * Removes all references to a given {@link AbstractDataSource}
     * 
     * @param key The {@link IKey} that identifies the {@link AbstractDataSource} to remove
     */
    public void removeDataSource(IKey key);

}
