/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.target.ITarget;

public class DataControllerProvider {

    private static final HashMap<GenericDescriptor, HashMap<GenericDescriptor, AbstractController<?>>> controllerMap = new HashMap<GenericDescriptor, HashMap<GenericDescriptor, AbstractController<?>>>();
    public static int countController = 0;

    /**
     * Return the controller corresponding to the source type and target type sended as parameters.
     * If this controller is not registered, it return <code>null</code>.</br>
     * This method use
     * scalar type for easier use of the "get" method. It is recommend to use the
     * {@link DataControllerProvider#getController(GenericDescriptor, GenericDescriptor)} for more
     * complex data types.
     *
     * @param <U> the source type
     * @param sourceType the {@link Class} object of the source data type
     * @param targetType the {@link Class} object of the target data type
     * @return the corresponding controller if founded, <code>null</code> otherwise
     */
    public static <U> AbstractController<U> getController(Class<?> sourceType, Class<?> targetType) {
        return getController(new GenericDescriptor(sourceType), new GenericDescriptor(targetType));
    }

    /**
     * Return the controller corresponding to the source type and target type sended as parameters.
     * If this controller is not registered, it return <code>null</code>.</br>
     * This method use
     * complete type with the {@link GenericDescriptor} object. It allow therefore the description
     * for a {@link Map} object or a {@link List} object, for example. For scalar type, it is easier
     * to use the method {@link DataControllerProvider#getController(Class, Class)}
     *
     * @param <U> the source type
     * @param sourceType the {@link GenericDescriptor} which describe the source data type
     * @param targetType the {@link GenericDescriptor} which describe the target data type
     * @return the corresponding controller if founded, <code>null</code> otherwise
     */
    @SuppressWarnings("unchecked")
    public static <U> AbstractController<U> getController(GenericDescriptor sourceType, GenericDescriptor targetType) {
        AbstractController<U> result = null;
        if (targetType != null && sourceType != null) {
            synchronized (controllerMap) {
                HashMap<GenericDescriptor, AbstractController<?>> subMap = controllerMap.get(sourceType);
                if (subMap != null) {
                    result = (AbstractController<U>) subMap.get(targetType);
                }
            }
        }
        return result;
    }

    /**
     * Push a new controller into the controller map. This method use scalar type for easier use of
     * the "push" method. It is recommend to use the
     * {@link DataControllerProvider#pushNewController(AbstractController, GenericDescriptor, GenericDescriptor)}
     * for more complex data types.
     *
     * @param <U> the source type
     * @param controller the controller to push
     * @param sourceType the {@link Class} object of the source data type
     * @param targetType the {@link Class} object of the target data type
     */
    public static <U> void pushNewController(AbstractController<U> controller, Class<?> sourceType,
            Class<?> targetType) {
        pushNewController(controller, new GenericDescriptor(sourceType), new GenericDescriptor(targetType));
    }

    /**
     * Push a new controller into the controller map. This method use complete type with the
     * {@link GenericDescriptor} object. It allow therefore the description for a {@link Map} object
     * or a {@link List} object, for example. For scalar type, it is easier to use the method
     * {@link DataControllerProvider#pushNewController(AbstractController, Class, Class)}
     *
     * @param <U> the source type
     * @param controller the controller to push
     * @param sourceType the {@link GenericDescriptor} which describe the source data type
     * @param targetType the {@link GenericDescriptor} which describe the target data type
     */
    public static <U> void pushNewController(AbstractController<U> controller, GenericDescriptor sourceType,
            GenericDescriptor targetType) {
        if (controller != null && sourceType != null && targetType != null) {
            synchronized (controllerMap) {
                HashMap<GenericDescriptor, AbstractController<?>> subMap = controllerMap.get(sourceType);
                if (subMap == null) {
                    subMap = new HashMap<GenericDescriptor, AbstractController<?>>();
                    controllerMap.put(sourceType, subMap);
                }
                if (subMap.get(targetType) == null) {
                    subMap.put(targetType, controller);
                    countController++;
                }
            }
        }
    }

    /**
     * @return a {@link Set} of all registered controller
     */
    public static Set<AbstractController<?>> getAllRegisteredController() {
        Set<AbstractController<?>> result = new HashSet<AbstractController<?>>();
        synchronized (controllerMap) {
            for (HashMap<GenericDescriptor, AbstractController<?>> subMap : controllerMap.values()) {
                result.addAll(subMap.values());
            }
        }
        return result;
    }

    /**
     * Search for all controller containing the {@link ITarget} parameter and return a {@link Set}
     * of them.
     *
     * @param target the {@link ITarget} you want each {@link AbstractController} in the {@link Set}
     *            contains.
     * @return a {@link Set} of all controller which contains the {@link ITarget} parameter
     */
    public static Set<AbstractController<?>> getAllAssociatedController(ITarget target) {
        Set<AbstractController<?>> result = new HashSet<AbstractController<?>>();
        for (AbstractController<?> tempController : getAllRegisteredController()) {
            if (tempController.contains(target)) {
                result.add(tempController);
            }
        }
        return result;
    }

}
