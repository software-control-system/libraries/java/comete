/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service.thread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;

import fr.soleil.data.thread.AutoSubmitThreadPoolExecutor;

/**
 * A {@link CompletionService} that uses a supplied {@link AutoSubmitThreadPoolExecutor} to execute tasks. This class
 * arranges that submitted tasks are, upon completion, placed on a queue accessible using {@link #take()}.
 */
public class TaskManagerCompletionService<V> implements CompletionService<V> {

    private final Executor executor;
    private final AutoSubmitThreadPoolExecutor aes;
    private final BlockingQueue<Future<V>> completionQueue;

    /**
     * Creates a TaskManagerCompletionService using the supplied executor for base task execution and a
     * {@link LinkedBlockingQueue} as a completion queue.
     *
     * @param executor the executor to use
     * @throws NullPointerException if executor is <tt>null</tt>
     */
    public TaskManagerCompletionService(Executor executor) {
        this(executor, new LinkedBlockingQueue<Future<V>>());
//        this(executor, new PriorityBlockingQueue<Future<V>>());
    }

    /**
     * Creates a TaskManagerCompletionService using the supplied executor for base task execution and the supplied queue
     * as its completion queue.
     *
     * @param executor the executor to use
     * @param completionQueue the queue to use as the completion queue normally one dedicated for use by this service
     * @throws NullPointerException if executor or completionQueue are <code>null</code>
     */
    public TaskManagerCompletionService(Executor executor, BlockingQueue<Future<V>> completionQueue) {
        if ((executor == null) || (completionQueue == null)) {
            throw new NullPointerException();
        }
        this.executor = executor;
        this.aes = (executor instanceof AutoSubmitThreadPoolExecutor) ? (AutoSubmitThreadPoolExecutor) executor : null;
        this.completionQueue = completionQueue;
    }

    protected RunnableFuture<V> newTaskFor(Callable<V> task) {
        RunnableFuture<V> future;
        if (aes == null) {
            future = new FutureTask<V>(task);
        } else {
            future = aes.newTaskFor(task);
        }
        return future;
    }

    protected RunnableFuture<V> newTaskFor(Runnable task, V result) {
        RunnableFuture<V> future;
        if (aes == null) {
            future = new FutureTask<V>(task, result);
        } else {
            future = aes.newTaskFor(task, result);
        }
        return future;
    }

    @Override
    public Future<V> submit(Callable<V> task) {
        if (task == null)
            throw new NullPointerException();
        RunnableFuture<V> f = newTaskFor(task);
        executor.execute(new AccessibleFutureTask<V>(f, completionQueue));
        return f;
    }

    public Future<V> submit(Callable<V> task, int priority) {
        if (task == null)
            throw new NullPointerException();
        RunnableFuture<V> f = newTaskFor(task);
        executor.execute(new AccessibleFutureTask<V>(f, completionQueue));
        return f;
    }

    @Override
    public Future<V> submit(Runnable task, V result) {
        if (task == null) {
            throw new NullPointerException();
        }
        RunnableFuture<V> f = newTaskFor(task, result);
        executor.execute(new AccessibleFutureTask<V>(f, completionQueue));
        return f;
    }

    @Override
    public Future<V> take() throws InterruptedException {
        return completionQueue.take();
    }

    @Override
    public Future<V> poll() {
        return completionQueue.poll();
    }

    @Override
    public Future<V> poll(long timeout, TimeUnit unit) throws InterruptedException {
        return completionQueue.poll(timeout, unit);
    }

}
