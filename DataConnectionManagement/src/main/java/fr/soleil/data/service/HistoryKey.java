/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.data.source.HistoryDataSourceProducer;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

/**
 * An {@link AbstractKey} used to create an history source
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class HistoryKey extends AbstractKey {

    public static final String HISTORY_KEY = "history";
    public static final String SUFFIX = " history";
    protected static final String WITH_SUFFIX = "withSuffix";

    private static final String CAN_T_PARSE_A_NULL_NODE = "Can't parse a null Node";

    static {
        DataSourceProducerProvider.pushNewProducer(HistoryDataSourceProducer.class);
    }

    /**
     * Constructs a new {@link HistoryKey}.
     * This empty constructor is mandatory for XML recovery.
     */
    public HistoryKey() {
        this(null);
    }

    /**
     * Constructs a new {@link HistoryKey}
     * 
     * @param history The {@link IKey} that identifies the source to historize
     */
    public HistoryKey(IKey history) {
        super(HistoryDataSourceProducer.ID);
        if (history != null) {
            registerProperty(HISTORY_KEY, history);
        }
        registerProperty(WITH_SUFFIX, Boolean.TRUE);
    }

    /**
     * Returns whether {@link #SUFFIX} is used in information key.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isUseSuffix() {
        Boolean value;
        try {
            value = (Boolean) getPropertyValue(WITH_SUFFIX);
            if (value == null) {
                value = Boolean.TRUE;
            }
        } catch (Exception e) {
            value = Boolean.TRUE;
        }
        return value.booleanValue();
    }

    /**
     * Sets whether {@link #SUFFIX} can be used in information key.
     * 
     * @param useSuffix Whether {@link #SUFFIX} can be used in information key.
     */
    public void setUseSuffix(boolean useSuffix) {
        registerProperty(WITH_SUFFIX, Boolean.valueOf(useSuffix));
    }

    /**
     * Returns the {@link IKey} that identifies the source to historize
     * 
     * @return An {@link IKey}
     */
    public IKey getHistory() {
        return (IKey) getPropertyValue(HISTORY_KEY);
    }

    @Override
    public boolean isValid() {
        return (getHistory() != null);
    }

    @Override
    public String getInformationKey() {
        IKey historyKey = getHistory();
        StringBuilder builder = new StringBuilder();
        if (historyKey == null) {
            builder.append(HISTORY_KEY);
        } else {
            builder.append(historyKey.getInformationKey());
            if (isUseSuffix()) {
                builder.append(SUFFIX);
            }
        }
        return builder.toString();
    }

    @Override
    protected XMLLine createOpeningOrEmptyLine(String tag, boolean empty) {
        XMLLine line = super.createOpeningOrEmptyLine(tag, empty);
        line.setAttribute(WITH_SUFFIX, Boolean.toString(isUseSuffix()));
        return line;
    }

    @Override
    public XMLLine[] toXMLLines(String tag) {
        XMLLine openingLine = createOpeningOrEmptyLine(tag, false);
        XMLLine closingLine = new XMLLine(tag, XMLLine.CLOSING_TAG_CATEGORY);
        IKey history = getHistory();
        XMLLine[] historyLines = (history == null ? null : history.toXMLLines(HISTORY_KEY));
        int count = 2;
        if (historyLines != null) {
            count += historyLines.length;
        }
        XMLLine[] result = new XMLLine[count];
        result[0] = openingLine;
        result[result.length - 1] = closingLine;
        if (historyLines != null) {
            System.arraycopy(historyLines, 0, result, 1, historyLines.length);
        }
        return result;
    }

    @Override
    public void parseXML(Node node) throws KeyCompatibilityException {
        if (node == null) {
            throw new KeyCompatibilityException(CAN_T_PARSE_A_NULL_NODE);
        } else {
            Map<String, String> attributes = checkKeyCompatibility(node, getClass());
            propertiesTable.clear();
            boolean useSuffix;
            try {
                useSuffix = Boolean.parseBoolean(attributes.get(WITH_SUFFIX));
            } catch (Exception e) {
                useSuffix = true;
            }
            setUseSuffix(useSuffix);
            registerProperty(SOURCE_PRODUCTION_PROPERTY_NAME, HistoryDataSourceProducer.ID);
            attributes.clear();
            if (node.hasChildNodes()) {
                NodeList subNodes = node.getChildNodes();
                for (int i = 0; i < subNodes.getLength(); i++) {
                    Node currentSubNode = subNodes.item(i);
                    if (!XMLUtils.isAFakeNode(currentSubNode)) {
                        String currentSubNodeType = currentSubNode.getNodeName().trim();
                        if (HISTORY_KEY.equals(currentSubNodeType)) {
                            registerProperty(HISTORY_KEY, recoverKeyFromXMLNode(currentSubNode));
                        }
                    }
                }
            }
        }
    }

}
