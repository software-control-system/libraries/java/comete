/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

/**
 * A class to define a common base for Polled refreshing strategies, which means you wan to
 * regularly check your data to update it.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class PolledRefreshingStrategy implements IRefreshingStrategy {

    private static final long serialVersionUID = 3271824594239905417L;

    private int refreshingPeriod;

    /**
     * Constructs this {@link PolledRefreshingStrategy}
     * 
     * @param refreshingPeriod The refreshing period to use
     */
    public PolledRefreshingStrategy(int refreshingPeriod) {
        super();
        this.refreshingPeriod = refreshingPeriod;
    }

    /**
     * Returns the time to wait, in milliseconds, between 2 data checking.
     * 
     * @return An <code>int</code>
     */
    public int getRefreshingPeriod() {
        return refreshingPeriod;
    }

}
