/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

/**
 * A {@link PolledRefreshingStrategy} used to group data to refresh by group name
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class GroupRefreshingStrategy extends PolledRefreshingStrategy {

    private static final long serialVersionUID = -6264194315104483350L;

    protected final String name;

    public GroupRefreshingStrategy(String name, int refreshingPeriod) {
        super(refreshingPeriod);
        this.name = name;
    }

    /**
     * Returns the group name
     * 
     * @return A {@link String}
     */
    public String getName() {
        return name;
    }

}
