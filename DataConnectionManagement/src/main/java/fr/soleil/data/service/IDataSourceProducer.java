/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import fr.soleil.data.source.AbstractDataSource;

/**
 * An {@link AbstractDataSource} producer
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IDataSourceProducer {

    /**
     * Creates an {@link AbstractDataSource} that manages a particular type of data
     * 
     * @param key The {@link IKey} that contains the necessary information to identify the way to
     *            create the {@link AbstractDataSource}
     * @return An {@link AbstractDataSource}, or <code>null</null> if there was no way creating it.
     */
    public AbstractDataSource<?> createDataSource(IKey key);

    /**
     * Returns the id used to recognize this producer. This is the id you may want to use for
     * {@link IKey#getSourceProduction()}. 2 different {@link IDataSourceProducer} may share the
     * same id if they are fully compatible in {@link IKey} treatment (for example: an
     * {@link IDataSourceProducer} and its corresponding {@link AbstractRefreshingManager})
     * 
     * @return A {@link String}
     */
    public String getId();

    /**
     * Returns a if a source created from IKey is settable or not
     * 
     * @return true if the source is settable
     * 
     */
    public boolean isSourceSettable(IKey key);

    /**
     * Returns a if a source is creatable from IKey
     * 
     * @return true if the source is creatable
     * 
     */
    public boolean isSourceCreatable(IKey key);

    /**
     * Returns an explicit name for the DataSourceProducer
     * 
     * @return the name of the DataSourceProducer
     */
    public String getName();

    /**
     * Returns a compatible Key with the DataSourceProducer
     * 
     * @return the new compatible Key
     */
    public IKey parseKey(IKey key);

    /**
     * Returns the rank of the data returned by the source that may be produced through a given
     * {@link IKey}
     * 
     * @param key The {@link IKey}
     * @return An <code>int</code>. 0 for scalar, 1 for spectrum, 2 for matrix, etc... . -1 if the
     *         rank can't be calculated (for example: no associated source, dynamic rank, etc...)
     */
    public int getRank(IKey key);

    /**
     * Returns the dimensions of the data returned by the source that may be produced through a
     * given {@link IKey}
     * 
     * @param key The {@link IKey}
     * @return An <code>int[]</code>. <code>null</code> if the dimensions can't be calculated (for
     *         example: no associated source, dynamic dimensions, etc...)
     */
    public int[] getShape(IKey key);

}
