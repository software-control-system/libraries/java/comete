/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.data.source.MultiDataSourceProducer;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

public class CompositeKey extends AbstractKey {

    public static final int X_INDEX = 0;
    public static final int Y_INDEX = 1;
    public static final int Z_INDEX = 3;

    public static final String X_INDEX_XML_TAG = "X_INDEX";
    public static final String Y_INDEX_XML_TAG = "Y_INDEX";
    public static final String Z_INDEX_XML_TAG = "Z_INDEX";

    protected List<IKey> keys;

    public CompositeKey() {
        super(MultiDataSourceProducer.ID);
        keys = new ArrayList<IKey>();
    }

    public void addKey(int index, IKey key) {
        keys.add(index, key);
    }

    public void removeKey(int index, IKey key) {
        keys.remove(key);
    }

    public IKey getKey(int index) {
        return keys.get(index);
    }

    @Override
    public boolean isValid() {
        boolean result = true;
        for (IKey key : keys) {
            result = result && key.isValid();
        }
        return result;
    }

    @Override
    public String getInformationKey() {
        StringBuilder resultBuffer = new StringBuilder();
        boolean first = true;
        for (IKey key : keys) {
            if (!first) {
                resultBuffer.append(" VS ");
            }
            resultBuffer.append(key.getInformationKey());
            first = false;
        }
        return resultBuffer.toString();
    }

    public List<IKey> getKeys() {
        return keys;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = super.equals(obj);
        if (obj != null) {
            if (getClass().equals(obj.getClass())) {
                CompositeKey toCompare = (CompositeKey) obj;
                result = keys.equals(toCompare.keys);
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        int code = super.hashCode();
        final int multiplier = 0x517E;
        code = multiplier * code + getClass().getName().hashCode();
        code = multiplier * code + keys.hashCode();
        return code;
    }

    @Override
    public CompositeKey clone() {
        CompositeKey clone = (CompositeKey) super.clone();
        List<IKey> newKeys = new ArrayList<IKey>();
        newKeys.addAll(clone.keys);
        clone.keys = newKeys;
        return clone;
    }

    @Override
    public XMLLine[] toXMLLines(String tag) {
        XMLLine openingLine = createOpeningOrEmptyLine(tag, false);
        XMLLine closingLine = new XMLLine(tag, XMLLine.CLOSING_TAG_CATEGORY);
        XMLLine[] xLines = getXMLLines(X_INDEX);
        XMLLine[] yLines = getXMLLines(Y_INDEX);
        XMLLine[] zLines = getXMLLines(Z_INDEX);
        int count = 2;
        if (xLines != null) {
            count += xLines.length;
        }
        if (yLines != null) {
            count += yLines.length;
        }
        if (zLines != null) {
            count += zLines.length;
        }
        XMLLine[] result = new XMLLine[count];
        result[0] = openingLine;
        result[result.length - 1] = closingLine;
        int index = 1;
        if (xLines != null) {
            System.arraycopy(xLines, 0, result, index, xLines.length);
            index += xLines.length;
        }
        if (yLines != null) {
            System.arraycopy(yLines, 0, result, index, yLines.length);
            index += yLines.length;
        }
        if (zLines != null) {
            System.arraycopy(zLines, 0, result, index, zLines.length);
        }
        return result;
    }

    protected XMLLine[] getXMLLines(int index) {
        XMLLine[] result;
        String tag;
        switch (index) {
            case X_INDEX:
                tag = X_INDEX_XML_TAG;
                break;
            case Y_INDEX:
                tag = Y_INDEX_XML_TAG;
                break;
            case Z_INDEX:
                tag = Z_INDEX_XML_TAG;
                break;
            default:
                tag = null;
                break;
        }
        if (tag == null) {
            result = null;
        } else {
            IKey key = getKey(index);
            if (key == null) {
                result = null;
            } else {
                result = key.toXMLLines(tag);
            }
        }
        return result;
    }

    @Override
    public void parseXML(Node node) throws KeyCompatibilityException {
        if (node == null) {
            throw new KeyCompatibilityException("Can't parse a null Node");
        } else {
            checkKeyCompatibility(node, getClass());
            keys.clear();
            if (node.hasChildNodes()) {
                NodeList subNodes = node.getChildNodes();
                for (int i = 0; i < subNodes.getLength(); i++) {
                    Node currentSubNode = subNodes.item(i);
                    if (!XMLUtils.isAFakeNode(currentSubNode)) {
                        String currentSubNodeType = currentSubNode.getNodeName().trim();
                        if (X_INDEX_XML_TAG.equals(currentSubNodeType)) {
                            addKey(X_INDEX, recoverKeyFromXMLNode(currentSubNode));
                        } else if (Y_INDEX_XML_TAG.equals(currentSubNodeType)) {
                            addKey(Y_INDEX, recoverKeyFromXMLNode(currentSubNode));
                        } else if (Z_INDEX_XML_TAG.equals(currentSubNodeType)) {
                            addKey(Z_INDEX, recoverKeyFromXMLNode(currentSubNode));
                        }
                    }
                }
            }
        }
    }
}
