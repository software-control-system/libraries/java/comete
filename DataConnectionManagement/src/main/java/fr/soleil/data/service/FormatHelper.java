/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.IDrawableTarget;
import fr.soleil.data.target.IFormatableTarget;
import fr.soleil.data.target.IMultiFormatableTarget;
import fr.soleil.data.target.IUnitTarget;

/**
 * A class that helps setting format to an {@link IFormatableTarget} or an {@link IMultiFormatableTarget}, taking
 * account of the case where that
 * {@link IUnitTarget}
 * might be an {@link IDrawableTarget}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class FormatHelper {

    /**
     * Applies a format to an {@link IFormatableTarget}.
     * 
     * @param target The {@link IFormatableTarget}.
     * @param format The format.
     */
    public static void setFormatToTarget(IFormatableTarget target, String format) {
        if (target instanceof IDrawableTarget) {
            IDrawingThreadManager manager = ((IDrawableTarget) target).getDrawingThreadManager();
            if (manager == null) {
                target.setFormat(format);
            } else {
                manager.runInDrawingThread(() -> {
                    target.setFormat(format);
                });
            }
        } else {
            target.setFormat(format);
        }
    }

    /**
     * Applies a format to an {@link IMultiFormatableTarget}.
     * 
     * @param target The {@link IMultiFormatableTarget}.
     * @param id The format id.
     * @param format The format.
     */
    public static void setFormatToTarget(IMultiFormatableTarget target, String id, String format) {
        if (target instanceof IDrawableTarget) {
            IDrawingThreadManager manager = ((IDrawableTarget) target).getDrawingThreadManager();
            if (manager == null) {
                target.setFormat(id, format);
            } else {
                manager.runInDrawingThread(() -> {
                    target.setFormat(id, format);
                });
            }
        } else {
            target.setFormat(id, format);
        }
    }

}
