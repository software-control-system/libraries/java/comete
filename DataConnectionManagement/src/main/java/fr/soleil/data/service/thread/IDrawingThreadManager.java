/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service.thread;

/**
 * An interface for something that manages a drawing thread
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IDrawingThreadManager {

    /**
     * Runs a {@link Runnable} in the managed drawing thread.
     * 
     * @param runnable The {@link Runnable}.
     */
    public void runInDrawingThread(Runnable runnable);

}
