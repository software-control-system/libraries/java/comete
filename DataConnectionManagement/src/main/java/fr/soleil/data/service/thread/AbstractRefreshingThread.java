/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service.thread;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.WeakHashMap;

import org.slf4j.LoggerFactory;

import fr.soleil.data.event.RefreshEvent;
import fr.soleil.data.listener.IRefreshingThreadListener;
import fr.soleil.data.mediator.Mediator;

/**
 * A {@link Thread} that manages data refreshing
 * 
 * @param <T> The type of data that can be refreshed
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AbstractRefreshingThread<T> extends Thread {

    protected static final String REFRESH_ERROR = "%s encountered an unexpected exception (but continues refreshing): %s";
    protected static final String NOTIFY_ERROR = "%s failed to notify listener %s";
    protected static final String MEMORY_ERROR = "%s memory error --> stopping";
    protected static final String IS_INTERRUPTED = "%s is interrupted";

    protected int sleepingPeriod = 1000;
    // "stopped" is "volatile" to force the Thread to stop as soon as possible
    protected volatile boolean stopped = false;
    protected final Collection<IRefreshingThreadListener> listeners;

    public AbstractRefreshingThread() {
        this(null);
    }

    public AbstractRefreshingThread(String name) {
        super(name);
        updateName();
        listeners = Collections.newSetFromMap(new WeakHashMap<IRefreshingThreadListener, Boolean>());
    }

    public AbstractRefreshingThread(ThreadGroup group, String name) {
        super(group, name);
        updateName();
        listeners = Collections.newSetFromMap(new WeakHashMap<IRefreshingThreadListener, Boolean>());
    }

    protected void updateName() {
        if (getName() == null) {
            setName(getClass().getSimpleName());
        }
    }

    @Override
    public void run() {
        try {
            while (!stopped) {
                long time = System.currentTimeMillis();
                // protected for unexpected exceptions to ensure stability
                try {
                    refreshData();
                } catch (Exception e) {
                    if (e instanceof InterruptedException) {
                        treatInterruptedException((InterruptedException) e);
                    } else {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .error(String.format(REFRESH_ERROR, getName(), e.getMessage()), e);
                    }
                }
                List<IRefreshingThreadListener> listenersCopy = new ArrayList<>();
                synchronized (listeners) {
                    listenersCopy.addAll(listeners);
                }
                RefreshEvent event = new RefreshEvent(this);
                for (IRefreshingThreadListener listener : listenersCopy) {
                    try {
                        listener.refreshed(event);
                    } catch (Exception e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .warn(String.format(NOTIFY_ERROR, getName(), listener), e);
                    }
                }
                listenersCopy.clear();
                time = System.currentTimeMillis() - time;
                // try to sleep sleepingPeriod - elapsed time
                long toSleep = sleepingPeriod - time;
                if (toSleep > 0) {
                    try {
                        sleep(toSleep);
                    } catch (InterruptedException e) {
                        treatInterruptedException(e);
                    }
                }
            }
        } catch (OutOfMemoryError oome) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(String.format(MEMORY_ERROR, getName()), oome);
        }
    }

    protected void treatInterruptedException(InterruptedException e) {
        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn(String.format(IS_INTERRUPTED, getName()), e);
        doStop();
    }

    public void addRefreshingListener(IRefreshingThreadListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    public void removeRefreshingListener(IRefreshingThreadListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    /**
     * Refreshes associated data
     */
    protected abstract void refreshData() throws Exception;

    /**
     * Changes the time to sleep between 2 refresh loops
     * 
     * @param sleepingPeriod The time to sleep in milliseconds
     */
    public void setSleepingPeriod(int sleepingPeriod) {
        this.sleepingPeriod = sleepingPeriod;
    }

    /**
     * Returns the time to sleep between 2 refresh loops (in ms)
     * 
     * @return An <code>int</code>
     */
    public int getSleepingPeriod() {
        return sleepingPeriod;
    }

    /**
     * Forces this {@link AbstractRefreshingThread} to stop
     */
    public void doStop() {
        stopped = true;
    }

    /**
     * Returns whether this {@link AbstractRefreshingThread} is stopped
     * 
     * @return A <code>boolean</code>
     */
    public boolean isStoped() {
        return stopped;
    }

    /**
     * Adds a data to refresh in this {@link AbstractRefreshingThread}
     * 
     * @param source The data to add
     */
    public abstract void addData(T data);

    /**
     * Stops the refreshing of some data
     * 
     * @param source The concerned data
     */
    public abstract void removeData(T data);

    /**
     * Returns a copy of the {@link Collection} of data managed by this {@link AbstractRefreshingThread}
     */
    public abstract Collection<T> getDataCopy();

    /**
     * Returns whether this {@link AbstractRefreshingThread} currently refreshes no data
     * 
     * @return A <code>boolean</code>
     */
    public abstract boolean isEmpty();

}
