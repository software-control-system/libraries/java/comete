/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.source.MultiDataSourceProducer;
import fr.soleil.lib.project.ObjectUtils;

/**
 * This class uses static methods to provide user access to the {@link IDataSourceProducer} he
 * needs. It is strongly recommend to uses this provider in order to have only one {@link IDataSourceProducer} created,
 * which is enough.
 * 
 * @author huriez, Rapha&euml;l GIRARDOT
 */
public class DataSourceProducerProvider {

    private static final Map<String, IDataSourceProducer> DATA_PRODUCERS = new HashMap<String, IDataSourceProducer>();
    private static final Map<IKey, List<Mediator<?>>> CONNECTION_MAP = new HashMap<IKey, List<Mediator<?>>>();
    private static boolean isInstanciated = false;

    /**
     * Adds a new {@link IDataSourceProducer} type to the list of {@link IDataSourceProducer}s which
     * can be return by the method {@link DataSourceProducerProvider#getProducer(IKey)}
     * 
     * @param producerClassName The {@link IDataSourceProducer} {@link Class} name
     * @return A <code>boolean</code> value. <code>true</code> If the {@link IDataSourceProducer} was successfully
     *         added, <code>false</code> otherwise. <code>false</code> is returned in any
     *         of these cases:
     *         <ul>
     *         <li><code>producerClassName</code> is <code>null</code> or empty</li>
     *         <li><code>producerClassName</code> does not match an {@link IDataSourceProducer} {@link Class}</li>
     *         <li>{@link IDataSourceProducer} instantiation failed (from {@link Class} named
     *         <code>producerClassName</code>)</li>
     *         <li>An {@link IDataSourceProducer} with the same id is already registered in list.</li>
     *         </ul>
     */
    public static boolean pushNewProducer(String producerClassName) {
        boolean pushed = false;
        if ((producerClassName != null) && (!producerClassName.trim().isEmpty())) {
            try {
                Class<?> factoryClass = Class.forName(producerClassName);
                if (IDataSourceProducer.class.isAssignableFrom(factoryClass)) {
                    Class<? extends IDataSourceProducer> producerClass = factoryClass
                            .asSubclass(IDataSourceProducer.class);
                    pushed = pushNewProducer(producerClass);
                }
            } catch (Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .error("Could not recover producer class " + producerClassName, e);
            }
        }
        return pushed;
    }

    /**
     * Adds a new {@link IDataSourceProducer} type to the list of {@link IDataSourceProducer}s which
     * can be return by the method {@link DataSourceProducerProvider#getProducer(IKey)}
     * 
     * @param producerClass The {@link IDataSourceProducer} {@link Class}
     * @return A <code>boolean</code> value. <code>true</code> If the {@link IDataSourceProducer} was successfully
     *         added, <code>false</code> otherwise. <code>false</code> is returned in any
     *         of these cases:
     *         <ul>
     *         <li><code>producerClass</code> is <code>null</code></li>
     *         <li>{@link IDataSourceProducer} instantiation failed (from {@link Class} <code>producerClass</code>)</li>
     *         <li>An {@link IDataSourceProducer} with the same id is already registered in list.</li>
     *         </ul>
     */
    public static boolean pushNewProducer(Class<? extends IDataSourceProducer> producerClass) {
        boolean pushed = false;
        if (producerClass != null) {
            IDataSourceProducer producer = null;
            synchronized (DATA_PRODUCERS) {
                for (IDataSourceProducer tmp : DATA_PRODUCERS.values()) {
                    if (producerClass.equals(tmp.getClass())) {
                        producer = tmp;
                        break;
                    }
                }
            }
            if (producer == null) {
                try {
                    producer = producerClass.newInstance();
                } catch (Exception e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .error("Cannot instianciate producer " + producerClass, e);
                    producer = null;
                }
            }
            pushed = pushNewProducer(producer);
        }
        return pushed;
    }

    /**
     * Adds a new {@link IDataSourceProducer} to the list of {@link IDataSourceProducer}s which can
     * be return by the method {@link DataSourceProducerProvider#getProducer(IKey)}
     * 
     * @param producer The {@link IDataSourceProducer} to add
     * @return A <code>boolean</code> value. <code>true</code> If the {@link IDataSourceProducer} was successfully
     *         added, <code>false</code> otherwise. <code>false</code> is returned in any
     *         of these cases:
     *         <ul>
     *         <li><code>producer</code> is <code>null</code></li>
     *         <li>An {@link IDataSourceProducer} with the same id is already registered in list.</li>
     *         </ul>
     */
    public static boolean pushNewProducer(IDataSourceProducer producer) {
        if (!isInstanciated) {
            isInstanciated = true;
            registerDefaultProducer();
        }
        boolean pushed = false;
        synchronized (DATA_PRODUCERS) {
            if (producer != null) {
                String id = producer.getId();
                if (!DATA_PRODUCERS.containsKey(id)) {
                    DATA_PRODUCERS.put(id, producer);
                    pushed = true;
                }
            }
        }
        return pushed;
    }

    /**
     * Forgets an {@link IDataSourceProducer} <div><big>EXPERT USAGE ONLY</big></div>
     * 
     * @param producerId The id of the {@link IDataSourceProducer} to forget;
     * @return A <code>boolean</code> value. <code>true</code> if there was a producer with such an
     *         id that could be forgotten, <code>false</code> otherwise
     */
    public static boolean forgetProducer(String producerId) {
        boolean done = false;
        if (!MultiDataSourceProducer.ID.equals(producerId)) {
            synchronized (DATA_PRODUCERS) {
                done = (DATA_PRODUCERS.remove(producerId) != null);
            }
        }
        return done;
    }

    /**
     * Search for the corresponding {@link IDataSourceProducer} for the given {@link String} id
     * 
     * @param id the producer id to search for. You can use either {@link IDataSourceProducer#getId()} or
     *            {@link IKey#getSourceProduction()}
     * @return a {@link IDataSourceProducer}
     */
    public static IDataSourceProducer getProducer(String id) {
        IDataSourceProducer result = null;
        synchronized (DATA_PRODUCERS) {
            if ((!DATA_PRODUCERS.isEmpty()) && (id != null)) {
                result = DATA_PRODUCERS.get(id);
            }
        }
        return result;
    }

    /**
     * Search for the corresponding {@link IDataSourceProducer} for the given {@link IKey}.
     * 
     * @param key The {@link IKey} for which to recover the {@link IDataSourceProducer}.
     * @return a {@link IDataSourceProducer}.
     */
    public static IDataSourceProducer getProducer(IKey key) {
        return key == null ? null : getProducer(key.getSourceProduction());
    }

    /**
     * Search for the corresponding {@link IDataSourceProducer} for the given {@link String} class
     * name
     * 
     * @param className the producer class name to search for.
     * @return a {@link IDataSourceProducer}
     */
    public static IDataSourceProducer getProducerByClassName(String className) {
        IDataSourceProducer result = null;
        if (className != null) {
            synchronized (DATA_PRODUCERS) {
                for (IDataSourceProducer tmp : DATA_PRODUCERS.values()) {
                    if (className.equals(tmp.getClass().getName())) {
                        result = tmp;
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Search for the corresponding {@link IDataSourceProducer} for the given {@link Class}
     * 
     * @param producerClass the producer.
     * @return A {@link IDataSourceProducer}. Will be <code>null</code> if no such producer was registered.
     */
    @SuppressWarnings("unchecked")
    public static <P extends IDataSourceProducer> P getProducerByClass(Class<P> producerClass) {
        P result = null;
        if (producerClass != null) {
            synchronized (DATA_PRODUCERS) {
                for (IDataSourceProducer tmp : DATA_PRODUCERS.values()) {
                    if (producerClass.equals(tmp.getClass())) {
                        result = (P) tmp;
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Check if the {@link IKey} and the {@link IDataSourceProducer} have the same producer type
     * 
     * @param key the {@link IKey} to compare
     * @param producer the {@link IDataSourceProducer} to compare
     * @return true if the {@link IKey} is compatible with the {@link IDataSourceProducer}
     */
    public static boolean isCorresponding(IKey key, IDataSourceProducer producer) {
        boolean result = false;
        if ((key != null) && (producer != null)) {
            result = ObjectUtils.sameObject(key.getSourceProduction(), producer.getId());
        }
        return result;
    }

    /**
     * Returns a {@link List} of all known {@link IDataSourceProducer} ids
     * 
     * @return A {@link List}
     */
    public static List<String> getProducerIdList() {
        List<String> producerIdList = new ArrayList<String>();
        synchronized (DATA_PRODUCERS) {
            producerIdList.addAll(DATA_PRODUCERS.keySet());
        }
        return producerIdList;
    }

    /**
     * This method is called when a producer is pushed for the first time. It add some default
     * producer that can be used mostly by other producer to call for specific type of key / source,
     * independently from the source origin.
     */
    private static void registerDefaultProducer() {
        pushNewProducer(MultiDataSourceProducer.class);
    }

    /**
     * <big>EXPERT USAGE ONLY</big>. Registers the fact that a source has been connected to a {@link Mediator}
     * 
     * @param key The {@link IKey} that identifies the connected source
     * @param mediator The {@link Mediator} to which the source was connected
     */
    public static void registerMediatorLink(IKey key, Mediator<?> mediator) {
        if ((key != null) && (mediator != null)) {
            synchronized (CONNECTION_MAP) {
                List<Mediator<?>> mediators = CONNECTION_MAP.get(key);
                if (mediators == null) {
                    mediators = new ArrayList<Mediator<?>>();
                    CONNECTION_MAP.put(key, mediators);
                }
                if (!mediators.contains(mediator)) {
                    mediators.add(mediator);
                }
            }
        }
    }

    /**
     * <big>EXPERT USAGE ONLY</big>. Registers the fact that a source has been disconnected from a {@link Mediator}. If
     * the source is considered as no more connected, it will be unreferenced
     * from its {@link IDataSourceProducer}, so that its refreshing is stopped, if concerned.
     * 
     * @param key The {@link IKey} that identifies the disconnected source
     * @param mediator The {@link Mediator} to which the source was disconnected
     */
    public static void unregisterMediatorLink(IKey key, Mediator<?> mediator) {
        if ((key != null) && (mediator != null)) {
            IConstantSourceProducer constantProducer = null;
            synchronized (CONNECTION_MAP) {
                List<Mediator<?>> mediators = CONNECTION_MAP.get(key);
                if (mediators != null) {
                    mediators.remove(mediator);
                    if (mediators.isEmpty()) {
                        CONNECTION_MAP.remove(key);
                        IDataSourceProducer producer = getProducer(key);
                        if (producer instanceof IConstantSourceProducer) {
                            constantProducer = (IConstantSourceProducer) producer;
                        }
                    }
                }
            }
            // Do this outside of synchronization block to avoid some dead locks
            if (constantProducer != null) {
                constantProducer.removeDataSource(key);
            }
        }
    }

    /**
     * Changes the refreshing strategy for a source
     * 
     * @param key The {@link IKey} that identifies the source
     * @param strategy The new strategy to apply
     */
    public static void setRefreshingStrategy(IKey key, IRefreshingStrategy strategy) {
        IDataSourceProducer producer = getProducer(key);
        if (producer instanceof AbstractRefreshingManager<?>) {
            ((AbstractRefreshingManager<?>) producer).setRefreshingStrategy(key, strategy);
        }
    }

    /**
     * Changes the refreshing strategy for a source
     * 
     * @param key The {@link IKey} that identifies the source
     * @return an {@link IRefreshingStrategy}
     */
    public static IRefreshingStrategy getRefreshingStrategy(IKey key) {
        IRefreshingStrategy strategy = null;
        IDataSourceProducer producer = getProducer(key);
        if (producer instanceof AbstractRefreshingManager<?>) {
            strategy = ((AbstractRefreshingManager<?>) producer).getRefreshingStrategy(key);
        }
        return strategy;
    }

}
