/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.iterator;

import java.util.concurrent.Callable;

import fr.soleil.data.manager.ISingleThreadManager;

/**
 * A Task {@link LimitedIterator} which also is an {@link ISingleThreadManager} and can do some treatment everything in
 * a single Thread
 *
 * @param <V> The type of data returned by the {@link Callable} for task treatment
 * @param <C> The type of {@link Callable} managed by this {@link CalculationIterator}
 * @param <E> The type of {@link Exception} returned by this {@link CalculationIterator} in case of treatment error.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface CalculationIterator<V, C extends Callable<V>, E extends Exception>
        extends LimitedIterator<C>, ISingleThreadManager<E> {

    /**
     * Returns whether this {@link CalculationIterator}'s tasks can be treated in parallel threads.
     * 
     * @return A <code>boolean</code>. If <code>false</code>, task treatment should be done in
     *         {@link #treatDataInASingleThread()}.
     */
    public boolean isThreadSafe();

}
