/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.iterator;

import java.util.Iterator;
import java.util.concurrent.Callable;

/**
 * A {@link LimitedIterator} that accumulates some tratments to do through multiple {@link LimitedIterator}s.
 * 
 * @param <V> The type of data returned by the treatment done by this {@link AMultiIterator}
 * @param <C> The type of treatment done by this {@link AMultiIterator}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AMultiIterator<V, C extends Callable<V>> implements LimitedIterator<C> {

    protected LimitedIterator<C>[] iterators;
    protected int iteratorIndex;
    protected final boolean initOk;

    /**
     * Creates a new {@link AMultiIterator}
     * 
     * @param iterators The {@link LimitedIterator}s that will do the work
     */
    public AMultiIterator(@SuppressWarnings("unchecked") LimitedIterator<C>... iterators) {
        this.iterators = iterators;
        iteratorIndex = 0;
        if ((iterators == null) || (iterators.length == 0)) {
            initOk = false;
        } else {
            boolean tmpOk = true;
            for (Iterator<C> iterator : iterators) {
                if (iterator == null) {
                    tmpOk = false;
                    break;
                }
            }
            initOk = tmpOk;
        }
    }

    /**
     * What should be done once a {@link LimitedIterator} finished
     * 
     * @param index The index of the finished {@link LimitedIterator}
     */
    protected abstract void afterTreatment(int index);

    @Override
    public boolean hasNext() {
        boolean hasNext = false;
        if (initOk) {
            while ((!hasNext) && (iteratorIndex < iterators.length)) {
                hasNext = iterators[iteratorIndex].hasNext();
                if (!hasNext) {
                    afterTreatment(iteratorIndex++);
                }
            }
        }
        return hasNext;
    }

    @Override
    public C next() {
        return iterators[iteratorIndex].next();
    }

    @Override
    public void remove() {
        // not managed
    }

    @Override
    public int getElementCount() {
        int count = 0;
        if (iterators != null) {
            for (LimitedIterator<C> iterator : iterators) {
                if (iterator != null) {
                    count += iterator.getElementCount();
                }
            }
        }
        return count;
    }
}
