/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.iterator;

import java.util.concurrent.Callable;

import fr.soleil.data.listener.IStepListener;
import fr.soleil.data.manager.IStepManager;
import fr.soleil.data.util.StepDelegate;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * A {@link LimitedIterator} that will do some treatment on some stack.
 *
 * @param <V> The type of data returned by each treatment.
 * @param <E> The type of {@link Exception} returned by this {@link AStackBrowser} in case of treatment error.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AStackBrowser<V, E extends Exception>
        implements CalculationIterator<V, Callable<V>, E>, IStepManager {

    protected volatile int[] position;
    protected volatile int[] shape;
    protected volatile int length;

    protected final StepDelegate stepDelegate;
    protected final ICancelable cancelable;

    public AStackBrowser(ICancelable cancelable) {
        stepDelegate = new StepDelegate();
        position = null;
        shape = null;
        length = 0;
        this.cancelable = cancelable;
    }

    public AStackBrowser(ICancelable cancelable, int... shape) {
        this(cancelable);
        setShape(shape);
    }

    protected void setShape(int[] shape) {
        length = 0;
        int[] position;
        if (shape == null) {
            position = null;
        } else {
            position = new int[shape.length];
            int length = 1;
            for (int dim : shape) {
                length *= dim;
            }
            this.length = length;
        }
        this.shape = shape;
        this.position = position;
    }

    @Override
    public boolean hasNext() {
        int[] shape = this.shape, position = this.position;
        return (shape != null) && (position != null) && ArrayUtils.isPosition(shape, position)
                && ((cancelable == null) || (!cancelable.isCanceled()));
    }

    @Override
    public Callable<V> next() {
        int[] shape = this.shape, position = this.position;
        Callable<V> callable;
        if (position == null) {
            callable = null;
        } else {
            callable = generateCallableAt(position.clone());
            ArrayUtils.incrementPosition(shape, position);
        }
        return callable;
    }

    protected abstract Callable<V> generateCallableAt(int... position);

    @Override
    public void remove() {
        // not managed
    }

    @Override
    public int getElementCount() {
        return length;
    }

    @Override
    public void addStepListener(IStepListener listener) {
        stepDelegate.addStepListener(listener);
    }

    @Override
    public void removeStepListener(IStepListener listener) {
        stepDelegate.removeStepListener(listener);
    }
}
