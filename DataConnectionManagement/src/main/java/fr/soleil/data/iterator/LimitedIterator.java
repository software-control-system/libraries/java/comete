/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.iterator;

import java.util.Iterator;

/**
 * An {@link Iterator} that knows on how many elements to iterate
 * 
 * @param <E> The type of data on which to iterate
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface LimitedIterator<E> extends Iterator<E> {

    /**
     * Returns on how many elements to iterate
     * 
     * @return An <code>int</code>
     */
    public int getElementCount();

    /**
     * Tells this {@link LimitedIterator} to finish what it has to, because it will no more be used.
     */
    public void close();
}
