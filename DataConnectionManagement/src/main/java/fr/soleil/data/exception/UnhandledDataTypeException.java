/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.exception;

import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IDataSourceProducer;

public class UnhandledDataTypeException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -5760543922235767510L;

    public UnhandledDataTypeException(GenericDescriptor descriptor, IDataSourceProducer producer) {
        super(selectSuitableAwnser(descriptor, producer));
    }

    private final static String selectSuitableAwnser(GenericDescriptor descriptor, IDataSourceProducer producer) {
        String message = null;
        if (descriptor == null) {
            message = "The data type for create the data source is unknown";
        } else {
            message = "the data type " + descriptor + " is not handle by the producer " + producer.getClass().getName();
        }
        return message;
    }
}
