/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.exception;

import fr.soleil.data.target.ITarget;

/**
 * This exception is thrown when the {@link ITarget} passed as a parameter of the method
 * getTargetSignature is not associated to any signature. This exception lists parent classes
 * inspected when exception thrown.
 * 
 * @author huriez
 * 
 */
public class UnhandledTargetSignatureException extends Exception {

    private static final long serialVersionUID = 7199578169056618650L;

    public UnhandledTargetSignatureException(String origin, StringBuilder errorTracks) {
        super("Unhandle signature for this target : " + origin + "\n" + errorTracks);
    }
}
