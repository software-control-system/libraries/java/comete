/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.exception;

import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;

public class KeyCompatibilityException extends Exception {

    private static final long serialVersionUID = -2914270561707881951L;

    public KeyCompatibilityException(IKey key, IDataSourceProducer producer) {
        this(verifyAnwser(key, producer));
    }

    public KeyCompatibilityException(String message) {
        super(message);
    }

    public KeyCompatibilityException(String message, Throwable cause) {
        super(message, cause);
    }

    private static final String verifyAnwser(IKey key, IDataSourceProducer producer) {
        StringBuilder result = new StringBuilder();
        result.append("the key ");
        if (key != null) {
            result.append(key.getClass().getName());
            result.append(" with id ");
            result.append(key.getSourceProduction());
            result.append(", ");
        } else {
            result.append(" informations cannot be used and ");
        }

        if (producer != null) {
            result.append("is incompatible with the producer ");
            result.append(producer.getClass().getName());
            result.append(" with id ");
            result.append(producer.getId());
            result.append(".");
        } else {
            result.append("the producer doesn't exists.");
        }

        return result.toString();
    }
}
