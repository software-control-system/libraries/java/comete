/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.exception;

/**
 * An exception that can be thrown when trying to write some invalid data in a source
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class InvalidSourceDataException extends Exception {

    private static final long serialVersionUID = 2361875806544766183L;

    public InvalidSourceDataException() {
        super();
    }

    public InvalidSourceDataException(String message) {
        super(message);
    }

    public InvalidSourceDataException(Throwable cause) {
        super(cause);
    }

    public InvalidSourceDataException(String message, Throwable cause) {
        super(message, cause);
    }

}
