/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.container;

import fr.soleil.data.source.AbstractDataSource;

/**
 * This interface defines an object able of containing data formatted to be used in an
 * {@link AbstractDataSource}.
 * 
 * @author huriez
 * 
 * @param <T> The type of data this {@link IDataContainer} can contain
 */
public interface IDataTypeContainer<T> {

    /**
     * Returns the type of data contained in this {@link IDataTypeContainer}
     * 
     * @return A {@link Class}
     */
    public Class<T> getType();
}
