/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.container.matrix;

/**
 * An {@link ObjectMatrix} of {@link Object}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BasicObjectMatrix extends ObjectMatrix<Object> {

    public BasicObjectMatrix(Class<Object> type) {
        super(type);
    }

    @Override
    protected Class<Object> generateDefaultDataType() {
        return Object.class;
    }

}
