/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.container;

import fr.soleil.data.service.GenericDescriptor;

/**
 * A basic implementation of {@link IDataContainer}
 * 
 * @param <T> The type of data contained/manipulated by this {@link BasicDataContainer}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BasicDataContainer<T> implements IDataContainer<T> {

    private T data;
    private final GenericDescriptor descriptor;
    private boolean settable;
    private boolean unsigned;

    public BasicDataContainer(T data, GenericDescriptor descriptor, boolean settable) {
        this.data = data;
        this.descriptor = descriptor;
        this.settable = settable;
    }

    public BasicDataContainer(T data, GenericDescriptor descriptor) {
        this(data, descriptor, false);
    }

    @Override
    public T getData() throws Exception {
        return data;
    }

    @Override
    public void setData(T data) throws Exception {
        if (settable) {
            this.data = data;
        }
    }

    @Override
    public boolean isSettable() {
        return settable;
    }

    @Override
    public GenericDescriptor getDataType() {
        return descriptor;
    }

    @Override
    public boolean isUnsigned() {
        return unsigned;
    }

    public void setUnsigned(boolean unsigned) {
        this.unsigned = unsigned;
    }

}
