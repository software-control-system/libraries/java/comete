/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.container.matrix.util;

import java.math.BigInteger;

import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.BasicNumberMatrix;
import fr.soleil.data.container.matrix.BigIntegerMatrix;
import fr.soleil.data.container.matrix.ByteMatrix;
import fr.soleil.data.container.matrix.DoubleMatrix;
import fr.soleil.data.container.matrix.FloatMatrix;
import fr.soleil.data.container.matrix.IntMatrix;
import fr.soleil.data.container.matrix.LongMatrix;
import fr.soleil.data.container.matrix.ShortMatrix;
import fr.soleil.lib.project.math.ArrayUtils;

public class NumberMatrixGenerator {

    @SuppressWarnings("unchecked")
    public static AbstractNumberMatrix<?> getNewMatrixFor(Object[] data) {
        AbstractNumberMatrix<?> result = null;
        if (data != null) {
            Class<?> type = ArrayUtils.recoverDataType(data);
            if (Byte.TYPE.equals(type) || Byte.class.equals(type)) {
                result = new ByteMatrix((Class<Byte>) type);
            } else if (Short.TYPE.equals(type) || Short.class.equals(type)) {
                result = new ShortMatrix((Class<Short>) type);
            } else if (Integer.TYPE.equals(type) || Integer.class.equals(type)) {
                result = new IntMatrix((Class<Integer>) type);
            } else if (Long.TYPE.equals(type) || Long.class.equals(type)) {
                result = new LongMatrix((Class<Long>) type);
            } else if (BigInteger.class.equals(type)) {
                result = new BigIntegerMatrix();
            } else if (Float.TYPE.equals(type) || Float.class.equals(type)) {
                result = new FloatMatrix((Class<Float>) type);
            } else if (Double.TYPE.equals(type) || Double.class.equals(type)) {
                result = new DoubleMatrix((Class<Double>) type);
            } else if (Number.class.equals(type)) {
                result = new BasicNumberMatrix();
            }
        }
        return result;
    }

}
