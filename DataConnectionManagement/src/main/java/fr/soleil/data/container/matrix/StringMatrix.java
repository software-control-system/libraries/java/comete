/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.container.matrix;

import fr.soleil.lib.project.ObjectUtils;

/**
 * This class represents a {@link Matrix} containing {@link String}. This class also provides
 * methods to perform mathematical operations on matrices.
 * 
 * @author huriez
 * 
 */
public class StringMatrix extends ObjectMatrix<String> {

    public StringMatrix() {
        super(String.class);
    }

    @Override
    protected Class<String> generateDefaultDataType() {
        return String.class;
    }

    /**
     * Removes empty and null values and updates the rectangle without theses values
     */
    @Override
    protected void updateTrimmedInformation() {
        int height = getHeight();
        int width = getWidth();
        String value = ObjectUtils.EMPTY_STRING;
        boolean validLineValue = false;
        boolean validColValue = false;
        setTrimStartColumn(width);
        setTrimEndColumn(0);
        setTrimStartLine(0);
        setTrimEndLine(0);

        if (isFlatValue()) {
            String[] flatValue = (String[]) getFlatValueUnlocked();
            for (int line = 0; line < height; line++) {
                validLineValue = false;
                for (int col = 0; col < width; col++) {
                    validColValue = false;
                    value = flatValue[line * width + col];
                    if ((value != null) && (!value.trim().isEmpty())) {
                        validLineValue = true;
                        validColValue = true;
                    }
                    if (validColValue) {
                        if (col <= getTrimStartColumn()) {
                            setTrimStartColumn(col);
                        }
                        if (getTrimEndColumn() < col) {
                            setTrimEndColumn(col);
                        }
                    }
                }
                if (validLineValue) {
                    if (getTrimStartLine() == 0) {
                        setTrimStartLine(line);
                    }
                    if (getTrimEndLine() <= line) {
                        setTrimEndLine(line);
                    }
                }
            }
        } else {
            String[][] m_values = (String[][]) getValueUnlocked();

            for (int line = 0; line < height; line++) {
                validLineValue = false;
                for (int col = 0; col < width; col++) {
                    validColValue = false;
                    value = m_values[line][col];

                    if ((value != null) && (!value.trim().isEmpty())) {
                        validLineValue = true;
                        validColValue = true;
                    }
                    if (validColValue) {
                        if (col <= getTrimStartColumn()) {
                            setTrimStartColumn(col);
                        }
                        if (getTrimEndColumn() < col) {
                            setTrimEndColumn(col);
                        }
                    }
                }
                if (validLineValue) {
                    if (getTrimStartLine() == 0) {
                        setTrimStartLine(line);
                    }
                    if (getTrimEndLine() <= line) {
                        setTrimEndLine(line);
                    }
                }
            }
        }

    }

    public boolean equals(StringMatrix matrice) {
        String[][] m_values = getValue();
        String Temp[][] = matrice.getValue();
        if (Temp == null || m_values == null || m_values.length != Temp.length) {
            return false;
        }

        int i, j = 0;

        for (i = 0; i < m_values.length; i++) {
            for (j = 0; j < m_values.length; j++) {
                if (Temp[i][j] != m_values[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

}
