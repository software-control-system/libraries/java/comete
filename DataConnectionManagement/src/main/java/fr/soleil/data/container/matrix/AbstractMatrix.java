/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.container.matrix;

import java.lang.reflect.Array;
import java.util.concurrent.Semaphore;

import javax.activation.UnsupportedDataTypeException;

import org.slf4j.LoggerFactory;

import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.adapter.AdapterUtils;
import fr.soleil.data.container.AbstractNamedObject;
import fr.soleil.data.container.IDataTypeContainer;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * <p>
 * This class represents a matrix of T data which can be stored in an array of rank 2 or rank 1. You
 * can only instantiate an empty matrix. Data is provided through the {@link setValues} method for
 * an array of rank 2 or the {@link setFlatValues} method for an array of rank 1.
 * </p>
 * <p>
 * <b>This class is fully compatible with primitive types</b>. To use primitive types, you just have
 * to use the corresponding Object for <code>T</code> (example: {@link Double} for double,
 * {@link Character} for char, {@link Boolean} for boolean, etc...), and use the associated
 * <code>TYPE</code> constant in constructor (example: {@link Double#TYPE}, {@link Character#TYPE},
 * {@link Boolean#TYPE}, etc...)
 * </p>
 * 
 * @author huriez, Rapha&euml;l GIRARDOT
 * 
 * @param <T> The type of data in the matrix
 */
public abstract class AbstractMatrix<T> extends AbstractNamedObject implements IDataTypeContainer<T>, Cloneable {

    protected final Semaphore valueSema;
    protected boolean flatValue;
    protected Object value;
    protected int width;
    protected int height;
    protected boolean saveColumnIndex;
    protected String valueSeparator;
    protected int trimStartColumn;
    protected int trimEndColumn;
    protected int trimStartLine;
    protected int trimEndLine;

    protected Class<T> dataType;
    protected AbstractAdapter<String, T> converter;

    /**
     * Constructor
     * 
     * @param type The type of data in the matrix
     */
    public AbstractMatrix(Class<T> type) {
        super();
        if (type == null) {
            type = generateDefaultDataType();
        }
        dataType = type;
        valueSema = new Semaphore(1);
        flatValue = false;
        value = null;
        width = 0;
        height = 0;
        saveColumnIndex = false;
        valueSeparator = "\t";
        trimStartColumn = 0;
        trimEndColumn = 0;
        trimStartLine = 0;
        trimEndLine = 0;
    }

    /**
     * Generates a default class type if constructor's parametered class type is <code>null</code>.
     * So, this method shall never return <code>null</code>.
     * 
     * @return a {@link Class}
     * @see #AbstractMatrix(Class)
     */
    protected abstract Class<T> generateDefaultDataType();

    protected void updateTrimmedInformation() {
        // nothing to do by default
    }

    /**
     * Returns whether this matrix value is represented as a flat matrix
     * 
     * @return A boolean value. <code>TRUE</code> in case of flat matrix, <code>FALSE</code>
     *         otherwise
     */
    public final boolean isFlatValue() {
        return flatValue;
    }

    /**
     * Returns a line in the matrix
     * 
     * @param line The line index
     * @return The desired line. You should cast it as the expected array type.
     */
    public Object getLine(int line) {
        Object result = null;
        try {
            valueSema.acquire();
            if ((value != null) && (line < height)) {
                if (isFlatValue()) {
                    Object flatValue = value;
                    result = Array.newInstance(ArrayUtils.recoverDataType(flatValue), width);
                    System.arraycopy(flatValue, line * width, result, 0, width);
                } else {
                    Object[] matrixValue = (Object[]) value;
                    result = matrixValue[line];
                }
            }
            valueSema.release();
        } catch (InterruptedException e) {
            // Should not happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn("Line extraction interrupted", e);
        } catch (Exception e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to extract desired line", e);
        }
        return result;
    }

    public Object getColumn(int column) {
        Object result = null;
        try {
            valueSema.acquire();
            if ((value != null) && (column < width)) {
                result = Array.newInstance(ArrayUtils.recoverDataType(value), height);
                if (isFlatValue()) {
                    for (int row = 0; row < height; row++) {
                        System.arraycopy(value, (row * width) + column, result, row, 1);
                    }
                } else {
                    for (int row = 0; row < height; row++) {
                        System.arraycopy(Array.get(value, row), column, result, row, 1);
                    }
                }
            }
            valueSema.release();
        } catch (InterruptedException e) {
            // Should not happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn("Column extraction interrupted", e);
        } catch (Exception e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to extract desired column", e);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public T getValueAt(int row, int col) {
        T result = null;
        Object line = getLine(row);
        if (line != null) {
            int length = Array.getLength(line);
            if (length > 0 && col < length) {
                result = (T) Array.get(line, col);
            }
        }
        return result;
    }

    /**
     * Sets a value in the matrix
     * 
     * @param value The value to set
     * @param row The row index
     * @param col The columns index
     */
    public void setValueAt(T value, int row, int col) {
        try {
            valueSema.acquire();
            if ((row > -1) && (col > -1) && (row < getHeight()) && (col < width)) {
                Object arrayToset;
                int index;
                if (isFlatValue()) {
                    arrayToset = getFlatValueUnlocked();
                    index = (row * width) + col;
                } else {
                    arrayToset = getValueUnlocked()[row];
                    index = col;
                }
                if (arrayToset instanceof Object[]) {
                    // general case
                    Array.set(arrayToset, index, value);
                } else if (value != null) {
                    // for primitive types, null value is not allowed
                    if (arrayToset instanceof boolean[]) {
                        // primitive boolean case
                        Array.setBoolean(arrayToset, index, (Boolean) value);
                    } else if (arrayToset instanceof char[]) {
                        // primitive char case
                        Array.setChar(arrayToset, index, (Character) value);
                    } else if (arrayToset instanceof byte[]) {
                        // primitive byte case
                        Array.setByte(arrayToset, index, (Byte) value);
                    } else if (arrayToset instanceof short[]) {
                        // primitive short case
                        Array.setShort(arrayToset, index, (Short) value);
                    } else if (arrayToset instanceof int[]) {
                        // primitive int case
                        Array.setInt(arrayToset, index, (Integer) value);
                    } else if (arrayToset instanceof long[]) {
                        // primitive long case
                        Array.setLong(arrayToset, index, (Long) value);
                    } else if (arrayToset instanceof float[]) {
                        // primitive float case
                        Array.setFloat(arrayToset, index, (Float) value);
                    } else if (arrayToset instanceof double[]) {
                        // primitive double case
                        Array.setDouble(arrayToset, index, (Double) value);
                    }
                }
                updateTrimmedInformation();
            }
            valueSema.release();
        } catch (InterruptedException e) {
            // Should not happen;
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to set value", e);
        }
    }

    /**
     * Sets a value in the matrix, based on its {@link String} representation
     * 
     * @param value The {@link String} representation of the value to set
     * @param row The row index
     * @param col The columns index
     */
    public void setValueFromStringAt(String value, int row, int col) {
        setValueAt(parseValue(value), row, col);
    }

    protected final Object[] getValueUnlocked() {
        Object[] result = null;
        if (value != null) {
            if (isFlatValue()) {
                Object flatValue = getFlatValueUnlocked();
                result = (Object[]) ArrayUtils.convertArrayDimensionFrom1ToN(flatValue, height, width);
            } else {
                result = (Object[]) value;
            }
        }
        return result;
    }

    /**
     * Returns The 2 dimensional table representation of the matrix
     * 
     * @return The expected matrix. You should cast it as the expected matrix type (example:
     *         <code>double[][]</code>, {@link String}<code>[][]</code>, etc...)
     */
    public Object[] getValue() {
        Object[] result = null;
        try {
            valueSema.acquire();
            result = getValueUnlocked();
            valueSema.release();
        } catch (InterruptedException e) {
            // Should not happen;
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn("Value extraction interrupted", e);
        }
        return result;
    }

    protected final Object getFlatValueUnlocked() {
        Object result = null;
        if (value != null) {
            if (isFlatValue()) {
                result = value;
            } else {
                result = ArrayUtils.convertArrayDimensionFromNTo1(value);
            }
        }
        return result;
    }

    public Object getFlatValue() {
        Object flatValue = null;
        try {
            valueSema.acquire();
            flatValue = getFlatValueUnlocked();
            valueSema.release();
        } catch (InterruptedException e) {
            // Should not happen;
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn("Flat value extraction interrupted", e);
        }
        return flatValue;
    }

    public Object initArray(int size) {
        return Array.newInstance(dataType, size);
    }

    public Object[] initValue(int height, int width) {
        return (Object[]) Array.newInstance(dataType, height, width);
    }

    /**
     * Sets this matrix data with a "flat" matrix
     * 
     * @param array The flat matrix
     * @param height The flat matrix height
     * @param width The flat matrix width
     * @throws UnsupportedDataTypeException If <code>array</code> is not of the expected type
     *             (example: if {@link #AbstractMatrix(Class)} parametered class is {@link String},
     *             then <code>array</code> should be a {@link String}<code>[]</code>. If
     *             {@link #AbstractMatrix(Class)} parametered class is {@link Double#TYPE}, then
     *             <code>array</code> should be a <code>double[]</code>)
     * @see #getType()
     */
    public void setFlatValue(Object array, int height, int width) throws UnsupportedDataTypeException {
        try {
            valueSema.acquire();
            if (array == null) {
                value = null;
                this.width = 0;
                this.height = 0;
            } else if (Array.newInstance(dataType, 0).getClass().equals(array.getClass())) {
                if (Array.getLength(array) == height * width) {
                    this.width = width;
                    this.height = height;
                    value = array;
                    flatValue = true;
                }
            } else {
                // release Semaphore before throwing exception
                valueSema.release();
                StringBuilder errorBuilder = new StringBuilder(getClass().getName());
                errorBuilder.append(".setFlatValue: Incompatible array type");
                throw new UnsupportedDataTypeException(errorBuilder.toString());
            }
            updateTrimmedInformation();
            valueSema.release();
        } catch (InterruptedException e) {
            // Should not happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn("Flat value setting interrupted", e);
        }
    }

    /**
     * Sets this matrix data with a classic matrix
     * 
     * @param matrixValue The classic matrix
     * @throws UnsupportedDataTypeException If <code>value</code> is not of the expected type
     *             (example: if {@link #AbstractMatrix(Class)} parametered class is {@link String},
     *             then <code>value</code> should be a {@link String}<code>[][]</code>. If
     *             {@link #AbstractMatrix(Class)} parametered class is {@link Double#TYPE}, then
     *             <code>value</code> should be a <code>double[][]</code>)
     * @see #getType()
     */
    public void setValue(Object[] matrixValue) throws UnsupportedDataTypeException {
        try {
            valueSema.acquire();
            flatValue = false;
            if (matrixValue == null) {
                height = 0;
                width = 0;
                value = null;
            } else if (Array.newInstance(Array.newInstance(dataType, 0).getClass(), 0).getClass()
                    .equals(matrixValue.getClass())) {
                height = matrixValue.length;
                if (height > 0) {
                    width = Array.getLength(matrixValue[0]);
                } else {
                    width = 0;
                }
                value = matrixValue;
            } else {
                // release Semaphore before throwing exception
                valueSema.release();
                StringBuilder errorBuilder = new StringBuilder(getClass().getName());
                errorBuilder.append(".setValue: Incompatible array type");
                throw new UnsupportedDataTypeException(errorBuilder.toString());
            }
            updateTrimmedInformation();
            valueSema.release();
        } catch (InterruptedException e) {
            // Should not happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn("Value setting interrupted", e);
        }
    }

    public int getTrimStartColumn() {
        return trimStartColumn;
    }

    public int getTrimEndColumn() {
        return trimEndColumn;
    }

    public int getTrimStartLine() {
        return trimStartLine;
    }

    public int getTrimEndLine() {
        return trimEndLine;
    }

    public void setTrimStartColumn(int value) {
        this.trimStartColumn = value;
    }

    public void setTrimEndColumn(int value) {
        this.trimEndColumn = value;
    }

    public void setTrimStartLine(int value) {
        this.trimStartLine = value;
    }

    public void setTrimEndLine(int value) {
        this.trimEndLine = value;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isSaveColumnIndex() {
        return saveColumnIndex;
    }

    public void setSaveColumnIndex(boolean saveColumnIndex) {
        this.saveColumnIndex = saveColumnIndex;
    }

    public String getValueSeparator() {
        return valueSeparator;
    }

    public void setValueSeparator(String valueSeparator) {
        this.valueSeparator = valueSeparator;
    }

    public boolean isDimensionEquals(AbstractMatrix<?> matrix) {
        boolean result;
        if (matrix == null) {
            result = false;
        } else if (matrix.getHeight() != height) {
            result = false;
        } else if (matrix.getWidth() != width) {
            result = false;
        } else {
            result = true;
        }
        return result;
    }

    /**
     * Returns the type of data the associated matrix contains
     * 
     * @return A {@link Class}
     */
    @Override
    public Class<T> getType() {
        return dataType;
    }

    /**
     * @return the converter
     */
    public AbstractAdapter<String, T> getConverter() {
        if ((converter == null) && (dataType != null)) {
            converter = AdapterUtils.getAdapter(new GenericDescriptor(String.class), new GenericDescriptor(dataType));
        }
        return converter;
    }

    public T parseValue(String value) {
        T result = null;
        if (getConverter() != null) {
            try {
                result = getConverter().adapt(value);
            } catch (DataAdaptationException e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn("Failed to parse value '" + value + "'", e);
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public AbstractMatrix<T> clone() {
        AbstractMatrix<T> clone;
        try {
            clone = (AbstractMatrix<T>) super.clone();
            try {
                valueSema.acquire();
                if (value == null) {
                    clone.value = null;
                } else {
                    if (flatValue) {
                        Object flatMatrix = getFlatValueUnlocked();
                        if (flatMatrix instanceof Object[]) {
                            // general case
                            clone.value = ((Object[]) flatMatrix).clone();
                        } else if (flatMatrix instanceof boolean[]) {
                            // primitive boolean case
                            clone.value = ((boolean[]) flatMatrix).clone();
                        } else if (flatMatrix instanceof char[]) {
                            // primitive char case
                            clone.value = ((char[]) flatMatrix).clone();
                        } else if (flatMatrix instanceof byte[]) {
                            // primitive byte case
                            clone.value = ((byte[]) flatMatrix).clone();
                        } else if (flatMatrix instanceof short[]) {
                            // primitive short case
                            clone.value = ((short[]) flatMatrix).clone();
                        } else if (flatMatrix instanceof int[]) {
                            // primitive int case
                            clone.value = ((int[]) flatMatrix).clone();
                        } else if (flatMatrix instanceof long[]) {
                            // primitive long case
                            clone.value = ((long[]) flatMatrix).clone();
                        } else if (flatMatrix instanceof float[]) {
                            // primitive float case
                            clone.value = ((float[]) flatMatrix).clone();
                        } else if (flatMatrix instanceof double[]) {
                            // primitive double case
                            clone.value = ((double[]) flatMatrix).clone();
                        }
                    } else {
                        clone.value = getValueUnlocked().clone();
                    }
                }
                valueSema.release();
            } catch (InterruptedException e) {
                // Should not happen
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn("Clone interrupted", e);
            }
        } catch (CloneNotSupportedException e) {
            // This can't happen because AbstractMatrix implements Cloneable, and so does any array
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            clone = null;
        }
        return clone;
    }

    @Override
    public int hashCode() {
        int code = 123;
        int mult = 456;
        code = (code * mult) + getClass().hashCode();
        code = (code * mult) + ObjectUtils.getHashCode(getType());
        code = (code * mult) + getWidth();
        code = (code * mult) + getHeight();
        code = (code * mult) + getHashCode(value);
        return code;
    }

    /**
     * Creates an hashcode for a value
     * 
     * @param value The value from which to generate an hashcode
     * @return an <code>int</code>
     */
    protected int getHashCode(Object value) {
        int hashCode = 0;
        if (value instanceof Object[]) {
            Object[] array = (Object[]) value;
            for (Object val : array) {
                hashCode += getHashCode(val);
            }
        } else if (value instanceof boolean[]) {
            boolean[] array = (boolean[]) value;
            for (boolean val : array) {
                hashCode += ObjectUtils.generateHashCode(val);
            }
        } else if (value instanceof char[]) {
            char[] array = (char[]) value;
            for (char val : array) {
                hashCode += val;
            }
        } else if (value instanceof byte[]) {
            byte[] array = (byte[]) value;
            for (byte val : array) {
                hashCode += val;
            }
        } else if (value instanceof short[]) {
            short[] array = (short[]) value;
            for (short val : array) {
                hashCode += val;
            }
        } else if (value instanceof int[]) {
            int[] array = (int[]) value;
            for (int val : array) {
                hashCode += val;
            }
        } else if (value instanceof long[]) {
            long[] array = (long[]) value;
            for (long val : array) {
                hashCode += val;
            }
        } else if (value instanceof float[]) {
            float[] array = (float[]) value;
            for (float val : array) {
                hashCode += val;
            }
        } else if (value instanceof double[]) {
            double[] array = (double[]) value;
            for (double val : array) {
                hashCode += val;
            }
        } else {
            hashCode += ObjectUtils.getHashCode(value);
        }
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && (getClass().equals(obj.getClass()))) {
            AbstractMatrix<?> toTest = (AbstractMatrix<?>) obj;
            if (getType().equals(toTest.getType())) {
                if ((getWidth() == toTest.getWidth()) && (getHeight() == toTest.getHeight())) {
                    if (isFlatValue()) {
                        equals = ObjectUtils.sameObject(getFlatValue(), toTest.getFlatValue());
                    } else {
                        equals = ObjectUtils.sameObject(getValue(), toTest.getValue());
                    }
                }
            }
        }
        return equals;
    }
}
