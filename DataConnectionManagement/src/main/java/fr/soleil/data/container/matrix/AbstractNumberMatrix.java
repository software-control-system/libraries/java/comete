/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.container.matrix;

import java.math.BigInteger;

/**
 * This class represents a number matrix. It is compatible with both {@link Number} objects and
 * their primitive types.
 * 
 * @author huriez, Rapha&euml;l GIRARDOT
 * 
 */
public abstract class AbstractNumberMatrix<T extends Number> extends AbstractMatrix<T> {

    public AbstractNumberMatrix(Class<T> numberClass) {
        super(numberClass);
    }

    /**
     * Removes <code>0</code>, <code>null</code> and <code>NaN</code> values and updates the
     * rectangle without theses values
     */
    @Override
    protected void updateTrimmedInformation() {
        int height = getHeight();
        int width = getWidth();
        boolean validLineValue = false;
        boolean validColValue = false;
        setTrimStartColumn(width);
        setTrimEndColumn(0);
        setTrimStartLine(0);
        setTrimEndLine(0);

        if (isFlatValue()) {
            // Flat value
            if (Number.class.equals(getType()) || Byte.class.equals(getType()) || Short.class.equals(getType())
                    || Integer.class.equals(getType()) || Long.class.equals(getType()) || Float.class.equals(getType())
                    || Double.class.equals(getType()) || BigInteger.class.equals(getType())) {
                // Flat Number matrix
                Number value;
                Number[] flatValue = (Number[]) getFlatValueUnlocked();
                for (int line = 0; line < height; line++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = flatValue[line * width + col];
                        if ((value != null) && (!Double.isNaN(value.doubleValue())) && (value.doubleValue() != 0)) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(line);
                        }
                        if (getTrimEndLine() <= line) {
                            setTrimEndLine(line);
                        }
                    }
                }
            } else if (Byte.TYPE.equals(getType())) {
                // Flat byte matrix
                byte value;
                byte[] flatValue = (byte[]) getFlatValueUnlocked();
                for (int row = 0; row < height; row++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = flatValue[row * width + col];
                        if (value != 0) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(row);
                        }
                        if (getTrimEndLine() <= row) {
                            setTrimEndLine(row);
                        }
                    }
                }
            } else if (Short.TYPE.equals(getType())) {
                // Flat short matrix
                short value;
                short[] flatValue = (short[]) getFlatValueUnlocked();
                for (int row = 0; row < height; row++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = flatValue[row * width + col];
                        if (value != 0) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(row);
                        }
                        if (getTrimEndLine() <= row) {
                            setTrimEndLine(row);
                        }
                    }
                }
            } else if (Integer.TYPE.equals(getType())) {
                // Flat int matrix
                int value;
                int[] flatValue = (int[]) getFlatValueUnlocked();
                for (int row = 0; row < height; row++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = flatValue[row * width + col];
                        if (value != 0) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(row);
                        }
                        if (getTrimEndLine() <= row) {
                            setTrimEndLine(row);
                        }
                    }
                }
            } else if (Long.TYPE.equals(getType())) {
                // Flat long matrix
                long value;
                long[] flatValue = (long[]) getFlatValueUnlocked();
                for (int row = 0; row < height; row++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = flatValue[row * width + col];
                        if (value != 0) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(row);
                        }
                        if (getTrimEndLine() <= row) {
                            setTrimEndLine(row);
                        }
                    }
                }
            } else if (Float.TYPE.equals(getType())) {
                // Flat float matrix
                float value;
                float[] flatValue = (float[]) getFlatValueUnlocked();
                for (int row = 0; row < height; row++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = flatValue[row * width + col];
                        if ((value != 0) && (!Float.isNaN(value))) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(row);
                        }
                        if (getTrimEndLine() <= row) {
                            setTrimEndLine(row);
                        }
                    }
                }
            } else if (Double.TYPE.equals(getType())) {
                // Flat double matrix
                double value;
                double[] flatValue = (double[]) getFlatValueUnlocked();
                for (int row = 0; row < height; row++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = flatValue[row * width + col];
                        if ((value != 0) && (!Double.isNaN(value))) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(row);
                        }
                        if (getTrimEndLine() <= row) {
                            setTrimEndLine(row);
                        }
                    }
                }
            }
        } else {
            // Classic matrix value
            if (Number.class.equals(getType()) || Byte.class.equals(getType()) || Short.class.equals(getType())
                    || Integer.class.equals(getType()) || Long.class.equals(getType()) || Float.class.equals(getType())
                    || Double.class.equals(getType()) || BigInteger.class.equals(getType())) {
                // Classic Number matrix
                Number value;
                Number[][] m_values = (Number[][]) getValueUnlocked();
                for (int line = 0; line < height; line++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = m_values[line][col];
                        if ((value != null) && (!Double.isNaN(value.doubleValue())) && (value.doubleValue() != 0)) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(line);
                        }
                        if (getTrimEndLine() <= line) {
                            setTrimEndLine(line);
                        }
                    }
                }
            } else if (Byte.TYPE.equals(getType())) {
                // Classic byte matrix
                byte value;
                byte[][] m_values = (byte[][]) getValueUnlocked();
                for (int line = 0; line < height; line++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = m_values[line][col];
                        if (value != 0) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(line);
                        }
                        if (getTrimEndLine() <= line) {
                            setTrimEndLine(line);
                        }
                    }
                }
            } else if (Short.TYPE.equals(getType())) {
                // Classic short matrix
                short value;
                short[][] m_values = (short[][]) getValueUnlocked();
                for (int line = 0; line < height; line++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = m_values[line][col];
                        if (value != 0) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(line);
                        }
                        if (getTrimEndLine() <= line) {
                            setTrimEndLine(line);
                        }
                    }
                }
            } else if (Integer.TYPE.equals(getType())) {
                // Classic int matrix
                int value;
                int[][] m_values = (int[][]) getValueUnlocked();
                for (int line = 0; line < height; line++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = m_values[line][col];
                        if (value != 0) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(line);
                        }
                        if (getTrimEndLine() <= line) {
                            setTrimEndLine(line);
                        }
                    }
                }
            } else if (Long.TYPE.equals(getType())) {
                // Classic long matrix
                long value;
                long[][] m_values = (long[][]) getValueUnlocked();
                for (int line = 0; line < height; line++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = m_values[line][col];
                        if (value != 0) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(line);
                        }
                        if (getTrimEndLine() <= line) {
                            setTrimEndLine(line);
                        }
                    }
                }
            } else if (Float.TYPE.equals(getType())) {
                // Classic float matrix
                float value;
                float[][] m_values = (float[][]) getValueUnlocked();
                for (int line = 0; line < height; line++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = m_values[line][col];
                        if ((value != 0) && (!Float.isNaN(value))) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(line);
                        }
                        if (getTrimEndLine() <= line) {
                            setTrimEndLine(line);
                        }
                    }
                }
            } else if (Double.TYPE.equals(getType())) {
                // Classic double matrix
                double value;
                double[][] m_values = (double[][]) getValueUnlocked();
                for (int line = 0; line < height; line++) {
                    validLineValue = false;
                    for (int col = 0; col < width; col++) {
                        validColValue = false;
                        value = m_values[line][col];
                        if ((value != 0) && (!Double.isNaN(value))) {
                            validLineValue = true;
                            validColValue = true;
                        }
                        if (validColValue) {
                            if (col <= getTrimStartColumn()) {
                                setTrimStartColumn(col);
                            }
                            if (getTrimEndColumn() < col) {
                                setTrimEndColumn(col);
                            }
                        }
                    }
                    if (validLineValue) {
                        if (getTrimStartLine() == 0) {
                            setTrimStartLine(line);
                        }
                        if (getTrimEndLine() <= line) {
                            setTrimEndLine(line);
                        }
                    }
                }
            }
        }
    }

}
