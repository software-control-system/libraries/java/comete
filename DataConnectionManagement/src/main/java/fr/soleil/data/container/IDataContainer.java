/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.container;

import fr.soleil.data.service.GenericDescriptor;

/**
 * The represents something that contains/manipulates some data
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <T> The type of data contained/manipulated by this {@link IDataContainer}
 */
public interface IDataContainer<T> {

    /**
     * Returns the data managed by this {@link IDataContainer}
     * 
     * @return The data managed by this {@link IDataContainer}
     * @throws Exception If some problem occurred when trying to access data
     */
    public T getData() throws Exception;

    /**
     * Sets the data managed by this {@link IDataContainer}
     * 
     * @param data The data to set
     * @throws Exception If some problem occurred when trying to write data
     */
    public void setData(T data) throws Exception;

    /**
     * Returns the writable status of the {@link IDataContainer}, in other words if the method
     * {@link IDataContainer#setData(Object)} can work
     * 
     * @return <code>true</code> if the {@link IDataContainer} can write data, <code>false</code>
     *         otherwise
     */
    public boolean isSettable();

    /**
     * Returns the {@link GenericDescriptor} that describe the type of this {@link IDataContainer}.
     * 
     * @return A {@link GenericDescriptor}.
     */
    public GenericDescriptor getDataType();

    /**
     * Returns whether the data managed by this {@link IDataContainer} is an unsigned number or contains unsigned
     * numbers.
     * 
     * @return
     */
    public boolean isUnsigned();
}
