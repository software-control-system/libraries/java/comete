/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.container.matrix;

import javax.activation.UnsupportedDataTypeException;

/**
 * An {@link AbstractMatrix} that only handles Object elements
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <T> The type of Object
 */
public abstract class ObjectMatrix<T> extends AbstractMatrix<T> {

    public ObjectMatrix(Class<T> type) {
        super(getTransformedClass(type));
    }

    @SuppressWarnings("unchecked")
    @Override
    public T[] getFlatValue() {
        return (T[]) super.getFlatValue();
    }

    @SuppressWarnings("unchecked")
    @Override
    public T[][] getValue() {
        return (T[][]) super.getValue();
    }

    /**
     * Sets this matrix data with a classic matrix. Prefer using this method to using
     * {@link #setValue(Object[])}
     * 
     * @param value The classic matrix
     * @see #setValue(Object[])
     */
    public void setValue(T[][] value) {
        try {
            super.setValue(value);
        } catch (UnsupportedDataTypeException e) {
            // can't happen because of this method signature
        }
    }

    /**
     * Sets this matrix data with a "flat" matrix. Prefer using this method to using
     * {@link #setFlatValue(Object, int, int)}
     * 
     * @param array The flat matrix
     * @param height The flat matrix height
     * @param width The flat matrix width
     * @see #setFlatValue(Object, int, int)
     */
    public void setFlatValue(T[] array, int height, int width) {
        try {
            super.setFlatValue(array, height, width);
        } catch (UnsupportedDataTypeException e) {
            // can't happen because of this method signature
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public T[][] initValue(int height, int width) {
        return (T[][]) super.initValue(height, width);
    }

    @SuppressWarnings("unchecked")
    @Override
    public T[] initArray(int size) {
        return (T[]) super.initArray(size);
    }

    @SuppressWarnings("unchecked")
    protected static <U> Class<U> getTransformedClass(Class<U> type) {
        Class<U> transformed = type;
        if (Boolean.TYPE.equals(type)) {
            transformed = (Class<U>) Boolean.class;
        } else if (Character.TYPE.equals(type)) {
            transformed = (Class<U>) Character.class;
        } else if (Byte.TYPE.equals(type)) {
            transformed = (Class<U>) Byte.class;
        } else if (Short.TYPE.equals(type)) {
            transformed = (Class<U>) Short.class;
        } else if (Integer.TYPE.equals(type)) {
            transformed = (Class<U>) Integer.class;
        } else if (Long.TYPE.equals(type)) {
            transformed = (Class<U>) Long.class;
        } else if (Float.TYPE.equals(type)) {
            transformed = (Class<U>) Float.class;
        } else if (Double.TYPE.equals(type)) {
            transformed = (Class<U>) Double.class;
        }
        return transformed;
    }

    @Override
    public ObjectMatrix<T> clone() {
        return (ObjectMatrix<T>) super.clone();
    }

}
