/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.container.matrix;

import java.lang.reflect.Array;

import fr.soleil.data.container.AbstractNamedObject;

/**
 * This class is used to store the flat representation of a matrix
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <T> The flat array type
 */
public class FlatMatrix<T> extends AbstractNamedObject {

    private int columns;
    private T value;

    public FlatMatrix() {
        columns = 0;
        value = null;
    }

    /**
     * Sets the flat value
     * 
     * @param columns The number of columns in the matrix
     * @param value The matrix flat value
     */
    public synchronized void setValue(int columns, T value) {
        if (value == null) {
            this.columns = 0;
            this.value = value;
        } else if ((columns > -1) && value.getClass().isArray()) {
            if (columns == 0) {
                if (Array.getLength(value) == 0) {
                    this.columns = columns;
                    this.value = value;
                }
            } else if (Array.getLength(value) % columns == 0) {
                this.columns = columns;
                this.value = value;
            }
        }
    }

    /**
     * Returns the matrix flat value
     * 
     * @return The matrix flat value
     */
    public T getValue() {
        return value;
    }

    /**
     * Returns the number of columns in the matrix.
     * 
     * @return An int
     */
    public int getColumns() {
        return columns;
    }

}
