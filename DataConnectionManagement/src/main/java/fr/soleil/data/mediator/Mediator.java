/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.mediator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.information.TargetInformation;

/**
 * This class represents something that makes an indirect link between some
 * {@link AbstractDataSource}s and some {@link ITarget}s. It follows the "mediator" design pattern.
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <U> The type of data managed by the {@link AbstractDataSource}s this {@link Mediator} is
 *            connected to
 */
public interface Mediator<U> {

    /**
     * {@link String} to recover the {@link Logger} used by the {@link Mediator}s, through
     * {@link LoggerFactory#getLogger(String)}
     */
    public static final String LOGGER_ACCESS = "DataConnectionManagement";

    /**
     * Adds a link between an {@link ITarget} and an {@link AbstractDataSource}
     * 
     * @param source The {@link AbstractDataSource} to link
     * @param target the {@link ITarget} to link
     */
    public boolean addLink(AbstractDataSource<U> source, ITarget target);

    /**
     * Removes a link between an {@link ITarget} and an {@link AbstractDataSource}
     * 
     * @param source The {@link AbstractDataSource} to unlink
     * @param target The {@link ITarget} to unlink
     */
    public void removeLink(AbstractDataSource<U> source, ITarget target);

    /**
     * Removes all the links to an {@link AbstractDataSource}
     * 
     * @param source The {@link AbstractDataSource} to unlink
     */
    public void removeAllReferencesTo(AbstractDataSource<U> source);

    /**
     * Removes all the links to an {@link ITarget}
     * 
     * @param target The {@link ITarget} to unlink
     */
    public void removeAllReferencesTo(ITarget target);

    /**
     * Transmits a change in an {@link AbstractDataSource}
     * 
     * @param source The {@link AbstractDataSource} that notifies this {@link Mediator}
     */
    public void transmitSourceChange(AbstractDataSource<U> source);

    /**
     * Transmits an information about a {@link ITarget}
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> A type of {@link TargetInformation} that is compatible with the {@link ITarget}s
     *            the mediator manages
     * @param targetInformation The {@link TargetInformation} that explains what happened in which
     *            {@link ITarget}
     */
    public <I, V extends TargetInformation<I>> void transmitTargetChange(V targetInformation);

    /**
     * Adds an {@link AbstractDataFilter} to an {@link ITarget}
     * 
     * @param <V> The type of data the {@link AbstractDataFilter} can filter
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     */
    public <V> void addFilterToTarget(AbstractDataFilter<V> filter, ITarget target);

    /**
     * Removes an {@link AbstractDataFilter} for a {@link ITarget}
     * 
     * @param <V> The type of data the {@link AbstractDataFilter} can filter
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     */
    public <V> void removeFilterFromTarget(AbstractDataFilter<V> filter, ITarget target);

    /**
     * Adds an {@link AbstractDataFilter} to an {@link AbstractDataSource}
     * 
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link AbstractDataSource}
     */
    public void addFilterToSource(AbstractDataFilter<U> filter, AbstractDataSource<U> source);

    /**
     * Removes an {@link AbstractDataFilter} for a {@link AbstractDataSource}
     * 
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link AbstractDataSource}
     */
    public void removeFilterFromSource(AbstractDataFilter<U> filter, AbstractDataSource<U> source);

}
