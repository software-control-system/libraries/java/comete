/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.mediator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;

import javax.swing.event.AncestorEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.filter.AbstractChecker;
import fr.soleil.data.filter.AbstractChecker.CheckingMode;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.ILoggingSystem;
import fr.soleil.data.service.LoggingSystemDelegate;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IErrorNotifiableTarget;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.util.DataConnectionUtils;

/**
 * An abstract class that gives some basic implementations for {@link Mediator}s
 * 
 * @author Rapha&euml;l GIRARDOT, huriez
 * 
 * @param <U> The type of data managed by the {@link AbstractDataSource}s this {@link Mediator} is
 *            connected to
 */
public abstract class AbstractController<U> implements Mediator<U>, ILoggingSystem {

    /**
     * Constant only used to keep this Logger alive (WeakReference in java code)
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(LOGGER_ACCESS);

    /**
     * A {@link Map} used to know with which {@link ITarget}s an {@link AbstractDataSource} is
     * linked
     */
    protected final Map<AbstractDataSource<U>, List<ITarget>> sourceAssociations;

    /**
     * A {@link Map} used to know with which {@link AbstractDataSource}s an {@link ITarget} is
     * linked
     */
    protected final Map<ITarget, List<AbstractDataSource<U>>> targetAssociations;

    /**
     * A {@link Map} used to know with which {@link AbstractDataFilter}s an {@link AbstractDataSource} is linked
     */
    protected final Map<AbstractDataSource<U>, AbstractDataFilter<U>> sourceFilters;

    /**
     * A {@link Map} used to store {@link AbstractChecker}s an {@link AbstractDataSource} needs to
     * validate.
     */
    protected final Map<AbstractDataSource<U>, List<AbstractChecker>> sourceCheckers;

    /**
     * A {@link Map} to store whether targets may be cancelled.
     */
    protected final Map<ITarget, Boolean> cancelableTargets;

    /**
     * A {@link Map} to store container data type in order to known whether unsigned conversion should be undone when
     * targets send data to sources.
     */
    // XXX SCAN-853: Used for source writing, but maybe not perfect yet
    protected final Map<IDataContainer<?>, Class<?>> dataTypeMap;

    /**
     * Allow or not the initialization of the linked {@link ITarget} when you make an
     * {@link AbstractController#addLink(AbstractDataSource, ITarget)}.
     */
    protected boolean firstInitAllowed;

    protected final LoggingSystemDelegate logger;

    public AbstractController() {
        super();
        sourceAssociations = new HashMap<>();
        targetAssociations = new HashMap<>();
        sourceFilters = new HashMap<>();
        // Use WeakHashMaps to avoid memory leaks (JAVAAPI-591, PROBLEM-1938)
        sourceCheckers = new WeakHashMap<>();
        cancelableTargets = new WeakHashMap<>();
        // XXX SCAN-853: As long as there is no ConcurrentWeakHashMap, we call Collections.synchronizedMap
        dataTypeMap = Collections.synchronizedMap(new WeakHashMap<>());
        firstInitAllowed = true;
        logger = new LoggingSystemDelegate();
    }

    /**
     * Returns whether a given {@link ITarget} can be canceled
     * 
     * @param target The {@link ITarget}
     * @return A <code>boolean</code> value
     * @see #setCancelable(ITarget, boolean)
     */
    public boolean isCancelable(ITarget target) {
        boolean cancelable;
        if (target == null) {
            cancelable = false;
        } else {
            Boolean tmp;
            synchronized (cancelableTargets) {
                tmp = cancelableTargets.get(target);
            }
            if (tmp == null) {
                cancelable = true;
            } else {
                cancelable = tmp.booleanValue();
            }
        }
        return cancelable;
    }

    /**
     * Sets whether a given {@link ITarget} can be canceled. If target can be canceled if its
     * content can be changed, depending on its associated {@link AbstractDataSource}s, on
     * {@link #transmitTargetChange(fr.soleil.data.target.information.TargetInformation)}, when a
     * source {@link AbstractChecker} refuses the change
     * 
     * @param target The {@link ITarget}
     * @param cancelable Whether the {@link ITarget} is cancelable
     */
    public void setCancelable(ITarget target, boolean cancelable) {
        if (target != null) {
            synchronized (cancelableTargets) {
                cancelableTargets.put(target, Boolean.valueOf(cancelable));
            }
        }
    }

    @Override
    public boolean addLink(AbstractDataSource<U> source, ITarget target) {
        boolean result = false;
        if ((source != null) && (target != null)) {
            if (isTargetAssignable(target)) {
                synchronized (sourceAssociations) {
                    List<ITarget> targetList = sourceAssociations.get(source);
                    if (targetList == null) {
                        targetList = new ArrayList<>();
                        sourceAssociations.put(source, targetList);
                        registerAdapter(source);
                        source.addMediator(this);
                    }
                    synchronized (targetList) {
                        if (!targetList.contains(target)) {
                            targetList.add(target);
                        }
                    }
                }
                synchronized (targetAssociations) {
                    List<AbstractDataSource<U>> srcList = targetAssociations.get(target);
                    if (srcList == null) {
                        srcList = new ArrayList<>();
                        targetAssociations.put(target, srcList);
                        target.addMediator(this);
                    }
                    synchronized (srcList) {
                        if (!srcList.contains(source)) {
                            srcList.add(source);
                        }
                    }
                }
                if (firstInitAllowed && source.isInitialized()) {
                    transmitSourceChange(source, target);
                }
                result = true;
            }
        }
        return result;
    }

    /**
     * Returns whether this {@link AbstractController} is able to transmit some data to an {@link ITarget}
     * 
     * @param target The {@link ITarget} to check
     * @return A <code>boolean</code>
     */
    public abstract boolean isTargetAssignable(ITarget target);

    @Override
    public void removeAllReferencesTo(AbstractDataSource<U> source) {
        if (source != null) {
            synchronized (sourceAssociations) {
                List<ITarget> targetList = sourceAssociations.get(source);
                if (targetList != null) {
                    synchronized (targetAssociations) {
                        for (ITarget target : targetList) {
                            removeLinkForTarget(source, target);
                        }
                    }
                }
                sourceAssociations.remove(source);
                unregisterAdapter(source);
            }
            source.removeMediator(this);
        }
    }

    @Override
    public void removeAllReferencesTo(ITarget target) {
        if (target != null) {
            synchronized (targetAssociations) {
                List<AbstractDataSource<U>> srcList = targetAssociations.get(target);
                if (srcList != null) {
                    synchronized (sourceAssociations) {
                        for (AbstractDataSource<U> source : srcList) {
                            removeLinkForSource(source, target);
                        }
                    }
                }
                targetAssociations.remove(target);
            }
            target.removeMediator(this);
        }
    }

    private void removeLinkForSource(AbstractDataSource<U> source, ITarget target) {
        List<ITarget> targetList = sourceAssociations.get(source);
        if (targetList != null) {
            synchronized (targetList) {
                targetList.remove(target);
                if (targetList.isEmpty()) {
                    sourceAssociations.remove(source);
                    unregisterAdapter(source);
                    source.removeMediator(this);
                }
            }
        }
    }

    private void removeLinkForTarget(AbstractDataSource<U> source, ITarget target) {
        List<AbstractDataSource<U>> srcList = targetAssociations.get(target);
        if (srcList != null) {
            synchronized (srcList) {
                srcList.remove(source);
                if (srcList.isEmpty()) {
                    targetAssociations.remove(target);
                    target.removeMediator(this);
                }
            }
        }
    }

    @Override
    public void removeLink(AbstractDataSource<U> source, ITarget target) {
        if (source != null && target != null) {
            synchronized (sourceAssociations) {
                removeLinkForSource(source, target);
            }
            synchronized (targetAssociations) {
                removeLinkForTarget(source, target);
            }
        }
    }

    /**
     * Check if the link between the {@link AbstractDataSource} and the {@link ITarget} is
     * registered.
     * 
     * @param source
     * @param target
     * @return true if the connection exists, false otherwise
     */
    public boolean hasLink(AbstractDataSource<U> source, ITarget target) {
        boolean result = false;
        if (source != null && target != null) {
            synchronized (sourceAssociations) {
                result = sourceAssociations.containsKey(source);
            }
            synchronized (targetAssociations) {
                result &= targetAssociations.containsKey(target);
            }
        }
        return result;
    }

    @Override
    public <V> void addFilterToTarget(AbstractDataFilter<V> filter, ITarget target) {
        addFilterToTarget(filter, target, true);
    }

    public <V> void addFilterToTarget(AbstractDataFilter<V> filter, ITarget target, boolean updateContent) {
        if (isFilterCompatibleWithTarget(filter, target)) {
            if (connectFilterToTarget(filter, target) && updateContent) {
                // connection successful: refresh target content
                List<AbstractDataSource<U>> sources = getAllRelatedSources(target);
                for (AbstractDataSource<U> source : sources) {
                    transmitSourceChange(source);
                }
            }
        }
    }

    /**
     * Tests whether an {@link AbstractDataFilter} is compatible with an {@link ITarget}
     * 
     * @param <V> The type of data the {@link AbstractDataFilter} manages
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     * @return A boolean value. <code>TRUE</code> if the {@link AbstractDataFilter} is compatible
     *         with the {@link ITarget}, <code>FALSE</code> otherwise
     */
    protected abstract <V> boolean isFilterCompatibleWithTarget(AbstractDataFilter<V> filter, ITarget target);

    /**
     * Does the connection between and {@link AbstractDataFilter} and {@link AncestorEvent} {@link ITarget} . In this
     * method, the {@link AbstractDataFilter} and the {@link ITarget} are
     * known to be compatible with each other.
     * 
     * @param <V> The type of data the {@link AbstractDataFilter} manages
     * @param filter The {@link AbstractDataFilter}
     * @param target The {@link ITarget}
     * @return Whether filter was successfully connected
     */
    protected abstract <V> boolean connectFilterToTarget(AbstractDataFilter<V> filter, ITarget target);

    @Override
    public void addFilterToSource(AbstractDataFilter<U> filter, AbstractDataSource<U> source) {
        addFilterToSource(filter, source, true);
    }

    public void addFilterToSource(AbstractDataFilter<U> filter, AbstractDataSource<U> source, boolean notify) {
        if ((filter != null) && (source != null)) {
            boolean success = false;
            synchronized (sourceFilters) {
                AbstractDataFilter<U> associated = sourceFilters.get(source);
                if (!filter.equals(associated)) {
                    removeFilterFromSource(associated, source, false);
                    updateFilter(filter, source);
                    sourceFilters.put(source, filter);
                    success = true;
                }
            }
            // filter successfully added: transmit filtered data
            if (notify && success) {
                transmitSourceChange(source);
            }
        }
    }

    @Override
    public void removeFilterFromSource(AbstractDataFilter<U> filter, AbstractDataSource<U> source) {
        removeFilterFromSource(filter, source, true);
    }

    public void removeFilterFromSource(AbstractDataFilter<U> filter, AbstractDataSource<U> source, boolean notify) {
        if ((filter != null) && (source != null)) {
            boolean success = false;
            synchronized (sourceFilters) {
                if (filter.equals(sourceFilters.get(source))) {
                    sourceFilters.remove(source);
                    updateFilter(filter, null);
                    success = true;
                }
            }
            // filter successfully removed: transmit unfiltered data
            if (notify && success) {
                transmitSourceChange(source);
            }
        }
    }

    /**
     * Search for the target association list of {@link AbstractDataSource} and add the {@link AbstractDataFilter}
     * object to them.
     * 
     * @param filter the {@link AbstractDataFilter} to add.
     * @param target the {@link ITarget} to check for the {@link AbstractDataSource} list.
     */
    public void addFilterToSourceFromTarget(AbstractDataFilter<U> filter, ITarget target) {
        if ((filter != null) && (target != null)) {
            List<AbstractDataSource<U>> sourceList = getAllRelatedSources(target);
            for (AbstractDataSource<U> source : sourceList) {
                addFilterToSource(filter, source);
            }
        }
    }

    /**
     * Search for the target association list of {@link AbstractDataSource} and remove the {@link AbstractDataFilter}
     * object from them.
     * 
     * @param filter the {@link AbstractDataFilter} to remove.
     * @param target the {@link ITarget} to check for the {@link AbstractDataSource} list.
     */
    public void removeFilterFromSourceFromTarget(AbstractDataFilter<U> filter, ITarget target) {
        if ((filter != null) && (target != null)) {
            List<AbstractDataSource<U>> sourceList = null;
            synchronized (targetAssociations) {
                sourceList = targetAssociations.get(target);
            }
            if (sourceList != null) {
                for (AbstractDataSource<U> source : sourceList) {
                    removeFilterFromSource(filter, source);
                }
            }
        }
    }

    /**
     * Updates a filter to set its top source
     * 
     * @param filter The {@link AbstractDataFilter} to update
     * @param source The source to set
     */
    protected <T> void updateFilter(AbstractDataFilter<T> filter, IDataContainer<T> source) {
        if (filter != null) {
            IDataContainer<T> dataContainer = filter;
            while (dataContainer instanceof AbstractDataFilter<?>) {
                filter = (AbstractDataFilter<T>) dataContainer;
                dataContainer = filter.getDataContainer();
            }
            filter.setDataContainer(source);
        }
    }

    /**
     * Connect an {@link AbstractChecker} to an {@link AbstractDataSource}.
     * 
     * @param checker the {@link AbstractChecker} to put
     * @param source the linked {@link AbstractDataSource}
     */
    public void addCheckerToSource(AbstractChecker checker, AbstractDataSource<U> source) {
        if ((checker != null) && (source != null)) {
            List<AbstractChecker> currentListCheckers = sourceCheckers.get(source);
            if (currentListCheckers == null) {
                currentListCheckers = new ArrayList<>();
                sourceCheckers.put(source, currentListCheckers);
            }
            if (!currentListCheckers.contains(checker)) {
                currentListCheckers.add(checker);
            }
        }
    }

    public List<AbstractChecker> getSourceCheckers(AbstractDataSource<U> source) {
        return source == null ? null : sourceCheckers.get(source);
    }

    /**
     * Disconnect the {@link AbstractChecker} from its {@link AbstractDataSource}.
     * 
     * @param source the {@link AbstractDataSource} to disconnect.
     * @param checker the {@link AbstractChecker} to remove
     */
    public void removeCheckerFromSource(AbstractDataSource<U> source, AbstractChecker checker) {
        if (source != null) {
            List<AbstractChecker> currentListCheckers = sourceCheckers.get(source);
            if (currentListCheckers != null) {
                currentListCheckers.remove(checker);
            }
        }
    }

    /**
     * Verify if the donate {@link AbstractDataSource} passes all associated checks.
     * 
     * @param source the {@link AbstractDataSource} to verify
     * @return false if the {@link AbstractDataSource} don't passes all checks, true otherwise
     */
    protected boolean isSourceChecked(AbstractDataSource<U> source, Object data, CheckingMode mode) {
        boolean result = true;
        if (source != null) {
            List<AbstractChecker> checkers = sourceCheckers.get(source);
            if (checkers != null) {
                switch (mode) {
                    case INPUT_MODE:
                        for (AbstractChecker checker : checkers) {
                            result &= checker.getInputDataCheck(source, data);
                        }
                        break;
                    case OUTPUT_MODE:
                        for (AbstractChecker checker : checkers) {
                            result &= checker.getOutputDataCheck(source, data);
                        }
                        break;
                }
            }
        }
        return result;
    }

    /**
     * @return the firstInitAllowed
     */
    public boolean isFirstInitAllowed() {
        return firstInitAllowed;
    }

    /**
     * @param firstInitAllowed the firstInitAllowed to set
     */
    public void setFirstInitAllowed(boolean firstInitAllowed) {
        this.firstInitAllowed = firstInitAllowed;
    }

    /**
     * Registers an {@link DataSourceAdapter} for an {@link AbstractDataSource}. This happens when
     * the {@link AbstractDataSource} is encountered for the first time in this {@link AbstractController}
     * 
     * @param source The {@link AbstractDataSource}
     */
    protected abstract void registerAdapter(AbstractDataSource<U> source);

    /**
     * Unregisters the {@link DataSourceAdapter} for an {@link AbstractDataSource}. This happens
     * when the {@link AbstractDataSource} is no more referenced in this {@link AbstractController}
     * 
     * @param source The {@link AbstractDataSource}
     */
    protected abstract void unregisterAdapter(AbstractDataSource<U> source);

    /**
     * Return true if the {@link ITarget} can receive informations. For a {@link IComponent} target,
     * it represents the fact that the {@link IComponent} has not the focus.
     * 
     * @return result True if the {@link ITarget} is the one concerned by the focus.
     */
    protected boolean isTargetSettable(ITarget target) {
        return true;
    }

    @Override
    public void transmitSourceChange(AbstractDataSource<U> source) {
        try {
            if ((source != null) && isSourceChecked(source, source.getData(), CheckingMode.OUTPUT_MODE)) {
                List<ITarget> targets = getAllRelatedTargets(source);
                AbstractDataFilter<U> filter;
                synchronized (sourceFilters) {
                    filter = sourceFilters.get(source);
                }
                transmitDataToTarget(source, filter, targets);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to transmit " + source + " data to targets: could not check source", e);
        }
    }

    protected void transmitSourceChange(AbstractDataSource<U> source, ITarget target) {
        try {
            if ((source != null) && (target != null)
                    && isSourceChecked(source, source.getData(), CheckingMode.OUTPUT_MODE)) {
                AbstractDataFilter<U> filter;
                synchronized (sourceFilters) {
                    filter = sourceFilters.get(source);
                }
                transmitDataToTarget(source, filter, Arrays.asList(target));
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to transmit " + source + " data to targets: could not check source", e);
        }
    }

    /**
     * Transmits some source data to a target list
     * 
     * @param source The source
     * @param associatedFilter The filter used to filter source data
     * @param targets The target list
     */
    protected abstract void transmitDataToTarget(AbstractDataSource<U> source, AbstractDataFilter<U> associatedFilter,
            List<ITarget> targets);

    /**
     * Default way to treat an error that happened on transmitting data to source from target.
     * 
     * @param origin The target.
     * @param source The source.
     * @param data The data.
     * @param error The error.
     */
    protected void treatSourceDataSendingError(ITarget origin, AbstractDataSource<U> source, Object data,
            Throwable error) {
        StringBuilder errorMessageBuilder = new StringBuilder("Failed to transmit ");
        if ((data == null) || (data instanceof Boolean) || (data instanceof Number)) {
            errorMessageBuilder.append(data);
        } else if ((data instanceof String) || (data instanceof Character)) {
            errorMessageBuilder.append('\'').append(data).append('\'');
        } else {
            errorMessageBuilder.append("data");
        }
        errorMessageBuilder.append(" to source ").append(source);
        String errorMessage = errorMessageBuilder.toString();
        errorMessageBuilder.delete(0, errorMessageBuilder.length());
        logger.log(Level.SEVERE, errorMessage, error);
        threadedCancelSourceDataSetting(source);
        if (origin instanceof IErrorNotifiableTarget) {
            ((IErrorNotifiableTarget) origin).notifyForError(errorMessage, error);
        }
    }

    protected U getAdaptedData(U data, IDataContainer<?>... containers) {
        U adaptedData = data;
        if ((data != null) && (containers != null)) {
            // XXX SCAN-853: This part might not work perfectly yet
            boolean convert = false;
            for (IDataContainer<?> container : containers) {
                if ((container != null) && container.isUnsigned()) {
                    Class<?> dataClass = dataTypeMap.get(container);
                    if ((dataClass != null) && (!dataClass.equals(data.getClass()))) {
                        convert = true;
                        break;
                    }
                }
            }
            if (convert) {
                @SuppressWarnings("unchecked")
                U tmp = (U) DataConnectionUtils.undoUnsignedConversion(data);
                adaptedData = tmp;
            }
        }
        return adaptedData;
    }

    /**
     * Transmits some data to an {@link AbstractDataSource}, using its filters if necessary
     * 
     * @param data The data to transmit
     * @param source The concerned {@link AbstractDataSource}
     */
    protected void transmitDataToSource(U data, AbstractDataSource<U> source, ITarget origin) {
        if (source != null) {
            U adaptedData = data;
            AbstractDataFilter<U> filter;
            synchronized (sourceFilters) {
                filter = sourceFilters.get(source);
            }
            try {
                adaptedData = getAdaptedData(adaptedData, filter, source);
                if (filter == null) {
                    source.setData(adaptedData);
                } else {
                    filter.setData(adaptedData);
                }
                if (source.isWriteBeforeRead() && source.hasSomeDataToReturn()) {
                    transmitSourceChange(source, origin);
                }
            } catch (Exception e) {
                treatSourceDataSendingError(origin, source, adaptedData, e);
            }
        }
    }

    protected void threadedCancelSourceDataSetting(final AbstractDataSource<U> source) {
        new Thread("Cancel source '" + source + "' data edition") {
            @Override
            public void run() {
                transmitSourceChange(source);
            }
        }.start();
    }

    /**
     * Returns a copy of the list of all {@link ITarget} treated by this {@link AbstractController}.
     * 
     * @return A {@link List}
     */
    public List<ITarget> getTargetList() {
        List<ITarget> result = new ArrayList<>();
        synchronized (targetAssociations) {
            result.addAll(targetAssociations.keySet());
        }
        return result;
    }

    /**
     * Returns a copy of the list of all {@link AbstractDataSource}s treated by this {@link AbstractController}
     * 
     * @return A {@link List}
     */
    public List<AbstractDataSource<U>> getSourceList() {
        List<AbstractDataSource<U>> result = new ArrayList<>();
        synchronized (sourceAssociations) {
            result.addAll(sourceAssociations.keySet());
        }
        return result;
    }

    /**
     * @return the {@link GenericDescriptor} corresponding to the source type
     */
    public abstract GenericDescriptor getSourceType();

    /**
     * Returns a copy of the list of all {@link AbstractDataSource} linked to the {@link ITarget} parameter.
     * 
     * @param target The {@link ITarget} to check
     * @return A {@link List}
     */
    public List<AbstractDataSource<U>> getAllRelatedSources(ITarget target) {
        List<AbstractDataSource<U>> result = new ArrayList<>();
        synchronized (targetAssociations) {
            List<AbstractDataSource<U>> temp = targetAssociations.get(target);
            if (temp != null) {
                synchronized (temp) {
                    result.addAll(temp);
                }
            }
        }
        return result;
    }

    /**
     * @return the list of all {@link ITarget} linked to the {@link AbstractDataSource} parameter.
     * @param source the {@link AbstractDataSource} to check
     */
    public List<ITarget> getAllRelatedTargets(AbstractDataSource<U> source) {
        List<ITarget> result = new ArrayList<>();
        synchronized (sourceAssociations) {
            List<ITarget> temp = sourceAssociations.get(source);
            if (temp != null) {
                synchronized (temp) {
                    result.addAll(temp);
                }
            }
        }
        return result;
    }

    /**
     * @param target the {@link ITarget} to check.
     * @return true if this {@link AbstractController} contains the {@link ITarget}, false
     *         otherwise.
     */
    public boolean contains(ITarget target) {
        boolean result = false;
        if (target != null) {
            synchronized (targetAssociations) {
                result = targetAssociations.containsKey(target);
            }
        }
        return result;
    }

    @Override
    public void addLogger(String loggerId) {
        logger.addLogger(loggerId);
    }

    @Override
    public void removeLogger(String loggerId) {
        logger.removeLogger(loggerId);
    }

}
