/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.CompositeKey;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.information.TargetInformation;

/**
 * An {@link AbstractDataSource} for basic interaction
 * 
 * @author viguier
 * 
 * @param <T> The type of data this {@link MultiDataSource} can manipulate
 */
public class MultiDataSource extends AbstractDataSource<List<Object>> {

    protected String label;

    protected static final GenericDescriptor DATA_TYPE = new GenericDescriptor(List.class,
            new GenericDescriptor[] { new GenericDescriptor(Object.class) });

    protected final List<AbstractDataSource<?>> dataList;
    protected final Map<GenericDescriptor, FakeMediator<?>> mediators;
    protected final FakeTarget fakeTarget;

    public MultiDataSource(IKey key) {
        super(key);
        dataList = new ArrayList<AbstractDataSource<?>>();
        mediators = new HashMap<GenericDescriptor, FakeMediator<?>>();
        fakeTarget = new FakeTarget();
    }

    @SuppressWarnings("unchecked")
    protected <T> FakeMediator<T> getMediator(AbstractDataSource<T> source) {
        FakeMediator<T> result = null;
        if (source != null) {
            GenericDescriptor descriptor = source.getDataType();
            if (descriptor != null) {
                result = (FakeMediator<T>) mediators.get(descriptor);
                if (result == null) {
                    result = new FakeMediator<T>(descriptor);
                    mediators.put(descriptor, result);
                }
            }
        }
        return result;
    }

    public <T> void addSource(AbstractDataSource<T> source, int index) {
        if (source != null) {
            synchronized (dataList) {
                int adaptedIndex = index;
                if (adaptedIndex < 0) {
                    adaptedIndex = 0;
                }
                if (adaptedIndex > dataList.size()) {
                    adaptedIndex = dataList.size();
                }
                dataList.add(adaptedIndex, source);
                FakeMediator<T> mediator = getMediator(source);
                if (mediator != null) {
                    mediator.addLink(source, fakeTarget);
                }
                if (index == CompositeKey.Y_INDEX) {
                    label = source.toString();
                }
            }
        }
    }

    public <T> void removeSource(AbstractDataSource<T> source) {
        synchronized (dataList) {
            dataList.remove(source);
        }
    }

    @Override
    public List<Object> getData() throws Exception {
        List<Object> result = new ArrayList<Object>();
        synchronized (dataList) {
            for (AbstractDataSource<?> source : dataList) {
                result.add(source.getData());
            }
        }
        return result;
    }

    @Override
    public void setData(List<Object> data) {
        // NOTHING TO DO
    }

    @Override
    public String toString() {
        return (label == null ? super.toString() : label);
    }

    @Override
    public boolean isSettable() {
        return false;
    }

    @Override
    public GenericDescriptor getDataType() {
        // GenericDescriptor result = null;
        // GenericDescriptor comparator = null;
        // boolean compareResult = false;
        // for (AbstractDataSource<?> aData : dataList) {
        // GenericDescriptor tempDesc = aData.getDataType();
        // if (comparator != null) {
        // compareResult &= comparator.equals(tempDesc);
        // }
        // else {
        // compareResult = true;
        // }
        // comparator = tempDesc;
        // }
        // if (compareResult) {
        // result = comparator;
        // }
        return DATA_TYPE;
    }

    public void clean() {
        synchronized (dataList) {
            for (FakeMediator<?> mediator : mediators.values()) {
                mediator.removeAllReferencesTo(fakeTarget);
            }
            mediators.clear();
            dataList.clear();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        clean();
        super.finalize();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link Mediator} used to connect this {@link MultiDataSource} to an encapsulated {@link AbstractDataSource}
     * 
     * @author Rapha&euml;l GIRARDOT
     * 
     * @param <U> The type of data managed by the {@link AbstractDataSources} this {@link Mediator} is connected to
     */
    protected class FakeMediator<T> implements Mediator<T> {
        private final GenericDescriptor descriptor;
        private final List<AbstractDataSource<T>> sources;

        public FakeMediator(GenericDescriptor descriptor) {
            this.descriptor = descriptor;
            sources = new ArrayList<AbstractDataSource<T>>();
        }

        public GenericDescriptor getDescriptor() {
            return descriptor;
        }

        @Override
        public boolean addLink(AbstractDataSource<T> source, ITarget target) {
            boolean result = false;
            if (target == fakeTarget) {
                synchronized (sources) {
                    result = true;
                    if (!sources.contains(source)) {
                        sources.add(source);
                        source.addMediator(this);
                    }
                }
            }
            return result;
        }

        @Override
        public void removeLink(AbstractDataSource<T> source, ITarget target) {
            if (target == fakeTarget) {
                removeAllReferencesTo(source);
            }
        }

        @Override
        public void removeAllReferencesTo(AbstractDataSource<T> source) {
            synchronized (sources) {
                sources.remove(source);
                source.removeMediator(this);
            }
        }

        @Override
        public void removeAllReferencesTo(ITarget target) {
            if (target == fakeTarget) {
                synchronized (sources) {
                    while (!sources.isEmpty()) {
                        removeAllReferencesTo(sources.get(0));
                    }
                }
            }
        }

        @Override
        public void transmitSourceChange(AbstractDataSource<T> source) {
            notifyMediators();
        }

        @Override
        public <I, V extends TargetInformation<I>> void transmitTargetChange(V targetInformation) {
            // Nothing to do
        }

        @Override
        public <V> void addFilterToTarget(AbstractDataFilter<V> filter, ITarget target) {
            // Not managed
        }

        @Override
        public <V> void removeFilterFromTarget(AbstractDataFilter<V> filter, ITarget target) {
            // Not managed
        }

        @Override
        public void addFilterToSource(AbstractDataFilter<T> filter, AbstractDataSource<T> source) {
            // Not managed
        }

        @Override
        public void removeFilterFromSource(AbstractDataFilter<T> filter, AbstractDataSource<T> source) {
            // Not managed
        }
    }

    /**
     * A fake {@link ITarget} used to update this {@link MultiDataSource} content
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class FakeTarget implements ITarget {

        @Override
        public void addMediator(Mediator<?> mediator) {
            // Nothing to do
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // Nothing to do
        }

    }

}
