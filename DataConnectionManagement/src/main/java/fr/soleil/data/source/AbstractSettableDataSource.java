/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import fr.soleil.data.service.IKey;

public abstract class AbstractSettableDataSource<T> extends AbstractDataSource<T> {

    private boolean settable;

    public AbstractSettableDataSource(IKey key) {
        super(key);
        settable = true;
    }

    public void setSettable(boolean settable) {
        this.settable = settable;
    }

    @Override
    public boolean isSettable() {
        return settable;
    }

    @Override
    public void setData(T data) throws Exception {
        if (isSettable()) {
            doSetData(data);
        }
    }

    /**
     * Does the data setting
     * 
     * @param data the data to set
     */
    protected abstract void doSetData(T data) throws Exception;

}
