/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.ObjectUtils;

/**
 * This class represents a data source.
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <T> The type of data
 */
public abstract class AbstractDataSource<T> implements IDataContainer<T> {

    protected List<Mediator<T>> mediators;
    protected final IKey originDescriptor;

    /**
     * Constructor
     * 
     * @param originDescriptor The {@link IKey} that identifies this {@link AbstractDataSource}
     */
    public AbstractDataSource(IKey originDescriptor) {
        mediators = new ArrayList<Mediator<T>>();
        this.originDescriptor = (originDescriptor == null ? null : originDescriptor.clone());
    }

    /**
     * Returns whether this source is already initialized. If not, there is no point in reading its
     * data
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isInitialized() {
        // by default, a source is supposed to be initialized
        return true;
    }

    /**
     * Returns whether this source must be written before getting read.
     * 
     * @return A boolean. If <code>true</code>, then {@link #notifyMediators()} won't do anything, as only the target
     *         that wrote this source may receive data
     */
    public boolean isWriteBeforeRead() {
        return false;
    }

    /**
     * Return whether this source has some data to return.
     * 
     * @returnA boolean. If <code>false</code>, then {@link #notifyMediators()} won't do anything, as this source has no
     *          data to transmit to targets
     */
    public boolean hasSomeDataToReturn() {
        return true;
    }

    /**
     * Returns the {@link IKey} that allowed to construct this {@link AbstractDataSource}
     * 
     * @return An {@link IKey}
     */
    public IKey getOriginDescriptor() {
        return originDescriptor;
    }

    /**
     * Links this {@link AbstractDataSource} with a {@link Mediator}
     * 
     * @param mediator The {@link Mediator} that should be linked with this {@link AbstractDataSource}
     */
    public void addMediator(Mediator<T> mediator) {
        if (mediator != null) {
            synchronized (mediators) {
                if (!mediators.contains(mediator)) {
                    DataSourceProducerProvider.registerMediatorLink(getOriginDescriptor(), mediator);
                    mediators.add(mediator);
                }
            }
        }
    }

    /**
     * Unlinks this {@link AbstractDataSource} from a {@link Mediator}
     * 
     * @param mediator The {@link Mediator} that should be unlinked from this {@link AbstractDataSource}
     */
    public void removeMediator(Mediator<T> mediator) {
        if (mediator != null) {
            synchronized (mediators) {
                if (mediators.remove(mediator)) {
                    DataSourceProducerProvider.unregisterMediatorLink(getOriginDescriptor(), mediator);
                }
            }
        }
    }

    /**
     * Default way for an {@link AbstractDataSource} to notify its linked {@link Mediator}s for some
     * change
     */
    public void notifyMediators() {
        if (hasSomeDataToReturn() && (!isWriteBeforeRead())) {
            List<Mediator<T>> copy = new ArrayList<Mediator<T>>();
            synchronized (mediators) {
                copy.addAll(mediators);
            }
            for (Mediator<T> mediator : copy) {
                mediator.transmitSourceChange(this);
            }
            copy.clear();
        }
    }

    protected List<Mediator<T>> getMediators() {
        return mediators;
    }

    /**
     * Returns whether this {@link AbstractDataSource} is compatible with data setting.
     * 
     * @return A <code>boolean</code> value. <code>TRUE</code> if you can set this {@link AbstractDataSource}'s data,
     *         <code>FALSE</code> otherwise.
     * @see #setData(T)
     */
    @Override
    public boolean isSettable() {
        return true;
    }

    // By default, an AbstractDataSouce declares no unsigned number.
    @Override
    public boolean isUnsigned() {
        return false;
    }

    /**
     * Returns whether this {@link AbstractDataSource} is linked with any {@link Mediator}.
     * 
     * @return A <code>boolean</code>
     */
    public boolean isLinked() {
        boolean linked = false;
        synchronized (mediators) {
            linked = (mediators.size() > 0);
        }
        return linked;
    }

    /**
     * Returns whether 2 different data represent the same value
     * 
     * @param data1 The 1st data to check
     * @param data2 The 2nd data to check
     * @return A <code>boolean</code> value
     */
    protected boolean sameData(T data1, T data2) {
        return ObjectUtils.sameObject(data1, data2);
    }

    @Override
    public String toString() {
        return (getOriginDescriptor() == null ? super.toString() : getOriginDescriptor().getInformationKey());
    }

}
