/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

/**
 * An {@link AbstractDataSource} that does the history of a scalar
 * {@link AbstractDataSource}
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <T>
 *            The type of data managed by the {@link AbstractDataSource} to
 *            historize
 */
public abstract class AbstractScalarHistoryDataSource<T> extends AbstractHistoryDataSource<double[][], T> {

    private static final GenericDescriptor DATA_TYPE = new GenericDescriptor(double[][].class);

    /**
     * Construct a new {@link AbstractScalarHistoryDataSource}
     * 
     * @param originDescriptor
     * @param source
     *            The {@link AbstractDataSource} to historize
     */
    public AbstractScalarHistoryDataSource(IKey originDescriptor, AbstractDataSource<T> source) {
        super(originDescriptor, source);
    }

    @Override
    protected double[][] generateDefaultData() {
        return new double[2][0];
    }

    @Override
    public GenericDescriptor getDataType() {
        return DATA_TYPE;
    }

    protected abstract double getDoubleValue(T value);

    @Override
    protected double[][] getNewData(T lastSourceData, double[] newHistory, int firstIndex, int length, int oldLength) {
        double value = getDoubleValue(lastSourceData);
        double[][] newData = new double[2][length];
        newData[0] = newHistory;
        if (firstIndex < data[0].length) {
            System.arraycopy(data[1], firstIndex, newData[1], 0, oldLength);
        }
        newData[1][oldLength] = value;
        return newData;
    }
}
