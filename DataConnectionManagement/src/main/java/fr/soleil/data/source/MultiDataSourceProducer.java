/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.soleil.data.service.CompositeKey;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IConstantSourceProducer;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;

public class MultiDataSourceProducer implements IConstantSourceProducer {

    public final static String ID = "fr.soleil.data.source.MultiDataSourceRefreshingManager";

    private final Map<IKey, AbstractDataSource<?>> knownSources = new HashMap<IKey, AbstractDataSource<?>>();

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public IKey parseKey(IKey key) {
        IKey newKey = null;
        if (key instanceof CompositeKey) {
            newKey = key;
        }
        return newKey;

    }

    @Override
    public AbstractDataSource<?> createDataSource(IKey key) {
        AbstractDataSource<?> source = getExistingSource(key);
        if (source == null) {
            source = makeDataSource(key);
        }
        return source;
    }

    @Override
    public boolean isSourceSettable(IKey key) {
        return false;
    }

    @Override
    public boolean isSourceCreatable(IKey key) {
        boolean creatable = false;
        CompositeKey parsedKey = (CompositeKey) parseKey(key);
        if (parsedKey != null) {
            creatable = isCreatable(parsedKey.getKey(CompositeKey.X_INDEX))
                    && isCreatable(parsedKey.getKey(CompositeKey.Y_INDEX));
        }
        return creatable;
    }

    @Override
    public String getName() {
        return "MultiSource";
    }

    @Override
    public AbstractDataSource<?> createDataSource(IKey key, boolean forceCreation) {
        return forceCreation ? makeDataSource(key) : createDataSource(key);
    }

    @Override
    public AbstractDataSource<?> getExistingSource(IKey key) {
        IKey parsedKey = parseKey(key);
        AbstractDataSource<?> result = null;
        if (parsedKey != null) {
            synchronized (knownSources) {
                result = knownSources.get(parsedKey);
            }
        }
        return result;
    }

    @Override
    public void removeDataSource(IKey key) {
        IKey parsedKey = parseKey(key);
        if (parsedKey != null) {
            AbstractDataSource<?> source;
            synchronized (knownSources) {
                source = knownSources.remove(parsedKey);
            }
            if (source instanceof MultiDataSource) {
                ((MultiDataSource) source).clean();
            }
        }
    }

    @Override
    public int getRank(IKey key) {
        int rank = -1;
        CompositeKey parsedKey = (CompositeKey) parseKey(key);
        if (parsedKey != null) {
            if (isCreatable(parsedKey.getKey(CompositeKey.X_INDEX))
                    && isCreatable(parsedKey.getKey(CompositeKey.Y_INDEX))) {
                rank = 1;
            }
        }
        return rank;
    }

    @Override
    public int[] getShape(IKey key) {
        int[] shape = null;
        CompositeKey parsedKey = (CompositeKey) parseKey(key);
        if (parsedKey != null) {
            int dim = Math.min(getDim(parsedKey.getKey(CompositeKey.X_INDEX)),
                    getDim(parsedKey.getKey(CompositeKey.Y_INDEX)));
            if (dim > -1) {
                shape = new int[] { dim };
            }
        }
        return shape;
    }

    protected AbstractDataSource<List<Object>> makeDataSource(IKey key) {
        AbstractDataSource<List<Object>> result = null;
        IKey parsedKey = parseKey(key);
        if (parsedKey != null) {
            CompositeKey compositeKey = (CompositeKey) parsedKey;
            IKey keyX = compositeKey.getKey(CompositeKey.X_INDEX);
            IKey keyY = compositeKey.getKey(CompositeKey.Y_INDEX);
            MultiDataSource source = new MultiDataSource(compositeKey);
            if (keyX != null) {
                IDataSourceProducer producerX = DataSourceProducerProvider.getProducer(keyX.getSourceProduction());
                AbstractDataSource<?> sourceX = producerX.createDataSource(keyX);
                if (sourceX == null) {
                    source = null;
                } else {
                    source.addSource(sourceX, CompositeKey.X_INDEX);
                }
            }
            if (keyY != null) {
                IDataSourceProducer producerY = DataSourceProducerProvider.getProducer(keyY.getSourceProduction());
                AbstractDataSource<?> sourceY = producerY.createDataSource(keyY);
                if (sourceY == null) {
                    source = null;
                } else if (source != null) {
                    source.addSource(sourceY, CompositeKey.Y_INDEX);
                }
            }
            if (source != null) {
                synchronized (knownSources) {
                    knownSources.put(parsedKey, source);
                }
                result = source;
            }
        }
        return result;
    }

    protected IDataSourceProducer getProducer(IKey key) {
        IDataSourceProducer producer = null;
        if (key != null) {
            producer = DataSourceProducerProvider.getProducer(key.getSourceProduction());
        }
        return producer;
    }

    protected boolean isCreatable(IKey key) {
        boolean creatable = false;
        if (key != null) {
            IDataSourceProducer producer = getProducer(key);
            if (producer != null) {
                creatable = producer.isSourceCreatable(key);
            }
        }
        return creatable;
    }

    protected int[] recoverShape(IKey key) {
        int[] shape = null;
        IDataSourceProducer producer = getProducer(key);
        if (producer != null) {
            shape = producer.getShape(key);
        }
        return shape;
    }

    protected int getDim(int[] shape) {
        int dim = -1;
        if (shape != null) {
            dim = 1;
            for (int size : shape) {
                dim *= size;
            }
        }
        return dim;
    }

    protected int getDim(IKey key) {
        return getDim(getShape(key));
    }

}
