/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.math.MathConst;

/**
 * An {@link AbstractDataSource} that does the history of a {@link Number}
 * {@link AbstractDataSource}
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <T>
 *            The type of {@link Number} managed by the
 *            {@link AbstractDataSource} to historize
 */
public class ScalarHistoryDataSource<T extends Number> extends AbstractScalarHistoryDataSource<T> {

    public ScalarHistoryDataSource(IKey originDescriptor, AbstractDataSource<T> source) {
        super(originDescriptor, source);
    }

    @Override
    protected double getDoubleValue(T value) {
        double result;
        if (value == null) {
            result = MathConst.NAN_FOR_NULL;
        } else {
            result = value.doubleValue();
        }
        return result;
    }

}
