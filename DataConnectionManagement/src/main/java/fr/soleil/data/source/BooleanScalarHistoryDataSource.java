/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.math.MathConst;

/**
 * An {@link AbstractDataSource} that does the history of a {@link Boolean}
 * {@link AbstractDataSource}, transforming its values to numbers
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BooleanScalarHistoryDataSource extends AbstractScalarHistoryDataSource<Boolean> {

    public BooleanScalarHistoryDataSource(IKey originDescriptor, AbstractDataSource<Boolean> source) {
        super(originDescriptor, source);
    }

    @Override
    protected double getDoubleValue(Boolean value) {
        double result;
        if (value == null) {
            result = MathConst.NAN_FOR_NULL;
        } else {
            result = value.booleanValue() ? 1d : 0d;
        }
        return result;
    }

}
