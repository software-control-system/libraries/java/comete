/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;

/**
 * An {@link AbstractDataSource} for basic interaction
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <T> The type of data this {@link AbstractBasicDataSource} can manipulate
 */
public abstract class AbstractBasicDataSource<T> extends AbstractSettableDataSource<T> {

    protected T data;
    protected boolean ignoreDuplicationTest;

    public AbstractBasicDataSource(IKey key) {
        this(key, null);
    }

    public AbstractBasicDataSource(IKey key, T data) {
        super(key);
        this.data = data;
        ignoreDuplicationTest = false;
    }

    /**
     * Returns whether this data source will ignore the test of duplication on data setting
     * 
     * @return A <code>boolean</code> value. <code>false</code> by default (i.e. this data source
     *         will test for duplication on data setting)
     */
    public boolean isIgnoreDuplicationTest() {
        return ignoreDuplicationTest;
    }

    /**
     * Sets whether this data source should ignore the test of duplication on data setting
     * 
     * @param ignoreDuplicationTest Whether this data source should ignore the test of duplication
     *            on data setting
     */
    public void setIgnoreDuplicationTest(boolean ignoreDuplicationTest) {
        this.ignoreDuplicationTest = ignoreDuplicationTest;
    }

    @Override
    public T getData() throws Exception {
        return data;
    }

    @Override
    public void setData(T data) throws Exception {
        try {
            super.setData(data);
        } catch (Exception e) {
            // Ignore because it can't happen: doSetData does not throw any Exception
        }
    }

    @Override
    protected void doSetData(T data) throws Exception {
        setData(data, isIgnoreDuplicationTest(), true);
    }

    /**
     * Sets the data, with the possibility to temporary ignore duplication test
     * 
     * @param data The data to set
     * @param ignoreDuplication Whether to ignore duplication test
     * @param notifyMediators Whether to notify {@link Mediator}s once data is set
     */
    protected final void setData(T data, boolean ignoreDuplication, boolean notifyMediators) {
        if (ignoreDuplication || (!sameData(data, this.data))) {
            this.data = data;
            if (notifyMediators) {
                notifyMediators();
            }
        }
    }

    @Override
    public GenericDescriptor getDataType() {
        GenericDescriptor result = null;
        if (data != null) {
            result = new GenericDescriptor(data.getClass());
        }
        return result;
    }

}
