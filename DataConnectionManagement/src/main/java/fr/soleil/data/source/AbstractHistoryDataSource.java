/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import org.slf4j.LoggerFactory;

import fr.soleil.data.exception.InvalidSourceDataException;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.date.DateUtil;

/**
 * An {@link AbstractDataSource} that does the history of another {@link AbstractDataSource}
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <U> The type of data managed by this {@link AbstractHistoryDataSource}
 * @param <T> The type of data managed by the {@link AbstractDataSource} to historize
 */
public abstract class AbstractHistoryDataSource<U, T> extends BufferedDataSource<U> implements Mediator<T> {

    /**
     * Default history depth
     */
    public static final long DEFAULT_HISTORY_DEPTH = 300000; // 5 minutes

    protected final AbstractDataSource<T> source;
    protected final Object dataLock;
    protected final Object sourceConnectionLock;
    protected long historyDepth;
    protected double[] timeHistory;

    /**
     * Construct a new {@link AbstractHistoryDataSource}
     * 
     * @param originDescriptor
     * @param source
     *            The {@link AbstractDataSource} to historize
     */
    public AbstractHistoryDataSource(IKey originDescriptor, AbstractDataSource<T> source) {
        super(originDescriptor);
        timeHistory = new double[0];
        data = generateDefaultData();
        dataLock = new Object();
        sourceConnectionLock = new Object();
        historyDepth = DEFAULT_HISTORY_DEPTH;
        setIgnoreDuplicationTest(true);
        this.source = source;
    }

    protected abstract U generateDefaultData();

    @Override
    public boolean isSettable() {
        return false;
    }

    @Override
    public boolean isUnsigned() {
        return source == null ? false : source.isUnsigned();
    }

    /**
     * Returns this {@link AbstractHistoryDataSource}'s history depth, in milliseconds
     * 
     * @return A <code>long</code>
     */
    public long getHistoryDepth() {
        return historyDepth;
    }

    /**
     * Sets the history depth, in milliseconds
     * 
     * @param period The history depth to set
     */
    public void setHistoryDepth(long period) {
        if (period < 0) {
            this.historyDepth = DEFAULT_HISTORY_DEPTH;
        } else {
            this.historyDepth = period;
        }
    }

    /**
     * Sets whether this {@link AbstractHistoryDataSource} should listen to is associated {@link AbstractDataSource}
     * change events
     * 
     * @param event Whether this {@link AbstractHistoryDataSource} should listen to is associated
     *            {@link AbstractDataSource} change events
     */
    public void setEventSource(boolean event) {
        synchronized (sourceConnectionLock) {
            if (source != null) {
                source.removeMediator(this);
                if (event) {
                    source.addMediator(this);
                    try {
                        updateData();
                    } catch (Exception e) {
                        logDataWritingError(e);
                    }
                }
            }
        }
    }

    /**
     * Forces this {@link AbstractHistoryDataSource} to reset its data.
     */
    public void reset() {
        synchronized (dataLock) {
            timeHistory = new double[0];
            data = generateDefaultData();
        }
    }

    /**
     * Finds the best matching date for given date: either the date itself or the one rounded to the closest second.
     * 
     * @param date The date.
     * @param preciseToMs Whether the date should be precise to the milliseconds.
     *            <ul>
     *            <li>if <code>true</code>, then the returned date is <code>date</code></li>
     *            <li>if <code>false</code>, then the returned date is <code>date</code> rounded to the closest second
     *            <br />
     *            Examples:
     *            <ul>
     *            <li><code>2022-09-01 15:28:02.525</code> will be rounded to <code>2022-09-01 15:28:03.000</code></li>
     *            <li><code>2022-09-12 17:00:05.017</code> will be rounded to <code>2022-09-12 17:00:05.000</code></li>
     *            </ul>
     *            </li>
     *            </ul>
     * @return A <code>long</code>: the adapted date.
     */
    protected long getAdaptedDate(long date, boolean preciseToMs) {
        long adaptedDate;
        if (preciseToMs) {
            adaptedDate = date;
        } else {
            adaptedDate = DateUtil.getRoundedDate(date, 1000);
        }
        return adaptedDate;
    }

    @Override
    public U readData() {
        U newData;
        synchronized (dataLock) {
            T lastSourceData;
            if (source == null) {
                lastSourceData = null;
            } else {
                try {
                    lastSourceData = source.getData();
                } catch (Exception e) {
                    lastSourceData = null;
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .warn("Failed to read " + source + " data for history", e);
                }
            }
            long delta = historyDepth;
            long current = System.currentTimeMillis();
            boolean preciseToMs = delta % 1000 != 0;
            // CONTROLGUI-382: use rounded dates to compute delta when history depths is in seconds, minutes, etc.
            long currentAdapted = getAdaptedDate(current, preciseToMs);
            int firstIndex = timeHistory.length;
            for (int i = 0; i < timeHistory.length; i++) {
                if ((currentAdapted - getAdaptedDate((long) timeHistory[i], preciseToMs)) <= delta) {
                    firstIndex = i;
                    break;
                }
            }
            int length = timeHistory.length + 1 - firstIndex;
            int oldLength = length - 1;
            double[] newHistory = new double[length];
            if (firstIndex < timeHistory.length) {
                System.arraycopy(timeHistory, firstIndex, newHistory, 0, oldLength);
            }
            newHistory[oldLength] = current;
            newData = getNewData(lastSourceData, newHistory, firstIndex, length, oldLength);
            timeHistory = newHistory;
        }
        return newData;
    }

    protected abstract U getNewData(T lastSourceData, double[] newHistory, int firstIndex, int length, int oldLength);

    @Override
    public void setData(U data) {
        // nothing to do;
    }

    @Override
    protected void writeData(U data) throws InvalidSourceDataException {
        // nothing to do;
    }

    @Override
    public boolean addLink(AbstractDataSource<T> source, ITarget target) {
        // not managed
        return false;
    }

    @Override
    public void removeLink(AbstractDataSource<T> source, ITarget target) {
        // not managed
    }

    @Override
    public void removeAllReferencesTo(AbstractDataSource<T> source) {
        // not managed
    }

    @Override
    public void removeAllReferencesTo(ITarget target) {
        // not managed
    }

    @Override
    public void transmitSourceChange(AbstractDataSource<T> source) {
        if (source == this.source) {
            try {
                updateData();
            } catch (Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn("Failed to update " + source + " data for history",
                        e);
            }
        }
    }

    @Override
    public <I, V extends TargetInformation<I>> void transmitTargetChange(V targetInformation) {
        // not managed
    }

    @Override
    public <V> void addFilterToTarget(AbstractDataFilter<V> filter, ITarget target) {
        // not managed
    }

    @Override
    public <V> void removeFilterFromTarget(AbstractDataFilter<V> filter, ITarget target) {
        // not managed
    }

    @Override
    public void addFilterToSource(AbstractDataFilter<T> filter, AbstractDataSource<T> source) {
        // not managed
    }

    @Override
    public void removeFilterFromSource(AbstractDataFilter<T> filter, AbstractDataSource<T> source) {
        // not managed
    }

    protected void clean() {
        setEventSource(false);
    }

    @Override
    protected void finalize() throws Throwable {
        clean();
        super.finalize();
    }
}
