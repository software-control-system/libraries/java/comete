/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import org.slf4j.LoggerFactory;

import fr.soleil.data.container.IUpdatableDataContainer;
import fr.soleil.data.exception.InvalidSourceDataException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.ObjectUtils;

public abstract class BufferedDataSource<T> extends AbstractBasicDataSource<T> implements IUpdatableDataContainer<T> {

    protected boolean sameReadAndWriteValues;
    protected boolean refreshAfterDataWriting;
    protected T writtenData;

    public BufferedDataSource(IKey key) {
        this(key, true, false);
    }

    public BufferedDataSource(IKey key, boolean sameReadAndWriteValues, boolean refreshAfterDataWriting) {
        super(key);
        data = getFirstDataValue();
        this.sameReadAndWriteValues = sameReadAndWriteValues;
        this.refreshAfterDataWriting = refreshAfterDataWriting;
        initWrittenData(null);
    }

    public BufferedDataSource(IKey key, T data) {
        this(key, data, true, false);
    }

    public BufferedDataSource(IKey key, T data, boolean sameReadAndWriteValues, boolean refreshAfterDataWriting) {
        super(key, data);
        this.sameReadAndWriteValues = sameReadAndWriteValues;
        this.refreshAfterDataWriting = refreshAfterDataWriting;
        initWrittenData(null);
    }

    protected void initWrittenData(T value) {
        if (sameReadAndWriteValues) {
            writtenData = data;
        } else {
            writtenData = value;
        }
    }

    /**
     * Returns the value to initialize this {@link BufferedDataSource}'s data
     * 
     * @return A value of the right type
     */
    protected T getFirstDataValue() {
        return null;
    }

    @Override
    public void updateData() throws Exception {
        updateData(isIgnoreDuplicationTest());
    }

    protected void updateData(boolean ignoreDuplication) throws Exception {
        T previous = data;
        T updated = readData();

        setData(updated, ignoreDuplication, false);
        initWrittenData(writtenData);
        if (ignoreDuplication || (!sameData(previous, updated))) {
            notifyMediators();
        }
    }

    /**
     * Buffers the data to set
     * 
     * @param data The data to set
     */
    protected void setBufferedData(T data) {
        try {
            super.doSetData(data);
        } catch (Exception e) {
            logDataWritingError(e);
        }
    }

    protected void logDataWritingError(Throwable t) {
        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                .error(this + " data writing error" + (t == null ? ObjectUtils.EMPTY_STRING : ":\n" + t.getMessage()));
    }

    @Override
    protected void doSetData(T data) throws Exception {
        boolean writeData;
        if (isIgnoreDuplicationTest()) {
            writeData = true;
        } else {
            if (sameReadAndWriteValues) {
                writeData = (!sameData(data, this.data));
            } else {
                writeData = (!sameData(data, this.writtenData));
            }
        }
        if (writeData) {
            T previous = writtenData;
            writtenData = data;
            boolean forceRefresh = false;
            if (sameReadAndWriteValues) {
                setBufferedData(data);
            }
            try {
                writeData(data);
            } catch (InvalidSourceDataException e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(this + " data writing error:\n" + e.getMessage());
                writtenData = previous;
                forceRefresh = true;
            }
            if (refreshAfterDataWriting || forceRefresh) {
                updateData(forceRefresh);
            }
        }
    }

    /**
     * Connects to the associated data and reads its value
     * 
     * @return A value of the right type
     */
    protected abstract T readData() throws Exception;

    /**
     * Writes some value to associated data
     * 
     * @param data the value to write
     * @throws InvalidSourceDataException If there is a problem with the value to write
     */
    protected abstract void writeData(T data) throws InvalidSourceDataException;

}
