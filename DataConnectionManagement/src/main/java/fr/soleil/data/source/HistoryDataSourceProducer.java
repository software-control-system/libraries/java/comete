/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import java.math.BigInteger;

import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.BooleanMatrix;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.EventRefreshingStrategy;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.HistoryKey;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.IRefreshingStrategy;
import fr.soleil.data.service.PolledRefreshingStrategy;

public class HistoryDataSourceProducer extends AbstractRefreshingManager<IKey> {

    public final static String ID = "fr.soleil.data.source.History";

    protected static final String HISTORY = "History";

    public HistoryDataSourceProducer() {
        super();
        setDefaultRefreshingStrategy(new EventRefreshingStrategy());
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public boolean isSourceSettable(IKey key) {
        return false;
    }

    @Override
    public boolean isSourceCreatable(IKey key) {
        boolean creatable = false;
        HistoryKey parsedKey = parseKey(key);
        if (parsedKey != null) {
            IKey toHistorize = parsedKey.getHistory();
            if (toHistorize != null) {
                IDataSourceProducer producer = DataSourceProducerProvider
                        .getProducer(toHistorize.getSourceProduction());
                if (producer != null) {
                    AbstractDataSource<?> source = producer.createDataSource(toHistorize);
                    if (source != null) {
                        GenericDescriptor descriptor = source.getDataType();
                        if (descriptor != null) {
                            creatable = (Number.class.isAssignableFrom(descriptor.getConcernedClass())
                                    || AbstractNumberMatrix.class.isAssignableFrom(descriptor.getConcernedClass()));
                        }
                    }
                }
            }
        }
        return creatable;
    }

    @Override
    public String getName() {
        return HISTORY;
    }

    @Override
    public HistoryKey parseKey(IKey key) {
        HistoryKey newKey = null;
        if (key instanceof HistoryKey) {
            newKey = (HistoryKey) key;
        }
        return newKey;
    }

    @Override
    protected HistoryKey createIdentifier(IKey key) {
        return parseKey(key);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected AbstractDataSource<?> constructDataSource(IKey key) {
        AbstractDataSource<?> result = null;
        HistoryKey parsedKey = parseKey(key);
        if (parsedKey != null) {
            synchronized (knownSources) {
                IKey toHistorize = parsedKey.getHistory();
                if (toHistorize != null) {
                    IDataSourceProducer producer = DataSourceProducerProvider
                            .getProducer(toHistorize.getSourceProduction());
                    if (producer != null) {
                        AbstractDataSource<?> source = producer.createDataSource(toHistorize);
                        if (source != null) {
                            GenericDescriptor descriptor = source.getDataType();
                            if (descriptor != null) {
                                if (Number.class.isAssignableFrom(descriptor.getConcernedClass())) {
                                    if (Number.class.equals(descriptor.getConcernedClass())) {
                                        result = new ScalarHistoryDataSource<Number>(parsedKey,
                                                (AbstractDataSource<Number>) source);
                                    } else if (Byte.class.equals(descriptor.getConcernedClass())) {
                                        result = new ScalarHistoryDataSource<Byte>(parsedKey,
                                                (AbstractDataSource<Byte>) source);
                                    } else if (Short.class.equals(descriptor.getConcernedClass())) {
                                        result = new ScalarHistoryDataSource<Short>(parsedKey,
                                                (AbstractDataSource<Short>) source);
                                    } else if (Integer.class.equals(descriptor.getConcernedClass())) {
                                        result = new ScalarHistoryDataSource<Integer>(parsedKey,
                                                (AbstractDataSource<Integer>) source);
                                    } else if (Long.class.equals(descriptor.getConcernedClass())) {
                                        result = new ScalarHistoryDataSource<Long>(parsedKey,
                                                (AbstractDataSource<Long>) source);
                                    } else if (BigInteger.class.equals(descriptor.getConcernedClass())) {
                                        result = new ScalarHistoryDataSource<BigInteger>(parsedKey,
                                                (AbstractDataSource<BigInteger>) source);
                                    } else if (Float.class.equals(descriptor.getConcernedClass())) {
                                        result = new ScalarHistoryDataSource<Float>(parsedKey,
                                                (AbstractDataSource<Float>) source);
                                    } else if (Double.class.equals(descriptor.getConcernedClass())) {
                                        result = new ScalarHistoryDataSource<Double>(parsedKey,
                                                (AbstractDataSource<Double>) source);
                                    }
                                } else if (Boolean.class.equals(descriptor.getConcernedClass())) {
                                    result = new BooleanScalarHistoryDataSource(parsedKey,
                                            (AbstractDataSource<Boolean>) source);
                                } else if (AbstractNumberMatrix.class
                                        .isAssignableFrom(descriptor.getConcernedClass())) {
                                    result = createHistory(parsedKey,
                                            (AbstractDataSource<? extends AbstractNumberMatrix<?>>) source);
                                } else if (AbstractMatrix.class.isAssignableFrom(descriptor.getConcernedClass())) {
                                    if (BooleanMatrix.class.isAssignableFrom(descriptor.getConcernedClass())) {
                                        result = createBooleanHistory(parsedKey,
                                                (AbstractDataSource<? extends AbstractMatrix<Boolean>>) source);
                                    } else if ((descriptor.getParameters() != null)
                                            && (descriptor.getParameters().length == 1)) {
                                        GenericDescriptor parameter = descriptor.getParameters()[0];
                                        if (parameter != null) {
                                            if (Boolean.class.equals(parameter.getConcernedClass())
                                                    || Boolean.TYPE.equals(parameter.getConcernedClass())) {
                                                result = createBooleanHistory(parsedKey,
                                                        (AbstractDataSource<? extends AbstractMatrix<Boolean>>) source);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } // end if (toHistorize != null)
                if (result != null) {
                    knownSources.put(parsedKey, result);
                }
            } // end synchronized (knownSources)
        } // end if (parsedKey != null)
        return result;
    }

    protected <T extends AbstractNumberMatrix<?>> SpectrumHistoryDataSource<T> createHistory(IKey originDescriptor,
            AbstractDataSource<T> source) {
        return new SpectrumHistoryDataSource<T>(originDescriptor, source);
    }

    protected <T extends AbstractMatrix<Boolean>> BooleanSpectrumHistoryDataSource<T> createBooleanHistory(
            IKey originDescriptor, AbstractDataSource<T> source) {
        return new BooleanSpectrumHistoryDataSource<T>(originDescriptor, source);
    }

    @Override
    protected void cleanDataSourceAfterRemoving(AbstractDataSource<?> source) {
        if (source instanceof AbstractHistoryDataSource<?, ?>) {
            ((AbstractHistoryDataSource<?, ?>) source).clean();
        }
    }

    /**
     * Returns the history depth for a given {@link IKey}
     * 
     * @param key The {@link IKey}
     * @return A <code>long</code>
     */
    public long getHistoryDepth(IKey key) {
        AbstractHistoryDataSource<?, ?> source = (AbstractHistoryDataSource<?, ?>) getExistingSource(key);
        return (source == null ? AbstractHistoryDataSource.DEFAULT_HISTORY_DEPTH : source.getHistoryDepth());
    }

    /**
     * Sets the history depth for a given {@link IKey}
     * 
     * @param key The {@link IKey}
     * @param period The history depth to set
     */
    public void setHistoryDepth(IKey key, long period) {
        AbstractHistoryDataSource<?, ?> source = (AbstractHistoryDataSource<?, ?>) createDataSource(key);
        if (source != null) {
            source.setHistoryDepth(period);
        }
    }

    /**
     * Resets the history for given {@link IKey}.
     * 
     * @param key The {@link IKey}.
     */
    public void resetHistory(IKey key) {
        AbstractHistoryDataSource<?, ?> source = (AbstractHistoryDataSource<?, ?>) getExistingSource(key);
        if (source != null) {
            source.reset();
        }
    }

    @Override
    protected <S extends AbstractDataSource<?>> void prepareDataSourceBeforeStrategyChange(S dataSource) {
        if (dataSource instanceof AbstractHistoryDataSource<?, ?>) {
            ((AbstractHistoryDataSource<?, ?>) dataSource).setEventSource(false);
        }
    }

    @Override
    protected <S extends AbstractDataSource<?>> void prepareDataSource(S dataSource, IRefreshingStrategy strategy) {
        if ((dataSource instanceof AbstractHistoryDataSource<?, ?>) && (strategy instanceof PolledRefreshingStrategy)) {
            ((AbstractHistoryDataSource<?, ?>) dataSource).setEventSource(false);
        }
    }

    @Override
    protected void applyOtherRefreshingStrategyToSource(AbstractDataSource<?> source, IRefreshingStrategy strategy) {
        if ((source instanceof AbstractHistoryDataSource<?, ?>) && (strategy instanceof EventRefreshingStrategy)) {
            ((AbstractHistoryDataSource<?, ?>) source).setEventSource(true);
        }
    }

    @Override
    public int getRank(IKey key) {
        int rank = -1;
        HistoryKey parsedKey = parseKey(key);
        if (parsedKey != null) {
            IKey history = parsedKey.getHistory();
            if (history != null) {
                IDataSourceProducer producer = DataSourceProducerProvider.getProducer(history.getSourceProduction());
                if (producer != null) {
                    int historyRank = producer.getRank(history);
                    if ((historyRank == 0) || (historyRank == 1)) {
                        rank = historyRank + 1;
                    }
                }
            }
        }
        return rank;
    }

    @Override
    public int[] getShape(IKey key) {
        // shape is dynamic and can't be calculated
        return null;
    }

}
