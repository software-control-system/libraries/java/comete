/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import fr.soleil.data.service.IKey;

/**
 * An {@link AbstractDataSource} for basic interaction and that does not throw Exceptions on get/setData
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <T> The type of data this {@link BasicDataSource} can manipulate
 */
public class BasicDataSource<T> extends AbstractBasicDataSource<T> {

    public BasicDataSource(IKey key) {
        super(key);
    }

    public BasicDataSource(IKey key, T data) {
        super(key, data);
    }

    @Override
    public T getData() {
        return data;
    }

    @Override
    public void setData(T data) {
        try {
            super.setData(data);
        } catch (Exception e) {
            // Ignored because it can't happen: doSetData does not throw any Exception
        }
    }

    @Override
    protected void doSetData(T data) {
        setData(data, isIgnoreDuplicationTest(), true);
    }

}
