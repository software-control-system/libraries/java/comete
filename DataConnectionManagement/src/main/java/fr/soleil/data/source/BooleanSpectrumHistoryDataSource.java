/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.math.MathConst;

public class BooleanSpectrumHistoryDataSource<T extends AbstractMatrix<Boolean>>
        extends AbstractSpectrumHistoryDataSource<T> {

    public BooleanSpectrumHistoryDataSource(IKey originDescriptor, AbstractDataSource<T> source) {
        super(originDescriptor, source);
    }

    @Override
    protected double[] extractDoubleArray(Object flatValue) {
        double[] result;
        if (flatValue instanceof Boolean[]) {
            Boolean[] array = (Boolean[]) flatValue;
            result = new double[array.length];
            for (int i = 0; i < array.length; i++) {
                if (array[i] == null) {
                    result[i] = MathConst.NAN_FOR_NULL;
                } else {
                    result[i] = array[i].booleanValue() ? 1d : 0d;
                }
            }
        } else if (flatValue instanceof boolean[]) {
            boolean[] array = (boolean[]) flatValue;
            result = new double[array.length];
            for (int i = 0; i < array.length; i++) {
                result[i] = array[i] ? 1d : 0d;
            }
        } else {
            result = null;
        }
        return result;
    }

}
