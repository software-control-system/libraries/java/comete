/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.math.MathConst;

public abstract class AbstractSpectrumHistoryDataSource<T extends AbstractMatrix<?>>
        extends AbstractHistoryDataSource<List<double[]>, T> {

    public AbstractSpectrumHistoryDataSource(IKey originDescriptor, AbstractDataSource<T> source) {
        super(originDescriptor, source);
    }

    @Override
    protected List<double[]> generateDefaultData() {
        return new ArrayList<>();
    }

    @Override
    protected List<double[]> getNewData(T lastSourceData, double[] newHistory, int firstIndex, int length,
            int oldLength) {
        // extract old data without time history
        List<double[]> oldData = new ArrayList<>();
        oldData.addAll(data);
        if (oldData.size() > 0) {
            oldData.remove(0);
        }
        // create new data and add time history in it
        List<double[]> newData = new ArrayList<>();
        newData.add(newHistory);
        // extract current spectrum value, and be sure to have it at least as
        // long as old data size
        double[] extractedValue;
        if (lastSourceData == null) {
            extractedValue = null;
        } else {
            extractedValue = extractDoubleArray(lastSourceData.getFlatValue());
        }
        if (extractedValue == null) {
            extractedValue = new double[oldData.size()];
            Arrays.fill(extractedValue, MathConst.NAN_FOR_NULL);
        } else if (extractedValue.length < oldData.size()) {
            double[] newValue = new double[oldData.size()];
            System.arraycopy(extractedValue, 0, newValue, 0, extractedValue.length);
            for (int i = extractedValue.length; i < newValue.length; i++) {
                newValue[i] = MathConst.NAN_FOR_NULL;
            }
        }
        // now, extract history of each index
        int index = 0;
        for (double[] oldValue : oldData) {
            double[] newValue = new double[length];
            if (firstIndex < timeHistory.length) {
                System.arraycopy(oldValue, firstIndex, newValue, 0, oldLength);
            }
            newValue[oldLength] = extractedValue[index++];
            newData.add(newValue);
        }
        // add eventual missing indexes
        if (index < extractedValue.length) {
            for (int i = index; i < extractedValue.length; i++) {
                double[] newValue = new double[newHistory.length];
                Arrays.fill(newValue, MathConst.NAN_FOR_NULL);
                newValue[newValue.length - 1] = extractedValue[i];
                newData.add(newValue);
            }
        }
        return newData;
    }

    protected abstract double[] extractDoubleArray(Object flatValue);

}
