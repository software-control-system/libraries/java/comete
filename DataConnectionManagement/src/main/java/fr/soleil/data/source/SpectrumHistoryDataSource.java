/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.source;

import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.math.NumberArrayUtils;

public class SpectrumHistoryDataSource<T extends AbstractNumberMatrix<?>> extends AbstractSpectrumHistoryDataSource<T> {

    public SpectrumHistoryDataSource(IKey originDescriptor, AbstractDataSource<T> source) {
        super(originDescriptor, source);
    }

    @Override
    protected double[] extractDoubleArray(Object flatValue) {
        return NumberArrayUtils.extractDoubleArray(flatValue);
    }

}
