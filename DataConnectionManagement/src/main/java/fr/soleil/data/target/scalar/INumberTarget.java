/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target.scalar;

import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;

public interface INumberTarget extends IScalarTarget {

    public abstract Number getNumberValue();

    public abstract void setNumberValue(Number value);

    public class INumberTargetSignature extends TargetSignature<Number> {

        public INumberTargetSignature() {
            super(INumberTarget.class, Number.class, IScalarTarget.class);
        }

        @Override
        protected void callFunction(ITarget target, Number data) {
            ((INumberTarget) target).setNumberValue(data);
        }
    }
}
