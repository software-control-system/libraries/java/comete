/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target.matrix;

import fr.soleil.data.target.ITarget;

/**
 * An {@link ITarget} that manipulates matrix elements. Matrix elements can be manipulated 2
 * different ways:
 * <ul>
 * <li>As 2 dimension tables (example: Object[][])</li>
 * <li>As 1 dimension tables, with 2 int, indicating matrix width and height (example: Object[] +
 * width, height). This case is called "flat values"</li>
 * </ul>
 * 
 * @author huriez, Rapha&euml;l GIRARDOT
 * 
 */
public interface IMatrixTarget extends ITarget {

    /**
     * Returns whether this target prefers using flat values for a particular type of data
     * 
     * @param concernedDataClass The type of data. This parameter is mainly used to consider the
     *            cases where you have an {@link ITarget} that is able to manipulate different kind
     *            of matrixes (example, an {@link ITarget} able to manipulate {@link String},
     *            {@link Double} and boolean matrixes). Such {@link ITarget} may prefer flat values
     *            for a particular type of data, and classic matrix for another type.
     * @return A boolean value. <code>TRUE</code> if it is preferable to use flat values for the
     *         given type, <code>FALSE</code> otherwise.
     */
    public boolean isPreferFlatValues(Class<?> concernedDataClass);

    /**
     * Returns the width of the matrix manipulated by this {@link IMatrixTarget}
     * 
     * @param concernedDataClass The type of data in the matrix. The reason for this parameter is
     *            the same as for {@link #isPreferFlatValues(Class)}
     * @return An int
     * @see #isPreferFlatValues(Class)
     */
    public int getMatrixDataWidth(Class<?> concernedDataClass);

    /**
     * Returns the height of the matrix manipulated by this {@link IMatrixTarget}
     * 
     * @param concernedDataClass The type of data in the matrix. The reason for this parameter is
     *            the same as for {@link #isPreferFlatValues(Class)}
     * @return An int
     * @see #isPreferFlatValues(Class)
     */
    public int getMatrixDataHeight(Class<?> concernedDataClass);

}
