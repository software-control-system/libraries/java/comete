/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target.array;

import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;

public interface IObjectArrayTarget extends IArrayTarget {

    public Object[] getObjectArray();

    public void setObjectArray(Object[] value);

    public class IObjectArrayTargetSignature extends TargetSignature<Object[]> {

        public IObjectArrayTargetSignature() {
            super(IObjectArrayTarget.class, Object.class, IArrayTarget.class);

        }

        @Override
        protected void callFunction(ITarget target, Object[] data) {
            ((IObjectArrayTarget) target).setObjectArray(data);
        }

    }

}
