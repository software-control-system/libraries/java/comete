/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.information.TargetInformation;

/**
 * This is a delegate class that does the work an {@link ITarget} should do to be connected to some
 * {@link Mediator}s
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 */
public class TargetDelegate {

    private final List<WeakReference<Mediator<?>>> mediators = new ArrayList<WeakReference<Mediator<?>>>();

    /**
     * Adds a {@link Mediator} in the list of linked {@link Mediator}s
     * 
     * @param mediator The {@link Mediator} to add
     */
    public void addMediator(Mediator<?> mediator) {
        if (mediator != null) {
            ArrayList<WeakReference<Mediator<?>>> toRemove = new ArrayList<WeakReference<Mediator<?>>>();
            synchronized (mediators) {
                boolean canAdd = true;
                for (WeakReference<Mediator<?>> reference : mediators) {
                    Mediator<?> temp = reference.get();
                    if (temp == null) {
                        toRemove.add(reference);
                    } else if (temp.equals(mediator)) {
                        canAdd = false;
                    }
                }
                mediators.removeAll(toRemove);
                if (canAdd) {
                    mediators.add(new WeakReference<Mediator<?>>(mediator));
                }
            }
            toRemove.clear();
        }
    }

    /**
     * Removes a {@link Mediator} from the list of linked {@link Mediator}s
     * 
     * @param mediator The {@link Mediator} to remove
     */
    public void removeMediator(Mediator<?> mediator) {
        if (mediator != null) {
            ArrayList<WeakReference<Mediator<?>>> toRemove = new ArrayList<WeakReference<Mediator<?>>>();
            synchronized (mediators) {
                for (WeakReference<Mediator<?>> reference : mediators) {
                    Mediator<?> temp = reference.get();
                    if ((temp == null) || temp.equals(mediator)) {
                        toRemove.add(reference);
                    }
                }
                mediators.removeAll(toRemove);
            }
            toRemove.clear();
        }
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to
     *            {@link Mediator}s
     */
    public <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        if (targetInformation != null) {
            ArrayList<WeakReference<Mediator<?>>> toRemove = new ArrayList<WeakReference<Mediator<?>>>();
            ArrayList<WeakReference<Mediator<?>>> copy = new ArrayList<WeakReference<Mediator<?>>>();
            synchronized (mediators) {
                copy.addAll(mediators);
            }
            for (WeakReference<Mediator<?>> reference : copy) {
                Mediator<?> temp = reference.get();
                if (temp == null) {
                    toRemove.add(reference);
                } else {
                    temp.transmitTargetChange(targetInformation);
                }
            }
            copy.clear();
            synchronized (mediators) {
                mediators.removeAll(toRemove);
            }
            toRemove.clear();
        }
    }

}
