/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target.information;

import fr.soleil.data.target.ITarget;

/**
 * A class used to warn about an information about an {@link ITarget} (example: some change in this
 * {@link ITarget})
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <I> The type of information that can be transmitted by this {@link TargetInformation}
 */
public class TargetInformation<I> {

    private ITarget target;
    private final I info;

    /**
     * Constructor
     * 
     * @param target The concerned {@link ITarget}
     * @param info The information to transmit
     */
    public TargetInformation(ITarget target, I info) {
        this.target = target;
        this.info = info;
    }

    /**
     * Returns the {@link ITarget} that is concerned by this {@link TargetInformation}
     * 
     * @return An {@link ITarget}
     */
    public ITarget getConcernedTarget() {
        return target;
    }

    /**
     * Use this method in order to change the concerned target. EXPERT USAGE ONLY !
     * 
     * @param target the target to replace with
     */
    public void setConcernedTarget(ITarget target) {
        this.target = target;
    }

    /**
     * Returns the information to transmit
     * 
     * @return Some information
     */
    public I getInformationData() {
        return info;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getClass().getName());
        builder.append(":");
        builder.append("\n\t- target: ").append(target);
        builder.append("\n\t- info: ").append(info);
        return builder.toString();
    }

}
