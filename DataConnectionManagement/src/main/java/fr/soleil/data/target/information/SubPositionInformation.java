/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target.information;

import fr.soleil.data.target.ITarget;

/**
 * A {@link TargetInformation} used to warn about the fact that a particular position changed in a
 * stack. The {@link #getInformationData()} is supposed to be an <code>int[]</code> of length: 2,
 * that represents: <code>{indexInShape, positionValue}</code>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class SubPositionInformation extends TargetInformation<int[]> {

    public SubPositionInformation(ITarget target, int[] info) {
        super(target, info);
    }

}
