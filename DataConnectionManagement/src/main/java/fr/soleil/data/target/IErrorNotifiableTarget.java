/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target;

/**
 * An {@link ITarget} that can be notified for an error
 * 
 * @author Rapha&euml;l GIRARDOT
 *
 */
public interface IErrorNotifiableTarget extends ITarget {

    /**
     * Notifies this {@link IErrorNotifiableTarget} for an error
     * 
     * @param message The error message to display
     * @param error The error
     */
    public void notifyForError(String message, Throwable error);

}
