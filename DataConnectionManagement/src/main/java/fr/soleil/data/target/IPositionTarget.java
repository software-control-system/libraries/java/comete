/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target;

/**
 * An interface for managers that are able to manipulate some stack, and a position in this stack.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IPositionTarget extends ITarget {

    /**
     * Returns the last position set in stack
     * 
     * @return An <code>int[]</code>.
     */
    public int[] getPositions();

    /**
     * Puts a position for a particular dimension index
     * 
     * @param shapeIndex The dimension index
     * @param position The position to set
     */
    public void setPositionAt(int shapeIndex, int position);

    /**
     * Sets a position in stack
     * 
     * @param positions The position to set
     */
    public void setPositions(int... positions);

}
