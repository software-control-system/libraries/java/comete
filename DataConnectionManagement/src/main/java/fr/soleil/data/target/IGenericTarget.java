/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target;

import fr.soleil.data.controller.DataTargetController;
import fr.soleil.data.service.GenericDescriptor;

/**
 * An {@link ITarget} that uses generic methods to transmit data. You can always transmit an
 * {@link IGenericTarget} to a compatible {@link DataTargetController}.
 * 
 * @author huriez
 * 
 * @param <T> the data type of this {@link IGenericTarget}
 */
public interface IGenericTarget<T> extends ITarget {

    /**
     * Sets this {@link IGenericTarget}'s data
     * 
     * @param data the data to set
     */
    public void setTargetData(T data);

    /**
     * Returns this {@link IGenericTarget}'s data
     * 
     * @return the data treated by the target
     */
    public T getTargetData();

    /**
     * Returns the {@link GenericDescriptor} that describes the data type. This descriptor is used
     * because Generic classes (example {@link java.util.List List}) become unsigned
     * 
     * @return A {@link GenericDescriptor}
     */
    public GenericDescriptor getTargetType();
}
