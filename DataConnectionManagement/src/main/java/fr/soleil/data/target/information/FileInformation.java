/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target.information;

import java.io.File;

import fr.soleil.data.target.ITarget;

/**
 * A {@link TargetInformation} that can be used by {@link ITarget}s for a change in a {@link File}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class FileInformation extends TargetInformation<File> {

    /**
     * Constructor
     * 
     * @param target The concerned {@link ITarget}
     * @param info The {@link File} that changed
     */
    public FileInformation(ITarget target, File info) {
        super(target, info);
    }

}
