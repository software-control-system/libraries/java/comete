/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target;

import fr.soleil.data.service.thread.IDrawingThreadManager;

/**
 * An {@link ITarget} that can be drawn in a a drawing thread.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IDrawableTarget extends ITarget {

    /**
     * Returns the {@link IDrawingThreadManager} that may manage drawing thread for this {@link IDrawableTarget}.
     * 
     * @return An {@link IDrawingThreadManager}.
     */
    public IDrawingThreadManager getDrawingThreadManager();

}
