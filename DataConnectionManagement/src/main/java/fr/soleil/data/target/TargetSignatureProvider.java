/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.target;

import org.slf4j.LoggerFactory;

import fr.soleil.data.exception.UnhandledTargetSignatureException;
import fr.soleil.data.mediator.Mediator;

/**
 * This class provided, through the static method
 * {@link TargetSignatureProvider#getTargetSignature(Class, Class, Class)} the
 * {@link TargetSignature} associated with a {@link ITarget}. the word "form" here means the
 * superclass of the concerned {@link ITarget}
 * 
 * @author huriez
 * 
 */
public class TargetSignatureProvider {

    /**
     * This method retrieve the {@link TargetSignature} associated with the {@link ITarget} send as
     * parameter. It allow the controller to obtain the correct {@link TargetSignature} for his
     * {@link ITarget} object and use it to send data. The search for the {@link TargetSignature} is
     * recursive, that mean the {@link ITarget} send do not have to be the precise class to check
     * for. The method retrieve the first {@link TargetSignature} he find, corresponding to the type
     * and the form send as parameters.
     * 
     * @param <T> the complete type of the data treated by the {@link ITarget} associate
     * @param target the target the method have to check for the {@link TargetSignature} retrieval
     * @param type the type desired for the {@link TargetSignature}
     * @param form the form of the data desired for the {@link TargetSignature}
     * @return the {@link TargetSignature} associated with the target send
     * @throws UnhandledTargetSignatureException
     */
    public static <T> TargetSignature<T> getTargetSignature(Class<?> target, Class<?> type, Class<?> form)
            throws UnhandledTargetSignatureException {

        TargetSignature<T> result = null;
        StringBuilder errorTracks = new StringBuilder("Error Tracks :\n");
        errorTracks.append("Main Target : " + target.getName());
        result = checkClassSignature(target, type, form, errorTracks);
        if (result == null) {
            result = checkInnerClassInterfaces(target, type, form, errorTracks);
        }
        if (result == null) {
            errorTracks.append("\nform : " + form + ", type : " + type);
            throw new UnhandledTargetSignatureException(target.getName(), errorTracks);
        }
        return result;
    }

    /**
     * Do a recursive search into interfaces of the target parameter in order to find a
     * corresponding {@link TargetSignature}.
     * 
     * @param <T> the complete type of the data treated by the {@link ITarget} associate
     * @param target the target the method have to check for the {@link TargetSignature} retrieval
     * @param type the type desired for the {@link TargetSignature}
     * @param form the form of the data desired for the {@link TargetSignature}
     * @param errorTracks the StringBuiler used to register error tracks
     * @return the {@link TargetSignature} associated with the target send
     * @see TargetSignatureProvider#getTargetSignature(Class, Class, Class)
     */
    private static <T> TargetSignature<T> checkInnerClassInterfaces(Class<?> target, Class<?> type, Class<?> form,
            StringBuilder errorTracks) {

        TargetSignature<T> result = null;
        errorTracks.append("\nActual Class : " + target.getName());
        Class<?>[] actualInterfacesArray = target.getInterfaces();
        for (Class<?> actualInterface : actualInterfacesArray) {
            errorTracks.append("\n\tActual Interface : " + actualInterface.getName());
            result = checkClassSignature(actualInterface, type, form, errorTracks);
            if (result == null) {
                result = checkInnerClassInterfaces(actualInterface, type, form, errorTracks);
            }
            if (result != null) {
                break;
            }
        }

        return result;
    }

    /**
     * Check if a given target class has a corresponding {@link TargetSignature} and return it.
     * 
     * @param <T> the complete type of the data treated by the {@link ITarget} associate
     * @param target the target the method have to check for the {@link TargetSignature} retrieval
     * @param type the type desired for the {@link TargetSignature}
     * @param form the form of the data desired for the {@link TargetSignature}
     * @param errorTracks the StringBuiler used to register error tracks
     * @return the {@link TargetSignature} associated with the target send
     * @see TargetSignatureProvider#getTargetSignature(Class, Class, Class)
     */
    @SuppressWarnings("unchecked")
    private static <T> TargetSignature<T> checkClassSignature(Class<?> target, Class<?> type, Class<?> form,
            StringBuilder errorTracks) {
        TargetSignature<T> result = null;

        Class<?>[] listClass = target.getClasses();
        for (Class<?> actualClass : listClass) {
            errorTracks.append("\n\t\tInnerClass : " + actualClass.getName());
            if (TargetSignature.class.isAssignableFrom(actualClass)) {
                try {
                    errorTracks.append("\n\t\t\t - IsAssignable");
                    TargetSignature<?> tempSignature = (TargetSignature<?>) actualClass.newInstance();
                    if ((type == null || form == null)
                            || (type == tempSignature.targetType && form == tempSignature.targetForm)) {
                        errorTracks.append("\n\t\t\t - Represents : " + tempSignature.getClass());
                        result = (TargetSignature<T>) tempSignature;
                    }
                } catch (InstantiationException | IllegalAccessException e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to check class signature", e);
                }
            }
        }
        return result;
    }

    /**
     * An abstract class that defines the target signature, used to retrieve the data. It allow to
     * check if a {@link ITarget} correspond to the signature or to send data.
     * 
     * @author huriez
     * 
     * @param <T> The complete type of data managed by the {@link ITarget}s this
     *            {@link TargetSignature} is linked
     */
    public static abstract class TargetSignature<T> {

        private final Class<?> targetSignature;
        private final Class<?> targetType;
        private final Class<?> targetForm;

        /**
         * Constructor
         * 
         * @param signature the class of the {@link ITarget} this {@link TargetSignature} is linked
         * @param type the type of the {@link ITarget}
         * @param form the form of the {@link ITarget}
         */
        public TargetSignature(Class<?> signature, Class<?> type, Class<?> form) {
            targetSignature = signature;
            targetForm = form;
            targetType = type;
        }

        /**
         * Check if a target correspond to this signature
         * 
         * @param target the {@link ITarget} to test
         * @return
         */
        public boolean isAssignable(ITarget target) {
            return targetSignature.isAssignableFrom(target.getClass());
        }

        /**
         * Send data to the given {@link ITarget}
         * 
         * @param target the {@link ITarget} to send data
         * @param data the data to send
         */
        public void sendData(ITarget target, T data) {
            if (isAssignable(target)) {
                callFunction(target, data);
            }
        }

        /**
         * 
         * @return the primitive type of the target
         */
        public Class<?> getTargetType() {
            return targetType;
        }

        /**
         * Send data to the given {@link ITarget}. This method belong to the specific
         * {@link TargetSignature}
         * 
         * @param target the {@link ITarget} to send data
         * @param data the data to send
         */
        protected abstract void callFunction(ITarget target, T data);
    }

}
