/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.metric;

import java.util.concurrent.atomic.AtomicLong;

public class Statistic implements Metric {

    private final AtomicLong total = new AtomicLong(0);
    private final AtomicLong numSamples = new AtomicLong(0);
    private final String name;
    private final String description;

    public Statistic(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void reset() {
        total.set(0);
        numSamples.set(0);
    }

    @Override
    public String toString() {
        return "Name:" + description + ", Total: " + total.get() + ", Number of samples: " + numSamples.get();
    }

    @Override
    public long getNumberOfSamples() {
        return numSamples.get();
    }

    @Override
    public long getTotal() {
        return total.get();
    }

    public long add(long value) {
        numSamples.incrementAndGet();
        return total.addAndGet(value);
    }
}