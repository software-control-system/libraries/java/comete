/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.metric;

public enum TaskMetric {

    TASK_QUEUE_WAIT_TIME("Average Task Queue Wait Time",
            "Average time in milliseconds tasks had to wait in the queue of the thread pool executor"),
    TASK_END_TO_END_TIME("Cumulative Task End Time",
            "Cumulative time in milliseconds spent by tasks from creation to completion"),
    TASK_EXECUTION_TIME("Average Task Execution Time", "Average time in milliseconds tasks took to execute"),
    TASK_COUNT("Tasks Count", "Counter for number of tasks submitted to the thread pool executor"), REJECTED_TASK_COUNT(
            "Rejected Tasks Count", "Counter for number of tasks that were rejected by the thread pool executor");

    private final Statistic metric;

    private TaskMetric(String metricName, String metricDescription) {
        metric = new Statistic(metricName, metricDescription);
    }

    public void update(long value) {
        metric.add(value);
    }

    // exposed for testing.
    public Metric getMetric() {
        return metric;
    }

    @Override
    public String toString() {
        return metric.toString();
    }

}