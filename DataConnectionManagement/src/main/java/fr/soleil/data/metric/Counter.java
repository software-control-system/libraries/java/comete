/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.metric;

import java.util.concurrent.atomic.AtomicLong;

public class Counter implements Metric {

    private final AtomicLong counter;
    private final String name;
    private final String description;

    public Counter(String name, String description) {
        this.name = name;
        this.description = description;
        this.counter = new AtomicLong(0);
    }

    public long increment() {
        return counter.incrementAndGet();
    }

    public long getCurrentCount() {
        return counter.get();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void reset() {
        counter.set(0);
    }

    @Override
    public String toString() {
        return "Name: " + name + ", Current count: " + counter.get();
    }

    @Override
    public long getNumberOfSamples() {
        return getCurrentCount();
    }

    @Override
    public long getTotal() {
        return getCurrentCount();
    }
}
