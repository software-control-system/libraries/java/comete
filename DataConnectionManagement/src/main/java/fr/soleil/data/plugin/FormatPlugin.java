/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.plugin;

import java.util.Collection;
import java.util.Map;
import java.util.WeakHashMap;

import fr.soleil.data.controller.FormatController;
import fr.soleil.data.service.FormatHelper;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IFormatableTarget;
import fr.soleil.data.target.IMultiFormatableTarget;
import fr.soleil.data.target.ITarget;

public class FormatPlugin extends AbstractPlugin<String> {

    private String defaultFormat;
    private final Map<AbstractDataSource<String>, String> defaultFormats;

    public FormatPlugin(IKey key) {
        super(key, new FormatController());
        defaultFormats = new WeakHashMap<>();
    }

    public void setFormatEnabled(ITarget target, boolean state) {
        setPluginAllowed(target, state);
    }

    public boolean isFormatEnabled(ITarget target) {
        return isPluginAllowed(target);
    }

    @Override
    protected void registerDefaultValue(ITarget target) {
        if (target instanceof IFormatableTarget) {
            defaultFormat = ((IFormatableTarget) target).getFormat();
        }
    }

    @Override
    protected void registerDefaultValue(ITarget target, AbstractDataSource<String> source) {
        if (target instanceof IFormatableTarget) {
            defaultFormat = ((IFormatableTarget) target).getFormat();
        } else if ((target instanceof IMultiFormatableTarget) && (source != null)) {
            IMultiFormatableTarget multiFormatableTarget = (IMultiFormatableTarget) target;
            String id = source.toString();
            if (id != null) {
                String format = multiFormatableTarget.getFormat(id);
                synchronized (defaultFormats) {
                    if (format == null) {
                        defaultFormats.remove(source);
                    } else {
                        defaultFormats.put(source, format);
                    }
                }
            }
        }
    }

    @Override
    protected void resetDefaultValue(ITarget target) {
        if (target instanceof IFormatableTarget) {
            ((IFormatableTarget) target).setFormat(defaultFormat);
        }
    }

    @Override
    protected void resetDefaultValue(ITarget target, Collection<AbstractDataSource<String>> originSources) {
        if (target instanceof IFormatableTarget) {
            FormatHelper.setFormatToTarget((IFormatableTarget) target, defaultFormat);
        } else if ((target instanceof IMultiFormatableTarget) && (originSources != null)) {
            IMultiFormatableTarget multiFormatableTarget = (IMultiFormatableTarget) target;
            synchronized (defaultFormats) {
                for (AbstractDataSource<String> source : originSources) {
                    if (source != null) {
                        String id = source.toString();
                        if (id != null) {
                            FormatHelper.setFormatToTarget(multiFormatableTarget, id, defaultFormats.remove(source));
                        }
                    }
                }
            }
        }
    }

}
