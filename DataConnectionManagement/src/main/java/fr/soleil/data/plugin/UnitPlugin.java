/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.plugin;

import fr.soleil.data.controller.UnitController;
import fr.soleil.data.filter.DataExclusionChecker;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.UnitHelper;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.IUnitTarget;

public class UnitPlugin extends AbstractPlugin<String> {

    private String defaultValue;
    protected DataExclusionChecker dataExclusionChecker;

    public UnitPlugin(IKey key) {
        super(key, new UnitController());
        dataExclusionChecker = new DataExclusionChecker();
    }

    public void setUnitEnabled(ITarget target, boolean state) {
        setPluginAllowed(target, state);
    }

    public boolean isUnitEnabled(ITarget target) {
        return isPluginAllowed(target);
    }

    @Override
    protected void registerDefaultValue(ITarget target) {
        if (target != null && target instanceof IUnitTarget) {
            defaultValue = ((IUnitTarget) target).getUnit();
        }
    }

    @Override
    protected void resetDefaultValue(ITarget target) {
        if (target instanceof IUnitTarget) {
            UnitHelper.setUnitToTarget((IUnitTarget) target, defaultValue);
        }
    }

    @Override
    protected void addCheckers(AbstractDataSource<String> pluginSource, ITarget target) {
        super.addCheckers(pluginSource, target);
        pluginController.addCheckerToSource(dataExclusionChecker, pluginSource);
    }
}
