/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import fr.soleil.data.filter.BasicChecker;
import fr.soleil.data.filter.BlackListClassChecker;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;

public abstract class AbstractPlugin<U> {

    protected final AbstractController<U> pluginController;
    protected final IKey pluginKey;
    protected final BasicChecker sourceTransmissionAllowed;
    protected final BlackListClassChecker classTransmissionAllowed;
    private final Map<ITarget, Boolean> pluginAllowedMap;
    private final Map<ITarget, Boolean> connectionMap;

    public AbstractPlugin(IKey pluginSourceReference, AbstractController<U> controller) {
        this.pluginKey = pluginSourceReference;
        pluginController = controller;
        sourceTransmissionAllowed = new BasicChecker();
        classTransmissionAllowed = new BlackListClassChecker();
        classTransmissionAllowed.addAllToBlackList(initBlackList());
        pluginAllowedMap = new WeakHashMap<ITarget, Boolean>();
        connectionMap = new WeakHashMap<ITarget, Boolean>();
    }

    /**
     * Allows or not plugin connection to a given {@link ITarget}
     * 
     * @param target The {@link ITarget}
     * @param allowed Whether to allow plugin connection
     */
    public void setPluginAllowed(ITarget target, boolean allowed) {
        boolean changed = false;
        if (target != null) {
            synchronized (pluginAllowedMap) {
                changed = (allowed != getData(pluginAllowedMap, target, true));
                pluginAllowedMap.put(target, Boolean.valueOf(allowed));
            }
            if (changed) {
                if (allowed) {
                    boolean connected = getData(connectionMap, target, false);
                    if (connected) {
                        addPlugin(target, false);
                    }
                } else {
                    removePlugin(target, false);
                }
            }
        }
    }

    /**
     * Returns whether plugin connection is allowed for a given {@link ITarget}
     * 
     * @param target The {@link ITarget}
     * @return A <code>boolean</code> value
     */
    public boolean isPluginAllowed(ITarget target) {
        boolean allowed = false;
        if (target != null) {
            allowed = getData(pluginAllowedMap, target, true);
        }
        return allowed;
    }

    /**
     * Returns the {@link IKey} that identifies this plugin's {@link AbstractDataSource}
     * 
     * @return An {@link IKey}
     */
    public IKey getPluginKey() {
        return pluginKey;
    }

    /**
     * Make the link between the plugin' {@link AbstractDataSource} and the {@link ITarget}.
     * 
     * @param target the target to link with
     * @return true if the connection work, false otherwise
     */
    public boolean addPlugin(ITarget target) {
        return addPlugin(target, true);
    }

    protected boolean addPlugin(ITarget target, boolean updateConnection) {
        boolean result = false;
        if ((target != null) && (pluginController != null)) {
            AbstractDataSource<U> source = getSource();
            if (source != null) {
                if (classTransmissionAllowed.isConfirmed(target, null)) {
                    if (updateConnection) {
                        synchronized (connectionMap) {
                            connectionMap.put(target, true);
                        }
                        registerDefaultValue(target, source);
                    }
                    if (pluginController.isTargetAssignable(target)) {
                        addCheckers(source, target);
                    }
                    boolean allowed = getData(pluginAllowedMap, target, true);
                    if (allowed) {
                        result = pluginController.addLink(source, target);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Extracts a <code>boolean</code> value from a {@link Map}
     * 
     * @param map The {@link Map}
     * @param target The {@link ITarget}, expected to be a key for the {@link Map}
     * @param defaultValue The default value to return if no matching
     * @return A <code>boolean</code> value
     */
    protected boolean getData(Map<ITarget, Boolean> map, ITarget target, boolean defaultValue) {
        Boolean result = null;
        if ((map != null) && (target != null)) {
            synchronized (map) {
                result = map.get(target);
            }
        }
        if (result == null) {
            result = Boolean.valueOf(defaultValue);
        }
        return result.booleanValue();
    }

    /**
     * Returns this plugin's {@link AbstractDataSource}
     * 
     * @return An {@link AbstractDataSource}. May be <code>null</code>.
     */
    @SuppressWarnings("unchecked")
    protected AbstractDataSource<U> getSource() {
        AbstractDataSource<U> source = null;
        if (pluginKey != null) {
            IDataSourceProducer producer = DataSourceProducerProvider.getProducer(pluginKey.getSourceProduction());
            if (producer != null) {
                source = (AbstractDataSource<U>) producer.createDataSource(pluginKey);
            }
        }
        return source;
    }

    protected void addCheckers(AbstractDataSource<U> pluginSource, ITarget target) {
        if ((pluginSource != null) && (target != null)) {
            pluginController.addCheckerToSource(sourceTransmissionAllowed, pluginSource);
        }
    }

    /**
     * Register the default value from the method treated by this plugin
     * 
     * @param target the target to register
     */
    protected abstract void registerDefaultValue(ITarget target);

    /**
     * Register the default value from the method treated by this plugin according to an {@link AbstractDataSource}
     * 
     * @param target the target to register
     * @param source the {@link AbstractDataSource}
     */
    protected void registerDefaultValue(ITarget target, AbstractDataSource<U> source) {
        registerDefaultValue(target);
    }

    /**
     * Sets the default value previously registered for a target
     * 
     * @param target the target to set the default value registered before
     */
    protected abstract void resetDefaultValue(ITarget target);

    /**
     * Sets the default value for a target, previously registered before the connection to some sources
     * 
     * @param target the target to set the default value registered before
     * @param originSources the sources
     */
    protected void resetDefaultValue(ITarget target, Collection<AbstractDataSource<U>> originSources) {
        resetDefaultValue(target);
    }

    /**
     * Remove link between the {@link ITarget} parameter and this plugin's {@link AbstractDataSource}.
     * 
     * @param target the {@link ITarget} used in the link to remove.
     * @param key the {@link IKey} the source was made from.
     */
    public void removePlugin(ITarget target) {
        removePlugin(target, true);
    }

    protected void removePlugin(ITarget target, boolean updateConnectionMap) {
        if ((target != null) && (pluginKey != null)) {
            List<AbstractDataSource<U>> toRemove = new ArrayList<AbstractDataSource<U>>();
            Collection<AbstractDataSource<U>> relatedSources = pluginController.getAllRelatedSources(target);
            for (AbstractDataSource<U> source : relatedSources) {
                if (pluginKey.equals(source.getOriginDescriptor())) {
                    toRemove.add(source);
                }
            }
            for (AbstractDataSource<U> source : toRemove) {
                pluginController.removeLink(source, target);
            }
            toRemove.clear();
            if (updateConnectionMap) {
                synchronized (connectionMap) {
                    connectionMap.remove(target);
                }
            }
            if (pluginController.getAllRelatedSources(target).isEmpty()) {
                resetDefaultValue(target, relatedSources);
            }
            relatedSources.clear();
        }
    }

    /**
     * Removes all possible link with a given {@link ITarget} in this plugin's known {@link Mediator}
     * 
     * @param target The {@link ITarget}
     */
    public void removePluginFromAll(ITarget target) {
        if (target != null) {
            Collection<AbstractDataSource<U>> relatedSources = pluginController.getAllRelatedSources(target);
            pluginController.removeAllReferencesTo(target);
            synchronized (connectionMap) {
                connectionMap.remove(target);
            }
            resetDefaultValue(target, relatedSources);
            relatedSources.clear();
        }
    }

    /**
     * Disconnects some {@link ITarget}s from their associated {@link AbstractDataSource}s, if theses {@link ITarget}s
     * are the last one connected to these {@link AbstractDataSource}s
     * 
     * @param targetsToClean The {@link ITarget}s to disconnect
     */
    public void removePluginFromAllIfLast(ITarget... targetsToClean) {
        if (targetsToClean != null) {
            List<ITarget> targetList = Arrays.asList(targetsToClean);
            List<AbstractDataSource<U>> sources = pluginController.getSourceList();
            for (AbstractDataSource<U> source : sources) {
                List<ITarget> connectedTargets = pluginController.getAllRelatedTargets(source);
                boolean containsOtherTargets = false;
                for (ITarget target : connectedTargets) {
                    if (!targetList.contains(target)) {
                        containsOtherTargets = true;
                        break;
                    }
                }
                if (!containsOtherTargets) {
                    for (ITarget target : targetList) {
                        pluginController.removeLink(source, target);
                    }
                }
            }
        }
    }

    /**
     * Disconnects some {@link ITarget}s of given classes from their associated {@link AbstractDataSource}s, if theses
     * {@link ITarget}s are the last one connected to these {@link AbstractDataSource}s
     * 
     * @param targetsToClean The {@link ITarget} classes to disconnect
     */
    public void removePluginFromAllIfLast(Class<?>... targetsToClean) {
        if (targetsToClean != null) {
            List<AbstractDataSource<U>> sources = pluginController.getSourceList();
            for (AbstractDataSource<U> source : sources) {
                List<ITarget> connectedTargets = pluginController.getAllRelatedTargets(source);
                boolean containsOtherTargets = false;
                for (ITarget target : connectedTargets) {
                    if (!containsTarget(targetsToClean, target)) {
                        containsOtherTargets = true;
                        break;
                    }
                }
                if (!containsOtherTargets) {
                    for (ITarget target : connectedTargets) {
                        pluginController.removeLink(source, target);
                    }
                }
            }
        }
    }

    /**
     * Returns whether a given target extends any class of a given class list
     * 
     * @param targetList The class list
     * @param target The target to check
     * @return A <code>boolean</code>
     */
    protected boolean containsTarget(Class<?>[] targetList, ITarget target) {
        boolean contains = false;
        if ((targetList != null) && (target != null)) {
            for (Class<?> targetClass : targetList) {
                if (targetClass.isAssignableFrom(target.getClass())) {
                    contains = true;
                    break;
                }
            }
        }
        return contains;
    }

    /**
     * Initializes the {@link ITarget} black list (the list of {@link ITarget} not to be compatible with)
     * 
     * @return A {@link Class} {@link Set}
     */
    protected Set<Class<?>> initBlackList() {
        return new HashSet<Class<?>>();
    }

    protected void setSourceTransmissionAllowed(boolean allowed) {
        sourceTransmissionAllowed.setAllowed(allowed);
    }

    protected boolean isSourceTransmissionAllowed() {
        return sourceTransmissionAllowed.isAllowed();
    }

    public void setFirstInitAllowed(boolean allowed) {
        pluginController.setFirstInitAllowed(allowed);
    }

    public boolean isFirstInitAllowed() {
        return pluginController.isFirstInitAllowed();
    }
}
