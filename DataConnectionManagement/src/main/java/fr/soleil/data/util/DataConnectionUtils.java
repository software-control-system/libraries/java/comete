/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.util;

import javax.activation.UnsupportedDataTypeException;

import org.slf4j.LoggerFactory;

import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.BigIntegerMatrix;
import fr.soleil.data.container.matrix.ByteMatrix;
import fr.soleil.data.container.matrix.IntMatrix;
import fr.soleil.data.container.matrix.LongMatrix;
import fr.soleil.data.container.matrix.ShortMatrix;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.math.UnsignedConverter;
import fr.soleil.lib.project.resource.MessageManager;

public class DataConnectionUtils {

    private static final String CONVERT_UNSIGNED_BYTE_MATRIX_KEY = "fr.soleil.data.unsigned.convert.byte";
    private static final String CONVERT_UNSIGNED_SHORT_MATRIX_KEY = "fr.soleil.data.unsigned.convert.short";
    private static final String CONVERT_UNSIGNED_INT_MATRIX_KEY = "fr.soleil.data.unsigned.convert.int";
    private static final String CONVERT_UNSIGNED_LONG_MATRIX_KEY = "fr.soleil.data.unsigned.convert.long";

    private static final String CONVERT_SIGNED_BIGINT_MATRIX_KEY = "fr.soleil.data.unsigned.convert.back.bigint";
    private static final String CONVERT_SIGNED_LONG_MATRIX_KEY = "fr.soleil.data.unsigned.convert.back.long";
    private static final String CONVERT_SIGNED_INT_MATRIX_KEY = "fr.soleil.data.unsigned.convert.back.int";
    private static final String CONVERT_SIGNED_SHORT_MATRIX_KEY = "fr.soleil.data.unsigned.convert.back.short";

    public static final MessageManager MESSAGE_MANAGER = new MessageManager("fr.soleil.data");

    /**
     * Converts any unsigned values (arrays and {@link AbstractNumberMatrix} allowed).
     * <p>
     * This method calls {@link UnsignedConverter#convertUnsigned(Object)} but adds compatibility with
     * {@link AbstractNumberMatrix} cases.
     * </p>
     * 
     * @param data The value to convert.
     * @return The converted value.
     * @see UnsignedConverter#convertUnsigned(Object)
     */
    public static Object convertUnsigned(Object data) {
        Object convertedData = data;
        if (data != null) {
            if ((data instanceof Number) || data.getClass().isArray()) {
                convertedData = UnsignedConverter.convertUnsigned(data);
            } else if (data instanceof AbstractNumberMatrix<?>) {
                if (data instanceof ByteMatrix) {
                    ByteMatrix matrix = (ByteMatrix) data;
                    ShortMatrix newMatrix = new ShortMatrix(
                            Byte.TYPE.equals(matrix.getType()) ? Short.TYPE : Short.class);
                    try {
                        newMatrix.setFlatValue(UnsignedConverter.convertUnsigned(matrix.getFlatValue()),
                                matrix.getHeight(), matrix.getWidth());
                        convertedData = newMatrix;
                    } catch (UnsupportedDataTypeException e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .warn(MESSAGE_MANAGER.getMessage(CONVERT_UNSIGNED_BYTE_MATRIX_KEY), e);
                    }
                } else if (data instanceof ShortMatrix) {
                    ShortMatrix matrix = (ShortMatrix) data;
                    IntMatrix newMatrix = new IntMatrix(
                            Short.TYPE.equals(matrix.getType()) ? Integer.TYPE : Integer.class);
                    try {
                        newMatrix.setFlatValue(UnsignedConverter.convertUnsigned(matrix.getFlatValue()),
                                matrix.getHeight(), matrix.getWidth());
                        convertedData = newMatrix;
                    } catch (UnsupportedDataTypeException e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .warn(MESSAGE_MANAGER.getMessage(CONVERT_UNSIGNED_SHORT_MATRIX_KEY), e);
                    }
                } else if (data instanceof IntMatrix) {
                    IntMatrix matrix = (IntMatrix) data;
                    LongMatrix newMatrix = new LongMatrix(
                            Integer.TYPE.equals(matrix.getType()) ? Long.TYPE : Long.class);
                    try {
                        newMatrix.setFlatValue(UnsignedConverter.convertUnsigned(matrix.getFlatValue()),
                                matrix.getHeight(), matrix.getWidth());
                        convertedData = newMatrix;
                    } catch (UnsupportedDataTypeException e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .warn(MESSAGE_MANAGER.getMessage(CONVERT_UNSIGNED_INT_MATRIX_KEY), e);
                    }
                } else if (UnsignedConverter.isUnsignedLongSupported() && (data instanceof LongMatrix)) {
                    LongMatrix matrix = (LongMatrix) data;
                    BigIntegerMatrix newMatrix = new BigIntegerMatrix();
                    try {
                        newMatrix.setFlatValue(UnsignedConverter.convertUnsigned(matrix.getFlatValue()),
                                matrix.getHeight(), matrix.getWidth());
                        convertedData = newMatrix;
                    } catch (UnsupportedDataTypeException e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .warn(MESSAGE_MANAGER.getMessage(CONVERT_UNSIGNED_LONG_MATRIX_KEY), e);
                    }
                }
            } // end else if (data instanceof AbstractNumberMatrix<?>)
        } // end if (data != null)
        return convertedData;
    }

    /**
     * Does the revert operation of {@link #convertUnsigned(Object)}
     * <p>
     * This method calls {@link UnsignedConverter#undoUnsignedConversion(Object)} but adds compatibility with
     * {@link AbstractNumberMatrix} cases.
     * </p>
     * 
     * @param data The value to convert.
     * @return The converted value.
     * @see UnsignedConverter#undoUnsignedConversion(Object)
     */
    public static Object undoUnsignedConversion(Object data) {
        Object convertedData = data;
        if (data != null) {
            if ((data instanceof Number) || data.getClass().isArray()) {
                convertedData = UnsignedConverter.undoUnsignedConversion(data);
            } else if (data instanceof AbstractNumberMatrix<?>) {
                if (data instanceof BigIntegerMatrix) {
                    BigIntegerMatrix matrix = (BigIntegerMatrix) data;
                    LongMatrix newMatrix = new LongMatrix(Long.TYPE);
                    try {
                        newMatrix.setFlatValue(UnsignedConverter.undoUnsignedConversion(matrix.getFlatValue()),
                                matrix.getHeight(), matrix.getWidth());
                        convertedData = newMatrix;
                    } catch (UnsupportedDataTypeException e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .warn(MESSAGE_MANAGER.getMessage(CONVERT_SIGNED_BIGINT_MATRIX_KEY), e);
                    }
                } else if (data instanceof LongMatrix) {
                    LongMatrix matrix = (LongMatrix) data;
                    IntMatrix newMatrix = new IntMatrix(
                            Long.TYPE.equals(matrix.getType()) ? Integer.TYPE : Integer.class);
                    try {
                        newMatrix.setFlatValue(UnsignedConverter.undoUnsignedConversion(matrix.getFlatValue()),
                                matrix.getHeight(), matrix.getWidth());
                        convertedData = newMatrix;
                    } catch (UnsupportedDataTypeException e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .warn(MESSAGE_MANAGER.getMessage(CONVERT_SIGNED_LONG_MATRIX_KEY), e);
                    }
                } else if (data instanceof IntMatrix) {
                    IntMatrix matrix = (IntMatrix) data;
                    ShortMatrix newMatrix = new ShortMatrix(
                            Integer.TYPE.equals(matrix.getType()) ? Short.TYPE : Short.class);
                    try {
                        newMatrix.setFlatValue(UnsignedConverter.undoUnsignedConversion(matrix.getFlatValue()),
                                matrix.getHeight(), matrix.getWidth());
                        convertedData = newMatrix;
                    } catch (UnsupportedDataTypeException e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .warn(MESSAGE_MANAGER.getMessage(CONVERT_SIGNED_INT_MATRIX_KEY), e);
                    }
                } else if (data instanceof ShortMatrix) {
                    ShortMatrix matrix = (ShortMatrix) data;
                    ByteMatrix newMatrix = new ByteMatrix(Short.TYPE.equals(matrix.getType()) ? Byte.TYPE : Byte.class);
                    try {
                        newMatrix.setFlatValue(UnsignedConverter.undoUnsignedConversion(matrix.getFlatValue()),
                                matrix.getHeight(), matrix.getWidth());
                        convertedData = newMatrix;
                    } catch (UnsupportedDataTypeException e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .warn(MESSAGE_MANAGER.getMessage(CONVERT_SIGNED_SHORT_MATRIX_KEY), e);
                    }
                }
            } // end else if (data instanceof AbstractNumberMatrix<?>)
        } // end if (data != null)
        return convertedData;
    }
}
