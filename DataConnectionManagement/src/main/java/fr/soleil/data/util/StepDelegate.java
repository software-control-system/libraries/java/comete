/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.util;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.data.event.StepEvent;
import fr.soleil.data.listener.IStepListener;
import fr.soleil.data.manager.IStepManager;

public class StepDelegate implements IStepManager {

    protected final Collection<IStepListener> listeners;

    public StepDelegate() {
        listeners = Collections.newSetFromMap(new ConcurrentHashMap<IStepListener, Boolean>());
    }

    @Override
    public void addStepListener(IStepListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeStepListener(IStepListener listener) {
        if (listener != null) {
            listeners.remove(listener);
        }
    }

    public void warnListeners(IStepManager source) {
        warnListeners(new StepEvent(source));
    }

    public void warnListeners(StepEvent stepEvent) {
        for (IStepListener listener : listeners) {
            if (listener != null) {
                listener.stepOccurred(stepEvent);
            }
        }
    }

}
