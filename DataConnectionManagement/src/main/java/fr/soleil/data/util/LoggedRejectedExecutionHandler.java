/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.util;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;

public class LoggedRejectedExecutionHandler implements RejectedExecutionHandler {

    protected final Logger logger;

    public LoggedRejectedExecutionHandler(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void rejectedExecution(Runnable worker, ThreadPoolExecutor executor) {
        if (logger != null) {
            logger.warn(String.format(DataConnectionUtils.MESSAGE_MANAGER.getMessage("fr.soleil.data.task.rejected"),
                    String.valueOf(worker)));
        }
    }

}
