/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.IFormatableTarget;
import fr.soleil.lib.project.Format;

/**
 * A {@link DataToStringAdapter} that adapts some {@link Number}s to {@link String} values
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <N> The type of {@link Number} managed by this {@link NumberToStringAdapter}
 */
public class NumberToStringAdapter<N extends Number> extends DataToStringAdapter<N> implements IFormatableTarget {

    protected String format;

    /**
     * Constructs a new {@link NumberToStringAdapter}, settings its managed {@link Number} type
     * 
     * @param firstType The type of {@link Number} managed by this {@link NumberToStringAdapter}
     */
    public NumberToStringAdapter(Class<N> firstType) {
        super(firstType);
        format = null;
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String adapt(N data) throws DataAdaptationException {
        String result;
        if (data == null) {
            result = null;
        } else {
            String formatToUse = format;
            if ((formatToUse == null) || formatToUse.trim().isEmpty() || (!Format.isNumberFormatOK(formatToUse))) {
                result = data.toString();
            } else {
                try {
                    result = Format.format(formatToUse, data);
                } catch (Exception e) {
                    throw new DataAdaptationException("Failed to adapt number data: '" + data + "'", e);
                }
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public N revertAdapt(String data) throws DataAdaptationException {
        N result;
        if (data == null) {
            result = null;
        } else if (firstType == null) {
            throw new DataAdaptationException("Can't adapt String to unknown number type");
        } else {
            try {
                if ((Byte.TYPE.equals(firstType)) || Byte.class.equals(firstType)) {
                    result = (N) Byte.valueOf(data);
                } else if ((Short.TYPE.equals(firstType)) || Short.class.equals(firstType)) {
                    result = (N) Short.valueOf(data);
                } else if ((Integer.TYPE.equals(firstType)) || Integer.class.equals(firstType)) {
                    result = (N) Integer.valueOf(data);
                } else if ((Long.TYPE.equals(firstType)) || Long.class.equals(firstType)) {
                    result = (N) Long.valueOf(data);
                } else if ((Float.TYPE.equals(firstType)) || Float.class.equals(firstType)) {
                    result = (N) Float.valueOf(data);
                } else {
                    result = (N) Double.valueOf(data);
                }
            } catch (Exception e) {
                throw new DataAdaptationException("Failed to adapt value '" + data + "' to number value", e);
            }
        }
        return result;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // the adapter will never interact with mediators
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // the adapter will never interact with mediators
    }

}
