/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import java.lang.reflect.Array;

import javax.activation.UnsupportedDataTypeException;

import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;

public class AbstractNumberMatrixToNumberArrayConvertor<N extends Number>
        extends AbstractAdapter<AbstractNumberMatrix<N>, Object> {

    protected Class<N> matrixScalarClass;
    protected Class<? extends Number> arrayScalarClass;

    public AbstractNumberMatrixToNumberArrayConvertor(Class<N> matrixScalarClass,
            Class<? extends Number> arrayScalarClass) {
        super(AbstractNumberMatrix.class, Object.class);
        this.matrixScalarClass = matrixScalarClass;
        this.arrayScalarClass = arrayScalarClass;
    }

    @Override
    public Object adapt(AbstractNumberMatrix<N> data) throws DataAdaptationException {
        Object result = null;
        if (Object.class.equals(arrayScalarClass) || ObjectUtils.sameObject(matrixScalarClass, arrayScalarClass)) {
            result = data.getFlatValue();
        } else if (arrayScalarClass == null) {
            throw new DataAdaptationException("Can not convert matrix data to unknown Object array");
        } else {
            result = NumberArrayUtils.extractArray(data.getFlatValue(), arrayScalarClass);
        }
        return result;
    }

    @Override
    public AbstractNumberMatrix<N> revertAdapt(Object data) throws DataAdaptationException {
        AbstractNumberMatrix<N> result;
        if (data == null) {
            result = null;
        } else if (matrixScalarClass == null) {
            throw new DataAdaptationException("Can not convert array to unknown matrix type");
        } else if (data.getClass().isArray()) {
            int length = Array.getLength(data);
            result = generateMatrix();
            try {
                result.setFlatValue(NumberArrayUtils.extractArray(data, matrixScalarClass), 1, length);
            } catch (UnsupportedDataTypeException e) {
                throw new DataAdaptationException("Unexpected data conversion exception", e);
            }
        } else {
            throw new DataAdaptationException("Can not convert data '" + data + "' because it is not an array");
        }
        return result;
    }

    /**
     * Generates a new {@link AbstractNumberMatrix} of expected scalar type
     * 
     * @return An {@link AbstractNumberMatrix}
     * @throws DataAdaptationException If no such {@link AbstractMatrix} could be created
     */
    protected AbstractNumberMatrix<N> generateMatrix() throws DataAdaptationException {
        try {
            return new AbstractNumberMatrix<N>(matrixScalarClass) {
                @Override
                protected Class<N> generateDefaultDataType() {
                    return matrixScalarClass;
                }
            };
        } catch (Exception e) {
            throw new DataAdaptationException("Failed to create a valid AbstractMatrix for data adaptation", e);
        }
    }

}
