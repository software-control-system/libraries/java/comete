/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter.source;

import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;

/**
 * An {@link IDataContainer} that is able to adapt data between an {@link AbstractDataSource} and
 * something else. This is typically used by {@link fr.soleil.data.mediator.Mediator}s An
 * {@link DataSourceAdapter} generally own a {@link AbstractAdapter} used to convert primitive types
 * theses datas have.
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <U> The type of data the associated {@link AbstractDataSource} understands
 * @param <T> The type of data This {@link IDataContainer} can return
 */
public class DataSourceAdapter<U, T> implements IDataContainer<T> {

    protected IDataContainer<U> dataSource;
    protected final AbstractAdapter<U, T> adapter;

    /**
     * Constructs the adapter, associating it with and {@link AbstractDataSource}
     * 
     * @param dataSource The {@link AbstractDataSource} this {@link DataSourceAdapter} should be
     *            associated with
     */
    public DataSourceAdapter(IDataContainer<U> dataSource, AbstractAdapter<U, T> adapter) {
        this.dataSource = dataSource;
        this.adapter = adapter;
    }

    @Override
    public T getData() throws Exception {
        T result = null;
        if (dataSource != null) {
            result = adaptSourceData(dataSource.getData());
        }
        return result;
    }

    @Override
    public void setData(T data) throws Exception {
        if (dataSource != null) {
            dataSource.setData(adaptContainerData(data));
        }
    }

    /**
     * Transforms a data that the {@link AbstractDataSource} can understand into a data this
     * {@link IDataContainer} can return
     * 
     * @param data The data the {@link AbstractDataSource} can understand
     * @return The expected transformed data
     */
    protected T adaptSourceData(U data) throws DataAdaptationException {
        T result = null;
        if (adapter != null) {
            result = adapter.adapt(data);
        }
        return result;
    }

    /**
     * Transforms a data this {@link IDataContainer} can understand into a data the associated
     * {@link AbstractDataSource} can understand
     * 
     * @param data The data this {@link IDataContainer} can understand
     * @return The expected transformed data
     */
    protected U adaptContainerData(T data) throws DataAdaptationException {
        U result = null;
        if (adapter != null) {
            result = adapter.revertAdapt(data);
        }
        return result;
    }

    public void setDataSource(IDataContainer<U> dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public boolean isSettable() {
        return dataSource.isSettable();
    }

    @Override
    public GenericDescriptor getDataType() {
        return dataSource.getDataType();
    }

    @Override
    public boolean isUnsigned() {
        return dataSource.isUnsigned();
    }

}
