/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import fr.soleil.data.exception.DataAdaptationException;

public class NumberToBooleanAdapter<N extends Number> extends AbstractAdapter<N, Boolean> {

    public NumberToBooleanAdapter(Class<N> firstType) {
        super(firstType, Boolean.class);
    }

    @Override
    public Boolean adapt(N data) throws DataAdaptationException {
        Boolean result;
        if (data == null) {
            result = null;
        } else {
            result = (data.byteValue() != 0);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public N revertAdapt(Boolean data) throws DataAdaptationException {
        N result;
        if (data == null) {
            result = null;
        } else if (firstType == null) {
            throw new DataAdaptationException("Can't adapt Boolean to unknown number type");
        } else {
            boolean test = data.booleanValue();
            if ((Byte.TYPE.equals(firstType)) || Byte.class.equals(firstType)) {
                result = (N) (test ? Byte.valueOf((byte) 1) : Byte.valueOf((byte) 0));
            } else if ((Short.TYPE.equals(firstType)) || Short.class.equals(firstType)) {
                result = (N) (test ? Short.valueOf((short) 1) : Short.valueOf((short) 0));
            } else if ((Integer.TYPE.equals(firstType)) || Integer.class.equals(firstType)) {
                result = (N) (test ? Integer.valueOf(1) : Integer.valueOf(0));
            } else if ((Long.TYPE.equals(firstType)) || Long.class.equals(firstType)) {
                result = (N) (test ? Long.valueOf(1) : Long.valueOf(0));
            } else if ((Float.TYPE.equals(firstType)) || Float.class.equals(firstType)) {
                result = (N) (test ? Float.valueOf(1) : Float.valueOf(0));
            } else {
                result = (N) (test ? Double.valueOf(1) : Double.valueOf(0));
            }
        }
        return result;
    }

}
