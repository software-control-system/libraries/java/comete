/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import java.lang.reflect.Array;

import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.service.GenericDescriptor;

/**
 * An {@link AbstractAdapter} that handles arrays conversions, using another {@link AbstractAdapter}
 * that should adapter array elements
 * 
 * @param <U> The type of elements in the first kind of array
 * @param <T> The type of elements in the second kind of array
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ArrayAdapter<U, T> extends AbstractAdapter<U[], T[]> {

    private final AbstractAdapter<U, T> adapter;

    public ArrayAdapter(Class<U[]> firstType, Class<T[]> secondType) {
        super(firstType, secondType);
        if ((firstType == null) || (secondType == null)) {
            adapter = null;
        } else {
            adapter = AdapterUtils.getAdapter(new GenericDescriptor(firstType.getComponentType()),
                    new GenericDescriptor(secondType.getComponentType()));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public T[] adapt(U[] data) throws DataAdaptationException {
        checkAdapter(firstType, secondType);
        T[] result;
        if (data == null) {
            result = null;
        } else {
            try {
                result = (T[]) Array.newInstance(secondType, data.length);
                for (int i = 0; i < data.length; i++) {
                    result[i] = adapter.adapt(data[i]);
                }
            } catch (DataAdaptationException e) {
                throw e;
            } catch (Exception e) {
                throw generateException(firstType, secondType, e);
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public U[] revertAdapt(T[] data) throws DataAdaptationException {
        checkAdapter(secondType, firstType);
        U[] result;
        if (data == null) {
            result = null;
        } else {
            try {
                result = (U[]) Array.newInstance(firstType, data.length);
                for (int i = 0; i < data.length; i++) {
                    result[i] = adapter.revertAdapt(data[i]);
                }
            } catch (DataAdaptationException e) {
                throw e;
            } catch (Exception e) {
                throw generateException(secondType, firstType, e);
            }
        }
        return result;
    }

    /**
     * Checks whether this {@link ArrayAdapter} is able o adapt data
     * 
     * @throws DataAdaptationException If this {@link ArrayAdapter} is not able to adapt data
     */
    public void checkAdapter() throws DataAdaptationException {
        checkAdapter(firstType, secondType);
    }

    /**
     * Checks whether contained {@link AbstractAdapter} is <code>null</code>. If so, throws a
     * {@link DataAdaptationException} explaining what kind of data adaptation can't be done
     * 
     * @param firstClass The type of data to transformed
     * @param secondClass The type of data to be transformed into
     * @throws DataAdaptationException If contained {@link AbstractAdapter} is <code>null</code>
     */
    protected void checkAdapter(Class<?> firstClass, Class<?> secondClass) throws DataAdaptationException {
        if (adapter == null) {
            throw new DataAdaptationException(
                    this + " can't adapt data of class " + firstClass + " into data of class " + secondClass);
        }
    }

    /**
     * Creates a {@link DataAdaptationException}, explaining why some data adaptation could not be
     * done (due to a problem, explained in a {@link Throwable})
     * 
     * @param firstClass The type of data to transformed
     * @param secondClass The type of data to be transformed into
     * @param cause The {@link Throwable} at the origin of the data adaptation problem
     * @return A {@link DataAdaptationException}
     */
    protected DataAdaptationException generateException(Class<?> firstClass, Class<?> secondClass, Throwable cause) {
        return new DataAdaptationException(
                this + " failed adapt data of class " + firstClass + " into data of class " + secondClass, cause);
    }
}
