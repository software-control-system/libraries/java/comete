/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import java.util.Arrays;

import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An {@link AbstractAdapter} that adapts some data to {@link String} values
 * 
 * @param <U> The type of data managed by this {@link DataToStringAdapter}
 * @author Rapha&euml;l GIRARDOT
 */
public class DataToStringAdapter<U> extends AbstractAdapter<U, String> {

    private static final String ARRAY_START = "[";
    private static final String ARRAY_END = "]";
    private static final String CAN_NOT_REVERT_ADAPT_DATA = " can not revert adapt data";

    /**
     * Constructs a new {@link DataToStringAdapter}, settings its managed data type
     * 
     * @param firstType The type of data managed by this {@link DataToStringAdapter}
     */
    public DataToStringAdapter(Class<?> firstType) {
        super(firstType, String.class);
    }

    @Override
    public String adapt(U data) throws DataAdaptationException {
        String result;
        if (data == null) {
            result = null;
        } else if (data.getClass().isArray()) {
            int rank = ArrayUtils.recoverArrayRank(data.getClass());
            if (rank == 2) {
                result = matrixToString((Object[]) data);
            } else if (rank > 1) {
                result = Arrays.deepToString((Object[]) data);
            } else if (data instanceof Object[]) {
                result = Arrays.toString((Object[]) data);
            } else if (data instanceof boolean[]) {
                result = Arrays.toString((boolean[]) data);
            } else if (data instanceof char[]) {
                result = Arrays.toString((char[]) data);
            } else if (data instanceof byte[]) {
                result = Arrays.toString((byte[]) data);
            } else if (data instanceof short[]) {
                result = Arrays.toString((short[]) data);
            } else if (data instanceof int[]) {
                result = Arrays.toString((int[]) data);
            } else if (data instanceof long[]) {
                result = Arrays.toString((long[]) data);
            } else if (data instanceof float[]) {
                result = Arrays.toString((float[]) data);
            } else if (data instanceof double[]) {
                result = Arrays.toString((double[]) data);
            } else {
                result = data.toString();
            }
        } else if (data instanceof AbstractMatrix<?>) {
            result = matrixToString(((AbstractMatrix<?>) data).getValue());
        } else {
            result = data.toString();
        }
        return result;
    }

    /**
     * Returns the {@link String} value of a matrix
     * 
     * @param matrix The matrix
     * @return A {@link String}
     */
    protected String matrixToString(Object[] matrix) {
        StringBuilder builder = new StringBuilder();
        if (matrix == null) {
            builder.append(String.valueOf(matrix));
        } else {
            Class<?> type = ArrayUtils.recoverDataType(matrix);
            int[] size = ArrayUtils.recoverShape(matrix);
            if (size.length == 1) {
                // prefer \n as separator for single dimension arrays
                for (Object val : matrix) {
                    builder.append(val).append(ObjectUtils.NEW_LINE);
                }
            } else if (matrix.length == 1) {
                // prefer \n as separator for single dimension arrays
                if (type.isPrimitive()) {
                    if (Character.TYPE.equals(type)) {
                        char[] row = (char[]) matrix[0];
                        for (char val : row) {
                            builder.append(val).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Boolean.TYPE.equals(type)) {
                        boolean[] row = (boolean[]) matrix[0];
                        for (boolean val : row) {
                            builder.append(val).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Byte.TYPE.equals(type)) {
                        byte[] row = (byte[]) matrix[0];
                        for (byte val : row) {
                            builder.append(val).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Short.TYPE.equals(type)) {
                        short[] row = (short[]) matrix[0];
                        for (short val : row) {
                            builder.append(val).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Integer.TYPE.equals(type)) {
                        int[] row = (int[]) matrix[0];
                        for (int val : row) {
                            builder.append(val).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Long.TYPE.equals(type)) {
                        long[] row = (long[]) matrix[0];
                        for (long val : row) {
                            builder.append(val).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Float.TYPE.equals(type)) {
                        float[] row = (float[]) matrix[0];
                        for (float val : row) {
                            builder.append(val).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Double.TYPE.equals(type)) {
                        double[] row = (double[]) matrix[0];
                        for (double val : row) {
                            builder.append(val).append(ObjectUtils.NEW_LINE);
                        }
                    }
                } else {
                    if (size.length == 2) {
                        Object[] row = (Object[]) matrix[0];
                        for (Object val : row) {
                            builder.append(val).append(ObjectUtils.NEW_LINE);
                        }
                    } else {
                        for (Object row : matrix) {
                            builder.append(Arrays.toString((Object[]) row)).append(ObjectUtils.NEW_LINE);
                        }
                    }
                }
            } else {
                builder.append(ARRAY_START);
                if (type.isPrimitive()) {
                    if (Character.TYPE.equals(type)) {
                        for (Object row : matrix) {
                            builder.append(Arrays.toString((char[]) row)).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Boolean.TYPE.equals(type)) {
                        for (Object row : matrix) {
                            builder.append(Arrays.toString((boolean[]) row)).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Byte.TYPE.equals(type)) {
                        for (Object row : matrix) {
                            builder.append(Arrays.toString((byte[]) row)).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Short.TYPE.equals(type)) {
                        for (Object row : matrix) {
                            builder.append(Arrays.toString((short[]) row)).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Integer.TYPE.equals(type)) {
                        for (Object row : matrix) {
                            builder.append(Arrays.toString((int[]) row)).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Long.TYPE.equals(type)) {
                        for (Object row : matrix) {
                            builder.append(Arrays.toString((long[]) row)).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Float.TYPE.equals(type)) {
                        for (Object row : matrix) {
                            builder.append(Arrays.toString((float[]) row)).append(ObjectUtils.NEW_LINE);
                        }
                    } else if (Double.TYPE.equals(type)) {
                        for (Object row : matrix) {
                            builder.append(Arrays.toString((double[]) row)).append(ObjectUtils.NEW_LINE);
                        }
                    }
                } else {
                    for (Object row : matrix) {
                        builder.append(Arrays.toString((Object[]) row)).append(ObjectUtils.NEW_LINE);
                    }
                }
            }
            int index = builder.lastIndexOf(ObjectUtils.NEW_LINE);
            if (index > -1) {
                builder.delete(index, index + 1);
            }
            if (matrix.length != 1) {
                builder.append(ARRAY_END);
            }
        }
        return builder.toString();
    }

    @Override
    public U revertAdapt(String data) throws DataAdaptationException {
        if (data == null) {
            return null;
        } else {
            // By default, we don't know how to revert adapt data
            throw new DataAdaptationException(this + CAN_NOT_REVERT_ADAPT_DATA);
        }
    }

}
