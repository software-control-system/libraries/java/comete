/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import fr.soleil.data.exception.DataAdaptationException;

/**
 * An {@link AbstractAdapter} that does the reverse adapting of another one
 * 
 * @param <T>
 * @param <U>
 * @author Rapha&euml;l GIRARDOT
 */
public class RevertAdapter<T, U> extends AbstractAdapter<T, U> {

    protected AbstractAdapter<U, T> adapter;

    public RevertAdapter(AbstractAdapter<U, T> adapter) {
        super(adapter == null ? null : adapter.getSecondType(), adapter == null ? null : adapter.getFirstType());
        this.adapter = adapter;
    }

    @Override
    public U adapt(T data) throws DataAdaptationException {
        U result = null;
        if (adapter != null) {
            result = adapter.revertAdapt(data);
        }
        return result;
    }

    @Override
    public T revertAdapt(U data) throws DataAdaptationException {
        T result = null;
        if (adapter != null) {
            result = adapter.adapt(data);
        }
        return result;
    }

}
