/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import fr.soleil.data.exception.DataAdaptationException;

/**
 * A {@link DataToStringAdapter} that adapts some {@link Boolean}s to {@link String} values
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BooleanToStringAdapter extends DataToStringAdapter<Boolean> {

    /**
     * Default constructor
     */
    public BooleanToStringAdapter() {
        super(Boolean.class);
    }

    @Override
    public Boolean revertAdapt(String data) throws DataAdaptationException {
        Boolean result;
        if (data == null) {
            result = null;
        } else {
            result = "true".equalsIgnoreCase(data.trim());
        }
        return result;
    }

}
