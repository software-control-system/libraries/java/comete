/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import fr.soleil.data.exception.DataAdaptationException;

/**
 * A class that is able to adapt one type of data into another one.
 * 
 * @param <U> The first type of data
 * @param <T> The second type of data
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AbstractAdapter<U, T> {

    protected final Class<?> firstType;
    protected final Class<?> secondType;

    /**
     * Constructs a new {@link AbstractAdapter}, setting its 2 types
     * 
     * @param firstType The first type of data
     * @param secondType The second type of data
     */
    public AbstractAdapter(Class<?> firstType, Class<?> secondType) {
        this.firstType = firstType;
        this.secondType = secondType;
    }

    /**
     * Returns the first type of data
     * 
     * @return A {@link Class}
     */
    public Class<?> getFirstType() {
        return firstType;
    }

    /**
     * Returns the second type of data
     * 
     * @return A {@link Class}
     */
    public Class<?> getSecondType() {
        return secondType;
    }

    /**
     * Adapts some data of the 1st type of into the 2nd one
     * 
     * @param data The data to adapt
     * @return The adapted data
     * @throws DataAdaptationException If a problem occurred while trying to adapt data
     */
    public abstract T adapt(U data) throws DataAdaptationException;

    /**
     * Adapts some data of the 2nd type of into the 1st one
     * 
     * @param data The data to adapt
     * @return The adapted data
     * @throws DataAdaptationException If a problem occurred while trying to adapt data
     */
    public abstract U revertAdapt(T data) throws DataAdaptationException;

}
