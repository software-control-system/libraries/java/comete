/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

import javax.activation.UnsupportedDataTypeException;

import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An {@link AbstractAdapter} that adapts some {@link AbstractMatrix} to {@link Object} arrays of a
 * particular deep content type (i.e. for example, if the {@link Object} array is a
 * <code>{@link java.lang.Number Number}[][]</code>, its deep content type is
 * {@link java.lang.Number Number})
 * 
 * @param <U> The type of data in the matrix
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AbstractMatrixToObjectMatrixAdapter<U> extends AbstractAdapter<AbstractMatrix<U>, Object[]> {

    protected final Class<U> matrixScalarClass;
    protected final Class<?> objectScalarClass;
    protected final AbstractAdapter<?, U> adapter;
    protected final Map<Class<?>, AbstractAdapter<?, U>> adapters = new HashMap<Class<?>, AbstractAdapter<?, U>>();

    /**
     * Constructs a new {@link AbstractMatrixToObjectMatrixAdapter}
     * 
     * @param scalarClass The {@link AbstractMatrix} scalar data type
     * @param objectScalarClass The expected {@link Object} array deep content type
     */
    public AbstractMatrixToObjectMatrixAdapter(Class<U> scalarClass, Class<?> objectScalarClass) {
        super(AbstractMatrix.class, Object[].class);
        this.matrixScalarClass = scalarClass;
        this.objectScalarClass = objectScalarClass;
        adapter = AdapterUtils.getAdapter(new GenericDescriptor(objectScalarClass),
                new GenericDescriptor(matrixScalarClass));
        adapters.put(objectScalarClass, adapter);
    }

    @Override
    public Object[] adapt(AbstractMatrix<U> data) throws DataAdaptationException {
        Object[] result = null;
        if (data != null) {
            if (objectScalarClass == null) {
                throw new DataAdaptationException("Can not convert matrix data to unknown Object array");
            } else if (Object.class.equals(objectScalarClass)
                    || ObjectUtils.sameObject(matrixScalarClass, objectScalarClass)) {
                result = data.getValue();
            } else if (adapter == null) {
                throw generateScalarAdapterFailed(objectScalarClass);
            } else {
                int width = data.getWidth(), height = data.getHeight();
                result = (Object[]) Array.newInstance(objectScalarClass, height, width);
                for (int i = 0; i < data.getHeight(); ++i) {
                    if (result[i] instanceof Object[]) {
                        // general case
                        Object[] array = (Object[]) result[i];
                        for (int j = 0; j < data.getWidth(); ++j) {
                            array[j] = adapter.revertAdapt(data.getValueAt(i, j));
                        }
                    } else if (result[i] instanceof char[]) {
                        // primitive char array case
                        char[] array = (char[]) result[i];
                        for (int j = 0; j < data.getWidth(); ++j) {
                            Character val = (Character) adapter.revertAdapt(data.getValueAt(i, j));
                            if (val != null) {
                                array[j] = val.charValue();
                            }
                        }
                    } else if (result[i] instanceof boolean[]) {
                        // primitive boolean array case
                        boolean[] array = (boolean[]) result[i];
                        for (int j = 0; j < data.getWidth(); ++j) {
                            Boolean val = (Boolean) adapter.revertAdapt(data.getValueAt(i, j));
                            if (val != null) {
                                array[j] = val.booleanValue();
                            }
                        }
                    } else if (result[i] instanceof byte[]) {
                        // primitive byte array case
                        byte[] array = (byte[]) result[i];
                        for (int j = 0; j < data.getWidth(); ++j) {
                            Byte val = (Byte) adapter.revertAdapt(data.getValueAt(i, j));
                            if (val != null) {
                                array[j] = val.byteValue();
                            }
                        }
                    } else if (result[i] instanceof short[]) {
                        // primitive short array case
                        short[] array = (short[]) result[i];
                        for (int j = 0; j < data.getWidth(); ++j) {
                            Short val = (Short) adapter.revertAdapt(data.getValueAt(i, j));
                            if (val != null) {
                                array[j] = val.shortValue();
                            }
                        }
                    } else if (result[i] instanceof int[]) {
                        // primitive int array case
                        int[] array = (int[]) result[i];
                        for (int j = 0; j < data.getWidth(); ++j) {
                            Integer val = (Integer) adapter.revertAdapt(data.getValueAt(i, j));
                            if (val != null) {
                                array[j] = val.intValue();
                            }
                        }
                    } else if (result[i] instanceof long[]) {
                        // primitive long array case
                        long[] array = (long[]) result[i];
                        for (int j = 0; j < data.getWidth(); ++j) {
                            Long val = (Long) adapter.revertAdapt(data.getValueAt(i, j));
                            if (val != null) {
                                array[j] = val.longValue();
                            }
                        }
                    } else if (result[i] instanceof float[]) {
                        // primitive float array case
                        float[] array = (float[]) result[i];
                        for (int j = 0; j < data.getWidth(); ++j) {
                            Float val = (Float) adapter.revertAdapt(data.getValueAt(i, j));
                            if (val != null) {
                                array[j] = val.floatValue();
                            }
                        }
                    } else if (result[i] instanceof double[]) {
                        // primitive double array case
                        double[] array = (double[]) result[i];
                        for (int j = 0; j < data.getWidth(); ++j) {
                            Double val = (Double) adapter.revertAdapt(data.getValueAt(i, j));
                            if (val != null) {
                                array[j] = val.doubleValue();
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public AbstractMatrix<U> revertAdapt(Object[] data) throws DataAdaptationException {
        AbstractMatrix<U> result;
        if (data == null) {
            result = null;
        } else if (matrixScalarClass == null) {
            throw new DataAdaptationException("Can not convert Object array to unknown matrix type");
        } else {
            result = generateMatrix();
            Class<?> dataScalarClass = ArrayUtils.recoverDataType(data);
            if (ObjectUtils.sameObject(matrixScalarClass, dataScalarClass)) {
                try {
                    result.setValue(data);
                } catch (UnsupportedDataTypeException e) {
                    throw new DataAdaptationException("Unexpected data conversion exception", e);
                }
            } else {
                fillMAtrixWithAdaptedData(result, data, dataScalarClass);
            }
        }
        return result;
    }

    /**
     * Fills an {@link AbstractMatrix} with some adapted data
     * 
     * @param matrix The {@link AbstractMatrix}
     * @param data The data to adapt
     * @param dataScalarClass The data to adapt scalar type (i.e. for example, if <code>data</code>
     *            is a <code>{@link java.lang.Number Number}[][]</code>,
     *            <code>dataScalarClass</code> is {@link java.lang.Number Number})
     * @throws DataAdaptationException If a problem occurred while trying to fill matrix with
     *             adapted data
     */
    @SuppressWarnings("unchecked")
    protected <T> void fillMAtrixWithAdaptedData(AbstractMatrix<U> matrix, Object[] data, Class<T> dataScalarClass)
            throws DataAdaptationException {
        AbstractAdapter<T, U> adapter = getAdapter(dataScalarClass);
        if (adapter == null) {
            throw generateScalarAdapterFailed(dataScalarClass);
        } else {
            if (data[0] != null) {
                int height = data.length, width = Array.getLength(data[0]);
                Object[] sourceData = (Object[]) Array.newInstance(adapter.getSecondType(), height, width);
                // XXX RG: there may be a more performing way to do this (Array.xx is slow)
                for (int i = 0; i < height; ++i) {
                    for (int j = 0; j < width; ++j) {
                        Array.set(sourceData[i], j, adapter.adapt((T) Array.get(data[i], j)));
                    }
                }
                try {
                    matrix.setValue(sourceData);
                } catch (UnsupportedDataTypeException e) {
                    throw new DataAdaptationException("Unexpected data conversion exception", e);
                }
            }
        }
    }

    /**
     * Recovers a previously built adapter from map, or builds a new one if necessary
     * 
     * @param dataScalarClass The data to adapt scalar type (i.e. for example, if <code>data</code>
     *            is a <code>{@link java.lang.Number Number}[][]</code>,
     *            <code>dataScalarClass</code> is {@link java.lang.Number Number})
     * @return An {@link AbstractAdapter}
     */
    @SuppressWarnings("unchecked")
    protected <T> AbstractAdapter<T, U> getAdapter(Class<T> dataScalarClass) {
        AbstractAdapter<T, U> adapter;
        if (dataScalarClass == null) {
            adapter = null;
        } else {
            synchronized (adapters) {
                adapter = (AbstractAdapter<T, U>) adapters.get(dataScalarClass);
                if (adapter == null) {
                    adapter = AdapterUtils.getAdapter(new GenericDescriptor(dataScalarClass),
                            new GenericDescriptor(matrixScalarClass));
                    if (adapter != null) {
                        adapters.put(dataScalarClass, adapter);
                    }
                }
            }
        }
        return adapter;
    }

    /**
     * Generates a {@link DataAdaptationException} for a failed scalar {@link AbstractAdapter}
     * creation
     * 
     * @param dataScalarClass The {@link Object} array scalar class
     * @return A {@link DataAdaptationException}
     */
    protected DataAdaptationException generateScalarAdapterFailed(Class<?> dataScalarClass) {
        return new DataAdaptationException("Failed to recover a valid scalar adapter for classes " + matrixScalarClass
                + " and " + dataScalarClass);
    }

    /**
     * Generates a new {@link AbstractMatrix} of expected scalar type
     * 
     * @return An {@link AbstractMatrix}
     * @throws DataAdaptationException If no such {@link AbstractMatrix} could be created
     */
    protected AbstractMatrix<U> generateMatrix() throws DataAdaptationException {
        try {
            return new AbstractMatrix<U>(matrixScalarClass) {
                @Override
                protected Class<U> generateDefaultDataType() {
                    return matrixScalarClass;
                }
            };
        } catch (Exception e) {
            throw new DataAdaptationException("Failed to create a valid AbstractMatrix for data adaptation", e);
        }
    }
}
