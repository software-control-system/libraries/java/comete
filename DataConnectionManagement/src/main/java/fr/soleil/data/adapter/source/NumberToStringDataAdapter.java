/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter.source;

import fr.soleil.data.adapter.NumberToStringAdapter;
import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IFormatableTarget;
import fr.soleil.lib.project.ObjectUtils;

public class NumberToStringDataAdapter<N extends Number> extends DataSourceAdapter<N, String>
        implements IFormatableTarget {

    public NumberToStringDataAdapter(IDataContainer<N> dataSource, NumberToStringAdapter<N> adapter) {
        super(dataSource, adapter);
    }

    @Override
    public String getFormat() {
        String format = null;
        if (adapter != null) {
            format = ((IFormatableTarget) adapter).getFormat();
        }
        return format;
    }

    @Override
    public void setFormat(String format) {
        boolean changed = false;
        if (adapter != null) {
            IFormatableTarget formatableTarget = ((IFormatableTarget) adapter);
            changed = (!ObjectUtils.sameObject(format, formatableTarget.getFormat()));
            formatableTarget.setFormat(format);
        }
        if (changed && (dataSource instanceof AbstractDataSource<?>)) {
            // The format did change, and this adapter is associated with an AbstractDataSource
            // --> We need to adapt last value again
            ((AbstractDataSource<?>) dataSource).notifyMediators();
        }
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // the adapter will never interact with mediators
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // the adapter will never interact with mediators
    }

}
