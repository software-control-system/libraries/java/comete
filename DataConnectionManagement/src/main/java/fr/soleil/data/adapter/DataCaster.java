/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import fr.soleil.data.exception.DataAdaptationException;

/**
 * An {@link AbstractAdapter} that a adapts one type of data into another compatible one
 * 
 * @param <U> The first type of data, which extends the second type of data
 * @param <T> The second type of data
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DataCaster<T, U extends T> extends AbstractAdapter<U, T> {

    public DataCaster(Class<?> firstType, Class<?> secondType) {
        super(firstType, secondType);
    }

    @Override
    public T adapt(U data) throws DataAdaptationException {
        return data;
    }

    @SuppressWarnings("unchecked")
    @Override
    public U revertAdapt(T data) throws DataAdaptationException {
        U adapted;
        try {
            adapted = (U) data;
        } catch (Exception e) {
            throw new DataAdaptationException(this + " failed to revert adapt data", e);
        }
        return adapted;
    }

}
