/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.target.IFormatableTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * Class used to recover some common {@link AbstractAdapter}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AdapterUtils {

    private static final Map<ClassContainer, Class<? extends AbstractAdapter<?, ?>>> EXTERNAL_ADAPTERS = new HashMap<ClassContainer, Class<? extends AbstractAdapter<?, ?>>>();

    /**
     * Registers a type of {@link AbstractAdapter}. Note that this method will only work with {@link AbstractAdapter}
     * {@link Class}es that do not need generic parameters, as java uses
     * unsigned classes (Example: <code>List&lt;String&gt;</code> class is <code>List</code>)
     * 
     * @param firstType The first type of data managed by the {@link AbstractAdapter}
     * @param secondType The second type of data managed by the {@link AbstractAdapter}
     * @param adapterClass The {@link AbstractAdapter} {@link Class}
     */
    public static final void registerExternalAdapter(GenericDescriptor firstType, GenericDescriptor secondType,
            Class<? extends AbstractAdapter<?, ?>> adapterClass) {
        if ((firstType != null) && (secondType != null) && (adapterClass != null)) {
            synchronized (EXTERNAL_ADAPTERS) {
                EXTERNAL_ADAPTERS.put(new ClassContainer(firstType, secondType), adapterClass);
            }
        }
    }

    /**
     * Recovers a previously registered {@link AbstractAdapter} {@link Class}
     * 
     * @param firstType The first type of data managed by the {@link AbstractAdapter}
     * @param secondType The second type of data managed by the {@link AbstractAdapter}
     * @return An {@link AbstractAdapter} {@link Class}
     */
    protected static Class<? extends AbstractAdapter<?, ?>> getAsynchronAdapterClass(GenericDescriptor firstType,
            GenericDescriptor secondType) {
        return EXTERNAL_ADAPTERS.get(new ClassContainer(firstType, secondType));
    }

    /**
     * Creates a new {@link AbstractAdapter} from a given {@link AbstractAdapter} {@link Class}
     * 
     * @param firstClass The first type of data managed by the {@link AbstractAdapter}
     * @param secondClass The second type of data managed by the {@link AbstractAdapter}
     * @param adapterClass The {@link AbstractAdapter} {@link Class}
     * @return An {@link AbstractAdapter}
     */
    protected static final AbstractAdapter<?, ?> createAdapterFromClass(Class<?> firstClass, Class<?> secondClass,
            Class<? extends AbstractAdapter<?, ?>> adapterClass) {
        AbstractAdapter<?, ?> adapter = null;
        if ((firstClass != null) && (secondClass != null) && (adapterClass != null)) {
            try {
                Constructor<? extends AbstractAdapter<?, ?>> constructor = adapterClass
                        .getDeclaredConstructor(Class.class, Class.class);
                adapter = constructor.newInstance(firstClass, secondClass);
            } catch (Exception e) {
                try {
                    adapter = adapterClass.newInstance();
                } catch (Exception e2) {
                    adapter = null;
                }
            }
        }
        return adapter;
    }

    /**
     * Recovers the best {@link AbstractAdapter} for 2 classes to adapt
     * 
     * @param firstType The first type of data to adapt
     * @param secondType The second type of data to adapt. This is an ellipse, because you may want
     *            to give some precisions for your classes (mainly used because of generics)
     * @return An {@link AbstractAdapter}. <code>null</code> If no such {@link AbstractAdapter} could be created
     */
    @SuppressWarnings("unchecked")
    public static <U, T> AbstractAdapter<U, T> getAdapter(GenericDescriptor firstType, GenericDescriptor secondType) {
        AbstractAdapter<U, T> adapter = null;
        if ((firstType != null) && (secondType != null) && (firstType.getConcernedClass() != null)
                && (secondType.getConcernedClass() != null)) {
            if (ObjectUtils.sameObject(firstType, secondType)) {
                // no conversion case
                adapter = (AbstractAdapter<U, T>) new FakeAdapter<U>(firstType.getConcernedClass());
            } else {
                synchronized (EXTERNAL_ADAPTERS) {
                    // check external adapters first
                    Class<? extends AbstractAdapter<?, ?>> adapterClass = getAsynchronAdapterClass(firstType,
                            secondType);
                    if (adapterClass == null) {
                        adapterClass = getAsynchronAdapterClass(secondType, firstType);
                        if (adapterClass != null) {
                            try {
                                adapter = (AbstractAdapter<U, T>) createRevertAdapter(createAdapterFromClass(
                                        secondType.getConcernedClass(), firstType.getConcernedClass(), adapterClass));
                            } catch (Exception e) {
                                adapter = null;
                            }
                        }
                    } else {
                        try {
                            adapter = (AbstractAdapter<U, T>) createAdapterFromClass(firstType.getConcernedClass(),
                                    secondType.getConcernedClass(), adapterClass);
                        } catch (Exception e) {
                            adapter = null;
                        }
                    }
                } // end synchronized (EXTERNAL_ADAPTERS)
                if (adapter == null) {
                    // If no external adapter found, try classic adapters
                    if (firstType.getConcernedClass().isAssignableFrom(secondType.getConcernedClass())) {
                        // data casting case
                        try {
                            Class<U> firstClass = (Class<U>) firstType.getConcernedClass();
                            adapter = (AbstractAdapter<U, T>) createDataCaster(firstClass,
                                    secondType.getConcernedClass().asSubclass(firstClass));
                        } catch (Exception e) {
                            adapter = null;
                        }
                    } else if (secondType.getConcernedClass().isAssignableFrom(firstType.getConcernedClass())) {
                        // data casting case
                        try {
                            Class<T> secondClass = (Class<T>) secondType.getConcernedClass();
                            adapter = (AbstractAdapter<U, T>) createDataCaster(secondClass,
                                    firstType.getConcernedClass().asSubclass(secondClass));
                        } catch (Exception e) {
                            adapter = null;
                        }
                    } else if (isSimpleArrayClass(firstType.getConcernedClass())
                            && isSimpleArrayClass(secondType.getConcernedClass())) {
                        // single dimension array case
                        if (ObjectUtils.isNumberClass(firstType.getConcernedClass().getComponentType())
                                && ((boolean[].class.equals(secondType.getConcernedClass()))
                                        || (Boolean[].class.equals(secondType.getConcernedClass())))) {
                            adapter = createNumberArrayToBooleanArrayAdapter(firstType.getConcernedClass(),
                                    secondType.getConcernedClass());
                        } else if (ObjectUtils.isNumberClass(secondType.getConcernedClass().getComponentType())
                                && ((boolean[].class.equals(firstType.getConcernedClass()))
                                        || (Boolean[].class.equals(firstType.getConcernedClass())))) {
                            adapter = (AbstractAdapter<U, T>) createRevertAdapter(
                                    createNumberArrayToBooleanArrayAdapter(secondType.getConcernedClass(),
                                            firstType.getConcernedClass()));
                        } else if (isValidArrayClass(firstType.getConcernedClass())
                                && isValidArrayClass(secondType.getConcernedClass())) {
                            // array conversion case
                            ArrayAdapter<?, ?> arrayAdapter = createArrayAdapter(
                                    getAsArrayClass(firstType.getConcernedClass().getComponentType(),
                                            firstType.getConcernedClass()),
                                    getAsArrayClass(secondType.getConcernedClass().getComponentType(),
                                            secondType.getConcernedClass()));
                            try {
                                arrayAdapter.checkAdapter();
                                adapter = (AbstractAdapter<U, T>) arrayAdapter;
                            } catch (DataAdaptationException e) {
                                adapter = null;
                            }
                        }
                    } else if (isValidArrayClass(firstType.getConcernedClass())
                            && isValidArrayClass(secondType.getConcernedClass())) {
                        // array conversion case
                        ArrayAdapter<?, ?> arrayAdapter = createArrayAdapter(
                                getAsArrayClass(firstType.getConcernedClass().getComponentType(),
                                        firstType.getConcernedClass()),
                                getAsArrayClass(secondType.getConcernedClass().getComponentType(),
                                        secondType.getConcernedClass()));
                        try {
                            arrayAdapter.checkAdapter();
                            adapter = (AbstractAdapter<U, T>) arrayAdapter;
                        } catch (DataAdaptationException e) {
                            adapter = null;
                        }
                    } else if (String.class.equals(secondType.getConcernedClass())) {
                        // object to string case
                        adapter = (AbstractAdapter<U, T>) createDataToStringAdapterIfPossible(firstType);
                    } else if (Boolean.class.equals(secondType.getConcernedClass())
                            || Boolean.TYPE.equals(secondType.getConcernedClass())) {
                        if (String.class.equals(firstType.getConcernedClass())) {
                            // string to boolean case
                            adapter = (AbstractAdapter<U, T>) createRevertAdapter(new BooleanToStringAdapter());
                        } else {
                            // number to boolean case
                            adapter = (AbstractAdapter<U, T>) createNumberToBooleanAdapterIfPossible(
                                    firstType.getConcernedClass());
                        }
                    } else if (Object[].class.isAssignableFrom(secondType.getConcernedClass())) {
                        // AbstractMatrix to matrix case
                        if (AbstractMatrix.class.equals(firstType.getConcernedClass())
                                && (firstType.getParameters() != null) && (firstType.getParameters().length == 1)) {
                            GenericDescriptor matrixType = firstType.getParameters()[0];
                            if ((matrixType != null) && (matrixType.getConcernedClass() != null)) {
                                try {
                                    adapter = (AbstractAdapter<U, T>) createMatrixAdapter(
                                            matrixType.getConcernedClass(),
                                            ArrayUtils.recoverDeepComponentType(secondType.getConcernedClass()));
                                } catch (DataAdaptationException e) {
                                    adapter = null;
                                }
                            }
                        }
                    } else if (secondType.getConcernedClass().isArray()
                            && ObjectUtils.isNumberClass(secondType.getConcernedClass().getComponentType())) {
                        // AbstractNumberMatrix to number array case
                        if (AbstractNumberMatrix.class.equals(firstType.getConcernedClass())
                                && (firstType.getParameters() != null) && (firstType.getParameters().length == 1)) {
                            GenericDescriptor matrixType = firstType.getParameters()[0];
                            if ((matrixType != null) && (matrixType.getConcernedClass() != null)) {
                                try {
                                    adapter = (AbstractAdapter<U, T>) createNumberMatrixAdapter(
                                            matrixType.getConcernedClass(),
                                            firstType.getConcernedClass().getComponentType());
                                } catch (DataAdaptationException e) {
                                    adapter = null;
                                }
                            }
                        }
                    } else if (String.class.equals(firstType.getConcernedClass())) {
                        // string to object case
                        adapter = (AbstractAdapter<U, T>) createRevertAdapter(
                                createDataToStringAdapterIfPossible(secondType));
                    } else if (Boolean.class.equals(firstType.getConcernedClass())
                            || Boolean.TYPE.equals(firstType.getConcernedClass())) {
                        // AbstractMatrix to array case
                        adapter = (AbstractAdapter<U, T>) createRevertAdapter(
                                createNumberToBooleanAdapterIfPossible(secondType.getConcernedClass()));
                    } else if (Object[].class.isAssignableFrom(firstType.getConcernedClass())) {
                        // matrix to AbstractMatrix case
                        if (AbstractMatrix.class.equals(secondType.getConcernedClass())
                                && (secondType.getParameters() != null) && (secondType.getParameters().length == 1)) {
                            GenericDescriptor matrixType = secondType.getParameters()[0];
                            if ((matrixType != null) && (matrixType.getConcernedClass() != null)) {
                                try {
                                    adapter = (AbstractAdapter<U, T>) createRevertAdapter(createMatrixAdapter(
                                            matrixType.getConcernedClass(),
                                            ArrayUtils.recoverDeepComponentType(firstType.getConcernedClass())));
                                } catch (DataAdaptationException e) {
                                    adapter = null;
                                }
                            }
                        }
                    } else if (firstType.getConcernedClass().isArray()
                            && ObjectUtils.isNumberClass(firstType.getConcernedClass().getComponentType())) {
                        // number array to AbstractNumberMatrix case
                        if (AbstractNumberMatrix.class.equals(secondType.getConcernedClass())) {
                            GenericDescriptor matrixType = secondType.getParameters()[0];
                            if ((matrixType != null) && (matrixType.getConcernedClass() != null)) {
                                try {
                                    adapter = (AbstractAdapter<U, T>) createRevertAdapter(
                                            createNumberMatrixAdapter(matrixType.getConcernedClass(),
                                                    firstType.getConcernedClass().getComponentType()));
                                } catch (DataAdaptationException e) {
                                    adapter = null;
                                }
                            }
                        }
                    }
                }
            }
        }
        return adapter;
    }

    /**
     * Checks whether a given {@link Class} represents an {@link Object} array
     * 
     * @param arrayClass The {@link Class} to check
     * @return A <code>boolean</code> value
     */
    protected static boolean isValidArrayClass(Class<?> arrayClass) {
        return ((arrayClass != null) && arrayClass.isArray() && (!arrayClass.getComponentType().isPrimitive()));
    }

    /**
     * Checks whether a given {@link Class} represents a single dimension array
     * 
     * @param arrayClass The {@link Class} to check
     * @return A <code>boolean</code> value
     */
    protected static boolean isSimpleArrayClass(Class<?> arrayClass) {
        return ((arrayClass != null) && arrayClass.isArray() && (!arrayClass.getComponentType().isArray()));
    }

    /**
     * Creates, if possible, a {@link DataToStringAdapter} for a given first data type
     * 
     * @param firstType The first data type
     * @return A {@link DataToStringAdapter}, or <code>null</code> if no such adapter could be
     *         created
     */
    @SuppressWarnings("unchecked")
    protected static <U> DataToStringAdapter<U> createDataToStringAdapterIfPossible(
            GenericDescriptor firstTypeDescriptor) {
        Class<?> firstType = firstTypeDescriptor.getConcernedClass();
        GenericDescriptor[] parameters = firstTypeDescriptor.getParameters();

        DataToStringAdapter<U> adapter = null;
        if (Boolean.class.equals(firstType) || Boolean.TYPE.equals(firstType)) {
            adapter = (DataToStringAdapter<U>) new BooleanToStringAdapter();
        } else if (firstType.isPrimitive()) {
            if (Byte.TYPE.equals(firstType)) {
                adapter = (DataToStringAdapter<U>) createNumberToStringAdapter(Byte.TYPE);
            } else if (Short.TYPE.equals(firstType)) {
                adapter = (DataToStringAdapter<U>) createNumberToStringAdapter(Short.TYPE);
            } else if (Integer.TYPE.equals(firstType)) {
                adapter = (DataToStringAdapter<U>) createNumberToStringAdapter(Integer.TYPE);
            } else if (Long.TYPE.equals(firstType)) {
                adapter = (DataToStringAdapter<U>) createNumberToStringAdapter(Long.TYPE);
            } else if (Float.TYPE.equals(firstType)) {
                adapter = (DataToStringAdapter<U>) createNumberToStringAdapter(Float.TYPE);
            } else if (Double.TYPE.equals(firstType)) {
                adapter = (DataToStringAdapter<U>) createNumberToStringAdapter(Double.TYPE);
            }
        } else if (Number.class.isAssignableFrom(firstType)) {
            adapter = (DataToStringAdapter<U>) createNumberToStringAdapter(firstType.asSubclass(Number.class));
        } else if (AbstractMatrix.class.isAssignableFrom(firstType)) {
            if (parameters != null && parameters.length > 0) {
                Class<?> subClass = parameters[0].getConcernedClass();
                adapter = (DataToStringAdapter<U>) createAbstractMatrixToStringAdapter(subClass);
            }
        } else {
            adapter = new DataToStringAdapter<U>(firstType);
        }
        return adapter;
    }

    /**
     * Creates, if possible, a {@link NumberToBooleanAdapter} for a given first data type
     * 
     * @param firstType The first data type
     * @return A {@link NumberToBooleanAdapter}, or <code>null</code> if no such adapter could be
     *         created
     */
    @SuppressWarnings("unchecked")
    protected static <N extends Number> NumberToBooleanAdapter<N> createNumberToBooleanAdapterIfPossible(
            Class<?> firstType) {
        NumberToBooleanAdapter<N> adapter = null;
        if (Number.class.isAssignableFrom(firstType)) {
            adapter = (NumberToBooleanAdapter<N>) createNumberToBooleanAdapter(firstType.asSubclass(Number.class));
        } else if (firstType.isPrimitive()) {
            if (Byte.TYPE.equals(firstType)) {
                adapter = (NumberToBooleanAdapter<N>) createNumberToBooleanAdapter(Byte.TYPE);
            } else if (Short.TYPE.equals(firstType)) {
                adapter = (NumberToBooleanAdapter<N>) createNumberToBooleanAdapter(Short.TYPE);
            } else if (Integer.TYPE.equals(firstType)) {
                adapter = (NumberToBooleanAdapter<N>) createNumberToBooleanAdapter(Integer.TYPE);
            } else if (Long.TYPE.equals(firstType)) {
                adapter = (NumberToBooleanAdapter<N>) createNumberToBooleanAdapter(Long.TYPE);
            } else if (Float.TYPE.equals(firstType)) {
                adapter = (NumberToBooleanAdapter<N>) createNumberToBooleanAdapter(Float.TYPE);
            } else if (Double.TYPE.equals(firstType)) {
                adapter = (NumberToBooleanAdapter<N>) createNumberToBooleanAdapter(Double.TYPE);
            }
        }
        return adapter;
    }

    /**
     * Creates a new {@link RevertAdapter} for a given {@link AbstractAdapter}
     * 
     * @param adapter The {@link AbstractAdapter}
     * @return A {@link RevertAdapter}
     */
    protected static <A, B> RevertAdapter<A, B> createRevertAdapter(AbstractAdapter<B, A> adapter) {
        RevertAdapter<A, B> result;
        if (adapter == null) {
            result = null;
        } else if (adapter instanceof IFormatableTarget) {
            result = new RevertFormatableAdaper<A, B>(adapter);
        } else {
            result = new RevertAdapter<A, B>(adapter);
        }
        return result;
    }

    /**
     * Creates a new {@link NumberToStringAdapter} of expected {@link Number} class
     * 
     * @param numberClass The {@link Number} class
     * @return A {@link NumberToStringAdapter}
     */
    protected static <N> AbstractMatrixToStringAdapter<N> createAbstractMatrixToStringAdapter(Class<?> subClass) {
        return new AbstractMatrixToStringAdapter<N>(subClass);

    }

    /**
     * Creates a new {@link NumberToStringAdapter} of expected {@link Number} class
     * 
     * @param numberClass The {@link Number} class
     * @return A {@link NumberToStringAdapter}
     */
    protected static <N extends Number> NumberToStringAdapter<N> createNumberToStringAdapter(Class<N> numberClass) {
        return new NumberToStringAdapter<N>(numberClass);
    }

    /**
     * Creates a new {@link NumberToBooleanAdapter} of expected {@link Number} class
     * 
     * @param numberClass The {@link Number} class
     * @return A {@link NumberToBooleanAdapter}
     */
    protected static <N extends Number> NumberToBooleanAdapter<N> createNumberToBooleanAdapter(Class<N> numberClass) {
        return new NumberToBooleanAdapter<N>(numberClass);
    }

    /**
     * Creates a new {@link AbstractMatrixToObjectMatrixAdapter}
     * 
     * @param firstClass The {@link AbstractMatrix} data type
     * @param secondClass The {@link Object} array deep content type
     * @return An {@link AbstractMatrixToObjectMatrixAdapter}
     * @throws DataAdaptationException If a problem occurred while creating such an adapter
     */
    @SuppressWarnings("unchecked")
    protected static <U> AbstractMatrixToObjectMatrixAdapter<U> createMatrixAdapter(Class<?> firstClass,
            Class<?> secondClass) throws DataAdaptationException {
        try {
            return new AbstractMatrixToObjectMatrixAdapter<U>((Class<U>) firstClass, secondClass);
        } catch (Exception e) {
            throw new DataAdaptationException("Failed to create a valid AbstractMatrix for data adaptation", e);
        }
    }

    /**
     * Creates a new {@link AbstractNumberMatrixToNumberArrayConvertor}
     * 
     * @param firstClass The {@link AbstractNumberMatrix} data type
     * @param secondClass The {@link Object} array deep content type
     * @return An {@link AbstractNumberMatrixToNumberArrayConvertor}
     * @throws DataAdaptationException If a problem occurred while creating such an adapter
     */
    @SuppressWarnings("unchecked")
    protected static <N extends Number> AbstractNumberMatrixToNumberArrayConvertor<N> createNumberMatrixAdapter(
            Class<?> firstClass, Class<?> secondClass) throws DataAdaptationException {
        try {
            Class<? extends Number> numberClass;
            if (Number.class.isAssignableFrom(secondClass)) {
                numberClass = secondClass.asSubclass(Number.class);
            } else if (secondClass.isPrimitive()) {
                if (Byte.TYPE.equals(secondClass)) {
                    numberClass = Byte.TYPE;
                } else if (Short.TYPE.equals(secondClass)) {
                    numberClass = Short.TYPE;
                } else if (Integer.TYPE.equals(secondClass)) {
                    numberClass = Integer.TYPE;
                } else if (Long.TYPE.equals(secondClass)) {
                    numberClass = Long.TYPE;
                } else if (Float.TYPE.equals(secondClass)) {
                    numberClass = Float.TYPE;
                } else if (Double.TYPE.equals(secondClass)) {
                    numberClass = Double.TYPE;
                } else {
                    throw generateNumberClassFailed(secondClass);
                }
            } else {
                throw generateNumberClassFailed(secondClass);
            }
            return new AbstractNumberMatrixToNumberArrayConvertor<N>((Class<N>) firstClass, numberClass);
        } catch (Exception e) {
            throw new DataAdaptationException("Failed to create a valid AbstractNumberMatrix for data adaptation", e);
        }
    }

    /**
     * Creates a new {@link DataCaster} for some given data types
     * 
     * @param secondType The second type of data
     * @param firstType The first type of data, that should extend the second one
     * @return A {@link DataCaster}
     */
    protected static <T, U extends T> DataCaster<T, U> createDataCaster(Class<T> secondType, Class<U> firstType) {
        return new DataCaster<T, U>(firstType, secondType);
    }

    /**
     * Creates a new {@link ArrayAdapter}
     * 
     * @param firstType The first type of array to adapt
     * @param secondType The second type of array to adapt
     * @return An {@link ArrayAdapter}
     */
    protected static <U, T> ArrayAdapter<U, T> createArrayAdapter(Class<U[]> firstType, Class<T[]> secondType) {
        return new ArrayAdapter<U, T>(firstType, secondType);
    }

    /**
     * Creates a new {@link NumberArrayToBooleanArrayAdapter}
     * 
     * @param numberArrayClass The type of number array to adapt
     * @param booleanArrayClass The type of boolean array to adapt
     * @return A {@link NumberArrayToBooleanArrayAdapter}
     */
    protected static <N, NA, BA> NumberArrayToBooleanArrayAdapter<Number, NA, BA> createNumberArrayToBooleanArrayAdapter(
            Class<?> numberArrayClass, Class<?> booleanArrayClass) {
        return new NumberArrayToBooleanArrayAdapter<Number, NA, BA>(
                getAsNumberClass(numberArrayClass.getComponentType()),
                getAsBooleanClass(booleanArrayClass.getComponentType()));
    }

    /**
     * Casts, if possible, a given {@link Class} as the {@link Class} class of another {@link Class}
     * 
     * @param componentClass The unary class
     * @param arrayClass The {@link Class} to cast to array {@link Class}
     * @return A {@link Class}
     */
    @SuppressWarnings("unchecked")
    protected static <C> Class<C[]> getAsArrayClass(Class<C> componentClass, Class<?> arrayClass) {
        Class<C[]> result = null;
        if ((componentClass != null) && (arrayClass != null) && arrayClass.isArray()
                && arrayClass.getComponentType().equals(componentClass)) {
            result = (Class<C[]>) arrayClass;
        }
        return result;
    }

    /**
     * Casts, if possible, a given {@link Class} as a {@link Number} {@link Class}
     * 
     * @param numberClass The {@link Class} to cast to {@link Number} {@link Class}
     * @return A {@link Class}
     */
    @SuppressWarnings("unchecked")
    protected static <N extends Number> Class<N> getAsNumberClass(Class<?> numberClass) {
        return (Class<N>) numberClass;
    }

    /**
     * Casts, if possible, a given {@link Class} as a {@link Boolean} {@link Class}
     * 
     * @param numberClass The {@link Class} to cast to {@link Boolean} {@link Class}
     * @return A {@link Class}
     */
    @SuppressWarnings("unchecked")
    protected static Class<Boolean> getAsBooleanClass(Class<?> booleanClass) {
        return (Class<Boolean>) booleanClass;
    }

    /**
     * Generates a {@link DataAdaptationException} for a failed scalar {@link AbstractAdapter} creation
     * 
     * @param dataScalarClass The {@link Object} array scalar class
     * @return A {@link DataAdaptationException}
     */
    protected static DataAdaptationException generateNumberClassFailed(Class<?> dataScalarClass) {
        return new DataAdaptationException(dataScalarClass + " is not a valid number class");
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link RevertAdapter} that also is an {@link IFormatableTarget}
     * 
     * @param <A> The {@link RevertAdapter} first data type
     * @param <B> The {@link RevertAdapter} second data type
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected static class RevertFormatableAdaper<A, B> extends RevertAdapter<A, B> implements IFormatableTarget {

        public RevertFormatableAdaper(AbstractAdapter<B, A> adapter) {
            super(adapter);
        }

        @Override
        public String getFormat() {
            String format = null;
            if (adapter instanceof IFormatableTarget) {
                format = ((IFormatableTarget) adapter).getFormat();
            }
            return format;
        }

        @Override
        public void setFormat(String format) {
            if (adapter instanceof IFormatableTarget) {
                ((IFormatableTarget) adapter).setFormat(format);
            }
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            // Nothing to do
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // Nothing to do
        }

    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static final class ClassContainer {
        private final GenericDescriptor firstClass;
        private final GenericDescriptor secondClass;

        public ClassContainer(GenericDescriptor firstClass, GenericDescriptor secondClass) {
            this.firstClass = firstClass;
            this.secondClass = secondClass;
        }

        public GenericDescriptor getFirstType() {
            return firstClass;
        }

        public GenericDescriptor getSecondType() {
            return secondClass;
        }

        @Override
        public boolean equals(Object obj) {
            boolean equals = false;
            if (obj instanceof ClassContainer) {
                ClassContainer container = (ClassContainer) obj;
                equals = ObjectUtils.sameObject(getFirstType(), container.getFirstType())
                        && ObjectUtils.sameObject(getSecondType(), container.getSecondType());
            }
            return equals;
        }

        @Override
        public int hashCode() {
            int code = 0xC1A55;
            int mult = 0xC027;
            code = code * mult + ObjectUtils.getHashCode(getFirstType());
            code = code * mult + ObjectUtils.getHashCode(getSecondType());
            return code;
        }
    }

}
