/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import java.math.BigInteger;

import javax.activation.UnsupportedDataTypeException;

import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.BigIntegerMatrix;
import fr.soleil.data.container.matrix.BooleanMatrix;
import fr.soleil.data.container.matrix.ByteMatrix;
import fr.soleil.data.container.matrix.DoubleMatrix;
import fr.soleil.data.container.matrix.FloatMatrix;
import fr.soleil.data.container.matrix.IntMatrix;
import fr.soleil.data.container.matrix.LongMatrix;
import fr.soleil.data.container.matrix.ShortMatrix;
import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.IFormatableTarget;

/**
 * A {@link DataToStringAdapter} that adapts some {@link Number}s to {@link String} values
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <N> The type of {@link Number} managed by this {@link AbstractMatrixToStringAdapter}
 */
public class AbstractMatrixToStringAdapter<N> extends DataToStringAdapter<AbstractMatrix<N>>
        implements IFormatableTarget {

    private static final String COMMA = ",";

    /**
     * Constructs a new {@link AbstractMatrixToStringAdapter}, settings its managed {@link Number}
     * type
     * 
     * @param firstType The type of {@link Number} managed by this
     *            {@link AbstractMatrixToStringAdapter}
     */
    public AbstractMatrixToStringAdapter(Class<?> firstType) {
        super(firstType);

    }

    @SuppressWarnings("unchecked")
    @Override
    public AbstractMatrix<N> revertAdapt(String data) throws DataAdaptationException {
        AbstractMatrix<N> result = null;
        if (data == null) {
            result = null;
        } else if (firstType == null) {
            throw new DataAdaptationException("Can't adapt String to unknown number type");
        } else {
            if (firstType.isAssignableFrom(Byte.class)) {
                result = (AbstractMatrix<N>) new ByteMatrix();
            } else if (firstType.isAssignableFrom(Double.class)) {
                result = (AbstractMatrix<N>) new DoubleMatrix();
            } else if (firstType.isAssignableFrom(Float.class)) {
                result = (AbstractMatrix<N>) new FloatMatrix();
            } else if (firstType.isAssignableFrom(Integer.class)) {
                result = (AbstractMatrix<N>) new IntMatrix();
            } else if (firstType.isAssignableFrom(Long.class)) {
                result = (AbstractMatrix<N>) new LongMatrix();
            } else if (firstType.isAssignableFrom(Short.class)) {
                result = (AbstractMatrix<N>) new ShortMatrix();
            } else if (firstType.isAssignableFrom(Boolean.class)) {
                result = (AbstractMatrix<N>) new BooleanMatrix();
            } else if (firstType.isAssignableFrom(String.class)) {
                result = (AbstractMatrix<N>) new StringMatrix();
            } else if (firstType.isAssignableFrom(BigInteger.class)) {
                result = (AbstractMatrix<N>) new BigIntegerMatrix();
            }

            String[] values = data.split(COMMA);

            try {
                result.setValue(result.initValue(1, values.length));
            } catch (UnsupportedDataTypeException e) {
                throw new DataAdaptationException("Unexpected data conversion exception", e);
            }

            int index = 0;
            for (String valueToSet : values) {
                result.setValueFromStringAt(valueToSet, 0, index);
                index++;
            }

        }
        return result;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // the adapter will never interact with mediators
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // the adapter will never interact with mediators
    }

    @Override
    public String getFormat() {
        return null;
    }

    @Override
    public void setFormat(String format) {
    }

}
