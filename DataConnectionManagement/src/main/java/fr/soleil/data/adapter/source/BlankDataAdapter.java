/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter.source;

import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;

/**
 * This adapter is only used to transmit data whose type is identical between
 * {@link AbstractDataSource} and {@link ITarget}. It is used when no operation is necessary for
 * adaptation.
 * 
 * @author huriez
 * 
 * @param <U> the type of the {@link AbstractDataSource}
 * @param <T> the type of the {@link ITarget}
 */
public class BlankDataAdapter<U, T> extends DataSourceAdapter<U, T> {

    /**
     * Constructor with the {@link AbstractDataSource}
     * 
     * @param dataSource the data source.
     */
    public BlankDataAdapter(AbstractDataSource<U> dataSource) {
        super(dataSource, null);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected T adaptSourceData(U data) {
        return (T) data;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected U adaptContainerData(T data) {
        return (U) data;
    }

}
