/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import fr.soleil.data.exception.DataAdaptationException;

/**
 * A Fake adapter: adapts some data into itself ;)
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <U> The data type
 */
public class FakeAdapter<U> extends AbstractAdapter<U, U> {

    public FakeAdapter(Class<?> dataType) {
        super(dataType, dataType);
    }

    @Override
    public U adapt(U data) throws DataAdaptationException {
        return data;
    }

    @Override
    public U revertAdapt(U data) throws DataAdaptationException {
        return data;
    }

}
