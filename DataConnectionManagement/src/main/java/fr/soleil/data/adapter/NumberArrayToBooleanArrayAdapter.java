/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.adapter;

import java.lang.reflect.Array;
import java.math.BigInteger;

import fr.soleil.data.exception.DataAdaptationException;

public class NumberArrayToBooleanArrayAdapter<N extends Number, NA, BA> extends AbstractAdapter<NA, BA> {

    public NumberArrayToBooleanArrayAdapter(Class<N> firstType, Class<Boolean> secondType) {
        super(generateArrayClass(firstType), generateArrayClass(secondType));
    }

    @SuppressWarnings("unchecked")
    protected static <A> Class<A> generateArrayClass(Class<?> dataClass) {
        Class<?> result;
        if (dataClass == null) {
            result = null;
        } else {
            result = Array.newInstance(dataClass, 0).getClass();
        }
        return (Class<A>) result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public BA adapt(NA data) throws DataAdaptationException {
        BA result;
        if (data == null) {
            result = null;
        }
        if (secondType == null) {
            throw new DataAdaptationException("Can't adapt number array to unknown boolean array type");
        } else if (data instanceof byte[]) {
            byte[] dataArray = (byte[]) data;
            if (boolean[].class.equals(secondType)) {
                boolean[] array = new boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = (dataArray[i] != 0);
                }
                result = (BA) array;
            } else {
                Boolean[] array = new Boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Boolean.valueOf(dataArray[i] != 0);
                }
                result = (BA) array;
            }
        } else if (data instanceof short[]) {
            short[] dataArray = (short[]) data;
            if (boolean[].class.equals(secondType)) {
                boolean[] array = new boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = (dataArray[i] != 0);
                }
                result = (BA) array;
            } else {
                Boolean[] array = new Boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Boolean.valueOf(dataArray[i] != 0);
                }
                result = (BA) array;
            }
        } else if (data instanceof int[]) {
            int[] dataArray = (int[]) data;
            if (boolean[].class.equals(secondType)) {
                boolean[] array = new boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = (dataArray[i] != 0);
                }
                result = (BA) array;
            } else {
                Boolean[] array = new Boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Boolean.valueOf(dataArray[i] != 0);
                }
                result = (BA) array;
            }
        } else if (data instanceof byte[]) {
            long[] dataArray = (long[]) data;
            if (boolean[].class.equals(secondType)) {
                boolean[] array = new boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = (dataArray[i] != 0);
                }
                result = (BA) array;
            } else {
                Boolean[] array = new Boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Boolean.valueOf(dataArray[i] != 0);
                }
                result = (BA) array;
            }
        } else if (data instanceof float[]) {
            float[] dataArray = (float[]) data;
            if (boolean[].class.equals(secondType)) {
                boolean[] array = new boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = (dataArray[i] != 0);
                }
                result = (BA) array;
            } else {
                Boolean[] array = new Boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Boolean.valueOf(dataArray[i] != 0);
                }
                result = (BA) array;
            }
        } else if (data instanceof double[]) {
            double[] dataArray = (double[]) data;
            if (boolean[].class.equals(secondType)) {
                boolean[] array = new boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = (dataArray[i] != 0);
                }
                result = (BA) array;
            } else {
                Boolean[] array = new Boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Boolean.valueOf(dataArray[i] != 0);
                }
                result = (BA) array;
            }
        } else {
            Number[] dataArray = (Number[]) data;
            if (boolean[].class.equals(secondType)) {
                boolean[] array = new boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = ((dataArray[i] != null) && (dataArray[i].doubleValue() != 0));
                }
                result = (BA) array;
            } else {
                Boolean[] array = new Boolean[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    if (dataArray[i] == null) {
                        array[i] = null;
                    } else {
                        array[i] = Boolean.valueOf(dataArray[i].doubleValue() != 0);
                    }
                }
                result = (BA) array;
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public NA revertAdapt(BA data) throws DataAdaptationException {
        NA result;
        if (data == null) {
            result = null;
        } else if (firstType == null) {
            throw new DataAdaptationException("Can't adapt boolean array to unknown number array type");
        } else if (byte[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                byte[] array = new byte[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = dataArray[i] ? (byte) 1 : 0;
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                byte[] array = new byte[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = ((dataArray[i] != null) && dataArray[i].booleanValue()) ? (byte) 1 : 0;
                }
                result = (NA) array;
            }
        } else if (Byte[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                Byte[] array = new Byte[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Byte.valueOf(dataArray[i] ? (byte) 1 : 0);
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                Byte[] array = new Byte[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    if (dataArray[i] == null) {
                        array[i] = null;
                    } else {
                        array[i] = Byte.valueOf(dataArray[i].booleanValue() ? (byte) 1 : 0);
                    }
                }
                result = (NA) array;
            }
        } else if (short[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                short[] array = new short[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = dataArray[i] ? (short) 1 : 0;
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                short[] array = new short[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = ((dataArray[i] != null) && dataArray[i].booleanValue()) ? (short) 1 : 0;
                }
                result = (NA) array;
            }
        } else if (Short[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                Short[] array = new Short[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Short.valueOf(dataArray[i] ? (short) 1 : 0);
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                Short[] array = new Short[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    if (dataArray[i] == null) {
                        array[i] = null;
                    } else {
                        array[i] = Short.valueOf(dataArray[i].booleanValue() ? (short) 1 : 0);
                    }
                }
                result = (NA) array;
            }
        } else if (int[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                int[] array = new int[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = dataArray[i] ? (int) 1 : 0;
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                int[] array = new int[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = ((dataArray[i] != null) && dataArray[i].booleanValue()) ? (int) 1 : 0;
                }
                result = (NA) array;
            }
        } else if (Integer[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                Integer[] array = new Integer[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Integer.valueOf(dataArray[i] ? (int) 1 : 0);
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                Integer[] array = new Integer[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    if (dataArray[i] == null) {
                        array[i] = null;
                    } else {
                        array[i] = Integer.valueOf(dataArray[i].booleanValue() ? (int) 1 : 0);
                    }
                }
                result = (NA) array;
            }
        } else if (long[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                long[] array = new long[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = dataArray[i] ? (long) 1 : 0;
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                long[] array = new long[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = ((dataArray[i] != null) && dataArray[i].booleanValue()) ? (long) 1 : 0;
                }
                result = (NA) array;
            }
        } else if (Long[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                Long[] array = new Long[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Long.valueOf(dataArray[i] ? (long) 1 : 0);
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                Long[] array = new Long[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    if (dataArray[i] == null) {
                        array[i] = null;
                    } else {
                        array[i] = Long.valueOf(dataArray[i].booleanValue() ? (long) 1 : 0);
                    }
                }
                result = (NA) array;
            }
        } else if (BigInteger[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                BigInteger[] array = new BigInteger[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = BigInteger.valueOf(dataArray[i] ? (long) 1 : 0);
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                BigInteger[] array = new BigInteger[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    if (dataArray[i] == null) {
                        array[i] = null;
                    } else {
                        array[i] = BigInteger.valueOf(dataArray[i].booleanValue() ? (long) 1 : 0);
                    }
                }
                result = (NA) array;
            }
        } else if (float[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                float[] array = new float[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = dataArray[i] ? (float) 1 : 0;
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                float[] array = new float[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = ((dataArray[i] != null) && dataArray[i].booleanValue()) ? (float) 1 : 0;
                }
                result = (NA) array;
            }
        } else if (Float[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                Float[] array = new Float[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Float.valueOf(dataArray[i] ? (float) 1 : 0);
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                Float[] array = new Float[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    if (dataArray[i] == null) {
                        array[i] = null;
                    } else {
                        array[i] = Float.valueOf(dataArray[i].booleanValue() ? (float) 1 : 0);
                    }
                }
                result = (NA) array;
            }
        } else if (double[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                double[] array = new double[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = dataArray[i] ? (double) 1 : 0;
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                double[] array = new double[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = ((dataArray[i] != null) && dataArray[i].booleanValue()) ? (double) 1 : 0;
                }
                result = (NA) array;
            }
        } else if (Double[].class.equals(firstType)) {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                Double[] array = new Double[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Double.valueOf(dataArray[i] ? (double) 1 : 0);
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                Double[] array = new Double[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    if (dataArray[i] == null) {
                        array[i] = null;
                    } else {
                        array[i] = Double.valueOf(dataArray[i].booleanValue() ? (double) 1 : 0);
                    }
                }
                result = (NA) array;
            }
        } else {
            if (data instanceof boolean[]) {
                boolean[] dataArray = (boolean[]) data;
                Number[] array = new Number[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    array[i] = Double.valueOf(dataArray[i] ? (double) 1 : 0);
                }
                result = (NA) array;
            } else {
                Boolean[] dataArray = (Boolean[]) data;
                Number[] array = new Number[dataArray.length];
                for (int i = 0; i < dataArray.length; i++) {
                    if (dataArray[i] == null) {
                        array[i] = null;
                    } else {
                        array[i] = Double.valueOf(dataArray[i].booleanValue() ? (double) 1 : 0);
                    }
                }
                result = (NA) array;
            }
        }
        return result;
    }

}
