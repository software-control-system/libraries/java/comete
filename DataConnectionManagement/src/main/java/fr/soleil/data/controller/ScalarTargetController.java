/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import org.slf4j.LoggerFactory;

import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.adapter.AdapterUtils;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.exception.UnhandledTargetSignatureException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;
import fr.soleil.data.target.scalar.IScalarTarget;

/**
 * This class allows you to connect a {@link ITarget} which can read a scalar T with a
 * {@link AbstractDataSource} sending a scalar U.
 * 
 * @author huriez
 * 
 * @param <T> the type of the {@link ITarget}
 * @param <U> the type of the {@link IDataContainer}
 */
public class ScalarTargetController<U, T> extends DataTargetController<U, T> {

    protected AbstractAdapter<U, T> converter;
    protected Class<T> targetType;
    protected final Class<U> sourceType;

    protected final Class<?> targetClass;

    /**
     * Constructor
     * 
     * @param classTarget the type of {@link ITarget} this controller has to manage
     * @param typeSource the primitive type of the {@link AbstractDataSource}
     * @param typeTarget the primitive type of the {@link ITarget}
     */
    @SuppressWarnings("unchecked")
    public ScalarTargetController(Class<?> classTarget, Class<U> typeSource, Class<T> typeTarget) {
        super();

        converter = null;
        sourceType = typeSource;
        targetClass = classTarget;

        if (typeTarget != null) {
            targetType = typeTarget;
        }

        try {
            targetSignature = initSignature();
        } catch (UnhandledTargetSignatureException e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to init signature", e);
        }

        if (typeTarget == null) {
            targetType = (Class<T>) targetSignature.getTargetType();
        }

    }

    @Override
    protected DataSourceAdapter<U, T> createAdapter(AbstractDataSource<U> source) {
        DataSourceAdapter<U, T> adapter = null;
        if (converter == null) {
            converter = AdapterUtils.getAdapter(
                    (source == null ? new GenericDescriptor(sourceType) : source.getDataType()),
                    new GenericDescriptor(targetType));
        }

        if (converter != null) {
            adapter = new DataSourceAdapter<U, T>(source, converter);
        }

        return adapter;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <I> T getCastedInfo(I info) {
        T result = null;
        if (isInstanceOf(info)) {
            result = (T) info;
        }
        return result;
    }

    @Override
    public <I> boolean isInstanceOf(I info) {
        return ((info == null) || targetType.isAssignableFrom(info.getClass()));
    }

    @Override
    protected TargetSignature<T> initSignature() throws UnhandledTargetSignatureException {
        // XXX method to remove after target signature removal
        TargetSignature<T> result = TargetSignatureProvider.getTargetSignature(targetClass, targetType,
                IScalarTarget.class);
        return result;
    }

    @Override
    public GenericDescriptor getSourceType() {
        return new GenericDescriptor(sourceType);
    }

    @Override
    public GenericDescriptor getTargetType() {
        return new GenericDescriptor(targetType);
    }

}
