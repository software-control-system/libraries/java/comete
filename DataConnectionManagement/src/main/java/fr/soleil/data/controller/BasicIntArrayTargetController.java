/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IPositionTarget;
import fr.soleil.data.target.ITarget;

public class BasicIntArrayTargetController extends BasicTargetController<int[]> {

    @Override
    protected int[] generateDefaultValue() {
        return new int[0];
    }

    @Override
    protected void setDataToTarget(ITarget target, int[] data, AbstractDataSource<int[]> source) {
        ((IPositionTarget) target).setPositions(data);
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return (data instanceof int[]);
    }

    @Override
    public boolean isCompatibleWith(ITarget target) {
        return (target instanceof IPositionTarget);
    }

}
