/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.scalar.INumberTarget;

/**
 * A {@link BasicTargetController} that makes link between {@link Number} {@link AbstractDataSource}
 * s and {@link INumberTarget}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BasicNumberTargetController extends BasicTargetController<Number> {

    protected static final GenericDescriptor NUMBER_DESCRIPTOR = new GenericDescriptor(Number.class);

    public BasicNumberTargetController() {
        super();
    }

    @Override
    protected Number generateDefaultValue() {
        return 0;
    }

    @Override
    protected void setDataToTarget(ITarget target, Number data, AbstractDataSource<Number> source) {
        ((INumberTarget) target).setNumberValue(data);
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return (data instanceof Number);
    }

    @Override
    public boolean isCompatibleWith(ITarget target) {
        return (target instanceof INumberTarget);
    }

    @Override
    public GenericDescriptor getSourceType() {
        return NUMBER_DESCRIPTOR;
    }

}
