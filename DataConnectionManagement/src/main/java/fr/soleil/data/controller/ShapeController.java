/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import java.util.List;
import java.util.logging.Level;

import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IStackShapeTarget;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.information.TargetInformation;

/**
 * This is the controller that transmits a stack shape to the {@link IStackShapeTarget}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ShapeController extends AbstractController<int[]> {

    public ShapeController() {
        super();
        setFirstInitAllowed(false);
    }

    @Override
    protected <V> boolean connectFilterToTarget(AbstractDataFilter<V> filter, ITarget target) {
        // not managed
        return false;
    }

    @Override
    protected <V> boolean isFilterCompatibleWithTarget(AbstractDataFilter<V> filter, ITarget target) {
        // not managed
        return false;
    }

    @Override
    public boolean isTargetAssignable(ITarget target) {
        return (target instanceof IStackShapeTarget);
    }

    @Override
    protected void registerAdapter(AbstractDataSource<int[]> source) {
        // not managed
    }

    @Override
    protected void transmitDataToTarget(AbstractDataSource<int[]> source, AbstractDataFilter<int[]> associatedFilter,
            List<ITarget> targets) {
        if (targets != null) {
            IDataContainer<int[]> dataContainer;
            if (associatedFilter == null) {
                dataContainer = source;
            } else {
                dataContainer = associatedFilter;
            }
            if (dataContainer != null) {
                int[] shape;
                try {
                    shape = dataContainer.getData();
                    for (ITarget target : targets) {
                        if (isTargetAssignable(target)) {
                            ((IStackShapeTarget) target).setStackShape(shape);
                        }
                    }
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Failed to transmit " + source + " data to targets", e);
                }
            }
        }
    }

    @Override
    protected void unregisterAdapter(AbstractDataSource<int[]> source) {
        // not managed
    }

    @Override
    public <V> void removeFilterFromTarget(AbstractDataFilter<V> filter, ITarget target) {
        // not managed
    }

    @Override
    public <I, V extends TargetInformation<I>> void transmitTargetChange(V targetInformation) {
        // not managed
    }

    @Override
    public GenericDescriptor getSourceType() {
        return new GenericDescriptor(int[].class);
    }

}
