/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import org.slf4j.LoggerFactory;

import fr.soleil.data.adapter.AbstractMatrixToObjectMatrixAdapter;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.exception.UnhandledTargetSignatureException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;
import fr.soleil.data.target.matrix.IMatrixTarget;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * This class allows you to connect a {@link ITarget} which can read an array of {@link Object} of
 * rank 2 with a {@link AbstractDataSource} sending a {@link Matrix} of type U.
 * 
 * @author huriez
 * 
 * @param <U> The type of data the {@link AbstractDataSource} can manipulate
 * @param <T> The type of data the {@link ITarget} can manipulate
 * 
 */
public class MatrixTargetController<U, T> extends DataTargetController<AbstractMatrix<U>, Object[]> {

    private Class<T> targetType;
    private final Class<U> sourceType;
    private final Class<?> targetClass;

    /**
     * Constructor
     * 
     * @param classTarget the type of {@link ITarget} this controller has to manage
     * @param typeSource the primitive type of the {@link AbstractDataSource}
     * @param typeTarget the primitive type of the {@link ITarget}
     */
    @SuppressWarnings("unchecked")
    public MatrixTargetController(Class<?> classTarget, Class<U> source, Class<T> typeTarget) {
        super();

        sourceType = source;
        targetClass = classTarget;

        if (typeTarget != null) {
            targetType = typeTarget;
        }

        try {
            targetSignature = initSignature();
        } catch (UnhandledTargetSignatureException e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to init signature", e);
        }

        if (typeTarget == null) {
            targetType = (Class<T>) targetSignature.getTargetType();
        }

    }

    @Override
    public DataSourceAdapter<AbstractMatrix<U>, Object[]> createAdapter(AbstractDataSource<AbstractMatrix<U>> source) {
        return new DataSourceAdapter<AbstractMatrix<U>, Object[]>(source,
                new AbstractMatrixToObjectMatrixAdapter<U>(sourceType, targetType));
    }

    @Override
    public <I> Object[] getCastedInfo(I info) {
        Object[] result = null;
        if (isInstanceOf(info)) {
            result = (Object[]) info;
        }
        return result;
    }

    @Override
    public <I> boolean isInstanceOf(I info) {
        return targetType.isAssignableFrom(ArrayUtils.recoverDataType(info));
    }

    @Override
    protected TargetSignature<Object[]> initSignature() throws UnhandledTargetSignatureException {
        TargetSignature<Object[]> result = TargetSignatureProvider.getTargetSignature(targetClass, targetType,
                IMatrixTarget.class);
        return result;
    }

    @Override
    public GenericDescriptor getSourceType() {
        return new GenericDescriptor(AbstractMatrix.class,
                new GenericDescriptor[] { new GenericDescriptor(sourceType) });
    }

    @Override
    public GenericDescriptor getTargetType() {
        return new GenericDescriptor(Object[].class);
    }

}
