/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import javax.activation.UnsupportedDataTypeException;

import org.slf4j.LoggerFactory;

import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.util.NumberMatrixGenerator;
import fr.soleil.data.exception.UnhandledTargetSignatureException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.TargetSignatureProvider;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;
import fr.soleil.data.target.matrix.IMatrixTarget;

public class NumberMatrixController extends DataTargetController<AbstractNumberMatrix<?>, Object[]> {

    protected final Class<?> targetClass;

    public NumberMatrixController(Class<?> targetClass) {
        super();
        this.targetClass = targetClass;
        try {
            targetSignature = initSignature();
        } catch (UnhandledTargetSignatureException e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to init signature", e);
        }
    }

    @Override
    protected DataSourceAdapter<AbstractNumberMatrix<?>, Object[]> createAdapter(
            AbstractDataSource<AbstractNumberMatrix<?>> source) {
        return new WildcardNumberAdapter(source);
    }

    @Override
    public <I> Object[] getCastedInfo(I info) {
        Object[] result = null;
        if (isInstanceOf(info)) {
            result = (Object[]) info;
        }
        return result;
    }

    @Override
    protected TargetSignature<Object[]> initSignature() throws UnhandledTargetSignatureException {
        TargetSignature<Object[]> result = TargetSignatureProvider.getTargetSignature(targetClass, Object[].class,
                IMatrixTarget.class);
        return result;
    }

    @Override
    public <I> boolean isInstanceOf(I info) {
        return info instanceof Object[];
    }

    @Override
    public GenericDescriptor getSourceType() {
        return new GenericDescriptor(AbstractNumberMatrix.class, new GenericDescriptor[1]);
    }

    @Override
    public GenericDescriptor getTargetType() {
        return new GenericDescriptor(Object[].class);
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    private class WildcardNumberAdapter extends DataSourceAdapter<AbstractNumberMatrix<?>, Object[]> {

        public WildcardNumberAdapter(AbstractDataSource<AbstractNumberMatrix<?>> source) {
            super(source, null);
        }

        @Override
        protected AbstractNumberMatrix<?> adaptContainerData(Object[] data) {
            AbstractNumberMatrix<?> result = NumberMatrixGenerator.getNewMatrixFor(data);
            if (result != null) {
                try {
                    result.setValue(data);
                } catch (UnsupportedDataTypeException e) {
                    // Should not happen
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to adapt container data", e);
                }
            }
            return result;
        }

        @Override
        protected Object[] adaptSourceData(AbstractNumberMatrix<?> data) {
            Object[] result = null;
            if (data != null) {
                result = data.getValue();
            }
            return result;
        }

    }

}
