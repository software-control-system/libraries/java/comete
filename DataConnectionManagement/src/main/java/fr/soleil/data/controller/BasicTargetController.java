/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.filter.AbstractChecker.CheckingMode;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IGenericTarget;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.data.util.DataConnectionUtils;

/**
 * This is an {@link AbstractController} that makes a link between {@link AbstractDataSource}s and {@link ITarget}s that
 * use the same type of data
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class BasicTargetController<U> extends AbstractController<U> {

    protected final Map<ITarget, AbstractDataFilter<U>> targetFilters;

    public BasicTargetController() {
        super();
        targetFilters = new HashMap<>();
        setFirstInitAllowed(false);
    }

    @Override
    protected void registerAdapter(AbstractDataSource<U> source) {
        // nothing to do: no adapter needed
    }

    @Override
    protected void unregisterAdapter(AbstractDataSource<U> source) {
        // nothing to do: no adapter needed
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <V> boolean connectFilterToTarget(AbstractDataFilter<V> filter, ITarget target) {
        boolean connected = false;
        synchronized (targetFilters) {
            if (target != null) {
                if (filter == null) {
                    targetFilters.remove(target);
                    connected = true;
                } else {
                    AbstractDataFilter<U> expected = (AbstractDataFilter<U>) filter;
                    targetFilters.put(target, expected);
                    connected = true;
                }
            }
        }
        return connected;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <V> boolean isFilterCompatibleWithTarget(AbstractDataFilter<V> filter, ITarget target) {
        boolean compatible = true;
        if (filter != null) {
            try {
                AbstractDataFilter<U> expected = (AbstractDataFilter<U>) filter;
                IDataContainer<U> previousTopContainer = null;
                AbstractDataFilter<U> tempFilter = expected;
                while (tempFilter.getDataContainer() instanceof AbstractDataFilter<?>) {
                    tempFilter = (AbstractDataFilter<U>) tempFilter.getDataContainer();
                }
                previousTopContainer = tempFilter.getDataContainer();
                updateFilter(expected, null);
                expected.setData(generateDefaultValue());
                updateFilter(expected, previousTopContainer);
            } catch (Exception e) {
                compatible = false;
            }
        }
        return compatible;
    }

    protected abstract U generateDefaultValue();

    @Override
    protected void transmitDataToTarget(AbstractDataSource<U> source, AbstractDataFilter<U> associatedFilter,
            List<ITarget> targets) {
        if (targets != null) {
            IDataContainer<U> dataContainer;
            if (associatedFilter == null) {
                dataContainer = source;
            } else {
                dataContainer = associatedFilter;
            }
            try {
                U originalData = dataContainer.getData();
                DataContainerTool dataContainerTool = new DataContainerTool(originalData, dataContainer.isUnsigned(),
                        source.getDataType());
                boolean unsigned = dataContainerTool.isUnsigned();
                if ((originalData != null) && unsigned) {
                    // XXX SCAN-853: remember read data class: maybe not perfect yet
                    dataTypeMap.put(dataContainer, originalData.getClass());
                }
                for (ITarget target : targets) {
                    AbstractDataFilter<U> targetFilter;
                    synchronized (targetFilters) {
                        targetFilter = targetFilters.get(target);
                    }
                    if (targetFilter == null) {
                        U data = dataContainerTool.getData();
                        if ((data != null) && unsigned) {
                            // SCAN-853: convert unsigned numbers
                            @SuppressWarnings("unchecked")
                            U dataToTransmit = (U) DataConnectionUtils.convertUnsigned(data);
                            data = dataToTransmit;
                        }
                        if (target instanceof IGenericTarget<?>) {
                            setDataToGenericTarget(target, data, source);
                        } else {
                            setDataToTarget(target, data, source);
                        }
                    } else {
                        synchronized (targetFilter) {
                            updateFilter(targetFilter, dataContainerTool);
                            U data = targetFilter.getData();
                            if ((data != null) && targetFilter.isUnsigned()) {
                                dataTypeMap.put(targetFilter, data.getClass());
                                // SCAN-853: convert unsigned numbers
                                @SuppressWarnings("unchecked")
                                U dataToTransmit = (U) DataConnectionUtils.convertUnsigned(data);
                                data = dataToTransmit;
                            }
                            if (target instanceof IGenericTarget<?>) {
                                setDataToGenericTarget(target, data, source);
                            } else {
                                setDataToTarget(target, data, source);
                            }
                            updateFilter(targetFilter, null);
                        }
                    } // end if (targetFilter == null) ... else
                } // end for (ITarget target : targets)
                dataContainerTool.clean();
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Failed to transmit " + source + " data to targets", e);
            }
        }
    }

    /**
     * Sets some {@link ITarget}'s data, knowing which {@link AbstractDataSource} is at the origin of this new data
     * 
     * @param target The {@link ITarget}
     * @param data The data to send to the {@link ITarget}
     * @param source The {@link AbstractDataSource} at the origin of this change
     */
    protected abstract void setDataToTarget(ITarget target, U data, AbstractDataSource<U> source);

    @SuppressWarnings("unchecked")
    protected void setDataToGenericTarget(ITarget target, U data, AbstractDataSource<U> source) {
        ((IGenericTarget<U>) target).setTargetData(data);
    }

    @Override
    public <V> void removeFilterFromTarget(AbstractDataFilter<V> filter, ITarget target) {
        synchronized (targetFilters) {
            if ((filter != null) && (target != null) && targetFilters.containsKey(target)
                    && (filter == targetFilters.get(target))) {
                targetFilters.remove(target);
                updateFilter(filter, null);
            }
        }
    }

    /**
     * Returns whether some data is compatible with source data
     * 
     * @param data The data to test
     * @return A <code>boolean</code>
     */
    protected abstract boolean isInstanceOfU(Object data);

    @SuppressWarnings("unchecked")
    @Override
    public <I, V extends TargetInformation<I>> void transmitTargetChange(V targetInformation) {
        if (targetInformation != null) {
            ITarget target = targetInformation.getConcernedTarget();
            Object info = targetInformation.getInformationData();
            if (isInstanceOfU(info)) {
                U data = (U) info;
                List<AbstractDataSource<U>> sourceList = null;
                synchronized (targetAssociations) {
                    List<AbstractDataSource<U>> tempList = targetAssociations.get(target);
                    if (tempList != null) {
                        sourceList = new ArrayList<>();
                        sourceList.addAll(tempList);
                    }
                }
                if (sourceList != null) {
                    AbstractDataFilter<U> targetFilter;
                    synchronized (targetFilters) {
                        targetFilter = targetFilters.get(target);
                    }
                    for (final AbstractDataSource<U> source : sourceList) {
                        if (isSourceChecked(source, info, CheckingMode.INPUT_MODE)) {
                            DataSetterTool setter = new DataSetterTool(source, source.getDataType(), target);
                            if (targetFilter == null) {
                                setter.setData(data);
                            } else {
                                synchronized (targetFilter) {
                                    updateFilter(targetFilter, setter);
                                    try {
                                        targetFilter.setData(data);
                                    } catch (Exception e) {
                                        treatSourceDataSendingError(target, source, data, e);
                                    }
                                    updateFilter(targetFilter, null);
                                }
                            }
                            setter.clean();
                        } else if (isCancelable(target)) {
                            threadedCancelSourceDataSetting(source);
                        }
                    }

                    sourceList.clear();
                }
            }
        }
    }

    @Override
    public boolean isTargetAssignable(ITarget target) {
        boolean result = false;
        if (target instanceof IGenericTarget) {
            result = getSourceType().isAssignableFrom(((IGenericTarget<?>) target).getTargetType());
        } else {
            result = isCompatibleWith(target);
        }
        return result;
    }

    /**
     * Returns whether This {@link BasicTargetController} is compatible with a given {@link ITarget} that is not an
     * {@link IGenericTarget}
     * 
     * @param target The {@link ITarget} to test
     * @return A <code>boolean</code>
     */
    protected abstract boolean isCompatibleWith(ITarget target);

    @Override
    public GenericDescriptor getSourceType() {
        GenericDescriptor result = null;
        U sourceDefault = generateDefaultValue();
        if (sourceDefault != null) {
            result = new GenericDescriptor(sourceDefault.getClass());
        }
        return result;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class DataContainerTool implements IDataContainer<U> {
        private U data;
        private boolean unsigned;
        private final GenericDescriptor descriptor;

        public DataContainerTool(U data, boolean unsigned, GenericDescriptor descriptor) {
            this.data = data;
            this.unsigned = unsigned;
            this.descriptor = descriptor;
        }

        @Override
        public U getData() {
            return data;
        }

        @Override
        public void setData(U data) {
            this.data = data;
        }

        @Override
        public GenericDescriptor getDataType() {
            return descriptor;
        }

        @Override
        public boolean isSettable() {
            return true;
        }

        @Override
        public boolean isUnsigned() {
            return unsigned;
        }

        public void setUnsigned(boolean unsigned) {
            this.unsigned = unsigned;
        }

        public void clean() {
            data = null;
        }
    }

    protected class DataSetterTool implements IDataContainer<U> {
        private AbstractDataSource<U> decorated;
        private final GenericDescriptor descriptor;
        private ITarget origin;

        public DataSetterTool(AbstractDataSource<U> decorated, GenericDescriptor descriptor, ITarget origin) {
            this.decorated = decorated;
            this.descriptor = descriptor;
            this.origin = origin;
        }

        @Override
        public U getData() throws Exception {
            return (decorated == null ? null : decorated.getData());
        }

        @Override
        public void setData(U data) {
            transmitDataToSource(data, decorated, origin);
        }

        @Override
        public GenericDescriptor getDataType() {
            return descriptor;
        }

        @Override
        public boolean isSettable() {
            return true;
        }

        @Override
        public boolean isUnsigned() {
            AbstractDataSource<U> decorated = this.decorated;
            return decorated == null ? false : decorated.isUnsigned();
        }

        public void clean() {
            decorated = null;
            origin = null;
        }
    }
}
