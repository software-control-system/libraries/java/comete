/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.scalar.IBooleanTarget;

/**
 * This is a {@link BasicTargetController} that makes link between {@link Boolean}
 * {@link AbstractDataSource}s and {@link IBooleanTarget}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BasicBooleanTargetController extends BasicTargetController<Boolean> {

    @Override
    protected Boolean generateDefaultValue() {
        return false;
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return (data instanceof Boolean);
    }

    @Override
    protected void setDataToTarget(ITarget target, Boolean data, AbstractDataSource<Boolean> source) {
        ((IBooleanTarget) target).setSelected(data == null ? false : data.booleanValue());
    }

    @Override
    public boolean isCompatibleWith(ITarget target) {
        return (target instanceof IBooleanTarget);
    }

}
