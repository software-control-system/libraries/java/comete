/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.IThreadConfigurationTarget;
import fr.soleil.data.thread.IThreadConfiguration;
import fr.soleil.data.thread.ThreadConfiguration;

public class ThreadConfigurationController extends BasicTargetController<IThreadConfiguration> {

    public ThreadConfigurationController() {
        super();
    }

    @Override
    protected IThreadConfiguration generateDefaultValue() {
        return new ThreadConfiguration();
    }

    @Override
    protected void setDataToTarget(ITarget target, IThreadConfiguration data,
            AbstractDataSource<IThreadConfiguration> source) {
        ((IThreadConfigurationTarget) target).setThreadConfiguration(data);
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return ((data == null) || (data instanceof IThreadConfiguration));
    }

    @Override
    protected boolean isCompatibleWith(ITarget target) {
        return target instanceof IThreadConfigurationTarget;
    }

}
