/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.array.INumberArrayTarget;

/**
 * A {@link BasicTargetController} that makes link between number (primitive type or {@link Number})
 * array {@link AbstractDataSource} s and {@link INumberArrayTarget}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BasicNumberArrayTargetController extends BasicTargetController<Object> {

    public BasicNumberArrayTargetController() {
        super();
    }

    @Override
    protected Object generateDefaultValue() {
        return new double[0];
    }

    @Override
    protected void setDataToTarget(ITarget target, Object data, AbstractDataSource<Object> source) {
        ((INumberArrayTarget) target).setNumberArray(data);
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return ((data == null) || (data instanceof Number[]) || (data instanceof byte[]) || (data instanceof short[])
                || (data instanceof int[]) || (data instanceof long[]) || (data instanceof float[])
                || (data instanceof double[]));
    }

    @Override
    public boolean isCompatibleWith(ITarget target) {
        return (target instanceof INumberArrayTarget);
    }

}
