/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import fr.soleil.data.adapter.NumberToStringAdapter;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.adapter.source.NumberToStringDataAdapter;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;

/**
 * A {@link ScalarTargetController} specialized in interaction between {@link Number} data source
 * and {@link String} targets
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class StringTargetNumberSourceController extends ScalarTargetController<Number, String> {

    public StringTargetNumberSourceController(Class<?> classTarget) {
        super(classTarget, Number.class, String.class);
    }

    @SuppressWarnings("unchecked")
    protected <N extends Number> DataSourceAdapter<N, String> generateAdapter(AbstractDataSource<N> source) {
        return new NumberToStringDataAdapter<N>(source,
                new NumberToStringAdapter<N>((source == null) || (source.getDataType() == null) ? null
                        : (Class<N>) source.getDataType().getConcernedClass()));
    }

    @Override
    protected DataSourceAdapter<Number, String> createAdapter(AbstractDataSource<Number> source) {
        return generateAdapter(source);
    }

    /**
     * Returns the {@link NumberToStringDataAdapter} associated with the a particular
     * {@link AbstractDataSource}. This method is available because a
     * {@link NumberToStringDataAdapter} is also an {@link ITarget}
     * 
     * @param source The {@link AbstractDataSource}
     * @return The corresponding {@link NumberToStringDataAdapter}, or <code>null</code> is there is
     *         no such adapter
     */
    @SuppressWarnings("unchecked")
    public <N extends Number> NumberToStringDataAdapter getAdapter(AbstractDataSource<N> source) {
        NumberToStringDataAdapter<N> result = null;
        DataSourceAdapter<Number, String> adapter = adapters.get(source);
        if (adapter instanceof NumberToStringDataAdapter) {
            result = (NumberToStringDataAdapter<N>) adapter;
        }
        return result;
    }

}
