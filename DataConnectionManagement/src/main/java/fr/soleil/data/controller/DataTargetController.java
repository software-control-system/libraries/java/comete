/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;

import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.container.BasicDataContainer;
import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.exception.UnhandledTargetSignatureException;
import fr.soleil.data.filter.AbstractChecker.CheckingMode;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IGenericTarget;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.data.util.DataConnectionUtils;

/**
 * This abstract class defined a {@link Mediator} able of transmitting the useful data of an {@link AbstractDataSource}
 * and a {@link ITarget}. For example, it will transmit a String data
 * displayed by a Label from an independent source. An {@link AbstractController} specializing this
 * class aims to treat form of the data, that is to say if his rank is an array, its container if
 * this is so etc.
 * 
 * @author huriez
 * 
 * @param <T> the complete type of the {@link ITarget}
 * @param <U> the complete type of the {@link IDataContainer}
 */
public abstract class DataTargetController<U, T> extends AbstractController<U> {

    protected final Map<AbstractDataSource<U>, DataSourceAdapter<U, T>> adapters;
    protected final Map<ITarget, DataSourceAdapter<U, T>> customTargetAdapters;
    protected final Map<ITarget, AbstractDataFilter<T>> targetFilters;
    protected TargetSignature<T> targetSignature;

    /**
     * Default Constructor
     */
    public DataTargetController() {
        super();
        adapters = new WeakHashMap<>();
        targetFilters = new WeakHashMap<>();
        customTargetAdapters = new WeakHashMap<>();
    }

    public DataSourceAdapter<U, T> getSourceAdapter(AbstractDataSource<U> source) {
        DataSourceAdapter<U, T> adapter = null;
        synchronized (adapters) {
            adapter = adapters.get(source);
        }
        return adapter;
    }

    public void setSourceAdapter(AbstractDataSource<U> source, DataSourceAdapter<U, T> adapter) {
        if ((adapter != null) && (source != null)) {
            synchronized (adapters) {
                if (adapters.containsKey(source)) {
                    adapters.put(source, adapter);
                }
            }
        }
    }

    public DataSourceAdapter<U, T> getCustomTargetAdapter(ITarget target) {
        DataSourceAdapter<U, T> adapter = null;
        synchronized (customTargetAdapters) {
            adapter = customTargetAdapters.get(target);
        }
        return adapter;
    }

    public void setCustomTargetAdapter(ITarget target, DataSourceAdapter<U, T> adapter) {
        if (target != null) {
            synchronized (customTargetAdapters) {
                if (adapter == null) {
                    customTargetAdapters.remove(target);
                } else {
                    customTargetAdapters.put(target, adapter);
                }
            }
        }
    }

    @Override
    protected void transmitDataToTarget(AbstractDataSource<U> source, AbstractDataFilter<U> associatedFilter,
            List<ITarget> targets) {
        DataSourceAdapter<U, T> adapter = getSourceAdapter(source);
        if ((targets != null) && (adapter != null)) {
            IDataContainer<U> container = source;
            if (associatedFilter != null) {
                container = associatedFilter;
            }
            adapter.setDataSource(container);
            ArrayList<ITarget> copy = new ArrayList<>();
            synchronized (targets) {
                copy.addAll(targets);
            }
            try {
                T targetValue = adapter.getData();
                if (adapter.isUnsigned()) {
                    if (targetValue != null) {
                        // XXX SCAN-853: remember read data class: maybe not perfect yet
                        dataTypeMap.put(adapter, targetValue.getClass());
                    }
                    // SCAN-853: convert unsigned numbers
                    @SuppressWarnings("unchecked")
                    T tmp = (T) DataConnectionUtils.convertUnsigned(targetValue);
                    targetValue = tmp;
                }
                IDataContainer<T> tempContainer = new BasicDataContainer<>(targetValue, null);
                for (ITarget target : copy) {
                    if (isTargetAssignable(target)) {
                        DataSourceAdapter<U, T> temp = getCustomTargetAdapter(target);
                        if (temp != null) {
                            temp.setDataSource(container);
                            adapter = temp;
                            targetValue = adapter.getData();
                            if (adapter.isUnsigned()) {
                                if (targetValue != null) {
                                    dataTypeMap.put(adapter, targetValue.getClass());
                                }
                                @SuppressWarnings("unchecked")
                                T tmp = (T) DataConnectionUtils.convertUnsigned(targetValue);
                                targetValue = tmp;
                            }
                            tempContainer = new BasicDataContainer<>(targetValue, null);
                        }
                        AbstractDataFilter<T> filter;
                        synchronized (targetFilters) {
                            filter = targetFilters.get(target);
                        }
                        if (filter == null) {
                            sendDataToTarget(target, targetValue, source);
                        } else {
                            try {
                                updateFilter(filter, tempContainer);
                                sendDataToTarget(target, filter.getData(), source);
                                updateFilter(filter, null);
                            } catch (Exception e) {
                                logger.log(Level.SEVERE, e.getMessage(), e);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Failed to transmit " + source + " data to targets", e);
            }
            copy.clear();
        }
    }

    @Override
    public <I, V extends TargetInformation<I>> void transmitTargetChange(V targetInformation) {
        if (targetInformation != null) {
            ITarget target = targetInformation.getConcernedTarget();
            if (target != null) {
                DataSourceAdapter<U, T> adapter = getCustomTargetAdapter(target);
                List<AbstractDataSource<U>> sources;
                synchronized (targetAssociations) {
                    sources = targetAssociations.get(target);
                }
                if (sources != null) {
                    ArrayList<AbstractDataSource<U>> copy = new ArrayList<>();
                    synchronized (sources) {
                        copy.addAll(sources);
                    }
                    I info = targetInformation.getInformationData();
                    if (isInstanceOf(info)) {
                        for (final AbstractDataSource<U> source : copy) {
                            if (adapter == null) {
                                adapter = getSourceAdapter(source);
                            }
                            AbstractDataFilter<U> filter;
                            synchronized (sourceFilters) {
                                filter = sourceFilters.get(source);
                            }
                            if (adapter != null) {
                                if (filter != null) {
                                    adapter.setDataSource(filter);
                                }
                                T data = getCastedInfo(info);
                                if (adapter.isSettable() && isSourceChecked(source, data, CheckingMode.INPUT_MODE)) {
                                    try {
                                        sendDataToSource(adapter, data);
                                        if (source.isWriteBeforeRead() && source.hasSomeDataToReturn()) {
                                            transmitSourceChange(source, target);
                                        }
                                    } catch (Exception e) {
                                        treatSourceDataSendingError(target, source, data, e);
                                    }
                                } else if (isCancelable(target)) {
                                    new Thread("Cancel source '" + source + "' data edition") {
                                        @Override
                                        public void run() {
                                            transmitSourceChange(source);
                                        }
                                    }.start();
                                } else {
                                }
                            }
                        }
                    }
                    copy.clear();
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <V> boolean connectFilterToTarget(AbstractDataFilter<V> filter, ITarget target) {
        boolean connected = false;
        synchronized (targetFilters) {
            if (target != null && isFilterCompatibleWithTarget(filter, target)) {
                if (filter == null) {
                    targetFilters.remove(target);
                    connected = true;
                } else {
                    AbstractDataFilter<T> expected = (AbstractDataFilter<T>) filter;
                    targetFilters.put(target, expected);
                    connected = true;
                }
            }
        }
        return connected;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <V> boolean isFilterCompatibleWithTarget(AbstractDataFilter<V> filter, ITarget target) {
        boolean compatible = true;
        if (filter != null) {
            try {
                AbstractDataFilter<T> expected = (AbstractDataFilter<T>) filter;
                compatible = expected.getDataType().equals(getTargetType());
            } catch (Exception e) {
                compatible = false;
            }
        }
        return compatible;
    }

    @Override
    public void registerAdapter(AbstractDataSource<U> source) {
        if (source != null) {
            synchronized (adapters) {
                DataSourceAdapter<U, T> adapter = adapters.get(source);
                if (adapter == null) {
                    adapter = createAdapter(source);
                    if (adapter != null) {
                        adapters.put(source, adapter);
                    }
                }
            }
        }
    }

    @Override
    protected void unregisterAdapter(AbstractDataSource<U> source) {
        if (source != null) {
            synchronized (adapters) {
                adapters.remove(source);
            }
        }
    }

    @Override
    public <V> void removeFilterFromTarget(AbstractDataFilter<V> filter, ITarget target) {
        synchronized (targetFilters) {
            if ((filter != null) && (target != null) && targetFilters.containsKey(target)
                    && (filter == targetFilters.get(target))) {
                targetFilters.remove(target);
                updateFilter(filter, null);
            }
        }
    }

    protected void sendDataToTarget(ITarget target, T targetValue, AbstractDataSource<U> source) {
        if (target instanceof IGenericTarget<?>) {
            sendDataToGenericTarget(target, targetValue, source);
        } else {
            sendDataToTargetUsingSignature(target, targetValue, source);
        }
    }

    protected void sendDataToTargetUsingSignature(ITarget target, T targetValue, AbstractDataSource<U> source) {
        if (targetSignature != null) {
            targetSignature.sendData(target, targetValue);
        }
    }

    @SuppressWarnings("unchecked")
    protected void sendDataToGenericTarget(ITarget target, T targetValue, AbstractDataSource<U> source) {
        if (target != null && targetValue != null && target instanceof IGenericTarget) {
            ((IGenericTarget<T>) target).setTargetData(targetValue);
        }
    }

    protected <I> void sendDataToSource(DataSourceAdapter<U, T> adapter, T data) throws Exception {
        if (adapter != null) {
            adapter.setData(data);
        }
    }

    @Override
    public boolean isTargetAssignable(ITarget target) {
        boolean result = false;
        if (target instanceof IGenericTarget) {
            result = getTargetType().isAssignableFrom(((IGenericTarget<?>) target).getTargetType());
        } else if (targetSignature != null) {
            result = targetSignature.isAssignable(target);
        }
        return result;
    }

    /**
     * Check if the information send is handle by the controller
     * 
     * @param <I> the type of the information
     * @param info the information sent
     * @return true if the information is handle, false otherwise
     */
    public abstract <I> boolean isInstanceOf(I info);

    /**
     * Create an {@link DataSourceAdapter} corresponding to the controller, with the appropriate {@link AbstractAdapter}
     * 
     * @param source the {@link AbstractDataSource} the adapter have to handle
     * @return the appropriate adapter
     */
    protected abstract DataSourceAdapter<U, T> createAdapter(AbstractDataSource<U> source);

    /**
     * Cast the information into a type handle by this controller
     * 
     * @param <I> the type of the information
     * @param info the information sent
     * @return the casted information
     */
    public abstract <I> T getCastedInfo(I info);

    /**
     * Initialize the signature attribute of the concrete controller with the {@link TargetSignature} linked to the
     * {@link ITarget} type treated by this controller.
     * 
     * @return the {@link TargetSignature} associated with the current {@link ITarget}
     * @throws UnhandledTargetSignatureException
     */
    protected abstract TargetSignature<T> initSignature() throws UnhandledTargetSignatureException;

    /**
     * @return the {@link GenericDescriptor} corresponding to the target type
     */
    public abstract GenericDescriptor getTargetType();

}
