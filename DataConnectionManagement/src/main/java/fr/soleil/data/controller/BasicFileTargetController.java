/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import java.io.File;

import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IFileTarget;
import fr.soleil.data.target.ITarget;

/**
 * A {@link BasicTargetController} specialized in {@link File} {@link AbstractDataSource}s and
 * {@link IFileTarget}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BasicFileTargetController extends BasicTargetController<File> {

    public BasicFileTargetController() {
        super();
    }

    @Override
    protected File generateDefaultValue() {
        return new File(".");
    }

    @Override
    protected void setDataToTarget(ITarget target, File data, AbstractDataSource<File> source) {
        ((IFileTarget) target).setFile(data);
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return (data instanceof File);
    }

    @Override
    public boolean isCompatibleWith(ITarget target) {
        return (target instanceof IFileTarget);
    }

}
