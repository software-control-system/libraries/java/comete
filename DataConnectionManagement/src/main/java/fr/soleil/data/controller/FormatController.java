/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.controller;

import fr.soleil.data.service.FormatHelper;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IFormatableTarget;
import fr.soleil.data.target.IMultiFormatableTarget;
import fr.soleil.data.target.ITarget;
import fr.soleil.lib.project.ObjectUtils;

public class FormatController extends BasicTargetController<String> {

    public FormatController() {
        super();
    }

    @Override
    public boolean isTargetAssignable(ITarget target) {
        return isCompatibleWith(target);
    }

    @Override
    public boolean isCompatibleWith(ITarget target) {
        return (target instanceof IFormatableTarget) || (target instanceof IMultiFormatableTarget);
    }

    @Override
    public GenericDescriptor getSourceType() {
        return new GenericDescriptor(String.class);
    }

    @Override
    protected String generateDefaultValue() {
        return ObjectUtils.EMPTY_STRING;
    }

    @Override
    protected void setDataToTarget(ITarget target, String data, AbstractDataSource<String> source) {
        if (target instanceof IFormatableTarget) {
            FormatHelper.setFormatToTarget((IFormatableTarget) target, data);
        } else if (target instanceof IMultiFormatableTarget) {
            FormatHelper.setFormatToTarget((IMultiFormatableTarget) target, source == null ? null : source.toString(),
                    data);
        }
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return data instanceof String;
    }

}
