/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.event;

import java.util.EventObject;

import fr.soleil.data.service.thread.AbstractRefreshingThread;

/**
 * An {@link EventObject} used to notify that an {@link AbstractRefreshingThread} did a refreshment step
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class RefreshEvent extends EventObject {

    private static final long serialVersionUID = -6515301724711570021L;

    public RefreshEvent(AbstractRefreshingThread<?> source) {
        super(source);
    }

    @Override
    public AbstractRefreshingThread<?> getSource() {
        return (AbstractRefreshingThread<?>) super.getSource();
    }

}
