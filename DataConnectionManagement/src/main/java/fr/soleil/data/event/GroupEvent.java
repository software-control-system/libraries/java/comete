/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.event;

import java.util.EventObject;

import fr.soleil.data.service.AbstractRefreshingManager;

/**
 * An {@link EventObject} used to notify that a group was refreshed in an {@link AbstractRefreshingManager}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class GroupEvent extends EventObject {

    private static final long serialVersionUID = -9095936315443391578L;

    private final String groupName;
    private final String metaInformation;

    /**
     * Constructs a new {@link GroupEvent}
     * 
     * @param source The {@link AbstractRefreshingManager} at the origin of this event
     * @param groupName The group name
     * @param metaInformation The meta information about the group
     */
    public GroupEvent(AbstractRefreshingManager<?> source, String groupName, String metaInformation) {
        super(source);
        this.groupName = groupName;
        this.metaInformation = metaInformation;
    }

    @Override
    public AbstractRefreshingManager<?> getSource() {
        return (AbstractRefreshingManager<?>) super.getSource();
    }

    /**
     * Returns the group name
     * 
     * @return A {@link String}
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Returns the meta information about the group
     * 
     * @return A {@link String}
     */
    public String getMetaInformation() {
        return metaInformation;
    }

}
