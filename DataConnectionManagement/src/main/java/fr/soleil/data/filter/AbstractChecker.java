/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.filter;

public abstract class AbstractChecker {

    protected AbstractChecker upperChecker;

    /**
     * Default constructor
     */
    public AbstractChecker() {
        this(null);
    }

    /**
     * Constructs a new {@link AbstractChecker} which source is a given {@link AbstractChecker}
     * 
     * @param dataContainer The {@link AbstractChecker} that contains the source data to filter
     */
    public AbstractChecker(AbstractChecker dataContainer) {
        upperChecker = dataContainer;
    }

    /**
     * Associates an {@link AbstractChecker} with this {@link AbstractChecker}
     * 
     * @param dataContainer The {@link AbstractChecker} to set
     */
    public void setDataContainer(AbstractChecker dataContainer) {
        upperChecker = dataContainer;
    }

    /**
     * Returns the {@link AbstractChecker} this {@link AbstractChecker} decorates.
     * <p>
     * <big> Expert usage only</big>
     * </p>
     * 
     * @return An {@link AbstractChecker}
     */
    public AbstractChecker getDataContainer() {
        return upperChecker;
    }

    public boolean getInputDataCheck(Object toSource, Object data) {
        boolean result = isInputConfirmed(toSource, data);
        if (result && upperChecker != null) {
            result &= upperChecker.isInputConfirmed(toSource, data);
        }
        return result;
    }

    public boolean getOutputDataCheck(Object fromSource, Object data) {
        boolean result = isOutputConfirmed(fromSource, data);
        if (result && upperChecker != null) {
            result &= upperChecker.isOutputConfirmed(fromSource, data);
        }
        return result;
    }

    public abstract boolean isInputConfirmed(Object toSource, Object data);

    public abstract boolean isOutputConfirmed(Object fromSource, Object data);

    public enum CheckingMode {
        OUTPUT_MODE, INPUT_MODE
    };
}
