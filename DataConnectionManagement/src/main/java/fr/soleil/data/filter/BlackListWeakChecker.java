/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.filter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * An {@link AbstractBlackListChecker} that manages {@link WeakReference}s in black list
 * 
 * @param <T> The type of data managed by the {@link WeakReference}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BlackListWeakChecker<T> extends AbstractBlackListChecker<WeakReference<T>> {

    public BlackListWeakChecker() {
        super();
    }

    @Override
    public void addToBlackList(WeakReference<T> black) {
        super.addToBlackList(black);
        startCleaningThread();
    }

    @Override
    public void addAllToBlackList(Collection<? extends WeakReference<T>> toBlackList) {
        super.addAllToBlackList(toBlackList);
        startCleaningThread();
    }

    @Override
    public void removeFromBlackList(WeakReference<T> black) {
        super.removeFromBlackList(black);
        startCleaningThread();
    }

    @Override
    public boolean isBlackListed(WeakReference<T> toCheck) {
        boolean blackListed = super.isBlackListed(toCheck);
        startCleaningThread();
        return blackListed;
    }

    @Override
    public boolean isConfirmed(Object source, Object data) {
        boolean confirmed = super.isConfirmed(source, data);
        startCleaningThread();
        return confirmed;
    }

    @Override
    protected boolean matches(Object source, WeakReference<T> elem) {
        boolean result = false;
        if ((source != null) && (elem != null)) {
            result = source.equals(elem.get());
        }
        return result;
    }

    /**
     * Starts a new {@link Thread} to clean all dead {@link WeakReference}s in black list
     */
    protected void startCleaningThread() {
        new Thread(BlackListWeakChecker.this.getClass().getName() + " cleaning thread") {
            @Override
            public void run() {
                List<WeakReference<T>> toRemove = new ArrayList<WeakReference<T>>();
                synchronized (blackList) {
                    for (WeakReference<T> weak : blackList) {
                        if (weak.get() == null) {
                            toRemove.add(weak);
                        }
                    }
                    blackList.removeAll(toRemove);
                }
                blackList.clear();
            }
        }.start();
    }
}
