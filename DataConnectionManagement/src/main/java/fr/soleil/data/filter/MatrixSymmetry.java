/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.filter;

import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An {@link AbstractDataFilter} used to do some symmetry on matrix data
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MatrixSymmetry extends AbstractDataFilter<Object[]> {

    /**
     * Used to do a symmetry of horizontal axis
     */
    public static final int HORIZONTAL = 0;
    /**
     * Used to do a symmetry of vertical axis
     */
    public static final int VERTICAL = 1;

    protected int symmetryAxis;

    public MatrixSymmetry(int symetryAxis) {
        this(null, symetryAxis);
    }

    public MatrixSymmetry(IDataContainer<Object[]> dataContainer, int symmetryAxis) {
        super(dataContainer);
        this.symmetryAxis = symmetryAxis;
    }

    public int getSymetryAxis() {
        return symmetryAxis;
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Object[].class);
    }

    @Override
    protected Object[] filterSourceData(Object[] data) throws DataAdaptationException {
        return symmetry(data, symmetryAxis);
    }

    @Override
    protected Object[] unFilterDataForSource(Object[] data) throws DataAdaptationException {
        return symmetry(data, symmetryAxis);
    }

    public static Object[] symmetry(Object[] data, int symmetryAxis) throws DataAdaptationException {
        Object[] result;
        Class<?> targetType = ArrayUtils.recoverDataType(data);
        int[] shape = ArrayUtils.recoverShape(data);
        if (data == null) {
            result = null;
        } else if ((targetType != null) && (shape != null) && (shape.length == 2)) {
            try {
                result = ArrayUtils.matrixSymmetry(data, symmetryAxis == HORIZONTAL);
            } catch (Exception e) {
                throw new DataAdaptationException(MatrixSymmetry.class.getName() + ": failed to revert data", e);
            }
        } else {
            throw new DataAdaptationException(MatrixSymmetry.class.getName() + ": incompatible data");
        }
        return result;
    }
}
