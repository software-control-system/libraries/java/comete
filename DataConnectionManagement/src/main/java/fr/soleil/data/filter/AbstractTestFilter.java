/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.filter;

/**
 * This class represents something to filter some data. It uses the "decorator" design pattern.
 *
 * @author Rapha&euml;l GIRARDOT
 *
 * @param <T> The type of data managed by this {@link AbstractTestFilter}
 */
public abstract class AbstractTestFilter<T> extends AbstractDataFilter<T> {

    protected boolean targetTestPasses;

    @Override
    protected T unFilterDataForSource(T data) {
        targetTestPasses = checkTargetData(data);
        return data;
    }

    public abstract boolean checkTargetData(T data);

    @Override
    public boolean isSettable() {
        boolean result = true;
        if (dataContainer != null) {
            result = targetTestPasses && dataContainer.isSettable();
        }
        return result;
    }

}
