/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.filter;

import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.lib.project.data.AFlatData;
import fr.soleil.lib.project.data.FlatDataUtils;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An {@link AbstractDataFilter} used to do a right angle (positive or negative) rotation on a
 * matrix
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class FlatMatrixRightAngleRotation<F extends AFlatData<?>> extends AbstractDataFilter<F> {

    private final boolean positiveAngle;
    private final Class<? extends F> dataClass;

    /**
     * Default constructor. Rotation angle is considered as positive
     * 
     * @param dataClass The class that represents the type of data managed by this {@link FlatMatrixRightAngleRotation}.
     *            Mandatory for {@link #getDataType()}
     */
    public FlatMatrixRightAngleRotation(Class<? extends F> dataClass) {
        this(dataClass, null, true);
    }

    /**
     * Constructs a new {@link FlatMatrixRightAngleRotation} with a given rotation angle.
     * 
     * @param dataClass The class that represents the type of data managed by this {@link FlatMatrixRightAngleRotation}.
     *            Mandatory for {@link #getDataType()}
     * @param positiveAngle Whether rotation angle should be positive
     */
    public FlatMatrixRightAngleRotation(Class<? extends F> dataClass, boolean positiveAngle) {
        this(dataClass, null, positiveAngle);
    }

    /**
     * Constructs a new {@link FlatMatrixRightAngleRotation} which source is a given {@link IDataContainer}. Rotation
     * angle
     * is considered as positive
     * 
     * @param dataClass The class that represents the type of data managed by this {@link FlatMatrixRightAngleRotation}.
     *            Mandatory for {@link #getDataType()}
     * @param dataContainer The {@link IDataContainer} that contains the source data to filter
     */
    public FlatMatrixRightAngleRotation(Class<? extends F> dataClass, IDataContainer<F> dataContainer) {
        this(dataClass, dataContainer, true);
    }

    /**
     * Constructs a new {@link FlatMatrixRightAngleRotation} which source is a given {@link IDataContainer}, and a
     * rotation
     * angle.
     * 
     * @param dataClass The class that represents the type of data managed by this {@link FlatMatrixRightAngleRotation}.
     *            Mandatory for {@link #getDataType()}
     * @param dataContainer The {@link IDataContainer} that contains the source data to filter
     * @param positiveAngle Whether rotation angle should be positive
     */
    public FlatMatrixRightAngleRotation(Class<? extends F> dataClass, IDataContainer<F> dataContainer,
            boolean positiveAngle) {
        super(dataContainer);
        this.dataClass = dataClass;
        this.positiveAngle = positiveAngle;
    }

    /**
     * Returns whether rotation angle is positive
     * 
     * @return A <code>boolean</code>
     */
    public boolean isPositiveAngle() {
        return positiveAngle;
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(dataClass);
    }

    @Override
    protected F filterSourceData(F data) throws DataAdaptationException {
        return rotate(data, positiveAngle);
    }

    @Override
    protected F unFilterDataForSource(F data) throws DataAdaptationException {
        return rotate(data, !positiveAngle);
    }

    public static <F extends AFlatData<?>> F rotate(F data, boolean positiveAngle) throws DataAdaptationException {
        F result;
        if (data == null) {
            result = null;
        } else {
            Object value = data.getValue();
            Object rotated;
            Class<?> targetType = ArrayUtils.recoverDataType(value);
            int[] shape = data.getRealShape();
            if ((targetType != null) && (shape != null) && (shape.length == 2)) {
                rotated = ArrayUtils.flatMatrixRotation(value, shape[0], shape[1], positiveAngle);
                result = FlatDataUtils.buildNewFlatData(data, rotated, shape[1], shape[0]);
            } else {
                throw new DataAdaptationException(FlatMatrixRightAngleRotation.class.getName() + ": incompatible data");
            }
        }
        return result;
    }

}
