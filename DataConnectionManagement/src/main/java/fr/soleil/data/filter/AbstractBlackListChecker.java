/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.filter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * A {@link BasicChecker} that manages a black list
 * 
 * @param <T> The type of data contained in the black list
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AbstractBlackListChecker<T> extends BasicChecker {

    protected final Set<T> blackList;
    protected boolean forceAuthorization;

    /**
     * Default constructor
     */
    public AbstractBlackListChecker() {
        super();
        blackList = new HashSet<T>();
        forceAuthorization = false;
    }

    /**
     * Adds some data to black list
     * 
     * @param black The data to add to black list
     */
    public void addToBlackList(T black) {
        if (black != null) {
            synchronized (blackList) {
                blackList.add(black);
            }
        }
    }

    /**
     * Removes some data from black list
     * 
     * @param black The data to remove from black list
     */
    public void removeFromBlackList(T black) {
        if (black != null) {
            synchronized (blackList) {
                blackList.remove(black);
            }
        }
    }

    /**
     * Adds many data to black list
     * 
     * @param toBlackList The data to add to black list
     */
    public void addAllToBlackList(Collection<? extends T> toBlackList) {
        if (toBlackList != null) {
            synchronized (blackList) {
                blackList.addAll(toBlackList);
            }
        }
    }

    /**
     * Returns whether some given data is black listed
     * 
     * @param toCheck The data to check
     * @return A <code>boolean</code> value
     */
    public boolean isBlackListed(T toCheck) {
        boolean blackListed = false;
        if (toCheck != null) {
            synchronized (blackList) {
                blackListed = blackList.contains(toCheck);
            }
        }
        return blackListed;
    }

    @Override
    public boolean isConfirmed(Object source, Object data) {
        boolean confirmed = forceAuthorization || super.isConfirmed(source, data);
        if (confirmed) {
            for (T elem : blackList) {
                if (matches(source, elem)) {
                    confirmed = false;
                    break;
                }
            }
        }
        return confirmed;
    }

    /**
     * Returns whether a given {@link Object} matches some data
     * 
     * @param source The {@link Object}
     * @param elem The data
     * @return A <code>boolean</code> value
     */
    protected abstract boolean matches(Object source, T elem);

    /**
     * Returns whether authorization is forced
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isForceAuthorization() {
        return forceAuthorization;
    }

    /**
     * Sets whether to force authorization
     * 
     * @param forceAuthorization Whether to force authorization
     */
    public void setForceAuthorization(boolean forceAuthorization) {
        this.forceAuthorization = forceAuthorization;
    }

}
