/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.filter;

import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An {@link AbstractDataFilter} used to do a right angle (positive or negative) rotation on a
 * matrix
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MatrixRightAngleRotation extends AbstractDataFilter<Object[]> {

    private final boolean positiveAngle;

    /**
     * Default constructor. Rotation angle is considered as positive
     */
    public MatrixRightAngleRotation() {
        this(null, true);
    }

    /**
     * Constructs a new {@link MatrixRightAngleRotation} with a given rotation angle.
     * 
     * @param positiveAngle Whether rotation angle should be positive
     */
    public MatrixRightAngleRotation(boolean positiveAngle) {
        this(null, positiveAngle);
    }

    /**
     * Constructs a new {@link MatrixRightAngleRotation} which source is a given {@link IDataContainer}. Rotation angle
     * is considered as positive
     * 
     * @param dataContainer The {@link IDataContainer} that contains the source data to filter
     */
    public MatrixRightAngleRotation(IDataContainer<Object[]> dataContainer) {
        this(dataContainer, true);
    }

    /**
     * Constructs a new {@link MatrixRightAngleRotation} which source is a given {@link IDataContainer}, and a rotation
     * angle.
     * 
     * @param dataContainer The {@link IDataContainer} that contains the source data to filter
     * @param positiveAngle Whether rotation angle should be positive
     */
    public MatrixRightAngleRotation(IDataContainer<Object[]> dataContainer, boolean positiveAngle) {
        super(dataContainer);
        this.positiveAngle = positiveAngle;
    }

    /**
     * Returns whether rotation angle is positive
     * 
     * @return A <code>boolean</code>
     */
    public boolean isPositiveAngle() {
        return positiveAngle;
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Object[].class);
    }

    @Override
    protected Object[] filterSourceData(Object[] data) throws DataAdaptationException {
        return rotate(data, positiveAngle);
    }

    @Override
    protected Object[] unFilterDataForSource(Object[] data) throws DataAdaptationException {
        return rotate(data, !positiveAngle);
    }

    public static Object[] rotate(Object[] data, boolean positiveAngle) throws DataAdaptationException {
        Object[] result;
        Class<?> targetType = ArrayUtils.recoverDataType(data);
        int[] shape = ArrayUtils.recoverShape(data);
        if (data == null) {
            result = null;
        } else if ((targetType != null) && (shape != null) && (shape.length == 2)) {
            result = ArrayUtils.matrixRotation(data, positiveAngle);
        } else {
            throw new DataAdaptationException(MatrixRightAngleRotation.class.getName() + ": incompatible data");
        }
        return result;
    }

}
