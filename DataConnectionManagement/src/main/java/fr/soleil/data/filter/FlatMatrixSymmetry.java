/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.filter;

import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.lib.project.data.AFlatData;
import fr.soleil.lib.project.data.FlatDataUtils;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An {@link AbstractDataFilter} used to do some symmetry on matrix data
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class FlatMatrixSymmetry<F extends AFlatData<?>> extends AbstractDataFilter<F> {

    /**
     * Used to do a symmetry of horizontal axis
     */
    public static final int HORIZONTAL = 0;
    /**
     * Used to do a symmetry of vertical axis
     */
    public static final int VERTICAL = 1;

    protected int symmetryAxis;
    private final Class<? extends F> dataClass;

    public FlatMatrixSymmetry(Class<? extends F> dataClass, int symetryAxis) {
        this(dataClass, null, symetryAxis);
    }

    public FlatMatrixSymmetry(Class<? extends F> dataClass, IDataContainer<F> dataContainer, int symmetryAxis) {
        super(dataContainer);
        this.dataClass = dataClass;
        this.symmetryAxis = symmetryAxis;
    }

    public int getSymetryAxis() {
        return symmetryAxis;
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(dataClass);
    }

    @Override
    protected F filterSourceData(F data) throws DataAdaptationException {
        return symmetry(data, symmetryAxis);
    }

    @Override
    protected F unFilterDataForSource(F data) throws DataAdaptationException {
        return symmetry(data, symmetryAxis);
    }

    public static <F extends AFlatData<?>> F symmetry(F data, int symmetryAxis) throws DataAdaptationException {
        F result;
        if (data == null) {
            result = null;
        } else {
            Object value = data.getValue();
            Object symmetry;
            Class<?> targetType = ArrayUtils.recoverDataType(value);
            int[] shape = data.getRealShape();
            if ((targetType != null) && (shape != null) && (shape.length == 2)) {
                try {
                    symmetry = ArrayUtils.flatMatrixSymmetry(value, shape[0], shape[1], symmetryAxis == HORIZONTAL);
                    result = FlatDataUtils.buildNewFlatData(data, symmetry, shape.clone());
                } catch (Exception e) {
                    throw new DataAdaptationException(FlatMatrixSymmetry.class.getName() + ": failed to revert data",
                            e);
                }
            } else {
                throw new DataAdaptationException(FlatMatrixSymmetry.class.getName() + ": incompatible data");
            }
        }
        return result;
    }
}
