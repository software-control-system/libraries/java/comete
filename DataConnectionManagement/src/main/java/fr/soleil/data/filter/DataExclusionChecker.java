/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.filter;

import java.util.HashSet;
import java.util.Set;

import fr.soleil.lib.project.ObjectUtils;

public class DataExclusionChecker extends BasicChecker {

    protected Set<Object> excludedData;

    public DataExclusionChecker() {
        super();
        excludedData = new HashSet<Object>();
    }

    public boolean add(Object e) {
        return excludedData.add(e);
    }

    public boolean addAll(Set<Object> c) {
        return excludedData.addAll(c);
    }

    public boolean contains(Object o) {
        return excludedData.contains(o);
    }

    public boolean remove(Object o) {
        return excludedData.remove(o);
    }

    public boolean removeAll(Set<Object> c) {
        return excludedData.removeAll(c);
    }

    @Override
    public boolean isOutputConfirmed(Object fromSource, Object data) {
        boolean result = super.isOutputConfirmed(fromSource, data);
        if (result) {
            for (Object noData : excludedData) {
                if (isSameData(data, noData)) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    protected boolean isSameData(Object o1, Object o2) {
        return ObjectUtils.sameObject(o1, o2);
    }

}
