/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.filter;

import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.exception.DataAdaptationException;

/**
 * This class represents something to filter some data. It uses the "decorator" design pattern.
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <T> The type of data managed by this {@link AbstractDataFilter}
 */
public abstract class AbstractDataFilter<T> implements IDataContainer<T> {

    protected IDataContainer<T> dataContainer;

    /**
     * Default constructor
     */
    public AbstractDataFilter() {
        this(null);
    }

    /**
     * Constructs a new {@link AbstractDataFilter} which source is a given {@link IDataContainer}
     * 
     * @param dataContainer The {@link IDataContainer} that contains the source data to filter
     */
    public AbstractDataFilter(IDataContainer<T> dataContainer) {
        this.dataContainer = dataContainer;
    }

    /**
     * Associates an {@link IDataContainer} with this {@link AbstractDataFilter}
     * 
     * @param dataContainer The {@link IDataContainer} to set
     */
    public void setDataContainer(IDataContainer<T> dataContainer) {
        this.dataContainer = dataContainer;
    }

    /**
     * Returns the {@link IDataContainer} this {@link AbstractDataFilter} decorates.
     * <p>
     * <big> Expert usage only</big>
     * </p>
     * 
     * @return An {@link IDataContainer}
     */
    public IDataContainer<T> getDataContainer() {
        return dataContainer;
    }

    @Override
    public T getData() throws Exception {
        T data = null;
        if (dataContainer != null) {
            data = filterSourceData(dataContainer.getData());
        }
        return data;
    }

    @Override
    public void setData(T data) throws Exception {
        if ((dataContainer != null) && canUndoFilter()) {
            dataContainer.setData(unFilterDataForSource(data));
        }
    }

    /**
     * Filters a data that is supposed to come from decorated {@link IDataContainer}. This method is
     * called in {@link #getData()}.
     * 
     * @param data The data to filter
     * @return The filtered data
     * @see #getData()
     * @throws DataAdaptationException if a problem occurred while trying to filter source data
     */
    protected abstract T filterSourceData(T data) throws DataAdaptationException;

    /**
     * Applies a reversed filter to some data, so that the result can be set to parent
     * {@link IDataContainer}. This method is called in {@link #setData(Object)}, if possible.
     * 
     * @param data The data to which to apply reversed filter
     * @return The unfiltered data
     * @see #setData(Object)
     * @see #canUndoFilter()
     * @throws DataAdaptationException if a problem occurred while trying to unfilter source data
     */
    protected abstract T unFilterDataForSource(T data) throws DataAdaptationException;

    /**
     * This method is used to know whether this {@link AbstractDataFilter} is able to undo/reverse
     * its filter. This method is called in {@link #setData(Object)}
     * 
     * @return A boolean value. <code>TRUE</code> if this {@link AbstractDataFilter} can undo its
     *         filter, <code>FALSE</code> otherwise.
     * @see #setData(Object)
     */
    protected boolean canUndoFilter() {
        return true;
    }

    @Override
    public boolean isSettable() {
        boolean result = true;
        if (dataContainer != null) {
            result = dataContainer.isSettable();
        }
        return result;
    }

    @Override
    public boolean isUnsigned() {
        IDataContainer<T> dataContainer = this.dataContainer;
        return dataContainer == null ? false : dataContainer.isUnsigned();
    }
}
