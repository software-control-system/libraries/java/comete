/*
 * This file is part of DataConnectionManagement.
 * 
 * DataConnectionManagement is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * DataConnectionManagement is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with DataConnectionManagement. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.data.filter;

public class BasicChecker extends AbstractChecker {

    protected boolean allowed;

    public BasicChecker() {
        allowed = true;
    }

    @Override
    public boolean isInputConfirmed(Object toSource, Object data) {
        return isConfirmed(toSource, data);
    }

    @Override
    public boolean isOutputConfirmed(Object fromSource, Object data) {
        return isConfirmed(fromSource, data);
    }

    public boolean isConfirmed(Object source, Object data) {
        return allowed;
    }

    /**
     * @param allowed the allowed to set
     */
    public void setAllowed(boolean allowed) {
        this.allowed = allowed;
    }

    /**
     * @return the allowed
     */
    public boolean isAllowed() {
        return allowed;
    }

}
