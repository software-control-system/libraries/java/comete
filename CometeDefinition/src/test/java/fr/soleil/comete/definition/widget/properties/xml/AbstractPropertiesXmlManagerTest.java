/*******************************************************************************
 * Copyright (c) 2008-2023 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.definition.widget.properties.xml;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Random;

import org.junit.Test;

import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

public abstract class AbstractPropertiesXmlManagerTest {

    @Test
    public void propertiesToXmlTest() {
        // Test PlotProperties
        String xmlString = buildXmlString();
        String xmlString2 = null;
        try {
            xmlString2 = buildXmlString2(xmlString);
        }
        catch (XMLWarning e) {
            xmlString2 = null;
        }
        boolean xmlStable = ObjectUtils.sameObject(xmlString, xmlString2);
        System.out.println("=======================================");
        System.out.println(getPropertyClass() + " XML test:");
        System.out.println("---------------------------------------");
        System.out.println(xmlString);
        System.out.println("---------------------------------------");
        System.out.println("second version:");
        System.out.println(xmlString2);
        System.out.println("---------------------------------------");
        System.out.print("Is " + getPropertyName() + " XML String stable ? ");
        System.out.println(xmlStable);
        System.out.println("=======================================");
        assertThat(getPropertyClass() + " are not equal", true, equalTo(xmlStable));
    }

    protected abstract String getPropertyClass();

    protected abstract String getPropertyName();

    protected abstract String buildXmlString();

    protected abstract String buildXmlString2(String xmlString) throws XMLWarning;

    protected static CometeColor generateRandomColor() {
        return new CometeColor(new Random().nextInt(256), new Random().nextInt(256),
                new Random().nextInt(256), new Random().nextInt(256));
    }

    protected static CometeFont generateRandomFont() {
        return new CometeFont((Math.random() < 0.5 ? CometeFont.SANS_SERIF : "Dialog"),
                new Random().nextInt(20), new Random().nextInt(50));
    }

}
