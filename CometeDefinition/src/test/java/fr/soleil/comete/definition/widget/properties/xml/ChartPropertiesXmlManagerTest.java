/*******************************************************************************
 * Copyright (c) 2008-2023 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.definition.widget.properties.xml;

import java.util.Random;

import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.DragProperties;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

public class ChartPropertiesXmlManagerTest extends AbstractPropertiesXmlManagerTest {

    public ChartPropertiesXmlManagerTest() {
        super();
    }

    @Override
    protected String getPropertyClass() {
        return "ChartProperties";
    }

    @Override
    protected String getPropertyName() {
        return "Chart";
    }

    @Override
    protected String buildXmlString() {
        return ChartPropertiesXmlManager.toXmlString(generateRandomChartProperties());
    }

    @Override
    protected String buildXmlString2(String xmlString) throws XMLWarning {
        return ChartPropertiesXmlManager.toXmlString(
                ChartPropertiesXmlManager.loadChartProperties(XMLUtils.getRootNodeFromFileContent(xmlString)));
    }

    private static SamplingProperties generateRandomSamplingProperties() {
        return new SamplingProperties((Math.random() >= 0.5), new Random().nextInt(20), new Random().nextInt(20));
    }

    private static DragProperties generateRandomDragProperties() {
        return new DragProperties(new Random().nextInt(4) - 1);
    }

    private static AxisProperties generateRandomAxisProperties() {
        return new AxisProperties(Math.random() * 50, Math.random() * 50, new Random().nextInt(20),
                (Math.random() >= 0.5), new Random().nextInt(20), "Random axis", generateRandomColor(),
                new Random().nextInt(20), (Math.random() >= 0.5), (Math.random() >= 0.5), (Math.random() >= 0.5),
                (Math.random() >= 0.5), new Random().nextInt(20), Math.random() * 50, new Random().nextInt(20));
    }

    private static ChartProperties generateRandomChartProperties() {
        return new ChartProperties((Math.random() >= 0.5), new Random().nextInt(20), generateRandomColor(),
                generateRandomFont(), generateRandomFont(), "Random Chart", Math.random() * 50,
                new Random().nextInt(50), (Math.random() < 0.5 ? "*" : "/"), (Math.random() >= 0.5),
                (Math.random() >= 0.5), (Math.random() >= 0.5), generateRandomAxisProperties(),
                generateRandomAxisProperties(), generateRandomAxisProperties(), generateRandomSamplingProperties(),
                generateRandomDragProperties());
    }

}
