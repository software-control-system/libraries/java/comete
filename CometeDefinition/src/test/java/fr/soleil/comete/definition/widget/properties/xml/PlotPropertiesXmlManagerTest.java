/*******************************************************************************
 * Copyright (c) 2008-2023 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.definition.widget.properties.xml;

import java.util.Random;

import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.comete.definition.widget.properties.OffsetProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

public class PlotPropertiesXmlManagerTest extends AbstractPropertiesXmlManagerTest {

    public PlotPropertiesXmlManagerTest() {
        super();
    }

    @Override
    protected String getPropertyClass() {
        return "PlotProperties";
    }

    @Override
    protected String getPropertyName() {
        return "Plot";
    }

    @Override
    protected String buildXmlString() {
        return PlotPropertiesXmlManager.toXmlString(generateRandomPlotProperties());
    }

    @Override
    protected String buildXmlString2(String xmlString) throws XMLWarning {
        return PlotPropertiesXmlManager.toXmlString(
                PlotPropertiesXmlManager.loadPlotProperties(XMLUtils.getRootNodeFromFileContent(xmlString)));
    }

    private static BarProperties generateRandomBarProperties() {
        return new BarProperties(generateRandomColor(), new Random().nextInt(50), new Random().nextInt(20),
                new Random().nextInt(20));
    }

    private static CurveProperties generateRandomCurveProperties() {
        return new CurveProperties(generateRandomColor(), new Random().nextInt(50), new Random().nextInt(20),
                "Random Curve");
    }

    private static MarkerProperties generateRandomMarkerProperties() {
        return new MarkerProperties(generateRandomColor(), new Random().nextInt(50), new Random().nextInt(20),
                (Math.random() >= 0.5));
    }

    private static ErrorProperties generateRandomErrorProperties() {
        return new ErrorProperties(generateRandomColor(), (Math.random() >= 0.5));
    }

    private static TransformationProperties generateRandomTransformationProperties() {
        return new TransformationProperties(Math.random() * 50, Math.random() * 50, Math.random() * 50);
    }

    private static OffsetProperties generateRandomOffsetProperties() {
        return new OffsetProperties(Math.random() * 50);
    }

    private static InterpolationProperties generateRandomInterpolationProperties() {
        return new InterpolationProperties(new Random().nextInt(20), new Random().nextInt(20), Math.random() * 50,
                Math.random() * 50);
    }

    private static SmoothingProperties generateRandomSmoothingProperties() {
        return new SmoothingProperties(new Random().nextInt(20), new Random().nextInt(10), Math.random() * 50,
                new Random().nextInt(20));
    }

    private static MathProperties generateRandomMathProperties() {
        return new MathProperties(new Random().nextInt(20));
    }

    private static PlotProperties generateRandomPlotProperties() {
        int a = new Random().nextInt(4), b = new Random().nextInt(10);
        String format = new StringBuilder().append('%').append(a + b).append('.').append(b)
                .append(Math.random() > 0.5 ? 'e' : 'f').toString();
        String unit = Math.random() > 0.5 ? "No unit" : ObjectUtils.EMPTY_STRING;
        return new PlotProperties(new Random().nextInt(10), new Random().nextInt(10), format, unit,
                generateRandomBarProperties(), generateRandomCurveProperties(), generateRandomMarkerProperties(),
                generateRandomErrorProperties(), generateRandomTransformationProperties(),
                generateRandomOffsetProperties(), generateRandomInterpolationProperties(),
                generateRandomSmoothingProperties(), generateRandomMathProperties());
    }

}
