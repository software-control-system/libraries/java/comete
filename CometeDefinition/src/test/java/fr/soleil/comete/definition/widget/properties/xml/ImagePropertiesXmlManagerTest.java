/*******************************************************************************
 * Copyright (c) 2008-2023 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.definition.widget.properties.xml;

import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

public class ImagePropertiesXmlManagerTest extends AbstractPropertiesXmlManagerTest {

    public ImagePropertiesXmlManagerTest() {
        super();
    }

    @Override
    protected String getPropertyClass() {
        return "ImageProperties";
    }

    @Override
    protected String getPropertyName() {
        return "Image";
    }

    @Override
    protected String buildXmlString() {
        return ImagePropertiesXmlManager.toXmlString(generateRandomImageProperties());
    }

    @Override
    protected String buildXmlString2(String xmlString) throws XMLWarning {
        return ImagePropertiesXmlManager.toXmlString(ImagePropertiesXmlManager
                .loadImageProperties(XMLUtils.getRootNodeFromFileContent(xmlString)));
    }

    private static ImageProperties generateRandomImageProperties() {
        Gradient gradient = new Gradient();
        if (Math.random() >= 0.5) {
            gradient.buildRainbowGradient();
        }
        return new ImageProperties((Math.random() >= 0.5), (Math.random() >= 0.5), Math.random(),
                Math.random(), Math.random() * 4, new String[] { "abgjk;hjklmhjk",
                        "QNjklhjKLHJ\",,kl", "46541qs3dqs134" }, gradient, (Math.random() >= 0.5));
    }

}
