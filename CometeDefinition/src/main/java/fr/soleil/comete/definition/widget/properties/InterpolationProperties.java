/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

public class InterpolationProperties implements IDataViewProperties, Cloneable, Serializable {

    private static final long serialVersionUID = -2208913691688691454L;

    private int interpolationMethod;
    private int interpolationStep;
    private double hermiteBias;
    private double hermiteTension;

    public InterpolationProperties() {
        this(0, 10, 0, 0);
    }

    public InterpolationProperties(int interpolationMethod, int interpolationStep, double hermiteBias,
            double hermiteTension) {
        super();
        setInterpolationMethod(interpolationMethod);
        setInterpolationStep(interpolationStep);
        setHermiteBias(hermiteBias);
        setHermiteTension(hermiteTension);
    }

    public int getInterpolationMethod() {
        return interpolationMethod;
    }

    public void setInterpolationMethod(int interpolationMethod) {
        this.interpolationMethod = interpolationMethod;
    }

    public int getInterpolationStep() {
        return interpolationStep;
    }

    public void setInterpolationStep(int step) {
        interpolationStep = step;
    }

    public double getHermiteBias() {
        return hermiteBias;
    }

    public void setHermiteBias(double hermiteBias) {
        this.hermiteBias = hermiteBias;
    }

    public double getHermiteTension() {
        return hermiteTension;
    }

    public void setHermiteTension(double hermiteTension) {
        this.hermiteTension = hermiteTension;
    }

    @Override
    public InterpolationProperties clone() {
        InterpolationProperties properties;
        try {
            properties = (InterpolationProperties) super.clone();
        } catch (CloneNotSupportedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            properties = null;
        }
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && getClass().equals(obj.getClass())) {
            InterpolationProperties properties = (InterpolationProperties) obj;
            equals = ((getInterpolationMethod() == properties.getInterpolationMethod())
                    && (getInterpolationStep() == properties.getInterpolationStep())
                    && ObjectUtils.sameDouble(getHermiteBias(), properties.getHermiteBias())
                    && ObjectUtils.sameDouble(getHermiteTension(), properties.getHermiteTension()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 123456789;
        int mult = 987;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + getInterpolationMethod();
        hashCode = hashCode * mult + getInterpolationStep();
        hashCode = hashCode * mult + (int) getHermiteBias();
        hashCode = hashCode * mult + (int) getHermiteTension();
        return hashCode;
    }

}
