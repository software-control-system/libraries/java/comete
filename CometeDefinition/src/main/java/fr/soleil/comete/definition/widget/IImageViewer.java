/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import fr.soleil.comete.definition.data.target.IBeamPointTarget;
import fr.soleil.comete.definition.data.target.matrix.IBooleanMatrixComponent;
import fr.soleil.comete.definition.data.target.matrix.INumberMatrixComponent;
import fr.soleil.comete.definition.data.target.scalar.IEditableComponent;
import fr.soleil.comete.definition.data.target.scalar.IFormatableComponent;
import fr.soleil.comete.definition.listener.IImageViewerListener;
import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.comete.definition.widget.util.IRoi;
import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MaskHandler;
import fr.soleil.comete.definition.widget.util.MathematicSector;
import fr.soleil.data.target.scalar.ITextTarget;

public interface IImageViewer extends IBeamPointTarget, ITextTarget, IFormatableComponent, INumberMatrixComponent,
        IBooleanMatrixComponent, IEditableComponent, ISnapshotableComponent, IDataSavableComponent, MaskHandler {

    /**
     * @return the image width
     */
    public int getImageWidth();

    /**
     * @return the image height
     */
    public int getImageHeight();

    /**
     * Returns the name of this {@link IImageViewer}'s image
     * 
     * @return A {@link String}
     */
    public String getImageName();

    /**
     * Sets the name of this {@link IImageViewer}'s image
     * 
     * @param name The name to set
     */
    public void setImageName(String name);

    public void addImageViewerListener(IImageViewerListener listener);

    public void removeImageViewerListener(IImageViewerListener listener);

    /**
     * Returns the {@link Gradient} this {@link IImageViewer} uses
     * 
     * @return A {@link Gradient}
     */
    public Gradient getGradient();

    /**
     * Sets the {@link Gradient} this {@link IImageViewer} should use
     * 
     * @param gradient The {@link Gradient} to set
     */
    public void setGradient(Gradient gradient);

    public boolean isUseMaskManagement();

    public void setUseMaskManagement(boolean useMask);

    /**
     * Loads a {@link Mask} from a file
     * 
     * @param path the path of the file
     * @param addToCurrentMask Whether to add loaded mask to current mask (or to replace current mask with loaded mask).
     */
    public void loadMask(String path, boolean addToCurrentMask);

    /**
     * Saves this viewer's {@link Mask} in a file
     * 
     * @param path the path of the file
     */
    public void saveMask(String path);

    /**
     * Returns whether this viewer will try to fit all available size when displaying an image
     * 
     * @return a boolean value
     */
    public boolean isAlwaysFitMaxSize();

    /**
     * Sets whether this viewer should try to fit all available size when displaying an image
     * 
     * @param alwaysFitMaxSize a boolean value
     */
    public void setAlwaysFitMaxSize(boolean alwaysFitMaxSize);

    public void setShowRoiInformationTable(boolean showTable);

    public boolean isShowRoiInformationTable();

    public boolean isDrawBeamPosition();

    public void setDrawBeamPosition(boolean drawBeam);

    /**
     * Registers a {@link MathematicSector} {@link Class} that can be used with this viewer, in
     * order to allow this viewer to display a list of known {@link MathematicSector} {@link Class}
     * 
     * @param sectorClass The {@link Class} to register
     */
    public void registerSectorClass(Class<?> sectorClass);

    public boolean isUseSectorManagement();

    public void setUseSectorManagement(boolean useSector);

    public MathematicSector getSector();

    public void setSector(MathematicSector sector);

    public IValueConvertor getXAxisConvertor();

    public void setXAxisConvertor(IValueConvertor xAxisConvertor);

    public IValueConvertor getYAxisConvertor();

    public void setYAxisConvertor(IValueConvertor yAxisConvertor);

    /**
     * Returns whether "single" roi mode is used
     * 
     * @return a boolean value
     * @see #setSingleRoiMode(boolean)
     */
    public boolean isSingleRoiMode();

    /**
     * Sets "single" or "multi" roi mode. In "multi" roi mode, you can create as many Rois as you
     * want. In "single" roi mode, creating a new Roi will delete previous ones.
     * 
     * @param singleRoiMode <code>true</code> to switch to "single" roi mode
     */
    public void setSingleRoiMode(boolean singleRoiMode);

    /**
     * Returns whether this {@link IImagePlayer} cleans its content (mask, sector, etc...) when
     * setting its data (through {@link #setFlatNumberMatrix(Object, int, int)} or {@link #setNumberMatrix(Object[])}).
     * 
     * @return A <code>boolean</code> value. <code>TRUE</code> by default
     */
    public boolean isCleanOnDataSetting();

    /**
     * Sets whether this {@link IImagePlayer} should clean its content (mask, sector, etc...) when
     * setting its data (through {@link #setFlatNumberMatrix(Object, int, int)} or
     * 
     * @param clean A <code>boolean</code> value. <code>TRUE</code> to clean
     */
    public void setCleanOnDataSetting(boolean clean);

    /**
     * Add a IRoi on the component
     * 
     * @param IRoi
     */
    public void addRoi(IRoi roi);

    /**
     * Add a IRoi on the component
     * 
     * @param IRoi
     * @param centered, if true an OVAL shape will be centered on x y
     */
    public void addRoi(IRoi roi, boolean centered);

    /**
     * Returns the number format applied on X axis.
     * 
     * @return A {@link String}.
     */
    public String getXAxisFormat();

    /**
     * Sets the number format for X axis.
     * 
     * @param format The number format for X axis.
     */
    public void setXAxisFormat(String format);

    /**
     * Returns the number format applied on Y axis.
     * 
     * @return A {@link String}.
     */
    public String getYAxisFormat();

    /**
     * Sets the number format for Y axis.
     * 
     * @param format The number format for Y axis.
     */
    public void setYAxisFormat(String format);

    /**
     * Returns the color used to display a <code>NaN</code> value
     * 
     * @return A {@link CometeColor}
     */
    public CometeColor getNanColor();

    /**
     * Sets the color to use to display a <code>NaN</code> value
     * 
     * @param nanColor The {@link CometeColor} to use
     */
    public void setNanColor(CometeColor nanColor);

    /**
     * Returns the color used to display a <code>positive infinity</code> value
     * 
     * @return A {@link CometeColor}
     */
    public CometeColor getPositiveInfinityColor();

    /**
     * Sets the color to use to display a <code>positive infinity</code> value
     * 
     * @param nanColor The {@link CometeColor} to use
     */
    public void setPositiveInfinityColor(CometeColor positiveInfinityColor);

    /**
     * Returns the color used to display a <code>negative infinity</code> value
     * 
     * @return A {@link CometeColor}
     */
    public CometeColor getNegativeInfinityColor();

    /**
     * Sets the color to use to display a <code>negative infinity</code> value
     * 
     * @param nanColor The {@link CometeColor} to use
     */
    public void setNegativeInfinityColor(CometeColor negativeInfinityColor);

    /**
     * Returns this {@link IImageViewer}'s {@link ImageProperties}
     * 
     * @return some {@link ImageProperties}
     */
    public ImageProperties getImageProperties();

    /**
     * Applies some {@link ImageProperties} to this {@link IImageViewer}
     * 
     * @param properties The {@link ImageProperties} to apply
     */
    public void setImageProperties(ImageProperties properties);

}
