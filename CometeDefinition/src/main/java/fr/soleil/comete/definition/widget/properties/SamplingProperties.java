/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

public class SamplingProperties implements Cloneable, Serializable {

    private static final long serialVersionUID = -7711107606068781277L;

    protected boolean enabled;
    protected int mean;
    protected int peakType;

    public SamplingProperties() {
        this(true, IChartViewer.LOCAL_MEAN, IChartViewer.PEAK_TYPE_GREATEST);
    }

    public SamplingProperties(boolean enabled, int mean, int peakType) {
        super();
        setEnabled(enabled);
        setMean(mean);
        setPeakType(peakType);
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getMean() {
        return mean;
    }

    public void setMean(int mean) {
        this.mean = mean;
    }

    public int getPeakType() {
        return peakType;
    }

    public void setPeakType(int peakType) {
        this.peakType = peakType;
    }

    @Override
    public SamplingProperties clone() {
        SamplingProperties properties;
        try {
            properties = (SamplingProperties) super.clone();
        } catch (CloneNotSupportedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            properties = null;
        }
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (getClass().equals(obj.getClass())) {
            SamplingProperties properties = (SamplingProperties) obj;
            equals = (this.enabled == properties.enabled) && (this.mean == properties.mean)
                    && (this.peakType == properties.peakType);
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0x5A391;
        int mult = 0x126;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.generateHashCode(enabled);
        code = code * mult + mean;
        code = code * mult + peakType;
        return code;
    }
}
