/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import java.util.EventObject;

import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.comete.definition.widget.util.ITreeNode;

/**
 * An {@link EventObject} to warn for some changes in {@link ITreeNode}'s selection
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 */
public class TreeNodeSelectionEvent extends EventObject {

    private static final long serialVersionUID = -208223371592212883L;

    private boolean selected;
    private ITreeNode[] concernedNodes;

    /**
     * Constructor
     * 
     * @param source The ITree on which this event occured
     * @param selected Whether the concerned {@link ITreeNode}s became selected or deselected
     * @param concernedNodes The concerned {@link ITreeNode}s
     */
    public TreeNodeSelectionEvent(ITree source, boolean selected, ITreeNode... concernedNodes) {
        super(source);
        this.selected = selected;
        this.concernedNodes = concernedNodes;
    }

    @Override
    public ITree getSource() {
        return (ITree) super.getSource();
    }

    /**
     * Returns the {@link ITreeNode}s concerned by this event
     * 
     * @return An {@link ITreeNode} array
     */
    public ITreeNode[] getConcernedNodes() {
        return concernedNodes;
    }

    /**
     * Whether the concerned {@link ITreeNode}s became selected or deselected
     * 
     * @return A <code>boolean</code> value. <code>true</code> to notify for node selection.
     */
    public boolean isSelected() {
        return selected;
    }

}
