/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import java.net.URI;
import java.net.URISyntaxException;

import fr.soleil.lib.project.ObjectUtils;

public class CometeImage implements IImage {

    private URI location;

    public CometeImage(URI location) {
        this.location = location;
    }

    public CometeImage(String aPath) {
        if (aPath == null) {
            location = null;
        } else {
            try {
                location = new URI(aPath);
            } catch (URISyntaxException e) {
                location = null;
            }
        }
    }

    @Override
    public String getImagePath() {
        return (location == null ? null : location.toString());
    }

    @Override
    public URI getImageLocation() {
        return location;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && (obj.getClass().equals(getClass()))) {
            CometeImage image = (CometeImage) obj;
            equals = ObjectUtils.sameObject(getImageLocation(), image.getImageLocation());
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0x13A6E;
        int mult = 0xC03E7E;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getImageLocation());
        return code;
    }

}
