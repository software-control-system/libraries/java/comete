/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import java.lang.reflect.Constructor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IButton;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IComboBox;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.definition.widget.ILabel;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.definition.widget.IPlayer;
import fr.soleil.comete.definition.widget.IProgressBar;
import fr.soleil.comete.definition.widget.IScrollPane;
import fr.soleil.comete.definition.widget.ISlider;
import fr.soleil.comete.definition.widget.ITextArea;
import fr.soleil.comete.definition.widget.ITextField;
import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.matrix.IBooleanMatrixTarget;
import fr.soleil.data.target.matrix.ITextMatrixTarget;
import fr.soleil.lib.project.SystemUtils;

/**
 * 
 * @author SAINTIN
 * 
 *         Class factory to create a comete widget
 *         Set the package in which the widget are
 * 
 */

public class CometeWidgetFactory {

    // TODO Maybe use Mediator.LOGGER_ACCESS
    private static final Logger LOGGER = LoggerFactory.getLogger(CometeWidgetFactory.class.getName());
    private static final String COMPOSITE_CLASS = "org.eclipse.swt.widgets.Composite";
    private static Class<?> compositeClass = null;
    private static final String SWING_PACKAGE = "fr.soleil.comete.swing.";
    private static final String AWT_PACKAGE = "fr.soleil.comete.awt.";
    private static final String SWT_PACKAGE = "fr.soleil.comete.swt.";
    private static final String FX_PACKAGE = "fr.soleil.comete.fx.";

    private static final String IS_NOT_A_ICOMPONENT = " is not a IComponent";
    private static final String CANNOT_CREATE_AN_INSTANCE_OF = "Cannot create an instance of ";
    private static final String CREATE_NEW = "create new ";

    private static CometePackage currentPackage;

    static {
        String systemProperty = SystemUtils.getSystemProperty("CometePackage");
        if ((systemProperty != null) && !systemProperty.isEmpty()) {
            try {
                currentPackage = CometePackage.valueOf(systemProperty);
            } catch (Exception e) {
                currentPackage = CometePackage.SWING;
            }
        }
    }

    public static void setCurrentPackage(final CometePackage tmpCometePackage) {
        CometeWidgetFactory.currentPackage = tmpCometePackage;
    }

    public static CometePackage getCurrentPackage() {
        return currentPackage;
    }

    public static IComponent createNewComponent(final String packageName, final String className, final Object parent,
            final int style) {
        IComponent component = null;
        if ((packageName != null) && (className != null)) {
            String completeClassName = packageName + className;
            Class<?> objectClass = null;
            try {
                objectClass = Class.forName(completeClassName);
                if ((parent != null) && packageName.equals(CometePackage.SWT.getPackagePrefix())) {
                    if (compositeClass == null) {
                        compositeClass = Class.forName(COMPOSITE_CLASS);
                    }

                    if ((compositeClass != null) && compositeClass.isInstance(parent)) {
                        Class<?>[] constructClassArgin = new Class[] { compositeClass, int.class };
                        Object[] constructArgin = new Object[] { parent, style };
                        component = instantiateComponent(objectClass, constructClassArgin, constructArgin);
                    }
                } else {
                    // With no argument in constructor
                    component = instantiateComponent(objectClass, new Class[0], new Object[0]);

                }
            } catch (ClassNotFoundException e) {
                LOGGER.error(e.getMessage());
                // System.err.println("Cannot resolve the class " + completeClassName);
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to create new component", e);
            }
        }
        return component;
    }

    public static IComponent createNewComponent(final String packageName, final String className) {
        return createNewComponent(packageName, className, null, 0);
    }

    public static IComponent createNewComponent(final ComponentType componentType, final Object parent,
            final int style) {
        String packageName = getCurrentPackage().getPackagePrefix();
        String className = componentType.getClassName();
        return createNewComponent(packageName, className, parent, style);
    }

    public static IComponent createNewComponent(final ComponentType componentType) {
        return createNewComponent(componentType, null, 0);
    }

    private static IComponent instantiateComponent(final Class<?> objectClass, final Class<?>[] paramsClasses,
            final Object[] params) {
        IComponent component = null;
        Object object = null;
        if ((objectClass != null) && (params != null)) {
            try {
                Constructor<?> tmpConstructor = objectClass.getConstructor(paramsClasses);
                object = tmpConstructor.newInstance(params);
                if (object instanceof IComponent) {
                    component = (IComponent) object;
                } else {
                    LOGGER.warn(objectClass.getName() + IS_NOT_A_ICOMPONENT);
                }
            } catch (Exception e) {
                LOGGER.error(CANNOT_CREATE_AN_INSTANCE_OF + objectClass.getName());
            }
        }
        return component;
    }

    private static void logComponentCreation(Object object) {
        LOGGER.trace(CREATE_NEW + object.getClass().getName());
    }

    // Panel
    public static IPanel createPanel(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.PANEL, parent, style);
        IPanel panel = null;
        if (object instanceof IPanel) {
            logComponentCreation(object);
            panel = (IPanel) object;
        }
        return panel;
    }

    public static IPanel createPanel() {
        return createPanel(null, 0);
    }

    // Panel
    public static IScrollPane createScrollPane(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.SCROLLPANE, parent, style);
        IScrollPane panel = null;
        if (object instanceof IScrollPane) {
            logComponentCreation(object);
            panel = (IScrollPane) object;
        }
        return panel;
    }

    public static IScrollPane createScrollPane() {
        return createScrollPane(null, 0);
    }

    // Label
    public static ILabel createLabel(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.LABEL, parent, style);
        ILabel label = null;
        if (object instanceof ILabel) {
            logComponentCreation(object);
            label = (ILabel) object;
        }
        return label;
    }

    public static ILabel createLabel() {
        return createLabel(null, 0);
    }

    // TextArea
    public static ITextField createTextField(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.TEXTFIELD, parent, style);
        ITextField field = null;
        if (object instanceof ITextField) {
            logComponentCreation(object);
            field = (ITextField) object;
        }
        return field;
    }

    public static ITextArea createTextField() {
        return createTextArea(null, 0);
    }

    // TextArea
    public static ITextArea createTextArea(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.TEXTAREA, parent, style);
        ITextArea area = null;
        if (object instanceof ITextArea) {
            logComponentCreation(object);
            area = (ITextArea) object;
        }
        return area;
    }

    public static ITextArea createTextArea() {
        return createTextArea(null, 0);
    }

    // ComboBox
    public static IComboBox createComboBox(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.COMBOBOX, parent, style);
        IComboBox combo = null;
        if (object instanceof IComboBox) {
            logComponentCreation(object);
            combo = (IComboBox) object;
        }
        return combo;
    }

    public static IComboBox createComboBox() {
        return createComboBox(null, 0);
    }

    // StringMatrixTable
    public static ITextMatrixTarget createStringMatrixTable(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.STRINGMATRIXTABLE, parent, style);
        ITextMatrixTarget textMatrix = null;
        if (object instanceof ITextMatrixTarget) {
            logComponentCreation(object);
            textMatrix = (ITextMatrixTarget) object;
        }
        return textMatrix;
    }

    public static ITextMatrixTarget createStringMatrixTable() {
        return createStringMatrixTable(null, 0);
    }

    // BooleanMatrixTable
    public static IBooleanMatrixTarget createBooleanMatrixTable(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.BOOLEANMATRIXTABLE, parent, style);
        IBooleanMatrixTarget booleanMatrix = null;
        if (object instanceof IBooleanMatrixTarget) {
            logComponentCreation(object);
            booleanMatrix = (IBooleanMatrixTarget) object;
        }
        return booleanMatrix;
    }

    public static IBooleanMatrixTarget createBooleanMatrixTable() {
        return createBooleanMatrixTable(null, 0);
    }

    // Tree
    public static ITree createTree(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.TREE, parent, style);
        ITree tree = null;
        if (object instanceof ITree) {
            logComponentCreation(object);
            tree = (ITree) object;
        }
        return tree;
    }

    public static ITree createTree() {
        return createTree(null, 0);
    }

    // ChartViewer
    public static IChartViewer createChartViewer(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.CHARTVIEWER, parent, style);
        IChartViewer chartViewer = null;
        if (object instanceof IChartViewer) {
            chartViewer = (IChartViewer) object;
        }
        return chartViewer;
    }

    public static IChartViewer createChartViewer() {
        return createChartViewer(null, 0);
    }

    // ImageViewer
    public static IImageViewer createImageViewer(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.IMAGEVIEWER, parent, style);
        IImageViewer imageViewer = null;
        if (object instanceof IImageViewer) {
            logComponentCreation(object);
            imageViewer = (IImageViewer) object;
        }
        return imageViewer;
    }

    public static IImageViewer createImageViewer() {
        return createImageViewer(null, 0);
    }

    // Player
    public static IPlayer createPlayer(final Object parent, final int style) {
        IComponent object = createNewComponent(ComponentType.PLAYER, parent, style);
        IPlayer player = null;
        if (object instanceof IPlayer) {
            logComponentCreation(object);
            player = (IPlayer) object;
        }
        return player;
    }

    public static IPlayer createPlayer() {
        return createPlayer(null, 0);
    }

    // Button
    public static IButton createButton(Object parent, int style) {
        IComponent object = createNewComponent(ComponentType.BUTTON, parent, style);
        IButton button = null;
        if (object instanceof IButton) {
            logComponentCreation(object);
            button = (IButton) object;
        }
        return button;
    }

    public static IButton createButton() {
        return createButton(null, 0);
    }

    // Slider
    public static ISlider createSlider(Object parent, int style) {
        IComponent object = createNewComponent(ComponentType.SLIDER, parent, style);
        ISlider slider = null;
        if (object instanceof ISlider) {
            logComponentCreation(object);
            slider = (ISlider) object;
        }
        return slider;
    }

    public static ISlider createSlider() {
        return createSlider(null, 0);
    }

    // ProgressBar
    public static IProgressBar createProgressBar(Object parent, int style) {
        IComponent object = createNewComponent(ComponentType.PROGRESSBAR, parent, style);
        IProgressBar progressBar = null;
        if (object instanceof IProgressBar) {
            logComponentCreation(object);
            progressBar = (IProgressBar) object;
        }
        return progressBar;
    }

    public static IProgressBar createProgressBar() {
        return createProgressBar(null, 0);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * 
     * Enumeration of the existing implemented package
     * SWING
     * AWT
     * SWT
     * FX
     */
    public static enum CometePackage {
        SWING(SWING_PACKAGE), AWT(AWT_PACKAGE), SWT(SWT_PACKAGE), FX(FX_PACKAGE);

        private final String packagePrefix;

        CometePackage(final String name) {
            packagePrefix = name;
        }

        public String getPackagePrefix() {
            return packagePrefix;
        }
    };

    public static enum ComponentType {
        FRAME("Frame"), SCROLLPANE("ScrollPane"), PANEL("Panel"), BUTTON("Button"), PROGRESSBAR("ProgressBar"),
        SLIDER("Slider"), LABEL("Label"), TEXTFIELD("TextField"), TEXTAREA("TextArea"), COMBOBOX("ComboBox"),
        STRINGMATRIXTABLE("StringMatrixTable"), BOOLEANMATRIXTABLE("BooleanMatrixTable"), TREE("Tree"),
        CHARTVIEWER("Chart"), IMAGEVIEWER("ImageViewer"), PLAYER("Player");

        private final String className;

        private ComponentType(final String name) {
            className = name;
        }

        public String getClassName() {
            return className;
        }

    }

}
