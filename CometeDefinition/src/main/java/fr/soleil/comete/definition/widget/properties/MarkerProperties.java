/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

public class MarkerProperties implements IDataViewProperties, Cloneable, Serializable {

    private static final long serialVersionUID = 646149491595894723L;

    private CometeColor color;
    private int size;
    private int style;
    private boolean legendVisible;
    private boolean samplingAllowed;

    public MarkerProperties() {
        this(CometeColor.RED);
    }

    public MarkerProperties(CometeColor color) {
        this(color, 6, 0, true, true);
    }

    public MarkerProperties(CometeColor color, int size, int style, boolean isLegendVisible) {
        this(color, size, style, isLegendVisible, true);
    }

    public MarkerProperties(CometeColor color, int size, int style, boolean isLegendVisible, boolean samplingAllowed) {
        super();
        setColor(color);
        setSize(size);
        setStyle(style);
        setLegendVisible(isLegendVisible);
        setSamplingAllowed(samplingAllowed);
    }

    /**
     * @return Returns the color.
     */
    public CometeColor getColor() {
        return color;
    }

    /**
     * @param color The color to set.
     */
    public void setColor(CometeColor color) {
        if (color == null) {
            color = CometeColor.RED;
        }
        this.color = color;
    }

    /**
     * @return Returns the isLegendVisible.
     */
    public boolean isLegendVisible() {
        return legendVisible;
    }

    /**
     * @param isLegendVisible The isLegendVisible to set.
     */
    public void setLegendVisible(boolean isLegendVisible) {
        this.legendVisible = isLegendVisible;
    }

    public boolean isSamplingAllowed() {
        return samplingAllowed;
    }

    public void setSamplingAllowed(boolean samplingAllowed) {
        this.samplingAllowed = samplingAllowed;
    }

    /**
     * @return Returns the size.
     */
    public int getSize() {
        return size;
    }

    /**
     * @param size The size to set.
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * @return Returns the style.
     */
    public int getStyle() {
        return style;
    }

    /**
     * @param style The style to set.
     */
    public void setStyle(int style) {
        this.style = style;
    }

    @Override
    public MarkerProperties clone() {
        MarkerProperties properties;
        try {
            properties = (MarkerProperties) super.clone();
            properties.color = (properties.color == null ? null : properties.color.clone());
        } catch (CloneNotSupportedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            properties = null;
        }
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && (getClass().equals(obj.getClass()))) {
            MarkerProperties properties = (MarkerProperties) obj;
            equals = (ObjectUtils.sameObject(getColor(), properties.getColor()) && (getSize() == properties.getSize())
                    && (getStyle() == properties.getStyle()) && (isLegendVisible() == properties.isLegendVisible())
                    && (isSamplingAllowed() == properties.isSamplingAllowed()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0x3A12;
        int mult = 654;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getColor());
        hashCode = hashCode * mult + getSize();
        hashCode = hashCode * mult + getStyle();
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isLegendVisible());
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isSamplingAllowed());
        return hashCode;
    }

}
