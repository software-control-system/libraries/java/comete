/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.target.complex;

import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.target.scalar.IScalarTarget;

/**
 * Note : this type of target can only be connected with CometeBox's controllers
 * 
 * @author HURIEZ
 */
public interface ITreeNodeTarget extends IScalarTarget {

    /**
     * Returns this {@link ITree}'s root node
     * 
     * @return An {@link ITreeNode}
     */
    public ITreeNode getRootNode();

    /**
     * Sets this {@link ITree}'s root node
     * 
     * @param rootNode The {@link ITreeNode} that should be root of this {@link ITree}
     */
    public void setRootNode(ITreeNode rootNode);

    /**
     * Returns the selected {@link ITreeNode}s
     * 
     * @return An {@link ITreeNode} array
     */
    public ITreeNode[] getSelectedNodes();

}
