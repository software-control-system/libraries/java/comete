/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.controller;

import java.net.URI;

import fr.soleil.comete.definition.data.target.IImageTarget;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.data.controller.BasicTargetController;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;

/**
 * A {@link BasicTargetController} compatible with {@link CometeImage} {@link AbstractDataSource}s
 * and {@link IImageTarget}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class CometeImageController extends BasicTargetController<CometeImage> {

    public CometeImageController() {
        super();
    }

    @Override
    protected CometeImage generateDefaultValue() {
        return new CometeImage((URI) null);
    }

    @Override
    protected void setDataToTarget(ITarget target, CometeImage data, AbstractDataSource<CometeImage> source) {
        if (isCompatibleWith(target)) {
            ((IImageTarget) target).setCometeImage(data);
        }
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return (data instanceof CometeImage);
    }

    @Override
    protected boolean isCompatibleWith(ITarget target) {
        return (target instanceof IImageTarget);
    }

}
