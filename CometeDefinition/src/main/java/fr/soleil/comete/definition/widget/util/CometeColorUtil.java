/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import fr.soleil.lib.project.ObjectUtils;

/**
 * Common color utilities.
 * 
 * package no.geosoft.cc.color.ui;
 * 
 * @author <a href="mailto:jacob.dreyer@geosoft.no">Jacob Dreyer</a>
 */
public class CometeColorUtil {

    public static final List<CometeColor> DEFAULT_COLORS = Collections.unmodifiableList(
            Arrays.asList(CometeColor.CYAN, CometeColor.GREEN, CometeColor.MAGENTA, CometeColor.ORANGE,
                    CometeColor.YELLOW, CometeColor.PINK, CometeColor.BLACK, CometeColor.RED, CometeColor.BLUE));

    private static List<CometeColor> colorVector;
    private static final Object COLOR_LOCK = new Object();
    private static int typeColor;

    /**
     * Blend two colors.
     * 
     * @param color1 First color to blend.
     * @param color2 Second color to blend.
     * @param ratio Blend ratio. 0.5 will give even blend, 1.0 will return color1, 0.0 will return color2 and so on.
     * @return Blended color.
     */
    public static CometeColor blend(CometeColor color1, CometeColor color2, double ratio) {
        int r = (int) ratio;
        int ir = (int) 1.0 - r;

        int rgb1[] = new int[] { color1.getRed(), color1.getGreen(), color1.getBlue() };
        int rgb2[] = new int[] { color2.getRed(), color2.getGreen(), color2.getBlue() };

        CometeColor color = new CometeColor(rgb1[0] * r + rgb2[0] * ir, rgb1[1] * r + rgb2[1] * ir,
                rgb1[2] * r + rgb2[2] * ir);

        return color;
    }

    /**
     * Make an even blend between two colors.
     * 
     * @param c1 First color to blend.
     * @param c2 Second color to blend.
     * @return Blended color.
     */
    public static CometeColor blend(CometeColor color1, CometeColor color2) {
        return CometeColorUtil.blend(color1, color2, 0.5);
    }

    /**
     * Make a color darker.
     * 
     * @param color Color to make darker.
     * @param fraction Darkness fraction.
     * @return Darker color.
     */
    public static CometeColor darker(CometeColor color, double fraction) {
        int red = (int) Math.round(color.getRed() * (1.0 - fraction));
        int green = (int) Math.round(color.getGreen() * (1.0 - fraction));
        int blue = (int) Math.round(color.getBlue() * (1.0 - fraction));
        if (red < 0) {
            red = 0;
        } else if (red > 255) {
            red = 255;
        }
        if (green < 0) {
            green = 0;
        } else if (green > 255) {
            green = 255;
        }
        if (blue < 0) {
            blue = 0;
        } else if (blue > 255) {
            blue = 255;
        }
        int alpha = color.getAlpha();
        return new CometeColor(red, green, blue, alpha);
    }

    /**
     * Make a color lighter.
     * 
     * @param color Color to make lighter.
     * @param fraction Darkness fraction.
     * @return Lighter color.
     */
    public static CometeColor lighter(CometeColor color, double fraction) {
        int red = (int) Math.round(color.getRed() * (1.0 + fraction));
        int green = (int) Math.round(color.getGreen() * (1.0 + fraction));
        int blue = (int) Math.round(color.getBlue() * (1.0 + fraction));
        if (red < 0) {
            red = 0;
        } else if (red > 255) {
            red = 255;
        }
        if (green < 0) {
            green = 0;
        } else if (green > 255) {
            green = 255;
        }
        if (blue < 0) {
            blue = 0;
        } else if (blue > 255) {
            blue = 255;
        }
        int alpha = color.getAlpha();
        return new CometeColor(red, green, blue, alpha);
    }

    /**
     * Make a color brighter.
     * 
     * @param color Color to make lighter.
     * @return Brighter color.
     */
    public static CometeColor brighter(CometeColor base) {
        int baseRGB = CometeColorUtil.getRGB(base);
        int[] rgb = { (baseRGB >> 16 & 0xFF), (baseRGB >> 8 & 0xFF), (baseRGB >> 0 & 0xFF) };
        boolean isChange = false;
        for (int i = 0; i < rgb.length; i++) {
            if (rgb[i] == 0) {
                rgb[i] += 150;
                isChange = true;
            }
        }
        if (!isChange) {
            for (int i = 0; i < rgb.length; i++) {
                rgb[i] = 255 - rgb[i] % 255 + 150;
                if (rgb[i] > 255) {
                    rgb[i] = 255;
                }
            }
        }
        return new CometeColor(rgb[0], rgb[1], rgb[2]);
    }

    /**
     * Return the hex name of a specified color.
     * 
     * @param color Color to get hex name of.
     * @return Hex name of color: "rrggbb".
     */
    public static String getHexName(CometeColor color) {
        int r = color.getRed();
        int g = color.getGreen();
        int b = color.getBlue();

        String rHex = Integer.toString(r, 16);
        String gHex = Integer.toString(g, 16);
        String bHex = Integer.toString(b, 16);

        return (rHex.length() == 2 ? ObjectUtils.EMPTY_STRING + rHex : "0" + rHex)
                + (gHex.length() == 2 ? ObjectUtils.EMPTY_STRING + gHex : "0" + gHex)
                + (bHex.length() == 2 ? ObjectUtils.EMPTY_STRING + bHex : "0" + bHex);
    }

    /**
     * Return the <code>int</code> corresponding to RGB in <code>int</code>.
     * 
     * @param the CometeColor.
     * @return <code>int</code> corresponding to RGB.
     */
    public static int getRGB(CometeColor color) {
        String hexName = getHexName(color);
        return hex2decimal(hexName);
    }

    /**
     * Return the <code>int</code> corresponding to a hexadecimal string.
     * 
     * @param the hexadecimal String.
     * @return <code>int</code> result.
     */
    public static int hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16 * val + d;
        }
        return val;
    }

    /**
     * Return the "distance" between two colors. The rgb entries are taken to be coordinates in a 3D space [0.0-1.0],
     * and this method returns the distance between the coordinates for the first and second color.
     * 
     * @param r1, g1, b1 First color.
     * @param r2, g2, b2 Second color.
     * @return Distance between colors.
     */
    public static double colorDistance(double r1, double g1, double b1, double r2, double g2, double b2) {
        double a = r2 - r1;
        double b = g2 - g1;
        double c = b2 - b1;

        return Math.sqrt(a * a + b * b + c * c);
    }

    /**
     * Return the "distance" between two colors.
     * 
     * @param color1 First color [r,g,b].
     * @param color2 Second color [r,g,b].
     * @return Distance between colors.
     */
    public static double colorDistance(double[] color1, double[] color2) {
        return CometeColorUtil.colorDistance(color1[0], color1[1], color1[2], color2[0], color2[1], color2[2]);
    }

    /**
     * Return the "distance" between two colors.
     * 
     * @param color1 First color.
     * @param color2 Second color.
     * @return Distance between colors.
     */
    public static double colorDistance(CometeColor color1, CometeColor color2) {
        float rgb1[] = new float[] { color1.getRed(), color1.getGreen(), color1.getBlue() };
        float rgb2[] = new float[] { color2.getRed(), color2.getGreen(), color2.getBlue() };

        return CometeColorUtil.colorDistance(rgb1[0], rgb1[1], rgb1[2], rgb2[0], rgb2[1], rgb2[2]);
    }

    /**
     * Check if a color is more dark than light. Useful if an entity of this color is to be labeled:
     * Use white label on a "dark" color and black label on a "light" color.
     * 
     * @param r,g,b Color to check.
     * @return True if this is a "dark" color, false otherwise.
     */
    public static boolean isDark(double r, double g, double b) {
        // Measure distance to white and black respectively
        double dWhite = CometeColorUtil.colorDistance(r, g, b, 1.0, 1.0, 1.0);
        double dBlack = CometeColorUtil.colorDistance(r, g, b, 0.0, 0.0, 0.0);

        return dBlack < dWhite;
    }

    /**
     * Check if a color is more dark than light. Useful if an entity of this color is to be labeled:
     * Use white label on a "dark" color and black label on a "light" color.
     * 
     * @param color Color to check.
     * @return True if this is a "dark" color, false otherwise.
     */
    public static boolean isDark(CometeColor color) {
        float r = color.getRed() / 255.0f;
        float g = color.getGreen() / 255.0f;
        float b = color.getBlue() / 255.0f;

        return isDark(r, g, b);
    }

    /**
     * Return a Color from colorVector that is always different (limited to around twenty). Useful to differentiate
     * curves.
     * 
     * @return a Color not used.
     */
    public static CometeColor getNewColor() {
        CometeColor color;
        synchronized (COLOR_LOCK) {
            if (colorVector == null) {
                typeColor = 0;
                initColor();
            }
            if (colorVector.isEmpty()) {
                colorVector = null;
                typeColor = typeColor + 1 % 3;
                initColor();
            }
            color = colorVector.remove(0);
        }
        return color;
    }

    public static CometeColor getMediumColor(CometeColor c1, CometeColor c2) {
        return new CometeColor((c1.getRed() + 3 * c2.getRed()) / 4, (c1.getGreen() + 3 * c2.getGreen()) / 4,
                (c1.getBlue() + 3 * c2.getBlue()) / 4);
    }

    /**
     * Initializes colorVector with basic Color, then darker and brighter Color.
     */
    private static void initColor() {
        colorVector = new ArrayList<>();
        for (CometeColor element : DEFAULT_COLORS) {
            switch (typeColor) {
                case 0:
                    colorVector.add(element);
                    break;
                case 1:
                    colorVector.add(darker(element, 0.4));
                    break;
                default:
                    colorVector.add(brighter(element));
                    break;
            }
        }
    }

}
