/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

import fr.soleil.comete.definition.event.TransferEvent;
import fr.soleil.comete.definition.event.TransferEvent.TransferReason;
import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.widget.IDataSavableComponent;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A class delegated to {@link TransferEvent} management for {@link IDataSavableComponent}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TransferEventDelegate {

    private final Set<IDataTransferListener> listeners;
    private final WeakReference<IDataSavableComponent> compRef;
    protected String dataDirectory;
    protected String datatFile;

    public TransferEventDelegate(IDataSavableComponent comp, String dataDirectory) {
        listeners = Collections.newSetFromMap(new WeakHashMap<IDataTransferListener, Boolean>());
        if (comp == null) {
            compRef = null;
        } else {
            compRef = new WeakReference<IDataSavableComponent>(comp);
        }
        this.dataDirectory = dataDirectory;
        datatFile = null;
    }

    public void addDataTransferListener(IDataTransferListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    public void removeDataTransferListener(IDataTransferListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    /**
     * Changes the last data file or directory location, once data was saved or loaded
     * 
     * @param location The new location to set
     */
    public void changeDataLocation(String location, boolean saved) {
        if ((location != null) && (!location.trim().isEmpty())) {
            File file = new File(location);
            if (file.exists()) {
                if (file.isDirectory()) {
                    datatFile = null;
                    dataDirectory = location;
                } else {
                    datatFile = location;
                    dataDirectory = file.getParentFile().getAbsolutePath();
                }
                warnListeners(saved ? TransferReason.SAVED : TransferReason.LOADED);
            }
        }
    }

    public String getDataDirectory() {
        return dataDirectory;
    }

    public void setDataDirectory(String dataDirectory) {
        if ((dataDirectory != null) && (!dataDirectory.trim().isEmpty())
                && (!ObjectUtils.sameObject(this.dataDirectory, dataDirectory))) {
            File temp = new File(dataDirectory);
            if (temp.isDirectory()) {
                this.datatFile = null;
                this.dataDirectory = dataDirectory;
                IDataSavableComponent component = getSavableComponent();
                if (component != null) {
                    warnListeners(TransferReason.PATH);
                }
            }
        }
    }

    public String getDataFile() {
        return datatFile;
    }

    protected IDataSavableComponent getSavableComponent() {
        return (compRef == null ? null : compRef.get());
    }

    public void warnListeners(TransferEvent.TransferReason reason) {
        if (reason != null) {
            IDataSavableComponent comp = (compRef == null ? null : compRef.get());
            if (comp != null) {
                TransferEvent event = new TransferEvent(comp, reason);
                if (event != null) {
                    List<IDataTransferListener> copy = new ArrayList<IDataTransferListener>();
                    synchronized (listeners) {
                        copy.addAll(listeners);
                    }
                    for (IDataTransferListener listener : copy) {
                        listener.dataTransfered(event);
                    }
                }
            }
        }
    }

}
