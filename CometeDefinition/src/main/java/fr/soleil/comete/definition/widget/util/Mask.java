/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import java.io.Serializable;

/**
 * Simple representation of a Mask.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public final class Mask implements Serializable {

    private static final long serialVersionUID = -5825993677987447411L;

    protected final int width;
    protected final int height;
    protected String name;
    protected long date;
    private final boolean[] value;

    /**
     * Constructor
     * 
     * @param mask The mask representation. Can't be <code>null</code>.
     * @param width The mask width. Can't be <code>&lt; 0</code>.
     * @param height The mask height. Can't be <code>&lt; 0</code>.
     * @param name Name of the Mask
     * @throws MaskException if <code>mask</code> is <code>null</code>, or if <code>width</code> or <code>height</code>
     *             is <code>&lt; 0</code>, or if <code>mask.length != width * height</code>.
     * @see #getValue()
     */
    public Mask(boolean[] mask, int width, int height, String name) throws MaskException {
        super();
        if (mask == null) {
            throw new MaskException("Mask can't be null");
        } else if ((width < 0) || (height < 0)) {
            throw new MaskException("Width and height can't be < 0");
        } else if (mask.length != width * height) {
            throw new MaskException("Invalid width and height");
        }
        setName(name);
        this.value = mask;
        this.width = width;
        this.height = height;
        date = System.currentTimeMillis();
    }

    /**
     * Returns the mask width
     * 
     * @return An <code>int</code>
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the mask height
     * 
     * @return An <code>int</code>
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the mask, represented as a boolean matrix.
     * <ul>
     * <li>If <code>true</code>, the corresponding pixel/value in an image is displayed/used</li>
     * <li>If <code>false</code>, the corresponding pixel/value in an image is NOT displayed/used</li>
     * </ul>
     * 
     * @return a not null boolean matrix.
     */
    public boolean[] getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns this Mask name
     * 
     * @return a String
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the date in milliseconds (generally, creation date).
     * 
     * @return a long
     */
    public long getDate() {
        return date;
    }

    /**
     * Sets a date in milliseconds for this Mask
     * 
     * @param date a long
     */
    public void setDate(long date) {
        this.date = date;
    }

}
