/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.DragProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and {@link DragProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DragPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "drag";

    public static final String DATAVIEW_OFFSET_DRAG_TYPE_PROPERTY_XML_TAG = "dataViewOffsetDragType";
    public static final String SCALE_DRAG_TYPE_PROPERTY_XML_TAG = "scaleDragType";
    public static final String SCALE_DRAG_SENSITIVITY_PROPERTY_XML_TAG = "scaleDragSensitivity";

    public static final String DRAG_NONE = "none";
    public static final String DRAG_X = "x";
    public static final String DRAG_Y = "y";
    public static final String DRAG_XY = "xy";

    /**
     * @deprecated USe {@link #DRAG_NONE} instead.
     */
    @Deprecated
    public static final String DRAG_OFFSET_NONE = DRAG_NONE;
    /**
     * @deprecated USe {@link #DRAG_X} instead.
     */
    @Deprecated
    public static final String DRAG_OFFSET_X = DRAG_X;
    /**
     * @deprecated USe {@link #DRAG_Y} instead.
     */
    @Deprecated
    public static final String DRAG_OFFSET_Y = DRAG_Y;
    /**
     * @deprecated USe {@link #DRAG_XY} instead.
     */
    @Deprecated
    public static final String DRAG_OFFSET_XY = DRAG_XY;

    protected static String dragTypeToString(int type) {
        String result;
        switch (type) {
            case IChartViewer.DRAG_X:
                result = DRAG_X;
                break;
            case IChartViewer.DRAG_Y:
                result = DRAG_Y;
                break;
            case IChartViewer.DRAG_XY:
                result = DRAG_XY;
                break;
            default:
                result = DRAG_NONE;
                break;
        }
        return result;
    }

    protected static int stringToDragType(String value, int defaultType) {
        int type;
        if (value == null) {
            type = defaultType;
        } else {
            value = value.trim();
            if (DRAG_X.equalsIgnoreCase(value)) {
                type = IChartViewer.DRAG_X;
            } else if (DRAG_Y.equalsIgnoreCase(value)) {
                type = IChartViewer.DRAG_Y;
            } else if (DRAG_XY.equalsIgnoreCase(value)) {
                type = IChartViewer.DRAG_XY;
            } else {
                type = IChartViewer.DRAG_NONE;
            }
        }
        return type;
    }

    /**
     * Transforms some {@link DragProperties} into an {@link XMLLine}
     * 
     * @param properties The {@link DragProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(DragProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttributeIfNotNull(DATAVIEW_OFFSET_DRAG_TYPE_PROPERTY_XML_TAG,
                    dragTypeToString(properties.getDataViewOffsetDragType()));
            openingLine.setAttributeIfNotNull(SCALE_DRAG_TYPE_PROPERTY_XML_TAG,
                    dragTypeToString(properties.getScaleDragType()));
            openingLine.setAttributeIfNotNull(SCALE_DRAG_SENSITIVITY_PROPERTY_XML_TAG,
                    Integer.toString(properties.getScaleDragSensitivity()));
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link DragProperties}
     * 
     * @param currentSubNode The {@link Node} to interpret
     * @return The recovered {@link DragProperties}
     */
    public static DragProperties loadDrag(Node currentSubNode) {
        return loadDragFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Map} to recover some {@link DragProperties}
     * 
     * @param propertyMap The {@link Map} to interpret. The {@link Map} is supposed to contain some xml node attributes
     * @return The recovered {@link DragProperties}
     */
    public static DragProperties loadDragFromXmlNodeAttributes(final Map<String, String> propertyMap) {
        DragProperties properties = new DragProperties();
        if (propertyMap != null) {
            String dataViewOffsetDragType_s = propertyMap.get(DATAVIEW_OFFSET_DRAG_TYPE_PROPERTY_XML_TAG);
            int dragType = stringToDragType(dataViewOffsetDragType_s, properties.getDataViewOffsetDragType());
            properties.setDataViewOffsetDragType(dragType);
            dragType = stringToDragType(propertyMap.get(SCALE_DRAG_TYPE_PROPERTY_XML_TAG),
                    properties.getScaleDragType());
            properties.setScaleDragType(dragType);
            int dragSensitivity = parseInt(propertyMap.get(SCALE_DRAG_SENSITIVITY_PROPERTY_XML_TAG),
                    properties.getScaleDragSensitivity());
            properties.setScaleDragSensitivity(dragSensitivity);
        }
        return properties;
    }

}
