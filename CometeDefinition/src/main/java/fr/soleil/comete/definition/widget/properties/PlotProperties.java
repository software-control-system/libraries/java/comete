/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

public class PlotProperties implements IDataViewProperties, Cloneable, Serializable {

    private static final long serialVersionUID = -1403933361761043871L;

    private int viewType;
    private int axisChoice;
    private String format;
    private String unit;

    private BarProperties bar;
    private CurveProperties curve;
    private MarkerProperties marker;
    private OffsetProperties xOffset;
    private TransformationProperties transform;
    private InterpolationProperties interpolation;
    private SmoothingProperties smoothing;
    private MathProperties math;
    private ErrorProperties error;

    public PlotProperties() {
        this(0, IChartViewer.Y1, null, ObjectUtils.EMPTY_STRING, new BarProperties(), new CurveProperties(),
                new MarkerProperties(), new ErrorProperties(), new TransformationProperties(), new OffsetProperties(),
                new InterpolationProperties(), new SmoothingProperties(), new MathProperties());
    }

    public PlotProperties(CometeColor color) {
        this(0, IChartViewer.Y1, null, ObjectUtils.EMPTY_STRING, new BarProperties(color), new CurveProperties(color),
                new MarkerProperties(color), new ErrorProperties(color), new TransformationProperties(),
                new OffsetProperties(), new InterpolationProperties(), new SmoothingProperties(), new MathProperties());
    }

    public PlotProperties(int viewType, int axisChoice, String format, String unit, BarProperties bar,
            CurveProperties curve, MarkerProperties marker, ErrorProperties errorProperties,
            TransformationProperties transform, OffsetProperties xTransform, InterpolationProperties interpolation,
            SmoothingProperties smoothing, MathProperties math) {
        super();
        setViewType(viewType);
        setAxisChoice(axisChoice);
        setFormat(format);
        setUnit(unit);
        setBar(bar);
        setCurve(curve);
        setMarker(marker);
        setError(errorProperties);
        setTransform(transform);
        setXOffset(xTransform);
        setInterpolation(interpolation);
        setSmoothing(smoothing);
        setMath(math);
    }

    /**
     * @return Returns the bar.
     */
    public BarProperties getBar() {
        return bar;
    }

    /**
     * @param bar The bar to set.
     */
    public void setBar(BarProperties bar) {
        this.bar = (bar == null ? new BarProperties() : bar);
    }

    /**
     * @return Returns the curve.
     */
    public CurveProperties getCurve() {
        return curve;
    }

    /**
     * @param curve The curve to set.
     */
    public void setCurve(CurveProperties curve) {
        this.curve = (curve == null ? new CurveProperties() : curve);
    }

    /**
     * @return Returns the transform.
     */
    public TransformationProperties getTransform() {
        return transform;
    }

    /**
     * @param transform The transform to set.
     */
    public void setTransform(TransformationProperties transform) {
        this.transform = (transform == null ? new TransformationProperties() : transform);
    }

    public OffsetProperties getXOffset() {
        return xOffset;
    }

    public void setXOffset(OffsetProperties xOffset) {
        this.xOffset = (xOffset == null ? new OffsetProperties() : xOffset);
    }

    /**
     * @return Returns the viewType.
     */
    public int getViewType() {
        return viewType;
    }

    /**
     * @param viewType The viewType to set.
     */
    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    /**
     * @return Returns the marker.
     */
    public MarkerProperties getMarker() {
        return marker;
    }

    /**
     * @param marker The marker to set.
     */
    public void setMarker(MarkerProperties marker) {
        this.marker = (marker == null ? new MarkerProperties() : marker);
    }

    public InterpolationProperties getInterpolation() {
        return interpolation;
    }

    public void setInterpolation(InterpolationProperties interpolation) {
        this.interpolation = (interpolation == null ? new InterpolationProperties() : interpolation);
    }

    public SmoothingProperties getSmoothing() {
        return smoothing;
    }

    public void setSmoothing(SmoothingProperties smoothing) {
        this.smoothing = (smoothing == null ? new SmoothingProperties() : smoothing);
    }

    public MathProperties getMath() {
        return math;
    }

    public void setMath(MathProperties math) {
        this.math = (math == null ? new MathProperties() : math);
    }

    /**
     * @return the error
     */
    public ErrorProperties getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(ErrorProperties error) {
        this.error = (error == null ? new ErrorProperties() : error);
    }

    /**
     * @return Returns the axisChoice.
     */
    public int getAxisChoice() {
        return axisChoice;
    }

    /**
     * @param axisChoice The axisChoice to set.
     */
    public void setAxisChoice(int axisChoice) {
        this.axisChoice = axisChoice;
    }

    /**
     * Returns the user format
     * 
     * @return A {@link String}
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the user format to use
     * 
     * @param format The user format to use
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * Returns the unit
     * 
     * @return A {@link String}
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets the unit to use
     * 
     * @param unit The unit to use
     */
    public void setUnit(String unit) {
        this.unit = unit == null ? ObjectUtils.EMPTY_STRING : unit;
    }

    @Override
    public PlotProperties clone() {
        PlotProperties plotProperties;
        try {
            plotProperties = (PlotProperties) super.clone();
            if (bar != null) {
                plotProperties.bar = bar.clone();
            }
            if (curve != null) {
                plotProperties.curve = curve.clone();
            }
            if (marker != null) {
                plotProperties.marker = marker.clone();
            }
            if (transform != null) {
                plotProperties.transform = transform.clone();
            }
            if (xOffset != null) {
                plotProperties.xOffset = xOffset.clone();
            }
            if (interpolation != null) {
                plotProperties.interpolation = interpolation.clone();
            }
            if (smoothing != null) {
                plotProperties.smoothing = smoothing.clone();
            }
            if (math != null) {
                plotProperties.math = math.clone();
            }
            if (error != null) {
                plotProperties.error = error.clone();
            }
        } catch (CloneNotSupportedException e) {
            // Should never happpen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            plotProperties = null;
        }
        return plotProperties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && (getClass().equals(obj.getClass()))) {
            PlotProperties properties = (PlotProperties) obj;
            equals = ((getViewType() == properties.getViewType()) && (getAxisChoice() == properties.getAxisChoice())
                    && ObjectUtils.sameObject(getFormat(), properties.getFormat())
                    && ObjectUtils.sameObject(getUnit(), properties.getUnit())
                    && ObjectUtils.sameObject(getBar(), properties.getBar())
                    && ObjectUtils.sameObject(getCurve(), properties.getCurve())
                    && ObjectUtils.sameObject(getMarker(), properties.getMarker())
                    && ObjectUtils.sameObject(getTransform(), properties.getTransform())
                    && ObjectUtils.sameObject(getInterpolation(), properties.getInterpolation())
                    && ObjectUtils.sameObject(getSmoothing(), properties.getSmoothing())
                    && ObjectUtils.sameObject(getMath(), properties.getMath())
                    && ObjectUtils.sameObject(getError(), properties.getError()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0x9107;
        int mult = 1793;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + getViewType();
        hashCode = hashCode * mult + getAxisChoice();
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getFormat());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getUnit());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getBar());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getCurve());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getMarker());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getTransform());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getInterpolation());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getSmoothing());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getMath());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getError());
        return hashCode;
    }

}
