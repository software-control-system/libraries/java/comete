/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.WeakHashMap;

import fr.soleil.comete.definition.event.TreeNodeEvent;
import fr.soleil.comete.definition.event.TreeNodeEvent.NodeEventReason;
import fr.soleil.comete.definition.listener.ITreeNodeListener;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A basic and project type independent implementation of {@link ITreeNode}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BasicTreeNode implements ITreeNode {

    protected final Collection<ITreeNodeListener> listeners;
    protected final List<ITreeNode> children;
    protected ITreeNode parent;
    protected String name;
    protected CometeImage image;
    protected Object data;
    protected final TargetDelegate delegate;
    protected String toolType;

    public BasicTreeNode() {
        listeners = Collections.newSetFromMap(new WeakHashMap<ITreeNodeListener, Boolean>());
        children = new ArrayList<ITreeNode>();
        delegate = new TargetDelegate();
    }

    @Override
    public ITreeNode getParent() {
        return parent;
    }

    @Override
    public void setParent(ITreeNode parent) {
        this.parent = parent;
    }

    @Override
    public List<ITreeNode> getChildren() {
        ArrayList<ITreeNode> copy = new ArrayList<ITreeNode>();
        synchronized (children) {
            copy.addAll(children);
        }
        return copy;
    }

    @Override
    public boolean isAncestorOf(ITreeNode node) {
        boolean ancestor;
        if (node == null) {
            ancestor = false;
        } else if (node == this) {
            ancestor = true;
        } else {
            ancestor = isAncestorOf(node.getParent());
        }
        return ancestor;
    }

    @Override
    public void addNodes(ITreeNode... nodes) {
        if ((nodes != null) && (nodes.length > 0)) {
            synchronized (children) {
                List<ITreeNode> toAdd = new ArrayList<ITreeNode>();
                for (ITreeNode node : nodes) {
                    if ((node != null) && (!children.contains(node) && (!node.isAncestorOf(this)))) {
                        toAdd.add(node);
                    }
                }
                if (toAdd.size() > 0) {
                    int[] indexes = new int[toAdd.size()];
                    int index = 0;
                    for (ITreeNode node : toAdd) {
                        if (node.getParent() != null) {
                            node.getParent().removeNodeTemporary(node);
                        }
                        // DATAREDUC-242: Added the possibility to specify index in node
                        int i = generateNewNodeIndex(node);
                        indexes[index++] = i;
                        if (i >= children.size()) {
                            children.add(node);
                        } else {
                            children.add(i, node);
                        }
                        node.setParent(this);
                    }
                    ITreeNode[] added = toAdd.toArray(new ITreeNode[toAdd.size()]);
                    toAdd.clear();
                    warnListeners(new TreeNodeEvent(this, NodeEventReason.INSERTED, added, indexes));
                }
            }
        }
    }

    /**
     * Computes index for a new child {@link ITreeNode}
     * 
     * @param node The {@link ITreeNode}
     * @return An <code>int</code>
     */
    protected int generateNewNodeIndex(ITreeNode node) {
        return children.size();
    }

    @Override
    public void addNode(ITreeNode node, int index) {
        if (node != null) {
            boolean canAdd = (!node.isAncestorOf(this));
            synchronized (children) {
                boolean contains = children.contains(node);
                if ((index < 0) || (index > children.size())) {
                    index = children.size();
                }
                if (contains) {
                    if (index == children.size()) {
                        index--;
                    }
                    if (children.indexOf(node) == index) {
                        canAdd = false;
                    }
                }
                if (canAdd) {
                    if (node.getParent() != null) {
                        node.getParent().removeNodeTemporary(node);
                    }
                    children.add(index, node);
                    node.setParent(this);
                }
                warnListeners(new TreeNodeEvent(this, NodeEventReason.INSERTED, new ITreeNode[] { node },
                        new int[] { index }));
            }
        }
    }

    @Override
    public void removeNodeTemporary(ITreeNode node) {
        synchronized (children) {
            removeNode(children.indexOf(node), true);
        }
    }

    @Override
    public void removeNode(ITreeNode node) {
        synchronized (children) {
            removeNode(children.indexOf(node), false);
        }
    }

    @Override
    public void removeNode(int index) {
        removeNode(index, false);
    }

    /**
     * Removes a child of this node, that corresponds to a given index
     * 
     * @param index The index of the child to remove
     * @param temporary Whether the child is temporary removed
     */
    protected void removeNode(int index, boolean temporary) {
        boolean removed = false;
        ITreeNode node = null;
        if (index > -1) {
            synchronized (children) {
                if (index < children.size()) {
                    node = children.remove(index);
                    node.setParent(null);
                    removed = true;
                }
            }
        }
        if (removed) {
            warnListeners(
                    new TreeNodeEvent(this, temporary ? NodeEventReason.REMOVED_TEMPORARY : NodeEventReason.REMOVED,
                            new ITreeNode[] { node }, new int[] { index }));
        }
    }

    @Override
    public ITreeNode getNodeAt(int index) {
        ITreeNode result = null;
        synchronized (children) {
            if ((index > -1) && (index < children.size())) {
                result = children.get(index);
            }
        }
        return result;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        if (!ObjectUtils.sameObject(this.name, name)) {
            this.name = name;
            warnListeners(new TreeNodeEvent(this, NodeEventReason.NAME));
        }
    }

    @Override
    public String getToolTip() {
        return toolType;
    }

    @Override
    public void setToolTip(String toolType) {
        this.toolType = toolType;
    }

    @Override
    public Object getData() {
        return data;
    }

    @Override
    public void setData(Object data) {
        if (!ObjectUtils.sameObject(data, this.data)) {
            this.data = data;
            warnListeners(new TreeNodeEvent(this, NodeEventReason.DATA));
        }
    }

    @Override
    public CometeImage getImage() {
        return image;
    }

    @Override
    public void setImage(CometeImage image) {
        if (!ObjectUtils.sameObject(image, this.image)) {
            this.image = image;
            warnListeners(new TreeNodeEvent(this, NodeEventReason.IMAGE));
        }
    }

    @Override
    public void addTreeNodeListener(ITreeNodeListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    @Override
    public void removeTreeNodeListener(ITreeNodeListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    @Override
    public void removeAllTreeNodeListeners() {
        synchronized (listeners) {
            listeners.clear();
        }
    }

    @Override
    public boolean isTreeNodeListenerRegistered(ITreeNodeListener listener) {
        boolean contains = false;
        if (listener != null) {
            synchronized (listeners) {
                contains = listeners.contains(listener);
            }
        }
        return contains;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    protected void warnListeners(TreeNodeEvent event) {
        if (event != null) {
            List<ITreeNodeListener> copy = new ArrayList<ITreeNodeListener>();
            synchronized (listeners) {
                copy.addAll(listeners);
            }
            for (ITreeNodeListener listener : copy) {
                listener.nodeChanged(event);
            }
            copy.clear();
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getClass().getName());
        builder.append("@").append(Integer.toHexString(hashCode())).append(":");
        builder.append("\n\t- ").append("name: ").append(getName());
        builder.append("\n\t- ").append("image: ").append(getImage());
        builder.append("\n\t- ").append("data: ").append(getData());
        builder.append("\n\t- ").append("children count: ").append(children.size());
        builder.append("\n\t- ").append("parent: ").append(parent == null ? null : parent.getName());
        return builder.toString();
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int size;
        synchronized (children) {
            size = children.size();
        }
        return size;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return 1;
    }
    // XXX equals and hashCode deactivated due to undesired data transmission bug (Mantis #26230)
    // @Override
    // public boolean equals(Object obj) {
    // boolean equals;
    // if (obj == null) {
    // equals = false;
    // } else if (obj == this) {
    // equals = true;
    // } else if (obj.getClass().equals(getClass())) {
    // BasicTreeNode node = (BasicTreeNode) obj;
    // equals = ObjectUtils.sameObject(getName(), node.getName())
    // && ObjectUtils.sameObject(getImage(), node.getImage())
    // && ObjectUtils.sameObject(getData(), node.getData());
    // } else {
    // equals = false;
    // }
    // return equals;
    // }
    //
    // @Override
    // public int hashCode() {
    // int code = 0xBA51C;
    // int mult = 0x20DE;
    // code = code * mult + getClass().hashCode();
    // code = code * mult + ObjectUtils.getHashCode(getName());
    // code = code * mult + ObjectUtils.getHashCode(getImage());
    // code = code * mult + ObjectUtils.getHashCode(getData());
    // return code;
    // }

}
