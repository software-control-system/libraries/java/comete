/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import java.util.EventObject;

import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MaskHandler;

public class MaskEvent extends EventObject {

    private static final long serialVersionUID = 7231444000527784380L;
    protected Mask value;

    public MaskEvent(MaskHandler source, Mask value) {
        super(source);
        this.value = value;
    }

    @Override
    public MaskHandler getSource() {
        return (MaskHandler) super.getSource();
    }

    public Mask getValue() {
        return value;
    }

}
