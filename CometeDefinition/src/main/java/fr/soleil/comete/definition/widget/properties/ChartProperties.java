/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

public class ChartProperties implements Cloneable, Serializable {

    private static final long serialVersionUID = -4682142136758002063L;

    public static final CometeColor DEFAULT_BACKGROUND = CometeColor.WHITE;
    public static final CometeFont DEFAULT_FONT = new CometeFont("Dialog", 0, 12);
    public static final String DEFAULT_NO_VALUE_STRING = "*";

    private boolean legendVisible;
    private int legendPlacement;
    private double legendProportion;
    private CometeColor backgroundColor;
    private CometeColor globalBackgroundColor;
    private CometeFont headerFont;
    private CometeFont labelFont;
    private String title;
    private double displayDuration;
    private int timePrecision;
    private String noValueString;
    private AxisProperties xAxisProperties;
    private AxisProperties y1AxisProperties;
    private AxisProperties y2AxisProperties;
    private SamplingProperties samplingProperties;
    private DragProperties dragProperties;
    private boolean autoHighlightOnLegend;
    private boolean limitDrawingZone;
    private boolean transparencyAllowed;

    public ChartProperties() {
        this(true, 0, DEFAULT_BACKGROUND, DEFAULT_FONT, DEFAULT_FONT, null, Double.POSITIVE_INFINITY, 0,
                DEFAULT_NO_VALUE_STRING, false, false, false, new AxisProperties(), new AxisProperties(),
                new AxisProperties(), new SamplingProperties(), new DragProperties());
        y1AxisProperties.setPosition(6);
        y2AxisProperties.setPosition(5);
        xAxisProperties.setPosition(1);
    }

    public ChartProperties(boolean legendVisible, int legendsPlacement, CometeColor backgroundColor,
            CometeFont headerFont, CometeFont labelFont, String title, double displayDuration, int timePrecision,
            String noValueString, boolean autoHighlightOnLegend, boolean limitDrawingZone, boolean transparencyAllowed,
            AxisProperties y1AxisProperties, AxisProperties y2AxisProperties, AxisProperties xAxisProperties,
            SamplingProperties samplingProperties, DragProperties dragProperties) {
        this(legendVisible, legendsPlacement, Double.NaN, backgroundColor, backgroundColor, headerFont, labelFont,
                title, displayDuration, timePrecision, noValueString, autoHighlightOnLegend, limitDrawingZone,
                transparencyAllowed, y1AxisProperties, y2AxisProperties, xAxisProperties, samplingProperties,
                dragProperties);
    }

    public ChartProperties(boolean legendVisible, int legendsPlacement, CometeColor backgroundColor,
            CometeColor globalBackgroundColor, CometeFont headerFont, CometeFont labelFont, String title,
            double displayDuration, int timePrecision, String noValueString, boolean autoHighlightOnLegend,
            boolean limitDrawingZone, boolean transparencyAllowed, AxisProperties y1AxisProperties,
            AxisProperties y2AxisProperties, AxisProperties xAxisProperties, SamplingProperties samplingProperties,
            DragProperties dragProperties) {
        this(legendVisible, legendsPlacement, Double.NaN, backgroundColor, globalBackgroundColor, headerFont, labelFont,
                title, displayDuration, timePrecision, noValueString, autoHighlightOnLegend, limitDrawingZone,
                transparencyAllowed, y1AxisProperties, y2AxisProperties, xAxisProperties, samplingProperties,
                dragProperties);
    }

    public ChartProperties(boolean legendVisible, int legendsPlacement, double legendProportion,
            CometeColor backgroundColor, CometeColor globalBackgroundColor, CometeFont headerFont, CometeFont labelFont,
            String title, double displayDuration, int timePrecision, String noValueString,
            boolean autoHighlightOnLegend, boolean limitDrawingZone, boolean transparencyAllowed,
            AxisProperties y1AxisProperties, AxisProperties y2AxisProperties, AxisProperties xAxisProperties,
            SamplingProperties samplingProperties, DragProperties dragProperties) {
        super();
        setLegendVisible(legendVisible);
        setLegendPlacement(legendsPlacement);
        setLegendProportion(legendProportion);
        setBackgroundColor(backgroundColor);
        setGlobalBackgroundColor(globalBackgroundColor);
        setHeaderFont(headerFont);
        setLabelFont(labelFont);
        setTitle(title);
        setDisplayDuration(displayDuration);
        setTimePrecision(timePrecision);
        setNoValueString(noValueString);
        setY1AxisProperties(y1AxisProperties);
        setY2AxisProperties(y2AxisProperties);
        setXAxisProperties(xAxisProperties);
        setSamplingProperties(samplingProperties);
        setDragProperties(dragProperties);
        setAutoHighlightOnLegend(autoHighlightOnLegend);
        setLimitDrawingZone(limitDrawingZone);
        setTransparencyAllowed(transparencyAllowed);
    }

    /**
     * Returns the chart background color.
     * 
     * @return A {@link CometeColor}.
     */
    public CometeColor getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * Sets the chart background color.
     * 
     * @param backgroundColor The chart background color to set.
     */
    public void setBackgroundColor(CometeColor backgroundColor) {
        if (backgroundColor == null) {
            backgroundColor = CometeColor.WHITE;
        }
        this.backgroundColor = backgroundColor;
    }

    /**
     * Returns the global background color (the background color of everything the panel that contains the curves
     * drawing zone).
     * 
     * @return A {@link CometeColor}.
     */
    public CometeColor getGlobalBackgroundColor() {
        return globalBackgroundColor;
    }

    /**
     * Sets the global background color (the background color of everything the panel that contains the curves
     * drawing zone).
     * 
     * @param globalBackgroundColor The global background color to set.
     */
    public void setGlobalBackgroundColor(CometeColor globalBackgroundColor) {
        this.globalBackgroundColor = globalBackgroundColor;
    }

    /**
     * Returns the display duration.
     * 
     * @return A <code>double</code>.
     */
    public double getDisplayDuration() {
        return displayDuration;
    }

    /**
     * Sets the display duration.
     * 
     * @param displayDuration The display duration to set.
     */
    public void setDisplayDuration(double displayDuration) {
        this.displayDuration = displayDuration;
    }

    /**
     * Returns the header (title) font.
     * 
     * @return A {@link CometeFont}.
     */
    public CometeFont getHeaderFont() {
        return headerFont;
    }

    /**
     * Sets the header (title) font.
     * 
     * @param headerFont The header (title) font to set.
     */
    public void setHeaderFont(CometeFont headerFont) {
        if (headerFont == null) {
            headerFont = DEFAULT_FONT;
        }
        this.headerFont = headerFont;
    }

    /**
     * Returns the label (legend) font.
     * 
     * @return A {@link CometeFont}.
     */
    public CometeFont getLabelFont() {
        return labelFont;
    }

    /**
     * Sets the label (legend) font.
     * 
     * @param labelFont The label (legend) font to set.
     */
    public void setLabelFont(CometeFont labelFont) {
        if (labelFont == null) {
            labelFont = DEFAULT_FONT;
        }
        this.labelFont = labelFont;
    }

    /**
     * Returns whether chart legend should be visible.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isLegendVisible() {
        return legendVisible;
    }

    /**
     * Sets whether chart legend should be visible.
     * 
     * @param legendVisible Whether chart legend should be visible.
     */
    public void setLegendVisible(boolean legendVisible) {
        this.legendVisible = legendVisible;
    }

    /**
     * @return Returns the legendPlacement.
     */
    public int getLegendPlacement() {
        return legendPlacement;
    }

    /**
     * @param legendPlacement The legendsPlacement to set.
     */
    public void setLegendPlacement(int legendPlacement) {
        this.legendPlacement = legendPlacement;
    }

    /**
     * @return Returns the legendsPlacement.
     * @deprecated Use {@link #getLegendPlacement()} instead.
     */
    @Deprecated
    public int getLegendsPlacement() {
        return getLegendPlacement();
    }

    /**
     * @param legendsPlacement The legendsPlacement to set.
     * @deprecated use {@link #setLegendPlacement(int)} instead.
     */
    @Deprecated
    public void setLegendsPlacement(int legendsPlacement) {
        setLegendPlacement(legendsPlacement);
    }

    /**
     * Returns the proportion of the legend in the chart, i.e. the amount of space in width or height <i>(depending on
     * {@link #getLegendPlacement()})</i> accorded to the legend.
     * 
     * @return A <code>double</code>: <code>0 &le; legendProportion &le; 1</code>, or <code>NaN</code>.
     */
    public double getLegendProportion() {
        return legendProportion;
    }

    /**
     * Sets the proportion of the legend in the chart, i.e. the amount of space in width or height <i>(depending on
     * {@link #getLegendPlacement()})</i> accorded to the legend.
     * 
     * @param legendProportion The legend proportion to set. Must be either a value respecting
     *            <code>0 &le; legendProportion &le; 1</code>, or <code>NaN</code>.
     *            Any invalid value will be considered as <code>NaN</code>.
     */
    public void setLegendProportion(double legendProportion) {
        if (legendProportion < 0 || legendProportion > 1) {
            this.legendProportion = Double.NaN;
        } else {
            this.legendProportion = legendProportion;
        }
    }

    /**
     * @return Returns the title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title to set.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public String getNoValueString() {
        return noValueString;
    }

    /**
     * Sets the {@link String} used te represent "no data" in chart output files.
     * 
     * @param noValueString a {@link String}
     */
    public void setNoValueString(String noValueString) {
        this.noValueString = (noValueString == null ? DEFAULT_NO_VALUE_STRING : noValueString);
    }

    public int getTimePrecision() {
        return timePrecision;
    }

    public void setTimePrecision(int timePrecision) {
        this.timePrecision = timePrecision;
    }

    public AxisProperties getXAxisProperties() {
        return xAxisProperties;
    }

    public void setXAxisProperties(AxisProperties xAxisProperties) {
        this.xAxisProperties = (xAxisProperties == null ? new AxisProperties() : xAxisProperties);
    }

    public AxisProperties getY1AxisProperties() {
        return y1AxisProperties;
    }

    public void setY1AxisProperties(AxisProperties y1AxisProperties) {
        this.y1AxisProperties = (y1AxisProperties == null ? new AxisProperties() : y1AxisProperties);
    }

    public AxisProperties getY2AxisProperties() {
        return y2AxisProperties;
    }

    public void setY2AxisProperties(AxisProperties y2AxisProperties) {
        this.y2AxisProperties = (y2AxisProperties == null ? new AxisProperties() : y2AxisProperties);
    }

    public SamplingProperties getSamplingProperties() {
        return samplingProperties;
    }

    public void setSamplingProperties(SamplingProperties samplingProperties) {
        this.samplingProperties = (samplingProperties == null ? new SamplingProperties() : samplingProperties);
    }

    public DragProperties getDragProperties() {
        return dragProperties;
    }

    public void setDragProperties(DragProperties dragProperties) {
        this.dragProperties = (dragProperties == null ? new DragProperties() : dragProperties);
    }

    public void setAutoHighlightOnLegend(boolean autoHighlightOnLegend) {
        this.autoHighlightOnLegend = autoHighlightOnLegend;
    }

    public boolean isAutoHighlightOnLegend() {
        return autoHighlightOnLegend;
    }

    public boolean isLimitDrawingZone() {
        return limitDrawingZone;
    }

    public void setLimitDrawingZone(boolean limitDrawingZone) {
        this.limitDrawingZone = limitDrawingZone;
    }

    public boolean isTransparencyAllowed() {
        return transparencyAllowed;
    }

    public void setTransparencyAllowed(boolean transparencyAllowed) {
        this.transparencyAllowed = transparencyAllowed;
    }

    @Override
    public ChartProperties clone() {
        ChartProperties properties;
        try {
            properties = (ChartProperties) super.clone();
            if (xAxisProperties != null) {
                properties.xAxisProperties = xAxisProperties.clone();
            }
            if (y1AxisProperties != null) {
                properties.y1AxisProperties = y1AxisProperties.clone();
            }
            if (y2AxisProperties != null) {
                properties.y2AxisProperties = y2AxisProperties.clone();
            }
            if (samplingProperties != null) {
                properties.samplingProperties = samplingProperties.clone();
            }
            if (dragProperties != null) {
                properties.dragProperties = dragProperties.clone();
            }
            properties.labelFont = (properties.labelFont == null ? null : properties.labelFont.clone());
            properties.headerFont = (properties.headerFont == null ? null : properties.headerFont.clone());
            properties.backgroundColor = (properties.backgroundColor == null ? null
                    : properties.backgroundColor.clone());
            properties.globalBackgroundColor = (properties.globalBackgroundColor == null ? null
                    : properties.globalBackgroundColor.clone());
        } catch (CloneNotSupportedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            properties = null;
        }
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && (getClass().equals(obj.getClass()))) {
            ChartProperties properties = (ChartProperties) obj;
            equals = ((isLegendVisible() == properties.isLegendVisible())
                    && (getLegendPlacement() == properties.getLegendPlacement())
                    && ObjectUtils.sameDouble(getLegendProportion(), properties.getLegendProportion())
                    && ObjectUtils.sameObject(getBackgroundColor(), properties.getBackgroundColor())
                    && ObjectUtils.sameObject(getGlobalBackgroundColor(), properties.getGlobalBackgroundColor())
                    && ObjectUtils.sameObject(getHeaderFont(), properties.getHeaderFont())
                    && ObjectUtils.sameObject(getLabelFont(), properties.getLabelFont())
                    && ObjectUtils.sameObject(getTitle(), properties.getTitle())
                    && ObjectUtils.sameDouble(getDisplayDuration(), properties.getDisplayDuration())
                    && (getTimePrecision() == properties.getTimePrecision())
                    && ObjectUtils.sameObject(getNoValueString(), properties.getNoValueString())
                    && ObjectUtils.sameObject(getXAxisProperties(), properties.getXAxisProperties())
                    && ObjectUtils.sameObject(getY1AxisProperties(), properties.getY1AxisProperties())
                    && ObjectUtils.sameObject(getY2AxisProperties(), properties.getY2AxisProperties())
                    && ObjectUtils.sameObject(getSamplingProperties(), properties.getSamplingProperties())
                    && ObjectUtils.sameObject(getDragProperties(), properties.getDragProperties())
                    && (isAutoHighlightOnLegend() == properties.isAutoHighlightOnLegend())
                    && (isLimitDrawingZone() == properties.isLimitDrawingZone())
                    && (isTransparencyAllowed() == properties.isTransparencyAllowed()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0xC11A127;
        int mult = 147;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isLegendVisible());
        hashCode = hashCode * mult + getLegendPlacement();
        hashCode = hashCode * mult + (int) getLegendProportion();
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getBackgroundColor());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getGlobalBackgroundColor());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getHeaderFont());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getLabelFont());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getTitle());
        hashCode = hashCode * mult + (int) getDisplayDuration();
        hashCode = hashCode * mult + getTimePrecision();
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getNoValueString());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getXAxisProperties());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getY1AxisProperties());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getY2AxisProperties());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getSamplingProperties());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getDragProperties());
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isAutoHighlightOnLegend());
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isLimitDrawingZone());
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isTransparencyAllowed());
        return hashCode;
    }
}
