/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

import java.io.File;

import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.definition.widget.util.CometeImage;

/**
 * An interface used to propose {@link IImageViewer} data importing/exporting
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IImageExporter {

    /**
     * Returns the {@link File} extension, without dot, used by this {@link IImageExporter}
     * (example: "txt")
     * 
     * @return A {@link String}
     */
    public String getFileExtension();

    /**
     * Returns the description of the {@link File}s managed by this {@link IImageExporter}, that can
     * be displayed in a file browser
     * 
     * @return A {@link String}
     */
    public String getFileDescription();

    /**
     * Returns the name of the {@link File} format used by this {@link IImageExporter}, so that an
     * {@link IImageViewer} can propose "export to xxx", where xxx is the format name
     * 
     * @return A {@link String}
     */
    public String getFormatName();

    /**
     * Returns the icon to use when proposing data export
     * 
     * @return A {@link CometeImage}
     */
    public CometeImage getExportIcon();

    /**
     * Returns the icon to use when proposing data import
     * 
     * @return A {@link CometeImage}
     */
    public CometeImage getImportIcon();

    /**
     * Exports an image of an {@link IImageViewer} to a {@link File}
     * 
     * @param imageViewer The {@link IImageViewer}
     * @param outputFile The {@link File} to which to export data
     */
    public void exportImage(IImageViewer imageViewer, File outputFile);

    /**
     * Imports an image from a {@link File} in an {@link IImageViewer}
     * 
     * @param imageViewer The {@link IImageViewer}
     * @param inputFile The {@link File} that contains the data to import
     */
    public void importImage(IImageViewer imageViewer, File inputFile);

}
