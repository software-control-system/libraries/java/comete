/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

public class SmoothingProperties implements IDataViewProperties, Cloneable, Serializable {

    private static final long serialVersionUID = -4796061559221879731L;

    private int method;
    private int neighbors;
    private double gaussSigma;
    private int extrapolation;

    public SmoothingProperties() {
        this(0, 3, 0.5, 2);
    }

    public SmoothingProperties(int method, int neighbors, double gaussSigma, int extrapolation) {
        super();
        this.method = method;
        this.neighbors = neighbors;
        this.gaussSigma = gaussSigma;
        this.extrapolation = extrapolation;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int smoothingMethod) {
        method = smoothingMethod;
    }

    public int getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(int smoothingNeighbors) {
        neighbors = smoothingNeighbors;
    }

    public double getGaussSigma() {
        return gaussSigma;
    }

    public void setGaussSigma(double smoothingGaussSigma) {
        gaussSigma = smoothingGaussSigma;
    }

    public int getExtrapolation() {
        return extrapolation;
    }

    public void setExtrapolation(int extrapolation) {
        this.extrapolation = extrapolation;
    }

    @Override
    public SmoothingProperties clone() {
        SmoothingProperties properties;
        try {
            properties = (SmoothingProperties) super.clone();
        } catch (CloneNotSupportedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            properties = null;
        }
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && (getClass().equals(obj.getClass()))) {
            SmoothingProperties properties = (SmoothingProperties) obj;
            equals = ((getMethod() == properties.getMethod()) && (getNeighbors() == properties.getNeighbors())
                    && ObjectUtils.sameDouble(getGaussSigma(), properties.getGaussSigma())
                    && (getExtrapolation() == properties.getExtrapolation()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0x830074;
        int mult = 159;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + getMethod();
        hashCode = hashCode * mult + getNeighbors();
        hashCode = hashCode * mult + (int) getGaussSigma();
        hashCode = hashCode * mult + getExtrapolation();
        return hashCode;
    }

}
