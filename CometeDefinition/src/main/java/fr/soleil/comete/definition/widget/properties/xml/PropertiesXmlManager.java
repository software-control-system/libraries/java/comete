/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

/**
 * A class that allows some interaction between XML {@link File}s and some properties
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class PropertiesXmlManager {

    protected static final String LINE_SEPARATOR = System.getProperty("line.separator");
    protected static final String COMMA = ",";
    protected static final String TRUE = "true";
    protected static final String NULL = "null";

    /**
     * Transforms a {@link CometeColor} into a {@link String} that can be
     * written in an XML file
     * 
     * @param color
     *            The {@link CometeColor} to transform
     * @return A {@link String}
     */
    public static String colorToString(CometeColor color) {
        String result = null;
        if (color != null) {
            StringBuilder builder = new StringBuilder();
            builder.append(color.getRed()).append(COMMA);
            builder.append(color.getGreen()).append(COMMA);
            builder.append(color.getBlue()).append(COMMA);
            builder.append(color.getAlpha());
            result = builder.toString();
        }
        return result;
    }

    /**
     * Interprets a {@link String} to create a {@link CometeColor}
     * 
     * @param in The {@link String} to interpret
     * @param defaultColor The default {@link CometeColor} to return.
     * @return A {@link CometeColor}. <code>defaultColor</code> if the {@link String} was <code>null</code> or if
     *         interpretation failed.
     */
    public static CometeColor stringToColor(String in, CometeColor defaultColor) {
        CometeColor color = defaultColor;
        if (in != null) {
            try {
                String[] values = in.split(COMMA);

                int r = 0, g = 0, b = 0, a = 255;

                if (values.length > 0) {
                    r = Integer.parseInt(values[0]);
                    if (values.length > 1) {
                        g = Integer.parseInt(values[1]);
                        if (values.length > 2) {
                            b = Integer.parseInt(values[2]);
                            if (values.length > 3) {
                                a = Integer.parseInt(values[3]);
                            }
                        }
                    }
                }
                color = new CometeColor(r, g, b, a);
            } catch (Exception e) {
                color = defaultColor;
            }
        }
        return color;
    }

    /**
     * Interprets a {@link String} to create a {@link CometeColor}
     * 
     * @param in The {@link String} to interpret
     * @return A {@link CometeColor}. Can be <code>null</code> if the {@link String} was <code>null</code> or if
     *         interpretation failed.
     */
    public static CometeColor stringToColor(String in) {
        return stringToColor(in, null);
    }

    /**
     * Transforms a {@link CometeFont} into a {@link String} that can be written in an XML file
     * 
     * @param font The {@link CometeFont} to transform
     * @return A {@link String}
     */
    public static String fontToString(CometeFont font) {
        String result = null;
        if (font != null) {
            StringBuilder ret = new StringBuilder();
            ret.append(font.getName()).append(COMMA);
            ret.append(font.getStyle()).append(COMMA);
            ret.append(font.getSize());
            result = ret.toString();
        }
        return result;
    }

    /**
     * Interprets a {@link String} to create a {@link CometeFont}
     * 
     * @param in The {@link String} to interpret.
     * @param defaultFont The default font to return.
     * @return A {@link CometeFont}. <code>defaultFont</code> if the {@link String} was <code>null</code> or if
     *         interpretation failed.
     */
    public static CometeFont stringToFont(String in, CometeFont defaultFont) {
        CometeFont result = defaultFont;
        if (in != null) {
            try {
                String[] values = in.split(COMMA);
                String name = CometeFont.DEFAULT_FONT.getName();
                int style = CometeFont.DEFAULT_FONT.getStyle();
                int size = CometeFont.DEFAULT_FONT.getSize();
                if (values.length > 0) {
                    name = values[0];
                    if (values.length > 1) {
                        style = Integer.parseInt(values[1]);
                        if (values.length > 2) {
                            size = Integer.parseInt(values[2]);
                        }
                    }
                }
                result = new CometeFont(name, style, size);
            } catch (Exception e) {
                result = defaultFont;
            }
        }
        return result;
    }

    /**
     * Interprets a {@link String} to create a {@link CometeFont}
     * 
     * @param in The {@link String} to interpret
     * @return A {@link CometeFont}. Can be <code>null</code> if the {@link String} was <code>null</code> or if
     *         interpretation failed.
     */
    public static CometeFont stringToFont(String in) {
        return stringToFont(in, null);
    }

    /**
     * Creates a {@link Map} that represents the attributes of an xml node
     * 
     * @param node The xml node
     * @return A {@link Map}. <code>null</code> if a problem occurred while reading node attributes
     */
    protected static Map<String, String> buildAttributeMap(Node node) {
        Map<String, String> propertyMap;
        try {
            propertyMap = XMLUtils.loadAttributes(node);
        } catch (DOMException e) {
            propertyMap = null;
        }
        return propertyMap;
    }

    /**
     * Parses an <code>int</code> from a {@link String}, returning a default value in scale of problem.
     * 
     * @param str The {@link String} to parse.
     * @param defaultValue The default value.
     * @return An <code>int</code>.
     */
    protected static int parseInt(String str, int defaultValue) {
        int result;
        try {
            result = Integer.parseInt(str.trim());
        } catch (Exception e) {
            result = defaultValue;
        }
        return result;
    }

    /**
     * Parses a <code>double</code> from a {@link String}, returning a default value in scale of problem.
     * 
     * @param str The {@link String} to parse.
     * @param defaultValue The default value.
     * @return A <code>double</code>.
     */
    protected static double parseDouble(String str, double defaultValue) {
        double result;
        try {
            result = Double.parseDouble(str.trim());
        } catch (Exception e) {
            result = defaultValue;
        }
        return result;
    }

    /**
     * Parses a <code>boolean</code> from a {@link String}, returning a default value in scale of problem.
     * 
     * @param str The {@link String} to parse.
     * @param defaultValue The default value.
     * @return A <code>boolean</code>.
     */
    protected static boolean parseBoolean(String str, boolean defaultValue) {
        boolean result;
        try {
            result = TRUE.equalsIgnoreCase(str.trim());
        } catch (Exception e) {
            result = defaultValue;
        }
        return result;
    }
}
