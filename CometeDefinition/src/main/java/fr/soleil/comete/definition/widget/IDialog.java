/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

public interface IDialog extends IComponent {

    /**
     * Type meaning Look and Feel should not supply any options -- only use the options from the
     */
    public static final int DEFAULT_OPTION = -1;
    /** Type used for <code>showConfirmDialog</code>. */
    public static final int YES_NO_OPTION = 0;
    /** Type used for <code>showConfirmDialog</code>. */
    public static final int YES_NO_CANCEL_OPTION = 1;
    /** Type used for <code>showConfirmDialog</code>. */
    public static final int OK_CANCEL_OPTION = 2;

    //
    // Return values.
    //
    /** Return value from class method if YES is chosen. */
    public static final int YES_OPTION = 0;
    /** Return value from class method if NO is chosen. */
    public static final int NO_OPTION = 1;
    /** Return value from class method if CANCEL is chosen. */
    public static final int CANCEL_OPTION = 2;
    /** Return value form class method if OK is chosen. */
    public static final int OK_OPTION = 0;

    //
    // Message types. Used by the UI to determine what icon to display,
    // and possibly what behavior to give based on the type.
    //
    /** Used for error messages. */
    public static final int ERROR_MESSAGE = 0;
    /** Used for information messages. */
    public static final int INFORMATION_MESSAGE = 1;
    /** Used for warning messages. */
    public static final int WARNING_MESSAGE = 2;
    /** Used for questions. */
    public static final int QUESTION_MESSAGE = 3;
    /** No icon is used. */
    public static final int PLAIN_MESSAGE = -1;

    public int showConfirmDialog(IComponent parent, String message, String title, int option);

    public String showInputDialog(IComponent parent, String message, String title, int messageType);

    public void showMessageDialog(IComponent parent, String message);
}
