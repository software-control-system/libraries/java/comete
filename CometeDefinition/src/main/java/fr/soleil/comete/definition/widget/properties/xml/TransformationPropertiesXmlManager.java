/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and {@link TransformationProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TransformationPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "transform";
    public static final String A0_PROPERTY_XML_TAG = "a0";
    public static final String A1_PROPERTY_XML_TAG = "a1";
    public static final String A2_PROPERTY_XML_TAG = "a2";

    /**
     * Transforms some {@link TransformationProperties} into an {@link XMLLine}
     * 
     * @param properties The {@link TransformationProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(TransformationProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttribute(A0_PROPERTY_XML_TAG, String.valueOf(properties.getA0()));
            openingLine.setAttribute(A1_PROPERTY_XML_TAG, String.valueOf(properties.getA1()));
            openingLine.setAttribute(A2_PROPERTY_XML_TAG, String.valueOf(properties.getA2()));
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link TransformationProperties}
     * 
     * @param currentSubNode The {@link Node} to interpret
     * @return The recovered {@link TransformationProperties}
     */
    public static TransformationProperties loadTransformation(Node currentSubNode) {
        return loadTransformationFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Map} to recover some {@link TransformationProperties}
     * 
     * @param propertyMap The {@link Map} to interpret. The {@link Map} is supposed to contain some xml node attributes
     * @return The recovered {@link TransformationProperties}
     */
    public static TransformationProperties loadTransformationFromXmlNodeAttributes(
            final Map<String, String> propertyMap) {
        TransformationProperties transformation = new TransformationProperties();
        if (propertyMap != null) {
            String a0_s = propertyMap.get(A0_PROPERTY_XML_TAG);
            String a1_s = propertyMap.get(A1_PROPERTY_XML_TAG);
            String a2_s = propertyMap.get(A2_PROPERTY_XML_TAG);

            double a0, a1, a2;
            a0 = parseDouble(a0_s, transformation.getA0());
            a1 = parseDouble(a1_s, transformation.getA1());
            a2 = parseDouble(a2_s, transformation.getA2());

            transformation.setA0(a0);
            transformation.setA1(a1);
            transformation.setA2(a2);
        }
        return transformation;
    }

}
