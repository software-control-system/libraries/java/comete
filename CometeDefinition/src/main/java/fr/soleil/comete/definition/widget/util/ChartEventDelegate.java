/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.widget.IChartViewer;

/**
 * A class that handles events management for {@link IChartViewer}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ChartEventDelegate {

    // Listeners
    protected final Set<IChartViewerListener> chartListeners;
    // ActionListeners
    protected final Set<ChartActionListener> chartActionListeners;
    // Associated IChartViewer
    protected final WeakReference<IChartViewer> chartReference;

    /**
     * Creates a new {@link ChartEventDelegate}
     * 
     * @param chart The {@link IChartViewer} this {@link ChartEventDelegate} should be delegated to
     */
    public ChartEventDelegate(IChartViewer chart) {
        chartListeners = Collections.newSetFromMap(new WeakHashMap<IChartViewerListener, Boolean>());
        chartActionListeners = Collections.newSetFromMap(new WeakHashMap<ChartActionListener, Boolean>());
        chartReference = (chart == null ? null : new WeakReference<IChartViewer>(chart));
    }

    /**
     * Returns this {@link ChartEventDelegate}'s associated {@link IChartViewer}
     * 
     * @return An {@link IChartViewer}
     */
    public IChartViewer getChart() {
        IChartViewer chart;
        if (chartReference == null) {
            chart = null;
        } else {
            chart = chartReference.get();
        }
        return chart;
    }

    /**
     * Registers a chart listener
     * 
     * @param listener The chart listener.
     */
    public void addChartViewerListener(IChartViewerListener listener) {
        if (listener != null) {
            synchronized (chartListeners) {
                chartListeners.add(listener);
            }
        }
    }

    /**
     * Removes a chart Listener
     * 
     * @param listener The chart listener.
     */
    public void removeChartViewerListener(IChartViewerListener listener) {
        if (listener != null) {
            synchronized (chartListeners) {
                chartListeners.remove(listener);
            }
        }
    }

    /**
     * Registers a chart action listener
     * 
     * @param listener The chart action listener.
     */
    public void addChartActionListener(ChartActionListener listener) {
        if (listener != null) {
            synchronized (chartActionListeners) {
                chartActionListeners.add(listener);
            }
        }
    }

    /**
     * Removes a chart action Listener
     * 
     * @param listener The chart action listener.
     */
    public void removeChartActionListener(ChartActionListener listener) {
        if (listener != null) {
            synchronized (chartActionListeners) {
                chartActionListeners.remove(listener);
            }
        }
    }

    /**
     * Notifies {@link IChartViewerListener}s for an event
     * 
     * @param event The {@link ChartViewerEvent} that represents the event
     */
    public void fireChartEvent(ChartViewerEvent event) {
        if (event != null) {
            List<IChartViewerListener> copy = new ArrayList<IChartViewerListener>();
            synchronized (chartListeners) {
                copy.addAll(chartListeners);
            }
            for (IChartViewerListener listener : copy) {
                listener.chartViewerChanged(event);
            }
            copy.clear();
        }
    }

    /**
     * Notifies {@link ChartActionListener}s for an event
     * 
     * @param event The {@link ChartActionEvent} that represents the event
     */
    public void fireChartActionEvent(ChartActionEvent event) {
        if (event != null) {
            List<ChartActionListener> copy = new ArrayList<ChartActionListener>();
            synchronized (chartActionListeners) {
                copy.addAll(chartActionListeners);
            }
            for (ChartActionListener listener : copy) {
                listener.actionPerformed(event);
            }
            copy.clear();
        }
    }

}
