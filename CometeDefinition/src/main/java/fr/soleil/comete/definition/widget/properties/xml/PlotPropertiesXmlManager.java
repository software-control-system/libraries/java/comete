/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

/**
 * A class that allows some interaction between XML {@link File}s and {@link PlotProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class PlotPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "plotParams";
    public static final String VIEW_TYPE_PROPERTY_XML_TAG = "viewType";
    public static final String AXIS_CHOICE_PROPERTY_XML_TAG = "axisChoice";
    public static final String USER_FORMAT_PROPERTY_XML_TAG = "userFormat";
    public static final String UNIT_PROPERTY_XML_TAG = "unit";

    /**
     * Transforms some {@link PlotProperties} into an XML {@link File} content
     * 
     * @param properties The {@link PlotProperties} to transform
     * @return A {@link String}
     */
    public static String toXmlString(PlotProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.OPENING_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttribute(VIEW_TYPE_PROPERTY_XML_TAG, String.valueOf(properties.getViewType()));
            openingLine.setAttribute(AXIS_CHOICE_PROPERTY_XML_TAG, String.valueOf(properties.getAxisChoice()));
            String userFormat = properties.getFormat();
            if (userFormat != null) {
                openingLine.setAttribute(USER_FORMAT_PROPERTY_XML_TAG, userFormat);
            }
            String unit = properties.getUnit();
            if (unit != null) {
                openingLine.setAttribute(UNIT_PROPERTY_XML_TAG, unit);
            }
        }
        XMLLine closingLine = new XMLLine(XML_TAG, XMLLine.CLOSING_TAG_CATEGORY);
        StringBuilder xmlBuilder = new StringBuilder();
        xmlBuilder = openingLine.toAppendable(xmlBuilder);
        xmlBuilder.append(LINE_SEPARATOR);
        if (properties != null) {
            xmlBuilder = BarPropertiesXmlManager.toXmlLine(properties.getBar()).toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = CurvePropertiesXmlManager.toXmlLine(properties.getCurve()).toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = MarkerPropertiesXmlManager.toXmlLine(properties.getMarker()).toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = ErrorPropertiesXmlManager.toXmlLine(properties.getError()).toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = TransformationPropertiesXmlManager.toXmlLine(properties.getTransform())
                    .toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = OffsetPropertiesXmlManager.toXmlLine(properties.getXOffset()).toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = InterpolationPropertiesXmlManager.toXmlLine(properties.getInterpolation())
                    .toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = SmoothingPropertiesXmlManager.toXmlLine(properties.getSmoothing()).toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = MathPropertiesXmlManager.toXmlLine(properties.getMath()).toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
        }
        xmlBuilder = closingLine.toAppendable(xmlBuilder);
        return xmlBuilder.toString();
    }

    /**
     * Interprets a {@link Node} to recover some {@link PlotProperties}
     * 
     * @param currentNode The {@link Node} to interpret
     * @return The recovered {@link PlotProperties}
     */
    public static PlotProperties loadPlotProperties(Node currentNode) {
        PlotProperties plot = new PlotProperties();
        if (currentNode != null) {
            Map<String, String> propertyMap = XMLUtils.loadAttributes(currentNode);
            String viewType_s = propertyMap.get(VIEW_TYPE_PROPERTY_XML_TAG);
            String axisChoice_s = propertyMap.get(AXIS_CHOICE_PROPERTY_XML_TAG);
            String userFormat = propertyMap.get(USER_FORMAT_PROPERTY_XML_TAG);
            String unit = propertyMap.get(UNIT_PROPERTY_XML_TAG);

            int viewType, axisChoice;
            viewType = parseInt(viewType_s, plot.getViewType());
            axisChoice = parseInt(axisChoice_s, plot.getAxisChoice());
            plot.setViewType(viewType);
            plot.setAxisChoice(axisChoice);
            plot.setFormat(userFormat);
            plot.setUnit(unit);

            if (currentNode.hasChildNodes()) {
                NodeList subNodes = currentNode.getChildNodes();

                for (int i = 0; i < subNodes.getLength(); i++) {
                    Node currentSubNode = subNodes.item(i);
                    if (!XMLUtils.isAFakeNode(currentSubNode)) {
                        String currentSubNodeType = currentSubNode.getNodeName().trim();
                        if (currentSubNodeType.equals(BarPropertiesXmlManager.XML_TAG)) {
                            plot.setBar(BarPropertiesXmlManager.loadBar(currentSubNode));
                        } else if (currentSubNodeType.equals(CurvePropertiesXmlManager.XML_TAG)) {
                            plot.setCurve(CurvePropertiesXmlManager.loadCurve(currentSubNode));
                        } else if (currentSubNodeType.equals(MarkerPropertiesXmlManager.XML_TAG)) {
                            plot.setMarker(MarkerPropertiesXmlManager.loadMarker(currentSubNode));
                        } else if (currentSubNodeType.equals(ErrorPropertiesXmlManager.XML_TAG)) {
                            plot.setError(ErrorPropertiesXmlManager.loadError(currentSubNode));
                        } else if (currentSubNodeType.equals(TransformationPropertiesXmlManager.XML_TAG)) {
                            plot.setTransform(TransformationPropertiesXmlManager.loadTransformation(currentSubNode));
                        } else if (currentSubNodeType.equals(OffsetPropertiesXmlManager.XML_TAG)) {
                            plot.setXOffset(OffsetPropertiesXmlManager.loadOffset(currentSubNode));
                        } else if (currentSubNodeType.equals(InterpolationPropertiesXmlManager.XML_TAG)) {
                            plot.setInterpolation(InterpolationPropertiesXmlManager.loadInterpolation(currentSubNode));
                        } else if (currentSubNodeType.equals(SmoothingPropertiesXmlManager.XML_TAG)) {
                            plot.setSmoothing(SmoothingPropertiesXmlManager.loadSmoothing(currentSubNode));
                        } else if (currentSubNodeType.equals(MathPropertiesXmlManager.XML_TAG)) {
                            plot.setMath(MathPropertiesXmlManager.loadMath(currentSubNode));
                        }
                    }
                }

            }

        }
        return plot;
    }
}
