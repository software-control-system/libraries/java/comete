/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

public class IntRotationTool {

    private int[] data;
    private volatile boolean cycle;
    private volatile int index;

    public IntRotationTool() {
        data = null;
        cycle = true;
        index = 0;
    }

    public int[] getData() {
        int[] data = this.data;
        return data == null ? null : data.clone();
    }

    public void setData(int[] data) {
        this.data = data;
        index = 0;
    }

    public boolean isCycle() {
        return cycle;
    }

    public void setCycle(boolean cycle) {
        this.cycle = cycle;
    }

    public int getNextInt(int defaultValue) {
        int result;
        int[] data = this.data;
        if ((data == null) || (data.length == 0)) {
            result = defaultValue;
        } else {
            if (cycle) {
                index = index % data.length;
            }
            int indexToUse = index;
            if (indexToUse < data.length) {
                result = data[indexToUse];
                index++;
            } else {
                result = defaultValue;
            }
        }
        return result;
    }

}
