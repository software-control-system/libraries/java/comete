/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.data.target.IDrawableTarget;
import fr.soleil.data.target.IErrorNotifiableTarget;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;

public interface IComponent extends IErrorNotifiableTarget, IDrawableTarget {

    // alignment
    public final static int CENTER = 0;
    public final static int TOP = 1;
    public final static int LEFT = 2;
    public final static int BOTTOM = 3;
    public final static int RIGHT = 4;

    public boolean hasFocus();

    public void setOpaque(boolean opaque);

    public boolean isOpaque();

    public void setCometeBackground(CometeColor color);

    public CometeColor getCometeBackground();

    public void setCometeForeground(CometeColor color);

    public CometeColor getCometeForeground();

    public void setEnabled(boolean enabled);

    public boolean isEnabled();

    public void setCometeFont(CometeFont font);

    public CometeFont getCometeFont();

    public void setToolTipText(String text);

    public String getToolTipText();

    public void setVisible(boolean visible);

    public boolean isVisible();

    public void setHorizontalAlignment(int halign);

    public int getHorizontalAlignment();

    public void setSize(int width, int height);

    public void setPreferredSize(int width, int height);

    public int getWidth();

    public int getHeight();

    public void setLocation(int x, int y);

    public int getX();

    public int getY();

    /**
     * Returns whether this component is currently editing its data.
     * 
     * @return A boolean value
     */
    public boolean isEditingData();

    /**
     * Sets a titled border to this {@link IComponent}
     * 
     * @param title The title to set
     */
    public void setTitledBorder(String title);

    /**
     * Adds an {@link IMouseListener} to this {@link IComponent}
     * 
     * @param listener The {@link IMouseListener} to add
     */
    public void addMouseListener(IMouseListener listener);

    /**
     * Removes an {@link IMouseListener} from this {@link IComponent}
     * 
     * @param listener The {@link IMouseListener} to add
     */
    public void removeMouseListener(IMouseListener listener);

    /**
     * Removes all {@link IMouseListener}s from this {@link IComponent}
     */
    public void removeAllMouseListeners();

    /**
     * Returns whether this {@link IComponent} should receive data in drawing thread, by using
     * {@link #getDrawingThreadManager()}.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean shouldReceiveDataInDrawingThread();

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public class IComponentSignature extends TargetSignature<CometeColor> {

        public IComponentSignature() {
            super(IComponent.class, CometeColor.class, IComponent.class);
        }

        @Override
        protected void callFunction(ITarget target, CometeColor data) {
            if (target instanceof IComponent) {
                ((IComponent) target).setCometeBackground(data);
            }
        }

    }

}
