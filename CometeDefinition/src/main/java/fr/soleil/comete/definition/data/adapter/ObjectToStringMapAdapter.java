/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.adapter;

import java.lang.ref.Reference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.adapter.NumberToBooleanAdapter;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.math.NumberArrayUtils;

public class ObjectToStringMapAdapter<U> extends AbstractAdapter<U, Map<String, Object>> {

    private String mapKey;
    private boolean splitMatrixThroughColumns;
    private final NumberToBooleanAdapter<Double> numberToBooleanAdapter;

    public ObjectToStringMapAdapter() {
        super(Object.class, Map.class);
        mapKey = null;
        splitMatrixThroughColumns = false;
        numberToBooleanAdapter = new NumberToBooleanAdapter<Double>(Double.class);
    }

    /**
     * Returns the key used in {@link Map}
     * 
     * @return A {@link String}
     */
    public String getMapKey() {
        return mapKey;
    }

    /**
     * Sets the key to use in {@link Map}
     * 
     * @param mapKey
     *            The key to use
     */
    public void setMapKey(String mapKey) {
        this.mapKey = mapKey;
    }

    /**
     * @return the splitMatrixThroughColumns
     */
    public boolean isSplitMatrixThroughColumns() {
        return splitMatrixThroughColumns;
    }

    /**
     * @param splitMatrixThroughColumns
     *            the splitMatrixThroughColumns to set
     */
    public void setSplitMatrixThroughColumns(boolean splitMatrixThroughColumns) {
        this.splitMatrixThroughColumns = splitMatrixThroughColumns;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> adapt(U data) throws DataAdaptationException {
        Map<String, Object> result = new LinkedHashMap<String, Object>();
        String label = mapKey;
        if (label == null) {
            label = toString();
        }
        if (data instanceof Map<?, ?>) {
            final Map<?, ?> dataMap = (Map<?, ?>) data;
            final Set<?> dataSet = dataMap.keySet();
            Iterator<?> iterator = dataSet.iterator();
            if (iterator.hasNext() && (dataSet.iterator().next() instanceof String)) {
                result = (Map<String, Object>) data;
            }
        } else if (data instanceof List<?>) {
            final List<?> sourceData = (List<?>) data;
            int index = 0;
            final List<Object> valueList = new ArrayList<Object>();
            for (final Object value : sourceData) {
                List<Object> currentList = extractArrays(value, index++, label);
                if ((currentList == null) || (currentList.size() == 0)) {
                    valueList.add(new double[0]);
                } else {
                    valueList.addAll(currentList);
                    currentList.clear();
                }
            }
            if ((valueList != null) && (valueList.size() > 0)) {
                if (valueList.size() == 1) {
                    result.put(label, extractIndexedArray(valueList.get(0)));
                } else {
                    Object firstArray = valueList.get(0);
                    double[] xArray;
                    if (firstArray instanceof Number) {
                        xArray = new double[] { ((Number) firstArray).doubleValue() };
                    } else {
                        xArray = NumberArrayUtils.extractDoubleArray(firstArray);
                    }
                    if (xArray == null) {
                        xArray = new double[0];
                    }
                    if (valueList.size() == 2) {
                        result.put(label, new Object[] { xArray, valueList.get(1) });
                    } else {
                        for (int valueIndex = 1; valueIndex < valueList.size(); valueIndex++) {
                            result.put(label + "_" + (valueIndex - 1),
                                    new Object[] { xArray, valueList.get(valueIndex) });
                        }
                    }
                }
                valueList.clear();
            } else {
                result.put(label, new double[0]);
            }
        } else {
            List<Object> valueList = extractArrays(data, -1, label);
            if (valueList == null) {
                treatUnusualData(data, result, label);
            } else {
                if (valueList.size() == 0) {
                    result.put(label, new double[0]);
                } else if (valueList.size() == 1) {
                    result.put(label, extractIndexedArray(valueList.get(0)));
                } else {
                    int index = 0;
                    for (Object value : valueList) {
                        result.put(label + "_" + index++, extractIndexedArray(value));
                    }
                }
                valueList.clear();
            }
        }
        return result;
    }

    /**
     * Extracts an indexed array from an array. An indexed array is an array of
     * 2 lines: 1 with indexes, the other one with values. It also may be a
     * single dimension double array, even indexed values representing indexes,
     * and odd indexed values representing values.
     * 
     * @param array
     *            The array from which to extract indexed array
     * @return An {@link Object}
     */
    protected Object extractIndexedArray(Object array) {
        Object indexedArray = array;
        if ((array == null) || (!ObjectUtils.isNumberClass(ArrayUtils.recoverDataType(array)))) {
            indexedArray = new double[0];
        } else {
            int[] shape = ArrayUtils.recoverShape(array);
            if (shape.length == 0) {
                double[] temp = new double[2];
                if (array instanceof Number) {
                    temp[1] = ((Number) array).doubleValue();
                } else {
                    temp[1] = Double.NaN;
                }
                indexedArray = temp;
            } else if ((shape.length == 1) && (shape[0] > 0)) {
                double[] indexes = generateIndexes(shape[0]);
                Object[] temp = new Object[2];
                temp[IChartViewer.X_INDEX] = indexes;
                temp[IChartViewer.Y_INDEX] = array;
                indexedArray = temp;
            }
        }
        return indexedArray;
    }

    /**
     * Extracts a {@link List} of arrays from some data. The arrays should only
     * contain the values, not the indexes
     * 
     * @param value
     *            The data from which to extract the array {@link List}
     * @param indexInList
     *            In case <code>value</code> comes from a {@link List}, this
     *            represents its index in the {@link List}
     * @param label
     *            The current label. Can be used in error messages
     * @return A {@link List}
     * @throws DataAdaptationException
     *             If a problem occurred while trying to extract array {@link List}
     */
    protected List<Object> extractArrays(Object value, int indexInList, String label) throws DataAdaptationException {
        List<Object> valueList = new ArrayList<Object>();
        if (value == null) {
            valueList.add(new double[0]);
        } else if (value instanceof AbstractMatrix<?>) {
            valueList = fillListFromMatrix((AbstractMatrix<?>) value, valueList, indexInList, label);
        } else if (value instanceof Number) {
            valueList.add(new double[] { ((Number) value).doubleValue() });
        } else if (value instanceof Boolean) {
            valueList.add(new double[] { getDoubleValue((Boolean) value) });
        } else if (value instanceof Boolean[]) {
            valueList.add(getDoubleArray((Boolean[]) value));
        } else if (value instanceof boolean[]) {
            valueList.add(getDoubleArray((boolean[]) value));
        } else if (value.getClass().isArray()) {
            if ((ArrayUtils.recoverArrayRank(value.getClass()) > 2)
                    || (!ObjectUtils.isNumberClass(ArrayUtils.recoverDataType(value)))) {
                valueList = extractArraysFromUnusualData(value, indexInList, label);
            } else {
                valueList.add(value);
            }
        } else if (value instanceof Reference<?>) {
            valueList = extractArrays(((Reference<?>) value).get(), indexInList, label);
        } else {
            valueList = extractArraysFromUnusualData(value, indexInList, label);
        }
        return valueList;
    }

    /**
     * Extracts a {@link List} of arrays from some {@link AbstractMatrix}, or
     * fills an existing one
     * 
     * @param value
     *            The {@link AbstractMatrix}
     * @param list
     *            The {@link List} to fill. Can be <code>null</code>
     * @param indexInList
     *            In case <code>value</code> comes from a {@link List}, this
     *            represents its index in the {@link List}
     * @param label
     *            The current label. Can be used in error messages
     * @return A not <code>null</code> {@link List} filled with the expected
     *         arrays
     * @throws DataAdaptationException
     *             If a problem occurred while trying to extract array {@link List}
     */
    protected List<Object> fillListFromMatrix(AbstractMatrix<?> value, List<Object> list, int indexInList, String label)
            throws DataAdaptationException {
        List<Object> valueList = list;
        if (valueList == null) {
            valueList = new ArrayList<Object>();
        }
        if (value == null) {
            valueList = extractArraysFromUnusualData(value, indexInList, label);
        } else if (indexInList == 0) {
            if (ObjectUtils.isNumberClass(value.getType())) {
                valueList.add(NumberArrayUtils.extractDoubleArray(value.getFlatValue()));
            } else if (Boolean.TYPE.equals(value.getType())) {
                valueList.add(getDoubleArray((boolean[]) value.getFlatValue()));
            } else if (Boolean.class.equals(value.getType())) {
                valueList.add(getDoubleArray((Boolean[]) value.getFlatValue()));
            }
        } else {
            if (value.getWidth() < 2 || value.getHeight() < 2) {
                if (ObjectUtils.isNumberClass(value.getType())) {
                    valueList.add(NumberArrayUtils.extractDoubleArray(value.getFlatValue()));
                } else if (Boolean.TYPE.equals(value.getType())) {
                    valueList.add(getDoubleArray((boolean[]) value.getFlatValue()));
                } else if (Boolean.class.equals(value.getType())) {
                    valueList.add(getDoubleArray((Boolean[]) value.getFlatValue()));
                }
            } else {
                if (splitMatrixThroughColumns) {
                    if (ObjectUtils.isNumberClass(value.getType())) {
                        for (int column = 0; column < value.getWidth(); column++) {
                            valueList.add(NumberArrayUtils.extractDoubleArray(value.getColumn(column)));
                        }
                    } else if (Boolean.TYPE.equals(value.getType())) {
                        for (int column = 0; column < value.getWidth(); column++) {
                            valueList.add(getDoubleArray((boolean[]) value.getColumn(column)));
                        }
                    } else if (Boolean.class.equals(value.getType())) {
                        for (int column = 0; column < value.getWidth(); column++) {
                            valueList.add(getDoubleArray((Boolean[]) value.getColumn(column)));
                        }
                    }
                } else {
                    if (ObjectUtils.isNumberClass(value.getType())) {
                        for (int line = 0; line < value.getHeight(); line++) {
                            valueList.add(NumberArrayUtils.extractDoubleArray(value.getLine(line)));
                        }
                    } else if (Boolean.TYPE.equals(value.getType())) {
                        for (int line = 0; line < value.getHeight(); line++) {
                            valueList.add(getDoubleArray((boolean[]) value.getLine(line)));
                        }
                    } else if (Boolean.class.equals(value.getType())) {
                        for (int line = 0; line < value.getHeight(); line++) {
                            valueList.add(getDoubleArray((Boolean[]) value.getLine(line)));
                        }
                    }
                }
            }
        }
        return valueList;
    }

    protected double getDoubleValue(boolean value) {
        return value ? 1d : 0d;
    }

    protected double getDoubleValue(Boolean value) {
        double result;
        if (value == null) {
            result = MathConst.NAN_FOR_NULL;
        } else {
            try {
                result = numberToBooleanAdapter.revertAdapt(value).doubleValue();
            } catch (DataAdaptationException e) {
                result = Double.NaN;
            }
        }
        return result;
    }

    protected double[] getDoubleArray(Boolean[] booleans) {
        double[] array;
        if (booleans == null) {
            array = null;
        } else {
            array = new double[booleans.length];
            for (int i = 0; i < array.length; i++) {
                array[i] = getDoubleValue(booleans[i]);
            }
        }
        return array;
    }

    protected double[] getDoubleArray(boolean[] booleans) {
        double[] array;
        if (booleans == null) {
            array = null;
        } else {
            array = new double[booleans.length];
            for (int i = 0; i < array.length; i++) {
                array[i] = getDoubleValue(booleans[i]);
            }
        }
        return array;
    }

    /**
     * Creates an array of indexes
     * 
     * @param length
     *            The array length
     * @return a <code>double[]</code>
     */
    protected double[] generateIndexes(int length) {
        double[] indexes = new double[length];
        for (int i = 0; i < length; i++) {
            indexes[i] = i;
        }
        return indexes;
    }

    protected List<Object> extractArraysFromUnusualData(Object data, int indexInList, String label)
            throws DataAdaptationException {
        throw generateDefaultException(data);
    }

    protected void treatUnusualData(Object data, Map<String, Object> result, String label)
            throws DataAdaptationException {
        throw generateDefaultException(data);
    }

    protected DataAdaptationException generateDefaultException(Object data) {
        DataAdaptationException exception;
        StringBuilder builder = new StringBuilder(getClass().getName());
        builder.append(" can't adapt ");
        if (data == null) {
            builder.append("null data");
        } else {
            builder.append("data of class ").append(data.getClass().getName());
        }
        exception = new DataAdaptationException(builder.toString());
        return exception;
    }

    @Override
    public U revertAdapt(Map<String, Object> data) throws DataAdaptationException {
        // this adapter can not revert adapt
        throw new DataAdaptationException(getClass().getName() + " can't revert adapt data");
    }

}
