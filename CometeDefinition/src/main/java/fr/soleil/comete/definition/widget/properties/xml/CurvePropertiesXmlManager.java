/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and {@link CurveProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class CurvePropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "curve";
    public static final String COLOR_PROPERTY_XML_TAG = "color";
    public static final String WIDTH_PROPERTY_XML_TAG = "width";
    public static final String STYLE_PROPERTY_XML_TAG = "style";
    public static final String NAME_PROPERTY_XML_TAG = "name";

    /**
     * Transforms some {@link CurveProperties} into an {@link XMLLine}
     * 
     * @param properties
     *            The {@link CurveProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(CurveProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttribute(COLOR_PROPERTY_XML_TAG, colorToString(properties.getColor()));
            openingLine.setAttribute(WIDTH_PROPERTY_XML_TAG, String.valueOf(properties.getWidth()));
            openingLine.setAttribute(STYLE_PROPERTY_XML_TAG, String.valueOf(properties.getLineStyle()));
            openingLine.setAttribute(NAME_PROPERTY_XML_TAG, properties.getName());
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link CurveProperties}
     * 
     * @param currentSubNode The {@link Node} to interpret
     * @return The recovered {@link CurveProperties}
     */
    public static CurveProperties loadCurve(Node currentSubNode) {
        return loadCurveFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Map} to recover some {@link CurveProperties}
     * 
     * @param propertyMap The {@link Map} to interpret. The {@link Map} is supposed to contain some xml node attributes
     * @return The recovered {@link CurveProperties}
     */
    public static CurveProperties loadCurveFromXmlNodeAttributes(Map<String, String> propertyMap) {
        CurveProperties curve = new CurveProperties();
        if (propertyMap != null) {
            String color_s = propertyMap.get(COLOR_PROPERTY_XML_TAG);
            String style_s = propertyMap.get(STYLE_PROPERTY_XML_TAG);
            String width_s = propertyMap.get(WIDTH_PROPERTY_XML_TAG);

            if (propertyMap.containsKey(NAME_PROPERTY_XML_TAG)) {
                curve.setName(propertyMap.get(NAME_PROPERTY_XML_TAG));
            }

            CometeColor fillColor = stringToColor(color_s);
            int width, style;
            width = parseInt(width_s, curve.getWidth());
            style = parseInt(style_s, curve.getLineStyle());

            if (fillColor != null) {
                curve.setColor(fillColor);
            }
            curve.setLineStyle(style);
            curve.setWidth(width);
        }
        return curve;
    }

}
