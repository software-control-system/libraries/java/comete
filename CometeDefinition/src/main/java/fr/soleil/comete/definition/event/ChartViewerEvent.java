/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import java.util.Map;

import fr.soleil.comete.definition.widget.IChartViewer;

/**
 * A {@link CometeEvent} to warn for some changes about an {@link IChartViewer}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ChartViewerEvent extends CometeSelectionEvent<IChartViewer> {

    private static final long serialVersionUID = 8603972538497519032L;

    protected String viewId;
    protected int index;
    protected Reason reason;
    protected Map<String, Object> concernedData;
    protected boolean shiftButtonPressed;
    protected boolean altButtonPressed;
    protected boolean controlButtonPressed;

    /**
     * Constructor
     * 
     * @param source The {@link IChartViewer} that generated this event
     * @param point The selected point, if concerned
     * @param index The selected index, if concerned
     * @param viewId The id of the view, if concerned
     * @param concernedData The data that changed in the {@link IChartViewer}, if concerned
     * @param reason The reason of this event
     */
    public ChartViewerEvent(IChartViewer source, double[] point, int index, String viewId,
            Map<String, Object> concernedData, Reason reason, int... keyboardButtonsActivated) {
        super(source, point);
        this.index = index;
        this.viewId = viewId;
        this.concernedData = concernedData;
        this.reason = reason;

        shiftButtonPressed = false;
        altButtonPressed = false;
        controlButtonPressed = false;

        for (int i : keyboardButtonsActivated) {
            if (i == KEY_ALT) {
                altButtonPressed = true;
            }
            if (i == KEY_SHIFT) {
                shiftButtonPressed = true;
            }
            if (i == KEY_CONTROL) {
                controlButtonPressed = true;
            }
        }
    }

    @Override
    public int getSelectedIndex() {
        return index;
    }

    /**
     * Returns the id of the view concerned by this event
     * 
     * @return A {@link String}. May be <code>null</code>
     */
    public String getViewId() {
        return viewId;
    }

    /**
     * Returns the {@link Map} that contains the changed data (if concerned):
     * <ol>
     * <li>In case of {@link Reason#DATA}:
     * <ul>
     * <li>The key is a {@link String}, and represents the data id</li>
     * <li>The value is an {@link Object}, that can be in fact on of those:
     * <ul>
     * <li>A <code>double</code> array (<code>double[]</code>), which can be understood that way:
     * <code>{x1,y1,x2,y2,...,xn,yn}</code></li>
     * <li>A <code>double</code> matrix (<code>double[][]</code>), that is, in fact, a row of x values (1st row) and row
     * of y values (2nd row)</li>
     * </ul>
     * </li>
     * </ul>
     * </li>
     * <li>In case of {@link Reason#HIGHLIGHT}:
     * <ul>
     * <li>The key is a {@link String}, and represents the data id</li>
     * <li>The value is an {@link Object} (due to generic signature reason), that is in fact a {@link Boolean}:
     * <ul>
     * <li><code>true</code> for highlighted</li>
     * <li><code>false</code> for unhighlighted</li>
     * </ul>
     * </li>
     * </ul>
     * </li>
     * </ol>
     * 
     * @return A {@link Map}. May be <code>null</code>
     * @see #getReason()
     * @see Reason#DATA
     * @see Reason#HIGHLIGHT
     */
    public Map<String, Object> getConcernedData() {
        return concernedData;
    }

    /**
     * Returns the reason of this event
     * 
     * @return A {@link Reason} :
     *         <ul>
     *         <li>{@link Reason#SELECTION} for an event due to a selection</li>
     *         <li>{@link Reason#CONFIGURATION} for an event due to a configuration change</li>
     *         <li>{@link Reason#DATA} for an event due to a chart data change</li>
     *         <li>{@link Reason#HIGHLIGHT} for an event due to a chart data highlighting change</li>
     *         </ul>
     */
    public Reason getReason() {
        return reason;
    }

    public boolean isAltButtonPressed() {
        return altButtonPressed;
    }

    public boolean isShiftButtonPressed() {
        return shiftButtonPressed;
    }

    public boolean isControlButtonPressed() {
        return controlButtonPressed;
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    /**
     * Describes the reasons to generate such an event
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    public static enum Reason {
        /**
         * Represents a selection change event
         */
        SELECTION,
        /**
         * Represents a configuration change event
         */
        CONFIGURATION,
        /**
         * Represents a data change event
         */
        DATA,
        /**
         * Represents a chart data highlight event
         */
        HIGHLIGHT
    }

}
