/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.data.mediator.Mediator;

public class MathProperties implements IDataViewProperties, Cloneable, Serializable {

    private static final long serialVersionUID = 4804080668358229362L;

    private int function;

    public MathProperties() {
        this(0);
    }

    public MathProperties(int function) {
        super();
        setFunction(function);
    }

    public int getFunction() {
        return function;
    }

    public void setFunction(int function) {
        this.function = function;
    }

    @Override
    public MathProperties clone() {
        MathProperties properties;
        try {
            properties = (MathProperties) super.clone();
        } catch (CloneNotSupportedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            properties = null;
        }
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && (getClass().equals(obj.getClass()))) {
            MathProperties properties = (MathProperties) obj;
            equals = (getFunction() == properties.getFunction());
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0x3A74;
        int mult = 321;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + getFunction();
        return hashCode;
    }

}
