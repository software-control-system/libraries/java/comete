/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

import fr.soleil.comete.definition.listener.IValueConvertorListener;

/**
 * An interface used to convert values into other values
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 */
public interface IValueConvertor {

    /**
     * Converts the given value into another value
     * 
     * @param value The value to convert
     * @return A double, that is the result of the value conversion
     */
    public double convertValue(double value);

    /**
     * Returns whether this {@link IValueConvertor} has enough information to produce valid values
     * (example: if this {@link IValueConvertor} needs some parameters, and these parameters are
     * NaN, it will return <code>FALSE</code>)
     * 
     * @return A boolean value. <code>TRUE</code> if this {@link IValueConvertor} is in valid
     *         conditions to do the conversion.
     */
    public boolean isValid();

    /**
     * Adds an {@link IValueConvertorListener} to this {@link IValueConvertor}
     * 
     * @param listener The {@link IValueConvertorListener} to add
     */
    public void addValueConvertorListener(IValueConvertorListener listener);

    /**
     * Removes an {@link IValueConvertorListener} from this {@link IValueConvertor}
     * 
     * @param listener The {@link IValueConvertorListener} to remove
     */
    public void removeValueConvertorListener(IValueConvertorListener listener);

    /**
     * Removes all {@link IValueConvertorListener}s from this {@link IValueConvertor}
     */
    public void clearValueConvertorListeners();

    /**
     * Warns listeners for some changes
     */
    public void warnListeners();

}
