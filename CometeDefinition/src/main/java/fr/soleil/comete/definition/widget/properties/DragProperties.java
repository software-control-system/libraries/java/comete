/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import fr.soleil.comete.definition.widget.IChartViewer;

/**
 * Properties used to setup drag behavior in charts
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DragProperties implements Cloneable, Serializable {

    private static final long serialVersionUID = 4093013821559528028L;

    private int dataViewOffsetDragType;
    private int scaleDragType, scaleDragSensitivity;

    /**
     * Constructs a new {@link DragProperties} with default drag behavior.
     */
    public DragProperties() {
        this(IChartViewer.DRAG_NONE);
    }

    /**
     * Constructs a new {@link DragProperties} with expected dataview drag type.
     * 
     * @param dataViewOffsetDragType The dataview drag type
     * @see IChartViewer#DRAG_NONE
     * @see IChartViewer#DRAG_X
     * @see IChartViewer#DRAG_Y
     * @see IChartViewer#DRAG_XY
     */
    public DragProperties(int dataViewOffsetDragType) {
        this(dataViewOffsetDragType, IChartViewer.DRAG_XY, IChartViewer.DEFAULT_SCALE_DRAG_SENTIVITY);
    }

    /**
     * Constructs a new {@link DragProperties} with expected dataview drag type.
     * 
     * @param dataViewOffsetDragType The dataview drag type
     * @param scaleDragType The scale drag type.
     * @param scaleDragSensitivity The scale drag sensitivity, i.e. the minimum amount of pixels before considering a
     *            scale drag occurred.
     * @see IChartViewer#DRAG_NONE
     * @see IChartViewer#DRAG_X
     * @see IChartViewer#DRAG_Y
     * @see IChartViewer#DRAG_XY
     */
    public DragProperties(int dataViewOffsetDragType, int scaleDragType, int scaleDragSensitivity) {
        super();
        setDataViewOffsetDragType(dataViewOffsetDragType);
        setScaleDragType(scaleDragType);
        setScaleDragSensitivity(scaleDragSensitivity);
    }

    /**
     * Returns the dataview drag type.
     * 
     * @return An <code>int</code>.
     * @see IChartViewer#DRAG_NONE
     * @see IChartViewer#DRAG_X
     * @see IChartViewer#DRAG_Y
     * @see IChartViewer#DRAG_XY
     */
    public int getDataViewOffsetDragType() {
        return dataViewOffsetDragType;
    }

    /**
     * Sets the dataview drag type.
     * 
     * @param dataViewOffsetDragType The dataview drag type.
     * @see IChartViewer#DRAG_NONE
     * @see IChartViewer#DRAG_X
     * @see IChartViewer#DRAG_Y
     * @see IChartViewer#DRAG_XY
     */
    public void setDataViewOffsetDragType(int dataViewOffsetDragType) {
        this.dataViewOffsetDragType = dataViewOffsetDragType;
    }

    /**
     * Returns the scale drag type.
     * 
     * @return An <code>int</code>.
     * @see IChartViewer#DRAG_NONE
     * @see IChartViewer#DRAG_X
     * @see IChartViewer#DRAG_Y
     * @see IChartViewer#DRAG_XY
     */
    public int getScaleDragType() {
        return scaleDragType;
    }

    /**
     * Sets the scale drag type.
     * 
     * @param scaleDragType The scale drag type.
     * @see IChartViewer#DRAG_NONE
     * @see IChartViewer#DRAG_X
     * @see IChartViewer#DRAG_Y
     * @see IChartViewer#DRAG_XY
     */
    public void setScaleDragType(int scaleDragType) {
        this.scaleDragType = scaleDragType;
    }

    /**
     * Returns the scale drag sensitivity, i.e. the minimum amount of pixels before considering a scale drag occurred.
     * 
     * @return The scale drag sensitivity.
     */
    public int getScaleDragSensitivity() {
        return scaleDragSensitivity;
    }

    /**
     * Sets the scale drag sensitivity, i.e. the minimum amount of pixels before considering a scale drag occurred.
     * 
     * @param scaleDragSensitivity The scale drag sensitivity to set.
     */
    public void setScaleDragSensitivity(int scaleDragSensitivity) {
        this.scaleDragSensitivity = scaleDragSensitivity < 0 ? 0 : scaleDragSensitivity;
    }

    @Override
    public DragProperties clone() {
        DragProperties clone;
        try {
            clone = (DragProperties) super.clone();
        } catch (CloneNotSupportedException e) {
            // will not happen as DragProperties implements Cloneable
            clone = null;
        }
        return clone;
    }

}
