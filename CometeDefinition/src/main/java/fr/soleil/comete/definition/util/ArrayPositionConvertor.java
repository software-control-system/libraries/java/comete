/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

import java.lang.reflect.Array;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.array.INumberArrayTarget;
import fr.soleil.data.target.matrix.INumberMatrixTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.NumberUtils;

/**
 * An {@link AbstractValueConvertor} that uses a number array to convert values.
 * That is why {@link ArrayPositionConvertor} is also an {@link INumberArrayTarget}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ArrayPositionConvertor extends AbstractValueConvertor
        implements IPiecewiseAffineValueConvertor, INumberArrayTarget, INumberMatrixTarget {

    protected final TargetDelegate targetDelegate;
    protected Object positions;
    protected boolean piecewiseAffine, followOrdinate;
    protected int width, height;

    public ArrayPositionConvertor() {
        targetDelegate = new TargetDelegate();
        positions = null;
        piecewiseAffine = false;
        width = height = 0;
    }

    @Override
    public boolean isPiecewiseAffine() {
        return piecewiseAffine;
    }

    /**
     * Sets whether this {@link ArrayPositionConvertor} should use a piecewise affine function to compute converted
     * value.
     * 
     * @param piecewiseAffine Whether this {@link ArrayPositionConvertor} should use a piecewise affine function to
     *            compute converted value.
     *            <p>
     *            If <code>true</code>, then the converted value will be a value at a proportion between 2 consecutive
     *            values in the known array, using a piecewise affine rule.
     *            Otherwise, the converted value will be the value at the index corresponding to the value to converted
     *            casted as an <code>int</code>.
     *            </p>
     */
    @Override
    public void setPiecewiseAffine(boolean piecewiseAffine) {
        if (piecewiseAffine != this.piecewiseAffine) {
            this.piecewiseAffine = piecewiseAffine;
            warnListeners();
        }
    }

    /**
     * Returns whether to follow ordinate instead of abscissa when the array represents a matrix.
     * 
     * @return Whether to follow ordinate instead of abscissa when the array represents a matrix.
     * @see #setFollowOrdinate(boolean) for more explanations.
     */
    public boolean isFollowOrdinate() {
        return followOrdinate;
    }

    /**
     * Sets whether to follow ordinate instead of abscissa when the array represents a matrix.
     * 
     * @param followOrdinate Whether to follow ordinate instead of abscissa when the array represents a matrix.
     *            <ul>
     *            <li>
     *            If <code>true</code>
     *            <ul>
     *            <li>
     *            If the array represents a matrix (array was set through
     *            {@link #setFlatNumberMatrix(Object, int, int)} or through {@link #setNumberMatrix(Object[])}), data
     *            conversion will be done by reading matrix through columns (column 1, then columns 2, etc.)
     *            </li>
     *            <li>If the array does not represent a matrix, this {@link ArrayPositionConvertor} will be considered
     *            as invalid and won't convert data.</li>
     *            </ul>
     *            </li>
     *            <li>
     *            Otherwise, data conversion will be done normally (using natural order).
     *            </li>
     *            </ul>
     */
    public void setFollowOrdinate(boolean followOrdinate) {
        if (this.followOrdinate != followOrdinate) {
            this.followOrdinate = followOrdinate;
            warnListeners();
        }
    }

    /**
     * Tries to recover the value in an array at given <code>double</code> index.
     * 
     * @param index The index.
     * @param length The array length.
     * @param positionToUse The array.
     * @param followOrdinate Whether to follow ordinate instead of abscissa when the array represents a matrix.
     * @param dimX Known matrix width.
     * @param dimY Known matrix height.
     * @return A <code>double</code>.
     */
    protected static double getValueAt(int index, int length, Object positionToUse, boolean followOrdinate, int dimX,
            int dimY) {
        double result = Double.NaN;
        if (followOrdinate) {
            if ((dimY < 1) || (dimX < 1)) {
                index = -1;
            } else {
                index = index * dimX;
            }
        }
        if ((index > -1) && (index < length)) {
            if (positionToUse instanceof Object[]) {
                Object[] objArray = (Object[]) positionToUse;
                if (objArray[index] instanceof Number) {
                    result = ((Number) objArray[index]).doubleValue();
                }
            } else {
                result = Array.getDouble(positionToUse, index);
            }
        }
        return result;
    }

    /**
     * Converts a value into another one, using an array. The converted value is the matching value in the array,
     * considering input value is an index.
     * <p>
     * Piecewise affine interpolation might be used, in which case the converted value is the best value using such an
     * interpolation.
     * </p>
     * 
     * @param value The input value.
     * @param positions The array.
     * @param piecewiseAffine Whether to use piecewise affine function.
     * @param followOrdinate Whether to follow ordinate instead of abscissa when the array represents a matrix.
     * @param dimX Known matrix width.
     * @param dimY Known matrix height.
     * @return A <code>double</code>.
     */
    public static double convertedValue(double value, Object positions, boolean piecewiseAffine, boolean followOrdinate,
            int dimX, int dimY) {
        double result = Double.NaN;
        try {
            if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                if ((positions != null) && positions.getClass().isArray()) {
                    int length = Array.getLength(positions);
                    if (length > 0) {
                        if (piecewiseAffine && (length > 1)) {
                            double floor = Math.floor(value), ceil = Math.ceil(value);
                            if (NumberUtils.sameDouble(value, floor, NumberUtils.FLOAT_DELTA_BIG_MARGIN)) {
                                result = getValueAt((int) floor, length, positions, followOrdinate, dimX, dimY);
                            } else if (NumberUtils.sameDouble(value, ceil, NumberUtils.FLOAT_DELTA_BIG_MARGIN)) {
                                result = getValueAt((int) ceil, length, positions, followOrdinate, dimX, dimY);
                            } else {
                                if (floor == length - 1) {
                                    floor--;
                                    ceil--;
                                }
                                double floorVal = getValueAt((int) floor, length, positions, followOrdinate, dimX,
                                        dimY);
                                double ceilVal = getValueAt((int) ceil, length, positions, followOrdinate, dimX, dimY);
                                result = NumberUtils.getAffineFunctionValue(value, floor, floorVal, ceil, ceilVal);
                            }
                        } else {
                            result = getValueAt((int) value, length, positions, followOrdinate, dimX, dimY);
                        }
                    }
                }
            }
        } catch (Exception e) {
            result = Double.NaN;
        }
        return result;
    }

    /**
     * Converts a value into another one, using an array. The converted value is the matching value in the array,
     * considering input value is an index.
     * <p>
     * Piecewise affine interpolation might be used, in which case the converted value is the best value using such an
     * interpolation.
     * </p>
     * 
     * @param value The input value.
     * @param positions The array.
     * @param piecewiseAffine Whether to use piecewise affine function.
     * @return A <code>double</code>.
     */
    public static double convertedValue(double value, Object positions, boolean piecewiseAffine) {
        return convertedValue(value, positions, piecewiseAffine, false, 0, 0);
    }

    @Override
    public double convertValue(double value) {
        return convertedValue(value, positions, piecewiseAffine, followOrdinate, width, height);
    }

    @Override
    public boolean isValid() {
        // local copy to avoid concurrent change
        Object positionToUse = positions;
        int width = this.width, height = this.height;
        boolean valid, ordinate = followOrdinate;
        if ((positionToUse == null) || (!positionToUse.getClass().isArray()) || (Array.getLength(positionToUse) < 1)) {
            valid = false;
        } else if (ordinate) {
            valid = (width > 0) && (height > 0);
        } else {
            valid = true;
        }
        return valid;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        targetDelegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        targetDelegate.removeMediator(mediator);
    }

    @Override
    public Object getNumberArray() {
        return positions;
    }

    protected boolean setValue(Object value) {
        boolean changed = false;
        if (!ObjectUtils.sameObject(positions, value)) {
            this.positions = value;
            changed = true;
        }
        return changed;
    }

    @Override
    public void setNumberArray(Object value) {
        int length = (value == null ? 0 : Array.getLength(value));
        if (setValue(value)) {
            if (value == null) {
                width = length;
                height = 0;
            }
            warnListeners();
        } else if ((width != length) || (height != 0)) {
            width = length;
            height = 0;
            warnListeners();
        }
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        // local copy to avoid concurrent change
        Object positionToUse = positions;
        int dim, width = this.width, height = this.height;
        boolean ordinate = followOrdinate;
        if (positionToUse == null) {
            dim = 0;
        } else if ((ordinate) && (width > 0) && (height > 0)) {
            dim = height;
        } else {
            dim = Array.getLength(positionToUse);
        }
        return dim;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return (positions == null ? 0 : 1);
    }

    @Override
    public Object[] getNumberMatrix() {
        Object[] result;
        // local copy to avoid concurrent change
        Object positionToUse = positions;
        if (positionToUse == null) {
            result = null;
        } else {
            result = (Object[]) Array.newInstance(positionToUse.getClass(), 1);
            result[0] = positionToUse;
        }
        return result;
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        if (value == null) {
            setNumberArray(null);
        } else {
            int[] shape = ArrayUtils.recoverShape(value);
            if ((shape == null) || (shape.length < 2)) {
                setNumberArray(ArrayUtils.convertArrayDimensionFromNTo1(value));
            } else {
                setFlatNumberMatrix(ArrayUtils.convertArrayDimensionFromNTo1(value), shape[1], shape[0]);
            }
        }
    }

    @Override
    public Object getFlatNumberMatrix() {
        return getNumberArray();
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        boolean changed = setValue(value);
        if ((this.width != width) || (this.height != height)) {
            this.width = width;
            this.height = height;
        }
        if (changed) {
            warnListeners();
        }
    }

}
