/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and {@link MathProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MathPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "math";
    public static final String FUNCTION_PROPERTY_XML_TAG = "function";

    /**
     * Transforms some {@link MathProperties} into an {@link XMLLine}
     * 
     * @param properties The {@link MathProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(MathProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttribute(FUNCTION_PROPERTY_XML_TAG, String.valueOf(properties.getFunction()));
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link MathProperties}
     * 
     * @param currentSubNode The {@link Node} to interpret
     * @return The recovered {@link MathProperties}
     */
    public static MathProperties loadMath(Node currentSubNode) {
        return loadMathFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Map} to recover some {@link MathProperties}
     * 
     * @param propertyMap The {@link Map} to interpret. The {@link Map} is supposed to contain some xml node attributes
     * @return The recovered {@link MathProperties}
     */
    public static MathProperties loadMathFromXmlNodeAttributes(final Map<String, String> propertyMap) {
        MathProperties math = new MathProperties();
        if (propertyMap != null) {
            String function_s = propertyMap.get(FUNCTION_PROPERTY_XML_TAG);
            int function = parseInt(function_s, math.getFunction());
            math.setFunction(function);
        }
        return math;
    }

}
