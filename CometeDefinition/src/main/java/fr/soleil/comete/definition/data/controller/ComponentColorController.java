/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.controller;

import java.util.Map;
import java.util.WeakHashMap;

import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.controller.ScalarTargetController;
import fr.soleil.data.exception.UnhandledTargetSignatureException;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;
import fr.soleil.data.target.cube.ICubeTarget;

public abstract class ComponentColorController<U> extends ScalarTargetController<U, CometeColor> {

    protected final Map<ITarget, Boolean> colorAsForegroundMap;
    protected final Map<ITarget, Boolean> colorEnabledMap;

    public ComponentColorController(Class<U> typeSource) {
        super(IComponent.class, typeSource, CometeColor.class);
        colorAsForegroundMap = new WeakHashMap<ITarget, Boolean>();
        colorEnabledMap = new WeakHashMap<ITarget, Boolean>();
    }

    @Override
    protected DataSourceAdapter<U, CometeColor> createAdapter(AbstractDataSource<U> source) {
        return initColorAdapter(source);
    }

    protected abstract DataSourceAdapter<U, CometeColor> initColorAdapter(AbstractDataSource<U> source);

    @Override
    public <I> CometeColor getCastedInfo(I info) {
        // not managed
        return null;
    }

    @Override
    public <I> boolean isInstanceOf(I info) {
        // not managed
        return false;
    }

    @Override
    protected void sendDataToTarget(ITarget target, CometeColor targetValue, AbstractDataSource<U> source) {
        if ((target instanceof IComponent) && isColorEnabled(target)) {
            if (isColorAsForeground(target)) {
                ((IComponent) target).setCometeForeground(targetValue);
            } else {
                ((IComponent) target).setCometeBackground(targetValue);
            }
        }
    }

    /**
     * @return the foregroundColor
     */
    public boolean isColorAsForeground(ITarget target) {
        boolean colorAsForeground = false;
        if (target != null) {
            synchronized (colorAsForegroundMap) {
                Boolean temp = colorAsForegroundMap.get(target);
                if (temp != null) {
                    colorAsForeground = temp.booleanValue();
                }
            }
        }
        return colorAsForeground;
    }

    /**
     * @param foregroundColor the foregroundColor to set
     */
    public void setColorAsForeground(boolean foregroundColor, ITarget target) {
        if (target != null) {
            colorAsForegroundMap.put(target, Boolean.valueOf(foregroundColor));
        }
    }

    /**
     * @return the foregroundColor
     */
    public boolean isColorEnabled(ITarget target) {
        boolean colorEnabled = true;
        if (target != null) {
            // Never use colors with matrix and cube targets
            if (target instanceof ICubeTarget) {
                colorEnabled = false;
            } else {
                synchronized (colorEnabledMap) {
                    Boolean temp = colorEnabledMap.get(target);
                    if (temp != null) {
                        colorEnabled = temp.booleanValue();
                    }
                }
            }
        }
        return colorEnabled;
    }

    /**
     * @param foregroundColor the foregroundColor to set
     */
    public void setColorEnabled(boolean foregroundColor, ITarget target) {
        if (target != null) {
            colorEnabledMap.put(target, Boolean.valueOf(foregroundColor));
        }
    }

    @Override
    protected TargetSignature<CometeColor> initSignature() throws UnhandledTargetSignatureException {
        TargetSignature<CometeColor> result = TargetSignatureProvider.getTargetSignature(targetClass, targetType,
                IComponent.class);
        return result;
    }

    public boolean isHandle(Class<?> aClass) {
        boolean result = false;
        if (aClass != null) {
            result = aClass.equals(sourceType);
        }
        return result;
    }

}
