/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

public class ChartPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "chartParams";

    public static final String LEGENDS_ARE_VISIBLE_PROPERTY_XML_TAG = "legendsAreVisible";
    public static final String LEGENDS_PLACEMENT_PROPERTY_XML_TAG = "legendsPlacement";
    public static final String LEGEND_PROPORTION_PROPERTY_XML_TAG = "legendProportion";
    public static final String BACKGROUND_COLOR_PROPERTY_XML_TAG = "backgroundColor";
    public static final String GLOBAL_BACKGROUND_COLOR_PROPERTY_XML_TAG = "globalBackgroundColor";
    public static final String HEADER_FONT_PROPERTY_XML_TAG = "headerFont";
    public static final String LABEL_FONT_PROPERTY_XML_TAG = "labelFont";
    public static final String TITLE_PROPERTY_XML_TAG = "title";
    public static final String DISPLAY_DURATION_PROPERTY_XML_TAG = "displayDuration";
    public static final String TIME_PRECISION_XML_TAG = "timePrecision";
    public static final String NO_VALUE_STRING_XML_TAG = "noValueString";
    public static final String AUTO_HIGHLIGHT_ON_LEGEND_XML_TAG = "autoHighlightOnLegend";
    public static final String LIMIT_DRAWING_ZONE_XML_TAG = "limitDrawingZone";
    public static final String TRANSPARENCY_ALLOWED_XML_TAG = "transparencyAllowed";

    /**
     * Transforms some {@link ChartProperties} into an XML {@link File} content
     * 
     * @param properties The {@link ChartProperties} to transform
     * @return A {@link String}
     */
    public static String toXmlString(ChartProperties properties) {
        return toXmlString(properties, XML_TAG);
    }

    /**
     * Transforms some {@link ChartProperties} into an XML {@link File} content, using a custom xml node name.
     * 
     * @param properties The {@link ChartProperties} to transform
     * @param xmlTag The xml node name to use
     * @return A {@link String}
     */
    public static String toXmlString(ChartProperties properties, String xmlTag) {
        if ((xmlTag == null) || xmlTag.trim().isEmpty()) {
            xmlTag = XML_TAG;
        }
        XMLLine openingLine = new XMLLine(xmlTag, XMLLine.OPENING_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttributeIfNotNull(LEGENDS_ARE_VISIBLE_PROPERTY_XML_TAG,
                    String.valueOf(properties.isLegendVisible()));
            openingLine.setAttributeIfNotNull(LEGENDS_PLACEMENT_PROPERTY_XML_TAG,
                    String.valueOf(properties.getLegendPlacement()));
            openingLine.setAttributeIfNotNull(LEGEND_PROPORTION_PROPERTY_XML_TAG,
                    String.valueOf(properties.getLegendProportion()));
            openingLine.setAttribute(AUTO_HIGHLIGHT_ON_LEGEND_XML_TAG,
                    String.valueOf(properties.isAutoHighlightOnLegend()));
            openingLine.setAttributeIfNotNull(BACKGROUND_COLOR_PROPERTY_XML_TAG,
                    colorToString(properties.getBackgroundColor()));
            openingLine.setAttributeIfNotNull(GLOBAL_BACKGROUND_COLOR_PROPERTY_XML_TAG,
                    colorToString(properties.getGlobalBackgroundColor()));
            openingLine.setAttributeIfNotNull(HEADER_FONT_PROPERTY_XML_TAG, fontToString(properties.getHeaderFont()));
            openingLine.setAttributeIfNotNull(LABEL_FONT_PROPERTY_XML_TAG, fontToString(properties.getLabelFont()));
            openingLine.setAttributeIfNotNull(TITLE_PROPERTY_XML_TAG, properties.getTitle());
            openingLine.setAttributeIfNotNull(DISPLAY_DURATION_PROPERTY_XML_TAG,
                    String.valueOf(properties.getDisplayDuration()));
            openingLine.setAttributeIfNotNull(TIME_PRECISION_XML_TAG, String.valueOf(properties.getTimePrecision()));
            openingLine.setAttributeIfNotNull(LIMIT_DRAWING_ZONE_XML_TAG,
                    String.valueOf(properties.isLimitDrawingZone()));
            openingLine.setAttributeIfNotNull(TRANSPARENCY_ALLOWED_XML_TAG,
                    String.valueOf(properties.isTransparencyAllowed()));
            openingLine.setAttributeIfNotNull(NO_VALUE_STRING_XML_TAG, properties.getNoValueString());
        }
        XMLLine closingLine = new XMLLine(xmlTag, XMLLine.CLOSING_TAG_CATEGORY);
        StringBuilder xmlBuilder = new StringBuilder();
        xmlBuilder = openingLine.toAppendable(xmlBuilder);
        xmlBuilder.append(LINE_SEPARATOR);
        if (properties != null) {
            xmlBuilder = AxisPropertiesXmlManager
                    .toXmlLine(AxisPropertiesXmlManager.XML_TAG_X, properties.getXAxisProperties())
                    .toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = AxisPropertiesXmlManager
                    .toXmlLine(AxisPropertiesXmlManager.XML_TAG_Y1, properties.getY1AxisProperties())
                    .toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = AxisPropertiesXmlManager
                    .toXmlLine(AxisPropertiesXmlManager.XML_TAG_Y2, properties.getY2AxisProperties())
                    .toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = SamplingPropertiesXmlManager.toXmlLine(properties.getSamplingProperties())
                    .toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
            xmlBuilder = DragPropertiesXmlManager.toXmlLine(properties.getDragProperties()).toAppendable(xmlBuilder);
            xmlBuilder.append(LINE_SEPARATOR);
        }
        xmlBuilder = closingLine.toAppendable(xmlBuilder);
        return xmlBuilder.toString();
    }

    /**
     * Interprets a {@link Node} to recover some {@link ChartProperties}
     * 
     * @param currentNode The {@link Node} to interpret
     * @return The recovered {@link ChartProperties}
     */
    public static ChartProperties loadChartProperties(Node currentNode) {
        ChartProperties chartProperties = new ChartProperties();
        if (currentNode != null) {
            Map<String, String> propertyMap = XMLUtils.loadAttributes(currentNode);
            String backgroundColor_s = propertyMap.get(BACKGROUND_COLOR_PROPERTY_XML_TAG);
            String globalBackgroundColor_s = propertyMap.get(BACKGROUND_COLOR_PROPERTY_XML_TAG);
            String displayDuration_s = propertyMap.get(DISPLAY_DURATION_PROPERTY_XML_TAG);
            String headerFont_s = propertyMap.get(HEADER_FONT_PROPERTY_XML_TAG);
            String labelFont_s = propertyMap.get(LABEL_FONT_PROPERTY_XML_TAG);
            String legendsAreVisible_s = propertyMap.get(LEGENDS_ARE_VISIBLE_PROPERTY_XML_TAG);
            String legendsPlacement_s = propertyMap.get(LEGENDS_PLACEMENT_PROPERTY_XML_TAG);
            String legendProportion_s = propertyMap.get(LEGEND_PROPORTION_PROPERTY_XML_TAG);
            String title_s = propertyMap.get(TITLE_PROPERTY_XML_TAG);
            String timePrecision_s = propertyMap.get(TIME_PRECISION_XML_TAG);
            String autoHighLightOnLegend_s = propertyMap.get(AUTO_HIGHLIGHT_ON_LEGEND_XML_TAG);
            String noValueString = propertyMap.get(NO_VALUE_STRING_XML_TAG);
            String limitDrawingZone_s = propertyMap.get(LIMIT_DRAWING_ZONE_XML_TAG);
            String transparencyAllowed_s = propertyMap.get(TRANSPARENCY_ALLOWED_XML_TAG);

            if (noValueString == null) {
                noValueString = chartProperties.getNoValueString();
            }

            int legendsPlacement, timePrecision;
            double legendProportion;
            CometeColor backgroundColor = stringToColor(backgroundColor_s);
            CometeColor globalBackgroundColor = stringToColor(globalBackgroundColor_s, backgroundColor);
            boolean autoHighLightOnLegend, legendsAreVisible, limitDrawingZone, transparencyAllowed;
            double displayDuration;
            CometeFont headerFont = stringToFont(headerFont_s);
            CometeFont labelFont = stringToFont(labelFont_s);

            legendsPlacement = parseInt(legendsPlacement_s, chartProperties.getLegendPlacement());
            legendProportion = parseDouble(legendProportion_s, chartProperties.getLegendProportion());
            timePrecision = parseInt(timePrecision_s, chartProperties.getTimePrecision());
            autoHighLightOnLegend = parseBoolean(autoHighLightOnLegend_s, chartProperties.isAutoHighlightOnLegend());
            legendsAreVisible = parseBoolean(legendsAreVisible_s, chartProperties.isLegendVisible());
            limitDrawingZone = parseBoolean(limitDrawingZone_s, chartProperties.isLimitDrawingZone());
            transparencyAllowed = parseBoolean(transparencyAllowed_s, chartProperties.isTransparencyAllowed());
            displayDuration = parseDouble(displayDuration_s, chartProperties.getDisplayDuration());

            chartProperties.setLegendPlacement(legendsPlacement);
            chartProperties.setLegendProportion(legendProportion);
            chartProperties.setAutoHighlightOnLegend(autoHighLightOnLegend);
            chartProperties.setBackgroundColor(backgroundColor);
            chartProperties.setGlobalBackgroundColor(globalBackgroundColor);
            chartProperties.setLegendVisible(legendsAreVisible);
            chartProperties.setDisplayDuration(displayDuration);
            chartProperties.setHeaderFont(headerFont);
            chartProperties.setLabelFont(labelFont);
            chartProperties.setTimePrecision(timePrecision);
            chartProperties.setNoValueString(noValueString);
            chartProperties.setLimitDrawingZone(limitDrawingZone);
            chartProperties.setTransparencyAllowed(transparencyAllowed);
            chartProperties.setTitle(title_s);

            if (currentNode.hasChildNodes()) {
                NodeList subNodes = currentNode.getChildNodes();
                for (int i = 0; i < subNodes.getLength(); i++) {
                    Node currentSubNode = subNodes.item(i);
                    if (!XMLUtils.isAFakeNode(currentSubNode)) {
                        String currentSubNodeType = currentSubNode.getNodeName().trim();
                        if (currentSubNodeType.equals(AxisPropertiesXmlManager.XML_TAG_X)) {
                            chartProperties
                                    .setXAxisProperties(AxisPropertiesXmlManager.loadAxis(currentSubNode, labelFont));
                        } else if (currentSubNodeType.equals(AxisPropertiesXmlManager.XML_TAG_Y1)) {
                            chartProperties
                                    .setY1AxisProperties(AxisPropertiesXmlManager.loadAxis(currentSubNode, labelFont));
                        } else if (currentSubNodeType.equals(AxisPropertiesXmlManager.XML_TAG_Y2)) {
                            chartProperties
                                    .setY2AxisProperties(AxisPropertiesXmlManager.loadAxis(currentSubNode, labelFont));
                        } else if (currentSubNodeType.equals(SamplingPropertiesXmlManager.XML_TAG)) {
                            chartProperties
                                    .setSamplingProperties(SamplingPropertiesXmlManager.loadSampling(currentSubNode));
                        } else if (currentSubNodeType.equals(DragPropertiesXmlManager.XML_TAG)) {
                            chartProperties.setDragProperties(DragPropertiesXmlManager.loadDrag(currentSubNode));
                        }
                    }
                }
            }
        }

        return chartProperties;
    }

}
