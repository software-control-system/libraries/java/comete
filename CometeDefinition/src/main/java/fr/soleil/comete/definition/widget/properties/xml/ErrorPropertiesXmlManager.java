/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and
 * {@link ErrorProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ErrorPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "error";
    public static final String COLOR_PROPERTY_XML_TAG = "color";
    public static final String VISIBLE_PROPERTY_XML_TAG = "visible";

    /**
     * Transforms some {@link ErrorProperties} into an {@link XMLLine}
     * 
     * @param properties
     *            The {@link ErrorProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(ErrorProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttribute(COLOR_PROPERTY_XML_TAG, colorToString(properties.getColor()));
            openingLine.setAttribute(VISIBLE_PROPERTY_XML_TAG, String.valueOf(properties.isVisible()));
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link ErrorProperties}
     * 
     * @param currentSubNode
     *            The {@link Node} to interpret
     * @return The recovered {@link ErrorProperties}
     */
    public static ErrorProperties loadError(Node currentSubNode) {
        return loadErrorFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Map} to recover some {@link ErrorProperties}
     * 
     * @param propertyMap
     *            The {@link Map} to interpret. The {@link Map} is supposed to
     *            contain some xml node attributes
     * @return The recovered {@link ErrorProperties}
     */
    public static ErrorProperties loadErrorFromXmlNodeAttributes(final Map<String, String> propertyMap) {
        ErrorProperties error = new ErrorProperties();
        if (propertyMap != null) {
            String color_s = propertyMap.get(COLOR_PROPERTY_XML_TAG);
            String visible_s = propertyMap.get(VISIBLE_PROPERTY_XML_TAG);

            CometeColor color = stringToColor(color_s);
            boolean visible;
            try {
                visible = "true".equalsIgnoreCase(visible_s.trim());
            } catch (Exception e) {
                visible = error.isVisible();
            }

            error.setColor(color);
            error.setVisible(visible);
        }
        return error;
    }

}
