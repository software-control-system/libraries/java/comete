/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import java.io.File;
import java.util.EventObject;

import fr.soleil.comete.definition.data.target.IImageTarget;
import fr.soleil.comete.definition.data.target.scalar.IEditableTextComponent;
import fr.soleil.comete.definition.listener.IFileBrowserListener;
import fr.soleil.data.target.IFileTarget;

public interface IFileBrowser extends IEditableTextComponent, IFileTarget, IImageTarget {

    public static final int FULLNAME = 0;
    public static final int FILENAME = 1;
    public static final int PATH = 2;
    public static final int ALL = 3;

    public void addFileBrowserListener(IFileBrowserListener e);

    public void removeFileBrowserListener(IFileBrowserListener e);

    public int getBrowserType();

    public void setBrowserType(int browserType);

    public void fireSelectedFileChange(EventObject event);

    public boolean isDisplayHiddenFile();

    public void setDisplayHiddenFile(boolean displayHiddenFile);

    public String getExtensionFile();

    public void setExtensionFile(String extensionFile);

    public File getFile();

    public String getDirectory();

    public String getDefaultDirectory();

    public void setDefaultDirectory(String defaultDirectory);

    public void setDirectory(String directory);

    public String getTitle();

    public void setTitle(String title);

}
