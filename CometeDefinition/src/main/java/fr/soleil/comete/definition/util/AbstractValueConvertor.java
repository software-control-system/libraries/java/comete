/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.definition.event.ValueConvertorEvent;
import fr.soleil.comete.definition.listener.IValueConvertorListener;

public abstract class AbstractValueConvertor implements IValueConvertor {

    private final List<WeakReference<IValueConvertorListener>> listeners;

    public AbstractValueConvertor() {
        listeners = new ArrayList<WeakReference<IValueConvertorListener>>();
    }

    @Override
    public void addValueConvertorListener(IValueConvertorListener listener) {
        if (listener != null) {
            List<WeakReference<IValueConvertorListener>> toRemove = new ArrayList<WeakReference<IValueConvertorListener>>();
            synchronized (listeners) {
                boolean canAdd = true;
                for (WeakReference<IValueConvertorListener> ref : listeners) {
                    IValueConvertorListener temp = ref.get();
                    if (temp == null) {
                        toRemove.add(ref);
                    } else if (temp.equals(listener)) {
                        canAdd = false;
                    }
                }
                listeners.removeAll(toRemove);
                if (canAdd) {
                    listeners.add(new WeakReference<IValueConvertorListener>(listener));
                }
            }
            toRemove.clear();
        }
    }

    @Override
    public void removeValueConvertorListener(IValueConvertorListener listener) {
        List<WeakReference<IValueConvertorListener>> toRemove = new ArrayList<WeakReference<IValueConvertorListener>>();
        synchronized (listeners) {
            for (WeakReference<IValueConvertorListener> ref : listeners) {
                IValueConvertorListener temp = ref.get();
                if ((temp == null) || temp.equals(listener)) {
                    toRemove.add(ref);
                }
            }
            listeners.removeAll(toRemove);
        }
        toRemove.clear();
    }

    @Override
    public void clearValueConvertorListeners() {
        synchronized (listeners) {
            listeners.clear();
        }
    }

    @Override
    public void warnListeners() {
        ValueConvertorEvent event = new ValueConvertorEvent(this);
        List<WeakReference<IValueConvertorListener>> toRemove = new ArrayList<WeakReference<IValueConvertorListener>>();
        synchronized (listeners) {
            for (WeakReference<IValueConvertorListener> ref : listeners) {
                IValueConvertorListener temp = ref.get();
                if ((temp == null)) {
                    toRemove.add(ref);
                } else {
                    temp.convertorChanged(event);
                }
            }
            listeners.removeAll(toRemove);
        }
        toRemove.clear();
    }

}
