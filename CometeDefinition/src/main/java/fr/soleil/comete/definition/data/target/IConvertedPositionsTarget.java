/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.target;

import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.data.target.ITarget;

/**
 * An {@link ITarget} that receives converted positions
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IConvertedPositionsTarget extends ITarget {

    /**
     * Returns the positions convertors
     * 
     * @return An {@link IValueConvertor} array
     */
    public IValueConvertor[] getValueConvertors();

    /**
     * Sets the position {@link IValueConvertor}s
     * 
     * @param valueConvertors The {@link IValueConvertor}<code>[]</code> to set
     */
    public void setValueConvertors(IValueConvertor[] valueConvertors);

}
