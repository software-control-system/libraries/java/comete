/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import fr.soleil.comete.definition.widget.IComponent;

/**
 * Comete version of mouse events
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class CometeMouseEvent extends CometeEvent<IComponent> {

    private static final long serialVersionUID = -1974394999734349897L;

    protected int x;
    protected int y;
    protected int button;
    protected int clicks;
    protected int scroll;
    protected int mask;
    protected long when;
    protected boolean altDown;
    protected boolean altGrDown;
    protected boolean controlDown;
    protected boolean shiftDown;
    protected boolean metaDown;
    protected boolean canceled;
    protected MouseReason reason;

    public CometeMouseEvent(IComponent source, MouseReason reason, int x, int y, int button, int clicks, int scroll,
            int mask, long when, boolean altDown, boolean altGrDown, boolean controlDown, boolean shiftDown,
            boolean metaDown) {
        super(source);
        this.reason = reason;
        this.x = x;
        this.y = y;
        this.button = button;
        this.clicks = clicks;
        this.scroll = scroll;
        this.mask = mask;
        this.when = when;
        this.altDown = altDown;
        this.altGrDown = altGrDown;
        this.controlDown = controlDown;
        this.shiftDown = shiftDown;
        this.metaDown = metaDown;
    }

    /**
     * Cancels this event, so that it won't be processed in the default manner by the source that
     * generated it
     */
    public void cancel() {
        canceled = true;
    }

    /**
     * Returns whether this event is canceled
     * 
     * @return A <code>boolean</code>
     */
    public boolean isCanceled() {
        return canceled;
    }

    /**
     * Returns the reason of this event
     * 
     * @return A {@link MouseReason}
     */
    public MouseReason getReason() {
        return reason;
    }

    /**
     * Returns the mouse x position
     * 
     * @return An <code>int</code>
     */
    public int getX() {
        return x;
    }

    /**
     * Returns the mouse y position
     * 
     * @return An <code>int</code>
     */
    public int getY() {
        return y;
    }

    /**
     * Returns the used mouse button (0 for the 1st one, 1 for the 2nd one, etc...)
     * 
     * @return An <code>int</code>
     */
    public int getButton() {
        return button;
    }

    /**
     * Returns the number of mouse clicks associated with this event
     * 
     * @return An <code>int</code>. Negative values if the mouse wheel was rotated up/away from the
     *         user, and positive values if the mouse wheel was rotated down/ towards the user
     */
    public int getClicks() {
        return clicks;
    }

    /**
     * Returns the number of "clicks" the mouse wheel was rotated.
     * 
     * @return An <code>int</code>
     */
    public int getScroll() {
        return scroll;
    }

    /**
     * Returns the state of the keyboard modifier keys and mouse masks at the time the event was
     * generated.
     * 
     * @return An <code>int</code>
     */
    public int getMask() {
        return mask;
    }

    /**
     * Returns the date in milliseconds of when this event occurred
     * 
     * @return A <code>long</code>
     */
    public long getWhen() {
        return when;
    }

    /**
     * Returns whether or not the Alt modifier is down on this event.
     * 
     * @return A <code>boolean</code>
     */
    public boolean isAltDown() {
        return altDown;
    }

    /**
     * Returns whether or not the AltGr modifier is down on this event.
     * 
     * @return A <code>boolean</code>
     */
    public boolean isAltGrDown() {
        return altGrDown;
    }

    /**
     * Returns whether or not the Ctrl modifier is down on this event.
     * 
     * @return A <code>boolean</code>
     */
    public boolean isControlDown() {
        return controlDown;
    }

    /**
     * Returns whether or not the Shift modifier is down on this event.
     * 
     * @return A <code>boolean</code>
     */
    public boolean isShiftDown() {
        return shiftDown;
    }

    /**
     * Returns whether or not the Meta modifier is down on this event.
     * 
     * @return A <code>boolean</code>
     */
    public boolean isMetaDown() {
        return metaDown;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * Describes the possibles reasons of a {@link CometeMouseEvent}
     */
    public static enum MouseReason {
        /**
         * Represents a button pressed
         */
        PRESSED,
        /**
         * Represents a button released
         */
        RELEASED,
        /**
         * Represents a button clicked
         */
        CLICKED,
        /**
         * Represents a mouse move
         */
        MOVED,
        /**
         * Represents a mouse dragged event
         */
        DRAGGED,
        /**
         * Represents the fact that the mouse entered the {@link IComponent}
         */
        ENTERED,
        /**
         * Represents the fact that the mouse exited the {@link IComponent}
         */
        EXITED,
        /**
         * Represents a mouse wheel move
         */
        WHEEL
    }

}
