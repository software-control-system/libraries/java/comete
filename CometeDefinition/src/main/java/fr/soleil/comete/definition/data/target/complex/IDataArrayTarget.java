/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.target.complex;

import java.util.Collection;
import java.util.Map;

import fr.soleil.data.target.cube.ICubeTarget;

public interface IDataArrayTarget extends ICubeTarget {

    public Map<String, Object> getData();

    public void setData(Map<String, Object> data);

    /**
     * Adds some data in target, represented as a <code>double</code> matrix or
     * a <code>double</code> array
     * <ul>
     * <li>For the <code>double</code> matrix:
     * <ul>
     * <li>The 1st line represents x values</li>
     * <li>The 2nd line represents y values</li>
     * </ul>
     * </li>
     * <li>For the <code>double</code> array:
     * <ul>
     * <li>each x value is at <code>2&times;k</code> position</li>
     * <li>each y value is at <code>2&times;k + 1</code> position</li>
     * </ul>
     * </li>
     * </ul>
     * If there already is some data that correspond to the id in {@link Map},
     * then the existing data values are updated.
     * 
     * @param dataToAdd
     *            the data to add, represented as a {@link Map} <code>&lt;id, value&gt;</code>
     */
    public void addData(Map<String, Object> dataToAdd);

    /**
     * Removes some data from target
     * 
     * @param idsToRemove
     *            The Collection of identifiers to recover the data to remove
     */
    public void removeData(Collection<String> idsToRemove);
}
