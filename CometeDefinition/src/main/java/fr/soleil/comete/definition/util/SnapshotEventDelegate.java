/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

import fr.soleil.comete.definition.event.SnapshotEvent;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.widget.ISnapshotableComponent;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A class delegated to {@link SnapshotEvent} management for {@link ISnapshotableComponent}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class SnapshotEventDelegate {

    private final Set<ISnapshotListener> listeners;
    private final WeakReference<ISnapshotableComponent> compRef;
    protected String snapshotDirectory;
    protected String snapshotFile;

    public SnapshotEventDelegate(ISnapshotableComponent comp, String snapshotDirectory) {
        listeners = Collections.newSetFromMap(new WeakHashMap<ISnapshotListener, Boolean>());
        if (comp == null) {
            compRef = null;
        } else {
            compRef = new WeakReference<ISnapshotableComponent>(comp);
        }
        this.snapshotDirectory = snapshotDirectory;
        snapshotFile = null;
    }

    public void addSnapshotListener(ISnapshotListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    public void removeSnapshotListener(ISnapshotListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    /**
     * Changes the last snapshot file or directory location
     * 
     * @param location The new location to set
     */
    public void changeSnapshotLocation(String location) {
        if ((location != null) && (!location.trim().isEmpty())) {
            File file = new File(location);
            if (file.exists()) {
                if (file.isDirectory()) {
                    snapshotFile = null;
                    snapshotDirectory = location;
                } else {
                    snapshotFile = location;
                    snapshotDirectory = file.getParentFile().getAbsolutePath();
                }
                warnListeners();
            }
        }
    }

    public String getSnapshotDirectory() {
        return snapshotDirectory;
    }

    /**
     * Returns the path of the directory where snapshot (picture/movies) files are written, or of
     * the last saved snapshot file
     * 
     * @return A {@link String}
     * @see #getSnapshotDirectory()
     * @see #getSnapshotFile()
     */
    public String getSnapshotLocation() {
        String snapshotLocation = getSnapshotFile();
        if (snapshotLocation == null) {
            snapshotLocation = getSnapshotDirectory();
        }
        return snapshotLocation;
    }

    public void setSnapshotDirectory(String snapshotDirectory) {
        if ((snapshotDirectory != null) && (!snapshotDirectory.trim().isEmpty())
                && (!ObjectUtils.sameObject(this.snapshotDirectory, snapshotDirectory))) {
            File temp = new File(snapshotDirectory);
            if (temp.isDirectory()) {
                this.snapshotFile = null;
                this.snapshotDirectory = snapshotDirectory;
                warnListeners();
            }
        }
    }

    public String getSnapshotFile() {
        return snapshotFile;
    }

    public void warnListeners() {
        ISnapshotableComponent comp = (compRef == null ? null : compRef.get());
        if (comp != null) {
            SnapshotEvent event = new SnapshotEvent(comp);
            List<ISnapshotListener> copy = new ArrayList<ISnapshotListener>();
            synchronized (listeners) {
                copy.addAll(listeners);
            }
            for (ISnapshotListener listener : copy) {
                listener.snapshotChanged(event);
            }
        }
    }

}
