/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import fr.soleil.lib.project.math.MathConst;

public interface IChartConst {

    public enum Orientation {
        /** Horizontal axis */
        HORIZONTAL,
        /** Vertical axis */
        VERTICAL
    }

    public enum ShapeType {
        /** Title of an axis */
        TITLE,
        /** Labels for an axis */
        LABELS,
        /** grid of axes */
        GRID,
        /** sub grid of axes */
        SUB_GRID,
        /** outside sub ticks for an axis */
        EXTRA_SUB_TICKS,
        /** sub ticks for an axis */
        SUB_TICKS,
        /** ticks for an axis */
        TICKS,
        /** bar of an axis */
        AXIS_BAR,
        /** outside sub ticks for an axis opposite */
        OPPOSITE_EXTRA_SUB_TICKS,
        /** ticks for an axis opposite */
        OPPOSITE_TICKS,
        /** sub ticks for an axis opposite */
        OPPOSITE_SUB_TICKS,
        /** bar of an axis opposite */
        OPPOSITE_AXIS_BAR,
        /** header of the chart */
        HEADER,
        /** dataview bundle */
        DATAVIEW,
        /** bars of a dataview */
        DATAVIEW_BARS,
        /** Markers of a dataview */
        DATAVIEW_MARKERS,
        /** Polyline of a dataview */
        DATAVIEW_POLYLINE,
        /** size of the windows */
        PANEL_SIZE,
        /** clipping of the dataview */
        CLIPPING;
    }

    public enum UpdateType {
        /** update generals attributes of the ChartViewer */
        CONFIGURATION_UPDATE,
        /** update dataviews */
        DATAVIEWS_UPDATE,
        /** update all axis attributes */
        ALL_AXIS_UPDATE,
        /** update horizontal axis size */
        HORIZONTAL_AXIS_SIZE_UPDATE,
        /** update horizontal axis position */
        HORIZONTAL_AXIS_POSITION_UPDATE,
        /** update vertical axis size */
        VERTICAL_AXIS_SIZE_UPDATE,
        /** update vertical axis position */
        VERTICAL_AXIS_POSITION_UPDATE,
        /** update axis labels */
        AXIS_LABELS_UPDATE,
        /** update axis attributes */
        AXIS_ATTRIBUTES_UPDATE;
    }

    public enum RepaintType {
        /** repaint all graphics */
        FULL_REPAINT,
        /** repaint only vertical axis with the new windows dimension */
        VERTICAL_SIZE_REPAINT,
        /** repaint only vertical axis with the new bunch of labels */
        VERTICAL_LABELS_REPAINT,
        /** repaint only horizontal axis with the new windows dimension */
        HORIZONTAL_SIZE_REPAINT,
        /** repaint only horizontal axis with the new bunch of labels */
        HORIZONTAL_LABELS_REPAINT,
        /** repaint only horizontal axis for Time Annotation update */
        TIME_REPAINT,
        /** repaint only dataviews */
        DATAVIEW_REPAINT;
    }

    public enum FormatNAN {
        /** NaN value used with y to represent a null value */
        NAN_FOR_NULL(MathConst.NAN_FOR_NULL),
        /** NaN value used with y to represent a positive infinity value */
        NAN_FOR_POSITIVE_INFINITY(Double.longBitsToDouble(0xfffbad00000000ffL)),
        /** NaN value used with y to represent a negative infinity value */
        NAN_FOR_NEGATIVE_INFINITY(Double.longBitsToDouble(0xfff00000000badffL));

        private final double value;

        FormatNAN(double value) {
            this.value = value;
        }

        public double getValue() {
            return value;
        }
    }

    public final static int DEFAULT_HIGHLIGHT_COEFF = 3;

    public static double logStep[] = { 0.301, 0.477, 0.602, 0.699, 0.778, 0.845, 0.903, 0.954 };
    public static double ln10 = Math.log(10);

}
