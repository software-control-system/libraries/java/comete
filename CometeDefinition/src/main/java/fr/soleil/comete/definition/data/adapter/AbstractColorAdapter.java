/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.adapter;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.source.AbstractDataSource;

/**
 * An {@link AbstractDataAdapter} that is able to transform data into {@link CometeColor}s
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <U> The type of data to transform into {@link CometeColor}
 */
public abstract class AbstractColorAdapter<U> extends DataSourceAdapter<U, CometeColor> {

    protected final Map<U, CometeColor> encodingMap;
    protected final Class<U> dataType;

    public AbstractColorAdapter(AbstractDataSource<U> dataSource, Class<U> dataType) {
        super(dataSource, null);
        this.dataType = dataType;
        encodingMap = new HashMap<>();
        try {
            initEncoding();
        } catch (Exception e) {
            // ignore exception, but trace it for debug
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to init encoding", e);
        }
    }

    protected abstract void initEncoding() throws DataAdaptationException;

    @Override
    protected U adaptContainerData(CometeColor data) {
        U value = null;
        if (data != null) {
            for (U key : encodingMap.keySet()) {
                if (data.equals(encodingMap.get(key))) {
                    value = key;
                    break;
                }
            }
        }
        return value;
    }

    @Override
    protected CometeColor adaptSourceData(U data) {
        CometeColor color = null;
        if (data != null) {
            color = encodingMap.get(data);
        }
        return color;
    }

}
