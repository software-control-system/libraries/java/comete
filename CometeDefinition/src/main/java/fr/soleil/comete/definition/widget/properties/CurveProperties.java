/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

public class CurveProperties implements IDataViewProperties, Cloneable, Serializable {

    private static final long serialVersionUID = 3601112899910715473L;

    private CometeColor color;
    private int width;
    private int lineStyle;
    private String name;

    public CurveProperties() {
        this(CometeColor.RED);
    }

    public CurveProperties(CometeColor color) {
        this(color, 1, 0, ObjectUtils.EMPTY_STRING);
    }

    public CurveProperties(CometeColor _color, int _width, int _lineStyle, String name) {
        super();
        setColor(_color);
        setWidth(_width);
        setLineStyle(_lineStyle);
        setName(name);
    }

    /**
     * @return Returns the color.
     */
    public CometeColor getColor() {
        return color;
    }

    /**
     * @param color The color to set.
     */
    public void setColor(CometeColor color) {
        if (color == null) {
            color = CometeColor.RED;
        }
        this.color = color;
    }

    /**
     * @return Returns the lineStyle.
     */
    public int getLineStyle() {
        return lineStyle;
    }

    /**
     * @param lineStyle The lineStyle to set.
     */
    public void setLineStyle(int lineStyle) {
        this.lineStyle = lineStyle;
    }

    /**
     * @return Returns the width.
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width The width to set.
     */
    public void setWidth(int width) {
        this.width = width;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public CurveProperties clone() {
        CurveProperties properties;
        try {
            properties = (CurveProperties) super.clone();
            properties.color = (properties.color == null ? null : properties.color.clone());
        } catch (CloneNotSupportedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            properties = null;
        }
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && getClass().equals(obj.getClass())) {
            CurveProperties properties = (CurveProperties) obj;
            equals = (ObjectUtils.sameObject(getColor(), properties.getColor()) && (getWidth() == properties.getWidth())
                    && (getLineStyle() == properties.getLineStyle())
                    && ObjectUtils.sameObject(getName(), properties.getName()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0xC90;
        int mult = 369;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getColor());
        hashCode = hashCode * mult + getWidth();
        hashCode = hashCode * mult + getLineStyle();
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getName());
        return hashCode;
    }

}
