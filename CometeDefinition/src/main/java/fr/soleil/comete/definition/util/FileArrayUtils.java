/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

import javax.activation.UnsupportedDataTypeException;

import org.slf4j.LoggerFactory;

import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.FlatMatrix;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

public class FileArrayUtils {

    private static boolean saveColumns(OutputStreamWriter fileWriter, String fileName, int columns,
            String valueSeparator) {
        boolean writeOk = true;
        try {
            for (int i = 0; i < columns; i++) {
                fileWriter.write(String.valueOf(i));
                if (i < columns - 1) {
                    fileWriter.write(valueSeparator);
                }
            }
            fileWriter.write(System.getProperty("line.separator"));
            fileWriter.flush();
        } catch (Exception e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to write matrix columns in file: " + fileName,
                    e);
            writeOk = false;
        }
        return writeOk;
    }

    /**
     * Saves a matrix value, represented as a flat array, in a File
     * 
     * @param fileName The path of the file to save
     * @param matrix The flat representation of the matrix value
     * @param columns The number of columns in the matrix
     * @param valueSeparator The separator that should be used to separate values in the same a row
     * @param saveColumnIndex A boolean value. If true, the first line in File will be the columns
     *            indexes
     */
    public static <T> void saveFlatMatrixToFile(String fileName, Object matrix, Class<?> contentType, int columns,
            String valueSeparator, boolean saveColumnIndex) {
        if ((fileName != null) && (!fileName.trim().isEmpty())) {
            FileWriter fileWriter = FileHelper.newFileWriter(fileName, LoggerFactory.getLogger(Mediator.LOGGER_ACCESS));
            saveFlatMatrixToFile(fileWriter, fileName, matrix, columns, valueSeparator, saveColumnIndex, false);
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .error("Failed to close file writer for file: " + fileName, e);
                }
            }
        }
    }

    /**
     * Saves a matrix value, represented as a flat array, in a File
     * 
     * @param fileWriter The {@link OutputStreamWriter} that will write the file
     * @param fileName The name of the file managed by the {@link OutputStreamWriter}. Will be used
     *            in error logs, if any
     * @param matrix The flat representation of the matrix value
     * @param columns The number of columns in the matrix
     * @param valueSeparator The separator that should be used to separate values in the same a row
     * @param saveColumnIndex A boolean value. If true, the first line in File will be the columns
     *            indexes
     * @param boolean startWithValueSeparator Whether to start each matrix row with
     *            <code>valueSeparator</code>
     */
    public static <T> void saveFlatMatrixToFile(OutputStreamWriter fileWriter, String fileName, Object matrix,
            int columns, String valueSeparator, boolean saveColumnIndex, boolean startWithValueSeparator) {
        if ((fileWriter != null) && (matrix != null) && matrix.getClass().isArray() && (Array.getLength(matrix) > 0)
                && (columns > -1) && (Array.getLength(matrix) % columns == 0) && (valueSeparator != null)) {
            boolean writeOk = true;
            Class<?> contentType = matrix.getClass().getComponentType();
            if (saveColumnIndex) {
                writeOk = saveColumns(fileWriter, fileName, columns, valueSeparator);
            }
            if (writeOk) {
                try {
                    int rows = Array.getLength(matrix) / columns;
                    ValueOfArray valueOf = new ValueOfArray();
                    if (byte.class.equals(contentType)) {
                        valueOf = new ValueOfByteArray();
                    } else if (short.class.equals(contentType)) {
                        valueOf = new ValueOfShortArray();
                    } else if (int.class.equals(contentType)) {
                        valueOf = new ValueOfIntArray();
                    } else if (long.class.equals(contentType)) {
                        valueOf = new ValueOfLongArray();
                    } else if (float.class.equals(contentType)) {
                        valueOf = new ValueOfFloatArray();
                    } else if (double.class.equals(contentType)) {
                        valueOf = new ValueOfDoubleArray();
                    } else if (boolean.class.equals(contentType)) {
                        valueOf = new ValueOfBooleanArray();
                    } else if (char.class.equals(contentType)) {
                        valueOf = new ValueOfCharArray();
                    }
                    for (int row = 0; row < rows; row++) {
                        if (startWithValueSeparator) {
                            fileWriter.write(valueSeparator);
                        }
                        for (int col = 0; col < columns; col++) {
                            int index = (row * columns) + col;
                            fileWriter.write(valueOf.getValueOf(matrix, index));
                            if (col < columns - 1) {
                                fileWriter.write(valueSeparator);
                            }
                        }
                        fileWriter.write(System.getProperty("line.separator"));
                        fileWriter.flush();
                    }
                } catch (Exception e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .error("Failed to write flat matrix in file: " + fileName, e);
                }
            }
        }
    }

    /**
     * Saves a matrix value in a File
     * 
     * @param fileName The path of the file to save
     * @param matrix The matrix value
     * @param valueSeparator The separator that should be used to separate values in the same a row
     * @param saveColumnIndex A boolean value. If true, the first line in File will be the columns
     *            indexes
     */
    public static void saveMatrixToFile(String fileName, Object[] matrix, Class<?> contentType, String valueSeparator,
            boolean saveColumnIndex) {
        if ((fileName != null) && (!fileName.trim().isEmpty())) {
            FileWriter fileWriter = FileHelper.newFileWriter(fileName, LoggerFactory.getLogger(Mediator.LOGGER_ACCESS));
            saveMatrixToFile(fileWriter, fileName, matrix, valueSeparator, saveColumnIndex, false);
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (Exception e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .error("Failed to close file writer for file: " + fileName, e);
                }
            }
        }
    }

    /**
     * Saves a matrix value in a File
     * 
     * @param fileWriter The {@link OutputStreamWriter} that will write the file
     * @param fileName The name of the file managed by the {@link OutputStreamWriter}. Will be used
     *            in error logs, if any
     * @param matrix The matrix value
     * @param valueSeparator The separator that should be used to separate values in the same a row
     * @param saveColumnIndex A boolean value. If true, the first line in File will be the columns
     *            indexes
     * @param boolean startWithValueSeparator Whether to start each matrix row with
     *            <code>valueSeparator</code>
     */
    public static void saveMatrixToFile(OutputStreamWriter fileWriter, String fileName, Object[] matrix,
            String valueSeparator, boolean saveColumnIndex, boolean startWithValueSeparator) {
        if ((fileWriter != null) && (matrix != null) && (matrix.length > 0) && (Array.getLength(matrix[0]) > 0)
                && (valueSeparator != null)) {
            boolean writeOk = true;
            if (saveColumnIndex) {
                writeOk = saveColumns(fileWriter, fileName, Array.getLength(matrix[0]), valueSeparator);
            }
            if (writeOk) {
                Class<?> contentType = ArrayUtils.recoverDataType(matrix);
                ValueOfArray valueOf = new ValueOfArray();
                if (byte.class.equals(contentType)) {
                    valueOf = new ValueOfByteArray();
                } else if (short.class.equals(contentType)) {
                    valueOf = new ValueOfShortArray();
                } else if (int.class.equals(contentType)) {
                    valueOf = new ValueOfIntArray();
                } else if (long.class.equals(contentType)) {
                    valueOf = new ValueOfLongArray();
                } else if (float.class.equals(contentType)) {
                    valueOf = new ValueOfFloatArray();
                } else if (double.class.equals(contentType)) {
                    valueOf = new ValueOfDoubleArray();
                } else if (boolean.class.equals(contentType)) {
                    valueOf = new ValueOfBooleanArray();
                } else if (char.class.equals(contentType)) {
                    valueOf = new ValueOfCharArray();
                }
                try {
                    int rows = matrix.length;
                    for (int row = 0; row < rows; row++) {
                        if (startWithValueSeparator) {
                            fileWriter.write(valueSeparator);
                        }
                        int columns = Array.getLength(matrix[row]);
                        for (int col = 0; col < columns; col++) {
                            fileWriter.write(valueOf.getValueOf(matrix, row, col));
                            if (col < columns - 1) {
                                fileWriter.write(valueSeparator);
                            }
                        }
                        fileWriter.write(System.getProperty("line.separator"));
                        fileWriter.flush();
                    }
                } catch (Exception e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to write matrix in file: " + fileName,
                            e);
                }
            }
        }
    }

    private static String[] extractLines(BufferedReader reader, boolean ignoreFirstLine, String finish) {
        String[] result = null;
        ArrayList<String> lines = new ArrayList<String>();
        try {
            if (ignoreFirstLine) {
                reader.readLine();
            }
            for (String line = reader.readLine(); (line != null)
                    && (!ObjectUtils.sameObject(line, finish)); line = reader.readLine()) {
                lines.add(line);
            }
        } catch (Exception e) {
            lines.clear();
            lines = null;
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("FileArrayUtils failed to extract lines", e);
        }
        if (lines != null) {
            result = lines.toArray(new String[lines.size()]);
            lines.clear();
        }
        return result;
    }

    private static <T> ArrayList<T> extractValueListFromReader(BufferedReader reader, AbstractAdapter<String, T> parser,
            String valueSeparator, boolean ignoreFirstLine, StringBuilder columnCountBuffer) {
        ArrayList<T> list = new ArrayList<T>();
        try {
            int columns = 0;
            if (ignoreFirstLine) {
                reader.readLine();
            }
            for (String line = reader.readLine(); (line != null) && (list != null); line = reader.readLine()) {
                String[] stringValues = split(line, valueSeparator);
                if (stringValues.length > columns) {
                    columns = stringValues.length;
                }
                for (String stringValue : stringValues) {
                    T value = parser.adapt(stringValue);
                    if (value == null) {
                        list.clear();
                        list = null;
                        break;
                    } else {
                        list.add(value);
                    }
                }
            }
            columnCountBuffer.append(columns);
            reader.close();
        } catch (Exception e) {
            list.clear();
            list = null;
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                    .error("FileArrayUtils failed to extract value list from reader", e);
            try {
                reader.close();
            } catch (Exception closeException) {
                // Just ignore this exception
            }
        }
        return list;
    }

    protected static String[] split(String toSplit, String separator) {
        String[] values = toSplit.split(separator);
        if ((values.length > 0) && ((values[0] == null) || (values[0].isEmpty()))) {
            values = Arrays.copyOfRange(values, 1, values.length);
        }
        return values;
    }

    protected static double parseDouble(String value) {
        double result;
        if ("NaN".equalsIgnoreCase(value)) {
            result = Double.NaN;
        } else {
            result = Double.parseDouble(value);
        }
        return result;
    }

    protected static float parseFloat(String value) {
        float result;
        if ("NaN".equalsIgnoreCase(value)) {
            result = Float.NaN;
        } else {
            result = Float.parseFloat(value);
        }
        return result;
    }

    /**
     * Extracts a matrix from a text file
     * 
     * @param <T> The matrix elements type
     * @param fileName The file path
     * @param parser The {@link AbstractAdapter} that will be used to parse each element string
     *            value
     * @param valueSeparator The separator supposedly used to separate elements in a same line
     * @param ignoreFirstLine A boolean value. If <code>TRUE</code>, the first line of the file is
     *            ignored
     * @return The {@link FlatMatrix} that stores the expected matrix, or <code>null</code> if a
     *         problem occurred while extracting it.
     */
    @SuppressWarnings("unchecked")
    public static <T> FlatMatrix<T[]> loadFlatMatrixFromFile(String fileName, AbstractAdapter<String, T> parser,
            String valueSeparator, boolean ignoreFirstLine) {
        FlatMatrix<T[]> result = null;
        boolean readOk = true;
        if ((fileName != null) && (!fileName.trim().isEmpty()) && (parser != null) && (parser.getSecondType() != null)
                && (valueSeparator != null)) {
            result = new FlatMatrix<T[]>();
            FileReader fileReader = FileHelper.newFileReader(fileName, LoggerFactory.getLogger(Mediator.LOGGER_ACCESS));
            if (fileReader == null) {
                readOk = false;
            } else {
                BufferedReader reader = new BufferedReader(fileReader);
                StringBuilder columnCountBuffer = new StringBuilder();
                ArrayList<T> list = extractValueListFromReader(reader, parser, valueSeparator, ignoreFirstLine,
                        columnCountBuffer);
                try {
                    fileReader.close();
                } catch (Exception e) {
                    // Just ignore this exception
                }
                if (list == null) {
                    readOk = false;
                } else {
                    try {
                        int columns = Integer.parseInt(columnCountBuffer.toString());
                        result.setValue(columns, list
                                .toArray((T[]) Array.newInstance(parser.getSecondType(), new int[] { list.size() })));
                    } catch (Exception e) {
                        readOk = false;
                    }
                }
            }
        } else {
            readOk = false;
        }
        if (!readOk) {
            result = null;
        }
        return result;
    }

    /**
     * Extracts a matrix from a text file
     * 
     * @param <T> The matrix elements type
     * @param fileName The file path
     * @param parser The {@link AbstractAdapter} that will be used to parse each element string
     *            value
     * @param valueSeparator The separator supposedly used to separate elements in a same line
     * @param ignoreFirstLine A boolean value. If <code>TRUE</code>, the first line of the file is
     *            ignored
     * @return The expected matrix, or <code>null</code> if a problem occurred while extracting it.
     */
    @SuppressWarnings("unchecked")
    public static <T> T[][] loadMatrixFromFile(String fileName, AbstractAdapter<String, T> parser,
            String valueSeparator, boolean ignoreFirstLine) {
        T[][] result = null;
        boolean readOk = true;
        if ((fileName != null) && (!fileName.trim().isEmpty()) && (parser != null) && (parser.getSecondType() != null)
                && (valueSeparator != null)) {
            FileReader fileReader = FileHelper.newFileReader(fileName, LoggerFactory.getLogger(Mediator.LOGGER_ACCESS));
            if (fileReader == null) {
                readOk = false;
            } else {
                BufferedReader reader = new BufferedReader(fileReader);
                StringBuilder columnCountBuffer = new StringBuilder();
                ArrayList<T> list = extractValueListFromReader(reader, parser, valueSeparator, ignoreFirstLine,
                        columnCountBuffer);
                try {
                    fileReader.close();
                } catch (Exception e) {
                    // Just ignore this exception
                }
                if (list == null) {
                    readOk = false;
                } else {
                    try {
                        int columns = Integer.parseInt(columnCountBuffer.toString());
                        if (columns > 0) {
                            int rows = list.size() / columns;
                            result = (T[][]) Array.newInstance(parser.getSecondType(), new int[] { rows, columns });
                            int y = 0, x = 0;
                            for (T value : list) {
                                result[y][x++] = value;
                                if (x >= result[y].length) {
                                    y++;
                                }
                            }
                        } else {
                            result = (T[][]) Array.newInstance(parser.getSecondType(), new int[] { list.size(), 0 });
                        }
                    } catch (Exception e) {
                        readOk = false;
                    }
                }
            }
        } else {
            readOk = false;
        }
        if (!readOk) {
            result = null;
        }
        return result;
    }

    /**
     * Extracts a primitive type matrix from a text file
     * 
     * @param fileName The file path
     * @param expectedType The class that represents the expected primitive type. For example, if
     *            you want a boolean matrix as result (<code>boolean[][]</code>), use
     *            {@link Boolean#TYPE}
     * @param valueSeparator The separator supposedly used to separate elements in a same line
     * @param ignoreFirstLine A boolean value. If <code>TRUE</code>, the first line of the file is
     *            ignored
     * @return The expected primitive type matrix, or <code>null</code> if a problem occurred while
     *         extracting it, or if the <code>expectedType</code> did not represent a valid type.
     */
    public static Object loadTypeMatrixFromFile(String fileName, Class<?> expectedType, String valueSeparator,
            boolean ignoreFirstLine) {
        Object result = null;
        if ((fileName != null) && (!fileName.trim().isEmpty()) && (expectedType != null) && (valueSeparator != null)) {
            FileReader fileReader = FileHelper.newFileReader(fileName, LoggerFactory.getLogger(Mediator.LOGGER_ACCESS));
            if (fileReader != null) {
                result = loadTypeMatrixFromFile(new BufferedReader(fileReader), fileName, expectedType, valueSeparator,
                        ignoreFirstLine, null);
                try {
                    fileReader.close();
                } catch (Exception e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to close file: " + fileName, e);
                }
            }
        }
        return result;
    }

    /**
     * Extracts a primitive type matrix from a text file
     * 
     * @param reader The {@link BufferedReader} that can access the file
     * @param fileName The name of the file managed by the {@link BufferedReader}. Will be used in
     *            error logs, if any
     * @param expectedType The class that represents the expected primitive type. For example, if
     *            you want a boolean matrix as result (<code>boolean[][]</code>), use
     *            {@link Boolean#TYPE}
     * @param valueSeparator The separator supposedly used to separate elements in a same line
     * @param ignoreFirstLine A boolean value. If <code>TRUE</code>, the first line of the file is
     *            ignored
     * @param finish Value of the last line. If not <code>null</code> and such a line is found, this
     *            line is ignored and the reading stops
     * @return The expected primitive type matrix, or <code>null</code> if a problem occurred while
     *         extracting it, or if the <code>expectedType</code> did not represent a valid type.
     */
    public static Object loadTypeMatrixFromFile(BufferedReader reader, String fileName, Class<?> expectedType,
            String valueSeparator, boolean ignoreFirstLine, String finish) {
        Object result = null;
        if ((reader != null) && (expectedType != null) && (valueSeparator != null)) {
            String[] lines = extractLines(reader, ignoreFirstLine, finish);
            if (lines != null) {
                if (Boolean.TYPE.equals(expectedType)) {
                    // boolean matrix
                    boolean[][] matrix = new boolean[lines.length][];
                    for (int y = 0; (matrix != null) && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        matrix[y] = new boolean[values.length];
                        for (int x = 0; (matrix != null) && (x < matrix[y].length); x++) {
                            try {
                                matrix[y][x] = Boolean.parseBoolean(values[x].trim());
                            } catch (Exception e) {
                                matrix = null;
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read matrix from file: " + fileName, e);
                            }
                        }
                    }
                    result = matrix;
                } else if (Character.TYPE.equals(expectedType)) {
                    // char matrix
                    char[][] matrix = new char[lines.length][];
                    for (int y = 0; (matrix != null) && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        matrix[y] = new char[values.length];
                        for (int x = 0; (matrix != null) && (x < matrix[y].length); x++) {
                            String tempValue = values[x].trim();
                            if (tempValue.length() == 1) {
                                matrix[y][x] = tempValue.charAt(0);
                            } else {
                                matrix = null;
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read matrix from file: " + fileName);
                            }
                        }
                    }
                    result = matrix;
                } else if (Byte.TYPE.equals(expectedType)) {
                    // byte matrix
                    byte[][] matrix = new byte[lines.length][];
                    for (int y = 0; (matrix != null) && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        matrix[y] = new byte[values.length];
                        for (int x = 0; (matrix != null) && (x < matrix[y].length); x++) {
                            try {
                                matrix[y][x] = Byte.parseByte(values[x].trim());
                            } catch (Exception e) {
                                matrix = null;
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read matrix from file: " + fileName, e);
                            }
                        }
                    }
                    result = matrix;
                } else if (Short.TYPE.equals(expectedType)) {
                    // short matrix
                    short[][] matrix = new short[lines.length][];
                    for (int y = 0; (matrix != null) && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        matrix[y] = new short[values.length];
                        for (int x = 0; (matrix != null) && (x < matrix[y].length); x++) {
                            try {
                                matrix[y][x] = Short.parseShort(values[x].trim());
                            } catch (Exception e) {
                                matrix = null;
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read matrix from file: " + fileName, e);
                            }
                        }
                    }
                    result = matrix;
                } else if (Integer.TYPE.equals(expectedType)) {
                    // int matrix
                    int[][] matrix = new int[lines.length][];
                    for (int y = 0; (matrix != null) && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        matrix[y] = new int[values.length];
                        for (int x = 0; (matrix != null) && (x < matrix[y].length); x++) {
                            try {
                                matrix[y][x] = Integer.parseInt(values[x].trim());
                            } catch (Exception e) {
                                matrix = null;
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read matrix from file: " + fileName, e);
                            }
                        }
                    }
                    result = matrix;
                } else if (Long.TYPE.equals(expectedType)) {
                    // long matrix
                    long[][] matrix = new long[lines.length][];
                    for (int y = 0; (matrix != null) && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        matrix[y] = new long[values.length];
                        for (int x = 0; (matrix != null) && (x < matrix[y].length); x++) {
                            try {
                                matrix[y][x] = Long.parseLong(values[x].trim());
                            } catch (Exception e) {
                                matrix = null;
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read matrix from file: " + fileName, e);
                            }
                        }
                    }
                    result = matrix;
                } else if (Float.TYPE.equals(expectedType)) {
                    // float matrix
                    float[][] matrix = new float[lines.length][];
                    for (int y = 0; (matrix != null) && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        matrix[y] = new float[values.length];
                        for (int x = 0; (matrix != null) && (x < matrix[y].length); x++) {
                            try {
                                matrix[y][x] = parseFloat(values[x].trim());
                            } catch (Exception e) {
                                matrix = null;
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read matrix from file: " + fileName, e);
                            }
                        }
                    }
                    result = matrix;
                } else if (Double.TYPE.equals(expectedType)) {
                    // double matrix
                    double[][] matrix = new double[lines.length][];
                    for (int y = 0; (matrix != null) && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        matrix[y] = new double[values.length];
                        for (int x = 0; (matrix != null) && (x < matrix[y].length); x++) {
                            try {
                                matrix[y][x] = parseDouble(values[x].trim());
                            } catch (Exception e) {
                                matrix = null;
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read matrix from file: " + fileName, e);
                            }
                        }
                    }
                    result = matrix;
                }
            }
        }
        return result;
    }

    /**
     * Extracts a primitive type matrix {@link FlatMatrix} representation from a text file
     * 
     * @param fileName The file path
     * @param expectedType The class that represents the expected primitive type. For example, if
     *            you want the result to be a {@link FlatMatrix} which value is a boolean table (
     *            <code>boolean[]</code>), use {@link Boolean#TYPE}
     * @param valueSeparator The separator supposedly used to separate elements in a same line
     * @param ignoreFirstLine A boolean value. If <code>TRUE</code>, the first line of the file is
     *            ignored
     * @return The expected {@link FlatMatrix}, or <code>null</code> if a problem occurred while
     *         extracting it, or if the <code>expectedType</code> did not represent a valid type.
     */
    public static FlatMatrix<Object> loadTypeFlatMatrixFromFile(String fileName, Class<?> expectedType,
            String valueSeparator, boolean ignoreFirstLine) {
        FlatMatrix<Object> result;
        if ((fileName != null) && (!fileName.trim().isEmpty()) && (expectedType != null) && (valueSeparator != null)) {
            FileReader fileReader = FileHelper.newFileReader(fileName, LoggerFactory.getLogger(Mediator.LOGGER_ACCESS));
            if (fileReader == null) {
                result = new FlatMatrix<Object>();
            } else {
                result = loadTypeFlatMatrixFromFile(new BufferedReader(fileReader), fileName, expectedType,
                        valueSeparator, ignoreFirstLine, null);
                try {
                    fileReader.close();
                } catch (Exception e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to close file: " + fileName, e);
                }
            }
        } else {
            result = new FlatMatrix<Object>();
        }
        if (result.getValue() == null) {
            result = null;
        }
        return result;
    }

    /**
     * Extracts a primitive type matrix {@link FlatMatrix} representation from a text file
     * 
     * @param reader The {@link BufferedReader} that can access the file
     * @param fileName The name of the file managed by the {@link BufferedReader}. Will be used in
     *            error logs, if any
     * @param expectedType The class that represents the expected primitive type. For example, if
     *            you want the result to be a {@link FlatMatrix} which value is a boolean table (
     *            <code>boolean[]</code>), use {@link Boolean#TYPE}
     * @param valueSeparator The separator supposedly used to separate elements in a same line
     * @param ignoreFirstLine A boolean value. If <code>TRUE</code>, the first line of the file is
     *            ignored
     * @param finish Value of the last line. If not <code>null</code> and such a line is found, this
     *            line is ignored and the reading stops
     * @return The expected {@link FlatMatrix}, or <code>null</code> if a problem occurred while
     *         extracting it, or if the <code>expectedType</code> did not represent a valid type.
     */
    public static FlatMatrix<Object> loadTypeFlatMatrixFromFile(BufferedReader reader, String fileName,
            Class<?> expectedType, String valueSeparator, boolean ignoreFirstLine, String finish) {
        FlatMatrix<Object> result = new FlatMatrix<Object>();
        if ((reader != null) && (expectedType != null) && (valueSeparator != null)) {
            String[] lines = extractLines(reader, ignoreFirstLine, finish);
            if (lines != null) {
                int columns = 0;
                int index = 0;
                boolean readOk = true;
                if (Boolean.TYPE.equals(expectedType)) {
                    // boolean flat matrix
                    boolean[] matrix = null;
                    for (int y = 0; readOk && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        if (matrix == null) {
                            columns = values.length;
                            matrix = new boolean[lines.length * columns];
                        }
                        for (int x = 0; readOk && (x < values.length); x++) {
                            try {
                                matrix[index++] = Boolean.parseBoolean(values[x].trim());
                            } catch (Exception e) {
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read flat matrix from file: " + fileName, e);
                                matrix = null;
                                readOk = false;
                            }
                        }
                    }
                    if (readOk) {
                        result.setValue(columns, matrix);
                    }
                } else if (Character.TYPE.equals(expectedType)) {
                    // char flat matrix
                    char[] matrix = null;
                    for (int y = 0; readOk && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        if (matrix == null) {
                            columns = values.length;
                            matrix = new char[lines.length * columns];
                        }
                        for (int x = 0; readOk && (x < values.length); x++) {
                            try {
                                String tempValue = values[x].trim();
                                if (tempValue.length() == 1) {
                                    matrix[index++] = tempValue.charAt(0);
                                } else {
                                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                            .error("Failed to read flat matrix from file: " + fileName);
                                    matrix = null;
                                    readOk = false;
                                }
                            } catch (Exception e) {
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read flat matrix from file: " + fileName, e);
                                matrix = null;
                                readOk = false;
                            }
                        }
                    }
                    if (readOk) {
                        result.setValue(columns, matrix);
                    }
                } else if (Byte.TYPE.equals(expectedType)) {
                    // byte flat matrix
                    byte[] matrix = null;
                    for (int y = 0; readOk && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        if (matrix == null) {
                            columns = values.length;
                            matrix = new byte[lines.length * columns];
                        }
                        for (int x = 0; readOk && (x < values.length); x++) {
                            try {
                                matrix[index++] = Byte.parseByte(values[x].trim());
                            } catch (Exception e) {
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read flat matrix from file: " + fileName, e);
                                matrix = null;
                                readOk = false;
                            }
                        }
                    }
                    if (readOk) {
                        result.setValue(columns, matrix);
                    }
                } else if (Short.TYPE.equals(expectedType)) {
                    // short flat matrix
                    short[] matrix = null;
                    for (int y = 0; readOk && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        if (matrix == null) {
                            columns = values.length;
                            matrix = new short[lines.length * columns];
                        }
                        for (int x = 0; readOk && (x < values.length); x++) {
                            try {
                                matrix[index++] = Short.parseShort(values[x].trim());
                            } catch (Exception e) {
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read flat matrix from file: " + fileName, e);
                                matrix = null;
                                readOk = false;
                            }
                        }
                    }
                    if (readOk) {
                        result.setValue(columns, matrix);
                    }
                } else if (Integer.TYPE.equals(expectedType)) {
                    // int flat matrix
                    int[] matrix = null;
                    for (int y = 0; readOk && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        if (matrix == null) {
                            columns = values.length;
                            matrix = new int[lines.length * columns];
                        }
                        for (int x = 0; readOk && (x < values.length); x++) {
                            try {
                                matrix[index++] = Integer.parseInt(values[x].trim());
                            } catch (Exception e) {
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read flat matrix from file: " + fileName, e);
                                matrix = null;
                                readOk = false;
                            }
                        }
                    }
                    if (readOk) {
                        result.setValue(columns, matrix);
                    }
                } else if (Long.TYPE.equals(expectedType)) {
                    // long flat matrix
                    long[] matrix = null;
                    for (int y = 0; readOk && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        if (matrix == null) {
                            columns = values.length;
                            matrix = new long[lines.length * columns];
                        }
                        for (int x = 0; readOk && (x < values.length); x++) {
                            try {
                                matrix[index++] = Long.parseLong(values[x].trim());
                            } catch (Exception e) {
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read flat matrix from file: " + fileName, e);
                                matrix = null;
                                readOk = false;
                            }
                        }
                    }
                    if (readOk) {
                        result.setValue(columns, matrix);
                    }
                } else if (Float.TYPE.equals(expectedType)) {
                    // float flat matrix
                    float[] matrix = null;
                    for (int y = 0; readOk && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        if (matrix == null) {
                            columns = values.length;
                            matrix = new float[lines.length * columns];
                        }
                        for (int x = 0; readOk && (x < values.length); x++) {
                            try {
                                matrix[index++] = parseFloat(values[x].trim());
                            } catch (Exception e) {
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read flat matrix from file: " + fileName, e);
                                matrix = null;
                                readOk = false;
                            }
                        }
                    }
                    if (readOk) {
                        result.setValue(columns, matrix);
                    }
                } else if (Double.TYPE.equals(expectedType)) {
                    // double flat matrix
                    double[] matrix = null;
                    for (int y = 0; readOk && (y < lines.length); y++) {
                        String[] values = split(lines[y], valueSeparator);
                        if (matrix == null) {
                            columns = values.length;
                            matrix = new double[lines.length * columns];
                        }
                        for (int x = 0; readOk && (x < values.length); x++) {
                            try {
                                matrix[index++] = parseDouble(values[x].trim());
                            } catch (Exception e) {
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                        .error("Failed to read flat matrix from file: " + fileName, e);
                                matrix = null;
                                readOk = false;
                            }
                        }
                    }
                    if (readOk) {
                        result.setValue(columns, matrix);
                    }
                }
            }
        }
        if (result.getValue() == null) {
            result = null;
        }
        return result;
    }

    public static void loadMatrixDataFile(AbstractMatrix<?> matrix, String loadDataFile) {
        if (loadDataFile != null) {
            FileReader fileReader = FileHelper.newFileReader(loadDataFile,
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS));
            if (fileReader != null) {
                String line = ObjectUtils.EMPTY_STRING;
                boolean first = true;
                int nbColumnMax = 0;
                StringTokenizer tokenizer = null;

                ArrayList<Object> lines = new ArrayList<Object>();

                Object tmpValues = null;

                while ((line = FileHelper.readLine(fileReader, System.out)) != null) {
                    if (matrix.isSaveColumnIndex() && first) {
                        // Ignore the first line corresponding to column name
                        first = false;
                        continue;
                    }

                    tokenizer = new StringTokenizer(line, matrix.getValueSeparator());
                    int nbToken = tokenizer.countTokens();
                    if (nbToken > nbColumnMax) {
                        nbColumnMax = nbToken;
                    }

                    tmpValues = matrix.initArray(nbToken);
                    int index = 0;
                    while (tokenizer.hasMoreElements()) {
                        Array.set(tmpValues, index, matrix.parseValue(tokenizer.nextToken()));
                        index++;
                    }

                    lines.add(tmpValues);
                }
                int nbLine = 0;
                Object[] tmpDValues = matrix.initValue(lines.size(), nbColumnMax);
                for (Object row : lines) {
                    for (int i = 0; i < nbColumnMax; i++) {
                        if (Array.getLength(row) > i) {
                            Array.set(tmpDValues[nbLine], i, Array.get(row, i));
                        }
                    }
                    nbLine++;
                }
                try {
                    matrix.setValue(tmpDValues);
                } catch (UnsupportedDataTypeException e) {
                    // Should not happen
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to set matrix value", e);
                }
            }
        }
    }

    public static void saveMatrixDataFile(AbstractMatrix<?> matrix, String fileName) {
        if (matrix.isFlatValue()) {
            FileArrayUtils.saveFlatMatrixToFile(fileName, matrix, matrix.getType(), matrix.getWidth(),
                    matrix.getValueSeparator(), matrix.isSaveColumnIndex());
        } else {
            FileArrayUtils.saveMatrixToFile(fileName, matrix.getValue(), matrix.getType(), matrix.getValueSeparator(),
                    matrix.isSaveColumnIndex());
        }
    }

    private static class ValueOfArray {

        public final String getValueOf(Object array, int... index) {
            return extractStringValue(extractLastArray(array, index), index[index.length - 1]);
        }

        protected final Object extractLastArray(Object array, int... index) {
            for (int i = 0; i < index.length - 1; i++) {
                array = Array.get(array, index[i]);
            }
            return array;
        }

        protected String extractStringValue(Object array, int index) {
            return String.valueOf(Array.get(array, index));
        }

    }

    private static class ValueOfBooleanArray extends ValueOfArray {

        @Override
        public String extractStringValue(Object matrix, int index) {
            return String.valueOf(Array.getBoolean(matrix, index));
        }
    }

    private static class ValueOfCharArray extends ValueOfArray {

        @Override
        public String extractStringValue(Object matrix, int index) {
            return String.valueOf(Array.getChar(matrix, index));
        }
    }

    private static class ValueOfByteArray extends ValueOfArray {

        @Override
        public String extractStringValue(Object matrix, int index) {
            return String.valueOf(Array.getByte(matrix, index));
        }
    }

    private static class ValueOfShortArray extends ValueOfArray {

        @Override
        public String extractStringValue(Object matrix, int index) {
            return String.valueOf(Array.getShort(matrix, index));
        }
    }

    private static class ValueOfIntArray extends ValueOfArray {

        @Override
        public String extractStringValue(Object matrix, int index) {
            return String.valueOf(Array.getInt(matrix, index));
        }
    }

    private static class ValueOfLongArray extends ValueOfArray {

        @Override
        public String extractStringValue(Object matrix, int index) {
            return String.valueOf(Array.getLong(matrix, index));
        }
    }

    private static class ValueOfFloatArray extends ValueOfArray {

        @Override
        public String extractStringValue(Object matrix, int index) {
            return String.valueOf(Array.getFloat(matrix, index));
        }
    }

    private static class ValueOfDoubleArray extends ValueOfArray {

        @Override
        public String extractStringValue(Object matrix, int index) {
            return String.valueOf(Array.getDouble(matrix, index));
        }
    }
}
