/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import fr.soleil.comete.definition.listener.IDataTransferListener;

/**
 * An {@link IComponent} that can save its data in a file, or load its data from a file
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IDataSavableComponent extends IComponent {

    /**
     * Loads data from a file
     * 
     * @param fileName The file path
     */
    public void loadDataFile(final String fileName);

    /**
     * Saves data in a file
     * 
     * @param path The file path
     */
    public void saveDataFile(String path);

    /**
     * Returns the directory for data files
     * 
     * @return A {@link String}
     */
    public String getDataDirectory();

    /**
     * Changes the directory for data files
     * 
     * @param path The directory path
     */
    public void setDataDirectory(String path);

    /**
     * Returns the last data file
     * 
     * @return A {@link String}
     */
    public String getDataFile();

    /**
     * Adds an {@link IDataTransferListener} to this {@link IDataSavableComponent}
     * 
     * @param listener The {@link IDataTransferListener} to add
     */
    public void addDataTransferListener(IDataTransferListener listener);

    /**
     * Removes an {@link IDataTransferListener} from this {@link IDataSavableComponent}
     * 
     * @param listener The {@link IDataTransferListener} to remove
     */
    public void removeDataTransferListener(IDataTransferListener listener);

}
