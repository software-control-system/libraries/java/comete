/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import java.util.EventObject;

import fr.soleil.comete.definition.widget.ISnapshotableComponent;

/**
 * An {@link EventObject} to notify for some changes in an {@link ISnapshotableComponent}'s
 * snapshots
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class SnapshotEvent extends EventObject {

    private static final long serialVersionUID = -3170290635067391315L;

    public SnapshotEvent(ISnapshotableComponent source) {
        super(source);
    }

    @Override
    public ISnapshotableComponent getSource() {
        return (ISnapshotableComponent) super.getSource();
    }

}
