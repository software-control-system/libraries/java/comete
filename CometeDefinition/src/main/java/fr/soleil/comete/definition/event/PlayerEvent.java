/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import fr.soleil.comete.definition.widget.IPlayer;

/**
 * A {@link CometeEvent} to warn for some changes about an {@link IPlayer}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class PlayerEvent extends CometeEvent<IPlayer> {

    private static final long serialVersionUID = -7703275886395642920L;

    public PlayerEvent(IPlayer source) {
        super(source);
    }

}
