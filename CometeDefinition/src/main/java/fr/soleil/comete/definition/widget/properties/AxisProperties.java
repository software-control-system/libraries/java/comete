/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateConstants;

public class AxisProperties implements Cloneable, Serializable {

    private static final long serialVersionUID = 1539951657575329031L;

    protected double scaleMin;
    protected double scaleMax;
    protected int scaleMode;
    protected boolean autoScale;
    protected int labelFormat;
    protected String title;
    protected CometeColor color;
    protected CometeFont labelFont;
    protected int gridStyle;
    protected boolean gridVisible;
    protected boolean subGridVisible;
    protected boolean isVisible;
    protected boolean drawOpposite;
    protected int position;
    protected double userLabelInterval;
    protected int titleAlignment;

    protected String dateFormat;
    protected String timeFormat;

    public AxisProperties() {
        this(0, 100, 0, true, 0, null, CometeColor.BLACK, 0, false, false, true, false, 0, Double.NaN,
                IComponent.CENTER, IDateConstants.DATE_LONG_FORMAT, IDateConstants.TIME_SHORT_FORMAT);
    }

    public AxisProperties(double scaleMin, double scaleMax, int scaleMode, boolean autoScale, int labelFormat,
            String title, CometeColor color, int gridStyle, boolean gridVisible, boolean subGridVisible,
            boolean visible, boolean drawOpposite, int position, double userLabelInterval, int titleAlignment) {
        this(scaleMin, scaleMax, scaleMode, autoScale, labelFormat, title, color, gridStyle, gridVisible,
                subGridVisible, visible, drawOpposite, position, userLabelInterval, titleAlignment,
                IDateConstants.DATE_LONG_FORMAT, IDateConstants.TIME_SHORT_FORMAT);
    }

    public AxisProperties(double scaleMin, double scaleMax, int scaleMode, boolean autoScale, int labelFormat,
            String title, CometeColor color, int gridStyle, boolean gridVisible, boolean subGridVisible,
            boolean visible, boolean drawOpposite, int position, double userLabelInterval, int titleAlignment,
            String dateFormat, String timeFormat) {
        this(scaleMin, scaleMax, scaleMode, autoScale, labelFormat, ChartProperties.DEFAULT_FONT, title, color,
                gridStyle, gridVisible, subGridVisible, visible, drawOpposite, position, userLabelInterval,
                titleAlignment, dateFormat, timeFormat);
    }

    public AxisProperties(double scaleMin, double scaleMax, int scaleMode, boolean autoScale, int labelFormat,
            CometeFont labelFont, String title, CometeColor color, int gridStyle, boolean gridVisible,
            boolean subGridVisible, boolean visible, boolean drawOpposite, int position, double userLabelInterval,
            int titleAlignment, String dateFormat, String timeFormat) {
        super();
        setScaleMin(scaleMin);
        setScaleMax(scaleMax);
        setScaleMode(scaleMode);
        setAutoScale(autoScale);
        setLabelFormat(labelFormat);
        setLabelFont(labelFont);
        setTitle(title);
        setColor(color);
        setGridStyle(gridStyle);
        setGridVisible(gridVisible);
        setSubGridVisible(subGridVisible);
        setVisible(visible);
        setDrawOpposite(drawOpposite);
        setPosition(position);
        setUserLabelInterval(userLabelInterval);
        setTitleAlignment(titleAlignment);
        setDateFormat(dateFormat);
        setTimeFormat(timeFormat);
    }

    /**
     * Returns whether the axis should use autoscale.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isAutoScale() {
        return autoScale;
    }

    /**
     * Sets whether the axis should use autoscale.
     * 
     * @param autoScale Whether the axis should use autoscale.
     */
    public void setAutoScale(boolean autoScale) {
        this.autoScale = autoScale;
    }

    /**
     * Returns the axis color.
     * 
     * @return A {@link CometeColor}.
     */
    public CometeColor getColor() {
        return color;
    }

    /**
     * Sets the axis color.
     * 
     * @param color The axis color to set.
     */
    public void setColor(CometeColor color) {
        if (color == null) {
            color = CometeColor.BLACK;
        }
        this.color = color;
    }

    /**
     * Returns the label font.
     * 
     * @return A {@link CometeFont}.
     */
    public CometeFont getLabelFont() {
        return labelFont;
    }

    /**
     * Sets the label font.
     * 
     * @param labelFont The label font to set.
     */
    public void setLabelFont(CometeFont labelFont) {
        this.labelFont = labelFont;
    }

    /**
     * Returns whether opposite axis should be drawn.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isDrawOpposite() {
        return drawOpposite;
    }

    /**
     * Sets whether opposite axis should be drawn.
     * 
     * @param drawOpposite Whether opposite axis should be drawn.
     */
    public void setDrawOpposite(boolean drawOpposite) {
        this.drawOpposite = drawOpposite;
    }

    /**
     * Returns whether the axis should be visible.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isVisible() {
        return isVisible;
    }

    /**
     * Sets whether the axis should be visible.
     * 
     * @param isVisible Whether the axis should be visible.
     */
    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    /**
     * Returns the label format
     * 
     * @return An <code>int</code>.
     * @see {@link IChartViewer} constants AUTO_FORMAT, SCIENTIFIC_FORMAT,
     *      TIME_FORMAT, DECINT_FORMAT, HEXINT_FORMAT, BININT_FORMAT,
     *      SCIENTIFICINT_FORMAT, DATE_FORMAT The labelFormat to set.
     */
    public int getLabelFormat() {
        return labelFormat;
    }

    /**
     * Sets the label format.
     * 
     * @param labelFormat The label format to set.
     * @see {@link IChartViewer} constants AUTO_FORMAT, SCIENTIFIC_FORMAT,
     *      TIME_FORMAT, DECINT_FORMAT, HEXINT_FORMAT, BININT_FORMAT,
     *      SCIENTIFICINT_FORMAT, DATE_FORMAT The labelFormat to set.
     */
    public void setLabelFormat(int labelFormat) {
        this.labelFormat = labelFormat;
    }

    /**
     * Returns the scale maximum value.
     * 
     * @return A <code>double</code>.
     */
    public double getScaleMax() {
        return scaleMax;
    }

    /**
     * Sets the scale maximum value.
     * 
     * @param scaleMax The maximum value to set.
     */
    public void setScaleMax(double scaleMax) {
        this.scaleMax = scaleMax;
    }

    /**
     * Returns the scale minimum value.
     * 
     * @return A <code>double</code>.
     */
    public double getScaleMin() {
        return scaleMin;
    }

    /**
     * Sets the scale minimum value.
     * 
     * @param scaleMin The minimum value to set.
     */
    public void setScaleMin(double scaleMin) {
        this.scaleMin = scaleMin;
    }

    /**
     * Returns the scale mode.
     * 
     * @return An <code>int</code>.
     * @see {@link IChartViewer} constants LINEAR_SCALE or LOG_SCALE
     */
    public int getScaleMode() {
        return scaleMode;
    }

    /**
     * Sets the scale mode.
     * 
     * @param scaleMode The scale mode to set.
     * @see {@link IChartViewer} constants LINEAR_SCALE or LOG_SCALE
     */
    public void setScaleMode(int scaleMode) {
        this.scaleMode = scaleMode;
    }

    /**
     * Returns the date format.
     * 
     * @return A {@link String}.
     * @see {@link IDateConstants#DATE_CLASSIC_FORMAT}
     * @see {@link IDateConstants#DATE_SHORT_FORMAT}
     */
    public String getDateFormat() {
        return dateFormat;
    }

    /**
     * Set the date format
     * 
     * @param dateFormat The date format to set. Can be {@link IDateConstants#DATE_CLASSIC_FORMAT},
     *            {@link IDateConstants#DATE_SHORT_FORMAT}, or a custom one.
     * @see {@link IDateConstants#DATE_CLASSIC_FORMAT}
     * @see {@link IDateConstants#DATE_SHORT_FORMAT}
     */
    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    /**
     * Returns the time format.
     * 
     * @return A {@link String}
     * @see {@link IDateConstants#TIME_LONG_FORMAT}
     * @see {@link IDateConstants#TIME_SHORT_FORMAT}
     */
    public String getTimeFormat() {
        return timeFormat;
    }

    /**
     * Set the time format.
     * 
     * @param timeFormat The time format to set. Can be {@link IDateConstants#TIME_LONG_FORMAT},
     *            {@link IDateConstants#TIME_SHORT_FORMAT} or a custom one.
     * @see {@link IDateConstants#TIME_LONG_FORMAT}
     * @see {@link IDateConstants#TIME_SHORT_FORMAT}
     */
    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
    }

    /**
     * Returns whether grid should be visible.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isGridVisible() {
        return gridVisible;
    }

    /**
     * Sets whether grid should be visible..
     * 
     * @param gridVisible Whether grid should be visible.
     */
    public void setGridVisible(boolean gridVisible) {
        this.gridVisible = gridVisible;
    }

    /**
     * Returns whether sub grid should be visible.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isSubGridVisible() {
        return subGridVisible;
    }

    /**
     * Sets whether sub grid should be visible.
     * 
     * @param subGridVisible whether sub grid should be visible.
     */
    public void setSubGridVisible(boolean subGridVisible) {
        this.subGridVisible = subGridVisible;
    }

    /**
     * Returns the grid style.
     * 
     * @return An <code>int</code>.
     * @see {@link IChartViewer#STYLE_SOLID}
     * @see {@link IChartViewer#STYLE_DOT}
     * @see {@link IChartViewer#STYLE_DASH}
     * @see {@link IChartViewer#STYLE_LONG_DASH}
     * @see {@link IChartViewer#STYLE_DASH_DOT}
     */
    public int getGridStyle() {
        return gridStyle;
    }

    /**
     * Sets the grid style.
     * 
     * @param gridStyle The grid style to set.
     * @see {@link IChartViewer#STYLE_SOLID}
     * @see {@link IChartViewer#STYLE_DOT}
     * @see {@link IChartViewer#STYLE_DASH}
     * @see {@link IChartViewer#STYLE_LONG_DASH}
     * @see {@link IChartViewer#STYLE_DASH_DOT}
     */
    public void setGridStyle(int gridStyle) {
        this.gridStyle = gridStyle;
    }

    /**
     * Returns the axis title.
     * 
     * @return A {@link String}.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the axis title.
     * 
     * @param title The title to set.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns the axis position.
     * 
     * @return An <code>int</code>.
     * @see {@link IChartViewer#HORIZONTAL_DOWN}
     * @see {@link IChartViewer#HORIZONTAL_UP}
     * @see {@link IChartViewer#HORIZONTAL_ORGY1}
     * @see {@link IChartViewer#HORIZONTAL_ORGY2}
     * @see {@link IChartViewer#VERTICAL_LEFT}
     * @see {@link IChartViewer#VERTICAL_RIGHT}
     * @see {@link IChartViewer#VERTICAL_ORGX}
     */
    public int getPosition() {
        return position;
    }

    /**
     * Sets the axis position.
     * 
     * @param position The position to set.
     * @see {@link IChartViewer#HORIZONTAL_DOWN}
     * @see {@link IChartViewer#HORIZONTAL_UP}
     * @see {@link IChartViewer#HORIZONTAL_ORGY1}
     * @see {@link IChartViewer#HORIZONTAL_ORGY2}
     * @see {@link IChartViewer#VERTICAL_LEFT}
     * @see {@link IChartViewer#VERTICAL_RIGHT}
     * @see {@link IChartViewer#VERTICAL_ORGX}
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * Returns the user label interval.
     * 
     * @return A <code>double</code>.
     */
    public double getUserLabelInterval() {
        return userLabelInterval;
    }

    /**
     * Sets the user label interval.
     * 
     * @param userLabelInterval The label interval to set.
     */
    public void setUserLabelInterval(double userLabelInterval) {
        this.userLabelInterval = userLabelInterval;
    }

    /**
     * Sets the title alignment.
     * 
     * @return An <code>int</code>.
     * @see {@link IComponent#CENTER}
     * @see {@link IComponent#TOP}
     * @see {@link IComponent#LEFT}
     * @see {@link IComponent#BOTTOM}
     * @see {@link IComponent#RIGHT}
     */
    public int getTitleAlignment() {
        return titleAlignment;
    }

    /**
     * Sets the title alignment.
     * 
     * @param titleAlignment The alignment to set.
     * @see {@link IComponent#CENTER}
     * @see {@link IComponent#TOP}
     * @see {@link IComponent#LEFT}
     * @see {@link IComponent#BOTTOM}
     * @see {@link IComponent#RIGHT}
     */
    public void setTitleAlignment(int titleAlignment) {
        this.titleAlignment = titleAlignment;
    }

    @Override
    public AxisProperties clone() {
        AxisProperties properties;
        try {
            properties = (AxisProperties) super.clone();
            properties.color = (properties.color == null ? null : properties.color.clone());
        } catch (CloneNotSupportedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            properties = null;
        }
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && (getClass().equals(obj.getClass()))) {
            AxisProperties properties = (AxisProperties) obj;
            equals = (ObjectUtils.sameDouble(getScaleMin(), properties.getScaleMin())
                    && ObjectUtils.sameDouble(getScaleMax(), properties.getScaleMax())
                    && (getScaleMode() == properties.getScaleMode()) && (isAutoScale() == properties.isAutoScale())
                    && (getLabelFormat() == properties.getLabelFormat())
                    && ObjectUtils.sameObject(getLabelFont(), properties.getLabelFont())
                    && ObjectUtils.sameObject(getTitle(), properties.getTitle())
                    && ObjectUtils.sameObject(getColor(), properties.getColor())
                    && (getGridStyle() == properties.getGridStyle()) && (isGridVisible() == properties.isGridVisible())
                    && (isSubGridVisible() == properties.isSubGridVisible()) && (isVisible() == properties.isVisible())
                    && (isDrawOpposite() == properties.isDrawOpposite()) && (getPosition() == properties.getPosition())
                    && (ObjectUtils.sameObject(getDateFormat(), properties.getDateFormat()))
                    && (ObjectUtils.sameObject(getTimeFormat(), properties.getTimeFormat()))
                    && ObjectUtils.sameDouble(getUserLabelInterval(), properties.getUserLabelInterval())
                    && (getTitleAlignment() == properties.getTitleAlignment()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0xA72E;
        int mult = 6;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + (int) getScaleMin();
        hashCode = hashCode * mult + (int) getScaleMax();
        hashCode = hashCode * mult + getScaleMode();
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isAutoScale());
        hashCode = hashCode * mult + getLabelFormat();
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getLabelFont());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getTitle());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getColor());
        hashCode = hashCode * mult + getGridStyle();
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isGridVisible());
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isSubGridVisible());
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isVisible());
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isDrawOpposite());
        hashCode = hashCode * mult + getPosition();
        hashCode = hashCode * mult + (int) getUserLabelInterval();
        hashCode = hashCode * mult + getTitleAlignment();
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getDateFormat());
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getTimeFormat());
        return hashCode;
    }

}
