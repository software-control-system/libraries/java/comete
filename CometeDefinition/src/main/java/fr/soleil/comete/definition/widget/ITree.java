/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import fr.soleil.comete.definition.data.target.complex.ITreeNodeTarget;
import fr.soleil.comete.definition.listener.ITreeNodeSelectionListener;
import fr.soleil.comete.definition.widget.util.ITreeNode;

/**
 * An interface for tree {@link IComponent}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ITree extends ITreeNodeTarget, IComponent {

    // Compatible with SWT Constant
    public static final int MULTI_SELECTION = 2;

    public static final int SINGLE_SELECTION = 4;

    /**
     * Returns the {@link ITreeNode} that corresponds to a given position
     * 
     * @param x The x coordinate
     * @param y The y coordinate
     * @return An {@link ITreeNode}
     */
    public ITreeNode getNodeAt(int x, int y);

    /**
     * Selects some {@link ITreeNode}s, with or without keeping previous selection
     * 
     * @param keepPreviousSelection Whether to keep previous selection. If <code>true</code>, the
     *            given {@link ITreeNode}s will only be added to selection. If <code>false</code>,
     *            the given {@link ITreeNode}s will be the only selected ones.
     * @param nodes The {@link ITreeNode}s to select
     */
    public void selectNodes(boolean keepPreviousSelection, ITreeNode... nodes);

    /**
     * Removes some {@link ITreeNode}s from selection
     * 
     * @param nodes The {@link ITreeNode}s to deselect
     */
    public void deselectNodes(ITreeNode... nodes);

    /**
     * Deselects all {@link ITreeNode}s
     */
    public void clearSelection();

    /**
     * Adds an {@link ITreeNodeSelectionListener} to this {@link ITree}
     * 
     * @param listener The {@link ITreeNodeSelectionListener} to add
     */
    public void addTreeNodeSelectionListener(ITreeNodeSelectionListener listener);

    /**
     * Removes an {@link ITreeNodeSelectionListener} from this {@link ITree}
     * 
     * @param listener The {@link ITreeNodeSelectionListener} to remove
     */
    public void removeTreeNodeSelectionListener(ITreeNodeSelectionListener listener);

    /**
     * Removes all {@link ITreeNodeSelectionListener}s from this {@link ITree}
     */
    public void removeAllTreeNodeSelectionListeners();

    /**
     * Returns whether a given {@link ITreeNodeSelectionListener} is registered as listening to this
     * {@link ITree}
     * 
     * @param listener The {@link ITreeNodeSelectionListener} to check
     * @return a <code>boolean</code> value
     */
    public boolean hasTreeNodeSelectionListener(ITreeNodeSelectionListener listener);

    public void setSelectionMode(int selectionMode);

    public int getSelectionMode();

}
