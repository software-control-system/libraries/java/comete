/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.adapter;

import java.util.Map;

import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;

/**
 * Can transform a given {@link AbstractDataSource} {@link Map} to a given {@link ITarget} rank 3
 * array of <code>double</code>, and vice versa. This adapter is associated with {@link ChartTargetController}.
 * 
 * @author huriez
 * 
 */
public class DataArrayAdapter<U> extends DataSourceAdapter<U, Map<String, Object>> {

    // private boolean dualMode;

    /**
     * Constructor with the {@link AbstractDataSource} and the {@link TypeConverter} associated
     * 
     * @param dataSource the data source.
     * @param converter the primitive type converter for this adapter.
     */
    public DataArrayAdapter(final AbstractDataSource<U> dataSource) {
        super(dataSource, new ObjectToStringMapAdapter<U>());
    }

    /**
     * @return the splitMatrixThroughColumns
     */
    public boolean isSplitMatrixThroughColumns() {
        return ((ObjectToStringMapAdapter<U>) adapter).isSplitMatrixThroughColumns();
    }

    /**
     * @param splitMatrixThroughColumns the splitMatrixThroughColumns to set
     */
    public void setSplitMatrixThroughColumns(boolean splitMatrixThroughColumns) {
        ((ObjectToStringMapAdapter<U>) adapter).setSplitMatrixThroughColumns(splitMatrixThroughColumns);
    }

    @Override
    protected U adaptContainerData(final Map<String, Object> data) {
        return null;
    }

    @Override
    protected Map<String, Object> adaptSourceData(final U data) throws DataAdaptationException {
        ((ObjectToStringMapAdapter<U>) adapter).setMapKey(dataSource.toString());
        return super.adaptSourceData(data);
    }
}
