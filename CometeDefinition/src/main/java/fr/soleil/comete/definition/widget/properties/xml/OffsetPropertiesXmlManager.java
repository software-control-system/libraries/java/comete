/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.OffsetProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and {@link OffsetProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class OffsetPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "offset";
    public static final String A0_PROPERTY_XML_TAG = "a0";

    /**
     * Transforms some {@link OffsetProperties} into an {@link XMLLine}
     * 
     * @param properties The {@link OffsetProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(OffsetProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttribute(A0_PROPERTY_XML_TAG, String.valueOf(properties.getA0()));
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link OffsetProperties}
     * 
     * @param currentSubNode {@link Node} to interpret
     * @return The recovered {@link OffsetProperties}
     */
    public static OffsetProperties loadOffset(Node currentSubNode) {
        return loadOffsetFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Map} to recover some {@link OffsetProperties}
     * 
     * @param propertyMap The {@link Map} to interpret. The {@link Map} is supposed to contain some xml node attributes
     * @return The recovered {@link OffsetProperties}
     */
    public static OffsetProperties loadOffsetFromXmlNodeAttributes(final Map<String, String> propertyMap) {
        OffsetProperties transformation = new OffsetProperties();
        if (propertyMap != null) {
            String a0_s = propertyMap.get(A0_PROPERTY_XML_TAG);
            double a0 = parseDouble(a0_s, transformation.getA0());
            transformation.setA0(a0);
        }
        return transformation;
    }

}
