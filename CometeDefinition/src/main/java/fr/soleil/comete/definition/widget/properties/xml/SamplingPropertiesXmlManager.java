/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and {@link SamplingProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class SamplingPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "sampling";
    public static final String ENABLED_PROPERTY_XML_TAG = "enabled";
    public static final String MEAN_PROPERTY_XML_TAG = "mean";
    public static final String PEAK_PROPERTY_XML_TAG = "peak";

    /**
     * Transforms some {@link SamplingProperties} into an {@link XMLLine}
     * 
     * @param properties The {@link SamplingProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(SamplingProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttribute(ENABLED_PROPERTY_XML_TAG, String.valueOf(properties.isEnabled()));
            openingLine.setAttribute(MEAN_PROPERTY_XML_TAG, String.valueOf(properties.getMean()));
            openingLine.setAttribute(PEAK_PROPERTY_XML_TAG, String.valueOf(properties.getPeakType()));
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link SamplingProperties}
     * 
     * @param currentSubNode The {@link Node} to interpret
     * @return The recovered {@link SamplingProperties}
     */
    public static SamplingProperties loadSampling(Node currentSubNode) {
        return loadSamplingFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Map} to recover some {@link SamplingProperties}
     * 
     * @param propertyMap The {@link Map} to interpret. The {@link Map} is supposed to contain some xml node attributes
     * @return The recovered {@link SamplingProperties}
     */
    public static SamplingProperties loadSamplingFromXmlNodeAttributes(final Map<String, String> propertyMap) {
        SamplingProperties sampling = new SamplingProperties();
        if (propertyMap != null) {
            String enabled_s = propertyMap.get(ENABLED_PROPERTY_XML_TAG);
            String mean_s = propertyMap.get(MEAN_PROPERTY_XML_TAG);
            String peak_s = propertyMap.get(PEAK_PROPERTY_XML_TAG);

            boolean enabled;
            int mean, peak;
            enabled = parseBoolean(enabled_s, sampling.isEnabled());
            mean = parseInt(mean_s, sampling.getMean());
            peak = parseInt(peak_s, sampling.getPeakType());
            sampling.setEnabled(enabled);
            sampling.setMean(mean);
            sampling.setPeakType(peak);
        }
        return sampling;
    }

}
