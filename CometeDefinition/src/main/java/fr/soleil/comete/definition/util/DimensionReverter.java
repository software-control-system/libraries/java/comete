/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

/**
 * An {@link AbstractValueConvertor} used to revert axis direction, based on a given dimension size.
 * The idea is: instead of going from <code>0</code> to <code>dimSize - 1</code>, go from
 * <code>dimSize - 1</code> to <code>0</code>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DimensionReverter extends AbstractValueConvertor {

    private int dimSize;

    /**
     * Constructs a new {@link DimensionReverter}
     * 
     * @param dimSize The dimension size to use
     */
    public DimensionReverter(int dimSize) {
        super();
        this.dimSize = dimSize;
    }

    @Override
    public double convertValue(double value) {
        return dimSize - value;
    }

    @Override
    public boolean isValid() {
        return (dimSize > 0);
    }

}
