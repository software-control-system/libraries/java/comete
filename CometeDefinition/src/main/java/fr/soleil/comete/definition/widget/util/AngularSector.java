/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import fr.soleil.comete.definition.event.MathematicSectorEvent;

public class AngularSector extends MathematicSector {

    private static final String ANGULAR_SECTOR = "Angular Sector";
    public static final String X0_KEY = "x0";
    public static final String Y0_KEY = "y0"; // former z0
    public static final String Z0_KEY = "z0";
    public static final String CENTER_X = "centerX";
    public static final String CENTER_Z = "centerZ";
    public static final String MEAN_PSI_KEY = "Mean_Psi";
    public static final String DELTA_PSI_KEY = "Delta_Psi";
    private static final String X0_NAME = "X\u2080";
    private static final String Z0_NAME = "Z\u2080";
    private static final String MEAN_PSI_NAME = "Mean \u03C8";
    private static final String DELTA_PSI_NAME = "\u0394\u03C8";
    private static final String PIXELS = "px";
    private static final String DEGREES = "\u00B0";
    private static final String DESCRIPTION_START = "Expected value:\nA floating value (double), so that:\n";
    private static final String INFERIOR_COORDINATE_DESCRIPTION = "-2\u00b3\u00b9\u2264";
    private static final String SUPERIOR_COORDINATE_DESCRIPTION = "\u22642\u00b3\u00b9-1";
    private static final String INFERIOR_ANGLE_DESCRIPTION = "0\u2264";
    private static final String SUPERIOR_ANGLE_DESCRIPTION = "\u2264180\u00B0";
    private static final String X0_DESCRIPTION = "X coordinate of the sector referent point";
    private static final String Z0_DESCRIPTION = "Z coordinate of the sector referent point";
    private static final String MEAN_PSI_DESCRIPTION = "Angle to position the sector, based on horizontal";
    private static final String DELTA_PSI_DESCRIPTION = "Aperture angle";

    private final static String[] VARIABLE_NAMES = { X0_KEY, Z0_KEY, MEAN_PSI_KEY, DELTA_PSI_KEY };

    private Double x0, z0, meanPsi, deltaPsi;

    public AngularSector() {
        super();
    }

    @Override
    public Mask generateMask(int dimX, int dimY) {
        boolean[] maskedZone = null;
        if ((x0 != null) && (z0 != null) && (meanPsi != null) && (deltaPsi != null)) {
            maskedZone = getSectorMatrix(x0.doubleValue(), z0.doubleValue(), dimX, dimY, meanPsi.intValue(),
                    deltaPsi.intValue());
        }
        Mask mask;
        try {
            mask = new Mask(maskedZone, dimX, dimY, getDescription());
        } catch (MaskException e) {
            mask = null;
        }
        return mask;
    }

    @Override
    public String getDescription() {
        return ANGULAR_SECTOR;
    }

    @Override
    public Number getParameter(String name) {
        Number parameter;
        if (X0_KEY.equals(name)) {
            parameter = x0;
        } else if (Z0_KEY.equals(name) || Y0_KEY.equals(name)) {
            parameter = z0;
        } else if (MEAN_PSI_KEY.equals(name)) {
            parameter = meanPsi;
        } else if (DELTA_PSI_KEY.equals(name)) {
            parameter = deltaPsi;
        } else {
            parameter = null;
        }
        return parameter;
    }

    @Override
    public String parameterToString(String name, double... beamPoint) {
        String result;
        if ((beamPoint != null) && (beamPoint.length == 2)) {
            if (X0_KEY.equals(name)) {
                if ((x0 != null) && (x0.doubleValue() == beamPoint[0])) {
                    result = CENTER_X;
                } else {
                    result = String.valueOf(x0);
                }
            } else if (Z0_KEY.equals(name) || Y0_KEY.equals(name)) {
                if ((z0 != null) && (z0.doubleValue() == beamPoint[1])) {
                    result = CENTER_Z;
                } else {
                    result = String.valueOf(z0);
                }
            } else {
                result = super.parameterToString(name, beamPoint);
            }
        } else {
            result = super.parameterToString(name, beamPoint);
        }
        return result;
    }

    @Override
    public String[] getParameterNames() {
        return VARIABLE_NAMES;
    }

    @Override
    public String getDisplayableName(String name) {
        String displayableName;
        if (X0_KEY.equals(name)) {
            displayableName = X0_NAME;
        } else if (Z0_KEY.equals(name) || Y0_KEY.equals(name)) {
            displayableName = Z0_NAME;
        } else if (MEAN_PSI_KEY.equals(name)) {
            displayableName = MEAN_PSI_NAME;
        } else if (DELTA_PSI_KEY.equals(name)) {
            displayableName = DELTA_PSI_NAME;
        } else {
            displayableName = null;
        }
        return displayableName;
    }

    @Override
    public String getParameterUnit(String name) {
        String unit;
        if (X0_KEY.equals(name) || Z0_KEY.equals(name) || Y0_KEY.equals(name)) {
            unit = PIXELS;
        } else if (MEAN_PSI_KEY.equals(name) || DELTA_PSI_KEY.equals(name)) {
            unit = DEGREES;
        } else {
            unit = null;
        }
        return unit;
    }

    @Override
    public boolean isValidParameterValue(String name, String value) {
        boolean valid;
        if (X0_KEY.equals(name) || Z0_KEY.equals(name) || Y0_KEY.equals(name)) {
            try {
                Double.valueOf(value);
                valid = true;
            } catch (Exception e) {
                valid = false;
            }
        } else if (MEAN_PSI_KEY.equals(name) || DELTA_PSI_KEY.equals(name)) {
            try {
                double tempValue = Double.parseDouble(value);
                if ((tempValue < 0) || (tempValue > 180)) {
                    valid = false;
                } else {
                    valid = true;
                }
            } catch (Exception e) {
                valid = false;
            }
        } else {
            valid = false;
        }
        return valid;
    }

    @Override
    public String getValidValuesDescription(String name) {
        String description;
        StringBuilder buffer = new StringBuilder(DESCRIPTION_START);
        if (X0_KEY.equals(name) || Z0_KEY.equals(name) || Y0_KEY.equals(name)) {
            buffer.append(INFERIOR_COORDINATE_DESCRIPTION).append(getDisplayableName(name))
                    .append(SUPERIOR_COORDINATE_DESCRIPTION);
            description = buffer.toString();
        } else if (MEAN_PSI_KEY.equals(name) || DELTA_PSI_KEY.equals(name)) {
            buffer.append(INFERIOR_ANGLE_DESCRIPTION).append(getDisplayableName(name))
                    .append(SUPERIOR_ANGLE_DESCRIPTION);
            description = buffer.toString();
        } else {
            description = null;
        }
        return description;
    }

    @Override
    public String getParameterDescription(String name) {
        String description;
        if (X0_KEY.equals(name)) {
            description = X0_DESCRIPTION;
        } else if (Z0_KEY.equals(name) || Y0_KEY.equals(name)) {
            description = Z0_DESCRIPTION;
        } else if (MEAN_PSI_KEY.equals(name)) {
            description = MEAN_PSI_DESCRIPTION;
        } else if (DELTA_PSI_KEY.equals(name)) {
            description = DELTA_PSI_DESCRIPTION;
        } else {
            description = null;
        }
        return description;
    }

    @Override
    public void setParameterWithString(String name, String value, double... beamPoint)
            throws MathematicSectorException {
        if (X0_KEY.equals(name)) {
            try {
                double tempValue;
                if (CENTER_X.equals(value) && (beamPoint != null) && (beamPoint.length == 2)) {
                    tempValue = beamPoint[0];
                } else {
                    tempValue = Double.parseDouble(value);
                }
                if (Double.isNaN(tempValue) || (tempValue < Integer.MIN_VALUE) || (tempValue > Integer.MAX_VALUE)) {
                    throw generateDefaultValueException(name);
                } else {
                    x0 = tempValue;
                    fireMathematicSectorEvent(MathematicSectorEvent.REASON_VALUE);
                }
            } catch (Exception e) {
                if (e instanceof MathematicSectorException) {
                    throw (MathematicSectorException) e;
                } else {
                    throw generateDefaultValueException(name, e);
                }
            }
        } else if (Z0_KEY.equals(name) || Y0_KEY.equals(name)) {
            try {
                double tempValue;
                if (CENTER_Z.equals(value) && (beamPoint != null) && (beamPoint.length == 2)) {
                    tempValue = beamPoint[1];
                } else {
                    tempValue = Double.parseDouble(value);
                }
                if (Double.isNaN(tempValue) || (tempValue < Integer.MIN_VALUE) || (tempValue > Integer.MAX_VALUE)) {
                    throw generateDefaultValueException(name);
                } else {
                    z0 = tempValue;
                    fireMathematicSectorEvent(MathematicSectorEvent.REASON_VALUE);
                }
            } catch (Exception e) {
                if (e instanceof MathematicSectorException) {
                    throw (MathematicSectorException) e;
                } else {
                    throw generateDefaultValueException(name, e);
                }
            }
        } else if (MEAN_PSI_KEY.equals(name)) {
            try {
                double tempValue = Double.parseDouble(value);
                if ((tempValue < 0) || (tempValue > 180)) {
                    throw generateDefaultValueException(name);
                } else {
                    meanPsi = Double.valueOf(tempValue);
                    fireMathematicSectorEvent(MathematicSectorEvent.REASON_VALUE);
                }
            } catch (Exception e) {
                if (e instanceof MathematicSectorException) {
                    throw (MathematicSectorException) e;
                } else {
                    throw generateDefaultValueException(name, e);
                }
            }
        } else if (DELTA_PSI_KEY.equals(name)) {
            try {
                double tempValue = Double.parseDouble(value);
                if ((tempValue < 0) || (tempValue > 180)) {
                    throw generateDefaultValueException(name);
                } else {
                    deltaPsi = Double.valueOf(tempValue);
                    fireMathematicSectorEvent(MathematicSectorEvent.REASON_VALUE);
                }
            } catch (Exception e) {
                if (e instanceof MathematicSectorException) {
                    throw (MathematicSectorException) e;
                } else {
                    throw generateDefaultValueException(name, e);
                }
            }
        } else {
            throw generateDefaultNameException(name);
        }
    }

    @Override
    protected void setParameter(String name, Number value) throws MathematicSectorException {
        if (value == null) {
            throw generateDefaultValueException(name);
        } else {
            if (X0_KEY.equals(name)) {
                x0 = Double.valueOf(value.doubleValue());
                fireMathematicSectorEvent(MathematicSectorEvent.REASON_VALUE);
            } else if (Z0_KEY.equals(name) || Y0_KEY.equals(name)) {
                z0 = Double.valueOf(value.doubleValue());
                fireMathematicSectorEvent(MathematicSectorEvent.REASON_VALUE);
            } else if (MEAN_PSI_KEY.equals(name)) {
                if ((value.doubleValue() < 0) || (value.doubleValue() > 180)) {
                    throw generateDefaultValueException(name);
                } else {
                    meanPsi = Double.valueOf(value.doubleValue());
                    fireMathematicSectorEvent(MathematicSectorEvent.REASON_VALUE);
                }
            } else if (DELTA_PSI_KEY.equals(name)) {
                if ((value.doubleValue() < 0) || (value.doubleValue() > 180)) {
                    throw generateDefaultValueException(name);
                } else {
                    deltaPsi = Double.valueOf(value.doubleValue());
                    fireMathematicSectorEvent(MathematicSectorEvent.REASON_VALUE);
                }
            } else {
                throw generateDefaultNameException(name);
            }
        }
    }

    @Override
    public AngularSector clone() {
        return (AngularSector) super.clone();
    }

    @Override
    public AngularSector clone(boolean transmitListeners) {
        return (AngularSector) super.clone(transmitListeners);
    }

    /**
     * Calculates a boolean matrix that corresponds to a sector and its symmetric. The sector is an
     * angle obtained that way:
     * <ul>
     * <li>There is a referent point. Let's call it "center"</li>
     * <li>An angle, mean&psi;, is given. This angle is the one made with horizontal right vector. Its unit is &deg;
     * (degrees)</li>
     * <li>Another angle, &Delta;&psi;, is given. It defines the aperture angle. Its unit is &deg; (degrees). This means
     * that the sector is the angle between [mean&psi; - (&Delta;&psi;/2), mean&psi; + (&Delta;&psi;/2)], starting to
     * "center" point</li>
     * <li>The returned matrix is a picture matrix. Each value is <code>TRUE</code> if the corresponding coordinate in
     * picture is in the sector or its symmetric, <code>FALSE</code> otherwise.</li>
     * </ul>
     * 
     * @param x0 The center x coordinate in picture coordinates. Musts be between {@link Integer#MIN_VALUE} and
     *            {@link Integer#MAX_VALUE}.
     * @param y0 The center y coordinate in picture coordinates. Musts be between {@link Integer#MIN_VALUE} and
     *            {@link Integer#MAX_VALUE}.
     * @param dimX The picture x dimension. Musts be &ge;0
     * @param dimY The picture y dimension. Musts be &ge;0
     * @param meanPsi The mean&psi; angle (The angle made with the horizontal). Musts be between 0&deg; and 180&deg;
     * @param deltaPsi The &Delta;&psi; angle (The aperture angle). Musts be between 0&deg; and 180&deg;
     * @return A boolean flat matrix. <code>null</code> if center coordinates are out of {@link Integer} range, or if
     *         there is a picture dimension < 0, or if at least 1 of the 2 angles is not between 0&deg; and 180&deg;
     */
    protected static boolean[] getSectorMatrix(double x0, double y0, int dimX, int dimY, double meanPsi,
            double deltaPsi) {
        boolean[] result;
        if ((dimX < 0) || (dimY < 0) || (x0 > Integer.MAX_VALUE) || (x0 < Integer.MIN_VALUE) || (y0 > Integer.MAX_VALUE)
                || (y0 < Integer.MIN_VALUE) || (meanPsi < 0) || (meanPsi > 180) || (deltaPsi < 0) || (deltaPsi > 180)) {
            result = null;
        } else {
            int fx0 = (int) Math.floor(x0);
            fx0 = Math.max(0, fx0);
            fx0 = Math.min(dimX, fx0);
            boolean isIntX0 = (x0 + 0.5 == Math.floor(x0 + 0.5));
            // ---------------------------
            // Left part (from 90 to 270°)
            // ---------------------------
            if (meanPsi < 90) {
                // meanPsi not in left part : take symmetric
                meanPsi = meanPsi + 180;
            }
            double halfAngle = deltaPsi / 2.0d;
            double upperAngle = meanPsi + halfAngle;
            double lowerAngle = meanPsi - halfAngle;
            double lowerRest = 90 - lowerAngle; // how much is going under 90°
            double upperRest = upperAngle - 270; // how much is going after 270°
            double lowerLimit = Math.max(90, lowerAngle);
            double upperLimit = Math.min(270, upperAngle);
            // limit case excluded, as identified for meanPsi = deltaPsi = 60° and similar cases
            boolean tooFarUp = ((upperLimit != upperAngle)/* || (upperLimit == 270)*/);
            boolean tooFarDown = ((lowerLimit != lowerAngle) || (lowerLimit == 90));
            result = new boolean[dimY * dimX];
            lowerLimit = Math.toRadians(lowerLimit);
            upperLimit = Math.toRadians(upperLimit);
            // Sub sectors from symmetric sector that go again in left part
            lowerRest = Math.toRadians(270 - lowerRest);
            upperRest = Math.toRadians(90 + upperRest);
            // is limit case included in sector ?
            boolean is90 = ((upperAngle >= 90) && (lowerAngle <= 90)) || ((upperAngle >= 270) && (lowerAngle <= 270));
            double lowerLimitTan = Math.tan(lowerLimit);
            double upperLimitTan = Math.tan(upperLimit);
            double lowerRestTan = Math.tan(lowerRest);
            double upperRestTan = Math.tan(upperRest);
            fillSectorMatrix(result, dimX, dimY, x0, y0, fx0, 0, true, isIntX0, is90, tooFarUp, tooFarDown,
                    upperLimitTan, upperRestTan, lowerLimitTan, lowerRestTan);

            // ----------------------------
            // Right part (from -90 to 90°)
            // ----------------------------
            if (meanPsi > 90) {
                // meanPsi not in right part : take symmetric
                meanPsi = meanPsi - 180;
            }
            upperAngle = meanPsi + halfAngle;
            lowerAngle = meanPsi - halfAngle;
            lowerRest = -90 - lowerAngle;
            upperRest = upperAngle - 90;
            lowerLimit = Math.max(-90, lowerAngle);
            upperLimit = Math.min(90, upperAngle);
            tooFarUp = ((upperLimit != upperAngle) || (upperLimit == 90));
            tooFarDown = ((lowerLimit != lowerAngle) || (lowerLimit == -90));
            lowerLimit = Math.toRadians(lowerLimit);
            upperLimit = Math.toRadians(upperLimit);
            lowerRest = Math.toRadians(90 - lowerRest);
            upperRest = Math.toRadians(-90 + upperRest);
            lowerLimitTan = Math.tan(lowerLimit);
            upperLimitTan = Math.tan(upperLimit);
            lowerRestTan = Math.tan(lowerRest);
            upperRestTan = Math.tan(upperRest);
            int xStart = fx0 + 1;
            if (x0 < 0) {
                xStart = 0;
            }
            fillSectorMatrix(result, dimX, dimY, x0, y0, fx0, xStart, false, isIntX0, is90, tooFarUp, tooFarDown,
                    upperLimitTan, upperRestTan, lowerLimitTan, lowerRestTan);
        }
        return result;
    }

    protected static void fillSectorMatrix(boolean[] result, int dimX, int dimY, double x0, double y0, int fx0,
            int xStart, boolean limitToX0, boolean isIntX0, boolean is90, boolean tooFarUp, boolean tooFarDown,
            double upperLimitTan, double upperRestTan, double lowerLimitTan, double lowerRestTan) {
        // 2 cases:
        // - Left part (from 90 to 270°)
        // - Right part (from -90 to 90°)
        for (int y = 0; y < dimY; y++) {
            double y1 = y0 - (y + 0.5);
            for (int x = xStart; x < dimX; x++) {
                double x1 = x + 0.5 - x0;
                if (limitToX0 && (x1 >= 0)) {
                    break;
                }
                // In picture coordinates, y gets bigger when going down.
                // For tangent calculation, we expect y getting bigger when going up
                double tan = y1 / x1;
                if (tooFarDown) {
                    // - Case Left part: The lower limit of the sector goes across right part
                    // - Case Right part: The lower limit of the sector goes across left part
                    if (tooFarUp) {
                        // limit case : the whole picture is included in the sector and its
                        // symmetric
                        result[y * dimX + x] = true;
                    } else {
                        // - Case Left part:
                        // x and y are in sector if their corresponding angle is between 90° and upperLimit,
                        // or between the symmetric of lower rest and 270°
                        // - Case Right part:
                        // x and y are in sector if their corresponding angle is between -90° and upperLimit,
                        // or between the symmetric of lower rest and 90°
                        result[y * dimX + x] = ((tan <= upperLimitTan) || (tan >= lowerRestTan));
                    }
                } else {
                    if (tooFarUp) {
                        // - Case Left part:
                        // The upper limit of the sector goes across right part
                        // x and y are in sector if their corresponding angle is between lowerLimit and 270°,
                        // or between 90° and the symmetric of upper rest
                        // - Case Right part:
                        // The upper limit of the sector goes across left part
                        // x and y are in sector if their corresponding angle is between lowerLimit and 90°,
                        // or between -90° and the symmetric of upper rest
                        result[y * dimX + x] = ((tan >= lowerLimitTan) || (tan <= upperRestTan));
                    } else {
                        // Classic case : angle must be between lower limit and upper limit
                        result[y * dimX + x] = ((tan >= lowerLimitTan) && (tan <= upperLimitTan));
                    }
                }
            }
            if (isIntX0) {
                result[y * dimX + fx0] = is90 || (y == y0);
            }
        }
    }

}
