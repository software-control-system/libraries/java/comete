/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.HtmlEscape;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

public class ImagePropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "imageParams";
    protected static final String GRADIENT_XML_TAG = "gradient";
    protected static final String COUNT_ATTRIBUTE = "count";
    protected static final String COLOR_ATTRIBUTE = "color";
    protected static final String POSITION_ATTRIBUTE = "position";
    protected static final String FIT_MAX_SIZE_ATTRIBUTE = "fitMaxSize";
    protected static final String BEST_FIT_ATTRIBUTE = "bestFit";
    protected static final String FIT_MIN_ATTRIBUTE = "fitMin";
    protected static final String FIT_MAX_ATTRIBUTE = "fitMax";
    protected static final String ZOOM_ATTRIBUTE = "zoom";
    protected static final String LOG_SCALE_ATTRIBUTE = "logScale";
    protected static final String ROI_XML_TAG = "roi";
    protected static final String ROI_ATTRIBUTE = "roiMacro";

    public static String toXmlString(ImageProperties properties) {
        List<XMLLine> lines = new ArrayList<XMLLine>();
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.OPENING_TAG_CATEGORY);
        lines.add(openingLine);
        if (properties != null) {
            openingLine.setAttribute(FIT_MAX_SIZE_ATTRIBUTE, Boolean.toString(properties.isFitMaxSize()));
            openingLine.setAttribute(BEST_FIT_ATTRIBUTE, Boolean.toString(properties.isBestfit()));
            openingLine.setAttribute(LOG_SCALE_ATTRIBUTE, Boolean.toString(properties.isLogScale()));
            openingLine.setAttribute(FIT_MIN_ATTRIBUTE, Double.toString(properties.getFitMin()));
            openingLine.setAttribute(FIT_MAX_ATTRIBUTE, Double.toString(properties.getFitMax()));
            openingLine.setAttribute(ZOOM_ATTRIBUTE, Double.toString(properties.getZoom()));
            String[] rois = properties.getRois();
            if (rois == null) {
                openingLine.setAttribute(COUNT_ATTRIBUTE, "0");
            } else {
                openingLine.setAttribute(COUNT_ATTRIBUTE, Integer.toString(rois.length));
                int i = 0;
                for (String roi : rois) {
                    XMLLine roiLine = new XMLLine(ROI_XML_TAG + "_" + i++, XMLLine.EMPTY_TAG_CATEGORY);
                    roiLine.setAttribute(ROI_ATTRIBUTE,
                            HtmlEscape.escape(roi.replace(ObjectUtils.NEW_LINE, ";").replace(";;", ";"), false, false));
                    lines.add(roiLine);
                }
            }
            Gradient gradient = properties.getGradient();
            if (gradient != null) {
                int count = gradient.getEntryNumber();
                XMLLine gradientOpeningLine = new XMLLine(GRADIENT_XML_TAG, XMLLine.OPENING_TAG_CATEGORY);
                gradientOpeningLine.setAttribute(COUNT_ATTRIBUTE, Integer.toString(count));
                lines.add(gradientOpeningLine);
                for (int i = 0; i < count; i++) {
                    XMLLine colorLine = new XMLLine(GRADIENT_XML_TAG + "_" + i, XMLLine.EMPTY_TAG_CATEGORY);
                    colorLine.setAttribute(POSITION_ATTRIBUTE, Double.toString(gradient.getPosAt(i)));
                    colorLine.setAttribute(COLOR_ATTRIBUTE, colorToString(gradient.getColorAt(i)));
                    lines.add(colorLine);
                }
                lines.add(new XMLLine(GRADIENT_XML_TAG, XMLLine.CLOSING_TAG_CATEGORY));
            }
        }
        lines.add(new XMLLine(XML_TAG, XMLLine.CLOSING_TAG_CATEGORY));
        StringBuilder builder = new StringBuilder();
        for (XMLLine line : lines) {
            line.toAppendable(builder).append(ObjectUtils.NEW_LINE);
        }
        return builder.toString().trim();
    }

    public static ImageProperties loadImageProperties(Node currentNode) {
        ImageProperties properties = null;
        if (currentNode != null) {
            Map<String, String> propertyMap = XMLUtils.loadAttributes(currentNode);
            boolean fitMaxSize, bestfit, logScale;
            double fitMin, fitMax, zoom;
            int roiCount;
            fitMaxSize = parseBoolean(propertyMap.get(FIT_MAX_SIZE_ATTRIBUTE), false);
            bestfit = parseBoolean(propertyMap.get(BEST_FIT_ATTRIBUTE), false);
            logScale = parseBoolean(propertyMap.get(LOG_SCALE_ATTRIBUTE), false);
            fitMin = parseDouble(propertyMap.get(FIT_MIN_ATTRIBUTE), Double.NaN);
            fitMax = parseDouble(propertyMap.get(FIT_MAX_ATTRIBUTE), Double.NaN);
            zoom = parseDouble(propertyMap.get(ZOOM_ATTRIBUTE), Double.NaN);
            roiCount = parseInt(propertyMap.get(COUNT_ATTRIBUTE), 0);
            String[] rois = new String[roiCount];
            Gradient gradient = null;
            if (currentNode.hasChildNodes()) {
                NodeList subNodes = currentNode.getChildNodes();
                for (int i = 0; i < subNodes.getLength(); i++) {
                    Node currentSubNode = subNodes.item(i);
                    if (!XMLUtils.isAFakeNode(currentSubNode)) {
                        String nodeName = currentSubNode.getNodeName();
                        if (nodeName != null) {
                            if (GRADIENT_XML_TAG.equals(nodeName)) {
                                gradient = loadGradient(currentSubNode);
                            } else if (nodeName.startsWith(ROI_XML_TAG)) {
                                updateRois(rois, currentSubNode);
                            }
                        }
                    }
                }
            }
            properties = new ImageProperties(fitMaxSize, bestfit, fitMin, fitMax, zoom, rois, gradient, logScale);
        }
        return properties;
    }

    protected static Gradient loadGradient(Node currentNode) {
        Map<String, String> propertyMap = XMLUtils.loadAttributes(currentNode);
        int count;
        Gradient gradient = null;
        count = parseInt(propertyMap.get(COUNT_ATTRIBUTE), 0);
        if (count > 0) {
            gradient = new Gradient();
            gradient.buildEmptyGradient(count);
            if (currentNode.hasChildNodes()) {
                NodeList subNodes = currentNode.getChildNodes();
                for (int i = 0; i < subNodes.getLength(); i++) {
                    Node currentSubNode = subNodes.item(i);
                    if (!XMLUtils.isAFakeNode(currentSubNode)) {
                        String nodeName = currentSubNode.getNodeName();
                        if ((nodeName != null) && nodeName.startsWith(GRADIENT_XML_TAG)) {
                            updateGradient(gradient, currentSubNode);
                        }
                    }
                }
            }
        }
        return gradient;
    }

    protected static void updateGradient(Gradient gradient, Node currentNode) {
        if ((gradient != null) && (currentNode != null)) {
            String nodeName = currentNode.getNodeName();
            Map<String, String> propertyMap = XMLUtils.loadAttributes(currentNode);
            try {
                int index = Integer.parseInt(nodeName.substring(GRADIENT_XML_TAG.length() + 1));
                CometeColor color = stringToColor(propertyMap.get(COLOR_ATTRIBUTE));
                double position = Double.parseDouble(propertyMap.get(POSITION_ATTRIBUTE));
                if (index < gradient.getEntryNumber()) {
                    gradient.setColorAt(index, color);
                    gradient.setPosAt(index, position);
                } else {
                    gradient.addEntry(color, position);
                }
            } catch (Exception e) {
                // nothing to do: do not update gradient
            }
        }
    }

    protected static void updateRois(String[] rois, Node currentNode) {
        if ((rois != null) && (currentNode != null)) {
            String nodeName = currentNode.getNodeName();
            Map<String, String> propertyMap = XMLUtils.loadAttributes(currentNode);
            try {
                int index = Integer.parseInt(nodeName.substring(ROI_XML_TAG.length() + 1));
                if (index < rois.length) {
                    rois[index] = HtmlEscape.unEscape(propertyMap.get(ROI_ATTRIBUTE), false, false).replace(";", ";\n")
                            .trim();
                }
            } catch (Exception e) {
                // nothing to do: do not update rois
            }
        }
    }

}
