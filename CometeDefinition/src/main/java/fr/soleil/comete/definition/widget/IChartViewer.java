/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import java.util.Map;

import fr.soleil.comete.definition.data.target.complex.IDataArrayComponent;
import fr.soleil.comete.definition.data.target.complex.IDataArrayTarget;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.DragProperties;
import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.comete.definition.widget.properties.OffsetProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.data.target.IMultiFormatableTarget;
import fr.soleil.lib.project.date.IDateFormattable;

public interface IChartViewer extends IDataArrayTarget, IMultiFormatableTarget, IDataArrayComponent,
        ISnapshotableComponent, IDataSavableComponent {

    /**
     * Identifier of the Y1 axis
     */
    public static final int Y1 = 0;
    /**
     * Identifier of the Y2 axis
     */
    public static final int Y2 = 1;
    /**
     * Identifier of the X axis
     */
    public static final int X = 2;

    // Sampling
    /** Use curve global mean as reference for sampling */
    public static final int GLOBAL_MEAN = 0;
    /** Use sample local mean as reference for sampling */
    public static final int LOCAL_MEAN = 1;
    /** Represents the fact you prefer to use the greatest distance to mean in sampling */
    public static final int PEAK_TYPE_GREATEST = 0;
    /** Represents the fact you prefer to use the sample minimum in sampling */
    public static final int PEAK_TYPE_MIN = 1;
    /** Represents the fact you prefer to use the sample maximum in sampling */
    public static final int PEAK_TYPE_MAX = 2;
    /** Represents the fact you prefer to use the lowest peak distance to mean in sampling */
    public static final int PEAK_TYPE_LOWEST = 3;
    /**
     * Represents the fact you prefer to use the in sampling:
     * <ul>
     * <li>Either the local mean (if desired mean is local mean)</li>
     * <li>Or the closest value to global mean (if desired mean is global mean)</li>
     * </ul>
     */
    public static final int PEAK_TYPE_CLOSEST = 4;

    // dataviews
    /**
     * X_INDEX indicates at which index to store x value in a <code>double</code> array of length &ge; 2
     */
    public static final int X_INDEX = 0;
    /**
     * Y_INDEX indicates at which index to store y value in a <code>double</code> array of length &ge; 2
     */
    public static final int Y_INDEX = 1;
    /**
     * ERROR_INDEX indicates at which index to store error value in a <code>double</code> array of length &ge; 3
     */
    public static final int ERROR_INDEX = 2;

    /** No marker displayed */
    public static final int MARKER_NONE = 0;
    /** Display a dot for each point of the view */
    public static final int MARKER_DOT = 1;
    /** Display a box for each point of the view */
    public static final int MARKER_BOX = 2;
    /** Display a triangle for each point of the view */
    public static final int MARKER_TRIANGLE = 3;
    /** Display a diamond for each point of the view */
    public static final int MARKER_DIAMOND = 4;
    /** Display a start for each point of the view */
    public static final int MARKER_STAR = 5;
    /** Display a vertical line for each point of the view */
    public static final int MARKER_VERT_LINE = 6;
    /** Display an horizontal line for each point of the view */
    public static final int MARKER_HORIZ_LINE = 7;
    /** Display a cross for each point of the view */
    public static final int MARKER_CROSS = 8;
    /** Display a circle for each point of the view */
    public static final int MARKER_CIRCLE = 9;
    /** Display a square for each point of the view */
    public static final int MARKER_SQUARE = 10;

    /** Solid line style */
    public static final int STYLE_SOLID = 0;
    /** Dot line style */
    public static final int STYLE_DOT = 1;
    /** Dash line style */
    public static final int STYLE_DASH = 2;
    /** Long Dash line style */
    public static final int STYLE_LONG_DASH = 3;
    /** Dash + Dot line style */
    public static final int STYLE_DASH_DOT = 4;

    /** Use linear scale for this axis */
    public static final int LINEAR_SCALE = 0;
    /** Use logarithmic scale for this axis */
    public static final int LOG_SCALE = 1;

    /** Line style */
    public static final int TYPE_LINE = 0;

    /** BarGraph style */
    public static final int TYPE_BAR = 1;

    /** Stairs style */
    public static final int TYPE_STAIRS = 2;

    /** No filling */
    public static final int FILL_STYLE_NONE = 0;
    /** Solid fill style */
    public static final int FILL_STYLE_SOLID = 1;
    /** Hatch fill style */
    public static final int FILL_STYLE_LARGE_RIGHT_HATCH = 2;
    /** Hatch fill style */
    public static final int FILL_STYLE_LARGE_LEFT_HATCH = 3;
    /** Hatch fill style */
    public static final int FILL_STYLE_LARGE_CROSS_HATCH = 4;
    /** Hatch fill style */
    public static final int FILL_STYLE_SMALL_RIGHT_HATCH = 5;
    /** Hatch fill style */
    public static final int FILL_STYLE_SMALL_LEFT_HATCH = 6;
    /** Hatch fill style */
    public static final int FILL_STYLE_SMALL_CROSS_HATCH = 7;
    /** Hatch fill style */
    public static final int FILL_STYLE_DOT_PATTERN_1 = 8;
    /** Hatch fill style */
    public static final int FILL_STYLE_DOT_PATTERN_2 = 9;
    /** Hatch fill style */
    public static final int FILL_STYLE_DOT_PATTERN_3 = 10;

    /** No interpolation */
    public static final int INTERPOLATE_NONE = 0;
    /** Linear interpolation method */
    public static final int INTERPOLATE_LINEAR = 1;
    /** Cosine interpolation method */
    public static final int INTERPOLATE_COSINE = 2;
    /* Cubic interpolation method (Require constant x interval) */
    public static final int INTERPOLATE_CUBIC = 3;
    /* Hermite interpolation method */
    public static final int INTERPOLATE_HERMITE = 4;

    /** No smoothing */
    public static final int SMOOTH_NONE = 0;
    /** Flat smoothing (Flat shape) */
    public static final int SMOOTH_FLAT = 1;
    /** Linear smoothing (Triangular shape) */
    public static final int SMOOTH_TRIANGULAR = 2;
    /** Gaussian smoothing (Gaussian shape) */
    public static final int SMOOTH_GAUSSIAN = 3;

    /** No smoothing extrapolation */
    public static final int SMOOTH_EXT_NONE = 0;
    /** flat smoothing extrapolation (duplicate last and end value) */
    public static final int SMOOTH_EXT_FLAT = 1;
    /** Linear smoothing extrapolation (linear extrapolation) */
    public static final int SMOOTH_EXT_LINEAR = 2;

    /** No mathematical operation */
    public static final int MATH_NONE = 0;
    /** Derivative operation */
    public static final int MATH_DERIVATIVE = 1;
    /** Integral operation */
    public static final int MATH_INTEGRAL = 2;
    /** FFT (modulus) operation */
    public static final int MATH_FFT_MODULUS = 3;
    /** FFT (phase) operation */
    public static final int MATH_FFT_PHASE = 4;

    /** Place label at the bottom of the chart */
    public static final int LABEL_DOWN = 0;
    /** Place label at the top of the chart */
    public static final int LABEL_UP = 1;
    /** Place label at the right of the chart */
    public static final int LABEL_RIGHT = 2;
    /** Place label at the left of the chart */
    public static final int LABEL_LEFT = 3;
    /** Place label at the bottom of the chart and try to arrange them in rows */
    public static final int LABEL_ROW = 4;

    /** Use default compiler format to display double */
    public static final int AUTO_FORMAT = 0;
    /** Display value using exponential representation (x.xxEyy) */
    public static final int SCIENTIFIC_FORMAT = 1;
    /** Display number of second as HH:MM:SS */
    public static final int TIME_FORMAT = 2;
    /** Display integer using decimal format */
    public static final int DECINT_FORMAT = 3;
    /** Display integer using haxadecimal format */
    public static final int HEXINT_FORMAT = 4;
    /** Display integer using binary format */
    public static final int BININT_FORMAT = 5;
    /** Display value using exponential representation (xEyy) */
    public static final int SCIENTIFICINT_FORMAT = 6;
    /** Display value as date */
    public static final int DATE_FORMAT = 7;

    /** Fill curve and bar from the top of the graph */
    public static final int METHOD_FILL_FROM_TOP = 0;

    /** Fill curve and bar from zero position (on axis) */
    public static final int METHOD_FILL_FROM_ZERO = 1;

    /** Fill curve and bar from the bottom of the graph */
    public static final int METHOD_FILL_FROM_BOTTOM = 2;

    /** Drag disabled */
    public static final int DRAG_NONE = -1;
    /** Drag can occur only on X */
    public static final int DRAG_X = 0;
    /** Drag can occur only on Y */
    public static final int DRAG_Y = 1;
    /** Drag can occur only on both X and Y */
    public static final int DRAG_XY = 2;

    /**
     * Curves offset through user drag disabled.
     * 
     * @deprecated Use {@link #DRAG_NONE} instead.
     */
    @Deprecated
    public static final int DRAG_OFFSET_NONE = DRAG_NONE;
    /**
     * Curves can receive x offset through user drag.
     * 
     * @deprecated Use {@link #DRAG_OFFSET_X} instead.
     */
    @Deprecated
    public static final int DRAG_OFFSET_X = DRAG_X;
    /**
     * Curves can receive y offset through user drag.
     * 
     * @deprecated Use {@link #DRAG_Y} instead.
     */
    @Deprecated
    public static final int DRAG_OFFSET_Y = DRAG_Y;
    /**
     * Curves can receive both x and y offsets through user drag.
     * 
     * @deprecated Use {@link #DRAG_XY} instead.
     */
    @Deprecated
    public static final int DRAG_OFFSET_XY = DRAG_XY;

    /**
     * Default amount of pixels before considering a scale drag occurred.
     */
    public static final int DEFAULT_SCALE_DRAG_SENTIVITY = 5;

    // constant
    /** Horizontal axis at bottom of the chart */
    public static final int HORIZONTAL_DOWN = 1;
    /** Horizontal axis at top of the chart */
    public static final int HORIZONTAL_UP = 2;
    /** Horizontal axis at 0 position (on Y1) */
    public static final int HORIZONTAL_ORGY1 = 3;
    /** Horizontal axis at 0 position (on Y2) */
    public static final int HORIZONTAL_ORGY2 = 4;
    /** Vertical right axis */
    public static final int VERTICAL_RIGHT = 5;
    /** Vertical left axis */
    public static final int VERTICAL_LEFT = 6;
    /** Vertical axis at X=0 */
    public static final int VERTICAL_ORGX = 7;

    /** US date format to format labels as dates */
    public static final String US_DATE_FORMAT = IDateFormattable.US_DATE_FORMAT;
    /** French date format to format labels as dates */
    public static final String FR_DATE_FORMAT = IDateFormattable.FR_DATE_FORMAT;
    // TODO Add all necessary date format
    public static final String DATE_SEPARATOR = IDateFormattable.DATE_SEPARATOR;
    public static final String YEAR_LONG_FORMAT = IDateFormattable.YEAR_LONG_FORMAT;
    public static final String YEAR_SHORT_FORMAT = IDateFormattable.YEAR_SHORT_FORMAT;
    public static final String MONTH_FORMAT = IDateFormattable.MONTH_FORMAT;
    public static final String DAY_FORMAT = IDateFormattable.DAY_FORMAT;
    public static final String DAY_WEEK_FORMAT = IDateFormattable.DAY_WEEK_FORMAT;

    public static final String DATE_LONG_FORMAT = IDateFormattable.DATE_LONG_FORMAT;
    public static final String DATE_SHORT_FORMAT = IDateFormattable.DATE_SHORT_FORMAT;
    public static final String DATE_DAY_FORMAT = IDateFormattable.DATE_DAY_FORMAT;

    public static final String TIME_SEPARATOR = IDateFormattable.TIME_SEPARATOR;
    public static final String HOUR_FORMAT = IDateFormattable.HOUR_FORMAT;
    public static final String MINUTE_FORMAT = IDateFormattable.MINUTE_FORMAT;
    public static final String SECOND_FORMAT = IDateFormattable.SECOND_FORMAT;
    public static final String MILLI_SECOND_FORMAT = IDateFormattable.MILLI_SECOND_FORMAT;

    public static final String TIME_LONG_FORMAT = IDateFormattable.TIME_LONG_FORMAT;
    public static final String TIME_SHORT_FORMAT = IDateFormattable.TIME_SHORT_FORMAT;
    public static final String HOUR_MINUTE_FORMAT = IDateFormattable.HOUR_MINUTE_FORMAT;

    public static final String DATAVIEW_PROPERTY_ACTION_NAME = "chkChange Property of DataView_";
    public static final String DATAVIEW_DELETE_ACTION_NAME = "chkDelete Property of DataView_";
    public static final String CHART_PROPERTY_ACTION_NAME = "chkChange " + IChartViewer.class.getSimpleName() + "_";

    public static final int HIGHLIGHT_ADD = 0;
    public static final int HIGHLIGHT_MULTIPLY = 1;

    /**
     * Set the chart background (curve area)
     * 
     * @param c Background color
     * @see CometeColor
     */
    public void setChartCometeBackground(CometeColor c);

    /**
     * Returns the chart background (curve area)
     * 
     * @return A {@link CometeColor}.
     */
    public CometeColor getChartCometeBackground();

    /**
     * Add a listener on this {@link IChartViewer}
     * 
     * @param listener the listener
     * @see IChartViewerListener
     */
    public void addChartViewerListener(final IChartViewerListener listener);

    /**
     * Removes a listener from this {@link IChartViewer}
     * 
     * @param listener the listeenr
     */
    public void removeChartViewerListener(final IChartViewerListener listener);

    /**
     * Notifies listeners for some changes in data
     * 
     * @param data The changed data
     */
    public void fireDataChanged(Map<String, Object> data);

    /**
     * Notifies listeners for some changes in configuration
     * 
     * @param id The configuration identifier
     */
    public void fireConfigurationChanged(String id);

    /**
     * Set the data table editable
     * 
     * @param editable
     */
    public void setEditable(boolean editable);

    /**
     * Returns whether the data tabke is editable
     * 
     * @return A <code>boolean</code>
     */
    public boolean isEditable();

    /**
     * Returns the axis name
     * 
     * @param axis the axis id
     * @return A {@link String}
     * @see #Y1
     * @see #Y2
     * @see #X
     */
    public String getAxisName(int axis);

    /**
     * Sets axis name.
     * 
     * @param name the name
     * @param axis the axis id
     * @see #Y1
     * @see #Y2
     * @see #X
     */
    public void setAxisName(final String name, final int axis);

    /**
     * Returns the axis title alignment
     * 
     * @param axis the axis
     * 
     * @return an int value
     * 
     * @see IComponent#TOP
     * @see IComponent#LEFT
     * @see IComponent#BOTTOM
     * @see IComponent#RIGHT
     */
    public int getAxisTitleAlignment(final int axis);

    /**
     * Sets the axis title alignment
     * 
     * @param align the alignment to set
     * @param axis the axis to set
     * 
     * @see IComponent#TOP
     * @see IComponent#LEFT
     * @see IComponent#BOTTOM
     * @see IComponent#RIGHT
     */
    public void setAxisTitleAlignment(final int align, final int axis);

    /**
     * Sets axis color.
     * 
     * @param color the color of the axis
     * @param axis the axis id
     * @see #Y1
     * @see #Y2
     * @see #X
     * @see CometeColor
     */
    public void setAxisCometeColor(final CometeColor color, final int axis);

    /**
     * Returns the color of an axis
     * 
     * @param axis The axis id
     * @return A {@link CometeColor}
     */
    public CometeColor getAxisCometeColor(final int axis);

    /**
     * Sets the grid style of an axis.
     * 
     * @param style Style of the grid. Can be one of the following:
     * @param axis The axis id
     * @see #STYLE_SOLID
     * @see #STYLE_DOT
     * @see #STYLE_DASH
     * @see #STYLE_LONG_DASH
     * @see #STYLE_DASH_DOT
     */
    public void setGridStyle(final int style, final int axis);

    /**
     * Returns the grid style of an axis
     * 
     * @param axis The axis id
     * @return An <code>int</code>
     * @see #setGridStyle(int, int)
     */
    public int getGridStyle(final int axis);

    /**
     * Sets a date format on the axis if it is TIME ANNOTATION
     * 
     * @param format The date format. Can be {@link #US_DATE_FORMAT} or {@link #FR_DATE_FORMAT} or custom e.g
     *            yyyy/MM/dd HH:mm:ss.SSS
     * @param axis the axis id
     * @see #US_DATE_FORMAT
     * @see #FR_DATE_FORMAT
     * @see #Y1
     * @see #Y2
     * @see #X
     */
    public void setAxisDateFormat(final String format, final int axis);

    /**
     * Returns the date format of an axis
     * 
     * @param axis the axis id
     * @return A {@link String}
     * @see #setAxisDateFormat(String, int)
     */
    public String getAxisDateFormat(final int axis);

    /**
     * Set whether the sub grid should be visible
     * 
     * @param visible whether the sub grid should be visible
     * @param axis the axis id
     * @see #Y1
     * @see #Y2
     * @see #X
     */
    public void setSubGridVisible(boolean visible, int axis);

    /**
     * Returns whether the sub grid is visible
     * 
     * @param axis the axis id
     * @return A <code>boolean</code>
     */
    public boolean isSubGridVisible(int axis);

    /**
     * Set whether the grid should be visible
     * 
     * @param visible whether the grid should be visible
     * @param axis the axis id
     * @see #Y1
     * @see #Y2
     * @see #X
     */
    public void setGridVisible(boolean visible, int axis);

    /**
     * Returns whether the grid is visible
     * 
     * @param axis the axis id
     * @return A <code>boolean</code>
     */
    public boolean isGridVisible(int axis);

    /**
     * Sets whether X axis should be placed at the bottom of the display zone
     * 
     * @param b whether X axis should be placed at the bottom of the display zone
     */
    public void setXAxisOnBottom(boolean b);

    /**
     * Sets whether X axis is placed at the bottom of the display zone
     * 
     * @return A <code>boolean</code>
     */
    public boolean isXAxisOnBottom();

    /**
     * Returns whether management panel is visible
     * 
     * @return A <code>boolean</code>
     */
    public boolean isManagementPanelVisible();

    /**
     * Sets whether management panel should be visible
     * 
     * @param visible whether management panel should be visible
     */
    public void setManagementPanelVisible(boolean visible);

    /**
     * Returns whether mathematical expressions are enabled
     * 
     * @return A <code>boolean</code>
     */
    public boolean isMathExpressionEnabled();

    /**
     * Sets whether mathematical expressions should be enabled
     * 
     * @param enable whether mathematical expressions should be enabled
     */
    public void setMathExpressionEnabled(boolean enable);

    /**
     * Sets the display duration
     * 
     * @param duration the display duration
     */
    public void setDisplayDuration(double duration);

    /**
     * Returns the display duration
     * 
     * @return A <code>double</code>
     */
    public double getDisplayDuration();

    /**
     * Sets the title to use
     * 
     * @param s the title to use
     */
    public void setHeader(String s);

    /**
     * Returns the title
     * 
     * @return A {@link String}
     */
    public String getHeader();

    /**
     * Sets the title color
     * 
     * @param c the title color
     */
    public void setHeaderCometeColor(CometeColor c);

    /**
     * Returns the title color
     * 
     * @return A {@link CometeColor}
     */
    public CometeColor getHeaderCometeColor();

    /**
     * Sets the title font
     * 
     * @param f the title font
     */
    public void setHeaderCometeFont(CometeFont f);

    /**
     * Returns the title font
     * 
     * @return A {@link CometeFont}
     */
    public CometeFont getHeaderCometeFont();

    /**
     * Sets whether title should be visible
     * 
     * @param visible whether title should be visible
     */
    public void setHeaderVisible(boolean visible);

    /**
     * Returns whether title is visible
     * 
     * @return A <code>boolean</code>
     */
    public boolean isHeaderVisible();

    /**
     * Sets the time precision
     * 
     * @param precision the time precision
     */
    public void setTimePrecision(int precision);

    /**
     * Returns the time precision
     * 
     * @return An <code>int</code>
     */
    public int getTimePrecision();

    /**
     * Sets whether freeze panel should be visible
     * 
     * @param freezePanelVisible whether freeze panel should be visible
     */
    public void setFreezePanelVisible(boolean freezePanelVisible);

    /**
     * Returns whether freeze panel is visible
     * 
     * @return A <code>boolean</code>
     */
    public boolean isFreezePanelVisible();

    /**
     * Sets the name of the date column
     * 
     * @param dateColumnName the name of the date column
     */
    public void setDateColumnName(String dateColumnName);

    /**
     * Returns the name of the date column
     * 
     * @return A {@link String}
     */
    public String getDateColumnName();

    /**
     * Sets the name of the index columns
     * 
     * @param name The name to use
     */
    public void setIndexColumnName(String name);

    /**
     * Returns the name of the index columns
     * 
     * @return A {@link String}
     */
    public String getIndexColumnName();

    /**
     * Returns whether legend is visible
     * 
     * @return A <code>boolean</code>
     */
    public boolean isLegendVisible();

    /**
     * Sets whether legend should be visible
     * 
     * @param b whether legend should be visible
     */
    public void setLegendVisible(boolean b);

    /**
     * Sets whether an axis should use automatic scale
     * 
     * @param autoscale whether the axis should use automatic scale
     * @param axis The axis id
     */
    public void setAutoScale(boolean autoscale, int axis);

    /**
     * Returns whether an axis uses automatic scale
     * 
     * @param axis The axis id
     * @return A <code>boolean</code>
     */
    public boolean isAutoScale(int axis);

    /**
     * Sets the graph type (Line or BarGraph) of a data view
     * 
     * @param id The data view identifier
     * @param style Type of graph
     * @see #TYPE_LINE
     * @see #TYPE_BAR
     */
    public void setDataViewStyle(String id, int style);

    /**
     * Returns the graph type (Line or BarGraph) of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     */
    public int getDataViewStyle(String id);

    /**
     * Sets the filling style of a data view.
     * 
     * @param id The data view identifier
     * @param style Filling style
     * @see #FILL_STYLE_NONE
     * @see #FILL_STYLE_SOLID
     * @see #FILL_STYLE_LARGE_RIGHT_HATCH
     * @see #FILL_STYLE_LARGE_LEFT_HATCH
     * @see #FILL_STYLE_LARGE_CROSS_HATCH
     * @see #FILL_STYLE_SMALL_RIGHT_HATCH
     * @see #FILL_STYLE_SMALL_LEFT_HATCH
     * @see #FILL_STYLE_SMALL_CROSS_HATCH
     * @see #FILL_STYLE_DOT_PATTERN_1
     * @see #FILL_STYLE_DOT_PATTERN_2
     * @see #FILL_STYLE_DOT_PATTERN_3
     */
    public void setDataViewFillStyle(String id, int style);

    /**
     * Returns the filling style of a data view.
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     * @see #setDataViewFillStyle(String, int)
     */
    public int getDataViewFillStyle(String id);

    /**
     * Sets the filling color of a data view.
     * 
     * @param id The data view identifier
     * @param color The filling color
     */
    public void setDataViewFillCometeColor(String id, CometeColor color);

    /**
     * Returns the filling color of a data view.
     * 
     * @param id The data view identifier
     * @return A {@link CometeColor}
     */
    public CometeColor getDataViewFillCometeColor(String id);

    /**
     * Sets the line drawing style of a data view
     * 
     * @param id The data view identifier
     * @param style the line drawing style to use
     * @see #STYLE_DASH
     * @see #STYLE_DASH_DOT
     * @see #STYLE_DOT
     * @see #STYLE_LONG_DASH
     * @see #STYLE_SOLID
     */
    public void setDataViewLineStyle(String id, int style);

    /**
     * Returns the line drawing style of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     * @see #setDataViewLineStyle(String, int)
     */
    public int getDataViewLineStyle(String id);

    /**
     * Sets the line width of a data view
     * 
     * @param id The data view identifier
     * @param lineWidth The line width
     */
    public void setDataViewLineWidth(String id, int lineWidth);

    /**
     * Returns the line width of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     */
    public int getDataViewLineWidth(String id);

    /**
     * Sets the bar width of a data view
     * 
     * @param id The data view identifier
     * @param barWidth the bar width
     */
    public void setDataViewBarWidth(String id, int barWidth);

    /**
     * Returns the bar width of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     */
    public int getDataViewBarWidth(String id);

    /**
     * Sets the line color of a data view
     * 
     * @param id The data view identifier
     * @param color the line color to use
     */
    public void setDataViewCometeColor(String id, CometeColor color);

    /**
     * Returns the line color of a data view
     * 
     * @param id The data view identifier
     * @return A {@link CometeColor}
     */
    public CometeColor getDataViewCometeColor(String id);

    /**
     * Sets the marker style of a data view
     * 
     * @param id The data view identifier
     * @param marker The marker style
     * @see #MARKER_BOX
     * @see #MARKER_CIRCLE
     * @see #MARKER_CROSS
     * @see #MARKER_DIAMOND
     * @see #MARKER_DOT
     * @see #MARKER_HORIZ_LINE
     * @see #MARKER_NONE
     * @see #MARKER_SQUARE
     * @see #MARKER_STAR
     * @see #MARKER_TRIANGLE
     * @see #MARKER_VERT_LINE
     */
    public void setDataViewMarkerStyle(String id, int marker);

    /**
     * Returns the marker style of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     * @see #setDataViewMarkerStyle(String, int)
     */
    public int getDataViewMarkerStyle(String id);

    /**
     * Sets the marker color of a data view
     * 
     * @param id The data view identifier
     * @param color the marker color
     */
    public void setDataViewMarkerCometeColor(String id, CometeColor color);

    /**
     * Returns the marker color of a data view
     * 
     * @param id The data view identifier
     * @return A {@link CometeColor}
     */
    public CometeColor getDataViewMarkerCometeColor(String id);

    /**
     * Sets the color of the error bars of a data view
     * 
     * @param id The data view identifier
     * @param color the color to use
     */
    public void setDataViewErrorCometeColor(String id, CometeColor color);

    /**
     * Returns the color of the error bars of a data view
     * 
     * @param id The data view identifier
     * @return A {@link CometeColor}
     */
    public CometeColor getDataViewErrorCometeColor(String id);

    /**
     * Sets the marker size of a data view
     * 
     * @param id The data view identifier
     * @param size the marker size
     */
    public void setDataViewMarkerSize(String id, int size);

    /**
     * Returns the marker size of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     */
    public int getDataViewMarkerSize(String id);

    /**
     * Sets the axis on which a data view should be displayed
     * 
     * @param id The data view identifier
     * @param axis the axis to use
     * @see #X
     * @see #Y1
     * @see #Y2
     */
    public void setDataViewAxis(String id, int axis);

    /**
     * Returns the axis on which a data view is displayed
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     * @see #setDataViewAxis(String, int)
     */
    public int getDataViewAxis(String id);

    /**
     * Sets the display name of a data view
     * 
     * @param id The data view identifier
     * @param displayName the display name
     */
    public void setDataViewDisplayName(String id, String displayName);

    /**
     * Returns the display name of a data view
     * 
     * @param id The data view identifier
     * @return A {@link String}
     */
    public String getDataViewDisplayName(String id);

    /**
     * Sets the legend font
     * 
     * @param f the font to use
     */
    public void setLabelCometeFont(CometeFont f);

    /**
     * Returns the legend font
     * 
     * @return A {@link CometeFont}
     */
    public CometeFont getLabelCometeFont();

    /**
     * Sets the scale of an axis
     * 
     * @param scale The scale
     * @param axis The axis id
     * @see #LOG_SCALE
     * @see #LINEAR_SCALE
     */
    public void setScale(int scale, int axis);

    /**
     * Returns the scale of an axis
     * 
     * @param axis The axis id
     * @return An <code>int</code>
     */
    public int getScale(int axis);

    /**
     * Returns whether this {@link IChartViewer} has a data view with given id
     * 
     * @param id The data view id
     * @return A <code>boolean</code>
     */
    public boolean containsDataView(String id);

    /**
     * Sets whether data table should be displayed in a dialog instead of a frame
     * 
     * @param preferDialog whether data table should be displayed in a dialog instead of a frame
     * @param modal whether the dialog should be modal
     */
    public void setPreferDialogForTable(boolean preferDialog, boolean modal);

    /**
     * Set the position of the legend.
     * 
     * @param p the position of the legend.
     * @see #LABEL_UP
     * @see #LABEL_DOWN
     * @see #LABEL_ROW
     * @see #LABEL_LEFT
     * @see #LABEL_RIGHT
     */
    public void setLabelPlacement(int p);

    /**
     * Returns position of the legend
     * 
     * @return An <code>int</code>
     * @see #setLabelPlacement(int)
     */
    public int getLabelPlacement();

    /**
     * Set the proportion of the legend in the chart, i.e. the amount of space in width or height <i>(depending on
     * {@link #getLabelPlacement()})</i> accorded to the legend.
     * 
     * @param p The legend proportion to set. Must be either a value respecting
     *            <code>0 &le; legendProportion &le; 1</code>, or <code>NaN</code>.
     *            Any invalid value will be considered as <code>NaN</code>.
     */
    public void setLegendProportion(double p);

    /**
     * Returns the proportion of the legend in the chart, i.e. the amount of space in width or height <i>(depending on
     * {@link #getLabelPlacement()})</i> accorded to the legend.
     * 
     * @return A <code>double</code>: <code>0 &le; legendProportion &le; 1</code>, or <code>NaN</code>.
     * @see #setLegendProportion(int)
     */
    public double getLegendProportion();

    /**
     * Used with saveDataFile(). Sets the String used to represent "no data"
     * (default : "*").
     * 
     * @param noValueString The String used to represent "no data"
     */
    public void setNoValueString(String noValueString);

    /**
     * Returns The String used to represent "no data"
     * 
     * @return A {@link String}
     */
    public String getNoValueString();

    /**
     * Sets the maximum on an axis
     * 
     * @param maximum the maximum to set
     * @param axis The axis id
     */
    public void setAxisMaximum(final double maximum, final int axis);

    /**
     * Returns the maximum on an axis
     * 
     * @param axis The axis id
     * @return A <code>double</code>
     */
    public double getAxisMaximum(final int axis);

    /**
     * Sets the minimum on an axis
     * 
     * @param minimum the minimum to set
     * @param axis The axis id
     */
    public void setAxisMinimum(final double minimum, final int axis);

    /**
     * Returns the minimum on an axis
     * 
     * @param axis The axis id
     * @return A <code>double</code>
     */
    public double getAxisMinimum(final int axis);

    /**
     * Sets the filling method for a data view
     * 
     * @param id The data view identifier
     * @param m the filling method to use
     * @see #METHOD_FILL_FROM_TOP
     * @see #METHOD_FILL_FROM_ZERO
     * @see #METHOD_FILL_FROM_BOTTOM
     */
    public void setDataViewFillMethod(String id, int m);

    /**
     * Returns the filling method for a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     * @see #setDataViewFillMethod(String, int)
     */
    public int getDataViewFillMethod(String id);

    /**
     * Sets whether to draw a second axis at the opposite side of an axis.
     * 
     * @param opposite <code>true</code> to enable opposite axis
     * @param axis The axis id
     */
    public void setAxisDrawOpposite(boolean opposite, final int axis);

    /**
     * Returns whether a second axis will be drawn at opposite side of a given axis
     * 
     * @param axis The axis id
     * @return A <code>boolean</code>
     */
    public boolean isAxisDrawOpposite(final int axis);

    /**
     * Sets the axis label format.
     * 
     * @param format Format of values displayed on axis on tool tip.
     * @param axis The axis identifier
     * @see #AUTO_FORMAT
     * @see #SCIENTIFIC_FORMAT
     * @see #TIME_FORMAT
     * @see #DECINT_FORMAT
     * @see #HEXINT_FORMAT
     * @see #BININT_FORMAT
     * @see #SCIENTIFICINT_FORMAT
     * @see #DATE_FORMAT
     * @see #getLabelFormat
     */
    public void setAxisLabelFormat(int format, final int axis);

    /**
     * Returns an axis label format
     * 
     * @param axis The axis identifier
     * @return An <code>int</code>
     * @see #setAxisLabelFormat(int, int)
     */
    public int getAxisLabelFormat(final int axis);

    /**
     * Sets the axis position
     * 
     * @param position Axis position
     * @see #VERTICAL_LEFT
     * @see #VERTICAL_RIGHT
     * @see #VERTICAL_ORGX
     * @see #HORIZONTAL_DOWN
     * @see #HORIZONTAL_UP
     * @see #HORIZONTAL_ORGY1
     * @see #HORIZONTAL_ORGY2
     */
    public void setAxisPosition(int position, final int axis);

    /**
     * Returns an axis position
     * 
     * @param axis The axis identifier
     * @return An <code>int</code>
     * @see #setAxisPosition(int, int)
     */
    public int getAxisPosition(final int axis);

    /**
     * Display or hide the axis.
     * 
     * @param visible True to make the axis visible.
     * @param axis The axis identifier
     */
    public void setAxisVisible(boolean visible, final int axis);

    /**
     * Returns whether an axis is visible
     * 
     * @param axis The axis identifier
     * @return A <code>boolean</code>
     */
    public boolean isAxisVisible(final int axis);

    /**
     * Returns whether a given axis uses a time scale
     * 
     * @param axis The axis identifier
     * @return A <code>boolean</code>
     */
    public boolean isTimeScale(int axis);

    /**
     * Sets the custom color attribution order
     * 
     * @param colorList the custom color attribution order
     */
    public void setCustomCometeColor(CometeColor[] colorList);

    /**
     * Returns the custom color attribution order
     * 
     * @return A {@link CometeColor} array
     */
    public CometeColor[] getCustomCometeColor();

    /**
     * Sets the custom fill color attribution order
     * 
     * @param colorList the custom fill color attribution order
     */
    public void setCustomFillCometeColor(CometeColor[] colorList);

    /**
     * Returns the custom fill color attribution order
     * 
     * @return A {@link CometeColor} array
     */
    public CometeColor[] getCustomFillCometeColor();

    /**
     * Sets the custom marker color attribution order
     * 
     * @param colorList the custom marker color attribution order
     */
    public void setCustomMarkerCometeColor(CometeColor[] colorList);

    /**
     * Returns the custom marker color attribution order
     * 
     * @return A {@link CometeColor} array
     */
    public CometeColor[] getCustomMarkerCometeColor();

    /**
     * Sets the custom line style attribution order
     * 
     * @param lineStyleList the custom line style attribution order
     */
    public void setCustomLineStyle(int[] lineStyleList);

    /**
     * Returns the custom line style attribution order
     * 
     * @return An <code>int[]</code>
     */
    public int[] getCustomLineStyle();

    /**
     * Sets the custom marker style attribution order
     * 
     * @param styleList the custom marker style attribution order
     */
    public void setCustomMarkerStyle(int[] styleList);

    /**
     * Returns the custom marker style attribution order
     * 
     * @return An <code>int[]</code>
     */
    public int[] getCustomMarkerStyle();

    /**
     * Sets the custom marker size attribution order
     * 
     * @param sizeList the custom marker size attribution order
     */
    public void setCustomMarkerSize(int[] sizeList);

    /**
     * Returns the custom marker size attribution order
     * 
     * @return An <code>int[]</code>
     */
    public int[] getCustomMarkerSize();

    /**
     * Sets the custom line width attribution order
     * 
     * @param widthList the custom line width attribution order
     */
    public void setCustomLineWidth(int[] widthList);

    /**
     * Returns the custom line width attribution order
     * 
     * @return An <code>int[]</code>
     */
    public int[] getCustomLineWidth();

    /**
     * Sets the custom bar width attribution order
     * 
     * @param widthList the custom bar width attribution order
     */
    public void setCustomBarWidth(int[] widthList);

    /**
     * Returns the custom bar width attribution order
     * 
     * @return An <code>int[]</code>
     */
    public int[] getCustomBarWidth();

    /**
     * Sets the custom data view style attribution order
     * 
     * @param styleList the custom data view style attribution order
     */
    public void setCustomDataViewStyle(int[] styleList);

    /**
     * Returns the custom data view style attribution order
     * 
     * @return An <code>int[]</code>
     */
    public int[] getCustomDataViewStyle();

    /**
     * Sets the custom data view axis attribution order
     * 
     * @param axisList the custom data view axis attribution order
     */
    public void setCustomAxis(int[] axisList);

    /**
     * Returns the custom data view axis attribution order
     * 
     * @return An <code>int[]</code>
     */
    public int[] getCustomAxis();

    /**
     * Sets the custom data view fill style attribution order
     * 
     * @param styleList the custom data view fill style attribution order
     */
    public void setCustomFillStyle(int[] styleList);

    /**
     * Returns the custom data view fill style attribution order
     * 
     * @return An <code>int[]</code>
     */
    public int[] getCustomFillStyle();

    /**
     * Returns the data view x offset a0 value
     * 
     * @param id The data view identifier
     * @return A <code>double</code>
     */
    public double getDataViewXOffsetA0(String id);

    /**
     * Sets the data view x offset a0 value
     * 
     * @param id The data view identifier
     * @param a0X The value to set
     */
    public void setDataViewXOffsetA0(String id, double a0X);

    /**
     * Returns the data view transformation a0 value
     * 
     * @param id The data view identifier
     * @return A <code>double</code>
     */
    public double getDataViewTransformA0(String id);

    /**
     * Sets the data view transformation a0 value
     * 
     * @param id The data view identifier
     * @param a0 The value to set
     */
    public void setDataViewTransformA0(String id, double a0);

    /**
     * Returns the data view transformation a1 value
     * 
     * @param id The data view identifier
     * @return A <code>double</code>
     */
    public double getDataViewTransformA1(String id);

    /**
     * Sets the data view transformation a1 value
     * 
     * @param id The data view identifier
     * @param a1 The value to set
     */
    public void setDataViewTransformA1(String id, double a1);

    /**
     * Returns the data view transformation a2 value
     * 
     * @param id The data view identifier
     * @return A <code>double</code>
     */
    public double getDataViewTransformA2(String id);

    /**
     * Sets the data view transformation a2 value
     * 
     * @param id The data view identifier
     * @param a2 The value to set
     */
    public void setDataViewTransformA2(String id, double a2);

    /**
     * Returns whether users can interact with a data view
     * 
     * @param id The data view identifier
     * @return A <code>boolean</code>
     */
    public boolean isDataViewClickable(String id);

    /**
     * Sets whether users can interact with a data view
     * 
     * @param id The data view identifier
     * @param clickable whether users can interact with the data view
     */
    public void setDataViewClickable(String id, boolean clickable);

    /**
     * Returns whether the legend of a data view is displayed
     * 
     * @param id The data view identifier
     * @return A <code>boolean</code>
     */
    public boolean isDataViewLabelVisible(String id);

    /**
     * Sets whether the legend of a data view should be displayed
     * 
     * @param id The data view identifier
     * @param visible whether the legend of the data view should be displayed
     */
    public void setDataViewLabelVisible(String id, boolean visible);

    /**
     * Sets whether graphical sampling is allowed for a given data view
     * 
     * @param id The data view identifier
     * @return A <code>boolean</code>
     */
    public boolean isDataViewSamplingAllowed(String id);

    /**
     * Sets whether graphical sampling should be allowed for a given data view
     * 
     * @param id The data view identifier
     * @param allowed whether graphical sampling should be allowed for the data view
     */
    public void setDataViewSamplingAllowed(String id, boolean allowed);

    /**
     * Returns the interpolation method of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     * @see #setDataViewInterpolationMethod(String, int)
     */
    public int getDataViewInterpolationMethod(String id);

    /**
     * Sets the interpolation method of a data view
     * 
     * @param id The data view identifier
     * @param method the interpolation method
     * @see #INTERPOLATE_COSINE
     * @see #INTERPOLATE_CUBIC
     * @see #INTERPOLATE_HERMITE
     * @see #INTERPOLATE_LINEAR
     * @see #INTERPOLATE_NONE
     */
    public void setDataViewInterpolationMethod(String id, int method);

    /**
     * Returns the interpolation step of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     */
    public int getDataViewInterpolationStep(String id);

    /**
     * Sets the interpolation step of a data view
     * 
     * @param id The data view identifier
     * @param step The interpolation step to use
     */
    public void setDataViewInterpolationStep(String id, int step);

    /**
     * Returns the hermite bias of a data view
     * 
     * @param id The data view identifier
     * @return A <code>double</code>
     */
    public double getDataViewHermiteBias(String id);

    /**
     * Sets the hermite bias of a data view
     * 
     * @param id The data view identifier
     * @param bias the hermite bias
     */
    public void setDataViewHermiteBias(String id, double bias);

    /**
     * Returns the hermite tension of a data view
     * 
     * @param id The data view identifier
     * @return A <code>double</code>
     */
    public double getDataViewHermiteTension(String id);

    /**
     * Sets the hermite tension of a data view
     * 
     * @param id The data view identifier
     * @param tension the hermite tension
     */
    public void setDataViewHermiteTension(String id, double tension);

    /**
     * Returns the smoothing method of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     * @see #setDataViewSmoothingMethod(String, int)
     */
    public int getDataViewSmoothingMethod(String id);

    /**
     * Sets the smoothing method of a data view
     * 
     * @param id The data view identifier
     * @param method the smoothing method
     * @see #SMOOTH_FLAT
     * @see #SMOOTH_GAUSSIAN
     * @see #SMOOTH_NONE
     * @see #SMOOTH_TRIANGULAR
     */
    public void setDataViewSmoothingMethod(String id, int method);

    /**
     * Returns the number of neighbors for the smoothing of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     */
    public int getDataViewSmoothingNeighbors(String id);

    /**
     * Sets the number of neighbors for the smoothing of a data view
     * 
     * @param id The data view identifier
     * @param neighbors Number of neighbors (Must be >=2)
     */
    public void setDataViewSmoothingNeighbors(String id, int neighbors);

    /**
     * Returns the standard deviation of the gaussian (Smoothing filter) of a data view
     * 
     * @param id The data view identifier
     * @return A <code>double</code>
     */
    public double getDataViewSmoothingGaussSigma(String id);

    /**
     * Sets the standard deviation of the gaussian (Smoothing filter) of a data view
     * 
     * @param id The data view identifier
     * @param sigma the standard deviation
     */
    public void setDataViewSmoothingGaussSigma(String id, double sigma);

    /**
     * Returns the extrapolation method used in smoothing operation of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     * @see #setDataViewSmoothingExtrapolation(String, int)
     */
    public int getDataViewSmoothingExtrapolation(String id);

    /**
     * Sets the extrapolation method used in smoothing operation of a data view
     * 
     * @param id The data view identifier
     * @param extrapolation the extrapolation method
     * @see #SMOOTH_EXT_NONE
     * @see #SMOOTH_EXT_FLAT
     * @see #SMOOTH_EXT_LINEAR
     */
    public void setDataViewSmoothingExtrapolation(String id, int extrapolation);

    /**
     * Returns the mathematical function of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     * @see #setDataViewMathFunction(String, int)
     */
    public int getDataViewMathFunction(String id);

    /**
     * Sets the mathematical function of a data view
     * 
     * @param id The data view identifier
     * @param function The mathematical function to use
     * @see #MATH_NONE
     * @see #MATH_DERIVATIVE
     * @see #MATH_INTEGRAL
     * @see #MATH_FFT_MODULUS
     * @see #MATH_FFT_PHASE
     */
    public void setDataViewMathFunction(String id, int function);

    /**
     * Returns the highlight method of a data view
     * 
     * @param id The data view identifier
     * @return An <code>int</code>
     * @see #setDataViewHighlightMethod(String, int)
     */
    public int getDataViewHighlightMethod(String id);

    /**
     * Sets the highlight method of a data view
     * 
     * @param id The data view identifier
     * @param method the highlight method
     * @see #HIGHLIGHT_ADD
     * @see #HIGHLIGHT_MULTIPLY
     */
    public void setDataViewHighlightMethod(String id, int method);

    /**
     * Returns the highlight coefficient of a data view
     * 
     * @param id The data view identifier
     * @return A <code>double</code>
     */
    public double getDataViewHighlightCoefficient(String id);

    /**
     * Sets the highlight coefficient of a data view
     * 
     * @param id The data view identifier
     * @param coef the highlight coefficient
     */
    public void setDataViewHighlightCoefficient(String id, double coef);

    /**
     * Returns whether a data view is highlighted
     * 
     * @param id The data view identifier
     * @return A <code>boolean</code>
     */
    public boolean isDataViewHighlighted(String id);

    /**
     * Sets whether a data view should be highlighted
     * 
     * @param id The data view identifier
     * @param highlighted whether the data view should be highlighted
     */
    public void setDataViewHighlighted(String id, boolean highlighted);

    /**
     * Sets whether data views are automatically highlighted through legend
     * 
     * @param autoHighlightOnLegend whether data views are automatically highlighted through legend
     *            <ul>
     *            <li>If <code>true</code>, then every time mouse cursor is over a data view legend,
     *            the corresponding data view will automatically be highlighted. When the cursor is no more over the
     *            data view legend, the corresponding data view goes back to its previous highlight state</li>
     *            <li>If <code>false</code>, legend won't interact with data views highlighting</li>
     *            </ul>
     */
    public void setAutoHighlightOnLegend(boolean autoHighlightOnLegend);

    /**
     * Returns whether data views automatic highlighting is activated
     * 
     * @return A <code>boolean</code>
     * @see #setAutoHighlightOnLegend(boolean)
     */
    public boolean isAutoHighlightOnLegend();

    /**
     * Returns whether users can remove data views through legend
     * 
     * @return A <code>boolean</code>
     * @see #setDataViewRemovingEnabled(boolean)
     */
    public boolean isDataViewRemovingEnabled();

    /**
     * Sets whether users can remove data views through legend
     * 
     * @param curveRemovingEnabled whether users can remove data views through legend
     *            <ul>
     *            <li>If <code>true</code>, then users can right click on the legend of a data view to see a popup that
     *            will allow the removing of the corresponding data view</li>
     *            <li>If <code>false</code>, there will be no such popup</li>
     *            </ul>
     */
    public void setDataViewRemovingEnabled(boolean curveRemovingEnabled);

    /**
     * Returns whether this {@link IChartViewer} will forget the configuration of the data view at each data view
     * removing
     * 
     * @return A <code>code</code>
     */
    public boolean isCleanDataViewConfigurationOnRemoving();

    /**
     * Sets whether this {@link IChartViewer} should forget the configuration of the data view at each data view
     * removing
     * 
     * @param cleanCurveConfigurationOnRemoving whether to forget data view configuration at data view removing
     */
    public void setCleanDataViewConfigurationOnRemoving(boolean cleanCurveConfigurationOnRemoving);

    /**
     * Returns whether there are some {@link PlotProperties} associated with a given data view id
     * 
     * @param id The data view identifier
     * @return A <code>boolean</code>
     */
    public boolean hasDataViewProperties(String id);

    /**
     * Forgets the {@link PlotProperties} of a data view
     * 
     * @param id The data view identifier
     */
    public void cleanDataViewProperties(String id);

    /**
     * Returns the @link PlotProperties} associated with a given data view id
     * 
     * @param id The data view identifier
     * @return Some {@link PlotProperties}
     */
    public PlotProperties getDataViewPlotProperties(String id);

    /**
     * Sets the {@link PlotProperties} for a given data view
     * 
     * @param id The data view identifier
     * @param properties The properties to set
     */
    public void setDataViewPlotProperties(String id, PlotProperties properties);

    /**
     * Returns the {@link BarProperties} associated with a given data view id
     * 
     * @param id The data view identifier
     * @return Some {@link BarProperties}
     */
    public BarProperties getDataViewBarProperties(String id);

    /**
     * Sets the {@link BarProperties} for a given data view
     * 
     * @param id The data view identifier
     * @param properties The properties to set
     */
    public void setDataViewBarProperties(String id, BarProperties properties);

    /**
     * Returns the {@link CurveProperties} associated with a given data view id
     * 
     * @param id The data view identifier
     * @return Some {@link CurveProperties}
     */
    public CurveProperties getDataViewCurveProperties(String id);

    /**
     * Sets the {@link CurveProperties} for a given data view
     * 
     * @param id The data view identifier
     * @param properties The properties to set
     */
    public void setDataViewCurveProperties(String id, CurveProperties properties);

    /**
     * Returns the {@link MarkerProperties} associated with a given data view id
     * 
     * @param id The data view identifier
     * @return Some {@link MarkerProperties}
     */
    public MarkerProperties getDataViewMarkerProperties(String id);

    /**
     * Sets the {@link MarkerProperties} for a given data view
     * 
     * @param id The data view identifier
     * @param properties The properties to set
     */
    public void setDataViewMarkerProperties(String id, MarkerProperties properties);

    /**
     * Returns the {@link ErrorProperties} associated with a given data view id
     * 
     * @param id The data view identifier
     * @return Some {@link ErrorProperties}
     */
    public ErrorProperties getDataViewErrorProperties(String id);

    /**
     * Sets the {@link ErrorProperties} for a given data view
     * 
     * @param id The data view identifier
     * @param properties The properties to set
     */
    public void setDataViewErrorProperties(String id, ErrorProperties properties);

    /**
     * Returns whether error bars are displayed for a given data view
     * 
     * @param id The data view identifier
     * @return A <code>boolean</code>
     */
    public boolean isDataViewErrorVisible(String id);

    /**
     * Sets whether error bars should be displayed for a given data view
     * 
     * @param id The data view identifier
     * @param visible whether error bars should be displayed
     */
    public void setDataViewErrorVisible(String id, boolean visible);

    /**
     * Returns the {@link TransformationProperties} associated with a given data view id
     * 
     * @param id The data view identifier
     * @return Some {@link TransformationProperties}
     */
    public TransformationProperties getDataViewTransformationProperties(String name);

    /**
     * Sets the {@link TransformationProperties} for a given data view
     * 
     * @param id The data view identifier
     * @param properties The properties to set
     */
    public void setDataViewTransformationProperties(String id, TransformationProperties properties);

    /**
     * Returns x the {@link OffsetProperties} associated with a given data view id
     * 
     * @param id The data view identifier
     * @return Some {@link OffsetProperties}
     */
    public OffsetProperties getDataViewXOffsetProperties(String name);

    /**
     * Sets the x {@link OffsetProperties} for a given data view
     * 
     * @param id The data view identifier
     * @param properties The properties to set
     */
    public void setDataViewXOffsetProperties(String id, OffsetProperties properties);

    /**
     * Returns the unit for a given data view
     * 
     * @param id The data view identifier
     * @return A {@link String}: the unit
     */
    public String getDataViewUnit(String id);

    /**
     * Sets the unit for a given data view
     * 
     * @param id The data view identifier
     * @param unit The unit to set
     */
    public void setDataViewUnit(String id, String unit);

    /**
     * Returns the dataview drag type.
     * 
     * @return An <code>int</code>.
     * @see #DRAG_NONE
     * @see #DRAG_X
     * @see #DRAG_Y
     * @see #DRAG_XY
     */
    public int getDataViewOffsetDragType();

    /**
     * Sets the dataview drag type.
     * 
     * @param dataViewOffsetDragType The dataview drag type.
     * @see #DRAG_NONE
     * @see #DRAG_X
     * @see #DRAG_Y
     * @see #DRAG_XY
     */
    public void setDataViewOffsetDragType(int dataViewOffsetDragType);

    /**
     * Returns the scale drag type.
     * 
     * @return An <code>int</code>.
     * @see #DRAG_NONE
     * @see #DRAG_X
     * @see #DRAG_Y
     * @see #DRAG_XY
     */
    public int getScaleDragType();

    /**
     * Sets the scale drag type.
     * 
     * @param scaleDragType The scale drag type.
     * @see #DRAG_NONE
     * @see #DRAG_X
     * @see #DRAG_Y
     * @see #DRAG_XY
     */
    public void setScaleDragType(int scaleDragType);

    /**
     * Returns the scale drag sensitivity, i.e. the minimum amount of pixels before considering a scale drag occurred.
     * 
     * @return The scale drag sensitivity.
     */
    public int getScaleDragSensitivity();

    /**
     * Sets the scale drag sensitivity, i.e. the minimum amount of pixels before considering a scale drag occurred.
     * 
     * @param scaleDragSensitivity The scale drag sensitivity to set.
     */
    public void setScaleDragSensitivity(int scaleDragSensitivity);

    /**
     * Returns the {@link InterpolationProperties} associated with a given data view id
     * 
     * @param id The data view identifier
     * @return Some {@link InterpolationProperties}
     */
    public InterpolationProperties getDataViewInterpolationProperties(String name);

    /**
     * Sets the {@link InterpolationProperties} for a given data view
     * 
     * @param id The data view identifier
     * @param properties The properties to set
     */
    public void setDataViewInterpolationProperties(String id, InterpolationProperties properties);

    /**
     * Returns the {@link SmoothingProperties} associated with a given data view id
     * 
     * @param id The data view identifier
     * @return Some {@link SmoothingProperties}
     */
    public SmoothingProperties getDataViewSmoothingProperties(String id);

    /**
     * Sets the {@link SmoothingProperties} for a given data view
     * 
     * @param id The data view identifier
     * @param properties The properties to set
     */
    public void setDataViewSmoothingProperties(String id, SmoothingProperties properties);

    /**
     * Returns the {@link MathProperties} associated with a given data view id
     * 
     * @param id The data view identifier
     * @return Some {@link MathProperties}
     */
    public MathProperties getDataViewMathProperties(String id);

    /**
     * Sets the {@link MathProperties} for a given data view
     * 
     * @param id The data view identifier
     * @param properties The properties to set
     */
    public void setDataViewMathProperties(String id, MathProperties properties);

    /**
     * Returns the {@link ChartProperties} of this {@link IChartViewer}
     * 
     * @return Some {@link ChartProperties}
     */
    public ChartProperties getChartProperties();

    /**
     * Sets the {@link ChartProperties} of this {@link IChartViewer}
     * 
     * @param properties The properties to set
     */
    public void setChartProperties(ChartProperties properties);

    /**
     * Returns the {@link AxisProperties} of an axis
     * 
     * @param axisChoice The axis identifier
     * @return Some {@link AxisProperties}
     */
    public AxisProperties getAxisProperties(int axisChoice);

    /**
     * Sets the {@link AxisProperties} of an axis
     * 
     * @param properties The properties to set
     * @param axisChoice The axis identifier
     */
    public void setAxisProperties(AxisProperties properties, int axisChoice);

    /**
     * Returns the {@link SamplingProperties} of this {@link IChartViewer}
     * 
     * @return Some {@link SamplingProperties}
     */
    public SamplingProperties getSamplingProperties();

    /**
     * Sets the {@link SamplingProperties} of this {@link IChartViewer}
     * 
     * @param properties The properties to set
     */
    public void setSamplingProperties(SamplingProperties properties);

    /**
     * Returns the {@link DragProperties} of this {@link IChartViewer}
     * 
     * @return Some {@link DragProperties}
     */
    public DragProperties getDragProperties();

    /**
     * Sets the {@link DragProperties} of this {@link IChartViewer}
     * 
     * @param properties The properties to set
     */
    public void setDragProperties(DragProperties properties);

    /**
     * Sets the custom user format (displayed in table and tool tip) attribution order
     * 
     * @param formatList the custom user format attribution order
     */
    public void setCustomUserFormat(String[] formatList);

    /**
     * Returns the custom user format (displayed in table and tool tip) attribution order
     * 
     * @return A {@link String} array
     */
    public String[] getCustomUserFormat();

    /**
     * Sets whether the custom data view properties map are cyclic or not
     * 
     * @param cyclingCustomMap whether the custom data view properties map are cyclic or not
     */
    public void setCyclingCustomMap(boolean cyclingCustomMap);

    /**
     * Returns whether the custom data view properties map are cyclic or not
     * 
     * @return A <code>boolean</code>
     */
    public boolean isCyclingCustomMap();

    /**
     * Returns whether data view axis selection combo box is visible
     */
    public boolean isAxisSelectionVisible();

    /**
     * Returns whether data view axis selection combo box should be visible
     * 
     * @param axisSelectionVisible whether data view axis selection combo box should be visible
     */
    public void setAxisSelectionVisible(boolean axisSelectionVisible);

    /**
     * Sets whether user can modify scale settings for a given axis
     * 
     * @param enabled whether user can modify scale settings for a given axis
     * @param axis The axis identifier
     */
    public void setAxisScaleEditionEnabled(boolean enabled, int axis);

    /**
     * Remove all data views and reset the default axis and chart configuration
     */
    public void resetAll();

    /**
     * Remove all data views, but you can choose whether to reset the default axis and chart configuration
     * 
     * @param clearConfiguration Whether to reset the default axis and chart configuration
     */
    public void resetAll(boolean clearConfiguration);

    /**
     * Call this method to evaluate an expression and have the result represented by a data view you previously
     * parametered
     * 
     * @param name The name and id of your new data view.
     * @param expression The String representing your expression. It must not be null.
     *            Example: "cos(x1) + 2*sin(x2)/exp(x3)"
     * @param axis The axis on which you want to put your data view.
     *            It can be <code>X</code>, <code>Y1</code> or <code>Y2</code>
     * @param variables A {@link String} array representing the data view names associated with your variables in order
     *            of the variables index.
     *            Example : You have two variables x1 and x2 in your expression. x1 is associated with the data view
     *            named "theCurve", and x2 with the data view named "theBar".
     *            Then, variables must be {"theCurve", "theBar"}.
     * @param x A boolean to know whether your expression looks like "f(x)".
     *            If your expression looks like "f(x1,...,xn)" then set x to <code>false</code>.
     *            If it looks like "f(x)", set x to <code>true</code>.
     * @see #X
     * @see #Y1
     * @see #Y2
     */
    public void addExpression(String name, String expression, int axis, String[] variables, boolean x);

    /**
     * Call this method to evaluate an expression and have the result represented by a data view you previously
     * parametered
     * 
     * @param id The id of your new data view.
     * @param name The name of your new data view.
     * @param expression The String representing your expression. It must not be null.
     *            Example: "cos(x1) + 2*sin(x2)/exp(x3)"
     * @param axis The axis on which you want to put your DataView.
     *            It can be <code>X</code>, <code>Y1</code> or <code>Y2</code>
     * @param variables A {@link String} array representing the data view names associated with your variables in order
     *            of the variables index.
     *            Example : You have two variables x1 and x2 in your expression. x1 is associated with the data view
     *            named "theCurve", and x2 with the data view named "theBar".
     *            Then, variables must be {"theCurve", "theBar"}.
     * @param x A boolean to know whether your expression looks like "f(x)".
     *            If your expression looks like "f(x1,...,xn)" then set x to <code>false</code>.
     *            If it looks like "f(x)", set x to <code>true</code>.
     * @see #X
     * @see #Y1
     * @see #Y2
     */
    public void addExpression(String id, String name, String expression, int axis, String[] variables, boolean x);

    /**
     * Sets whether user can edit the plot properties of a data view
     * 
     * @param id The data view identifier
     * @param editable whether user can edit the plot properties of the data view
     */
    public void setDataViewEditable(String id, boolean editable);

    /**
     * Returns whether the chart is able to draw data views outside of the drawing zone
     * 
     * @return a boolean value. <code>true</code> if the chart can't draw outside of the zone
     * @see #setLimitDrawingZone(boolean)
     */
    public boolean isLimitDrawingZone();

    /**
     * Enables or disables the chart to draw data views outside of the drawing zone defined by the surrounding
     * rectangle.
     * 
     * @param limitDrawingZone a <code>boolean</code> value. <code>true</code> to limit the drawing zone
     *            (i.e. <code>true</code> to disable the possibility to draw data views outside of the zone)
     */
    public void setLimitDrawingZone(boolean limitDrawingZone);

    /**
     * Returns whether every view added to this {@link IChartViewer} is always added as hidden.
     * This takes effect only if management panel is visible. Otherwise, every view added is added as visible
     * 
     * @return A <code>boolean</code> value.
     * @see #setAutoHideViews(boolean)
     * @see #isManagementPanelVisible()
     * @see #setManagementPanelVisible(boolean)
     */
    public boolean isAutoHideViews();

    /**
     * Sets whether every view added to this {@link IChartViewer} is always added as hidden.
     * This takes effect only if management panel is visible. Otherwise, every view added is added as visible
     * 
     * @param hide Whether every view added to this {@link IChartViewer} is always added as hidden. This takes effect
     *            only if management panel is visible. Otherwise, every view added is added as visible
     * @see #isAutoHideViews()
     * @see #isManagementPanelVisible()
     * @see #setManagementPanelVisible(boolean)
     */
    public void setAutoHideViews(boolean hide);

    /**
     * Returns the ids and names of all views, including hidden ones. This contains more id and name information than
     * using {@link #getData()}
     * 
     * @return A 2 rows matrix:
     *         <ul>
     *         <li>first row contains views ids</li>
     *         <li>second row contains views names</li>
     *         </ul>
     */
    public String[][] getAllViewsIdsAndNames();

    // JIRA JAVAAPI-39
    /**
     * Sets the labels of an axis
     * 
     * @param axis The axis identifier
     * @param labels The labels
     * @param labelPositions The values with which the labels are associated
     */
    public void setLabels(int axis, String[] labels, double[] labelPositions);

    /**
     * Clears previously set labels for a given axis
     * 
     * @param axis The axis identifier
     * @see #setLabels(int, String[], double[])
     */
    public void clearLabels(int axis);

}
