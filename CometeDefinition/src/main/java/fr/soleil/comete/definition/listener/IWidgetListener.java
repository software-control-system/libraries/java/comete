/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.listener;

import java.util.EventListener;

/**
 * valueChanged methods are called when user change the value on widget and dataChanged are called
 * when the source change value on widget
 * 
 */
public interface IWidgetListener extends EventListener {

    /**
     * if the value of widget is not a boolean, you must follow this conversion rules
     * 
     * <ul>
     * <li>the value of widget is a Number: return true if the value is equals to 1 otherwise false</li>
     * <li>the value of widget is a String: try to convert in a number and apply previous rule. if
     * conversion in Number failed return false</li>
     * </ul>
     * 
     * @param value the value of widget converted in a boolean.
     */
    public void booleanValueChanged(boolean value);

    /**
     * if the value of widget is not a Number, you must follow this conversion rules
     * 
     * <ul>
     * <li>the value of widget is a boolean: return 0.0 or 1.0</li>
     * <li>the value of widget is a String: convert the value in a number using Double.valueOf(...)
     * function . if conversion failed return Double.NaN</li>
     * </ul>
     * 
     * @param value the value of widget converted in a double.
     */
    public void doubleValueChanged(double value);

    /**
     * if the value of widget is not a String, you must use String.valueOf() to convert the value of
     * widget.
     * 
     * @param value the value of widget converted in a String.
     */
    public void stringValueChanged(String value);

    /**
     * @see booleanValueChanged
     * @param value the value of widget converted in a boolean.
     */
    public void booleanDataChanged(boolean value);

    /**
     * @see doubleValueChanged
     * @param value the value of widget converted in a double.
     */
    public void doubleDataChanged(double value);

    /**
     * @see stringValueChanged
     * @param value the value of widget converted in a String.
     */
    public void stringDataChanged(String value);

}
