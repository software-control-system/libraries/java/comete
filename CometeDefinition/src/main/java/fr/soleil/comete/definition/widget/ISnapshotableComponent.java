/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import fr.soleil.comete.definition.listener.ISnapshotListener;

/**
 * An {@link IComponent} that can manage some snapshot (picture/movie) files
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ISnapshotableComponent extends IComponent {

    /**
     * Returns the path of the directory where snapshot (picture/movie) files are written
     * 
     * @return A {@link String}
     */
    public String getSnapshotDirectory();

    /**
     * Sets the path of the directory where snapshot (picture/movie) files are written
     * 
     * @param snapshotDirectory the path to set
     */
    public void setSnapshotDirectory(String snapshotDirectory);

    /**
     * Returns the path of the last snapshot (picture/movie) file
     * 
     * @return A {@link String}
     */
    public String getSnapshotFile();

    /**
     * Adds an {@link ISnapshotListener} to this {@link ISnapshotableComponent}
     * 
     * @param listener The {@link ISnapshotListener} to add
     */
    public void addSnapshotListener(ISnapshotListener listener);

    /**
     * Removes an {@link ISnapshotListener} from this {@link ISnapshotableComponent}
     * 
     * @param listener The {@link ISnapshotListener} to remove
     */
    public void removeSnapshotListener(ISnapshotListener listener);

}
