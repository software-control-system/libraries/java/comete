/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

public class OffsetProperties extends ATransformationProperties {

    private static final long serialVersionUID = -4288966655132853810L;

    public OffsetProperties() {
        this(0);
    }

    public OffsetProperties(double a0) {
        super(a0);
    }

    @Override
    public OffsetProperties clone() {
        return (OffsetProperties) super.clone();
    }
}
