/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import javax.swing.JFileChooser;

import fr.soleil.comete.definition.widget.IPlayer;

/**
 * This interface describes behavioral classes for {@link IPlayer} animation tools.
 * 
 * @author huriez
 */
public interface IPlayerAnimationBehavior {

    /**
     * Generate the animation with the procedure associated with this class.
     * 
     * @param player the player to read
     * @param targetDirectory the directory where the {@link JFileChooser} must be positioned.
     * @return the path of the generated animation file
     */
    public String generateAnimation(IPlayer player, String targetDirectory);
}
