/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateFormattable;

public class ChartFormatUtil {

    public static String getFormat(int format) {
        String result;
        switch (format) {
            case IChartViewer.AUTO_FORMAT:
                // Use default compiler format to display double
                result = "%g";
                break;
            case IChartViewer.SCIENTIFIC_FORMAT:
                // Display value using exponential representation (x.xxEyy)
                result = "%e";
                break;
            case IChartViewer.TIME_FORMAT:
                // Display number of second as HH:MM:SS
                result = "HH:MM:SS";
                break;
            case IChartViewer.DECINT_FORMAT:
                // Display integer using decimal format
                result = "%d";
                break;
            case IChartViewer.HEXINT_FORMAT:
                // Display integer using hexadecimal format
                result = "%X";
                break;
            case IChartViewer.BININT_FORMAT:
                // Display integer using binary format
                result = "%o";
                break;
            case IChartViewer.SCIENTIFICINT_FORMAT:
                // Display value using exponential representation (xEyy)
                result = "%.0e";
                break;
            case IChartViewer.DATE_FORMAT:
                // Display value as date
                result = IDateFormattable.US_DATE_FORMAT;
                break;
            default:
                result = ObjectUtils.EMPTY_STRING;
                break;
        }
        return result;
    }
}
