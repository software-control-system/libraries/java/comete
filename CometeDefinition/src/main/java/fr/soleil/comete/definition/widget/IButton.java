/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import java.util.EventObject;

import fr.soleil.comete.definition.data.target.IImageTarget;
import fr.soleil.comete.definition.data.target.scalar.IBooleanComponent;
import fr.soleil.comete.definition.data.target.scalar.IEditableTextComponent;
import fr.soleil.comete.definition.listener.IButtonListener;

public interface IButton extends IBooleanComponent, IEditableTextComponent, IImageTarget {

    public void setBorderPainted(boolean painted);

    public boolean isBorderPainted();

    public void setButtonLook(boolean look);

    public boolean isButtonLook();

    public void addButtonListener(final IButtonListener listener);

    public void removeButtonListener(final IButtonListener listener);

    public void fireActionPerformed(final EventObject event);

    public void execute();

    public String getActionName();

    public void setActionName(String actionName);

}
