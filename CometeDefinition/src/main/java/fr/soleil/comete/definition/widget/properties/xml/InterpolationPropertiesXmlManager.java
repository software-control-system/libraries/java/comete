/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and {@link InterpolationProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class InterpolationPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "interpolation";
    public static final String INTERPOLATION_METHOD_PROPERTY_XML_TAG = "interpolationMethod";
    public static final String INTERPOLATION_STEP_PROPERTY_XML_TAG = "interpolationStep";
    public static final String HERMITE_BIAS_PROPERTY_XML_TAG = "hermiteBias";
    public static final String HERMITE_TENSION_PROPERTY_XML_TAG = "hermiteTension";

    /**
     * Transforms some {@link InterpolationProperties} into an {@link XMLLine}
     * 
     * @param properties The {@link InterpolationProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(InterpolationProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttribute(INTERPOLATION_METHOD_PROPERTY_XML_TAG,
                    String.valueOf(properties.getInterpolationMethod()));
            openingLine.setAttribute(INTERPOLATION_STEP_PROPERTY_XML_TAG,
                    String.valueOf(properties.getInterpolationStep()));
            openingLine.setAttribute(HERMITE_BIAS_PROPERTY_XML_TAG, String.valueOf(properties.getHermiteBias()));
            openingLine.setAttribute(HERMITE_TENSION_PROPERTY_XML_TAG, String.valueOf(properties.getHermiteTension()));
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link InterpolationProperties}
     * 
     * @param currentSubNode The {@link Node} to interpret
     * @return The recovered {@link InterpolationProperties}
     */
    public static InterpolationProperties loadInterpolation(Node currentSubNode) {
        return loadInterpolationFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Map} to recover some {@link InterpolationProperties}
     * 
     * @param propertyMap The {@link Map} to interpret. The {@link Map} is supposed to contain some xml node attributes
     * @return The recovered {@link InterpolationProperties}
     */
    public static InterpolationProperties loadInterpolationFromXmlNodeAttributes(
            final Map<String, String> propertyMap) {
        InterpolationProperties interpolation = new InterpolationProperties();
        if (propertyMap != null) {
            String hermliteBias_s = propertyMap.get(HERMITE_BIAS_PROPERTY_XML_TAG);
            String hermiteTension_s = propertyMap.get(HERMITE_TENSION_PROPERTY_XML_TAG);
            String interpolationMethod_s = propertyMap.get(INTERPOLATION_METHOD_PROPERTY_XML_TAG);
            String interpolationStep_s = propertyMap.get(INTERPOLATION_STEP_PROPERTY_XML_TAG);

            double hermiteBias, hermiteTension;
            int interpolationMethod, interpolationStep;
            hermiteBias = parseDouble(hermliteBias_s, interpolation.getHermiteBias());
            hermiteTension = parseDouble(hermiteTension_s, interpolation.getHermiteTension());
            interpolationMethod = parseInt(interpolationMethod_s, interpolation.getInterpolationMethod());
            interpolationStep = parseInt(interpolationStep_s, interpolation.getInterpolationStep());

            interpolation.setHermiteBias(hermiteBias);
            interpolation.setHermiteTension(hermiteTension);
            interpolation.setInterpolationMethod(interpolationMethod);
            interpolation.setInterpolationStep(interpolationStep);
        }
        return interpolation;
    }

}
