/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

public class CometeColor implements IColor, Cloneable {

    private static final long serialVersionUID = -7996590981414178749L;

    public static final CometeColor WHITE = new CometeColor(255, 255, 255);
    public static final CometeColor BLACK = new CometeColor(0, 0, 0);
    public static final CometeColor RED = new CometeColor(255, 0, 0);
    public static final CometeColor GREEN = new CometeColor(0, 255, 0);
    public static final CometeColor BLUE = new CometeColor(0, 0, 255);
    public static final CometeColor YELLOW = new CometeColor(255, 255, 0);
    public static final CometeColor ORANGE = new CometeColor(255, 200, 0);
    public static final CometeColor CYAN = new CometeColor(0, 255, 255);
    public static final CometeColor MAGENTA = new CometeColor(255, 0, 255);
    public static final CometeColor PURPLE = new CometeColor(160, 32, 240);
    public static final CometeColor PINK = new CometeColor(255, 175, 175);
    public static final CometeColor GRAY = new CometeColor(128, 128, 128);
    public static final CometeColor DARK_GRAY = new CometeColor(64, 64, 64);
    public static final CometeColor LIGHT_GRAY = new CometeColor(192, 192, 192);

    private int blue;
    private int green;
    private int red;
    private int alpha;

    public CometeColor(int r, int g, int b) {
        this(r, g, b, 255);
    }

    public CometeColor(int r, int g, int b, int a) {
        red = r;
        blue = b;
        green = g;
        alpha = a;
    }

    @Override
    public int getBlue() {
        return blue;
    }

    @Override
    public int getGreen() {
        return green;
    }

    @Override
    public int getRed() {
        return red;
    }

    @Override
    public int getAlpha() {
        return alpha;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if ((obj != null) && (obj.getClass().equals(getClass()))) {
            CometeColor color = (CometeColor) obj;
            equals = ((getRed() == color.getRed()) && (getGreen() == color.getGreen()) && (getBlue() == color.getBlue())
                    && (getAlpha() == color.getAlpha()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xC03E7E;
        int mult = 0xC01012;
        code = code * mult + getClass().hashCode();
        code = code * mult + getRed();
        code = code * mult + getGreen();
        code = code * mult + getBlue();
        code = code * mult + getAlpha();
        return code;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder("{");
        buffer.append(getClass().getName()).append(": ");
        buffer.append("Red = ").append(getRed()).append(", ");
        buffer.append("Green = ").append(getGreen()).append(", ");
        buffer.append("Blue = ").append(getBlue()).append(", ");
        buffer.append("Alpha = ").append(getAlpha()).append("}");
        return buffer.toString();
    }

    @Override
    public CometeColor clone() {
        CometeColor clone;
        try {
            clone = (CometeColor) super.clone();
        } catch (CloneNotSupportedException e) {
            // Will never happen as CometeColor implements Cloneable
            clone = null;
        }
        return clone;
    }

}
