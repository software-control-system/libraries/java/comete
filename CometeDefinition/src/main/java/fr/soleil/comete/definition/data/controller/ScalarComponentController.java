/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.controller;

import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.data.controller.ScalarTargetController;
import fr.soleil.data.target.ITarget;

public class ScalarComponentController<U, T> extends ScalarTargetController<U, T> {

    public ScalarComponentController(Class<?> classTarget, Class<U> source, Class<T> targetStrategy) {
        super(classTarget, source, targetStrategy);
    }

    @Override
    protected boolean isTargetSettable(ITarget target) {
        boolean settable = false;
        if (target instanceof IComponent) {
            settable = !((IComponent) target).isEditingData();
        }
        return settable;
    }
}
