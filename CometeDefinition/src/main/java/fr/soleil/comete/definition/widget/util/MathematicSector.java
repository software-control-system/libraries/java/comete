/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.WeakHashMap;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.event.MathematicSectorEvent;
import fr.soleil.comete.definition.listener.MathematicSectorListener;
import fr.soleil.data.mediator.Mediator;

public abstract class MathematicSector implements Cloneable {

    private static final String INVALID_VALUE_FOR = "Invalid value for ";
    private static final String UNKNOWN_PARAMETER_NAME = "Unknown parameter name: ";
    private static final String CLASS_NAME_SEPARATOR = ":";
    private static final String PARAMETERS_SEPARATOR = ";";
    private static final String VALUE_SEPARATOR = "=";

    protected static final Class<?>[] EMPTY_CLASS = new Class[0];
    protected static final Object[] EMPTY_PARAMETER = new Object[0];

    private Collection<MathematicSectorListener> listeners = Collections
            .newSetFromMap(new WeakHashMap<MathematicSectorListener, Boolean>());
    protected boolean editing;

    public MathematicSector() {
        super();
        editing = true;
    }

    public abstract boolean isValidParameterValue(String name, String value);

    public abstract void setParameterWithString(String name, String value, double... beamPoint)
            throws MathematicSectorException;

    protected abstract void setParameter(String name, Number value) throws MathematicSectorException;

    public abstract String getDescription();

    public abstract String[] getParameterNames();

    public abstract String getDisplayableName(String name);

    public abstract Number getParameter(String name);

    public String parameterToString(String name, double... beamPoint) {
        return String.valueOf(getParameter(name));
    }

    public abstract String getParameterUnit(String name);

    public abstract String getValidValuesDescription(String name);

    public abstract String getParameterDescription(String name);

    public abstract Mask generateMask(int dimX, int dimY);

    public void addMathematicSectorListener(MathematicSectorListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    public void removeMathematicSectorListener(MathematicSectorListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    public void removeAllMathematicSectorListeners() {
        synchronized (listeners) {
            listeners.clear();
        }
    }

    protected void fireMathematicSectorEvent(int reason) {
        Collection<MathematicSectorListener> copy = new ArrayList<MathematicSectorListener>();
        MathematicSectorEvent event = new MathematicSectorEvent(this, reason);
        synchronized (listeners) {
            copy.addAll(listeners);
        }
        for (MathematicSectorListener listener : copy) {
            listener.sectorChanged(event);
        }
        copy.clear();
    }

    public boolean isConstructing() {
        return editing;
    }

    public void validate() {
        editing = false;
        fireMathematicSectorEvent(MathematicSectorEvent.REASON_VALIDATION);
    }

    public void setEditingMode() {
        editing = true;
        fireMathematicSectorEvent(MathematicSectorEvent.REASON_VALIDATION);
    }

    protected MathematicSectorException generateDefaultValueException(String name) {
        return new MathematicSectorException(INVALID_VALUE_FOR + name);
    }

    protected MathematicSectorException generateDefaultValueException(String name, Throwable cause) {
        return new MathematicSectorException(INVALID_VALUE_FOR + name, cause);
    }

    protected MathematicSectorException generateDefaultNameException(String name) {
        return new MathematicSectorException(UNKNOWN_PARAMETER_NAME + name);
    }

    @Override
    public MathematicSector clone() {
        return clone(false);
    }

    /**
     * Creates a clone of this {@link MathematicSector}
     * 
     * @param transmitListeners Whether to transmit listeners to cloned {@link MathematicSector}
     * @return A {@link MathematicSector}
     */
    public MathematicSector clone(boolean transmitListeners) {
        MathematicSector clone;
        try {
            clone = (MathematicSector) super.clone();
            clone.listeners = Collections.newSetFromMap(new WeakHashMap<MathematicSectorListener, Boolean>());
            if (transmitListeners) {
                clone.listeners.addAll(listeners);
            }
        } catch (CloneNotSupportedException e) {
            // Should not happen as MathematicSector implements Cloneable
            clone = this;
        }
        return clone;
    }

    @Override
    public String toString() {
        return toString(false);
    }

    public String toString(double... beamPoint) {
        return toString(false, beamPoint);
    }

    public String toSimpleString(double... beamPoint) {
        return toString(true, beamPoint);
    }

    protected String toString(boolean simpleName, double... beamPoint) {
        StringBuilder buffer = new StringBuilder();
        if (simpleName) {
            buffer.append(getClass().getSimpleName());
        } else {
            buffer.append(getClass().getName());
        }
        String[] names = getParameterNames();
        if ((names != null) && (names.length > 0)) {
            buffer.append(CLASS_NAME_SEPARATOR);
            for (String name : names) {
                String nameToAdd = simpleName ? getDisplayableName(name) : name;
                if (nameToAdd != null) {
                    buffer.append(nameToAdd).append(VALUE_SEPARATOR);
                    buffer.append(parameterToString(name, beamPoint));
                    buffer.append(PARAMETERS_SEPARATOR);
                }
            }
            buffer.deleteCharAt(buffer.length() - 1);
        }
        return buffer.toString();
    }

    /**
     * Builds a {@link MathematicSector} from a {@link String} representation, without throwing any exception, knowing
     * the beam point (PONI).
     * 
     * @param sectorAsString The {@link String} that represents the {@link MathematicSector} to build
     * @param beamPoint The known beam point (PONI)
     * @return a {@link MathematicSector}. <code>null</code> in case of failure.
     */
    public static MathematicSector recoverSectorFormStringRepresentation(String sectorAsString, double... beamPoint) {
        MathematicSector sector;
        try {
            sector = recoverSectorFormStringRepresentationWithException(sectorAsString, beamPoint);
        } catch (Exception e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to recover sector from string", e);
            sector = null;
        }
        return sector;
    }

    /**
     * Builds a {@link MathematicSector} from a {@link String} representation, knowing the beam point (PONI).
     * 
     * @param sectorAsString The {@link String} that represents the {@link MathematicSector} to build
     * @param beamPoint The known beam point (PONI)
     * @return a {@link MathematicSector}. May be <code>null</code>.
     * @throws Exception If a problem occurred during sector recovering
     */
    public static MathematicSector recoverSectorFormStringRepresentationWithException(String sectorAsString,
            double... beamPoint) throws Exception {
        MathematicSector sector;
        if ((sectorAsString == null) || sectorAsString.isEmpty()) {
            sector = null;
        } else {
            int classNameEnd = sectorAsString.indexOf(CLASS_NAME_SEPARATOR);
            if ((classNameEnd) < 0) {
                sector = null;
            } else {
                String className = sectorAsString.substring(0, classNameEnd);
                Class<?> objectClass = Class.forName(className);
                Constructor<?> tmpConstructor = objectClass.getConstructor(EMPTY_CLASS);
                Object object = tmpConstructor.newInstance(EMPTY_PARAMETER);
                if (object instanceof MathematicSector) {
                    sector = (MathematicSector) object;
                    classNameEnd++;
                    if (classNameEnd < sectorAsString.length()) {
                        String params = sectorAsString.substring(classNameEnd, sectorAsString.length());
                        String[] splitedParams = params.split(PARAMETERS_SEPARATOR);
                        for (String paramString : splitedParams) {
                            int separatorIndex = paramString.indexOf(VALUE_SEPARATOR);
                            if (separatorIndex > 0) {
                                String paramName = paramString.substring(0, separatorIndex);
                                separatorIndex++;
                                if (separatorIndex < paramString.length()) {
                                    sector.setParameterWithString(paramName,
                                            paramString.substring(separatorIndex, paramString.length()), beamPoint);
                                }
                            }
                        }
                    }
                    sector.validate();
                } else {
                    sector = null;
                }
            }
        }
        return sector;
    }

}
