/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import java.util.EventObject;

import fr.soleil.comete.definition.widget.util.MathematicSector;

public class MathematicSectorEvent extends EventObject {

    private static final long serialVersionUID = 8553116623280079502L;
    public static final int REASON_VALUE = 0;
    public static final int REASON_VALIDATION = 1;

    private int reason;

    public MathematicSectorEvent(MathematicSector source, int reason) {
        super(source);
        this.reason = reason;
    }

    @Override
    public MathematicSector getSource() {
        return (MathematicSector) super.getSource();
    }

    public int getReason() {
        return reason;
    }

}
