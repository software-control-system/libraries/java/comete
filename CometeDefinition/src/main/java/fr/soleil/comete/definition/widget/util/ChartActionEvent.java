/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import java.util.EventObject;

public class ChartActionEvent extends EventObject implements Cloneable {

    private static final long serialVersionUID = -5680411286119829337L;

    private final String actionName;
    private final boolean state;

    public ChartActionEvent(Object source, String name) {
        this(source, name, false);
    }

    public ChartActionEvent(Object source, String name, boolean s) {
        super(source);
        actionName = name;
        state = s;
    }

    /**
     * Gets the action name
     */
    public String getName() {
        return actionName;
    }

    /**
     * Gets the action state
     */
    public boolean getState() {
        return state;
    }

}
