/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

/**
 * An interface for widgets that have an automatic scroll
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IAutoScrolledComponent extends IComponent {

    /**
     * An {@link Enum} that represents the different ways to do an automatic scroll
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    public static enum ScrollMode {
        /**
         * A loop autoscroll is a classic autoscroll, where the scrolled part is continuously
         * scrolled, as if it were stuck on a wheel (for example: an advertising text)
         */
        LOOP,
        /**
         * A pendular autoscroll is a scrolling mode where the scrolled part goes from start to end,
         * and back to start, in a pendular way
         */
        PENDULAR,
        /**
         * A reach end autoscroll is like a pendular autoscroll, except that when the end is
         * reached, the view directly goes at it start position, without reverse scrolling
         */
        REACH_END
    }

    /**
     * Returns this {@link IAutoScrolledComponent}'s scrolling mode
     * 
     * @return A {@link ScrollMode}
     * @see #setScrollMode(ScrollMode)
     */
    public ScrollMode getScrollMode();

    /**
     * Sets this {@link IAutoScrolledComponent}'s scolling mode
     * 
     * @param scrollMode The {@link ScrollMode} to set
     * @see #getScrollMode()
     */
    public void setScrollMode(ScrollMode scrollMode);

    /**
     * Returns the time (in milliseconds) to wait between 2 scroll positions. A value less than or
     * equal to <code>0</code> indicates a stopped scrolling mode
     * 
     * @return An <code>int</code>.
     * @see #setScrollStepsTime(int)
     */
    public int getScrollStepsTime();

    /**
     * Sets the time (in milliseconds) to wait between 2 scroll positions (for example: for the
     * scrolled part moving from 1 pixel)
     * 
     * @param stepTime The time to set (the bigger the value, the slower the scroll speed). A value
     *            less than or equal to <code>0</code> stops the scrolling mode
     * @see #getScrollStepsTime()
     */
    public void setScrollStepsTime(int stepTime);

    /**
     * Returns the time (in milliseconds) to wait once an extremity is reached. This should be used
     * only in case of {@link ScrollMode#PENDULAR} or {@link ScrollMode#REACH_END} scrolling mode
     * 
     * @return An <code>int</code>
     * @see #setExtremeWaitingTime(int)
     * @see #getScrollMode()
     * @see #setScrollMode(ScrollMode)
     */
    public int getReachEndWaitingTime();

    /**
     * Sets the time (in milliseconds) to wait once an extremity is reached
     * 
     * @param waitingTime The waiting time to set
     * @see #getExtremeWaitingTime()
     */
    public void setReachEndWaitingTime(int waitingTime);

}
