/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import fr.soleil.comete.definition.data.target.scalar.IFormatableComponent;
import fr.soleil.comete.definition.listener.ITableListener;

public interface ITable<T> extends IFormatableComponent {

    public void addTableListener(final ITableListener<T> listener);

    public void removeTableListener(final ITableListener<T> listener);

    public void fireValueChanged(T matrix);

    public void setData(T matrix);

    public T getData();

    public boolean isEditable();

    public void setEditable(boolean editable);

    public void refresh();

    public String[] getCustomColumnNames();

    public void setCustomColumnNames(String[] customColumnNames);

    public String[] getCustomRowNames();

    public void setCustomRowNames(String[] customRowNames);

}
