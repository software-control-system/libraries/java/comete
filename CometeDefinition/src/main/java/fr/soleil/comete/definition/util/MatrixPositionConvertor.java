/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.matrix.INumberMatrixTarget;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An {@link AbstractValueConvertor} that uses a number matrix and a number to convert values. The
 * number represents the line index in the matrix. That is why {@link MatrixPositionConvertor} is
 * also an {@link INumberMatrixTarget} and an {@link INumberTarget}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MatrixPositionConvertor extends AbstractValueConvertor
        implements IPiecewiseAffineValueConvertor, INumberMatrixTarget, INumberTarget {

    protected final TargetDelegate targetDelegate;
    protected Object[] matrixData;
    protected int yPosition, width, height;
    protected boolean piecewiseAffine;

    public MatrixPositionConvertor() {
        super();
        targetDelegate = new TargetDelegate();
        matrixData = null;
        yPosition = width = height = 0;
        piecewiseAffine = false;
    }

    @Override
    public boolean isPiecewiseAffine() {
        return piecewiseAffine;
    }

    @Override
    public void setPiecewiseAffine(boolean piecewiseAffine) {
        if (piecewiseAffine != this.piecewiseAffine) {
            this.piecewiseAffine = piecewiseAffine;
            warnListeners();
        }
    }

    @Override
    public void setNumberValue(Number value) {
        yPosition = Math.max(0, value == null ? 0 : value.intValue());
        warnListeners();
    }

    @Override
    public double convertValue(double value) {
        double result = Double.NaN;
        try {
            int yPosition = this.yPosition;
            Object[] matrixData = this.matrixData;
            if ((matrixData != null) && (yPosition > -1) && (yPosition < height)) {
                Object line = matrixData[yPosition];
                result = ArrayPositionConvertor.convertedValue(value, line, piecewiseAffine);
            }
        } catch (Exception e) {
            result = Double.NaN;
        }
        return result;
    }

    @Override
    public boolean isValid() {
        return (matrixData != null) && (width > 0) && (height > 0);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        targetDelegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        targetDelegate.removeMediator(mediator);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return false;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        return width;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return height;
    }

    @Override
    public Number getNumberValue() {
        return yPosition;
    }

    @Override
    public Object[] getNumberMatrix() {
        Object[] result = null;
        Object[] matrixData = this.matrixData;
        if (matrixData != null) {
            result = matrixData.clone();
        }
        return result;
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        int height, width;
        int[] shape = ArrayUtils.recoverShape(value);
        if ((shape != null) && (shape.length == 2)) {
            height = shape[0];
            width = shape[1];
        } else {
            height = 0;
            width = 0;
        }
        this.matrixData = value;
        this.height = height;
        this.width = width;
        warnListeners();
    }

    @Override
    public Object getFlatNumberMatrix() {
        return ArrayUtils.convertArrayDimensionFromNTo1(matrixData);
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        setNumberMatrix((Object[]) ArrayUtils.convertArrayDimensionFrom1ToN(value, height, width));
    }
}
