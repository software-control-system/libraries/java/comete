/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import java.util.EventObject;

import fr.soleil.comete.definition.data.target.scalar.IEditableTextComponent;
import fr.soleil.comete.definition.listener.IComboBoxListener;
import fr.soleil.data.target.array.IObjectArrayTarget;
import fr.soleil.data.target.array.ITextArrayTarget;

public interface IComboBox extends IEditableTextComponent, ITextArrayTarget, IObjectArrayTarget, IComponent {

    public void addComboBoxListener(final IComboBoxListener listener);

    public void removeComboBoxListener(final IComboBoxListener listener);

    public void fireSelectedItemChanged(final EventObject event);

    public String[] getDisplayedList();

    public void setDisplayedList(final String... displayedList);

    public Object[] getValueList();

    public void setValueList(final Object... valueList);

    public int getItemCount();

    public Object getSelectedValue();

    public void setSelectedValue(final Object value);

    public int getSelectedIndex();

    public void setSelectedIndex(final int index);

    @Override
    public boolean hasFocus();

}
