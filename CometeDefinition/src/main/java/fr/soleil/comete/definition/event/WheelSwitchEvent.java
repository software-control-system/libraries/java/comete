/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import fr.soleil.comete.definition.widget.IWheelSwitch;

public class WheelSwitchEvent extends CometeEvent<IWheelSwitch> {

    private static final long serialVersionUID = -1530869250667760332L;

    protected double value;

    public WheelSwitchEvent(IWheelSwitch source, double val) {
        super(source);
        setValue(val);
    }

    public void setValue(double val) {
        this.value = val;
    }

    public double getValue() {
        return value;
    }

}
