/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.plugin;

import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import fr.soleil.comete.definition.data.controller.ComponentColorController;
import fr.soleil.comete.definition.widget.IComboBox;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.data.plugin.AbstractPlugin;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.ITarget;

public abstract class ComponentColorPlugin<U> extends AbstractPlugin<U> {

    private final Map<ITarget, CometeColor> colorMap;

    public ComponentColorPlugin(IKey key, ComponentColorController<U> controller) {
        super(key, controller);
        colorMap = new WeakHashMap<ITarget, CometeColor>();
    }

    @Override
    protected void registerDefaultValue(ITarget target) {
        if (target instanceof IComponent) {
            synchronized (colorMap) {
                CometeColor color = ((IComponent) target).getCometeBackground();
                if (color != null) {
                    colorMap.put(target, color);
                }
            }
        }
    }

    @Override
    protected void resetDefaultValue(ITarget target) {
        if (target instanceof IComponent) {
            CometeColor color;
            synchronized (colorMap) {
                color = colorMap.get(target);
            }
            if (color != null) {
                ((IComponent) target).setCometeBackground(color);
            }
        }
    }

    public boolean isColorEnabled(ITarget target) {
        return isPluginAllowed(target);
    }

    public void setColorEnabled(ITarget target, boolean state) {
        setPluginAllowed(target, state);
    }

    /**
     * @return the foregroundColor
     */
    public boolean isColorAsForeground(ITarget target) {
        ComponentColorController<?> colorController = (ComponentColorController<?>) pluginController;
        return colorController.isColorAsForeground(target);
    }

    /**
     * @param foregroundColor the foregroundColor to set
     */
    public void setColorAsForeground(boolean foregroundColor, ITarget target) {
        ComponentColorController<?> colorController = (ComponentColorController<?>) pluginController;
        colorController.setColorAsForeground(foregroundColor, target);
    }

    @Override
    protected Set<Class<?>> initBlackList() {
        Set<Class<?>> result = super.initBlackList();
        result.add(IComboBox.class);
        return result;
    }
}
