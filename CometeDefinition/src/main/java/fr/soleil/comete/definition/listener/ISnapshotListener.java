/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.listener;

import java.util.EventListener;

import fr.soleil.comete.definition.event.SnapshotEvent;
import fr.soleil.comete.definition.widget.ISnapshotableComponent;

/**
 * An {@link EventListener} that listens to the changes in the snapshots of an
 * {@link ISnapshotableComponent}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ISnapshotListener extends EventListener {

    /**
     * Notifies this {@link ISnapshotListener} for some changes in the snapshots of an
     * {@link ISnapshotableComponent}
     * 
     * @param event The {@link SnapshotEvent} that represents the changes
     */
    public void snapshotChanged(SnapshotEvent event);

}
