/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and {@link MarkerProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MarkerPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "marker";
    public static final String COLOR_PROPERTY_XML_TAG = "color";
    public static final String SIZE_PROPERTY_XML_TAG = "size";
    public static final String STYLE_PROPERTY_XML_TAG = "style";
    public static final String IS_LEGEND_VISIBLE_PROPERTY_XML_TAG = "isLegendVisible";
    public static final String IS_SAMPLING_ALLOWED_PROPERTY_XML_TAG = "isSamplingAllowed";

    /**
     * Transforms some {@link MarkerProperties} into an {@link XMLLine}
     * 
     * @param properties The {@link MarkerProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(MarkerProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttribute(COLOR_PROPERTY_XML_TAG, colorToString(properties.getColor()));
            openingLine.setAttribute(SIZE_PROPERTY_XML_TAG, String.valueOf(properties.getSize()));
            openingLine.setAttribute(STYLE_PROPERTY_XML_TAG, String.valueOf(properties.getStyle()));
            openingLine.setAttribute(IS_LEGEND_VISIBLE_PROPERTY_XML_TAG, String.valueOf(properties.isLegendVisible()));
            openingLine.setAttribute(IS_SAMPLING_ALLOWED_PROPERTY_XML_TAG,
                    String.valueOf(properties.isSamplingAllowed()));
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link MarkerProperties}
     * 
     * @param currentSubNode The {@link Node} to interpret
     * @return The recovered {@link MarkerProperties}
     */
    public static MarkerProperties loadMarker(Node currentSubNode) {
        return loadMarkerFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Map} to recover some {@link MarkerProperties}
     * 
     * @param propertyMap The {@link Map} to interpret. The {@link Map} is supposed to contain some xml node attributes
     * @return The recovered {@link MarkerProperties}
     */
    public static MarkerProperties loadMarkerFromXmlNodeAttributes(final Map<String, String> propertyMap) {
        MarkerProperties marker = new MarkerProperties();
        if (propertyMap != null) {
            String color_s = propertyMap.get(COLOR_PROPERTY_XML_TAG);
            String style_s = propertyMap.get(STYLE_PROPERTY_XML_TAG);
            String size_s = propertyMap.get(SIZE_PROPERTY_XML_TAG);
            String visible_s = propertyMap.get(IS_LEGEND_VISIBLE_PROPERTY_XML_TAG);
            String sampling_s = propertyMap.get(IS_SAMPLING_ALLOWED_PROPERTY_XML_TAG);

            CometeColor color = stringToColor(color_s);
            int size, style;
            boolean visible;
            boolean sampling;
            size = parseInt(size_s, marker.getSize());
            style = parseInt(style_s, marker.getStyle());
            visible = parseBoolean(visible_s, marker.isLegendVisible());
            sampling = parseBoolean(sampling_s, marker.isSamplingAllowed());
            marker.setColor(color);
            marker.setStyle(style);
            marker.setSize(size);
            marker.setLegendVisible(visible);
            marker.setSamplingAllowed(sampling);
        }
        return marker;
    }

}
