/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import fr.soleil.lib.project.ObjectUtils;

public class TransformationProperties extends ATransformationProperties {

    private static final long serialVersionUID = -1851539218327527782L;

    private double a1 = 1;
    private double a2 = 0;

    public TransformationProperties() {
        this(0, 1, 0);
    }

    public TransformationProperties(double a0, double a1, double a2) {
        super(a0);
        this.a1 = a1;
        this.a2 = a2;
    }

    private boolean isInvalid() {
        return Double.isNaN(getA0()) || Double.isNaN(getA1()) || Double.isNaN(getA2());
    }

    @Override
    public double transform(double in) {
        double result;
        if (isInvalid()) {
            result = in;
        } else {
            double in2 = in * in;
            result = getA0() + getA1() * in + getA2() * in2;
        }
        return result;
    }

    /**
     * @return Returns the a1.
     */
    public double getA1() {
        return a1;
    }

    /**
     * @param a1 The a1 to set.
     */
    public void setA1(double a1) {
        this.a1 = a1;
    }

    /**
     * @return Returns the a2.
     */
    public double getA2() {
        return a2;
    }

    /**
     * @param a2 The a2 to set.
     */
    public void setA2(double a2) {
        this.a2 = a2;
    }

    @Override
    public TransformationProperties clone() {
        return (TransformationProperties) super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && (getClass().equals(obj.getClass()))) {
            TransformationProperties properties = (TransformationProperties) obj;
            equals = (ObjectUtils.sameDouble(getA0(), properties.getA0())
                    && ObjectUtils.sameDouble(getA1(), properties.getA1())
                    && ObjectUtils.sameDouble(getA2(), properties.getA2()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0x712A;
        int mult = 753;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + (int) getA0();
        hashCode = hashCode * mult + (int) getA1();
        hashCode = hashCode * mult + (int) getA2();
        return hashCode;
    }

}
