/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import java.util.EventObject;

import fr.soleil.comete.definition.data.target.scalar.IBooleanComponent;
import fr.soleil.comete.definition.data.target.scalar.IEditableComponent;
import fr.soleil.comete.definition.listener.ICheckBoxListener;

public interface ICheckBox extends IBooleanComponent, IEditableComponent {

    public void addCheckBoxListener(ICheckBoxListener listener);

    public void removeCheckBoxListener(ICheckBoxListener listener);

    public void fireSelectedChanged(final EventObject event);

    public void setFalseLabel(String falseLabel);

    public String getFalseLabel();

    public void setTrueLabel(String trueLabel);

    public String getTrueLabel();

}
