/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

public class BarProperties implements IDataViewProperties, Cloneable, Serializable {

    private static final long serialVersionUID = 5779506268954761233L;

    private CometeColor fillColor;
    private int width;
    private int fillStyle;
    private int fillingMethod;

    public BarProperties() {
        this(CometeColor.LIGHT_GRAY);
    }

    public BarProperties(CometeColor color) {
        this(color, 10, 0, 2);
    }

    public BarProperties(CometeColor _fillColor, int _width, int _fillStyle, int _fillingMethod) {
        super();
        setFillColor(_fillColor);
        setFillingMethod(_fillingMethod);
        setWidth(_width);
        setFillStyle(_fillStyle);
    }

    /**
     * @return Returns the fillColor.
     */
    public CometeColor getFillColor() {
        return fillColor;
    }

    /**
     * @param fillColor The fillColor to set.
     */
    public void setFillColor(CometeColor fillColor) {
        if (fillColor == null) {
            fillColor = CometeColor.LIGHT_GRAY;
        }
        this.fillColor = fillColor;
    }

    /**
     * @return Returns the fillingMethod.
     */
    public int getFillingMethod() {
        return fillingMethod;
    }

    /**
     * @param fillingMethod The fillingMethod to set.
     */
    public void setFillingMethod(int fillingMethod) {
        this.fillingMethod = fillingMethod;
    }

    /**
     * @return Returns the fillStyle.
     */
    public int getFillStyle() {
        return fillStyle;
    }

    /**
     * @param fillStyle The fillStyle to set.
     */
    public void setFillStyle(int fillStyle) {
        this.fillStyle = fillStyle;
    }

    /**
     * @return Returns the width.
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width The width to set.
     */
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public BarProperties clone() {
        BarProperties properties;
        try {
            properties = (BarProperties) super.clone();
            properties.fillColor = (properties.fillColor == null ? null : properties.fillColor.clone());
        } catch (CloneNotSupportedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            properties = null;
        }
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && getClass().equals(obj.getClass())) {
            BarProperties properties = (BarProperties) obj;
            equals = (ObjectUtils.sameObject(getFillColor(), properties.getFillColor())
                    && (getWidth() == properties.getWidth()) && (getFillStyle() == properties.getFillStyle())
                    && (getFillingMethod() == properties.getFillingMethod()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0xBA12;
        int mult = 258;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getFillColor());
        hashCode = hashCode * mult + getWidth();
        hashCode = hashCode * mult + getFillStyle();
        hashCode = hashCode * mult + getFillingMethod();
        return hashCode;
    }

}
