/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import java.io.Serializable;
import java.util.Arrays;

import fr.soleil.lib.project.math.NumberArrayUtils;

/**
 * Gradient class
 */
public class Gradient implements Cloneable, Serializable {

    private static final long serialVersionUID = 8838879855510870798L;

    private CometeColor[] defaultColorVal;
    private double[] defaultColorPos;

    private CometeColor[] colorVal;
    private double[] colorPos;

    /**
     * Construct a default gradient black to white
     */
    public Gradient() {
        forgetDefaultRainbowGradient();
        buildMonochromeGradient();
    }

    public void buildEmptyGradient(int count) {
        if (count > 0) {
            colorVal = new CometeColor[count];
            Arrays.fill(colorVal, CometeColor.BLACK);
            colorPos = NumberArrayUtils.buildArray(0, 1.0d / count, count);
        }
    }

    /**
     * Constructs a monochrome gradient.
     */
    public void buildMonochromeGradient() {
        colorVal = new CometeColor[2];
        colorPos = new double[2];
        colorVal[0] = CometeColor.BLACK;
        colorVal[1] = CometeColor.WHITE;
        colorPos[0] = 0.0;
        colorPos[1] = 1.0;
    }

    /**
     * Constructs a rainbow gradient.
     */
    public void buildRainbowGradient() {
        CometeColor[] colorVal = this.defaultColorVal;
        double[] colorPos = this.defaultColorPos;
        if ((colorVal != null) && (colorPos != null) && (colorVal.length == colorPos.length)) {
            colorVal = colorVal.clone();
            colorPos = colorPos.clone();
        } else {
            colorVal = new CometeColor[5];
            colorPos = new double[5];

            colorVal[0] = new CometeColor(200, 0, 250); // Purple
            colorVal[1] = new CometeColor(40, 40, 255); // Blue
            colorVal[2] = new CometeColor(40, 255, 40); // Green
            colorVal[3] = new CometeColor(250, 250, 0); // Yellow
            colorVal[4] = new CometeColor(255, 0, 0); // Red

            colorPos[0] = 0.0;
            colorPos[1] = 0.25;
            colorPos[2] = 0.50;
            colorPos[3] = 0.75;
            colorPos[4] = 1.0;
        }
        this.colorVal = colorVal;
        this.colorPos = colorPos;
    }

    /**
     * Sets current rainbow gradient as default one to use.
     * i.e. every time {@link #buildRainbowGradient()} will be called, the same gradient as current one will be
     * built.
     * 
     * @see #buildRainbowGradient()
     */
    public void setAsDefaultRainbowGradient() {
        CometeColor[] colorVal = this.colorVal;
        double[] colorPos = this.colorPos;
        if ((colorVal != null) && (colorPos != null) && (colorVal.length == colorPos.length)) {
            defaultColorVal = colorVal.clone();
            defaultColorPos = colorPos.clone();
        }
    }

    /**
     * Forgets previously set default rainbow gradient.
     * 
     * @see #setAsDefaultRainbowGradient()
     */
    public void forgetDefaultRainbowGradient() {
        defaultColorVal = null;
        defaultColorPos = null;
    }

    /**
     * Invert color entries.
     */
    public void invertGradient() {
        CometeColor[] nColorVal = new CometeColor[colorVal.length];
        for (int i = 0; i < colorVal.length; i++) {
            nColorVal[i] = colorVal[colorVal.length - i - 1];
        }
        System.arraycopy(nColorVal, 0, colorVal, 0, colorVal.length);
    }

    /**
     * Build a color map for this gradient.
     * 
     * @param nb Number of color for the colormap
     * @return a nb 32Bit [ARGB] array, null if fails
     */
    public int[] buildColorMap(int nb) {
        int[] ret;
        if (colorVal == null) {
            ret = null;
        } else if (colorVal.length <= 1) {
            ret = null;
        } else {
            int colId;
            colId = 0;
            ret = new int[nb];
            for (int i = 0; i < nb; i++) {
                double r1, g1, b1;
                double r2, g2, b2;
                double r = (double) i / (double) nb;
                if (colId < colorPos.length - 2 && r >= colorPos[colId + 1]) {
                    colId++;
                }
                r1 = colorVal[colId].getRed();
                g1 = colorVal[colId].getGreen();
                b1 = colorVal[colId].getBlue();
                r2 = colorVal[colId + 1].getRed();
                g2 = colorVal[colId + 1].getGreen();
                b2 = colorVal[colId + 1].getBlue();
                double rr = (r - colorPos[colId]) / (colorPos[colId + 1] - colorPos[colId]);
                if (rr < 0.0) {
                    rr = 0.0;
                }
                if (rr > 1.0) {
                    rr = 1.0;
                }
                ret[i] = (int) (r1 + (r2 - r1) * rr) * 65536 + (int) (g1 + (g2 - g1) * rr) * 256
                        + (int) (b1 + (b2 - b1) * rr);
            }
        }
        return ret;
    }

    /**
     * Returns number of color entry.
     * 
     * @return Number of entry
     */
    public int getEntryNumber() {
        return colorVal.length;
    }

    /**
     * Returns color information for the specified entry
     * 
     * @param id Entry id
     * @return Color value
     */
    public CometeColor getColorAt(int id) {
        return colorVal[id];
    }

    /**
     * Returns the specified pos for the specified entry
     * 
     * @param id Entry id
     * @return A floating point between 0.0 and 1.0
     */
    public double getPosAt(int id) {
        return colorPos[id];
    }

    /**
     * Sets the color for a specified entry.
     * 
     * @param id Entry id
     * @param c New color value
     */
    public void setColorAt(int id, CometeColor c) {
        if ((id >= 0) && (id < colorVal.length)) {
            colorVal[id] = c;
        }
    }

    /**
     * Sets the position for a specified id. Position value must be greater that the previous pos
     * and lower than the next pos. It must also be in the range 0.0 => 1.0. If those conditions are
     * not validated , no change happens.
     * 
     * @param id Entry id
     * @param pos New position value
     */
    public void setPosAt(int id, double pos) {
        if ((id >= 0) && (id < colorPos.length)) {
            colorPos[id] = pos;
        }
    }

    /**
     * Adds a color,pos entry to the gradient Note that you have by default 2 entries at 0.0 and 1.0
     * 
     * @param c Color value
     * @param pos Position [0.0 => 1.0]
     * @return Entry id (-1 when fails)
     */
    public int addEntry(CometeColor c, double pos) {
        boolean found;
        int i;
        if (pos <= 0.0 || pos >= 1.0) {
            return -1;
        }
        found = false;
        i = 0;
        while (i < colorPos.length && !found) {
            found = pos < colorPos[i];
            if (!found) {
                i++;
            }
        }
        if (found) {
            // Check validity
            if (Math.abs(colorPos[i] - pos) < 1e-2) {
                return -1;
            }
            // Oki :)
            CometeColor[] oldColor = colorVal;
            double[] oldPos = colorPos;
            colorVal = new CometeColor[colorPos.length + 1];
            colorPos = new double[colorVal.length];
            System.arraycopy(oldColor, 0, colorVal, 0, i);
            System.arraycopy(oldPos, 0, colorPos, 0, i);
            colorVal[i] = c;
            colorPos[i] = pos;
            System.arraycopy(oldColor, i, colorVal, i + 1, oldColor.length - i);
            System.arraycopy(oldPos, i, colorPos, i + 1, oldColor.length - i);
            return i;
        }
        // Not found
        return -1;
    }

    /**
     * Removes a color entry
     * 
     * @param id Entry id (Cannot be fisrt or last value)
     */
    public void removeEntry(int id) {
        if (id > 0 && id < colorVal.length - 1) {
            CometeColor[] oldColor = colorVal;
            double[] oldPos = colorPos;
            colorVal = new CometeColor[colorPos.length - 1];
            colorPos = new double[colorVal.length];
            System.arraycopy(oldColor, 0, colorVal, 0, id);
            System.arraycopy(oldColor, id + 1, colorVal, id, oldColor.length - id - 1);
            System.arraycopy(oldPos, 0, colorPos, 0, id);
            System.arraycopy(oldPos, id + 1, colorPos, id, oldPos.length - id - 1);
        }
    }

    @Override
    public Gradient clone() {
        Gradient clone;
        try {
            clone = (Gradient) super.clone();
            if (clone.colorPos != null) {
                clone.colorPos = clone.colorPos.clone();
            }
            if (clone.colorVal != null) {
                clone.colorVal = clone.colorVal.clone();
            }
        } catch (CloneNotSupportedException e) {
            // Will never happen as Gradient implements Cloneable
            clone = null;
        }
        return clone;
    }

}
