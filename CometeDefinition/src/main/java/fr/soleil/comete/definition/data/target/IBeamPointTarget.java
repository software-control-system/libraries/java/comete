/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.target;

import fr.soleil.data.target.ITarget;

/**
 * An ITarget that manages beam point (PONI) coordinates
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IBeamPointTarget extends ITarget {

    /**
     * Returns the beam point
     * 
     * @return A <code>double[]</code>: <code>{centerX, centerZ}</code>
     */
    public double[] getBeamPoint();

    /**
     * Sets the beam point
     * 
     * @param beamPoint The beam point to set, that should be represented that way: <code>{centerX, centerZ}</code>
     */
    public void setBeamPoint(double... beamPoint);

    /**
     * Clears the beam point (i.e. forget the known value)
     */
    public void clearBeamPoint();

}
