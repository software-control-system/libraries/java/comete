/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.definition.widget.util.Gradient;

/**
 * Properties that define a display configuration for an {@link IImageViewer}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ImageProperties implements Cloneable, Serializable {

    private static final long serialVersionUID = 5809013196659261268L;

    protected boolean fitMaxSize;
    protected boolean bestfit;
    protected double fitMin;
    protected double fitMax;
    protected double zoom;
    protected String[] rois;
    protected Gradient gradient;
    protected boolean logScale;

    /**
     * Constructs a new {@link ImageProperties}
     * 
     * @param fitMaxSize Whether associated {@link IImageViewer} should fit max size
     * @param bestfit Whether associated {@link IImageViewer} should use auto best fit
     * @param fitMin Associated {@link IImageViewer} minimum fit
     * @param fitMax Associated {@link IImageViewer} maximum fit
     * @param zoom Associated {@link IImageViewer} zoom
     * @param rois Associated {@link IImageViewer} rois
     * @param gradient Associated {@link IImageViewer} {@link Gradient}
     * @param logScale Whether associated {@link IImageViewer} should use log scale
     */
    public ImageProperties(boolean fitMaxSize, boolean bestfit, double fitMin, double fitMax, double zoom,
            String[] rois, Gradient gradient, boolean logScale) {
        super();
        this.fitMaxSize = fitMaxSize;
        this.bestfit = bestfit;
        this.fitMin = fitMin;
        this.fitMax = fitMax;
        this.zoom = zoom;
        this.rois = rois;
        this.gradient = gradient;
        this.logScale = logScale;
    }

    /**
     * Returns whether associated {@link IImageViewer} should fit max size
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isFitMaxSize() {
        return fitMaxSize;
    }

    /**
     * Returns whether associated {@link IImageViewer} should use auto best fit
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isBestfit() {
        return bestfit;
    }

    /**
     * Returns the associated {@link IImageViewer} minimum fit
     * 
     * @return A <code>double</code> value
     */
    public double getFitMin() {
        return fitMin;
    }

    /**
     * Returns the associated {@link IImageViewer} maximum fit
     * 
     * @return A <code>double</code> value
     */
    public double getFitMax() {
        return fitMax;
    }

    /**
     * Returns associated {@link IImageViewer} zoom
     * 
     * @return A <code>double</code> value
     */
    public double getZoom() {
        return zoom;
    }

    /**
     * Returns associated {@link IImageViewer} rois
     * 
     * @return A {@link String} array
     */
    public String[] getRois() {
        return rois;
    }

    /**
     * Returns associated {@link IImageViewer} {@link Gradient}
     * 
     * @return A {@link Gradient}
     */
    public Gradient getGradient() {
        return gradient;
    }

    /**
     * Returns whether associated {@link IImageViewer} should use log scale
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isLogScale() {
        return logScale;
    }

    @Override
    protected ImageProperties clone() {
        ImageProperties clone;
        try {
            clone = (ImageProperties) super.clone();
            clone.gradient = (clone.gradient == null ? null : clone.gradient.clone());
            clone.rois = (clone.rois == null ? null : clone.rois.clone());
        } catch (CloneNotSupportedException e) {
            // Will never happen as ImageProperties implements Cloneable
            clone = null;
        }
        return clone;
    }

}
