/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import java.util.EventObject;

import fr.soleil.comete.definition.widget.IDataSavableComponent;

/**
 * An {@link EventObject} to notify for some changes in an {@link IDataSavableComponent}'s file
 * saving
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TransferEvent extends EventObject {

    private static final long serialVersionUID = -2110012117418612634L;

    private TransferReason reason;

    public TransferEvent(IDataSavableComponent source, TransferReason reason) {
        super(source);
        this.reason = reason;
    }

    @Override
    public IDataSavableComponent getSource() {
        return (IDataSavableComponent) super.getSource();
    }

    /**
     * Returns the reason of this event
     * 
     * @return A {@link TransferReason}
     */
    public TransferReason getReason() {
        return reason;
    }

    /**
     * Represents the different possible reasons of a {@link TransferEvent}
     */
    public static enum TransferReason {
        /**
         * Represents the fact that some data was saved in a file
         */
        SAVED,
        /**
         * Represents the fact that some data was loaded from a file
         */
        LOADED,
        /**
         * Represents the fact that the data save/load directory changed, without saving/loading
         * file
         */
        PATH
    }

}
