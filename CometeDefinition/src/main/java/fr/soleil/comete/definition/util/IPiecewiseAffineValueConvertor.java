/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

/**
 * An {@link IValueConvertor} that may use a piece wise affine function/interpolation to convert values.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IPiecewiseAffineValueConvertor extends IValueConvertor {

    /**
     * Returns whether this {@link IValueConvertor} will use a piecewise affine function/interpolation to compute
     * converted value.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isPiecewiseAffine();

    /**
     * Sets whether this {@link IValueConvertor} should use a piecewise affine function/interpolatio to compute
     * converted value.
     * 
     * @param piecewiseAffine Whether this {@link IValueConvertor} should use a piecewise affine function/interpolatio
     *            to compute converted value.
     */
    public void setPiecewiseAffine(boolean piecewiseAffine);

}
