/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import fr.soleil.lib.project.ObjectUtils;

public class CometeFont implements IFont, Cloneable {

    private static final long serialVersionUID = -8270015489429751957L;

    public static final int PLAIN = 0;
    public static final String SANS_SERIF = "SansSerif";

    private String name = SANS_SERIF;
    private int size = 12;
    private int style = PLAIN;

    public static final CometeFont DEFAULT_FONT = new CometeFont(SANS_SERIF, PLAIN, 12);

    public CometeFont(String afontName, int style, int size) {
        name = afontName;
        this.size = size;
        this.style = style;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int getStyle() {
        return style;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if ((obj != null) && (obj.getClass().equals(getClass()))) {
            CometeFont font = (CometeFont) obj;
            equals = ((ObjectUtils.sameObject(getName(), font.getName())) && (getSize() == font.getSize())
                    && (getStyle() == font.getStyle()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xC03E7E;
        int mult = 0xF027;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getName());
        code = code * mult + getSize();
        code = code * mult + getStyle();
        return code;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder("{");
        buffer.append(getClass().getName()).append(": ");
        buffer.append("Name = ").append(getName()).append(", ");
        buffer.append("Style = ").append(getStyle()).append(", ");
        buffer.append("Size = ").append(getSize()).append("}");
        return buffer.toString();
    }

    @Override
    public CometeFont clone() {
        CometeFont clone;
        try {
            clone = (CometeFont) super.clone();
        } catch (CloneNotSupportedException e) {
            // Will never happen as CometeFont implements Cloneable
            clone = null;
        }
        return clone;
    }

}
