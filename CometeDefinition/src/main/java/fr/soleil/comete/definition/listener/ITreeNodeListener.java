/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.listener;

import java.util.EventListener;

import fr.soleil.comete.definition.event.TreeNodeEvent;
import fr.soleil.comete.definition.widget.util.ITreeNode;

/**
 * An interface for something that listens to an {@link ITreeNode}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ITreeNodeListener extends EventListener {

    /**
     * Notifies this {@link ITreeNodeListener} that a change occurred in an {@link ITreeNode}
     * 
     * @param event The event that describes the change
     */
    public void nodeChanged(TreeNodeEvent event);

}
