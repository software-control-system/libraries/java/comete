/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import fr.soleil.comete.definition.widget.IImageViewer;

/**
 * A {@link CometeEvent} to warn for some changes about an {@link IImageViewer}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ImageViewerEvent extends CometeSelectionEvent<IImageViewer> {

    private static final long serialVersionUID = 7588309994508933002L;

    private int dimX;
    private Object value;
    private Reason reason;

    /**
     * Constructor
     * 
     * @param source The {@link IImageViewer} that generated this event
     * @param point The selected point, if concerned
     * @param value The associated value
     * @param reason The reason of this event
     */
    public ImageViewerEvent(IImageViewer source, double[] point, Object value, Reason reason) {
        super(source, point);
        this.value = value;
        this.reason = reason;
        dimX = (source == null ? 0 : source.getImageWidth());
    }

    /**
     * Returns the value associated with this event
     * 
     * @return An {@link Object}
     */
    public Object getValue() {
        return value;
    }

    /**
     * Returns the reason of this event
     * 
     * @return A {@link Reason} :
     *         <ul>
     *         <li>{@link Reason#SELECTION} for an event due to a selection</li>
     *         <li>{@link Reason#VALUE} for an event due to a value change</li>
     *         </ul>
     */
    public Reason getReason() {
        return reason;
    }

    @Override
    public int getSelectedIndex() {
        int index = -1;
        if ((point != null) && (point.length == 2)) {
            index = (int) (point[1] * dimX + point[0]);
        }
        return index;
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    /**
     * Describes the reasons to generate such an event
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    public static enum Reason {
        /**
         * Represents a selection change event
         */
        SELECTION,
        /**
         * Represents a value change event
         */
        VALUE
    }

}
