/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import java.util.EventObject;

import fr.soleil.comete.definition.data.target.scalar.IEditableComponent;
import fr.soleil.comete.definition.data.target.scalar.IFormatableComponent;
import fr.soleil.comete.definition.data.target.scalar.INumberComponent;
import fr.soleil.comete.definition.listener.ISpinnerListener;

public interface ISpinner extends INumberComponent, IFormatableComponent, IEditableComponent {

    public void addSpinnerListener(ISpinnerListener listener);

    public void removeSpinnerListener(ISpinnerListener listener);

    public void fireValueChanged(final EventObject event);

    @Override
    public Number getNumberValue();

    @Override
    public void setNumberValue(Number value);

    public Number getMinimum();

    public void setMinimum(Number minimum);

    public Number getMaximum();

    public void setMaximum(Number maximum);

    public Number getStep();

    public void setStep(Number step);

}
