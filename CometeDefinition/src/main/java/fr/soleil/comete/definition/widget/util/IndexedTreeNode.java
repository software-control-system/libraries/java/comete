/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

/**
 * A {@link BasicTreeNode} that knows at which index it should be in parent node's children list.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
// DATAREDUC-242: Added the possibility to specify index in node
public class IndexedTreeNode extends BasicTreeNode {

    private final int index;

    /**
     * Construct a new {@link IndexedTreeNode} with specified index
     * 
     * @param index The index
     */
    public IndexedTreeNode(int index) {
        super();
        this.index = index;
    }

    /**
     * Returns the index at which this {@link IndexedTreeNode} should be in parent node's children list.
     * 
     * @return An <code>int</code>
     */
    public int getIndex() {
        return index;
    }

    @Override
    protected int generateNewNodeIndex(ITreeNode node) {
        int refIndex = super.generateNewNodeIndex(node);
        int index;
        if (node instanceof IndexedTreeNode) {
            index = ((IndexedTreeNode) node).getIndex();
            if ((index < 0) || (index > refIndex)) {
                index = refIndex;
            }
        } else {
            index = refIndex;
        }
        return index;
    }

}
