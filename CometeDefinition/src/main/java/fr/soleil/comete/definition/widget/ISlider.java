/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import fr.soleil.comete.definition.data.target.scalar.IEditableComponent;
import fr.soleil.comete.definition.listener.ISliderListener;

public interface ISlider extends IRangeNumberComponent, IEditableComponent {

    public void addSliderListener(ISliderListener listener);

    public void removeSliderListener(ISliderListener listener);

    public double getDoubleValue();

    public void setDoubleValue(double value);

    public void setMinimumDouble(double min);

    public void setMaximumDouble(double max);

    public void setMinorTickSpacing(int space);

    public void setMajorTickSpacing(int space);

    public int getMinimum();

    public int getMaximum();

    public double getMinimumDouble();

    public double getMaximumDouble();

    public int getMinorTickSpacing();

    public int getMajorTickSpacing();

    public double getStep();

    public void setStep(double step);

    public void setLabelTable(String[] labels, int[] values);

    public void setPaintTicks(boolean paint);

    public void setPaintLabels(boolean paint);

    public boolean isPaintTicks();

    public boolean isPaintLabels();

}
