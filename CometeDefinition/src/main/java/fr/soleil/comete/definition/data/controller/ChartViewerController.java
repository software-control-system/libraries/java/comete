/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import fr.soleil.comete.definition.data.adapter.DataArrayAdapter;
import fr.soleil.comete.definition.data.target.complex.IDataArrayTarget;
import fr.soleil.data.adapter.source.DataSourceAdapter;
import fr.soleil.data.controller.DataTargetController;
import fr.soleil.data.exception.UnhandledTargetSignatureException;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;

/**
 * This class allows you to connect a {@link ITarget} which can read an array of double of rank 3
 * with a {@link AbstractDataSource} sending a {@link Map}. This class does not handle data send by
 * the {@link ITarget}.
 * 
 * @author huriez
 * 
 * @param <double> the primitive type of the {@link ITarget}
 */
public class ChartViewerController<U> extends DataTargetController<U, Map<String, Object>> {

    protected boolean multiDataViewEnabled;

    private final Map<AbstractDataSource<U>, Map<String, Object>> sourceDataMemoryMap;

    private final GenericDescriptor sourceType;

    /**
     * Constructor
     * 
     */
    public ChartViewerController(GenericDescriptor sourceType) {
        super();
        sourceDataMemoryMap = new WeakHashMap<AbstractDataSource<U>, Map<String, Object>>();
        multiDataViewEnabled = false;
        this.sourceType = sourceType;
    }

    @Override
    public DataSourceAdapter<U, Map<String, Object>> createAdapter(AbstractDataSource<U> source) {
        return new DataArrayAdapter<U>(source);
    }

    @Override
    public <I> Map<String, Object> getCastedInfo(I info) {
        Map<String, Object> result = null;
        // Object subElem = ArraySet.getSubElement(info);
        // if (subElem != null && info.getClass().isInstance(targetType)) {
        // result = (Object[][]) info;
        // }
        return result;
    }

    @Override
    public <I> boolean isInstanceOf(I info) {
        boolean result = false;
        // Object subElem = ArraySet.getSubElement(info);
        // if (subElem != null) {
        // result = targetType.isAssignableFrom(info.getClass());
        // }
        return result;
    }

    @Override
    protected TargetSignature<Map<String, Object>> initSignature() throws UnhandledTargetSignatureException {
        return null;
    }

    @Override
    protected void sendDataToTargetUsingSignature(ITarget target, Map<String, Object> targetValue,
            AbstractDataSource<U> source) {
        if (target instanceof IDataArrayTarget) {
            if (multiDataViewEnabled) {
                // In this mode, many sources may interact with the target.
                // So, we need to keep memory of what change they brought,
                // to do some cleaning when necessary
                if (source != null) {
                    List<String> dataToRemove = new ArrayList<String>();
                    synchronized (sourceDataMemoryMap) {
                        Map<String, Object> lastData = sourceDataMemoryMap.get(source);
                        if (lastData != null) {
                            for (String key : lastData.keySet()) {
                                if ((targetValue == null) || (!targetValue.containsKey(key))) {
                                    dataToRemove.add(key);
                                }
                            }
                        }
                        sourceDataMemoryMap.put(source, targetValue);
                    }
                    if (!dataToRemove.isEmpty()) {
                        ((IDataArrayTarget) target).removeData(dataToRemove);
                        dataToRemove.clear();
                    }
                }
                ((IDataArrayTarget) target).addData(targetValue);
            } else {
                ((IDataArrayTarget) target).setData(targetValue);
            }
        }
    }

    @Override
    public void removeLink(AbstractDataSource<U> source, ITarget target) {
        boolean wasLinked = hasLink(source, target);
        super.removeLink(source, target);
        // try to remove source data from target
        if (wasLinked && (target instanceof IDataArrayTarget) && (source != null)) {
            List<String> toRemove = new ArrayList<String>();
            synchronized (sourceDataMemoryMap) {
                Map<String, Object> lastData = sourceDataMemoryMap.get(source);
                if (lastData != null) {
                    toRemove.addAll(lastData.keySet());
                }
            }
            if (!toRemove.isEmpty()) {
                ((IDataArrayTarget) target).removeData(toRemove);
                toRemove.clear();
            }
        }
    }

    @Override
    public boolean isTargetAssignable(ITarget target) {
        return target instanceof IDataArrayTarget;
    }

    @Override
    public GenericDescriptor getSourceType() {
        return sourceType;
    }

    @Override
    public GenericDescriptor getTargetType() {
        return new GenericDescriptor(Map.class,
                new GenericDescriptor[] { new GenericDescriptor(String.class), new GenericDescriptor(Object.class) });
    }

    /**
     * @return the multiDataViewEnabled
     */
    public boolean isMultiDataViewEnabled() {
        return multiDataViewEnabled;
    }

    /**
     * @param multiDataViewEnabled the multiDataViewEnabled to set
     */
    public void setMultiDataViewEnabled(boolean multiDataViewEnabled) {
        this.multiDataViewEnabled = multiDataViewEnabled;
    }

}
