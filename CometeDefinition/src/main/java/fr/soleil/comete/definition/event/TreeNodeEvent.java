/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import java.util.EventObject;

import fr.soleil.comete.definition.widget.util.ITreeNode;

/**
 * An {@link EventObject} to warn for some changes about an {@link ITreeNode}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TreeNodeEvent extends EventObject {

    private static final long serialVersionUID = -8135806770609532244L;

    private NodeEventReason reason;
    private int[] indexes;
    private ITreeNode[] children;

    /**
     * Constructor
     * 
     * @param source The {@link ITreeNode} that is source of this event
     * @param reason The reason of the event
     */
    public TreeNodeEvent(ITreeNode source, NodeEventReason reason) {
        this(source, reason, null, null);
    }

    /**
     * Constructor
     * 
     * @param source The {@link ITreeNode} that is source of this event
     * @param reason The reason of the event
     * @param children The concerned children
     * @param indexes The concerned children indexes
     */
    public TreeNodeEvent(ITreeNode source, NodeEventReason reason, ITreeNode[] children, int[] indexes) {
        super(source);
        this.reason = reason;
        this.children = children;
        this.indexes = indexes;
    }

    @Override
    public ITreeNode getSource() {
        return (ITreeNode) super.getSource();
    }

    /**
     * Returns the reason of this {@link TreeNodeEvent}
     * 
     * @return A {@link NodeEventReason}
     */
    public NodeEventReason getReason() {
        return reason;
    }

    /**
     * The inserted/removed children
     * 
     * @return An {@link ITreeNode} array
     */
    public ITreeNode[] getChildren() {
        return children;
    }

    /**
     * The inserted/removed children indexes
     * 
     * @return An {@link ITreeNode} array
     */
    public int[] getIndexes() {
        return indexes;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * The possibles reasons of a {@link TreeNodeEvent}
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    public static enum NodeEventReason {
        /**
         * Some {@link ITreeNode}s were inserted as children of this {@link TreeNodeEvent}'s source
         * {@link ITreeNode}
         */
        INSERTED,
        /**
         * Some children {@link ITreeNode}s were removed from this {@link TreeNodeEvent}'s source
         * {@link ITreeNode}
         */
        REMOVED,
        /**
         * <big><b><u>EXPERT USAGE ONLY</u></b></big>. Some children {@link ITreeNode}s were
         * temporary removed from this {@link TreeNodeEvent}'s source {@link ITreeNode}
         */
        REMOVED_TEMPORARY,
        /**
         * This {@link TreeNodeEvent}'s source {@link ITreeNode} had a change in its name
         */
        NAME,
        /**
         * This {@link TreeNodeEvent}'s source {@link ITreeNode} had a change in its data
         */
        DATA,
        /**
         * This {@link TreeNodeEvent}'s source {@link ITreeNode} had a change in its image
         */
        IMAGE
    }

}
