/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

public abstract class ATransformationProperties implements IDataViewProperties, Cloneable, Serializable {

    private static final long serialVersionUID = -3587802483293536637L;

    private double a0 = 0;

    public ATransformationProperties(double a0) {
        super();
        this.a0 = a0;
    }

    private boolean isInvalid() {
        return Double.isNaN(a0);
    }

    public double transform(double value) {
        double result;
        if (this.isInvalid()) {
            result = value;
        } else {
            result = a0 + value;
        }
        return result;
    }

    /**
     * @return Returns the a0.
     */
    public double getA0() {
        return a0;
    }

    /**
     * @param a0 The a0 to set.
     */
    public void setA0(double a0) {
        this.a0 = a0;
    }

    @Override
    public ATransformationProperties clone() {
        ATransformationProperties properties;
        try {
            properties = (ATransformationProperties) super.clone();
        } catch (CloneNotSupportedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            properties = null;
        }
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && (getClass().equals(obj.getClass()))) {
            ATransformationProperties properties = (ATransformationProperties) obj;
            equals = ObjectUtils.sameDouble(getA0(), properties.getA0());
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0xA712A;
        int mult = 753;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + (int) getA0();
        return hashCode;
    }

}
