/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import fr.soleil.comete.definition.widget.IComponent;

/**
 * A {@link CometeEvent} to warn for some changes about an {@link IComponent}
 * 
 * @param C The type of {@link IComponent} that can generate this event
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class CometeSelectionEvent<C extends IComponent> extends CometeEvent<C> {

    private static final long serialVersionUID = -4981774261089389934L;

    protected double[] point;

    /**
     * Constructor
     * 
     * @param source The {@link IComponent} that generated this event
     * @param point The selected point, if concerned
     */
    public CometeSelectionEvent(C source, double[] point) {
        super(source);
        this.point = point;
    }

    /**
     * Returns the selected point
     * 
     * @return A <code>double[]</code>. <code>null</code> if there is no selection
     */
    public double[] getSelectedPoint() {
        return point;
    }

    /**
     * Returns the index of the last selected data
     * 
     * @return An <code>int</code>. <code>-1</code> if there is no valid selection
     */
    public abstract int getSelectedIndex();

}
