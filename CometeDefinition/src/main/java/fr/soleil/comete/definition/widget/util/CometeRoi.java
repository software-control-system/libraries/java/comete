/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

public class CometeRoi implements IRoi {

    private String name;
    private int lineWidth;
    private int x;
    private int y;
    private RoiShape shape;
    private int height;
    private int width;
    private CometeColor color;

    public CometeRoi(String name, RoiShape shape, int lineWidth, int x, int y, int height, int width,
            CometeColor color) {
        this.name = name;
        this.shape = shape;
        this.lineWidth = lineWidth;
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.color = color;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getLineWidth() {
        return lineWidth;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public RoiShape getShape() {
        return shape;
    }

    @Override
    public CometeColor getCometeColor() {
        return color;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getWidth() {
        return width;
    }

}
