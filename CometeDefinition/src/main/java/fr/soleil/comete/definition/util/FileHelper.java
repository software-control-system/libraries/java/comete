/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;

import org.slf4j.Logger;

public class FileHelper {

    public static FileReader newFileReader(String fileName, PrintStream out) {
        return newFileReaderCommon(fileName, out);
    }

    public static FileReader newFileReader(String fileName, Logger logger) {
        return newFileReaderCommon(fileName, logger);
    }

    public static FileWriter newFileWriter(String fileName, Logger logger) {
        return newFileWriterCommon(fileName, logger);
    }

    public static FileWriter newFileWriter(String fileName, PrintStream out) {
        return newFileWriterCommon(fileName, out);
    }

    // Write one line if the config file
    public static void writeLine(FileWriter f, String writeLine, PrintStream out) {
        writeLineCommon(f, writeLine, out);
    }

    // Reads one line in the config file
    // return null when file is ended
    public static String readLine(FileReader f, PrintStream out) {
        return readLineCommon(f, out);
    }

    // Reads one line in the config file
    // return null when file is ended
    public static String readLine(FileReader f, Logger out) {
        return readLineCommon(f, out);
    }

    private static FileReader newFileReaderCommon(String fileName, Object out) {
        FileReader result;
        if (fileName == null) {
            result = null;
        } else {
            File file = new File(fileName);
            try {
                if (!file.exists() || (!file.isFile()) || (!file.canRead())) {
                    StringBuilder builder = new StringBuilder(fileName);
                    builder.append(" not found or not readable or not a file.");
                    if (out instanceof PrintStream) {
                        ((PrintStream) out).println(builder.toString());
                    } else if (out instanceof Logger) {
                        ((Logger) out).error(builder.toString());
                    }
                    result = null;
                } else {
                    result = new FileReader(file);
                }
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder("Cannot read file ");
                builder.append(fileName);
                if (out instanceof PrintStream) {
                    builder.append(' ').append(e.getMessage());
                    ((PrintStream) out).println(builder.toString());
                } else if (out instanceof Logger) {
                    ((Logger) out).error(builder.toString(), e);
                }
                result = null;
            }
        }
        return result;
    }

    private static FileWriter newFileWriterCommon(String fileName, Object out) {
        FileWriter result;
        if (fileName == null) {
            result = null;
        } else {
            File file = new File(fileName);
            try {
                result = new FileWriter(file);
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder("Cannot access file ");
                builder.append(fileName);
                if (out instanceof PrintStream) {
                    builder.append(' ').append(e.getMessage());
                    ((PrintStream) out).println(builder.toString());
                } else if (out instanceof Logger) {
                    ((Logger) out).error(builder.toString(), e);
                }
                result = null;
            }
        }
        return result;
    }

    // Write one line if the config file
    private static void writeLineCommon(FileWriter f, String writeLine, Object out) {
        if (f != null) {
            try {
                f.write(writeLine);
                f.write(System.getProperty("line.separator"));
            } catch (Exception e) {
                StringBuilder messageBuilder = new StringBuilder("Cannot write file");
                if (out instanceof PrintStream) {
                    messageBuilder.append(' ').append(e.getMessage());
                    ((PrintStream) out).println(messageBuilder);
                } else if (out instanceof Logger) {
                    ((Logger) out).error(messageBuilder.toString(), e);
                }
            }
        }
    }

    // Reads one line in the config file
    // return null when file is ended
    private static String readLineCommon(FileReader f, Object out) {
        String result;
        if (f == null) {
            result = null;
        } else {
            int c = 0;
            StringBuilder builder = new StringBuilder();
            boolean eor = false;
            while (!eor) {
                try {
                    c = f.read();
                } catch (IOException e) {
                    if (out instanceof PrintStream) {
                        ((PrintStream) out).println(f.toString() + " " + e.getMessage());
                    } else if (out instanceof Logger) {
                        ((Logger) out).error("Failed to read line in " + f, e);
                    }
                }
                // c = 9 corresponding to a tabulation
                boolean ok = (c >= 32 || c == 9);
                if (ok) {
                    builder.append((char) c);
                }
                eor = (c == -1) || ((!ok) && builder.length() > 0);
            }
            if (builder.length() == 0) {
                result = null;
            } else {
                result = builder.toString();
            }
        }
        return result;
    }

}
