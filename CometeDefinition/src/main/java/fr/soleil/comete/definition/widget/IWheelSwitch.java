/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import fr.soleil.comete.definition.data.target.scalar.IEditableTextComponent;
import fr.soleil.comete.definition.data.target.scalar.IFormatableComponent;
import fr.soleil.comete.definition.data.target.scalar.INumberComponent;
import fr.soleil.comete.definition.listener.IWheelSwitchListener;

public interface IWheelSwitch extends INumberComponent, IEditableTextComponent, IFormatableComponent {

    public void addWheelSwitchListener(IWheelSwitchListener listener);

    public void removeWheelSwitchListener(IWheelSwitchListener listener);

    @Override
    public void setEditable(boolean b);

    @Override
    public boolean isEditable();

    public void setMaxValue(double value);

    public void setMinValue(double value);

    public double getMaxValue();

    public double getMinValue();
}
