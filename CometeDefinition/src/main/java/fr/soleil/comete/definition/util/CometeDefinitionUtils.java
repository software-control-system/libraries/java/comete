/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.util;

public class CometeDefinitionUtils {

    private static final String PIPE = "|";
    private static final String UNDERSCORE = "_";
    private static final String NULL = "null";

    /**
     * Generates a default id for a {@link Class}.
     * 
     * @param idRequiring The {@link Class} that requires an id.
     * @return A {@link String}.
     */
    public static String generateIdForClass(Class<?> idRequiring) {
        return generateIdForClass(idRequiring, 200);
    }

    /**
     * Generates a default id for a {@link Class}, depending on a range for a random value.
     * 
     * @param idRequiring The {@link Class} that requires an id.
     * @param randomRange The range for a random value.
     * @return A {@link String}.
     */
    public static String generateIdForClass(Class<?> idRequiring, int randomRange) {
        String className = null;
        if (idRequiring != null) {
            className = idRequiring.getName();
        }
        return generateRandomID(className, randomRange);
    }

    /**
     * Generates a default id for a class name.
     * 
     * @param className The class name.
     * @return A {@link String}, never <code>null</code>.
     */
    public static final String generateRandomID(String className) {
        return generateRandomID(className, 5000);
    }

    /**
     * Generates a default id for a class name, depending on a range for a random value.
     * 
     * @param className The class name.
     * @param randomRange The range for a random value.
     * @return A {@link String}.
     */
    public static final String generateRandomID(String className, int randomRange) {
        StringBuilder idBuffer = new StringBuilder(className == null ? NULL : className);
        idBuffer.append(UNDERSCORE);
        idBuffer.append(System.currentTimeMillis());
        idBuffer.append(PIPE);
        idBuffer.append(Math.random() * randomRange);
        return idBuffer.toString();
    }

}
