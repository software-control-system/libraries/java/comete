/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and {@link AxisProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AxisPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG_Y1 = "Y1Axis";
    public static final String XML_TAG_Y2 = "Y2Axis";
    public static final String XML_TAG_X = "XAxis";

    public static final String GRID_STYLE_PROPERTY_XML_TAG = "gridStyle";
    public static final String GRID_VISIBLE_XML_TAG = "gridVisible";
    public static final String SHOW_SUB_GRID_PROPERTY_XML_TAG = "showSubGrid";
    public static final String POSITION_PROPERTY_XML_TAG = "position";
    public static final String SCALE_MIN_PROPERTY_XML_TAG = "scaleMin";
    public static final String SCALE_MAX_PROPERTY_XML_TAG = "scaleMax";
    public static final String SCALE_MODE_PROPERTY_XML_TAG = "scaleMode";
    public static final String AUTO_SCALE_PROPERTY_XML_TAG = "autoScale";
    public static final String LABEL_FORMAT_PROPERTY_XML_TAG = "labelFormat";
    public static final String LABEL_FONT_PROPERTY_XML_TAG = "labelFont";
    public static final String COLOR_PROPERTY_XML_TAG = "color";
    public static final String IS_VISIBLE_PROPERTY_XML_TAG = "isVisible";
    public static final String DRAW_OPPOSITE_PROPERTY_XML_TAG = "drawOpposite";
    public static final String TITLE_PROPERTY_XML_TAG = "title";
    public static final String TITLE_ALIGNMENT_PROPERTY_XML_TAG = "titleAlignment";
    public static final String LABEL_INTERVAL_XML_TAG = "labelInterval";
    public static final String DATE_FORMAT_XML_TAG = "dateFormatText";
    public static final String TIME_FORMAT_XML_TAG = "timeFormatText";

    /**
     * Transforms some {@link AxisProperties} into an {@link XMLLine}, identified by a tag
     * 
     * @param tag The tag that identifies the {@link XMLLine}
     * @param properties The {@link AxisProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(String tag, AxisProperties properties) {
        XMLLine openingLine = new XMLLine(tag, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttributeIfNotNull(POSITION_PROPERTY_XML_TAG, String.valueOf(properties.getPosition()));
            openingLine.setAttributeIfNotNull(SCALE_MIN_PROPERTY_XML_TAG, String.valueOf(properties.getScaleMin()));
            openingLine.setAttributeIfNotNull(SCALE_MAX_PROPERTY_XML_TAG, String.valueOf(properties.getScaleMax()));
            openingLine.setAttributeIfNotNull(SCALE_MODE_PROPERTY_XML_TAG, String.valueOf(properties.getScaleMode()));
            openingLine.setAttributeIfNotNull(AUTO_SCALE_PROPERTY_XML_TAG, String.valueOf(properties.isAutoScale()));
            openingLine.setAttributeIfNotNull(LABEL_FORMAT_PROPERTY_XML_TAG,
                    String.valueOf(properties.getLabelFormat()));
            openingLine.setAttributeIfNotNull(LABEL_FONT_PROPERTY_XML_TAG, fontToString(properties.getLabelFont()));
            openingLine.setAttributeIfNotNull(COLOR_PROPERTY_XML_TAG, colorToString(properties.getColor()));
            openingLine.setAttributeIfNotNull(IS_VISIBLE_PROPERTY_XML_TAG, String.valueOf(properties.isVisible()));
            openingLine.setAttributeIfNotNull(DRAW_OPPOSITE_PROPERTY_XML_TAG,
                    String.valueOf(properties.isDrawOpposite()));
            openingLine.setAttributeIfNotNull(TITLE_PROPERTY_XML_TAG, properties.getTitle());
            openingLine.setAttributeIfNotNull(LABEL_INTERVAL_XML_TAG,
                    String.valueOf(properties.getUserLabelInterval()));
            openingLine.setAttributeIfNotNull(GRID_VISIBLE_XML_TAG, String.valueOf(properties.isGridVisible()));
            openingLine.setAttributeIfNotNull(GRID_STYLE_PROPERTY_XML_TAG, String.valueOf(properties.getGridStyle()));
            openingLine.setAttributeIfNotNull(SHOW_SUB_GRID_PROPERTY_XML_TAG,
                    String.valueOf(properties.isSubGridVisible()));
            openingLine.setAttributeIfNotNull(TITLE_ALIGNMENT_PROPERTY_XML_TAG,
                    String.valueOf(properties.getTitleAlignment()));
            openingLine.setAttributeIfNotNull(DATE_FORMAT_XML_TAG, String.valueOf(properties.getDateFormat()));
            openingLine.setAttributeIfNotNull(TIME_FORMAT_XML_TAG, String.valueOf(properties.getTimeFormat()));
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link AxisProperties}
     * 
     * @param currentSubNode The {@link Node} to interpret
     * @return The recovered {@link AxisProperties}
     */
    public static AxisProperties loadAxis(Node currentSubNode) {
        return loadAxisFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Node} to recover some {@link AxisProperties}
     * 
     * @param currentSubNode The {@link Node} to interpret
     * @param defaultLabelFont The default label font to use if not configured.
     * @return The recovered {@link AxisProperties}
     */
    public static AxisProperties loadAxis(Node currentSubNode, CometeFont defaultLabelFont) {
        return loadAxisFromXmlNodeAttributes(buildAttributeMap(currentSubNode), defaultLabelFont);
    }

    /**
     * Interprets a {@link Map} to recover some {@link AxisProperties}
     * 
     * @param propertyMap The {@link Map} to interpret. The {@link Map} is supposed to contain some xml node attributes
     * @return The recovered {@link AxisProperties}
     */
    public static AxisProperties loadAxisFromXmlNodeAttributes(final Map<String, String> propertyMap) {
        return loadAxisFromXmlNodeAttributes(propertyMap, ChartProperties.DEFAULT_FONT);
    }

    /**
     * Interprets a {@link Map} to recover some {@link AxisProperties}
     * 
     * @param propertyMap The {@link Map} to interpret. The {@link Map} is supposed to contain some xml node attributes
     * @param defaultLabelFont The default label font to use if not configured.
     * @return The recovered {@link AxisProperties}
     */
    public static AxisProperties loadAxisFromXmlNodeAttributes(final Map<String, String> propertyMap,
            CometeFont defaultLabelFont) {
        AxisProperties axis = new AxisProperties();
        if (propertyMap != null) {
            String position_s = propertyMap.get(POSITION_PROPERTY_XML_TAG);
            String scaleMode_s = propertyMap.get(SCALE_MODE_PROPERTY_XML_TAG);
            String labelFormat_s = propertyMap.get(LABEL_FORMAT_PROPERTY_XML_TAG);
            String labelFont_s = propertyMap.get(LABEL_FONT_PROPERTY_XML_TAG);
            String showSubGrid_s = propertyMap.get(SHOW_SUB_GRID_PROPERTY_XML_TAG);
            String drawOpposite_s = propertyMap.get(DRAW_OPPOSITE_PROPERTY_XML_TAG);
            String isVisible_s = propertyMap.get(IS_VISIBLE_PROPERTY_XML_TAG);
            String autoScale_s = propertyMap.get(AUTO_SCALE_PROPERTY_XML_TAG);
            String color_s = propertyMap.get(COLOR_PROPERTY_XML_TAG);
            String scaleMax_s = propertyMap.get(SCALE_MAX_PROPERTY_XML_TAG);
            String scaleMin_s = propertyMap.get(SCALE_MIN_PROPERTY_XML_TAG);
            String title_s = propertyMap.get(TITLE_PROPERTY_XML_TAG);
            String labelInterval_s = propertyMap.get(LABEL_INTERVAL_XML_TAG);
            // String annotation_s = propertyMap.get(ANNOTATION_XML_TAG);
            String gridVisible_s = propertyMap.get(GRID_VISIBLE_XML_TAG);
            String titleAlignment_s = propertyMap.get(TITLE_ALIGNMENT_PROPERTY_XML_TAG);
            String gridStyle_s = propertyMap.get(GRID_STYLE_PROPERTY_XML_TAG);
            String dateFormat_s = propertyMap.get(DATE_FORMAT_XML_TAG);
            String timeFormat_s = propertyMap.get(TIME_FORMAT_XML_TAG);

            int position, scaleMode, labelFormat, /* annotation, */titleAlignment, gridStyle;
            CometeFont labelFont;
            double scaleMax, scaleMin, labelInterval;
            boolean showSubGrid, drawOpposite, isVisible, autoScale, gridVisible;
            CometeColor color = stringToColor(color_s);

            position = parseInt(position_s, axis.getPosition());
            scaleMode = parseInt(scaleMode_s, axis.getScaleMode());
            labelFormat = parseInt(labelFormat_s, axis.getLabelFormat());
            labelFont = stringToFont(labelFont_s, defaultLabelFont);
            titleAlignment = parseInt(titleAlignment_s, axis.getTitleAlignment());
            gridStyle = parseInt(gridStyle_s, axis.getGridStyle());
            scaleMax = parseDouble(scaleMax_s, axis.getScaleMax());
            scaleMin = parseDouble(scaleMin_s, axis.getScaleMin());
            labelInterval = parseDouble(labelInterval_s, axis.getUserLabelInterval());
            showSubGrid = parseBoolean(showSubGrid_s, axis.isSubGridVisible());
            drawOpposite = parseBoolean(drawOpposite_s, axis.isDrawOpposite());
            isVisible = parseBoolean(isVisible_s, axis.isVisible());
            autoScale = parseBoolean(autoScale_s, axis.isAutoScale());
            gridVisible = parseBoolean(gridVisible_s, axis.isGridVisible());

            try {
                if (NULL.equalsIgnoreCase(timeFormat_s.trim())) {
                    timeFormat_s = null;
                }
            } catch (Exception e) {
                timeFormat_s = null;
            }

            axis.setAutoScale(autoScale);
            axis.setColor(color);
            axis.setDrawOpposite(drawOpposite);
            axis.setLabelFormat(labelFormat);
            axis.setLabelFont(labelFont);
            axis.setPosition(position);
            axis.setScaleMax(scaleMax);
            axis.setScaleMin(scaleMin);
            axis.setScaleMode(scaleMode);
            axis.setSubGridVisible(showSubGrid);
            axis.setTitle(title_s);
            axis.setTitleAlignment(titleAlignment);
            axis.setVisible(isVisible);
            axis.setUserLabelInterval(labelInterval);
            axis.setGridVisible(gridVisible);
            axis.setGridStyle(gridStyle);
            axis.setDateFormat(dateFormat_s);
            axis.setTimeFormat(timeFormat_s);
        }
        return axis;
    }

}
