/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and {@link SmoothingProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class SmoothingPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "smoothing";
    public static final String METHOD_PROPERTY_XML_TAG = "method";
    public static final String NEIGHBORS_PROPERTY_XML_TAG = "neighbors";
    public static final String GAUSS_SIGMA_PROPERTY_XML_TAG = "gaussSigma";
    public static final String EXTRAPOLATION_PROPERTY_XML_TAG = "extrapolation";

    /**
     * Transforms some {@link SmoothingProperties} into an {@link XMLLine}
     * 
     * @param properties The {@link SmoothingProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(SmoothingProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttribute(METHOD_PROPERTY_XML_TAG, String.valueOf(properties.getMethod()));
            openingLine.setAttribute(NEIGHBORS_PROPERTY_XML_TAG, String.valueOf(properties.getNeighbors()));
            openingLine.setAttribute(GAUSS_SIGMA_PROPERTY_XML_TAG, String.valueOf(properties.getGaussSigma()));
            openingLine.setAttribute(EXTRAPOLATION_PROPERTY_XML_TAG, String.valueOf(properties.getExtrapolation()));
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link SmoothingProperties}
     * 
     * @param currentSubNode The {@link Node} to interpret
     * @return The recovered {@link SmoothingProperties}
     */
    public static SmoothingProperties loadSmoothing(Node currentSubNode) {
        return loadSmoothingFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Map} to recover some {@link SmoothingProperties}
     * 
     * @param propertyMap The {@link Map} to interpret. The {@link Map} is supposed to contain some xml node attributes
     * @return The recovered {@link SmoothingProperties}
     */
    public static SmoothingProperties loadSmoothingFromXmlNodeAttributes(final Map<String, String> propertyMap) {
        SmoothingProperties smoothing = new SmoothingProperties();
        if (propertyMap != null) {
            String extrapolation_s = propertyMap.get(EXTRAPOLATION_PROPERTY_XML_TAG);
            String gaussSigma_s = propertyMap.get(GAUSS_SIGMA_PROPERTY_XML_TAG);
            String method_s = propertyMap.get(METHOD_PROPERTY_XML_TAG);
            String neighbors_s = propertyMap.get(NEIGHBORS_PROPERTY_XML_TAG);

            int extrapolation, method, neighbors;
            double gaussSigma;
            extrapolation = parseInt(extrapolation_s, smoothing.getExtrapolation());
            method = parseInt(method_s, smoothing.getMethod());
            neighbors = parseInt(neighbors_s, smoothing.getNeighbors());
            gaussSigma = parseDouble(gaussSigma_s, smoothing.getGaussSigma());

            smoothing.setExtrapolation(extrapolation);
            smoothing.setMethod(method);
            smoothing.setNeighbors(neighbors);
            smoothing.setGaussSigma(gaussSigma);
        }
        return smoothing;
    }

}
