/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.data.controller;

import fr.soleil.data.controller.NumberMatrixController;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;

/**
 * This class allows you to connect a {@link ITarget} which can read an image with a {@link AbstractDataSource} sending
 * a {@link Matrix} of {@link Number}. This class does not handle
 * data send by the {@link ITarget}.
 * 
 * @author huriez
 * 
 */
public class ImageComponentController extends NumberMatrixController {

    /**
     * Constructor
     * 
     * @param classTarget the type of {@link ITarget} this controller has to manage
     */
    public ImageComponentController(Class<?> classTarget) {
        super(classTarget);
    }

}
