/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import fr.soleil.comete.definition.listener.MaskListener;

/**
 * Interface to handle {@link Mask}s.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface MaskHandler {

    /**
     * Register a new {@link MaskListener} for this MaskHandler
     * 
     * @param listener the {@link MaskListener}
     */
    public void addMaskListener(MaskListener listener);

    /**
     * Removes a {@link MaskListener} for this MaskHandler
     * 
     * @param listener the {@link MaskListener}
     */
    public void removeMaskListener(MaskListener listener);

    /**
     * Removes all {@link MaskListener} for this MaskHandler
     */
    public void removeAllMaskListeners();

    /**
     * Sets this MaskHandler's {@link Mask}
     * 
     * @param mask the {@link Mask} to set
     * @return a <code>boolean</code> value. <code>TRUE</code> if the {@link Mask} was correctly
     *         set. <code>FALSE</code> otherwise (for example: the {@link Mask} does not fit
     *         expected dimensions)
     */
    public boolean setMask(Mask mask);

    /**
     * Returns this MaskHandler's {@link Mask}
     * 
     * @return a {@link Mask}
     */
    public Mask getMask();

}
