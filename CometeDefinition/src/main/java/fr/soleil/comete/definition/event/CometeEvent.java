/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.event;

import java.util.EventObject;

import fr.soleil.comete.definition.widget.IComponent;

/**
 * Common class for events thrown by {@link IComponent}s
 * 
 * @param <C>
 *            The type of {@link IComponent} that generated this event
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class CometeEvent<C extends IComponent> extends EventObject {

    private static final long serialVersionUID = -785901590776290211L;

    public static final int KEY_ENTER = '\n';
    public static final int KEY_BACK_SPACE = '\b';
    public static final int KEY_TAB = '\t';
    public static final int KEY_CANCEL = 0x03;
    public static final int KEY_CLEAR = 0x0C;
    public static final int KEY_SHIFT = 0x10;
    public static final int KEY_CONTROL = 0x11;
    public static final int KEY_ALT = 0x12;
    public static final int KEY_PAUSE = 0x13;
    public static final int KEY_CAPS_LOCK = 0x14;
    public static final int KEY_ESCAPE = 0x1B;
    public static final int KEY_SPACE = 0x20;
    public static final int KEY_PAGE_UP = 0x21;
    public static final int KEY_PAGE_DOWN = 0x22;
    public static final int KEY_END = 0x23;
    public static final int KEY_HOME = 0x24;

    /**
     * Constructs a prototypical Event.
     * 
     * @param source
     *            The {@link IComponent} on which the Event initially occurred.
     */
    public CometeEvent(C source) {
        super(source);
    }

    @SuppressWarnings("unchecked")
    @Override
    public C getSource() {
        return (C) super.getSource();
    }

}
