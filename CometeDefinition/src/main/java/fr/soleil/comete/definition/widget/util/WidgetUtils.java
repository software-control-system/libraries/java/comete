/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import fr.soleil.comete.definition.widget.IImageViewer;

/**
 * A class that does some common operations to different Comete Widgets
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class WidgetUtils {

    /**
     * Returns whether an {@link IImageViewer} is in monochrome mode
     * 
     * @param viewer The {@link IImageViewer}
     * @return A <code>boolean</code> value
     * @deprecated Use {@link #isMonochrome(IImageViewer)} instead
     */
    @Deprecated
    public static boolean isMonochrom(IImageViewer viewer) {
        return isMonochrome(viewer);
    }

    /**
     * Returns whether an {@link IImageViewer} is in monochrome mode
     * 
     * @param viewer The {@link IImageViewer}
     * @return A <code>boolean</code> value
     */
    public static boolean isMonochrome(IImageViewer viewer) {
        boolean monochrome = false;
        if (viewer != null) {
            monochrome = isMonochrome(viewer.getGradient());
        }
        return monochrome;
    }

    /**
     * Returns whether a {@link Gradient} is in monochrome mode
     * 
     * @param gradient The {@link Gradient}
     * @return A <code>boolean</code> value
     */
    public static boolean isMonochrome(Gradient gradient) {
        boolean monochrome = false;
        if (gradient != null) {
            if (gradient.getEntryNumber() == 2) {
                boolean black = false, white = false;
                for (int i = 0; i < gradient.getEntryNumber(); i++) {
                    CometeColor color = gradient.getColorAt(i);
                    if (CometeColor.BLACK.equals(color)) {
                        black = true;
                    } else if (CometeColor.WHITE.equals(color)) {
                        white = true;
                    }
                }
                monochrome = (black && white);
            }
        }
        return monochrome;
    }

    /**
     * Sets an {@link IImageViewer} in monochrome or rainbow colors mode
     * 
     * @param imageViewer The concerned {@link IImageViewer}
     * @param monochrom Whether to use monochrome mode
     * @deprecated use {@link #setMonochrome(IImageViewer, boolean)} instead
     */
    @Deprecated
    public static void setMonochrom(IImageViewer imageViewer, boolean monochrom) {
        setMonochrome(imageViewer, monochrom);
    }

    /**
     * Sets an {@link IImageViewer} in monochrome or rainbow colors mode
     * 
     * @param imageViewer The concerned {@link IImageViewer}
     * @param monochrom Whether to use monochrome mode
     */
    public static void setMonochrome(IImageViewer imageViewer, boolean monochrom) {
        if (monochrom) {
            setMonochrome(imageViewer);
        } else {
            setRainbow(imageViewer);
        }
    }

    /**
     * Sets an {@link IImageViewer} in monochrome mode
     * 
     * @param imageViewer The concerned {@link IImageViewer}
     * @deprecated use {@link #setMonochrome(IImageViewer)} instead
     */
    @Deprecated
    public static void setMonochrom(IImageViewer imageViewer) {
        if (imageViewer != null) {
            imageViewer.setGradient(new Gradient());
        }
    }

    /**
     * Sets an {@link IImageViewer} in monochrome mode
     * 
     * @param imageViewer The concerned {@link IImageViewer}
     */
    public static void setMonochrome(IImageViewer imageViewer) {
        if (imageViewer != null) {
            imageViewer.setGradient(new Gradient());
        }
    }

    /**
     * Sets an {@link IImageViewer} in rainbow colors mode
     * 
     * @param imageViewer The concerned {@link IImageViewer}
     */
    public static void setRainbow(IImageViewer imageViewer) {
        if (imageViewer != null) {
            Gradient gradient = new Gradient();
            gradient.buildRainbowGradient();
            imageViewer.setGradient(gradient);
        }
    }

}
