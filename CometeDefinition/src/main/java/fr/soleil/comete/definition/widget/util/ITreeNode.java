/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.util;

import java.util.List;

import fr.soleil.comete.definition.listener.ITreeNodeListener;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetSignatureProvider.TargetSignature;
import fr.soleil.data.target.matrix.IMatrixTarget;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An interface that represents a tree node
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ITreeNode extends IMatrixTarget {

    /**
     * Returns the {@link ITreeNode} parent of this node
     * 
     * @return An {@link ITreeNode}
     */
    public ITreeNode getParent();

    /**
     * Sets the {@link ITreeNode} parent of this node
     * 
     * @param parent The parent to set
     */
    public void setParent(ITreeNode parent);

    /**
     * Returns this {@link ITreeNode}'s children
     * 
     * @return An {@link ITreeNode} {@link List}
     */
    public List<ITreeNode> getChildren();

    /**
     * Returns whether this {@link ITreeNode} is an ancestor of a given {@link ITreeNode}
     * 
     * @param node The {@link ITreeNode} to check
     * @return A <code>boolean</code> value. <code>TRUE</code> if this {@link ITreeNode} is the same
     *         as or an ancestor of parametered {@link ITreeNode}
     */
    public boolean isAncestorOf(ITreeNode node);

    /**
     * Adds some children nodes to this node
     * 
     * @param nodes The {@link ITreeNode}s to add
     */
    public void addNodes(ITreeNode... nodes);

    /**
     * Adds a child node to this node at a particular index
     * 
     * @param node The {@link ITreeNode} to add
     * @param index The index
     */
    public void addNode(ITreeNode node, int index);

    /**
     * Removes a child from this node
     * 
     * @param node The {@link ITreeNode} to remove
     */
    public void removeNode(ITreeNode node);

    /**
     * <big><b><u>EXPERT USAGE ONLY</u></b></big>. Temporary removes a child from this node. Used,
     * for example, when moving a node from a parent node to another one
     * 
     * @param node The {@link ITreeNode} to remove
     */
    public void removeNodeTemporary(ITreeNode node);

    /**
     * Removes a child of this node, that corresponds to a given index
     * 
     * @param index The index of the child to remove
     */
    public void removeNode(int index);

    /**
     * Returns the child {@link ITreeNode} that corresponds to a given index
     * 
     * @param index The index
     * @return An {@link ITreeNode}
     */
    public ITreeNode getNodeAt(int index);

    /**
     * Returns this {@link ITreeNode}'s name
     * 
     * @return A {@link String}
     */
    public String getName();

    /**
     * Sets this {@link ITreeNode}'s name
     * 
     * @param name The name to set
     */
    public void setName(String name);

    /**
     * Returns this {@link ITreeNode}'s tooltip
     * 
     * @return tooltip {@link String}
     */
    public String getToolTip();

    /**
     * Sets this {@link ITreeNode}'s tooltip
     * 
     * @param toolTip The tooltip to set
     */
    public void setToolTip(String toolTip);

    /**
     * Returns this {@link ITreeNode}'s data
     * 
     * @return An {@link Object}
     */
    public Object getData();

    /**
     * Sets this {@link ITreeNode}'s data
     * 
     * @param data The data to set
     */
    public void setData(Object data);

    /**
     * Returns this {@link ITreeNode}'s image
     * 
     * @return A {@link CometeImage}
     */
    public CometeImage getImage();

    /**
     * Sets this {@link ITreeNode}'s image
     * 
     * @param image The image to set
     */
    public void setImage(CometeImage image);

    /**
     * Adds an {@link ITreeNodeListener} to this node
     * 
     * @param listener The {@link ITreeNodeListener} to add
     */
    public void addTreeNodeListener(ITreeNodeListener listener);

    /**
     * Removes an {@link ITreeNodeListener} from this node
     * 
     * @param listener The {@link ITreeNodeListener} to remove
     */
    public void removeTreeNodeListener(ITreeNodeListener listener);

    /**
     * Removes all {@link ITreeNodeListener}s from this node
     */
    public void removeAllTreeNodeListeners();

    /**
     * Returns whether an {@link ITreeNodeListener} is registered as listener of this {@link ITreeNode}
     * 
     * @param listener The {@link ITreeNodeListener} to test
     * @return A <code>boolean</code> value
     */
    public boolean isTreeNodeListenerRegistered(ITreeNodeListener listener);

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public class ITreeNodeMatrixTargetSignature extends TargetSignature<ITreeNode[][]> {

        public ITreeNodeMatrixTargetSignature() {
            super(ITreeNode.class, ITreeNode.class, IMatrixTarget.class);
        }

        @Override
        protected void callFunction(ITarget target, ITreeNode[][] data) {
            ((ITreeNode) target).addNodes((ITreeNode[]) ArrayUtils.convertArrayDimensionFromNTo1(data));
        }
    }

}
