/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties;

import java.io.Serializable;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

public class ErrorProperties implements IDataViewProperties, Cloneable, Serializable {

    private static final long serialVersionUID = -5041990802377586561L;

    private CometeColor color;
    private boolean visible;

    public ErrorProperties() {
        this(CometeColor.RED);
    }

    public ErrorProperties(CometeColor color) {
        this(color, true);
    }

    public ErrorProperties(CometeColor color, boolean visible) {
        super();
        setColor(color);
        setVisible(visible);
    }

    /**
     * @return Returns the color.
     */
    public CometeColor getColor() {
        return color;
    }

    /**
     * @param color The color to set.
     */
    public void setColor(CometeColor color) {
        if (color == null) {
            color = CometeColor.RED;
        }
        this.color = color;
    }

    /**
     * Returns whether error should be visible
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Sets whether error should be visible
     * 
     * @param visible Whether error should be visible
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public ErrorProperties clone() {
        ErrorProperties properties;
        try {
            properties = (ErrorProperties) super.clone();
            properties.color = (properties.color == null ? null : properties.color.clone());
        } catch (CloneNotSupportedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(getClass().getName() + " failed to clone", e);
            properties = null;
        }
        return properties;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == this) {
            equals = true;
        } else if ((obj != null) && (getClass().equals(obj.getClass()))) {
            ErrorProperties properties = (ErrorProperties) obj;
            equals = (ObjectUtils.sameObject(getColor(), properties.getColor())
                    && (isVisible() == properties.isVisible()));
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0xE1212012;
        int mult = 0x91209;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + ObjectUtils.getHashCode(getColor());
        hashCode = hashCode * mult + ObjectUtils.generateHashCode(isVisible());
        return hashCode;
    }

}
