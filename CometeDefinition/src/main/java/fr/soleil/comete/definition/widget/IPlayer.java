/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget;

import fr.soleil.comete.definition.event.PlayerEvent;
import fr.soleil.comete.definition.listener.IPlayerListener;
import fr.soleil.comete.definition.widget.util.IPlayerAnimationBehavior;

/**
 * An {@link IComponent} that can play some data along a track
 * 
 * @author saintin
 */
public interface IPlayer extends ISnapshotableComponent {

    /**
     * Adds a listener to this {@link IPlayer}
     * 
     * @param listener The {@link IPlayerListener} to add
     */
    public void addPlayerListener(IPlayerListener listener);

    /**
     * Removes a listener from this {@link IPlayer}
     * 
     * @param listener The {@link IPlayerListener} to remove
     */
    public void removePlayerListener(IPlayerListener listener);

    /**
     * Forces the {@link IPlayer} to notify its listeners for some changes in index
     * 
     * @param event The {@link PlayerEvent} that will warn the listeners
     */
    public void fireIndexChanged(PlayerEvent event);

    /**
     * Forces this {@link IPlayer} to go at a particular index
     * 
     * @param index The index
     */
    public void setIndex(int index);

    /**
     * Returns this {@link IPlayer}'s read index
     * 
     * @return An <code>int</code>
     */
    public int getIndex();

    /**
     * Return the {@link IPlayer}'s last index
     * 
     * @return <code>int</code>
     */
    public int getMaximum();

    /**
     * Must be called when you want to change animation generation behavior
     * 
     * @param behavior
     */
    public void setPlayerAnimationBehavior(IPlayerAnimationBehavior behavior);

    /**
     * @return the current {@link IPlayerAnimationBehavior}
     */
    public IPlayerAnimationBehavior getPlayerAnimationBehavior();

    /**
     * Enabled / Disabled visibility of the animation generation button
     */
    public void setAnimationButtonVisible(boolean visible);

    /**
     * @return true is the animation generator button is visible, false otherwise
     */
    public boolean isAnimationButtonVisible();

    /**
     * Forces this {@link IPlayer} to reset its {@link ISlider} and put its maximum at a particular
     * value
     * 
     * @param size The {@link ISlider}'s maximum
     */
    public void initSlider(int size);

    /**
     * Returns this {@link IPlayer}'s {@link ISlider}
     * 
     * Note : this method return a slider with Swing and AWT player only.
     * 
     * @return An {@link ISlider}
     */
    @Deprecated
    public ISlider getSlider();

    /**
     * Returns the period in milliseconds between 2 steps during play
     * 
     * @return An <code>int</code>
     */
    public int getPeriodInMs();

    /**
     * Sets the period in milliseconds between 2 steps during play
     * 
     * @param periodInMs The period to set
     */
    public void setPeriodInMs(int periodInMs);

    /**
     * Stars playing
     */
    public void play();

    /**
     * Stops playing
     */
    public void stop();

    /**
     * Forces this {@link IPlayer} to go to 1st position
     */
    public void gotoFirst();

    /**
     * Forces this {@link IPlayer} to go to last position
     */
    public void gotoLast();

    /**
     * Forces this {@link IPlayer} to go to previous position
     */
    public void previous();

    /**
     * Forces this {@link IPlayer} to go to next position
     */
    public void next();

    /**
     * Returns whether this {@link IPlayer} restarts playing once the end is reached
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isRepeatActivated();

    /**
     * Sets whether this {@link IPlayer} restarts playing once the end is reached
     * 
     * @param activated Whether this {@link IPlayer} restarts playing once the end is reached.
     *            <code>TRUE</code> to loop, <code>FALSE</code> otherwise
     */
    public void setRepeatActivated(boolean activated);

}
