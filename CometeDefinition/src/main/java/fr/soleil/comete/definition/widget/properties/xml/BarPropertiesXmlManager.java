/*
 * This file is part of CometeDefinition.
 * 
 * CometeDefinition is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeDefinition is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeDefinition. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.definition.widget.properties.xml;

import java.io.File;
import java.util.Map;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * A class that allows some interaction between XML {@link File}s and
 * {@link BarProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BarPropertiesXmlManager extends PropertiesXmlManager {

    public static final String XML_TAG = "bar";
    public static final String COLOR_PROPERTY_XML_TAG = "color";
    public static final String WIDTH_PROPERTY_XML_TAG = "width";
    public static final String FILL_STYLE_PROPERTY_XML_TAG = "fillStyle";
    public static final String FILL_METHOD_PROPERTY_XML_TAG = "fillMethod";

    /**
     * Transforms some {@link BarProperties} into an {@link XMLLine}
     * 
     * @param properties
     *            The {@link BarProperties} to transform
     * @return An {@link XMLLine}
     */
    public static XMLLine toXmlLine(BarProperties properties) {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (properties != null) {
            openingLine.setAttribute(COLOR_PROPERTY_XML_TAG, colorToString(properties.getFillColor()));
            openingLine.setAttribute(WIDTH_PROPERTY_XML_TAG, String.valueOf(properties.getWidth()));
            openingLine.setAttribute(FILL_STYLE_PROPERTY_XML_TAG, String.valueOf(properties.getFillStyle()));
            openingLine.setAttribute(FILL_METHOD_PROPERTY_XML_TAG, String.valueOf(properties.getFillingMethod()));
        }
        return openingLine;
    }

    /**
     * Interprets a {@link Node} to recover some {@link BarProperties}
     * 
     * @param currentSubNode
     *            The {@link Node} to interpret
     * @return The recovered {@link BarProperties}
     */
    public static BarProperties loadBar(Node currentSubNode) {
        return loadBarFromXmlNodeAttributes(buildAttributeMap(currentSubNode));
    }

    /**
     * Interprets a {@link Map} to recover some {@link BarProperties}
     * 
     * @param propertyMap
     *            The {@link Map} to interpret. The {@link Map} is supposed to
     *            contain some xml node attributes
     * @return The recovered {@link BarProperties}
     */
    public static BarProperties loadBarFromXmlNodeAttributes(Map<String, String> propertyMap) {
        BarProperties bar = new BarProperties();
        if (propertyMap != null) {
            String color_s = propertyMap.get(COLOR_PROPERTY_XML_TAG);
            String style_s = propertyMap.get(FILL_STYLE_PROPERTY_XML_TAG);
            String width_s = propertyMap.get(WIDTH_PROPERTY_XML_TAG);
            String method_s = propertyMap.get(FILL_METHOD_PROPERTY_XML_TAG);

            CometeColor fillColor = stringToColor(color_s);
            int width, style, method;
            width = parseInt(width_s, bar.getWidth());
            style = parseInt(style_s, bar.getFillStyle());
            method = parseInt(method_s, bar.getFillingMethod());

            bar.setFillColor(fillColor);
            bar.setFillStyle(style);
            bar.setWidth(width);
            bar.setFillingMethod(method);
        }
        return bar;
    }

}
