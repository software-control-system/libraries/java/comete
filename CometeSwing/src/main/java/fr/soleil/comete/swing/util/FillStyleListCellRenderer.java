/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.image.BufferedImage;

import javax.swing.JLabel;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.chart.util.ChartUtils;

public class FillStyleListCellRenderer extends ImageableTypeListCellRenderer {

    private static final long serialVersionUID = -5932878290810296607L;

    private static final String STYLE_NO = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.no");
    private static final String STYLE_SOLID = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.solid");
    private static final String STYLE_HATCHING_LEFT_LARGE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.hatching.left.large");
    private static final String STYLE_HATCHING_LEFT_SMALL = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.hatching.left.small");
    private static final String STYLE_HATCHING_RIGHT_LARGE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.hatching.right.large");
    private static final String STYLE_HATCHING_RIGHT_SMALL = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.hatching.right.small");
    private static final String STYLE_HATCHING_CROSS_LARGE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.hatching.cross.large");
    private static final String STYLE_HATCHING_CROSS_SMALL = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.hatching.cross.small");
    private static final String STYLE_DOT_PATTERN_1 = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.dot.pattern.1");
    private static final String STYLE_DOT_PATTERN_2 = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.dot.pattern.2");
    private static final String STYLE_DOT_PATTERN_3 = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.dot.pattern.3");
    private static final String FILL_STYLE_INFO = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.tooltip");

    public FillStyleListCellRenderer() {
        super();
    }

    @Override
    public String getText(int id) {
        String text;
        switch (id) {
            case IChartViewer.FILL_STYLE_NONE:
                text = STYLE_NO;
                break;
            case IChartViewer.FILL_STYLE_SOLID:
                text = STYLE_SOLID;
                break;
            case IChartViewer.FILL_STYLE_LARGE_LEFT_HATCH:
                text = STYLE_HATCHING_LEFT_LARGE;
                break;
            case IChartViewer.FILL_STYLE_LARGE_RIGHT_HATCH:
                text = STYLE_HATCHING_RIGHT_LARGE;
                break;
            case IChartViewer.FILL_STYLE_LARGE_CROSS_HATCH:
                text = STYLE_HATCHING_CROSS_LARGE;
                break;
            case IChartViewer.FILL_STYLE_SMALL_LEFT_HATCH:
                text = STYLE_HATCHING_LEFT_SMALL;
                break;
            case IChartViewer.FILL_STYLE_SMALL_RIGHT_HATCH:
                text = STYLE_HATCHING_RIGHT_SMALL;
                break;
            case IChartViewer.FILL_STYLE_SMALL_CROSS_HATCH:
                text = STYLE_HATCHING_CROSS_SMALL;
                break;
            case IChartViewer.FILL_STYLE_DOT_PATTERN_1:
                text = STYLE_DOT_PATTERN_1;
                break;
            case IChartViewer.FILL_STYLE_DOT_PATTERN_2:
                text = STYLE_DOT_PATTERN_2;
                break;
            case IChartViewer.FILL_STYLE_DOT_PATTERN_3:
                text = STYLE_DOT_PATTERN_3;
                break;
            default:
                text = Integer.toString(id);
                break;
        }
        return text;
    }

    @Override
    protected BufferedImage getImage(int id, Color bg, Color fg) {
        Paint pattern = CometeUtils.createPatternForFilling(id, fg, bg, bg, false);
        return createImage(pattern == null ? bg : pattern, 20, 20);
    }

    /**
     * Given a pattern, create an image that shows it.
     * 
     * @param pattern the pattern to draw on the Icon.
     * @param width the width of the icon.
     * @param height the height of the icon.
     */
    protected static BufferedImage createImage(Paint pattern, int width, int height) {
        BufferedImage image;
        if (pattern == null) {
            image = null;
        } else {
            image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = (Graphics2D) image.getGraphics();
            g.setPaint(pattern);
            g.fillRect(0, 0, width, height);
        }
        return image;
    }

    @Override
    protected String getDefaultToolTipText(JLabel comp) {
        return FILL_STYLE_INFO;
    }

}
