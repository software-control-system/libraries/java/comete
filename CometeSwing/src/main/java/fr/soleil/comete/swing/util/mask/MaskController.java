/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util.mask;

import java.awt.Color;
import java.awt.Component;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MaskException;
import fr.soleil.comete.definition.widget.util.MaskHandler;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.IFileTarget;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.FileInformation;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.SnapshotUtil;
import fr.soleil.lib.project.swing.file.ExtensionFileFilter;

/**
 * A class used to control {@link Mask}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MaskController implements IFileTarget {

    public static final String MASK_COLUMN_SEPARATOR = ";";
    public static final String MASK_LINE_SEPARATOR = ObjectUtils.NEW_LINE;
    public static final String DEFAULT_MASK_FILE_NAME = "mask";
    public static final String LOAD_MASK_COMMAND = "loadmask";
    public static final String MASK_NAME_PREFIX = "mask_";
    protected static final String TXT_EXTENSION = "txt";
    protected static final String CSV_EXTENSION = "csv";
    protected static final String MASK_EXTENSION = "msk";

    protected volatile MaskInfo maskInfo;
    protected AtomicInteger maskId;
    protected Component parentComponent;

    // Messages
    protected String warningTitle = "Warning";
    protected String errorTitle = "Error";
    protected String writeErrorMessage = "Failed to write file !";
    protected String readErrorMessage = "Failed to read file !";
    protected String closeErrorMessage = "Failed to close file !";
    protected String fileNotFoundMessage = "Mask File not found";
    protected String invalidFileMessage = "Invalid mask file";
    protected String saveMaskTitle = "Save Mask...";
    protected String restoreMaskTitle = "Restore Mask...";
    protected String fileExistsMessage = "File exists. Overwrite?";
    protected boolean manageErrors = true;
    protected final TargetDelegate targetDelegate;

    public MaskController() {
        targetDelegate = new TargetDelegate();
        maskInfo = new MaskInfo(new File(ChartUtils.DOT).getAbsolutePath(), null, -1, null);
        maskId = new AtomicInteger(0);
        parentComponent = null;
    }

    /**
     * Returns the directory path used for {@link JFileChooser}s
     * 
     * @return a {@link String}
     */
    public String getMaskDirectory() {
        MaskInfo maskInfo = this.maskInfo;
        return maskInfo == null ? null : maskInfo.getMaskDirectory();
    }

    /**
     * Sets the directory path used for {@link JFileChooser}s
     * 
     * @param maskDirectory
     *            the path to use
     */
    public void setMaskDirectory(String maskDirectory) {
        MaskInfo maskInfo = this.maskInfo;
        if ((maskInfo == null) || (maskInfo.getMaskDirectory() == null)
                || (!ObjectUtils.sameObject(maskDirectory, maskInfo.getMaskDirectory()))) {
            this.maskInfo = new MaskInfo(maskDirectory, null, -1, null);
        }
    }

    /**
     * Saves a {@link MaskHandler}'s {@link Mask} in a {@link File}, getting it
     * from a {@link JFileChooser}
     * 
     * @param handler The {@link MaskHandler} from which to save the {@link Mask}
     */
    public void saveMask(MaskHandler handler) {
        saveMask(handler, null);
    }

    /**
     * Saves a {@link MaskHandler}'s {@link Mask} in a {@link File}, getting it
     * from a {@link JFileChooser}
     * 
     * @param handler The {@link MaskHandler} from which to save the {@link Mask}
     * @param fc The {@link JFileChooser} to use
     */
    public void saveMask(MaskHandler handler, JFileChooser fc) {
        if (handler != null) {
            Mask mask = handler.getMask();
            if (mask != null) {
                String maskDirectory = null;
                MaskInfo maskInfo = this.maskInfo;
                if (maskInfo != null) {
                    maskDirectory = maskInfo.getMaskDirectory();
                }
                if (maskDirectory == null) {
                    maskDirectory = new File(ChartUtils.DOT).getAbsolutePath();
                }
                String maskFile = launchSaveJFileChooser(fc, saveMaskTitle, maskDirectory, DEFAULT_MASK_FILE_NAME);
                if (maskFile != null) {
                    saveMask(mask, maskFile);
                }
            }
        }
    }

    protected String launchSaveJFileChooser(String title, String directory, String fileName) {
        return launchSaveJFileChooser(null, title, directory, fileName);
    }

    protected String launchSaveJFileChooser(JFileChooser fc, String title, String directory, String fileName) {
        String result = null;
        try {
            if (fc == null) {
                fc = new JFileChooser();
                addFileFilters(fc);
            }
            fc.setDialogTitle(title);
            File fdir = null;
            if (directory != null) {
                fdir = new File(directory);
            }
            if (fdir != null && fdir.isDirectory()) {
                fc.setCurrentDirectory(fdir);
            }
            if (fileName != null) {
                fc.setSelectedFile(new File(fileName));
            }
            if (fc.showSaveDialog(parentComponent) == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                FileFilter filter = fc.getFileFilter();
                if (filter instanceof ExtensionFileFilter) {
                    ExtensionFileFilter tmp = (ExtensionFileFilter) filter;
                    if (!tmp.getExtension().equalsIgnoreCase(FileUtils.getExtension(file))) {
                        file = new File(file.getAbsolutePath() + ChartUtils.DOT + tmp.getExtension());
                    }
                }
                if (file.exists()) {
                    Object[] options = { "Yes", "No" };
                    int ret = JOptionPane.showOptionDialog(fc, fileExistsMessage, warningTitle,
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
                    if (ret != JOptionPane.OK_OPTION) {
                        file = null;
                    }
                }
                if (file != null) {
                    result = file.getAbsolutePath();
                }
            }
        } catch (Exception ex) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to open save mask dialog", ex);
        }
        return result;
    }

    /**
     * Saves a {@link Mask} in a File
     * 
     * @param mask The {@link Mask} to save
     * @param fileName the path of the File
     */
    public void saveMask(Mask mask, String fileName) {
        if ((mask != null) && (fileName != null) && (!fileName.trim().isEmpty())) {
            String formerDirectory = getMaskDirectory();
            boolean warnMediators = false;
            boolean canWrite;
            boolean writeOK = false;
            try {
                File tempFile = new File(fileName);
                File parent = tempFile.getParentFile();
                if (parent.exists()) {
                    canWrite = parent.isDirectory();
                } else {
                    parent.mkdirs();
                    canWrite = true;
                }
                if (canWrite) {
                    String extension = FileUtils.getExtension(fileName);
                    if (SnapshotUtil.isLosslessImage(extension)) {
                        // Export mask to image file
                        File f = new File(fileName);
                        int width = 0;
                        int height = 0;
                        if (mask.getValue().length > 0) {
                            height = mask.getHeight();
                            width = mask.getWidth();
                        }
                        IndexColorModel maskModel = new IndexColorModel(1, 2, new byte[] { 0, (byte) 255 },
                                new byte[] { 0, (byte) 255 }, new byte[] { 0, (byte) 255 });
                        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_INDEXED,
                                maskModel);
                        for (int y = 0; y < height; y++) {
                            for (int x = 0; x < width; x++) {
                                if (mask.getValue()[y * width + x]) {
                                    image.setRGB(x, y, Color.BLACK.getRGB());
                                } else {
                                    image.setRGB(x, y, Color.WHITE.getRGB());
                                }
                            }
                        }
                        ImageIO.write(image, extension, f);
                        image.flush();
                    } else {
                        // Export mask to text file
                        StringBuilder maskBuffer = maskToStringBuilder(mask, null);
                        FileWriter commandFile = new FileWriter(fileName);
                        try {
                            commandFile.write(maskBuffer.toString(), 0, maskBuffer.length());
                            commandFile.flush();
                        } finally {
                            commandFile.close();
                        }
                    }
                    writeOK = true;
                    String parentPath = parent.getAbsolutePath();
                    MaskInfo maskInfo = new MaskInfo(parentPath, tempFile.getAbsolutePath(), tempFile.lastModified(),
                            mask);
                    mask.setName(maskInfo.getMaskFile());
                    mask.setDate(tempFile.lastModified());
                    this.maskInfo = maskInfo;
                    if (!ObjectUtils.sameObject(formerDirectory, parentPath)) {
                        warnMediators = true;
                    }
                }
            } catch (IOException e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(writeOK ? closeErrorMessage : writeErrorMessage,
                        e);
                if (isManageErrors()) {
                    if (writeOK) {
                        JOptionPane.showMessageDialog(parentComponent, closeErrorMessage, errorTitle,
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(parentComponent, writeErrorMessage, errorTitle,
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
            if (warnMediators) {
                warnDirectoryMediators();
            }
        }
    }

    /**
     * Appends a {@link String} representation of a {@link Mask} in a {@link StringBuilder}
     * 
     * @param mask The {@link Mask} to get the {@link String} representation of
     * @param buffer The {@link StringBuilder} in which to append the {@link String} . Can be <code>null</code>.
     *            In this case, a new {@link StringBuilder}, containing the expected {@link String}, is returned.
     * @return a {@link StringBuilder}. If <code>buffer</code> is <code>null</code>, the result is a new
     *         {@link StringBuilder} containing the expected {@link String}. Otherwise, <code>buffer</code> is returned,
     *         and has the expected {@link String} appended.
     */
    public StringBuilder maskToStringBuilder(Mask mask, StringBuilder buffer) {
        StringBuilder result = buffer;
        if (result == null) {
            result = new StringBuilder();
        }
        if ((mask != null) && (mask.getValue() != null)) {
            for (int y = 0; y < mask.getHeight(); y++) {
                for (int x = 0; x < mask.getWidth(); x++) {
                    result.append(mask.getValue()[y * mask.getWidth() + x] ? "1" : "0");
                    if (x < mask.getWidth() - 1) {
                        result.append(MASK_COLUMN_SEPARATOR);
                    }
                }
                if (y < mask.getHeight() - 1) {
                    result.append(MASK_LINE_SEPARATOR);
                }
            }
        }
        return result;
    }

    /**
     * Tries to load a {@link Mask} from a File
     * 
     * @param path The File path
     * @return a {@link Mask}. Can be <code>null</code>
     */
    public Mask loadMaskFile(String path) {
        Mask result = null;
        MaskInfo maskInfo = this.maskInfo;
        if ((maskInfo != null) && (maskInfo.getMaskFile() != null) && maskInfo.getMaskFile().equals(path)) {
            try {
                File tmp = new File(maskInfo.getMaskFile());
                if (tmp.exists() && (tmp.lastModified() == maskInfo.getMaskTime())) {
                    result = maskInfo.getMask();
                }
            } catch (Exception e) {
                result = null;
            }
        }
        if (result == null) {
            if (path != null) {
                boolean readOK = false;
                try {
                    if (SnapshotUtil.isLosslessImage(FileUtils.getExtension(path))) {
                        BufferedImage image = ImageIO.read(new File(path));
                        int height = image.getHeight();
                        int width = image.getWidth();
                        boolean[] matrix = new boolean[height * width];
                        for (int y = 0; y < height; y++) {
                            for (int x = 0; x < width; x++) {
                                matrix[y * width + x] = Color.BLACK.getRGB() == image.getRGB(x, y);
                            }
                        }
                        readOK = true;
                        image.flush();
                        try {
                            result = new Mask(matrix, width, height, path);
                        } catch (MaskException e) {
                            result = null;
                            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to load mask", e);
                            if (isManageErrors()) {
                                JOptionPane.showMessageDialog(parentComponent, e.getMessage(), errorTitle,
                                        JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    } else {
                        FileReader fileReader = new FileReader(path);
                        BufferedReader reader = null;
                        StringBuilder lineBuffer = new StringBuilder();
                        try {
                            reader = new BufferedReader(fileReader);
                            String line = reader.readLine();
                            while (line != null) {
                                line = line.trim();
                                if (!line.isEmpty()) {
                                    lineBuffer.append(line).append(ObjectUtils.NEW_LINE);
                                }
                                line = reader.readLine();
                            }
                            readOK = true;
                        } finally {
                            if (reader != null) {
                                reader.close();
                            }
                            fileReader.close();
                        }
                        result = loadMask(lineBuffer.toString(), path);
                    }
                    File maskFile = new File(path);
                    maskInfo = new MaskInfo(maskFile.getParent(), path, maskFile.lastModified(), result);
                    this.maskInfo = maskInfo;
                } catch (FileNotFoundException e) {
                    result = null;
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to load mask", e);
                    if (isManageErrors()) {
                        JOptionPane.showMessageDialog(parentComponent, fileNotFoundMessage, errorTitle,
                                JOptionPane.ERROR_MESSAGE);
                    }
                } catch (IOException e) {
                    result = null;
                    if (isManageErrors()) {
                        if (readOK) {
                            JOptionPane.showMessageDialog(parentComponent, closeErrorMessage, errorTitle,
                                    JOptionPane.ERROR_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(parentComponent, readErrorMessage, errorTitle,
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to load mask", e);
                }
            }
        }
        return result;
    }

    /**
     * Tries to interpret a {@link String} and return the corresponding {@link Mask}
     * 
     * @param maskText The {@link String} to interpret
     * @param maskName The name to set to the resulting {@link Mask}
     * @return a {@link Mask}. Can be <code>null</code>
     */
    public Mask loadMask(String maskText, String maskName) {
        Mask result = null;
        if (maskText != null) {
            try {
                ArrayList<String> lines = new ArrayList<String>();
                String[] lineArray = maskText.split(ObjectUtils.NEW_LINE);
                String line = ObjectUtils.EMPTY_STRING;
                if (lineArray != null) {
                    for (String element : lineArray) {
                        line = element.trim();
                        if (!line.isEmpty()) {
                            lines.add(line);
                        }
                    }
                }
                int dimY = lines.size();
                int dimX = 0;
                if (dimY > 0) {
                    line = lines.get(0);
                    dimX = line.split(";").length;
                }
                if (dimX > 0 && dimY > 0) {
                    boolean[] mask = new boolean[dimY * dimX];
                    for (int y = 0; y < dimY; y++) {
                        line = lines.get(y);
                        String[] values = line.split(";");
                        for (int x = 0; x < values.length; x++) {
                            try {
                                if ("0".equals(values[x].trim())) {
                                    mask[y * dimX + x] = false;
                                } else if ("1".equals(values[x].trim())) {
                                    mask[y * dimX + x] = true;
                                } else {
                                    throw new MaskException(invalidFileMessage);
                                }
                            } catch (ArrayIndexOutOfBoundsException e) {
                                throw new MaskException(invalidFileMessage, e);
                            }
                        }
                    }
                    result = new Mask(mask, dimX, dimY, maskName);
                } else {
                    result = null;
                }
            } catch (MaskException e) {
                result = null;
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to load mask", e);
                if (isManageErrors()) {
                    JOptionPane.showMessageDialog(parentComponent, e.getMessage(), errorTitle,
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        return result;
    }

    public void addFileFilters(JFileChooser fc) {
        if (fc != null) {
            fc.setAcceptAllFileFilterUsed(false);
            ExtensionFileFilter txtFilter = new ExtensionFileFilter(TXT_EXTENSION);
            fc.addChoosableFileFilter(txtFilter);
            fc.addChoosableFileFilter(new ExtensionFileFilter(CSV_EXTENSION));
            fc.addChoosableFileFilter(new ExtensionFileFilter(MASK_EXTENSION));
            for (String extension : SnapshotUtil.LOSSLESS_IMAGE_EXTENSION_LIST) {
                fc.addChoosableFileFilter(new ExtensionFileFilter(extension));
            }
            fc.setFileFilter(txtFilter);
        }
    }

    /**
     * Method called to display the restore mask browser
     */
    public String launchRestoreBrowser() {
        return launchRestoreBrowser(null);
    }

    /**
     * Method called to display the restore mask browser
     * 
     * @return The file chooser's selected file
     */
    public String launchRestoreBrowser(JFileChooser fc) {
        String result = null;
        boolean warnMediators = false;
        try {
            if (fc == null) {
                fc = new JFileChooser();
                addFileFilters(fc);
            }
            fc.setDialogTitle(restoreMaskTitle);
            File fdir = null;
            String maskDirectory = getMaskDirectory();
            if (maskDirectory != null) {
                fdir = new File(maskDirectory);
            }
            if (fdir != null) {
                fc.setCurrentDirectory(fdir);
            }
            MaskInfo maskInfo = this.maskInfo;
            if ((maskInfo != null) && (maskInfo.getMaskFile() != null)) {
                File temp = new File(maskInfo.getMaskFile());
                if (temp.getParentFile().equals(fdir)) {
                    fc.setSelectedFile(temp);
                }
                temp = null;
            }
            int returnVal = fc.showOpenDialog(parentComponent);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                if (file != null) {
                    String parentPath = fc.getCurrentDirectory().getPath();
                    if (!ObjectUtils.sameObject(maskDirectory, parentPath)) {
                        maskDirectory = fc.getCurrentDirectory().getPath();
                        warnMediators = true;
                    }
                    result = file.getAbsolutePath();
                }
            }
        } catch (Exception ex) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to launch mask restoration browser", ex);
            result = null;
        }
        if (warnMediators) {
            warnDirectoryMediators();
        }
        return result;
    }

    /**
     * Calls a {@link JFileChooser} to select and load a {@link Mask}, and
     * applies this {@link Mask} to a {@link MaskHandler}
     * 
     * @param handler The {@link MaskHandler}
     */
    public void loadMask(MaskHandler handler) {
        String path = launchRestoreBrowser();
        if (path != null) {
            Mask mask = loadMaskFile(path);
            if (mask != null) {
                handler.setMask(mask);
            }
        }
    }

    /**
     * Returns an int used to automatically name a new mask. Each time this
     * method is called, the id is incremented
     * 
     * @return an int
     */
    public int getNextMaskId() {
        return maskId.incrementAndGet();
    }

    /**
     * Returns the parent {@link Component} for popups
     * 
     * @return a {@link Component}
     */
    public Component getParentComponent() {
        return parentComponent;
    }

    /**
     * Sets the parent {@link Component} for popups
     * 
     * @param parentComponent The {@link Component} to use
     */
    public void setParentComponent(Component parentComponent) {
        this.parentComponent = parentComponent;
    }

    /**
     * Returns the {@link String} used as title of Warning messages
     * 
     * @return a {@link String}
     */
    public String getWarningTitle() {
        return warningTitle;
    }

    /**
     * Sets the {@link String} used as title of Warning messages
     * 
     * @param warningTitle The {@link String} to use
     */
    public void setWarningTitle(String warningTitle) {
        this.warningTitle = warningTitle;
    }

    /**
     * Returns the {@link String} used as title of Error messages
     * 
     * @return a {@link String}
     */
    public String getErrorTitle() {
        return errorTitle;
    }

    /**
     * Sets the {@link String} used as title of Error messages
     * 
     * @param errorTitle
     *            the {@link String} to use
     */
    public void setErrorTitle(String errorTitle) {
        this.errorTitle = errorTitle;
    }

    /**
     * Returns the message displayed in case of File writing error
     * 
     * @return a {@link String}
     */
    public String getWriteErrorMessage() {
        return writeErrorMessage;
    }

    /**
     * Sets the message displayed in case of File writing error, represented as
     * a {@link String}
     * 
     * @param writeErrorMessage
     *            the {@link String} to use
     */
    public void setWriteErrorMessage(String writeErrorMessage) {
        this.writeErrorMessage = writeErrorMessage;
    }

    /**
     * Returns the message displayed in case of File reading error
     * 
     * @return a {@link String}
     */
    public String getReadErrorMessage() {
        return readErrorMessage;
    }

    /**
     * Sets the message displayed in case of File reading error, represented as
     * a {@link String}
     * 
     * @param readErrorMessage
     *            the {@link String} to use
     */
    public void setReadErrorMessage(String readErrorMessage) {
        this.readErrorMessage = readErrorMessage;
    }

    /**
     * Returns the message displayed in case of File closing error
     * 
     * @return a {@link String}
     */
    public String getCloseErrorMessage() {
        return closeErrorMessage;
    }

    /**
     * Sets the message displayed in case of File closing error, represented as
     * a {@link String}
     * 
     * @param closeErrorMessage
     *            the {@link String} to use
     */
    public void setCloseErrorMessage(String closeErrorMessage) {
        this.closeErrorMessage = closeErrorMessage;
    }

    /**
     * Returns the message displayed in case of File not found error
     * 
     * @return a {@link String}
     */
    public String getFileNotFoundMessage() {
        return fileNotFoundMessage;
    }

    /**
     * Sets the message displayed in case of File not found error, represented
     * as a {@link String}
     * 
     * @param fileNotFoundMessage
     *            the {@link String} to use
     */
    public void setFileNotFoundMessage(String fileNotFoundMessage) {
        this.fileNotFoundMessage = fileNotFoundMessage;
    }

    /**
     * Returns the message displayed in case of an invalid Mask File
     * 
     * @return a {@link String}
     */
    public String getInvalidFileMessage() {
        return invalidFileMessage;
    }

    /**
     * Sets the message displayed in case of an invalid Mask File, represented
     * as a {@link String}
     * 
     * @param invalidFileMessage
     *            the {@link String} to use
     */
    public void setInvalidFileMessage(String invalidFileMessage) {
        this.invalidFileMessage = invalidFileMessage;
    }

    /**
     * Returns the title used in the "save mask" dialog
     * 
     * @return a {@link String}
     */
    public String getSaveMaskTitle() {
        return saveMaskTitle;
    }

    /**
     * Sets the title used in the "save mask" dialog, represented as a String
     * 
     * @param saveMaskTitle
     *            the {@link String} to use
     */
    public void setSaveMaskTitle(String saveMaskTitle) {
        this.saveMaskTitle = saveMaskTitle;
    }

    /**
     * Returns the title used in the "restore mask" dialog
     * 
     * @return a {@link String}
     */
    public String getRestoreMaskTitle() {
        return restoreMaskTitle;
    }

    /**
     * Sets the title used in the "restore mask" dialog, represented as a String
     * 
     * @param restoreMaskTitle
     *            the {@link String} to use
     */
    public void setRestoreMaskTitle(String restoreMaskTitle) {
        this.restoreMaskTitle = restoreMaskTitle;
    }

    /**
     * Returns the message displayed when you want to save in a File that
     * already exists
     * 
     * @return a {@link String}
     */
    public String getFileExistsMessage() {
        return fileExistsMessage;
    }

    /**
     * Sets the message displayed when you want to save in a File that already
     * exists, represented as a {@link String}
     * 
     * @param fileExistsMessage
     *            the {@link String} to use
     */
    public void setFileExistsMessage(String fileExistsMessage) {
        this.fileExistsMessage = fileExistsMessage;
    }

    /**
     * Returns whether this MaskController should display a popup in case of
     * Error, or just print error stack trace.
     * 
     * @return <code>true</code> if popup should be displayed, <code>false</code> otherwise
     */
    public boolean isManageErrors() {
        return manageErrors;
    }

    /**
     * Sets whether this MaskController should display a popup in case of Error,
     * or just print error stack trace.
     * 
     * @param manageErrors
     *            <code>true</code> if popup should be displayed, <code>false</code> otherwise
     */
    public void setManageErrors(boolean manageErrors) {
        this.manageErrors = manageErrors;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        targetDelegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        targetDelegate.removeMediator(mediator);
    }

    @Override
    public void setFile(File file) {
        setMaskDirectory((file == null || (!file.isDirectory())) ? null : file.getAbsolutePath());
    }

    protected void warnDirectoryMediators() {
        String maskDirectory = getMaskDirectory();
        targetDelegate
                .warnMediators(new FileInformation(this, (maskDirectory == null ? null : new File(maskDirectory))));
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * Class used to store information about a mask read from a file, in order not to reload it when already loaded.
     * 
     * @author pilif
     */
    protected class MaskInfo {

        protected final String maskDirectory;
        protected final String maskFile;
        protected final long maskTime;
        protected final Mask mask;

        public MaskInfo(String maskDirectory, String maskFile, long maskTime, Mask mask) {
            this.maskDirectory = maskDirectory;
            this.maskFile = maskFile;
            this.maskTime = maskTime;
            this.mask = mask;
        }

        public String getMaskDirectory() {
            return maskDirectory;
        }

        protected String getMaskFile() {
            return maskFile;
        }

        protected long getMaskTime() {
            return maskTime;
        }

        protected Mask getMask() {
            return mask;
        }

    }

}
