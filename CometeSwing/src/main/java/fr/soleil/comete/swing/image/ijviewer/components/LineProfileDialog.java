/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.awt.Window;

import javax.swing.JDialog;

import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.ImageViewer;

/**
 * A {@link JDialog} used to display line profile
 * 
 * @author MAINGUY
 */
public class LineProfileDialog extends JDialog {

    private static final long serialVersionUID = -4210106376904280981L;

    protected Chart chart;

    public LineProfileDialog(Window owner, Chart chart) {
        super(owner);
        setTitle(ImageViewer.LINE_PROFILE_NAME);
        this.chart = chart;
        setContentPane(chart);
        pack();
        setSize(400, 200);
    }

    public Chart getChart() {
        return chart;
    }

}
