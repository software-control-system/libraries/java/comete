/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util.mask;

import java.awt.Color;
import java.awt.Component;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.swing.util.RowHeaderedTable;

public class MaskedTable extends RowHeaderedTable {

    private static final long serialVersionUID = 116105769645121085L;

    public final static Color DEFAULT_MASK_COLOR = Color.GRAY;
    public final static Color DEFAULT_MASK_SELECTION_COLOR = Color.LIGHT_GRAY;
    protected Color maskColor;
    protected Color maskSelectionColor;
    protected final MaskedTableRenderer maskRenderer;
    protected Mask mask;

    public MaskedTable() {
        super();
        maskRenderer = new MaskedTableRenderer();
        mask = null;
    }

    public MaskedTable(TableModel dm) {
        super(dm);
        maskRenderer = new MaskedTableRenderer();
        mask = null;
    }

    public MaskedTable(TableModel dm, TableColumnModel cm) {
        super(dm, cm);
        maskRenderer = new MaskedTableRenderer();
        mask = null;
    }

    public MaskedTable(int numRows, int numColumns) {
        super(numRows, numColumns);
        maskRenderer = new MaskedTableRenderer();
        mask = null;
    }

    public MaskedTable(Vector<?> rowData, Vector<?> columnNames) {
        super(rowData, columnNames);
        maskRenderer = new MaskedTableRenderer();
        mask = null;
    }

    public MaskedTable(Object[][] rowData, Object[] columnNames) {
        super(rowData, columnNames);
        maskRenderer = new MaskedTableRenderer();
        mask = null;
    }

    public MaskedTable(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
        super(dm, cm, sm);
        maskRenderer = new MaskedTableRenderer();
        mask = null;
    }

    public Mask getMask() {
        return mask;
    }

    public void setMask(Mask mask) {
        this.mask = mask;
        repaint();
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component comp = super.prepareRenderer(renderer, row, column);
        Component maskedComp = super.prepareRenderer(maskRenderer, row, column);
        if (comp instanceof JComponent) {
            ((JComponent) comp).setOpaque(true);
        }
        comp.setBackground(maskedComp.getBackground());
        return comp;
    }

    public Color getMaskColor() {
        if (maskColor == null) {
            maskColor = DEFAULT_MASK_COLOR;
        }
        return maskColor;
    }

    public void setMaskColor(Color maskColor) {
        this.maskColor = maskColor;
    }

    public Color getMaskSelectionColor() {
        if (maskSelectionColor == null) {
            maskSelectionColor = DEFAULT_MASK_SELECTION_COLOR;
        }
        return maskSelectionColor;
    }

    public void setMaskSelectionColor(Color maskSelectionColor) {
        this.maskSelectionColor = maskSelectionColor;
    }

    public boolean isHiddenByMask(int row, int column) {
        boolean maskedZone = false;
        Mask toCheck = mask;
        if (toCheck != null) {
            boolean[] maskedValue = toCheck.getValue();
            if ((row > -1) && (row < toCheck.getHeight())) {
                if ((column > -1) && (column < toCheck.getWidth())) {
                    maskedZone = (!maskedValue[row * toCheck.getWidth() + column]);
                }
            }
        }
        return maskedZone;
    }

}
