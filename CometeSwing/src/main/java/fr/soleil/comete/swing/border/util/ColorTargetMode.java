package fr.soleil.comete.swing.border.util;

import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.border.CometeTitledBorder;

/**
 * An {@link Enum} that tells how a {@link CometeTitledBorder} should manage received foreground and background.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public enum ColorTargetMode {
    /**
     * {@link CometeTitledBorder#setCometeForeground(CometeColor)} and
     * {@link CometeTitledBorder#setCometeBackground(CometeColor)} will do
     * nothing.
     */
    NO_COLOR_TARGET,
    /**
     * {@link CometeTitledBorder#setCometeForeground(CometeColor)} will change {@link CometeTitledBorder}'s label
     * foreground, and {@link CometeTitledBorder#setCometeBackground(CometeColor)} will change
     * {@link CometeTitledBorder}'s label background.
     */
    COMETE_FOREGROUND_AS_LABEL_FOREGROUND_AND_COMETE_BACKGROUND_AS_LABEL_BACKGROUND,
    /**
     * {@link CometeTitledBorder#setCometeForeground(CometeColor)} will change {@link CometeTitledBorder}'s label
     * background, and {@link CometeTitledBorder#setCometeBackground(CometeColor)} will change
     * {@link CometeTitledBorder}'s label foreground.
     */
    COMETE_FOREGROUND_AS_LABEL_BACKGROUND_AND_COMETE_BACKGROUND_AS_LABEL_FOREGROUND,
    /**
     * {@link CometeTitledBorder#setCometeForeground(CometeColor)} will change {@link CometeTitledBorder}'s label
     * foreground, and {@link CometeTitledBorder#setCometeBackground(CometeColor)} will do nothing.
     */
    COMETE_FOREGROUND_AS_LABEL_FOREGROUND,
    /**
     * {@link CometeTitledBorder#setCometeForeground(CometeColor)} will change {@link CometeTitledBorder}'s label
     * background, and {@link CometeTitledBorder#setCometeBackground(CometeColor)} will do nothing.
     */
    COMETE_FOREGROUND_AS_LABEL_BACKGROUND,
    /**
     * {@link CometeTitledBorder#setCometeForeground(CometeColor)} do nothing, and
     * {@link CometeTitledBorder#setCometeBackground(CometeColor)} will change {@link CometeTitledBorder}'s label
     * background.
     */
    COMETE_BACKGROUND_AS_LABEL_BACKGROUND,
    /**
     * {@link CometeTitledBorder#setCometeForeground(CometeColor)} do nothing, and
     * {@link CometeTitledBorder#setCometeBackground(CometeColor)} will change {@link CometeTitledBorder}'s label
     * foreground.
     */
    COMETE_BACKGROUND_AS_LABEL_FOREGROUND
}
