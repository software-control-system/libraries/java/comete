/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.periodictable;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.metal.MetalButtonUI;

import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.ColorUtils;

public class ElementButton extends JButton implements MouseListener {

    private static final long serialVersionUID = -6786031056334905436L;

    private ElementModel element;
    private JLabel elementNameLabel;
    private JLabel symboleLabel;
    private JLabel atomicNumberLabel;
    private JLabel energyTypeLabel;

    private final int selectedEnergyType = 0;
    private final int selectedEnergySubType = 0;
    private boolean innerBorder;

    public ElementButton() {
        super();
    }

    public ElementButton(ElementModel model) {
        super();
        element = model;
        initialize();
        updateFonts();
        setMargin(CometeUtils.getzInset());
    }

    @Override
    public void updateUI() {
        // Force metal ui (for Coox)
        setUI(new MetalButtonUI());
    }

    protected GridBagConstraints generateConstraints(int y) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = y;
        constraints.weightx = 1;
        constraints.weighty = 0;
        constraints.insets = CometeUtils.getzInset();
        return constraints;
    }

    private void initialize() {
        addMouseListener(this);
        setLayout(new GridBagLayout());
        int y = 0;
        add(getAtomicNumberLabel(), generateConstraints(y++));
        add(getSymboleLabel(), generateConstraints(y++));
        add(getElementNameLabel(), generateConstraints(y++));
        add(getEnergyTypeLabel(), generateConstraints(y++));
        setBorder(new LineBorder(Color.WHITE));
    }

    public void refresh() {
        getAtomicNumberLabel();
        getSymboleLabel();
        getElementNameLabel();
        getEnergyTypeLabel();
    }

    /**
     * 
     * @return atomicNumberLabel
     */
    public JLabel getAtomicNumberLabel() {
        if (atomicNumberLabel == null) {
            atomicNumberLabel = new JLabel(" " + element.getAtomicNumber());
            atomicNumberLabel.setForeground(Color.RED);
        } else {
            atomicNumberLabel.setText(ObjectUtils.EMPTY_STRING + element.getAtomicNumber());
        }
        return atomicNumberLabel;
    }

    /**
     * 
     * @return JLabel
     */
    public JLabel getSymboleLabel() {
        if (symboleLabel == null) {
            symboleLabel = new JLabel(element.getSymbole());
            symboleLabel.setHorizontalAlignment(SwingConstants.CENTER);
        } else {
            symboleLabel.setText(element.getSymbole());
        }
        return symboleLabel;
    }

    protected int computeBestFontAdd() {
        int size = getFont().getSize();
        int fact = size / 10;
        if (fact < 1) {
            fact = 1;
        }
        return 3 * fact;
    }

    protected Font deriveFont(float add) {
        Font font = getFont();
        return font.deriveFont(font.getSize() + add);
    }

    /**
     * 
     * @return JLabel
     */
    public JLabel getElementNameLabel() {
        if (elementNameLabel == null) {
            elementNameLabel = new JLabel(element.getElementName());
            elementNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
        } else {
            elementNameLabel.setText(element.getElementName());
        }
        return elementNameLabel;
    }

    /**
     * 
     * @return JLabel
     */
    public JLabel getEnergyTypeLabel() {
        if (energyTypeLabel == null) {
            energyTypeLabel = new JLabel();

            if (element.getEnergyElements().size() > 0) {
                EnergyElement elemEnergy = element.getEnergyElements().get(selectedEnergyType);
                energyTypeLabel.setText(
                        elemEnergy.getEnergyType() + "-" + elemEnergy.getEnergyNames().get(selectedEnergySubType));
                // this.setEnabled(true);
            }
            // else{
            // this.setEnabled(false);
            // }
            energyTypeLabel.setHorizontalAlignment(SwingConstants.CENTER);
            energyTypeLabel.setForeground(Color.BLUE);
        } else {
            if (element.getEnergyElements().size() > 0) {
                EnergyElement elemEnergy = element.getEnergyElements().get(selectedEnergyType);
                energyTypeLabel.setText(
                        elemEnergy.getEnergyType() + "-" + elemEnergy.getEnergyNames().get(selectedEnergySubType));
            } else {
                energyTypeLabel.setText(ObjectUtils.EMPTY_STRING);
            }
        }
        return energyTypeLabel;
    }

    public ElementModel getElement() {
        return element;
    }

    public void setElement(ElementModel element) {
        this.element = element;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (innerBorder && isEnabled()) {
            Color color = getBackground();
            if (Math.round(ColorUtils.getRealGray(color)) < 128) {
                color = ColorUtils.blend(color, Color.WHITE);
            } else {
                color = ColorUtils.blend(color, Color.BLACK);
            }
            Insets insets;
            Border border = getBorder();
            if (border == null) {
                insets = CometeUtils.getzInset();
            } else {
                insets = border.getBorderInsets(this);
            }
            Color previous = g.getColor();
            g.setColor(color);
            g.drawRect(insets.left, insets.top, getWidth() - (insets.left + insets.right + 2),
                    getHeight() - (insets.top + insets.bottom + 1));
            g.setColor(previous);
        }
    }

    @Override
    public String toString() {
        return element.toString();
    }

    protected void updateFonts() {
        int add = computeBestFontAdd();
        if (elementNameLabel != null) {
            elementNameLabel.setFont(deriveFont(-add));
        }
        if (symboleLabel != null) {
            symboleLabel.setFont(deriveFont(add));
        }
        if (atomicNumberLabel != null) {
            atomicNumberLabel.setFont(getFont());
        }
        if (energyTypeLabel != null) {
            energyTypeLabel.setFont(getFont());
        }
        revalidate();
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        updateFonts();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // not managed
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // not managed
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // not managed
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        innerBorder = true;
        repaint();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        innerBorder = false;
        repaint();
    }
}
