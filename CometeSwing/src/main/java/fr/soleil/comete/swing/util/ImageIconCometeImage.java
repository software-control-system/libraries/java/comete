/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import javax.swing.ImageIcon;

import fr.soleil.comete.definition.widget.util.CometeImage;

/**
 * A {@link CometeImage} that stores an {@link ImageIcon}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ImageIconCometeImage extends CometeImage {

    protected ImageIcon icon;

    /**
     * Creates a new {@link ImageIconCometeImage} with a given {@link ImageIcon}
     * 
     * @param icon The {@link ImageIcon}
     */
    public ImageIconCometeImage(ImageIcon icon) {
        super((icon == null) || (icon.getDescription() == null) ? null : icon.getDescription());
        this.icon = icon;
    }

    /**
     * Returns this {@link ImageIconCometeImage}'s {@link ImageIcon}
     * 
     * @return An {@link ImageIcon}
     */
    public ImageIcon getIcon() {
        return icon;
    }

}
