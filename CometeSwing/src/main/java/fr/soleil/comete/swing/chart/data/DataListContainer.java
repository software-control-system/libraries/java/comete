/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.data;

/**
 * Basic implementation of {@link IDataListContainer}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DataListContainer implements IDataListContainer {

    private static final long serialVersionUID = -7131827805278731999L;

    private DataList dataList;

    public DataListContainer(DataList dataList) {
        this.dataList = dataList;
    }

    @Override
    public boolean isValid() {
        return (dataList != null);
    }

    @Override
    public void init() {
        // nothing to do
    }

    @Override
    public void next() {
        dataList = dataList.next;
    }

    @Override
    public DataList getDataY() {
        return dataList;
    }

    @Override
    protected void finalize() {
        dataList = null;
    }

}
