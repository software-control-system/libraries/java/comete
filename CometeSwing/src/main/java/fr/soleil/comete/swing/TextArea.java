/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.listener.ITextAreaListener;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.definition.widget.ITextArea;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.ICancelableTarget;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;

public class TextArea extends JPanel
        implements ITextArea, ICancelableTarget, MouseListener, ActionListener, KeyListener, DocumentListener {

    private static final long serialVersionUID = 6001897798946766920L;

    protected static final String DEFAULT_DATA_SENDING_TEXT = "Set data";

    protected final JPopupMenu sendPopupMenu;
    protected final JMenuItem sendMenu;
    protected final JScrollPane scrollPane;
    protected final JTextArea textArea;
    private final List<ITextAreaListener> textAreaListeners;
    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;

    private int horizontalAlignment;
    protected String lastText;
    protected volatile boolean isEditingData;

    protected boolean popupMenuVisible;

    public TextArea() {
        super(new BorderLayout());

        horizontalAlignment = IComponent.CENTER;
        lastText = null;
        isEditingData = false;
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);

        textArea = new JTextArea();
        textArea.setLineWrap(true);
        textArea.setColumns(20);
        textArea.setRows(5);
        textArea.addMouseListener(this);
        textArea.addKeyListener(this);
        textArea.getDocument().addDocumentListener(this);

        sendMenu = new JMenuItem(DEFAULT_DATA_SENDING_TEXT);
        sendMenu.addActionListener(this);
        sendPopupMenu = new JPopupMenu();
        sendPopupMenu.setDoubleBuffered(true);
        sendPopupMenu.add(sendMenu);

        popupMenuVisible = true;

        scrollPane = new JScrollPane(textArea);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        textAreaListeners = new ArrayList<ITextAreaListener>();

        add(scrollPane, BorderLayout.CENTER);
    }

    public void setColumnHeader(Component header) {
        scrollPane.setColumnHeaderView(header);
    }

    public void setRowHeader(Component header) {
        scrollPane.setRowHeaderView(header);
    }

    public void setVerticalScrollBarPolicy(int verticalBarPolicy) {
        scrollPane.setVerticalScrollBarPolicy(verticalBarPolicy);
    }

    public void setHorizontalScrollBarPolicy(int horizontalBarPolicy) {
        scrollPane.setHorizontalScrollBarPolicy(horizontalBarPolicy);
    }

    public boolean isLineWrap() {
        return textArea.getLineWrap();
    }

    public void setLineWripe(boolean wrap) {
        textArea.setLineWrap(wrap);
    }

    /**
     * Returns the text displayed in popup to send data
     * 
     * @return A {@link String}
     */
    public String getDataSendingText() {
        return sendMenu.getText();
    }

    /**
     * Sets the text displayed in popup to send data
     * 
     * @param text The text to set
     */
    public void setDataSendingText(String text) {
        if ((text == null) || text.trim().isEmpty()) {
            sendMenu.setText(DEFAULT_DATA_SENDING_TEXT);
        } else {
            sendMenu.setText(text);
        }
    }

    /**
     * Returns the position of the text insertion caret for the text area
     * 
     * @return the position of the text insertion caret for the text area <code>>= 0</code>
     */
    public int getCaretPosition() {
        return textArea.getCaretPosition();
    }

    /**
     * Sets the position of the text insertion caret for the TextArea. Note that the caret tracks
     * change, so this may move if the underlying text of the component is changed. The position
     * must be between 0 and the length of the component's text or else an exception is thrown
     * 
     * @param position The position
     * 
     * @throws IllegalArgumentException If the value supplied for <code>position</code> is less than
     *             zero or greater than the component's text length
     */
    public void setCaretPosition(int position) throws IllegalArgumentException {
        textArea.setCaretPosition(position);
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(textArea.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(textArea.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(textArea.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        textArea.setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(CometeFont font) {
        textArea.setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        textArea.setForeground(ColorTool.getColor(color));
    }

    @Override
    public int getHorizontalAlignment() {
        return horizontalAlignment;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        horizontalAlignment = halign;
        switch (horizontalAlignment) {
            case IComponent.CENTER:
                textArea.setAlignmentX(JTextArea.CENTER_ALIGNMENT);
                break;
            case IComponent.LEFT:
                textArea.setAlignmentX(JTextArea.LEFT_ALIGNMENT);
                break;
            case IComponent.RIGHT:
                textArea.setAlignmentX(JTextArea.RIGHT_ALIGNMENT);
                break;
        }
    }

    @Override
    public void addTextAreaListener(ITextAreaListener listener) {
        if (!textAreaListeners.contains(listener)) {
            textAreaListeners.add(listener);
        }
    }

    @Override
    public void fireActionPerformed(EventObject event) {
        ListIterator<ITextAreaListener> iterator = textAreaListeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().actionPerformed(event);
        }
    }

    @Override
    public void fireTextChanged(EventObject event) {
        ListIterator<ITextAreaListener> iterator = textAreaListeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().textChanged(event);
        }
    }

    @Override
    public void removeTextAreaListener(ITextAreaListener listener) {
        if (textAreaListeners.contains(listener)) {
            synchronized (textAreaListeners) {
                textAreaListeners.remove(listener);
            }
        }
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        super.setOpaque(isOpaque);
        if (textArea != null) {
            textArea.setOpaque(isOpaque);
        }
    }

    @Override
    public String getToolTipText() {
        return textArea.getToolTipText();
    }

    @Override
    public void setToolTipText(String text) {
        textArea.setToolTipText(text);
    }

    @Override
    public String getText() {
        return textArea.getText();
    }

    @Override
    public boolean isEnabled() {
        return textArea.isEnabled();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(false);
        if (textArea != null) {
            textArea.setEnabled(enabled);
        }
        if (scrollPane != null) {
            scrollPane.setEnabled(enabled);
            scrollPane.getHorizontalScrollBar().setEnabled(enabled);
            scrollPane.getVerticalScrollBar().setEnabled(enabled);
            scrollPane.getViewport().setEnabled(enabled);
        }
        if (sendPopupMenu != null) {
            sendPopupMenu.setEnabled(enabled);
        }
        if (!enabled) {
            isEditingData = false;
        }
    }

    @Override
    public boolean isEditable() {
        return textArea.isEditable();
    }

    @Override
    public void setEditable(boolean editable) {
        textArea.setEditable(editable);
        if (!editable) {
            isEditingData = false;
        }
    }

    /**
     * Sets this {@link TextArea}'s text if possible, or else stores the value to set it later
     * 
     * @param text The text to set
     */
    @Override
    public void setText(String text) {
        lastText = text;
        if (!isEditingData) {
            doSetText(text);
        }
    }

    /**
     * Does set this {@link TextArea}'s text
     * 
     * @param text The text to set
     */
    protected void doSetText(String text) {
        this.isEditingData = false;
        if (text == null) {
            text = ObjectUtils.EMPTY_STRING;
        }
        String oldText = getText();
        if (!text.equals(oldText)) {
            textArea.getDocument().removeDocumentListener(this);
            textArea.setText(text);
            textArea.getDocument().addDocumentListener(this);
            applyChange(null);
        }
    }

    @Override
    public boolean hasFocus() {
        return (textArea.hasFocus() || sendPopupMenu.isVisible());
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getClickCount() == 1 && e.getButton() == MouseEvent.BUTTON3 && sendPopupMenu.isEnabled()
                && popupMenuVisible) {
            sendPopupMenu.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event != null) {
            Object source = event.getSource();
            if ((source != null) && (source == sendMenu)) {
                send();
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        this.isEditingData = true;
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            cancel();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // nothing to do
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // nothing to do
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        applyChange(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        applyChange(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        applyChange(e);
    }

    protected void applyChange(DocumentEvent e) {
        fireTextChanged(new EventObject(this));
    }

    /**
     * Force the scrollPane to recompute preferred size
     * 
     * @param setMinimumSize Whether to update text area minimum size
     */
    protected void updateScrollPane(boolean setMinimumSize) {
        if (setMinimumSize) {
            textArea.setMinimumSize(textArea.getPreferredSize());
        }
        textArea.revalidate();
        scrollPane.revalidate();
        revalidate();
        repaint();
    }

    @Override
    public void setColumns(int nbColumns) {
        textArea.setColumns(nbColumns);
    }

    @Override
    public void setRows(int nbRows) {
        textArea.setRows(nbRows);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return false/*(isEditable() && isEnabled() && hasFocus())*/;
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void cancel() {
        doSetText(lastText);
    }

    @Override
    public void send() {
        String text = getText();
        this.isEditingData = false;
        warnMediators(new TextInformation(this, text));
        fireActionPerformed(new EventObject(this));
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

    public synchronized void setPopupMenuVisible(boolean visible) {
        popupMenuVisible = visible;
        sendPopupMenu.setVisible(false);
    }

    public boolean isPopupMenuVisible() {
        return popupMenuVisible;
    }

    public Document getDocument() {
        return textArea.getDocument();
    }

    /**
     * Main class, so you can have an example.
     */
    public static void main(String[] args) {
        TextArea widget = new TextArea();
        widget.setText("truc de test");

        IPanel panel = new Panel();
        panel.add(widget, BorderLayout.CENTER);

        IFrame frame = new Frame();
        frame.setTitle("TextArea test");
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        ((JFrame) frame).setSize(300, 300);
        frame.setVisible(true);
        ((JFrame) frame).setLocationRelativeTo(null);

    }

}
