/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.WeakHashMap;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.ListCellRenderer;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.DragProperties;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.util.CometeUtils;

/**
 * A class to build a drag setting panel.
 */
public class DragPanel extends JPanel implements ActionListener, ChangeListener {

    private static final long serialVersionUID = -2404037712217640080L;

    private static final Insets TITLE_INSETS = new Insets(5, 5, 0, 5);
    private static final Insets EDITOR_INSETS = new Insets(5, 0, 0, 5);

    protected static final String DISABLED = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.disabled");
    protected static final String CHANGE_X_OFFSET = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.dataview.x");
    protected static final String CHANGE_Y_OFFSET = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.dataview.y");
    protected static final String CHANGE_XY_OFFSETS = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.dataview.xy");
    protected static final String X_ONLY = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.scale.x");
    protected static final String Y_ONLY = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.scale.y");
    protected static final String XY = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dragging.scale.xy");
    protected static final String DATA_VIEWS_DRAGGING = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.dataview.text");
    protected static final String DATA_VIEWS_DRAG_HELP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.dataview.tooltip");
    protected static final String SCALES_DRAGGING = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.scale.text");
    protected static final String SCALES_DRAG_HELP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.scale.tooltip");
    protected static final String SCALES_DRAG_SENSITIVITY = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.scale.sensivity.text");
    protected static final String SCALES_DRAG_SENSITIVITY_HELP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dragging.scale.sensivity.tooltip");

    protected static final ImageIcon HELP_ICON = new ImageIcon(
            DragPanel.class.getResource("/com/famfamfam/silk/help.png"));

    protected DragProperties properties;

    protected final JLabel dataViewDragTypeLabel, dataViewDragHelpLabel;
    protected final JComboBox<Integer> dataViewDragTypeCombo;

    protected final JLabel scalesDragTypeLabel, scalesDragHelpLabel;
    protected final JComboBox<Integer> scalesDragTypeCombo;

    protected final JLabel scaleSensitivityLabel, scaleSensitivityHelpLabel;
    protected final JSpinner scaleSensitivitySpinner;

    protected Collection<DragPanelListener> listeners;

    /**
     * Construct a drag setting panel.
     */
    public DragPanel() {
        super(new GridBagLayout());
        listeners = Collections.newSetFromMap(new WeakHashMap<>());

        dataViewDragTypeLabel = generateTitleLabel(DATA_VIEWS_DRAGGING);
        dataViewDragHelpLabel = generateHelpLabel(DATA_VIEWS_DRAG_HELP);
        dataViewDragTypeCombo = generateDragTypeComboBox(CHANGE_X_OFFSET, CHANGE_Y_OFFSET, CHANGE_XY_OFFSETS, DISABLED);

        scalesDragTypeLabel = generateTitleLabel(SCALES_DRAGGING);
        scalesDragHelpLabel = generateHelpLabel(SCALES_DRAG_HELP);
        scalesDragTypeCombo = generateDragTypeComboBox(X_ONLY, Y_ONLY, XY, DISABLED);

        scaleSensitivityLabel = generateTitleLabel(SCALES_DRAG_SENSITIVITY);
        scaleSensitivityHelpLabel = generateHelpLabel(SCALES_DRAG_SENSITIVITY_HELP);
        Integer value = Integer.valueOf(IChartViewer.DEFAULT_SCALE_DRAG_SENTIVITY);
        Integer min = Integer.valueOf(0);
        Integer max = Integer.valueOf(Integer.MAX_VALUE);
        Integer step = Integer.valueOf(1);
        SpinnerNumberModel spModel = new SpinnerNumberModel(value, min, max, step);
        scaleSensitivitySpinner = new JSpinner(spModel);
        scaleSensitivitySpinner.addChangeListener(this);

        int y = 0;

        addComponents(dataViewDragTypeLabel, dataViewDragTypeCombo, dataViewDragHelpLabel, y++);
        addComponents(scalesDragTypeLabel, scalesDragTypeCombo, scalesDragHelpLabel, y++);
        addComponents(scaleSensitivityLabel, scaleSensitivitySpinner, scaleSensitivityHelpLabel, y++);

        initProperties();
    }

    protected void addComponents(JLabel title, JComponent editor, JLabel help, int y) {
        int x = 0;
        if (title == null) {
            x++;
        } else {
            GridBagConstraints titleConstraints = new GridBagConstraints();
            titleConstraints.fill = GridBagConstraints.BOTH;
            titleConstraints.gridx = x++;
            titleConstraints.gridy = y;
            titleConstraints.weightx = 0;
            titleConstraints.weighty = 0;
            titleConstraints.insets = TITLE_INSETS;
            add(title, titleConstraints);
        }
        if (editor == null) {
            x++;
        } else {
            GridBagConstraints editorConstraints = new GridBagConstraints();
            editorConstraints.fill = GridBagConstraints.BOTH;
            editorConstraints.gridx = x++;
            editorConstraints.gridy = y;
            editorConstraints.weightx = 1;
            editorConstraints.weighty = 0;
            editorConstraints.insets = EDITOR_INSETS;
            add(editor, editorConstraints);
        }
        if (help != null) {
            GridBagConstraints helpConstraints = new GridBagConstraints();
            helpConstraints.fill = GridBagConstraints.NONE;
            helpConstraints.gridx = x++;
            helpConstraints.gridy = y;
            helpConstraints.weightx = 0;
            helpConstraints.weighty = 0;
            helpConstraints.insets = EDITOR_INSETS;
            add(help, helpConstraints);
        }
    }

    protected void initProperties() {
        setProperties(null);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (dataViewDragTypeLabel != null) {
            dataViewDragTypeLabel.setEnabled(enabled);
        }
        if (dataViewDragTypeCombo != null) {
            dataViewDragTypeCombo.setEnabled(enabled);
        }
        if (scalesDragTypeLabel != null) {
            scalesDragTypeLabel.setEnabled(enabled);
        }
        if (scalesDragTypeCombo != null) {
            scalesDragTypeCombo.setEnabled(enabled);
        }
    }

    protected void applyProperties() {
        DragProperties tempProperties = getProperties();
        dataViewDragTypeCombo.setSelectedItem(Integer.valueOf(tempProperties.getDataViewOffsetDragType()));
        scalesDragTypeCombo.setSelectedItem(Integer.valueOf(tempProperties.getScaleDragType()));
        scaleSensitivitySpinner.setValue(Integer.valueOf(tempProperties.getScaleDragSensitivity()));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == dataViewDragTypeCombo) {
            Integer type = (Integer) dataViewDragTypeCombo.getSelectedItem();
            if (type != null) {
                properties.setDataViewOffsetDragType(type.intValue());
                fireDragChanged();
            }
        } else if (e.getSource() == scalesDragTypeCombo) {
            Integer type = (Integer) scalesDragTypeCombo.getSelectedItem();
            if (type != null) {
                properties.setScaleDragType(type.intValue());
                fireDragChanged();
            }
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() == scaleSensitivitySpinner) {
            Number sensitivity = (Number) scaleSensitivitySpinner.getValue();
            if (sensitivity != null) {
                int tmp = sensitivity.intValue();
                if (tmp != properties.getScaleDragSensitivity()) {
                    properties.setScaleDragSensitivity(sensitivity.intValue());
                    fireDragChanged();
                }
            }
        }
    }

    protected void fireDragChanged() {
        List<DragPanelListener> copy = new ArrayList<>();
        DragPanelEvent event = new DragPanelEvent(this);
        synchronized (listeners) {
            copy.addAll(listeners);
        }
        for (DragPanelListener listener : copy) {
            listener.dragPanelChanged(event);
        }
        copy.clear();
    }

    protected JComboBox<Integer> generateDragTypeComboBox(String dragX, String dragY, String dragXY, String dragNone) {
        JComboBox<Integer> dragTypeCombo = new JComboBox<>(
                new Integer[] { Integer.valueOf(IChartViewer.DRAG_NONE), Integer.valueOf(IChartViewer.DRAG_X),
                        Integer.valueOf(IChartViewer.DRAG_Y), Integer.valueOf(IChartViewer.DRAG_XY) });
        dragTypeCombo.setRenderer(new DragTypeComboRenderer(dragX, dragY, dragXY, dragNone));
        dragTypeCombo.addActionListener(this);
        return dragTypeCombo;
    }

    protected JLabel generateTitleLabel(String text) {
        JLabel label = new JLabel(text);
        label.setFont(CometeUtils.getLabelFont());
        label.setForeground(CometeUtils.getfColor());
        return label;
    }

    protected JLabel generateHelpLabel(String tooltip) {
        JLabel label = new JLabel(HELP_ICON);
        label.setToolTipText(tooltip);
        return label;
    }

    public void addDragPanelListener(DragPanelListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    public void removeDragPanelListener(DragPanelListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    public void removeAllDragPanelListeners() {
        synchronized (listeners) {
            listeners.clear();
        }
    }

    public DragProperties getProperties() {
        if (properties == null) {
            properties = new DragProperties();
        }
        return properties.clone();
    }

    public void setProperties(DragProperties dragProperties) {
        if (dragProperties == null) {
            properties = new DragProperties();
        } else {
            properties = dragProperties.clone();
        }
        applyProperties();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class DragTypeComboRenderer implements ListCellRenderer<Integer> {

        protected final ListCellRenderer<? super Integer> renderer;
        protected final String dragX, dragY, dragXY, dragNone;

        public DragTypeComboRenderer(ListCellRenderer<? super Integer> renderer, String dragX, String dragY,
                String dragXY, String dragNone) {
            super();
            this.renderer = renderer;
            this.dragX = dragX;
            this.dragY = dragY;
            this.dragXY = dragXY;
            this.dragNone = dragNone;
        }

        public DragTypeComboRenderer(String dragX, String dragY, String dragXY, String dragNone) {
            this(new DefaultListCellRenderer(), dragX, dragY, dragXY, dragNone);
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends Integer> list, Integer value, int index,
                boolean isSelected, boolean cellHasFocus) {
            Component comp = renderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if ((comp instanceof JLabel) && (value != null)) {
                JLabel label = (JLabel) comp;
                switch (value.intValue()) {
                    case IChartViewer.DRAG_X:
                        label.setText(dragX);
                        break;
                    case IChartViewer.DRAG_Y:
                        label.setText(dragY);
                        break;
                    case IChartViewer.DRAG_XY:
                        label.setText(dragXY);
                        break;
                    default:
                        label.setText(dragNone);
                        break;
                }

            }
            return comp;
        }

    }

}
