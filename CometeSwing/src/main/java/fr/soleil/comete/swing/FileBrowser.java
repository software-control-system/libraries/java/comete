/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;
import java.util.WeakHashMap;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.listener.IFileBrowserListener;
import fr.soleil.comete.definition.widget.IFileBrowser;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.data.target.information.FileInformation;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.file.FileUtils;

public class FileBrowser extends AbstractButton implements IFileBrowser, CometeConstants {

    private static final long serialVersionUID = -6864938065477278944L;

    private final Collection<IFileBrowserListener> fileBrowserlisteners;
    protected JFileChooser fileChooser;
    protected AdaptedExtensionFileFilter fileFilter;
    protected boolean displayHiddenFile;
    protected String extensionFile;
    protected volatile File selectedFile;
    protected String defaultDirectory;
    protected int browserType;
    protected boolean updateAllowed;
    protected volatile boolean isEditing;
    protected boolean allowNonExistingFiles;

    public FileBrowser() {
        super();
        setPreferredSize(null);
        super.setText(SUSPENSION_POINTS);
        fileBrowserlisteners = Collections.newSetFromMap(new WeakHashMap<IFileBrowserListener, Boolean>());
        displayHiddenFile = false;
        extensionFile = "*";
        selectedFile = null;
        defaultDirectory = ObjectUtils.EMPTY_STRING;
        browserType = IFileBrowser.FULLNAME;
        updateAllowed = false;
        isEditing = false;
        fileChooser = null;
        fileFilter = new AdaptedExtensionFileFilter();
        allowNonExistingFiles = false;
    }

    @Override
    public void addFileBrowserListener(IFileBrowserListener listener) {
        synchronized (fileBrowserlisteners) {
            fileBrowserlisteners.add(listener);
        }
    }

    @Override
    public void removeFileBrowserListener(IFileBrowserListener listener) {
        synchronized (fileBrowserlisteners) {
            fileBrowserlisteners.remove(listener);
        }
    }

    @Override
    public void fireSelectedFileChange(EventObject event) {
        // can be null is selected file is root path
        String path = getDirectory();
        if ((selectedFile != null) && (allowNonExistingFiles || selectedFile.exists())) {
            String fullName = selectedFile.getAbsolutePath();
            String fileName = selectedFile.getName();
            if (browserType == FILENAME) {
                warnMediators(new TextInformation(this, fileName));
            } else {
                warnMediators(new TextInformation(this, fullName));
            }
            warnMediators(new FileInformation(this, selectedFile));
            List<IFileBrowserListener> copy = new ArrayList<IFileBrowserListener>();
            synchronized (fileBrowserlisteners) {
                copy.addAll(fileBrowserlisteners);
            }
            for (IFileBrowserListener listener : copy) {
                if (listener != null) {
                    listener.selectedFullFileNameChange(fullName);
                    listener.selectedPathChange(path);
                    listener.selectedFileNameChange(fileName);
                }
            }
            copy.clear();
        }

    }

    @Override
    public int getBrowserType() {
        return browserType;
    }

    @Override
    public boolean isDisplayHiddenFile() {
        return displayHiddenFile;
    }

    @Override
    public void setDisplayHiddenFile(boolean displayHiddenFile) {
        this.displayHiddenFile = displayHiddenFile;
    }

    @Override
    public String getExtensionFile() {
        return extensionFile;
    }

    @Override
    public void setExtensionFile(String extensionFile) {
        if (!ObjectUtils.sameObject(extensionFile, this.extensionFile)) {
            this.extensionFile = extensionFile;
        }
    }

    @Override
    public File getFile() {
        return selectedFile;
    }

    @Override
    public String getDirectory() {
        if (selectedFile != null) {
            File parent = selectedFile.getParentFile();
            if (parent == null) {
                return null;
            } else {
                return parent.getAbsolutePath();
            }
        }
        return null;
    }

    @Override
    public String getDefaultDirectory() {
        return defaultDirectory;
    }

    @Override
    public void setDefaultDirectory(String defaultDirectory) {
        this.defaultDirectory = defaultDirectory;
    }

    @Override
    public void setDirectory(String directory) {
        if ((directory != null) && (!directory.trim().isEmpty())) {
            File tmpDir = new File(directory);
            if ((tmpDir != null) && (tmpDir.exists()) && (tmpDir.isDirectory())) {
                updateFileChooserWithFile(tmpDir);
            }
        }
    }

    @Override
    public String getTitle() {
        if (fileChooser != null) {
            return fileChooser.getDialogTitle();
        }
        return "File Browser";
    }

    @Override
    public void setTitle(String title) {
        if (fileChooser != null) {
            fileChooser.setDialogTitle(title);
        }
    }

    @Override
    public void setBrowserType(int browserType) {
        this.browserType = browserType;
    }

    @Override
    public void setFile(File file) {
        boolean isValidFile = true;
        if ((file != null) && (allowNonExistingFiles || file.exists())) {
            if (browserType == IFileBrowser.PATH) {
                isValidFile = file.isDirectory();
            } else if (browserType == IFileBrowser.ALL) {
                isValidFile = true;
            } else {
                isValidFile = file.isFile();
            }
        }
        if (isValidFile) {
            selectedFile = file;
            updateFileChooserWithFile(selectedFile);
        }
    }

    public boolean isAllowNonExistingFiles() {
        return allowNonExistingFiles;
    }

    public void setAllowNonExistingFiles(boolean allowNonExistingFiles) {
        this.allowNonExistingFiles = allowNonExistingFiles;
    }

    protected void updateFileChooserWithFile(File file) {
        if (fileChooser != null) {
            if ((file != null) && (allowNonExistingFiles || file.exists())) {
                fileChooser.setCurrentDirectory(file.getParentFile());
            }
            fileChooser.setSelectedFile(file);
        }
    }

    protected void initFileChooser() {
        fileChooser = new JFileChooser();
        switch (browserType) {
            case PATH:
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                break;
            case ALL:
                fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                break;
            default:
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                break;
        }
        updateFileChooserWithFile(selectedFile);
        fileChooser.setFileFilter(fileFilter);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        isEditing = true;
        if (fileChooser == null) {
            initFileChooser();
        }

        if (fileChooser.showOpenDialog(CometeUtils.getWindowForComponent(this)) == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooser.getSelectedFile();
            isEditing = false;
            fireSelectedFileChange(new EventObject(this));
        }
        isEditing = false;

    }

    @Override
    public boolean hasFocus() {
        if (fileChooser == null) {
            return super.hasFocus();
        }
        return fileChooser.isShowing() || super.hasFocus();
    }

    /**
     * @param updateAllowed
     *            the updateAllowed to set
     */
    public void setUpdateAllowed(boolean updateAllowed) {
        this.updateAllowed = updateAllowed;
    }

    /**
     * @return the updateAllowed
     */
    public boolean isUpdateAllowed() {
        return updateAllowed;
    }

    @Override
    public void setText(String text) {
        if (updateAllowed) {
            super.setText(text);
        }
    }

    @Override
    public boolean isEditingData() {
        return isEditing;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class AdaptedExtensionFileFilter extends FileFilter {
        public AdaptedExtensionFileFilter() {
            super();
        }

        protected boolean isGoodExtension() {
            return ((extensionFile != null) && (!extensionFile.equals("*")) && (!extensionFile.isEmpty()));
        }

        @Override
        public boolean accept(File f) {
            boolean accepted = true;
            if ((!displayHiddenFile) && f.isHidden()) {
                accepted = false;
            } else if (isGoodExtension() && f.isFile()) {
                accepted = ObjectUtils.sameObject(extensionFile, FileUtils.getExtension(f));
            }
            return accepted;
        }

        @Override
        public String getDescription() {
            if (isGoodExtension()) {
                return extensionFile + " files";
            } else {
                return ObjectUtils.EMPTY_STRING;
            }
        }
    }

}
