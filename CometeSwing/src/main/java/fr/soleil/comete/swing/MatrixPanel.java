/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Arrays;
import java.util.WeakHashMap;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.data.target.matrix.ICometeColorMatrixComponent;
import fr.soleil.comete.definition.data.target.matrix.ITextMatrixComponent;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.event.GroupEvent;
import fr.soleil.data.listener.IRefreshingGroupListener;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.ColorUtils;
import fr.soleil.lib.project.awt.layout.VerticalFlowLayout;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * A {@link JPanel} that receives {@link String} matrix and {@link CometeColor} matrix.
 * <ul>
 * <li>The {@link String} matrix is used to display texts in this panel</li>
 * <li>The {@link CometeColor} matrix is used as texts backgrounds</li>
 * </ul>
 * You can choose whether to respect the {@link String} matrix organization. If so, the texts will be displayed
 * following a {@link GridLayout}. Otherwise, they will be displayed following a {@link VerticalFlowLayout}
 * <p>
 * You can change the vertival and horizontal gaps used by the layout.
 * </p>
 * <p>
 * A title can be set to this panel.
 * </p>
 * <p>
 * This panel is also an {@link IRefreshingGroupListener}, so that you can decide when to refresh its content.
 * </p>
 * <p>
 * You can choose the foreground to use for labels in {@link #setCometeForeground(CometeColor)}. If you choose
 * <code>null</code>, this panel will try to guess the best foreground between black and white for each label, depending
 * on the label background.
 * </p>
 * 
 * @see #setRespectMatrix(boolean)
 * @see #isRespectMatrix()
 * @see #setGaps(int, int)
 * @see #setHorizontalGap(int)
 * @see #getHorizontalGap()
 * @see #setVerticalGap(int)
 * @see #getVerticalGap()
 * @see #setTitle(String)
 * @see #setTitle(String, int, int)
 * @see #getTitle()
 * @see #setListenedGroup(String)
 * @see #getListenedGroup()
 * @see #setListenedGroupMetaInformation(String)
 * @see #getListenedGroupMetaInformation()
 * @see #setCometeForeground(CometeColor)
 *
 * @author Rapha&euml;l GIRARDOT
 */
public class MatrixPanel extends JPanel
        implements ICometeColorMatrixComponent, ITextMatrixComponent, IRefreshingGroupListener {

    private static final long serialVersionUID = -5790779348217770640L;

    protected static final String DEFAULT_TEXT = ObjectUtils.EMPTY_STRING;

    protected final WeakHashMap<CometeColor, Color> colorMap;
    protected static final int DEFAULT_H_GAP = 10;
    protected static final int DEFAULT_V_GAP = 5;

    protected String listenedGroup;
    protected String listenedGroupMetaInformation;
    protected volatile JLabel[] labels;
    protected volatile String[] stringValues;
    protected volatile CometeColor[] colors;
    protected int rows, cols;
    protected int halign;
    protected boolean respectMatrix;
    protected volatile boolean stringChanged, colorChanged, layoutChanged;
    private final ErrorNotificationDelegate errorNotificationDelegate;

    protected int hgap, vgap;

    protected Color fg;

    public MatrixPanel() {
        super(new VerticalFlowLayout(VerticalFlowLayout.CENTER, DEFAULT_H_GAP, DEFAULT_V_GAP));
        colorMap = new WeakHashMap<>();
        listenedGroup = null;
        listenedGroupMetaInformation = null;
        labels = null;
        stringValues = null;
        colors = null;
        rows = 0;
        cols = 0;
        halign = SwingConstants.CENTER;
        respectMatrix = false;
        stringChanged = false;
        colorChanged = false;
        layoutChanged = false;
        hgap = DEFAULT_H_GAP;
        vgap = DEFAULT_V_GAP;
        fg = null;
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
    }

    @Override
    public CometeColor[][] getCometeColorMatrix() {
        // Not managed
        return null;
    }

    @Override
    public void setCometeColorMatrix(CometeColor[][] value) {
        CometeColor[] flatValue;
        int height, width;
        if (value == null) {
            height = 0;
            width = 0;
            flatValue = null;
        } else if (value.length == 0) {
            height = 0;
            width = 0;
            flatValue = new CometeColor[0];
        } else {
            height = value.length;
            width = 0;
            for (CometeColor[] array : value) {
                if (array != null) {
                    width = array.length;
                    break;
                }
            }
            flatValue = (CometeColor[]) ArrayUtils.convertArrayDimensionFromNTo1(value);
        }
        setFlatCometeColorMatrix(flatValue, width, height);
    }

    @Override
    public CometeColor[] getFlatCometeColorMatrix() {
        return colors;
    }

    @Override
    public void setFlatCometeColorMatrix(CometeColor[] value, int width, int height) {
        colors = value;
        colorChanged = true;
        if (listenedGroup == null) {
            updateColorsInEDT();
        }
    }

    @Override
    public String[][] getStringMatrix() {
        // Not managed
        return null;
    }

    @Override
    public void setStringMatrix(String[][] value) {
        String[] flatValue;
        int height, width;
        if (value == null) {
            height = 0;
            width = 0;
            flatValue = null;
        } else if (value.length == 0) {
            height = 0;
            width = 0;
            flatValue = new String[0];
        } else {
            height = value.length;
            width = 0;
            for (String[] array : value) {
                if (array != null) {
                    width = array.length;
                    break;
                }
            }
            flatValue = (String[]) ArrayUtils.convertArrayDimensionFromNTo1(value);
        }
        setFlatStringMatrix(flatValue, width, height);
    }

    @Override
    public String[] getFlatStringMatrix() {
        return stringValues;
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        stringValues = value;
        if (rows != height) {
            rows = height;
            layoutChanged = true;
        }
        if (cols != width) {
            cols = width;
            layoutChanged = true;
        }
        stringChanged = true;
        if (listenedGroup == null) {
            updateLabelsInEDT();
        }
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        return cols;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return rows;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // Not managed. This panel only receives information
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // Not managed. This panel only receives information
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        setBackground(ColorTool.getColor(color));
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(getBackground());
    }

    /**
     * Changes the labels foreground.
     * 
     * @param color The foreground to use. If <code>null</code>, this panel will try to guess the best foreground
     *            between black and white for each label, depending on the label background.
     */
    @Override
    public void setCometeForeground(CometeColor color) {
        Color fg = ColorTool.getColor(color);
        this.fg = fg;
        JLabel[] labels = this.labels;
        for (JLabel label : labels) {
            updateLabelForeGround(label);
        }
    }

    protected void updateLabelForeGround(JLabel label) {
        if (label != null) {
            if (label.isOpaque() && (fg == null)) {
                label.setForeground(getAutoForeground(label.getBackground()));
            } else {
                label.setForeground(Color.BLACK);
            }
        }
    }

    protected Color getAutoForeground(Color bg) {
        Color fg;
        if (bg == null) {
            fg = Color.BLACK;
        } else {
            fg = Math.round(ColorUtils.getRealGray(bg)) < 128 ? Color.WHITE : Color.BLACK;
        }
        return fg;
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(getForeground());
    }

    @Override
    public void setCometeFont(CometeFont cometeFont) {
        Font font = FontTool.getFont(cometeFont);
        setFont(font);
        JLabel[] labels = this.labels;
        for (JLabel label : labels) {
            if (label != null) {
                label.setFont(font);
            }
        }
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(getFont());
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        if (this.halign != halign) {
            this.halign = halign;
            JLabel[] labels = this.labels;
            for (JLabel label : labels) {
                if (label != null) {
                    label.setHorizontalAlignment(halign);
                }
            }
        }
    }

    @Override
    public int getHorizontalAlignment() {
        return halign;
    }

    @Override
    public void setPreferredSize(int width, int height) {
        if ((width < 1) || (height < 1)) {
            setPreferredSize(null);
        } else {
            setPreferredSize(new Dimension(width, height));
        }
    }

    @Override
    public boolean isEditingData() {
        // Not managed
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        setTitle(title);
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void groupRefreshed(GroupEvent event) {
        String listenedGroup = this.listenedGroup;
        String listenedGroupMetaInformation = this.listenedGroupMetaInformation;
        if ((event != null) && (listenedGroup != null) && listenedGroup.equals(event.getGroupName())
                && ((listenedGroupMetaInformation == null)
                        || listenedGroupMetaInformation.equals(event.getMetaInformation()))) {
            updateLabelsAndColorsInEDT();
        }
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

    public String getListenedGroup() {
        return listenedGroup;
    }

    /**
     * Sets the name of the refreshing group this panel should listen to.
     * 
     * @param listenedGroup The name of the refreshing group this panel should listen to. Use <code>null</code> if you
     *            prefer to refresh content at each event (string matrix event and color matrix event)
     */
    public void setListenedGroup(String listenedGroup) {
        this.listenedGroup = listenedGroup;
    }

    /**
     * Returns the name of the refreshing group this panel listens to.
     * 
     * @return A {@link String}
     */
    public String getListenedGroupMetaInformation() {
        return listenedGroupMetaInformation;
    }

    /**
     * Sets the meta information this panel should check when refreshing group matched.
     * 
     * @param listenedGroupMetaInformation The meta information this panel should check when refreshing group matched.
     *            Use <code>null</code> if you don't care for meta information.
     */
    public void setListenedGroupMetaInformation(String listenedGroupMetaInformation) {
        this.listenedGroupMetaInformation = listenedGroupMetaInformation;
    }

    /**
     * Returns the title
     * 
     * @return A {@link String}
     */
    public String getTitle() {
        String title;
        Border border = getBorder();
        if (border instanceof TitledBorder) {
            title = ((TitledBorder) border).getTitle();
        } else {
            title = null;
        }
        return title;
    }

    /**
     * Sets the title
     * 
     * @param title The title to set
     */
    public void setTitle(String title) {
        setTitle(title, -1, -1);
    }

    /**
     * Sets the title and is position and justification
     * 
     * @param title The title to set
     * @param titleJustification The title justification.
     *            Can be one of these:
     *            <ul>
     *            <li>{@link SwingConstants#LEFT}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#RIGHT}</li>
     *            <li>{@link SwingConstants#LEADING}</li>
     *            <li>{@link SwingConstants#TRAILING}</li>
     *            </ul>
     * @param titlePosition The title position.
     *            Can be one of these:
     *            <ul>
     *            <li>{@link SwingConstants#TOP}</li>
     *            <li>{@link SwingConstants#BOTTOM}</li>
     *            </ul>
     */
    public void setTitle(String title, int titleJustification, int titlePosition) {
        if ((title == null) || (title.trim().isEmpty())) {
            setBorder(null);
        } else {
            int justification, position;
            switch (titlePosition) {
                case SwingConstants.TOP:
                    position = TitledBorder.TOP;
                    break;
                case SwingConstants.BOTTOM:
                    position = TitledBorder.BOTTOM;
                    break;
                default:
                    position = TitledBorder.DEFAULT_POSITION;
                    break;
            }
            switch (titleJustification) {
                case SwingConstants.LEFT:
                    justification = TitledBorder.LEFT;
                    break;
                case SwingConstants.CENTER:
                    justification = TitledBorder.CENTER;
                    break;
                case SwingConstants.RIGHT:
                    justification = TitledBorder.RIGHT;
                    break;
                case SwingConstants.LEADING:
                    justification = TitledBorder.LEADING;
                    break;
                case SwingConstants.TRAILING:
                    justification = TitledBorder.TRAILING;
                    break;
                default:
                    justification = TitledBorder.DEFAULT_JUSTIFICATION;
                    break;
            }
            setBorder(new TitledBorder(null, title, justification, position));
        }
    }

    /**
     * Returns the horizontal gap used by the layout
     * 
     * @return An <code>int</code>
     */
    public int getHorizontalGap() {
        return hgap;
    }

    /**
     * Sets the horizontal gap that should be used by the layout
     * 
     * @param hgap The horizontal gap that should be used by the layout
     */
    public void setHorizontalGap(int hgap) {
        this.hgap = hgap < 0 ? DEFAULT_H_GAP : hgap;
        updateLayout();
    }

    /**
     * Returns the vertical gap used by the layout
     * 
     * @return An <code>int</code>
     */
    public int getVerticalGap() {
        return vgap;
    }

    /**
     * Sets the vertical gap that should be used by the layout
     * 
     * @param vgap The vertical gap that should be used by the layout
     */
    public void setVerticalGap(int vgap) {
        this.vgap = hgap < 0 ? DEFAULT_V_GAP : vgap;
        updateLayout();
    }

    /**
     * Sets both the horizontal and vertical gaps that should be used by the layout
     * 
     * @param hgap The horizontal gap that should be used by the layout
     * @param vgap The vertical gap that should be used by the layout
     */
    public void setGaps(int hgap, int vgap) {
        this.hgap = hgap < 0 ? DEFAULT_H_GAP : hgap;
        this.vgap = hgap < 0 ? DEFAULT_V_GAP : vgap;
        updateLayout();
    }

    /**
     * Returns the {@link String} value displayed at a given position in this panel
     * 
     * @param x The x position
     * @param y The y position
     * @return A {@link String}. Can be <code>null</code>.
     */
    public String getStringValueAt(int x, int y) {
        String result;
        Component comp = getComponentAt(x, y);
        if (comp instanceof JLabel) {
            result = ((JLabel) comp).getText();
        } else {
            result = null;
        }
        return result;
    }

    /**
     * Returns whether this panel respects {@link String} matrix organization
     * 
     * @return A <code>boolean</code>
     */
    public boolean isRespectMatrix() {
        return respectMatrix;
    }

    /**
     * Sets whether this panel will respect {@link String} matrix organization
     * 
     * @param respectMatrix Whether this panel will respect {@link String} matrix organization
     */
    public void setRespectMatrix(boolean respectMatrix) {
        if (this.respectMatrix != respectMatrix) {
            this.respectMatrix = respectMatrix;
            updateLayout();
        }
    }

    protected void updateLayout() {
        if (respectMatrix) {
            updateGridLayout();
        } else {
            setLayout(new VerticalFlowLayout(VerticalFlowLayout.CENTER, hgap, vgap));
        }
    }

    protected void updateGridLayout() {
        int rows = this.rows, cols = this.cols;
        if ((rows < 1) || (cols < 1)) {
            setLayout(new GridLayout(1, 1, hgap, vgap));
        } else {
            setLayout(new GridLayout(rows, cols, hgap, vgap));
        }
    }

    protected void setLabelColor(JLabel label, CometeColor cometeColor) {
        if (cometeColor == null) {
            if (label != null) {
                label.setBackground(null);
                label.setOpaque(false);
            }
        } else {
            Color color = colorMap.get(cometeColor);
            if (color == null) {
                color = ColorTool.getColor(cometeColor);
                colorMap.put(cometeColor, color);
            }
            if (label != null) {
                label.setBackground(color);
                label.setOpaque(true);
            }
        }
        updateLabelForeGround(label);
    }

    protected final void updateLabelsInEDT() {
        if (stringChanged) {
            stringChanged = false;
            CometeColor[] colors = this.colors;
            JLabel[] labels = this.labels;
            String[] stringValues = this.stringValues;
            if (SwingUtilities.isEventDispatchThread()) {
                updateLabels(labels, stringValues, colors);
            } else {
                SwingUtilities.invokeLater(() -> {
                    updateLabels(labels, stringValues, colors);
                });
            }
        }
    }

    protected void updateLabels(JLabel[] labels, String[] stringValues, CometeColor[] colors) {
        removeAll();
        if (layoutChanged) {
            layoutChanged = false;
            if (respectMatrix) {
                updateGridLayout();
            }
        }
        if (stringValues == null) {
            labels = null;
        } else {
            int length;
            if (labels == null) {
                labels = new JLabel[stringValues.length];
                length = 0;
            } else {
                length = labels.length;
            }
            if (labels.length != stringValues.length) {
                labels = Arrays.copyOf(labels, stringValues.length);
            }
            for (int i = length; i < labels.length; i++) {
                labels[i] = createLabel();
            }
            if (colors != null) {
                synchronized (colorMap) {
                    int minLength = Math.min(labels.length, colors.length);
                    // add missing colors
                    for (int i = length; i < minLength; i++) {
                        CometeColor cometeColor = colors[i];
                        JLabel label = labels[i];
                        setLabelColor(label, cometeColor);
                    } // end for (int i = length; i < minLength; i++)
                } // end synchronized (colorMap)
            } // end if (colors != null)
            int i = 0;
            for (String value : stringValues) {
                JLabel label = labels[i++];
                setLabelText(label, value);
                add(label);
            }
        } // end if (stringValues == null) ... else
        this.labels = labels;
        revalidate();
        repaint();
    }

    protected final void updateColorsInEDT() {
        if (colorChanged) {
            colorChanged = false;
            CometeColor[] colors = this.colors;
            if (SwingUtilities.isEventDispatchThread()) {
                updateColors(colors);
            } else {
                SwingUtilities.invokeLater(() -> {
                    updateColors(colors);
                });
            }
        }
    }

    protected void updateColors(CometeColor[] colors) {
        JLabel[] labels = this.labels;
        if (labels != null) {
            if (colors == null) {
                for (JLabel label : labels) {
                    if (label != null) {
                        label.setBackground(null);
                        label.setOpaque(false);
                    }
                    updateLabelForeGround(label);
                }
            } else {
                int length = Math.min(labels.length, colors.length);
                synchronized (colorMap) {
                    for (int i = 0; i < length; i++) {
                        CometeColor cometeColor = colors[i];
                        JLabel label = labels[i];
                        setLabelColor(label, cometeColor);
                    } // end for (int i = 0; i < length; i++)
                } // end synchronized (colorMap)
            } // end if (colors == null) ... else
            repaint();
        } // end if (labels != null)
    }

    protected final void updateLabelsAndColorsInEDT() {
        if (stringChanged || colorChanged) {
            stringChanged = false;
            colorChanged = false;
            String[] stringValues = this.stringValues;
            CometeColor[] colors = this.colors;
            if (SwingUtilities.isEventDispatchThread()) {
                updateLabelsAndColors(stringValues, colors);
            } else {
                SwingUtilities.invokeLater(() -> {
                    updateLabelsAndColors(stringValues, colors);
                });
            }
        }
    }

    protected void updateLabelsAndColors(String[] stringValues, CometeColor[] colors) {
        removeAll();
        JLabel[] labels = this.labels;
        if (stringValues == null) {
            labels = null;
        } else {
            if (labels == null) {
                labels = new JLabel[stringValues.length];
                for (int i = 0; i < labels.length; i++) {
                    labels[i] = createLabel();
                }
            }
            if (labels.length != stringValues.length) {
                int length = labels.length;
                labels = Arrays.copyOf(labels, stringValues.length);
                for (int i = length; i < labels.length; i++) {
                    labels[i] = createLabel();
                }
            }
            this.labels = labels;
            int i = 0;
            if (colors == null) {
                for (String value : stringValues) {
                    JLabel label = labels[i++];
                    label.setBackground(null);
                    label.setOpaque(false);
                    updateLabelForeGround(label);
                    setLabelText(label, value);
                    add(label);
                }
            } else {
                synchronized (colorMap) {
                    for (String value : stringValues) {
                        CometeColor cometeColor;
                        if (i < colors.length) {
                            cometeColor = colors[i];
                        } else {
                            cometeColor = null;
                        }
                        JLabel label = labels[i++];
                        setLabelColor(label, cometeColor);
                        setLabelText(label, value);
                        add(label);
                    } // end for (String value : stringValues)
                } // end synchronized (colorMap)
            } // end if (colors == null) ... else
        } // end if (stringValues == null) ... else
        revalidate();
        repaint();
    }

    protected void setLabelText(JLabel label, String value) {
        label.setText(value == null ? DEFAULT_TEXT : value);
        label.revalidate();
    }

    protected JLabel createLabel() {
        JLabel label = new JLabel();
        label.setHorizontalAlignment(halign);
        label.setFont(getFont());
        label.setForeground(getForeground());
        return label;
    }

}
