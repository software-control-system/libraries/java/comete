/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.axis.model;

import java.awt.Point;

/**
 * Class that stores necessary information to draw a label along an axis
 */
public class AxisLabel {

    private String value;
    private final int width;
    private final int height;
    private final double pos;
    private int offSetX;
    private int offSetY;

    /**
     * Creates a new {@link AxisLabel}
     * 
     * @param lab The string to display
     * @param w The label height
     * @param h The label height
     * @param pos The label position
     */
    public AxisLabel(String lab, int w, int h, double pos) {
        value = lab;
        this.width = w;
        this.height = h;
        this.pos = pos;
        offSetX = 0;
        offSetY = 0;
    }

    /**
     * Returns the label width
     * 
     * @return An <code>int</code>
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the label total width, offset included
     * 
     * @return An <code>int</code>
     */
    public int getTotalWidth() {
        return width + Math.abs(offSetX);
    }

    /**
     * Returns the label height
     * 
     * @return An <code>int</code>
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the label total height, offset included
     * 
     * @return An <code>int</code>
     */
    public int getTotalHeight() {
        return height + Math.abs(offSetY);
    }

    /**
     * Updates the label's string
     * 
     * @param value The new string
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Returns the label's string
     * 
     * @return A {@link String}
     */
    public String getValue() {
        return value;
    }

    /**
     * Returns the label's position
     * 
     * @return A <code>double</code>
     */
    public double getPos() {
        return pos;
    }

    /**
     * Returns the label's x offset
     * 
     * @return An <code>int</code>
     */
    public int getX() {
        return offSetX;
    }

    /**
     * Returns the label's y offset
     * 
     * @return An <code>int</code>
     */
    public int getY() {
        return offSetY;
    }

    /**
     * Updates the label's offset
     * 
     * @param labelOffset The new offset
     */
    public void setOffset(Point labelOffset) {
        offSetX = labelOffset.x;
        offSetY = labelOffset.y;
    }

    /**
     * Updates the label's offset
     * 
     * @param x The x offset
     * @param y The y offset
     */
    public void setOffset(int x, int y) {
        this.offSetX = x;
        this.offSetY = y;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getClass().getSimpleName());
        builder.append("{'").append(value);
        builder.append("', height: ").append(height);
        builder.append(", width: ").append(width);
        builder.append("', y: ").append(offSetY);
        builder.append(", x: ").append(offSetX);
        builder.append(", pos: ").append(pos).append('}');
        return builder.toString();
    }
}
