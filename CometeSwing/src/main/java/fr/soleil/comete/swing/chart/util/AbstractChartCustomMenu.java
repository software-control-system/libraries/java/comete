/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

import org.jdesktop.swingx.JXTitledSeparator;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.data.DataView;
import fr.soleil.comete.swing.chart.data.DataViewList;

public abstract class AbstractChartCustomMenu extends JMenu {

    private static final long serialVersionUID = 3950710824595975279L;

    private final int menuPosition;
    private final String menuText;
    private final boolean showAllMenu;
    private final String dialogTitle;
    private final String itemMenu;
    private final JMenuItem allMenuItem;
    protected final JMenuItem selectMenuItem;
    private final List<Component> componentList;

    public AbstractChartCustomMenu(String menuText, boolean showAllMenu, String dialogTitle, String itemMenu) {
        this(-1, menuText, showAllMenu, dialogTitle, itemMenu);
    }

    public AbstractChartCustomMenu(int menuPosition, String menuText, boolean showAllMenu, String dialogTitle,
            String itemMenu) {
        super(menuText);
        allMenuItem = new JMenuItem("All");
        selectMenuItem = new JMenuItem("Select View...");
        componentList = new ArrayList<Component>();
        this.menuPosition = menuPosition;
        this.menuText = menuText;
        this.showAllMenu = showAllMenu;
        this.dialogTitle = dialogTitle;
        this.itemMenu = itemMenu;
        initDefaultMenu();
    }

    public void actionPerformed(Object src, Chart chart) {
        if (src == allMenuItem) {
            showAllDataView(chart);
        } else if (src == selectMenuItem) {
            showDataViewDialog(chart);
        } else if (src instanceof JMenuItem) {
            String item = ((JMenuItem) src).getName();
            showSingleDataViewDialog(item, chart);
        }
    }

    public void showSingleDataViewDialog(String item, Chart chart) {
        DataView dataView = chart.showSingleDataViewDialog(item);
        // System.out.println("showSingleDataViewDialog=" + item);
        if (dataView != null) {
            List<DataView> result = new ArrayList<DataView>();
            result.add(dataView);
            manageDataViews(result, chart);
        }
    }

    public void showDataViewDialog(Chart chart) {
        List<DataView> result = chart.showDataViewSelectionDialog(dialogTitle);
        manageDataViews(result, chart);
        // System.out.println("showDataViewDialog");
    }

    public void showAllDataView(Chart chart) {
        // System.out.println("showAllDataView");
        DataViewList dataViewList = chart.getDataViewList();
        List<DataView> result = dataViewList.getDataViews(false, IChartViewer.Y1, IChartViewer.Y2);
        manageDataViews(result, chart);
    }

    public void clearItemMenu(ActionListener actionListener) {
        allMenuItem.removeActionListener(actionListener);
        selectMenuItem.removeActionListener(actionListener);
        removeAll();
        componentList.clear();
        initDefaultMenu();
        selectMenuItem.addActionListener(actionListener);
        allMenuItem.addActionListener(actionListener);
    }

    public boolean containsComponent(Object src) {
        boolean found = false;
        if (src == allMenuItem) {
            found = true;
        } else if (src == selectMenuItem) {
            found = true;
        } else {
            for (Component component : componentList) {
                if (src == component) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }

    @Override
    public Component add(Component c) {
        Component copyComponent = copyComponent(c);
        componentList.add(copyComponent);
        return super.add(copyComponent);
    }

    // Cannot add a JMenuItem in more than one JMenu
    private Component copyComponent(Component c) {
        Component copyComp = c;
        if (c instanceof JMenuItem) {
            JMenuItem origMenu = (JMenuItem) c;
            JMenuItem copyMenu = (JMenuItem) c;
            copyMenu = new JMenuItem();
            copyMenu.setText(origMenu.getText());
            copyMenu.setName(origMenu.getName());
            copyMenu.setToolTipText(origMenu.getToolTipText());
            copyMenu.setIcon(origMenu.getIcon());
            ActionListener[] actionListeners = origMenu.getActionListeners();
            if (actionListeners != null) {
                for (ActionListener listener : actionListeners) {
                    copyMenu.addActionListener(listener);
                }
            }
            copyComp = copyMenu;
        } else if (c instanceof JSeparator) {
            copyComp = new JSeparator(((JSeparator) c).getOrientation());
        } else if (c instanceof JXTitledSeparator) {
            JXTitledSeparator origSep = (JXTitledSeparator) c;
            copyComp = new JXTitledSeparator(origSep.getTitle(), origSep.getHorizontalAlignment());
        }
        return copyComp;
    }

    private void initDefaultMenu() {
        if (showAllMenu) {
            add(allMenuItem);
        }
    }

    public void addSelectMenu() {
        addSeparator();
        add(selectMenuItem);
    }

    public abstract void manageDataViews(List<DataView> result, Chart chart);

    public int getMenuPosition() {
        return menuPosition;
    }

    public String getMenuText() {
        return menuText;
    }

    public boolean isShowAllMenu() {
        return showAllMenu;
    }

    public String getDialogTitle() {
        return dialogTitle;
    }

    public String getItemMenu() {
        return itemMenu;

    }
}
