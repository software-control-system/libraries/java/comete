/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.Dimension;
import java.awt.Font;
import java.util.List;

import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.view.component.AbstractChartLegend;

@Deprecated
public class JLChartLegend extends AbstractChartLegend {

    private static final long serialVersionUID = 5774039023286118800L;

    private final JLChart chart;

    public JLChartLegend(JLChart chart) {
        super();
        this.chart = chart;
        setBackground(chart.getBackground());
    }

    @Override
    protected void deleteDataView() {
        if (currentView != null) {
            chart.visibleDataView.remove(currentView.getId());
            chart.hiddenDataView.remove(currentView.getId());
            chart.removeDataView((JLDataView) currentView, true);
        }
        currentView = null;
        repaint();
        chart.repaint();
    }

    @Override
    protected void repaintChartIfNecessary() {
        if (chart.isAutoHighlightOnLegend()) {
            chart.repaint();
        }
    }

    protected List<? extends AbstractDataView> getY1Views() {
        return chart.getY1Axis().getViewsCopy();
    }

    protected List<? extends AbstractDataView> getY2Views() {
        return chart.getY2Axis().getViewsCopy();
    }

    @Override
    protected void computeY1AndY2ViewLists() {
        y1Views = getY1Views();
        y2Views = getY2Views();
    }

    @Override
    protected boolean isChartLegendVisible() {
        return chart.isLabelVisible();
    }

    @Override
    protected Font getLegendFont() {
        return chart.getLabelFont();
    }

    @Override
    protected int getChartLegendPlacement() {
        return chart.getLabelPlacement();
    }

    @Override
    protected Dimension getMarginSize() {
        return chart.getMargin();
    }

    @Override
    protected String getY1Name() {
        return chart.getY1Axis().getAxeName();
    }

    @Override
    protected String getY2Name() {
        return chart.getY2Axis().getAxeName();
    }

    @Override
    protected boolean isDataViewRemovingEnabled() {
        return chart.isDataViewRemovingEnabled();
    }

    @Override
    protected boolean isDataViewClickable(String id) {
        return chart.isDataViewClickable(id);
    }

    @Override
    protected void showDataOptionDialog(AbstractDataView view) {
        chart.showDataOptionDialog((JLDataView) view);
    }

    @Override
    protected boolean isAutoHighlightOnLegend() {
        return chart.isAutoHighlightOnLegend();
    }

    @Override
    protected boolean isDataViewHighlighted(String idToHighlight) {
        return chart.isDataViewHighlighted(idToHighlight);
    }

    @Override
    protected void setDataViewHighlighted(String idToHighlight, boolean highlighted) {
        chart.setDataViewHighlighted(idToHighlight, highlighted);
    }

}
