/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.EventObject;
import java.util.List;
import java.util.WeakHashMap;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.soleil.comete.swing.util.CometeUtils;

/**
 * A {@link JPanel} used to manage time precision in charts
 *
 * @author Rapha&euml;l GIRARDOT
 */
public class PrecisionPanel extends JPanel implements ActionListener {

    private static final long serialVersionUID = 8927785700659397946L;

    public static final String TIME_PRECISION_TITLE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.time.precision");
    public static final String VALUE_PRECISION_TITLE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.abscissa.error.margin");

    protected static final Integer[] TIME_PRECISION_VALUE = { 1, 1000, 60000, 3600000, 86400000 };
    protected static final String[] TIME_PRECISION_NAME = {
            ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.time.precision.unit.milliseconds"),
            ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.time.precision.unit.seconds"),
            ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.time.precision.unit.minutes"),
            ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.time.precision.unit.hours"),
            ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.time.precision.unit.days") };
    protected static final String DEFAULT_TOOLTIP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.time.precision.tooltip");
    protected static final String ERROR_TOOLTIP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.time.precision.tooltip.error");

    protected volatile int precision;
    protected final Object precisionLock;
    protected volatile boolean timeScale;
    protected final JLabel precisionLabel;
    protected final JTextField precisionText;
    protected final JComboBox<Integer> precisionCombo;
    protected final Collection<PrecisionPanelListener> listeners = Collections.newSetFromMap(new WeakHashMap<>());
    protected String defaultTooltip, errorTooltip, timePrecisionTitle, valuePrecisionTitle;

    protected int maxPrecisionUnit;

    private String[] timePrecisionNames;

    public PrecisionPanel() {
        super(new BorderLayout(5, 5));
        maxPrecisionUnit = Integer.MAX_VALUE;
        precision = 200;
        precisionLock = new Object();
        precisionLabel = new JLabel(VALUE_PRECISION_TITLE);
        precisionLabel.setFont(CometeUtils.getLabelFont());
        precisionLabel.setForeground(CometeUtils.getfColor());
        precisionText = new JTextField(8);
        precisionText.addKeyListener(new PrecisionKeyValidator());
        precisionText.setMargin(CometeUtils.getTextFieldInsets());
        precisionText.setEditable(true);
        defaultTooltip = DEFAULT_TOOLTIP;
        errorTooltip = ERROR_TOOLTIP;
        timePrecisionTitle = TIME_PRECISION_TITLE;
        valuePrecisionTitle = VALUE_PRECISION_TITLE;
        precisionText.setToolTipText(getDefaultTooltip());
        precisionText.setText("0");
        timePrecisionNames = TIME_PRECISION_NAME;
        precisionCombo = new JComboBox<>(TIME_PRECISION_VALUE);
        precisionCombo.setRenderer(new TimeRenderer());
        precisionCombo.setSelectedIndex(0);
        precisionCombo.addActionListener(this);
        add(precisionLabel, BorderLayout.WEST);
        add(precisionText, BorderLayout.CENTER);
        add(precisionCombo, BorderLayout.EAST);
        timeScale = false;
        updateComponentsFromScale();
    }

    public void setFonts(Font... fonts) {
        if (fonts != null) {
            if (fonts.length > 0) {
                if (fonts[0] != null) {
                    precisionLabel.setFont(fonts[0]);
                }
                if (fonts.length > 1) {
                    if (fonts[1] != null) {
                        precisionText.setFont(fonts[1]);
                    }
                    if (fonts.length > 2) {
                        if (fonts[2] != null) {
                            precisionCombo.setFont(fonts[2]);
                        }
                    }
                }
            }
        }
    }

    public void setTextColumns(int columns) {
        precisionText.setColumns(columns);
    }

    public int getMaxPrecisionUnit() {
        return maxPrecisionUnit;
    }

    public void setMaxPrecisionUnit(int maxPrecisionUnit) {
        if (maxPrecisionUnit >= 1) {
            synchronized (precisionLock) {
                this.maxPrecisionUnit = maxPrecisionUnit;
                int tmp = precision;
                precisionCombo.removeAllItems();
                for (Integer value : TIME_PRECISION_VALUE) {
                    if (value.intValue() <= maxPrecisionUnit) {
                        precisionCombo.addItem(value);
                    }
                }
                setPrecisionNoLock(tmp, false);
            }
        }
    }

    public String[] getTimePrecisionNames() {
        return timePrecisionNames.clone();
    }

    public void setTimePrecisionNames(String... names) {
        String[] tmp;
        if (names == null) {
            tmp = TIME_PRECISION_NAME;
        } else {
            tmp = new String[TIME_PRECISION_NAME.length];
            int length = Math.min(names.length, TIME_PRECISION_NAME.length);
            if (length < TIME_PRECISION_NAME.length) {
                System.arraycopy(TIME_PRECISION_NAME, length, tmp, length, TIME_PRECISION_NAME.length - length);
            }
            for (int i = 0; i < length; i++) {
                String name = names[i];
                if (name == null) {
                    name = TIME_PRECISION_NAME[i];
                }
                tmp[i] = name;
            }
            this.timePrecisionNames = tmp;
        }
        repaint();
    }

    protected String getDefaultTooltip() {
        return defaultTooltip;
    }

    public void setDefaultTooltip(String defaultTooltip) {
        this.defaultTooltip = ((defaultTooltip != null) && defaultTooltip.trim().isEmpty() ? DEFAULT_TOOLTIP
                : defaultTooltip);
        if (computePrecision()) {
            precisionText.setToolTipText(getDefaultTooltip());
        }
    }

    protected String getErrorTooltip() {
        return errorTooltip;
    }

    public void setErrorTooltip(String errorTooltip) {
        this.errorTooltip = ((errorTooltip != null) && errorTooltip.trim().isEmpty() ? ERROR_TOOLTIP : errorTooltip);
        if (!computePrecision()) {
            precisionText.setToolTipText(getErrorTooltip());
        }
    }

    public String getTimePrecisionTitle() {
        return timePrecisionTitle;
    }

    public void setTimePrecisionTitle(String timePrecisionTitle) {
        this.timePrecisionTitle = (timePrecisionTitle == null ? TIME_PRECISION_TITLE : timePrecisionTitle);
        precisionLabel.setText(timeScale ? getTimePrecisionTitle() : getValuePrecisionTitle());
    }

    public String getValuePrecisionTitle() {
        return valuePrecisionTitle;
    }

    public void setValuePrecisionTitle(String valuePrecisionTitle) {
        this.valuePrecisionTitle = (valuePrecisionTitle == null ? VALUE_PRECISION_TITLE : valuePrecisionTitle);
        precisionLabel.setText(timeScale ? getTimePrecisionTitle() : getValuePrecisionTitle());
    }

    public boolean isTimeScale() {
        return timeScale;
    }

    public void setTimeScale(boolean timeScale) {
        if (this.timeScale != timeScale) {
            this.timeScale = timeScale;
            updateComponentsFromScale();
        }
    }

    protected void updateComponentsFromScale() {
        precisionLabel.setText(timeScale ? getTimePrecisionTitle() : getValuePrecisionTitle());
        precisionCombo.setVisible(timeScale);
        synchronized (precisionLock) {
            setPrecisionNoLock(precision, false);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (computePrecision()) {
            precisionChanged();
        }
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        synchronized (precisionLock) {
            setPrecisionNoLock(precision, true);
        }
    }

    protected void setPrecisionNoLock(int precision, boolean testForDuplicate) {
        if ((!testForDuplicate) || (this.precision != precision)) {
            this.precision = precision;
            int effectivePrecision = precision;
            int selected = 1;
            if ((precision > 0) && isTimeScale()) {
                for (Integer tmp : TIME_PRECISION_VALUE) {
                    if ((precision % tmp.intValue() == 0) && (tmp.intValue() <= maxPrecisionUnit)) {
                        effectivePrecision = precision / tmp;
                        selected = tmp;
                    } else {
                        break;
                    }
                }
            }
            precisionText.setText(Integer.toString(effectivePrecision));
            precisionCombo.removeActionListener(this);
            precisionCombo.setSelectedItem(selected);
            precisionCombo.addActionListener(this);
        }
    }

    protected void precisionChanged() {
        List<PrecisionPanelListener> copy = new ArrayList<>();
        synchronized (listeners) {
            copy.addAll(listeners);
        }
        PrecisionPanelEvent event = new PrecisionPanelEvent(this);
        for (PrecisionPanelListener listener : copy) {
            listener.precisionPanelChanged(event);
        }
        copy.clear();
    }

    protected boolean computePrecision() {
        boolean computed;
        synchronized (precisionLock) {
            int tmp;
            try {
                tmp = Integer.parseInt(precisionText.getText());
                tmp *= ((Integer) precisionCombo.getSelectedItem()).intValue();
                computed = true;
                precision = tmp;
            } catch (Exception e) {
                computed = false;
            }
        }
        return computed;
    }

    public void addPrecisionPanelListener(PrecisionPanelListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    public void removePrecisionPanelListener(PrecisionPanelListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class TimeRenderer extends DefaultListCellRenderer {

        private static final long serialVersionUID = -3761016758603755118L;

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
                boolean cellHasFocus) {
            Object adaptedValue = value;
            if ((index > -1) && (index < timePrecisionNames.length)) {
                adaptedValue = timePrecisionNames[index];
            } else if (value instanceof Integer) {
                for (int i = 0; i < timePrecisionNames.length; i++) {
                    if (TIME_PRECISION_VALUE[i].equals(value)) {
                        adaptedValue = timePrecisionNames[i];
                        break;
                    }
                }
            }
            return super.getListCellRendererComponent(list, adaptedValue, index, isSelected, cellHasFocus);
        }
    }

    public static class PrecisionPanelEvent extends EventObject {

        private static final long serialVersionUID = 7557689422680578548L;

        public PrecisionPanelEvent(PrecisionPanel panel) {
            super(panel);
        }

        @Override
        public PrecisionPanel getSource() {
            return (PrecisionPanel) super.getSource();
        }
    }

    public static interface PrecisionPanelListener extends EventListener {

        public void precisionPanelChanged(PrecisionPanelEvent event);
    }

    protected class PrecisionKeyValidator implements KeyListener {

        @Override
        public void keyPressed(KeyEvent evt) {
            if (!PositiveIntegerKeyValidator.isValid(evt)) {
                evt.consume();
            }
        }

        @Override
        public void keyReleased(KeyEvent evt) {
            if (PositiveIntegerKeyValidator.isValid(evt)) {
                if (evt.getSource() == precisionText) {
                    if (computePrecision()) {
                        precisionText.setBackground(Color.WHITE);
                        precisionText.setToolTipText(getDefaultTooltip());
                        precisionChanged();
                    } else {
                        precisionText.setBackground(Color.ORANGE);
                        precisionText.setToolTipText(getErrorTooltip());
                    }
                }
            } else {
                evt.consume();
            }
        }

        @Override
        public void keyTyped(KeyEvent evt) {
            if (!PositiveIntegerKeyValidator.isValid(evt)) {
                evt.consume();
            }
        }
    }
}
