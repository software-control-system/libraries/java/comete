/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.image.ijviewer.events.RoiStatisticsListener;
import fr.soleil.comete.swing.image.util.ij.CometeIJMathTool;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.math.NumberArrayUtils;
import ij.gui.Roi;

/**
 * A component that may compute and display statistics of some data inside a {@link Roi} in an {@link ImageViewer}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IStatisticsComponent {

    /**
     * Whether only rectangle {@link Roi} will be accepted for statistics calculation
     */
    public static final boolean RECTANGLE_ROI_ONLY = true;
    /**
     * The default {@link Font} that can be used to display information.
     */
    public static final Font DEFAULT_FONT = new Font(Font.DIALOG, Font.PLAIN, 12);
    /**
     * The default format that may be used when no value format is set.
     * 
     * @see #getValueFormat()
     * @see #setValueFormat(String)
     */
    public static final String DEFAULT_VALUE_FORMAT = "%.2f";
    /**
     * Separator used for ranges.
     */
    public static final String RANGES_SEPARATOR = ", ";
    /**
     * Default text that may be used to indicate there is no value.
     */
    public static final String NO_VALUE = "no value";
    /**
     * Expected index for minimum in statistics array.
     */
    public static final int MIN_INDEX = 0;
    /**
     * Expected index for maximum in statistics array.
     */
    public static final int MAX_INDEX = 1;
    /**
     * Expected index for average in statistics array.
     */
    public static final int AVERAGE_INDEX = 2;
    /**
     * Expected index for standard deviation in statistics array.
     */
    public static final int STDDEV_INDEX = 3;
    /**
     * Expected index for sample standard deviation in statistics array.
     */
    public static final int SAMPLESTDDEV_INDEX = 4;

    /**
     * Generates an array that can be used for storing statistics.
     * 
     * @return A <code>double</code> array.
     */
    public static double[] generateStatisticsArray() {
        double[] statistics = new double[5];
        Arrays.fill(statistics, Double.NaN);
        return statistics;
    }

    /**
     * Computes statistics from a {@link Roi} in an {@link ImageViewer} and puts them in a <code>double</code> array.
     * 
     * @param imgViewer The {@link ImageViewer}.
     * @param roi The {@link Roi}.
     * @param statistics The <code>double</code> array.
     */
    public static void computeStatistics(ImageViewer imgViewer, Roi roi, double[] statistics) {
        if (statistics != null) {
            Arrays.fill(statistics, Double.NaN);
            if ((roi != null) && (imgViewer != null)) {
                double[] data = CometeIJMathTool.getDataAsFlatDoubleMatrix(roi, imgViewer.getValue(),
                        imgViewer.getDimX(), imgViewer.getDimY(), true, imgViewer.getMask(),
                        CometeIJMathTool.getValidatedSectorMask(imgViewer));
                NumberArrayUtils.fillStatistics(statistics, MIN_INDEX, MAX_INDEX, AVERAGE_INDEX, -1, STDDEV_INDEX,
                        SAMPLESTDDEV_INDEX, -1, -1, data);
            }
        }
    }

    /**
     * Returns the {@link String} representation of a value, according to a format.
     * 
     * @param value The value.
     * @param valueFormat The potential format to use.
     * @param format Whether to
     * @return
     */
    public static String toString(double value, String valueFormat, boolean format) {
        String result;
        String formatToUse = valueFormat;
        if (!Format.isNumberFormatOK(formatToUse)) {
            formatToUse = null;
        }
        if ((formatToUse == null) && format) {
            formatToUse = DEFAULT_VALUE_FORMAT;
        }
        if (Double.isNaN(value)) {
            result = NO_VALUE;
        } else if (format) {
            result = Format.formatValue(value, DEFAULT_VALUE_FORMAT);
        } else {
            result = Double.toString(value);
        }
        return result;
    }

    /**
     * Returns whether a {@link Roi} type is valid for statistics calculation.
     * 
     * @param roi The {@link Roi}.
     * @param rectangleRoiOnly Whether to only accept rectangle {@link Roi}.
     * @return A <code>boolean</code>.
     */
    public static boolean validateType(Roi roi, boolean rectangleRoiOnly) {
        boolean roiTypeOk;
        if (roi == null) {
            roiTypeOk = false;
        } else if (rectangleRoiOnly) {
            roiTypeOk = (roi.getType() == Roi.RECTANGLE);
        } else {
            roiTypeOk = true;
        }
        return roiTypeOk;
    }

    /**
     * Extracts the {@link Roi} to use from an {@link ImageViewer} to compute statistics, according to a {@link Roi}
     * validation method.
     * 
     * @param imgViewer The {@link ImageViewer}.
     * @param roiCheckFunction The method that will accept a {@link Roi} as argument and tell whether it is valid for
     *            statistics calculation.
     * @return A {@link Roi}.
     */
    public static Roi getRoi(ImageViewer imgViewer, Predicate<Roi> roiCheckFunction) {
        Roi roi = null;
        if (imgViewer != null) {
            IJRoiManager manager = imgViewer.getRoiManager();
            if (manager != null) {
                Collection<Roi> specialRois = manager.getSpecialRois();
                if (specialRois == null) {
                    specialRois = new ArrayList<>();
                }
                Collection<Roi> selectedRois = manager.getSelectedRois();
                if ((selectedRois != null) && (!selectedRois.isEmpty())) {
                    for (Roi selectedRoi : selectedRois) {
                        if (roiCheckFunction.test(selectedRoi) && !specialRois.contains(selectedRoi)) {
                            roi = selectedRoi;
                            break;
                        }
                    }
                }
            }
        }
        return roi;
    }

    /**
     * Returns the {@link RoiStatisticsListener} that will listen to {@link ImageViewer} and {@link Roi} to
     * automatically update statistics information.
     * 
     * @return A {@link RoiStatisticsListener}.
     */
    public RoiStatisticsListener getRoiListener();

    /**
     * Returns the known value format.
     * 
     * @return A {@link String}.
     * @see #setValueFormat(String)
     * @see #DEFAULT_VALUE_FORMAT
     */
    public String getValueFormat();

    /**
     * Sets the value format to use.
     * 
     * @param valueFormat The value format to use.
     * @see #getValueFormat()
     * @see #DEFAULT_VALUE_FORMAT
     */
    public void setValueFormat(String valueFormat);

    /**
     * Computes statistics from an {@link ImageViewer} and its selected {@link Roi}, and displays these statistics and
     * the information about that {@link Roi}.
     * 
     * @param imageViewer The {@link ImageViewer}.
     */
    public void computeStatisticsAndDisplayRoiInformation(ImageViewer imageViewer);

    /**
     * Checks whether a {@link Roi} is usable for statistics calculation, according to its type.
     * 
     * @param roi The {@link Roi} to check.
     * @return A <code>boolean</code>.
     */
    public boolean checkType(Roi roi);

}
