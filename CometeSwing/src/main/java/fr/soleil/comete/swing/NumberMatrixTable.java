/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.lang.reflect.Array;

import javax.activation.UnsupportedDataTypeException;
import javax.swing.JDialog;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.JXTable;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.data.target.matrix.INumberMatrixComponent;
import fr.soleil.comete.definition.listener.INumberTableListener;
import fr.soleil.comete.definition.listener.ITableListener;
import fr.soleil.comete.definition.util.FileArrayUtils;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.DefaultTableModelWithRowName;
import fr.soleil.comete.swing.util.mask.MaskedTable;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.BasicNumberMatrix;
import fr.soleil.data.container.matrix.ByteMatrix;
import fr.soleil.data.container.matrix.DoubleMatrix;
import fr.soleil.data.container.matrix.FloatMatrix;
import fr.soleil.data.container.matrix.IntMatrix;
import fr.soleil.data.container.matrix.LongMatrix;
import fr.soleil.data.container.matrix.ShortMatrix;
import fr.soleil.data.container.matrix.util.NumberMatrixGenerator;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;

public class NumberMatrixTable extends AbstractTable<AbstractNumberMatrix<?>> implements INumberMatrixComponent {

    private static final long serialVersionUID = 2171524833350225610L;

    @Override
    protected DefaultTableModelWithRowName generateDefaultModel() {
        return new DefaultTableModelWithRowName() {

            private static final long serialVersionUID = 7040065033196154065L;

            @Override
            public int getRowCount() {
                if (matrix != null) {
                    return matrix.getHeight();
                }
                return 0;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (Format.isNumberFormatOK(format)) {
                    return String.class;
                } else {
                    return Number.class;
                }
            }

            @Override
            public int getColumnCount() {
                if (matrix != null) {
                    return matrix.getWidth();
                }
                return 0;
            }

            @Override
            public String getColumnName(int column) {
                String[] customColumnNames = NumberMatrixTable.this.customColumnNames;
                if ((customColumnNames == null) || (column >= customColumnNames.length) || (column < 0)) {
                    return String.valueOf(column);
                } else {
                    return customColumnNames[column];
                }
            }

            @Override
            public String getRowName(int row) {
                String[] customRowNames = NumberMatrixTable.this.customRowNames;
                if ((customRowNames == null) || (row >= customRowNames.length) || (row < 0)) {
                    return String.valueOf(row);
                } else {
                    return customRowNames[row];
                }
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                if (loadDataVisible) {
                    return false;
                }
                return editable;
            }

            @Override
            public Object getValueAt(int row, int column) {
                Object result = null;
                if (matrix != null) {
                    result = matrix.getValueAt(row, column);
                    if ((result instanceof Number) && Format.isNumberFormatOK(format)) {
                        Number nb = (Number) result;
                        try {
                            result = Format.format(format, nb);
                        } catch (Exception e) {
                            result = String.valueOf(nb);
                        }
                    }
                }
                return result;
            }

            @Override
            public void setValueAt(Object aValue, int row, int column) {
                if (matrix != null) {
                    try {
                        matrix.setValueFromStringAt(aValue.toString(), row, column);
                        tableModel.fireTableDataChanged();
                        fireValueChanged(matrix);
                    } catch (Exception e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .error("Failed to set value at " + row + ", " + column, e);
                        // super.setValueAt(aValue, row, column);
                    }
                }
            }
        };
    }

    @Override
    public Object[] getNumberMatrix() {
        Object[] result = null;
        if (matrix != null) {
            result = matrix.getValue();
        }
        return result;
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        AbstractNumberMatrix<?> result = NumberMatrixGenerator.getNewMatrixFor(value);
        if (result != null) {
            try {
                result.setValue(value);
            } catch (UnsupportedDataTypeException e) {
                // Should not happen
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to set number matrix", e);
            }
        }
        setData(result);
    }

    @Override
    public Object getFlatNumberMatrix() {
        Object result = null;
        if (matrix != null) {
            result = matrix.getFlatValue();
        }
        return result;
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        BasicNumberMatrix result = new BasicNumberMatrix();
        try {
            result.setFlatValue(value, height, width);
        } catch (UnsupportedDataTypeException e) {
            // Should not happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to set flat number matrix", e);
        }
        setData(result);
    }

    @Override
    protected void applySelection() {
        if ((matrix != null) && (table.getSelectedColumnCount() > 0)) {
            int[] selectedColumn = table.getSelectedColumns();
            int[] selectedRow = table.getSelectedRows();

            for (int row = 0; row < selectedRow.length; row++) {
                for (int col = 0; col < selectedColumn.length; col++) {
                    matrix.setValueFromStringAt(table.getValueAt(selectedRow[row], selectedColumn[col]).toString(), row,
                            col);
                }
            }

            fireValueChanged(matrix);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected AbstractNumberMatrix<?> getCopy(AbstractNumberMatrix<?> toCopy) {
        AbstractNumberMatrix<?> copy = null;
        if (matrix != null) {
            Class<?> type = matrix.getType();
            if (Byte.TYPE.equals(type) || Byte.class.equals(type)) {
                copy = new ByteMatrix((Class<Byte>) type);
            } else if (Short.TYPE.equals(type) || Short.class.equals(type)) {
                copy = new ShortMatrix((Class<Short>) type);
            } else if (Integer.TYPE.equals(type) || Integer.class.equals(type)) {
                copy = new IntMatrix((Class<Integer>) type);
            } else if (Long.TYPE.equals(type) || Long.class.equals(type)) {
                copy = new LongMatrix((Class<Long>) type);
            } else if (Float.TYPE.equals(type) || Float.class.equals(type)) {
                copy = new FloatMatrix((Class<Float>) type);
            } else if (Double.TYPE.equals(type) || Double.class.equals(type)) {
                copy = new DoubleMatrix((Class<Double>) type);
            } else if (Number.class.equals(type)) {
                copy = new BasicNumberMatrix();
            }
            if (copy != null) {
                try {
                    copy.setValue(matrix.getValue());
                } catch (UnsupportedDataTypeException e) {
                    // Should not happen
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to get copy", e);
                }
            }
        }
        return copy;
    }

    @Override
    protected boolean isSameMatrixStructure(AbstractNumberMatrix<?> matrix1, AbstractNumberMatrix<?> matrix2) {
        boolean sameStructure = false;
        if (matrix1 == null) {
            sameStructure = (matrix2 == null);
        } else if (matrix2 != null) {
            sameStructure = ((matrix1.getHeight() == matrix2.getHeight())
                    && (matrix1.getWidth() == matrix2.getWidth()));
        }
        return sameStructure;
    }

    @Override
    public void loadDataFile(String loadDataFile) {
        loadMatrix = new DoubleMatrix();
        FileArrayUtils.loadMatrixDataFile(loadMatrix, loadDataFile);
        setLoadDataVisible(true);
    }

    @Override
    public void saveDataFile(String fileName) {
        if ((fileName != null) && (matrix != null)) {
            synchronized (matrix) {
                setSaveColumnIndex(saveColumnIndex);
                setValueSeparator(valueSeparator);
                FileArrayUtils.saveMatrixDataFile(matrix, fileName);
            }
        }
    }

    @Override
    protected void showTrimTable(final AbstractNumberMatrix<?> matrix) {
        MaskedTable trimedTable = new MaskedTable() {

            private static final long serialVersionUID = 6588341515040451454L;

            @Override
            public boolean isHiddenByMask(int row, int column) {
                return super.isHiddenByMask(row - matrix.getTrimStartLine(), column - matrix.getTrimStartColumn());
            }
        };

        DefaultTableModelWithRowName trimedTableModel = new DefaultTableModelWithRowName() {

            private static final long serialVersionUID = 5034187355187329609L;

            @Override
            public int getRowCount() {
                int count = 0;
                if (matrix != null) {
                    count = matrix.getTrimEndLine() - matrix.getTrimStartLine() + 1;
                }
                if (count < 0) {
                    count = 0;
                }
                return count;
            }

            @Override
            public int getColumnCount() {
                int count = 0;
                if (matrix != null) {
                    count = matrix.getTrimEndColumn() - matrix.getTrimStartColumn() + 1;
                }
                if (count < 0) {
                    count = 0;
                }
                return count;
            }

            @Override
            public String getColumnName(int column) {
                if (matrix != null) {
                    String[] customColumnNames = NumberMatrixTable.this.customColumnNames;
                    int index = matrix.getTrimStartColumn() + column;
                    if ((customColumnNames == null) || (index >= customColumnNames.length) || (index < 0)) {
                        return String.valueOf(index);
                    } else {
                        return customColumnNames[index];
                    }
                }
                return ObjectUtils.EMPTY_STRING;
            }

            @Override
            public String getRowName(int row) {
                if (matrix != null) {
                    String[] customRowNames = NumberMatrixTable.this.customRowNames;
                    int index = matrix.getTrimStartLine() + row;
                    if ((customRowNames == null) || (index >= customRowNames.length) || (index < 0)) {
                        return String.valueOf(index);
                    } else {
                        return customRowNames[index];
                    }
                }
                return ObjectUtils.EMPTY_STRING;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;

            }

            @Override
            public Object getValueAt(int row, int column) {
                Object tmpValue;
                if (matrix == null) {
                    tmpValue = ObjectUtils.EMPTY_STRING;
                } else {
                    try {
                        tmpValue = matrix.getValueAt(matrix.getTrimStartLine() + row,
                                matrix.getTrimStartColumn() + column);
                        if ((tmpValue instanceof Number) && Format.isNumberFormatOK(format)) {
                            tmpValue = Format.format(format, (Number) tmpValue);
                        }
                    } catch (Exception e) {
                        tmpValue = null;
                    }
                }
                return tmpValue;
            }

        };

        trimedTable.setModel(trimedTableModel);
        trimedTableModel.fireTableDataChanged();
        trimedTable.setColumnControlVisible(false);
        trimedTable.setAutoResizeMode(JXTable.AUTO_RESIZE_OFF);
        trimedTable.packAll();

        JScrollPane trimedScrollPane = new JScrollPane();
        trimedScrollPane.setViewportView(trimedTable);

        JDialog trimedDialog = new JDialog(CometeUtils.getFrameForComponent(this),
                "Matrix Trimed Data at " + DateUtil.milliToString(System.currentTimeMillis(), DATE_FORMAT), false);
        trimedDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        trimedDialog.setContentPane(trimedScrollPane);

        if (trimedDialog != null && !trimedDialog.isVisible()) {
            trimedDialog.setLocationRelativeTo(null);
            trimedDialog.setLocation(trimedDialog.getLocation().x, 0);
            trimedDialog.setSize(600, 600);
            trimedDialog.setPreferredSize(trimedDialog.getSize());
            trimedDialog.pack();
            trimedDialog.setVisible(true);
        }
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        int height = 0;
        if ((matrix != null) && matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if (value != null) {
                height = value.length;
            }
        }
        return height;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int width = 0;
        if ((matrix != null) && matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if ((value != null) && (value.length > 0)) {
                width = Array.getLength(value[0]);
            }
        }
        return width;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return false;
    }

    public Number getValueAtPoint(double[] point) {
        Number value = (matrix == null ? null
                : matrix.getValueAt((int) point[IChartViewer.Y_INDEX], (int) point[IChartViewer.X_INDEX]));
        if ((!listeners.isEmpty()) && (value != null)) {
            for (ITableListener<AbstractNumberMatrix<?>> listener : listeners) {
                if (listener != null) {
                    if (listener instanceof INumberTableListener) {
                        ((INumberTableListener) listener).selectedValueChanged(value);
                    }
                    listener.selectedRowChanged((int) (point[IChartViewer.Y_INDEX]));
                    listener.selectedColumnChanged((int) (point[IChartViewer.X_INDEX]));
                    listener.selectedPointChanged(point);
                }
            }
        }
        return value;
    }

    public Number getValueAtIndex(int column) {
        return getValueAtPoint(new double[] { 0, column });
    }

}
