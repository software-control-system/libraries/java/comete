/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import ij.gui.Roi;
import ij.gui.Toolbar;

public abstract class AutoDeleteSimpleRoiGenerator extends AbstractRoiGenerator {

    public AutoDeleteSimpleRoiGenerator(String text, String description, ImageIcon icon) {
        super(text, description, icon);
    }

    @Override
    public Roi createNewRoi(int sx, int sy, IJRoiManager roiManager) {
        return new Roi(sx, sy, roiManager);
    }

    @Override
    public boolean isCompatibleWithRoi(Roi roi) {
        return false;
    }

    @Override
    public boolean needsNoHandledRoi() {
        return true;
    }

    @Override
    public boolean isSilentRoi() {
        return true;
    }

    @Override
    public int getRoiModeMenu() {
        return ImageViewer.TYPE_SELECTION_MENU;
    }

    @Override
    protected int getTool() {
        return Toolbar.RECTANGLE;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e != null) {
            ImageViewer viewer = recoverViewer(e.getComponent());
            if (viewer != null) {
                Roi roi = viewer.getRoiManager().getRoi();
                viewer.getRoiManager().deleteSpecialRoi(this);
                doExpectedWork(e, viewer, roi, viewer.getImageCanvas());
            }
        }
    }

    @Override
    public MouseEvent adaptMouseClickedEvent(MouseEvent event) {
        return null;
    }

    protected abstract void doExpectedWork(MouseEvent e, ImageViewer viewer, Roi roi, IJCanvas canvas);

}
