/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.comete.swing.chart.axis.model.AxisAttributes;
import fr.soleil.comete.swing.chart.axis.scale.LogarithmicScale;
import fr.soleil.comete.swing.chart.axis.view.VerticalAxisView;
import fr.soleil.lib.project.awt.FontUtils;

/** A labeled color gradient viewer (vertical orientation only) */
public class JGradientViewer extends JComponent {

    private static final long serialVersionUID = 1698293469830041526L;

    private static final int DEFAULT_PREFERRED_WIDTH = -1;

    private Gradient gradient;
    private int[] colorMap;
    private final VerticalAxisView axis;

    private int barWidth;
    private int preferredWidth;

    /**
     * Construct a default black and white gradient.
     */
    public JGradientViewer() {
        super();
        barWidth = 20;
        setLayout(null);
        setBorder(null);
        setOpaque(true);
        gradient = new Gradient();
        colorMap = gradient.buildColorMap(256);
        AxisAttributes attributes = new AxisAttributes(IChartViewer.VERTICAL_RIGHT, null);
        attributes.setAutoScale(false);
        attributes.setInverted(true);
        attributes.setMinimum(0);
        attributes.setMaximum(100);
        attributes.setDrawOpposite(false);
        attributes.setZeroAlwaysVisible(false);
        axis = new VerticalAxisView(attributes, SwingConstants.LEFT, SwingConstants.RIGHT);
//        axis = new JLAxis(this, IChartViewer.VERTICAL_RIGHT, false);
//        axis.setAutoScale(false);
//        // axis.setAnnotation(IChartViewer.VALUE_ANNO);
//        axis.setMinimum(0.0);
//        axis.setMaximum(100.0);
        preferredWidth = DEFAULT_PREFERRED_WIDTH;
    }

    /**
     * Return the current gradient.
     * 
     * @see #setGradient
     */
    public Gradient getGradient() {
        return gradient;
    }

    /**
     * Sets the gradient to be displayed.
     * 
     * @param gradient Gradient object
     */
    public void setGradient(Gradient gradient) {
        this.gradient = gradient;
        colorMap = this.gradient.buildColorMap(256);
    }

    /**
     * Returns a Handle to the axis.
     */
    public VerticalAxisView getAxis() {
        return axis;
    }

    /** Returns the current bar width */
    public int getBarWidth() {
        return barWidth;
    }

    /** Sets the bar thickness. */
    public void setBarWidth(int barWidth) {
        this.barWidth = barWidth;
    }

    private int computeAxisHeight(int height) {
        return height - 21;
    }

    @Override
    public void paint(Graphics g) {
        Dimension d = getSize();

        if (isOpaque()) {
            g.setColor(getBackground());
            g.fillRect(0, 0, d.width, d.height);
        }

        int bw, startX = 0;

        if (d.height > 20 && d.width > 0) {
            boolean axisVisible;
//            axis.measureAxis(FontUtils.getDefaultRenderContext(), 0, d.height - 21);
            int axisStart = 10, axisHeight = computeAxisHeight(d.height);
            axis.measureAxis(FontUtils.getDefaultRenderContext(), axisHeight, null);

//            if (d.width < barWidth) {
//                bw = d.width;
//                startX = 0;
//                axisVisible = false;
//            } else {
//                bw = barWidth;
//                if (d.width < barWidth + axis.getThickness()) {
//                    startX = (d.width - barWidth) / 2;
//                    axisVisible = false;
//                } else {
//                    startX = (d.width - (barWidth + axis.getThickness())) / 2;
//                    axisVisible = true;
//                }
//            }
            if (d.width < barWidth) {
                bw = d.width;
                axisVisible = false;
            } else {
                bw = barWidth;
                axisVisible = true;
            }

            double r = 256.0 / (d.height - 20);
            for (int i = 10; i < d.height - 10; i++) {
                int id = (int) (r * (i - 10));

                if (id <= 255) {
                    g.setColor(new Color(colorMap[255 - id]));
                } else {
                    g.setColor(new Color(colorMap[0]));
                }

                g.drawLine(startX, i, startX + bw, i);
            }

            if (axisVisible && (g instanceof Graphics2D)) {
//                axis.paintAxisDirect(g, FontUtils.getDefaultRenderContext(), startX + bw, 10, Color.BLACK, 0, 0);
                axis.paintAxis((Graphics2D) g, FontUtils.getDefaultRenderContext(), null, startX + bw, axisStart, 0, 0,
                        false, axisHeight, axisStart);
            }
        }

    }

    /**
     * Returns this {@link JGradientViewer}'s preferred width
     * 
     * @return An <code>int</code>
     */
    public int getPreferredWidth() {
        return preferredWidth;
    }

    /**
     * Changes this {@link JGradientViewer}'s preferred width
     * 
     * @param preferredWidth The preferred width to use
     */
    public void setPreferredWidth(int preferredWidth) {
        this.preferredWidth = preferredWidth;
    }

    @Override
    public Dimension getPreferredSize() {
        return getMinimumSize();
    }

    @Override
    public Dimension getMinimumSize() {
        int width = getPreferredWidth();
//        axis.measureAxis(FontUtils.getDefaultRenderContext(), 0, getHeight() - 21);
        axis.measureAxis(FontUtils.getDefaultRenderContext(), computeAxisHeight(getHeight()), null);
        if (width < 1) {
            width = axis.getThickness() + barWidth + 4;
        }
        return new Dimension(width, 20);
    }

    public static void main(String args[]) {
        final JFrame f = new JFrame();
        final JGradientViewer gv = new JGradientViewer();
        gv.setPreferredSize(new Dimension(50, 200));
        gv.getAxis().getAttributes().setMinimum(1e-9);
        gv.getAxis().getAttributes().setMaximum(1e-6);
        gv.getAxis().getAttributes().setScale(IChartViewer.LOG_SCALE);
        gv.getAxis().setScale(new LogarithmicScale(gv.getAxis().getAttributes()));
        f.setContentPane(gv);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }

}
