/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.filechooser.FileFilter;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;

import org.jdesktop.swingx.action.AbstractActionExt;
import org.jdesktop.swingx.action.ActionContainerFactory;
import org.jdesktop.swingx.action.ActionManager;
import org.slf4j.LoggerFactory;

import com.bric.swing.ColorPicker;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.event.ImageViewerEvent;
import fr.soleil.comete.definition.event.ImageViewerEvent.Reason;
import fr.soleil.comete.definition.event.MaskEvent;
import fr.soleil.comete.definition.event.MathematicSectorEvent;
import fr.soleil.comete.definition.event.ValueConvertorEvent;
import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.listener.IImageViewerListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.listener.ITableListener;
import fr.soleil.comete.definition.listener.IValueConvertorListener;
import fr.soleil.comete.definition.listener.MaskListener;
import fr.soleil.comete.definition.listener.MathematicSectorListener;
import fr.soleil.comete.definition.util.AbstractValueConvertor;
import fr.soleil.comete.definition.util.FileArrayUtils;
import fr.soleil.comete.definition.util.IImageExporter;
import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.comete.definition.util.SnapshotEventDelegate;
import fr.soleil.comete.definition.util.TransferEventDelegate;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeRoi;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.comete.definition.widget.util.IRoi;
import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MaskException;
import fr.soleil.comete.definition.widget.util.MaskHandler;
import fr.soleil.comete.definition.widget.util.MathematicSector;
import fr.soleil.comete.definition.widget.util.RoiShape;
import fr.soleil.comete.swing.chart.axis.model.AxisAttributes;
import fr.soleil.comete.swing.chart.axis.scale.LinearScale;
import fr.soleil.comete.swing.chart.axis.scale.LogarithmicScale;
import fr.soleil.comete.swing.chart.axis.view.AbstractAxisView;
import fr.soleil.comete.swing.chart.axis.view.HorizontalAxisView;
import fr.soleil.comete.swing.chart.axis.view.VerticalAxisView;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.image.ImageJManager;
import fr.soleil.comete.swing.image.exception.ApplicationIdException;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.image.ijviewer.IJRoiPermission;
import fr.soleil.comete.swing.image.ijviewer.components.ARoiInfoTable;
import fr.soleil.comete.swing.image.ijviewer.components.CalibrationDialog;
import fr.soleil.comete.swing.image.ijviewer.components.CoordsLabel;
import fr.soleil.comete.swing.image.ijviewer.components.HistogramDialog;
import fr.soleil.comete.swing.image.ijviewer.components.IStatisticsComponent;
import fr.soleil.comete.swing.image.ijviewer.components.LineProfileDialog;
import fr.soleil.comete.swing.image.ijviewer.components.RoiInfoTable;
import fr.soleil.comete.swing.image.ijviewer.components.StatisticsPanel;
import fr.soleil.comete.swing.image.ijviewer.events.IIJViewerListener;
import fr.soleil.comete.swing.image.ijviewer.events.IJViewerEvent;
import fr.soleil.comete.swing.image.ijviewer.events.IRoiManagerListener;
import fr.soleil.comete.swing.image.ijviewer.hidden.HiddenImageJ;
import fr.soleil.comete.swing.image.ijviewer.hidden.HiddenImageWindow;
import fr.soleil.comete.swing.image.util.ImageScreenPositionCalculator;
import fr.soleil.comete.swing.image.util.JGradientEditor;
import fr.soleil.comete.swing.image.util.JGradientViewer;
import fr.soleil.comete.swing.image.util.NumberImageConverter;
import fr.soleil.comete.swing.image.util.ij.RoiAndPluginUtil;
import fr.soleil.comete.swing.image.util.ij.roi.DualProfileRoiGenerator;
import fr.soleil.comete.swing.image.util.ij.roi.Focus595RoiGenerator;
import fr.soleil.comete.swing.image.util.ij.roi.HistogramRoiGenerator;
import fr.soleil.comete.swing.image.util.ij.roi.LineProfileRoiGenerator;
import fr.soleil.comete.swing.image.util.ij.roi.MaskManager;
import fr.soleil.comete.swing.image.util.ij.roi.RoiGenerator;
import fr.soleil.comete.swing.image.util.ij.roi.SelectionRoiGenerator;
import fr.soleil.comete.swing.image.util.ij.roi.ZoomRoiGenerator;
import fr.soleil.comete.swing.util.CfFileReader;
import fr.soleil.comete.swing.util.ClassListCellRenderer;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.ImageTool;
import fr.soleil.comete.swing.util.TextImageExporter;
import fr.soleil.comete.swing.util.mask.MaskController;
import fr.soleil.comete.swing.util.mask.MatrixMaskUndoableEdit;
import fr.soleil.comete.swing.util.mask.sector.MathematicSectorEditor;
import fr.soleil.data.adapter.NumberArrayToBooleanArrayAdapter;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.FlatMatrix;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.information.MatrixInformation;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.ColorUtils;
import fr.soleil.lib.project.awt.FontUtils;
import fr.soleil.lib.project.awt.IDataComponent;
import fr.soleil.lib.project.awt.listener.DataComponentDelegate;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.SnapshotUtil;
import fr.soleil.lib.project.swing.file.MultiExtFileFilter;
import fr.soleil.lib.project.swing.icons.DecorableIcon;
import fr.soleil.lib.project.swing.icons.Icons;
import fr.soleil.lib.project.swing.print.ComponentPrinter;
import fr.soleil.lib.project.swing.text.StateFocusTextField;
import fr.soleil.lib.project.swing.ui.ColoredSplitPaneUI;
import ij.IJ;
import ij.ImageJ;
import ij.gui.ImageCanvas;
import ij.gui.ImageWindow;
import ij.gui.Line;
import ij.gui.OvalRoi;
import ij.gui.PointRoi;
import ij.gui.Roi;
import ij.gui.TextRoi;
import ij.gui.Toolbar;
import ij.process.ColorProcessor;

/**
 * This class is the renderer for the ImageJ canvas. It is responsible for converting the image to
 * IJ comprehensive format, drawing the axis. It also provides buttons for interacting with the IJ's
 * canvas, and other widgets such as profiles. The panel grab the focus when the mouse is enters,
 * and set it back to the previous component when mouse exits.
 * <p>
 * Commands:
 * <ul>
 * <li>[C] key + left press/drag -> set the crosshair point</li>
 * <li>[C] key + right click -> clear the crosshair point</li>
 * </ul>
 * </p>
 * <p>
 * autoscale property and range values for axis are not the one user can get from axis. These are only used for display.
 * The right properties to use are embeded by the viewer
 * </p>
 * <p>
 * Known limitations :
 * <ul>
 * <li>We don't use Escape key to go back to selection mode, it introduces some side effects as we could be dragging
 * mouse while pressing the key.</li>
 * <li>Point ROI originally allows multipoints. It uses the previous ROI in IJ single ROI environment. We loose this
 * feature since we do not care about the previous ROI. By default, we don't manage Point ROIs</li>
 * <li>Two identical ROIs at the same place is not well managed by IJ, when comparing them it considers there is only
 * one. It is visible on coordinates.</li>
 * </ul>
 * </p>
 * 
 * @author MAINGUY
 * @author Rapha&euml;l GIRARDOT
 */
public class ImageViewer extends AbstractPanel implements IDataComponent, IMaskedImageViewer, IImageViewer, MaskHandler,
        MouseWheelListener, MouseListener, MouseMotionListener, KeyListener, PropertyChangeListener,
        ITableListener<Object>, MathematicSectorListener, IValueConvertorListener, SwingConstants {

    /*
     * - XXX: There is a bug when subtracting a composite ROI from another surrounding it. Undo action puts a composite ROI (0, 0, 0, 0)
     *
     * - TODO canvas is created twice: once on construction, other on setImage. This does not look good conceptually speaking, but there was no way to change this. Nothing must be done on canvas before setting an image
     *
     * - TODO If a window comes above the viewer, cursor coordinates may not be up to date anymore once viewer displayed again (cursor is outside, but previous position coordinates are still displayed)
     *
     * - TODO User can modify axis range from the outside (by accessing the axis properties), so we have to update the display.
     */

    private static final long serialVersionUID = 8618802407427987897L;

    /**
     * The default {@link Color} used to draw {@link Mask}s
     */
    public static final Color DEFAULT_MASK_COLOR = Color.WHITE;
    /**
     * The default {@link Color} used to draw {@link MathematicSector}s that are reported as
     * validated
     */
    public static final Color DEFAULT_SECTOR_VALIDATED_COLOR = new Color(220, 255, 255, 200);
    /**
     * The default {@link Color} used to draw not yet validated {@link MathematicSector}s
     */
    public static final Color DEFAULT_SECTOR_PREVIEW_COLOR = new Color(250, 255, 255, 150);
    /**
     * The default String used as title of the Dialog to choose a {@link Mask} {@link Color}
     */
    protected static final String DEFAULT_CHOOSE_MASK_COLOR_TITLE = "Choose a mask Color...";
    /**
     * The default String used as title of the Dialog to choose the beam center {@link Color}
     */
    protected static final String DEFAULT_CHOOSE_BEAM_COLOR_TITLE = "Choose a beam center Color...";
    /**
     * The default String used as title of the Dialog to choose a {@link Roi} {@link Color}
     */
    protected static final String DEFAULT_CHOOSE_ROI_COLOR_TITLE = "Choose a Roi Color...";
    /**
     * The default String used as title of the Dialog to choose a {@link Roi} Selection {@link Color}
     */
    protected static final String DEFAULT_CHOOSE_SELECTED_ROI_COLOR_TITLE = "Choose a Roi Selection Color...";
    /**
     * The default String used as title of the Dialog to choose a {@link Roi} Hovered {@link Color}
     */
    protected static final String DEFAULT_CHOOSE_HOVERED_ROI_COLOR_TITLE = "Choose a Roi Hovered Color...";
    /**
     * The default String used as title of the Dialog to choose an Inner {@link Roi} {@link Color}
     */
    protected static final String DEFAULT_CHOOSE_INNER_ROI_COLOR_TITLE = "Choose an Inner Roi Color...";
    /**
     * The default String used as title of the Dialog to choose an Inner {@link Roi} Selection {@link Color}
     */
    protected static final String DEFAULT_CHOOSE_SELECTED_INNER_ROI_COLOR_TITLE = "Choose an Inner Roi Selection Color...";
    /**
     * The default String used as title of the Dialog to choose an Inner {@link Roi} Hovered {@link Color}
     */
    protected static final String DEFAULT_CHOOSE_HOVERED_INNER_ROI_COLOR_TITLE = "Choose an Inner Roi Hovered Color...";
    /**
     * The default String used as title of the Dialog to choose an Outer {@link Roi} {@link Color}
     */
    protected static final String DEFAULT_CHOOSE_OUTER_ROI_COLOR_TITLE = "Choose an Outer Roi Color...";
    /**
     * The default String used as title of the Dialog to choose an Outer {@link Roi} Selection {@link Color}
     */
    protected static final String DEFAULT_CHOOSE_SELECTED_OUTER_ROI_COLOR_TITLE = "Choose an Outer Roi Selection Color...";
    /**
     * The default String used as title of the Dialog to choose an Outer {@link Roi} Hovered {@link Color}
     */
    protected static final String DEFAULT_CHOOSE_HOVERED_OUTER_ROI_COLOR_TITLE = "Choose an Outer Roi Hovered Color...";
    /**
     * The default String used as title of the Dialog displayed in case of problems with gradient
     * fit bounds
     */
    protected static final String DEFAULT_FIT_BOUNDS_ERROR_TITLE = "Error!";
    /**
     * The default String used as text of the Dialog displayed in case of problems with gradient fit
     * bounds
     */
    protected static final String DEFAULT_FIT_BOUNDS_ERROR_TEXT = "maximum best fit value is lower or equal than minimum!";

    /**
     * Default format to use for axis labels
     */
    protected static final String DEFAULT_LABEL_FORMAT = "%.2f";

    public static final Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 11);
    public static final Font TITLE_FONT = new Font("Dialog", Font.BOLD, 12);

    protected static final int DEFAULT_MIN_FIT = 0;
    protected static final int DEFAULT_MAX_FIT = 65535;
    protected static final int GRADIENT_WIDTH = 100;

    protected static final ResourceBundle MESSAGES = ResourceBundle
            .getBundle("fr.soleil.comete.swing.image.ijviewer.messages");
    protected static final Icons ICONS = new Icons("fr.soleil.comete.swing.image.ijviewer.icons");

    // Icons
    public static final ImageIcon RECTANGLE_ROI_ICON = ICONS.getIcon("ImageJViewer.Action.RectangleMode");
    public static final ImageIcon ANGLE_ROI_ICON = ICONS.getIcon("ImageJViewer.Action.Angle");
    public static final ImageIcon ELLIPSE_ROI_ICON = ICONS.getIcon("ImageJViewer.Action.EllipseMode");
    public static final ImageIcon POLYGON_ROI_ICON = ICONS.getIcon("ImageJViewer.Action.PolygonMode");
    public static final ImageIcon FREE_ROI_ICON = ICONS.getIcon("ImageJViewer.Action.FreeHandMode");
    public static final ImageIcon LINE_ROI_ICON = ICONS.getIcon("ImageJViewer.Action.LineMode");
    public static final ImageIcon POINT_ROI_ICON = ICONS.getIcon("ImageJViewer.Action.PointMode");
    public static final ImageIcon WAND_ROI_ICON = ICONS.getIcon("ImageJViewer.Action.WandMode");
    public static final ImageIcon PAN_MODE_ICON = ICONS.getIcon("ImageJViewer.Action.PanMode");
    public static final ImageIcon SELECTION_MODE_ICON = ICONS.getIcon("ImageJViewer.Action.SelectionMode");
    public static final ImageIcon ZOOM_MODE_ICON = ICONS.getIcon("ImageJViewer.Action.ZoomMode");
    public static final ImageIcon ZOOM_MODE2_ICON = ICONS.getIcon("ImageJViewer.Action.ZoomMode2");
    public static final ImageIcon FOCUS_MODE_ICON = ICONS.getIcon("ImageJViewer.Action.ContrastRoi");
    public static final ImageIcon CONTRAST_DEFAULT = ICONS.getIcon("ImageJViewer.Action.ContrastRoi");
    public static final DecorableIcon CONTRAST_5_95 = ICONS.getDecorableIcon("ImageJViewer.Action.ContrastRoi",
            "ImageJViewer.Decoration.5-95");
    public static final DecorableIcon CONTRAST_STDDEV = ICONS.getDecorableIcon("ImageJViewer.Action.ContrastRoi",
            "ImageJViewer.Decoration.Sigma10");
    public static final ImageIcon LINE_PROFILE_MODE_ICON = ICONS.getIcon("ImageJViewer.Action.LineProfile");
    public static final ImageIcon LINE_PROFILE_MODE2_ICON = ICONS.getIcon("ImageJViewer.Action.LineProfile2");
    public static final ImageIcon DUAL_PROFILE_ICON = ICONS.getIcon("ImageJViewer.Action.DualProfile");
    public static final ImageIcon HISTOGRAM_MODE_ICON = ICONS.getIcon("ImageJViewer.Action.Histogram");
    public static final ImageIcon CALIBRATION_MODE_ICON = ICONS.getIcon("ImageJViewer.Action.Calibration");
    public static final ImageIcon DELETE_SELECTED_ROIS_ICON = ICONS.getIcon("ImageJViewer.Action.DeleteSelectedRois");
    public static final ImageIcon DELETE_ALL_ROIS_ICON = ICONS.getIcon("ImageJViewer.Action.DeleteAllRois");
    public static final ImageIcon ZOOM_IN_ICON = ICONS.getIcon("ImageJViewer.Action.Zoom.In");
    public static final ImageIcon ZOOM_OUT_ICON = ICONS.getIcon("ImageJViewer.Action.Zoom.Out");
    public static final ImageIcon OR_SELECTED_ROIS_ICON = ICONS.getIcon("ImageJViewer.Action.OrRois");
    public static final ImageIcon AND_SELECTED_ROIS_ICON = ICONS.getIcon("ImageJViewer.Action.AndRois");
    public static final ImageIcon NOT_SELECTED_ROIS_ICON = ICONS.getIcon("ImageJViewer.Action.NotRois");
    public static final ImageIcon XOR_SELECTED_ROIS_ICON = ICONS.getIcon("ImageJViewer.Action.XorRois");
    public static final ImageIcon UNDO_ICON = ICONS.getIcon("ImageJViewer.Action.Undo");
    public static final ImageIcon REDO_ICON = ICONS.getIcon("ImageJViewer.Action.Redo");
    public static final ImageIcon SNAPSHOT_ICON = ICONS.getIcon("ImageJViewer.Action.SaveImage");
    public static final ImageIcon PRINT_ICON = ICONS.getIcon("ImageJViewer.Action.PrintImage");
    public static final ImageIcon FIT_MAX_SIZE_ICON = ICONS.getIcon("ImageJViewer.Action.FitMaxSize");
    public static final ImageIcon SHOW_TABLE_ICON = ICONS.getIcon("ImageJViewer.Action.ShowTable");
    public static final ImageIcon SAVE_DATA_ICON = ICONS.getIcon("ImageJViewer.Action.SaveData");
    public static final DecorableIcon LOAD_DATA_ICON = ICONS.getDecorableIcon("ImageJViewer.Action.SaveData",
            "ImageJViewer.Decoration.Load");
    public static final DecorableIcon SAVE_SETTINGS_ICON = ICONS.getDecorableIcon("ImageJViewer.Action.Settings",
            "ImageJViewer.Decoration.Save");
    public static final DecorableIcon LOAD_SETTINGS_ICON = ICONS.getDecorableIcon("ImageJViewer.Action.Settings",
            "ImageJViewer.Decoration.Load");
    public static final ImageIcon INNER_ROIS_ICON = ICONS.getIcon("ImageJViewer.Action.SetInnerRoi");
    public static final ImageIcon OUTER_ROIS_ICON = ICONS.getIcon("ImageJViewer.Action.SetOuterRoi");
    public static final ImageIcon INVALIDATE_ROIS_ICON = ICONS.getIcon("ImageJViewer.Action.InvalidateRoi");
    public static final ImageIcon SET_AS_MASK_ICON = ICONS.getIcon("ImageJViewer.Action.SetMask");
    public static final ImageIcon ADD_TO_MASK_ICON = ICONS.getIcon("ImageJViewer.Action.AddToMask");
    public static final ImageIcon REMOVE_FROM_MASK_ICON = ICONS.getIcon("ImageJViewer.Action.RemoveFromMask");
    public static final ImageIcon CLEAR_MASK_ICON = ICONS.getIcon("ImageJViewer.Action.ClearMask");
    public static final ImageIcon SAVE_MASK_ICON = ICONS.getIcon("ImageJViewer.Action.SaveMask");
    public static final ImageIcon LOAD_MASK_ICON = ICONS.getIcon("ImageJViewer.Action.LoadMask");
    public static final ImageIcon LOAD_MASK_ADD_ICON = ICONS.getIcon("ImageJViewer.Action.LoadMask.Add");
    public static final ImageIcon SET_MASK_COLOR_ICON = ICONS.getIcon("ImageJViewer.Action.SetMaskColor");
    public static final ImageIcon SET_BEAM_POSITION_COLOR_ICON = ICONS
            .getIcon("ImageJViewer.Action.SetBeamPositionColor");
    public static final ImageIcon PACK_TABLE_ICON = ICONS.getIcon("ImageJViewer.Action.PackTable");
    public static final ImageIcon UNZOOM_ICON = ICONS.getIcon("ImageJViewer.Action.UnZoom");
    public static final ImageIcon SET_ROI_COLORS_ICON = ICONS.getIcon("ImageJViewer.Action.SetRoisColors");
    public static final ImageIcon NEW_SECTOR_ICON = ICONS.getIcon("ImageJViewer.Action.NewSector");
    public static final ImageIcon DELETE_SECTOR_ICON = ICONS.getIcon("ImageJViewer.Action.DeleteSector");
    public static final ImageIcon EDIT_SECTOR_ICON = ICONS.getIcon("ImageJViewer.Action.EditSector");
    public static final ImageIcon PREVIEW_SECTOR_ICON = ICONS.getIcon("ImageJViewer.Action.PreviewSector");
    public static final ImageIcon APPLY_SECTOR_ICON = ICONS.getIcon("ImageJViewer.Action.ApplySector");
    public static final ImageIcon SET_SECTOR_COLORS_ICON = ICONS.getIcon("ImageJViewer.Action.SetSectorColor");
    public static final ImageIcon SHOW_CENTER_ICON = ICONS.getIcon("ImageJViewer.Action.DisplayBeamPosition");
    public static final ImageIcon CLICK_CENTER_ICON = ICONS.getIcon("ImageJViewer.Action.AllowBeamCenterByClickAction");
    public static final ImageIcon MOUSE_WHEEL_ZOOM_ICON = ICONS.getIcon("ImageJViewer.Action.Zoom.MouseWheel");

    static {
        CONTRAST_5_95.setDecorationHorizontalAlignment(SwingConstants.CENTER);
        CONTRAST_5_95.setDecorationVerticalAlignment(SwingConstants.BOTTOM);
        CONTRAST_5_95.setDecorated(true);
        CONTRAST_STDDEV.setDecorationHorizontalAlignment(SwingConstants.CENTER);
        CONTRAST_STDDEV.setDecorationVerticalAlignment(SwingConstants.BOTTOM);
        CONTRAST_STDDEV.setDecorated(true);
        LOAD_DATA_ICON.setDecorationHorizontalAlignment(SwingConstants.RIGHT);
        LOAD_DATA_ICON.setDecorationVerticalAlignment(SwingConstants.BOTTOM);
        LOAD_DATA_ICON.setDecorated(true);
        SAVE_SETTINGS_ICON.setDecorationHorizontalAlignment(SwingConstants.RIGHT);
        SAVE_SETTINGS_ICON.setDecorationVerticalAlignment(SwingConstants.BOTTOM);
        SAVE_SETTINGS_ICON.setDecorated(true);
        LOAD_SETTINGS_ICON.setDecorationHorizontalAlignment(SwingConstants.RIGHT);
        LOAD_SETTINGS_ICON.setDecorationVerticalAlignment(SwingConstants.BOTTOM);
        LOAD_SETTINGS_ICON.setDecorated(true);
    }

    // Zoom
    protected static final String ZOOM_TITLE = "Zoom: ";
    protected static final String UNKNOW_ZOOM = "??";

    // ROI creation modes + zoom + line profile + selection
    public static final String MODE_ANGLE_ACTION = "ModeAngleAction";
    public static final String MODE_RECTANGLE_ACTION = "ModeRectangleAction";
    public static final String MODE_ELLIPSE_ACTION = "ModeEllipseAction";
    public static final String MODE_POLYGON_ACTION = "ModePolygonAction";
    public static final String MODE_FREE_HAND_ACTION = "ModeFreeHandAction";
    public static final String MODE_LINE_ACTION = "ModeLineAction";
    public static final String MODE_POINT_ACTION = "ModePointAction";
    public static final String MODE_WAND_ACTION = "ModeWandAction";
    public static final String MODE_PAN_ACTION = "ModePanAction";
    public static final String MODE_SELECTION_ACTION = "ModeSelectionAction";
    public static final String MODE_FOCUS_ACTION = "ModeFocusAction";
    public static final String MODE_ZOOM_ACTION = "ModeZoomAction";
    public static final String MODE_LINE_PROFILE_ACTION = "ModeLineProfileAction";
    public static final String MODE_CALIBRATION_ACTION = "ModeCalibrationAction";
    public static final String MODE_HISTOGRAM_ACTION = "ModeHistogramAction";

    // Mouse Wheel Zoom Actions
    public static final String MOUSE_WHEEL_ZOOM_DEFAULT = "ModeWheelZoomDefault";
    public static final String MOUSE_WHEEL_ZOOM_CENTER = "ModeWheelZoomCenter";
    public static final String MOUSE_WHEEL_ZOOM_PIXEL = "ModeWheelZoomPixel";
    public static final String MOUSE_WHEEL_ZOOM_GROUP = "ModeWheelZoomGroup";

    // Other actions
    public static final String DUAL_PROFILE_ACTION = "DualProfileAction";
    public static final String ZOOM_IN_ACTION = "ZoomInAction";
    public static final String ZOOM_OUT_ACTION = "ZoomOutAction";
    public static final String UNZOOM_ACTION = "UnZoomAction";
    public static final String DELETE_SELECTED_ROIS_ACTION = "DeleteSelectedRoisAction";
    public static final String DELETE_ALL_ROIS_ACTION = "DeleteAllRoisAction";
    public static final String OR_SELECTED_ROIS_ACTION = "OrSelectedRoisAction";
    public static final String AND_SELECTED_ROIS_ACTION = "AndSelectedRoisAction";
    public static final String NOT_SELECTED_ROIS_ACTION = "NotSelectedRoisAction";
    public static final String XOR_SELECTED_ROIS_ACTION = "XorSelectedRoisAction";
    public static final String INNER_ROIS_ACTION = "InnerRoisAction";
    public static final String OUTER_ROIS_ACTION = "OuterRoisAction";
    public static final String INVALIDATE_ROIS_ACTION = "InvalidateRoisAction";
    public static final String ADD_TO_MASK_ACTION = "AddToMaskAction";
    public static final String REMOVE_FROM_MASK_ACTION = "RemoveFromMaskAction";
    public static final String SET_AS_MASK_ACTION = "SetAsMaskAction";
    public static final String CLEAR_MASK_ACTION = "ClearMaskAction";
    public static final String SAVE_MASK_ACTION = "SaveMaskAction";
    public static final String LOAD_MASK_ACTION = "LoadMaskAction";
    public static final String LOAD_MASK_ADD_ACTION = "LoadMaskAddAction";
    public static final String SET_MASK_COLOR_ACTION = "SetMaskColorAction";
    public static final String SET_BEAM_POSITION_COLOR_ACTION = "SetBeamPositionColorAction";
    public static final String UNDO_ACTION = "UndoAction";
    public static final String REDO_ACTION = "RedoAction";
    public static final String SNAPSHOT_ACTION = "SnapshotAction";
    public static final String PRINT_ACTION = "PrintAction";
    public static final String LOG_SCALE_ACTION = "LogScaleAction";
    public static final String FIT_MAX_SIZE_ACTION = "FitMaxAction";
    public static final String SET_AUTO_BEST_FIT_ACTION = "SetAutoBestAction";
    public static final String SHOW_TABLE_ACTION = "ShowTableAction";
    public static final String PACK_TABLE_ACTION = "PackTableAction";
    public static final String SAVE_DATA_ACTION = "SavaDataAction";
    public static final String LOAD_DATA_ACTION = "LoadDataAction";
    public static final String LOAD_DATA_VISIBLE_ACTION = "LoadDataVisibleAction";
    public static final String DISPLAY_ROI_COLORS_MENU_ACTION = "DisplayRoiColorsMenuAction";
    public static final String CHANGE_ROI_COLOR_ACTION = "ChangeRoiColorAction";
    public static final String CHANGE_SELECTED_ROI_COLOR_ACTION = "ChangeSelectedRoiColorAction";
    public static final String CHANGE_HOVERED_ROI_COLOR_ACTION = "ChangeHoveredRoiColorAction";
    public static final String CHANGE_INNER_ROI_COLOR_ACTION = "ChangeInnerRoiColorAction";
    public static final String CHANGE_SELECTED_INNER_ROI_COLOR_ACTION = "ChangeSelectedInnerRoiColorAction";
    public static final String CHANGE_HOVERED_INNER_ROI_COLOR_ACTION = "ChangeHoveredInnerRoiColorAction";
    public static final String CHANGE_OUTER_ROI_COLOR_ACTION = "ChangeOuterRoiColorAction";
    public static final String CHANGE_SELECTED_OUTER_ROI_COLOR_ACTION = "ChangeSelectedOuterRoiColorAction";
    public static final String CHANGE_HOVERED_OUTER_ROI_COLOR_ACTION = "ChangeHoveredOuterRoiColorAction";
    public static final String NEW_SECTOR_ACTION = "NewSectorAction";
    public static final String DELETE_SECTOR_ACTION = "DeleteSectorAction";
    public static final String EDIT_SECTOR_ACTION = "EditSectorAction";
    public static final String PREVIEW_SECTOR_ACTION = "PreviewSectorAction";
    public static final String DISPLAY_SECTOR_COLORS_MENU_ACTION = "DisplaySectorColorsMenuAction";
    public static final String CHANGE_EDITING_SECTOR_COLOR_ACTION = "ChangeEditingSectorColorAction";
    public static final String CHANGE_VALIDATED_SECTOR_COLOR_ACTION = "ChangeValidatedSectorColorAction";
    public static final String MASK_LOADED_PROPERTY = "MaskLoaded";
    public static final String MASK_LOADED_ADD_PROPERTY = "MaskLoadedAdd";
    public static final String VALID_SECTOR_CHANGED_PROPERTY = "ValidSectorChanged";
    public static final String DISPLAY_BEAM_CENTER_ACTION = "DisplayBeamCenterAction";
    public static final String ALLOW_BEAM_CENTER_BY_CLICK_ACTION = "AllowBeamCenterByClickAction";
    public static final String SAVE_SETTINGS_ACTION = "SaveSettings";
    public static final String LOAD_SETTINGS_ACTION = "LoadSettings";

    // actions based on IJ modes
    public static final String IJ_ACTION_GROUP = "ModeIJ";

    public static final int TYPE_POPUP_MENU = 0;
    public static final int TYPE_EXPORT_MENU = 1;
    public static final int TYPE_IMPORT_MENU = 2;
    public static final int TYPE_ACTION_MENU = 3;
    public static final int TYPE_COLOR_MENU = 4;
    public static final int TYPE_SELECTION_MENU = 5;
    public static final int TYPE_MOUSE_WHEEL_ZOOM_MENU = 6;

    public static final Color DEFAULT_CROSSHAIR_COLOR = new Color(255, 255, 0, 120);

    protected static final Color DEFAULT_BEAM_COLOR = Color.CYAN;
    protected static final Color DEFAULT_NAN_COLOR = Color.GRAY;
    protected static final Color DEFAULT_POSITIVE_INFINITY_COLOR = Color.GRAY;
    protected static final Color DEFAULT_NEGATIVE_INFINITY_COLOR = Color.GRAY;

    protected static final Color POPUP_MENU_PANEL_COLOR = new Color(0, 128, 192);

    // profiles
    public static final String LINE_PROFILE_NAME = MESSAGES.getString("ImageJViewer.Action.LineProfileMode.Text");
    public static final String HORIZONTAL_PROFILE_NAME = MESSAGES
            .getString("ImageJViewer.Action.DualProfile.Horizontal");
    public static final String VERTICAL_PROFILE_NAME = MESSAGES.getString("ImageJViewer.Action.DualProfile.Vertical");
    public static final String CROSSHAIR_NAME = MESSAGES.getString("ImageJViewer.Description.Crosshair.Text");
    public static final String DUAL_PROFILE_NAME = MESSAGES.getString("ImageJViewer.Action.DualProfile.Text");

    // histogram
    public static final String HISTOGRAM_NAME = MESSAGES.getString("ImageJViewer.Action.HistogramMode.Text");

    // properties
    public static final String FORMAT = "format";
    public static final String IMAGE_NAME = "imageName";
    public static final String X_AXIS_FORMAT = "xAxisFormat";
    public static final String Z_AXIS_FORMAT = "zAxisFormat";
    public static final String HISTOGRAM_MIN = "histogramMin";
    public static final String HISTOGRAM_MAX = "histogramMax";
    public static final String HISTOGRAM_STEP = "histogramStep";
    public static final String HISTOGRAM_AUTO_STEP = "histogramAutoStep";
    public static final String HISTOGRAM_AUTO_SCALE = "histogramAutoScale";

    // value separator
    protected static final String DEFAULT_VALUE_SEPARATOR = " ";

    // vertical and horizontal profilers reference widht/height
    protected static final int PROFILER_MIN_SIZE = 80;
    protected static final int PROFILER_MAX_SIZE = 200;
    protected static final int PROFILER_SIZE_STEP = 10;
    protected int currentProfilerSize;

    // selection
    protected int selectedX, selectedY;
    protected double selectedAxisX, selectedAxisY;
    protected double selectedValue;

    // colors
    protected Color maskColor;
    protected Color roiColor;
    protected Color hoveredRoiColor;
    protected Color selectedRoiColor;
    protected Color innerRoiColor;
    protected Color hoveredInnerRoiColor;
    protected Color selectedInnerRoiColor;
    protected Color outerRoiColor;
    protected Color hoveredOuterRoiColor;
    protected Color selectedOuterRoiColor;
    protected Color sectorValidatedColor;
    protected Color sectorPreviewColor;
    protected Color nanColor;
    protected Color positiveInfinityColor;
    protected Color negativeInfinityColor;

    protected BufferedImage theImage;

    // the canvas to paint
    protected IJCanvas imageCanvas;
    protected volatile int[] roiNameAlignment;
    protected ImagePanel imagePanel;
    protected JScrollPane imageScrollPane;
    protected JPanel imageView;
    protected IJRoiManager imagePlus;

    // ----------
    // Axis stuff
    // ----------
    protected HorizontalAxisView xAxis;
    protected VerticalAxisView yAxis;
    protected Rectangle xTitleBounds, yTitleBounds;
    protected Color axisColor;
    protected int axisLineWidth;
    protected BasicStroke axisStroke;
    protected boolean xAutoScale;
    protected boolean yAutoScale;

    // canvas offset
    protected int upMargin;
    protected int downMargin;
    protected int leftMargin;
    protected int rightMargin;

    // this is equal to leftMargin
    protected int xOrigine;
    // this is equal to upMargin
    protected int yOrigine;

    protected JPopupMenu roiColorsPopupMenu;
    protected JPopupMenu sectorColorsPopupMenu;

    protected JDialog fitDialog;
    protected JPanel fitSettingsPanel;
    protected JLabel minFitLabel;
    protected JFormattedTextField minFitTextField;
    protected JLabel maxFitLabel;
    protected JFormattedTextField maxFitTextField;
    protected JButton applyFitButton;
    protected JGradientEditor gradientEditor;

    protected String chooseRoiColorTitle;
    protected String chooseSelectedRoiColorTitle;
    protected String chooseHoveredRoiColorTitle;
    protected String chooseInnerRoiColorTitle;
    protected String chooseSelectedInnerRoiColorTitle;
    protected String chooseHoveredInnerRoiColorTitle;
    protected String chooseOuterRoiColorTitle;
    protected String chooseSelectedOuterRoiColorTitle;
    protected String chooseHoveredOuterRoiColorTitle;
    protected String fitBoundsErrorTitle;
    protected String fitBoundsErrorText;

    protected Mask mask;
    protected MaskController maskController;
    protected JFileChooser maskFileChooser;

    protected ArrayList<WeakReference<MaskListener>> maskListeners;
    protected String chooseMaskColorTitle;
    protected String chooseBeamPositionColorTitle;

    // used for button creation
    protected ActionContainerFactory containerFactory;

    protected JToolBar toolBar;
    protected JScrollPane toolBarScrollPane;

    protected FlatMatrix<Object> loadMatrix;
    protected FlatMatrix<Object> originalMatrix;
    protected Object value;
    protected int dimX;
    protected int dimY;

    // the gradient
    protected JGradientViewer gradientViewer;
    protected Gradient gradient;
    protected int[] gColormap;

    protected NumberImageConverter converter;
    protected boolean autoBestFit;
    protected boolean useImageConverterMinimumInLogScale;

    // user values
    protected double fitMin;
    protected double fitMax;

    // Beam position, defined as a real axis position
    protected boolean beamPointEnabled;
    protected boolean drawBeamPosition;
    protected boolean allowBeamPositionByClick;
    protected double[] beamPoint;
    protected Color beamColor;

    // Undo
    protected MyUndoManager undoManager;

    protected JFileChooser fileChooser;

    protected JPopupMenu popupMenu;
    protected JMenu formatMenu;
    protected JMenu exportMenu;
    protected JMenu importMenu;
    protected JMenu actionMenu;
    protected JMenu colorMenu;
    protected JMenu selectionMenu;
    protected JMenu mouseWheelZoomMenu;

    // -------------
    // Profile stuff
    // -------------
    protected boolean stopDualProfileAtCursorPosition;
    protected boolean horizontalDataForVerticalProfiler;
    protected Chart verticalProfiler;
    protected Chart horizontalProfiler;
    protected JPanel verticalProfilerPanel;
    protected JPanel horizontalProfilerPanel;
    protected JPanel profilerSizeButtonPanel;
    protected JPanel biggerProfilerButtonPanel;
    protected JButton biggerProfilerButton;
    protected JPanel smallerProfilerButtonPanel;
    protected JButton smallerProfilerButton;

    // ---------------
    // Histogram stuff
    // ---------------
    protected HistogramDialog histogramDialog;
    protected double histoMin, histoMax, histoStep;
    protected boolean histoAutoScale, histoAutoStep;

    protected JDialog matrixDialog;
    protected NumberTable matrixTable;

    protected Chart lineProfiler;
    protected LineProfileDialog lineProfilerDialog;

    protected CalibrationDialog calibrationDialog;
    protected double pixelSize;

    protected boolean saveColumnIndex;
    protected String valueSeparator;
    protected boolean loadDataVisible;

    protected AbstractActionExt loadDataVisibleCheckBox;

    protected IJRoiPermission roiPermission;

    protected boolean autoZoom;
    protected boolean logScale;
    protected MouseZoomMode mouseZoomMode;

    protected boolean useMaskManagement;
    protected boolean useSectorManagement;

    // The "viewerId" is used to differenciate 2 intances of IJImage, especially
    // their Actions
    protected String viewerId;

    // The "applicationId" is used to interact with ImageJManager, to be sure to always have the desired activated
    // ImageViewer (the one that shall be ImageJ macros' target)
    private String applicationId;

    // The "parent" JScrollPane is used to hack maximum displayable size
    protected boolean alwaysFitMaxSize;
    protected boolean limitDrawingZone;
    protected Dimension refSize;
    protected final Semaphore refSizeSema;

    // Axes converion
    protected IValueConvertor xAxisConvertor;
    protected IValueConvertor yAxisConvertor;

    // Mathematic Sectors
    protected MathematicSector sector;
    protected Mask sectorMask;
    protected boolean previewSector;
    protected final ArrayList<Class<?>> possibleSectors;

    protected ImageScreenPositionCalculator imageScreenPositionCalculator;

    protected final List<IImageViewerListener> listeners;
    protected ARoiInfoTable roiInfoTable;
    protected JScrollPane roiInfoTableScrollPane;
    protected JPanel infoPanel;
    protected JScrollPane infoScrollPane;
    protected JSplitPane southSplitPane;
    protected JLabel nameLabel;
    protected JLabel coordsLabel;
    protected JLabel zoomLabel;
    protected JComponent statisticsComponent;
    protected JCheckBox showStatisticsCheckBox;
    protected boolean cleanOnDataSetting;

    protected final Map<String, Roi> roiMap;

    protected boolean editable;
    protected volatile boolean manageActionsAvailability;

    // settings
    protected String lastConfig;

    // data import/export
    protected final List<IImageExporter> imageExporters;
    protected final TextImageExporter textImageExporter;
    protected final TransferEventDelegate transferEventDelegate;

    protected boolean paintAllowed;
    protected String centerName;
    protected String imageName;

    // snapshot management
    protected final SnapshotEventDelegate snapshotEventDelegate;
    protected String preferredSnapshotExtension;

    protected final NumberArrayToBooleanArrayAdapter<Byte, byte[], boolean[]> arrayAdapter;

    // Mask manager
    protected MaskManager maskManager;

    // Limit changes when not displayed
    protected final DataComponentDelegate dataComponentDelegate;
    protected volatile boolean shouldAutoResize;

    public ImageViewer() {
        this(null);
    }

    public ImageViewer(IJRoiManager imgp) {
        super();
        generateViewerId();

        dataComponentDelegate = new DataComponentDelegate();
        dataComponentDelegate.setupAndListenToComponent(this);
        dataComponentDelegate.setUpdateOnDataChanged(this, true);
        shouldAutoResize = false;

        setMaskManager(null);

        arrayAdapter = new NumberArrayToBooleanArrayAdapter<Byte, byte[], boolean[]>(Byte.TYPE, Boolean.TYPE);
        roiNameAlignment = null;
        applicationId = null;

        listeners = new ArrayList<IImageViewerListener>();
        imagePlus = (imgp == null ? new IJRoiManager() : imgp);
        imagePlus.setImageViewer(this);
        cleanOnDataSetting = true;
        roiMap = new ConcurrentHashMap<String, Roi>();

        currentProfilerSize = PROFILER_MIN_SIZE;
        selectedX = selectedY = -1;
        selectedAxisX = selectedAxisY = Double.NaN;
        selectedValue = Double.NaN;
        theImage = null;
        imageCanvas = null;
        xAxis = null;
        yAxis = null;
        axisColor = Color.BLACK;
        axisLineWidth = 1;
        axisStroke = null;
        xAutoScale = true;
        yAutoScale = true;
        upMargin = 0;
        downMargin = 0;
        leftMargin = 0;
        rightMargin = 0;
        xOrigine = 0;
        yOrigine = 0;
        containerFactory = new ActionContainerFactory();
        loadMatrix = null;
        originalMatrix = null;
        value = null;
        dimX = 0;
        dimY = 0;
        converter = null;
        drawBeamPosition = false;
        beamPointEnabled = true;
        allowBeamPositionByClick = false;
        beamPoint = null;
        beamColor = DEFAULT_BEAM_COLOR;
        stopDualProfileAtCursorPosition = false;
        verticalProfiler = null;
        horizontalDataForVerticalProfiler = true;
        horizontalProfiler = null;
        verticalProfilerPanel = null;
        horizontalProfilerPanel = null;
        biggerProfilerButtonPanel = null;
        smallerProfilerButtonPanel = null;
        profilerSizeButtonPanel = null;
        biggerProfilerButton = null;
        smallerProfilerButton = null;
        lineProfiler = null;
        histogramDialog = null;
        histoMin = 0;
        histoMax = 65536;
        histoStep = 1;
        histoAutoScale = true;
        histoAutoStep = true;
        lineProfilerDialog = null;
        calibrationDialog = null;
        pixelSize = 1;
        saveColumnIndex = false;
        valueSeparator = DEFAULT_VALUE_SEPARATOR;
        loadDataVisible = false;
        loadDataVisibleCheckBox = null;
        roiPermission = null;
        alwaysFitMaxSize = false;
        limitDrawingZone = false;
        refSize = null;
        refSizeSema = new Semaphore(1);
        xAxisConvertor = null;
        yAxisConvertor = null;
        xTitleBounds = new Rectangle();
        yTitleBounds = new Rectangle();
        sector = null;
        sectorMask = null;
        previewSector = false;
        possibleSectors = new ArrayList<Class<?>>();
        mouseZoomMode = MouseZoomMode.PIXEL;

        maskColor = DEFAULT_MASK_COLOR;
        sectorValidatedColor = DEFAULT_SECTOR_VALIDATED_COLOR;
        sectorPreviewColor = DEFAULT_SECTOR_PREVIEW_COLOR;
        chooseBeamPositionColorTitle = DEFAULT_CHOOSE_BEAM_COLOR_TITLE;
        chooseMaskColorTitle = DEFAULT_CHOOSE_MASK_COLOR_TITLE;
        chooseRoiColorTitle = DEFAULT_CHOOSE_ROI_COLOR_TITLE;
        chooseSelectedRoiColorTitle = DEFAULT_CHOOSE_SELECTED_ROI_COLOR_TITLE;
        chooseHoveredRoiColorTitle = DEFAULT_CHOOSE_HOVERED_ROI_COLOR_TITLE;
        chooseInnerRoiColorTitle = DEFAULT_CHOOSE_INNER_ROI_COLOR_TITLE;
        chooseSelectedInnerRoiColorTitle = DEFAULT_CHOOSE_SELECTED_INNER_ROI_COLOR_TITLE;
        chooseHoveredInnerRoiColorTitle = DEFAULT_CHOOSE_HOVERED_INNER_ROI_COLOR_TITLE;
        chooseOuterRoiColorTitle = DEFAULT_CHOOSE_OUTER_ROI_COLOR_TITLE;
        chooseSelectedOuterRoiColorTitle = DEFAULT_CHOOSE_SELECTED_OUTER_ROI_COLOR_TITLE;
        chooseHoveredOuterRoiColorTitle = DEFAULT_CHOOSE_HOVERED_OUTER_ROI_COLOR_TITLE;
        fitBoundsErrorText = DEFAULT_FIT_BOUNDS_ERROR_TEXT;
        fitBoundsErrorTitle = DEFAULT_FIT_BOUNDS_ERROR_TITLE;
        nanColor = DEFAULT_NAN_COLOR;
        positiveInfinityColor = DEFAULT_POSITIVE_INFINITY_COLOR;
        negativeInfinityColor = DEFAULT_NEGATIVE_INFINITY_COLOR;

        useMaskManagement = false;

        lastConfig = ObjectUtils.EMPTY_STRING;

        matrixTable = new NumberTable();
        matrixTable.setPackAllOnInit(false);
        matrixTable.addTableListener(this);
        editable = matrixTable.isEditable();

        maskListeners = new ArrayList<WeakReference<MaskListener>>();
        maskController = new MaskController();
        maskController.setParentComponent(this);

        autoBestFit = true;
        fitMin = DEFAULT_MIN_FIT;
        fitMax = DEFAULT_MAX_FIT;
        useImageConverterMinimumInLogScale = false;

        manageActionsAvailability = true;

        imageExporters = new ArrayList<IImageExporter>();
        textImageExporter = new TextImageExporter();
        imageExporters.add(textImageExporter);
        transferEventDelegate = new TransferEventDelegate(this, ChartUtils.DOT);

        paintAllowed = true;

        snapshotEventDelegate = new SnapshotEventDelegate(this, null);
        preferredSnapshotExtension = null;

        centerName = MESSAGES.getString("ImageJViewer.BeamCenter");
        imageName = ObjectUtils.EMPTY_STRING;

        initLayout();
        initComponents();
        initPopupMenu();
        registerRoiGenerators();
        initToolBar();
        initToolBarScrollPane();

        roiColor = getImageCanvas().getRoiColor();
        hoveredRoiColor = getImageCanvas().getHoveredRoiColor();
        selectedRoiColor = getImageCanvas().getSelectedRoiColor();
        innerRoiColor = getImageCanvas().getInnerRoiColor();
        hoveredInnerRoiColor = getImageCanvas().getHoveredInnerRoiColor();
        selectedInnerRoiColor = getImageCanvas().getSelectedInnerRoiColor();
        outerRoiColor = getImageCanvas().getOuterRoiColor();
        hoveredOuterRoiColor = getImageCanvas().getHoveredOuterRoiColor();
        selectedOuterRoiColor = getImageCanvas().getSelectedOuterRoiColor();
        beamColor = getImageCanvas().getBeamColor();

        addComponents();

        setStatisticsVisible(isStatisticsVisible());

        updateTooltip();
    }

    public void adjustToCanvas() {
        IJCanvas imageCanvas = this.imageCanvas;
        if (imageCanvas != null) {
            // Sometimes, on zoom change, imageCanvas rectangle goes crazy.
            // Here, we try to update it in such cases
            int maxWidth = dimX;
            int maxHeight = dimY;
            if ((maxWidth > 0) && (maxHeight > 0)) {
                Rectangle srcRect = imageCanvas.getSrcRect();
                if (srcRect != null) {
                    boolean changed = false;
                    int x = srcRect.x, y = srcRect.y, width = srcRect.width, height = srcRect.height;
                    if (x < 0) {
                        x = 0;
                        changed = true;
                    }
                    if (x >= maxWidth) {
                        x = maxWidth - 1;
                        changed = true;
                    }
                    if (y < 0) {
                        y = 0;
                        changed = true;
                    }
                    if (y >= maxHeight) {
                        y = maxHeight - 1;
                        changed = true;
                    }
                    if (x + width > maxWidth) {
                        width -= (x + width - maxWidth);
                        changed = true;
                    }
                    if (y + height > maxHeight) {
                        height -= y + height - maxHeight;
                        changed = true;
                    }
                    if (changed) {
                        imageCanvas.setSourceRect(new Rectangle(x, y, width, height));
                    }
                }
            }
        }
        imagePanel.revalidate();
        imageScrollPane.revalidate();
        repaint();
    }

    protected void registerRoiGenerators() {
        registerRoiGenerator(
                new SelectionRoiGenerator(null, MESSAGES.getString("ImageJViewer.Action.SelectionMode.Tooltip")));
        registerRoiGenerator(new ZoomRoiGenerator(null, MESSAGES.getString("ImageJViewer.Action.ZoomMode.Tooltip")));
        registerRoiGenerator(
                new Focus595RoiGenerator(null, MESSAGES.getString("ImageJViewer.Action.Contrast.5-95.Tooltip")));
        registerRoiGenerator(new LineProfileRoiGenerator(MESSAGES.getString("ImageJViewer.Action.LineProfileMode.Text"),
                MESSAGES.getString("ImageJViewer.Action.LineProfileMode.Tooltip"), this));
        registerRoiGenerator(new DualProfileRoiGenerator(MESSAGES.getString("ImageJViewer.Action.DualProfile.Text"),
                MESSAGES.getString("ImageJViewer.Action.DualProfile.Text"), this));
        registerRoiGenerator(new HistogramRoiGenerator(HISTOGRAM_NAME, HISTOGRAM_NAME, this));
    }

    public void registerRoiGenerator(RoiGenerator generator) {
        if (generator != null) {
            getRoiManager().addRoiGenerator(generator);
            AbstractActionExt action = generator.getRoiModeAction();
            if (action != null) {
                int menu = generator.getRoiModeMenu();
                int index = generator.getPreferredPositionInMenu();
                switch (menu) {
                    case TYPE_POPUP_MENU:
                        if ((index > -1) && (index < popupMenu.getComponentCount())) {
                            popupMenu.insert(containerFactory.createMenuItem(action), index);
                        } else {
                            popupMenu.add(containerFactory.createMenuItem(action));
                        }
                        break;
                    case TYPE_IMPORT_MENU:
                        if ((index > -1) && (index < importMenu.getComponentCount())) {
                            importMenu.insert(containerFactory.createMenuItem(action), index);
                        } else {
                            importMenu.add(containerFactory.createMenuItem(action));
                        }
                        break;
                    case TYPE_ACTION_MENU:
                        if ((index > -1) && (index < exportMenu.getComponentCount())) {
                            exportMenu.insert(containerFactory.createMenuItem(action), index);
                        } else {
                            exportMenu.add(containerFactory.createMenuItem(action));
                        }
                        break;
                    case TYPE_COLOR_MENU:
                        if ((index > -1) && (index < colorMenu.getComponentCount())) {
                            colorMenu.insert(containerFactory.createMenuItem(action), index);
                        } else {
                            colorMenu.add(containerFactory.createMenuItem(action));
                        }
                        break;
                    case TYPE_SELECTION_MENU:
                        if ((index > -1) && (index < selectionMenu.getComponentCount())) {
                            selectionMenu.insert(containerFactory.createMenuItem(action), index);
                        } else {
                            selectionMenu.add(containerFactory.createMenuItem(action));
                        }
                        break;
                    case TYPE_MOUSE_WHEEL_ZOOM_MENU:
                        if ((index > -1) && (index < mouseWheelZoomMenu.getComponentCount())) {
                            mouseWheelZoomMenu.insert(containerFactory.createMenuItem(action), index);
                        } else {
                            mouseWheelZoomMenu.add(containerFactory.createMenuItem(action));
                        }
                        break;
                }
            }
        }
    }

    public void unregisterRoiGenerator(RoiGenerator generator) {
        if (generator != null) {
            getRoiManager().removeRoiGenerator(generator);
            int menu = generator.getRoiModeMenu();
            switch (menu) {
                case TYPE_POPUP_MENU:
                    removeAction(generator.getRoiModeAction(), popupMenu);
                    break;
                case TYPE_IMPORT_MENU:
                    removeAction(generator.getRoiModeAction(), importMenu);
                    break;
                case TYPE_ACTION_MENU:
                    removeAction(generator.getRoiModeAction(), exportMenu);
                    break;
                case TYPE_COLOR_MENU:
                    removeAction(generator.getRoiModeAction(), colorMenu);
                    break;
                case TYPE_SELECTION_MENU:
                    removeAction(generator.getRoiModeAction(), selectionMenu);
                    break;
                case TYPE_MOUSE_WHEEL_ZOOM_MENU:
                    removeAction(generator.getRoiModeAction(), mouseWheelZoomMenu);
                    break;
            }
        }
    }

    protected void removeAction(Action action, Container parent) {
        if ((action != null) && (parent != null)) {
            Component toRemove = null;
            for (Component comp : parent.getComponents()) {
                if ((comp instanceof AbstractButton) && action.equals(((AbstractButton) comp).getAction())) {
                    toRemove = comp;
                    break;
                }
            }
            if (toRemove != null) {
                parent.remove(toRemove);
            }
        }
    }

    /**
     * Returns the list of {@link IImageExporter}s used by this {@link ImageViewer}
     * 
     * @return A {@link List}
     */
    public List<IImageExporter> getImageExporters() {
        List<IImageExporter> exporters = new ArrayList<IImageExporter>();
        synchronized (imageExporters) {
            exporters.addAll(imageExporters);
        }
        return exporters;
    }

    /**
     * Adds an {@link IImageViewer} to this {@link ImageViewer}
     * 
     * @param exporter The {@link IImageExporter} to add
     */
    public void addImageExporter(IImageExporter exporter) {
        if (exporter != null) {
            boolean canAdd = true;
            synchronized (imageExporters) {
                for (IImageExporter imageExporter : imageExporters) {
                    if (imageExporter.getClass().equals(exporter.getClass())) {
                        canAdd = false;
                        break;
                    }
                }
                if (canAdd) {
                    imageExporters.add(exporter);
                    importMenu.add(createImportAction(exporter));
                    exportMenu.add(createExportAction(exporter));
                }
            }
        }
    }

    /**
     * Removes an {@link IImageViewer} from this {@link ImageViewer}
     * 
     * @param exporter The {@link IImageExporter} to remove
     */
    public void removeImageExporter(IImageExporter exporter) {
        if (exporter != null) {
            synchronized (imageExporters) {
                imageExporters.remove(exporter);
                AbstractActionExt exportAction = getAction(SAVE_DATA_ACTION + exporter.getFormatName());
                AbstractActionExt importAction = getAction(LOAD_DATA_ACTION + exporter.getFormatName());
                if (exportAction != null) {
                    removeAction(exportMenu, exportAction);
                }
                if (importAction != null) {
                    removeAction(importMenu, importAction);
                }
            }
        }
    }

    /**
     * Removes an {@link AbstractAction} from a {@link JMenu}
     * 
     * @param menu The {@link JMenu}
     * @param action The {@link AbstractAction} to remove
     */
    protected void removeAction(JMenu menu, AbstractAction action) {
        if ((menu != null) && (action != null)) {
            List<JMenuItem> toRemove = new ArrayList<JMenuItem>();
            for (int i = 0; i < menu.getItemCount(); i++) {
                JMenuItem item = menu.getItem(i);
                if (ObjectUtils.sameObject(action, item.getAction())) {
                    toRemove.add(item);
                }
            }
            for (JMenuItem item : toRemove) {
                menu.remove(item);
            }
            toRemove.clear();
        }

    }

    /**
     * Creates an "export data" action based on an {@link IImageExporter}
     * 
     * @param exporter The {@link IImageExporter}
     * @return An {@link AbstractActionExt}
     */
    protected AbstractActionExt createExportAction(final IImageExporter exporter) {
        AbstractActionExt saveDataAction = null;
        if (exporter != null) {
            String formatName = exporter.getFormatName();
            saveDataAction = new AbstractActionExt() {

                private static final long serialVersionUID = 4631498104030668531L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    JFileChooser fileChooser = new JFileChooser(transferEventDelegate.getDataDirectory());
                    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                    FileFilter filter = new FileFilter() {
                        @Override
                        public String getDescription() {
                            return exporter.getFileDescription();
                        }

                        @Override
                        public boolean accept(File f) {
                            String extension = exporter.getFileExtension();
                            if (extension != null) {
                                extension = extension.toLowerCase();
                            }
                            return f.isDirectory() || ObjectUtils.sameObject(extension, FileUtils.getExtension(f));
                        }
                    };
                    fileChooser.addChoosableFileFilter(filter);
                    fileChooser.setFileFilter(filter);
                    int returnVal = fileChooser.showSaveDialog(CometeUtils.getDialogForComponent(ImageViewer.this));
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        int choice = JOptionPane.YES_OPTION;
                        File selected = fileChooser.getSelectedFile();
                        if ((exporter.getFileExtension() != null) && (!exporter.getFileExtension().trim().isEmpty())
                                && (!ObjectUtils.sameObject(exporter.getFileExtension(),
                                        FileUtils.getExtension(selected)))) {
                            selected = new File(
                                    selected.getAbsolutePath() + ChartUtils.DOT + exporter.getFileExtension());
                        }
                        if (selected.exists()) {
                            choice = JOptionPane.showConfirmDialog(ImageViewer.this,
                                    String.format(MESSAGES.getString("ImageJViewer.SaveFile.Overwrite.Message"),
                                            selected.getName()),
                                    MESSAGES.getString("ImageJViewer.SaveFile.Overwrite.Title"),
                                    JOptionPane.YES_NO_OPTION);
                        }
                        if (choice == JOptionPane.YES_OPTION) {
                            try {
                                exporter.exportImage(ImageViewer.this, selected);
                            } finally {
                                transferEventDelegate.changeDataLocation(selected.getAbsolutePath(), true);
                            }
                        }
                    }
                }

            };
            saveDataAction.setName(MESSAGES.getString("ImageJViewer.Action.ExportData.Text") + formatName);
            ImageIcon icon = ImageTool.getImage(exporter.getExportIcon());
            if (icon != null) {
                saveDataAction.setSmallIcon(icon);
            }
            saveDataAction.setActionCommand(getTransformedActionCommand(SAVE_DATA_ACTION + formatName));
            ActionManager.getInstance().addAction(saveDataAction);
        }
        return saveDataAction;
    }

    /**
     * Creates an "import data" action based on an {@link IImageExporter}
     * 
     * @param exporter The {@link IImageExporter}
     * @return An {@link AbstractActionExt}
     */
    protected AbstractActionExt createImportAction(final IImageExporter exporter) {
        AbstractActionExt loadDataAction = null;
        if (exporter != null) {
            String formatName = exporter.getFormatName();
            loadDataAction = new AbstractActionExt() {

                private static final long serialVersionUID = 8579481343204816393L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    JFileChooser fileChooser = new JFileChooser(transferEventDelegate.getDataDirectory());
                    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                    FileFilter filter = new FileFilter() {
                        @Override
                        public String getDescription() {
                            return exporter.getFileDescription();
                        }

                        @Override
                        public boolean accept(File f) {
                            String extension = exporter.getFileExtension();
                            if (extension != null) {
                                extension = extension.toLowerCase();
                            }
                            return f.isDirectory() || ObjectUtils.sameObject(extension, FileUtils.getExtension(f));
                        }
                    };
                    fileChooser.addChoosableFileFilter(filter);
                    fileChooser.setFileFilter(filter);
                    int returnVal = fileChooser.showOpenDialog(CometeUtils.getDialogForComponent(ImageViewer.this));
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        try {
                            exporter.importImage(ImageViewer.this, fileChooser.getSelectedFile());
                        } finally {
                            transferEventDelegate.changeDataLocation(fileChooser.getSelectedFile().getAbsolutePath(),
                                    true);
                        }

                    }
                }

            };
            loadDataAction.setName(MESSAGES.getString("ImageJViewer.Action.ImportData.Text") + formatName);
            ImageIcon icon = ImageTool.getImage(exporter.getImportIcon());
            if (icon != null) {
                loadDataAction.setSmallIcon(icon);
            }
            loadDataAction.setActionCommand(getTransformedActionCommand(LOAD_DATA_ACTION + formatName));
            ActionManager.getInstance().addAction(loadDataAction);
        }
        return loadDataAction;
    }

    /**
     * Returns the configuration as string.
     * 
     * @return A {@link String}
     * @see #saveSettings
     */
    public String getSettings() {
        StringBuilder to_write = new StringBuilder();
        synchronized (this) {
            // General settings
            to_write.append("minBestFitText:\'").append(fitMin).append("\'\n");
            to_write.append("maxBestFitText:\'").append(fitMax).append("\'\n");
            to_write.append("fitMaxSize:\'").append(alwaysFitMaxSize).append("\'\n");
            to_write.append("autoBestFit:\'").append(autoBestFit).append("\'\n");
            to_write.append("zoom:\'").append(imageCanvas == null ? 1 : imageCanvas.getMagnification()).append("\'\n");
            int gradientCount = gradient.getEntryNumber();
            to_write.append("gradientCount:\'").append(gradientCount).append("\'\n");
            for (int i = 0; i < gradientCount; i++) {
                CometeColor gradientColor = gradient.getColorAt(i);
                to_write.append("gradientColor_").append(i).append("_red:\'").append(gradientColor.getRed())
                        .append("\'\n");
                to_write.append("gradientColor_").append(i).append("_green:\'").append(gradientColor.getGreen())
                        .append("\'\n");
                to_write.append("gradientColor_").append(i).append("_blue:\'").append(gradientColor.getBlue())
                        .append("\'\n");
                to_write.append("gradientPos_").append(i).append(":\'").append(gradient.getPosAt(i)).append("\'\n");
            }
        }
        return to_write.toString();
    }

    public void applySettings(String settings) {
        if ((settings != null) && (!settings.trim().isEmpty())) {
            CfFileReader reader = new CfFileReader();
            reader.parseText(settings);
            applySettings(reader);
        }
    }

    protected void applySettings(CfFileReader f) {
        if (f != null) {
            double bfMin, bfMax;
            bfMin = recoverDouble("minBestFitText", f);
            bfMax = recoverDouble("maxBestFitText", f);
            Boolean fitMax = recoverBoolean("fitMaxSize", f);
            Boolean autoBestFit = recoverBoolean("autoBestFit", f);
            int gradientCount = recoverPositiveInt("gradientCount", f, -1);
            CometeColor[] gradientColor = null;
            double[] gradientPos = null;
            if (gradientCount > 0) {
                gradientColor = new CometeColor[gradientCount];
                gradientPos = new double[gradientCount];
            }
            double zoom = recoverDouble("zoom", f);
            for (int i = 0; i < gradientCount; i++) {
                int red = recoverPositiveInt("gradientColor_" + i + "_red", f, 0);
                int green = recoverPositiveInt("gradientColor_" + i + "_green", f, 0);
                int blue = recoverPositiveInt("gradientColor_" + i + "_blue", f, 0);
                double pos = recoverDouble("gradientPos_" + i, f);
                gradientColor[i] = new CometeColor(red, green, blue);
                gradientPos[i] = pos;
            }
            if (autoBestFit != null) {
                setAutoBestFit(autoBestFit.booleanValue());
            }
            if ((!Double.isNaN(bfMin)) && (!Double.isNaN(bfMax))) {
                setFitMinMax(bfMin, bfMax, false);
            }
            if ((gradientColor != null) && (gradientPos != null)) {
                Gradient gColor = new Gradient();
                if (gradientCount == 5) {
                    gColor.buildRainbowGradient();
                }
                for (int i = 0; i < gradientCount; i++) {
                    gColor.setColorAt(i, gradientColor[i]);
                    gColor.setPosAt(i, gradientPos[i]);
                }
                setGradient(gColor);
            }
            setZoom(zoom);
            if (fitMax != null) {
                setAlwaysFitMaxSize(fitMax.booleanValue());
            }
        }
    }

    /**
     * Load NumberImageViewer settings.
     * 
     * @param filename file to be read
     * @return An error string or An empty string when succes
     */
    public void loadSettings(String filename) {
        CfFileReader f = new CfFileReader();
        // Read and browse the file
        if (!f.readFile(filename)) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to read " + filename);
        }
        lastConfig = filename;
        applySettings(f);
    }

    /**
     * Save settings.
     * 
     * @param filename file to be saved.
     */
    public void saveSettings(String filename) {
        try {
            FileWriter f = new FileWriter(filename);
            String s = getSettings();
            f.write(s, 0, s.length());
            f.close();
            lastConfig = filename;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Failed to write " + filename, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Method called when clocking on "save settings" button
     */
    protected void saveSettings() {
        int ok = JOptionPane.YES_OPTION;
        JFileChooser chooser = new JFileChooser(ChartUtils.DOT);
        chooser.addChoosableFileFilter(new MultiExtFileFilter("Text files", "txt"));
        if (lastConfig.length() > 0) {
            chooser.setSelectedFile(new File(lastConfig));
        }
        int returnVal = chooser.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            if (f != null) {
                if (FileUtils.getExtension(f) == null) {
                    f = new File(f.getAbsolutePath() + ".txt");
                }
                if (f.exists()) {
                    ok = JOptionPane.showConfirmDialog(this, "Do you want to overwrite " + f.getName() + " ?",
                            "Confirm overwrite", JOptionPane.YES_NO_OPTION);
                }
                if (ok == JOptionPane.YES_OPTION) {
                    saveSettings(f.getAbsolutePath());
                }
            }
        }
    }

    /**
     * Method called when clocking on "load settings" button
     */
    protected void loadSettings() {
        int ok = JOptionPane.YES_OPTION;
        JFileChooser chooser = new JFileChooser();
        chooser.addChoosableFileFilter(new MultiExtFileFilter("Text files", "txt"));
        if (lastConfig.length() > 0) {
            chooser.setSelectedFile(new File(lastConfig));
        }
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            if (f != null) {
                if (ok == JOptionPane.YES_OPTION) {
                    loadSettings(f.getAbsolutePath());
                }
            }
        }
    }

    /**
     * Hack to avoid a NullPointerException in ImageCanvas
     */
    protected final void unzoomImageCanvas() {
        try {
            imageCanvas.unzoom();
        } catch (NullPointerException e) {
            // Somehow, the ImageWindow became null
            IJRoiManager imp = imageCanvas.getImagePlus();
            imp.setWindow(new HiddenImageWindow(imp, imageCanvas));
            imageCanvas.unzoom();
        }
    }

    public double getZoom() {
        return (imageCanvas == null ? 1 : imageCanvas.getMagnification());
    }

    public void setZoom(double zoom) {
        if ((imageCanvas != null) && (!Double.isNaN(zoom))) {
            setZoom(zoom, true);
        }
    }

    // JAVAAPI-625: add the possibility not to notify zoom roi generators
    protected void setZoom(double zoom, boolean notifyRoiGenerators) {
        if ((imageCanvas != null) && (!Double.isNaN(zoom))) {
            unzoomImageCanvas();
            imageCanvas.setMagnification(zoom);
            adjustToCanvas();
            if (notifyRoiGenerators) {
                updateZoomRoiGenerators();
            }
        }
    }

    protected int recoverPositiveInt(String key, CfFileReader f, int defaultValue) {
        int result = defaultValue;
        double tmp = recoverDouble(key, f);
        if (!Double.isNaN(tmp)) {
            result = (int) tmp;
            if (result < 0) {
                result = defaultValue;
            }
        }
        return result;
    }

    protected double recoverDouble(String key, CfFileReader f) {
        double result = Double.NaN;
        String item = recoverParam(key, f);
        if (item != null) {
            try {
                result = Double.parseDouble(item);
            } catch (Exception e) {
                result = Double.NaN;
            }
        }
        return result;
    }

    protected Boolean recoverBoolean(String key, CfFileReader f) {
        Boolean result = null;
        String item = recoverParam(key, f);
        if (item != null) {
            result = "true".equalsIgnoreCase(item.trim());
        }
        return result;
    }

    protected String recoverParam(String key, CfFileReader f) {
        String result = null;
        List<String> param = f.getParam(key);
        if (param != null) {
            result = param.get(0).toString().replace("'", ObjectUtils.EMPTY_STRING);
        }
        return result;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return false;
    }

    @Override
    public boolean dataChanged() {
        return dataComponentDelegate.dataChanged(this);
    }

    @Override
    public void updateFromData() {
        getDrawingThreadManager().runInDrawingThread(() -> {
            updateImageAndComponents();
            mayAutoResize();
            updateRoisImagePlus();
            updateDataRoiGenerators();
            if (isStatisticsVisible() && (statisticsComponent instanceof IStatisticsComponent)) {
                ((IStatisticsComponent) statisticsComponent).computeStatisticsAndDisplayRoiInformation(this);
            }
            repaint();
        });
    }

    @Override
    public CometeColor getNanColor() {
        return ColorTool.getCometeColor(nanColor);
    }

    @Override
    public void setNanColor(CometeColor nanColor) {
        Color tmp = ColorTool.getColor(nanColor);
        if (tmp == null) {
            tmp = DEFAULT_NAN_COLOR;
        }
        this.nanColor = tmp;
        if (converter != null) {
            converter.setNaNColor(this.nanColor.getRGB());
        }
        convertImage(true);
    }

    @Override
    public CometeColor getPositiveInfinityColor() {
        return ColorTool.getCometeColor(positiveInfinityColor);
    }

    @Override
    public void setPositiveInfinityColor(CometeColor positiveInfinityColor) {
        Color tmp = ColorTool.getColor(positiveInfinityColor);
        if (tmp == null) {
            tmp = DEFAULT_POSITIVE_INFINITY_COLOR;
        }
        this.positiveInfinityColor = tmp;
        if (converter != null) {
            converter.setPositiveInfinityColor(this.positiveInfinityColor.getRGB());
        }
        convertImage(true);
    }

    @Override
    public CometeColor getNegativeInfinityColor() {
        return ColorTool.getCometeColor(negativeInfinityColor);
    }

    @Override
    public void setNegativeInfinityColor(CometeColor negativeInfinityColor) {
        Color tmp = ColorTool.getColor(negativeInfinityColor);
        if (tmp == null) {
            tmp = DEFAULT_NEGATIVE_INFINITY_COLOR;
        }
        this.negativeInfinityColor = tmp;
        if (converter != null) {
            converter.setNegativeInfinityColor(this.negativeInfinityColor.getRGB());
        }
        convertImage(true);
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void setEditable(boolean editable) {
        this.editable = editable;
        matrixTable.setEditable(editable);
    }

    public boolean isManageActionsAvailability() {
        return manageActionsAvailability;
    }

    public void setManageActionsAvailability(boolean manageActionsAvailability) {
        this.manageActionsAvailability = manageActionsAvailability;
    }

    @Override
    public String getFormat() {
        return matrixTable.getFormat();
    }

    @Override
    public void setFormat(String format) {
        String old = getFormat();
        matrixTable.setFormat(format);
        if (coordsLabel instanceof CoordsLabel) {
            ((CoordsLabel) coordsLabel).setValueFormat(format);
            repaint();
        }
        if ((matrixDialog != null) && (matrixDialog.isVisible())) {
            matrixTable.repaint();
        }
        if (statisticsComponent instanceof IStatisticsComponent) {
            ((IStatisticsComponent) statisticsComponent).setValueFormat(format);
        }
        updateGradientFormat(true);
        updateProfilersFormat();
        firePropertyChange(FORMAT, old, format);
    }

    protected void updateGradientFormat(boolean repaint) {
        String format = getFormat();
        if (gradientViewer != null) {
            gradientViewer.getAxis().getAttributes().setValueFormat(format);
            if (repaint && gradientViewer.isShowing()) {
                gradientViewer.repaint();
            }
        }
    }

    protected void updateProfilersFormat() {
        Chart profiler = getLineProfiler(false);
        Chart xProfiler = getHorizontalProfiler(false);
        Chart yProfiler = getVerticalProfiler(!isHorizontalDataForVerticalProfiler(), false);
        String format = getFormat(), xFormat = getAxisFormat(xAxis), yFormat = getAxisFormat(yAxis);
        if (profiler != null) {
            profiler.setFormat(LINE_PROFILE_NAME, format);
            profiler.getChartView().measureGraphItems();
            profiler.repaint();
        }
        if (xProfiler != null) {
            xProfiler.setFormat(HORIZONTAL_PROFILE_NAME, format);
            xProfiler.getChartView().getAxis(IChartViewer.X).getAttributes().setValueFormat(xFormat);
            xProfiler.getChartView().getAxis(IChartViewer.Y1).getAttributes().setValueFormat(format);
            xProfiler.getChartView().measureGraphItems();
            xProfiler.repaint();
        }
        if (yProfiler != null) {
            yProfiler.setFormat(VERTICAL_PROFILE_NAME, format);
            if (isHorizontalDataForVerticalProfiler()) {
                yProfiler.getChartView().getAxis(IChartViewer.X).getAttributes().setValueFormat(yFormat);
                yProfiler.getChartView().getAxis(IChartViewer.Y1).getAttributes().setValueFormat(format);
            } else {
                yProfiler.getChartView().getAxis(IChartViewer.Y1).getAttributes().setValueFormat(yFormat);
                yProfiler.getChartView().getAxis(IChartViewer.X).getAttributes().setValueFormat(format);
            }
            yProfiler.getChartView().measureGraphItems();
            yProfiler.repaint();
        }
    }

    protected final void checkApplicationId() throws ApplicationIdException {
        if (applicationId == null) {
            StringBuilder builder = new StringBuilder(MESSAGES.getString("ImageJViewer.Exception.Application.Id"));
            Window window = CometeUtils.getWindowForComponent(this);
            if (window != null) {
                builder.append(ObjectUtils.NEW_LINE);
                builder.append(MESSAGES.getString("ImageJViewer.Exception.Application.Id.Window"));
                builder.append(" ").append(window.getClass().getName());
            }
            throw new ApplicationIdException(builder.toString());
        }
    }

    /**
     * Returns this {@link ImageViewer}'s application id. This applicaiton id is used by {@link ImageJManager} to
     * organize ImageJ interaction.
     * 
     * @return A {@link String}
     */
    @Override
    public final String getApplicationId() {
        return applicationId;
    }

    /**
     * Sets this {@link ImageViewer}'s application id. This application id is used by {@link ImageJManager} to organize
     * ImageJ interaction.
     * 
     * @param applicationId The application id to use
     */
    public final void setApplicationId(String applicationId) {
        if (this.applicationId == null && applicationId != null) {
            this.applicationId = applicationId;
        }
    }

    public boolean isStopDualProfileAtCursorPosition() {
        return stopDualProfileAtCursorPosition;
    }

    public void setStopDualProfileAtCursorPosition(boolean stopDualProfileAtCursorPosition) {
        this.stopDualProfileAtCursorPosition = stopDualProfileAtCursorPosition;
    }

    public JPanel getVerticalProfilerPanel() {
        if (verticalProfilerPanel == null) {
            verticalProfiler = getVerticalProfiler(true);
            verticalProfiler.setPreferredSize(new Dimension(PROFILER_MAX_SIZE, 0));
            verticalProfiler.setMaximumSize(verticalProfiler.getPreferredSize());
            verticalProfilerPanel = new JPanel(new BorderLayout());
            verticalProfilerPanel.add(verticalProfiler, BorderLayout.NORTH);
            GridBagConstraints verticalGlueConstraints = new GridBagConstraints();
            verticalGlueConstraints.fill = GridBagConstraints.VERTICAL;
            verticalGlueConstraints.gridx = 0;
            verticalGlueConstraints.gridy = 1;
            verticalGlueConstraints.weightx = 0;
            verticalGlueConstraints.weighty = 1;
            verticalProfilerPanel.add(Box.createGlue(), BorderLayout.CENTER);
        }
        return verticalProfilerPanel;
    }

    public JPanel getHorizontalProfilerPanel() {
        if (horizontalProfilerPanel == null) {
            horizontalProfiler = getHorizontalProfiler();
            horizontalProfiler.setPreferredSize(new Dimension(0, PROFILER_MAX_SIZE));
            horizontalProfiler.setMaximumSize(horizontalProfiler.getPreferredSize());
            horizontalProfilerPanel = new JPanel(new BorderLayout());
            horizontalProfilerPanel.add(horizontalProfiler, BorderLayout.WEST);
            horizontalProfilerPanel.add(Box.createGlue(), BorderLayout.CENTER);
        }
        return horizontalProfilerPanel;
    }

    public JPanel getProfilerSizeButtonPanel() {
        if (profilerSizeButtonPanel == null) {
            profilerSizeButtonPanel = new JPanel(new BorderLayout());
            profilerSizeButtonPanel.add(getBiggerProfilerButtonPanel(), BorderLayout.NORTH);
            profilerSizeButtonPanel.add(getSmallerProfilerButtonPanel(), BorderLayout.SOUTH);
            profilerSizeButtonPanel.setVisible(horizontalProfiler.getPreferredSize().width > 0);
        }
        return profilerSizeButtonPanel;
    }

    public JPanel getBiggerProfilerButtonPanel() {
        if (biggerProfilerButtonPanel == null) {
            biggerProfilerButtonPanel = new JPanel(new BorderLayout());
            biggerProfilerButtonPanel.setOpaque(false);
            biggerProfilerButtonPanel.add(getBiggerProfilerButton(), BorderLayout.WEST);
        }
        return biggerProfilerButtonPanel;
    }

    public JButton getBiggerProfilerButton() {
        if (biggerProfilerButton == null) {
            biggerProfilerButton = new JButton("+");
            biggerProfilerButton.setMargin(new Insets(0, 0, 0, 0));
            biggerProfilerButton.setToolTipText(MESSAGES.getString("ImageJViewer.Action.Profiler.Bigger"));
            biggerProfilerButton.addActionListener((e) -> {
                changeProfilerSize(true);
            });
            biggerProfilerButton.setEnabled(currentProfilerSize < PROFILER_MAX_SIZE);
        }
        return biggerProfilerButton;
    }

    public JPanel getSmallerProfilerButtonPanel() {
        if (smallerProfilerButtonPanel == null) {
            smallerProfilerButtonPanel = new JPanel(new BorderLayout());
            smallerProfilerButtonPanel.setOpaque(false);
            smallerProfilerButtonPanel.add(getSmallerProfilerButton(), BorderLayout.WEST);
        }
        return smallerProfilerButtonPanel;
    }

    public JButton getSmallerProfilerButton() {
        if (smallerProfilerButton == null) {
            smallerProfilerButton = new JButton("-");
            smallerProfilerButton.setMargin(new Insets(0, 0, 0, 0));
            smallerProfilerButton.setToolTipText(MESSAGES.getString("ImageJViewer.Action.Profiler.Smaller"));
            smallerProfilerButton.addActionListener((e) -> {
                changeProfilerSize(false);
            });
            smallerProfilerButton.setEnabled(currentProfilerSize > PROFILER_MIN_SIZE);
        }
        return smallerProfilerButton;
    }

    protected void changeProfilerSize(boolean bigger) {
        if (bigger) {
            getSmallerProfilerButton().setEnabled(true);
            if (currentProfilerSize < PROFILER_MAX_SIZE) {
                currentProfilerSize += PROFILER_SIZE_STEP;
                if (currentProfilerSize >= PROFILER_MAX_SIZE) {
                    getBiggerProfilerButton().setEnabled(false);
                }
            }
        } else {
            getBiggerProfilerButton().setEnabled(true);
            if (currentProfilerSize > PROFILER_MIN_SIZE) {
                currentProfilerSize -= PROFILER_SIZE_STEP;
                if (currentProfilerSize <= PROFILER_MIN_SIZE) {
                    getSmallerProfilerButton().setEnabled(false);
                }
            }
        }
        profilesFollowImage();
        revalidate();
        repaint();
    }

    /**
     * Forces the profilers to take maximum or minimum size
     * 
     * @param maximum Whether profilers should take maximum size
     */
    public void setProfilerToExtremeSize(boolean maximum) {
        currentProfilerSize = maximum ? PROFILER_MAX_SIZE : PROFILER_MIN_SIZE;
        getSmallerProfilerButton().setEnabled(maximum);
        getBiggerProfilerButton().setEnabled(!maximum);
        profilesFollowImage();
        revalidate();
        repaint();
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        Color bg = ColorTool.getColor(color);
        setBackground(bg);
        infoPanel.setBackground(bg);
        imagePanel.setBackground(bg);
        if (horizontalProfilerPanel != null) {
            horizontalProfilerPanel.setBackground(bg);
        }
        if (verticalProfilerPanel != null) {
            verticalProfilerPanel.setBackground(bg);
        }
        setScrollBackground(imageScrollPane, bg);
        setScrollBackground(infoScrollPane, bg);
        imageView.setBackground(bg);
    }

    protected void setScrollBackground(JScrollPane scrollPane, Color bg) {
        scrollPane.setBackground(bg);
        if (scrollPane.getRowHeader() != null) {
            scrollPane.getRowHeader().setBackground(bg);
        }
        if (scrollPane.getColumnHeader() != null) {
            scrollPane.getColumnHeader().setBackground(bg);
        }
        scrollPane.getViewport().setBackground(bg);
        scrollPane.getHorizontalScrollBar().setBackground(bg);
        scrollPane.getVerticalScrollBar().setBackground(bg);
    }

    /**
     * Returns the roi name alignment
     * 
     * @return An <code>int[]</code>of length 2: <code>{horizontalAlignment, verticalAlignment}</code>
     * @see #setRoiNameAlignment(int, int)
     */
    public int[] getRoiNameAlignment() {
        return roiNameAlignment;
    }

    /**
     * Changes the roi name alignment
     * 
     * @param roiNameAlignment the roi name alignment, expected to be either <code>null</code> or an <code>int[]</code>
     *            of lenght 2:
     *            <ol start="0">
     *            <li>horizontalAlignment The horizontal alignment. Can be one of these values:
     *            <ul>
     *            <li>{@link SwingConstants#LEFT}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#RIGHT}</li>
     *            </ul>
     *            </li>
     *            <li>verticalAlignment The vertical alignment. Can be one of these values:
     *            <ul>
     *            <li>{@link SwingConstants#TOP}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#BOTTOM}</li>
     *            </ul>
     *            </li>
     *            </ol>
     */
    public void setRoiNameAlignment(int... roiNameAlignment) {
        if ((roiNameAlignment == null) || (roiNameAlignment.length == 2)) {
            this.roiNameAlignment = roiNameAlignment;
            IJCanvas canvas = imageCanvas;
            if ((canvas != null) && (roiNameAlignment != null)) {
                canvas.setRoiNameAlignment(roiNameAlignment[0], roiNameAlignment[1]);
            }
        }
    }

    public ImageScreenPositionCalculator getImageScreenPositionCalculator() {
        if (imageScreenPositionCalculator == null) {
            imageScreenPositionCalculator = new ImageScreenPositionCalculator();
            imageScreenPositionCalculator.setXConvertor(xAxisConvertor);
            imageScreenPositionCalculator.setYConvertor(yAxisConvertor);
        }
        return imageScreenPositionCalculator;
    }

    // Here, we generate an id that whill help differenciating 2 instances of
    // the same classs
    protected void generateViewerId() {
        viewerId = CometeUtils.generateIdForClass(getClass(), 200);
    }

    // This function returns a transformed action command, so that 2 different
    // instances of the same class do not share the same action commands
    protected String getTransformedActionCommand(String command) {
        return viewerId + "/" + command;
    }

    // This method was added to avoid some problem with JGradientViewer's axis:
    // Typically, it ensures with minimum at 0 to always (or at least more often) draw ticks
    // Found while studying DATAREDUC-745
    protected double getBestMinForLogScale(double min, double max) {
        double bestMin;
        if (min <= 0) {
            if ((max > min) && (max < 10)) {
                bestMin = ChartUtils.computeLowTen(max / 10);
            } else {
                bestMin = 1;
            }
        } else {
            bestMin = min;
        }
        return bestMin;
    }

    protected void updateImageConverter(boolean logScale) {
        converter.setGradientTool(getGradientViewer());
        converter.setGColormap(gColormap);
        if (logScale) {
            // in case of log scale, compute axis bounds log
            fitMin = getBestMinForLogScale(fitMin, fitMax);
            converter.setBfMin(Math.log10(fitMin));
            converter.setBfMax(Math.log10(fitMax));
        } else {
            converter.setBfMin(fitMin);
            converter.setBfMax(fitMax);
        }
    }

    protected boolean isChangeRoiModeAtInitilization() {
        return true;
    }

    protected void initComponents() {
        undoManager = new MyUndoManager();

        createImageCanvas(imagePlus);
        imagePlus.addUndoableEditListener(undoManager);
        // TODO save other undoableedit sources

        initColorStuff();

        initAxis();

        initActions();

        if (isChangeRoiModeAtInitilization()) {
            // we start with pan mode
            setPanMode();
        }

        initFitSettingsPanel();

        initImagePanel();
        // do not fit max size while user is scrolling image
        MouseListener listener = new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                IJRoiManager roiManager = getRoiManager();
                if (roiManager != null) {
                    ImageWindow window = roiManager.getWindow();
                    if (window instanceof HiddenImageWindow) {
                        ((HiddenImageWindow) window).setIgnoreResizeEvents(true);
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                IJRoiManager roiManager = getRoiManager();
                if (roiManager != null) {
                    ImageWindow window = roiManager.getWindow();
                    if (window instanceof HiddenImageWindow) {
                        ((HiddenImageWindow) window).setIgnoreResizeEvents(false);
                    }
                }
            }
        };
        imageScrollPane.getHorizontalScrollBar().addMouseListener(listener);
        imageScrollPane.getVerticalScrollBar().addMouseListener(listener);
        initNameLabel();
        initCoordsLabel();
        initZoomLabel();
        initStatisticsComponent();
        initStatisticsCheckBox();
        initInfoPanel();
        initInfoScrollPane();
        initRoiInfoTableScrollPane();
        if (coordsLabel instanceof CoordsLabel) {
            ((CoordsLabel) coordsLabel).setValueFormat(getFormat());
        }
    }

    protected void initStatisticsComponent() {
        statisticsComponent = createDefaultStatisticsComponent();
    }

    protected void initStatisticsCheckBox() {
        showStatisticsCheckBox = new JCheckBox("Show Statistics");
        showStatisticsCheckBox.addActionListener((e) -> {
            setStatisticsVisible(showStatisticsCheckBox.isSelected());
        });
    }

    protected void addComponents() {
        add(toolBarScrollPane, BorderLayout.NORTH);
        add(imageView, BorderLayout.CENTER);
        add(infoScrollPane, BorderLayout.SOUTH);
        // no toolbar by default
        setToolBarVisible(false);
    }

    protected void initLayout() {
        setLayout(new BorderLayout());
    }

    protected void initImagePanel() {
        imagePanel = new ImagePanel();
        imageScrollPane = new JScrollPane(imagePanel);
        imageScrollPane.addComponentListener(imagePanel);
        imageView = new JPanel(new BorderLayout());
        imageView.add(imageScrollPane, BorderLayout.CENTER);
        imageView.add(getGradientViewer(), BorderLayout.EAST);
        addListeners();
    }

    protected void removeListeners() {
        imagePanel.removeMouseListener(this);
        imagePanel.removeMouseMotionListener(this);
        imagePanel.removeMouseWheelListener(this);
        imagePanel.removeKeyListener(this);
    }

    protected void addListeners() {
        imagePanel.addMouseListener(this);
        imagePanel.addMouseMotionListener(this);
        imagePanel.addMouseWheelListener(this);
        imagePanel.addKeyListener(this);
    }

    protected void initNameLabel() {
        nameLabel = new JLabel();
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        nameLabel.setFont(CometeUtils.getLabelFont());
    }

    protected void initCoordsLabel() {
        coordsLabel = createDefaultCoordsLabel();
        coordsLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
    }

    protected void initZoomLabel() {
        zoomLabel = createDefaultZoomLabel();
        zoomLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
    }

    protected void initInfoScrollPane() {
        infoScrollPane = new JScrollPane(infoPanel) {
            private static final long serialVersionUID = -6193438590389585713L;

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(super.getPreferredSize().width,
                        super.getPreferredSize().height + getHorizontalScrollBar().getHeight());
            }
        };
    }

    protected void initInfoPanel() {
        infoPanel = new JPanel(new BorderLayout(5, 5));
        infoPanel.add(nameLabel, BorderLayout.NORTH);
        infoPanel.add(coordsLabel, BorderLayout.CENTER);
        infoPanel.add(zoomLabel, BorderLayout.WEST);
        infoPanel.add(statisticsComponent, BorderLayout.SOUTH);
        infoPanel.add(showStatisticsCheckBox, BorderLayout.EAST);
    }

    protected void initRoiTableJSplitPane() {
        southSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        southSplitPane.setTopComponent(imageView);
        southSplitPane.setBottomComponent(roiInfoTableScrollPane);
        southSplitPane.setDividerSize(8);
        southSplitPane.setDividerLocation(0.7);
        southSplitPane.setResizeWeight(0.9);
        southSplitPane.setOneTouchExpandable(true);
        southSplitPane.setUI(new ColoredSplitPaneUI());
    }

    protected void initRoiInfoTable() {
        roiInfoTable = new RoiInfoTable(this);
    }

    protected void initRoiInfoTableScrollPane() {
        initRoiInfoTable();
        roiInfoTableScrollPane = new JScrollPane(roiInfoTable);
        roiInfoTableScrollPane.setPreferredSize(new Dimension(0, 0));
        roiInfoTableScrollPane.setMinimumSize(new Dimension(0, 0));
        roiInfoTableScrollPane.setVisible(false);
    }

    // inits the contentpane of the JDialog that appears when clicking on the
    // gradient
    protected void initFitSettingsPanel() {
        minFitLabel = new JLabel(MESSAGES.getString("ImageJViewer.Label.MinFit.Text"));
        maxFitLabel = new JLabel(MESSAGES.getString("ImageJViewer.Label.MaxFit.Text"));

        ActionListener applyFitActionListener = (e) -> {
            applyFitMinAndMaxFromFields();
        };
        // We use this formatter with this Locale because it uses "." as decimal
        // separator
        NumberFormat format = NumberFormat.getNumberInstance(Locale.UK);
        // Here, we do not allow "," as a 10^3 separator
        format.setGroupingUsed(false);
        minFitTextField = new JFormattedTextField(format);
        minFitTextField.setColumns(10);
        minFitTextField.addActionListener(applyFitActionListener);

        maxFitTextField = new JFormattedTextField(format);
        maxFitTextField.setColumns(10);
        maxFitTextField.addActionListener(applyFitActionListener);

        applyFitButton = new JButton(MESSAGES.getString("ImageJViewer.Action.ApplyFit.Text"));
        applyFitButton.addActionListener(applyFitActionListener);

        minFitTextField.setValue(Double.valueOf(fitMin));
        maxFitTextField.setValue(Double.valueOf(fitMax));

        gradientEditor = new JGradientEditor();
        gradientEditor.setEditable(false);
        gradientEditor.setGradient(getGradient());
        gradientEditor.addMouseListener(this);
        gradientEditor.setToolTipText(MESSAGES.getString("ImageJViewer.GradientEditor.Edit.Tooltip"));

        // layout components
        fitSettingsPanel = new JPanel(new GridBagLayout());

        // auto fit and log scale checkboxes
        JPanel checkBoxPanel = new JPanel(new GridBagLayout());
        JMenuItem bestFitMenuItem = containerFactory.createMenuItem(getAction(SET_AUTO_BEST_FIT_ACTION));
        bestFitMenuItem.setBorder(new EmptyBorder(0, 0, 0, 0));
        bestFitMenuItem.setBorderPainted(false);
        bestFitMenuItem.setOpaque(false);
        bestFitMenuItem.setMargin(new Insets(0, 0, 0, 0));
        GridBagConstraints bestFitConstraints = new GridBagConstraints();
        bestFitConstraints.fill = GridBagConstraints.NONE;
        bestFitConstraints.gridx = 0;
        bestFitConstraints.gridy = 0;
        bestFitConstraints.weightx = 0;
        bestFitConstraints.weighty = 0;
        checkBoxPanel.add(bestFitMenuItem, bestFitConstraints);
        JMenuItem logScaleMenuItem = containerFactory.createMenuItem(getAction(LOG_SCALE_ACTION));
        logScaleMenuItem.setBorder(new EmptyBorder(0, 0, 0, 0));
        logScaleMenuItem.setBorderPainted(false);
        logScaleMenuItem.setOpaque(false);
        logScaleMenuItem.setMargin(new Insets(0, 0, 0, 0));
        GridBagConstraints logScaleConstraints = new GridBagConstraints();
        logScaleConstraints.fill = GridBagConstraints.NONE;
        logScaleConstraints.gridx = 1;
        logScaleConstraints.gridy = 0;
        logScaleConstraints.weightx = 0;
        logScaleConstraints.weighty = 0;
        checkBoxPanel.add(logScaleMenuItem, logScaleConstraints);
        GridBagConstraints glue1Constraints = new GridBagConstraints();
        glue1Constraints.fill = GridBagConstraints.BOTH;
        glue1Constraints.gridx = 2;
        glue1Constraints.gridy = 0;
        glue1Constraints.weightx = 1;
        glue1Constraints.weighty = 0;
        checkBoxPanel.add(Box.createGlue(), glue1Constraints);
        GridBagConstraints checkBoxPanelConstraints = new GridBagConstraints();
        checkBoxPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        checkBoxPanelConstraints.gridx = 0;
        checkBoxPanelConstraints.gridy = 0;
        checkBoxPanelConstraints.weightx = 1;
        checkBoxPanelConstraints.weighty = 0;
        checkBoxPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        fitSettingsPanel.add(checkBoxPanel, checkBoxPanelConstraints);

        // min and max fit labels and text fields
        GridBagConstraints minFitLabelConstraints = new GridBagConstraints();
        minFitLabelConstraints.fill = GridBagConstraints.NONE;
        minFitLabelConstraints.gridx = 0;
        minFitLabelConstraints.gridy = 1;
        minFitLabelConstraints.weightx = 0;
        minFitLabelConstraints.weighty = 0;
        minFitLabelConstraints.insets = new Insets(5, 5, 5, 10);
        fitSettingsPanel.add(minFitLabel, minFitLabelConstraints);
        GridBagConstraints minFitTextFieldConstraints = new GridBagConstraints();
        minFitTextFieldConstraints.fill = GridBagConstraints.NONE;
        minFitTextFieldConstraints.gridx = 1;
        minFitTextFieldConstraints.gridy = 1;
        minFitTextFieldConstraints.weightx = 0;
        minFitTextFieldConstraints.weighty = 0;
        fitSettingsPanel.add(minFitTextField, minFitTextFieldConstraints);
        GridBagConstraints glue2Constraints = new GridBagConstraints();
        glue2Constraints.fill = GridBagConstraints.BOTH;
        glue2Constraints.gridx = 2;
        glue2Constraints.gridy = 1;
        glue2Constraints.weightx = 1;
        glue2Constraints.weighty = 0;
        fitSettingsPanel.add(Box.createGlue(), glue2Constraints);

        GridBagConstraints maxFitLabelConstraints = new GridBagConstraints();
        maxFitLabelConstraints.fill = GridBagConstraints.NONE;
        maxFitLabelConstraints.gridx = 0;
        maxFitLabelConstraints.gridy = 2;
        maxFitLabelConstraints.weightx = 0;
        maxFitLabelConstraints.weighty = 0;
        maxFitLabelConstraints.insets = new Insets(5, 5, 5, 10);
        fitSettingsPanel.add(maxFitLabel, maxFitLabelConstraints);
        GridBagConstraints maxFitTextFieldConstraints = new GridBagConstraints();
        maxFitTextFieldConstraints.fill = GridBagConstraints.NONE;
        maxFitTextFieldConstraints.gridx = 1;
        maxFitTextFieldConstraints.gridy = 2;
        maxFitTextFieldConstraints.weightx = 0;
        maxFitTextFieldConstraints.weighty = 0;
        fitSettingsPanel.add(maxFitTextField, maxFitTextFieldConstraints);
        GridBagConstraints glue3Constraints = new GridBagConstraints();
        glue3Constraints.fill = GridBagConstraints.BOTH;
        glue3Constraints.gridx = 2;
        glue3Constraints.gridy = 2;
        glue3Constraints.weightx = 1;
        glue3Constraints.weighty = 0;
        fitSettingsPanel.add(Box.createGlue(), glue3Constraints);

        // "apply" button
        GridBagConstraints applyFitButtonConstraints = new GridBagConstraints();
        applyFitButtonConstraints.fill = GridBagConstraints.NONE;
        applyFitButtonConstraints.gridx = 1;
        applyFitButtonConstraints.gridy = 3;
        applyFitButtonConstraints.weightx = 0;
        applyFitButtonConstraints.weighty = 0;
        fitSettingsPanel.add(applyFitButton, applyFitButtonConstraints);

        // gradient
        GridBagConstraints gradientEditorConstraints = new GridBagConstraints();
        gradientEditorConstraints.fill = GridBagConstraints.HORIZONTAL;
        gradientEditorConstraints.gridx = 0;
        gradientEditorConstraints.gridy = 4;
        gradientEditorConstraints.weightx = 1;
        gradientEditorConstraints.weighty = 0;
        gradientEditorConstraints.gridwidth = GridBagConstraints.REMAINDER;
        fitSettingsPanel.add(gradientEditor, gradientEditorConstraints);
        updateBestFitInfo();
    }

    protected void initMatrixDialog() {
        matrixDialog = new JDialog(CometeUtils.getFrameForComponent(this), "Matrix Data", false);
        setMatrixDialogContentPane();
    }

    protected void setMatrixDialogContentPane() {
        matrixDialog.setContentPane(matrixTable);
    }

    protected void packTable() {
        if (matrixTable != null) {
            matrixTable.packAll();
        }
    }

    public void showTable() {
        if (matrixDialog == null) {
            initMatrixDialog();
        }

        if (matrixTable.isPackAllOnInit()) {
            matrixTable.packAll();
        }

        if (matrixDialog != null && !matrixDialog.isVisible()) {
            matrixDialog.setLocationRelativeTo(null);
            matrixDialog.setLocation(matrixDialog.getLocation().x, 0);
            matrixDialog.setSize(800, 600);
            matrixDialog.setPreferredSize(matrixDialog.getSize());
            matrixDialog.pack();
            matrixDialog.setVisible(true);
        }
    }

    /**
     * Generates a new {@link IJCanvas} according to given {@link IJRoiManager}.
     * 
     * @param imp The {@link IJRoiManager}.
     * @return An {@link IJCanvas}.
     */
    protected IJCanvas generateImageCanvas(IJRoiManager imp) {
        return new IJCanvas(imp, this);
    }

    /**
     * Configures a newly created {@link IJCanvas} according to former existing one.
     * 
     * @param canvas The former {@link IJCanvas}.
     * @param newCanvas The newly created {@link IJCanvas}.
     */
    protected void setupNewCanvasFromOldCanvas(IJCanvas canvas, IJCanvas newCanvas) {
        int[] alignment = roiNameAlignment;
        if (alignment == null) {
            alignment = canvas.getRoiNameAlignment();
        }
        newCanvas.setRoiNameAlignment(alignment[0], alignment[1]);
    }

    /**
     * Used to instantiate a new canvas
     * 
     * @param imp
     */
    protected final void createImageCanvas(IJRoiManager imp) {
        IJCanvas canvas = imageCanvas;
        ImageJ tmp = null;
        if (imageCanvas != null) {
            tmp = imageCanvas.getHiddenIJ();
        }
        IJCanvas newCanvas = generateImageCanvas(imp);
        newCanvas.addPropertyChangeListener(this);
        // Avoid too many windows creation (DATAREDUC-322)
        if (tmp instanceof HiddenImageJ) {
            newCanvas.setHiddenIJ(tmp);
        } else {
            newCanvas.setHiddenIJ(new HiddenImageJ());
        }
        newCanvas.setBeamColor(beamColor);
        if (canvas != null) {
            setupNewCanvasFromOldCanvas(canvas, newCanvas);
            canvas.clean();
        }
        newCanvas.setMouseZoomMode(getMouseZoomMode());
        imageCanvas = newCanvas;

        updateImageCanvasMaxDrawingSize();

        // Mandatory hack to ensure mask drawing, roi size and image best size management.
        // Though it is a new Window creation, there is no way to avoid it.
        imp.setWindow(new HiddenImageWindow(imp, imageCanvas));
    }

    protected void initAxis() {
        // X axis
        AxisAttributes xAxisAttributes = new AxisAttributes(IChartViewer.HORIZONTAL_UP, getBackground());
        xAxisAttributes.setDrawOpposite(true);
        // important, we don't use autoscale built-in functionality
        xAxisAttributes.setAutoScale(false);
        xAxis = new HorizontalAxisView(xAxisAttributes, SwingConstants.BOTTOM, SwingConstants.TOP);
        xAxis.setTitlePosition(SwingConstants.TOP);
        xAxis.setAxisColor(axisColor);
        xAxis.setVisible(true);

        // Y axis
        AxisAttributes yAxisAttributes = new AxisAttributes(IChartViewer.VERTICAL_LEFT, getBackground());
        yAxisAttributes.setInverted(false);
        yAxisAttributes.setDrawOpposite(true);
        // important, we don't use autoscale built-in functionality
        yAxisAttributes.setAutoScale(false);
        yAxis = new VerticalAxisView(yAxisAttributes, SwingConstants.RIGHT, SwingConstants.LEFT);
        yAxis.setAxisColor(axisColor);
        yAxis.setVisible(true);
    }

    public void setAxisColor(Color color) {
        axisColor = color;
    }

    public Color getAxisColor() {
        return axisColor;
    }

    protected void initColorStuff() {
        // GradientViewer
        gradientViewer = new JGradientViewer();
        gradientViewer.setPreferredWidth(GRADIENT_WIDTH);
        gradientViewer.getAxis().getAttributes().setMinimum(0);
        gradientViewer.getAxis().getAttributes().setMaximum(65535);
        // Gradient
        gradient = gradientViewer.getGradient();// only to avoid creating a new object
        gradient.buildRainbowGradient();
        gradientViewer.setGradient(gradient);
        gColormap = gradient.buildColorMap(65536);

        gradientViewer.setToolTipText(MESSAGES.getString("ImageJViewer.GradientViewer.Tooltip"));
        gradientViewer.addMouseListener(this);
        updateGradientFormat(true);
    }

    @Override
    public void registerSectorClass(Class<?> sectorClass) {
        synchronized (possibleSectors) {
            if (sectorClass != null && MathematicSector.class.isAssignableFrom(sectorClass)
                    && !possibleSectors.contains(sectorClass)) {
                possibleSectors.add(sectorClass);
                if (isManageActionsAvailability()) {
                    getAction(NEW_SECTOR_ACTION).setEnabled(true);
                }
            }
        }
    }

    protected MathematicSector createSector(Class<?> sectorClass) {
        MathematicSector sector = null;
        if ((sectorClass != null) && MathematicSector.class.isAssignableFrom(sectorClass)) {
            Object object = null;
            try {
                Constructor<?> tmpConstructor = sectorClass.getConstructor(new Class[] {});
                object = tmpConstructor.newInstance(new Object[] {});
                if (object instanceof MathematicSector) {
                    sector = (MathematicSector) object;
                }
            } catch (Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to load sector from class " + sectorClass,
                        e);
            }
        }
        return sector;
    }

    protected void initActions() {
        // Actions shared by every instances of IJImage
        synchronized (ActionManager.getInstance()) {
            if (getAction(MODE_RECTANGLE_ACTION) == null) {
                AbstractActionExt modeRectangleAction = new AbstractActionExt() {

                    private static final long serialVersionUID = 2774991536486720379L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setRectangleMode();
                    }
                };
                modeRectangleAction.setStateAction();
                modeRectangleAction.setGroup(IJ_ACTION_GROUP);
                modeRectangleAction
                        .setShortDescription(MESSAGES.getString("ImageJViewer.Action.RectangleMode.Tooltip"));
                modeRectangleAction.setSmallIcon(RECTANGLE_ROI_ICON);
                modeRectangleAction.setActionCommand(MODE_RECTANGLE_ACTION);
                ActionManager.getInstance().addAction(modeRectangleAction);
            }
            if (getAction(MODE_ANGLE_ACTION) == null) {
                AbstractActionExt modeAngleAction = new AbstractActionExt() {

                    private static final long serialVersionUID = -2753099950774447512L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setAngleMode();
                    }
                };
                modeAngleAction.setStateAction();
                modeAngleAction.setGroup(IJ_ACTION_GROUP);
                modeAngleAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.Angle.Tooltip"));
                modeAngleAction.setSmallIcon(ANGLE_ROI_ICON);
                modeAngleAction.setActionCommand(MODE_ANGLE_ACTION);
                ActionManager.getInstance().addAction(modeAngleAction);
            }
            if (getAction(MODE_ELLIPSE_ACTION) == null) {

                AbstractActionExt modeEllipseAction = new AbstractActionExt() {

                    private static final long serialVersionUID = -2753099950774447512L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setEllipseMode();
                    }
                };
                modeEllipseAction.setStateAction();
                modeEllipseAction.setGroup(IJ_ACTION_GROUP);
                modeEllipseAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.EllipseMode.Tooltip"));
                modeEllipseAction.setSmallIcon(ELLIPSE_ROI_ICON);
                modeEllipseAction.setActionCommand(MODE_ELLIPSE_ACTION);
                ActionManager.getInstance().addAction(modeEllipseAction);
            }
            if (getAction(MODE_POLYGON_ACTION) == null) {
                AbstractActionExt modePolygonAction = new AbstractActionExt() {

                    private static final long serialVersionUID = -7118436662240123386L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setPolygonMode();
                    }
                };
                modePolygonAction.setStateAction();
                modePolygonAction.setGroup(IJ_ACTION_GROUP);
                modePolygonAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.PolygonMode.Tooltip"));
                modePolygonAction.setSmallIcon(POLYGON_ROI_ICON);
                modePolygonAction.setActionCommand(MODE_POLYGON_ACTION);
                ActionManager.getInstance().addAction(modePolygonAction);
            }
            if (getAction(MODE_FREE_HAND_ACTION) == null) {
                AbstractActionExt modeFreeHandAction = new AbstractActionExt() {

                    private static final long serialVersionUID = 8742620871884813273L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setFreeHandMode();
                    }
                };
                modeFreeHandAction.setStateAction();
                modeFreeHandAction.setGroup(IJ_ACTION_GROUP);
                modeFreeHandAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.FreeHandMode.Tooltip"));
                modeFreeHandAction.setSmallIcon(FREE_ROI_ICON);
                modeFreeHandAction.setActionCommand(MODE_FREE_HAND_ACTION);
                ActionManager.getInstance().addAction(modeFreeHandAction);
            }
            if (getAction(MODE_LINE_ACTION) == null) {
                AbstractActionExt modeLineAction = new AbstractActionExt() {

                    private static final long serialVersionUID = -754215639236967787L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setLineMode();
                    }
                };
                modeLineAction.setStateAction();
                modeLineAction.setGroup(IJ_ACTION_GROUP);
                modeLineAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.LineMode.Tooltip"));
                modeLineAction.setSmallIcon(LINE_ROI_ICON);
                modeLineAction.setActionCommand(MODE_LINE_ACTION);
                ActionManager.getInstance().addAction(modeLineAction);
            }
            if (getAction(MODE_POINT_ACTION) == null) {
                AbstractActionExt modePointAction = new AbstractActionExt() {

                    private static final long serialVersionUID = 8945957279153196460L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setPointMode();
                    }
                };
                modePointAction.setStateAction();
                modePointAction.setGroup(IJ_ACTION_GROUP);
                modePointAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.PointMode.Tooltip"));
                modePointAction.setSmallIcon(POINT_ROI_ICON);
                modePointAction.setActionCommand(MODE_POINT_ACTION);
                ActionManager.getInstance().addAction(modePointAction);
            }
            if (getAction(MODE_WAND_ACTION) == null) {
                AbstractActionExt modeWandAction = new AbstractActionExt() {

                    private static final long serialVersionUID = -5398533256928429334L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setWandMode();
                    }
                };
                modeWandAction.setStateAction();
                modeWandAction.setGroup(IJ_ACTION_GROUP);
                modeWandAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.WandMode.Tooltip"));
                modeWandAction.setSmallIcon(WAND_ROI_ICON);
                modeWandAction.setActionCommand(MODE_WAND_ACTION);
                ActionManager.getInstance().addAction(modeWandAction);
            }
            if (getAction(MODE_PAN_ACTION) == null) {
                AbstractActionExt modePanAction = new AbstractActionExt() {

                    private static final long serialVersionUID = 7322700920187109931L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setPanMode();
                    }
                };
                modePanAction.setStateAction();
                modePanAction.setGroup(IJ_ACTION_GROUP);
                modePanAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.PanMode.Tooltip"));
                modePanAction.setSmallIcon(PAN_MODE_ICON);
                modePanAction.setActionCommand(MODE_PAN_ACTION);
                ActionManager.getInstance().addAction(modePanAction);
            }
        }

        // Other actions

        AbstractActionExt deleteSelectedRoisAction = new AbstractActionExt() {

            private static final long serialVersionUID = 6031785875328958934L;

            @Override
            public void actionPerformed(ActionEvent e) {
                deleteSelectedRois();
            }
        };

        deleteSelectedRoisAction
                .setShortDescription(MESSAGES.getString("ImageJViewer.Action.DeleteSelectedRois.Tooltip"));
        deleteSelectedRoisAction.setSmallIcon(DELETE_SELECTED_ROIS_ICON);
        deleteSelectedRoisAction.setActionCommand(getTransformedActionCommand(DELETE_SELECTED_ROIS_ACTION));
        ActionManager.getInstance().addAction(deleteSelectedRoisAction);

        AbstractActionExt deleteAllRoisAction = new AbstractActionExt() {

            private static final long serialVersionUID = 6978188281528710353L;

            @Override
            public void actionPerformed(ActionEvent e) {
                deleteAllRois(false);
            }
        };

        deleteAllRoisAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.DeleteAllRois.Tooltip"));
        deleteAllRoisAction.setSmallIcon(DELETE_ALL_ROIS_ICON);
        deleteAllRoisAction.setActionCommand(getTransformedActionCommand(DELETE_ALL_ROIS_ACTION));
        ActionManager.getInstance().addAction(deleteAllRoisAction);

        AbstractActionExt zoomInAction = new AbstractActionExt() {

            private static final long serialVersionUID = -4270026063158886902L;

            @Override
            public void actionPerformed(ActionEvent e) {
                zoomIn();
            }
        };

        zoomInAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.ZoomIn.Tooltip"));
        zoomInAction.setSmallIcon(ZOOM_IN_ICON);
        zoomInAction.setActionCommand(getTransformedActionCommand(ZOOM_IN_ACTION));
        ActionManager.getInstance().addAction(zoomInAction);

        AbstractActionExt zoomOutAction = new AbstractActionExt() {

            private static final long serialVersionUID = -6775641831056753133L;

            @Override
            public void actionPerformed(ActionEvent e) {
                zoomOut();
            }
        };
        zoomOutAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.ZoomOut.Tooltip"));
        zoomOutAction.setSmallIcon(ZOOM_OUT_ICON);
        zoomOutAction.setActionCommand(getTransformedActionCommand(ZOOM_OUT_ACTION));
        ActionManager.getInstance().addAction(zoomOutAction);

        AbstractActionExt orSelectedRoisAction = new AbstractActionExt() {

            private static final long serialVersionUID = -8178497016453478321L;

            @Override
            public void actionPerformed(ActionEvent e) {
                orSelectedRois();
            }
        };
        orSelectedRoisAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.OrRois.Tooltip"));
        orSelectedRoisAction.setSmallIcon(OR_SELECTED_ROIS_ICON);
        orSelectedRoisAction.setActionCommand(getTransformedActionCommand(OR_SELECTED_ROIS_ACTION));
        ActionManager.getInstance().addAction(orSelectedRoisAction);

        AbstractActionExt andSelectedRoisAction = new AbstractActionExt() {

            private static final long serialVersionUID = -7225432656640341371L;

            @Override
            public void actionPerformed(ActionEvent e) {
                andSelectedRois();
            }
        };
        andSelectedRoisAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.AndRois.Tooltip"));
        andSelectedRoisAction.setSmallIcon(AND_SELECTED_ROIS_ICON);
        andSelectedRoisAction.setActionCommand(getTransformedActionCommand(AND_SELECTED_ROIS_ACTION));
        ActionManager.getInstance().addAction(andSelectedRoisAction);

        AbstractActionExt notSelectedRoisAction = new AbstractActionExt() {

            private static final long serialVersionUID = 2854415867342287982L;

            @Override
            public void actionPerformed(ActionEvent e) {
                notSelectedRois();
            }
        };
        notSelectedRoisAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.NotRois.Tooltip"));
        notSelectedRoisAction.setSmallIcon(NOT_SELECTED_ROIS_ICON);
        notSelectedRoisAction.setActionCommand(getTransformedActionCommand(NOT_SELECTED_ROIS_ACTION));
        ActionManager.getInstance().addAction(notSelectedRoisAction);

        AbstractActionExt xorSelectedRoisAction = new AbstractActionExt() {

            private static final long serialVersionUID = -2561504880121129682L;

            @Override
            public void actionPerformed(ActionEvent e) {
                xorSelectedRois();
            }
        };
        xorSelectedRoisAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.XorRois.Tooltip"));
        xorSelectedRoisAction.setSmallIcon(XOR_SELECTED_ROIS_ICON);
        xorSelectedRoisAction.setActionCommand(getTransformedActionCommand(XOR_SELECTED_ROIS_ACTION));
        ActionManager.getInstance().addAction(xorSelectedRoisAction);

        AbstractActionExt undoAction = new AbstractActionExt() {

            private static final long serialVersionUID = -6915845670575075963L;

            @Override
            public void actionPerformed(ActionEvent e) {
                undo();
            }
        };
        undoAction.setSmallIcon(UNDO_ICON);
        undoAction.setActionCommand(getTransformedActionCommand(UNDO_ACTION));
        ActionManager.getInstance().addAction(undoAction);

        AbstractActionExt redoAction = new AbstractActionExt() {

            private static final long serialVersionUID = 2563295896664360993L;

            @Override
            public void actionPerformed(ActionEvent e) {
                redo();
            }
        };
        redoAction.setSmallIcon(REDO_ICON);
        redoAction.setActionCommand(getTransformedActionCommand(REDO_ACTION));
        ActionManager.getInstance().addAction(redoAction);

        AbstractActionExt snapshotAction = new AbstractActionExt() {

            private static final long serialVersionUID = -2933141118856176086L;

            @Override
            public void actionPerformed(ActionEvent e) {
                saveImage();
            }
        };
        snapshotAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SaveImage.Tooltip"));
        snapshotAction.setSmallIcon(SNAPSHOT_ICON);
        snapshotAction.setName(MESSAGES.getString("ImageJViewer.Action.SaveImage.Text"));
        snapshotAction.setActionCommand(getTransformedActionCommand(SNAPSHOT_ACTION));
        ActionManager.getInstance().addAction(snapshotAction);

        AbstractActionExt printAction = new AbstractActionExt() {

            private static final long serialVersionUID = -1927022495658752785L;

            @Override
            public void actionPerformed(ActionEvent e) {
                printImage();
            }
        };
        printAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.PrintImage.Tooltip"));
        printAction.setSmallIcon(PRINT_ICON);
        printAction.setName(MESSAGES.getString("ImageJViewer.Action.PrintImage.Text"));
        printAction.setActionCommand(getTransformedActionCommand(PRINT_ACTION));
        ActionManager.getInstance().addAction(printAction);

        AbstractActionExt logScaleAction = new AbstractActionExt() {

            private static final long serialVersionUID = -6347176804535738175L;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof AbstractButton) {
                    setLogScale(((AbstractButton) e.getSource()).isSelected());
                }
            }
        };
        logScaleAction.setStateAction();
        logScaleAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.LogScale.Tooltip"));
        logScaleAction.setName(MESSAGES.getString("ImageJViewer.Action.LogScale.Text"));
        logScaleAction.setActionCommand(getTransformedActionCommand(LOG_SCALE_ACTION));
        logScaleAction.setSelected(false);
        ActionManager.getInstance().addAction(logScaleAction);

        AbstractActionExt fitMaxSizeAction = new AbstractActionExt() {

            private static final long serialVersionUID = -6347176804535738175L;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof AbstractButton) {
                    setAlwaysFitMaxSize(((AbstractButton) e.getSource()).isSelected());
                }
            }
        };
        fitMaxSizeAction.setStateAction();
        fitMaxSizeAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.FitMaxSize.Tooltip"));
        fitMaxSizeAction.setName(MESSAGES.getString("ImageJViewer.Action.FitMaxSize.Text"));
        fitMaxSizeAction.setSmallIcon(FIT_MAX_SIZE_ICON);
        fitMaxSizeAction.setActionCommand(getTransformedActionCommand(FIT_MAX_SIZE_ACTION));
        ActionManager.getInstance().addAction(fitMaxSizeAction);

        AbstractActionExt autoBestFitAction = new AbstractActionExt() {

            private static final long serialVersionUID = -6514389705087548288L;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof AbstractButton) {
                    setAutoBestFit(!isAutoBestFit());
                }
            }
        };
        autoBestFitAction.setStateAction();
        autoBestFitAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.AutoFit.Tooltip"));
        autoBestFitAction.setName(MESSAGES.getString("ImageJViewer.Action.AutoFit.Text"));
        autoBestFitAction.setActionCommand(getTransformedActionCommand(SET_AUTO_BEST_FIT_ACTION));
        autoBestFitAction.setSelected(isAutoBestFit());
        ActionManager.getInstance().addAction(autoBestFitAction);

        AbstractActionExt showTableAction = new AbstractActionExt() {

            private static final long serialVersionUID = 7169175409566835515L;

            @Override
            public void actionPerformed(ActionEvent e) {
                showTable();
            }

        };
        showTableAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.ShowTable.Tooltip"));
        showTableAction.setSmallIcon(SHOW_TABLE_ICON);
        showTableAction.setName(MESSAGES.getString("ImageJViewer.Action.ShowTable.Text"));
        showTableAction.setActionCommand(getTransformedActionCommand(SHOW_TABLE_ACTION));
        ActionManager.getInstance().addAction(showTableAction);

        AbstractActionExt saveDataAction = new AbstractActionExt() {

            private static final long serialVersionUID = 7169175409566835515L;

            @Override
            public void actionPerformed(ActionEvent e) {
                saveDataFile();
            }

        };
        saveDataAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SaveData.Tooltip"));
        saveDataAction.setName(MESSAGES.getString("ImageJViewer.Action.SaveData.Text"));
        saveDataAction.setSmallIcon(SAVE_DATA_ICON);
        saveDataAction.setActionCommand(getTransformedActionCommand(SAVE_DATA_ACTION));
        ActionManager.getInstance().addAction(saveDataAction);

        AbstractActionExt loadDataAction = new AbstractActionExt() {

            private static final long serialVersionUID = 7169175409566835515L;

            @Override
            public void actionPerformed(ActionEvent e) {
                loadDataFile();
            }

        };
        loadDataAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.LoadData.Tooltip"));
        loadDataAction.setName(MESSAGES.getString("ImageJViewer.Action.LoadData.Text"));
        loadDataAction.setSmallIcon(LOAD_DATA_ICON);
        loadDataAction.setActionCommand(getTransformedActionCommand(LOAD_DATA_ACTION));
        ActionManager.getInstance().addAction(loadDataAction);

        loadDataVisibleCheckBox = new AbstractActionExt() {

            private static final long serialVersionUID = -6514389705087548288L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setLoadDataVisible(false);
            }
        };
        loadDataVisibleCheckBox.setStateAction();
        loadDataVisibleCheckBox.setShortDescription(MESSAGES.getString("ImageJViewer.Action.LogValues.Tooltip"));
        loadDataVisibleCheckBox.setName(MESSAGES.getString("ImageJViewer.Action.LogValues.Text"));
        loadDataVisibleCheckBox.setActionCommand(getTransformedActionCommand(LOAD_DATA_VISIBLE_ACTION));
        ActionManager.getInstance().addAction(loadDataVisibleCheckBox);

        AbstractActionExt saveSettingsAction = new AbstractActionExt() {
            private static final long serialVersionUID = 2915990817578380000L;

            @Override
            public void actionPerformed(ActionEvent e) {
                saveSettings();
            }
        };
        saveSettingsAction.setName(MESSAGES.getString("ImageJViewer.Action.SaveSettings.Text"));
        saveSettingsAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SaveSettings.Tooltip"));
        saveSettingsAction.setSmallIcon(SAVE_SETTINGS_ICON);
        saveSettingsAction.setActionCommand(getTransformedActionCommand(SAVE_SETTINGS_ACTION));
        ActionManager.getInstance().addAction(saveSettingsAction);

        AbstractActionExt loadSettingsAction = new AbstractActionExt() {
            private static final long serialVersionUID = -4975901650291369960L;

            @Override
            public void actionPerformed(ActionEvent e) {
                loadSettings();
            }
        };
        loadSettingsAction.setName(MESSAGES.getString("ImageJViewer.Action.LoadSettings.Text"));
        loadSettingsAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.LoadSettings.Tooltip"));
        loadSettingsAction.setSmallIcon(LOAD_SETTINGS_ICON);
        loadSettingsAction.setActionCommand(getTransformedActionCommand(LOAD_SETTINGS_ACTION));
        ActionManager.getInstance().addAction(loadSettingsAction);

        AbstractActionExt innerRoiAction = new AbstractActionExt() {

            private static final long serialVersionUID = 175687121977539585L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setInner();
            }

        };
        innerRoiAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetInnerRoi.Tooltip"));
        innerRoiAction.setSmallIcon(INNER_ROIS_ICON);
        innerRoiAction.setName(null);
        innerRoiAction.setActionCommand(getTransformedActionCommand(INNER_ROIS_ACTION));
        ActionManager.getInstance().addAction(innerRoiAction);

        AbstractActionExt outerRoiAction = new AbstractActionExt() {

            private static final long serialVersionUID = 3335300007811609645L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setOuter();
            }

        };
        outerRoiAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetOuterRoi.Tooltip"));
        outerRoiAction.setSmallIcon(OUTER_ROIS_ICON);
        outerRoiAction.setName(null);
        outerRoiAction.setActionCommand(getTransformedActionCommand(OUTER_ROIS_ACTION));
        ActionManager.getInstance().addAction(outerRoiAction);

        AbstractActionExt invalidateRoiAction = new AbstractActionExt() {

            private static final long serialVersionUID = 5221805056100015778L;

            @Override
            public void actionPerformed(ActionEvent e) {
                invalidateRoi();
            }

        };
        invalidateRoiAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.InvalidateRoi.Tooltip"));
        invalidateRoiAction.setSmallIcon(INVALIDATE_ROIS_ICON);
        invalidateRoiAction.setName(null);
        invalidateRoiAction.setActionCommand(getTransformedActionCommand(INVALIDATE_ROIS_ACTION));
        ActionManager.getInstance().addAction(invalidateRoiAction);

        AbstractActionExt setAsMaskAction = new AbstractActionExt() {

            private static final long serialVersionUID = 5221805056100015778L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setAsMask();
            }

        };
        setAsMaskAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetMask.Tooltip"));
        setAsMaskAction.setSmallIcon(SET_AS_MASK_ICON);
        setAsMaskAction.setName(null);
        setAsMaskAction.setActionCommand(getTransformedActionCommand(SET_AS_MASK_ACTION));
        ActionManager.getInstance().addAction(setAsMaskAction);

        AbstractActionExt addToMaskAction = new AbstractActionExt() {

            private static final long serialVersionUID = -3327196705925036670L;

            @Override
            public void actionPerformed(ActionEvent e) {
                addToMask();
            }

        };
        addToMaskAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.AddToMask.Tooltip"));
        addToMaskAction.setSmallIcon(ADD_TO_MASK_ICON);
        addToMaskAction.setName(null);
        addToMaskAction.setActionCommand(getTransformedActionCommand(ADD_TO_MASK_ACTION));
        ActionManager.getInstance().addAction(addToMaskAction);

        AbstractActionExt removeFromMaskAction = new AbstractActionExt() {

            private static final long serialVersionUID = 276607768405397361L;

            @Override
            public void actionPerformed(ActionEvent e) {
                removeFromMask();
            }

        };
        removeFromMaskAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.RemoveFromMask.Tooltip"));
        removeFromMaskAction.setSmallIcon(REMOVE_FROM_MASK_ICON);
        removeFromMaskAction.setName(null);
        removeFromMaskAction.setActionCommand(getTransformedActionCommand(REMOVE_FROM_MASK_ACTION));
        ActionManager.getInstance().addAction(removeFromMaskAction);

        AbstractActionExt clearMaskAction = new AbstractActionExt() {

            private static final long serialVersionUID = 2300467961087380371L;

            @Override
            public void actionPerformed(ActionEvent e) {
                clearMask();
            }

        };
        clearMaskAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.ClearMask.Tooltip"));
        clearMaskAction.setSmallIcon(CLEAR_MASK_ICON);
        clearMaskAction.setName(null);
        clearMaskAction.setActionCommand(getTransformedActionCommand(CLEAR_MASK_ACTION));
        ActionManager.getInstance().addAction(clearMaskAction);

        AbstractActionExt saveMaskAction = new AbstractActionExt() {

            private static final long serialVersionUID = 6672734022304650928L;

            @Override
            public void actionPerformed(ActionEvent e) {
                saveMask();
            }

        };
        saveMaskAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SaveMask.Tooltip"));
        saveMaskAction.setSmallIcon(SAVE_MASK_ICON);
        saveMaskAction.setName(null);
        saveMaskAction.setActionCommand(getTransformedActionCommand(SAVE_MASK_ACTION));
        ActionManager.getInstance().addAction(saveMaskAction);

        AbstractActionExt loadMaskAction = new AbstractActionExt() {

            private static final long serialVersionUID = 9062630625284741142L;

            @Override
            public void actionPerformed(ActionEvent e) {
                loadMask(false);
            }

        };
        loadMaskAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.LoadMask.Tooltip"));
        loadMaskAction.setSmallIcon(LOAD_MASK_ICON);
        loadMaskAction.setName(null);
        loadMaskAction.setActionCommand(getTransformedActionCommand(LOAD_MASK_ACTION));
        ActionManager.getInstance().addAction(loadMaskAction);

        AbstractActionExt loadMaskAddAction = new AbstractActionExt() {

            private static final long serialVersionUID = 5692180590556408810L;

            @Override
            public void actionPerformed(ActionEvent e) {
                loadMask(true);
            }

        };
        loadMaskAddAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.LoadMask.Add.Tooltip"));
        loadMaskAddAction.setSmallIcon(LOAD_MASK_ADD_ICON);
        loadMaskAddAction.setName(null);
        loadMaskAddAction.setActionCommand(getTransformedActionCommand(LOAD_MASK_ADD_ACTION));
        ActionManager.getInstance().addAction(loadMaskAddAction);

        AbstractActionExt setMaskColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = 7429896543817271207L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeMaskColor();
            }

        };
        setMaskColorAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetMaskColor.Tooltip"));
        setMaskColorAction.setName(MESSAGES.getString("ImageJViewer.Action.SetMaskColor.Text"));
        setMaskColorAction.setSmallIcon(SET_MASK_COLOR_ICON);
        setMaskColorAction.setActionCommand(getTransformedActionCommand(SET_MASK_COLOR_ACTION));
        ActionManager.getInstance().addAction(setMaskColorAction);

        AbstractActionExt setBeamPositionColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = 7169175409566835515L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeBeamPositionColor();
            }

        };
        setBeamPositionColorAction
                .setShortDescription(applyCenterName("ImageJViewer.Action.SetBeamPositionColor.Tooltip"));
        setBeamPositionColorAction.setName(applyCenterName("ImageJViewer.Action.SetBeamPositionColor.Text"));
        setBeamPositionColorAction.setSmallIcon(SET_BEAM_POSITION_COLOR_ICON);
        setBeamPositionColorAction.setActionCommand(getTransformedActionCommand(SET_BEAM_POSITION_COLOR_ACTION));
        ActionManager.getInstance().addAction(setBeamPositionColorAction);

        AbstractActionExt packTableAction = new AbstractActionExt() {

            private static final long serialVersionUID = -7513266883050082853L;

            @Override
            public void actionPerformed(ActionEvent e) {
                packTable();
            }

        };
        packTableAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.PackTable.Text"));
        packTableAction.setSmallIcon(PACK_TABLE_ICON);
        packTableAction.setName(MESSAGES.getString("ImageJViewer.Action.PackTable.Text"));
        packTableAction.setActionCommand(getTransformedActionCommand(PACK_TABLE_ACTION));
        ActionManager.getInstance().addAction(packTableAction);

        AbstractActionExt unZoomAction = new AbstractActionExt() {

            private static final long serialVersionUID = 4325567901446116477L;

            @Override
            public void actionPerformed(ActionEvent e) {
                unZoom();
            }

        };
        unZoomAction.setSmallIcon(UNZOOM_ICON);
        unZoomAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.UnZoom.Tooltip"));
        unZoomAction.setActionCommand(getTransformedActionCommand(UNZOOM_ACTION));
        ActionManager.getInstance().addAction(unZoomAction);

        AbstractActionExt changeRoiColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = -3575067379697743184L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeRoiColor();
            }

        };
        changeRoiColorAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetRoiColor.Text"));
        changeRoiColorAction.setName(MESSAGES.getString("ImageJViewer.Action.SetRoiColor.Text"));
        changeRoiColorAction.setActionCommand(getTransformedActionCommand(CHANGE_ROI_COLOR_ACTION));
        ActionManager.getInstance().addAction(changeRoiColorAction);

        AbstractActionExt changeSelectedRoiColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = 2717524000087346082L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeSelectedRoiColor();
            }

        };
        changeSelectedRoiColorAction
                .setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetSelectedRoiColor.Text"));
        changeSelectedRoiColorAction.setName(MESSAGES.getString("ImageJViewer.Action.SetSelectedRoiColor.Text"));
        changeSelectedRoiColorAction.setActionCommand(getTransformedActionCommand(CHANGE_SELECTED_ROI_COLOR_ACTION));
        ActionManager.getInstance().addAction(changeSelectedRoiColorAction);

        AbstractActionExt changeHoveredRoiColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = 7539210432851094582L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeHoveredRoiColor();
            }

        };
        changeHoveredRoiColorAction
                .setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetHoveredRoiColor.Text"));
        changeHoveredRoiColorAction.setName(MESSAGES.getString("ImageJViewer.Action.SetHoveredRoiColor.Text"));
        changeHoveredRoiColorAction.setActionCommand(getTransformedActionCommand(CHANGE_HOVERED_ROI_COLOR_ACTION));
        ActionManager.getInstance().addAction(changeHoveredRoiColorAction);

        AbstractActionExt changeInnerRoiColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = 97838463541426891L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeInnerRoiColor();
            }

        };
        changeInnerRoiColorAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetInnerRoiColor.Text"));
        changeInnerRoiColorAction.setName(MESSAGES.getString("ImageJViewer.Action.SetInnerRoiColor.Text"));
        changeInnerRoiColorAction.setActionCommand(getTransformedActionCommand(CHANGE_INNER_ROI_COLOR_ACTION));
        ActionManager.getInstance().addAction(changeInnerRoiColorAction);

        AbstractActionExt changeSelectedInnerRoiColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = -4194029028899819777L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeSelectedInnerRoiColor();
            }

        };
        changeSelectedInnerRoiColorAction
                .setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetSelectedInnerRoiColor.Text"));
        changeSelectedInnerRoiColorAction
                .setName(MESSAGES.getString("ImageJViewer.Action.SetSelectedInnerRoiColor.Text"));
        changeSelectedInnerRoiColorAction
                .setActionCommand(getTransformedActionCommand(CHANGE_SELECTED_INNER_ROI_COLOR_ACTION));
        ActionManager.getInstance().addAction(changeSelectedInnerRoiColorAction);

        AbstractActionExt changeHoveredInnerRoiColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = -4474205353175704146L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeHoveredInnerRoiColor();
            }

        };
        changeHoveredInnerRoiColorAction
                .setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetHoveredInnerRoiColor.Text"));
        changeHoveredInnerRoiColorAction
                .setName(MESSAGES.getString("ImageJViewer.Action.SetHoveredInnerRoiColor.Text"));
        changeHoveredInnerRoiColorAction
                .setActionCommand(getTransformedActionCommand(CHANGE_HOVERED_INNER_ROI_COLOR_ACTION));
        ActionManager.getInstance().addAction(changeHoveredInnerRoiColorAction);

        AbstractActionExt changeOuterRoiColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = 8223515770218660826L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeOuterRoiColor();
            }

        };
        changeOuterRoiColorAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetOuterRoiColor.Text"));
        changeOuterRoiColorAction.setName(MESSAGES.getString("ImageJViewer.Action.SetOuterRoiColor.Text"));
        changeOuterRoiColorAction.setActionCommand(getTransformedActionCommand(CHANGE_OUTER_ROI_COLOR_ACTION));
        ActionManager.getInstance().addAction(changeOuterRoiColorAction);

        AbstractActionExt changeSelectedOuterRoiColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = 4947061580378441242L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeSelectedOuterRoiColor();
            }

        };
        changeSelectedOuterRoiColorAction
                .setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetSelectedOuterRoiColor.Text"));
        changeSelectedOuterRoiColorAction
                .setName(MESSAGES.getString("ImageJViewer.Action.SetSelectedOuterRoiColor.Text"));
        changeSelectedOuterRoiColorAction
                .setActionCommand(getTransformedActionCommand(CHANGE_SELECTED_OUTER_ROI_COLOR_ACTION));
        ActionManager.getInstance().addAction(changeSelectedOuterRoiColorAction);

        AbstractActionExt changeHoveredOuterRoiColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = -7821617318929786481L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeHoveredOuterRoiColor();
            }

        };
        changeHoveredOuterRoiColorAction
                .setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetHoveredOuterRoiColor.Text"));
        changeHoveredOuterRoiColorAction
                .setName(MESSAGES.getString("ImageJViewer.Action.SetHoveredOuterRoiColor.Text"));
        changeHoveredOuterRoiColorAction
                .setActionCommand(getTransformedActionCommand(CHANGE_HOVERED_OUTER_ROI_COLOR_ACTION));
        ActionManager.getInstance().addAction(changeHoveredOuterRoiColorAction);

        AbstractActionExt displayRoiColorsMenuAction = new AbstractActionExt() {

            private static final long serialVersionUID = 4214270213394009239L;

            @Override
            public void actionPerformed(ActionEvent e) {
                initRoiColorsPopupMenu();
                Component comp = (Component) e.getSource();
                int x = 0, y = 0;
                if (!comp.isShowing()) {
                    comp = CometeUtils.getWindowForComponent(ImageViewer.this);
                    Point position = comp.getMousePosition();
                    if (position == null) {
                        comp = ImageViewer.this;
                    } else {
                        x = position.x;
                        y = position.y;
                    }
                }
                roiColorsPopupMenu.show(comp, x, y);
            }

        };
        displayRoiColorsMenuAction.setName(MESSAGES.getString("ImageJViewer.Action.SetRoisColors.Tooltip"));
        displayRoiColorsMenuAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetRoisColors.Tooltip"));
        displayRoiColorsMenuAction.setSmallIcon(SET_ROI_COLORS_ICON);
        displayRoiColorsMenuAction.setActionCommand(getTransformedActionCommand(DISPLAY_ROI_COLORS_MENU_ACTION));
        ActionManager.getInstance().addAction(displayRoiColorsMenuAction);

        AbstractActionExt newSectorAction = new AbstractActionExt() {

            private static final long serialVersionUID = 6539748180167793724L;

            @Override
            public void actionPerformed(ActionEvent e) {
                displaySectorSelectionDialog();
            }

        };
        newSectorAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.NewSector.Text"));
        newSectorAction.setSmallIcon(NEW_SECTOR_ICON);
        newSectorAction.setActionCommand(getTransformedActionCommand(NEW_SECTOR_ACTION));
        newSectorAction.setEnabled(false);
        ActionManager.getInstance().addAction(newSectorAction);

        AbstractActionExt deleteSectorAction = new AbstractActionExt() {

            private static final long serialVersionUID = -2281820243456274076L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setSector(null);
            }
        };
        deleteSectorAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.DeleteSector.Text"));
        deleteSectorAction.setSmallIcon(DELETE_SECTOR_ICON);
        deleteSectorAction.setActionCommand(getTransformedActionCommand(DELETE_SECTOR_ACTION));
        deleteSectorAction.setEnabled(false);
        ActionManager.getInstance().addAction(deleteSectorAction);

        AbstractActionExt editSectorAction = new AbstractActionExt() {

            private static final long serialVersionUID = -2492584106918465183L;

            @Override
            public void actionPerformed(ActionEvent e) {
                displayEditSectorDialog();
            }
        };
        editSectorAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.EditSector.Text"));
        editSectorAction.setSmallIcon(EDIT_SECTOR_ICON);
        editSectorAction.setActionCommand(getTransformedActionCommand(EDIT_SECTOR_ACTION));
        editSectorAction.setEnabled(false);
        ActionManager.getInstance().addAction(editSectorAction);

        AbstractActionExt previewSectorAction = new AbstractActionExt() {

            private static final long serialVersionUID = -2045360345824953979L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setPreviewSector(!isPreviewSector());
            }
        };
        previewSectorAction.setSelected(isPreviewSector());
        previewSectorAction.setShortDescription(MESSAGES.getString("ImageJViewer.Action.PreviewSector.Tooltip"));
        previewSectorAction.setSmallIcon(PREVIEW_SECTOR_ICON);
        previewSectorAction.setActionCommand(getTransformedActionCommand(PREVIEW_SECTOR_ACTION));
        previewSectorAction.setStateAction();
        ActionManager.getInstance().addAction(previewSectorAction);

        AbstractActionExt changeEditingSectorColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = -8876693343501734412L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeEditingSectorColor();
            }

        };
        changeEditingSectorColorAction
                .setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetSectorColor.Editing.Text"));
        changeEditingSectorColorAction.setName(MESSAGES.getString("ImageJViewer.Action.SetSectorColor.Editing.Text"));
        changeEditingSectorColorAction
                .setActionCommand(getTransformedActionCommand(CHANGE_EDITING_SECTOR_COLOR_ACTION));
        ActionManager.getInstance().addAction(changeEditingSectorColorAction);

        AbstractActionExt changeValidatedSectorColorAction = new AbstractActionExt() {

            private static final long serialVersionUID = -8160818372426178517L;

            @Override
            public void actionPerformed(ActionEvent e) {
                changeValidatedSectorColor();
            }

        };
        changeValidatedSectorColorAction
                .setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetSectorColor.Valid.Text"));
        changeValidatedSectorColorAction.setName(MESSAGES.getString("ImageJViewer.Action.SetSectorColor.Valid.Text"));
        changeValidatedSectorColorAction
                .setActionCommand(getTransformedActionCommand(CHANGE_VALIDATED_SECTOR_COLOR_ACTION));
        ActionManager.getInstance().addAction(changeValidatedSectorColorAction);

        AbstractActionExt displaySectorColorsMenuAction = new AbstractActionExt() {

            private static final long serialVersionUID = 2553712109043272857L;

            @Override
            public void actionPerformed(ActionEvent e) {
                initSectorColorsPopupMenu();
                Component comp = (Component) e.getSource();
                int x = 0, y = 0;
                if (!comp.isShowing()) {
                    comp = CometeUtils.getWindowForComponent(ImageViewer.this);
                    Point position = comp.getMousePosition();
                    if (position == null) {
                        comp = ImageViewer.this;
                    } else {
                        x = position.x;
                        y = position.y;
                    }
                }
                sectorColorsPopupMenu.show(comp, x, y);
            }

        };
        displaySectorColorsMenuAction
                .setShortDescription(MESSAGES.getString("ImageJViewer.Action.SetSectorColor.Tooltip"));
        displaySectorColorsMenuAction.setName(MESSAGES.getString("ImageJViewer.Action.SetSectorColor.Tooltip"));
        displaySectorColorsMenuAction.setSmallIcon(SET_SECTOR_COLORS_ICON);
        displaySectorColorsMenuAction.setActionCommand(getTransformedActionCommand(DISPLAY_SECTOR_COLORS_MENU_ACTION));
        ActionManager.getInstance().addAction(displaySectorColorsMenuAction);

        AbstractActionExt displayBeamCenterAction = new AbstractActionExt() {
            private static final long serialVersionUID = -7690373028817545469L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setDrawBeamPosition(!isDrawBeamPosition());
            }
        };
        displayBeamCenterAction.setName(applyCenterName("ImageJViewer.Action.DisplayBeamPosition.Text"));
        displayBeamCenterAction.setSmallIcon(SHOW_CENTER_ICON);
        displayBeamCenterAction.setActionCommand(getTransformedActionCommand(DISPLAY_BEAM_CENTER_ACTION));
        displayBeamCenterAction.setStateAction();
        displayBeamCenterAction.setSelected(isDrawBeamPosition());
        ActionManager.getInstance().addAction(displayBeamCenterAction);

        AbstractActionExt allowBeamCenterByClickAction = new AbstractActionExt() {
            private static final long serialVersionUID = -7690373028817545469L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setAllowBeamPositionByClick(!isAllowBeamPositionByClick());
            }
        };
        allowBeamCenterByClickAction.setName(applyCenterName("ImageJViewer.Action.AllowBeamCenterByClickAction.Text"));
        allowBeamCenterByClickAction.setSmallIcon(CLICK_CENTER_ICON);
        allowBeamCenterByClickAction
                .setShortDescription(applyCenterName("ImageJViewer.Action.AllowBeamCenterByClickAction.Tooltip"));
        allowBeamCenterByClickAction.setActionCommand(getTransformedActionCommand(ALLOW_BEAM_CENTER_BY_CLICK_ACTION));
        allowBeamCenterByClickAction.setStateAction();
        allowBeamCenterByClickAction.setSelected(isAllowBeamPositionByClick());
        ActionManager.getInstance().addAction(allowBeamCenterByClickAction);

        String mouseWheelZoomGroup = getTransformedActionCommand(MOUSE_WHEEL_ZOOM_GROUP);
        AbstractActionExt mouseWheelZoomDefaultAction = new AbstractActionExt() {

            private static final long serialVersionUID = -6894909431857045088L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setMouseZoomMode(MouseZoomMode.NONE);
            }
        };
        mouseWheelZoomDefaultAction.setName(MESSAGES.getString("ImageJViewer.Action.Zoom.MouseWheel.Default.Text"));
        mouseWheelZoomDefaultAction.setActionCommand(getTransformedActionCommand(MOUSE_WHEEL_ZOOM_DEFAULT));
        mouseWheelZoomDefaultAction.setStateAction();
        mouseWheelZoomDefaultAction.setGroup(mouseWheelZoomGroup);
        ActionManager.getInstance().addAction(mouseWheelZoomDefaultAction);
        AbstractActionExt mouseWheelZoomCenterAction = new AbstractActionExt() {

            private static final long serialVersionUID = -9189836178988790076L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setMouseZoomMode(MouseZoomMode.CENTERED);
            }
        };
        mouseWheelZoomCenterAction.setName(MESSAGES.getString("ImageJViewer.Action.Zoom.MouseWheel.Center.Text"));
        mouseWheelZoomCenterAction.setActionCommand(getTransformedActionCommand(MOUSE_WHEEL_ZOOM_CENTER));
        mouseWheelZoomCenterAction.setStateAction();
        mouseWheelZoomCenterAction.setGroup(mouseWheelZoomGroup);
        ActionManager.getInstance().addAction(mouseWheelZoomCenterAction);
        AbstractActionExt mouseWheelZoomPixelAction = new AbstractActionExt() {

            private static final long serialVersionUID = -9096429960193550982L;

            @Override
            public void actionPerformed(ActionEvent e) {
                setMouseZoomMode(MouseZoomMode.PIXEL);
            }
        };
        mouseWheelZoomPixelAction.setName(MESSAGES.getString("ImageJViewer.Action.Zoom.MouseWheel.Pixel.Text"));
        mouseWheelZoomPixelAction.setActionCommand(getTransformedActionCommand(MOUSE_WHEEL_ZOOM_PIXEL));
        mouseWheelZoomPixelAction.setStateAction();
        mouseWheelZoomPixelAction.setGroup(mouseWheelZoomGroup);
        ActionManager.getInstance().addAction(mouseWheelZoomPixelAction);

        updateUndoRedoActionState();
    }

    protected void initToolBar() {
        if (toolBar == null) {
            toolBar = createDefaultToolBar();
        }
    }

    protected void initToolBarScrollPane() {
        if (toolBarScrollPane == null) {
            toolBarScrollPane = new JScrollPane(toolBar);
            toolBarScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
            toolBarScrollPane.setMinimumSize(new Dimension(0, toolBarScrollPane.getPreferredSize().height));
        }
    }

    public boolean isToolBarVisible() {
        return toolBarScrollPane.isVisible();
    }

    public void setToolBarVisible(boolean visible) {
        toolBarScrollPane.setVisible(visible);
    }

    public void saveDataFile() {
        String filename = CometeUtils.showFileChooserDialog(this, false);
        saveDataFile(filename);
    }

    @Override
    public void saveDataFile(String fileName) {
        if (fileName != null && !fileName.trim().isEmpty()) {
            if (value != null) {
                FileArrayUtils.saveFlatMatrixToFile(fileName, value, ArrayUtils.recoverDataType(value), dimX,
                        getValueSeparator(), isSaveColumnIndex());
                transferEventDelegate.changeDataLocation(fileName, true);
            }
        }
    }

    protected String applyCenterName(String key) {
        return String.format(MESSAGES.getString(key), getCenterName());
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        if ((centerName == null) || (centerName.trim().isEmpty())) {
            this.centerName = MESSAGES.getString("ImageJViewer.BeamCenter");
        } else {
            this.centerName = centerName;
            AbstractActionExt setBeamPositionColorAction = getAction(SET_BEAM_POSITION_COLOR_ACTION);
            setBeamPositionColorAction
                    .setShortDescription(applyCenterName("ImageJViewer.Action.SetBeamPositionColor.Tooltip"));
            setBeamPositionColorAction.setName(applyCenterName("ImageJViewer.Action.SetBeamPositionColor.Text"));
            AbstractActionExt displayBeamCenterAction = getAction(DISPLAY_BEAM_CENTER_ACTION);
            displayBeamCenterAction.setName(applyCenterName("ImageJViewer.Action.DisplayBeamPosition.Text"));
            AbstractActionExt allowBeamCenterByClickAction = getAction(ALLOW_BEAM_CENTER_BY_CLICK_ACTION);
            allowBeamCenterByClickAction
                    .setName(applyCenterName("ImageJViewer.Action.AllowBeamCenterByClickAction.Text"));
            allowBeamCenterByClickAction
                    .setShortDescription(applyCenterName("ImageJViewer.Action.AllowBeamCenterByClickAction.Tooltip"));
        }
        updateTooltip();
    }

    public boolean isSaveColumnIndex() {
        return saveColumnIndex;
    }

    public void setSaveColumnIndex(boolean saveColumnIndex) {
        this.saveColumnIndex = saveColumnIndex;
    }

    public String getValueSeparator() {
        return valueSeparator;
    }

    public void setValueSeparator(String valueSeparator) {
        if ((valueSeparator == null) || (valueSeparator.isEmpty())) {
            this.valueSeparator = DEFAULT_VALUE_SEPARATOR;
        } else {
            this.valueSeparator = valueSeparator;
        }
    }

    public boolean isLoadDataVisible() {
        return loadDataVisible;
    }

    public void setLoadDataVisible(boolean loadDataVisible) {
        this.loadDataVisible = loadDataVisible;

        if (loadMatrix == null) {
            loadDataVisibleCheckBox.setEnabled(false);
            loadDataVisible = false;
        } else {
            loadDataVisibleCheckBox.setEnabled(true);
        }

        if (loadDataVisible && loadMatrix != null) {
            if (value != null) {
                originalMatrix = new FlatMatrix<Object>();
                originalMatrix.setValue(getDimX(), value);
            }
            int width = loadMatrix.getColumns();
            int height = 0;
            if (width > 0 && loadMatrix.getValue() != null) {
                height = Array.getLength(loadMatrix.getValue()) / width;
            }
            setFlatNumberMatrix(loadMatrix.getValue(), width, height);
        }
        if (!loadDataVisible && originalMatrix != null) {
            int width = originalMatrix.getColumns();
            int height = 0;
            if (width > 0 && originalMatrix.getValue() != null) {
                height = Array.getLength(originalMatrix.getValue()) / width;
            }
            setFlatNumberMatrix(originalMatrix.getValue(), width, height);
            originalMatrix = null;
        }

        loadDataVisibleCheckBox.setSelected(loadDataVisible);
    }

    public void loadDataFile() {
        String filename = CometeUtils.showFileChooserDialog(this, true);
        loadDataFile(filename);
    }

    @Override
    public void loadDataFile(String loadDataFile) {
        if (loadDataFile != null) {
            setLoadedMatrix(FileArrayUtils.loadTypeFlatMatrixFromFile(loadDataFile, Double.TYPE, valueSeparator,
                    saveColumnIndex));
            transferEventDelegate.changeDataLocation(loadDataFile, false);
        }
    }

    public void setLoadedMatrix(FlatMatrix<Object> flatMatrix) {
        loadMatrix = flatMatrix;
        setLoadDataVisible(true);
        matrixTable.setLoadedMatrix(flatMatrix);
    }

    @Override
    public String getDataDirectory() {
        return transferEventDelegate.getDataDirectory();
    }

    @Override
    public void setDataDirectory(String path) {
        transferEventDelegate.setDataDirectory(path);
    }

    @Override
    public String getDataFile() {
        return transferEventDelegate.getDataFile();
    }

    @Override
    public void addDataTransferListener(IDataTransferListener listener) {
        transferEventDelegate.addDataTransferListener(listener);
    }

    @Override
    public void removeDataTransferListener(IDataTransferListener listener) {
        transferEventDelegate.removeDataTransferListener(listener);
    }

    protected void initPopupMenu() {
        popupMenu = new JPopupMenu();

        String title = MESSAGES.getString("ImageJViewer.Popup.Title");
        if (title != null && title.length() > 0) {
            JPanel titlePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
            JLabel label = new JLabel(" " + title + " ");
            titlePanel.add(label);
            titlePanel.setBackground(POPUP_MENU_PANEL_COLOR);
            label.setForeground(Color.WHITE);
            popupMenu.add(titlePanel);
            popupMenu.addSeparator();
        }
        popupMenu.addSeparator();

        popupMenu.add(containerFactory.createMenuItem(getAction(LOG_SCALE_ACTION)));
        popupMenu.add(createFitMaxSizeItem(popupMenu));
        popupMenu.addSeparator();
        popupMenu.add(containerFactory.createMenuItem(getAction(SNAPSHOT_ACTION)));
        popupMenu.add(containerFactory.createMenuItem(getAction(PRINT_ACTION)));

        popupMenu.add(containerFactory.createMenuItem(getAction(SHOW_TABLE_ACTION)));
        popupMenu.addSeparator();

        formatMenu = new JMenu(MESSAGES.getString("ImageJViewer.Settings.Axis.Format.Menu") + SUSPENSION_POINTS);
        JComponent xFormatEditor = generateAxisFormatEditor(true, null, null, 5);
        xFormatEditor.setBorder(new EmptyBorder(5, 5, 5, 5));
        JComponent zFormatEditor = generateAxisFormatEditor(false, null, null, 5);
        zFormatEditor.setBorder(new EmptyBorder(0, 5, 5, 5));
        formatMenu.add(xFormatEditor);
        formatMenu.add(zFormatEditor);
        exportMenu = new JMenu(MESSAGES.getString("ImageJViewer.Action.SaveData.Text") + SUSPENSION_POINTS);
        exportMenu.setIcon(SAVE_DATA_ICON);
        exportMenu.add(createExportAction(textImageExporter));
        importMenu = new JMenu(MESSAGES.getString("ImageJViewer.Action.LoadData.Text") + SUSPENSION_POINTS);
        importMenu.setIcon(LOAD_DATA_ICON);
        importMenu.add(createImportAction(textImageExporter));
        popupMenu.add(formatMenu);
        popupMenu.addSeparator();
        popupMenu.add(exportMenu);
        popupMenu.add(importMenu);

        popupMenu.addSeparator();
        popupMenu.add(containerFactory.createMenuItem(getAction(SAVE_SETTINGS_ACTION)));
        popupMenu.add(containerFactory.createMenuItem(getAction(LOAD_SETTINGS_ACTION)));

        popupMenu.addSeparator();
        // Create an Action Menu

        initSelectionMenu();
        popupMenu.add(selectionMenu);

        popupMenu.addSeparator();
        initMouseWheelZoomMenu();
        popupMenu.add(mouseWheelZoomMenu);

        popupMenu.addSeparator();

        initActionMenu();
        initColorMenu();

        /*
         * defaultToolBar.addSeparator();
         * defaultToolBar.add(createSnapshotButton());
         * defaultToolBar.addSeparator();
         * defaultToolBar.add(createPrintButton());
         * defaultToolBar.addSeparator();
         * defaultToolBar.add(createShowTableButton());
         * defaultToolBar.addSeparator();
         * defaultToolBar.add(createLineProfileButton());
         */
    }

    protected void initSelectionMenu() {
        selectionMenu = new JMenu("Selection mode");
        selectionMenu.add(containerFactory.createMenuItem(getAction(MODE_RECTANGLE_ACTION)));
        selectionMenu.add(containerFactory.createMenuItem(getAction(MODE_ELLIPSE_ACTION)));
        selectionMenu.add(containerFactory.createMenuItem(getAction(MODE_POLYGON_ACTION)));
        selectionMenu.add(containerFactory.createMenuItem(getAction(MODE_FREE_HAND_ACTION)));
        selectionMenu.add(containerFactory.createMenuItem(getAction(MODE_LINE_ACTION)));
        selectionMenu.add(containerFactory.createMenuItem(getAction(MODE_POINT_ACTION)));
        selectionMenu.add(containerFactory.createMenuItem(getAction(MODE_WAND_ACTION)));
        selectionMenu.add(containerFactory.createMenuItem(getAction(MODE_PAN_ACTION)));
    }

    protected void initMouseWheelZoomMenu() {
        mouseWheelZoomMenu = new JMenu(MESSAGES.getString("ImageJViewer.Action.Zoom.MouseWheel.Text"));
        mouseWheelZoomMenu.setIcon(MOUSE_WHEEL_ZOOM_ICON);
        mouseWheelZoomMenu.add(containerFactory.createMenuItem(getAction(MOUSE_WHEEL_ZOOM_DEFAULT)));
        mouseWheelZoomMenu.add(containerFactory.createMenuItem(getAction(MOUSE_WHEEL_ZOOM_CENTER)));
        mouseWheelZoomMenu.add(containerFactory.createMenuItem(getAction(MOUSE_WHEEL_ZOOM_PIXEL)));
        setMouseZoomMode(getMouseZoomMode());
    }

    protected void initActionMenu() {
        int position;
        if (actionMenu == null) {
            position = -1;
        } else {
            position = popupMenu.getComponentIndex(actionMenu);
            popupMenu.remove(actionMenu);
            actionMenu.removeAll();
            actionMenu = null;
        }
        actionMenu = new JMenu("Action");

        actionMenu.add(containerFactory.createMenuItem(getAction(OR_SELECTED_ROIS_ACTION), actionMenu));
        actionMenu.add(containerFactory.createMenuItem(getAction(AND_SELECTED_ROIS_ACTION), actionMenu));
        actionMenu.add(containerFactory.createMenuItem(getAction(NOT_SELECTED_ROIS_ACTION), actionMenu));
        actionMenu.add(containerFactory.createMenuItem(getAction(XOR_SELECTED_ROIS_ACTION), actionMenu));

        if (isUseMaskManagement()) {
            actionMenu.addSeparator();

            actionMenu.add(containerFactory.createMenuItem(getAction(INNER_ROIS_ACTION), actionMenu));
            actionMenu.add(containerFactory.createMenuItem(getAction(OUTER_ROIS_ACTION), actionMenu));
            actionMenu.add(containerFactory.createMenuItem(getAction(INVALIDATE_ROIS_ACTION), actionMenu));

            actionMenu.addSeparator();

            actionMenu.add(containerFactory.createMenuItem(getAction(SET_AS_MASK_ACTION), actionMenu));
            actionMenu.add(containerFactory.createMenuItem(getAction(ADD_TO_MASK_ACTION), actionMenu));
            actionMenu.add(containerFactory.createMenuItem(getAction(REMOVE_FROM_MASK_ACTION), actionMenu));
            actionMenu.add(containerFactory.createMenuItem(getAction(CLEAR_MASK_ACTION), actionMenu));

            actionMenu.addSeparator();

            actionMenu.add(containerFactory.createMenuItem(getAction(LOAD_MASK_ACTION), actionMenu));
            actionMenu.add(containerFactory.createMenuItem(getAction(LOAD_MASK_ADD_ACTION), actionMenu));
            actionMenu.add(containerFactory.createMenuItem(getAction(SAVE_MASK_ACTION), actionMenu));
        }

        if (isUseSectorManagement()) {
            actionMenu.addSeparator();
            actionMenu.add(containerFactory.createMenuItem(getAction(NEW_SECTOR_ACTION), actionMenu));
            actionMenu.add(containerFactory.createMenuItem(getAction(EDIT_SECTOR_ACTION), actionMenu));
            actionMenu.add(containerFactory.createMenuItem(getAction(DELETE_SECTOR_ACTION), actionMenu));
        }

        actionMenu.addSeparator();

        actionMenu.add(containerFactory.createMenuItem(getAction(ZOOM_IN_ACTION), actionMenu));
        actionMenu.add(containerFactory.createMenuItem(getAction(ZOOM_OUT_ACTION), actionMenu));

        actionMenu.addSeparator();

        actionMenu.add(containerFactory.createMenuItem(getAction(DELETE_SELECTED_ROIS_ACTION), actionMenu));
        actionMenu.add(containerFactory.createMenuItem(getAction(DELETE_ALL_ROIS_ACTION), actionMenu));

        actionMenu.addSeparator();

        actionMenu.add(containerFactory.createMenuItem(getAction(UNDO_ACTION), actionMenu));
        actionMenu.add(containerFactory.createMenuItem(getAction(REDO_ACTION), actionMenu));

        if (position < 0) {
            popupMenu.add(actionMenu);
        } else {
            popupMenu.add(actionMenu, position);
        }
    }

    protected void initColorMenu() {
        int position;
        if (colorMenu == null) {
            position = -1;
        } else {
            position = popupMenu.getComponentIndex(colorMenu);
            popupMenu.remove(colorMenu);
            colorMenu.removeAll();
            colorMenu = null;
        }
        colorMenu = new JMenu("Colors");
        colorMenu.add(containerFactory.createMenuItem(getAction(DISPLAY_ROI_COLORS_MENU_ACTION), colorMenu));
        if (isUseMaskManagement()) {
            colorMenu.add(containerFactory.createMenuItem(getAction(SET_MASK_COLOR_ACTION), colorMenu));
        }
        if (isUseSectorManagement()) {
            colorMenu.add(containerFactory.createMenuItem(getAction(DISPLAY_SECTOR_COLORS_MENU_ACTION), colorMenu));
        }
        colorMenu.add(containerFactory.createMenuItem(getAction(SET_BEAM_POSITION_COLOR_ACTION), colorMenu));

        if (position < 0) {
            popupMenu.add(colorMenu);
        } else {
            popupMenu.add(colorMenu, position);
        }
    }

    protected void initRoiColorsPopupMenu() {
        if (roiColorsPopupMenu != null) {
            roiColorsPopupMenu.setVisible(false);
            roiColorsPopupMenu.removeAll();
            roiColorsPopupMenu = null;
        }

        roiColorsPopupMenu = new JPopupMenu();

        roiColorsPopupMenu.add(containerFactory.createMenuItem(getAction(CHANGE_ROI_COLOR_ACTION), roiColorsPopupMenu));
        roiColorsPopupMenu
                .add(containerFactory.createMenuItem(getAction(CHANGE_SELECTED_ROI_COLOR_ACTION), roiColorsPopupMenu));
        roiColorsPopupMenu
                .add(containerFactory.createMenuItem(getAction(CHANGE_HOVERED_ROI_COLOR_ACTION), roiColorsPopupMenu));
        if (isUseMaskManagement()) {
            roiColorsPopupMenu
                    .add(containerFactory.createMenuItem(getAction(CHANGE_INNER_ROI_COLOR_ACTION), roiColorsPopupMenu));
            roiColorsPopupMenu.add(containerFactory.createMenuItem(getAction(CHANGE_SELECTED_INNER_ROI_COLOR_ACTION),
                    roiColorsPopupMenu));
            roiColorsPopupMenu.add(containerFactory.createMenuItem(getAction(CHANGE_HOVERED_INNER_ROI_COLOR_ACTION),
                    roiColorsPopupMenu));
            roiColorsPopupMenu
                    .add(containerFactory.createMenuItem(getAction(CHANGE_OUTER_ROI_COLOR_ACTION), roiColorsPopupMenu));
            roiColorsPopupMenu.add(containerFactory.createMenuItem(getAction(CHANGE_SELECTED_OUTER_ROI_COLOR_ACTION),
                    roiColorsPopupMenu));
            roiColorsPopupMenu.add(containerFactory.createMenuItem(getAction(CHANGE_HOVERED_OUTER_ROI_COLOR_ACTION),
                    roiColorsPopupMenu));
        }
    }

    protected void initSectorColorsPopupMenu() {
        if (sectorColorsPopupMenu != null) {
            sectorColorsPopupMenu.setVisible(false);
            sectorColorsPopupMenu.removeAll();
            sectorColorsPopupMenu = null;
        }
        sectorColorsPopupMenu = new JPopupMenu();
        sectorColorsPopupMenu.add(
                containerFactory.createMenuItem(getAction(CHANGE_EDITING_SECTOR_COLOR_ACTION), sectorColorsPopupMenu));
        sectorColorsPopupMenu.add(containerFactory.createMenuItem(getAction(CHANGE_VALIDATED_SECTOR_COLOR_ACTION),
                sectorColorsPopupMenu));
    }

    protected void updateMenus() {
        initActionMenu();
        initColorMenu();
    }

    /**
     * @param g
     */
    protected void paintViewer(Graphics g) {
        try {
            Graphics2D g2d = (Graphics2D) g;
            if ((imageCanvas != null) && (theImage != null)) {
                // TODO move this outside the paint() method.
                // It is done here because user can modify axes' range from the outside.
                computeZoomedAxisRange();

                computeAxisMargins();

                g.translate(xOrigine, yOrigine);
                Rectangle clipBounds = g.getClipBounds();
                if (isLimitDrawingZone()) {
                    // Limit drawing to the image zone, so that ROIs that are
                    // outside are not displayed until we pan
                    Rectangle canvasRectangle = imageCanvas.getCanvasRectangle();
                    g.clipRect(canvasRectangle.x, canvasRectangle.y, canvasRectangle.width, canvasRectangle.height);
                }
                imageCanvas.paint(g2d);
                g.translate(-xOrigine, -yOrigine);
                if (isLimitDrawingZone()) {
                    g.setClip(clipBounds);
                }
                drawAxis(g2d);
            }
        } catch (Exception e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Error during ImageViewer rendering", e);
        }
    }

    protected void prepareAxisDrawing(Graphics2D g2d) {
        Rectangle imageSize = imageCanvas.getCanvasRectangle();
        int imageWidth = imageSize.width;
        int imageHeight = imageSize.height;

        if (axisStroke == null) {
            axisStroke = new BasicStroke(axisLineWidth);
        }

        g2d.setStroke(axisStroke);
        g2d.setColor(axisColor);
        // draw a rectangle to fill corners
        g2d.drawRect(Math.max(xOrigine - axisLineWidth, 0), Math.max(yOrigine - axisLineWidth, 0),
                imageWidth + axisLineWidth, imageHeight + axisLineWidth);
    }

    protected void drawXAxis(Graphics2D g2d) {
        if (xAxis.isVisible()) {
            int xBegin = xOrigine;
            int yBeginTop = Math.max(yOrigine - axisLineWidth, 0);
            int yBeginBottom = Math.max(yOrigine + imageCanvas.getCanvasRectangle().height - 1 + axisLineWidth, 0);

            if (xAxis.getAttributes().getOrigin() == IChartViewer.HORIZONTAL_UP) {
                xAxis.paintAxis(g2d, FontUtils.getDefaultRenderContext(), null, xBegin, yBeginTop, yBeginBottom,
                        SwingConstants.TOP, xAxis.getAttributes().isDrawOpposite(), yBeginTop, yBeginBottom);
            } else if (xAxis.getAttributes().getOrigin() == IChartViewer.HORIZONTAL_DOWN) {
                xAxis.paintAxis(g2d, FontUtils.getDefaultRenderContext(), null, xBegin, yBeginBottom, yBeginTop,
                        SwingConstants.BOTTOM, xAxis.getAttributes().isDrawOpposite(), yBeginBottom, yBeginTop);
            }
        }
    }

    protected void drawYAxis(Graphics2D g2d) {
        if (yAxis.isVisible()) {
            int yBegin = yOrigine;
            int imageWidth = imageCanvas.getCanvasRectangle().width;
            int xLeft = Math.max(xOrigine - axisLineWidth, 0);
            int xRight = Math.max(xOrigine + imageWidth, 0);

            if (yAxis.getAttributes().getOrigin() == IChartViewer.VERTICAL_LEFT) {
                yAxis.paintAxis(g2d, FontUtils.getDefaultRenderContext(), null, xLeft, yBegin, xRight,
                        SwingConstants.LEFT, yAxis.getAttributes().isDrawOpposite(), xLeft, xRight);
            } else if (yAxis.getAttributes().getOrigin() == IChartViewer.VERTICAL_RIGHT) {
                yAxis.paintAxis(g2d, FontUtils.getDefaultRenderContext(), null, xRight, yBegin, xLeft,
                        SwingConstants.RIGHT, yAxis.getAttributes().isDrawOpposite(), xRight, xLeft);
            }
        }
    }

    protected void drawAxis(Graphics2D g2d) {
        prepareAxisDrawing(g2d);
        drawXAxis(g2d);
        drawYAxis(g2d);
    }

    public void setAxisLineWidth(int width) {
        axisLineWidth = width;
        axisStroke = new BasicStroke(axisLineWidth);
    }

    public int getAxisLineWidth() {
        return axisLineWidth;
    }

    public IJRoiPermission getRoiPermission() {
        return roiPermission;
    }

    public void setRoiPermission(IJRoiPermission roiPermission) {
        this.roiPermission = roiPermission;
    }

    protected void setRectangleMode() {
        Toolbar.getInstance().setTool(Toolbar.RECTANGLE);
        IJRoiManager.setClassicRoiMode();
        ActionManager.getInstance().setSelected(MODE_RECTANGLE_ACTION, true);
    }

    protected void setAngleMode() {
        Toolbar.getInstance().setTool(Toolbar.ANGLE);
        IJRoiManager.setClassicRoiMode();
        ActionManager.getInstance().setSelected(MODE_ANGLE_ACTION, true);
    }

    protected void setEllipseMode() {
        Toolbar.getInstance().setTool(Toolbar.OVAL);
        IJRoiManager.setClassicRoiMode();
        ActionManager.getInstance().setSelected(MODE_ELLIPSE_ACTION, true);
    }

    protected void setPolygonMode() {
        Toolbar.getInstance().setTool(Toolbar.POLYGON);
        IJRoiManager.setClassicRoiMode();
        ActionManager.getInstance().setSelected(MODE_POLYGON_ACTION, true);
    }

    protected void setFreeHandMode() {
        Toolbar.getInstance().setTool(Toolbar.FREEROI);
        IJRoiManager.setClassicRoiMode();
        ActionManager.getInstance().setSelected(MODE_FREE_HAND_ACTION, true);
    }

    protected void setLineMode() {
        Toolbar.getInstance().setTool(Toolbar.LINE);
        IJRoiManager.setClassicRoiMode();
        ActionManager.getInstance().setSelected(MODE_LINE_ACTION, true);
    }

    protected void setPointMode() {
        Toolbar.getInstance().setTool(Toolbar.POINT);
        IJRoiManager.setClassicRoiMode();
        ActionManager.getInstance().setSelected(MODE_POINT_ACTION, true);
    }

    protected void setWandMode() {
        Toolbar.getInstance().setTool(Toolbar.WAND);
        IJRoiManager.setClassicRoiMode();
        ActionManager.getInstance().setSelected(MODE_WAND_ACTION, true);
    }

    protected void setPanMode() {
        Toolbar.getInstance().setTool(Toolbar.HAND);
        IJRoiManager.setClassicRoiMode();
        ActionManager.getInstance().setSelected(MODE_PAN_ACTION, true);
    }

    public Chart getLineProfiler(boolean autoConstruct) {
        if ((lineProfiler == null) && autoConstruct) {
            lineProfiler = createLineProfiler();
            lineProfiler.setDataViewCometeColor(LINE_PROFILE_NAME, CometeColor.RED);
            lineProfiler.setDataViewLineWidth(LINE_PROFILE_NAME, 2);
            updateProfilersFormat();
        }
        return lineProfiler;
    }

    public Chart getLineProfiler() {
        return getLineProfiler(true);
    }

    public LineProfileDialog getLineProfilerDialog() {
        if (lineProfilerDialog == null) {
            Window owner = CometeUtils.getWindowForComponent(this);
            if (owner != null) {
                lineProfilerDialog = new LineProfileDialog(owner, getLineProfiler(true));
                lineProfilerDialog.setLocationRelativeTo(this);
            }
        }
        return lineProfilerDialog;
    }

    public CalibrationDialog getCalibrationDialog() {
        if (calibrationDialog == null) {
            calibrationDialog = new CalibrationDialog(CometeUtils.getWindowForComponent(this), this);
        }
        return calibrationDialog;
    }

    public HistogramDialog getHistogramDialog() {
        if (histogramDialog == null) {
            Window owner = CometeUtils.getWindowForComponent(this);
            if (owner != null) {
                histogramDialog = new HistogramDialog(owner, HISTOGRAM_NAME, this);
                histogramDialog.setLocationRelativeTo(this);
            }
        }
        return histogramDialog;
    }

    public double getHistogramMin() {
        return histoMin;
    }

    public void setHistogramMin(double histogramMin) {
        if (this.histoMin != histogramMin) {
            double old = this.histoMin;
            this.histoMin = histogramMin;
            firePropertyChange(HISTOGRAM_MIN, old, histogramMin);
        }
    }

    public double getHistogramMax() {
        return histoMax;
    }

    public void setHistogramMax(double histogramMax) {
        if (this.histoMax != histogramMax) {
            double old = this.histoMax;
            this.histoMax = histogramMax;
            firePropertyChange(HISTOGRAM_MAX, old, histogramMax);
        }
    }

    public double getHistogramStep() {
        return histoStep;
    }

    public void setHistogramStep(double histogramStep) {
        if (this.histoStep != histogramStep) {
            double old = this.histoStep;
            this.histoStep = histogramStep;
            firePropertyChange(HISTOGRAM_STEP, old, histogramStep);
        }
    }

    public boolean isHistogramAutoStep() {
        return histoAutoStep;
    }

    public void setHistogramAutoStep(boolean histogramAutoStep) {
        if (this.histoAutoStep != histogramAutoStep) {
            boolean old = this.histoAutoStep;
            this.histoAutoStep = histogramAutoStep;
            firePropertyChange(HISTOGRAM_AUTO_STEP, old, histogramAutoStep);
        }
    }

    public boolean isHistogramAutoScale() {
        return histoAutoScale;
    }

    public void setHistogramAutoScale(boolean histogramAutoScale) {
        if (this.histoAutoScale != histogramAutoScale) {
            boolean old = this.histoAutoScale;
            this.histoAutoScale = histogramAutoScale;
            firePropertyChange(HISTOGRAM_AUTO_SCALE, old, histogramAutoScale);
        }
    }

    protected void selectCalibrationMode() {
        ActionManager.getInstance().setSelected(getTransformedActionCommand(MODE_CALIBRATION_ACTION), true);
    }

    protected void selectHistogramMode() {
        ActionManager.getInstance().setSelected(getTransformedActionCommand(MODE_HISTOGRAM_ACTION), true);
    }

    public double[] getTrajectories() {
        int roiType = getRoiManager().getLastCreatedRoi().getType();
        List<Roi> rois = getRoiManager().getAllRois();

        double[] result = null;
        int index = 0;

        if (Roi.POINT == roiType) {
            result = new double[rois.size() * 2];
            for (Iterator<Roi> iterator = rois.iterator(); iterator.hasNext();) {
                Roi roi = iterator.next();
                if (roiType == roi.getType()) {
                    Rectangle r = roi.getBounds();
                    result[index++] = getImageScreenPositionCalculator().imagePixelToAxisXD(r.x);
                    result[index++] = getImageScreenPositionCalculator().imagePixelToAxisYD(r.y);
                }
            }
        } else if (Roi.LINE == roiType) {
            String s = JOptionPane.showInputDialog(this, "Enter the number of steps in trajectory:",
                    "Custom Trajectory", JOptionPane.PLAIN_MESSAGE);
            int numberOfPoints = -1;
            try {
                numberOfPoints = Integer.parseInt(s);
            } catch (NumberFormatException nfe) {
                // SILENT
            }
            if (numberOfPoints > 1) {
                result = new double[numberOfPoints * 2 + 2];
                Roi roi = null;
                for (Iterator<Roi> iterator = rois.iterator(); iterator.hasNext();) {
                    roi = iterator.next();
                    if (roiType == roi.getType()) {
                        break;
                    }
                }
                if (roi != null) {
                    Line roiLine = (Line) roi;
                    double xSteps = (roiLine.x2d - roiLine.x1d) / (numberOfPoints);
                    double ySteps = (roiLine.y2d - roiLine.y1d) / (numberOfPoints);

                    result[0] = roiLine.x1d;
                    result[1] = roiLine.y1d;

                    for (int i = 2; i < 2 * numberOfPoints + 2; i += 2) {
                        result[i] = result[i - 2] + xSteps;
                        result[i + 1] = result[i - 1] + ySteps;
                    }
                    for (int i = 0; i < result.length; i += 2) {
                        result[i] = getImageScreenPositionCalculator().imagePixelToAxisXD(result[i]);
                        result[i + 1] = getImageScreenPositionCalculator().imagePixelToAxisYD(result[i + 1]);
                    }
                }
            }

        }

        //

        return result;
    }

    public boolean isXAutoScale() {
        return xAutoScale;
    }

    public void setXAutoScale(boolean autoScale) {
        xAutoScale = autoScale;
    }

    public boolean isYAutoScale() {
        return yAutoScale;
    }

    public void setYAutoScale(boolean autoScale) {
        yAutoScale = autoScale;
    }

    /**
     * @return the pixelSize
     */
    public double getPixelSize() {
        return pixelSize;
    }

    /**
     * @param pixelSize the pixelSize to set
     */
    public void setPixelSize(double pixelSize) {
        this.pixelSize = pixelSize;

        setAutoScaleX(false);
        setAutoScaleY(false);
        setAxisRangeX(0, getImageWidth() * pixelSize, false);
        setAxisRangeY(0, getImageHeight() * pixelSize, false);
        repaint();

        updatePixelSizeRoiGenerators();
        firePixelSizeChanged();
    }

    public void deleteSelectedRois() {
        imagePlus.deleteSelectedRois();
        repaint();
    }

    public void deleteAllRois(boolean resetRoiCounter) {
        if (canDeleteAllRois()) {
            imagePlus.deleteAllRois(resetRoiCounter);
            repaint();
        }
    }

    public void deleteAllRoisButSpecial(int mode, boolean resetRoiCounter) {
        if (canDeleteAllRois()) {
            imagePlus.deleteAllRoisButSpecial(mode, resetRoiCounter);
            ;
            repaint();
        }
    }

    public void deleteAllRoisButSpecials(boolean resetRoiCounter) {
        if (canDeleteAllRois()) {
            imagePlus.deleteAllRoisButSpecials(resetRoiCounter);
            repaint();
        }
    }

    public void zoomIn() {
        imageCanvas.zoomIn();
        adjustToCanvas();
        updateZoomRoiGenerators();
    }

    public void zoomOut() {
        imageCanvas.zoomOut();
        adjustToCanvas();
        updateZoomRoiGenerators();
    }

    public void orSelectedRois() {
        imagePlus.orSelectedRois();
        repaint();
    }

    protected void andSelectedRois() {
        imagePlus.andSelectedRois();
        repaint();
    }

    public void notSelectedRois() {
        imagePlus.notSelectedRois();
        repaint();
    }

    public void xorSelectedRois() {
        imagePlus.xorSelectedRois();
        repaint();
    }

    public void undo() {
        undoManager.undo();
        repaint();
    }

    public void redo() {
        undoManager.redo();
        repaint();
    }

    public void saveImage() {
        snapshotEventDelegate.changeSnapshotLocation(SnapshotUtil.snapshot(this,
                snapshotEventDelegate.getSnapshotLocation(), getPreferredSnapshotExtension(), true));
    }

    /**
     * 
     * @return true if user accepts to delete all ROIs, false otherwise
     */
    protected boolean canDeleteAllRois() {
        boolean allowed;
        if (roiPermission == null) {
            allowed = true;
        } else {
            allowed = roiPermission.canDeleteAllRois();
        }
        return allowed;
    }

    /**
     * 
     * @return true if user accepts to delete all ROIs, false otherwise
     */
    protected boolean canDeleteRoi(Roi roi) {
        if (roiPermission == null) {
            return true;
        }
        return roiPermission.canDeleteRoi(roi);
    }

    public BufferedImage getImage() {
        return theImage;
    }

    public Number getImageValue(Point imagePos) {
        return getImageValue(imagePos.x, imagePos.y);
    }

    public Number getImageValue(int imagePosX, int imagePosY) {
        Number result = null;
        int index = imagePosY * dimX + imagePosX;
        if (value instanceof Number[] || value instanceof byte[] || value instanceof short[] || value instanceof int[]
                || value instanceof long[] || value instanceof float[] || value instanceof double[]) {
            if (index > -1 && index < Array.getLength(value)) {
                result = (Number) Array.get(value, index);
            }
        }
        return result;
    }

    public Point getImagePosition() {
        return getImagePosition(imagePanel.getMousePosition());
    }

    public Point getImagePosition(Point screenPos) {
        Point result = null;
        if (imageCanvas != null && screenPos != null) {
            screenPos = new Point(screenPos.x - xOrigine, screenPos.y - yOrigine);
            result = imageCanvas.getImagePosition(screenPos);
        }
        return result;
    }

    public Point2D.Double getImagePositionD() {
        return getImagePositionD(imagePanel.getMousePosition());
    }

    public Point2D.Double getImagePositionD(Point screenPos) {
        Point2D.Double result = null;
        if ((imageCanvas != null) && (screenPos != null)) {
            screenPos = new Point(screenPos.x - xOrigine, screenPos.y - yOrigine);
            result = imageCanvas.getImagePositionD(screenPos);
        }
        return result;
    }

    public Point2D.Double getAxisPosition() {
        return getAxisPosition(imagePanel.getMousePosition());
    }

    /**
     * Get the axis position for the specified mouse position.
     * 
     * @param screenPos
     * @return the corresponding point according to axis' ranges
     */
    public Point2D.Double getAxisPosition(Point screenPos) {
        Point2D.Double result = null;
        if ((imageCanvas != null) && (screenPos != null)) {
            screenPos = new Point(screenPos.x - xOrigine, screenPos.y - yOrigine);
            if (imageCanvas.getCanvasRectangle().contains(screenPos)) {
                result = new Point2D.Double(screenToAxisX(screenPos.x), screenToAxisY(screenPos.y));
            }
        }
        return result;
    }

    protected String getAxisFormat(AbstractAxisView axis) {
        return axis == null ? null : axis.getAttributes().getValueFormat();
    }

    protected void setAxisFormat(AbstractAxisView axis, String format) {
        if (axis != null) {
            String oldFormat = axis.getAttributes().getValueFormat();
            axis.getAttributes().setValueFormat(format);
            if (coordsLabel instanceof CoordsLabel) {
                if (axis == xAxis) {
                    ((CoordsLabel) coordsLabel).setXAxisFormat(format);
                    firePropertyChange(X_AXIS_FORMAT, oldFormat, format);
                } else if (axis == yAxis) {
                    ((CoordsLabel) coordsLabel).setYAxisFormat(format);
                    firePropertyChange(Z_AXIS_FORMAT, oldFormat, format);
                }
            }
            if (axis == xAxis) {
                updateMatrixTableHeaders(getXAxisConvertor() != null, true);
            } else {
                updateMatrixTableHeaders(getYAxisConvertor() != null, false);
            }
            updateProfilersFormat();
            repaint();
        }
    }

    @Override
    public String getXAxisFormat() {
        return getAxisFormat(xAxis);
    }

    @Override
    public void setXAxisFormat(String format) {
        setAxisFormat(xAxis, format);
    }

    @Override
    public String getYAxisFormat() {
        return getAxisFormat(yAxis);
    }

    @Override
    public void setYAxisFormat(String format) {
        setAxisFormat(yAxis, format);
    }

    protected double screenToAxisX(int sx) {
        return getImageScreenPositionCalculator().imagePixelToAxisXD(imageCanvas.offScreenXD(sx));
    }

    protected double screenToAxisY(int sy) {
        return getImageScreenPositionCalculator().imagePixelToAxisYD(imageCanvas.offScreenYD(sy));
    }

    protected void computeBeamPositionFromAxis(ImageScreenPositionCalculator calculator) {
        computeBeamPositionFromAxis(calculator, beamPoint);
    }

    protected void computeBeamPositionFromAxis(ImageScreenPositionCalculator calculator, double[] beamPoint) {
        Point2D.Double theBeamPoint = null;
        if ((beamPoint != null) && (calculator != null)) {
            double ox = calculator.axisToImagePixelXD(beamPoint[0]);
            double oy = calculator.axisToImagePixelYD(beamPoint[1]);
            theBeamPoint = new Point2D.Double(ox, oy);
        }
        if (isBeamPointEnabled()) {
            imageCanvas.setBeamPoint(theBeamPoint);
            imageCanvas.setDrawBeamPosition(isDrawBeamPosition());
        }
    }

    protected void updateBeamPosition() {
        ImageScreenPositionCalculator tempCalculator = new ImageScreenPositionCalculator();
        updateAxisRange(tempCalculator);
        computeBeamPositionFromAxis(tempCalculator);
        updateAxisRange(getImageScreenPositionCalculator());
        repaint();
    }

    /**
     * Adapt axis' range to the displayed area. In autoscale mode, IJ gives us information about the
     * displayed zone. Otherwise, we have to compute the min and max.
     */
    protected void computeZoomedAxisRange() {
        Rectangle srcRect = imageCanvas.getSrcRect();
        int srcX = Math.max(srcRect.x, 0);
        int srcXMax = srcX + srcRect.width;
        double xMin = srcX, xMax = srcXMax;
        if (!xAutoScale) {
            xMin = getImageScreenPositionCalculator().imagePixelToAxisXD(xMin);
            xMax = getImageScreenPositionCalculator().imagePixelToAxisXD(xMax);
        }
        int srcY = Math.max(srcRect.y, 0);
        int srcYMax = srcY + srcRect.height;
        double yMin = srcY, yMax = srcYMax;
        if (!yAutoScale) {
            yMin = getImageScreenPositionCalculator().imagePixelToAxisYD(yMin);
            yMax = getImageScreenPositionCalculator().imagePixelToAxisYD(yMax);
        }
        applyLabels(xAxis, getImageScreenPositionCalculator().getCustomXPositions(srcX, srcXMax), xMin, xMax, true);
        applyLabels(yAxis, getImageScreenPositionCalculator().getCustomYPositions(srcY, srcYMax), yMin, yMax, true);
    }

    protected void applyLabels(AbstractAxisView axis, Map<Double, Double> positionMap, double min, double max,
            boolean clearMap) {
        if (axis != null) {
            if (min < max) {
                axis.getAttributes().setMinimum(min);
                axis.getAttributes().setMaximum(max);
                axis.getAttributes().setInverted(false);
            } else {
                axis.getAttributes().setMinimum(max);
                axis.getAttributes().setMinimum(min);
                axis.getAttributes().setInverted(true);
            }
            axis.getScale().updateScaleBoundsFromAttributes();
            if (positionMap == null) {
                axis.getAttributes().setLabels(null, null);
            } else {
                String[] labels = new String[positionMap.size()];
                double[] labelPos = new double[labels.length];
                int index = 0;
                String formatToUse = axis.getAttributes().getValueFormat();
                boolean autoFormat;
                if (Format.isNumberFormatOK(formatToUse)) {
                    autoFormat = false;
                } else {
                    formatToUse = DEFAULT_LABEL_FORMAT;
                    autoFormat = true;
                }
                for (Entry<Double, Double> entry : positionMap.entrySet()) {
                    Double transformed = entry.getValue();
                    Double toLabel = transformed == null ? Double.valueOf(MathConst.NAN_FOR_NULL) : transformed;
                    String val;
                    try {
                        if (toLabel.isNaN() || toLabel.isInfinite()) {
                            val = toLabel.toString();
                        } else {
                            val = Format.format(formatToUse, toLabel);
                            if (autoFormat) {
                                val = ChartUtils.suppressZero(val);
                            }
                        }
                    } catch (Exception e) {
                        val = toLabel.toString();
                        if (autoFormat) {
                            val = ChartUtils.suppressZero(val);
                        }
                    }
                    labels[index] = val;
                    Double pos = entry.getKey();
                    // align tick on pixel limit
                    labelPos[index] = pos == null ? MathConst.NAN_FOR_NULL : Math.floor(pos.doubleValue());
                    index++;
                }
                if (clearMap) {
                    positionMap.clear();
                }
                axis.getAttributes().setLabels(labels, labelPos);
            }
        }
    }

    /**
     * Translates a {@link MouseEvent}
     * 
     * @param e the original {@link MouseEvent}
     * @return The translated {@link MouseEvent}
     */
    public MouseEvent translateMouseEvent(MouseEvent e) {
        MouseEvent translatedEvent = new MouseEvent((Component) e.getSource(), e.getID(), e.getWhen(), e.getModifiers(),
                e.getX() - xOrigine, e.getY() - yOrigine, e.getClickCount(), e.isPopupTrigger());
        return translatedEvent;
    }

    /**
     * Translates a {@link MouseWheelEvent}
     * 
     * @param e the original {@link MouseWheelEvent}
     * @return The translated {@link MouseWheelEvent}
     */
    public MouseWheelEvent translateMouseWheelEvent(MouseWheelEvent e) {
        MouseWheelEvent translatedEvent = new MouseWheelEvent((Component) e.getSource(), e.getID(), e.getWhen(),
                e.getModifiers(), e.getX() - xOrigine, e.getY() - yOrigine, e.getClickCount(), e.isPopupTrigger(),
                e.getScrollType(), e.getScrollAmount(), e.getWheelRotation());
        return translatedEvent;
    }

    protected void saveMouseEvent(MouseEvent event) {
        if ((event != null) && (imagePanel != null) && (event.getSource() == imagePanel)) {
            imagePanel.setMousePosition(new Point(event.getX(), event.getY()));
        }
    }

    /**
     * Calculates and sets the beam point from a mouse click.
     * 
     * @param sx The click x coordinate inside the image canvas (screen X).
     * @param sy The click y coordinate inside the image canvas (screen Y).
     */
    protected void applyBeamPointFromClick(int sx, int sy) {
        IJCanvas canvas = imageCanvas;
        if (canvas == null) {
            setBeamPoint(getAxisPosition());
        } else {
            setBeamPointFromClick(canvas.offScreenXD(sx), canvas.offScreenYD(sy), screenToAxisX(sx), screenToAxisY(sy));
        }
    }

    protected void maybeShowPopup(MouseEvent e) {
        if (e.isPopupTrigger() && canShowPopup()) {
            popupMenu.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    protected boolean canShowPopup() {
        boolean result = true;

        // Don't show popup in ROI creation mode (useful for polygon)
        Roi roi = imagePlus.getRoi();
        result &= (roi == null) || (roi.getState() != Roi.CONSTRUCTING);

        return result;
    }

    /**
     * Returns whether beam point can be set at mouse pressed event.
     * 
     * @return A <code>boolean</code>.
     */
    // This method is useful for SCAN-848
    protected boolean isBeamPointSetAtMousePressed() {
        return true;
    }

    /**
     * Returns whether beam point can be set at mouse released event.
     * 
     * @return A <code>boolean</code>.
     */
    // This method is useful for SCAN-848
    protected boolean isBeamPointSetAtMouseReleased() {
        return false;
    }

    /**
     * Returns whether beam point can be set at mouse clicked event.
     * 
     * @return A <code>boolean</code>.
     */
    // This method is useful for SCAN-848
    protected boolean isBeamPointSetAtMouseClicked() {
        return false;
    }

    /**
     * Returns whether a {@link MouseEvent} represents a click to set beam center coordinates.
     * 
     * @param e The {@link MouseEvent}.
     * @param te The transposed {@link MouseEvent} for {@link IJCanvas}. Useful to interact with {@link IJCanvas}.
     * @return A <code>boolean</code>. <code>true</code> if the {@link MouseEvent} is detected as a beam center setting
     *         click.
     */
    // This method is useful for SCAN-848
    protected boolean isBeamPointClick(MouseEvent e, MouseEvent te) {
        return isAllowBeamPositionByClick() && isBeamPointEnabled() && SwingUtilities.isLeftMouseButton(te)
                && (e.isAltDown() || e.isControlDown() || e.isShiftDown());
    }

    // -----------------------------------------------------------
    // Viewer listener
    // -----------------------------------------------------------
    public void addViewerListener(IIJViewerListener l) {
        listenerList.add(IIJViewerListener.class, l);
    }

    public void removeViewerListener(IIJViewerListener l) {
        listenerList.remove(IIJViewerListener.class, l);
    }

    protected void fireMouseChanged() {
        IJViewerEvent event = new IJViewerEvent(this);
        IIJViewerListener[] listeners = listenerList.getListeners(IIJViewerListener.class);
        for (IIJViewerListener listener : listeners) {
            listener.mouseChanged(event);
        }
    }

    protected void fireMouseClicked() {
        IJViewerEvent event = new IJViewerEvent(this);
        IIJViewerListener[] listeners = listenerList.getListeners(IIJViewerListener.class);
        for (IIJViewerListener listener : listeners) {
            listener.mouseClicked(event);
        }
        fireSelectedPointChanged(getSelectedX(), getSelectedY(), getSelectedAxisX(), getSelectedAxisY());
    }

    protected void fireSelectedPointChanged(double... selected) {
        ImageViewerEvent event = new ImageViewerEvent(this, selected, getSelectedValue(), Reason.SELECTION);
        for (IImageViewerListener listener : listeners) {
            listener.imageViewerChanged(event);
        }
    }

    protected void firePixelSizeChanged() {
        IJViewerEvent event = new IJViewerEvent(this);
        IIJViewerListener[] listeners = listenerList.getListeners(IIJViewerListener.class);
        for (IIJViewerListener listener : listeners) {
            listener.pixelSizeChanged(event);
        }
    }

    // ----------------------------------------------------------
    // MouseWheelListener
    // ----------------------------------------------------------
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        saveMouseEvent(e);
        IJCanvas imageCanvas = this.imageCanvas;
        if ((imageCanvas != null) && (value != null)) {
            MouseWheelEvent te = translateMouseWheelEvent(e);

            // wheel can be used on the whole panel, not only above image zone
            imageCanvas.mouseWheelMoved(te);

            // for performance reasons, we should update axis range here
            // it is actually done in the repaint for convenience reasons
            // adapt axis' range to the displayed area
            // updateAxesRange();//this is done in paint
            imagePanel.setCursor(imageCanvas.getCursor());
            adjustToCanvas();
            updateZoomRoiGenerators();
            fireMouseChanged();
        }
    }

    // ----------------------------------------------------------
    // MouseListener
    // ----------------------------------------------------------
    @Override
    public void mouseClicked(MouseEvent e) throws ApplicationIdException {
        saveMouseEvent(e);
        if (e.getSource() == imagePanel) {

            selectedValue = Double.NaN;
            selectedAxisX = selectedAxisY = Double.NaN;
            selectedX = selectedY = -1;

            // Point mousePosition = imagePanel.getMousePosition();
            Point mousePosition = e.getPoint();
            Point imagePosition = getImagePosition(mousePosition);
            Point2D.Double axisPosition = getAxisPosition(mousePosition);
            Number imageValue = null;
            if (imagePosition != null) {
                imageValue = getImageValue(imagePosition);
                selectedX = imagePosition.x;
                selectedY = imagePosition.y;
                if (axisPosition != null) {
                    selectedAxisX = axisPosition.getX();
                    selectedAxisY = axisPosition.getY();
                }
                if (imageValue != null) {
                    selectedValue = imageValue.doubleValue();
                }
                fireMouseClicked();
            }

            MouseEvent te = translateMouseEvent(e);

            if ((imageCanvas != null)
                    && ((!isLimitDrawingZone()) || imageCanvas.getCanvasRectangle().contains(te.getPoint()))) {
                imageCanvas.mouseClicked(te);
                imagePanel.setCursor(imageCanvas.getCursor());
                if (isBeamPointSetAtMouseClicked() && isBeamPointClick(e, te)) {
                    applyBeamPointFromClick(te.getX(), te.getY());
                }
                repaint();
            }
        } else if (e.getSource() == gradientViewer) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                if (fitDialog == null) {
                    fitDialog = new JDialog(CometeUtils.getWindowForComponent(this),
                            MESSAGES.getString("ImageJViewer.Border.Fit.Text"));
                    fitDialog.setContentPane(fitSettingsPanel);
                    fitDialog.setModal(false);
                    fitDialog.setResizable(false);
                }
                if (!fitDialog.isVisible()) {
                    fitDialog.pack();
                    fitDialog.setLocationRelativeTo(gradientViewer);
                }
                fitDialog.setVisible(true);
                fitDialog.toFront();
            }
        } else if (e.getSource() == gradientEditor) {
            Gradient g = JGradientEditor.showDialog(gradientEditor, getGradient());
            if (g != null) {
                setGradient(g);
                gradientEditor.setGradient(g);
                if (gradientEditor.isValid()) {
                    gradientEditor.repaint();
                }
            }
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {
        saveMouseEvent(e);
        // grab the focus to listen to keyboard
        imagePanel.requestFocusInWindow();
        if (e.getSource() == imagePanel) {
            if (imageCanvas != null) {
                MouseEvent te = translateMouseEvent(e);
                if (!isLimitDrawingZone() || imageCanvas.getCanvasRectangle().contains(te.getPoint())) {
                    imageCanvas.mousePressed(te);
                    imagePanel.setCursor(imageCanvas.getCursor());
                    if (isBeamPointSetAtMousePressed() && isBeamPointClick(e, te)) {
                        applyBeamPointFromClick(te.getX(), te.getY());
                    }
                    repaint();
                }
                IJRoiManager imp = getRoiManager();
                if (e.getClickCount() == 2 && (imp != null) && (!imp.isSpecialRoiMode())) {
                    boolean roiCreationMode;
                    switch (Toolbar.getToolId()) {
                        case Toolbar.RECTANGLE:
                        case Toolbar.OVAL:
                        case Toolbar.POLYGON:
                        case Toolbar.FREEROI:
                        case Toolbar.LINE:
                        case Toolbar.POLYLINE:
                        case Toolbar.FREELINE:
                        case Toolbar.POINT:
                        case Toolbar.WAND:
                        case Toolbar.TEXT:
                            roiCreationMode = true;
                            break;
                        default:
                            roiCreationMode = false;
                            break;
                    }
                    if (roiCreationMode) {
                        // JAVAAPI-638: Roi selection/deselection at double click
                        Roi roi = imageCanvas.getRoiAtPoint(te.getX(), te.getY());
                        if (roi != null) {
                            if (imp.getSelectedRois().contains(roi)) {
                                imp.deselectRoi(roi);
                            } else {
                                if (!imageCanvas.isKeepSelection(te)) {
                                    imp.clearRoiSelection();
                                }
                                imp.selectRoi(roi);
                            }
                        }
                        repaint();
                    }
                }
                // let popup appear on the whole viewer, not canvas only
                maybeShowPopup(e);
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        saveMouseEvent(e);
        if (e.getSource() == imagePanel) {
            if (imageCanvas != null) {
                MouseEvent te = translateMouseEvent(e);
                imageCanvas.mouseReleased(te);
                imagePanel.setCursor(imageCanvas.getCursor());
                if (isBeamPointSetAtMouseReleased() && isBeamPointClick(e, te)) {
                    applyBeamPointFromClick(te.getX(), te.getY());
                }
                repaint();

                // let popup appear on the whole viewer, not canvas only
                maybeShowPopup(e);
            }
            fireMouseChanged();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        saveMouseEvent(e);
        if (e.getSource() == imagePanel) {
            if (imageCanvas != null) {
                MouseEvent te = translateMouseEvent(e);
                imageCanvas.mouseEntered(te);
                imagePanel.setCursor(imageCanvas.getCursor());
                repaint();

                fireMouseChanged();
            }
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        saveMouseEvent(e);
        if (e.getSource() == imagePanel) {
            if (imageCanvas != null) {
                MouseEvent te = translateMouseEvent(e);
                imageCanvas.mouseExited(te);
                imagePanel.setCursor(imageCanvas.getCursor());
                repaint();

                fireMouseChanged();
            }
        }
    }

    // ----------------------------------------------------------
    // MouseMotionListener
    // ----------------------------------------------------------
    @Override
    public void mouseDragged(MouseEvent e) {
        saveMouseEvent(e);
        if (e.getSource() == imagePanel) {
            if (imageCanvas != null) {
                MouseEvent te = translateMouseEvent(e);

                // TODO: maybe transmit event to canvas depending of its ROI
                if (!isLimitDrawingZone() || imageCanvas.getCanvasRectangle().contains(te.getPoint())) {
                    imageCanvas.mouseDragged(te);
                    imagePanel.setCursor(imageCanvas.getCursor());
                }
                // for performance reasons, we should update axis range here
                // it is actually done in the repaint for convenience reasons
                // adapt axis' range to the displayed area
                // updateAxesRange();//this is done in paint

                repaint();

                fireMouseChanged();
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        saveMouseEvent(e);
        if (e.getSource() == imagePanel) {
            if (imageCanvas != null) {
                MouseEvent te = translateMouseEvent(e);

                // special case when constructing a polygon:
                // we follow sides of the whole image (offscreen coordinates)
                Roi roi = imagePlus.getRoi();
                if (roi != null && roi.getType() == Roi.POLYGON && roi.getState() == Roi.CONSTRUCTING) {
                    int sx = te.getX();
                    int sy = te.getY();
                    int ox = imageCanvas.offScreenX(sx);
                    int oy = imageCanvas.offScreenY(sy);
                    if (ox >= dimX) {
                        ox = dimX;
                        sx = imageCanvas.screenX(ox);
                    } else if (ox < 0) {
                        ox = 0;
                        sx = imageCanvas.screenX(ox);
                    }
                    if (oy >= dimY) {
                        oy = dimY;
                        sy = imageCanvas.screenY(oy);
                    } else if (oy < 0) {
                        oy = 0;
                        sy = imageCanvas.screenY(oy);
                    }
                    // TODO there is a problem if magnification is less than 1:
                    // the ROI limit is one or 2 pixels inside the image
                    // although it is displayed "outside" (let's zoom to see it!)

                    te = new MouseEvent(te.getComponent(), te.getID(), te.getWhen(), te.getModifiersEx(), sx, sy,
                            te.getClickCount(), te.isPopupTrigger(), te.getButton());

                    imageCanvas.mouseMoved(te);
                    imagePanel.setCursor(imageCanvas.getCursor());
                    repaint();
                } else if (!isLimitDrawingZone() || imageCanvas.getCanvasRectangle().contains(te.getPoint())) {
                    imageCanvas.mouseMoved(te);
                    imagePanel.setCursor(imageCanvas.getCursor());
                    repaint();
                }

                fireMouseChanged();
            }
        }
    }

    // ----------------------------------------------------------
    // KeyListener
    // ----------------------------------------------------------
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource() == imagePanel) {
            if (imageCanvas != null) {
                boolean treated = false;
                RoiGenerator generator = getRoiManager().getCurrentRoiGenerator();
                if (generator != null) {
                    treated = generator.keyPressed(e);
                }
                if (treated) {
                    if (e != null) {
                        e.consume();
                    }
                } else {
                    imageCanvas.keyPressed(e);
                    imagePanel.setCursor(imageCanvas.getCursor());
                }
                repaint();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == imagePanel) {
            if (imageCanvas != null) {
                boolean treated = false;
                RoiGenerator generator = getRoiManager().getCurrentRoiGenerator();
                if (generator != null) {
                    treated = generator.keyReleased(e);
                }
                if (treated) {
                    if (e != null) {
                        e.consume();
                    }
                } else {
                    imageCanvas.keyPressed(e);
                    imagePanel.setCursor(imageCanvas.getCursor());
                }
                repaint();
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getSource() == imagePanel) {
            if (imageCanvas != null) {
                boolean treated = false;
                RoiGenerator generator = getRoiManager().getCurrentRoiGenerator();
                if (generator != null) {
                    treated = generator.keyTyped(e);
                }
                if (treated) {
                    if (e != null) {
                        e.consume();
                    }
                } else {
                    imageCanvas.keyTyped(e);
                    imagePanel.setCursor(imageCanvas.getCursor());
                }
                repaint();
            }
        }
    }

    // ----------------------------------------------------------
    // PropertyChangeListener
    // ----------------------------------------------------------
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ((evt != null) && (evt.getSource() == imageCanvas)
                && IJCanvas.MAGNIFICATION_PROPERTY.equals(evt.getPropertyName())) {
            if (zoomLabel != null) {
                zoomLabel.setText(computeZoomLabelText());
            }
        }
    }

    protected void updateImageAndComponents() throws ApplicationIdException {
        matrixTable.setEditable(editable);
        convertImage(false);
        if (isPaintAllowed()) {
            gradientViewer.revalidate();
        }
        updateBeamPosition();
        updateImageCanvasParameters();
    }

    // Convert the image from raw matrix value to SCREEN format according to transformation
    protected void convertImage(boolean repaint) throws ApplicationIdException {
        if (converter == null) {
            converter = new NumberImageConverter(gradientViewer, gColormap);
            converter.setNaNColor(nanColor.getRGB());
            converter.setNegativeInfinityColor(negativeInfinityColor.getRGB());
            converter.setPositiveInfinityColor(positiveInfinityColor.getRGB());
            converter.setImageFillingAllowed(isPaintAllowed());
        }
        if (logScale) {
            computeLogScale(repaint);
        } else {
            convertImage(value, autoBestFit, true, true, repaint);
            updateColorScaleRoiGenerators();
            getGradientViewer().getAxis().getAttributes().setScale(IChartViewer.LINEAR_SCALE);
            getGradientViewer().getAxis().setScale(new LinearScale(getGradientViewer().getAxis().getAttributes()));
            updateGradientFormat(false);
            getGradientViewer().revalidate();
            getGradientViewer().repaint();
        }
    }

    // Convert the image to SCREEN format according to transformation
    protected void convertImage(Object value, boolean autoBestFit, boolean updateGradientViewer,
            boolean updateImageConverter, boolean repaint) throws ApplicationIdException {
        if (updateImageConverter) {
            updateImageConverter(isLogScale());
        }
        BufferedImage img = converter.convertToImage(value, dimX, dimY, theImage, true, autoBestFit,
                updateGradientViewer, isLogScale());
        if (theImage == img) {
            refreshImage();
        } else {
            setImage(img);
        }
        if (repaint) {
            repaint();
        }
    }

    protected void refreshImage() {
        IJRoiManager imp = imageCanvas.getImagePlus();
        if ((imp != null) && (theImage != null)) {
            imp.setImage(theImage);
        }
        applyMaskAndSectorToImage();
        ImageJManager.warnForContentChange(this);
    }

    /**
     * Sets the image to be displayed
     * 
     * @param i Image
     */
    protected void setImage(BufferedImage i) throws ApplicationIdException {
        setImage(i, false);
    }

    protected void setImage(BufferedImage i, boolean ignoreDuplicationTest) throws ApplicationIdException {
        Component rootComponent = SwingUtilities.getRoot(this);
        Cursor cursorToRestore = null;
        if (rootComponent != null) {
            cursorToRestore = rootComponent.getCursor();
            if ((cursorToRestore != null) && (cursorToRestore.getType() == Cursor.WAIT_CURSOR)) {
                cursorToRestore = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
            }
            setCursorToComponent(rootComponent, Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }
        checkApplicationId();
        if (ignoreDuplicationTest || (theImage != i)) {
            Image formerImage = theImage;
            if (formerImage != null) {
                formerImage.flush();
            }
            theImage = i;

            IJRoiManager imp = imagePlus;
            if (theImage != null) {
                removeListeners();
                imp.setImage(theImage);
                // This part ensures Rois and masks drawing, and image best size management
                createImageCanvas(imp);
                addListeners();
            }

        }
        applyMaskAndSectorToImage();
        ImageJManager.warnForContentChange(this);
        if (rootComponent != null) {
            setCursorToComponent(rootComponent, cursorToRestore);
        }
    }

    protected void setCursorToComponent(final Component component, final Cursor cursor) {
        if (component != null) {
            if (SwingUtilities.isEventDispatchThread()) {
                component.setCursor(cursor);
            } else {
                SwingUtilities.invokeLater(() -> {
                    component.setCursor(cursor);
                });
            }
        }
    }

    protected void updateAxisRange(ImageScreenPositionCalculator calculator) {
        updateAxisRange(true, calculator);
        updateAxisRange(false, calculator);
        fireMouseChanged();
    }

    protected void updateTooltip() {
        if (imagePanel != null) {
            String tooltip = imagePanel.getToolTipText();
            if (isAllowBeamPositionByClick()) {
                tooltip = applyCenterName("ImageJViewer.Description.BeamCenter.Change");
            } else {
                tooltip = null;
            }
            imagePanel.setToolTipText(tooltip);
        }
    }

    // /**
    // * Calculates axis min and max, according to its associated {@link IValueConvertor}
    // *
    // * @param origin The origin (generally 0)
    // * @param length The axis length
    // * @param axisConvertor The {@link IValueConvertor}
    // * @return a <code>double[]</code>: <code>{min, max}</code>
    // */
    // protected double[] getAxisMinAndMax(double origin, int length, IValueConvertor axisConvertor) {
    // double min = origin;
    // double max = origin + length;
    // if ((axisConvertor != null) && (axisConvertor.isValid())) {
    // min = axisConvertor.convertValue(min);
    // double beforeMax = max - 1;
    // max = axisConvertor.convertValue(max);
    // // check extreme case
    // if (beforeMax > -1) {
    // beforeMax = axisConvertor.convertValue(beforeMax);
    // if (beforeMax > min) {
    // if ((beforeMax > max) && (max <= min)) {
    // max = beforeMax + NumberArrayUtils.recoverStep(min, beforeMax, length);
    // } else if ((max == origin + length) && (beforeMax != max - 1)) {
    // max = beforeMax + NumberArrayUtils.recoverStep(min, beforeMax, length);
    // } else if (Double.isNaN(max) && (!Double.isNaN(beforeMax)) && (!Double.isNaN(min))) {
    // max = beforeMax + NumberArrayUtils.recoverStep(min, beforeMax, length);
    // }
    // } else if (beforeMax < min) {
    // if ((beforeMax < max) && (max >= min)) {
    // max = beforeMax - NumberArrayUtils.recoverStep(beforeMax, min, length);
    // } else if ((max == origin + length) && (beforeMax != max - 1)) {
    // max = beforeMax - NumberArrayUtils.recoverStep(beforeMax, min, length);
    // } else if (Double.isNaN(max) && (!Double.isNaN(beforeMax)) && (!Double.isNaN(min))) {
    // max = beforeMax - NumberArrayUtils.recoverStep(beforeMax, min, length);
    // }
    // }
    // }
    // }
    // return new double[] { min, max };
    // }

    protected void updateMatrixTableHeaders(final boolean conversion, final boolean x) {
        Runnable runnable = () -> {
            if (matrixTable != null) {
                String[] names = null;
                String format = x ? getXAxisFormat() : getYAxisFormat();
                if (conversion || ((format != null) && (!format.trim().isEmpty()))) {
                    names = x ? generateColumnNames() : generateRowNames();
                }
                if (x) {
                    matrixTable.setCustomColumnNames(names);
                } else {
                    matrixTable.setCustomRowNames(names);
                }
            }
        };
        getDrawingThreadManager().runInDrawingThread(runnable);
    }

    protected void updateAxisRange(final boolean x, ImageScreenPositionCalculator calculator) {
        if (calculator != null) {
            final boolean isMyCalculator = (calculator == getImageScreenPositionCalculator());
            double min, max;
            final boolean conversion;
            boolean autoScale = x ? xAutoScale : yAutoScale;
            if (autoScale) {
                IValueConvertor axisConvertor = x ? xAxisConvertor : yAxisConvertor;
                conversion = (isMyCalculator && (axisConvertor != null) && (axisConvertor.isValid()));
                // double[] minMax = getAxisMinAndMax(0, x ? dimX : dimY, axisConvertor);
                // min = minMax[0];
                // max = minMax[1];
                min = 0;
                max = x ? dimX : dimY;
            } else {
                conversion = false;
                min = x ? getImageScreenPositionCalculator().getXAxisMinUser()
                        : getImageScreenPositionCalculator().getYAxisMinUser();
                max = x ? getImageScreenPositionCalculator().getXAxisMaxUser()
                        : getImageScreenPositionCalculator().getYAxisMaxUser();
            }
            if (x) {
                calculator.setXAxisMin(min);
                calculator.setXAxisMax(max);
                calculator.updateAxisRatioX(dimX);
            } else {
                calculator.setYAxisMin(min);
                calculator.setYAxisMax(max);
                calculator.updateAxisRatioY(dimY);
            }
            if (isMyCalculator && (matrixTable != null)) {
                String[] names = null;
                if (conversion || Format.isNumberFormatOK(x ? getXAxisFormat() : getYAxisFormat())) {
                    names = x ? generateColumnNames() : generateRowNames();
                }
                if (x) {
                    matrixTable.setCustomColumnNames(names);
                } else {
                    matrixTable.setCustomRowNames(names);
                }
            }
            updateAxisRoiGenerators();
        }
    }

    protected int getFontHeight(AbstractAxisView axis, String text) {
        return FontUtils.measureString(text, axis.getAttributes().getLabelFont()).height;
    }

    protected int getFontHeight2(AbstractAxisView axis, String text) {
        Rectangle2D bounds = axis.getAttributes().getLabelFont().getStringBounds(text,
                FontUtils.getDefaultRenderContext());
        return (int) Math.round(bounds.getY() + bounds.getHeight());
    }

    /**
     * Calculate the offset needed to draw axis. We want to draw axis outside the image, so we
     * increase margin with axisLineWidth
     */
    protected void computeAxisMargins() {
        Dimension imageSize = getCanvasSize();
        if (imageSize != null) {
            int imageWidth = imageSize.width;
            int imageHeight = imageSize.height;

            upMargin = AbstractAxisView.DEFAULT_MARGIN;
            downMargin = AbstractAxisView.DEFAULT_MARGIN;
            leftMargin = AbstractAxisView.DEFAULT_MARGIN;
            rightMargin = AbstractAxisView.DEFAULT_MARGIN;

            int width = Math.max(imageWidth, 1);
            int height = Math.max(imageHeight, 1);
            boolean xVisible = xAxis.isVisible(), yVisible = yAxis.isVisible();

            int deltaUp = 0, deltaLeft = 0;
            FontRenderContext frc = null;
            Graphics g = getGraphics();
            if (g instanceof Graphics2D) {
                frc = ((Graphics2D) g).getFontRenderContext();
            }
            if (frc == null) {
                frc = FontUtils.getDefaultRenderContext();
            }

            if (xVisible) {
                int xOrigin = xAxis.getAttributes().getOrigin();
                xAxis.measureAxis(frc, width, null);
                // Vertical margin : xAxis thickness + default margin
                int vMargin = xAxis.getThickness() + AbstractAxisView.DEFAULT_MARGIN;
                String title = xAxis.getAttributes().getTitle();
                if (xOrigin == IChartViewer.HORIZONTAL_UP) {
                    upMargin = Math.max(upMargin, vMargin);
                    deltaUp = -xAxis.getLabelHeight(frc);
                    if (title != null) {
                        xAxis.applyTitleBounds(frc, AbstractAxisView.DEFAULT_MARGIN, AbstractAxisView.DEFAULT_MARGIN,
                                xTitleBounds);
                        deltaUp = xTitleBounds.y;
                    }
                    downMargin += deltaUp;
                } else if (xOrigin == IChartViewer.HORIZONTAL_DOWN) {
                    downMargin = Math.max(downMargin, vMargin);
                }
            }
            if (yVisible) {
                int yOrigin = yAxis.getAttributes().getOrigin();
                yAxis.measureAxis(frc, height, null);
                // Horizontal margin : yAxis thickness + default margin
                int hMargin = yAxis.getThickness() + AbstractAxisView.DEFAULT_MARGIN;
                if (yOrigin == IChartViewer.VERTICAL_LEFT) {
                    leftMargin = Math.max(leftMargin, hMargin);
                } else if (yOrigin == IChartViewer.VERTICAL_RIGHT) {
                    rightMargin = Math.max(rightMargin, hMargin);
                }
            }

            int lineWidth = Math.max(axisLineWidth, 1);
            upMargin += lineWidth;
            downMargin += lineWidth;
            leftMargin += lineWidth;
            rightMargin += lineWidth;

            xOrigine = leftMargin + deltaLeft;
            yOrigine = upMargin + deltaUp;
        }
    }

    public IJCanvas getImageCanvas() {
        return imageCanvas;
    }

    @Override
    public Gradient getGradient() {
        return gradient;
    }

    @Override
    public void setGradient(Gradient theGradient) throws ApplicationIdException {
        quickSetGradient(theGradient);
        updateView(false);
    }

    // JAVAAPI-625: set gradient without updating view
    protected void quickSetGradient(Gradient theGradient) {
        gradient = theGradient;
        gradientViewer.setGradient(theGradient);
        gradientEditor.setGradient(theGradient);
        gColormap = theGradient.buildColorMap(65536);
    }

    // JAVAAPI-625: call convertImage and maybe updateBestFitInfo
    protected void updateView(boolean updateFit) {
        // Don't call convertImage in a synchronized block to avoid a dead lock (JAVAAPI-641)
        convertImage(true);
        if (updateFit) {
            updateBestFitInfo();
        }
    }

    public boolean isLockGradientEditorStartAndEndCursors() {
        return gradientEditor.isLockStartAndEndCursors();
    }

    public void setLockGradientEditorStartAndEndCursors(boolean lock) {
        gradientEditor.setLockStartAndEndCursors(lock);
    }

    public JGradientViewer getGradientViewer() {
        return gradientViewer;
    }

    public boolean isUseImageConverterMinimumInLogScale() {
        return useImageConverterMinimumInLogScale;
    }

    public void setUseImageConverterMinimumInLogScale(boolean useImageConverterMinimumInLogScale) {
        this.useImageConverterMinimumInLogScale = useImageConverterMinimumInLogScale;
    }

    /**
     * Returns true when automatic best fit is enabled
     * 
     * @return Auto best fit state
     */
    public boolean isAutoBestFit() {
        return autoBestFit;
    }

    // JAVAAPI-625: set auto best fit without updating view
    protected void quickSetAutoBestFit(boolean bestFit) {
        this.autoBestFit = bestFit;
    }

    /**
     * Sets the auto Best fit mode. Computes maximum and minimum value of the image when enabled
     * else uses the min and max best fit user values.
     * 
     * @param bestFit True to enable auto best fit
     */
    public void setAutoBestFit(boolean bestFit) throws ApplicationIdException {
        quickSetAutoBestFit(bestFit);
        updateView(true);
    }

    protected void applyFitMinAndMaxFromFields() throws ApplicationIdException {
        Number fitMin = (Number) minFitTextField.getValue();
        Number fitMax = (Number) maxFitTextField.getValue();

        if ((fitMin == null) || (fitMax == null) || (fitMin.doubleValue() >= fitMax.doubleValue())) {
            JOptionPane.showMessageDialog(this, fitBoundsErrorText, fitBoundsErrorTitle, JOptionPane.ERROR_MESSAGE);

            fitMin = Integer.valueOf(DEFAULT_MIN_FIT);
            fitMax = Integer.valueOf(DEFAULT_MAX_FIT);
            minFitTextField.setValue(fitMin);
            maxFitTextField.setValue(fitMax);
            setAutoBestFit(true);
        } else {
            setFitMinMax(fitMin.doubleValue(), fitMax.doubleValue(), false);
        }
        updateBestFitInfo();
    }

    /**
     * Updates the state of the best fit checkbox and of the fit bounds setting fields and button
     */
    protected void updateBestFitInfo() {
        getAction(SET_AUTO_BEST_FIT_ACTION).setSelected(isAutoBestFit());
        getAction(LOG_SCALE_ACTION).setSelected(isLogScale());
        minFitTextField.setEnabled(!isAutoBestFit());
        maxFitTextField.setEnabled(!isAutoBestFit());
        applyFitButton.setEnabled(!isAutoBestFit());
    }

    /**
     * @return the fitMin
     */
    public double getFitMin() {
        return fitMin;
    }

    /**
     * @return the fitMax
     */
    public double getFitMax() {
        return fitMax;
    }

    public void setFitMinMax(double min, double max) throws ApplicationIdException {
        setFitMinMax(min, max, true);
        updateColorScaleRoiGenerators();
    }

    protected synchronized void setFitMinMax(double min, double max, boolean disableAutoBestFit)
            throws ApplicationIdException {
        setFitMinMaxNoLock(min, max, disableAutoBestFit);
    }

    // JAVAAPI-625: add the possibility not to update view and not to notify zoom roi generators
    protected synchronized void setFitMinMax(double min, double max, boolean disableAutoBestFit, boolean mayUpdateView)
            throws ApplicationIdException {
        setFitMinMaxNoLock(min, max, disableAutoBestFit, mayUpdateView);
    }

    protected final void setFitMinMaxNoLock(double min, double max, boolean disableAutoBestFit)
            throws ApplicationIdException {
        setFitMinMaxNoLock(min, max, disableAutoBestFit, true);
    }

    // JAVAAPI-625: add the possibility not to update view and not to notify zoom roi generators
    protected final void setFitMinMaxNoLock(double min, double max, boolean disableAutoBestFit, boolean mayUpdateView)
            throws ApplicationIdException {
        if (min < max) {
            fitMin = min;
            fitMax = max;
        }
        if (disableAutoBestFit) {
            autoBestFit = false;
        }
        if (autoBestFit) {
            if (mayUpdateView) {
                updateBestFitInfo();
            }
        } else {
            minFitTextField.setValue(fitMin);
            maxFitTextField.setValue(fitMax);
            if (mayUpdateView) {
                updateView(true);
            }
        }
    }

    /**
     * @return the autoScaleX
     */
    public boolean isAutoScaleX() {
        return xAutoScale;
    }

    /**
     * @param autoScale the autoScaleX to set
     */
    public void setAutoScaleX(boolean autoScale) {
        if (autoScale != xAutoScale) {
            xAutoScale = autoScale;
            xAxis.getAttributes().setAutoScale(xAutoScale);
            updateAxisRange(true, getImageScreenPositionCalculator());
            fireMouseChanged();
            computeBeamPositionFromAxis(getImageScreenPositionCalculator());
        }
    }

    /**
     * @return the yAutoScale
     */
    public boolean isAutoScaleY() {
        return yAutoScale;
    }

    /**
     * @param autoScale the yAutoScale to set
     */
    public void setAutoScaleY(boolean autoScale) {
        if (autoScale != yAutoScale) {
            yAutoScale = autoScale;
            yAxis.getAttributes().setAutoScale(yAutoScale);
            updateAxisRange(false, getImageScreenPositionCalculator());
            fireMouseChanged();
            computeBeamPositionFromAxis(getImageScreenPositionCalculator());
        }
    }

    public double[] getAxisRangeX() {
        return new double[] { getImageScreenPositionCalculator().getXAxisMin(),
                getImageScreenPositionCalculator().getXAxisMax() };
    }

    /**
     * @param axisMin
     * @param axisMax
     */
    public void setAxisRangeX(double axisMin, double axisMax) {
        setAxisRangeX(axisMin, axisMax, true);
    }

    protected void setAxisRangeX(double axisMin, double axisMax, boolean repaint) {
        getImageScreenPositionCalculator().setXAxisMinUser(axisMin);
        getImageScreenPositionCalculator().setXAxisMaxUser(axisMax);

        // if autoscale, new range is not considered, no need to update mouse coordinates
        if (!xAutoScale) {
            getImageScreenPositionCalculator().setXAxisMin(getImageScreenPositionCalculator().getXAxisMinUser());
            getImageScreenPositionCalculator().setXAxisMax(getImageScreenPositionCalculator().getXAxisMaxUser());
            getImageScreenPositionCalculator().updateAxisRatioX(dimX);

            fireMouseChanged();
            computeBeamPositionFromAxis(getImageScreenPositionCalculator());
        }
        if (repaint) {
            repaint();
        }
    }

    public double[] getAxisRangeY() {
        return new double[] { getImageScreenPositionCalculator().getYAxisMin(),
                getImageScreenPositionCalculator().getYAxisMax() };
    }

    /**
     * @param axisMin
     * @param axisMax
     */
    public void setAxisRangeY(double axisMin, double axisMax) {
        setAxisRangeY(axisMin, axisMax, true);
    }

    protected void setAxisRangeY(double axisMin, double axisMax, boolean repaint) {
        getImageScreenPositionCalculator().setYAxisMinUser(axisMin);
        getImageScreenPositionCalculator().setYAxisMaxUser(axisMax);

        // if autoscale, new range is not considered, no need to update mouse coordinates
        if (!yAutoScale) {
            getImageScreenPositionCalculator().setYAxisMin(getImageScreenPositionCalculator().getYAxisMinUser());
            getImageScreenPositionCalculator().setYAxisMax(getImageScreenPositionCalculator().getYAxisMaxUser());
            getImageScreenPositionCalculator().updateAxisRatioY(dimY);

            fireMouseChanged();
            computeBeamPositionFromAxis(getImageScreenPositionCalculator());
        }
        if (repaint) {
            repaint();
        }
    }

    /**
     * @return
     */
    @Override
    public IJRoiManager getRoiManager() {
        return imagePlus;
    }

    public JTable getRoiInfoTable() {
        return roiInfoTable;
    }

    @Override
    public void setRoiManager(IJRoiManager roiManager) {
        if ((roiManager != null) && (roiManager != this.imagePlus)) {
            IJRoiManager old = imagePlus;
            if (old != null) {
                old.removeUndoableEditListener(undoManager);
            }
            clean();
            this.imagePlus = roiManager;
            roiManager.setImageViewer(this);
            roiManager.addUndoableEditListener(undoManager);
            roiManager.addRoiSelectionListener(roiInfoTable);
            roiManager.addRoiManagerListener((IRoiManagerListener) roiInfoTable.getModel());
            setImage(theImage, true);
            for (Roi roi : roiManager.getAllRois()) {
                roi.setImage(roiManager);
            }
            updateBeamPosition();
        }
    }

    /**
     * Be careful using the axis. Range and autoscale are managed in the viewer.
     * 
     * @return the xAxis
     */
    public HorizontalAxisView getXAxis() {
        return xAxis;
    }

    /**
     * Be careful using the axis. Range and autoscale are managed in the viewer.
     * 
     * @return the yAxis
     */
    public VerticalAxisView getYAxis() {
        return yAxis;
    }

    @Override
    public boolean isDrawBeamPosition() {
        return drawBeamPosition;
    }

    @Override
    public void setDrawBeamPosition(boolean drawBeam) {
        drawBeamPosition = drawBeam;
        getAction(DISPLAY_BEAM_CENTER_ACTION).setSelected(drawBeamPosition);
        if (isBeamPointEnabled()) {
            imageCanvas.setDrawBeamPosition(isDrawBeamPosition());
        }
        repaint();
    }

    public boolean isAllowBeamPositionByClick() {
        return allowBeamPositionByClick;
    }

    public void setAllowBeamPositionByClick(boolean allowBeamPositionByClick) {
        this.allowBeamPositionByClick = allowBeamPositionByClick;
        getAction(ALLOW_BEAM_CENTER_BY_CLICK_ACTION).setSelected(allowBeamPositionByClick);
        updateTooltip();
    }

    public boolean isBeamPointEnabled() {
        return beamPointEnabled;
    }

    public void setBeamPointEnabled(boolean beamPointEnabled) {
        this.beamPointEnabled = beamPointEnabled;
    }

    @Override
    public double[] getBeamPoint() {
        return beamPoint;
    }

    @Override
    public void setBeamPoint(double... theBeamPoint) {
        beamPoint = theBeamPoint;
        computeBeamPositionFromAxis(getImageScreenPositionCalculator());
        if (isBeamPointEnabled() && isDrawBeamPosition()) {
            repaint();
        }
    }

    /**
     * Sets the beam point, knowing where click occurred to set it.
     * 
     * @param ox The click x position in image coordinates (i.e. offscreen x).
     * @param oy The click y position in image coordinates (i.e. offscreen y).
     * @param theBeamPoint The beam point to set.
     */
    protected void setBeamPointFromClick(double ox, double oy, double... theBeamPoint) {
        boolean canSet;
        if (theBeamPoint == null || theBeamPoint.length == 0) {
            canSet = false;
        } else {
            canSet = true;
            for (double value : theBeamPoint) {
                if (!Double.isFinite(value)) {
                    canSet = false;
                    break;
                }
            }
        }
        if (canSet) {
            beamPoint = theBeamPoint;
            if (isBeamPointEnabled()) {
                imageCanvas.setBeamPoint(new Point2D.Double(ox, oy));
                imageCanvas.setDrawBeamPosition(isDrawBeamPosition());
                repaint();
            }
        }
    }

    protected void setBeamPoint(Point2D.Double point) {
        if (point == null) {
            setBeamPoint((double[]) null);
        } else {
            setBeamPoint(point.x, point.y);
        }
    }

    @Override
    public void clearBeamPoint() {
        beamPoint = null;
        if (imageCanvas != null) {
            imageCanvas.clearBeamPoint();
        }
    }

    public boolean isHorizontalDataForVerticalProfiler() {
        return horizontalDataForVerticalProfiler;
    }

    public Chart getVerticalProfiler() {
        return getVerticalProfiler(false);
    }

    public Chart getVerticalProfiler(boolean vertical) {
        return getVerticalProfiler(vertical, true);
    }

    public Chart getVerticalProfiler(boolean vertical, boolean autoConstruct) {
        if ((verticalProfiler == null) && autoConstruct) {
            horizontalDataForVerticalProfiler = !vertical;
            verticalProfiler = createLineProfiler();
            if (vertical) {
                verticalProfiler.setAxisPosition(IChartViewer.HORIZONTAL_UP, IChartViewer.X);
            }
            verticalProfiler.setOpaque(false);
            verticalProfiler.setLegendVisible(false);
            verticalProfiler.setDataViewCometeColor(VERTICAL_PROFILE_NAME, CometeColor.RED);
            verticalProfiler.setDataViewLabelVisible(VERTICAL_PROFILE_NAME, false);
            verticalProfiler.setDataViewLineWidth(VERTICAL_PROFILE_NAME, 2);
            verticalProfiler.setDataViewCometeColor(CROSSHAIR_NAME, CometeColor.BLACK);
            verticalProfiler.setDataViewLabelVisible(CROSSHAIR_NAME, false);
            verticalProfiler.getChartView().getAxis(IChartViewer.Y1).getAttributes()
                    .setInverted(horizontalDataForVerticalProfiler);
            updateProfilersFormat();
        }
        return verticalProfiler;
    }

    public Chart getHorizontalProfiler() {
        return getHorizontalProfiler(true);
    }

    public Chart getHorizontalProfiler(boolean autoConstruct) {
        if ((horizontalProfiler == null) && autoConstruct) {
            horizontalProfiler = createLineProfiler();
            horizontalProfiler.setOpaque(false);
            horizontalProfiler.setLegendVisible(false);
            horizontalProfiler.setDataViewCometeColor(HORIZONTAL_PROFILE_NAME, CometeColor.RED);
            horizontalProfiler.setDataViewLabelVisible(HORIZONTAL_PROFILE_NAME, false);
            horizontalProfiler.setDataViewLineWidth(HORIZONTAL_PROFILE_NAME, 2);
            horizontalProfiler.setDataViewCometeColor(CROSSHAIR_NAME, CometeColor.BLACK);
            horizontalProfiler.setDataViewLabelVisible(CROSSHAIR_NAME, false);
            updateProfilersFormat();
        }
        return horizontalProfiler;
    }

    public Chart getHistogramViewer() {
        return histogramDialog == null ? null : histogramDialog.getHistogramViewer();
    }

    public JScrollPane getImageScrollPane() {
        return imageScrollPane;
    }

    public JComponent getImagePanel() {
        return imagePanel;
    }

    protected Chart createLineProfiler() {
        Chart profiler = new Chart();
        profiler.setRefreshLater(true);

        SamplingProperties samplingProperties = profiler.getSamplingProperties();
        samplingProperties.setEnabled(false);
        profiler.setSamplingProperties(samplingProperties);
        AxisProperties axisProperties = profiler.getAxisProperties(IChartViewer.X);
        axisProperties.setAutoScale(false);
        axisProperties.setUserLabelInterval(Double.NaN);
        profiler.setAxisProperties(axisProperties, IChartViewer.X);
        axisProperties = profiler.getAxisProperties(IChartViewer.Y1);
        axisProperties.setAutoScale(false);
        axisProperties.setUserLabelInterval(Double.NaN);
        profiler.setAxisProperties(axisProperties, IChartViewer.Y1);
        axisProperties = profiler.getAxisProperties(IChartViewer.Y2);
        axisProperties.setAutoScale(false);
        axisProperties.setUserLabelInterval(Double.NaN);
        axisProperties.setVisible(false);
        profiler.setAxisProperties(axisProperties, IChartViewer.Y2);
        profiler.setHeader(null);
        profiler.setManagementPanelVisible(false);
        profiler.setFreezePanelVisible(false);
        return profiler;
    }

    protected String getHistogramName(String name) {
        return name == null || name.trim().isEmpty() ? HISTOGRAM_NAME : name;
    }

    /**
     * Update state and tooltip
     */
    protected void updateUndoRedoActionState() {
        AbstractActionExt undoAction = getAction(UNDO_ACTION);
        AbstractActionExt redoAction = getAction(REDO_ACTION);

        if (isManageActionsAvailability()) {
            boolean canUndo = undoManager.canUndo();
            undoAction.setEnabled(canUndo);
            boolean canRedo = undoManager.canRedo();
            redoAction.setEnabled(canRedo);
        }

        undoAction.setShortDescription(undoManager.getUndoPresentationName());
        redoAction.setShortDescription(undoManager.getRedoPresentationName());
    }

    /*
     * Return the available image writer formats sorted alphabetically and in
     * lower case
     */
    public static ArrayList<String> getAvailableImageWriterFormats() {
        String[] formats = ImageIO.getWriterFormatNames();
        TreeSet<String> formatSet = new TreeSet<String>();
        for (String s : formats) {
            formatSet.add(s.toLowerCase());
        }
        return new ArrayList<String>(formatSet);
    }

    public void printImage() {
        ComponentPrinter printer = new ComponentPrinter(this);
        printer.setFitMode(ComponentPrinter.FIT_PAGE);
        printer.setJobName(MESSAGES.getString("ImageJViewer.PrintImage.Title"));
        printer.print();
    }

    public AbstractActionExt getAction(String actionId) {
        Action action = ActionManager.getInstance().getAction(getTransformedActionCommand(actionId));
        if (action == null) {
            action = ActionManager.getInstance().getAction(actionId);
        }
        return (AbstractActionExt) action;
    }

    // ---------------------------------
    // convenience methods
    // ---------------------------------
    public ActionContainerFactory getContainerFactory() {
        return containerFactory;
    }

    public JToggleButton createRectangleButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_RECTANGLE_ACTION), container);
    }

    public JToggleButton createEllipseButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_ELLIPSE_ACTION), container);
    }

    public JToggleButton createPolygonButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_POLYGON_ACTION), container);
    }

    public JToggleButton createFreeHandButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_FREE_HAND_ACTION), container);
    }

    public JToggleButton createLineButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_LINE_ACTION), container);
    }

    public JToggleButton createPointButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_POINT_ACTION), container);
    }

    public JToggleButton createWandButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_WAND_ACTION), container);
    }

    public JToggleButton createPanButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_PAN_ACTION), container);
    }

    public JToggleButton createSelectionButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_SELECTION_ACTION), container);
    }

    public JToggleButton createZoomButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_ZOOM_ACTION), container);
    }

    public JToggleButton createFocusButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_FOCUS_ACTION), container);
    }

    public JToggleButton createLineProfileButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_LINE_PROFILE_ACTION), container);
    }

    public JToggleButton createCalibrationButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_CALIBRATION_ACTION), container);
    }

    public JToggleButton createHistogramButton(JComponent container) {
        return (JToggleButton) containerFactory.createButton(getAction(MODE_HISTOGRAM_ACTION), container);
    }

    public JButton createDeleteSelectedRoisButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(DELETE_SELECTED_ROIS_ACTION), container);
    }

    public JButton createDeleteAllRoisButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(DELETE_ALL_ROIS_ACTION), container);
    }

    public JButton createZoomInButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(ZOOM_IN_ACTION), container);
    }

    public JButton createZoomOutButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(ZOOM_OUT_ACTION), container);
    }

    public JButton createOrSelectedRoisButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(OR_SELECTED_ROIS_ACTION), container);
    }

    public JButton createAndSelectedRoisButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(AND_SELECTED_ROIS_ACTION), container);
    }

    public JButton createNotSelectedRoisButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(NOT_SELECTED_ROIS_ACTION), container);
    }

    public JButton createXorSelectedRoisButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(XOR_SELECTED_ROIS_ACTION), container);
    }

    public JButton createUndoButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(UNDO_ACTION), container);
    }

    public JButton createRedoButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(REDO_ACTION), container);
    }

    public JButton createSnapshotButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(SNAPSHOT_ACTION), container);
    }

    public JButton createPrintButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(PRINT_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to display Matrix data in {@link JTable}
     * 
     * @return a {@link JButton}
     */
    public JButton createShowTableButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(SHOW_TABLE_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to unzoom
     * 
     * @return a {@link JButton}
     */
    public JButton createUnZoomButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(UNZOOM_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to set {@link Roi}s {@link Color}s
     * 
     * @return a {@link JButton}
     */
    public JButton createSetRoiColorsButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(DISPLAY_ROI_COLORS_MENU_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to set {@link Mask} {@link Color}
     * 
     * @return a {@link JButton}
     */
    public JButton createSetMaskColorButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(SET_MASK_COLOR_ACTION), container);
    }

    public JButton createSetBeamPositionColorButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(SET_BEAM_POSITION_COLOR_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to set {@link MathematicSector}s {@link Color}s
     * 
     * @return a {@link JButton}
     */
    public JButton createSetSectorColorsButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(DISPLAY_SECTOR_COLORS_MENU_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to set a {@link Roi} as Inner
     * 
     * @return a {@link JButton}
     */
    public JButton createInnerRoiButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(INNER_ROIS_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to set a {@link Roi} as Outer
     * 
     * @return a {@link JButton}
     */
    public JButton createOuterRoiButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(OUTER_ROIS_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to invalidate a {@link Roi}
     * 
     * @return a {@link JButton}
     */
    public JButton createInvalidateRoiButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(INVALIDATE_ROIS_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to set ROI's {@link Mask} as {@link Mask}
     * 
     * @return a {@link JButton}
     */
    public JButton createSetMaskButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(SET_AS_MASK_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to add ROI's {@link Mask} to {@link Mask}
     * 
     * @return a {@link JButton}
     */
    public JButton createAddToMaskButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(ADD_TO_MASK_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to remove ROI's visible zone from {@link Mask}
     * 
     * @return a {@link JButton}
     */
    public JButton createRemoveFromMaskButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(REMOVE_FROM_MASK_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to set a new sector
     * 
     * @return a {@link JButton}
     */
    public JButton createNewSectorButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(NEW_SECTOR_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to change sector
     * 
     * @return a {@link JButton}
     */
    public JButton createEditSectorButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(EDIT_SECTOR_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to remove sector
     * 
     * @return a {@link JButton}
     */
    public JButton createRemoveSectorButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(DELETE_SECTOR_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to clear {@link Mask}
     * 
     * @return a {@link JButton}
     */
    public JButton createClearMaskButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(CLEAR_MASK_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to save current {@link Mask}
     * 
     * @return a {@link JButton}
     */
    public JButton createSaveMaskButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(SAVE_MASK_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to load a {@link Mask}
     * 
     * @return a {@link JButton}
     */
    public JButton createLoadMaskButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(LOAD_MASK_ACTION), container);
    }

    /**
     * Creates a {@link JButton} used to load a {@link Mask} and add it to current {@link Mask}
     * 
     * @return a {@link JButton}
     */
    public JButton createLoadMaskAddButton(JComponent container) {
        return (JButton) containerFactory.createButton(getAction(LOAD_MASK_ADD_ACTION), container);
    }

    /**
     * Creates a {@link JMenuItem} used to activate/deactivate the possibility for this component to
     * always fit maximum available space
     * 
     * @return a {@link JMenuItem}
     */
    public JMenuItem createFitMaxSizeItem(JComponent container) {
        return containerFactory.createMenuItem(getAction(FIT_MAX_SIZE_ACTION), container);
    }

    /**
     * Returns whether image painting is allowed
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isPaintAllowed() {
        return paintAllowed;
    }

    /**
     * Sets whether image painting is allowed
     * 
     * @param paintAllowed whether image painting is allowed
     */
    public void setPaintAllowed(boolean paintAllowed) {
        if (this.paintAllowed != paintAllowed) {
            this.paintAllowed = paintAllowed;
            if (converter != null) {
                converter.setImageFillingAllowed(paintAllowed);
            }
            if (imageCanvas != null) {
                imageCanvas.setPaintAllowed(paintAllowed);
            }
            updateImageAndComponents();
        }
    }

    protected double[] computeLog(Object values) {
        double[] logs;
        if (values == null) {
            logs = null;
        } else {
            double logMin;
            double bfMin = converter.getBfMin();
            if (bfMin <= 0) {
                logMin = 0;
            } else {
                logMin = Math.log10(bfMin);
            }
            int length = Array.getLength(values);
            logs = new double[length];
            if (values instanceof byte[]) {
                byte[] array = (byte[]) values;
                int i = 0;
                for (byte value : array) {
                    if (value < 0) {
                        logs[i++] = Double.NEGATIVE_INFINITY;
                    } else if ((value == 0) && isUseImageConverterMinimumInLogScale()) {
                        logs[i++] = logMin;
                    } else {
                        logs[i++] = Math.log10(value);
                    }
                }
            } // end if (values instanceof byte[])
            else if (values instanceof short[]) {
                short[] array = (short[]) values;
                int i = 0;
                for (short value : array) {
                    if (value < 0) {
                        logs[i++] = Double.NEGATIVE_INFINITY;
                    } else if ((value == 0) && isUseImageConverterMinimumInLogScale()) {
                        logs[i++] = logMin;
                    } else {
                        logs[i++] = Math.log10(value);
                    }
                }
            } // end if (values instanceof short[])
            else if (values instanceof int[]) {
                int[] array = (int[]) values;
                int i = 0;
                for (int value : array) {
                    if (value < 0) {
                        logs[i++] = Double.NEGATIVE_INFINITY;
                    } else if ((value == 0) && isUseImageConverterMinimumInLogScale()) {
                        logs[i++] = logMin;
                    } else {
                        logs[i++] = Math.log10(value);
                    }
                }
            } // end if (values instanceof int[])
            else if (values instanceof long[]) {
                long[] array = (long[]) values;
                int i = 0;
                for (long value : array) {
                    if (value < 0) {
                        logs[i++] = Double.NEGATIVE_INFINITY;
                    } else if ((value == 0) && isUseImageConverterMinimumInLogScale()) {
                        logs[i++] = logMin;
                    } else {
                        logs[i++] = Math.log10(value);
                    }
                }
            } // end if (values instanceof long[])
            else if (values instanceof float[]) {
                float[] array = (float[]) values;
                int i = 0;
                for (float value : array) {
                    if (value < 0) {
                        logs[i++] = Double.NEGATIVE_INFINITY;
                    } else if ((value == 0) && isUseImageConverterMinimumInLogScale()) {
                        logs[i++] = logMin;
                    } else {
                        logs[i++] = Math.log10(value);
                    }
                }
            } // end if (values instanceof float[])
            else if (values instanceof double[]) {
                double[] array = (double[]) values;
                int i = 0;
                for (double value : array) {
                    if (value < 0) {
                        logs[i++] = Double.NEGATIVE_INFINITY;
                    } else if ((value == 0) && isUseImageConverterMinimumInLogScale()) {
                        logs[i++] = logMin;
                    } else {
                        logs[i++] = Math.log10(value);
                    }
                }
            } // end if (values instanceof double[])
            else if (values instanceof Number[]) {
                Number[] array = (Number[]) values;
                int i = 0;
                for (Number value : array) {
                    double val = NumberImageConverter.extractDouble(value);
                    if (val < 0) {
                        logs[i++] = Double.NEGATIVE_INFINITY;
                    } else if ((val == 0) && isUseImageConverterMinimumInLogScale()) {
                        logs[i++] = logMin;
                    } else {
                        logs[i++] = Math.log10(val);
                    }
                }
            } // end if (values instanceof Number[])
        }
        return logs;
    }

    protected void computeLogScale(boolean repaint) throws ApplicationIdException {
        // JAVAAPI-383: save fitMin and fitMax before treatment
        double savedFitMin = fitMin, savedFitMax = fitMax;

        // Compute axis min and max before applying log.
        // This is needed because some changes may not have been applied on GradientViewer yet
        updateImageConverter(false);
        double bestFitMin, bestFitMax;
        // DATAREDUC-856: ask for strictly positive min and max in case of logarithmic scale
        double[] minAndMax = converter.preComputeBestFit(value, dimX, dimY, true, autoBestFit, true);
        if (minAndMax == null) {
            bestFitMin = getGradientViewer().getAxis().getAttributes().getMinimum();
            bestFitMax = getGradientViewer().getAxis().getAttributes().getMaximum();
        } else {
            bestFitMin = minAndMax[0];
            bestFitMax = minAndMax[1];
        }

        // Compute log
        double[] logValue = computeLog(value);

        synchronized (this) {
            // JAVAAPI-383: update fit min and fit max, and force autoBestFit to false
            // DATAREDUC-745: ensure more often gradient axis scales drawing
            bestFitMin = getBestMinForLogScale(bestFitMin, bestFitMax);
            fitMin = bestFitMin;
            fitMax = bestFitMax;
            updateImageConverter(true);
            converter.preComputeBestFit(logValue, dimX, dimY, true, false, false);
            convertImage(logValue, false, false, false, repaint);
            // JAVAAPI-383: restore fitMin and fitMax
            fitMin = savedFitMin;
            fitMax = savedFitMax;
        }
        // Apply GradientViewer's axis real min and max (not log ones)
        getGradientViewer().getAxis().getAttributes().setMinimum(bestFitMin);
        getGradientViewer().getAxis().getAttributes().setMaximum(bestFitMax);

        updateAxisRange(getImageScreenPositionCalculator());

        updateColorScaleRoiGenerators();

        profilesFollowImage();

        // Set GradientViewer's axis in log scale
        if ((bestFitMin == 0) && (bestFitMax > 0) && (ChartUtils.computeLowTen(bestFitMax) == 0)) {
            bestFitMin = bestFitMax / 10;
            getGradientViewer().getAxis().getAttributes().setMinimum(bestFitMin);
        }
        getGradientViewer().getAxis().getAttributes().setScale(IChartViewer.LOG_SCALE);
        getGradientViewer().getAxis().setScale(new LogarithmicScale(getGradientViewer().getAxis().getAttributes()));
        updateGradientFormat(false);
        getGradientViewer().revalidate();
        getGradientViewer().repaint();
    }

    protected void updateColorScaleRoiGenerators() {
        for (RoiGenerator generator : getRoiManager().getRoiGenerators()) {
            if ((generator != null) && (generator.isColorScaleDependent())) {
                generator.colorScaleChanged(this);
            }
        }
    }

    protected void updateDataRoiGenerators() {
        for (RoiGenerator generator : getRoiManager().getRoiGenerators()) {
            if ((generator != null) && (generator.isDataDependent())) {
                generator.dataChanged(this);
            }
        }
    }

    protected void updateAxisRoiGenerators() {
        for (RoiGenerator generator : getRoiManager().getRoiGenerators()) {
            if ((generator != null) && (generator.isAxisDependent())) {
                generator.axisChanged(this);
            }
        }
    }

    protected void updatePixelSizeRoiGenerators() {
        for (RoiGenerator generator : getRoiManager().getRoiGenerators()) {
            if ((generator != null) && (generator.isPixelSizeDependent())) {
                generator.pixelSizeChanged(this);
            }
        }
    }

    protected void updateZoomRoiGenerators() {
        for (RoiGenerator generator : getRoiManager().getRoiGenerators()) {
            if ((generator != null) && (generator.isZoomDependent())) {
                generator.zoomChanged(this);
            }
        }
    }

    public boolean isLogScale() {
        return logScale;
    }

    public void setLogScale(boolean logScale) throws ApplicationIdException {
        if (logScale != this.logScale) {
            quickSetLogScale(logScale);
            updateView(true);
        }
    }

    // JAVAAPI-625: set log scale without updating view
    protected void quickSetLogScale(boolean logScale) {
        this.logScale = logScale;
    }

    /**
     * Returns whether zoom/unzoom with mouse wheel is centered on cursor position
     * 
     * @return A <code>boolean</code>
     * @deprecated use {@link #getMouseZoomMode()} instead
     */
    @Deprecated
    public boolean isCenterZoomOnCursor() {
        return mouseZoomMode == MouseZoomMode.CENTERED;
    }

    /**
     * Sets whether zoom/unzoom with mouse wheel should be centered on cursor position
     * 
     * @param centerZoomOnCursor Whether zoom/unzoom with mouse wheel should be centered on cursor position
     * @deprecated use {@link #setMouseZoomMode(MouseZoomMode)} instead
     */
    @Deprecated
    public void setCenterZoomOnCursor(boolean centerZoomOnCursor) {
        setMouseZoomMode(centerZoomOnCursor ? MouseZoomMode.CENTERED : MouseZoomMode.NONE);
    }

    /**
     * Returns the way mouse wheel interacts with image zoom
     * 
     * @return A {@link MouseZoomMode}
     */
    public MouseZoomMode getMouseZoomMode() {
        return mouseZoomMode;
    }

    /**
     * Sets the way mouse wheel interacts with image zoom
     * 
     * @param mouseZoomMode The {@link MouseZoomMode} that represents the way mouse wheel interacts with image zoom
     */
    public void setMouseZoomMode(MouseZoomMode mouseZoomMode) {
        if (mouseZoomMode == null) {
            mouseZoomMode = MouseZoomMode.NONE;
        }
        this.mouseZoomMode = mouseZoomMode;
        IJCanvas imageCanvas = this.imageCanvas;
        if (imageCanvas != null) {
            imageCanvas.setMouseZoomMode(mouseZoomMode);
        }
        switch (mouseZoomMode) {
            case NONE:
                getAction(MOUSE_WHEEL_ZOOM_DEFAULT).setSelected(true);
                break;
            case CENTERED:
                getAction(MOUSE_WHEEL_ZOOM_CENTER).setSelected(true);
                break;
            case PIXEL:
                getAction(MOUSE_WHEEL_ZOOM_PIXEL).setSelected(true);
                break;
        }
    }

    /**
     * Returns whether statistics are displayed.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isStatisticsVisible() {
        return showStatisticsCheckBox.isSelected();
    }

    /**
     * Sets whether statistics should be displayed.
     * 
     * @param visible Whether statistics should be displayed.
     */
    public void setStatisticsVisible(boolean visible) {
        if (visible != isStatisticsVisible()) {
            showStatisticsCheckBox.setSelected(visible);
        }
        statisticsComponent.setVisible(visible);
        // Update statistics when panel becomes visible (detected during JAVAAPI-643)
        if (isStatisticsVisible() && (statisticsComponent instanceof IStatisticsComponent)) {
            ((IStatisticsComponent) statisticsComponent).computeStatisticsAndDisplayRoiInformation(this);
        }
        infoPanel.revalidate();
        infoScrollPane.revalidate();
        revalidate();
        repaint();
    }

    /**
     * Constructs a full toolbar.
     * 
     * @return a default toolbar with all available buttons
     */
    public JToolBar createDefaultToolBar() {
        JToolBar defaultToolBar = new JToolBar();
        defaultToolBar.setFloatable(false);

        addButton(defaultToolBar, createRectangleButton(defaultToolBar));
        addButton(defaultToolBar, createEllipseButton(defaultToolBar));
        addButton(defaultToolBar, createPolygonButton(defaultToolBar));
        addButton(defaultToolBar, createFreeHandButton(defaultToolBar));
        addButton(defaultToolBar, createLineButton(defaultToolBar));
        addButton(defaultToolBar, createPointButton(defaultToolBar));
        addButton(defaultToolBar, createWandButton(defaultToolBar));
        defaultToolBar.addSeparator();
        addButton(defaultToolBar, createSelectionButton(defaultToolBar));
        defaultToolBar.addSeparator();
        addButton(defaultToolBar, createOrSelectedRoisButton(defaultToolBar));
        addButton(defaultToolBar, createAndSelectedRoisButton(defaultToolBar));
        addButton(defaultToolBar, createNotSelectedRoisButton(defaultToolBar));
        addButton(defaultToolBar, createXorSelectedRoisButton(defaultToolBar));
        defaultToolBar.addSeparator();
        addButton(defaultToolBar, createDeleteSelectedRoisButton(defaultToolBar));
        addButton(defaultToolBar, createDeleteAllRoisButton(defaultToolBar));
        defaultToolBar.addSeparator();
        if (isUseMaskManagement()) {
            addButton(defaultToolBar, createInnerRoiButton(defaultToolBar));
            addButton(defaultToolBar, createOuterRoiButton(defaultToolBar));
            defaultToolBar.addSeparator();
            addButton(defaultToolBar, createSetMaskButton(defaultToolBar));
            addButton(defaultToolBar, createAddToMaskButton(defaultToolBar));
            addButton(defaultToolBar, createRemoveFromMaskButton(defaultToolBar));
            addButton(defaultToolBar, createClearMaskButton(defaultToolBar));
            defaultToolBar.addSeparator();
            if (isUseSectorManagement()) {
                addButton(defaultToolBar, createNewSectorButton(defaultToolBar));
                addButton(defaultToolBar, createEditSectorButton(defaultToolBar));
                addButton(defaultToolBar, createRemoveSectorButton(defaultToolBar));
                defaultToolBar.addSeparator();
            }
            addButton(defaultToolBar, createSetMaskColorButton(defaultToolBar));
            addButton(defaultToolBar, createSetRoiColorsButton(defaultToolBar));
            if (isUseSectorManagement()) {
                addButton(defaultToolBar, createSetSectorColorsButton(defaultToolBar));
            }
            defaultToolBar.addSeparator();
        } else if (isUseSectorManagement()) {
            addButton(defaultToolBar, createNewSectorButton(defaultToolBar));
            addButton(defaultToolBar, createEditSectorButton(defaultToolBar));
            addButton(defaultToolBar, createRemoveSectorButton(defaultToolBar));
            defaultToolBar.addSeparator();
            addButton(defaultToolBar, createSetSectorColorsButton(defaultToolBar));
            defaultToolBar.addSeparator();
        }
        addButton(defaultToolBar, createZoomButton(defaultToolBar));
        addButton(defaultToolBar, createPanButton(defaultToolBar));
        addButton(defaultToolBar, createZoomInButton(defaultToolBar));
        addButton(defaultToolBar, createZoomOutButton(defaultToolBar));
        defaultToolBar.addSeparator();
        addButton(defaultToolBar, createFocusButton(defaultToolBar));
        defaultToolBar.addSeparator();
        addButton(defaultToolBar, createUndoButton(defaultToolBar));
        addButton(defaultToolBar, createRedoButton(defaultToolBar));
        defaultToolBar.addSeparator();
        addButton(defaultToolBar, createSnapshotButton(defaultToolBar));
        defaultToolBar.addSeparator();
        addButton(defaultToolBar, createPrintButton(defaultToolBar));
        defaultToolBar.addSeparator();
        addButton(defaultToolBar, createShowTableButton(defaultToolBar));
        defaultToolBar.addSeparator();
        addButton(defaultToolBar, createLineProfileButton(defaultToolBar));

        return defaultToolBar;
    }

    protected void addButton(JToolBar toolbar, AbstractButton button) {
        if ((toolbar != null) && (button != null)) {
            toolbar.add(button);
        }
    }

    @Override
    public int getImageWidth() {
        return dimX;
    }

    @Override
    public int getImageHeight() {
        return dimY;
    }

    public JLabel createDefaultCoordsLabel() {
        CoordsLabel label = new CoordsLabel();
        addViewerListener(label);
        return label;
    }

    public JComponent createDefaultStatisticsComponent() {
        StatisticsPanel component = new StatisticsPanel(this);
        imagePlus.addRoiManagerListener(component.getRoiListener());
        imagePlus.addRoiSelectionListener(component.getRoiListener());
        return component;
    }

    public JLabel createDefaultZoomLabel() {
        return new JLabel(ZOOM_TITLE + UNKNOW_ZOOM + "%");
    }

    /**
     * Computes and returns the text for a zoom label
     * 
     * @return A {@link String}
     */
    protected String computeZoomLabelText() {
        StringBuilder builder = new StringBuilder(ZOOM_TITLE);
        if (getImageCanvas() == null) {
            builder.append(UNKNOW_ZOOM);
        } else {
            double magnification = getImageCanvas().getMagnification() * 100;
            String format = "%.3f";
            if (magnification == Math.floor(magnification)) {
                format = "%d";
            }
            // We use Locale.UK because it does note use "," as a 10^3 separator
            // (whereas Locale.US does use it)
            builder.append(Format.format(Locale.UK, format, Double.valueOf(magnification)));
            builder.append("%");
        }
        return builder.toString();
    }

    public int getSelectedX() {
        return selectedX;
    }

    public void setSelectedX(int selectedX) {
        this.selectedX = selectedX;
    }

    public int getSelectedY() {
        return selectedY;
    }

    public void setSelectedY(int selectedY) {
        this.selectedY = selectedY;
    }

    public double getSelectedAxisX() {
        return selectedAxisX;
    }

    public void setSelectedAxisX(double selectedAxisX) {
        this.selectedAxisX = selectedAxisX;
    }

    public double getSelectedAxisY() {
        return selectedAxisY;
    }

    public void setSelectedAxisY(double selectedAxisY) {
        this.selectedAxisY = selectedAxisY;
    }

    public double getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(double selectedValue) {
        this.selectedValue = selectedValue;
    }

    protected void fireMaskChanged() {
        synchronized (maskListeners) {
            for (int i = 0; i < maskListeners.size(); i++) {
                WeakReference<MaskListener> soft = maskListeners.get(i);
                MaskListener listener = soft.get();
                if (listener != null) {
                    listener.maskChanged(new MaskEvent(this, getMask()));
                }
            }
        }
    }

    @Override
    public void addMaskListener(MaskListener listener) {
        synchronized (maskListeners) {
            boolean canAdd = true;
            if (listener == null) {
                canAdd = false;
            } else {
                for (WeakReference<MaskListener> ref : maskListeners) {
                    MaskListener maskListener = ObjectUtils.recoverObject(ref);
                    if ((maskListener != null) && maskListener.equals(listener)) {
                        canAdd = false;
                        break;
                    }
                }
            }
            if (canAdd) {
                maskListeners.add(new WeakReference<MaskListener>(listener));
            }
        }
    }

    @Override
    public void removeAllMaskListeners() {
        synchronized (maskListeners) {
            for (int i = 0; i < maskListeners.size(); i++) {
                WeakReference<MaskListener> soft = maskListeners.get(i);
                soft.clear();
            }
            maskListeners.clear();
        }
    }

    @Override
    public void removeMaskListener(MaskListener listener) {
        synchronized (maskListeners) {
            int indexToRemove = -1;
            if (listener != null) {
                for (int i = 0; i < maskListeners.size(); i++) {
                    WeakReference<MaskListener> soft = maskListeners.get(i);
                    MaskListener maskListener = soft.get();
                    if (maskListener != null && maskListener.equals(listener)) {
                        indexToRemove = i;
                        soft.clear();
                        break;
                    }
                }
            }
            if (indexToRemove != -1) {
                maskListeners.remove(indexToRemove);
            }
        }
    }

    public void setData(AbstractNumberMatrix<?> matrix) {
        if (matrix == null) {
            setFlatNumberMatrix(null, 0, 0);
        } else {
            setFlatNumberMatrix(matrix.getFlatValue(), matrix.getWidth(), matrix.getHeight());
        }
    }

    public void setData(FlatMatrix<?> flat) {
        if (flat == null || flat.getValue() == null || !flat.getValue().getClass().isArray()) {
            setFlatNumberMatrix(null, 0, 0);
        } else {
            int width = flat.getColumns();
            int height = 0;
            if (width > 0) {
                height = Array.getLength(flat.getValue()) / width;
            }
            setFlatNumberMatrix(flat.getValue(), width, height);
        }
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public int getDimX() {
        return dimX;
    }

    @Override
    public int getDimY() {
        return dimY;
    }

    protected void updateImageCanvasParameters() {
        getImageCanvas().setRoiColor(roiColor);
        getImageCanvas().setSelectedRoiColor(selectedRoiColor);
        getImageCanvas().setHoveredRoiColor(hoveredRoiColor);
        getImageCanvas().setInnerRoiColor(innerRoiColor);
        getImageCanvas().setSelectedInnerRoiColor(selectedInnerRoiColor);
        getImageCanvas().setHoveredInnerRoiColor(hoveredInnerRoiColor);
        getImageCanvas().setOuterRoiColor(outerRoiColor);
        getImageCanvas().setSelectedOuterRoiColor(selectedOuterRoiColor);
        getImageCanvas().setHoveredOuterRoiColor(hoveredOuterRoiColor);
        getImageCanvas().setBeamColor(beamColor);
        getImageCanvas().setPaintAllowed(isPaintAllowed());
    }

    @Override
    public boolean hasFocus() {
        return false;
    }

    @Override
    public void selectedColumnChanged(int col) {
    }

    @Override
    public void selectedPointChanged(double[] point) {
    }

    @Override
    public void selectedRowChanged(int col) {
    }

    @Override
    public void valueChanged(Object matrix) {
        if (matrix instanceof Number[] || matrix instanceof byte[] || matrix instanceof short[]
                || matrix instanceof int[] || matrix instanceof long[] || matrix instanceof float[]
                || matrix instanceof double[]) {
            if (!ObjectUtils.sameObject(value, matrix)) {
                fireValueChanged();
            }
        }
    }

    protected void fireValueChanged() {
        IJViewerEvent event = new IJViewerEvent(this);
        IIJViewerListener[] listeners = listenerList.getListeners(IIJViewerListener.class);
        for (IIJViewerListener listener : listeners) {
            listener.valueChanged(event);
        }
        setFlatNumberMatrix(getNewValue(), getNewValueWidth(), getNewValueHeight());
        FlatMatrix<Object> newValue = new FlatMatrix<Object>();
        newValue.setValue(getNewValueWidth(), getNewValue());
        ImageViewerEvent imageViewerEvent = new ImageViewerEvent(this, null, newValue, Reason.VALUE);
        for (IImageViewerListener listener : this.listeners) {
            if (listener != null) {
                listener.imageViewerChanged(imageViewerEvent);
            }
        }
        warnMediators(
                new MatrixInformation(this, (Object[]) ArrayUtils.convertArrayDimensionFrom1ToN(value, dimY, dimX)));
    }

    public Object getNewValue() {
        return matrixTable.getData();
    }

    public int getNewValueWidth() {
        return matrixTable.getMatrixDataWidth(ArrayUtils.recoverDataType(matrixTable.getData()));
    }

    public int getNewValueHeight() {
        return matrixTable.getMatrixDataHeight(ArrayUtils.recoverDataType(matrixTable.getData()));
    }

    /**
     * Enables or disables the auto zoom. When enabled, the image size (zoom) is automatically
     * adjusted according to the component size. The calcul of the size is triggered by a call to
     * setData() or imageChange().
     * 
     * @param auto AutoZoom flag
     */
    public void setAutoZoom(boolean auto) {
        autoZoom = auto;
        computeAutoZoom();
    }

    /**
     * Determines whether this image viewer has auto zoom enabled.
     * 
     * @return A <code>boolean</code> value
     * 
     * @see #setAutoZoom
     */
    public boolean isAutoZoom() {
        return autoZoom;
    }

    public void unZoom() {
        unzoomImageCanvas();
        computeAutoZoom();
        revalidate();
        differedUpdateRefSize();
    }

    /**
     * Calculates the best zoom if in auto zoom mode
     */
    public void computeAutoZoom() {
        if (autoZoom && getImageCanvas() != null && getImageCanvas().getImagePlus() != null
                && getImageCanvas().getImagePlus().getWindow() != null) {
            // workaround to avoid zoom problems when drawn picture is a sub
            // part of real picture
            unzoomImageCanvas();
            // end workaround

            if (converter != null) {
                int width = 0;
                int height = 0;
                Rectangle rect = getVisibleRect();
                if (rect != null) {
                    width = rect.width - xOrigine;
                    height = rect.height - yOrigine;
                }
                if (width <= 1) {
                    // default value
                    width = 600;
                }
                if (height <= 1) {
                    // default value
                    height = 400;
                }
                rect = null;
                if (converter != null) {
                    // min and max zoom values are taken from ImageCanvas
                    double zoom = converter.getBestZoom(dimX, dimY, width, height, 0.03125, 32.0);
                    getImageCanvas().setMagnification(zoom);
                    fireMouseChanged();
                    revalidate();
                }
            }
        }
    }

    /**
     * Calls a {@link JFileChooser} to select and load a {@link Mask}
     * 
     * @param addToCurrentMask Whether to add loaded mask to current mask
     */
    protected void loadMask(boolean addToCurrentMask) {
        String path = maskController.launchRestoreBrowser(getMaskFileChooser());
        loadMask(path, addToCurrentMask);
    }

    @Override
    public void loadMask(String path, boolean addToCurrentMask) {
        if (path != null) {
            Mask mask = maskController.loadMaskFile(path);
            if (mask != null) {
                String textSet = getAction(LOAD_MASK_ACTION).getShortDescription();
                if (textSet == null || textSet.trim().isEmpty()) {
                    textSet = getAction(LOAD_MASK_ACTION).getName();
                }
                if (textSet == null || textSet.trim().isEmpty()) {
                    textSet = "load Mask";
                }
                Mask formerMask = getMask();
                if (addToCurrentMask && (formerMask != null)
                        && (formerMask.getValue().length == mask.getValue().length)) {
                    boolean[] newMask = new boolean[mask.getValue().length];
                    for (int i = 0; i < newMask.length; i++) {
                        newMask[i] = formerMask.getValue()[i] && mask.getValue()[i];
                    }
                    try {
                        mask = new Mask(newMask, mask.getWidth(), mask.getHeight(), mask.getName());
                    } catch (MaskException e) {
                        // Should not happen;
                    }
                }
                setMask(mask, textSet);
                firePropertyChange(addToCurrentMask ? MASK_LOADED_ADD_PROPERTY : MASK_LOADED_PROPERTY, formerMask,
                        path);
            }
        }
    }

    @Override
    public void saveMask(String path) {
        if (path != null && !path.trim().isEmpty() && getMaskController() != null) {
            getMaskController().saveMask(getMask(), path);
        }
    }

    protected void saveMask() {
        if (getMaskController() != null) {
            getMaskController().saveMask(this, getMaskFileChooser());
        }
    }

    /**
     * Calls a {@link ColorPicker} to choose and set the {@link Color} used to draw {@link Mask}s
     */
    protected void changeMaskColor() {
        setMaskColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this), getChooseMaskColorTitle(),
                getMaskColor(), true));
    }

    /**
     * Calls a {@link ColorPicker} to choose and set the {@link Color} used to draw {@link BeamPosition}s
     */
    protected void changeBeamPositionColor() {
        setBeamColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this), getChooseBeamPositionColorTitle(),
                getBeamColor(), true));
    }

    /**
     * Returns the text used as title of the Dialog to choose a {@link Mask} {@link Color}
     * 
     * @return a {@link String}
     */
    public String getChooseMaskColorTitle() {
        return chooseMaskColorTitle;
    }

    /**
     * Sets the text used as title of the Dialog to choose a {@link Mask} {@link Color}
     * 
     * @param chooseMaskColorTitle the text to use. If <code>null</code> or empty, a default value
     *            is used.
     */
    public void setChooseMaskColorTitle(String chooseMaskColorTitle) {
        if ((chooseMaskColorTitle == null) || chooseMaskColorTitle.trim().isEmpty()) {
            this.chooseMaskColorTitle = DEFAULT_CHOOSE_MASK_COLOR_TITLE;
        } else {
            this.chooseMaskColorTitle = chooseMaskColorTitle;
        }
    }

    /**
     * Returns the text used as title of the Dialog to choose a beam center {@link Color}
     * 
     * @return a {@link String}
     */
    public String getChooseBeamPositionColorTitle() {
        return chooseBeamPositionColorTitle;
    }

    /**
     * Sets the text used as title of the Dialog to choose a beam center {@link Color}
     * 
     * @param chooseBeamPositionColorTitle the text to use. If <code>null</code> or empty, a default
     *            value is used.
     */
    public void setChooseBeamPositionColorTitle(String chooseBeamPositionColorTitle) {
        if ((chooseBeamPositionColorTitle == null) || chooseBeamPositionColorTitle.trim().isEmpty()) {
            this.chooseBeamPositionColorTitle = DEFAULT_CHOOSE_BEAM_COLOR_TITLE;
        } else {
            this.chooseBeamPositionColorTitle = chooseBeamPositionColorTitle;
        }
    }

    /**
     * Returns the text used as title of the Dialog to choose a {@link Roi} {@link Color}
     * 
     * @return a {@link String}
     */
    public String getChooseRoiColorTitle() {
        return chooseRoiColorTitle;
    }

    /**
     * Sets the text used as title of the Dialog to choose a {@link Roi} {@link Color}
     * 
     * @param chooseRoiColorTitle the text to use. If <code>null</code> or empty, a default value is
     *            used.
     */
    public void setChooseRoiColorTitle(String chooseRoiColorTitle) {
        if (chooseRoiColorTitle == null || chooseRoiColorTitle.trim().isEmpty()) {
            this.chooseRoiColorTitle = DEFAULT_CHOOSE_ROI_COLOR_TITLE;
        } else {
            this.chooseRoiColorTitle = chooseRoiColorTitle;
        }
    }

    /**
     * Returns the text used as title of the Dialog to choose a {@link Roi} Selection {@link Color}
     * 
     * @return a {@link String}
     */
    public String getChooseSelectedRoiColorTitle() {
        return chooseSelectedRoiColorTitle;
    }

    /**
     * Sets the text used as title of the Dialog to choose a {@link Roi} Selection {@link Color}
     * 
     * @param chooseSelectedRoiColorTitle the text to use. If <code>null</code> or empty, a default
     *            value is used.
     */
    public void setChooseSelectedRoiColorTitle(String chooseSelectedRoiColorTitle) {
        if (chooseSelectedRoiColorTitle == null || chooseSelectedRoiColorTitle.trim().isEmpty()) {
            this.chooseSelectedRoiColorTitle = DEFAULT_CHOOSE_SELECTED_ROI_COLOR_TITLE;
        } else {
            this.chooseSelectedRoiColorTitle = chooseSelectedRoiColorTitle;
        }
    }

    /**
     * Returns the text used as title of the Dialog to choose a {@link Roi} Hovered {@link Color}
     * 
     * @return a {@link String}
     */
    public String getChooseHoveredRoiColorTitle() {
        return chooseHoveredRoiColorTitle;
    }

    /**
     * Sets the text used as title of the Dialog to choose a {@link Roi} Hovered {@link Color}
     * 
     * @param chooseHoveredRoiColorTitle the text to use. If <code>null</code> or empty, a default
     *            value is used.
     */
    public void setChooseHoveredRoiColorTitle(String chooseHoveredRoiColorTitle) {
        if (chooseHoveredRoiColorTitle == null || chooseHoveredRoiColorTitle.trim().isEmpty()) {
            this.chooseHoveredRoiColorTitle = DEFAULT_CHOOSE_HOVERED_ROI_COLOR_TITLE;
        } else {
            this.chooseHoveredRoiColorTitle = chooseHoveredRoiColorTitle;
        }
    }

    /**
     * Returns the text used as title of the Dialog to choose an Inner {@link Roi} {@link Color}
     * 
     * @return a {@link String}
     */
    public String getChooseInnerRoiColorTitle() {
        return chooseInnerRoiColorTitle;
    }

    /**
     * Sets the text used as title of the Dialog to choose an Inner {@link Roi} {@link Color}
     * 
     * @param chooseInnerRoiColorTitle the text to use. If <code>null</code> or empty, a default
     *            value is used.
     */
    public void setChooseInnerRoiColorTitle(String chooseInnerRoiColorTitle) {
        if (chooseInnerRoiColorTitle == null || chooseInnerRoiColorTitle.trim().isEmpty()) {
            chooseInnerRoiColorTitle = DEFAULT_CHOOSE_INNER_ROI_COLOR_TITLE;
        } else {
            this.chooseInnerRoiColorTitle = chooseInnerRoiColorTitle;
        }
    }

    /**
     * Returns the text used as title of the Dialog to choose an Inner {@link Roi} Selection {@link Color}
     * 
     * @return a {@link String}
     */
    public String getChooseSelectedInnerRoiColorTitle() {
        return chooseSelectedInnerRoiColorTitle;
    }

    /**
     * Sets the text used as title of the Dialog to choose an Inner {@link Roi} Selection {@link Color}
     * 
     * @param chooseSelectedInnerRoiColorTitle the text to use. If <code>null</code> or empty, a
     *            default value is used.
     */
    public void setChooseSelectedInnerRoiColorTitle(String chooseSelectedInnerRoiColorTitle) {
        if (chooseSelectedInnerRoiColorTitle == null || chooseSelectedInnerRoiColorTitle.trim().isEmpty()) {
            chooseSelectedInnerRoiColorTitle = DEFAULT_CHOOSE_SELECTED_INNER_ROI_COLOR_TITLE;
        }
        this.chooseSelectedInnerRoiColorTitle = chooseSelectedInnerRoiColorTitle;
    }

    /**
     * Returns the text used as title of the Dialog to choose an Inner {@link Roi} Hovered {@link Color}
     * 
     * @return a {@link String}
     */
    public String getChooseHoveredInnerRoiColorTitle() {
        return chooseHoveredInnerRoiColorTitle;
    }

    /**
     * Sets the text used as title of the Dialog to choose an Inner {@link Roi} Hovered {@link Color}
     * 
     * @param chooseHoveredInnerRoiColorTitle the text to use. If <code>null</code> or empty, a
     *            default value is used.
     */
    public void setChooseHoveredInnerRoiColorTitle(String chooseHoveredInnerRoiColorTitle) {
        if (chooseHoveredInnerRoiColorTitle == null || chooseHoveredInnerRoiColorTitle.trim().isEmpty()) {
            chooseHoveredInnerRoiColorTitle = DEFAULT_CHOOSE_HOVERED_INNER_ROI_COLOR_TITLE;
        } else {
            this.chooseHoveredInnerRoiColorTitle = chooseHoveredInnerRoiColorTitle;
        }
    }

    /**
     * Returns the text used as title of the Dialog to choose an Outer {@link Roi} {@link Color}
     * 
     * @return a {@link String}
     */
    public String getChooseOuterRoiColorTitle() {
        return chooseOuterRoiColorTitle;
    }

    /**
     * Sets the text used as title of the Dialog to choose an Outer {@link Roi} {@link Color}
     * 
     * @param chooseOuterRoiColorTitle the text to use. If <code>null</code> or empty, a default
     *            value is used.
     */
    public void setChooseOuterRoiColorTitle(String chooseOuterRoiColorTitle) {
        if (chooseOuterRoiColorTitle == null || chooseOuterRoiColorTitle.trim().isEmpty()) {
            chooseOuterRoiColorTitle = DEFAULT_CHOOSE_OUTER_ROI_COLOR_TITLE;
        } else {
            this.chooseOuterRoiColorTitle = chooseOuterRoiColorTitle;
        }
    }

    /**
     * Returns the text used as title of the Dialog to choose an Outer {@link Roi} Selection {@link Color}
     * 
     * @return a {@link String}
     */
    public String getChooseSelectedOuterRoiColorTitle() {
        return chooseSelectedOuterRoiColorTitle;
    }

    /**
     * Sets the text used as title of the Dialog to choose an Outer {@link Roi} Selection {@link Color}
     * 
     * @param chooseSelectedOuterRoiColorTitle the text to use. If <code>null</code> or empty, a
     *            default value is used.
     */
    public void setChooseSelectedOuterRoiColorTitle(String chooseSelectedOuterRoiColorTitle) {
        if (chooseSelectedOuterRoiColorTitle == null || chooseSelectedOuterRoiColorTitle.trim().isEmpty()) {
            chooseSelectedOuterRoiColorTitle = DEFAULT_CHOOSE_SELECTED_OUTER_ROI_COLOR_TITLE;
        }
        this.chooseSelectedOuterRoiColorTitle = chooseSelectedOuterRoiColorTitle;
    }

    /**
     * Returns the text used as title of the Dialog to choose an Outer {@link Roi} Hovered {@link Color}
     * 
     * @return a {@link String}
     */
    public String getChooseHoveredOuterRoiColorTitle() {
        return chooseHoveredOuterRoiColorTitle;
    }

    /**
     * Sets the text used as title of the Dialog to choose an Outer {@link Roi} Hovered {@link Color}
     * 
     * @param chooseHoveredOuterRoiColorTitle the text to use. If <code>null</code> or empty, a
     *            default value is used.
     */
    public void setChooseHoveredOuterRoiColorTitle(String chooseHoveredOuterRoiColorTitle) {
        if (chooseHoveredOuterRoiColorTitle == null || chooseHoveredOuterRoiColorTitle.trim().isEmpty()) {
            chooseHoveredOuterRoiColorTitle = DEFAULT_CHOOSE_HOVERED_OUTER_ROI_COLOR_TITLE;
        } else {
            this.chooseHoveredOuterRoiColorTitle = chooseHoveredOuterRoiColorTitle;
        }
    }

    public String getFitBoundsErrorTitle() {
        return fitBoundsErrorTitle;
    }

    public void setFitBoundsErrorTitle(String fitBoundsErrorTitle) {
        if (fitBoundsErrorTitle == null || fitBoundsErrorTitle.trim().isEmpty()) {
            fitBoundsErrorTitle = DEFAULT_FIT_BOUNDS_ERROR_TITLE;
        } else {
            this.fitBoundsErrorTitle = fitBoundsErrorTitle;
        }
    }

    public String getFitBoundsErrorText() {
        return fitBoundsErrorText;
    }

    public void setFitBoundsErrorText(String fitBoundsErrorText) {
        if (fitBoundsErrorText == null || fitBoundsErrorText.trim().isEmpty()) {
            fitBoundsErrorText = DEFAULT_FIT_BOUNDS_ERROR_TEXT;
        } else {
            this.fitBoundsErrorText = fitBoundsErrorText;
        }
    }

    /**
     * Calls a {@link ColorPicker} to change the {@link Roi} {@link Color}
     */
    protected void changeRoiColor() {
        if (roiColorsPopupMenu != null) {
            roiColorsPopupMenu.setVisible(false);
        }
        setRoiColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this), getChooseRoiColorTitle(), roiColor,
                true));
    }

    /**
     * Calls a {@link ColorPicker} to change the {@link Roi} Selection {@link Color}
     */
    protected void changeSelectedRoiColor() {
        if (roiColorsPopupMenu != null) {
            roiColorsPopupMenu.setVisible(false);
        }
        setSelectedRoiColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this),
                getChooseSelectedRoiColorTitle(), selectedRoiColor, true));
    }

    /**
     * Calls a {@link ColorPicker} to change the {@link Roi} Hovered {@link Color}
     */
    protected void changeHoveredRoiColor() {
        if (roiColorsPopupMenu != null) {
            roiColorsPopupMenu.setVisible(false);
        }
        setHoveredRoiColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this),
                getChooseHoveredRoiColorTitle(), hoveredRoiColor, true));
    }

    /**
     * Calls a {@link ColorPicker} to change the Inner {@link Roi} {@link Color}
     */
    protected void changeInnerRoiColor() {
        if (roiColorsPopupMenu != null) {
            roiColorsPopupMenu.setVisible(false);
        }
        setInnerRoiColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this), getChooseInnerRoiColorTitle(),
                innerRoiColor, true));
    }

    /**
     * Calls a {@link ColorPicker} to change the Inner {@link Roi} Selection {@link Color}
     */
    protected void changeSelectedInnerRoiColor() {
        if (roiColorsPopupMenu != null) {
            roiColorsPopupMenu.setVisible(false);
        }
        setSelectedInnerRoiColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this),
                getChooseSelectedInnerRoiColorTitle(), selectedInnerRoiColor, true));
    }

    /**
     * Calls a {@link ColorPicker} to change the Inner {@link Roi} Hovered {@link Color}
     */
    protected void changeHoveredInnerRoiColor() {
        if (roiColorsPopupMenu != null) {
            roiColorsPopupMenu.setVisible(false);
        }
        setHoveredInnerRoiColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this),
                getChooseHoveredInnerRoiColorTitle(), hoveredInnerRoiColor, true));
    }

    /**
     * Calls a {@link ColorPicker} to change the Outer {@link Roi} {@link Color}
     */
    protected void changeOuterRoiColor() {
        if (roiColorsPopupMenu != null) {
            roiColorsPopupMenu.setVisible(false);
        }
        setOuterRoiColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this), getChooseOuterRoiColorTitle(),
                outerRoiColor, true));
    }

    /**
     * Calls a {@link ColorPicker} to change the Outer {@link Roi} Selection {@link Color}
     */
    protected void changeSelectedOuterRoiColor() {
        if (roiColorsPopupMenu != null) {
            roiColorsPopupMenu.setVisible(false);
        }
        setSelectedOuterRoiColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this),
                getChooseSelectedOuterRoiColorTitle(), selectedOuterRoiColor, true));
    }

    /**
     * Calls a {@link ColorPicker} to change the Outer {@link Roi} Hovered {@link Color}
     */
    protected void changeHoveredOuterRoiColor() {
        if (roiColorsPopupMenu != null) {
            roiColorsPopupMenu.setVisible(false);
        }
        setHoveredOuterRoiColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this),
                getChooseHoveredOuterRoiColorTitle(), hoveredOuterRoiColor, true));
    }

    /**
     * Sets the {@link Roi} {@link Color}. Transmits this value to the {@link ImageCanvas}. If the {@link ImageCanvas}
     * changes, this value will be transmitted again.
     * 
     * @param roiColor the {@link Color} to set. If <code>null</code>, nothing is done.
     */
    public void setRoiColor(Color roiColor) {
        if (roiColor != null) {
            this.roiColor = roiColor;
            getImageCanvas().setRoiColor(roiColor);
            if (getImageCanvas().isValid()) {
                getImageCanvas().repaint();
            }
        }
    }

    /**
     * Sets the {@link Roi} selection {@link Color}. Transmits this value to the {@link ImageCanvas} . If the
     * {@link ImageCanvas} changes, this value will be transmitted again.
     * 
     * @param roiColor the {@link Color} to set. If <code>null</code>, nothing is done.
     */
    public void setSelectedRoiColor(Color roiColor) {
        if (roiColor != null) {
            selectedRoiColor = roiColor;
            getImageCanvas().setSelectedRoiColor(roiColor);
            if (getImageCanvas().isValid()) {
                getImageCanvas().repaint();
            }
        }
    }

    /**
     * Sets the {@link Roi} hovered {@link Color}. Transmits this value to the {@link ImageCanvas}.
     * If the {@link ImageCanvas} changes, this value will be transmitted again.
     * 
     * @param roiColor the {@link Color} to set. If <code>null</code>, nothing is done.
     */
    public void setHoveredRoiColor(Color roiColor) {
        if (roiColor != null) {
            hoveredRoiColor = roiColor;
            getImageCanvas().setHoveredRoiColor(roiColor);
            if (getImageCanvas().isValid()) {
                getImageCanvas().repaint();
            }
        }
    }

    /**
     * Sets the inner {@link Roi} {@link Color}. Transmits this value to the {@link ImageCanvas}. If
     * the {@link ImageCanvas} changes, this value will be transmitted again.
     * 
     * @param roiColor the {@link Color} to set. If <code>null</code>, nothing is done.
     */
    public void setInnerRoiColor(Color roiColor) {
        if (roiColor != null) {
            innerRoiColor = roiColor;
            getImageCanvas().setInnerRoiColor(roiColor);
            if (getImageCanvas().isValid()) {
                getImageCanvas().repaint();
            }
        }
    }

    /**
     * Sets the inner {@link Roi} selection {@link Color}. Transmits this value to the {@link ImageCanvas}. If the
     * {@link ImageCanvas} changes, this value will be transmitted
     * again.
     * 
     * @param roiColor the {@link Color} to set. If <code>null</code>, nothing is done.
     */
    public void setSelectedInnerRoiColor(Color roiColor) {
        if (roiColor != null) {
            selectedInnerRoiColor = roiColor;
            getImageCanvas().setSelectedInnerRoiColor(roiColor);
            if (getImageCanvas().isValid()) {
                getImageCanvas().repaint();
            }
        }
    }

    /**
     * Sets the inner {@link Roi} hovered {@link Color}. Transmits this value to the {@link ImageCanvas}. If the
     * {@link ImageCanvas} changes, this value will be transmitted
     * again.
     * 
     * @param roiColor the {@link Color} to set. If <code>null</code>, nothing is done.
     */
    public void setHoveredInnerRoiColor(Color roiColor) {
        if (roiColor != null) {
            hoveredInnerRoiColor = roiColor;
            getImageCanvas().setHoveredInnerRoiColor(roiColor);
            if (getImageCanvas().isValid()) {
                getImageCanvas().repaint();
            }
        }
    }

    /**
     * Sets the outer {@link Roi} {@link Color}. Transmits this value to the {@link ImageCanvas}. If
     * the {@link ImageCanvas} changes, this value will be transmitted again.
     * 
     * @param roiColor the {@link Color} to set. If <code>null</code>, nothing is done.
     */
    public void setOuterRoiColor(Color roiColor) {
        if (roiColor != null) {
            outerRoiColor = roiColor;
            getImageCanvas().setOuterRoiColor(roiColor);
            if (getImageCanvas().isValid()) {
                getImageCanvas().repaint();
            }
        }
    }

    /**
     * Sets the outer {@link Roi} selection {@link Color}. Transmits this value to the {@link ImageCanvas}. If the
     * {@link ImageCanvas} changes, this value will be transmitted
     * again.
     * 
     * @param roiColor the {@link Color} to set. If <code>null</code>, nothing is done.
     */
    public void setSelectedOuterRoiColor(Color roiColor) {
        if (roiColor != null) {
            selectedOuterRoiColor = roiColor;
            getImageCanvas().setSelectedOuterRoiColor(roiColor);
            if (getImageCanvas().isValid()) {
                getImageCanvas().repaint();
            }
        }
    }

    /**
     * Sets the outer {@link Roi} hovered {@link Color}. Transmits this value to the {@link ImageCanvas}. If the
     * {@link ImageCanvas} changes, this value will be transmitted
     * again.
     * 
     * @param roiColor the {@link Color} to set. If <code>null</code>, nothing is done.
     */
    public void setHoveredOuterRoiColor(Color roiColor) {
        if (hoveredOuterRoiColor != null) {
            hoveredOuterRoiColor = roiColor;
            getImageCanvas().setHoveredOuterRoiColor(roiColor);
            if (getImageCanvas().isValid()) {
                getImageCanvas().repaint();
            }
        }
    }

    /**
     * Sets the selected {@link Roi}s as Inner
     */
    protected void setInner() {
        List<Roi> selected = getRoiManager().getSelectedRois();
        if (selected != null) {
            for (int i = 0; i < selected.size(); i++) {
                getRoiManager().setInner(selected.get(i));
            }
        }
        selected = null;
        repaint();
    }

    /**
     * Sets the selected {@link Roi}s as Outer
     */
    protected void setOuter() {
        List<Roi> selected = getRoiManager().getSelectedRois();
        if (selected != null) {
            for (int i = 0; i < selected.size(); i++) {
                getRoiManager().setOuter(selected.get(i));
            }
        }
        selected = null;
        repaint();
    }

    /**
     * Invalidates selected {@link Roi}s
     */
    protected void invalidateRoi() {
        List<Roi> selected = getRoiManager().getSelectedRois();
        if (selected != null) {
            for (int i = 0; i < selected.size(); i++) {
                getRoiManager().invalidateRoi(selected.get(i));
            }
        }
        selected = null;
        repaint();
    }

    /**
     * Returns the {@link Color} used to draw {@link Mask}s
     * 
     * @return a {@link Color}
     */
    public Color getMaskColor() {
        return maskColor;
    }

    /**
     * Sets the {@link Color} used to draw {@link Mask}s.
     * 
     * @param maskColor the {@link Color} to set. If <code>null</code>, nothing is done
     */
    public void setMaskColor(Color maskColor) {
        if (maskColor != null) {
            this.maskColor = maskColor;
        }
        applyMaskAndSectorToImage();
    }

    /**
     * Calls a {@link ColorPicker} to change the {@link MathematicSector} outside preview {@link Color}
     */
    protected void changeEditingSectorColor() {
        if (sectorColorsPopupMenu != null) {
            sectorColorsPopupMenu.setVisible(false);
        }
        setSectorPreviewColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this),
                MESSAGES.getString("ImageJViewer.Action.SetSectorColor.Editing.Text"), sectorPreviewColor, true));
    }

    /**
     * Calls a {@link ColorPicker} to change the {@link MathematicSector} outside preview {@link Color}
     */
    protected void changeValidatedSectorColor() {
        if (sectorColorsPopupMenu != null) {
            sectorColorsPopupMenu.setVisible(false);
        }
        setSectorValidatedColor(ColorPicker.showDialog(CometeUtils.getWindowForComponent(this),
                MESSAGES.getString("ImageJViewer.Action.SetSectorColor.Valid.Text"), sectorValidatedColor, true));
    }

    public Color getSectorPreviewColor() {
        return sectorPreviewColor;
    }

    public void setSectorPreviewColor(Color sectorPreviewColor) {
        if (sectorPreviewColor != null) {
            this.sectorPreviewColor = sectorPreviewColor;
            applyMaskAndSectorToImage();
        }
    }

    public Color getSectorValidatedColor() {
        return sectorValidatedColor;
    }

    public void setSectorValidatedColor(Color sectorValidatedColor) {
        if (sectorValidatedColor != null) {
            this.sectorValidatedColor = sectorValidatedColor;
            applyMaskAndSectorToImage();
        }
    }

    public Color getBeamColor() {
        return beamColor;
    }

    public void setBeamColor(Color theBeamColor) {
        if (theBeamColor == null) {
            theBeamColor = beamColor;
        } else {
            beamColor = theBeamColor;
        }
        getImageCanvas().setBeamColor(theBeamColor);
    }

    @Override
    public Mask getMask() {
        return mask;
    }

    /**
     * Sets the mask of this viewer, without generating any {@link UndoableEdit}
     * 
     * @param mask the mask to set
     * @see #setMask(Mask, String)
     */
    @Override
    public boolean setMask(Mask mask) {
        return setMask(mask, null);
    }

    /**
     * Sets the mask of this viewer, and generates an {@link UndoableEdit} with a particular
     * presentation name.
     * 
     * @param mask the mask to set
     * @param undoRepresentation the presentation name. If <code>null</code>, no {@link UndoableEdit} is generated
     */
    public boolean setMask(Mask mask, String undoRepresentation) {
        boolean canSetMask = true;
        if (mask != this.mask) {
            if (mask != null) {
                if (mask.getHeight() == dimY) {
                    if (mask.getWidth() != dimX) {
                        canSetMask = false;
                    }
                } else {
                    canSetMask = false;
                }
            }
            if (canSetMask) {
                Mask undoMask = this.mask;
                this.mask = mask;
                applyMaskAndSectorToImage();
                if (isValid()) {
                    repaint();
                }
                if (undoRepresentation != null) {
                    MatrixMaskUndoableEdit edit = new MatrixMaskUndoableEdit(this, undoMask);
                    edit.setPresentationName(undoRepresentation);
                    undoManager.addEdit(edit);
                    updateUndoRedoActionState();
                }
                if (isManageActionsAvailability()) {
                    getAction(SAVE_MASK_ACTION).setEnabled(mask != null);
                }
                if (matrixTable != null) {
                    matrixTable.setMask(mask);
                }
                if (matrixDialog != null && matrixDialog.isVisible()) {
                    matrixDialog.repaint();
                }
            }
        }
        return canSetMask;
    }

    @Override
    public Mask getSectorMask() {
        return sectorMask;
    }

    @Override
    public MathematicSector getSector() {
        return sector;
    }

    @Override
    public void setSector(MathematicSector sector) {
        if (this.sector != sector) {
            if (this.sector != null) {
                this.sector.removeMathematicSectorListener(this);
            }
            this.sector = sector;
            boolean sectorNotNull = this.sector != null;
            if (sectorNotNull) {
                this.sector.addMathematicSectorListener(this);
            }
            if (isManageActionsAvailability()) {
                getAction(DELETE_SECTOR_ACTION).setEnabled(sectorNotNull);
                getAction(EDIT_SECTOR_ACTION).setEnabled(sectorNotNull);
            }
            updateSectorMask();
        }
    }

    public MaskManager getMaskManager() {
        return maskManager;
    }

    public final void setMaskManager(MaskManager maskManager) {
        this.maskManager = (maskManager == null ? MaskManager.DEFAULT_INSTANCE : maskManager);
    }

    protected boolean checkSectorMaskChanged() {
        boolean changed = true;
        Mask formerSectorMask = sectorMask;
        sectorMask = maskManager.updateSectorMask(sector, dimX, dimY, isPreviewSector());
        if (sectorMask == null) {
            changed = (formerSectorMask != null);
        }
        return changed;
    }

    protected void updateSectorMask() {
        if (checkSectorMaskChanged()) {
            applyMaskAndSectorToImage();
            repaint();
        }
    }

    public boolean isPreviewSector() {
        return previewSector;
    }

    public void setPreviewSector(boolean previewSector) {
        setPreviewSector(previewSector, true);
    }

    protected void setPreviewSector(boolean previewSector, boolean updateDrawing) {
        if (this.previewSector != previewSector) {
            this.previewSector = previewSector;
            getAction(PREVIEW_SECTOR_ACTION).setSelected(this.previewSector);
            if (updateDrawing) {
                updateSectorMask();
            }
        }
    }

    public void displaySectorSelectionDialog() {
        final JDialog sectorDialog = new JDialog(CometeUtils.getWindowForComponent(this),
                MESSAGES.getString("ImageJViewer.Action.NewSector.Text"));
        sectorDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        sectorDialog.setModal(true);
        final JList<Class<?>> sectorList;
        synchronized (possibleSectors) {
            sectorList = new JList<>(possibleSectors.toArray(new Class<?>[possibleSectors.size()]));
        }
        sectorList.setCellRenderer(new ClassListCellRenderer());
        JScrollPane listScrollPane = new JScrollPane(sectorList);
        final JPanel selector = new JPanel(new BorderLayout());
        JButton okButton = new JButton("ok");
        selector.add(listScrollPane, BorderLayout.CENTER);
        selector.add(okButton, BorderLayout.SOUTH);
        sectorDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                selector.removeAll();
                sectorList.clearSelection();
                sectorList.setListData(new Class<?>[] {});
            }
        });
        okButton.addActionListener((e) -> {
            Class<?> value = sectorList.getSelectedValue();
            selector.removeAll();
            sectorList.clearSelection();
            sectorList.setListData(new Class<?>[] {});
            sectorDialog.setVisible(false);
            if (value != null) {
                setPreviewSector(false, false);
                setSector(createSector(value));
                initSectorParameters(getSector());
                displayEditSectorDialog();
            }
            value = null;
        });
        sectorDialog.setContentPane(selector);
        sectorDialog.pack();
        sectorDialog.setLocationRelativeTo(imageScrollPane == null ? this : imageScrollPane);
        sectorDialog.setVisible(true);
    }

    public void displayEditSectorDialog() {
        if (getSector() != null) {
            MathematicSector formerSector = getSector().clone(true);
            final MathematicSector previousSector = formerSector;
            final JDialog editorDialog = new JDialog(CometeUtils.getWindowForComponent(this),
                    MESSAGES.getString("ImageJViewer.Action.EditSector.Text"));
            editorDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            editorDialog.setModal(true);
            editorDialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    MathematicSector sector = getSector();
                    if ((sector == null) || (sector.isConstructing())) {
                        setSector(previousSector);
                    }
                }
            });
            getSector().removeMathematicSectorListener(this);
            getSector().setEditingMode();
            applyMaskAndSectorToImage();
            repaint();
            final MathematicSectorEditor editor = new MathematicSectorEditor();
            editor.setSector(getSector());
            JPanel buttonPanel = new JPanel(new BorderLayout());
            AbstractButton previewButton = containerFactory.createButton(getAction(PREVIEW_SECTOR_ACTION), buttonPanel);
            JButton okButton = new JButton(MESSAGES.getString("ImageJViewer.Action.ApplySector.Text"));
            okButton.setIcon(APPLY_SECTOR_ICON);
            okButton.addActionListener((e) -> {
                getSector().removeMathematicSectorListener(ImageViewer.this);
                editor.setParameters();
                getSector().addMathematicSectorListener(ImageViewer.this);
                if (!getSector().isConstructing()) {
                    editorDialog.getContentPane().removeAll();
                    editorDialog.setVisible(false);
                    editorDialog.dispose();
                    if (sectorMask == null) {
                        updateSectorMask();
                    } else {
                        applyMaskAndSectorToImage();
                        repaint();
                    }
                    ImageViewer.this.firePropertyChange(VALID_SECTOR_CHANGED_PROPERTY, previousSector, getSector());
                }
            });
            buttonPanel.add(previewButton, BorderLayout.WEST);
            buttonPanel.add(okButton, BorderLayout.EAST);
            JPanel mainPanel = new JPanel(new BorderLayout());
            mainPanel.add(editor, BorderLayout.CENTER);
            mainPanel.add(buttonPanel, BorderLayout.SOUTH);
            editorDialog.setContentPane(mainPanel);
            editorDialog.pack();
            editorDialog.setLocationRelativeTo(imageScrollPane == null ? this : imageScrollPane);
            getSector().addMathematicSectorListener(this);
            Runnable displayDiag = () -> {
                editorDialog.setVisible(true);
            };
            try {
                SwingUtilities.invokeLater(displayDiag);
            } catch (Exception e) {
                // This should not happen.
                displayDiag.run();
            }
        }
    }

    /**
     * Override this method to customize your mathematicSectors
     * 
     * @param toInit The {@link MathematicSector} to customize
     */
    protected void initSectorParameters(MathematicSector toInit) {
        // Nothing to do by default
    }

    /**
     * Applies mask and sector to image
     */
    protected void applyMaskAndSectorToImage() {
        if (isPaintAllowed()) {
            IJRoiManager manager = getRoiManager();
            if ((manager != null) && (theImage != null) && (dimX > 0) && (dimY > 0)) {
                ColorProcessor processor = (ColorProcessor) manager.getProcessor();
                int[] pixels = buildMaskedPixels();
                if (processor != null) {
                    processor.setPixels(pixels);
                }
                manager.updateAndDraw();
                imagePanel.repaint();
            } // end if (manager != null && theImage != null && dimX > 0 && dimY // > 0)
        }
    }

    protected int[] buildMaskedPixels() {
        int dimX = this.dimX, dimY = this.dimY;
        Mask mask = this.mask;
        MathematicSector sector = this.sector;
        Mask sectorMask = this.sectorMask;
        BufferedImage theImage = this.theImage;
        int[] pixels;
        if ((dimX > 0) && (dimY > 0) && (theImage != null)) {
            pixels = new int[dimX * dimY];
            boolean maskIsNull = (mask == null);
            int maskColor = getMaskColor().getRGB();
            boolean blendMask = ColorUtils.getAlpha(maskColor) < 255;
            boolean sectorIsNull = (sector == null) || (sectorMask == null);
            int sectorColor = sectorIsNull || sector.isConstructing() ? getSectorPreviewColor().getRGB()
                    : getSectorValidatedColor().getRGB();
            boolean blendSector = ColorUtils.getAlpha(sectorColor) < 255;
            // Colors are blended to make believe the sector is over the mask, and the mask is over the image
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    int index = y * dimX + x;
                    int color = theImage.getRGB(x, y);
                    if (sectorIsNull) {
                        // no sector
                        if (!maskIsNull) {
                            // mask, but no sector
                            if (!mask.getValue()[index]) {
                                if (blendMask) {
                                    // Case where mask color and image color should be blended
                                    color = ColorUtils.blend(color, maskColor);
                                } else {
                                    color = maskColor;
                                }
                            }
                        }
                    } else {
                        if (maskIsNull) {
                            // sector, but no mask
                            if (!sectorMask.getValue()[index]) {
                                if (blendSector) {
                                    // Case where sector color and image color should be blended
                                    color = ColorUtils.blend(color, sectorColor);
                                } else {
                                    color = sectorColor;
                                }
                            }
                        } else {
                            // sector and mask
                            if (!mask.getValue()[index]) {
                                if (blendMask) {
                                    // Case where mask color and image color should be blended
                                    color = ColorUtils.blend(color, maskColor);
                                } else {
                                    color = maskColor;
                                }
                            }
                            if (!sectorMask.getValue()[index]) {
                                if (blendSector) {
                                    // Case where sector color, mask color and image color should be blended
                                    color = ColorUtils.blend(color, sectorColor);
                                } else {
                                    color = sectorColor;
                                }
                            }
                        }
                    }
                    pixels[index] = color;
                } // end for (int x = 0; x < dimX; x++)
            } // end for (int y = 0; y < dimY; y++)
        } else {
            pixels = new int[0];
        }
        return pixels;
    }

    /**
     * Sets {@link Roi}'s corresponding {@link Mask} as {@link Mask}
     */
    @Override
    public synchronized void setAsMask() {
        if (theImage != null) {
            Mask mask = maskManager.setAsMask(getRoiManager(), dimX, dimY, maskController.getNextMaskId());
            if (mask != null) {
                String textSet = getAction(SET_AS_MASK_ACTION).getShortDescription();
                if (textSet == null || textSet.trim().isEmpty()) {
                    textSet = getAction(SET_AS_MASK_ACTION).getName();
                }
                if (textSet == null || textSet.trim().isEmpty()) {
                    textSet = "set Mask";
                }
                setMask(mask, textSet);
                textSet = null;
                updateUndoRedoActionState();
            }
        }
    }

    /**
     * Adds {@link Roi}'s corresponding mask to existing mask. If there is no already existing {@link Mask}, applies
     * {@link Mask}
     * 
     * @see #setAsMask()
     */
    public synchronized void addToMask() {
        if (getMask() == null) {
            setAsMask();
        } else {
            if (theImage != null) {
                Mask mask = maskManager.addToMask(getRoiManager(), dimX, dimY, maskController.getNextMaskId(),
                        getMask().getValue());
                if (mask != null) {
                    String textSet = getAction(ADD_TO_MASK_ACTION).getShortDescription();
                    if (textSet == null || textSet.trim().isEmpty()) {
                        textSet = getAction(ADD_TO_MASK_ACTION).getName();
                    }
                    if (textSet == null || textSet.trim().isEmpty()) {
                        textSet = "add Mask";
                    }
                    setMask(mask, textSet);
                    textSet = null;
                }
            }
        }
    }

    /**
     * Removes {@link Roi}'s corresponding visible zone from existing {@link Mask}. Does nothing if
     * there is no already present mask
     */
    public synchronized void removeFromMask() {
        if (getMask() != null && theImage != null) {
            Mask mask = maskManager.removeFromMask(getRoiManager(), dimX, dimY, maskController.getNextMaskId(),
                    getMask().getValue());

            String textSet = getAction(REMOVE_FROM_MASK_ACTION).getShortDescription();
            if (textSet == null || textSet.trim().isEmpty()) {
                textSet = getAction(REMOVE_FROM_MASK_ACTION).getName();
            }
            if (textSet == null || textSet.trim().isEmpty()) {
                textSet = "remove from Mask";
            }
            setMask(mask, textSet);
            textSet = null;
        }
    }

    /**
     * Clears {@link Mask}, generating the corresponding {@link UndoableEdit}
     */
    public void clearMask() {
        if (getMask() != null) {
            String textSet = getAction(CLEAR_MASK_ACTION).getShortDescription();
            if (textSet == null || textSet.trim().isEmpty()) {
                textSet = getAction(CLEAR_MASK_ACTION).getName();
            }
            if (textSet == null || textSet.trim().isEmpty()) {
                textSet = "clear Mask";
            }
            setMask(null, textSet);
            textSet = null;
        }
    }

    public boolean isHidden(int x, int y) {
        boolean hidden;
        Mask mask = this.mask;
        boolean[] maskedZone = null;
        if (mask != null) {
            maskedZone = mask.getValue();
        }
        if (maskedZone == null) {
            hidden = false;
        } else if (y >= mask.getHeight()) {
            hidden = false;
        } else if (x > mask.getWidth()) {
            hidden = false;
        } else {
            hidden = !maskedZone[y * mask.getWidth() + x];
        }
        return hidden;
    }

    /**
     * Returns the {@link MaskController} used by this {@link ImageViewer}
     * 
     * @return a {@link MaskController}
     */
    @Override
    public MaskController getMaskController() {
        return maskController;
    }

    /**
     * Sets the {@link MaskController} this {@link ImageViewer} should use
     * 
     * @param maskController The {@link MaskController} to set
     */
    public void setMaskController(MaskController maskController) {
        if (maskController != null && maskController.equals(this.maskController)) {
            this.maskController = maskController;
        }
    }

    public JFileChooser getMaskFileChooser() {
        if (maskFileChooser == null) {
            maskFileChooser = new JFileChooser();
            getMaskController().addFileFilters(maskFileChooser);
        }
        return maskFileChooser;
    }

    @Override
    public boolean isUseMaskManagement() {
        return useMaskManagement;
    }

    @Override
    public void setUseMaskManagement(boolean useMask) {
        useMaskManagement = useMask;
        // adapt menus
        updateMenus();
    }

    @Override
    public boolean isUseSectorManagement() {
        return useSectorManagement;
    }

    @Override
    public void setUseSectorManagement(boolean useSectorManagement) {
        this.useSectorManagement = useSectorManagement;
        // adapt menus
        updateMenus();
    }

    @Override
    public boolean isAlwaysFitMaxSize() {
        return alwaysFitMaxSize;
    }

    @Override
    public void setAlwaysFitMaxSize(boolean alwaysFitMaxSize) {
        setAlwaysFitMaxSize(alwaysFitMaxSize, true);
    }

    // JAVAAPI-625: add the possibility not to notify zoom roi generators
    protected void setAlwaysFitMaxSize(boolean alwaysFitMaxSize, boolean notifyRoiGenerators) {
        this.alwaysFitMaxSize = alwaysFitMaxSize;
        getAction(FIT_MAX_SIZE_ACTION).setSelected(alwaysFitMaxSize);
        if (alwaysFitMaxSize) {
            fitMaxSize(notifyRoiGenerators);
        }
    }

    /**
     * Returns whether this image component will limit the drawing zone for its ROIs
     * 
     * @return a boolean value
     */
    public boolean isLimitDrawingZone() {
        return limitDrawingZone;
    }

    /**
     * Sets whether this image component should limit the drawing zone for its ROIs (i.e. whther
     * ROIs can go outside of the image and be displayed if so)
     * 
     * @param limitDrawingZone a boolean value
     */
    public void setLimitDrawingZone(boolean limitDrawingZone) {
        this.limitDrawingZone = limitDrawingZone;
    }

    /**
     * Forces this viewer to render image in maximum available size.
     */
    public void fitMaxSize() {
        fitMaxSize(true);
    }

    protected void fitMaxSize(boolean notifyRoiGenerators) {
        if (imageCanvas != null) {
            IJRoiManager imp = imageCanvas.getImagePlus();
            if (imp != null) {
                ImageWindow window = imp.getWindow();
                Dimension size = imageCanvas.getMaxDrawingSize();
                if ((window != null) && (size != null)) {
                    int x = imp.getWindow().getX();
                    int y = imp.getWindow().getY();
                    unzoomImageCanvas();
                    window.setLocationAndSize(x, y, size.width, size.height);
                    window.setSize(size.width, size.height);
                    window.setMaximumSize(size);
                    // hack to avoid undesired window reset, which caused Mantis #22388
                    imp.setWindow(window);
                    adjustToCanvas();
                    if (notifyRoiGenerators) {
                        updateZoomRoiGenerators();
                    }
                }
            }
        }
    }

    protected void differedUpdateRefSize() {
        SwingUtilities.invokeLater(() -> {
            doUpdateRefSize();
        });
    }

    protected void doUpdateRefSize() {
        if (imageScrollPane != null) {
            try {
                refSizeSema.acquire();
                int width = imageScrollPane.getWidth();
                width -= leftMargin + rightMargin + AbstractAxisView.DEFAULT_MARGIN;
                if (imageScrollPane.getRowHeader() != null) {
                    width -= imageScrollPane.getRowHeader().getWidth();
                }
                if (width < 0) {
                    width = 0;
                }
                int height = imageScrollPane.getHeight();
                height -= upMargin + downMargin + AbstractAxisView.DEFAULT_MARGIN;
                if (imageScrollPane.getColumnHeader() != null) {
                    height -= imageScrollPane.getColumnHeader().getHeight();
                }
                if (height < 0) {
                    height = 0;
                }
                refSize = new Dimension(width, height);
                refSizeSema.release();
            } catch (InterruptedException e1) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn("ImageViewer size reference update canceled", e1);
            }
            updateImageCanvasMaxDrawingSize();
            if (isAlwaysFitMaxSize()) {
                boolean fitMaxSize = true;
                IJRoiManager manager = getRoiManager();
                if (manager != null) {
                    ImageWindow window = manager.getWindow();
                    if (window instanceof HiddenImageWindow) {
                        // do not fit max size while user is scrolling image
                        fitMaxSize = !((HiddenImageWindow) window).isIgnoreResizeEvents();
                    }
                }
                if (fitMaxSize) {
                    fitMaxSize();
                }
            } else {
                imagePanel.revalidate();
                imagePanel.repaint();
            }
        }
    }

    // Hack to limit image canvas drawing size
    protected void updateImageCanvasMaxDrawingSize() {
        if ((imageCanvas != null) && (imageCanvas.getImagePlus() != null)
                && (imageCanvas.getImagePlus().getWindow() != null)) {
            try {
                refSizeSema.acquire();
                // test refSize to avoid a NullPointerException
                // retest previous test because of the wait for sema
                if ((refSize != null) && (imageCanvas != null) && (imageCanvas.getImagePlus() != null)
                        && (imageCanvas.getImagePlus().getWindow() != null)) {
                    imageCanvas.getImagePlus().getWindow().setPreferredSize(refSize);
                    imageCanvas.getImagePlus().getWindow().setSize(refSize);
                    imageCanvas.setMaxDrawingSize(refSize);
                }
                refSizeSema.release();
            } catch (InterruptedException e1) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn("ImageViewer max drawing size update canceled",
                        e1);
            }
        }
    }

    @Override
    public IValueConvertor getXAxisConvertor() {
        return xAxisConvertor;
    }

    @Override
    public void setXAxisConvertor(IValueConvertor xAxisConvertor) {
        setXAxisConvertor(xAxisConvertor, true);
    }

    public void setXAxisConvertor(IValueConvertor xAxisConvertor, boolean repaint) {
        if (!ObjectUtils.sameObject(xAxisConvertor, this.xAxisConvertor)) {
            if (this.xAxisConvertor != null) {
                this.xAxisConvertor.removeValueConvertorListener(this);
            }
            this.xAxisConvertor = xAxisConvertor;
            getImageScreenPositionCalculator().setXConvertor(xAxisConvertor);
            if (this.xAxisConvertor != null) {
                this.xAxisConvertor.addValueConvertorListener(this);
            }
            updateAxisRange(true, getImageScreenPositionCalculator());
            if (repaint && isShowing()) {
                repaint();
                if (isAlwaysFitMaxSize()) {
                    differedUpdateRefSize();
                }
            }
        }
    }

    @Override
    public IValueConvertor getYAxisConvertor() {
        return yAxisConvertor;
    }

    @Override
    public void setYAxisConvertor(IValueConvertor yAxisConvertor) {
        setYAxisConvertor(yAxisConvertor, true);
    }

    public void setYAxisConvertor(IValueConvertor yAxisConvertor, boolean repaint) {
        if (!ObjectUtils.sameObject(yAxisConvertor, this.yAxisConvertor)) {
            if (this.yAxisConvertor != null) {
                this.yAxisConvertor.removeValueConvertorListener(this);
            }
            this.yAxisConvertor = yAxisConvertor;
            getImageScreenPositionCalculator().setYConvertor(yAxisConvertor);
            if (this.yAxisConvertor != null) {
                this.yAxisConvertor.addValueConvertorListener(this);
            }
            updateAxisRange(false, getImageScreenPositionCalculator());
            if (repaint && isShowing()) {
                repaint();
                if (isAlwaysFitMaxSize()) {
                    differedUpdateRefSize();
                }
            }
        }
    }

    protected String[] generateRowNames() {
        String[] rowNames = new String[dimY];
        String format = getYAxisFormat();
        boolean useFormat = Format.isNumberFormatOK(format);
        for (int i = 0; i < rowNames.length; i++) {
            rowNames[i] = getDefaultNumberConversion(getImageScreenPositionCalculator().imagePixelToAxisYD(i), format,
                    useFormat);
        }
        return rowNames;
    }

    protected String[] generateColumnNames() {
        String[] colNames = new String[dimX];
        String format = getXAxisFormat();
        boolean useFormat = Format.isNumberFormatOK(format);
        for (int i = 0; i < colNames.length; i++) {
            colNames[i] = getDefaultNumberConversion(getImageScreenPositionCalculator().imagePixelToAxisXD(i), format,
                    useFormat);
        }
        return colNames;
    }

    protected String getDefaultNumberConversion(double d, String format, boolean useFormat) {
        String stringValue;
        if (useFormat) {
            stringValue = Format.formatValue(d, format);
        } else {
            stringValue = getDefaultNumberConversion(d);
        }
        return stringValue;
    }

    protected String getDefaultNumberConversion(double d) {
        if (d == Math.floor(d) && d <= Long.MAX_VALUE && d >= Long.MIN_VALUE) {
            return Long.toString((long) d);
        } else {
            return Double.toString(d);
        }
    }

    /**
     * Generates an axis format editor.
     * 
     * @param x Whether the axis is x one.
     * @param title The title to use. <code>null</code> to use default title.
     * @param titleFont The {@link Font} to use for title part. <code>null</code> to use default title font.
     * @param editorColumns The number of columns in editor.
     * @return A {@link JComponent}.
     */
    public JComponent generateAxisFormatEditor(boolean x, String title, Font titleFont, int editorColumns) {
        JPanel panel = new JPanel(new BorderLayout(1, 0));
        JLabel label = new JLabel(title == null
                ? String.format(MESSAGES.getString("ImageJViewer.Settings.Axis.Format"),
                        MESSAGES.getString(x ? "ImageJViewer.Settings.Axis.X" : "ImageJViewer.Settings.Axis.Z"))
                : title);
        if (titleFont != null) {
            label.setFont(titleFont);
        }
        panel.add(label, BorderLayout.WEST);
        AxisFormatField field = new AxisFormatField(x, editorColumns);
        addPropertyChangeListener(field);
        panel.add(field, BorderLayout.CENTER);
        return panel;
    }

    public JPopupMenu getPopupMenu() {
        return popupMenu;
    }

    public JMenu getSelectionMenu() {
        return selectionMenu;
    }

    public JMenu getActionMenu() {
        return actionMenu;
    }

    public JMenu getColorMenu() {
        return colorMenu;
    }

    public void cleanUndoManager() {
        undoManager.discardAllEdits();
        updateUndoRedoActionState();
    }

    public boolean isPresetRoiName(String roiName) {
        return (roiName != null) && roiMap.containsKey(roiName);
    }

    public boolean isEditableRoiName(Roi roi) {
        return (roi != null) && (!getImageCanvas().getImagePlus().isSpecialRoi(roi))
                && (!isPresetRoiName(roi.getName()));
    }

    protected void addRoi(String name, RoiShape shape, int lineWidth, int x, int y, int height, int width, Color color,
            boolean centered) {
        IJRoiManager roiManager = imagePlus;
        if ((roiManager != null) && (name != null) && !name.isEmpty()) {
            Roi roi = roiMap.get(name);
            Roi horizontalRoi = null;
            Roi verticalRoi = null;
            String horizontalRoiName = name + "_H";
            String verticalRoiName = name + "_V";

            if (roi != null) {
                roiManager.deleteRoi(roi);
                roiMap.remove(name);
                roi = null;
            }

            // In case of cross
            horizontalRoi = roiMap.get(horizontalRoiName);
            if (horizontalRoi != null) {
                roiManager.deleteRoi(horizontalRoi);
                roiMap.remove(horizontalRoiName);
                horizontalRoi = null;
            }

            // In case of cross
            verticalRoi = roiMap.get(verticalRoiName);
            if (verticalRoi != null) {
                roiManager.deleteRoi(verticalRoi);
                roiMap.remove(verticalRoiName);
                verticalRoi = null;
            }

            if (shape != null) {
                switch (shape) {
                    case RECTANGLE:
                        if (centered) {
                            roi = new Roi(x - width / 2, y - height / 2, width, height);
                        } else {
                            roi = new Roi(x, y, width, height);
                        }
                        break;
                    case LINE:
                        roi = new Line(x, y, x + width, y + height);
                        break;
                    case OVAL:
                        if (centered) {
                            roi = new OvalRoi(x - width / 2, y - height / 2, width, height);
                        } else {
                            roi = new OvalRoi(x, y, width, height);
                        }
                        break;
                    case CROSS:
                        // Creation of 2 lines

                        // Horizontal line of the cross
                        // Calculation of x origin
                        int origine = x - width / 2;
                        horizontalRoi = new Line(origine, y, origine + width, y);

                        // Vertical line of the cross
                        origine = y - height / 2;
                        verticalRoi = new Line(x, origine, x, origine + height);
                        break;
                    case TEXT:
                        roi = new TextRoi(x, y, roiManager);
                        break;
                    case POINT:
                        roi = new PointRoi(x, y, roiManager);
                        break;
                    case NONE:
                        // Nothing to do
                        break;
                }
            }
            setRoi(roi, name, name, color, lineWidth);
            setRoi(horizontalRoi, horizontalRoiName, name, color, lineWidth);
            setRoi(verticalRoi, verticalRoiName, name, color, lineWidth);
            repaint();
        }
    }

    protected void setRoi(Roi roi, String keyName, String name, Color color, int lineWidth) {
        IJRoiManager roiManager = imagePlus;
        if ((roiManager != null) && (roi != null) && (keyName != null) && (!keyName.isEmpty())) {
            roi.setName(name);
            roi.setStrokeWidth(lineWidth);
            roi.setStrokeColor(color);
            roiMap.put(keyName, roi);
            roiManager.addRoi(roi);
        }
    }

    @Override
    public void addRoi(IRoi roi) {
        if (roi != null) {
            addRoi(roi, false);
        }
    }

    @Override
    public void addRoi(IRoi roi, boolean centered) {
        if (roi != null) {
            addRoi(roi.getName(), roi.getShape(), roi.getLineWidth(), roi.getX(), roi.getY(), roi.getHeight(),
                    roi.getWidth(), ColorTool.getColor(roi.getCometeColor()), centered);
        }
    }

    @Override
    public boolean isSingleRoiMode() {
        return getRoiManager().isSingleRoiMode();
    }

    /**
     * Sets "single" or "multi" roi mode. In "multi" roi mode, you can create as many Rois as you
     * want. In "single" roi mode, creating a new Roi will delete previous ones.
     * 
     * @param singleRoiMode <code>true</code> to switch to "single" roi mode
     */
    @Override
    public void setSingleRoiMode(boolean singleRoiMode) {
        getRoiManager().setSingleRoiMode(singleRoiMode);
    }

    @Override
    public void sectorChanged(MathematicSectorEvent event) {
        if (event != null && event.getSource() == sector) {
            updateSectorMask();
        }
    }

    @Override
    public Object[] getNumberMatrix() {
        // ImageViewer does not return data informations
        return null;
    }

    /**
     * Transforms a matrix value into its flat value before setting data
     * 
     * @param value the matrix value
     */
    @Override
    public void setNumberMatrix(Object[] value) {
        Object flatValue = null;
        int width = 0, height = 0;
        if (value != null) {
            height = value.length;
            int[] shape = ArrayUtils.recoverShape(value);
            if (shape != null && shape.length == 2) {
                height = shape[0];
                width = shape[1];
                flatValue = ArrayUtils.convertArrayDimensionFromNTo1(value);
            }
        }
        setFlatNumberMatrix(flatValue, width, height);
    }

    @Override
    public Object getFlatNumberMatrix() {
        // ImageViewer does not return data information
        return null;
    }

    protected void clean() {
        clearMask();
        getRoiManager().clearHandledRoi();
        deleteAllRoisButSpecials(true);
        setSector(null);
        cleanUndoManager();
    }

    protected void updateRoisImagePlus() {
        if (!isCleanOnDataSetting()) {
            for (Roi roi : getRoiManager().getAllRois()) {
                roi.setImage(getRoiManager());
            }
        }
    }

    protected void updateMatrixTable() {
        if (matrixTable != null) {
            // No need to listen to table while updating it
            // ==> avoids image data writing concurrency (DATAREDUC-670)
            matrixTable.removeTableListener(ImageViewer.this);
            matrixTable.setBufferredWidth(dimX);
            matrixTable.setBufferredHeight(dimY);
            matrixTable.setData(value);
            // Once table is updated, we can listen to it again
            matrixTable.addTableListener(ImageViewer.this);
        }
    }

    protected void mayAutoResize() {
        if (shouldAutoResize) {
            shouldAutoResize = false;
            if (isAlwaysFitMaxSize()) {
                differedUpdateRefSize();
            }
        }
    }

    /**
     * Sets the data to be displayed an an image
     * 
     * @param value The data to be displayed as an image. <code>data</code> is the flat
     *            represenation of a matrix. This {@link Object} must be an array of a {@link Number} or
     *            primitive number type. This means, <code>data</code> must be one of these:
     *            <ul>
     *            <li><code>Number[]</code></li>
     *            <li><code>byte[]</code></li>
     *            <li><code>short[]</code></li>
     *            <li><code>int[]</code></li>
     *            <li><code>long[]</code></li>
     *            <li><code>float[]</code></li>
     *            <li><code>double[]</code></li>
     *            </ul>
     *            Otherwise, this method won't do anything.
     * @param width The width (i.e. how many columns) of the matrix
     * @param height The height (i.e. how many rows) of the matrix
     */
    @Override
    public void setFlatNumberMatrix(final Object value, final int width, final int height)
            throws ApplicationIdException {
        checkApplicationId();
        if (value == null || value instanceof Number[] || value instanceof byte[] || value instanceof short[]
                || value instanceof int[] || value instanceof long[] || value instanceof float[]
                || value instanceof double[]) {
            if (isCleanOnDataSetting()) {
                clean();
            }
            int formerWidth = dimX, formerHeight = dimY;
            if (value == null) {
                dimY = 0;
                dimX = 0;
            } else {
                dimY = height;
                dimX = width;
            }
            this.value = value;
            if (!shouldAutoResize) {
                shouldAutoResize = (formerWidth != dimX) || (formerHeight != dimY);
            }
            updateMatrixTable();
            // Even when not showing (JAVAAPI-621), a few things must be updated.
            if (!isShowing()) {
                updateRoisImagePlus();
                updateDataRoiGenerators();
            }
            dataComponentDelegate.setDataChanged(this, true);
        }
    }

    @Override
    public boolean[][] getBooleanMatrix() {
        // ImageViewer does not return data informations
        return null;
    }

    @Override
    public void setBooleanMatrix(boolean[][] value) {
        Object flatValue = null;
        int width = 0, height = 0;
        if (value != null) {
            height = value.length;
            int[] shape = ArrayUtils.recoverShape(value);
            if (shape != null && shape.length == 2) {
                height = shape[0];
                width = shape[1];
                flatValue = ArrayUtils.convertArrayDimensionFromNTo1(value);
            }
        }
        setFlatBooleanMatrix((boolean[]) flatValue, width, height);

    }

    @Override
    public boolean[] getFlatBooleanMatrix() {
        // ImageViewer does not return data informations
        return null;
    }

    @Override
    public void setFlatBooleanMatrix(boolean[] value, int width, int height) {
        byte[] byteValue;
        try {
            byteValue = arrayAdapter.revertAdapt(value);
        } catch (DataAdaptationException e) {
            byteValue = null;
        }
        setFlatNumberMatrix(byteValue, width, height);
    }

    @Override
    public String getText() {
        return getImageName();
    }

    @Override
    public void setText(String text) {
        setImageName(text);
    }

    protected boolean isAssignableClass(Class<?> concernedDataClass) {
        return Number.class.isAssignableFrom(concernedDataClass) || Byte.TYPE.equals(concernedDataClass)
                || Short.TYPE.equals(concernedDataClass) || Integer.TYPE.equals(concernedDataClass)
                || Long.TYPE.equals(concernedDataClass) || Float.TYPE.equals(concernedDataClass)
                || Double.TYPE.equals(concernedDataClass);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        boolean result = false;
        if (isAssignableClass(concernedDataClass)) {
            result = true;
        }
        return result;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int result = 0;
        if (isAssignableClass(concernedDataClass)) {
            result = getDimX();
        }
        return result;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        int result = 0;
        if (isAssignableClass(concernedDataClass)) {
            result = getDimY();
        }
        return result;
    }

    @Override
    public String getImageName() {
        return imageName;
    }

    @Override
    public void setImageName(String name) {
        final String imageName;
        if (name == null) {
            imageName = ObjectUtils.EMPTY_STRING;
        } else {
            imageName = name.trim();
        }
        if (imageName != this.imageName) {
            String old = this.imageName;
            this.imageName = imageName;
            getDrawingThreadManager().runInDrawingThread(() -> {
                displayImageName(imageName);
            });
            firePropertyChange(IMAGE_NAME, old, imageName);
        }
    }

    protected void displayImageName(String imageName) {
        nameLabel.setText(imageName);
        nameLabel.setToolTipText(imageName.isEmpty() ? null : imageName);
        nameLabel.revalidate();
    }

    @Override
    public void addImageViewerListener(IImageViewerListener listener) {
        synchronized (listeners) {
            if (!listeners.contains(listener)) {
                listeners.add(listener);
            }
        }
    }

    @Override
    public void removeImageViewerListener(IImageViewerListener listener) {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }

    @Override
    public boolean isShowRoiInformationTable() {
        return roiInfoTableScrollPane.isVisible();
    }

    @Override
    public synchronized void setShowRoiInformationTable(boolean showTable) {
        if (showTable != roiInfoTableScrollPane.isVisible()) {
            roiInfoTableScrollPane.setVisible(showTable);
            remove(imageView);
            if (southSplitPane != null) {
                remove(southSplitPane);
                southSplitPane.removeAll();
                southSplitPane = null;
            }
            if (showTable) {
                initRoiTableJSplitPane();
                roiInfoTableScrollPane.setVisible(showTable);
                add(southSplitPane, BorderLayout.CENTER);
            } else {
                add(imageView, BorderLayout.CENTER);
            }
        }
    }

    @Override
    public boolean isCleanOnDataSetting() {
        return cleanOnDataSetting;
    }

    @Override
    public void setCleanOnDataSetting(boolean cleanOnDataSetting) {
        this.cleanOnDataSetting = cleanOnDataSetting;
    }

    /**
     * This method is used to readapt vertical and horizontal profilers to image dimensions
     */
    public void profilesFollowImage() {
        Rectangle newSize = getImageCanvas().getCanvasRectangle();

        if (verticalProfiler != null) {
            verticalProfiler.setPreferredSize(new Dimension(currentProfilerSize,
                    newSize.height + yOrigine + verticalProfiler.getChartView().getXAxisThickness()));
            verticalProfiler.setMinimumSize(verticalProfiler.getPreferredSize());
            verticalProfiler.setSize(verticalProfiler.getPreferredSize());
            verticalProfiler.revalidate();
        }
        if (horizontalProfiler != null) {
            horizontalProfiler
                    .setPreferredSize(new Dimension(
                            newSize.width + xOrigine
                                    + Math.max(horizontalProfiler.getChartView().getY1AxisThickness(),
                                            horizontalProfiler.getChartView().getY2AxisThickness()),
                            currentProfilerSize));
            horizontalProfiler.setMinimumSize(horizontalProfiler.getPreferredSize());
            horizontalProfiler.setSize(horizontalProfiler.getPreferredSize());
            horizontalProfiler.revalidate();
            getProfilerSizeButtonPanel().setVisible(horizontalProfiler.getPreferredSize().width > 0);
        }
        imageScrollPane.revalidate();
        imageScrollPane.repaint();
        if (horizontalProfiler != null) {
            horizontalProfiler.refresh(true);
        }
        if (verticalProfiler != null) {
            verticalProfiler.refresh(true);
        }
    }

    /**
     * Returns the image zone bounds on screen, axis excluded.
     * 
     * @return A {@link Rectangle}, with x and y value corresponding to image zone location on
     *         screen, and width and height to image zone drawing size.
     */
    public Rectangle getImageZoneBoundsOnScreen() {
        Rectangle bounds;
        Point location = imagePanel.getLocationOnScreen();
        if (location == null) {
            bounds = null;
        } else {
            Rectangle canvasRectangle = null;
            if (imageCanvas != null) {
                canvasRectangle = imageCanvas.getCanvasRectangle();
            }
            if (canvasRectangle == null) {
                canvasRectangle = new Rectangle(0, 0);
            }
            bounds = new Rectangle(location.x + xOrigine, location.y + yOrigine, canvasRectangle.width,
                    canvasRectangle.height);
        }
        return bounds;
    }

    @Override
    public void convertorChanged(ValueConvertorEvent event) {
        if (event != null) {
            if (event.getSource() == xAxisConvertor) {
                updateAxisRange(true, getImageScreenPositionCalculator());
                if (beamPointEnabled) {
                    updateBeamPosition();
                }
                repaint();
                if (isAlwaysFitMaxSize()) {
                    differedUpdateRefSize();
                }
            } else if (event.getSource() == yAxisConvertor) {
                updateAxisRange(false, getImageScreenPositionCalculator());
                if (beamPointEnabled) {
                    updateBeamPosition();
                }
                repaint();
                if (isAlwaysFitMaxSize()) {
                    differedUpdateRefSize();
                }
            }
        }
    }

    @Override
    public String getSnapshotDirectory() {
        return snapshotEventDelegate.getSnapshotDirectory();
    }

    @Override
    public void setSnapshotDirectory(String snapshotDirectory) {
        snapshotEventDelegate.setSnapshotDirectory(snapshotDirectory);
    }

    @Override
    public String getSnapshotFile() {
        return snapshotEventDelegate.getSnapshotFile();
    }

    @Override
    public void addSnapshotListener(ISnapshotListener listener) {
        snapshotEventDelegate.addSnapshotListener(listener);
    }

    @Override
    public void removeSnapshotListener(ISnapshotListener listener) {
        snapshotEventDelegate.removeSnapshotListener(listener);
    }

    @Override
    public ImageProperties getImageProperties() {
        String[] rois = null;
        IJRoiManager roiManager = getRoiManager();
        Gradient clone = gradient;
        if (clone != null) {
            clone = clone.clone();
        }
        if (roiManager != null) {
            List<Roi> toChange = roiManager.getAllRois();
            rois = new String[toChange.size()];
            int i = 0;
            for (Roi roi : toChange) {
                rois[i++] = RoiAndPluginUtil.roiToStringBuilder(this, roi).toString();
            }
        }
        return new ImageProperties(isAlwaysFitMaxSize(), isAutoBestFit(), getFitMin(), getFitMax(), getZoom(), rois,
                clone, isLogScale());
    }

    @Override
    public void setImageProperties(ImageProperties properties) throws ApplicationIdException {
        if (properties != null) {
            // JAVAAPI-625: quick set all values and then do a single updateView(true) and updateZoomRoiGenerators()
            quickSetGradient(properties.getGradient());
            setZoom(properties.getZoom(), false);
            setAlwaysFitMaxSize(properties.isFitMaxSize(), false);
            quickSetAutoBestFit(properties.isBestfit());
            setFitMinMax(properties.getFitMin(), properties.getFitMax(), false, false);
            quickSetLogScale(properties.isLogScale());
            updateView(true);
            updateZoomRoiGenerators();
            // try to apply rois
            if (applicationId != null) {
                ImageViewer former = ImageJManager.getActivatedImageViewer(applicationId);
                ImageJManager.setActivatedImageViewer(applicationId, this);
                try {
                    ImageJManager.grabLock(applicationId);
                    // Register default plugins when necessary (EXPDATA-187)
                    if (!RoiAndPluginUtil.areDefaultPluginsRegistered()) {
                        RoiAndPluginUtil.registerDefaultPlugins();
                    }
                    String[] rois = properties.getRois();
                    if (rois != null) {
                        for (String roi : rois) {
                            if (roi != null) {
                                IJ.runMacro(roi);
                            }
                        }
                    }
                    ImageJManager.releaseLock(applicationId);
                } catch (Exception e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .warn(getClass().getSimpleName() + " failed to apply rois on setImageProperties", e);
                }
                ImageJManager.setActivatedImageViewer(applicationId, former);
            }
            revalidate();
            repaint();
        }
    }

    public String getPreferredSnapshotExtension() {
        return preferredSnapshotExtension;
    }

    public void setPreferredSnapshotExtension(String preferredSnapshotExtension) {
        this.preferredSnapshotExtension = preferredSnapshotExtension;
    }

    protected Dimension getCanvasSize() {
        Dimension size;
        IJCanvas canvas = imageCanvas;
        if (canvas == null) {
            size = null;
        } else {
            // IJCanvas returns real drawing size in preferred size.
            // Sometimes, this real drawing size is bigger than max drawing size.
            size = canvas.getPreferredSize();
            if ((size == null) || (size.getWidth() == 0) || (size.getHeight() == 0)) {
                size = canvas.getSize();
            }
        }
        return size;
    }

    @Override
    protected void finalize() throws Throwable {
        imageCanvas = null;
        imagePlus = null;
        removeAll();
        super.finalize();
    }

    public static void main(String[] args) {
        int test = 1;
        if ((args != null) && (args.length > 0)) {
            try {
                test = Integer.parseInt(args[0]);
            } catch (Exception e) {
                test = 1;
            }
        }
        switch (test) {
            case 2:
                test2AddRoi();
                break;
            case 3:
                test3SmallImage();
                break;
            default:
                test1AddRoi();
                break;
        }
    }

    private static IValueConvertor generateAxisConvertor(final double coef, final double offset) {
        IValueConvertor result = new AbstractValueConvertor() {
            @Override
            public boolean isValid() {
                return true;
            }

            @Override
            public double convertValue(double value) {
                return value * coef + offset;
            }
        };
        return result;
    }

    /**
     * 1st test example
     */
    private static void test1AddRoi() {
        JFrame frame = new JFrame();

        JPanel mainPanel = new JPanel(new BorderLayout());

        final ImageViewer imageViewer = new ImageViewer();
        imageViewer.setXAxisConvertor(generateAxisConvertor(5, 100));
        imageViewer.setYAxisConvertor(generateAxisConvertor(10, 10));
        imageViewer.setApplicationId(ImageJManager.generateApplicationId(ImageViewer.class.getName() + "Test"));
        imageViewer.setCleanOnDataSetting(false);

        final JButton button = new JButton(new AbstractAction("Traj") {

            private static final long serialVersionUID = -8226815785641158386L;

            @Override
            public void actionPerformed(ActionEvent e) {
                double[] traj = imageViewer.getTrajectories();
                System.out.println("Trajectories");
                System.out.println("-------------");

                System.out.println(Arrays.toString(traj));

            }
        });

        mainPanel.add(imageViewer, BorderLayout.CENTER);
        mainPanel.add(button, BorderLayout.SOUTH);
        frame.setContentPane(mainPanel);

        frame.setSize(new Dimension(780, 740));

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        // CometeRoi roi = new CometeRoi("Monnom", RoiShape.RECTANGLE, 3, 0, 0, 40, 20, CometeColor.GREEN);
        //
        // CometeRoi roi1 = new CometeRoi("C1", RoiShape.CROSS, 2, 70, 70, 60, 60, CometeColor.BLUE);
        // CometeRoi roi2 = new CometeRoi("C2", RoiShape.CROSS, 2, 100, 110, 80, 80, CometeColor.CYAN);
        // CometeRoi roi3 = new CometeRoi("C2", RoiShape.OVAL, 2, 200, 200, 80, 80, CometeColor.CYAN);
        //
        // CometeRoi roi4 = new CometeRoi("Monnom", RoiShape.RECTANGLE, 3, 0, 0, 100, 100, CometeColor.RED);
        // CometeRoi roi5 = new CometeRoi("C1", RoiShape.NONE, 2, 70, 70, 60, 60, CometeColor.BLUE);
        //
        // imageViewer.addRoi(roi);
        // imageViewer.addRoi(roi1);
        // imageViewer.addRoi(roi2);
        // imageViewer.addRoi(roi3);
        // imageViewer.addRoi(roi4);
        // imageViewer.addRoi(roi5);

        new Thread("Image data") {
            private final Random random = new Random();
            private final int width = 290;
            private final int height = 251;

            @Override
            public void run() {
                while (true) {
                    double[] attr_beam_image_read = new double[width * height];
                    int max_xy = width;

                    int bimg_center_x = random.nextInt() % (int) (max_xy * 0.050000000000000003D);
                    if (random.nextLong() % 2L == 0L) {
                        bimg_center_x *= -1;
                    }
                    int bimg_center_y = random.nextInt() % (int) (max_xy * 0.050000000000000003D);
                    if (random.nextInt() % 2 == 0) {
                        bimg_center_y *= -1;
                    }
                    int bimg_offset_to_zero = (max_xy - 1) / 2;
                    int bimg_x_offset_to_zero = bimg_offset_to_zero + bimg_center_x;
                    int bimg_y_offset_to_zero = bimg_offset_to_zero + bimg_center_y;
                    int limit = max_xy / 8;
                    int noise = random.nextInt() % (int) (limit * 0.20000000000000001D);
                    if (random.nextInt() % 2 == 0) {
                        noise *= -1;
                    }
                    limit += noise;
                    for (int cpt = 0; cpt < width * height; cpt++) {
                        attr_beam_image_read[cpt] = 0;
                    }

                    for (int i = -limit; i < limit; i++) {
                        int y = i + bimg_y_offset_to_zero;
                        if (y >= 0 && y < max_xy) {
                            for (int j = -limit; j < limit; j++) {
                                int x = j + bimg_x_offset_to_zero;
                                if (x >= 0 && x < max_xy) {
                                    int value = (int) Math.sqrt(i * i + j * j);
                                    attr_beam_image_read[x * max_xy + y] = value >= limit ? 0 : limit - value;
                                }
                            }

                        }
                    }
                    imageViewer.setFlatNumberMatrix(attr_beam_image_read, width, height);
                    imageViewer.revalidate();
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        // don't care about this exception: just quit test;
                    }
                }
            }
        }.start();
    }

    /**
     * 2nd test example
     */
    private static void test2AddRoi() {
        ImageViewer imageViewer = new ImageViewer();
        imageViewer.setApplicationId(ImageJManager.generateApplicationId(ImageViewer.class.getName() + "Test"));
        imageViewer.setCleanOnDataSetting(false);
        imageViewer.setShowRoiInformationTable(true);
        double[] flatValues = new double[500 * 300];
        for (int i = 0; i < flatValues.length; i++) {
            flatValues[i] = i * 0.152;
        }

        CometeRoi roi1 = new CometeRoi("Monnom", RoiShape.RECTANGLE, 3, 0, 0, 40, 20,
                fr.soleil.comete.definition.widget.util.CometeColor.GREEN);
        CometeRoi roi2 = new CometeRoi("C1", RoiShape.CROSS, 2, 70, 70, 60, 60,
                fr.soleil.comete.definition.widget.util.CometeColor.BLUE);
        CometeRoi roi3 = new CometeRoi("C2", RoiShape.CROSS, 2, 100, 110, 80, 80,
                fr.soleil.comete.definition.widget.util.CometeColor.CYAN);
        CometeRoi roi4 = new CometeRoi("OVALE", RoiShape.OVAL, 2, 100, 110, 100, 100,
                fr.soleil.comete.definition.widget.util.CometeColor.CYAN);
        CometeRoi roi5 = new CometeRoi("Monnom", RoiShape.RECTANGLE, 3, 0, 0, 100, 100,
                fr.soleil.comete.definition.widget.util.CometeColor.RED);
        CometeRoi roi6 = new CometeRoi(ObjectUtils.EMPTY_STRING, RoiShape.RECTANGLE, 3, 10, 30, 80, 40,
                fr.soleil.comete.definition.widget.util.CometeColor.GREEN);

        imageViewer.addRoi(roi1);
        imageViewer.addRoi(roi2);
        imageViewer.addRoi(roi3);
        imageViewer.addRoi(roi4);
        imageViewer.addRoi(roi5);
        imageViewer.addRoi(roi6);

        imageViewer.setFlatNumberMatrix(flatValues, 300, 500);

        JFrame testFrame = new JFrame();
        testFrame.setContentPane(imageViewer);
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setSize(600, 600);
        testFrame.setVisible(true);
    }

    private static void test3SmallImage() {
        ImageViewer imageViewer = new ImageViewer();
        imageViewer.setBeamPointEnabled(true);
        imageViewer.setAllowBeamPositionByClick(true);
        imageViewer.setDrawBeamPosition(true);
        imageViewer.setApplicationId(ImageJManager.generateApplicationId(ImageViewer.class.getName() + "Test"));
        imageViewer.setCleanOnDataSetting(false);
        imageViewer.setShowRoiInformationTable(true);
        double[] flatValues = new double[3 * 4];
        for (int i = 0; i < flatValues.length; i++) {
            flatValues[i] = i * 0.152;
        }

        imageViewer.setFlatNumberMatrix(flatValues, 3, 4);

        JFrame testFrame = new JFrame();
        testFrame.setContentPane(imageViewer);
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setSize(600, 600);
        testFrame.setVisible(true);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * Let the UndoManager update the buttons' state when events occur.
     * 
     * @author MAINGUY
     * 
     */
    protected class MyUndoManager extends UndoManager {

        private static final long serialVersionUID = -1906642514432864398L;

        public MyUndoManager() {
            super();
        }

        @Override
        public synchronized void discardAllEdits() {
            super.discardAllEdits();
            updateUndoRedoActionState();
        }

        @Override
        public synchronized void undo() throws CannotUndoException {
            super.undo();
            updateUndoRedoActionState();
        }

        @Override
        public synchronized void redo() throws CannotRedoException {
            super.redo();
            updateUndoRedoActionState();
        }

        @Override
        public synchronized boolean addEdit(UndoableEdit anEdit) {
            boolean addEdit = super.addEdit(anEdit);
            updateUndoRedoActionState();
            return addEdit;
        }

        @Override
        public void undoableEditHappened(UndoableEditEvent e) {
            super.undoableEditHappened(e);
            updateUndoRedoActionState();
        }
    }

    /**
     * A class that displays the image
     * 
     * @author Rapha&euml;l GIRARDOT
     * 
     */
    protected class ImagePanel extends JComponent implements ComponentListener, AncestorListener {

        private static final long serialVersionUID = -911828400102774312L;

        private Point mousePosition;

        public ImagePanel() {
            super();
            addAncestorListener(this);
            mousePosition = null;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            paintViewer(g);
        }

        @Override
        public Point getMousePosition() {
            return mousePosition;
        }

        public void setMousePosition(Point mousePosition) {
            this.mousePosition = mousePosition;
        }

        @Override
        public Dimension getPreferredSize() {
            Dimension result = getCanvasSize();
            if (result == null) {
                result = super.getPreferredSize();
            } else {
                computeAxisMargins();
                int width = result.width + leftMargin + rightMargin;
                int height = result.height + upMargin + downMargin;
                result = new Dimension(width, height);
            }
            return result;
        }

        @Override
        public void componentHidden(ComponentEvent e) {
            // nothing to do
        }

        @Override
        public void componentShown(ComponentEvent e) {
            // nothing to do
        }

        @Override
        public void componentMoved(ComponentEvent e) {
            // nothing to do
        }

        @Override
        public void componentResized(ComponentEvent e) {
            // The size of the JScrollpane changed: we set this size as the ImageWindow preferred size,
            // so that the IJCanvas can know the maximum size allowed for zoomed view.
            differedUpdateRefSize();
        }

        @Override
        public void ancestorAdded(AncestorEvent e) {
            differedUpdateRefSize();
        }

        @Override
        public void ancestorMoved(AncestorEvent e) {
            // no need to update size when parent moved
        }

        @Override
        public void ancestorRemoved(AncestorEvent event) {
            // nothing to do
        }

    }

    /**
     * A {@link StateFocusTextField} to edit axis format.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class AxisFormatField extends StateFocusTextField implements PropertyChangeListener {

        private static final long serialVersionUID = 2455818195067873407L;

        protected final boolean x;

        public AxisFormatField(boolean x) {
            this(DEFAULT_MESSAGE_MANAGER, x);
        }

        public AxisFormatField(boolean x, int columns) {
            this(DEFAULT_MESSAGE_MANAGER, x, columns);
        }

        public AxisFormatField(MessageManager messageManager, boolean x) {
            this(messageManager, x, 5);
        }

        public AxisFormatField(MessageManager messageManager, boolean x, int columns) {
            super(messageManager);
            this.x = x;
            setNoValueText(ObjectUtils.EMPTY_STRING);
            setFormatValue(x ? getXAxisFormat() : getYAxisFormat());
            if (columns > -1) {
                setColumns(columns);
            }
            setToolTipText(MESSAGES.getString("ImageJViewer.Settings.Axis.Format.Help"));
            addActionListener((e) -> {
                AbstractAxisView axis = (x ? xAxis : yAxis);
                if (axis != null) {
                    String format = getText();
                    if (format == null) {
                        format = ObjectUtils.EMPTY_STRING;
                    } else {
                        format = format.trim();
                    }
                    setValidValue(format);
                    setAxisFormat(axis, format);
                }
            });
        }

        protected void setFormatValue(String format) {
            setValidValue(format == null ? ObjectUtils.EMPTY_STRING : format);
        }

        @Override
        public void propertyChange(PropertyChangeEvent e) {
            if ((e != null) && (e.getSource() == ImageViewer.this)) {
                if ((X_AXIS_FORMAT.equals(e.getPropertyName()) && x)
                        || (Z_AXIS_FORMAT.equals(e.getPropertyName()) && !x)) {
                    setFormatValue((String) e.getNewValue());
                }
            }
        }

    }

    /**
     * An enum representing how to zoom/unzoom with mouse wheel
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    public static enum MouseZoomMode {
        /**
         * Ignore mouse position: always zoom at image center
         */
        NONE,
        /**
         * The center of the zoomed/unzoomed area should be as close as possible to mouse position
         */
        CENTERED,
        /**
         * The pixel under mouse position should be as close as possible to mouse position after zoom/unzoom
         */
        PIXEL;
    }

}