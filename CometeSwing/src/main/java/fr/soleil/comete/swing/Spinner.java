/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Dimension;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.LinkedHashSet;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.SpinnerUI;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.data.information.NumberInformation;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.listener.ISpinnerListener;
import fr.soleil.comete.definition.widget.ISpinner;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.swing.ui.MetalSpinnerUI;

public class Spinner extends JSpinner implements ISpinner {

    private static final long serialVersionUID = -7143168443585581260L;

    private final Collection<ISpinnerListener> spinnerListeners;
    private String format;
    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;

    private Number min;
    private Number max;
    private Number step;

    private CometeColor background;

    private boolean editable = true, fontSet, foregroundSet;

    public Spinner() {
        super();
        setLocale(Locale.US);
        spinnerListeners = new LinkedHashSet<>();
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        min = Double.valueOf(Double.NEGATIVE_INFINITY);
        max = Double.valueOf(Double.POSITIVE_INFINITY);
        step = Double.valueOf(1);
        background = null;
        setFormat(null, true);
        setSize(70, 30);
        setNumberValue(0L);
    }

    @Override
    public void updateUI() {
        setUI(new MetalSpinnerUI());
        invalidate();
    }

    protected NumberEditor generateEditor() {
        int hAlign;
        try {
            hAlign = getHorizontalAlignment();
        } catch (Exception e) {
            hAlign = 0;
        }
        NumberEditor editor;
        String format = Format.classicFormatToTextDecimalPattern(this.format);
        if (format == null) {
            editor = new NumberEditor(this);
        } else {
            editor = new NumberEditor(this, format);
        }
        editor.getFormat().setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
        editor.getFormat().setGroupingUsed(false);
        if (fontSet) {
            editor.getTextField().setFont(getFont());
        }
        if (foregroundSet) {
            editor.getTextField().setForeground(getForeground());
        }
        CometeColor background = this.background;
        if (background != null) {
            editor.getTextField().setBackground(ColorTool.getColor(background));
        }
        editor.getTextField().setOpaque(isOpaque());
        if (hAlign != 0) {
            editor.getTextField().setHorizontalAlignment(hAlign);
        }
        return editor;
    }

    @Override
    protected JComponent createEditor(SpinnerModel model) {
        return generateEditor();
    }

    @Override
    public void setUI(SpinnerUI newUI) {
        super.setUI(newUI);
        updateEditability();
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void setSize(int width, int height) {
        super.setSize(width, height);
        setPreferredSize(getSize());
    }

    @Override
    public void setFormat(String format) {
        setFormat(format, false);
    }

    protected void updateEditor() {
        NumberEditor editor = generateEditor();
        // here, we make sure the textfield displays the correctly formatted value
        editor.getTextField().setValue(getValue());
        setEditor(editor);
        if (background != null) {
            setCometeBackground(background);
        }
    }

    protected void setFormat(String format, boolean forceEditorChange) {
        boolean canSet = true;
        if (this.format == null) {
            if (format == null) {
                canSet = false;
            }
        } else if (this.format.equals(format)) {
            canSet = false;
        }
        if (canSet || forceEditorChange) {
            this.format = format;
            updateEditor();
        }
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setBackground(ColorTool.getColor(color));
        background = color;
        JComponent editor = getEditor();
        if (editor instanceof NumberEditor) {
            ((NumberEditor) editor).getTextField().setBackground(ColorTool.getColor(color));
        }
    }

    @Override
    public void setCometeFont(CometeFont font) {
        fontSet = (font != null);
        super.setFont(FontTool.getFont(font));
        JComponent editor = getEditor();
        if (editor instanceof NumberEditor) {
            ((NumberEditor) editor).getTextField().setFont(FontTool.getFont(font));
        }
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        foregroundSet = (color != null);
        super.setForeground(ColorTool.getColor(color));
        JComponent editor = getEditor();
        if (editor instanceof NumberEditor) {
            ((NumberEditor) editor).getTextField().setForeground(ColorTool.getColor(color));
        }
    }

    @Override
    public int getHorizontalAlignment() {
        int align = 0;
        JComponent editor = getEditor();
        if (editor instanceof NumberEditor) {
            align = ((NumberEditor) editor).getTextField().getHorizontalAlignment();
        }
        return align;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        JComponent editor = getEditor();
        if (editor instanceof NumberEditor) {
            ((NumberEditor) editor).getTextField().setHorizontalAlignment(halign);
        }
    }

    @Override
    public void setOpaque(boolean opaque) {
        super.setOpaque(opaque);
        JComponent editor = getEditor();
        if (editor instanceof NumberEditor) {
            ((NumberEditor) editor).getTextField().setOpaque(opaque);
        }
    }

    @Override
    public void commitEdit() throws ParseException {
        // XXX commitEdit() brings bad behaviors, like going back to previous value
    }

    @Override
    public void addSpinnerListener(final ISpinnerListener listener) {
        if (listener != null) {
            synchronized (spinnerListeners) {
                spinnerListeners.add(listener);
            }
        }
    }

    @Override
    public void removeSpinnerListener(final ISpinnerListener listener) {
        if (listener != null) {
            synchronized (spinnerListeners) {
                spinnerListeners.remove(listener);
            }
        }
    }

    protected void fireValueChanged() {
        fireValueChanged(new EventObject(this));
    }

    @Override
    public void fireValueChanged(EventObject event) {
        Collection<ISpinnerListener> listeners;
        synchronized (spinnerListeners) {
            listeners = new ArrayList<>(spinnerListeners);
        }
        for (ISpinnerListener listener : listeners) {
            listener.valueChanged(event);
        }
        listeners.clear();
    }

    protected Number getNumberValue(boolean nanForUnset) {
        Number result;
        Object value = getValue();
        if (value instanceof Number) {
            result = (Number) value;
        } else if (nanForUnset) {
            result = Double.valueOf(Double.NaN);
        } else {
            result = Long.valueOf(0L);
        }
        return result;
    }

    @Override
    public Number getNumberValue() {
        return getNumberValue(false);
    }

    protected boolean isFloat(Number value) {
        return (value == null) || (value instanceof Double) || (value instanceof Float);
    }

    @Override
    public void setNumberValue(Number value) {
        setValue(value, isFloat(value), false);
    }

    @Override
    public void setValue(Object value) {
        setValue(value, isFloat(getNumberValue()), true);
    }

    protected synchronized void setValue(final Object value, final boolean floatValue, final boolean notify) {
        if ((value == null) || (value instanceof Number)) {
            Number nb = (Number) value;
            if (nb == null) {
                nb = Double.valueOf(MathConst.NAN_FOR_NULL);
            }
            NumberEditor editor = (NumberEditor) getEditor();
            removeChangeListener(editor);
            editor.getTextField().removePropertyChangeListener(editor);
            try {
                if (floatValue) {
                    if (!(nb instanceof Double)) {
                        nb = Double.valueOf(nb.doubleValue());
                    }
                } else if (!(nb instanceof Long)) {
                    nb = Long.valueOf(nb.longValue());
                }
                updateModel(nb);
                editor.getTextField().setValue(getValue());
            } finally {
                NumberEditor newEditor = (NumberEditor) getEditor();
                // No need to register PropertyChangeListener if editor changed
                if (newEditor == editor) {
                    editor.getTextField().addPropertyChangeListener(editor);
                }
            }
            addChangeListener(editor);
        } else {
            super.setValue(value);
        }
        fireValueChanged();
        if (notify) {
            warnMediators();
        }
    }

    @Override
    public Number getMinimum() {
        return min;
    }

    @Override
    public synchronized void setMinimum(Number minimum) {
        if (minimum == null) {
            minimum = Double.valueOf(Double.NEGATIVE_INFINITY);
        }
        min = minimum;
        updateModel(getNumberValue());
    }

    @Override
    public Number getMaximum() {
        return max;
    }

    @Override
    public synchronized void setMaximum(Number maximum) {
        if (maximum == null) {
            maximum = Double.valueOf(Double.POSITIVE_INFINITY);
        }
        max = maximum;
        updateModel(getNumberValue());
    }

    @Override
    public Number getStep() {
        return step;
    }

    @Override
    public synchronized void setStep(Number step) {
        if (step == null) {
            step = Long.valueOf(1);
        }
        this.step = step;
        updateModel(getNumberValue());
    }

    protected void warnModelListeners() {
        SpinnerNumberModel model = (SpinnerNumberModel) getModel();
        ChangeListener[] listeners = model.getChangeListeners();
        ChangeEvent event = new ChangeEvent(model);
        if (listeners != null) {
            for (ChangeListener listener : listeners) {
                listener.stateChanged(event);
            }
        }
    }

    private void updateModel(Number value) {
        // updates model if the different Numbers are not of the same class, or if a value changed
        SpinnerModel tmpModel = getModel();
        SpinnerNumberModel model;
        if (tmpModel instanceof SpinnerNumberModel) {
            model = (SpinnerNumberModel) tmpModel;
        } else {
            model = null;
        }

        // 1st, check Number type
        boolean isOk = model == null ? false
                : sameNumberType(value, model.getStepSize()) && sameNumberType(value, (Number) model.getMinimum())
                        && sameNumberType(value, (Number) model.getMaximum());

        // then, check values
        if ((value instanceof Double) || (value instanceof Float)) {
            // double values
            isOk = isOk && sameDouble(step, model.getStepSize()) && sameDouble(min, (Number) model.getMinimum())
                    && sameDouble(max, (Number) model.getMaximum());

            // update if necessary
            Double doubleValue = getDouble(value);
            if (isOk) {
                model.setValue(doubleValue);
            } else {
                Double dmin, dmax, dstep;
                if (Double.isNaN(doubleValue)) {
                    // NaN is a particular case
                    dmin = null;
                    dmax = null;
                } else {
                    dmin = getDouble(min);
                    dmax = getDouble(max);
                }
                dstep = getDouble(step);
                if (model == null) {
                    model = new SpinnerNumberModel(doubleValue, dmin, dmax, dstep);
                    setModel(model);
                } else {
                    model.setMinimum(dmin);
                    model.setMaximum(dmax);
                    model.setStepSize(dstep);
                    model.setValue(doubleValue);
                }
            }
        } else {
            // long values
            isOk = isOk && sameLong(step, model.getStepSize()) && sameLong(min, (Number) model.getMinimum())
                    && sameLong(max, (Number) model.getMaximum());

            // update if necessary
            Long longValue = getLong(value);
            if (isOk) {
                model.setValue(longValue);
            } else {
                Long lmin, lmax, lstep;
                lmin = getLong(min);
                lmax = getLong(max);
                lstep = getLong(step);
                if (model == null) {
                    model = new SpinnerNumberModel(longValue, lmin, lmax, lstep);
                    setModel(model);
                } else {
                    model.setMinimum(lmin);
                    model.setMaximum(lmax);
                    model.setStepSize(lstep);
                    model.setValue(longValue);
                }
            }
        }
        // Editor has to be updated after model update.
        // Otherwise, value edition through textfield won't be possible (SCAN-938).
        updateEditor();
    }

    private Long getLong(Number n) {
        Long result;
        if ((n == null) || (n instanceof Long)) {
            result = (Long) n;
        } else {
            result = Long.valueOf(n.longValue());
        }
        return result;
    }

    private Double getDouble(Number n) {
        Double result;
        if ((n == null) || (n instanceof Double)) {
            result = (Double) n;
        } else {
            result = Double.valueOf(n.doubleValue());
        }
        return result;
    }

    private boolean sameLong(Number n1, Number n2) {
        boolean same = false;
        if (n1 == n2) {
            same = true;
        } else if (n1 != null) {
            same = ((n2 != null) && (n1.longValue() == n2.longValue()));
        }
        return same;
    }

    private boolean sameDouble(Number n1, Number n2) {
        boolean same = false;
        if (n1 == n2) {
            same = true;
        } else if (n1 != null) {
            same = ((n2 != null) && (n1.doubleValue() == n2.doubleValue()));
        }
        return same;
    }

    private boolean sameNumberType(Number n1, Number n2) {
        boolean same = false;
        if (n1 != null) {
            same = ((n2 != null) && n1.getClass().equals(n2.getClass()));
        }
        return same;
    }

    protected void updateEditability() {
        JComponent tempEditor = getEditor();
        if (tempEditor instanceof DefaultEditor) {
            DefaultEditor editor = (DefaultEditor) tempEditor;
            editor.getTextField().setEditable(editable);
            editor.getTextField().setEnabled(isEnabled());
        }
        for (java.awt.Component comp : getComponents()) {
            if (comp instanceof JButton) {
                ((JButton) comp).setEnabled(editable && isEnabled());
            }
        }
    }

    @Override
    public void setEditor(JComponent editor) {
        super.setEditor(editor);
        updateEditability();
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void setEditable(boolean b) {
        if (this.editable != b) {
            editable = b;
            updateEditability();
        }
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Warns Mediators for some changes in this {@link Spinner}
     */
    public void warnMediators() {
        Object value = getValue();
        if (value instanceof Number) {
            warnMediators(new NumberInformation(this, (Number) value));
        }
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        boolean editing = false;
        if (isEditable() && isEnabled()) {
            JComponent editor = getEditor();
            if (editor instanceof DefaultEditor) {
                editing = ((DefaultEditor) editor).getTextField().hasFocus();
            }
        }
        return editing;
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(Spinner.class.getSimpleName() + " test");
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        final Spinner spinner = new Spinner();
        spinner.setNumberValue(Math.random());
        spinner.setFormat("%5.2e");
        testFrame.setContentPane(spinner);
        testFrame.setSize(300, 200);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

}
