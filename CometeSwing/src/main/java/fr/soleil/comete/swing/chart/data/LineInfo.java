/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.data;

/**
 * A class used to store information about a file: its first readable line and whether the file follows NxExtractor
 * format.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LineInfo {

    private final String line;
    private final boolean nxExtractorFormat;

    public LineInfo(String line, boolean nxExtractorFormat) {
        super();
        this.line = line;
        this.nxExtractorFormat = nxExtractorFormat;
    }

    /**
     * Returns the first readable line.
     * 
     * @return The first readable line.
     */
    public String getLine() {
        return line;
    }

    /**
     * Returns whether the file follows NxExtractor format.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isNxExtractorFormat() {
        return nxExtractorFormat;
    }

}