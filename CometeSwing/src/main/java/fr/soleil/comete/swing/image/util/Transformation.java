/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util;

/**
 * List of all possible transformations
 */
public enum Transformation {
    POSITIVE_ROTATION("Rotation+"), NEGATIVE_ROTATION("Rotation-"), NO_ROTATION("None"),
    VERTICAL_SYMMETRY("Symmetry(V axis)"), HORIZONTAL_SYMMETRY("Symmetry(H axis)");

    protected String stringValue;

    private Transformation(String stringValue) {
        this.stringValue = stringValue;
    }

    @Override
    public String toString() {
        return stringValue;
    }

    /**
     * Parses a {@link String} value to return a {@link Transformation}
     * 
     * @param value The {@link String} value
     * @return A {@link Transformation}. May be <code>null</code>.
     */
    public static Transformation parseTransformation(String value) {
        Transformation transformation = null;
        for (Transformation tmp : values()) {
            if (tmp.toString().equals(value)) {
                transformation = tmp;
                break;
            }
        }
        return transformation;
    }
}
