/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.shape;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;

import fr.soleil.comete.definition.widget.util.IChartConst.ShapeType;

/**
 * {@link AbstractShape} specialized in strings
 * 
 * @author huriez
 */
public class StringShape extends AbstractShape {

    protected final String stringValue;
    protected final Point stringOrigin;
    protected Font stringFont;
    private final double stringRotation;
    private final boolean stringFontAcsent;

    public StringShape(ShapeType type, String string, Point point) {
        this(type, string, point, null, null, 0, false);
    }

    public StringShape(ShapeType type, String string, Point point, Color color, Font font) {
        this(type, string, point, color, font, 0, false);
    }

    public StringShape(ShapeType type, String string, Point point, Color color, Font font, double rotation,
            boolean fontAscent) {
        super(type);
        this.color = color;
        stringValue = string;
        stringOrigin = point;
        stringFont = font;
        stringRotation = rotation;
        stringFontAcsent = fontAscent;
    }

    public void setStringColor(Color stringColor) {
        color = stringColor;
    }

    public void setStringFont(Font stringFont) {
        this.stringFont = stringFont;
    }

    /**
     * @return the stringColor
     */
    public Color getStringColor() {
        return color;
    }

    /**
     * @return the stringValue
     */
    public String getStringValue() {
        return stringValue;
    }

    /**
     * @return the stringOrigin
     */
    public Point getStringOrigin() {
        return stringOrigin;
    }

    /**
     * @return the stringFont
     */
    public Font getStringFont() {
        return stringFont;
    }

    @Override
    public void drawShape(Graphics2D g) {
        if (color != null) {
            g.setColor(color);
        }
        if (stringFont != null) {
            g.setFont(stringFont);
        }
        int yOrigin = stringOrigin.y;
        if (stringFontAcsent) {
            yOrigin += g.getFontMetrics(stringFont).getAscent();
        }
        g.translate(x, y);
        if (stringRotation != 0) {
            g.rotate(stringRotation);
            g.drawString(stringValue, stringOrigin.x, yOrigin);
            g.rotate(-stringRotation);
        } else {
            g.drawString(stringValue, stringOrigin.x, yOrigin);
        }
        g.translate(-x, -y);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getClass().getSimpleName());
        builder.append(": {");
        builder.append("stringValue: \"").append(stringValue).append("\"|");
        builder.append("color: ").append(color).append("|");
        builder.append("stringOrigin: ").append(stringOrigin).append("|");
        builder.append("stringFont: ").append(stringFont).append("|");
        builder.append("stringRotation: ").append(stringRotation).append("|");
        builder.append("stringFontAcsent? ").append(stringFontAcsent).append("}");
        return builder.toString();
    }
}
