/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.io.IOException;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.util.ij.plugin.CometePlugInFilter;
import fr.soleil.comete.swing.image.util.ij.plugin.DisplayMask;
import fr.soleil.comete.swing.image.util.ij.plugin.InsideValidation;
import fr.soleil.comete.swing.image.util.ij.plugin.Intersection;
import fr.soleil.comete.swing.image.util.ij.plugin.LoadMaskAddPlugin;
import fr.soleil.comete.swing.image.util.ij.plugin.LoadMaskPlugin;
import fr.soleil.comete.swing.image.util.ij.plugin.MakeCirclePlugin;
import fr.soleil.comete.swing.image.util.ij.plugin.MakeShapePlugin;
import fr.soleil.comete.swing.image.util.ij.plugin.OutsideValidation;
import fr.soleil.comete.swing.image.util.ij.plugin.RoiSelection;
import fr.soleil.comete.swing.image.util.ij.plugin.SetSectorPlugin;
import fr.soleil.comete.swing.image.util.ij.plugin.Subtraction;
import fr.soleil.comete.swing.image.util.ij.plugin.Union;
import fr.soleil.comete.swing.image.util.ij.plugin.XOr;
import fr.soleil.comete.swing.image.util.ij.roi.CircleRoi;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import ij.Menus;
import ij.gui.Line;
import ij.gui.OvalRoi;
import ij.gui.PointRoi;
import ij.gui.PolygonRoi;
import ij.gui.Roi;
import ij.gui.ShapeRoi;

/**
 * A useful class to interact with imageJ Rois and plugins
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class RoiAndPluginUtil implements CometeConstants {

    public static final String INSIDE_SELECTION_RUN_COMMAND = "run(\"insideselection\");";
    public static final String OUTSIDE_SELECTION_RUN_COMMAND = "run(\"outsideselection\");";
    public static final String SELECTION_RUN_COMMAND = "run(\"selection\");";
    public static final String INSIDE_SELECTION_COMMAND = "insideselection";
    public static final String OUTSIDE_SELECTION_COMMAND = "outsideselection";
    public static final String SELECTION_COMMAND = "selection";

    public static final String ROI_XML_TAG = "type";
    public static final String ROI_CREATION_ATTRIBUTE = "creation";
    public static final String ROI_REGION_ATTRIBUTE = "region";
    public static final String SELECTION_ATTRIBUTE = "selected";
    public static final String TYPE_INNER_VALUE = "inner";
    public static final String TYPE_OUTER_VALUE = "outer";
    public static final String TYPE_NONE_VALUE = "none";

    protected static final String SELECTION_BUILDER = "makeSelection(";
    protected static final String LINE_BUILDER = "makeLine(";
    protected static final String OVAL_BUILDER = "makeOval(";
    protected static final String POLYGON_BUILDER = "makePolygon(";
    protected static final String RECTANGLE_BUILDER = "makeRectangle(";
    protected static final String POINT_BUILDER = "makePoint(";
    protected static final String SHAPE_BUILDER = "makeShape";
    protected static final String CIRCLE_BUILDER = "makeCircle";
    protected static final String ARRAY_START = "newArray";
    protected static final String ARRAY_BUILDER = "newArray(";
    protected static final String RUN_BUILDER = "run(";
    protected static final String RUN_STRING_BUILDER = "run(\"";
    protected static final String MACRO_CLOSER = ");\n";
    protected static final String ARRAY_CLOSER = ")";
    protected static final String SELECTION_SEPARATOR = ", ";
    protected static final String VALUE_SEPARATOR = COMMA;
    protected static final String PARAMETER_SEPARATOR = ",\"";
    protected static final String ARRAY_SEPARATOR = "), ";
    protected static final String STRING_DECORATOR = "\"";
    protected static final String TYPE_ANGLE = "angle";
    protected static final String TYPE_FREE_LINE = "freeline";
    protected static final String TYPE_FREE_HAND = "freehand";
    protected static final String TYPE_POLYLINE = "polyline";
    protected static final String TYPE_POINT = "point";
    protected static final String LINE_FEED = ObjectUtils.NEW_LINE;
    protected static final String SHAPE_OPENER = "(";
    public static final String SHAPE_CLOSER = ")";
    protected static final String SHAPE_CLOSER_SEPARATOR = "),";
    protected static final String AND_SO_ON = "[...]";

    protected static final String SHAPE_ANGLE = "Angle(";
    protected static final String SHAPE_FREE_LINE = "FreeLine(";
    protected static final String SHAPE_FREE_ROI = "FreeRoi(";
    protected static final String SHAPE_LINE = "Line(";
    protected static final String SHAPE_OVAL = "Oval(";
    protected static final String SHAPE_POLYGON = "Polygon(";
    protected static final String SHAPE_POLY_LINE = "PolyLine";
    protected static final String SHAPE_COMPOSITE = "Shape(";
    protected static final String SHAPE_POINT = "Point(";
    protected static final String SHAPE_CIRCLE = "Circle(";
    protected static final String SHAPE_RECTANGLE = "Rectangle(";

    protected static final int MAX_POINTS = 6;

    /**
     * Appends a {@link Polygon}'s points in an {@link Appendable}
     * 
     * @param p the {@link Polygon} If <code>null</code>, this method does nothing.
     * @param buffer the {@link Appendable}. If <code>null</code>, this method does nothing.
     * @throws IOException If a problem occurred when adding information to the {@link Appendable}
     * @see Appendable#append(CharSequence)
     */
    private static <T extends Appendable> void appendPolygonInBuffer(Polygon p, T buffer) throws IOException {
        appendPolygonInBuffer(p, buffer, -1);
    }

    /**
     * Appends a {@link Polygon}'s points in an {@link Appendable}, according to a certain limit (maximum
     * {@link #MAX_POINTS} {x, y} coordinates).
     * 
     * @param p the {@link Polygon} If <code>null</code>, this method does nothing.
     * @param buffer the {@link Appendable}. If <code>null</code>, this method does nothing.
     * @throws IOException If a problem occurred when adding information to the {@link Appendable}
     * @see Appendable#append(CharSequence)
     */
    private static <T extends Appendable> void appendLimitedPolygonInBuffer(Polygon p, T buffer) throws IOException {
        appendPolygonInBuffer(p, buffer, MAX_POINTS);
    }

    /**
     * Appends a {@link Polygon}'s points in an {@link Appendable}
     * 
     * @param p the {@link Polygon} If <code>null</code>, this method does nothing.
     * @param buffer the {@link Appendable}. If <code>null</code>, this method does nothing.
     * @param maxLength The maximum number of {x, y} to write in buffer. Use a negative value to write them all.
     * @throws IOException If a problem occurred when adding information to the {@link Appendable}
     * @see Appendable#append(CharSequence)
     */
    private static <T extends Appendable> void appendPolygonInBuffer(Polygon p, T buffer, int maxLength)
            throws IOException {
        if (buffer != null && p != null) {
            if (maxLength < 0) {
                for (int i = 0; i < p.npoints; i++) {
                    buffer.append(Integer.toString(p.xpoints[i])).append(VALUE_SEPARATOR);
                    buffer.append(Integer.toString(p.ypoints[i]));
                    if (i != p.npoints - 1) {
                        buffer.append(SELECTION_SEPARATOR);
                    }
                }
            } else {
                for (int i = 0; i < p.npoints; i++) {
                    if (i >= maxLength) {
                        buffer.append(AND_SO_ON);
                        break;
                    }
                    buffer.append(Integer.toString(p.xpoints[i])).append(VALUE_SEPARATOR);
                    buffer.append(Integer.toString(p.ypoints[i]));
                    if (i != p.npoints - 1) {
                        buffer.append(SELECTION_SEPARATOR);
                    }
                }
            }
        }
    }

    /**
     * Appends a {@link Polygon}'s points in an {@link Appendable}, as 2 separated arrays
     * 
     * @param p the {@link Polygon} If <code>null</code>, this method does nothing.
     * @param buffer the {@link Appendable}. If <code>null</code>, this method does nothing.
     * @throws IOException If a problem occurred when adding information to the {@link Appendable}
     * @see Appendable#append(CharSequence)
     */
    private static <T extends Appendable> void appendSeparatedPolygonInBuffer(Polygon p, T buffer) throws IOException {
        if (buffer != null && p != null) {
            buffer.append(ARRAY_BUILDER);
            for (int i = 0; i < p.npoints; i++) {
                buffer.append(Integer.toString(p.xpoints[i]));
                if (i != p.npoints - 1) {
                    buffer.append(VALUE_SEPARATOR);
                }
            }
            buffer.append(ARRAY_SEPARATOR).append(ARRAY_BUILDER);
            for (int i = 0; i < p.npoints; i++) {
                buffer.append(Integer.toString(p.ypoints[i]));
                if (i != p.npoints - 1) {
                    buffer.append(VALUE_SEPARATOR);
                }
            }
            buffer.append(ARRAY_CLOSER);
        }
    }

    /**
     * Creates a {@link StringBuilder} that contains a {@link Roi} {@link String} representation
     * 
     * @param imageViewer The {@link ImageViewer} that knows the {@link Roi}
     * @param roi the {@link Roi}
     * @return A {@link StringBuilder}
     */
    public static StringBuilder roiToStringBuilder(ImageViewer imageViewer, Roi roi) {
        StringBuilder result = null;
        try {
            result = roiToAppendable(imageViewer, roi, new StringBuilder());
        } catch (IOException e) {
            // should never happen
            result = null;
        }
        return result;
    }

    /**
     * Creates a {@link StringBuilder} that contains a {@link Roi} {@link String} representation
     * 
     * @param imageViewer The {@link ImageViewer} that knows the {@link Roi}
     * @param roi the {@link Roi}
     * @return A {@link StringBuilder}
     */
    public static StringBuilder roiToStringBuilder(IMaskedImageViewer imageViewer, Roi roi) {
        StringBuilder result = null;
        try {
            result = roiToAppendable(imageViewer, roi, new StringBuilder());
        } catch (IOException e) {
            // should never happen
            result = null;
        }
        return result;
    }

    /**
     * Returns {@link Roi} {@link String} representation in an {@link Appendable}. If you want to
     * use the resulting code in a macro, you need to register the comete plugins firsts by calling
     * {@link #registerDefaultPlugins()}
     * 
     * @param imageViewer The {@link ImageViewer} that knows the {@link Roi}
     * @param roi The {@link Roi}
     * @param buffer The {@link Appendable} in which to add roi's String representation. Must not be <code>null</code>
     *            (otherwise, nothing is done)
     * @return The {@link Appendable} with roi's String representation.
     * @throws IOException If a problem occurred when adding information to the {@link Appendable}
     * @see Appendable#append(CharSequence)
     */
    public static <T extends Appendable> T roiToAppendable(ImageViewer imageViewer, Roi roi, T buffer)
            throws IOException {
        T result = roiConstructionToAppendable(roi, buffer, '"');
        roiDetailsToAppendable(imageViewer, roi, result);
        return result;
    }

    /**
     * Returns {@link Roi} {@link String} representation in an {@link Appendable}. If you want to
     * use the resulting code in a macro, you need to register the comete plugins firsts by calling
     * {@link #registerDefaultPlugins()}
     * 
     * @param imageViewer The {@link ImageViewer} that knows the {@link Roi}
     * @param roi The {@link Roi}
     * @param buffer The {@link Appendable} in which to add roi's String representation. Must not be <code>null</code>
     *            (otherwise, nothing is done)
     * @return The {@link Appendable} with roi's String representation.
     * @throws IOException If a problem occurred when adding information to the {@link Appendable}
     * @see Appendable#append(CharSequence)
     */
    public static <T extends Appendable> T roiToAppendable(IMaskedImageViewer imageViewer, Roi roi, T buffer)
            throws IOException {
        T result = roiConstructionToAppendable(roi, buffer, '"');
        roiDetailsToAppendable(imageViewer, roi, result);
        return result;
    }

    /**
     * Creates an {@link XMLLine} that represents a given {@link Roi}
     * 
     * @param imageViewer The {@link ImageViewer} that knows the {@link Roi}
     * @param roi The {@link Roi}
     * @return An {@link XMLLine}
     */
    public static XMLLine roiToXmlLine(ImageViewer imageViewer, Roi roi) {
        XMLLine openingLine = new XMLLine(ROI_XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        if (roi != null) {
            try {
                openingLine.setAttribute(ROI_CREATION_ATTRIBUTE,
                        roiConstructionToAppendable(roi, new StringBuilder(), '\'').toString());
            } catch (IOException e) {
                // should not happen becaus StringBuilder does not throw this exception
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to convert Roi " + roi + " to XML line",
                        e);
                imageViewer = null;
            }
            if (imageViewer != null) {
                if (imageViewer.getRoiManager().isInner(roi)) {
                    openingLine.setAttribute(ROI_REGION_ATTRIBUTE, TYPE_INNER_VALUE);
                } else if (imageViewer.getRoiManager().isOuter(roi)) {
                    openingLine.setAttribute(ROI_REGION_ATTRIBUTE, TYPE_OUTER_VALUE);
                } else {
                    openingLine.setAttribute(ROI_REGION_ATTRIBUTE, TYPE_NONE_VALUE);
                }
                openingLine.setAttribute(SELECTION_ATTRIBUTE,
                        Boolean.toString(imageViewer.getRoiManager().isSelectedRoi(roi)));
            }
        }
        return openingLine;
    }

    /**
     * Returns {@link Roi} {@link String} representation in an {@link Appendable}. This {@link String} representation
     * only contains the way to construct the roi (rectangle/polygone, etc..). It will not contain any information about
     * whether it is selected or considered as inner or outer, as there is no referent {@link ImageViewer}
     * 
     * @param roi the {@link Roi}
     * @param buffer The {@link Appendable} in which to add roi's String representation. Must not be <code>null</code>
     *            (otherwise, nothing is done)
     * @param stringCharacter The character that can be used for strings (generally &quot;, but you can also use &apos;)
     * @return The {@link Appendable} with roi's String representation.
     * @throws IOException If a problem occurred when adding information to the {@link Appendable}
     * @see Appendable#append(CharSequence)
     */
    protected static <T extends Appendable> T roiConstructionToAppendable(Roi roi, T buffer, char stringCharacter)
            throws IOException {
        T result = buffer;
        if ((result != null) && (roi != null)) {
            Polygon roiPolygon = roi.getPolygon();
            Rectangle roiBounds = roi.getBounds();
            switch (roi.getType()) {
                case Roi.ANGLE:
                    if (roiPolygon != null) {
                        result.append(SELECTION_BUILDER).append(stringCharacter).append(TYPE_ANGLE)
                                .append(stringCharacter).append(SELECTION_SEPARATOR);
                        appendSeparatedPolygonInBuffer(roiPolygon, result);
                        result.append(MACRO_CLOSER);
                    }
                    break;
                case Roi.FREELINE:
                    if (roiPolygon != null) {
                        result.append(SELECTION_BUILDER).append(stringCharacter).append(TYPE_FREE_LINE)
                                .append(stringCharacter).append(SELECTION_SEPARATOR);
                        appendSeparatedPolygonInBuffer(roiPolygon, result);
                        result.append(MACRO_CLOSER);
                    }
                    break;
                case Roi.FREEROI:
                case Roi.TRACED_ROI:
                    if (roiPolygon != null) {
                        result.append(SELECTION_BUILDER).append(stringCharacter).append(TYPE_FREE_HAND)
                                .append(stringCharacter).append(SELECTION_SEPARATOR);
                        appendSeparatedPolygonInBuffer(roiPolygon, result);
                        result.append(MACRO_CLOSER);
                    }
                    break;
                case Roi.LINE:
                    Line lroi = (Line) roi;
                    result.append(LINE_BUILDER);
                    result.append(Integer.toString(lroi.x1)).append(VALUE_SEPARATOR);
                    result.append(Integer.toString(lroi.y1)).append(VALUE_SEPARATOR);
                    result.append(Integer.toString(lroi.x2)).append(VALUE_SEPARATOR);
                    result.append(Integer.toString(lroi.y2)).append(MACRO_CLOSER);
                    break;
                case Roi.OVAL:
                    if (roiBounds != null) {
                        result.append(OVAL_BUILDER);
                        result.append(Integer.toString(roiBounds.x)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.y)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.width)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.height)).append(MACRO_CLOSER);
                    }
                    break;
                case Roi.POLYGON:
                    if (roiPolygon != null) {
                        result.append(POLYGON_BUILDER);
                        appendPolygonInBuffer(roiPolygon, result);
                        result.append(MACRO_CLOSER);
                    }
                    break;
                case Roi.POLYLINE:
                    if (roiPolygon != null) {
                        result.append(SELECTION_BUILDER).append(stringCharacter).append(TYPE_POLYLINE)
                                .append(stringCharacter).append(SELECTION_SEPARATOR);
                        appendSeparatedPolygonInBuffer(roiPolygon, result);
                        result.append(MACRO_CLOSER);
                    }
                    break;
                case Roi.COMPOSITE:
                    ShapeRoi sroi = (ShapeRoi) roi;
                    float[] shapeArray = sroi.getShapeAsArray();
                    if (shapeArray != null) {
                        result.append(RUN_BUILDER).append(stringCharacter).append(SHAPE_BUILDER).append(stringCharacter)
                                .append(VALUE_SEPARATOR).append(stringCharacter).append(SHAPE_OPENER);
                        for (int i = 0; i < shapeArray.length; i++) {
                            result.append(Float.toString(shapeArray[i]));
                            if (i != shapeArray.length - 1) {
                                result.append(VALUE_SEPARATOR);
                            }
                        }
                        result.append(ARRAY_CLOSER).append(stringCharacter).append(MACRO_CLOSER);
                    }
                    break;
                case Roi.POINT:
                    if ((roiPolygon != null) && (roiPolygon.npoints == 1)) {
                        result.append(POINT_BUILDER);
                        result.append(Integer.toString(roiPolygon.xpoints[0]));
                        result.append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiPolygon.ypoints[0]));
                        result.append(MACRO_CLOSER);
                    }
                    break;
                case CircleRoi.TYPE:
                    CircleRoi circleRoi = (CircleRoi) roi;
                    result.append(RUN_BUILDER).append(stringCharacter).append(CIRCLE_BUILDER).append(stringCharacter)
                            .append(VALUE_SEPARATOR).append(stringCharacter).append(SHAPE_OPENER)
                            .append(Double.toString(circleRoi.getCenterX())).append(VALUE_SEPARATOR)
                            .append(Double.toString(circleRoi.getCenterZ())).append(VALUE_SEPARATOR)
                            .append(Float.toString(circleRoi.getRadius())).append(VALUE_SEPARATOR)
                            .append(circleRoi.getStringRadius()).append(ARRAY_CLOSER).append(stringCharacter)
                            .append(MACRO_CLOSER);
                    break;
                default:
                    if (roiBounds != null) {
                        result.append(RECTANGLE_BUILDER);
                        result.append(Integer.toString(roiBounds.x)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.y)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.width)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.height)).append(MACRO_CLOSER);
                    }
                    break;
            }
        }
        return result;
    }

    public static <T extends Appendable> T roiShapeToAppendable(Roi roi, T buffer) throws IOException {
        T result = buffer;
        if ((result != null) && (roi != null)) {
            Polygon roiPolygon = roi.getPolygon();
            Rectangle roiBounds = roi.getBounds();
            switch (roi.getType()) {
                case Roi.ANGLE:
                    if (roiPolygon != null) {
                        result.append(SHAPE_ANGLE);
                        appendLimitedPolygonInBuffer(roiPolygon, result);
                        result.append(')');
                    }
                    break;
                case Roi.FREELINE:
                    if (roiPolygon != null) {
                        result.append(SHAPE_FREE_LINE);
                        appendLimitedPolygonInBuffer(roiPolygon, result);
                        result.append(')');
                    }
                    break;
                case Roi.FREEROI:
                case Roi.TRACED_ROI:
                    if (roiPolygon != null) {
                        result.append(SHAPE_FREE_ROI);
                        appendLimitedPolygonInBuffer(roiPolygon, result);
                        result.append(')');
                    }
                    break;
                case Roi.LINE:
                    Line lroi = (Line) roi;
                    result.append(SHAPE_LINE);
                    result.append(Integer.toString(lroi.x1)).append(VALUE_SEPARATOR);
                    result.append(Integer.toString(lroi.y1)).append(VALUE_SEPARATOR);
                    result.append(Integer.toString(lroi.x2)).append(VALUE_SEPARATOR);
                    result.append(Integer.toString(lroi.y2)).append(')');
                    break;
                case Roi.OVAL:
                    if (roiBounds != null) {
                        result.append(SHAPE_OVAL);
                        result.append(Integer.toString(roiBounds.x)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.y)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.width)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.height)).append(')');
                    }
                    break;
                case Roi.POLYGON:
                    if (roiPolygon != null) {
                        result.append(SHAPE_POLYGON);
                        appendLimitedPolygonInBuffer(roiPolygon, result);
                        result.append(')');
                    }
                    break;
                case Roi.POLYLINE:
                    if (roiPolygon != null) {
                        result.append(SHAPE_POLY_LINE);
                        appendLimitedPolygonInBuffer(roiPolygon, result);
                        result.append(')');
                    }
                    break;
                case Roi.COMPOSITE:
                    ShapeRoi sroi = (ShapeRoi) roi;
                    float[] shapeArray = sroi.getShapeAsArray();
                    if (shapeArray != null) {
                        result.append(SHAPE_COMPOSITE);
                        int max = MAX_POINTS * 2;
                        for (int i = 0; i < shapeArray.length; i++) {
                            if (i >= max) {
                                result.append(AND_SO_ON);
                                break;
                            }
                            result.append(Float.toString(shapeArray[i]));
                            if (i != shapeArray.length - 1) {
                                result.append(VALUE_SEPARATOR);
                            }
                        }
                        result.append(')');
                    }
                    break;
                case Roi.POINT:
                    if (roiPolygon != null) {
                        result.append(SHAPE_POINT);
                        appendLimitedPolygonInBuffer(roiPolygon, result);
                        result.append(')');
                    }
                    break;
                case CircleRoi.TYPE:
                    CircleRoi circleRoi = (CircleRoi) roi;
                    result.append(SHAPE_CIRCLE).append(Double.toString(circleRoi.getCenterX())).append(VALUE_SEPARATOR)
                            .append(Double.toString(circleRoi.getCenterZ())).append(VALUE_SEPARATOR)
                            .append(Float.toString(circleRoi.getRadius())).append(VALUE_SEPARATOR)
                            .append(circleRoi.getStringRadius()).append(')');
                    break;
                default:
                    if (roiBounds != null) {
                        result.append(SHAPE_RECTANGLE);
                        result.append(Integer.toString(roiBounds.x)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.y)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.width)).append(VALUE_SEPARATOR);
                        result.append(Integer.toString(roiBounds.height)).append(')');
                    }
                    break;
            }
        }
        return result;
    }

    /**
     * Parses a {@link String} to recognize a {@link Roi}
     * 
     * @param str The {@link String} to parse
     * @return A {@link Roi}. Can be <code>null</code>
     */
    public static Roi parseRoi(String str) {
        return parseRoi(str, '"');
    }

    /**
     * Parses a {@link String} to recognize a {@link Roi}
     * 
     * @param str The {@link String} to parse
     * @param stringCharacter The character that can be used for strings (generally &quot;, but you can also use &apos;)
     * @return A {@link Roi}. Can be <code>null</code>
     */
    public static Roi parseRoi(String str, char stringCharacter) {
        Roi roi = null;
        if (str != null) {
            str = str.trim();
            if (!str.isEmpty()) {
                int open = str.indexOf('(');
                if (open > -1) {
                    int close = str.lastIndexOf(')');
                    if (close > open) {
                        String arg = str.substring(open + 1, close).trim();
                        if (!arg.isEmpty()) {
                            if (str.startsWith(SELECTION_BUILDER)) {
                                open = arg.indexOf(stringCharacter);
                                if (open > -1) {
                                    close = arg.indexOf(stringCharacter, open + 1);
                                    if (close > open) {
                                        int split = arg.indexOf(VALUE_SEPARATOR, close);
                                        if (split > close) {
                                            String type = arg.substring(open + 1, close).trim();
                                            arg = arg.substring(split + 1).trim();
                                            Polygon polygon = parsePolygon(arg);
                                            if (polygon != null) {
                                                if (TYPE_ANGLE.equals(type)) {
                                                    roi = new PolygonRoi(polygon, Roi.ANGLE);
                                                } else if (TYPE_FREE_LINE.equals(type)) {
                                                    roi = new PolygonRoi(polygon, Roi.FREELINE);
                                                } else if (TYPE_FREE_HAND.equals(type)) {
                                                    roi = new PolygonRoi(polygon, Roi.FREEROI);
                                                } else if (TYPE_POLYLINE.equals(type)) {
                                                    roi = new PolygonRoi(polygon, Roi.POLYLINE);
                                                } else if (TYPE_POINT.equals(type)) {
                                                    roi = new PointRoi(polygon);
                                                }
                                            }
                                        }
                                    }
                                }
                            } else if (str.startsWith(LINE_BUILDER)) {
                                String[] coordinates = arg.split(VALUE_SEPARATOR);
                                if (coordinates.length == 4) {
                                    roi = new Line(Integer.parseInt(coordinates[0].trim()),
                                            Integer.parseInt(coordinates[1].trim()),
                                            Integer.parseInt(coordinates[2].trim()),
                                            Integer.parseInt(coordinates[3].trim()));
                                }
                            } else if (str.startsWith(OVAL_BUILDER)) {
                                String[] coordinates = arg.split(VALUE_SEPARATOR);
                                if (coordinates.length == 4) {
                                    roi = new OvalRoi(Integer.parseInt(coordinates[0].trim()),
                                            Integer.parseInt(coordinates[1].trim()),
                                            Integer.parseInt(coordinates[2].trim()),
                                            Integer.parseInt(coordinates[3].trim()));
                                }
                            } else if (str.startsWith(POLYGON_BUILDER)) {
                                Polygon polygon = parsePolygon(arg);
                                if (polygon != null) {
                                    roi = new PolygonRoi(polygon, Roi.POLYGON);
                                }
                            } else if (str.startsWith(RECTANGLE_BUILDER)) {
                                String[] coordinates = arg.split(VALUE_SEPARATOR);
                                if (coordinates.length == 4) {
                                    roi = new Roi(Integer.parseInt(coordinates[0].trim()),
                                            Integer.parseInt(coordinates[1].trim()),
                                            Integer.parseInt(coordinates[2].trim()),
                                            Integer.parseInt(coordinates[3].trim()));
                                }
                            } else if (str.startsWith(POINT_BUILDER)) {
                                String[] coordinates = arg.split(VALUE_SEPARATOR);
                                if (coordinates.length == 2) {
                                    roi = new PointRoi(Integer.parseInt(coordinates[0].trim()),
                                            Integer.parseInt(coordinates[1].trim()));
                                }
                            }
                        }
                    }
                }
            }
        }
        return roi;
    }

    protected static int[] parseIntArray(String arg) {
        int[] array = null;
        if (arg != null) {
            String[] coordinates = arg.split(VALUE_SEPARATOR);
            array = new int[coordinates.length];
            for (int i = 0; i < coordinates.length; i++) {
                array[i] = Integer.parseInt(coordinates[i].trim());
            }
        }
        return array;
    }

    protected static Polygon parsePolygon(String arg) {
        Polygon polygon = null;
        if (arg != null) {
            int[] x = null, y = null;
            if (arg.indexOf(ARRAY_BUILDER) > -1) {
                String[] arrays = arg.split(ARRAY_START);
                for (String str : arrays) {
                    if (!str.trim().isEmpty()) {
                        if (x == null) {
                            x = parseIntArray(str.replace(SHAPE_OPENER, ObjectUtils.EMPTY_STRING)
                                    .replace(SHAPE_CLOSER_SEPARATOR, ObjectUtils.EMPTY_STRING));
                        } else if (y == null) {
                            y = parseIntArray(str.replace(SHAPE_OPENER, ObjectUtils.EMPTY_STRING).replace(SHAPE_CLOSER,
                                    ObjectUtils.EMPTY_STRING));
                        } else {
                            x = null;
                            y = null;
                            break;
                        }
                    }
                }
            } else {
                String[] coordinates = arg.split(VALUE_SEPARATOR);
                if ((coordinates.length > 0) && (coordinates.length % 2 == 0)) {
                    int length = coordinates.length / 2;
                    x = new int[length];
                    y = new int[length];
                    for (int k = 0; k < length; k++) {
                        x[k] = Integer.parseInt(coordinates[2 * k].trim());
                        y[k] = Integer.parseInt(coordinates[2 * k + 1].trim());
                    }
                }
            }
            if ((x != null) && (y != null) && (x.length == y.length)) {
                polygon = new Polygon(x, y, x.length);
            }
        }
        return polygon;
    }

    protected static <T extends Appendable> T roiDetailsToAppendable(ImageViewer imageViewer, Roi roi, T buffer)
            throws IOException {
        T result = buffer;
        if (result != null) {
            if (roi != null) {
                if (imageViewer.getRoiManager().isInner(roi)) {
                    result.append(INSIDE_SELECTION_RUN_COMMAND).append(LINE_FEED);
                } else if (imageViewer.getRoiManager().isOuter(roi)) {
                    result.append(OUTSIDE_SELECTION_RUN_COMMAND).append(LINE_FEED);
                }
                if (imageViewer.getRoiManager().isSelectedRoi(roi)) {
                    result.append(SELECTION_RUN_COMMAND).append(LINE_FEED);
                }
            }
        }
        return result;
    }

    protected static <T extends Appendable> T roiDetailsToAppendable(IMaskedImageViewer imageViewer, Roi roi, T buffer)
            throws IOException {
        T result = buffer;
        if (result != null) {
            if (roi != null) {
                if (imageViewer.getRoiManager().isInner(roi)) {
                    result.append(INSIDE_SELECTION_RUN_COMMAND).append(LINE_FEED);
                } else if (imageViewer.getRoiManager().isOuter(roi)) {
                    result.append(OUTSIDE_SELECTION_RUN_COMMAND).append(LINE_FEED);
                }
                if (imageViewer.getRoiManager().isSelectedRoi(roi)) {
                    result.append(SELECTION_RUN_COMMAND).append(LINE_FEED);
                }
            }
        }
        return result;
    }

    /**
     * Creates a shaped selection.
     */
    public static void makeShape(float[] shapePoints, ImageViewer imageViewer) {
        if (shapePoints != null && shapePoints.length > 0) {
            imageViewer.getRoiManager().setRoi(new ShapeRoi(shapePoints));
        }
    }

    /**
     * Returns whether the default plugins that may be used by any CometeSwing application are already registered
     * 
     * @return A <code>boolean</code>
     */
    public static boolean areDefaultPluginsRegistered() {
        return (Menus.getCommands() != null) && (Menus.getCommands().containsKey(DisplayMask.COMMAND_NAME));
    }

    /**
     * Registers the default plugins that may be used by any CometeSwing application
     */
    public static void registerDefaultPlugins() {
        registerPlugin(DisplayMask.class.getName(), DisplayMask.COMMAND_NAME);
        registerPlugin(Intersection.class.getName(), Intersection.COMMAND_NAME);
        registerPlugin(XOr.class.getName(), XOr.COMMAND_NAME);
        registerPlugin(Subtraction.class.getName(), Subtraction.COMMAND_NAME);
        registerPlugin(Union.class.getName(), Union.COMMAND_NAME);
        registerPlugin(InsideValidation.class.getName(), InsideValidation.COMMAND_NAME);
        registerPlugin(OutsideValidation.class.getName(), OutsideValidation.COMMAND_NAME);
        registerPlugin(RoiSelection.class.getName(), RoiSelection.COMMAND_NAME);
        registerPlugin(LoadMaskPlugin.class.getName(), LoadMaskPlugin.COMMAND_NAME);
        registerPlugin(LoadMaskAddPlugin.class.getName(), LoadMaskAddPlugin.COMMAND_NAME);
        registerPlugin(SetSectorPlugin.class.getName(), SetSectorPlugin.COMMAND_NAME);
        registerPlugin(MakeShapePlugin.class.getName(), MakeShapePlugin.COMMAND_NAME);
        registerPlugin(MakeCirclePlugin.class.getName(), MakeCirclePlugin.COMMAND_NAME);
    }

    /**
     * Registers a plugin class in ImageJ, associating a command name to it
     * 
     * @param pluginClass The plugin class full name
     * @param commandName The command name
     */
    @SuppressWarnings("unchecked")
    public static void registerPlugin(String pluginClass, String commandName) {
        if ((pluginClass != null) && (!pluginClass.trim().isEmpty()) && (commandName != null)
                && (!commandName.trim().isEmpty()) && (Menus.getCommands() != null)
                && (!Menus.getCommands().containsKey(commandName))) {
            Menus.getCommands().put(commandName, pluginClass);
        }
    }

    /**
     * Transforms a {@link CometePlugInFilter} into an executable macro code
     * 
     * @param plugin The {@link CometePlugInFilter}
     * @param param The macro parameters
     * @return A {@link String} value
     */
    public static String plugInFilterToString(CometePlugInFilter plugin, String param) {
        String result = null;
        if (plugin != null) {
            result = plugInFilterToString(plugin.getCommandName(), param);
        }
        return result;
    }

    /**
     * Transforms a {@link CometePlugInFilter} into an executable macro code, knowing its command
     * name
     * 
     * @param plugin The {@link CometePlugInFilter} command name
     * @param param The macro parameters
     * @return A {@link String} value
     * @see CometePlugInFilter#getCommandName()
     */
    public static String plugInFilterToString(String plugin, String param) {
        String result = null;
        if (plugin != null) {
            StringBuilder buffer = new StringBuilder(RUN_STRING_BUILDER);
            buffer.append(plugin);
            buffer.append(STRING_DECORATOR);
            if (param != null) {
                buffer.append(PARAMETER_SEPARATOR);
                buffer.append(param);
                buffer.append(STRING_DECORATOR);
            }
            buffer.append(MACRO_CLOSER);
            result = buffer.toString();
        }
        return result;
    }

}
