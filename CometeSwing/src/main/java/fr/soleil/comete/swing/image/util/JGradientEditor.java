/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.IGradientEditor;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.comete.swing.Component;
import fr.soleil.comete.swing.util.CometeUtils;

/**
 * A Color gradient editor component.
 */
public class JGradientEditor extends Component
        implements ActionListener, MouseMotionListener, MouseListener, IGradientEditor {

    private static final long serialVersionUID = -1488917597824003393L;

    private static final int[] CURSOR_SHAPE_X = { -5, -2, 2, 5, 5, 2, -2, -5, -5 };
    private static final int[] CURSOR_SHAPE_Y = { 10, 15, 15, 10, -10, -15, -15, -10, 10 };

    private boolean lockStartAndEndCursors;
    private boolean editable;
    private boolean isDragging;
    private Gradient grad;
    private int[] palette;
    private int currentSel = -1;
    private double currentPos;

    // Height of the JGradientEditor
    private final int barHeight = 25;

    private final JPopupMenu popMenu;
    private final JMenuItem infoMenuItem;
    private final JMenuItem addMenuItem;
    private final JMenuItem removeMenuItem;
    private final JMenuItem changeColorMenuItem;
    private final JMenuItem resetMenuItem;
    private final JMenuItem rainbowMenuItem;

    // ----------------------------------------------
    // Contruction
    // ----------------------------------------------

    public JGradientEditor() {
        super();
        setBorder(null);
        setOpaque(true);
        setOpaque(true);
        addMouseMotionListener(this);
        addMouseListener(this);

        lockStartAndEndCursors = true;
        editable = true;
        grad = new Gradient();
        palette = grad.buildColorMap(256);

        popMenu = new JPopupMenu();

        infoMenuItem = new JMenuItem("Gradient editor");
        infoMenuItem.setEnabled(false);

        addMenuItem = new JMenuItem("Add color here");
        addMenuItem.addActionListener(this);

        removeMenuItem = new JMenuItem("Remove");
        removeMenuItem.addActionListener(this);

        changeColorMenuItem = new JMenuItem("Change color");
        changeColorMenuItem.addActionListener(this);

        resetMenuItem = new JMenuItem("Reset to default gradient");
        resetMenuItem.addActionListener(this);

        rainbowMenuItem = new JMenuItem("Reset to rainbow gradient");
        rainbowMenuItem.addActionListener(this);

        popMenu.add(infoMenuItem);
        popMenu.add(new JSeparator());
        popMenu.add(addMenuItem);
        popMenu.add(removeMenuItem);
        popMenu.add(changeColorMenuItem);
        popMenu.add(resetMenuItem);
        popMenu.add(rainbowMenuItem);

    }

    // ----------------------------------------------
    // Property
    // ----------------------------------------------

    public boolean isLockStartAndEndCursors() {
        return lockStartAndEndCursors;
    }

    public void setLockStartAndEndCursors(boolean lock) {
        this.lockStartAndEndCursors = lock;
    }

    public boolean isEditable() {
        Dimension d = getSize();
        return editable && (d.width >= 256);
    }

    public void setEditable(boolean b) {
        editable = b;
    }

    @Override
    public void setGradient(Gradient g) {
        if (g != null) {
            grad = g;
            palette = g.buildColorMap(256);
        }
    }

    @Override
    public Gradient getGradient() {
        return grad;
    }

    public void setRainbowGradient() {
        grad.buildRainbowGradient();
        palette = grad.buildColorMap(256);
        repaint();
    }

    public void setDefaultGradient() {
        grad.buildMonochromeGradient();
        palette = grad.buildColorMap(256);
        repaint();
    }

    public void invertGradient() {
        grad.invertGradient();
        palette = grad.buildColorMap(256);
        repaint();
    }

    // --------------------------------------------------------
    // Overrides
    // --------------------------------------------------------

    @Override
    public void paint(Graphics g) {
        Dimension d = getSize();

        if (d.width > 0) {
            if (isOpaque()) {
                g.setColor(getBackground());
                g.fillRect(0, 0, d.width, d.height);
            }
            if (d.width < 256) {

                // Minimal view
                int startY = (d.height - barHeight) / 2;

                double r = 256.0 / d.width;
                for (int i = 0; i < d.width; i++) {
                    int id = (int) (r * i);

                    if (id <= 255) {
                        g.setColor(new Color(palette[id]));
                    } else {
                        g.setColor(new Color(palette[255]));
                    }

                    g.drawLine(i, startY, i, startY + barHeight);
                }

            } else {
                // Normal view
                int startX = (d.width - 256) / 2;
                int startY = (d.height - barHeight) / 2;

                // Draw gradient
                for (int i = 0; i < 256; i++) {
                    g.setColor(new Color(palette[i]));
                    g.drawLine(startX + i, startY, startX + i, startY + barHeight);
                }

                // Draw entries
                if (editable) {
                    for (int i = 0; i < grad.getEntryNumber(); i++) {
                        Color c = ColorTool.getColor(grad.getColorAt(i));
                        double p = grad.getPosAt(i);
                        int xc = startX + (int) (p * 256.0);
                        int yc = startY + barHeight / 2;
                        g.translate(xc, yc);
                        paintCursor(g, c);
                        g.translate(-xc, -yc);
                    }
                }
            }
        }
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(300, 50);
    }

    @Override
    public Dimension getPreferredSize() {
        return getMinimumSize();
    }

    // ----------------------------------------------------------
    // Mouse Listeners
    // ----------------------------------------------------------

    @Override
    public void mouseDragged(MouseEvent e) {

        if (isDragging == true) {

            Dimension d = getSize();
            int xe = e.getX();
            int startX = (d.width - 256) / 2;

            double currentPos = ((xe - startX) / 256.0);
            if (currentPos < 0.0) {
                currentPos = 0.0;
            }
            if (currentPos > 1.0) {
                currentPos = 1.0;
            }

            setCurrentPos(currentPos);

            palette = grad.buildColorMap(256);
            repaint();

        }

    }

    protected void setCurrentPos(double pos) {
        int id = currentSel;
        if ((!isLockStartAndEndCursors()) || ((id > 0) && (id < grad.getEntryNumber() - 1))) {
            if (pos >= 0.0 && pos <= 1.0) {
                if (id == 0) {
                    if (pos < grad.getPosAt(id + 1)) {
                        grad.setPosAt(id, currentPos);
                        this.currentPos = pos;
                    }
                } else if (id == grad.getEntryNumber() - 1) {
                    if (pos > grad.getPosAt(id - 1)) {
                        grad.setPosAt(id, currentPos);
                        this.currentPos = pos;
                    }
                } else {
                    if (pos < grad.getPosAt(id + 1) && pos > grad.getPosAt(id - 1)) {
                        grad.setPosAt(id, currentPos);
                        this.currentPos = pos;
                    }
                }
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (isEditable() && isInsideBar(e)) {
            if (e.getClickCount() == 2) {
                if (foundEntry(e)) {
                    Color nColor = JColorChooser.showDialog(this, "Choose gradient color #" + (currentSel + 1),
                            ColorTool.getColor(grad.getColorAt(currentSel)));
                    if (nColor != null) {
                        grad.setColorAt(currentSel, ColorTool.getCometeColor(nColor));
                        palette = grad.buildColorMap(256);
                        repaint();
                    }
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        isDragging = false;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (isEditable() && isInsideBar(e)) {
            if (foundEntry(e)) {

                if (e.getButton() == MouseEvent.BUTTON3) {
                    addMenuItem.setVisible(false);
                    removeMenuItem.setVisible(currentSel > 0 && currentSel < (grad.getEntryNumber() - 1));
                    changeColorMenuItem.setVisible(true);
                    popMenu.show(this, e.getX(), e.getY());
                }

                if (e.getButton() == MouseEvent.BUTTON1) {
                    isDragging = true;
                }

            } else {

                if (e.getButton() == MouseEvent.BUTTON3) {
                    addMenuItem.setVisible(true);
                    removeMenuItem.setVisible(false);
                    changeColorMenuItem.setVisible(false);
                    popMenu.show(this, e.getX(), e.getY());
                }

            }
        }
    }

    // ----------------------------------------------------------
    // Action Listener
    // ----------------------------------------------------------

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (evt.getSource() == changeColorMenuItem) {
            if (currentSel >= 0 && currentSel < grad.getEntryNumber()) {
                Color nColor = JColorChooser.showDialog(this, "Choose gradient color #" + (currentSel + 1),
                        ColorTool.getColor(grad.getColorAt(currentSel)));
                if (nColor != null) {
                    grad.setColorAt(currentSel, ColorTool.getCometeColor(nColor));
                    palette = grad.buildColorMap(256);
                    repaint();
                }
            }
        } else if (evt.getSource() == resetMenuItem) {
            setDefaultGradient();
        } else if (evt.getSource() == rainbowMenuItem) {
            setRainbowGradient();
        } else if (evt.getSource() == addMenuItem) {
            Color nColor = JColorChooser.showDialog(this, "Choose new color", Color.black);
            if (nColor != null) {
                grad.addEntry(ColorTool.getCometeColor(nColor), currentPos);
                palette = grad.buildColorMap(256);
                repaint();
            }
        } else if (evt.getSource() == removeMenuItem) {
            grad.removeEntry(currentSel);
            palette = grad.buildColorMap(256);
            repaint();
        }
    }

    // ------------------------------------------------------------
    // Static Help public function
    // ------------------------------------------------------------

    /**
     * Display the Gradient editor view.
     * 
     * @param parent
     *            parent dialog
     * @param g
     *            Gradient to be edited (can be null)
     * @return New Gradient , null when canceled
     */
    static public Gradient showDialog(JDialog parent, Gradient g) {

        JDialog gDialog;
        gDialog = new JDialog(parent, true);
        return showDefDialog(gDialog, g);

    }

    /**
     * Display the Gradient editor view.
     * 
     * @param parent
     *            parent frame
     * @param g
     *            Gradient to be edited (can be null)
     * @return New Gradient , null when canceled
     */
    static public Gradient showDialog(JFrame parent, Gradient g) {

        JDialog gDialog;
        gDialog = new JDialog(parent, true);
        return showDefDialog(gDialog, g);

    }

    /**
     * Display the Gradient editor view.
     * 
     * @param parent
     *            parent componenent
     * @param g
     *            Gradient to be edited (can be null)
     * @return New Gradient , null when canceled
     */
    static public Gradient showDialog(JComponent parent, Gradient g) {
        JDialog gDialog;
        Window parentWindow = CometeUtils.getWindowForComponent(parent);
        gDialog = new JDialog(parentWindow);
        gDialog.setModal(true);
        return showDefDialog(gDialog, parent, g);
    }

    // ----------------------------------------------------------
    // Private stuff
    // ----------------------------------------------------------

    private Color transformColor(Color c, int offset) {
        int nr = (c.getRed() + offset);
        int ng = (c.getBlue() + offset);
        int nb = (c.getGreen() + offset);
        if (nr > 255) {
            nr = 255;
        }
        if (ng > 255) {
            ng = 255;
        }
        if (nb > 255) {
            nb = 255;
        }
        if (nr < 0) {
            nr = 0;
        }
        if (ng < 0) {
            ng = 0;
        }
        if (nb < 0) {
            nb = 0;
        }
        return new Color(nr, ng, nb);
    }

    private boolean isLightColor(Color c) {
        int r = c.getRed();
        int g = c.getGreen();
        int b = c.getBlue();
        return Math.sqrt(r * r + g * g + b * b) > 180.0;
    }

    private void paintCursor(Graphics g, Color c) {

        g.setColor(c);
        g.fillPolygon(CURSOR_SHAPE_X, CURSOR_SHAPE_Y, 9);

        if (isLightColor(c)) {
            g.setColor(transformColor(c, -128));
        } else {
            g.setColor(transformColor(c, 128));
        }

        g.drawPolygon(CURSOR_SHAPE_X, CURSOR_SHAPE_Y, 9);

    }

    private boolean isInsideBar(MouseEvent e) {
        Dimension d = getSize();
        int xe = e.getX();
        int ye = e.getY();
        int startX = (d.width - 256) / 2;
        int startY = (d.height - barHeight) / 2;
        return (xe >= (startX - 5) && xe <= (startX + 261) && ye >= startY && ye <= (startY + barHeight));
    }

    private boolean foundEntry(MouseEvent e) {
        boolean found = false;
        Dimension d = getSize();
        int xe = e.getX();
        int ye = e.getY();
        int startX = (d.width - 256) / 2;
        int startY = (d.height - barHeight) / 2;
        int i = 0;
        while (i < grad.getEntryNumber() && !found) {
            double p = grad.getPosAt(i);
            int xc = startX + (int) (p * 256.0);
            int yc = startY + barHeight / 2;
            found = (xe > xc - 8) && (xe < xc + 8) && (ye > yc - 15) && (ye < yc + 15);
            if (!found) {
                i++;
            }
        }
        if (found) {
            currentSel = i;
        } else {
            currentSel = -1;
        }
        currentPos = ((xe - startX) / 256.0);
        if (currentPos < 0.0) {
            currentPos = 0.0;
        }
        if (currentPos > 1.0) {
            currentPos = 1.0;
        }
        return found;
    }

    private static Gradient showDefDialog(JDialog gDialog, Gradient gOrg) {
        return showDefDialog(gDialog, gDialog.getOwner(), gOrg);
    }

    private static Gradient showDefDialog(JDialog gDialog, java.awt.Component locationReference, Gradient gOrg) {

        // Construct editor panel
        EditorPanel gradEditor = new EditorPanel();
        if (gOrg != null) {
            Gradient g = gOrg.clone();
            gradEditor.setGradient(g);
        }

        gDialog.setResizable(false);
        gDialog.setContentPane(gradEditor);
        gDialog.setTitle("Colormap gradient editor");

        // Placement
        gDialog.pack();
        gDialog.setLocationRelativeTo(locationReference);

        gDialog.setVisible(true);
        gDialog.dispose();

        if (gradEditor.getDlgRetValue() == 0) {
            return null;
        } else {
            return gradEditor.getGradient();
        }

    }

    /** Main test function. */
    public static void main(String[] args) {
        final Gradient d = new Gradient();
        d.buildRainbowGradient();
        showDialog((JFrame) null, d);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /** A panel for the grad editor. */
    protected static class EditorPanel extends JPanel implements ActionListener {

        private static final long serialVersionUID = -7817630611443641705L;

        private final JButton bwButton;
        private final JButton colorButton;
        private final JButton invertButton;
        private final JButton helpButton;
        private final JButton okButton;
        private final JButton cancelButton;
        private final JGradientEditor gradEditor;

        private static final String HELP_STRING = "Cursor color can be changed by double clicking on it.\nCursors can be moved by dragging them.\nColor entry can be added or removed by right clicking on the gradient.";

        private int dlgRetValue = 0;

        /**
         * Construction
         */
        public EditorPanel() {
            super(new BorderLayout());
            // ----------------------------------------------------------
            JPanel gPanel = new JPanel();
            gPanel.setLayout(new BorderLayout());
            gPanel.setBorder(BorderFactory.createEtchedBorder());

            gradEditor = new JGradientEditor();
            gPanel.add(gradEditor, BorderLayout.CENTER);

            JPanel cPanel = new JPanel();
            cPanel.setLayout(new FlowLayout());

            bwButton = new JButton("Monochrome");
            bwButton.addActionListener(this);
            cPanel.add(bwButton);

            colorButton = new JButton("Color");
            colorButton.addActionListener(this);
            cPanel.add(colorButton);

            invertButton = new JButton("Invert");
            invertButton.addActionListener(this);
            cPanel.add(invertButton);

            gPanel.add(cPanel, BorderLayout.SOUTH);

            add(gPanel, BorderLayout.CENTER);

            // ----------------------------------------------------------
            FlowLayout fl = new FlowLayout();
            fl.setAlignment(FlowLayout.RIGHT);
            JPanel panelButton = new JPanel(fl);

            helpButton = new JButton("Help");
            helpButton.addActionListener(this);

            okButton = new JButton("Apply");
            okButton.addActionListener(this);

            cancelButton = new JButton("Dismiss");
            cancelButton.addActionListener(this);

            panelButton.add(helpButton);
            panelButton.add(okButton);
            panelButton.add(cancelButton);

            add(panelButton, BorderLayout.SOUTH);
        }

        public int getDlgRetValue() {
            return dlgRetValue;
        }

        // ----------------------------------------------
        // Property
        // ----------------------------------------------
        /**
         * Sets the Gradient editable. Must be fully sized !!!
         * 
         * @param b
         *            editable value
         */
        public void setEditable(boolean b) {
            gradEditor.setEditable(b);
        }

        public boolean isEditable() {
            return gradEditor.isEditable();
        }

        public void setGradient(Gradient g) {
            gradEditor.setGradient(g);
        }

        public Gradient getGradient() {
            return gradEditor.getGradient();
        }

        // ----------------------------------------------------------
        // Action Listener
        // ----------------------------------------------------------
        @Override
        public void actionPerformed(ActionEvent evt) {
            Object src = evt.getSource();
            if (src == okButton) {
                dlgRetValue = 1;
                getRootPane().getParent().setVisible(false);
            } else if (src == cancelButton) {
                dlgRetValue = 0;
                getRootPane().getParent().setVisible(false);
            } else if (src == bwButton) {
                gradEditor.setDefaultGradient();
            } else if (src == colorButton) {
                gradEditor.setRainbowGradient();
            } else if (src == invertButton) {
                gradEditor.invertGradient();
            } else if (src == helpButton) {
                JOptionPane.showMessageDialog(helpButton, HELP_STRING, "Help on Gradient Editor",
                        JOptionPane.INFORMATION_MESSAGE);
            }

        }

    }

}
