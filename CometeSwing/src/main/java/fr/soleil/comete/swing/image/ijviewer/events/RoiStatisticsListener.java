/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.events;

import java.lang.ref.WeakReference;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.components.IStatisticsComponent;
import fr.soleil.lib.project.ObjectUtils;

public class RoiStatisticsListener implements IRoiManagerListener, IRoiSelectionListener {

    protected final WeakReference<ImageViewer> imageViewerRef;
    protected final WeakReference<IStatisticsComponent> statisticsComponentRef;

    public RoiStatisticsListener(ImageViewer imageViewer, IStatisticsComponent statisticsComponent) {
        imageViewerRef = imageViewer == null ? null : new WeakReference<>(imageViewer);
        statisticsComponentRef = statisticsComponent == null ? null : new WeakReference<>(statisticsComponent);
    }

    @Override
    public void roiModeChanged(RoiEvent event) {
        // not managed
    }

    @Override
    public void roiAdded(RoiEvent event) {
        treatEvent(event);
    }

    @Override
    public void roiChanged(RoiEvent event) {
        treatEvent(event);
    }

    @Override
    public void roiDeleted(RoiEvent event) {
        treatEvent(event);
    }

    @Override
    public void roiSelected(RoiEvent event) {
        treatEvent(event);
    }

    @Override
    public void roiDeselected(RoiEvent event) {
        treatEvent(event);
    }

    protected void treatEvent(RoiEvent event) {
        if (event != null) {
            IStatisticsComponent statisticsComponent = ObjectUtils.recoverObject(statisticsComponentRef);
            if (statisticsComponent != null) {
                statisticsComponent
                        .computeStatisticsAndDisplayRoiInformation(ObjectUtils.recoverObject(imageViewerRef));
            }
        }
    }

}
