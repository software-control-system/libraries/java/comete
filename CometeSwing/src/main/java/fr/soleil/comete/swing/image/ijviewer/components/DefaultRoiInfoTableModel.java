/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.SwingUtilities;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.events.IJViewerEvent;
import fr.soleil.comete.swing.image.ijviewer.events.RoiEvent;
import fr.soleil.lib.project.ObjectUtils;
import ij.gui.Line;
import ij.gui.Roi;
import ij.gui.RoiHack;

/**
 * A default implementation.
 * 
 * @author MAINGUY, Rapha&euml;l GIRARDOT
 */
public class DefaultRoiInfoTableModel extends ARoiInfoTableModel {

    private static final long serialVersionUID = -6147021115908061174L;

    protected List<Column> columnList;
    protected Map<Column, String> columnNames;

    public DefaultRoiInfoTableModel(ImageViewer viewer) {
        this(viewer, Column.values());
    }

    public DefaultRoiInfoTableModel(ImageViewer viewer, Column... columns) {
        super(viewer);
        columnList = new ArrayList<>(columns.length);
        columnNames = new EnumMap<>(Column.class);
        ResourceBundle messages = ResourceBundle.getBundle("fr.soleil.comete.swing.image.ijviewer.components.messages");
        for (Column column : columns) {
            columnList.add(column);
            columnNames.put(column, messages.getString("RoiTable.ColumnTitle." + column));
        }
    }

    @Override
    public int getColumnCount() {
        return columnList.size();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames.get(columnList.get(column));
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class<?> columnClass;
        Column column = columnList.get(columnIndex);
        if (column == null) {
            columnClass = Object.class;
        } else {
            switch (column) {
                case NAME:
                    columnClass = String.class;
                    break;
                case TYPE:
                    columnClass = String.class;
                    break;
                case X:
                case Y:
                case WIDTH:
                case HEIGHT:
                    columnClass = Integer.class;
                    break;
                case REAL_WIDTH:
                case REAL_HEIGHT:
                    columnClass = Double.class;
                    break;
                default:
                    columnClass = Object.class;
                    break;
            }
        }
        return columnClass;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object returnValue = null;
        Roi roi = data.get(rowIndex);
        Column column = columnList.get(columnIndex);
        switch (column) {
            case NAME:
                returnValue = roi.getName();
                break;
            case TYPE:
                returnValue = roi.getTypeAsString();
                break;
            case X:
                returnValue = Integer.valueOf(roi.getBounds().x);
                break;
            case Y:
                returnValue = Integer.valueOf(roi.getBounds().y);
                break;
            case WIDTH:
                returnValue = Integer.valueOf(roi.getBounds().width);
                break;
            case HEIGHT:
                returnValue = Integer.valueOf(roi.getBounds().height);
                break;
            case REAL_WIDTH:
                returnValue = Double.valueOf(roi.getBounds().getWidth() * pixelSize);
                break;
            case REAL_HEIGHT:
                returnValue = Double.valueOf(roi.getBounds().getHeight() * pixelSize);
                break;
            case LINE_LENGTH:
                if (roi.getType() == Roi.LINE) {
                    returnValue = Double.valueOf(((Line) roi).getLength());
                } else {
                    returnValue = ObjectUtils.EMPTY_STRING;
                }
                break;
            case LINE_REAL_LENGTH:
                if (roi.getType() == Roi.LINE) {
                    returnValue = Double.valueOf(((Line) roi).getLength() * pixelSize);
                } else {
                    returnValue = ObjectUtils.EMPTY_STRING;
                }
                break;
        }
        return returnValue;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        boolean editable;
        Roi roi = data.get(rowIndex);
        Column column = columnList.get(columnIndex);
        if ((column == null) || (roi == null)) {
            editable = false;
        } else {
            switch (column) {
                case NAME:
                    ImageViewer viewer = ObjectUtils.recoverObject(viewerRef);
                    if (viewer == null) {
                        editable = true;
                    } else {
                        editable = viewer.isEditableRoiName(roi);
                    }
                    break;
                case X:
                case Y:
                    editable = true;
                    break;
                case WIDTH:
                case HEIGHT:
                    editable = (roi != null) && roi.getType() == Roi.RECTANGLE;
                    break;
                case LINE_LENGTH:
                    editable = (roi.getType() == Roi.LINE);
                    break;
                default:
                    editable = false;
                    break;
            }
        }
        return editable;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        boolean roiUpdated = false;
        Roi roi = data.get(rowIndex);
        Column column = columnList.get(columnIndex);
        ImageViewer viewer = ObjectUtils.recoverObject(viewerRef);
        if ((column != null) && (roi != null)) {
            switch (column) {
                case NAME:
                    String name = (String) aValue;
                    roi.setName(name == null || name.trim().isEmpty() ? ObjectUtils.EMPTY_STRING : name);
                    roiUpdated = true;
                    break;
                case X:
                    Integer xObj = (Integer) aValue;
                    if (xObj != null) {
                        int y = roi.getBounds().y;
                        if (roi.getType() == Roi.LINE) {
                            // RoiHack.setLocation is more precise than roi.setLocation, which may drift by 1.
                            RoiHack.setLocation((Line) roi, xObj.intValue(), y);
                        } else {
                            roi.setLocation(xObj.intValue(), y);
                        }
                        roiUpdated = true;
                    }
                    break;
                case Y:
                    Integer yObj = (Integer) aValue;
                    if (yObj != null) {
                        int x = roi.getBounds().x;
                        if (roi.getType() == Roi.LINE) {
                            // RoiHack.setLocation is more precise than roi.setLocation, which may drift by 1.
                            RoiHack.setLocation((Line) roi, x, yObj.intValue());
                        } else {
                            roi.setLocation(x, yObj.intValue());
                        }
                        roiUpdated = true;
                    }
                    break;
                case WIDTH:
                    Integer width = (Integer) aValue;
                    if (width != null) {
                        roiUpdated = RoiHack.resizeRoi(roi, width.intValue(), roi.getBounds().height);
                    }
                    break;
                case HEIGHT:
                    Integer height = (Integer) aValue;
                    if (height != null) {
                        roiUpdated = RoiHack.resizeRoi(roi, roi.getBounds().width, height.intValue());
                    }
                    break;
                case LINE_LENGTH:
                    double length;
                    if (aValue instanceof Number) {
                        length = ((Number) aValue).doubleValue();
                    } else {
                        String str = String.valueOf(aValue);
                        try {
                            length = Double.parseDouble(str.trim());
                        } catch (Exception e) {
                            length = Double.NaN;
                        }
                        roiUpdated = RoiHack.setLength((Line) roi, length, true);
                    }
                    break;
                default:
                    // nothing to do;
                    break;
            }
        }
        if (roiUpdated) {
            if (viewer != null) {
                viewer.repaint();
                viewer.getImageCanvas().roiChanged(roi);
            }
            // Invoke later to ensure updating data after all Roi events
            SwingUtilities.invokeLater(() -> {
                fireTableDataChanged();
            });
        }
    }

    protected Object getColumnId(int modelIndex) {
        return columnList.get(modelIndex);
    }

    @Override
    public void pixelSizeChanged(IJViewerEvent event) {
        pixelSize = event.getSource().getPixelSize();
        fireTableDataChanged();
    }

    @Override
    public void roiModeChanged(RoiEvent event) {
        // nop
    }

    @Override
    public void mouseChanged(IJViewerEvent event) {
        // nop
    }

    @Override
    public void mouseClicked(IJViewerEvent event) {
        // nop
    }

    @Override
    public void valueChanged(IJViewerEvent event) {
        // nop
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public enum Column {
        NAME, TYPE, X, Y, WIDTH, HEIGHT, REAL_WIDTH, REAL_HEIGHT, LINE_LENGTH, LINE_REAL_LENGTH;
    }

}
