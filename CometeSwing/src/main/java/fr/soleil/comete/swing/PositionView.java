/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.data.target.IConvertedPositionsTarget;
import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.comete.definition.util.MatrixPositionConvertor;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.IFormatableTarget;
import fr.soleil.data.target.IPositionTarget;
import fr.soleil.data.target.IStackShapeTarget;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.FullPositionInformation;
import fr.soleil.data.target.information.SubPositionInformation;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.lib.project.ObjectUtils;

/**
 * This View handles the stack position sliders
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class PositionView extends AbstractPanel
        implements IBooleanTarget, IPositionTarget, IFormatableTarget, IStackShapeTarget, IConvertedPositionsTarget {

    private static final long serialVersionUID = 6993162328599218716L;

    protected static final Insets DEFAULT_INSETS = new Insets(2, 2, 2, 2);
    protected static final String DEFAULT_TITLE = "Stack:";
    protected static final ImageIcon DEFAULT_STACK_ICON = new ImageIcon(
            PositionView.class.getResource("/com/famfamfam/silk/page_white_stack.png"));

    public static final String DEFAULT_FORMAT = "%.2f";

    protected final Map<Integer, IdentifiedSlider> sliderMap;
    protected final Map<Integer, JLabel> labelMap;
    protected final Map<Integer, ChangeListener> sliderListeners;

    protected final TargetLoadingPanel<JComponent> loadingPanel;
    protected final JPanel sliderPanel;
    protected final JScrollPane sliderView;
    protected final JLabel titleLabel;
    protected String slidersTitle;
    protected IValueConvertor[] userSetConvertors;
    protected IValueConvertor[] adaptedConvertors;

    protected boolean sliderEnabled;

    protected final TargetDelegate targetDelegate;
    protected String format;
    protected boolean continuousSend;
    protected int majorTicksPerSlider;
    protected int[] lastShape;
    protected boolean notifyPositionChangedOnShapeChanged;
    protected boolean autoStopLoading;

    public PositionView() {
        super();
        setLayout(new BorderLayout());
        majorTicksPerSlider = 10;
        continuousSend = false;
        format = DEFAULT_FORMAT;
        targetDelegate = new TargetDelegate();
        // use TreeMap for SliderMap to keep trace of the dimension size
        sliderMap = new TreeMap<>();
        labelMap = new HashMap<>();
        userSetConvertors = null;
        adaptedConvertors = null;
        sliderListeners = new HashMap<>();
        sliderPanel = new JPanel(new GridBagLayout());
        sliderView = new JScrollPane(sliderPanel);
        sliderView.setBorder(null);
        slidersTitle = DEFAULT_TITLE;
        titleLabel = new JLabel(slidersTitle, JLabel.CENTER);
        setTitleIcon(DEFAULT_STACK_ICON);
        add(titleLabel, BorderLayout.NORTH);
        loadingPanel = new TargetLoadingPanel<JComponent>(sliderView);
        sliderEnabled = true;
        lastShape = null;
        notifyPositionChangedOnShapeChanged = true;
        autoStopLoading = false;
        add(loadingPanel, BorderLayout.CENTER);
    }

    /**
     * Returns the number of major ticks per slider
     * 
     * @return An <code>int</code>
     */
    public int getMajorTicksPerSlider() {
        return majorTicksPerSlider;
    }

    /**
     * Sets the number of major ticks per slider
     * 
     * @param majorTicksPerSlider The desired number of major ticks per slider
     */
    public void setMajorTicksPerSlider(int majorTicksPerSlider) {
        if (majorTicksPerSlider < 2) {
            majorTicksPerSlider = 2;
        }
        if (this.majorTicksPerSlider != majorTicksPerSlider) {
            this.majorTicksPerSlider = majorTicksPerSlider;
            synchronized (sliderMap) {
                updateFromLastShapeNoLock();
            }
        }
    }

    /**
     * Returns whether sliders will always send update event, even while editing data
     * 
     * @return A <code>boolean</code> value. <code>FALSE</code> by default (i.e. sliders wait for
     *         data editing end)
     * @see Slider#isEditingData()
     * @see #setContinuousSend(boolean)
     */
    public boolean isContinuousSend() {
        return continuousSend;
    }

    /**
     * Sets whether sliders should always send update event, even while editing data
     * 
     * @param continuousSend A <code>boolean</code> value. If <code>TRUE</code>, sliders will always
     *            send update even when cursor is moving. Otherwise, they will wait for data editing
     *            end before sending event
     * @see Slider#isEditingData()
     * @see #isContinuousSend()
     */
    public void setContinuousSend(boolean continuousSend) {
        this.continuousSend = continuousSend;
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public void setFormat(String format) {
        if (!ObjectUtils.sameObject(format, this.format)) {
            this.format = format;
            synchronized (sliderMap) {
                for (IdentifiedSlider slider : sliderMap.values()) {
                    slider.setFormat(format);
                }
            }
        }
    }

    public void setSliderViewEnabled(boolean enabled) {
        sliderEnabled = enabled;
        synchronized (sliderMap) {
            for (Integer key : sliderMap.keySet()) {
                sliderMap.get(key).setEnabled(sliderEnabled);
            }
        }
    }

    public boolean isNotifyPositionChangedOnShapeChanged() {
        return notifyPositionChangedOnShapeChanged;
    }

    public void setNotifyPositionChangedOnShapeChanged(boolean notifyPositionChangedOnShapeChanged) {
        this.notifyPositionChangedOnShapeChanged = notifyPositionChangedOnShapeChanged;
    }

    public boolean isAutoStopLoading() {
        return autoStopLoading;
    }

    public void setAutoStopLoading(boolean autoStopLoading) {
        this.autoStopLoading = autoStopLoading;
    }

    @Override
    public void setValueConvertors(IValueConvertor[] valueConvertors) {
        synchronized (sliderMap) {
            this.userSetConvertors = valueConvertors;
            updateConvertors(sliderMap.size());
            for (Integer dim : sliderMap.keySet()) {
                sliderMap.get(dim).setValueConvertor(adaptedConvertors[dim]);
            }
            new Thread("Asynchronous revalidation of " + getClass().getName()) {
                @Override
                public void run() {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            recursiveRevalidate(sliderView);
                        }
                    });
                }
            }.start();
        }
    }

    @Override
    public IValueConvertor[] getValueConvertors() {
        return userSetConvertors;
    }

    protected void recursiveRevalidate(JComponent comp) {
        if (comp != null) {
            comp.revalidate();
            Container parent = comp.getParent();
            if (parent instanceof JComponent) {
                recursiveRevalidate(((JComponent) parent));
            }
        }
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setCometeBackground(color);
        Color bg = ColorTool.getColor(color);
        setSliderViewBackGround(bg);
        setSliderViewScrollColor(bg);
    }

    /**
     * Sets the background of the slider view
     * 
     * @param bg The background to set
     */
    public void setSliderViewBackGround(Color bg) {
        sliderPanel.setBackground(bg);
        sliderView.setBackground(bg);
        titleLabel.setOpaque(bg != null);
        titleLabel.setBackground(bg);
        loadingPanel.setBackground(bg);
        synchronized (sliderMap) {
            for (IdentifiedSlider slider : sliderMap.values()) {
                slider.setBackground(bg);
            }
        }
    }

    /**
     * Sets the slider view scrollbar's background
     * 
     * @param bg The background to set
     */
    public void setSliderViewScrollColor(Color bg) {
        sliderView.getHorizontalScrollBar().setBackground(bg);
        sliderView.getVerticalScrollBar().setBackground(bg);
    }

    /**
     * Returns the title that is displayed above sliders
     * 
     * @return A {@link String}
     */
    public String getTitle() {
        return slidersTitle;
    }

    /**
     * Sets the title that is displayed above sliders
     * 
     * @param title The title to set (default: "Stack:"). If <code>null</code>, the default value is
     *            restored.
     */
    public void setTitle(String title) {
        if (title == null) {
            title = DEFAULT_TITLE;
        }
        slidersTitle = title;
        titleLabel.setText(slidersTitle);
    }

    /**
     * Returns the {@link Icon} displayed above sliders
     * 
     * @return An {@link Icon}
     */
    public Icon getTitleIcon() {
        return titleLabel.getIcon();
    }

    /**
     * Sets the {@link Icon} that should be displayed above sliders
     * 
     * @param titleIcon The {@link Icon} to set
     */
    public void setTitleIcon(Icon titleIcon) {
        titleLabel.setIcon(titleIcon);
    }

    /**
     * Returns horizontal alignment of the text displayed above sliders
     * 
     * @return An <code>int</code>
     * @see SwingConstants#LEFT
     * @see SwingConstants#CENTER
     * @see SwingConstants#RIGHT
     */
    public int getTitleAlignment() {
        return titleLabel.getHorizontalAlignment();
    }

    /**
     * Sets the horizontal alignment of the text displayed above sliders
     * 
     * @param alignment The alignment to set
     * @see SwingConstants#LEFT
     * @see SwingConstants#CENTER
     * @see SwingConstants#RIGHT
     */
    public void setTitleAlignment(int alignment) {
        titleLabel.setHorizontalAlignment(alignment);
    }

    /**
     * Returns the target that can be notified for data loading. This target is able to update the
     * slider view. This is useful if you want to notify users for some data loading.
     * 
     * @return An {@link IBooleanTarget}.
     */
    public IBooleanTarget getLoadingTarget() {
        return loadingPanel;
    }

    /**
     * Sets the text to display when a data loading occurs
     * 
     * @param text The text to set
     */
    public void setLoadingText(String text) {
        loadingPanel.setLoadingText(text);
    }

    /**
     * Sets the {@link Icon} to display when a data loading occurs
     * 
     * @param icon The {@link Icon} to set
     */
    public void setLoadingIcon(Icon icon) {
        loadingPanel.setLoadingIcon(icon);
    }

    public int[] getStackShape() {
        int[] shape = lastShape;
        if (shape != null) {
            shape = shape.clone();
        }
        return shape;
    }

    @Override
    public void setStackShape(int[] shape) {
        synchronized (sliderMap) {
            if (!ObjectUtils.sameObject(shape, lastShape)) {
                lastShape = shape;
                updateFromLastShapeNoLock();
            }
        }
        if (isAutoStopLoading()) {
            // stack shape is updated: nothing more to load
            loadingPanel.setLoading(false);
        }
    }

    /**
     * Updates the sliders to adapt to last shape
     */
    protected void updateFromLastShapeNoLock() {
        if (lastShape == null) {
            adaptedConvertors = null;
            ArrayList<Integer> toRemove = new ArrayList<Integer>();
            toRemove.addAll(sliderMap.keySet());
            for (Integer dimIndex : toRemove) {
                cleanSlider(dimIndex);
            }
            toRemove.clear();
            if (isNotifyPositionChangedOnShapeChanged()) {
                notifyFullPositionChanged(null);
            }
        } else {
            updateConvertors(lastShape.length);
            int sliderCount = sliderMap.size();
            int updateLength = Math.min(lastShape.length, sliderCount);
            // update existing sliders
            for (int i = 0; i < updateLength; i++) {
                updateSlider(lastShape[i], i, lastShape.length);
            }
            // remove useless sliders
            for (int i = lastShape.length; i < sliderCount; i++) {
                cleanSlider(i);
            }
            // add missing sliders
            for (int i = sliderCount; i < lastShape.length; i++) {
                buildSlider(lastShape[i], i, lastShape.length);
            }
            if (isNotifyPositionChangedOnShapeChanged()) {
                notifyFullPositionChanged(new int[lastShape.length]);
            }
        }
        sliderPanel.revalidate();
        sliderPanel.repaint();
    }

    // recalculates the adaptedConvertors
    protected void updateConvertors(int length) {
        adaptedConvertors = new IValueConvertor[length];
        if (userSetConvertors != null) {
            if (userSetConvertors.length > length) {
                System.arraycopy(userSetConvertors, userSetConvertors.length - length, adaptedConvertors, 0, length);
            } else {
                System.arraycopy(userSetConvertors, 0, adaptedConvertors, length - userSetConvertors.length,
                        userSetConvertors.length);
            }
        }
    }

    protected void afterSliderCreation(IdentifiedSlider slider, int dimSize, int dimIndex, int shapeLength) {
        slider.setFormatOnlyConvertedValue(true);
        slider.setMinimumDouble(0);
        slider.setMaximumDouble(0);
        slider.setPaintLabels(true);
        slider.setFormat(format);
        slider.setPreferedMajorTickColor(Color.BLACK);
        slider.setAutoScale(true);
        slider.setMaximumTickCount(majorTicksPerSlider);
        sliderMap.put(dimIndex, slider);
        JLabel sliderTitle = new JLabel();
        labelMap.put(dimIndex, sliderTitle);
        updateSlider(dimSize, dimIndex, shapeLength);
        // listen to the slider to warn for a position change
        ChangeListener sliderListener = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if ((e != null) && (e.getSource() instanceof IdentifiedSlider)) {
                    final IdentifiedSlider slider = (IdentifiedSlider) e.getSource();
                    if (isContinuousSend() || (!slider.isEditingData())) {
                        new Thread("Slider " + slider.getDimIndex() + " changed") {
                            @Override
                            public void run() {
                                SwingUtilities.invokeLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        notifyChange(slider);
                                    }
                                });
                            }
                        }.start();
                    }
                }
            }
        };
        slider.addChangeListener(sliderListener);
        sliderListeners.put(dimIndex, sliderListener);
        GridBagConstraints labelConstraints = new GridBagConstraints();
        labelConstraints.fill = GridBagConstraints.HORIZONTAL;
        labelConstraints.gridx = 0;
        labelConstraints.gridy = dimIndex;
        labelConstraints.weightx = 0;
        labelConstraints.weighty = 0;
        labelConstraints.insets = DEFAULT_INSETS;
        sliderPanel.add(sliderTitle, labelConstraints);
        GridBagConstraints sliderConstraints = new GridBagConstraints();
        sliderConstraints.fill = GridBagConstraints.HORIZONTAL;
        sliderConstraints.gridx = 1;
        sliderConstraints.gridy = dimIndex;
        sliderConstraints.weightx = 1;
        sliderConstraints.weighty = 0;
        sliderConstraints.insets = DEFAULT_INSETS;
        sliderPanel.add(slider, sliderConstraints);
    }

    // builds a slider and its associated components, to put them in the view
    protected void buildSlider(int dimSize, int dimIndex, int shapeLength) {
        final IdentifiedSlider slider = new IdentifiedSlider(dimIndex);
        afterSliderCreation(slider, dimSize, dimIndex, shapeLength);
    }

    protected void notifyChange(IdentifiedSlider slider) {
        if (slider != null) {
            notifySubPositionChanged(slider.getDimIndex(), slider.getValue());
        }
    }

    protected void notifySubPositionChanged(int dimIndex, int position) {
        targetDelegate.warnMediators(new SubPositionInformation(this, new int[] { dimIndex, position }));
    }

    protected void notifyFullPositionChanged(int... position) {
        targetDelegate.warnMediators(new FullPositionInformation(this, position));
    }

    // updates a slider and its associated components
    protected void updateSlider(int dimSize, int dimIndex, int shapeLength) {
        final IdentifiedSlider slider = sliderMap.get(dimIndex);
        slider.setEnabled(sliderEnabled);
        slider.setMaximumTickCount(majorTicksPerSlider);
        ChangeListener sliderListener = sliderListeners.get(dimIndex);
        labelMap.get(dimIndex).setText(computeSliderTitle(dimIndex, shapeLength));
        if (sliderListener != null) {
            slider.removeChangeListener(sliderListener);
        }
        slider.setBackground(sliderView.getBackground());
        if (dimSize > 0) {
            slider.setMaximumDouble(dimSize - 1);
            int tickSpacing = Math.max((int) Math.ceil((double) dimSize / (double) getMajorTicksPerSlider()), 1);
            if (tickSpacing > 1) {
                slider.setMinorTickSpacing(1);
            } else {
                slider.setMinorTickSpacing(0);
            }
            slider.setPaintTicks(true);
            slider.setSnapToTicks(true);
        } else {
            slider.setMaximumDouble(0);
            slider.setMajorTickSpacing(0);
            slider.setMinorTickSpacing(0);
            slider.setPaintTicks(false);
            slider.setSnapToTicks(false);
        }
        slider.setValue(0);
        if ((adaptedConvertors != null) && (adaptedConvertors.length > dimIndex)) {
            slider.setValueConvertor(adaptedConvertors[dimIndex]);
        } else {
            slider.setValueConvertor(null);
        }
        if (sliderListener != null) {
            slider.addChangeListener(sliderListener);
        }
        // Reset label table to update its labels
        slider.setLabelTable(null);
    }

    // cleans a slider and its associated components, and removes them from view
    protected void cleanSlider(int dimIndex) {
        IdentifiedSlider slider = sliderMap.get(dimIndex);
        slider.removeChangeListener(sliderListeners.get(dimIndex));
        sliderPanel.remove(slider);
        sliderPanel.remove(labelMap.get(dimIndex));
        sliderMap.remove(dimIndex);
    }

    // computes a slider's title
    protected String computeSliderTitle(int dimIndex, int shapeLength) {
        StringBuilder titleBuilder = new StringBuilder();
        if (dimIndex == shapeLength - 1) {
            titleBuilder.append("X");
        } else if (dimIndex == shapeLength - 2) {
            titleBuilder.append("Y");
        } else if (dimIndex == shapeLength - 3) {
            titleBuilder.append("Z");
        } else if (dimIndex == shapeLength - 4) {
            titleBuilder.append("T");
        } else {
            titleBuilder.append("dim").append(dimIndex);
        }
        titleBuilder.append(": ");
        return titleBuilder.toString();
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        targetDelegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        targetDelegate.removeMediator(mediator);
    }

    @Override
    public int[] getPositions() {
        int[] positions;
        synchronized (sliderMap) {
            positions = new int[sliderMap.size()];
            for (int shapeIndex = 0; shapeIndex < positions.length; shapeIndex++) {
                IdentifiedSlider slider = sliderMap.get(shapeIndex);
                if (slider != null) {
                    positions[shapeIndex] = slider.getValue();
                }
            }
        }
        return positions;
    }

    @Override
    public void setPositionAt(int shapeIndex, int position) {
        synchronized (sliderMap) {
            setNoLockPositionAt(shapeIndex, position);
        }
    }

    protected void setNoLockPositionAt(int shapeIndex, int position) {
        if ((shapeIndex > -1) && (position > -1)) {
            Integer key = Integer.valueOf(shapeIndex);
            IdentifiedSlider slider = sliderMap.get(key);
            if ((slider != null) && (slider.getValue() != position)) {
                ChangeListener listener = sliderListeners.get(key);
                if (listener != null) {
                    slider.removeChangeListener(listener);
                }
                slider.setValue(position);
                if ((adaptedConvertors != null) && (position < adaptedConvertors.length - 1)
                        && (adaptedConvertors[position + 1] instanceof MatrixPositionConvertor)) {
                    ((MatrixPositionConvertor) adaptedConvertors[position + 1]).setNumberValue(position);
                }
                if (listener != null) {
                    slider.addChangeListener(listener);
                }
            }
        }
    }

    @Override
    public void setPositions(final int... positions) {
        if (positions != null) {
            synchronized (sliderMap) {
                for (int shapeIndex = 0; shapeIndex < positions.length; shapeIndex++) {
                    setNoLockPositionAt(shapeIndex, positions[shapeIndex]);
                }
            }
            revalidate();
            repaint();
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class IdentifiedSlider extends Slider {

        private static final long serialVersionUID = -1740323153242461506L;

        protected final int dimIndex;

        public IdentifiedSlider(int dimIndex) {
            super();
            this.dimIndex = dimIndex;
            setAutoScale(false);
        }

        public int getDimIndex() {
            return dimIndex;
        }

    }

    @Override
    public boolean isSelected() {
        return sliderEnabled;
    }

    @Override
    public void setSelected(boolean selected) {
        setSliderViewEnabled(selected);
    }

}
