/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.io.File;

import javax.swing.ImageIcon;

import fr.soleil.comete.definition.util.IImageExporter;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.swing.ImageViewer;

public class TextImageExporter implements IImageExporter {

    protected static final CometeImage EXPORT_IMAGE = ImageTool.getCometeImage(
            new ImageIcon(TextImageExporter.class.getResource("/fr/soleil/comete/icons/bullet_disk_trim.png")));
    protected static final CometeImage IMPORT_IMAGE = ImageTool.getCometeImage(
            new ImageIcon(TextImageExporter.class.getResource("/fr/soleil/comete/icons/bullet_folder_trim.png")));

    @Override
    public String getFileExtension() {
        return "ctxti";
    }

    @Override
    public String getFileDescription() {
        return "[ctxti] Comete Text Image Files";
    }

    @Override
    public String getFormatName() {
        return "Comete Text Image Format";
    }

    @Override
    public CometeImage getExportIcon() {
        return EXPORT_IMAGE;
    }

    @Override
    public CometeImage getImportIcon() {
        return IMPORT_IMAGE;
    }

    @Override
    public void exportImage(IImageViewer imageViewer, File outputFile) {
        if ((imageViewer instanceof ImageViewer) && (outputFile != null) && (!outputFile.isDirectory())) {
            ((ImageViewer) imageViewer).saveDataFile(outputFile.getAbsolutePath());
        }
    }

    @Override
    public void importImage(IImageViewer imageViewer, File inputFile) {
        if ((imageViewer instanceof ImageViewer) && (inputFile != null) && (inputFile.exists())
                && (!inputFile.isDirectory())) {
            ((ImageViewer) imageViewer).loadDataFile(inputFile.getAbsolutePath());
        }
    }

}
