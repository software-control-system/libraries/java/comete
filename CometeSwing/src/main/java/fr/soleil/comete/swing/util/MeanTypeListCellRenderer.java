/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import javax.swing.ListCellRenderer;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.chart.util.ChartUtils;

/**
 * A {@link ListCellRenderer} for sampling mean type selection.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MeanTypeListCellRenderer extends TypeListCellRenderer {

    private static final long serialVersionUID = 3720732900972244387L;

    private static final String GLOBAL_MEAN_TEXT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.mean.global.text");
    private static final String GLOBAL_MEAN_TOOLTIP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.mean.global.tooltip");
    private static final String LOCAL_MEAN_TEXT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.mean.sample.text");
    private static final String LOCAL_MEAN_TOOLTIP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.mean.sample.tooltip");

    public MeanTypeListCellRenderer() {
        super();
    }

    @Override
    protected String getText(int id) {
        String text;
        switch (id) {
            case IChartViewer.GLOBAL_MEAN:
                text = GLOBAL_MEAN_TEXT;
                break;
            case IChartViewer.LOCAL_MEAN:
                text = LOCAL_MEAN_TEXT;
                break;
            default:
                text = Integer.toString(id);
                break;
        }
        return text;
    }

    @Override
    protected String getToolTipText(int id, String defaultTooltip) {
        String text;
        switch (id) {
            case IChartViewer.GLOBAL_MEAN:
                text = GLOBAL_MEAN_TOOLTIP;
                break;
            case IChartViewer.LOCAL_MEAN:
                text = LOCAL_MEAN_TOOLTIP;
                break;
            default:
                text = defaultTooltip;
                break;
        }
        return text;
    }

}
