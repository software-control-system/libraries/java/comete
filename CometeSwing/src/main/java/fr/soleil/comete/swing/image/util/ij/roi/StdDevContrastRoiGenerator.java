/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Cursor;

import javax.swing.ImageIcon;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.exception.ApplicationIdException;
import fr.soleil.lib.project.ij.FlatMatrixTool;
import ij.gui.Roi;

public class StdDevContrastRoiGenerator extends AbstractContrastRoiGenerator {

    public StdDevContrastRoiGenerator(String text, String description) {
        this(text, description, ImageViewer.CONTRAST_STDDEV);
    }

    protected StdDevContrastRoiGenerator(String text, String description, ImageIcon icon) {
        super(text, description, icon);
    }

    @Override
    public Cursor getCursor() {
        return CURSOR;
    }

    @Override
    protected void contrast(Roi roi, ImageViewer viewer) throws ApplicationIdException {
        stdDevContrast(roi, viewer);
    }

    /**
     * Applies the contrast based on standard deviation and 10% of mean on an {@link ImageViewer}
     * 
     * @param viewer The {@link ImageViewer}
     * @param roi The {@link Roi} that determines the rectangle region for which to compute the contrast
     * @throws ApplicationIdException If the application id is not yet set for this {@link ImageViewer}
     * @see ImageViewer#setApplicationId(String)
     */
    public static void stdDevContrast(Roi roi, ImageViewer viewer) throws ApplicationIdException {
        double[] stats = FlatMatrixTool.extractSubMatrixMinMaxMeanAndStdDev(roi, viewer.getValue(), viewer.getDimX(),
                viewer.getDimY());
        double min = stats[0];
        if (!Double.isNaN(min)) {
            double max = stats[1], mean = stats[2], stdDev = stats[3];
            double fivePercentMean = mean * 0.1;
            double distance = stdDev + fivePercentMean;
            min = mean - distance;
            max = mean + distance;
            if (min > max) {
                double temp = min;
                min = max;
                max = temp;
            }
            viewer.setFitMinMax(min, max);
        }
    }

}
