/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.listener.ITextComponentListener;
import fr.soleil.comete.definition.widget.IListenableTextComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.swing.text.HtmlTextPane;

/**
 * A viewer that can be used to display HTML formated text
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class HtmlFormattedTextViewer extends HtmlTextPane implements IListenableTextComponent, DocumentListener {

    private static final long serialVersionUID = -1181732251935118152L;

    private final Set<ITextComponentListener> textComponentListeners;
    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;

    /**
     * Default constructor
     */
    public HtmlFormattedTextViewer() {
        super();
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        textComponentListeners = Collections.newSetFromMap(new WeakHashMap<ITextComponentListener, Boolean>());
        getDocument().addDocumentListener(this);
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(getForeground());
    }

    @Override
    public int getHorizontalAlignment() {
        // not compatible
        return 0;
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(CometeFont font) {
        setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        setForeground(ColorTool.getColor(color));
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // nothing to do: not compatible
    }

    @Override
    public void setPreferredSize(int width, int height) {
        if ((width < 1) || (height < 1)) {
            setPreferredSize(null);
        } else {
            setPreferredSize(new Dimension(width, height));
        }
    }

    @Override
    public void addTextComponentListener(ITextComponentListener listener) {
        if (listener != null) {
            synchronized (textComponentListeners) {
                textComponentListeners.add(listener);
            }
        }
    }

    @Override
    public void removeTextComponentListener(ITextComponentListener listener) {
        if (listener != null) {
            synchronized (textComponentListeners) {
                textComponentListeners.remove(listener);
            }
        }
    }

    @Override
    public void fireTextChanged(EventObject event) {
        warnMediators(new TextInformation(this, getText()));
        List<ITextComponentListener> copy = new ArrayList<ITextComponentListener>();
        synchronized (textComponentListeners) {
            copy.addAll(textComponentListeners);
        }
        for (ITextComponentListener listener : copy) {
            listener.textChanged(event);
        }
        copy.clear();
    }

    private void fireTextChanged() {
        fireTextChanged(new EventObject(this));
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        fireTextChanged();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        fireTextChanged();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        fireTextChanged();
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I>
     *            The type of Data the {@link TargetInformation} is able to
     *            manipulate
     * @param <V>
     *            The type of {@link TargetInformation} to transmit
     * @param targetInformation
     *            The {@link TargetInformation} that should be transmitted to
     *            {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return (isEditable() && isEnabled() && hasFocus());
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        if (listener != null) {
            ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
        }
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        if (listener != null) {
            ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
        }
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
