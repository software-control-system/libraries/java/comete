/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.net.URI;

import javax.swing.ImageIcon;

import fr.soleil.comete.definition.widget.util.CometeImage;

public class ImageTool {

    public static ImageIcon getImage(CometeImage image) {
        ImageIcon result = null;
        if (image != null) {
            if (image instanceof ImageIconCometeImage) {
                result = ((ImageIconCometeImage) image).getIcon();
            } else {
                URI uri = image.getImageLocation();
                if (uri != null) {
                    try {
                        result = new ImageIcon(uri.toURL());
                    } catch (Exception e) {
                        result = null;
                    }
                }
            }
        }
        return result;
    }

    public static CometeImage getCometeImage(ImageIcon imageIcon) {
        CometeImage result = null;
        if (imageIcon != null) {
            try {
                // description may contain the path to recover the image
                result = new CometeImage(new URI(imageIcon.getDescription()));
            } catch (Exception e) {
                // if description did not contain the path to recover the image
                result = null;
            }
        }
        return result;
    }
}
