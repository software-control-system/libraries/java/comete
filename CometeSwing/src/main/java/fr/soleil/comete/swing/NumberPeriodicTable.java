/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import fr.soleil.comete.definition.listener.IPeriodicTableListener;
import fr.soleil.comete.definition.widget.INumberPeriodicTable;
import fr.soleil.comete.swing.periodictable.ElementButton;
import fr.soleil.comete.swing.periodictable.ElementModel;
import fr.soleil.comete.swing.periodictable.PeriodicTable;

public class NumberPeriodicTable extends PeriodicTable
        implements INumberPeriodicTable, fr.soleil.comete.swing.periodictable.IPeriodicTableListener {

    private static final long serialVersionUID = 1806640933836009255L;

    private final List<IPeriodicTableListener<Number>> listeners = new ArrayList<IPeriodicTableListener<Number>>();

    public NumberPeriodicTable() {
        super();
        setMode(PeriodicTable.ATOMIC_NUMBER_MODE);
        addPeriodicTableListener(this);
    }

    @Override
    public void setValue(Number number) {
        if (number != null) {
            String value = String.valueOf(number.intValue());
            // System.out.println("initFromDAO=" + value);
            ElementButton founded_element = searchElementFromValue(value);
            if (founded_element == null) {
                setData(ElementModel.defaultElement());
            } else {
                // System.out.println("founded_element=" + founded_element);
                setData(founded_element.getElement());
            }
        }
    }

    @Override
    public void addPeriodicTableListener(IPeriodicTableListener<Number> listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }

    }

    @Override
    public void fireValueChanged(Number value) {
        ListIterator<IPeriodicTableListener<Number>> iterator = listeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().valueChanged(value);
        }
    }

    @Override
    public void removePeriodicTableListener(IPeriodicTableListener<Number> listener) {
        if (listeners.contains(listener)) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }

    }

    @Override
    public void selectedAtomicNumber(String atomicNumber) {
        ListIterator<IPeriodicTableListener<Number>> iterator = listeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().selectedAtomicNumber(atomicNumber);
        }
    }

    @Override
    public void selectedName(String name) {
        ListIterator<IPeriodicTableListener<Number>> iterator = listeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().selectedName(name);
        }
    }

    @Override
    public void selectedSymbole(String symbole) {
        ListIterator<IPeriodicTableListener<Number>> iterator = listeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().selectedSymbole(symbole);
        }
    }

    @Override
    public void selectedValueChanged(String value) {
        try {
            Double dValue = Double.valueOf(value);
            fireValueChanged(dValue.intValue());
        } catch (NumberFormatException e) {
        }
    }

    @Override
    public Number getNumberValue() {
        ElementButton elementButton = getSelectedElement();
        Number result = null;
        if (elementButton != null) {
            int theMode = getMode();
            ElementModel model = elementButton.getElement();
            if (model != null && theMode == PeriodicTable.ATOMIC_NUMBER_MODE) {
                result = Integer.parseInt(model.getAtomicNumber());
            }
        }
        return result;
    }

    @Override
    public void setNumberValue(Number value) {
        super.setValue(value.toString());
    }

    @Override
    public String getFormat() {
        // not managed
        return null;
    }

    @Override
    public void setFormat(String format) {
        // not managed
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame(NumberPeriodicTable.class.getSimpleName() + " test");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        NumberPeriodicTable table = new NumberPeriodicTable();
//        table.setFont(new Font(Font.DIALOG, Font.PLAIN, 10));
        frame.setContentPane(table);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

}
