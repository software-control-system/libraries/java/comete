/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.DataList;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Class used to represent chart data as a String
 * 
 * @author JL Pons
 */
public class TabbedLine {

    public static final String DEFAULT_INDEX_COLUMN_NAME = "Index";
    public static final String DEFAULT_DATE_COLUMN_NAME = "Time";

    protected final AbstractDataView[] dv;
    protected final DataList[] dl;
    protected boolean timeScale;
    protected int sIndex;
    protected int precision;
    protected String noValueString;
    protected String indexColumnName;
    protected String dateColumnName;
    protected boolean useDisplayNameInFiles;

    public TabbedLine(int nb) {
        dv = new AbstractDataView[nb];
        dl = new DataList[nb];
        precision = 0;
        noValueString = "*";
        indexColumnName = DEFAULT_INDEX_COLUMN_NAME;
        dateColumnName = DEFAULT_DATE_COLUMN_NAME;
        useDisplayNameInFiles = false;
    }

    public boolean isUseDisplayNameInFiles() {
        return useDisplayNameInFiles;
    }

    public void setUseDisplayNameInFiles(boolean useDisplayNameInFiles) {
        this.useDisplayNameInFiles = useDisplayNameInFiles;
    }

    public void setPrecision(int milliseconds) {
        precision = milliseconds;
    }

    public void setNoValueString(String noValueString) {
        this.noValueString = noValueString;
    }

    public void add(int id, AbstractDataView v) {
        dv[id] = v;
        dl[id] = v.getData();
    }

    public double getMinTime() {
        boolean nan = false;
        double minTime = Double.MAX_VALUE;
        for (DataList element : dl) {
            if (element != null) {
                double x = element.getX();
                if (x < minTime) {
                    minTime = element.getX();
                } else if (Double.isNaN(x)) {
                    nan = true;
                }
            }
        }
        if ((minTime == Double.MAX_VALUE) && nan) {
            minTime = Double.NaN;
        }
        return minTime;
    }

    public String getFirstLine(boolean timeScale) {
        return getFirstLine(timeScale, false);
    }

    // CSV compatibility (JAVAAPI-573)
    public String getFirstLine(boolean timeScale, boolean csv) {
        StringBuilder lineBuilder = new StringBuilder();
        if ((dateColumnName == null) || dateColumnName.trim().isEmpty()) {
            dateColumnName = DEFAULT_DATE_COLUMN_NAME;
        }
        if ((indexColumnName == null) || indexColumnName.trim().isEmpty()) {
            indexColumnName = DEFAULT_INDEX_COLUMN_NAME;
        }
        this.timeScale = timeScale;
        String separator = csv ? ChartUtils.CSV_SEPARATOR : ChartUtils.TAB;
        if (timeScale) {
            lineBuilder.append(dateColumnName).append(separator);
        } else {
            lineBuilder.append(indexColumnName).append(separator);
        }
        for (int i = 0; i < dv.length; i++) {
            if (dv[i] != null) {
                if (isUseDisplayNameInFiles()) {
                    lineBuilder.append(dv[i].getDisplayName());
                } else {
                    lineBuilder.append(dv[i].getId());
                }
            }
            if (i < dv.length - 1) {
                lineBuilder.append(separator);
            }
        }
        lineBuilder.append(ChartUtils.NEW_LINE);
        return lineBuilder.toString();
    }

    public void setIndexColumnName(String name) {
        indexColumnName = name;
    }

    public String getIndexColumnName() {
        return indexColumnName;
    }

    public String getDateColumnName() {
        return dateColumnName;
    }

    public void setDateColumnName(String dateColumnName) {
        this.dateColumnName = dateColumnName;
    }

    protected boolean isInXRange(double x, double t0) {
        boolean rangeOk;
        if (Double.isNaN(t0)) {
            rangeOk = Double.isNaN(x);
        } else {
            rangeOk = ((x >= t0 - precision) && (x <= t0 + precision));
        }
        return rangeOk;
    }

    public String getNextLine() {
        return getNextLine(false);
    }

    // CSV compatibility (JAVAAPI-573)
    public String getNextLine(boolean csv) {
        String line;
        double t0 = getMinTime();
        // Test end of data
        if (t0 == Double.MAX_VALUE) {
            line = null;
        } else {
            String separator, nullString;
            if (csv) {
                separator = ChartUtils.CSV_SEPARATOR;
                nullString = ObjectUtils.EMPTY_STRING;
            } else {
                separator = ChartUtils.TAB;
                nullString = noValueString;
            }
            StringBuilder lineBuilder = new StringBuilder();
            if (timeScale) {
                lineBuilder.append(ChartUtils.formatTimeValue(t0)).append(separator);
            } else {
                lineBuilder.append(ChartUtils.formatNaNValue(t0, nullString)).append(separator);
            }
            String format;
            AbstractDataView view;
            for (int i = 0; i < dl.length; i++) {
                view = dv[i];
                if (view == null) {
                    format = null;
                } else {
                    format = view.getFormat();
                }
                if (dl[i] != null) {
                    if (isInXRange(dl[i].getX(), t0)) {
                        lineBuilder.append(ChartUtils.formatValue(dl[i].getY(), nullString, format));
                        dl[i] = dl[i].next;
                    } else {
                        lineBuilder.append(nullString);
                    }
                } else {
                    lineBuilder.append(nullString);
                }
                if (i < dl.length - 1) {
                    lineBuilder.append(separator);
                }
            }
            lineBuilder.append(ChartUtils.NEW_LINE);
            line = lineBuilder.toString();
        }
        return line;
    }

    public String[] getFirstFields(boolean timeScale, boolean showIndex) {
        this.timeScale = timeScale;
        sIndex = (showIndex) ? 1 : 0;
        String[] fields = new String[dv.length + sIndex];
        if (sIndex > 0) {
            if (timeScale) {
                fields[0] = dateColumnName;
            } else {
                fields[0] = indexColumnName;
            }
        }
        for (int i = 0; i < dv.length; i++) {
            if (dv[i] != null) {
                fields[i + sIndex] = dv[i].getDisplayName();
            }
        }
        return fields;
    }

    public String[] getNextFields(Object parent) {
        String[] fields;
        double t0 = getMinTime();
        // Test end of data
        if (t0 == Double.MAX_VALUE) {
            fields = null;
        } else {
            fields = new String[dv.length + sIndex];
            if (sIndex > 0) {
                if (timeScale) {
                    fields[0] = ChartUtils.formatTimeValue(t0);
                } else {
                    fields[0] = ChartUtils.formatNaNValue(t0, noValueString);
                }
            }
            for (int i = 0; i < dl.length; i++) {
                if (dl[i] != null) {
                    if (isInXRange(dl[i].getX(), t0)) {
                        if (dv[i] == null) {
                            fields[i + sIndex] = ObjectUtils.EMPTY_STRING;
                        } else {
                            fields[i + sIndex] = dv[i].formatValue(dl[i].getY(), parent);
                        }
                        dl[i] = dl[i].next;
                    } else {
                        fields[i + sIndex] = ObjectUtils.EMPTY_STRING;
                    }
                } else {
                    fields[i + sIndex] = ObjectUtils.EMPTY_STRING;
                }
            }
        }
        return fields;
    }

}
