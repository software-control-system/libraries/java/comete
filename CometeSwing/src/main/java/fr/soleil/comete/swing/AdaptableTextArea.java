/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.listener.ITextAreaListener;
import fr.soleil.comete.definition.widget.ITextArea;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.DynamicSizeTextArea;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A text area for which user can change its height (number of rows) through buttons
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AdaptableTextArea extends JPanel implements ActionListener, ITextArea, ITextAreaListener {

    private static final long serialVersionUID = -1096623200369188111L;

    protected static final ImageIcon MORE_SCROLL = new ImageIcon(
            AdaptableTextArea.class.getResource("/fr/soleil/comete/icons/scroll_add.png"));
    protected static final ImageIcon LESS_SCROLL = new ImageIcon(
            AdaptableTextArea.class.getResource("/fr/soleil/comete/icons/scroll_delete.png"));

    protected static final String LF = ObjectUtils.NEW_LINE;
    protected static final int MAX_INITIAL_ROWS = 5;

    protected final DynamicSizeTextArea textArea;
    protected final JPanel rightPanel;
    protected final JPanel buttonPanel;
    protected final JButton addRowButton;
    protected final JButton removeRowButton;
    protected final TargetDelegate delegate;
    protected final ErrorNotificationDelegate errorNotificationDelegate;
    protected final Collection<ITextAreaListener> listeners;
    protected int rows;

    public AdaptableTextArea(String text) {
        this();
        textArea.removeTextAreaListener(this);
        textArea.setText(text);
        textArea.addTextAreaListener(this);
        String[] counter = getTextArea().getText().split(LF);
        rows = Math.min(counter.length, MAX_INITIAL_ROWS);
        updateRows();
    }

    public AdaptableTextArea() {
        super(new BorderLayout(10, 10));
        listeners = Collections.newSetFromMap(new ConcurrentHashMap<ITextAreaListener, Boolean>());
        textArea = generateTextArea();
        textArea.addTextAreaListener(this);
        addRowButton = new JButton(MORE_SCROLL);
        addRowButton.setToolTipText("Increase text zone");
        addRowButton.setMargin(CometeUtils.getzInset());
        addRowButton.addActionListener(this);
        removeRowButton = new JButton(LESS_SCROLL);
        removeRowButton.setToolTipText("Reduce text zone");
        removeRowButton.setMargin(CometeUtils.getzInset());
        removeRowButton.addActionListener(this);
        buttonPanel = new JPanel(new BorderLayout(2, 2));
        buttonPanel.add(addRowButton, BorderLayout.WEST);
        buttonPanel.add(removeRowButton, BorderLayout.EAST);
        rightPanel = new JPanel(new BorderLayout());
        rightPanel.add(buttonPanel, BorderLayout.NORTH);
        add(textArea, BorderLayout.CENTER);
        add(rightPanel, BorderLayout.EAST);
        rows = 1;
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        updateRows();
    }

    protected DynamicSizeTextArea generateTextArea() {
        return new DynamicSizeTextArea();
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    /**
     * Gives access to the {@link TextArea} inside this {@link AdaptableTextArea}
     * 
     * @return A {@link TextArea}
     */
    protected DynamicSizeTextArea getTextArea() {
        return textArea;
    }

    protected void updateRows() {
        rows = Math.max(rows, 1);
        textArea.setRows(rows);
        textArea.updateScrollPane();
        addRowButton.setEnabled(rows < Integer.MAX_VALUE);
        removeRowButton.setEnabled(rows > 1);
        revalidate();
        Container parent = getParent();
        if (parent instanceof JComponent) {
            JComponent comp = (JComponent) parent;
            comp.revalidate();
            comp.repaint();
        } else {
            repaint();
        }
    }

    // ////////////// //
    // ActionListener //
    // ////////////// //

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e != null) {
            if (e.getSource() == addRowButton) {
                rows++;
                updateRows();
            } else if (e.getSource() == removeRowButton) {
                rows--;
                updateRows();
            }
        }
    }

    // ///////// //
    // ITextArea //
    // ///////// //

    @Override
    public void setToolTipText(String text) {
        super.setToolTipText(text);
        if (textArea != null) {
            textArea.setToolTipText(text);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (textArea != null) {
            textArea.setEnabled(enabled);
        }
    }

    @Override
    public String getText() {
        return textArea.getText();
    }

    @Override
    public void setText(String text) {
        textArea.setText(text);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        textArea.setCometeBackground(color);
    }

    @Override
    public CometeColor getCometeBackground() {
        return textArea.getCometeBackground();
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        textArea.setCometeForeground(color);
    }

    @Override
    public CometeColor getCometeForeground() {
        return textArea.getCometeForeground();
    }

    @Override
    public void setCometeFont(CometeFont font) {
        textArea.setCometeFont(font);
    }

    @Override
    public CometeFont getCometeFont() {
        return textArea.getCometeFont();
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        textArea.setHorizontalAlignment(halign);
    }

    @Override
    public int getHorizontalAlignment() {
        return textArea.getHorizontalAlignment();
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public boolean isEditingData() {
        return textArea.isEditingData;
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void setEditable(boolean editable) {
        textArea.setEditable(editable);
    }

    @Override
    public boolean isEditable() {
        return textArea.isEditable();
    }

    @Override
    public void addTextAreaListener(ITextAreaListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeTextAreaListener(ITextAreaListener listener) {
        if (listener != null) {
            listeners.remove(listener);
        }
    }

    @Override
    public void fireTextChanged(EventObject event) {
        for (ITextAreaListener listener : listeners) {
            if (listener != null) {
                listener.textChanged(event);
            }
        }
    }

    @Override
    public void fireActionPerformed(EventObject event) {
        for (ITextAreaListener listener : listeners) {
            if (listener != null) {
                listener.actionPerformed(event);
            }
        }
    }

    @Override
    public void setColumns(int nbColumns) {
        textArea.setColumns(nbColumns);
    }

    @Override
    public void setRows(int nbRows) {
        rows = nbRows;
        updateRows();
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

    // ///////////////// //
    // ITextAreaListener //
    // ///////////////// //

    @Override
    public void textChanged(EventObject event) {
        if ((event != null) && (event.getSource() == textArea)) {
            fireTextChanged(new EventObject(this));
        }
    }

    @Override
    public void actionPerformed(EventObject event) {
        if ((event != null) && (event.getSource() == textArea)) {
            warnMediators(new TextInformation(this, getText()));
            fireActionPerformed(new EventObject(this));
        }
    }

}
