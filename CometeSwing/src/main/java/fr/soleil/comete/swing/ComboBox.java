/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;
import java.util.WeakHashMap;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.metal.MetalComboBoxUI;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.IComboBoxListener;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IComboBox;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.DefaultValueListCellRenderer;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.comete.swing.util.ValueListCellRenderer;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.IErrorNotifiableTarget;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.combo.DynamicRenderingComboBox;
import fr.soleil.lib.project.swing.combo.OpaqueAdaptedComboPopup;
import fr.soleil.lib.project.swing.ui.OpaqueAdaptedComboBoxUI;

/**
 * A {@link JComboBox} that implements the {@link IComboBox} interface
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ComboBox extends DynamicRenderingComboBox<Object> implements IComboBox, IErrorNotifiableTarget {

    private static final long serialVersionUID = 4840821474974790614L;

    private final Collection<IComboBoxListener> comboBoxListeners;
    private int horizontalAlignment;
    private Object[] valueList;
    private final Object valueLock;
    protected volatile boolean editing;
    private final TargetDelegate delegate;
    protected volatile boolean editable;
    protected volatile boolean fieldEditable;
    protected volatile boolean popupDisplayable;
    protected volatile boolean linkPopupVisibilityWithEditable;
    protected boolean possibleValuesInTextArray;
    private final boolean initialized;
    private final ErrorNotificationDelegate errorNotificationDelegate;

    public ComboBox() {
        super();
        // By default, dynamic tooltip text should not be used, as tooltip text is generally adapted to connected source
        setDynamicTooltipText(false);
        editable = true;
        fieldEditable = super.isEditable();
        popupDisplayable = true;
        linkPopupVisibilityWithEditable = true;
        possibleValuesInTextArray = false;
        valueLock = new Object();
        editing = false;
        // Here, we use WeakHashMap to avoid memory leaks
        comboBoxListeners = Collections.newSetFromMap(new WeakHashMap<>());
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        setValueList((Object[]) null);
        setRenderer(new DefaultValueListCellRenderer(this));
        setHorizontalAlignment(IComponent.CENTER);
        initialized = true;
        setUI(buildUI());
    }

    @Override
    public void setUI(ComboBoxUI ui) {
        super.setUI(ui);
        if (ui instanceof EditableComboBoxUI) {
            ((EditableComboBoxUI) ui).updateButton();
        }
    }

    @Override
    protected OpaqueAdaptedComboBoxUI buildUI() {
        // return a new EditableComboBoxUI only when it has a chance to know whether popup is displayable
        return initialized ? new EditableComboBoxUI() : new OpaqueAdaptedComboBoxUI();
    }

    /**
     * Returns whether this combobox' textfield is supposed to be editable
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isFieldEditable() {
        return fieldEditable;
    }

    /**
     * Sets this combobox's textfield editable, if possible
     * 
     * @param fieldEditable Whether this combobox's textfield should be editable
     */
    public void setFieldEditable(boolean fieldEditable) {
        this.fieldEditable = fieldEditable;
        super.setEditable(fieldEditable && editable);
    }

    // XXX There is no way to override this method because of bad consequences
    // in java swing
    // @Override
    // public boolean isEditable() {
    // return editable;
    // }

    /**
     * Returns whether setting this {@link ComboBox} as not editable will forbid the display of its associated popup
     * 
     * @return A <code>boolean</code>
     */
    public boolean isLinkPopupVisibilityWithEditable() {
        return linkPopupVisibilityWithEditable;
    }

    /**
     * Sets whether setting this {@link ComboBox} as not editable will forbid the display of its associated popup
     * 
     * @param linkPopupVisibilityWithEditable Whether setting this {@link ComboBox} as not editable will forbid the
     *            display of its associated popup
     */
    public void setLinkPopupVisibilityWithEditable(boolean linkPopupVisibilityWithEditable) {
        if (this.linkPopupVisibilityWithEditable != linkPopupVisibilityWithEditable) {
            this.linkPopupVisibilityWithEditable = linkPopupVisibilityWithEditable;
            updateEditable();
        }
    }

    /**
     * Returns whether {@link #setStringArray(String[])} method will set possible values.<br />
     * <code>false</code> by default.
     * 
     * @return Whether {@link #setStringArray(String[])} method will set possible values.
     *         <ul>
     *         <li>If <code>true</code>, {@link #setStringArray(String[])} will call
     *         {@link #setValueList(Object...)}.</li>
     *         <li>If <code>false</code>, {@link #setStringArray(String[])} will call
     *         {@link #setDisplayedList(String...)}.</li>
     *         </ul>
     */
    public boolean isPossibleValuesInTextArray() {
        return possibleValuesInTextArray;
    }

    /**
     * Sets whether {@link #setStringArray(String[])} method will set possible values.
     * 
     * @param possibleValuesInTextArray Whether {@link #setStringArray(String[])} method will set possible values.
     *            <ul>
     *            <li>If <code>true</code>, {@link #setStringArray(String[])} will call
     *            {@link #setValueList(Object...)}.</li>
     *            <li>If <code>false</code>, {@link #setStringArray(String[])} will call
     *            {@link #setDisplayedList(String...)}.</li>
     *            </ul>
     */
    public void setPossibleValuesInTextArray(boolean possibleValuesInTextArray) {
        this.possibleValuesInTextArray = possibleValuesInTextArray;
    }

    @Override
    public void setEditable(boolean editable) {
        if (this.editable != editable) {
            this.editable = editable;
            updateEditable();
        }
    }

    protected final void updateEditable() {
        if (editable) {
            popupDisplayable = true;
        } else if (linkPopupVisibilityWithEditable) {
            popupDisplayable = false;
        }
        if (isPopupVisible() && (!popupDisplayable)) {
            hidePopup();
        }
        super.setEditable(editable && isFieldEditable());
        ComboBoxUI ui = getUI();
        if (ui instanceof EditableComboBoxUI) {
            ((EditableComboBoxUI) ui).updateButton();
        }
    }

    @Override
    public void setPopupVisible(boolean visible) {
        super.setPopupVisible(visible && popupDisplayable);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this) {
            updateRendering();
            synchronized (valueLock) {
                if (!editing) {
                    // Differ event to let time to close popup
                    SwingUtilities.invokeLater(() -> {
                        fireSelectedItemChanged(new EventObject(ComboBox.this));
                    });
                }
            }
        } else {
            super.actionPerformed(e);
        }
    }

    @Override
    public String getText() {
        int index = getSelectedIndex();
        String displayedValue = null;
        String[] displayedList = getDisplayedList();
        if ((displayedList != null) && (index > -1) && (index < displayedList.length)) {
            displayedValue = displayedList[index];
        } else {
            Object value = getSelectedValue();
            if (value != null) {
                displayedValue = value.toString();
            }
        }
        return displayedValue;
    }

    @Override
    public void setText(String text) {
        if (text != null) {
            String[] displayedList = getDisplayedList();
            int index = -1;
            if (displayedList != null) {
                for (int i = 0; (i < displayedList.length) && (index == -1); i++) {
                    if (ObjectUtils.sameObject(displayedList[i], text)) {
                        index = i;
                    }
                }
            }
            if (index == -1) {
                setSelectedValue(text);
            } else {
                synchronized (valueLock) {
                    setSelectedIndex(index);
                }
            }
        }
    }

    @Override
    public Object getSelectedValue() {
        Object item;
        synchronized (valueLock) {
            item = getSelectedItem();
        }
        return item;
    }

    @Override
    public void setSelectedValue(Object value) {
        boolean repaint = false;
        synchronized (valueLock) {
            editing = true;
            boolean canSetListener = false;
            for (ActionListener listener : getActionListeners()) {
                if (listener == this) {
                    canSetListener = true;
                    break;
                }
            }
            removeActionListener(this);
            repaint = updateSelectedItem(value, repaint);
            if (canSetListener) {
                addActionListener(this);
            }
            editing = false;
        }
        if (repaint) {
            repaint();
        }
    }

    protected final boolean updateSelectedItem(Object value, boolean repaint) {
        if (valueList != null) {
            for (Object temp : valueList) {
                if ((temp != null) && (temp.equals(value) || isSameNumberValue(temp, value))) {
                    setSelectedItem(temp);
                    repaint = true;
                    break;
                }
            }
            if (!repaint) {
                // JAVAAPI-227: If the value does not match value list, setSelectedIndex(-1)
                repaint = (getSelectedIndex() == -1);
                setSelectedIndex(-1);
            }
        }
        return repaint;
    }

    @Override
    public String[] getDisplayedList() {
        String[] list = null;
        ValueListCellRenderer renderer = getRenderer();
        if (renderer != null) {
            list = renderer.getDisplayableValues();
        }
        return list;
    }

    @Override
    public void setDisplayedList(String... displayedList) {
        String[] displayed = null;
        ValueListCellRenderer renderer = getRenderer();
        if (renderer != null) {
            displayed = renderer.getDisplayableValues();
        }
        if (!ObjectUtils.sameObject(displayedList, displayed)) {
            boolean wasVisible = isPopupVisible();
            if (wasVisible) {
                setPopupVisible(false);
            }
            if (renderer != null) {
                renderer.setDisplayableValues(displayedList);
            }
            if (wasVisible) {
                setPopupVisible(true);
            }
        }
        repaintIfPossible();
    }

    /**
     * repaints only when component is displayed on screen
     */
    protected void repaintIfPossible() {
        if (isShowing()) {
            repaint();
        }
    }

    // for number toString() case
    protected boolean isSameNumberValue(Object value1, Object value2) {
        boolean same = false;
        try {
            double d1 = recoverNumberValue(value1);
            double d2 = recoverNumberValue(value2);
            same = ((d1 == d2) || (Double.isNaN(d1) && Double.isNaN(d2)));
        } catch (Exception e) {
            same = false;
        }
        return same;
    }

    protected double recoverNumberValue(Object value) throws NumberFormatException {
        double result;
        if (value instanceof Number) {
            result = ((Number) value).doubleValue();
        } else if (value instanceof String) {
            result = Double.parseDouble((String) value);
        } else {
            throw new NumberFormatException(value + " can't be parsed to a number value");
        }
        return result;
    }

    @Override
    public Object[] getValueList() {
        return valueList;
    }

    @Override
    public void setValueList(Object... valueList) {
        synchronized (valueLock) {
            // change value list only if necessary
            if (!ObjectUtils.sameObject(valueList, this.valueList)) {
                editing = true;
                // following test is done in case someone wanted to deactivate listener
                boolean canSetListener = false;
                for (ActionListener listener : getActionListeners()) {
                    if (listener == this) {
                        canSetListener = true;
                        break;
                    }
                }
                removeActionListener(this);
                this.valueList = valueList;
                boolean wasVisible = isPopupVisible();
                if (wasVisible) {
                    setPopupVisible(false);
                }
                Object item = getSelectedItem();
                removeAllItems();
                if (valueList != null) {
                    for (Object value : valueList) {
                        addItem(value);
                    }
                }
                updateSelectedItem(item, false);
                editing = false;
                if (wasVisible) {
                    setPopupVisible(true);
                }
                if (canSetListener) {
                    addActionListener(this);
                }
            }
        }
    }

    @Override
    public String[] getStringArray() {
        return getDisplayedList();
    }

    @Override
    public void setStringArray(String[] value) {
        if (isPossibleValuesInTextArray()) {
            setObjectArray(value);
        } else {
            setDisplayedList(value);
        }
    }

    @Override
    public Object[] getObjectArray() {
        return getValueList();
    }

    @Override
    public void setObjectArray(Object[] value) {
        setValueList(value);
    }

    @Override
    public void setCometeFont(CometeFont font) {
        setFont(FontTool.getFont(font));
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(getFont());
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(getBackground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        setBackground(ColorTool.getColor(color));
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(getForeground());
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        setForeground(ColorTool.getColor(color));
    }

    @Override
    public int getHorizontalAlignment() {
        return horizontalAlignment;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        horizontalAlignment = halign;
        switch (horizontalAlignment) {
            case IComponent.CENTER:
                super.setAlignmentX(JComponent.CENTER_ALIGNMENT);
                break;
            case IComponent.LEFT:
                super.setAlignmentX(JComponent.LEFT_ALIGNMENT);
                break;
            case IComponent.RIGHT:
                super.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
                break;
        }
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public ValueListCellRenderer getRenderer() {
        return (ValueListCellRenderer) super.getRenderer();
    }

    @Override
    public void setRenderer(ListCellRenderer<? super Object> aRenderer) {
        if (aRenderer instanceof ValueListCellRenderer) {
            super.setRenderer(aRenderer);
        }
    }

    @Override
    public void addComboBoxListener(IComboBoxListener listener) {
        if (listener != null) {
            synchronized (comboBoxListeners) {
                comboBoxListeners.add(listener);
            }
        }
    }

    @Override
    public void removeComboBoxListener(IComboBoxListener listener) {
        if (listener != null) {
            synchronized (comboBoxListeners) {
                comboBoxListeners.remove(listener);
            }
        }
    }

    @Override
    public void fireSelectedItemChanged(EventObject event) {
        List<IComboBoxListener> listeners = new ArrayList<IComboBoxListener>();
        synchronized (comboBoxListeners) {
            listeners.addAll(comboBoxListeners);
        }
        for (IComboBoxListener listener : listeners) {
            listener.selectedItemChanged(event);
        }
        listeners.clear();
        if (editable) {
            warnMediators();
        }
    }

    protected void warnMediators() {
        warnMediators(new TargetInformation<Object>(this, getSelectedValue()));
    }

    @Override
    public boolean hasFocus() {
        return (super.hasFocus() || isPopupVisible());
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return isPopupVisible();
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    /**
     * A {@link MetalComboBoxUI} that will display popup values only if editable
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class EditableComboBoxUI extends OpaqueAdaptedComboBoxUI {

        public EditableComboBoxUI() {
            super();
        }

        @Override
        protected OpaqueAdaptedComboPopup createPopup() {
            return new EditableComboPopup(comboBox);
        }

        // suppress warning forced as same signature in parent class
        @SuppressWarnings("unchecked")
        @Override
        public void setPopupVisible(JComboBox c, boolean v) {
            super.setPopupVisible(c, v && popupDisplayable);
        }

        /**
         * Updates arrow button availability depending on ComboBox editability
         */
        public void updateButton() {
            if (arrowButton != null) {
                arrowButton.setEnabled(popupDisplayable);
            }
        }
    }

    /**
     * A {@link BasicComboPopup} that will display popup values only if editable
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class EditableComboPopup extends OpaqueAdaptedComboPopup {

        private static final long serialVersionUID = 2327205114154275009L;

        public EditableComboPopup(JComboBox<?> combo) {
            super(combo);
        }

        @Override
        protected void togglePopup() {
            if (popupDisplayable) {
                super.togglePopup();
            } else {
                hide();
            }
        }
    }

}
