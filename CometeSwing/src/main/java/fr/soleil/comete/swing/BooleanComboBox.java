/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;

import fr.soleil.comete.definition.data.information.BooleanInformation;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.data.target.scalar.IBooleanTarget;

public class BooleanComboBox extends ComboBox implements IBooleanTarget {

    private static final long serialVersionUID = -7257524884937998703L;

    protected String trueLabel = "true";
    protected String falseLabel = "false";

    protected static final int TRUE_IDX = 1;
    protected static final int FALSE_IDX = 0;

    public BooleanComboBox() {
        super();
        setDisplayedList(new String[] { falseLabel, trueLabel });
        setValueList(false, true);
    }

    public String getFalseLabel() {
        return falseLabel;
    }

    public void setFalseLabel(String label) {
        if ((label != null) && (!label.equals(falseLabel))) {
            falseLabel = label;
            setDisplayedList(new String[] { falseLabel, trueLabel });
        }
    }

    public String getTrueLabel() {
        return trueLabel;
    }

    public void setTrueLabel(String label) {
        if ((label != null) && (!label.equals(trueLabel))) {
            trueLabel = label;
            setDisplayedList(new String[] { falseLabel, trueLabel });
        }
    }

    @Override
    protected void warnMediators() {
        warnMediators(new BooleanInformation(this, isSelected()));
    }

    @Override
    public boolean isSelected() {
        return (getSelectedIndex() == TRUE_IDX);
    }

    @Override
    public void setSelected(boolean bool) {
        setSelectedValue(bool);
    }

    /**
     * Main class, so you can have an example. You can monitor your own attribute by giving its full
     * path name in argument
     */
    public static void main(String[] args) {
        IPanel panel = new Panel();

        BooleanComboBox widget = new BooleanComboBox();
        BooleanComboBox widget2 = new BooleanComboBox();

        panel.add(widget, BorderLayout.NORTH);
        panel.add(widget2, BorderLayout.SOUTH);

        IFrame frame = new Frame();

        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setTitle("BooleanComboBox test");
    }

}
