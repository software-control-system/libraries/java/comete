/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

import fr.soleil.lib.project.swing.WindowSwingUtils;
import net.miginfocom.swing.MigLayout;

public class ChartViewOptionDialog extends JDialog {

    private static final long serialVersionUID = -225296092264480887L;

    private static final ImageIcon CHART_LINE = new ImageIcon(
            ChartViewOptionDialog.class.getResource("/com/famfamfam/silk/chart_line.png"));
    private static final ImageIcon CHART_CURVE = new ImageIcon(
            ChartViewOptionDialog.class.getResource("/com/famfamfam/silk/chart_curve.png"));
    private static final ImageIcon CHART_PIE = new ImageIcon(
            ChartViewOptionDialog.class.getResource("/com/famfamfam/silk/chart_pie.png"));
    private static final ImageIcon CHART_BAR = new ImageIcon(
            ChartViewOptionDialog.class.getResource("/com/famfamfam/silk/chart_bar.png"));
    private static final ImageIcon ASTERISK_YELLOW = new ImageIcon(
            ChartViewOptionDialog.class.getResource("/com/famfamfam/silk/asterisk_yellow.png"));
    private static final ImageIcon WORLD_ICON = new ImageIcon(
            ChartViewOptionDialog.class.getResource("/com/famfamfam/silk/world.png"));
    private static final ImageIcon FLAG_BLUE = new ImageIcon(
            ChartViewOptionDialog.class.getResource("/com/famfamfam/silk/flag_blue.png"));
    private static final ImageIcon FLAG_GREEN = new ImageIcon(
            ChartViewOptionDialog.class.getResource("/com/famfamfam/silk/flag_green.png"));
    private static final ImageIcon RAINBOW_ICON = new ImageIcon(
            ChartViewOptionDialog.class.getResource("/com/famfamfam/silk/rainbow.png"));
    private static final ImageIcon SHIELD = new ImageIcon(
            ChartViewOptionDialog.class.getResource("/com/famfamfam/silk/shield.png"));
    public static final ImageIcon[] IMAGES = { CHART_LINE, CHART_PIE, CHART_CURVE, CHART_BAR, ASTERISK_YELLOW,
            WORLD_ICON, FLAG_BLUE, FLAG_GREEN, SHIELD, RAINBOW_ICON };

    // Map<Name, SingleChartView>
    private final Map<String, SingleChartView> chartViewMap;

    private SingleChartView selectedView;

    private final JPanel panelMain;
    private final JPanel panelCombo;
    private final JPanel panelBtn;
    private final JLabel labelTitle;
    private final JLabel labelIcon;
    private final JButton boutonOk;
    private final JButton boutonCancel;
    private final JComboBox<String> comboPlot;
    private final JComboBox<ImageIcon> comboIcon;

    public ChartViewOptionDialog(Component owner) {
        super(WindowSwingUtils.getWindowForComponent(owner));
        setModal(true);
        setAlwaysOnTop(true);
        chartViewMap = new HashMap<String, SingleChartView>();
        this.setTitle("Chart Option");
        this.setSize(250, 250);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setModal(true);

        panelMain = new JPanel(new BorderLayout());
        panelCombo = new JPanel(new MigLayout("fill, wrap 2, insets 2"));
        panelBtn = new JPanel();
        labelTitle = new JLabel("Title : ");
        labelIcon = new JLabel("Icon : ");
        boutonOk = new JButton("OK");
        boutonCancel = new JButton("Cancel");

        comboPlot = new JComboBox<>();
        comboPlot.setEditable(true);
        comboIcon = new JComboBox<>();
        for (ImageIcon icon : IMAGES) {
            comboIcon.addItem(icon);
        }

        ComboIconRenderer iconRenderer = new ComboIconRenderer();
        comboIcon.setRenderer(iconRenderer);

        panelMain.add(panelCombo, BorderLayout.CENTER);
        panelCombo.add(labelTitle);
        panelCombo.add(comboPlot);

        panelCombo.add(labelIcon);
        panelCombo.add(comboIcon);

        panelMain.add(panelBtn, BorderLayout.SOUTH);
        panelBtn.add(boutonOk);
        boutonOk.setEnabled(false);
        panelBtn.add(boutonCancel);

        comboPlot.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Object newItem = comboPlot.getSelectedItem();
                int index = comboPlot.getSelectedIndex();
                if ((newItem != null) && (index == -1)) {
                    String itemToString = newItem.toString();
                    // auto add or auto selected item
                    Object item = null;
                    boolean found = false;
                    for (int i = 0; i < comboPlot.getItemCount(); i++) {
                        item = comboPlot.getItemAt(i);
                        if ((item != null) && itemToString.equalsIgnoreCase(newItem.toString())) {
                            found = true;
                            index = i;
                            break;
                        }
                    }
                    if (found) {
                        // auto select item
                        synchronized (this) {
                            comboPlot.removeItemListener(this);
                            comboPlot.setSelectedIndex(index);
                            comboPlot.addItemListener(this);
                        }
                    } else {
                        // auto add missing item
                        comboPlot.addItem(itemToString);
                    }
                }
                boutonOk.setEnabled(comboPlot.getItemCount() > 0);
            }
        });

        boutonOk.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Object item = comboPlot.getSelectedItem();
                // System.out.println("getSelectedItem = " + item);
                // System.out.println("comboPlot = " + comboPlot.getComponentCount());
                if (item != null) {
                    String title = item.toString();
                    ImageIcon icon = (ImageIcon) comboIcon.getSelectedItem();
                    if (chartViewMap.containsKey(title)) {
                        selectedView = chartViewMap.get(title);
                    } else {

                        selectedView = new SingleChartView();
                        selectedView.setTitle(title);
                        chartViewMap.put(title, selectedView);

                    }
                    selectedView.setIconImage(icon);
                }
                setVisible(false);
            }
        });
        boutonCancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                selectedView = null;
                setVisible(false);
            }
        });

        this.setContentPane(panelMain);

    }

    private class ComboIconRenderer extends DefaultListCellRenderer {

        private static final long serialVersionUID = 8173124003387100907L;

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
                boolean cellHasFocus) {
            Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if ((comp instanceof JLabel) && (value instanceof ImageIcon)) {
                ((JLabel) comp).setIcon((ImageIcon) value);
            }
            return comp;
        }
    }

    public SingleChartView getSelectedView() {
        return selectedView;
    }

    public void removeSingleChartView(String title) {
        if (chartViewMap.containsKey(title)) {
            chartViewMap.remove(title);
        }
        comboPlot.removeItem(title);
    }

    public static void main(String[] args) {
        ChartViewOptionDialog chart = new ChartViewOptionDialog(null);
        chart.setVisible(true);
    }

}
