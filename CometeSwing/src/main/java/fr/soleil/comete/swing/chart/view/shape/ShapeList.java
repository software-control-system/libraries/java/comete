/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.shape;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.definition.widget.util.IChartConst.ShapeType;

/**
 * {@link AbstractShape} specialized in shape combinations
 * 
 * @author huriez
 */
public class ShapeList<T extends AbstractShape> extends AbstractShape {

    private final Class<T> shapeClass;

    private List<T> shapes = new ArrayList<T>();

    public ShapeList(ShapeType type, Class<T> shapeClass) {
        super(type);
        this.shapeClass = shapeClass;
    }

    public void add(T shape) {
        if (shape != null) {
            shapes.add(shape);
        }
    }

    @Override
    public void drawShape(Graphics2D g) {
        g.translate(x, y);
        for (AbstractShape shape : shapes) {
            shape.drawShape(g);
        }
        g.translate(-x, -y);
    }

    public boolean isEmpty() {
        return shapes.isEmpty();
    }

    public int size() {
        return shapes.size();
    }

    public AbstractShape get(int index) {
        return shapes.get(index);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getClass().getSimpleName());
        builder.append("<");
        builder.append(shapeClass == null ? AbstractShape.class : shapeClass);
        builder.append(">: [");
        if (shapes.size() > 0) {
            for (AbstractShape shape : shapes) {
                builder.append(shape).append(", ");
            }
            builder.delete(builder.length() - 2, builder.length());
        }
        builder.append("]");
        return builder.toString();
    }

}
