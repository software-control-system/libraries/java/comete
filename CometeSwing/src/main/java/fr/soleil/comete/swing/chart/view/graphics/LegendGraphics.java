/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.graphics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.util.Collection;
import java.util.List;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.util.LabelRect;
import fr.soleil.comete.swing.chart.view.shape.AbstractShape;
import fr.soleil.comete.swing.chart.view.shape.ShapeGenerator;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.xmlhelpers.HtmlEscape;

public class LegendGraphics implements CometeConstants {

    /**
     * Appends the name to display in legend for a given {@link AbstractDataView} to a {@link StringBuilder}.
     * 
     * @param builder The {@link StringBuilder}.
     * @param v The {@link AbstractDataView}.
     * @param axisName The name of the axis the {@link AbstractDataView} is linked with.
     * @param parent Parent {@link IChartViewer}.
     * @param escapeHtml Whether to escape html special characters
     * @return The {@link StringBuilder}, with the display name appended to it. If builder was null, It returns a new
     *         {@link StringBuilder} containing the display name.
     */
    public static StringBuilder appendDisplayabledName(StringBuilder builder, AbstractDataView v, String axisName,
            IChartViewer parent, boolean escapeHtml) {
        StringBuilder result = builder;
        if (result == null) {
            result = new StringBuilder();
        }
        if (v != null) {
            if (escapeHtml) {
                result.append(HtmlEscape.escape(v.getExtendedName(parent), true, true));
            } else {
                result.append(v.getExtendedName(parent));
            }
            if ((axisName != null)) {
                String name = axisName.trim();
                if (!name.isEmpty()) {
                    if (escapeHtml) {
                        name = HtmlEscape.escape(name, true, true);
                    }
                    result.append(SPACE);
                    if (name.startsWith(OPEN_PAR) && name.endsWith(CLOSE_PAR)) {
                        result.append(name);
                    } else {
                        result.append(OPEN_PAR).append(name).append(CLOSE_PAR);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the name to display in legend for a given {@link AbstractDataView}.
     * 
     * @param v The {@link AbstractDataView}.
     * @param axisName The name of the axis the {@link AbstractDataView} is linked with.
     * @param parent Parent {@link IChartViewer}.
     * @param escapeHtml Whether to escape html special characters.
     * @return A {@link String}.
     */
    public static String getDisplayableName(AbstractDataView v, String axisName, IChartViewer parent,
            boolean escapeHtml) {
        return appendDisplayabledName(null, v, axisName, parent, escapeHtml).toString();
    }

    /**
     * Expert usage. Draw a sample line of a dataview.
     * 
     * @param g {@link Graphics2D} in which to draw the sample line.
     * @param x x coordinates (pixel space).
     * @param y y coordinates (pixel space).
     * @param v The dataview.
     */
    public static void drawSampleLine(Graphics2D g, int x, int y, AbstractDataView v) {
        Color oldColor = g.getColor();
        Stroke oldStroke = g.getStroke();
        BasicStroke bs = CometeUtils.createStrokeForLine(v.getAdaptedLineWidth(), v.getStyle());
        // Draw
        if ((v.getViewType() == IChartViewer.TYPE_LINE) || (v.getViewType() == IChartViewer.TYPE_STAIRS)) {
            AbstractShape shape = ShapeGenerator.generateMarkerShape(v.getMarker(), v.getAdaptedMarkerSize(), x + 20, y,
                    v.getMarkerColor());
            if (shape != null) {
                shape.drawShape(g);
            }
            if (v.getAdaptedLineWidth() > 0) {
                g.setColor(v.getColor());
                if (bs != null) {
                    g.setStroke(bs);
                }
                g.drawLine(x, y, x + 40, y);
            }
        } else if (v.getViewType() == IChartViewer.TYPE_BAR) {
            if (v.getFillStyle() != IChartViewer.FILL_STYLE_NONE) {
                g.setColor(v.getFillColor());
                g.fillRect(x + 16, y - 4, 8, 8);
            }
            g.setColor(v.getColor());
            if (bs != null) {
                g.setStroke(bs);
            }
            if (v.getAdaptedLineWidth() > 0) {
                g.drawLine(x + 16, y - 4, x + 24, y - 4);
                g.drawLine(x + 24, y - 4, x + 24, y + 4);
                g.drawLine(x + 24, y + 4, x + 16, y + 4);
                g.drawLine(x + 16, y + 4, x + 16, y - 4);
            }
        }
        // Restore
        g.setStroke(oldStroke);
        g.setColor(oldColor);
    }

    /**
     * Draws the legend of an {@link AbstractDataView}.
     * 
     * @param g The {@link Graphics2D} in which to paint the {@link AbstractDataView} representation.
     * @param v The {@link AbstractDataView}.
     * @param axisName The name of the axis the {@link AbstractDataView} is linked with.
     * @param x The x coordinate at which to paint the representation.
     * @param y The y coordinate at which to paint the representation.
     * @param w The representation width.
     * @param a The representation ascent.
     */
    protected static void drawDataViewLegend(Graphics2D g, Font noSamplingFont, AbstractDataView v, IChartViewer parent,
            String axisName, int x, int y, int w, int a, List<LabelRect> labelRect, int labelWidth, int labelHeight) {
        int xm = x + (w - labelWidth) / 2 + 7;
        int ym = y + labelHeight / 2 + 2;
        g.setColor(v.getColor());
        drawSampleLine(g, xm, ym - 2, v);
        g.setColor(v.getLabelColor());
        Font font = g.getFont();
        boolean useSamplingFont = ((noSamplingFont != null) && (!v.isSamplingAllowed()));
        if (useSamplingFont) {
            g.setFont(noSamplingFont);
        }
        g.drawString(getDisplayableName(v, axisName, parent, false), xm + 44, ym + labelHeight - a);
        if (useSamplingFont) {
            g.setFont(font);
        }
        synchronized (labelRect) {
            labelRect.add(new LabelRect(xm, ym - a, labelWidth, labelHeight, v));
        }
    }

    /**
     * Draws the legend on prepared {@link Graphics2D}.
     * 
     * @param g The {@link Graphics2D}.
     * @param parent The parent {@link IChartViewer}.
     * @param y1Views The {@link AbstractDataView}s on Y1 axis.
     * @param y2Views The {@link AbstractDataView}s on Y2 axis.
     * @param legendFont The default legend font.
     * @param noSamplingFont The font to use for "no sampling" case.
     * @param samplingEnabled Whether sampling is enabled.
     * @param labelRect The {@link Collection} of {@link LabelRect} that may contain the {@link AbstractDataView}'s
     *            representations
     * @param labelR The {@link Rectangle} that represents the legend drawing zone.
     * @param labelPerLine The number of labels per line.
     * @param labelWidth The previously computed label width.
     * @param labelHeight The previously computed label height.
     * @param nbLabel The number of labels.
     * @param chartLegendPlacement The legend placement.
     * @param y1Name The Y1 axis name.
     * @param y2Name The Y2 axis name.
     */
    public static void paintLegend(Graphics2D g, IChartViewer parent, Collection<? extends AbstractDataView> y1Views,
            Collection<? extends AbstractDataView> y2Views, Font legendFont, Font noSamplingFont,
            boolean samplingEnabled, List<LabelRect> labelRect, Rectangle labelR, int labelPerLine, int labelWidth,
            int labelHeight, int nbLabel, int chartLegendPlacement, String y1Name, String y2Name) {
        // Draw labels
        synchronized (labelRect) {
            labelRect.clear();
        }
        synchronized (labelR) {
            if (labelR.width > 0) {
                if (!samplingEnabled) {
                    noSamplingFont = legendFont;
                }
                g.setFont(legendFont);
                int a = g.getFontMetrics(legendFont).getAscent();
                if ((noSamplingFont != null) && (!noSamplingFont.equals(legendFont))) {
                    a = Math.max(a, g.getFontMetrics(noSamplingFont).getAscent());
                }
                int viewIndex = 0;
                if (chartLegendPlacement == IChartViewer.LABEL_ROW && labelPerLine > 1) {
                    int rowWidth = labelR.width / labelPerLine;
                    // Draw labels (in row/column)
                    if (y1Views != null) {
                        for (AbstractDataView v : y1Views) {
                            if (v.isLabelVisible()) {
                                int x = (viewIndex % labelPerLine) * rowWidth + labelR.x;
                                int y = (viewIndex / labelPerLine) * labelHeight + labelR.y;
                                drawDataViewLegend(g, noSamplingFont, v, parent, y1Name, x, y, rowWidth, a, labelRect,
                                        labelWidth, labelHeight);
                                viewIndex++;
                            }
                        }
                    }
                    if (y2Views != null) {
                        for (AbstractDataView v : y2Views) {
                            if (v.isLabelVisible()) {
                                int x = (viewIndex % labelPerLine) * rowWidth + labelR.x;
                                int y = (viewIndex / labelPerLine) * labelHeight + labelR.y;
                                drawDataViewLegend(g, noSamplingFont, v, parent, y2Name, x, y, rowWidth, a, labelRect,
                                        labelWidth, labelHeight);
                                viewIndex++;
                            }
                        }
                    }
                } else {
                    // Draw labels (in column)
                    if (y1Views != null) {
                        for (AbstractDataView v : y1Views) {
                            if (v.isLabelVisible()) {
                                int y = labelR.y + (labelR.height - nbLabel * labelHeight) / 2
                                        + labelHeight * viewIndex;
                                drawDataViewLegend(g, noSamplingFont, v, parent, y1Name, labelR.x, y, labelR.width, a,
                                        labelRect, labelWidth, labelHeight);
                                viewIndex++;
                            }
                        }
                    }
                    if (y2Views != null) {
                        for (AbstractDataView v : y2Views) {
                            if (v.isLabelVisible()) {
                                int y = labelR.y + (labelR.height - nbLabel * labelHeight) / 2
                                        + labelHeight * viewIndex;
                                drawDataViewLegend(g, noSamplingFont, v, parent, y2Name, labelR.x, y, labelR.width, a,
                                        labelRect, labelWidth, labelHeight);
                                viewIndex++;
                            }
                        }
                    }
                }
            }
        }
    }

}
