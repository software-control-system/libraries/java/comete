/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.component;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ToolTipManager;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.util.LabelRect;
import fr.soleil.comete.swing.chart.util.PlotPropertiesTool;
import fr.soleil.comete.swing.chart.view.graphics.LegendGraphics;

/**
 * Abstract class for chart legends.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AbstractChartLegend extends JComponent
        implements MouseListener, MouseMotionListener, ComponentListener, HierarchyListener, ActionListener {

    private static final long serialVersionUID = -4094861975363660020L;

    protected static final String KO_PAINT_STRING = "Unsupported Graphics";

    private final List<LabelRect> labelRect;
    private final Rectangle labelR;
    private int labelHeight;
    private int labelWidth;
    private int labelPerLine;
    private int nbLabel;
    private String idToHighlight;
    private boolean wasHighlighted;
    private Dimension preferredSize;
    private final JPopupMenu dataViewMenu;
    private final JMenuItem removeDataViewMenuItem;
    private final JMenuItem dataViewOptionMenuItem;
    protected AbstractDataView currentView;

    private Container lastKnownParent;

    protected Collection<? extends AbstractDataView> y1Views;
    protected Collection<? extends AbstractDataView> y2Views;

    public AbstractChartLegend() {
        super();
        currentView = null;
        wasHighlighted = false;
        preferredSize = null;
        labelRect = new ArrayList<>();
        labelR = new Rectangle();
        dataViewMenu = new JPopupMenu();
        removeDataViewMenuItem = new JMenuItem("Remove: ");
        removeDataViewMenuItem.addActionListener(this);
        dataViewOptionMenuItem = new JMenuItem("Options: ");
        dataViewOptionMenuItem.addActionListener(this);
        dataViewMenu.add(removeDataViewMenuItem);
        dataViewMenu.add(dataViewOptionMenuItem);
        setOpaque(true);
        addMouseListener(this);
        addMouseMotionListener(this);
        addHierarchyListener(this);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension size;
        if (preferredSize == null) {
            computePreferredSize();
        }
        size = preferredSize == null ? super.getPreferredSize() : preferredSize;
        return size;
    }

    @Override
    public void revalidate() {
        computePreferredSize();
        super.revalidate();
    }

    @Override
    public void paint(Graphics g) {
        if (g instanceof Graphics2D) {
            Graphics2D g2 = (Graphics2D) g;
            Color formerColor = g2.getColor();
            int w = getWidth();
            int h = getHeight();
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            g2.setPaintMode();
            if (isOpaque()) {
                g2.setColor(getBackground());
                g2.fillRect(0, 0, w, h);
            }
            LegendGraphics.paintLegend(g2, recoverChartViewer(), y1Views, y2Views, getLegendFont(), getNoSamplingFont(),
                    isSamplingEnabled(), labelRect, labelR, labelPerLine, labelWidth, labelHeight, nbLabel,
                    getChartLegendPlacement(), getY1Name(), getY2Name());
            g2.setColor(formerColor);
            repaintChartIfNecessary();
        } else {
            g.drawString(KO_PAINT_STRING, 5, 5);
        }
    }

    /**
     * Repaints associated chart if necessary.
     */
    protected abstract void repaintChartIfNecessary();

    /**
     * Calculates the preferred size.
     */
    protected void computePreferredSize() {
        Graphics g = getGraphics();
        if ((g != null) && (g instanceof Graphics2D)) {
            measureLabelItems((Graphics2D) g);
            synchronized (labelR) {
                preferredSize = new Dimension(labelR.x + labelR.width, labelR.y + labelR.height);
            }
        } else {
            preferredSize = null;
        }
    }

    protected abstract boolean isChartLegendVisible();

    /**
     * Returns the {@link Font} to use in legend.
     * 
     * @return A {@link Font}.
     */
    protected abstract Font getLegendFont();

    /**
     * Returns a {@link Font} to use in legend, when an {@link AbstractDataView} does not allow sampling.
     * 
     * @return A {@link Font}.
     */
    protected Font getNoSamplingFont() {
        // by default, return legend Font
        return getLegendFont();
    }

    protected abstract int getChartLegendPlacement();

    protected abstract Dimension getMarginSize();

    /**
     * Returns the Y1 axis name.
     * 
     * @return A {@link String}.
     */
    protected abstract String getY1Name();

    /**
     * Returns the Y2 axis name.
     * 
     * @return A {@link String}.
     */
    protected abstract String getY2Name();

    protected int computeNbLabels(Collection<? extends AbstractDataView> views, int currentNbLabel) {
        int nbLabel = currentNbLabel;
        if (views != null) {
            for (AbstractDataView view : views) {
                if (view.isLabelVisible()) {
                    nbLabel++;
                }
            }
        }
        return nbLabel;
    }

    /**
     * Updates known y1 and y2 view lists.
     */
    protected abstract void computeY1AndY2ViewLists();

    protected boolean isSamplingEnabled() {
        return false;
    }

    /**
     * Recovers surrounding {@link IChartViewer}.
     * 
     * @return An {@link IChartViewer}.
     */
    protected IChartViewer recoverChartViewer() {
        IChartViewer viewer = null;
        Container parent = getParent();
        while (parent != null) {
            if (parent instanceof IChartViewer) {
                viewer = (IChartViewer) parent;
                break;
            }
            parent = parent.getParent();
        }
        return viewer;
    }

    /**
     * Analyzes the legends to draw to compute size calculations.
     * 
     * @param g The {@link Graphics2D} in which to draw the legends.
     */
    private void measureLabelItems(Graphics2D g) {
        int labelTHeight = 0; // Total label height
        Dimension size = getMarginSize();
        int marginWidth = size.width;
        int marginHeight = size.height;
        int w = getWidth();
        IChartViewer parent = recoverChartViewer();
        FontRenderContext frc = g.getFontRenderContext();
        synchronized (labelR) {
            labelR.setBounds(0, 0, 0, 0);
        }
        labelWidth = 0;
        // ------------------------------------------------------
        // Compute label number
        // ------------------------------------------------------
        nbLabel = 0;
        computeY1AndY2ViewLists();
        nbLabel = computeNbLabels(y2Views, computeNbLabels(y1Views, nbLabel));

        Font legendFont = getLegendFont();
        Font noSamplingFont = (isSamplingEnabled() ? getNoSamplingFont() : legendFont);
        if (isSamplingEnabled()) {
            // No need to use special Font when there is no dataview with sampling deactivated
            boolean hasNoSampling = false;
            if (y1Views != null) {
                for (AbstractDataView view : y1Views) {
                    if ((view != null) && !view.isSamplingAllowed()) {
                        hasNoSampling = true;
                        break;
                    }
                }
            }
            if ((y2Views != null) && !hasNoSampling) {
                for (AbstractDataView view : y2Views) {
                    if ((view != null) && !view.isSamplingAllowed()) {
                        hasNoSampling = true;
                        break;
                    }
                }
            }
            if (!hasNoSampling) {
                noSamplingFont = legendFont;
            }
        }

        // ------------------------------------------------------
        // Measure labels
        // ------------------------------------------------------
        ToolTipManager manager = ToolTipManager.sharedInstance();
        boolean needToRegister = (super.getToolTipText() == null);
        if (needToRegister) {
            manager.unregisterComponent(this);
        }
        if (isChartLegendVisible() && (nbLabel > 0) && (legendFont != null)) {
            double[] maxLabelBounds = new double[2]; // {maxLabelWidth, maxLabelHeight}
            final Collection<? extends AbstractDataView> y1Views = this.y1Views;
            final Collection<? extends AbstractDataView> y2Views = this.y2Views;
            if (y1Views != null) {
                needToRegister = updateMaxLabelBounds(maxLabelBounds, y1Views, parent, legendFont, noSamplingFont, frc,
                        getY1Name(), manager, needToRegister);
            }
            if (y2Views != null) {
                needToRegister = updateMaxLabelBounds(maxLabelBounds, y2Views, parent, legendFont, noSamplingFont, frc,
                        getY2Name(), manager, needToRegister);
            }
            // sample line height & margin
            labelHeight = (int) (Math.round(maxLabelBounds[1]) + 2);
            labelTHeight = (labelHeight * nbLabel) + 10;
            // sample line width & margin
            labelWidth = (int) (Math.round(maxLabelBounds[0]) + 55);

            switch (getChartLegendPlacement()) {
                case IChartViewer.LABEL_UP:
                case IChartViewer.LABEL_DOWN:
                case IChartViewer.LABEL_RIGHT:
                case IChartViewer.LABEL_LEFT:
                    labelPerLine = 1;
                    break;
                case IChartViewer.LABEL_ROW:
                    if (labelWidth > 0) {
                        labelPerLine = Math.max(w / labelWidth, 1);
                    } else {
                        labelPerLine = 1;
                    }
                    if (labelPerLine > nbLabel) {
                        labelPerLine = Math.max(nbLabel, 1);
                    }
                    labelTHeight = labelHeight * (nbLabel / labelPerLine);
                    if (nbLabel % labelPerLine != 0) {
                        labelTHeight += labelHeight;
                    }
                    break;
            }
            synchronized (labelR) {
                labelR.setBounds(marginWidth, marginHeight, labelPerLine * labelWidth, labelTHeight);
            }
        }
    }

    /**
     * Updates maximum label bounds, depending on some data views.
     * 
     * @param maxLabelBounds A array representing the current maximum label bounds:
     *            <code>{maxLabelWidth, maxLabelHeight}</code>.
     * @param yViews The data views
     * @param legendFont The legend font.
     * @param noSamplingFont the no sampling font.
     * @param frc The {@link FontRenderContext} to use.
     * @param yName The y name to use.
     * @param manager The current {@link ToolTipManager}.
     * @param needToRegister Whether this {@link AbstractChartLegend} should be registered in {@link ToolTipManager}.
     * @return Whether this {@link AbstractChartLegend} should still be registered in {@link ToolTipManager}.
     */
    protected boolean updateMaxLabelBounds(double[] maxLabelBounds, Collection<? extends AbstractDataView> yViews,
            IChartViewer parent, Font legendFont, Font noSamplingFont, FontRenderContext frc, String yName,
            ToolTipManager manager, boolean needToRegister) {
        Rectangle2D bounds;
        for (AbstractDataView v : yViews) {
            if (v.isLabelVisible()) {
                if (needToRegister && (!v.isSamplingAllowed())) {
                    needToRegister = false;
                    manager.registerComponent(this);
                }
                bounds = legendFont.getStringBounds(LegendGraphics.getDisplayableName(v, yName, parent, false), frc);
                if (bounds.getWidth() > maxLabelBounds[0]) {
                    maxLabelBounds[0] = bounds.getWidth();
                }
                if (bounds.getHeight() > maxLabelBounds[1]) {
                    maxLabelBounds[1] = bounds.getHeight();
                }
                if ((noSamplingFont != null) && (!noSamplingFont.equals(legendFont))) {
                    bounds = noSamplingFont.getStringBounds(LegendGraphics.getDisplayableName(v, yName, parent, false),
                            frc);
                    if (bounds.getWidth() > maxLabelBounds[0]) {
                        maxLabelBounds[0] = bounds.getWidth();
                    }
                    if (bounds.getHeight() > maxLabelBounds[1]) {
                        maxLabelBounds[1] = bounds.getHeight();
                    }
                }
            }
        }
        return needToRegister;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == this) {
            highlightAt(e.getX(), e.getY());
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (isAutoHighlightOnLegend()) {
            unHilightView();
            repaint();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (isDataViewRemovingEnabled()
                && (e.getButton() == MouseEvent.BUTTON2 || e.getButton() == MouseEvent.BUTTON3)) {
            displayPopupMenu(e);
        } else {
            // Click on label
            currentView = getDataViewAt(e.getX(), e.getY());
            if ((currentView != null) && isDataViewClickable(currentView.getId())) {
                if (isAutoHighlightOnLegend()) {
                    unHilightView();
                    repaint();
                }
                // Display the Dataview options
                editDataView();
            }
            currentView = null;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (e.getSource() == this) {
            highlightAt(e.getX(), e.getY());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == dataViewOptionMenuItem) {
            editDataView();
        } else if (e.getSource() == removeDataViewMenuItem) {
            deleteDataView();
        }
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        // nothing to do
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        // nothing to do
    }

    @Override
    public void componentResized(ComponentEvent e) {
        updatePreferredSizeFromParent();
    }

    @Override
    public void componentShown(ComponentEvent e) {
        // nothing to do
    }

    /**
     * Recovers the {@link AbstractDataView} the legend of which is displayed at a given coordinate.
     * 
     * @param x The x coordinate.
     * @param y The y coordinate.
     * @return An {@link AbstractDataView}. <code>null</code> If no {@link AbstractDataView} could be found.
     */
    private AbstractDataView getDataViewAt(int x, int y) {
        AbstractDataView result = null;
        synchronized (labelRect) {
            for (LabelRect r : labelRect) {
                if (r.getRect().contains(x, y)) {
                    result = r.getView();
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Displays the legend popup menu.
     * 
     * @param e The {@link MouseEvent} that allowed popup menu click detection.
     */
    private void displayPopupMenu(MouseEvent e) {
        if (e != null) {
            currentView = getDataViewAt(e.getX(), e.getY());
            if ((currentView != null) && isDataViewClickable(currentView.getId())) {
                if (isAutoHighlightOnLegend()) {
                    unHilightView();
                    repaint();
                }
                dataViewOptionMenuItem.setText("Options: " + currentView.getDisplayName());
                dataViewOptionMenuItem.setBackground(getBackground());
                dataViewOptionMenuItem.setForeground(currentView.getColor());
                removeDataViewMenuItem.setText("Remove: " + currentView.getDisplayName());
                removeDataViewMenuItem.setBackground(getBackground());
                removeDataViewMenuItem.setForeground(currentView.getColor());
                dataViewMenu.setBackground(getBackground());
                dataViewMenu.show(this, e.getX(), e.getY());
            }
        }
    }

    /**
     * Shows the hovered {@link AbstractDataView}'s property editor
     */
    private void editDataView() {
        if (currentView != null) {
            showDataOptionDialog(currentView);
        }
        currentView = null;
    }

    /**
     * Deletes the hovered {@link AbstractDataView}
     */
    protected abstract void deleteDataView();

    @Override
    public void hierarchyChanged(HierarchyEvent e) {
        if (lastKnownParent != null) {
            lastKnownParent.removeComponentListener(this);
        }
        lastKnownParent = getParent();
        if (lastKnownParent instanceof JViewport) {
            JViewport viewport = (JViewport) lastKnownParent;
            lastKnownParent = viewport.getParent();
        }
        if (lastKnownParent != null) {
            lastKnownParent.removeComponentListener(this);
            lastKnownParent.addComponentListener(this);
        }
        updatePreferredSizeFromParent();
    }

    /**
     * Forces preferred size calculation
     */
    private void updatePreferredSizeFromParent() {
        if (lastKnownParent instanceof JScrollPane) {
            Dimension mySize = getSize();
            Dimension parentSize = lastKnownParent.getSize();
            if (getChartLegendPlacement() == IChartViewer.LABEL_ROW) {
                setSize(parentSize.width, mySize.height);
            }
        }
        revalidate();
        if (isVisible()) {
            repaint();
        }
    }

    /**
     * Highlights the {@link AbstractDataView}'s legend that correspond to a given coordinate
     * 
     * @param x The x coordinate
     * @param y The y coordinate
     */
    private void highlightAt(int x, int y) {
        if (isAutoHighlightOnLegend()) {
            AbstractDataView view = getDataViewAt(x, y);
            if (view == null) {
                unHilightView();
            } else {
                if (!view.getId().equals(idToHighlight)) {
                    unHilightView();
                    idToHighlight = view.getId();
                    wasHighlighted = isDataViewHighlighted(idToHighlight);
                    view.setHighlighted(true);
                    setDataViewHighlighted(view.getId(), true);
                }
            }
            repaint();
        }
    }

    @Override
    public String getToolTipText() {
        Point p = getMousePosition();
        String text;
        if (p == null) {
            text = super.getToolTipText();
        } else {
            text = getTooltipText(p.x, p.y, recoverChartViewer());
        }
        return text;
    }

    @Override
    public String getToolTipText(MouseEvent event) {
        String text;
        if (event == null) {
            text = super.getToolTipText();
        } else {
            text = getTooltipText(event.getX(), event.getY(), recoverChartViewer());
        }
        return text;
    }

    protected String getTooltipText(int x, int y, IChartViewer parent) {
        String text;
        AbstractDataView dataView = getDataViewAt(x, y);
        if ((dataView != null) && (!dataView.isSamplingAllowed())) {
            text = LegendGraphics
                    .appendDisplayabledName(new StringBuilder("<html><body>"), dataView,
                            PlotPropertiesTool.isY1View(dataView) ? getY1Name() : getY2Name(), parent, true)
                    .append(" <span style=\"font-weight:bold; color:red;\">[SAMPLING DISABLED]</span></body></html>")
                    .toString();
        } else {
            text = super.getToolTipText();
        }
        return text;
    }

    /**
     * Unhighlights last highlighted {@link AbstractDataView}
     */
    private void unHilightView() {
        if (idToHighlight != null) {
            setDataViewHighlighted(idToHighlight, wasHighlighted);
            idToHighlight = null;
            wasHighlighted = false;
        }
    }

    /**
     * Returns whether {@link AbstractDataView} removing is allowed through legend.
     * 
     * @return A <code>boolean</code> value.
     */
    protected abstract boolean isDataViewRemovingEnabled();

    /**
     * Returns whether the legend of a given {@link AbstractDataView} is clickable.
     * 
     * @param id The {@link AbstractDataView} id.
     * @return A <code>boolean</code> value.
     */
    protected abstract boolean isDataViewClickable(String id);

    /**
     * Displays the property editor for a given {@link AbstractDataView}.
     * 
     * @param view The {@link AbstractDataView}.
     */
    protected abstract void showDataOptionDialog(AbstractDataView view);

    /**
     * Returns whether legend highlighting is allowed.
     * 
     * @return A <code>boolean</code> value.
     */
    protected abstract boolean isAutoHighlightOnLegend();

    /**
     * Returns whether an {@link AbstractDataView}'s legend is highlighted.
     * 
     * @param idToHighlight The {@link AbstractDataView} id.
     * @return A <code>boolean</code> value.
     */
    protected abstract boolean isDataViewHighlighted(String idToHighlight);

    /**
     * Highlights or unhighlights an {@link AbstractDataView}'s legend.
     * 
     * @param idToHighlight The {@link AbstractDataView} id.
     * @param highlighted
     *            Whether to highlight the {@link AbstractDataView}'s legend.
     */
    protected abstract void setDataViewHighlighted(String idToHighlight, boolean highlighted);

}
