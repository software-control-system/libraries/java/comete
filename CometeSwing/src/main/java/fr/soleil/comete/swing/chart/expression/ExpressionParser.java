/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.expression;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.DataList;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.expression.IExpressionParser;
import fr.soleil.lib.project.expression.jeval.JEvalExpressionParser;

//based on TabbedLine (from JLChart)
public class ExpressionParser {

    protected static final String FAILED_TO_BUILD_DATAVIEW = "Failed to build dataview";

    protected AbstractDataView[] dv;
    protected DataList[] dl;
    protected int anno;
    protected int precision = 0;
    protected final IExpressionParser expressionParser;
    protected String expression;
    protected boolean x = false;

    public ExpressionParser(int nb, String expr, IExpressionParser expressionParser) {
        this.expressionParser = (expressionParser == null ? new JEvalExpressionParser() : expressionParser);
        dv = new AbstractDataView[nb];
        dl = new DataList[nb];
        expression = expr;
    }

    public void setPrecision(int milliseconds) {
        precision = milliseconds;
    }

    public void add(int id, AbstractDataView v) {
        dv[id] = v;
        dl[id] = v.getData();
    }

    protected double getMinTime() {
        double r = Double.MAX_VALUE;
        for (DataList element : dl) {
            if (element != null) {
                if (element.getX() < r) {
                    r = element.getX();
                }
            }
        }
        return r;
    }

    protected void initialize() {
        if (x) {
            expressionParser.addVariable("x", Double.valueOf(Double.NaN));
        } else {
            for (int i = 0; i < dv.length; i++) {
                expressionParser.addVariable("x" + (i + 1), Double.valueOf(Double.NaN));
            }
        }
    }

    protected double[] getNextValues() {
        double t0 = getMinTime();
        // Test end of data
        if (t0 == Double.MAX_VALUE) {
            return null;
        }
        double[] result = new double[2];
        result[0] = t0;
        if (x) {
            if (dl[0] != null) {
                if ((dl[0].getX() >= t0 - precision) && (dl[0].getX() <= t0 + precision)) {
                    expressionParser.removeVariable("x");
                    expressionParser.addVariable("x", Double.valueOf(dl[0].getY()));
                    dl[0] = dl[0].next;
                }
            }
        } else {
            for (int i = 0; i < dl.length; i++) {
                if (dl[i] != null) {
                    if ((dl[i].getX() >= t0 - precision) && (dl[i].getX() <= t0 + precision)) {
                        expressionParser.removeVariable("x" + (i + 1));
                        expressionParser.addVariable("x" + (i + 1), Double.valueOf(dl[i].getY()));
                        dl[i] = dl[i].next;
                    }
                }
            }
        }
        try {
            result[1] = expressionParser.parseExpression(expression);
        } catch (Exception e) {
            result[1] = Double.NaN;
        }
        return result;
    }

    public <DV extends AbstractDataView> DV buildDataView(DV initDv, Class<DV> dvClass) {
        DV dv;
        try {
            dv = initDv == null ? dvClass.newInstance() : initDv;
        } catch (InstantiationException | IllegalAccessException e) {
            dv = null;
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(FAILED_TO_BUILD_DATAVIEW, e);
        }
        return buildExpressionDataView(dv);
    }

    protected <DV extends AbstractDataView> DV buildExpressionDataView(DV initDv) {
        initialize();
        DV dataView = initDv;
        if (dataView != null) {
            double[] nextValues = getNextValues();
            while (nextValues != null) {
                dataView.add(nextValues[0], nextValues[1]);
                nextValues = getNextValues();
            }
        }
        return dataView;
    }

    public void clean() {
        if (dv != null) {
            for (int i = 0; i < dv.length; i++) {
                dv[i] = null;
                dl[i] = null;
            }
            dv = null;
            dl = null;
        }
        // evaluator = null;
        expression = null;
        expressionParser.clean();
    }

    public boolean isX() {
        return x;
    }

    public void setX(boolean x) {
        this.x = x;
    }
}
