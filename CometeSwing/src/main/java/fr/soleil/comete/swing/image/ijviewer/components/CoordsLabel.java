/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.awt.Point;
import java.awt.geom.Point2D;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.events.IIJViewerListener;
import fr.soleil.comete.swing.image.ijviewer.events.IJViewerEvent;
import fr.soleil.lib.project.Format;

/**
 * 
 * 
 * @author MAINGUY, Rapha&euml;l GIRARDOT
 * 
 */
public class CoordsLabel extends JLabel implements IIJViewerListener {

    private static final long serialVersionUID = -8219151226046991300L;

    private static final String AXES = "axes: [";
    private static final String AXES_SEPARATOR = ", ";
    private static final String AXES_END = "] = ";
    private static final String NAN_VALUE = "NaN (";
    private static final String NAN_VALUE_END = ")";
    private static final String DEFAULT_VALUE = "axes: [out]";
    private static final String DEFAULT_VALUE_FORMAT = "%s";
    private static final String DEFAULT_X_FORMAT = "%.2f";
    private static final String DEFAULT_Y_FORMAT = "%.2f";

    private String valueFormat, xAxisFormat, yAxisFormat;

    /**
     * Constructor
     */
    public CoordsLabel() {
        super(DEFAULT_VALUE);
        valueFormat = DEFAULT_VALUE_FORMAT;
        xAxisFormat = DEFAULT_X_FORMAT;
        yAxisFormat = DEFAULT_Y_FORMAT;
    }

    private void updateMouseInfo(ImageViewer source) {
        final Point imagePosition;
        final Point2D.Double axisPosition;
        final Number imageValue;
        final boolean isHidden;
        if (source == null) {
            imagePosition = null;
            axisPosition = null;
        } else {
            imagePosition = source.getImagePosition();
            axisPosition = source.getAxisPosition();
        }
        final boolean outOfPicture = ((imagePosition == null) || (axisPosition == null));
        if (outOfPicture) {
            imageValue = null;
            isHidden = false;
        } else {
            imageValue = source.getImageValue(imagePosition);
            isHidden = source.isHidden(imagePosition.x, imagePosition.y);
        }
        SwingUtilities.invokeLater(() -> {
            if (outOfPicture || (imageValue == null)) {
                setText(DEFAULT_VALUE);
            } else {
                StringBuilder builder = new StringBuilder();
                builder.append(AXES);
                builder.append(Format.formatValue(axisPosition.getX(), xAxisFormat, false));
                builder.append(AXES_SEPARATOR);
                builder.append(Format.formatValue(axisPosition.getY(), yAxisFormat, false));
                builder.append(AXES_END);
                String val;
                try {
                    val = Format.format(valueFormat, imageValue);
                } catch (Exception e) {
                    val = String.valueOf(imageValue);
                }
                if (isHidden) {
                    builder.append(NAN_VALUE);
                    builder.append(val);
                    builder.append(NAN_VALUE_END);
                } else {
                    builder.append(val);
                }
                setText(builder.toString());
            }
        });
    }

    @Override
    public void mouseChanged(IJViewerEvent event) {
        ImageViewer source = event.getSource();
        updateMouseInfo(source);
    }

    @Override
    public void mouseClicked(IJViewerEvent event) {
        // Nothing to do
    }

    @Override
    public void valueChanged(IJViewerEvent even) {
        // Nothing to do
    }

    @Override
    public void pixelSizeChanged(IJViewerEvent event) {
        // Nothing to do
    }

    public String getValueFormat() {
        return valueFormat;
    }

    public void setValueFormat(String valueFormat) {
        this.valueFormat = getFormat(valueFormat, DEFAULT_VALUE_FORMAT);
    }

    public String getXAxisFormat() {
        return xAxisFormat;
    }

    public void setXAxisFormat(String xAxisFormat) {
        this.xAxisFormat = getFormat(xAxisFormat, DEFAULT_X_FORMAT);
    }

    public String getYAxisFormat() {
        return yAxisFormat;
    }

    public void setYAxisFormat(String yAxisFormat) {
        this.yAxisFormat = getFormat(yAxisFormat, DEFAULT_X_FORMAT);
    }

    protected String getFormat(String format, String defaultFormat) {
        return (format == null) || format.trim().isEmpty() ? defaultFormat : format;
    }

}
