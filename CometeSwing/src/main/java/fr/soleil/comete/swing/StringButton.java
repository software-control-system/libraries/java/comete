/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.util.EventObject;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

/**
 * An {@link AbstractButton} that can store a parameter to send to its {@link Mediator}s
 *
 * @author SOLEIL
 */
public class StringButton extends AbstractButton {

    private static final long serialVersionUID = -7953729295653076494L;

    private String parameter;

    /**
     * Default constructor
     */
    public StringButton() {
        super();
        parameter = null;
    }

    @Override
    public void fireActionPerformed(EventObject event) {
        // String toSend = (parameter == null) ? ObjectUtils.EMPTY_STRING : parameter;
        warnMediators(new TextInformation(this, parameter));
        super.fireActionPerformed(event);
    }

    /**
     * Returns this {@link StringButton}'s parameter
     *
     * @return A {@link String}
     */
    public String getParameter() {
        return parameter;
    }

    /**
     * Sets this {@link StringButton}'s parameter
     *
     * @param parameter The parameter to set
     */
    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public void setEnabled(boolean enabled) {
        // XXX RG: Hack to try to understand the reason of Mantis 25374
        if (isEnabled() && !enabled) {
            String trace = ObjectUtils.printStackTrace(new Exception(
                    "The button with text='" + getText() + "' and tooltip='" + getToolTipText() + "' is set disabled"));
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).trace(trace);
        }
        super.setEnabled(enabled);
    }

}
