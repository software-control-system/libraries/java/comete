/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.chart.util.FontChooser;
import fr.soleil.comete.swing.chart.util.PrecisionPanel;
import fr.soleil.comete.swing.chart.util.PrecisionPanel.PrecisionPanelEvent;
import fr.soleil.comete.swing.chart.util.PrecisionPanel.PrecisionPanelListener;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.LineStyleListCellRenderer;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.text.JSmoothLabel;

/**
 * A class to display global graph settings dialog.
 */
public class ChartOption extends AbstractChartDataOption
        implements AxisPanelListener, PrecisionPanelListener, SamplingPanelListener, DragPanelListener {

    private static final long serialVersionUID = -2765678779975529131L;

    private static final Insets LABEL_CONSTRAINTS = new Insets(0, 5, 5, 30);
    private static final Insets EDITOR_CONSTRAINTS = new Insets(0, 0, 5, 5);

    protected static final int ORIGIN_Y1 = 0;
    protected static final int ORIGIN_Y2 = 1;
    protected static final int ORIGIN_X = 2;
    protected static final int ORIGIN_GENERAL_PROPERTIES = 3;
    protected static final int ORIGIN_DRAG_PROPERTIES = 4;

    protected static final String TIME_PRECISION_TITLE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.time.precision.unit");
    protected static final String VALUE_PRECISION_TITLE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.abscissa.error.margin");

    // Local declaration
    protected ChartProperties properties;

    // general panel
    protected JPanel generalPanel;

    // Legends
    protected JPanel gLegendPanel;
    protected JCheckBox generalLabelVisibleCheck;
    protected JCheckBox autoHighlightCheck;

    // Colors & Fonts
    protected JPanel gColorFontPanel;
    protected JLabel titleFontLabel;
    protected JSmoothLabel titleFontSampleLabel;
    protected JButton titleFontBtn;
    protected JLabel legendFontLabel;
    protected JSmoothLabel legendFontSampleLabel;
    protected JButton legendFontBtn;
    protected JLabel chartBackColorLabel;
    protected JLabel chartBackColorView;
    protected JButton chartBackColorBtn;
    protected JLabel generalBackColorLabel;
    protected JLabel generalBackColorView;
    protected JButton generalBackColorBtn;
    protected JCheckBox transparencyCheckBox;

    // Axis grid
    protected JPanel gGridPanel;
    protected JComboBox<String> generalGridCombo;
    protected JComboBox<String> generalLabelPCombo;
    protected JLabel generalLabelPLabel;
    protected JComboBox<Integer> generalGridStyleCombo;
    protected JLabel generalGridStyleLabel;

    // Misc
    protected JPanel gMiscPanel;
    protected JLabel generalLegendLabel;
    protected JTextField generalLegendText;
    protected JLabel generalDurationLabel;
    protected JTextField generalDurationText;
    protected PrecisionPanel timePrecisionPanel;

    // Axis panels
    protected AxisPanel y1Panel;
    protected AxisPanel y2Panel;
    protected AxisPanel xPanel;

    // Drag panel
    protected DragPanel dragPanel;

    // Sampling panel
    protected SamplingPanel samplingPanel;

    /**
     * JLChartOption constructor.
     * 
     * @param parent Parent dialog
     * @param chart Chart to be edited.
     */
    public ChartOption() {
        super();
    }

    @Override
    protected void updatePropertiesFromChart() {
        if (chart != null) {
            setProperties(chart.getChartProperties());
        }
    }

    @Override
    protected String getExpectedName() {
        return ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.properties");
    }

    @Override
    protected void initComponents() {
        super.initComponents();
        initProperties();
    }

    @Override
    protected void initTabPane() {
        super.initTabPane();
        generalPanel = new JPanel(new GridBagLayout());

        gLegendPanel = new JPanel(new GridBagLayout());
        gLegendPanel.setBorder(
                CometeUtils.createTitleBorder(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.legends")));

        gColorFontPanel = new JPanel(new GridBagLayout());
        gColorFontPanel.setBorder(CometeUtils
                .createTitleBorder(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.colors.fonts")));

        gGridPanel = new JPanel(new GridBagLayout());
        gGridPanel.setBorder(
                CometeUtils.createTitleBorder(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.grid")));

        gMiscPanel = new JPanel(new GridBagLayout());
        gMiscPanel.setBorder(
                CometeUtils.createTitleBorder(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.misc")));

        generalLegendLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.title"));
        generalLegendLabel.setFont(CometeUtils.getLabelFont());
        generalLegendLabel.setForeground(CometeUtils.getfColor());
        generalLegendText = new JTextField(8);
        generalLegendText.setMargin(CometeUtils.getTextFieldInsets());
        generalLegendText.setEditable(true);
        generalLegendText.setText(ObjectUtils.EMPTY_STRING);
        generalLegendText.addKeyListener(this);

        generalLabelVisibleCheck = new JCheckBox();
        generalLabelVisibleCheck.setFont(CometeUtils.getLabelFont());
        generalLabelVisibleCheck.setForeground(CometeUtils.getfColor());
        generalLabelVisibleCheck.setText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.visible"));
        generalLabelVisibleCheck.setSelected(false);
        generalLabelVisibleCheck.addActionListener(this);

        autoHighlightCheck = new JCheckBox();
        autoHighlightCheck.setFont(CometeUtils.getLabelFont());
        autoHighlightCheck.setForeground(CometeUtils.getfColor());
        autoHighlightCheck.setText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.highlighting.auto"));
        autoHighlightCheck.setSelected(false);
        autoHighlightCheck.addActionListener(this);

        chartBackColorLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.background"));
        chartBackColorLabel.setFont(CometeUtils.getLabelFont());
        chartBackColorLabel.setForeground(CometeUtils.getfColor());
        chartBackColorView = new JLabel(ObjectUtils.EMPTY_STRING);
        chartBackColorView.setOpaque(true);
        chartBackColorView.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        chartBackColorView.setBackground(Color.WHITE);
        chartBackColorView.setPreferredSize(CometeUtils.getViewLabelSize());
        chartBackColorView.setMinimumSize(chartBackColorView.getPreferredSize());
        chartBackColorBtn = new JButton(SUSPENSION_POINTS);
        chartBackColorBtn.setMargin(CometeUtils.getBtnInsets());
        chartBackColorBtn.addMouseListener(this);

        generalBackColorLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.background.main"));
        generalBackColorLabel.setFont(CometeUtils.getLabelFont());
        generalBackColorLabel.setForeground(CometeUtils.getfColor());
        generalBackColorView = new JLabel(ObjectUtils.EMPTY_STRING);
        generalBackColorView.setOpaque(true);
        generalBackColorView.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        generalBackColorView.setBackground(Color.WHITE);
        generalBackColorView.setPreferredSize(CometeUtils.getViewLabelSize());
        generalBackColorView.setMinimumSize(generalBackColorView.getPreferredSize());
        generalBackColorBtn = new JButton(SUSPENSION_POINTS);
        generalBackColorBtn.setMargin(CometeUtils.getBtnInsets());
        generalBackColorBtn.addMouseListener(this);

        transparencyCheckBox = new JCheckBox(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.color.transparency.enable"));
        transparencyCheckBox.addActionListener(this);
        transparencyCheckBox.setFont(CometeUtils.getLabelFont());
        transparencyCheckBox.setForeground(CometeUtils.getfColor());
        transparencyCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
        transparencyCheckBox.setHorizontalAlignment(SwingConstants.LEFT);
        transparencyCheckBox
                .setIconTextGap(CometeUtils.getViewLabelSize().width + chartBackColorBtn.getPreferredSize().width + 7);

        generalLabelPLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.placement"));
        generalLabelPLabel.setHorizontalAlignment(JLabel.RIGHT);
        generalLabelPLabel.setFont(CometeUtils.getLabelFont());
        generalLabelPLabel.setForeground(CometeUtils.getfColor());

        generalLabelPCombo = new JComboBox<>();
        generalLabelPCombo.setFont(CometeUtils.getLabelFont());
        generalLabelPCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.bottom"));
        generalLabelPCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.top"));
        generalLabelPCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.right"));
        generalLabelPCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.left"));
        generalLabelPCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.row"));
        generalLabelPCombo.setSelectedIndex(IChartViewer.LABEL_ROW);
        generalLabelPCombo.addActionListener(this);

        generalGridCombo = new JComboBox<>();
        generalGridCombo.setFont(CometeUtils.getLabelFont());
        generalGridCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.grid.none"));
        generalGridCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.grid.x"));
        generalGridCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.grid.y1"));
        generalGridCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.grid.y2"));
        generalGridCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.grid.xy1"));
        generalGridCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.grid.xy2"));

        generalGridCombo.setSelectedIndex(0);
        generalGridCombo.addActionListener(this);

        generalGridStyleLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.style"));
        generalGridStyleLabel.setFont(CometeUtils.getLabelFont());
        generalGridStyleLabel.setHorizontalAlignment(JLabel.RIGHT);
        generalGridStyleLabel.setForeground(CometeUtils.getfColor());

        generalGridStyleCombo = new JComboBox<>();
        generalGridStyleCombo.setFont(CometeUtils.getLabelFont());
        generalGridStyleCombo.addItem(IChartViewer.STYLE_SOLID);
        generalGridStyleCombo.addItem(IChartViewer.STYLE_DOT);
        generalGridStyleCombo.addItem(IChartViewer.STYLE_DASH);
        generalGridStyleCombo.addItem(IChartViewer.STYLE_LONG_DASH);
        generalGridStyleCombo.addItem(IChartViewer.STYLE_DASH_DOT);
        generalGridStyleCombo.setSelectedIndex(0);
        generalGridStyleCombo.setRenderer(new LineStyleListCellRenderer());
        generalGridStyleCombo.addActionListener(this);

        generalDurationLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.display.duration"));
        generalDurationLabel.setFont(CometeUtils.getLabelFont());
        generalDurationLabel.setForeground(CometeUtils.getfColor());
        generalDurationText = new JTextField(8);
        generalDurationText.setMargin(CometeUtils.getTextFieldInsets());
        generalDurationText.setEditable(true);
        generalDurationText.setToolTipText(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.display.duration.infinity"));
        generalDurationText.setText(Double.toString(Double.POSITIVE_INFINITY));
        generalDurationText.addKeyListener(this);

        timePrecisionPanel = new PrecisionPanel();
        timePrecisionPanel.addPrecisionPanelListener(this);

        titleFontLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.title.font.text"));
        titleFontLabel.setFont(CometeUtils.getLabelFont());
        titleFontLabel.setForeground(CometeUtils.getfColor());
        titleFontSampleLabel = new JSmoothLabel();
        titleFontSampleLabel.setText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.text.sample"));
        titleFontSampleLabel.setForeground(Color.BLACK);
        titleFontSampleLabel.setOpaque(false);
        titleFontSampleLabel.setFont(CometeUtils.getLabelFont());
        titleFontBtn = new JButton(SUSPENSION_POINTS);
        titleFontBtn.setToolTipText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.title.font.tooltip"));
        titleFontBtn.setMargin(CometeUtils.getBtnInsets());
        titleFontBtn.addMouseListener(this);

        legendFontLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.legend.font.text"));
        legendFontLabel.setFont(CometeUtils.getLabelFont());
        legendFontLabel.setForeground(CometeUtils.getfColor());
        legendFontSampleLabel = new JSmoothLabel();
        legendFontSampleLabel
                .setText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.text.sample.numbers"));
        legendFontSampleLabel.setForeground(Color.BLACK);
        legendFontSampleLabel.setOpaque(false);
        legendFontSampleLabel.setFont(CometeUtils.getLabelFont());
        legendFontBtn = new JButton(SUSPENSION_POINTS);
        legendFontBtn.setToolTipText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.legend.font.tooltip"));
        legendFontBtn.setMargin(CometeUtils.getBtnInsets());
        legendFontBtn.addMouseListener(this);

        int x = 0, y = 0, mainY = 0;
        // Legends
        GridBagConstraints generalLabelVisibleCheckConstraints = new GridBagConstraints();
        generalLabelVisibleCheckConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalLabelVisibleCheckConstraints.gridx = x++;
        generalLabelVisibleCheckConstraints.gridy = y;
        generalLabelVisibleCheckConstraints.weightx = 0;
        generalLabelVisibleCheckConstraints.weighty = 0;
        gLegendPanel.add(generalLabelVisibleCheck, generalLabelVisibleCheckConstraints);
        GridBagConstraints generalLabelLabelPLabelConstraints = new GridBagConstraints();
        generalLabelLabelPLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalLabelLabelPLabelConstraints.gridx = x++;
        generalLabelLabelPLabelConstraints.gridy = y;
        generalLabelLabelPLabelConstraints.weightx = 0.5;
        generalLabelLabelPLabelConstraints.weighty = 0;
        generalLabelLabelPLabelConstraints.insets = new Insets(0, 30, 0, 5);
        gLegendPanel.add(generalLabelPLabel, generalLabelLabelPLabelConstraints);
        GridBagConstraints generalLabelLabelPComboConstraints = new GridBagConstraints();
        generalLabelLabelPComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalLabelLabelPComboConstraints.gridx = x;
        generalLabelLabelPComboConstraints.gridy = y++;
        generalLabelLabelPComboConstraints.weightx = 0.5;
        generalLabelLabelPComboConstraints.weighty = 0;
        gLegendPanel.add(generalLabelPCombo, generalLabelLabelPComboConstraints);
        x = 0;
        GridBagConstraints autoHighlightCheckConstraints = new GridBagConstraints();
        autoHighlightCheckConstraints.fill = GridBagConstraints.HORIZONTAL;
        autoHighlightCheckConstraints.gridx = x;
        autoHighlightCheckConstraints.gridy = y;
        autoHighlightCheckConstraints.weightx = 0;
        autoHighlightCheckConstraints.weighty = 0;
        gLegendPanel.add(autoHighlightCheck, autoHighlightCheckConstraints);

        GridBagConstraints gLegendPanelConstraints = new GridBagConstraints();
        gLegendPanelConstraints.fill = GridBagConstraints.BOTH;
        gLegendPanelConstraints.gridx = 0;
        gLegendPanelConstraints.gridy = mainY++;
        gLegendPanelConstraints.weightx = 1;
        gLegendPanelConstraints.weighty = 0.25;
        generalPanel.add(gLegendPanel, gLegendPanelConstraints);

        // Colors & Fonts
        x = 0;
        y = 0;
        GridBagConstraints chartBackColorLabelConstraints = new GridBagConstraints();
        chartBackColorLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        chartBackColorLabelConstraints.gridx = x++;
        chartBackColorLabelConstraints.gridy = y;
        chartBackColorLabelConstraints.weightx = 0;
        chartBackColorLabelConstraints.weighty = 0;
        chartBackColorLabelConstraints.insets = LABEL_CONSTRAINTS;
        gColorFontPanel.add(chartBackColorLabel, chartBackColorLabelConstraints);
        GridBagConstraints chartBackColorViewConstraints = new GridBagConstraints();
        chartBackColorViewConstraints.fill = GridBagConstraints.HORIZONTAL;
        chartBackColorViewConstraints.gridx = x++;
        chartBackColorViewConstraints.gridy = y;
        chartBackColorViewConstraints.weightx = 1;
        chartBackColorViewConstraints.weighty = 0;
        chartBackColorViewConstraints.insets = EDITOR_CONSTRAINTS;
        gColorFontPanel.add(chartBackColorView, chartBackColorViewConstraints);
        GridBagConstraints chartBackColorBtnConstraints = new GridBagConstraints();
        chartBackColorBtnConstraints.fill = GridBagConstraints.NONE;
        chartBackColorBtnConstraints.gridx = x;
        chartBackColorBtnConstraints.gridy = y++;
        chartBackColorBtnConstraints.weightx = 0;
        chartBackColorBtnConstraints.weighty = 0;
        chartBackColorBtnConstraints.insets = EDITOR_CONSTRAINTS;
        gColorFontPanel.add(chartBackColorBtn, chartBackColorBtnConstraints);
        x = 0;
        GridBagConstraints generalBackColorLabelConstraints = new GridBagConstraints();
        generalBackColorLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalBackColorLabelConstraints.gridx = x++;
        generalBackColorLabelConstraints.gridy = y;
        generalBackColorLabelConstraints.weightx = 0;
        generalBackColorLabelConstraints.weighty = 0;
        generalBackColorLabelConstraints.insets = LABEL_CONSTRAINTS;
        gColorFontPanel.add(generalBackColorLabel, generalBackColorLabelConstraints);
        GridBagConstraints generalBackColorViewConstraints = new GridBagConstraints();
        generalBackColorViewConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalBackColorViewConstraints.gridx = x++;
        generalBackColorViewConstraints.gridy = y;
        generalBackColorViewConstraints.weightx = 1;
        generalBackColorViewConstraints.weighty = 0;
        generalBackColorViewConstraints.insets = EDITOR_CONSTRAINTS;
        gColorFontPanel.add(generalBackColorView, generalBackColorViewConstraints);
        GridBagConstraints generalBackColorBtnConstraints = new GridBagConstraints();
        generalBackColorBtnConstraints.fill = GridBagConstraints.NONE;
        generalBackColorBtnConstraints.gridx = x;
        generalBackColorBtnConstraints.gridy = y++;
        generalBackColorBtnConstraints.weightx = 0;
        generalBackColorBtnConstraints.weighty = 0;
        generalBackColorBtnConstraints.insets = EDITOR_CONSTRAINTS;
        gColorFontPanel.add(generalBackColorBtn, generalBackColorBtnConstraints);
        x = 0;
        GridBagConstraints titleFontLabelConstraints = new GridBagConstraints();
        titleFontLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        titleFontLabelConstraints.gridx = x++;
        titleFontLabelConstraints.gridy = y;
        titleFontLabelConstraints.weightx = 0;
        titleFontLabelConstraints.weighty = 0;
        titleFontLabelConstraints.insets = LABEL_CONSTRAINTS;
        gColorFontPanel.add(titleFontLabel, titleFontLabelConstraints);
        GridBagConstraints titleFontSampleLabelConstraints = new GridBagConstraints();
        titleFontSampleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        titleFontSampleLabelConstraints.gridx = x++;
        titleFontSampleLabelConstraints.gridy = y;
        titleFontSampleLabelConstraints.weightx = 1;
        titleFontSampleLabelConstraints.weighty = 0;
        titleFontSampleLabelConstraints.insets = EDITOR_CONSTRAINTS;
        gColorFontPanel.add(titleFontSampleLabel, titleFontSampleLabelConstraints);
        GridBagConstraints titleFontBtnConstraints = new GridBagConstraints();
        titleFontBtnConstraints.fill = GridBagConstraints.NONE;
        titleFontBtnConstraints.gridx = x;
        titleFontBtnConstraints.gridy = y++;
        titleFontBtnConstraints.weightx = 0;
        titleFontBtnConstraints.weighty = 0;
        titleFontBtnConstraints.insets = EDITOR_CONSTRAINTS;
        gColorFontPanel.add(titleFontBtn, titleFontBtnConstraints);
        x = 0;
        GridBagConstraints legendFontLabelConstraints = new GridBagConstraints();
        legendFontLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        legendFontLabelConstraints.gridx = x++;
        legendFontLabelConstraints.gridy = y;
        legendFontLabelConstraints.weightx = 0;
        legendFontLabelConstraints.weighty = 0;
        legendFontLabelConstraints.insets = new Insets(0, 5, 0, 30);
        gColorFontPanel.add(legendFontLabel, legendFontLabelConstraints);
        GridBagConstraints legendFontSampleLabelConstraints = new GridBagConstraints();
        legendFontSampleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        legendFontSampleLabelConstraints.gridx = x++;
        legendFontSampleLabelConstraints.gridy = y;
        legendFontSampleLabelConstraints.weightx = 1;
        legendFontSampleLabelConstraints.weighty = 0;
        legendFontSampleLabelConstraints.insets = new Insets(0, 0, 0, 5);
        gColorFontPanel.add(legendFontSampleLabel, legendFontSampleLabelConstraints);
        GridBagConstraints legendFontBtnConstraints = new GridBagConstraints();
        legendFontBtnConstraints.fill = GridBagConstraints.NONE;
        legendFontBtnConstraints.gridx = x;
        legendFontBtnConstraints.gridy = y++;
        legendFontBtnConstraints.weightx = 0;
        legendFontBtnConstraints.weighty = 0;
        legendFontBtnConstraints.insets = new Insets(0, 0, 0, 5);
        gColorFontPanel.add(legendFontBtn, legendFontBtnConstraints);
        x = 0;
        GridBagConstraints transparencyCheckBoxConstraints = new GridBagConstraints();
        transparencyCheckBoxConstraints.fill = GridBagConstraints.HORIZONTAL;
        transparencyCheckBoxConstraints.gridx = x;
        transparencyCheckBoxConstraints.gridy = y;
        transparencyCheckBoxConstraints.weightx = 1;
        transparencyCheckBoxConstraints.weighty = 0;
        transparencyCheckBoxConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gColorFontPanel.add(transparencyCheckBox, transparencyCheckBoxConstraints);

        GridBagConstraints gColorFontPanelConstraints = new GridBagConstraints();
        gColorFontPanelConstraints.fill = GridBagConstraints.BOTH;
        gColorFontPanelConstraints.gridx = 0;
        gColorFontPanelConstraints.gridy = mainY++;
        gColorFontPanelConstraints.weightx = 1;
        gColorFontPanelConstraints.weighty = 0.25;
        generalPanel.add(gColorFontPanel, gColorFontPanelConstraints);

        // Axis Grid
        x = 0;
        y = 0;
        GridBagConstraints generalGridComboConstraints = new GridBagConstraints();
        generalGridComboConstraints.fill = GridBagConstraints.NONE;
        generalGridComboConstraints.gridx = x++;
        generalGridComboConstraints.gridy = y;
        generalGridComboConstraints.weightx = 0;
        generalGridComboConstraints.weighty = 0;
        generalGridComboConstraints.insets = new Insets(5, 0, 5, 0);
        gGridPanel.add(generalGridCombo, generalGridComboConstraints);
        GridBagConstraints generalGridStyleLabelConstraints = new GridBagConstraints();
        generalGridStyleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalGridStyleLabelConstraints.gridx = x++;
        generalGridStyleLabelConstraints.gridy = y;
        generalGridStyleLabelConstraints.weightx = 1;
        generalGridStyleLabelConstraints.weighty = 0;
        generalGridStyleLabelConstraints.insets = new Insets(5, 30, 5, 5);
        gGridPanel.add(generalGridStyleLabel, generalGridStyleLabelConstraints);
        GridBagConstraints generalGridStyleComboConstraints = new GridBagConstraints();
        generalGridStyleComboConstraints.fill = GridBagConstraints.NONE;
        generalGridStyleComboConstraints.gridx = x;
        generalGridStyleComboConstraints.gridy = y++;
        generalGridStyleComboConstraints.weightx = 0;
        generalGridStyleComboConstraints.weighty = 0;
        generalGridStyleComboConstraints.insets = new Insets(5, 0, 5, 0);
        gGridPanel.add(generalGridStyleCombo, generalGridStyleComboConstraints);

        GridBagConstraints gGridPanelConstraints = new GridBagConstraints();
        gGridPanelConstraints.fill = GridBagConstraints.BOTH;
        gGridPanelConstraints.gridx = 0;
        gGridPanelConstraints.gridy = mainY++;
        gGridPanelConstraints.weightx = 2;
        gGridPanelConstraints.weighty = 0.25;
        generalPanel.add(gGridPanel, gGridPanelConstraints);

        // Misc
        x = 0;
        y = 0;
        GridBagConstraints generalLegendLabelConstraints = new GridBagConstraints();
        generalLegendLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalLegendLabelConstraints.gridx = x++;
        generalLegendLabelConstraints.gridy = y;
        generalLegendLabelConstraints.weightx = 0;
        generalLegendLabelConstraints.weighty = 0;
        generalLegendLabelConstraints.insets = new Insets(0, 5, 5, 10);
        gMiscPanel.add(generalLegendLabel, generalLegendLabelConstraints);
        GridBagConstraints generalLegendTextConstraints = new GridBagConstraints();
        generalLegendTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalLegendTextConstraints.gridx = x;
        generalLegendTextConstraints.gridy = y++;
        generalLegendTextConstraints.weightx = 1;
        generalLegendTextConstraints.weighty = 0;
        generalLegendTextConstraints.insets = new Insets(0, 5, 5, 5);
        gMiscPanel.add(generalLegendText, generalLegendTextConstraints);
        x = 0;
        GridBagConstraints generalDurationConstraints = new GridBagConstraints();
        generalDurationConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalDurationConstraints.gridx = x++;
        generalDurationConstraints.gridy = y;
        generalDurationConstraints.weightx = 0;
        generalDurationConstraints.weighty = 0;
        generalDurationConstraints.insets = new Insets(0, 5, 5, 10);
        gMiscPanel.add(generalDurationLabel, generalDurationConstraints);
        GridBagConstraints generalDurationTextConstraints = new GridBagConstraints();
        generalDurationTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalDurationTextConstraints.gridx = x;
        generalDurationTextConstraints.gridy = y++;
        generalDurationTextConstraints.weightx = 1;
        generalDurationTextConstraints.weighty = 0;
        generalDurationTextConstraints.insets = new Insets(0, 5, 5, 5);
        gMiscPanel.add(generalDurationText, generalDurationTextConstraints);
        x = 0;
        GridBagConstraints timePrecisionConstraints = new GridBagConstraints();
        timePrecisionConstraints.fill = GridBagConstraints.BOTH;
        timePrecisionConstraints.gridx = x;
        timePrecisionConstraints.gridy = y++;
        timePrecisionConstraints.weightx = 1;
        timePrecisionConstraints.weighty = 0;
        timePrecisionConstraints.gridwidth = GridBagConstraints.REMAINDER;
        timePrecisionConstraints.insets = new Insets(0, 5, 0, 10);
        gMiscPanel.add(timePrecisionPanel, timePrecisionConstraints);

        GridBagConstraints gMiscPanelConstraints = new GridBagConstraints();
        gMiscPanelConstraints.fill = GridBagConstraints.BOTH;
        gMiscPanelConstraints.gridx = 0;
        gMiscPanelConstraints.gridy = mainY++;
        gMiscPanelConstraints.weightx = 2;
        gMiscPanelConstraints.weighty = 0.25;
        generalPanel.add(gMiscPanel, gMiscPanelConstraints);

        // **********************************************
        // Axis panel construction
        // **********************************************
        y1Panel = new AxisPanel(AxisPanel.Y1_TYPE);
        y1Panel.addAxisPanelListener(this);
        y2Panel = new AxisPanel(AxisPanel.Y2_TYPE);
        y2Panel.addAxisPanelListener(this);
        xPanel = new AxisPanel(AxisPanel.X_TYPE);
        xPanel.addAxisPanelListener(this);

        // **********************************************
        // Sampling panel construction
        // **********************************************
        samplingPanel = new SamplingPanel();
        samplingPanel.addSamplingPanelListener(this);

        // **********************************************
        // Drag panel construction
        // **********************************************
        dragPanel = new DragPanel();
        dragPanel.addDragPanelListener(this);

        // Global frame construction

        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.general.title"), generalPanel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.sampling.title"), samplingPanel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.x.title"), xPanel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.y1.title"), y1Panel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.y2.title"), y2Panel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dragging.title"), dragPanel);
    }

    @Override
    protected void commit() {
        commit(ORIGIN_GENERAL_PROPERTIES);
    }

    protected void commit(int origin) {
        if (chart != null) {
            switch (origin) {
                case ORIGIN_Y1:
                    AxisProperties y1axisProperties = getProperties().getY1AxisProperties();
                    chart.setAxisProperties(y1axisProperties, IChartViewer.Y1);
                    y1Panel.setProperties(chart.getAxisProperties(IChartViewer.Y1), isTimeScale(IChartViewer.Y1));
                    break;
                case ORIGIN_Y2:
                    AxisProperties y2axisProperties = getProperties().getY2AxisProperties();
                    chart.setAxisProperties(y2axisProperties, IChartViewer.Y2);
                    y2Panel.setProperties(chart.getAxisProperties(IChartViewer.Y2), isTimeScale(IChartViewer.Y2));
                    break;
                case ORIGIN_X:
                    AxisProperties xaxisProperties = getProperties().getXAxisProperties();
                    chart.setAxisProperties(xaxisProperties, IChartViewer.X);
                    applyXProperties(chart.getAxisProperties(IChartViewer.X));
                    break;
                case ORIGIN_DRAG_PROPERTIES:
                    chart.setDragProperties(getProperties().getDragProperties());
                    dragPanel.setProperties(chart.getDragProperties());
                    break;
                case ORIGIN_GENERAL_PROPERTIES:
                    chart.setChartProperties(getProperties());
                    break;
            }
            if (chart instanceof Component) {
                ((Component) chart).repaint();
            }
        }
    }

    // ***************************************************************
    // Mouse Listener
    // ***************************************************************
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == chartBackColorBtn) {
            Color c = JColorChooser.showDialog(this,
                    ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.background.color.choose"),
                    ColorTool.getColor(getProperties().getBackgroundColor()));
            CometeColor cc = ColorTool.getCometeColor(c);
            if ((c != null) && (cc != null)) {
                properties.setBackgroundColor(cc);
                chartBackColorView.setBackground(c);
                commit(ORIGIN_GENERAL_PROPERTIES);
            }
        } else if (e.getSource() == generalBackColorBtn) {
            Color c = JColorChooser.showDialog(this,
                    ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.background.color.choose"),
                    ColorTool.getColor(getProperties().getBackgroundColor()));
            CometeColor cc = ColorTool.getCometeColor(c);
            if ((c != null) && (cc != null)) {
                properties.setGlobalBackgroundColor(cc);
                generalBackColorView.setBackground(c);
                commit(ORIGIN_GENERAL_PROPERTIES);
            }
        } else if (e.getSource() == titleFontBtn) {
            Font f = FontChooser.getNewFont(this, titleFontBtn.getToolTipText(),
                    FontTool.getFont(getProperties().getHeaderFont()));
            CometeFont cf = FontTool.getCometeFont(f);
            if ((f != null) && (cf != null)) {
                properties.setHeaderFont(cf);
                titleFontSampleLabel.setFont(f);
                commit(ORIGIN_GENERAL_PROPERTIES);
            }
        } else if (e.getSource() == legendFontBtn) {
            Font f = FontChooser.getNewFont(this, legendFontBtn.getToolTipText(),
                    FontTool.getFont(getProperties().getLabelFont()));
            CometeFont cf = FontTool.getCometeFont(f);
            if ((f != null) && (cf != null)) {
                properties.setLabelFont(cf);
                legendFontSampleLabel.setFont(f);
                commit(ORIGIN_GENERAL_PROPERTIES);
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // nothing to do
    }

    // ***************************************************************
    // Action listener
    // ***************************************************************
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == generalLabelVisibleCheck) {
            properties.setLegendVisible(generalLabelVisibleCheck.isSelected());
            commit(ORIGIN_GENERAL_PROPERTIES);
        } else if (e.getSource() == autoHighlightCheck) {
            properties.setAutoHighlightOnLegend(autoHighlightCheck.isSelected());
            commit(ORIGIN_GENERAL_PROPERTIES);
        } else if (e.getSource() == generalGridCombo) {
            int sel = generalGridCombo.getSelectedIndex();
            switch (sel) {
                case 1: // On X
                    properties.getXAxisProperties().setGridVisible(true);
                    properties.getY1AxisProperties().setGridVisible(false);
                    properties.getY2AxisProperties().setGridVisible(false);
                    break;
                case 2: // On Y1
                    properties.getXAxisProperties().setGridVisible(false);
                    properties.getY1AxisProperties().setGridVisible(true);
                    properties.getY2AxisProperties().setGridVisible(false);
                    break;
                case 3: // On Y2
                    properties.getXAxisProperties().setGridVisible(false);
                    properties.getY1AxisProperties().setGridVisible(false);
                    properties.getY2AxisProperties().setGridVisible(true);
                    break;
                case 4: // On X,Y1
                    properties.getXAxisProperties().setGridVisible(true);
                    properties.getY1AxisProperties().setGridVisible(true);
                    properties.getY2AxisProperties().setGridVisible(false);
                    break;
                case 5: // On X,Y2
                    properties.getXAxisProperties().setGridVisible(true);
                    properties.getY1AxisProperties().setGridVisible(false);
                    properties.getY2AxisProperties().setGridVisible(true);
                    break;
                default: // None
                    properties.getXAxisProperties().setGridVisible(false);
                    properties.getY1AxisProperties().setGridVisible(false);
                    properties.getY2AxisProperties().setGridVisible(false);
                    break;
            }
            y1Panel.setProperties(properties.getY1AxisProperties(), isTimeScale(IChartViewer.Y1));
            y2Panel.setProperties(properties.getY2AxisProperties(), isTimeScale(IChartViewer.Y2));
            xPanel.setProperties(properties.getXAxisProperties(), isTimeScale(IChartViewer.X));
            commit(ORIGIN_GENERAL_PROPERTIES);
        } else if (e.getSource() == generalGridStyleCombo) {
            int s = generalGridStyleCombo.getSelectedIndex();
            properties.getXAxisProperties().setGridStyle(s);
            properties.getY1AxisProperties().setGridStyle(s);
            properties.getY2AxisProperties().setGridStyle(s);
            commit(ORIGIN_GENERAL_PROPERTIES);
        } else if (e.getSource() == generalLabelPCombo) {
            int s = generalLabelPCombo.getSelectedIndex();
            properties.setLegendPlacement(s);
            commit(ORIGIN_GENERAL_PROPERTIES);
        } else if (e.getSource() == transparencyCheckBox) {
            properties.setTransparencyAllowed(transparencyCheckBox.isSelected());
            commit(ORIGIN_GENERAL_PROPERTIES);
        } else {
            super.actionPerformed(e);
        }
    }

    // ***************************************************************
    // Change listener
    // ***************************************************************
    @Override
    public void stateChanged(ChangeEvent e) {
        // nothing to do
    }

    // ***************************************************************
    // Key listener
    // ***************************************************************
    @Override
    public void keyPressed(KeyEvent e) {
        // nothing to do
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // nothing to do
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // General ------------------------------------------------------------
        if (e.getSource() == generalLegendText) {
            String title = generalLegendText.getText().trim();
            if (title.isEmpty()) {
                title = null;
            }
            properties.setTitle(title);
            commit(ORIGIN_GENERAL_PROPERTIES);
        } else if (e.getSource() == generalDurationText) {
            if (generalDurationText.getText().equalsIgnoreCase("infinty")) {
                properties.setDisplayDuration(Double.POSITIVE_INFINITY);
                commit(ORIGIN_GENERAL_PROPERTIES);
            } else {
                try {
                    double d = Double.parseDouble(generalDurationText.getText());
                    properties.setDisplayDuration(d * 1000);
                    generalDurationText.setBackground(Color.WHITE);
                    generalDurationText.setToolTipText(
                            ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.display.duration.infinity"));
                    commit(ORIGIN_GENERAL_PROPERTIES);
                } catch (NumberFormatException err) {
                    generalDurationText.setBackground(Color.ORANGE);
                    generalDurationText.setToolTipText(
                            ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.display.duration.malformed"));
                }
            }
        }
    } // End keyReleased

    // ***************************************************************
    // PrecisionPanel listener
    // ***************************************************************
    @Override
    public void precisionPanelChanged(PrecisionPanelEvent event) {
        if ((event != null) && (event.getSource() == timePrecisionPanel)) {
            properties.setTimePrecision(timePrecisionPanel.getPrecision());
            commit(ORIGIN_GENERAL_PROPERTIES);
        }
    }

    // Error message
    protected void error(String m) {
        JOptionPane.showMessageDialog(this, m,
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.option.error"), JOptionPane.ERROR_MESSAGE);
    }

    protected void initProperties() {
        setProperties(null);
    }

    public ChartProperties getProperties() {
        if (properties == null) {
            properties = new ChartProperties();
        }
        return properties.clone();
    }

    public void setProperties(ChartProperties chartProperties) {
        if (chartProperties == null) {
            properties = new ChartProperties();
        } else {
            properties = chartProperties.clone();
        }
        applyProperties();
    }

    protected void applyProperties() {
        ChartProperties tempProperties = getProperties();

        // Axis properties START
        y1Panel.removeAxisPanelListener(this);
        y2Panel.removeAxisPanelListener(this);
        xPanel.removeAxisPanelListener(this);

        y1Panel.setProperties(tempProperties.getY1AxisProperties(), isTimeScale(IChartViewer.Y1));
        y2Panel.setProperties(tempProperties.getY2AxisProperties(), isTimeScale(IChartViewer.Y2));
        applyXProperties(tempProperties.getXAxisProperties());
        dragPanel.setProperties(tempProperties.getDragProperties());
        properties.setY1AxisProperties(y1Panel.getProperties());
        properties.setY2AxisProperties(y2Panel.getProperties());
        properties.setXAxisProperties(xPanel.getProperties());
        properties.setDragProperties(dragPanel.getProperties());

        y1Panel.addAxisPanelListener(this);
        y2Panel.addAxisPanelListener(this);
        xPanel.addAxisPanelListener(this);
        // Axis properties END

        tempProperties = getProperties();
        generalLegendText
                .setText(tempProperties.getTitle() == null ? ObjectUtils.EMPTY_STRING : tempProperties.getTitle());
        chartBackColorView.setBackground(ColorTool.getColor(tempProperties.getBackgroundColor()));
        generalBackColorView.setBackground(ColorTool.getColor(tempProperties.getGlobalBackgroundColor()));
        titleFontSampleLabel.setFont(FontTool.getFont(tempProperties.getHeaderFont()));
        legendFontSampleLabel.setFont(FontTool.getFont(tempProperties.getLabelFont()));
        generalLabelVisibleCheck.setSelected(tempProperties.isLegendVisible());
        transparencyCheckBox.setSelected(tempProperties.isTransparencyAllowed());

        // Legend placement START
        generalLabelPCombo.removeActionListener(this);
        generalLabelPCombo.setSelectedIndex(tempProperties.getLegendPlacement());
        generalLabelPCombo.addActionListener(this);
        // Legend placement END

        generalDurationText.setText(Double.toString(tempProperties.getDisplayDuration() / 1000));
        timePrecisionPanel.setPrecision(tempProperties.getTimePrecision());
        autoHighlightCheck.setSelected(tempProperties.isAutoHighlightOnLegend());

        // Axis grid selection

        // default: None
        int sel = 0;

        if ((tempProperties.getXAxisProperties().isGridVisible())
                && (!tempProperties.getY1AxisProperties().isGridVisible())
                && (!tempProperties.getY2AxisProperties().isGridVisible())) {
            // On X
            sel = 1;
        } else if ((!tempProperties.getXAxisProperties().isGridVisible())
                && (tempProperties.getY1AxisProperties().isGridVisible())
                && (!tempProperties.getY2AxisProperties().isGridVisible())) {
            // On Y1
            sel = 2;
        } else if ((!tempProperties.getXAxisProperties().isGridVisible())
                && (!tempProperties.getY1AxisProperties().isGridVisible())
                && (tempProperties.getY2AxisProperties().isGridVisible())) {
            // On Y2
            sel = 3;
        } else if ((tempProperties.getXAxisProperties().isGridVisible())
                && (tempProperties.getY1AxisProperties().isGridVisible())
                && (!tempProperties.getY2AxisProperties().isGridVisible())) {
            // On X,Y1
            sel = 4;
        } else if ((tempProperties.getXAxisProperties().isGridVisible())
                && (!tempProperties.getY1AxisProperties().isGridVisible())
                && (tempProperties.getY2AxisProperties().isGridVisible())) {
            // On X,Y2
            sel = 5;
        }

        // Grid position START
        generalGridCombo.removeActionListener(this);
        generalGridCombo.setSelectedIndex(sel);
        generalGridCombo.addActionListener(this);
        // Grid position END

        // Grid style START
        generalGridStyleCombo.removeActionListener(this);
        generalGridStyleCombo.setSelectedIndex(tempProperties.getXAxisProperties().getGridStyle());
        generalGridStyleCombo.addActionListener(this);
        // Grid style END

        // Sampling properties START
        samplingPanel.setProperties(tempProperties.getSamplingProperties());
        // Sampling properties END
    }

    @Override
    public void axisPanelChanged(AxisPanelEvent event) {
        if (event.getSource() == y1Panel) {
            properties.setY1AxisProperties(y1Panel.getProperties());
            commit(ORIGIN_Y1);
        } else if (event.getSource() == y2Panel) {
            properties.setY2AxisProperties(y2Panel.getProperties());
            commit(ORIGIN_Y2);
        } else if (event.getSource() == xPanel) {
            properties.setXAxisProperties(xPanel.getProperties());
            commit(ORIGIN_X);
        }
    }

    @Override
    public void samplingPanelChanged(SamplingPanelEvent event) {
        if (event.getSource() == samplingPanel) {
            properties.setSamplingProperties(samplingPanel.getProperties());
            commit(ORIGIN_GENERAL_PROPERTIES);
        }
    }

    @Override
    public void dragPanelChanged(DragPanelEvent event) {
        if (event.getSource() == dragPanel) {
            properties.setDragProperties(dragPanel.getProperties());
            commit(ORIGIN_DRAG_PROPERTIES);
        }
    }

    protected void applyXProperties(AxisProperties xProperties) {
        boolean xTime = isTimeScale(IChartViewer.X);
        timePrecisionPanel.setTimeScale(xTime);
        xPanel.setProperties(xProperties, xTime);
    }

    public boolean isTimeScale(int axis) {
        return ((chart != null) && (chart.getChartView().getAxis(axis).getScale().isTimeScale()));
    }

    public void setScalePanelEnabled(boolean enabled, int axis) {
        AxisPanel axisPanel = getAxisPanel(axis);
        if (axisPanel != null) {
            axisPanel.setScalePanelEnabled(enabled);
        }
    }

    private AxisPanel getAxisPanel(int axis) {
        AxisPanel panel;
        switch (axis) {
            case AxisPanel.Y1_TYPE:
                panel = y1Panel;
                break;
            case AxisPanel.Y2_TYPE:
                panel = y2Panel;
                break;
            case AxisPanel.X_TYPE:
                panel = xPanel;
                break;
            default:
                panel = null;
        }
        return panel;
    }

}
