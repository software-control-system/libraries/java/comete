/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.definition.widget.util.ITreeNode;

/**
 * A {@link DefaultTreeCellRenderer} that can be used with {@link fr.soleil.comete.swing.Tree Tree}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class CometeTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = -3683365993983303268L;

    protected final AdvancedCheckBox cb;

    /**
     * Constructor
     */
    public CometeTreeCellRenderer() {
        super();
        cb = new AdvancedCheckBox();
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
            int row, boolean hasFocus) {
        JComponent comp = (JComponent) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row,
                hasFocus);
        ITreeNode treeNode = getTreeNode(value, tree);
        if (treeNode != null) {
            Object data = treeNode.getData();
            boolean isBoolean = false;
            if (data instanceof Boolean) {
                JComponent temp = comp;
                comp = cb;
                comp.setBackground(temp.getBackground());
                comp.setBorder(temp.getBorder());
                comp.setFont(temp.getFont());
                comp.setForeground(temp.getForeground());
                cb.setSelected((Boolean) data);
                isBoolean = true;
            }
            String name = treeNode.getName();
            if (name != null) {
                if (isBoolean) {
                    ((AdvancedCheckBox) comp).setText(name);
                } else {
                    ((JLabel) comp).setText(name);
                }
            }
            CometeImage image = treeNode.getImage();
            if (image != null) {
                ImageIcon icon = ImageTool.getImage(image);
                if (isBoolean) {
                    ((AdvancedCheckBox) comp).setIcon(icon);
                } else {
                    ((JLabel) comp).setIcon(icon);
                }
            }

            if (comp != null) {
                comp.setToolTipText(treeNode.getToolTip());
            }
        }

        return comp;
    }

    /**
     * Recovers the {@link ITreeNode} from a value given in
     * {@link #getTreeCellRendererComponent(JTree, Object, boolean, boolean, boolean, int, boolean)}
     * 
     * @param value The value
     * @param tree The concerned {@link JTree}
     * @return An {@link ITreeNode}
     */
    protected ITreeNode getTreeNode(Object value, JTree tree) {
        ITreeNode treeNode = null;
        if (value instanceof ITreeNode) {
            treeNode = (ITreeNode) value;
        } else if ((value instanceof DefaultMutableTreeNode) && (tree.getModel() instanceof CometeTreeModel)) {
            treeNode = ((CometeTreeModel) tree.getModel()).getTreeNode((DefaultMutableTreeNode) value);
        }
        return treeNode;
    }

    /**
     * A class to have a {@link JCheckBox} that displays its usual icon and a custom icon
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class AdvancedCheckBox extends JPanel {
        private static final long serialVersionUID = -6131417633333464512L;

        protected final JLabel label;
        protected final JCheckBox checkBox;

        public AdvancedCheckBox() {
            super(new BorderLayout(5, 5));
            label = new JLabel();
            checkBox = new JCheckBox();
        }

        @Override
        public void setFont(Font font) {
            super.setFont(font);
            if (checkBox != null) {
                checkBox.setFont(font);
            }
        }

        @Override
        public void setForeground(Color fg) {
            super.setForeground(fg);
            if (checkBox != null) {
                checkBox.setForeground(fg);
            }
        }

        public String getText() {
            return checkBox.getText();
        }

        public void setText(String text) {
            checkBox.setText(text);
            revalidate();
        }

        public Icon getIcon() {
            return label.getIcon();
        }

        public void setIcon(Icon icon) {
            label.setIcon(icon);
            revalidate();
        }

        public boolean isSelected() {
            return checkBox.isSelected();
        }

        public void setSelected(boolean selected) {
            checkBox.setSelected(selected);
        }
    }

}
