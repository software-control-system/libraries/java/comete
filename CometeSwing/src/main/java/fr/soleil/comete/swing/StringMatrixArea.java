/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import fr.soleil.comete.definition.data.target.matrix.ITextMatrixComponent;
import fr.soleil.comete.definition.listener.ITableListener;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.StringMatrix;

public class StringMatrixArea extends AbstractMatrixArea implements ITextMatrixComponent {

    private static final long serialVersionUID = -7562732906458560253L;

    public StringMatrixArea() {
        super();
    }

    @Override
    public String[][] getStringMatrix() {
        String[][] values = null;
        if (matrix instanceof StringMatrix) {
            values = ((StringMatrix) matrix).getValue();
        }
        return values;
    }

    @Override
    public void setStringMatrix(String[][] value) {
        if (!(matrix instanceof StringMatrix)) {
            matrix = new StringMatrix();
        }
        ((StringMatrix) matrix).setValue(value);
        updateSize(matrix.getWidth(), matrix.getHeight());
        updateText();
    }

    @Override
    public String[] getFlatStringMatrix() {
        String[] flatValue = null;
        if (matrix instanceof StringMatrix) {
            flatValue = ((StringMatrix) matrix).getFlatValue();
        }
        return flatValue;
    }

    @Override
    public void setFlatStringMatrix(String[] value, int awidth, int aheight) {
        if (!(matrix instanceof StringMatrix)) {
            matrix = new StringMatrix();
        }
        ((StringMatrix) matrix).setFlatValue(value, aheight, awidth);
        updateSize(matrix.getWidth(), matrix.getHeight());
        updateText();
    }

    @Override
    protected void updateValue() {
        String[] flatValues = getFlatValue(false, true);
        int newCol = textAreaWidth;
        int newRow = textAreaHeight;
        if (flatValues != null) {
            if (textAreaWidth == 1) {// this is a spectrum
                newCol = textAreaHeight;
                newRow = textAreaWidth;
            }
            setFlatStringMatrix(flatValues, newCol, newRow);
        } else {
            setStringMatrix(null);
        }
        fireValueChanged();
    }

    @Override
    protected Object[] getMatrix() {
        return getStringMatrix();
    }

    public static void main(String[] args) {
        StringMatrixArea stringMatrixArea = new StringMatrixArea();
        stringMatrixArea.setFlatStringMatrix(
                new String[] { "0", "hjk", "klhkl", "0", "hjk", "klhkl", "0", "hjk", "klhkl" }, 3, 3);
        JFrame frame = new JFrame();
        frame.setContentPane(stringMatrixArea);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setTitle("StringMatrixArea");
        frame.setAlwaysOnTop(true);
        frame.setSize(500, 500);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.toFront();
        ITableListener<AbstractMatrix<?>> iTableListener = new ITableListener<AbstractMatrix<?>>() {

            @Override
            public void valueChanged(AbstractMatrix<?> matrix) {
                System.out.println("valueChanged=" + matrix);
            }

            @Override
            public void selectedPointChanged(double[] point) {

            }

            @Override
            public void selectedColumnChanged(int col) {

            }

            @Override
            public void selectedRowChanged(int col) {

            }

        };
        stringMatrixArea.addTableListener(iTableListener);
    }
}
