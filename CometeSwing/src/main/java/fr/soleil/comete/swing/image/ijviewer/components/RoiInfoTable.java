/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.text.DecimalFormat;

import javax.swing.SwingConstants;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.FormatStringValue;
import org.jdesktop.swingx.renderer.StringValues;
import org.jdesktop.swingx.table.ColumnFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.components.DefaultRoiInfoTableModel.Column;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Default implementation for the table. Final to force user to create their own
 * table with their own specific model.
 * 
 * @author MAINGUY
 */
public final class RoiInfoTable extends ARoiInfoTable {

    private static final long serialVersionUID = -3518657035270066836L;

    public RoiInfoTable(ImageViewer theViewer) {
        super(theViewer);

        setColumnControlVisible(true);
        setHorizontalScrollEnabled(false);
        // Disable selection to avoid table selection to interact with ROI manager selection
        setCellSelectionEnabled(false);

        // Following has to be set after the model, which auto creates the columns.
        // Before setting the model, columns are not defined, i.e. getColumnExt returns null

        // Set columns' renderer and highlighter
        setCellRenderers();
        setCellHighlighters();
    }

    @Override
    protected ARoiInfoTableModel createTableModel() {
        setColumnFactory(new ColumnFactory() {
            /**
             * We use a Column object to identify the column, useful to retrieve a particular column and customize it.
             * 
             * @author MAINGUY
             */
            @Override
            public void configureTableColumn(TableModel model, TableColumnExt columnExt) {
                super.configureTableColumn(model, columnExt);
                if (model instanceof DefaultRoiInfoTableModel) {
                    columnExt.setIdentifier(((DefaultRoiInfoTableModel) model).getColumnId(columnExt.getModelIndex()));
                }
            }
        });

        return new DefaultRoiInfoTableModel(ObjectUtils.recoverObject(viewerRef));
    }

    protected void setCellRenderers() {
        getColumnExt(Column.TYPE)
                .setCellRenderer(new DefaultTableRenderer(StringValues.TO_STRING, SwingConstants.CENTER));
        getColumnExt(Column.X)
                .setCellRenderer(new DefaultTableRenderer(StringValues.NUMBER_TO_STRING, SwingConstants.RIGHT));
        getColumnExt(Column.Y)
                .setCellRenderer(new DefaultTableRenderer(StringValues.NUMBER_TO_STRING, SwingConstants.RIGHT));
        getColumnExt(Column.WIDTH)
                .setCellRenderer(new DefaultTableRenderer(StringValues.NUMBER_TO_STRING, SwingConstants.RIGHT));
        getColumnExt(Column.HEIGHT)
                .setCellRenderer(new DefaultTableRenderer(StringValues.NUMBER_TO_STRING, SwingConstants.RIGHT));

        DecimalFormat numberFormat = new DecimalFormat();
        numberFormat.setMaximumFractionDigits(6);

        getColumnExt(Column.REAL_WIDTH)
                .setCellRenderer(new DefaultTableRenderer(new FormatStringValue(numberFormat), SwingConstants.RIGHT));
        getColumnExt(Column.REAL_HEIGHT)
                .setCellRenderer(new DefaultTableRenderer(new FormatStringValue(numberFormat), SwingConstants.RIGHT));
        getColumnExt(Column.LINE_LENGTH)
                .setCellRenderer(new DefaultTableRenderer(new FormatStringValue(numberFormat), SwingConstants.RIGHT));
        getColumnExt(Column.LINE_REAL_LENGTH)
                .setCellRenderer(new DefaultTableRenderer(new FormatStringValue(numberFormat), SwingConstants.RIGHT));
    }

    protected void setCellHighlighters() {
        setHighlighters(HighlighterFactory.createAlternateStriping());
    }

}
