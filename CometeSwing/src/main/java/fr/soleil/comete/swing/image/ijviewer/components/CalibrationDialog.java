/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.lib.project.ObjectUtils;
import ij.gui.Roi;

/**
 * A {@link JDialog} used to change an {@link ImageViewer}'s pixel size, based on a {@link Roi} size.
 * 
 * @author MAINGUY
 */
public class CalibrationDialog extends JDialog
        implements KeyListener, ActionListener, WindowListener, DocumentListener {

    private static final long serialVersionUID = 860903411912267747L;

    protected static final ResourceBundle MESSAGES = ResourceBundle
            .getBundle("fr.soleil.comete.swing.image.ijviewer.messages");
    private static final int NB_DIGITS = 6;

    private final WeakReference<ImageViewer> imageViewerReference;
    private final WeakReference<IJRoiManager> roiManagerReference;

    protected JTextField sampleSizeTextField;
    protected JTextField pixelSizeField;
    protected JButton computePixelSizeButton;
    protected JButton resetPixelSizeButton;

    protected DecimalFormat numberFormat;
    private final Object lock;

    /**
     * @param owner
     */
    public CalibrationDialog(Window owner, ImageViewer imageViewer) {
        super(owner);
        lock = new Object();
        setTitle(MESSAGES.getString("ImageJViewer.Settings.PixelSize.Title"));
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

        if (imageViewer == null) {
            this.imageViewerReference = null;
            this.roiManagerReference = null;
        } else {
            this.imageViewerReference = new WeakReference<ImageViewer>(imageViewer);
            this.roiManagerReference = new WeakReference<IJRoiManager>(imageViewer.getRoiManager());
        }

        addWindowListener(this);

        initComponents();
        layoutComponents();

        updateFields();

        pack();
    }

    private void initComponents() {
        sampleSizeTextField = new JTextField();
        sampleSizeTextField.setColumns(12);
        sampleSizeTextField.addKeyListener(this);
        sampleSizeTextField.getDocument().addDocumentListener(this);

        computePixelSizeButton = new JButton(MESSAGES.getString("ImageJViewer.Settings.PixelSize.Go"));
        computePixelSizeButton.setMargin(new Insets(0, 2, 0, 2));
        computePixelSizeButton.addActionListener(this);

        resetPixelSizeButton = new JButton(MESSAGES.getString("ImageJViewer.Settings.PixelSize.Reset"));
        resetPixelSizeButton.setMargin(new Insets(0, 2, 0, 2));
        resetPixelSizeButton.addActionListener(this);

        pixelSizeField = new JTextField();
        pixelSizeField.setEditable(false);

        numberFormat = new DecimalFormat();
        numberFormat.setMaximumFractionDigits(NB_DIGITS);
    }

    private void layoutComponents() {
        JLabel sampleSizeLabel = new JLabel(MESSAGES.getString("ImageJViewer.Settings.PixelSize.SampleSize"));
        JLabel pixelSizeLabel = new JLabel(MESSAGES.getString("ImageJViewer.Settings.PixelSize.PixelSize"));

        JPanel calibrationPanel = new JPanel();
        calibrationPanel.setBorder(BorderFactory.createEmptyBorder(2, 4, 4, 4));

        GroupLayout layout = new GroupLayout(calibrationPanel);
        calibrationPanel.setLayout(layout);

        layout.setAutoCreateGaps(false);
        layout.setAutoCreateContainerGaps(false);

        SequentialGroup hGroup = layout.createSequentialGroup();
        hGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(sampleSizeLabel)
                .addComponent(pixelSizeLabel));
        hGroup.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED);
        hGroup.addGroup(layout.createParallelGroup(Alignment.LEADING, true).addComponent(sampleSizeTextField)
                .addComponent(pixelSizeField));
        hGroup.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED);
        hGroup.addGroup(layout.createParallelGroup(Alignment.LEADING, true).addComponent(computePixelSizeButton)
                .addComponent(resetPixelSizeButton));
        layout.setHorizontalGroup(hGroup);

        SequentialGroup vGroup = layout.createSequentialGroup();
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(sampleSizeLabel)
                .addComponent(sampleSizeTextField).addComponent(computePixelSizeButton));
        vGroup.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED);
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(pixelSizeLabel)
                .addComponent(pixelSizeField).addComponent(resetPixelSizeButton));
        layout.setVerticalGroup(vGroup);

        setContentPane(calibrationPanel);
    }

    public void updateFields() {
        ImageViewer imageViewer = ObjectUtils.recoverObject(imageViewerReference);
        if (imageViewer != null) {
            double pixelSize = imageViewer.getPixelSize();
            pixelSizeField.setText(numberFormat.format(pixelSize));
            double sampleSize = 0;
            Roi calibrationRoi = getCalibrationRoi();
            if (calibrationRoi != null) {
                double roiSize = calibrationRoi.getLength();
                sampleSize = roiSize * pixelSize;
            }
            synchronized (lock) {
                sampleSizeTextField.getDocument().removeDocumentListener(this);
                sampleSizeTextField.setText(numberFormat.format(sampleSize));
                sampleSizeTextField.getDocument().addDocumentListener(this);
            }
        }
        computePixelSizeButton.setEnabled(false);
    }

    private void computePixelSize() {
        ImageViewer imageViewer = ObjectUtils.recoverObject(imageViewerReference);
        if (imageViewer != null) {
            try {
                Number sampleSize = numberFormat.parse(sampleSizeTextField.getText());

                Roi calibrationRoi = getCalibrationRoi();
                if (calibrationRoi != null) {
                    double roiSize = calibrationRoi.getLength();
                    double pixelSize = sampleSize.doubleValue() / roiSize;
                    imageViewer.setPixelSize(pixelSize);
                } else {
                    JOptionPane.showMessageDialog(this,
                            MESSAGES.getString("ImageJViewer.Settings.PixelSize.Error.MissingRoi"),
                            MESSAGES.getString("ImageJViewer.Settings.PixelSize.Error.Title"),
                            JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception e) {
                // nop
            }
        }
    }

    protected Roi getCalibrationRoi() {
        Roi calibration = null;
        IJRoiManager roiManager = ObjectUtils.recoverObject(roiManagerReference);
        if (roiManager != null) {
            calibration = roiManager.getSpecialRoi(roiManager.getRoiGenerator(IJRoiManager.CALIBRATION_ROI_MODE));
        }
        return calibration;
    }

    private void resetPixelSize() {
        ImageViewer imageViewer = ObjectUtils.recoverObject(imageViewerReference);
        if (imageViewer != null) {
            imageViewer.setPixelSize(1);
        }
    }

    // Methods from WindowListener

    @Override
    public void windowActivated(WindowEvent e) {
        updateFields();
    }

    @Override
    public void windowClosing(WindowEvent e) {
        // nop
    }

    @Override
    public void windowClosed(WindowEvent e) {
        // nop
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        // nop
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        // nop
    }

    @Override
    public void windowIconified(WindowEvent e) {
        // nop
    }

    @Override
    public void windowOpened(WindowEvent e) {
        // nop
    }

    // Methods from ActionListener

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == computePixelSizeButton) {
            computePixelSize();
        } else if (e.getSource() == resetPixelSizeButton) {
            resetPixelSize();
        }
    }

    // Methods from KeyListener

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource() == sampleSizeTextField) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                computePixelSize();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // nop
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // nop
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        treatDocumentEvent(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        treatDocumentEvent(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        treatDocumentEvent(e);
    }

    protected void treatDocumentEvent(DocumentEvent e) {
        computePixelSizeButton.setEnabled(true);
    }

}
