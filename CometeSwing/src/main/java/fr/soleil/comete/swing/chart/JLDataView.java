/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.Component;
import java.io.Serializable;

import fr.soleil.comete.swing.chart.data.AbstractDataView;

/**
 * A class to handle data view. It handles data and all graphics stuff related to a data set.
 * 
 * @author JL Pons
 */
@Deprecated
public class JLDataView extends AbstractDataView implements Serializable {

    private static final long serialVersionUID = 3499264691331074149L;

    private JLAxis parentAxis;

    public JLDataView() {
        this(null);
    }

    public JLDataView(String id) {
        super(id);
        parentAxis = null;
    }

    @Override
    public JLAxis getAxis() {
        return parentAxis;
    }

    public void setAxis(JLAxis axis) {
        this.parentAxis = axis;
    }

    @Override
    protected String formatValueFromParentAxis(double value, int precision, Object parent) {
        return parentAxis.formatValue(value, precision);
    }

    @Override
    protected boolean isXTimeScale(Object parent) {
        boolean time;
        Component comp = parentAxis.getParent();
        if (comp instanceof JLChart) {
            time = ((JLChart) comp).getXAxis().getAnnotation() == JLAxis.TIME_ANNO;
        } else {
            time = false;
        }

        return time;
    }

    @Override
    protected void finalize() {
        super.finalize();
        parentAxis = null;
    }
}
