/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import javax.swing.ImageIcon;

public interface IIconComponent extends IconConstants {

    public final static int BULB_OFF = 0;
    public final static int BULB_ON = 1;
    public final static int BULB_KO = 2;
    public final static int LED_BLUE = 3;
    public final static int LED_BROWNGRAY = 4;
    public final static int LED_DARKGRAY = 5;
    public final static int LED_ORANGE = 6;
    public final static int LED_GRAY = 7;
    public final static int LED_GREEN = 8;
    public final static int LED_DARKGREEN = 9;
    public final static int LED_LIGHTORANGE = 10;
    public final static int LED_PINK = 11;
    public final static int LED_RED = 12;
    public final static int LED_WHITE = 13;
    public final static int LED_YELLOW = 14;
    public final static int LED_KO = 15;

    /**
     * Computes the {@link ImageIcon} that matches given icon id.
     * 
     * @param iconId The icon id.
     * @return An {@link ImageIcon}. <code>null</code> if none matching.
     */
    public static ImageIcon getIconForId(int iconId) {
        ImageIcon icon;
        switch (iconId) {
            case BULB_OFF:
                icon = ICON_BULB_OFF;
                break;
            case BULB_ON:
                icon = ICON_BULB_ON;
                break;
            case BULB_KO:
                icon = ICON_BULB_KO;
                break;
            case LED_BLUE:
                icon = ICON_LED_BLUE;
                break;
            case LED_BROWNGRAY:
                icon = ICON_LED_BROWNGRAY;
                break;
            case LED_DARKGRAY:
                icon = ICON_LED_DARKGRAY;
                break;
            case LED_ORANGE:
                icon = ICON_LED_ORANGE;
                break;
            case LED_GRAY:
                icon = ICON_LED_GRAY;
                break;
            case LED_GREEN:
                icon = ICON_LED_GREEN;
                break;
            case LED_DARKGREEN:
                icon = ICON_LED_DARKGREEN;
                break;
            case LED_LIGHTORANGE:
                icon = ICON_LED_LIGHTORANGE;
                break;
            case LED_PINK:
                icon = ICON_LED_PINK;
                break;
            case LED_RED:
                icon = ICON_LED_RED;
                break;
            case LED_WHITE:
                icon = ICON_LED_WHITE;
                break;
            case LED_YELLOW:
                icon = ICON_LED_YELLOW;
                break;
            case LED_KO:
                icon = ICON_LED_KO;
                break;
            default:
                icon = null;
                break;
        }
        return icon;
    }

    /**
     * Sets this component's icon according to given icon id
     * 
     * @param iconId The icon id.
     */
    public void setIconId(int iconId);

}
