/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.WeakHashMap;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.chart.util.DateTimeFormatChooser;
import fr.soleil.comete.swing.chart.util.FontChooser;
import fr.soleil.comete.swing.util.AlignmentListCellRenderer;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.date.IDateFormattable;
import fr.soleil.lib.project.swing.text.JSmoothLabel;

/**
 * A class to build a axis setting panel.
 */
public class AxisPanel extends JPanel implements ActionListener, KeyListener, CometeConstants {

    private static final long serialVersionUID = 4292351440687011990L;

    private static final Insets LABEL_CONSTRAINTS = new Insets(5, 5, 0, 5);
    private static final Insets EDITOR_CONSTRAINTS = new Insets(5, 0, 0, 5);

    protected static final ImageIcon CALENDAR_ICON = new ImageIcon(
            AxisPanel.class.getResource("/com/famfamfam/silk/calendar_view_day.png"));
    protected static final ImageIcon COLOR_WHEEL_ICON = new ImageIcon(
            AxisPanel.class.getResource("/com/famfamfam/silk/color_wheel.png"));

    protected static final String LABELS_INTERVAL = "LabelsInterval";
    protected static final String NO_LABELS_INTERVAL = "NoLabelsInterval";
    protected static final String ZERO = "0";
    protected static final String DEFAULT_FORMAT = "%6.3E";
    protected static final String OPEN_PARENTHESIS = "(";
    protected static final String CLOSE_PARENTHESIS = ")";

    public static final int Y1_TYPE = 0;
    public static final int Y2_TYPE = 1;
    public static final int X_TYPE = 2;

    protected final SimpleDateFormat dateFormat;

    protected int type;
    protected AxisProperties properties;

    protected final JPanel scalePanel;
    protected final JPanel scaleBoundsSubPanel;
    protected final JPanel labelsIntervalSubPanel;
    protected final JPanel intervalSubPanel;
    protected final CardLayout intervalLayout;
    protected final JPanel settingPanel;

    protected final Calendar calendar;
    protected final JLabel minLabel;
    protected final JTextField minText;
    protected final JLabel maxLabel;
    protected final JTextField maxText;
    protected final JCheckBox autoScaleCheck;

    protected final JLabel scaleLabel;
    protected final JComboBox<String> scaleCombo;
    protected final JCheckBox subGridCheck;
    protected final JCheckBox visibleCheck;
    protected final JCheckBox oppositeCheck;

    protected JLabel labelFontLabel;
    protected JSmoothLabel labelFontSampleLabel;
    protected JButton labelFontBtn;
    protected final JComboBox<String> formatCombo;
    protected final JLabel formatLabel;
    protected final JButton formatBtn;

    protected final JLabel titleLabel;
    protected final JTextField titleText;
    protected final JComboBox<Integer> titleAlignmentCombo;

    protected final JLabel colorLabel;
    protected final JLabel colorView;
    protected final JButton colorBtn;

    protected final JLabel positionLabel;
    protected final JComboBox<String> positionCombo;

    protected JLabel labelNbLabel;
    protected JTextField labelNbText;

    protected Collection<AxisPanelListener> listeners;

    protected volatile boolean timeScale;

    protected DateTimeFormatChooser dateTimeFormatChooser;

    /**
     * Construct an axis setting panel.
     * 
     * @param a
     *            Axis
     * @param axisType
     *            Type of axe
     * @param parentChart
     *            parent chart (can be null)
     */
    public AxisPanel(int type) {
        super(new GridBagLayout());
        this.type = type;
        dateFormat = new SimpleDateFormat(IDateFormattable.US_DATE_FORMAT);
        listeners = Collections.newSetFromMap(new WeakHashMap<AxisPanelListener, Boolean>());
        // Scale
        calendar = Calendar.getInstance();
        scalePanel = new JPanel(new GridBagLayout());
        scalePanel.setBorder(
                CometeUtils.createTitleBorder(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.scale")));
        scaleBoundsSubPanel = new JPanel(new GridBagLayout());
        minLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.minimum.short"));
        minLabel.setFont(CometeUtils.getLabelFont());
        minText = new JTextField(8);
        minText.setMargin(CometeUtils.getTextFieldInsets());
        minLabel.setForeground(CometeUtils.getfColor());
        minLabel.setEnabled(false);
        minText.setText(Double.toString(0));
        minText.setEditable(true);
        minText.setEnabled(false);
        minText.addKeyListener(this);
        maxLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.maximum.short"));
        maxLabel.setFont(CometeUtils.getLabelFont());
        maxText = new JTextField(8);
        maxText.setMargin(CometeUtils.getTextFieldInsets());
        maxLabel.setForeground(CometeUtils.getfColor());
        maxLabel.setHorizontalAlignment(JLabel.RIGHT);
        maxLabel.setEnabled(false);
        maxText.setText(Double.toString(100));
        maxText.setEditable(true);
        maxText.setEnabled(false);
        maxText.addKeyListener(this);
        GridBagConstraints minLabelConstraints = new GridBagConstraints();
        minLabelConstraints.fill = GridBagConstraints.NONE;
        minLabelConstraints.gridx = 0;
        minLabelConstraints.gridy = 0;
        minLabelConstraints.weightx = 0;
        minLabelConstraints.weighty = 0;
        scaleBoundsSubPanel.add(minLabel, minLabelConstraints);
        GridBagConstraints minTextConstraints = new GridBagConstraints();
        minTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        minTextConstraints.gridx = 1;
        minTextConstraints.gridy = 0;
        minTextConstraints.weightx = 0.5;
        minTextConstraints.weighty = 0;
        minTextConstraints.insets = new Insets(0, 5, 0, 30);
        scaleBoundsSubPanel.add(minText, minTextConstraints);
        GridBagConstraints maxLabelConstraints = new GridBagConstraints();
        maxLabelConstraints.fill = GridBagConstraints.NONE;
        maxLabelConstraints.gridx = 2;
        maxLabelConstraints.gridy = 0;
        maxLabelConstraints.weightx = 0;
        maxLabelConstraints.weighty = 0;
        scaleBoundsSubPanel.add(maxLabel, maxLabelConstraints);
        GridBagConstraints maxTextConstraints = new GridBagConstraints();
        maxTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        maxTextConstraints.gridx = 3;
        maxTextConstraints.gridy = 0;
        maxTextConstraints.weightx = 0.5;
        maxTextConstraints.weighty = 0;
        maxTextConstraints.insets = new Insets(0, 5, 0, 0);
        scaleBoundsSubPanel.add(maxText, maxTextConstraints);
        GridBagConstraints scaleBoundsSubPanelConstraints = new GridBagConstraints();
        scaleBoundsSubPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        scaleBoundsSubPanelConstraints.gridx = 0;
        scaleBoundsSubPanelConstraints.gridy = 0;
        scaleBoundsSubPanelConstraints.weightx = 1;
        scaleBoundsSubPanelConstraints.weighty = 0;
        scaleBoundsSubPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        scaleBoundsSubPanelConstraints.insets = LABEL_CONSTRAINTS;
        scalePanel.add(scaleBoundsSubPanel, scaleBoundsSubPanelConstraints);
        scaleLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.scale.mode"));
        scaleLabel.setFont(CometeUtils.getLabelFont());
        scaleLabel.setForeground(CometeUtils.getfColor());
        scaleCombo = new JComboBox<>();
        scaleCombo.setFont(CometeUtils.getLabelFont());
        scaleCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.scale.linear"));
        scaleCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.scale.logarithmic"));
        scaleCombo.setSelectedIndex(0);
        scaleCombo.addActionListener(this);
        GridBagConstraints scaleLabelConstraints = new GridBagConstraints();
        scaleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        scaleLabelConstraints.gridx = 0;
        scaleLabelConstraints.gridy = 1;
        scaleLabelConstraints.weightx = 0;
        scaleLabelConstraints.weighty = 0;
        scaleLabelConstraints.insets = new Insets(5, 5, 0, 10);
        scalePanel.add(scaleLabel, scaleLabelConstraints);
        GridBagConstraints scaleComboConstraints = new GridBagConstraints();
        scaleComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        scaleComboConstraints.gridx = 1;
        scaleComboConstraints.gridy = 1;
        scaleComboConstraints.weightx = 1;
        scaleComboConstraints.weighty = 0;
        scaleComboConstraints.insets = EDITOR_CONSTRAINTS;
        scalePanel.add(scaleCombo, scaleComboConstraints);
        autoScaleCheck = new JCheckBox(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.scale.auto"));
        autoScaleCheck.setFont(CometeUtils.getLabelFont());
        autoScaleCheck.setForeground(CometeUtils.getfColor());
        autoScaleCheck.setSelected(true);
        autoScaleCheck.addActionListener(this);
        GridBagConstraints autoScaleCheckConstraints = new GridBagConstraints();
        autoScaleCheckConstraints.fill = GridBagConstraints.HORIZONTAL;
        autoScaleCheckConstraints.gridx = 0;
        autoScaleCheckConstraints.gridy = 2;
        autoScaleCheckConstraints.weightx = 0;
        autoScaleCheckConstraints.weighty = 0;
        autoScaleCheckConstraints.insets = new Insets(5, 5, 5, 10);
        scalePanel.add(autoScaleCheck, autoScaleCheckConstraints);
        intervalLayout = new CardLayout();
        intervalSubPanel = new JPanel(intervalLayout);
        labelsIntervalSubPanel = new JPanel(new GridBagLayout());
        labelNbLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.interval"));
        labelNbLabel.setFont(CometeUtils.getLabelFont());
        labelNbLabel.setForeground(CometeUtils.getfColor());
        labelNbLabel.setEnabled(false);
        labelNbText = new JTextField(8);
        labelNbText.setMargin(CometeUtils.getTextFieldInsets());
        labelNbText.addKeyListener(this);
        labelNbText.setEnabled(false);
        labelNbText.addFocusListener(new FocusListener() {

            @Override
            public void focusLost(FocusEvent arg0) {
                setUserLabelInterval(properties.getUserLabelInterval());
            }

            @Override
            public void focusGained(FocusEvent arg0) {
                // TODO Auto-generated method stub
            }
        });
        GridBagConstraints labelNbLabelConstraints = new GridBagConstraints();
        labelNbLabelConstraints.fill = GridBagConstraints.NONE;
        labelNbLabelConstraints.gridx = 0;
        labelNbLabelConstraints.gridy = 0;
        labelNbLabelConstraints.weightx = 0;
        labelNbLabelConstraints.weighty = 0;
        labelNbLabelConstraints.insets = new Insets(0, 0, 0, 5);
        labelsIntervalSubPanel.add(labelNbLabel, labelNbLabelConstraints);
        GridBagConstraints labelNbTextConstraints = new GridBagConstraints();
        labelNbTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        labelNbTextConstraints.gridx = 1;
        labelNbTextConstraints.gridy = 0;
        labelNbTextConstraints.weightx = 1;
        labelNbTextConstraints.weighty = 0;
        labelsIntervalSubPanel.add(labelNbText, labelNbTextConstraints);
        intervalSubPanel.add(Box.createGlue(), NO_LABELS_INTERVAL);
        intervalSubPanel.add(labelsIntervalSubPanel, LABELS_INTERVAL);
        GridBagConstraints intervalSubPanelConstraints = new GridBagConstraints();
        intervalSubPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        intervalSubPanelConstraints.gridx = 1;
        intervalSubPanelConstraints.gridy = 2;
        intervalSubPanelConstraints.weightx = 1;
        intervalSubPanelConstraints.weighty = 0;
        intervalSubPanelConstraints.insets = new Insets(5, 0, 5, 5);
        scalePanel.add(intervalSubPanel, intervalSubPanelConstraints);
        GridBagConstraints scalePanelConstraints = new GridBagConstraints();
        scalePanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        scalePanelConstraints.gridx = 0;
        scalePanelConstraints.gridy = 0;
        scalePanelConstraints.weightx = 1;
        scalePanelConstraints.weighty = 0;
        add(scalePanel, scalePanelConstraints);
        // Axis settings
        settingPanel = new JPanel(new GridBagLayout());
        settingPanel.setBorder(CometeUtils
                .createTitleBorder(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.settings")));
        labelFontLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.font"));
        labelFontLabel.setFont(CometeUtils.getLabelFont());
        labelFontLabel.setForeground(CometeUtils.getfColor());
        labelFontSampleLabel = new JSmoothLabel();
        labelFontSampleLabel.setText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.text.sample.numbers"));
        labelFontSampleLabel.setForeground(CometeUtils.getfColor());
        labelFontSampleLabel.setOpaque(false);
        labelFontSampleLabel.setFont(CometeUtils.getLabelFont());
        labelFontBtn = new JButton(SUSPENSION_POINTS);
        labelFontBtn.setToolTipText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.font.choose"));
        labelFontBtn.setMargin(CometeUtils.getBtnInsets());
        labelFontBtn.addActionListener(this);
        formatLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.format"));
        formatLabel.setFont(CometeUtils.getLabelFont());
        formatLabel.setForeground(CometeUtils.getfColor());
        formatCombo = new JComboBox<>();

        ListCellRenderer<? super String> renderer = formatCombo.getRenderer();
        FormatComboRenderer<? super String> formatRenderer = new FormatComboRenderer<>(renderer);
        formatCombo.setRenderer(formatRenderer);

        formatCombo.setFont(CometeUtils.getLabelFont());
        formatCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.format.automatic"));
        formatCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.format.scientific"));
        formatCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.format.time"));
        formatCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.format.decimal"));
        formatCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.format.hexadecimal"));
        formatCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.format.binary"));
        formatCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.format.scientific.int"));
        formatCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.format.date"));
        formatCombo.setSelectedIndex(0);
        formatCombo.addActionListener(this);
        formatBtn = new JButton();
        formatBtn
                .setToolTipText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.label.format.date.select"));
        formatBtn.addActionListener(this);

        int x = 0, y = 0;
        GridBagConstraints labelFontLabelConstraints = new GridBagConstraints();
        labelFontLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        labelFontLabelConstraints.gridx = x++;
        labelFontLabelConstraints.gridy = y;
        labelFontLabelConstraints.weightx = 0;
        labelFontLabelConstraints.weighty = 0;
        labelFontLabelConstraints.insets = LABEL_CONSTRAINTS;
        settingPanel.add(labelFontLabel, labelFontLabelConstraints);
        GridBagConstraints labelFontSampleLabelConstraints = new GridBagConstraints();
        labelFontSampleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        labelFontSampleLabelConstraints.gridx = x++;
        labelFontSampleLabelConstraints.gridy = y;
        labelFontSampleLabelConstraints.weightx = 1;
        labelFontSampleLabelConstraints.weighty = 0;
        labelFontSampleLabelConstraints.insets = EDITOR_CONSTRAINTS;
        settingPanel.add(labelFontSampleLabel, labelFontSampleLabelConstraints);
        GridBagConstraints labelFontBtnConstraints = new GridBagConstraints();
        labelFontBtnConstraints.fill = GridBagConstraints.NONE;
        labelFontBtnConstraints.gridx = x;
        labelFontBtnConstraints.gridy = y++;
        labelFontBtnConstraints.weightx = 0;
        labelFontBtnConstraints.weighty = 0;
        labelFontBtnConstraints.insets = EDITOR_CONSTRAINTS;
        settingPanel.add(labelFontBtn, labelFontBtnConstraints);
        x = 0;

        GridBagConstraints formatLabelConstraints = new GridBagConstraints();
        formatLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        formatLabelConstraints.gridx = x++;
        formatLabelConstraints.gridy = y;
        formatLabelConstraints.weightx = 0;
        formatLabelConstraints.weighty = 0;
        formatLabelConstraints.insets = LABEL_CONSTRAINTS;
        settingPanel.add(formatLabel, formatLabelConstraints);

        GridBagConstraints formatComboConstraints = new GridBagConstraints();
        formatComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        formatComboConstraints.gridx = x++;
        formatComboConstraints.gridy = y;
        formatComboConstraints.weightx = 1;
        formatComboConstraints.weighty = 2;
        formatComboConstraints.insets = EDITOR_CONSTRAINTS;
        // formatComboConstraints.gridwidth = GridBagConstraints.REMAINDER;
        settingPanel.add(formatCombo, formatComboConstraints);

        GridBagConstraints formatBtnConstraints = new GridBagConstraints();
        formatBtnConstraints.fill = GridBagConstraints.NONE;
        formatBtnConstraints.gridx = x;
        formatBtnConstraints.gridy = y++;
        formatBtnConstraints.weightx = 1;
        formatBtnConstraints.weighty = 0;
        formatBtnConstraints.insets = EDITOR_CONSTRAINTS;
        formatComboConstraints.gridwidth = GridBagConstraints.REMAINDER;
        settingPanel.add(formatBtn, formatBtnConstraints);
        formatBtn.setIcon(CALENDAR_ICON);
        x = 0;
        titleLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.title.simple"));
        titleLabel.setFont(CometeUtils.getLabelFont());
        titleLabel.setForeground(CometeUtils.getfColor());
        titleText = new JTextField(8);
        titleText.setMargin(CometeUtils.getTextFieldInsets());
        titleText.setEditable(true);
        titleText.setText(ObjectUtils.EMPTY_STRING);
        titleText.addKeyListener(this);
        titleAlignmentCombo = new JComboBox<>();
        titleAlignmentCombo.setFont(CometeUtils.getLabelFont());
        titleAlignmentCombo.setRenderer(new AlignmentListCellRenderer());
        titleAlignmentCombo.addItem(IComponent.CENTER);
        GridBagConstraints titleLabelConstraints = new GridBagConstraints();
        titleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        titleLabelConstraints.gridx = x++;
        titleLabelConstraints.gridy = y;
        titleLabelConstraints.weightx = 0;
        titleLabelConstraints.weighty = 0;
        titleLabelConstraints.insets = new Insets(5, 5, 0, 10);
        settingPanel.add(titleLabel, titleLabelConstraints);
        GridBagConstraints titleTextConstraints = new GridBagConstraints();
        titleTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        titleTextConstraints.gridx = x++;
        titleTextConstraints.gridy = y;
        titleTextConstraints.weightx = 1;
        titleTextConstraints.weighty = 0;
        titleTextConstraints.insets = EDITOR_CONSTRAINTS;
        settingPanel.add(titleText, titleTextConstraints);
        GridBagConstraints titleAlignmentComboConstraints = new GridBagConstraints();
        titleAlignmentComboConstraints.fill = GridBagConstraints.NONE;
        titleAlignmentComboConstraints.gridx = x;
        titleAlignmentComboConstraints.gridy = y++;
        titleAlignmentComboConstraints.weightx = 0;
        titleAlignmentComboConstraints.weighty = 0;
        titleAlignmentComboConstraints.insets = EDITOR_CONSTRAINTS;
        settingPanel.add(titleAlignmentCombo, titleAlignmentComboConstraints);
        x = 0;
        colorLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.color"));
        colorLabel.setFont(CometeUtils.getLabelFont());
        colorLabel.setForeground(CometeUtils.getfColor());
        colorView = new JLabel(ObjectUtils.EMPTY_STRING);
        colorView.setOpaque(true);
        colorView.setBorder(BorderFactory.createLineBorder(Color.black));
        colorView.setBackground(Color.BLACK);
        colorView.setPreferredSize(CometeUtils.getViewLabelSize());
        colorView.setMinimumSize(colorView.getPreferredSize());
        colorBtn = new JButton();
        colorBtn.setToolTipText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.color.select"));
        colorBtn.setMargin(CometeUtils.getBtnInsets());
        colorBtn.addActionListener(this);
        colorBtn.setIcon(COLOR_WHEEL_ICON);
        GridBagConstraints colorLabelConstraints = new GridBagConstraints();
        colorLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        colorLabelConstraints.gridx = x++;
        colorLabelConstraints.gridy = y;
        colorLabelConstraints.weightx = 0;
        colorLabelConstraints.weighty = 0;
        colorLabelConstraints.insets = new Insets(5, 5, 0, 10);
        settingPanel.add(colorLabel, colorLabelConstraints);
        GridBagConstraints colorViewConstraints = new GridBagConstraints();
        colorViewConstraints.fill = GridBagConstraints.HORIZONTAL;
        colorViewConstraints.gridx = x++;
        colorViewConstraints.gridy = y;
        colorViewConstraints.weightx = 1;
        colorViewConstraints.weighty = 0;
        colorViewConstraints.insets = EDITOR_CONSTRAINTS;
        settingPanel.add(colorView, colorViewConstraints);
        GridBagConstraints colorBtnConstraints = new GridBagConstraints();
        colorBtnConstraints.fill = GridBagConstraints.NONE;
        colorBtnConstraints.gridx = x;
        colorBtnConstraints.gridy = y++;
        colorBtnConstraints.weightx = 0;
        colorBtnConstraints.weighty = 0;
        colorBtnConstraints.insets = EDITOR_CONSTRAINTS;
        settingPanel.add(colorBtn, colorBtnConstraints);
        x = 0;
        positionLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.position"));
        positionLabel.setFont(CometeUtils.getLabelFont());
        positionLabel.setForeground(CometeUtils.getfColor());
        positionCombo = new JComboBox<>();
        positionCombo.setFont(CometeUtils.getLabelFont());
        switch (type) {
            case X_TYPE:
                positionCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.position.down"));
                positionCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.position.up"));
                positionCombo
                        .addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.position.origin.y1"));
                positionCombo
                        .addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.position.origin.y2"));
                positionCombo.setSelectedIndex(0);
                intervalLayout.show(intervalSubPanel, NO_LABELS_INTERVAL);
                titleAlignmentCombo.addItem(IComponent.LEFT);
                titleAlignmentCombo.addItem(IComponent.RIGHT);
                break;
            case Y1_TYPE:
                positionCombo.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.position.left"));
                positionCombo
                        .addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.position.origin.x"));
                positionCombo.setSelectedIndex(0);
                intervalLayout.show(intervalSubPanel, LABELS_INTERVAL);
                titleAlignmentCombo.addItem(IComponent.TOP);
                titleAlignmentCombo.addItem(IComponent.BOTTOM);
                break;
            case Y2_TYPE:
                positionCombo
                        .addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.position.right"));
                positionCombo
                        .addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.position.origin.x"));
                positionCombo.setSelectedIndex(0);
                intervalLayout.show(intervalSubPanel, LABELS_INTERVAL);
                titleAlignmentCombo.addItem(IComponent.TOP);
                titleAlignmentCombo.addItem(IComponent.BOTTOM);
                break;
        }
        positionCombo.addActionListener(this);
        titleAlignmentCombo.addActionListener(this);
        GridBagConstraints positionLabelConstraints = new GridBagConstraints();
        positionLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        positionLabelConstraints.gridx = x++;
        positionLabelConstraints.gridy = y;
        positionLabelConstraints.weightx = 0;
        positionLabelConstraints.weighty = 0;
        positionLabelConstraints.insets = new Insets(5, 5, 0, 10);
        settingPanel.add(positionLabel, positionLabelConstraints);
        GridBagConstraints positionComboConstraints = new GridBagConstraints();
        positionComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        positionComboConstraints.gridx = x;
        positionComboConstraints.gridy = y++;
        positionComboConstraints.weightx = 1;
        positionComboConstraints.weighty = 0;
        positionComboConstraints.insets = EDITOR_CONSTRAINTS;
        positionComboConstraints.gridwidth = GridBagConstraints.REMAINDER;
        settingPanel.add(positionCombo, positionComboConstraints);
        x = 0;
        subGridCheck = new JCheckBox(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.grid.sub.show"));
        subGridCheck.setFont(CometeUtils.getLabelFont());
        subGridCheck.setForeground(CometeUtils.getfColor());
        subGridCheck.setSelected(false);
        subGridCheck
                .setToolTipText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.grid.select.info"));
        subGridCheck.addActionListener(this);
        GridBagConstraints subGridCheckConstraints = new GridBagConstraints();
        subGridCheckConstraints.fill = GridBagConstraints.HORIZONTAL;
        subGridCheckConstraints.gridx = x++;
        subGridCheckConstraints.gridy = y;
        subGridCheckConstraints.weightx = 0;
        subGridCheckConstraints.weighty = 0;
        subGridCheckConstraints.insets = new Insets(5, 5, 0, 10);
        settingPanel.add(subGridCheck, subGridCheckConstraints);
        oppositeCheck = new JCheckBox(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.opposite.draw.text"));
        oppositeCheck.setHorizontalAlignment(JCheckBox.CENTER);
        oppositeCheck.setFont(CometeUtils.getLabelFont());
        oppositeCheck.setForeground(CometeUtils.getfColor());
        oppositeCheck.setSelected(false);
        oppositeCheck.setToolTipText(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.opposite.draw.tooltip"));
        oppositeCheck.addActionListener(this);
        GridBagConstraints oppositeCheckConstraints = new GridBagConstraints();
        oppositeCheckConstraints.fill = GridBagConstraints.HORIZONTAL;
        oppositeCheckConstraints.gridx = x;
        oppositeCheckConstraints.gridy = y++;
        oppositeCheckConstraints.weightx = 1;
        oppositeCheckConstraints.weighty = 0;
        oppositeCheckConstraints.insets = EDITOR_CONSTRAINTS;
        oppositeCheckConstraints.gridwidth = GridBagConstraints.REMAINDER;
        settingPanel.add(oppositeCheck, oppositeCheckConstraints);
        x = 0;
        visibleCheck = new JCheckBox(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.visible"));
        visibleCheck.setFont(CometeUtils.getLabelFont());
        visibleCheck.setForeground(CometeUtils.getfColor());
        visibleCheck.setSelected(false);
        visibleCheck.setToolTipText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.visible.tooltip"));
        visibleCheck.addActionListener(this);
        GridBagConstraints visibleCheckConstraints = new GridBagConstraints();
        visibleCheckConstraints.fill = GridBagConstraints.HORIZONTAL;
        visibleCheckConstraints.gridx = x;
        visibleCheckConstraints.gridy = y;
        visibleCheckConstraints.weightx = 0;
        visibleCheckConstraints.weighty = 0;
        visibleCheckConstraints.insets = new Insets(5, 5, 5, 10);
        settingPanel.add(visibleCheck, visibleCheckConstraints);
        GridBagConstraints settingPanelConstraints = new GridBagConstraints();
        settingPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        settingPanelConstraints.gridx = 0;
        settingPanelConstraints.gridy = 1;
        settingPanelConstraints.weightx = 1;
        settingPanelConstraints.weighty = 0;
        add(settingPanel, settingPanelConstraints);
        initProperties();
    }

    protected void initProperties() {
        setProperties(null, false);
    }

    public void setScalePanelEnabled(boolean enabled) {
        scalePanel.setEnabled(enabled);
        autoScaleCheck.setEnabled(enabled);
        scaleLabel.setEnabled(enabled);
        scaleCombo.setEnabled(enabled);
        enabled = enabled && !autoScaleCheck.isSelected();
        minLabel.setEnabled(enabled);
        minText.setEnabled(enabled);
        maxLabel.setEnabled(enabled);
        maxText.setEnabled(enabled);
        labelNbText.setEnabled(enabled);
        labelNbLabel.setEnabled(enabled);
    }

    protected void applyProperties(boolean timeScale) {
        this.timeScale = timeScale;
        AxisProperties tempProperties = getProperties();
        boolean scaleEnabled = (scalePanel.isEnabled() && (!tempProperties.isAutoScale()));
        minLabel.setEnabled(scaleEnabled);
        minText.setEnabled(scaleEnabled);
        maxLabel.setEnabled(scaleEnabled);
        maxText.setEnabled(scaleEnabled);
        if (timeScale) {
            Date minDate, maxDate;
            synchronized (calendar) {
                calendar.setTimeInMillis((long) tempProperties.getScaleMin());
                minDate = calendar.getTime();
                calendar.setTimeInMillis((long) tempProperties.getScaleMax());
                maxDate = calendar.getTime();
            }
            synchronized (dateFormat) {
                applyDate(minDate, minText);
                applyDate(maxDate, maxText);
            }
        } else {
            applyValue(tempProperties.getScaleMin(), null, minText);
            applyValue(tempProperties.getScaleMax(), null, maxText);
        }
        minText.setToolTipText(minText.getText());
        maxText.setToolTipText(maxText.getText());
        autoScaleCheck.setSelected(tempProperties.isAutoScale());
        scaleCombo.removeActionListener(this);
        // Avoid exception out of bounds
        int scaleMode = tempProperties.getScaleMode();
        int itemCount = scaleCombo.getItemCount();
        int selectedScaleMode = scaleMode >= 0 && scaleMode < itemCount ? scaleMode : 0;
        scaleCombo.setSelectedIndex(selectedScaleMode);
        scaleCombo.addActionListener(this);

        subGridCheck.setSelected(tempProperties.isSubGridVisible());
        visibleCheck.setSelected(tempProperties.isVisible());
        oppositeCheck.setSelected(tempProperties.isDrawOpposite());
        labelFontSampleLabel.setFont(FontTool.getFont(tempProperties.getLabelFont()));
        formatCombo.removeActionListener(this);

        // Avoid exception out of bounds
        int labelFormat = tempProperties.getLabelFormat();
        itemCount = formatCombo.getItemCount();
        int selectedLabelFormat = labelFormat >= 0 && labelFormat < itemCount ? labelFormat : 0;
        formatCombo.setSelectedIndex(selectedLabelFormat);
        formatCombo.addActionListener(this);

        // formatCombo.ListCellRenderer(getRenderer());
        titleText.setText(tempProperties.getTitle());
        Color c = ColorTool.getColor(tempProperties.getColor());
        colorView.setBackground(c);
        labelFontSampleLabel.setForeground(c);
        double lblInterval = tempProperties.getUserLabelInterval();
        double currentLabelInterval;
        try {
            currentLabelInterval = Double.parseDouble(labelNbText.getText().trim());
        } catch (Exception e) {
            currentLabelInterval = Double.NaN;
        }
        if (lblInterval == 0 || Double.isNaN(lblInterval)) {
            if ((currentLabelInterval != 0) && (!Double.isNaN(currentLabelInterval))) {
                labelNbText.setText(ObjectUtils.EMPTY_STRING);
            }
        } else {
            applyValue(lblInterval, DEFAULT_FORMAT, labelNbText);
        }
        labelNbLabel.setEnabled(!tempProperties.isAutoScale());
        labelNbText.setEnabled(!tempProperties.isAutoScale());

        // Avoid exception out of bounds
        int titleAlignment = tempProperties.getTitleAlignment();
        itemCount = titleAlignmentCombo.getItemCount();
        int selectedTitleAlignment = titleAlignment >= 0 && titleAlignment < itemCount ? titleAlignment : 0;
        titleAlignmentCombo.setSelectedItem(selectedTitleAlignment);
        updateFromType(tempProperties);
    }

    protected void applyDate(Date date, JTextField targetField) {
        try {
            Date current = dateFormat.parse(targetField.getText());
            if (current.getTime() != date.getTime()) {
                targetField.setText(dateFormat.format(date));
            }
        } catch (Exception e) {
            targetField.setText(dateFormat.format(date));
        }
    }

    protected void applyValue(double value, String format, JTextField targetField) {
        double current;
        try {
            current = Double.parseDouble(targetField.getText().trim());
        } catch (Exception e) {
            current = Double.NaN;
        }
        if (current != value) {
            if ((format == null) || format.trim().isEmpty()) {
                targetField.setText(Double.toString(value));
            } else {
                String formattedValue = Format.formatValue(value, format);
                String formattedCurrentValue = Format.formatValue(current, format);
                if (formattedValue == null) {
                    formattedValue = Double.toString(value);
                }
                if (formattedCurrentValue == null) {
                    formattedCurrentValue = Double.toString(current);
                }
                if (!formattedValue.equals(formattedCurrentValue)) {
                    targetField.setText(formattedValue);
                }
            }
        }
    }

    protected void updateFromType(AxisProperties tempProperties) {
        positionCombo.removeActionListener(this);
        switch (type) {
            case X_TYPE:
                positionCombo.setSelectedIndex(tempProperties.getPosition() - 1);
                break;
            case Y1_TYPE:
            case Y2_TYPE:
                positionCombo.setSelectedIndex((tempProperties.getPosition() == IChartViewer.VERTICAL_ORGX) ? 1 : 0);
                break;
        }
        positionCombo.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == autoScaleCheck) {
            boolean b = autoScaleCheck.isSelected();
            properties.setAutoScale(b);
            if (!b) {
                double min = getMinOrMax(minText, getProperties().getScaleMin());
                double max = getMinOrMax(maxText, getProperties().getScaleMax());
                if (max > min) {
                    properties.setScaleMin(min);
                    properties.setScaleMax(max);
                }
                if (getType() == IChartViewer.X) {
                    properties.setUserLabelInterval(Double.NaN);
                }
            }
            minLabel.setEnabled(!b);
            minText.setEnabled(!b);
            maxLabel.setEnabled(!b);
            maxText.setEnabled(!b);
            labelNbLabel.setEnabled(!b);
            labelNbText.setEnabled(!b);
            updateAxisRange(null);
            // ------------------------------------------------------------
        } else if (e.getSource() == labelFontBtn) {
            Font f = FontChooser.getNewFont(this, labelFontBtn.getToolTipText(),
                    FontTool.getFont(getProperties().getLabelFont()));
            CometeFont cf = FontTool.getCometeFont(f);
            if ((f != null) && (cf != null)) {
                properties.setLabelFont(cf);
                labelFontSampleLabel.setFont(f);
                fireAxisChanged();
            }
        } else if (e.getSource() == formatCombo) {
            int s = formatCombo.getSelectedIndex();
            properties.setLabelFormat(s);
            fireAxisChanged();
            // ------------------------------------------------------------
        } else if (e.getSource() == positionCombo) {
            int s = positionCombo.getSelectedIndex();
            switch (type) {
                case X_TYPE:
                    properties.setPosition(s + 1);
                    break;
                case Y1_TYPE:
                    switch (s) {
                        case 0:
                            properties.setPosition(IChartViewer.VERTICAL_LEFT);
                            break;
                        case 1:
                            properties.setPosition(IChartViewer.VERTICAL_ORGX);
                            break;
                    }
                    break;
                case Y2_TYPE:
                    switch (s) {
                        case 0:
                            properties.setPosition(IChartViewer.VERTICAL_RIGHT);
                            break;
                        case 1:
                            properties.setPosition(IChartViewer.VERTICAL_ORGX);
                            break;
                    }
                    break;
            }
            fireAxisChanged();
        } else if (e.getSource() == scaleCombo) {
            int s = scaleCombo.getSelectedIndex();
            double min = getMinOrMax(minText, getProperties().getScaleMin());
            double max = getMinOrMax(maxText, getProperties().getScaleMax());
            if (!properties.isAutoScale() && s == IChartViewer.LOG_SCALE && (min <= 0 || max <= 0)) {
                error(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.scale.logarithmic.bounds.error"),
                        null);
                scaleCombo.setSelectedItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.scale.linear"));
            } else {
                properties.setScaleMode(s);
                if (s == IChartViewer.LOG_SCALE) {
                    labelNbText.setEnabled(false);
                    labelNbLabel.setEnabled(false);
                    // pAxis.clearUserLabels();
                    fireAxisChanged();
                } else {
                    if (!autoScaleCheck.isSelected()) {
                        labelNbText.setEnabled(true);
                        labelNbLabel.setEnabled(true);
                    }
                    updateAxisRange(null);
                }
            }
        } else if (e.getSource() == subGridCheck) {
            properties.setSubGridVisible(subGridCheck.isSelected());
            fireAxisChanged();
        } else if (e.getSource() == oppositeCheck) {
            properties.setDrawOpposite(oppositeCheck.isSelected());
            fireAxisChanged();
        } else if (e.getSource() == visibleCheck) {
            properties.setVisible(visibleCheck.isSelected());
            fireAxisChanged();
        } else if (e.getSource() == formatBtn) {
            if (dateTimeFormatChooser == null) {
                dateTimeFormatChooser = new DateTimeFormatChooser(this);
            }
            if (!dateTimeFormatChooser.isVisible()) {
                // dateTimeFormatChooser.setSelectTimeFormat(properties.getTimeFormat());
                dateTimeFormatChooser.setSelectTimeFormat(IDateConstants.TIME_LONG_FORMAT);
                // dateTimeFormatChooser.setSelectDateFormat(properties.getDateFormat());
                dateTimeFormatChooser.setSelectDateFormat(IDateConstants.DATE_CLASSIC_FORMAT);
                dateTimeFormatChooser.setLocationRelativeTo(this);
                dateTimeFormatChooser.setVisible(true);
            }
            String[] selectFormat = dateTimeFormatChooser.getSelectedFormat();
            if (selectFormat != null) {
                if (selectFormat.length > 0) {
                    String selectedDateFormat = selectFormat[0];
                    properties.setDateFormat(selectedDateFormat);
                }
                if (selectFormat.length == 2) {
                    String selectedTimeFormat = selectFormat[1];
                    properties.setTimeFormat(selectedTimeFormat);
                } else {
                    properties.setTimeFormat(null);
                }
                fireAxisChanged();
            }

        } else if (e.getSource() == colorBtn) {
            Color c = JColorChooser.showDialog(this,
                    ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.color.choose"),
                    ColorTool.getColor(properties.getColor()));
            if (c != null) {
                properties.setColor(ColorTool.getCometeColor(c));
                colorView.setBackground(c);
                labelFontSampleLabel.setForeground(c);
                fireAxisChanged();
            }

        } else if (e.getSource() == titleAlignmentCombo) {
            properties.setTitleAlignment(((Number) titleAlignmentCombo.getSelectedItem()).intValue());
            fireAxisChanged();
        }
    }

    protected double getMinOrMax(JTextField textField, double defaultValue) {
        double val;
        try {
            if (timeScale) {
                synchronized (dateFormat) {
                    val = dateFormat.parse(textField.getText()).getTime();
                }
            } else {
                val = Double.parseDouble(textField.getText());
            }
        } catch (Exception ex) {
            val = defaultValue;
        }
        return val;
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if ((e.getSource() == minText || e.getSource() == maxText) && !getProperties().isAutoScale()) {
            updateAxisRange((JTextField) e.getSource());
        } else if (e.getSource() == titleText) {
            properties.setTitle(titleText.getText());
            fireAxisChanged();
        } else if (e.getSource() == labelNbText) {
            updateAxisRange(labelNbText);
        }
    }

    protected void error(String m, JTextField origin) {
        if (origin == null) {
            JOptionPane.showMessageDialog(this, m,
                    ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.option.error"),
                    JOptionPane.ERROR_MESSAGE);
        } else {
            origin.setBackground(Color.ORANGE);
            origin.setToolTipText(m);
        }
    }

    protected void updateAxisRange(JTextField origin) {
        try {
            boolean inError = false;
            if (!autoScaleCheck.isSelected()) {
                double min;
                try {
                    if (timeScale) {
                        synchronized (dateFormat) {
                            min = dateFormat.parse(minText.getText()).getTime();
                        }
                    } else {
                        min = Double.parseDouble(minText.getText());
                    }
                } catch (Exception ex) {
                    error(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.error.malformed"), origin);
                    min = Double.NaN;
                    inError = true;
                }
                if (!inError) {
                    double max;
                    try {
                        if (timeScale) {
                            synchronized (dateFormat) {
                                max = dateFormat.parse(maxText.getText()).getTime();
                            }
                        } else {
                            max = Double.parseDouble(maxText.getText());
                        }
                    } catch (Exception ex) {
                        max = getProperties().getScaleMax();
                        error(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.error.malformed"), origin);
                        max = Double.NaN;
                        inError = true;
                    }
                    if (!inError) {
                        if (max <= min) {
                            error(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.error.bounds"), origin);
                            inError = true;
                        } else {
                            if (!getProperties().isAutoScale() && properties.getScaleMode() == IChartViewer.LOG_SCALE) {
                                if (min <= 0 || max <= 0) {
                                    error(ChartUtils.MESSAGES.getString(
                                            "fr.soleil.comete.swing.chart.scale.logarithmic.bounds.error"), origin);
                                    inError = true;
                                }
                            }
                            if (!inError) {
                                properties.setScaleMin(min);
                                properties.setScaleMax(max);
                            }
                        }
                    }
                }
            }
            if (!inError) {
                if (labelNbText.getText() == null || labelNbText.getText().isEmpty()
                        || labelNbText.getText().equals(ZERO)) {
                    properties.setUserLabelInterval(Double.NaN);
                } else {
                    try {
                        properties.setUserLabelInterval(Double.parseDouble(labelNbText.getText()));
                    } catch (Exception e) {
                        properties.setUserLabelInterval(Double.NaN);
                    }
                }
                if (origin != null) {
                    origin.setBackground(Color.WHITE);
                    origin.setToolTipText(null);
                }
                fireAxisChanged();
            }
        } catch (NumberFormatException err) {
            error(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.error.bounds.malformed"), origin);
        }

    }

    protected void fireAxisChanged() {
        List<AxisPanelListener> copy = new ArrayList<AxisPanelListener>();
        AxisPanelEvent event = new AxisPanelEvent(this);
        synchronized (listeners) {
            copy.addAll(listeners);
        }
        for (AxisPanelListener listener : copy) {
            listener.axisPanelChanged(event);
        }
        copy.clear();
    }

    public void addAxisPanelListener(AxisPanelListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    public void removeAxisPanelListener(AxisPanelListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    public void removeAllAxisPanelListeners() {
        synchronized (listeners) {
            listeners.clear();
        }
    }

    public AxisProperties getProperties() {
        if (properties == null) {
            properties = new AxisProperties();
        }
        return properties.clone();
    }

    public void setUserLabelInterval(double value) {
        properties.setUserLabelInterval(value);
        if (!labelNbText.hasFocus()) {
            String text;
            if (Double.isNaN(value)) {
                text = ObjectUtils.EMPTY_STRING;
            } else {
                text = Double.toString(value);
                // Count nb digit
                int nbDigit = text.length();
                if (nbDigit > 6) {
                    text = Format.formatValue(value, DEFAULT_FORMAT);
                }
            }
            labelNbText.setText(text);
        }
    }

    public void setProperties(AxisProperties axisProperties, boolean timeScale) {
        if (axisProperties == null) {
            properties = new AxisProperties();
            switch (type) {
                case Y1_TYPE:
                    properties.setPosition(6);
                    break;
                case Y2_TYPE:
                    properties.setPosition(5);
                    break;
                case X_TYPE:
                    properties.setPosition(1);
                    break;
            }
        } else {
            properties = axisProperties.clone();
        }
        if (properties.getColor() == null) {
            properties.setColor(CometeColor.BLACK);
        }
        applyProperties(timeScale);
    }

    public int getType() {
        return type;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class FormatComboRenderer<T> implements ListCellRenderer<T> {

        private final ListCellRenderer<T> renderer;

        public FormatComboRenderer(ListCellRenderer<T> renderer) {
            this.renderer = renderer;
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends T> list, T value, int index, boolean isSelected,
                boolean cellHasFocus) {
            Component comp = renderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if ((comp instanceof JLabel) && (properties != null)) {
                JLabel label = (JLabel) comp;
                if (index == 2) {
                    // Index of time
                    String timeFormatText = properties.getTimeFormat();
                    if (timeFormatText == null) {
                        timeFormatText = IDateFormattable.TIME_SHORT_FORMAT;
                    }
                    String labelValue = new StringBuilder(String.valueOf(value)).append(OPEN_PARENTHESIS)
                            .append(timeFormatText).append(CLOSE_PARENTHESIS).toString();
                    label.setText(labelValue);
                    label.setToolTipText(labelValue);
                } else if (index == 7) {
                    // index of Date
                    String dateFormatText = properties.getDateFormat();
                    if (properties.getTimeFormat() != null) {
                        dateFormatText = new StringBuilder(dateFormatText).append(CometeConstants.SPACE)
                                .append(properties.getTimeFormat()).toString();
                    }
                    String labelValue = new StringBuilder(String.valueOf(value)).append(OPEN_PARENTHESIS)
                            .append(dateFormatText).append(CLOSE_PARENTHESIS).toString();
                    label.setText(labelValue);
                    label.setToolTipText(labelValue);
                }

            }
            return comp;
        }

    }

}
