/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.plugin;

import java.awt.event.KeyEvent;

import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import ij.IJ;
import ij.gui.Roi;
import ij.process.ImageProcessor;

/**
 * A {@link CometePlugInFilter} that unifies 2 {@link Roi}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class Union extends CometePlugInFilter {

    public static final String COMMAND_NAME = "union";

    @Override
    public boolean isSetupArgumentMandatory() {
        return false;
    }

    @Override
    public void run(ImageProcessor ip) {
        try {
            IJ.setKeyUp(KeyEvent.VK_CONTROL);
            IMaskedImageViewer imageViewer = recoverImageContext();
            if (imageViewer != null) {
                IJRoiManager roiManager = imageViewer.getRoiManager();
                if (roiManager != null) {
                    roiManager.orSelectedRois();
                }
            }
        } catch (Exception ex) {
            getLogger().error("Failed to apply roi union", ex);
        }

    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }

}
