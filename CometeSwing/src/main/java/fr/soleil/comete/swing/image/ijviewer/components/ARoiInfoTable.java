/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.lang.ref.WeakReference;

import org.jdesktop.swingx.JXTable;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.image.ijviewer.events.IRoiSelectionListener;
import fr.soleil.comete.swing.image.ijviewer.events.RoiEvent;
import ij.gui.Roi;

/**
 * 
 * 
 * @author MAINGUY
 * 
 */

public abstract class ARoiInfoTable extends JXTable implements IRoiSelectionListener {

    private static final long serialVersionUID = 2792940964437008798L;

    protected WeakReference<ImageViewer> viewerRef;
    protected IJRoiManager roiManager;

    protected ARoiInfoTableModel roiTableModel;

    public ARoiInfoTable(ImageViewer viewer) {
        super();

        viewerRef = viewer == null ? null : new WeakReference<ImageViewer>(viewer);

        roiTableModel = createTableModel();
        setModel(roiTableModel);

        if (viewer != null) {
            viewer.addViewerListener(roiTableModel);
        }

        roiManager = viewer.getRoiManager();
        if (roiManager != null) {
            roiManager.addRoiManagerListener(roiTableModel);
            roiManager.addRoiSelectionListener(this);
        }
    }

    /**
     * Create the model (and set other specific table properties related to the model)
     */
    abstract protected ARoiInfoTableModel createTableModel();

    @Override
    public void roiDeselected(RoiEvent event) {
        Roi roi = event.getRoi();
        int index = roiTableModel.getRoiIndex(roi);
        selectionModel.removeSelectionInterval(index, index);
    }

    @Override
    public void roiSelected(RoiEvent event) {
        Roi roi = event.getRoi();
        int index = roiTableModel.getRoiIndex(roi);
        selectionModel.addSelectionInterval(index, index);
    }

}
