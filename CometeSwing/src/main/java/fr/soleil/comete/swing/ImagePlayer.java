/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.listener.IImageViewerListener;
import fr.soleil.comete.definition.listener.MaskListener;
import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.comete.definition.widget.IImagePlayer;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.comete.definition.widget.util.IRoi;
import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MathematicSector;
import fr.soleil.comete.swing.stackviewer.StackViewer;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.FlatMatrix;

/**
 * Swing implementation of {@link IImagePlayer}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ImagePlayer extends StackViewer<IImageViewer> implements IImagePlayer {

    private static final long serialVersionUID = -8556088737696526216L;

    @Override
    public void addImageViewerListener(IImageViewerListener listener) {
    }

    @Override
    public void removeImageViewerListener(IImageViewerListener listener) {
    }

    @Override
    public boolean isShowRoiInformationTable() {
        return getViewer().isShowRoiInformationTable();
    }

    @Override
    public void setShowRoiInformationTable(boolean showTable) {
        getViewer().setShowRoiInformationTable(showTable);
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return getViewer().getMatrixDataHeight(concernedDataClass);
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        return getViewer().getMatrixDataHeight(concernedDataClass);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return getViewer().isPreferFlatValues(concernedDataClass);
    }

    @Override
    public Number[][] getNumberMatrix() {
        // ImageViewer does not return data informations
        return null;
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        setValue(value);
    }

    @Override
    public Number[] getFlatNumberMatrix() {
        // ImageViewer does not return data informations
        return null;
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        getViewer().setFlatNumberMatrix(value, width, height);
    }

    @Override
    public boolean isEditingData() {
        return (getRecordBarPanel().isEditingData() || getSlider().isEditingData());
    }

    @Override
    public String getText() {
        return getViewer().getText();
    }

    @Override
    public void setText(String text) {
        getViewer().setText(text);
    }

    public void setApplicationId(String id) {
        ((ImageViewer) getViewer()).setApplicationId(id);
    }

    @Override
    public boolean isEditable() {
        return getViewer().isEditable();
    }

    @Override
    public void setEditable(boolean editable) {
        getViewer().setEditable(editable);
    }

    @Override
    public String getFormat() {
        return getViewer().getFormat();
    }

    @Override
    public void setFormat(String format) {
        getViewer().setFormat(format);
    }

    public void setValue(Object value) {
        if (value instanceof AbstractNumberMatrix<?>) {
            ((ImageViewer) getViewer()).setData((AbstractNumberMatrix<?>) value);
        } else if (value instanceof FlatMatrix<?>) {
            ((ImageViewer) getViewer()).setData((FlatMatrix<?>) value);
        } else if (value == null || value instanceof Object[]) {
            getViewer().setNumberMatrix((Object[]) value);
        }
    }

    public void setFlatValue(Object flatValue, int... dimensions) {
        if (flatValue == null) {
            getViewer().setFlatNumberMatrix(null, 0, 0);
        } else if (dimensions != null && dimensions.length == 2) {
            getViewer().setFlatNumberMatrix(flatValue, dimensions[1], dimensions[0]);
        }
    }

    @Override
    public int getImageWidth() {
        return getViewer().getImageWidth();
    }

    @Override
    public int getImageHeight() {
        return getViewer().getImageHeight();
    }

    @Override
    public String getImageName() {
        return getViewer().getImageName();
    }

    @Override
    public void setImageName(String name) {
        getViewer().setImageName(name);
    }

    @Override
    public Gradient getGradient() {
        return getViewer().getGradient();
    }

    @Override
    public void setGradient(Gradient gradient) {
        getViewer().setGradient(gradient);
    }

    @Override
    public boolean isUseMaskManagement() {
        return getViewer().isUseMaskManagement();
    }

    @Override
    public Mask getMask() {
        return getViewer().getMask();
    }

    @Override
    public boolean setMask(Mask mask) {
        return getViewer().setMask(mask);
    }

    @Override
    public void setUseMaskManagement(boolean useMask) {
        getViewer().setUseMaskManagement(useMask);
    }

    @Override
    public boolean isUseSectorManagement() {
        return getViewer().isUseSectorManagement();
    }

    @Override
    public void setUseSectorManagement(boolean useSector) {
        getViewer().setUseSectorManagement(useSector);
    }

    @Override
    public void loadMask(String path, boolean addToCurrentMask) {
        getViewer().loadMask(path, addToCurrentMask);
    }

    @Override
    public void saveMask(String path) {
        getViewer().saveMask(path);
    }

    @Override
    public boolean isAlwaysFitMaxSize() {
        return getViewer().isAlwaysFitMaxSize();
    }

    @Override
    public void setAlwaysFitMaxSize(boolean alwaysFitMaxSize) {
        getViewer().setAlwaysFitMaxSize(alwaysFitMaxSize);
    }

    @Override
    public boolean isDrawBeamPosition() {
        return getViewer().isDrawBeamPosition();
    }

    @Override
    public void setDrawBeamPosition(boolean drawBeam) {
        getViewer().setDrawBeamPosition(drawBeam);
    }

    @Override
    public double[] getBeamPoint() {
        return getViewer().getBeamPoint();
    }

    @Override
    public void setBeamPoint(double... theBeamPoint) {
        getViewer().setBeamPoint(theBeamPoint);
    }

    @Override
    public void clearBeamPoint() {
        getViewer().clearBeamPoint();
    }

    @Override
    public MathematicSector getSector() {
        return getViewer().getSector();
    }

    @Override
    public void registerSectorClass(Class<?> sectorClass) {
        getViewer().registerSectorClass(sectorClass);
    }

    @Override
    public void setSector(MathematicSector sector) {
        getViewer().setSector(sector);
    }

    @Override
    public IValueConvertor getXAxisConvertor() {
        return getViewer().getXAxisConvertor();
    }

    @Override
    public IValueConvertor getYAxisConvertor() {
        return getViewer().getYAxisConvertor();
    }

    @Override
    public void setXAxisConvertor(IValueConvertor xAxisConvertor) {
        getViewer().setXAxisConvertor(xAxisConvertor);
    }

    @Override
    public void setYAxisConvertor(IValueConvertor yAxisConvertor) {
        getViewer().setYAxisConvertor(yAxisConvertor);
    }

    @Override
    public boolean isSingleRoiMode() {
        return getViewer().isSingleRoiMode();
    }

    @Override
    public void setSingleRoiMode(boolean singleRoiMode) {
        getViewer().setSingleRoiMode(singleRoiMode);
    }

    @Override
    public boolean isCleanOnDataSetting() {
        return getViewer().isCleanOnDataSetting();
    }

    @Override
    public void setCleanOnDataSetting(boolean clean) {
        getViewer().setCleanOnDataSetting(clean);
    }

    @Override
    public void addRoi(IRoi roi) {
        getViewer().addRoi(roi);
    }

    @Override
    public void addRoi(IRoi roi, boolean centered) {
        getViewer().addRoi(roi, centered);
    }

    @Override
    public void addMaskListener(MaskListener listener) {
        getViewer().addMaskListener(listener);
    }

    @Override
    public void removeAllMaskListeners() {
        getViewer().removeAllMaskListeners();
    }

    @Override
    public void removeMaskListener(MaskListener listener) {
        getViewer().removeMaskListener(listener);
    }

    @Override
    public CometeColor getNanColor() {
        return getViewer().getNanColor();
    }

    @Override
    public void setNanColor(CometeColor nanColor) {
        getViewer().setNanColor(nanColor);
    }

    @Override
    public CometeColor getNegativeInfinityColor() {
        return getViewer().getNegativeInfinityColor();
    }

    @Override
    public void setNegativeInfinityColor(CometeColor negativeInfinityColor) {
        getViewer().setNegativeInfinityColor(negativeInfinityColor);
    }

    @Override
    public CometeColor getPositiveInfinityColor() {
        return getViewer().getPositiveInfinityColor();
    }

    @Override
    public void setPositiveInfinityColor(CometeColor positiveInfinityColor) {
        getViewer().setPositiveInfinityColor(positiveInfinityColor);
    }

    @Override
    public ImageProperties getImageProperties() {
        return getViewer().getImageProperties();
    }

    @Override
    public void setImageProperties(ImageProperties properties) {
        getViewer().setImageProperties(properties);
    }

    @Override
    public void loadDataFile(String fileName) {
        getViewer().loadDataFile(fileName);
    }

    @Override
    public void saveDataFile(String path) {
        getViewer().saveDataFile(path);
    }

    @Override
    public String getDataDirectory() {
        return getViewer().getDataDirectory();
    }

    @Override
    public void setDataDirectory(String path) {
        getViewer().setDataDirectory(path);
    }

    @Override
    public boolean[][] getBooleanMatrix() {
        return getViewer().getBooleanMatrix();
    }

    @Override
    public void setBooleanMatrix(boolean[][] value) {
        getViewer().setBooleanMatrix(value);

    }

    @Override
    public boolean[] getFlatBooleanMatrix() {
        return getViewer().getFlatBooleanMatrix();
    }

    @Override
    public void setFlatBooleanMatrix(boolean[] value, int width, int height) {
        getViewer().setFlatBooleanMatrix(value, height, height);
    }

    @Override
    public String getDataFile() {
        return getViewer().getDataFile();
    }

    @Override
    public void addDataTransferListener(IDataTransferListener listener) {
        getViewer().addDataTransferListener(listener);
    }

    @Override
    public void removeDataTransferListener(IDataTransferListener listener) {
        getViewer().removeDataTransferListener(listener);
    }

    @Override
    protected IImageViewer generateComponent() {
        return new ImageViewer();
    }

    @Override
    public String getXAxisFormat() {
        return getViewer().getXAxisFormat();
    }

    @Override
    public void setXAxisFormat(String format) {
        getViewer().setXAxisFormat(format);
    }

    @Override
    public String getYAxisFormat() {
        return getViewer().getYAxisFormat();
    }

    @Override
    public void setYAxisFormat(String format) {
        getViewer().setYAxisFormat(format);
    }

}
