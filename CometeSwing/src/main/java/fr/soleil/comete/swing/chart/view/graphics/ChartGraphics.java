/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.graphics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.List;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.swing.chart.axis.view.AbstractAxisView;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.DataListContainer;
import fr.soleil.comete.swing.chart.data.DataXY;
import fr.soleil.comete.swing.chart.data.IDataListContainer;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.chart.view.shape.AbstractShape;
import fr.soleil.comete.swing.chart.view.shape.ShapeGenerator;
import fr.soleil.comete.swing.util.CometeUtils;

/**
 * A class that handles common chart data painting
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ChartGraphics {

    /**
     * Default {@link Font} to use in {@link #paintTitle(String, Graphics2D, Rectangle, int, Font, Color)} when
     * given {@link Font} is <code>null</code>
     */
    public static final Font DEFAULT_FONT = new Font(Font.DIALOG, Font.PLAIN, 12);

    public static final String SAMPLING_WARNING = "Warning: too many points in views. Sampling used";
    public static final Font WARNING_FONT = new Font(Font.DIALOG, Font.ITALIC, 10);

    /**
     * Expert usage. Paints dataviews along the given axis.
     * 
     * @param g The graphics in which to paint dataviews
     * @param margin The margin used
     * @param yViews The views to draw
     * @param xViews The the views associated to x axis
     * @param yAxis The axis to which the dataviews are associated
     * @param xAxis The x axis
     * @param xOrg X origin (pixel space)
     * @param yOrg Y origin (pixel space)
     * @param limitDrawingZone A <code>boolean</code> to limit or not drawing zone. <code>true</code> to limit the
     *            drawing zone
     * @param chartBackground The chart background {@link Color}
     * @param samplingProperties The sampling properties to follow
     * @param canWarnForSampling Whether warning message can be displayed in case of sampling
     * @param useTransparency Whether transparency can be used for fill colors
     * @return whether sampling was used
     */
    public static boolean paintDataViews(Graphics2D g, Dimension margin, List<? extends AbstractDataView> yViews,
            List<? extends AbstractDataView> xViews, AbstractAxisView yAxis, AbstractAxisView xAxis, int xOrg, int yOrg,
            boolean limitDrawingZone, Color chartBackground, SamplingProperties samplingProperties,
            boolean canWarnForSampling, boolean useTransparency) {
        boolean warnForSampling = false, computePoint = true;
        SamplingProperties properties = samplingProperties;
        if (properties == null) {
            properties = new SamplingProperties();
        }
        Dimension usedMargin = margin;
        if (usedMargin == null) {
            usedMargin = new Dimension(0, 0);
        }
        boolean useGlobalMean = (properties.getMean() == IChartViewer.GLOBAL_MEAN);
        boolean useGreatestPeaks = (properties.getPeakType() == IChartViewer.PEAK_TYPE_GREATEST);
        boolean useClosestToMean = (properties.getPeakType() == IChartViewer.PEAK_TYPE_CLOSEST);
        double[] currentCoordinates = new double[4];
        double[] minOrPreviousCoordinates = new double[4];
        double[] maxCoordinates = new double[4];
        double[] bufferedCoordinates = new double[4];
        boolean isXY = ((xViews != null) && (!xViews.isEmpty()));
        AbstractDataView vx = null;
        // -------- Clipping
        int xClip = xOrg - 1;
        int yClip = yOrg - (yAxis.getLength() + 1);
        int wClip = xAxis.getLength() + 2;
        int hClip = yAxis.getLength() + 2;
        if (wClip > 1 && hClip > 1) {
            // Maximum number of points = 2 times the display width.
            int maxPoints = wClip * 2;
            if (limitDrawingZone) {
                g.clipRect(xClip, yClip, wClip, hClip);
            }
            // -------- Draw dataView
            if (isXY) {
                vx = xViews.get(0);
            }
            if (yViews != null) {
                for (AbstractDataView v : yViews) {
                    AbstractShape marker = ShapeGenerator.generateMarkerShape(v.getMarker(), v.getAdaptedMarkerSize(),
                            v.getMarkerColor());
                    IDataListContainer l = getDataListContainer(v, vx, isXY);
                    if (l.isValid()) {
                        Color errorColor = (v.isErrorVisible() ? v.getErrorColor() : null);
                        // Transform points
                        int lx, ly;
                        double minx, maxx;
                        double miny, maxy;
                        double vtx, vty;
                        double a0y = v.getA0();
                        double a1y = v.getA1();
                        double a2y = v.getA2();
                        double mean, localMean;
                        double localMinX, localMaxX, error, maxError;
                        int length = v.getDataLength();
                        if (a2y == 0) {
                            mean = ChartGraphicsMath.getTransformedValue(v.getTotalY(), a0y, a1y, a2y);
                        } else {
                            l.init();
                            mean = 0;
                            while (l.isValid()) {
                                vty = ChartGraphicsMath.getYValue(l, a0y, a1y, a2y);
                                if ((!Double.isNaN(vty)) && (!Double.isInfinite(vty))) {
                                    mean += vty;
                                }
                                l.next();
                            }
                            l = getDataListContainer(v, vx, isXY);
                        }
                        mean = (length > 0 ? mean / length : Double.NaN);
                        int count;
                        double a0x, a1x, a2x;
                        if (isXY) {
                            a0x = vx.getA0();
                            a1x = vx.getA1();
                            a2x = vx.getA2();
                        } else {
                            a0x = v.getA0X();
                            a1x = 1;
                            a2x = 0;
                        }
                        int y0;
                        boolean limitBounds = xAxis.getScale().isZoomed() || (!xAxis.getAttributes().isAutoScale());
                        minx = xAxis.getScale().getScaleMinimum();
                        maxx = xAxis.getScale().getScaleMaximum();
                        lx = xAxis.getLength();
                        miny = yAxis.getScale().getScaleMinimum();
                        maxy = yAxis.getScale().getScaleMaximum();
                        ly = yAxis.getLength();
                        int pointIndex = 0, lastValidPointIndex = -1, lastDrawnPointIndex = lastValidPointIndex;
                        boolean valid = true;
                        // Set the stroke mode for dashed line
                        BasicStroke bs = CometeUtils.createStrokeForLine(v.getAdaptedLineWidth(), v.getStyle());
                        // Create the filling pattern
                        Paint fPattern = CometeUtils.createPatternForFilling(v.getFillStyle(), v.getFillColor(),
                                v.getColor(), chartBackground, useTransparency);
                        // Compute zero vertical offset
                        switch (v.getFillMethod()) {
                            case IChartViewer.METHOD_FILL_FROM_TOP:
                                y0 = yOrg - ly;
                                break;
                            case IChartViewer.METHOD_FILL_FROM_ZERO:
                                y0 = yAxis.getScale().getZeroPosition(yOrg, ly);
                                break;
                            default:
                                y0 = yOrg;
                                break;
                        }
                        int barWidth = xAxis.getScale().computeBarWidth(v, vx, xAxis.getLength());

                        // Max number of point
                        int nbPoint = v.getDataLength();
                        if (isXY) {
                            nbPoint += vx.getDataLength();
                        }
                        int firstIndex = -1;
                        int lastIndex = -1;
                        int index = 0;
                        // No need to draw dataviews outside of axis bounds
                        if (limitBounds) {
                            if (l.isValid()) {
                                l.init();
                            }
                            while (l.isValid()) {
                                vtx = xAxis.getScale()
                                        .getScaledValue(ChartGraphicsMath.getXValue(l, a0x, a1x, a2x, isXY));
                                if ((vtx >= minx) && (vtx <= maxx)) {
                                    if (firstIndex == -1) {
                                        firstIndex = index;
                                    }
                                    lastIndex = index;
                                }
                                index++;
                                l.next();
                            }
                            l = getDataListContainer(v, vx, isXY);
                            index = 0;
                            if (firstIndex == -1) {
                                nbPoint = 0;
                            } else {
                                firstIndex--;
                                if (firstIndex < 0) {
                                    firstIndex = 0;
                                }
                                lastIndex++;
                                if (lastIndex >= nbPoint) {
                                    lastIndex = nbPoint - 1;
                                }
                                nbPoint = lastIndex + 1 - firstIndex;
                            }
                        }
                        // Now that dataviews bounds are limited, check whether to sample
                        boolean useSampling = properties.isEnabled() && v.isSamplingAllowed() && (nbPoint > maxPoints);
                        int step = 1;
                        int currentIndex = 0;
                        if (useSampling) {
                            int formerNbPoint = nbPoint;
                            step = (int) Math.floor(nbPoint / (double) maxPoints);
                            if (step < 1) {
                                step = 1;
                            }
                            nbPoint = nbPoint / step;
                            useSampling = (formerNbPoint != nbPoint);
                        }
                        if (nbPoint > -1) {
                            boolean hasNaN = false;
                            int[] pointX = new int[nbPoint];
                            int[] pointY = new int[nbPoint];
                            int[] pointErrorMin = new int[nbPoint];
                            int[] pointErrorMax = new int[nbPoint];

                            while (l.isValid()) {
                                // Go to starting time position
                                hasNaN = false;
                                lastDrawnPointIndex = lastValidPointIndex = -1;
                                l.init();
                                if (firstIndex != -1) {
                                    while ((index < firstIndex) && l.isValid()) {
                                        index++;
                                        l.next();
                                    }
                                }
                                while (valid && l.isValid() && !hasNaN) {
                                    // In this loop, we iterate on each curve point or interval point (for sampling)

                                    // Compute transform here for performance
                                    ChartGraphicsMath.computeBasicCoordinates(currentCoordinates, l, a0x, a1x, a2x, a0y,
                                            a1y, a2y, isXY);
                                    // currentCoordinates = {y, x, errorMin, errorMax}
                                    valid = ChartGraphicsMath.isValidAllY(currentCoordinates, pointIndex, nbPoint,
                                            xAxis, yAxis);
                                    if (valid) {
                                        hasNaN = updatePointTables(usedMargin, currentCoordinates, pointX, pointY,
                                                pointErrorMin, pointErrorMax, pointIndex, minx, maxx, miny, maxy, xAxis,
                                                yAxis, lx, ly, xOrg, yOrg);
                                        // Go to next position
                                        if (useSampling) {
                                            // Sampling case: search for the point representing the sampling interval
                                            if (hasNaN) {
                                                // consider whole interval as a hole
                                                while ((currentIndex != step) && l.isValid()) {
                                                    currentIndex++;
                                                    l.next();
                                                }
                                                computePoint = false;
                                            } else {
                                                // compute min and max on current interval
                                                count = 1;
                                                localMean = currentCoordinates[0];
                                                minOrPreviousCoordinates[0] = currentCoordinates[0];
                                                maxCoordinates[0] = currentCoordinates[0];
                                                minOrPreviousCoordinates[1] = currentCoordinates[1];
                                                maxCoordinates[1] = currentCoordinates[1];
                                                minOrPreviousCoordinates[2] = currentCoordinates[2];
                                                maxCoordinates[2] = currentCoordinates[2];
                                                minOrPreviousCoordinates[3] = currentCoordinates[3];
                                                maxCoordinates[3] = currentCoordinates[3];
                                                warnForSampling = true;
                                                computePoint = true;
                                                localMinX = currentCoordinates[1];
                                                localMaxX = localMinX;
                                                maxError = ChartGraphicsMath.getMaxError(currentCoordinates);
                                                while ((currentIndex != step) && l.isValid() && !hasNaN) {
                                                    currentIndex++;
                                                    l.next();
                                                    if (useGlobalMean && (useGreatestPeaks || useClosestToMean)) {
                                                        // In case of globalMean with greatest peaks or closest to mean,
                                                        // store previous coordinates
                                                        System.arraycopy(currentCoordinates, 0,
                                                                minOrPreviousCoordinates, 0, currentCoordinates.length);
                                                    }
                                                    ChartGraphicsMath.computeBasicCoordinates(currentCoordinates, l,
                                                            a0x, a1x, a2x, a0y, a1y, a2y, isXY);
                                                    // currentCoordinates = {y, x, errorMin, errorMax}
                                                    if (ChartGraphicsMath.isValid(currentCoordinates, currentIndex,
                                                            step, xAxis, yAxis)) {
                                                        localMean += currentCoordinates[0];
                                                        count++;
                                                        if (Double.isNaN(localMinX)
                                                                || currentCoordinates[1] < localMinX) {
                                                            localMinX = currentCoordinates[1];
                                                        }
                                                        if (Double.isNaN(localMaxX)
                                                                || currentCoordinates[1] > localMaxX) {
                                                            localMaxX = currentCoordinates[1];
                                                        }
                                                        error = ChartGraphicsMath.getMaxError(currentCoordinates);
                                                        if (Double.isNaN(maxError) || error > maxError) {
                                                            maxError = error;
                                                        }
                                                        if (useGlobalMean && useGreatestPeaks) {
                                                            computePoint = false;
                                                            // GlobalMean with greatest peaks case:
                                                            // try to keep greatest distance to global mean
                                                            if (Math.abs(currentCoordinates[0] - mean) > Math
                                                                    .abs(minOrPreviousCoordinates[0] - mean)) {
                                                                hasNaN = updatePointTables(usedMargin,
                                                                        currentCoordinates, pointX, pointY,
                                                                        pointErrorMin, pointErrorMax, pointIndex, minx,
                                                                        maxx, miny, maxy, xAxis, yAxis, lx, ly, xOrg,
                                                                        yOrg) || hasNaN;
                                                            }
                                                        } else if (useGlobalMean && useClosestToMean) {
                                                            computePoint = false;
                                                            // GlobalMean with closest to mean case:
                                                            // try to keep closest distance to global mean
                                                            if (Math.abs(currentCoordinates[0] - mean) < Math
                                                                    .abs(minOrPreviousCoordinates[0] - mean)) {
                                                                hasNaN = updatePointTables(usedMargin,
                                                                        currentCoordinates, pointX, pointY,
                                                                        pointErrorMin, pointErrorMax, pointIndex, minx,
                                                                        maxx, miny, maxy, xAxis, yAxis, lx, ly, xOrg,
                                                                        yOrg) || hasNaN;
                                                            }
                                                        } else {
                                                            if (currentCoordinates[0] < minOrPreviousCoordinates[0]) {
                                                                System.arraycopy(currentCoordinates, 0,
                                                                        minOrPreviousCoordinates, 0,
                                                                        currentCoordinates.length);
                                                            }
                                                            if (currentCoordinates[0] > maxCoordinates[0]) {
                                                                System.arraycopy(currentCoordinates, 0, maxCoordinates,
                                                                        0, currentCoordinates.length);
                                                            }
                                                        }
                                                    } else {
                                                        if (currentIndex < step && l.isValid()) {
                                                            // consider whole interval as a hole
                                                            computePoint = false;
                                                            hasNaN = updatePointTables(usedMargin, currentCoordinates,
                                                                    pointX, pointY, pointErrorMin, pointErrorMax,
                                                                    pointIndex, minx, maxx, miny, maxy, xAxis, yAxis,
                                                                    lx, ly, xOrg, yOrg) || hasNaN;
                                                            while ((currentIndex != step) && l.isValid()) {
                                                                currentIndex++;
                                                                l.next();
                                                            }
                                                        }
                                                        break;
                                                    } // end if (isValid(currentCoordinates, currentIndex, step, xAxis,
                                                      // yAxis)) ... else
                                                } // end while ((currentIndex != step) && l.isValid())
                                                if (hasNaN) {
                                                    // consider whole interval as a hole
                                                    while ((currentIndex != step) && l.isValid()) {
                                                        currentIndex++;
                                                        l.next();
                                                    }
                                                    computePoint = false;
                                                }
                                                // Compute drawing points if not already done
                                                if (computePoint) {
                                                    localMean /= count;
                                                    switch (properties.getPeakType()) {
                                                        case IChartViewer.PEAK_TYPE_MIN:
                                                            // keep local minimum
                                                            hasNaN = updatePointTables(usedMargin,
                                                                    minOrPreviousCoordinates, pointX, pointY,
                                                                    pointErrorMin, pointErrorMax, pointIndex, minx,
                                                                    maxx, miny, maxy, xAxis, yAxis, lx, ly, xOrg, yOrg)
                                                                    || hasNaN;
                                                            break;
                                                        case IChartViewer.PEAK_TYPE_MAX:
                                                            // keep local maximum
                                                            hasNaN = updatePointTables(usedMargin, maxCoordinates,
                                                                    pointX, pointY, pointErrorMin, pointErrorMax,
                                                                    pointIndex, minx, maxx, miny, maxy, xAxis, yAxis,
                                                                    lx, ly, xOrg, yOrg) || hasNaN;
                                                            break;
                                                        case IChartViewer.PEAK_TYPE_GREATEST:
                                                            // try to keep absolute max on current interval
                                                            // Only local mean case at this point (see computePoint)
                                                            if ((minOrPreviousCoordinates[0] != currentCoordinates[0])
                                                                    || (maxCoordinates[0] != currentCoordinates[0])) {
                                                                if (Math.abs(
                                                                        minOrPreviousCoordinates[0] - localMean) > Math
                                                                                .abs(maxCoordinates[0] - localMean)) {
                                                                    hasNaN = updatePointTables(usedMargin,
                                                                            minOrPreviousCoordinates, pointX, pointY,
                                                                            pointErrorMin, pointErrorMax, pointIndex,
                                                                            minx, maxx, miny, maxy, xAxis, yAxis, lx,
                                                                            ly, xOrg, yOrg) || hasNaN;
                                                                } else {
                                                                    hasNaN = updatePointTables(usedMargin,
                                                                            maxCoordinates, pointX, pointY,
                                                                            pointErrorMin, pointErrorMax, pointIndex,
                                                                            minx, maxx, miny, maxy, xAxis, yAxis, lx,
                                                                            ly, xOrg, yOrg) || hasNaN;
                                                                }
                                                            }
                                                            break;
                                                        case IChartViewer.PEAK_TYPE_LOWEST:
                                                            // try to keep the peak closest to desired mean
                                                            double desiredMean = useGlobalMean ? mean : localMean;
                                                            if (Math.abs(
                                                                    minOrPreviousCoordinates[0] - desiredMean) < Math
                                                                            .abs(maxCoordinates[0] - desiredMean)) {
                                                                hasNaN = updatePointTables(usedMargin,
                                                                        minOrPreviousCoordinates, pointX, pointY,
                                                                        pointErrorMin, pointErrorMax, pointIndex, minx,
                                                                        maxx, miny, maxy, xAxis, yAxis, lx, ly, xOrg,
                                                                        yOrg) || hasNaN;
                                                            } else {
                                                                hasNaN = updatePointTables(usedMargin, maxCoordinates,
                                                                        pointX, pointY, pointErrorMin, pointErrorMax,
                                                                        pointIndex, minx, maxx, miny, maxy, xAxis,
                                                                        yAxis, lx, ly, xOrg, yOrg) || hasNaN;
                                                            }
                                                            break;
                                                        default:
                                                            // Only local mean case at this point (see computePoint)
                                                            bufferedCoordinates[0] = localMean;
                                                            bufferedCoordinates[1] = (localMinX + localMaxX) / 2;
                                                            bufferedCoordinates[2] = localMean - maxError;
                                                            bufferedCoordinates[3] = localMean + maxError;
                                                            hasNaN = updatePointTables(usedMargin, bufferedCoordinates,
                                                                    pointX, pointY, pointErrorMin, pointErrorMax,
                                                                    pointIndex, minx, maxx, miny, maxy, xAxis, yAxis,
                                                                    lx, ly, xOrg, yOrg) || hasNaN;
                                                            break;
                                                    } // end switch (properties.getPeakType())
                                                } // end if (computePoint)
                                                if (hasNaN) {
                                                    // consider whole interval as a hole
                                                    while ((currentIndex != step) && l.isValid()) {
                                                        currentIndex++;
                                                        l.next();
                                                    }
                                                }
                                                currentIndex = 0;
                                                computePoint = true;
                                                if (!l.isValid()) {
                                                    break;
                                                }
                                            } // end if (Double.isNaN(currentCoordinates[0])) ... else
                                        } else {
                                            // No sampling case: just go to next point
                                            l.next();
                                        } // end if (useSampling) ... else

                                        // Here: dataview's next point or sampling interval point is obtained

                                        // JAVAAPI-631: Don't draw if there is a NaN --> create a hole
                                        if (pointIndex < nbPoint && !hasNaN) {
                                            lastValidPointIndex = pointIndex;
                                            // Draw marker
                                            if (marker != null) {
                                                marker.setX(pointX[pointIndex]);
                                                marker.setY(pointY[pointIndex]);
                                                marker.drawShape(g);
                                            }
                                            // Draw error
                                            if ((errorColor != null)
                                                    && ((pointErrorMin[pointIndex] != pointY[pointIndex])
                                                            || (pointErrorMax[pointIndex] != pointY[pointIndex]))
                                                    && (!Double.isNaN(pointErrorMin[pointIndex]))
                                                    && (!Double.isNaN(pointErrorMax[pointIndex]))) {
                                                Color c = g.getColor();
                                                g.setColor(errorColor);
                                                g.drawLine(pointX[pointIndex] - 2, pointErrorMin[pointIndex],
                                                        pointX[pointIndex] + 2, pointErrorMin[pointIndex]);
                                                g.drawLine(pointX[pointIndex] - 2, pointErrorMax[pointIndex],
                                                        pointX[pointIndex] + 2, pointErrorMax[pointIndex]);
                                                g.drawLine(pointX[pointIndex], pointErrorMin[pointIndex],
                                                        pointX[pointIndex], pointErrorMax[pointIndex]);
                                                if (c != null) {
                                                    g.setColor(c);
                                                }
                                            }
                                            // Draw bar
                                            paintDataViewBar(g, v, barWidth, bs, fPattern, y0, pointX[pointIndex],
                                                    pointY[pointIndex]);
                                        } // end if (pointIndex < nbPoint)

                                        pointIndex++;
                                    } // end if (valid)
                                } // end while (valid && l.isValid() && !hasNaN)

                                // Here, one or many valid points where found: draw them.
                                // JAVAAPI-631: Don't draw NaN points --> create a hole
                                if (hasNaN) {
                                    // Check lastValidPointIndex to ensure not missing some points to be drawn
                                    if (lastValidPointIndex > -1 && lastDrawnPointIndex < lastValidPointIndex) {
                                        // Draw the polyline
                                        paintDataViewPolyline(g, v, bs, fPattern, lastValidPointIndex + 1, y0, pointX,
                                                pointY);
                                        lastDrawnPointIndex = lastValidPointIndex;
                                    }
                                } else {
                                    // Draw the polyline
                                    paintDataViewPolyline(g, v, bs, fPattern, pointIndex, y0, pointX, pointY);
                                    lastDrawnPointIndex = pointIndex - 1;
                                }
                                pointIndex = 0;
                                if ((hasNaN || !valid) && l.isValid()) {
                                    // Go to next position
                                    l.next();
                                    valid = true;
                                    hasNaN = false;
                                }
                            } // end while (l.isValid())
                        } // end if (nbPoint > -1)
                    } // end if (l.isValid())
                } // end for (AbstractDataView v : yViews)

                // All dataviews are drawn. Sampling warning message may be displayed.
                if (warnForSampling && canWarnForSampling) {
                    // When sampling used, display warning message
                    Font formerFont = g.getFont();
                    Color formerColor = g.getColor();
                    int x = 0;
                    int y = 0;
                    g.setFont(WARNING_FONT);
                    g.setColor(Color.RED);
                    FontRenderContext frc = g.getFontRenderContext();
                    if (frc != null) {
                        Rectangle2D bounds = WARNING_FONT.getStringBounds(SAMPLING_WARNING, frc);
                        if (bounds != null) {
                            x = (int) (wClip - bounds.getWidth()) / 2;
                            y = (int) (bounds.getHeight());
                        }
                    }
                    g.drawString(SAMPLING_WARNING, x + xClip, y + yClip);
                    if (formerFont != null) {
                        g.setFont(formerFont);
                    }
                    if (formerColor != null) {
                        g.setColor(formerColor);
                    }
                }
            } // end if (yViews != null)
        } // end if (wClip > 1 && hClip > 1)
        return warnForSampling;
    }

    public static void paintDataViewBar(Graphics2D g, AbstractDataView v, int barWidth, BasicStroke bs, Paint fPattern,
            int y0, int x, int y) {
        if (v.getViewType() == IChartViewer.TYPE_BAR) {
            paintBar(g, fPattern, barWidth, v.getFillColor(), v.getFillStyle(), y0, x, y);
            // Draw bar border
            if (v.getAdaptedLineWidth() > 0) {
                Stroke old = g.getStroke();
                if (bs != null) {
                    g.setStroke(bs);
                }
                g.setColor(v.getColor());
                paintBarBorder(g, barWidth, y0, x, y);
                g.setStroke(old);
            }
        }
    }

    protected static void paintBarBorder(Graphics g, int barWidth, int y0, int x, int y) {
        g.drawLine(x - barWidth / 2, y, x + barWidth / 2, y);
        g.drawLine(x + barWidth / 2, y, x + barWidth / 2, y0);
        g.drawLine(x + barWidth / 2, y0, x - barWidth / 2, y0);
        g.drawLine(x - barWidth / 2, y0, x - barWidth / 2, y);
    }

    protected static void paintBar(Graphics2D g, Paint pattern, int barWidth, Color background, int fillStyle, int y0,
            int x, int y) {
        if (fillStyle != IChartViewer.FILL_STYLE_NONE) {
            g.setPaint(pattern == null ? background : pattern);
            if (y > y0) {
                g.fillRect(x - barWidth / 2, y0, barWidth, y - y0);
            } else {
                g.fillRect(x - barWidth / 2, y, barWidth, (y0 - y));
            }
        }
    }

    public static void paintDataViewPolyline(Graphics2D g, AbstractDataView v, BasicStroke bs, Paint fPattern, int nb,
            int yOrg, int[] pointX, int[] pointY) {
        if (nb > 0) {
            if (v.getViewType() == IChartViewer.TYPE_LINE) {
                paintDataViewPolylineNoCheck(g, v, bs, fPattern, nb, yOrg, pointX, pointY);
            } else if (v.getViewType() == IChartViewer.TYPE_STAIRS) {
                int nb2 = 2 * nb - 1;
                int[] pointX2, pointY2;
                pointX2 = new int[nb2];
                pointY2 = new int[nb2];
                for (int i = 0; i < nb; i++) {
                    pointX2[2 * i] = pointX[i];
                    pointY2[2 * i] = pointY[i];
                    if (i < nb - 1) {
                        pointX2[2 * i + 1] = pointX[i + 1];
                        pointY2[2 * i + 1] = pointY[i];
                    }
                }
                paintDataViewPolylineNoCheck(g, v, bs, fPattern, nb2, yOrg, pointX2, pointY2);
            }
        }
    }

    protected static void paintDataViewPolylineNoCheck(Graphics2D g, AbstractDataView v, BasicStroke bs, Paint fPattern,
            int nb, int yOrg, int[] pointX, int[] pointY) {
        // Ensure drawing at least a pixel
        int nb2 = nb;
        int[] pointX2 = pointX;
        int[] pointY2 = pointY;
        if (nb == 1) {
            nb2 = 2;
            pointX2 = new int[] { pointX[0], pointX[0] };
            pointY2 = new int[] { pointY[0], pointY[0] };
        }
        int length = Math.min(pointX2.length, pointY2.length);
        length = Math.min(length, nb2);
        // Draw surface
        if (v.getFillStyle() != IChartViewer.FILL_STYLE_NONE) {
            int[] Xs = new int[length + 2];
            int[] Ys = new int[length + 2];
            for (int i = 0; i < length; i++) {
                Xs[i + 1] = pointX2[i];
                Ys[i + 1] = pointY2[i];
            }
            Xs[0] = Xs[1];
            Ys[0] = yOrg;
            Xs[length + 1] = Xs[length];
            Ys[length + 1] = yOrg;
            if (fPattern != null) {
                g.setPaint(fPattern);
            }
            g.fillPolygon(Xs, Ys, length + 2);
        }
        if (v.getAdaptedLineWidth() > 0) {
            Stroke old = g.getStroke();
            if (bs != null) {
                g.setStroke(bs);
            }
            g.setColor(v.getColor());
            // XXX RG: drawPolyline is a lot slower than for... drawLine
            if (length > 1) {
                for (int i = 0; i < length - 1; i++) {
                    g.drawLine(pointX2[i], pointY2[i], pointX2[i + 1], pointY2[i + 1]);
                }
            } else if (length == 1) {
                g.drawLine(pointX2[0], pointY2[0], pointX2[0], pointY2[0]);
            }
            // g.drawPolyline(pointX2, pointY2, nb2);
            if (old != null) {
                g.setStroke(old);
            }
        }
    }

    /**
     * Paints a title in a {@link Rectangle}
     * 
     * @param title The title
     * @param g The {@link Graphics2D} in which to paint the title
     * @param titleR The {@link Rectangle}
     * @param titleWidth The previously calculated title width
     * @param titleFont The title {@link Font}
     * @param titleColor The title {@link Color}
     */
    public static void paintTitle(String title, Graphics2D g, Rectangle titleR, int titleWidth, Font titleFont,
            Color titleColor) {
        if ((title != null) && (titleR != null)) {
            // Draw title
            if (titleR.width > 0) {
                Font font = (titleFont == null ? DEFAULT_FONT : titleFont);
                g.setFont(font);
                int xpos = ((titleR.width - titleWidth) / 2);
                g.setColor(titleColor == null ? Color.BLACK : titleColor);
                g.drawString(title, xpos, titleR.y + g.getFontMetrics(font).getAscent() - 1);
            }
        }
    }

    /**
     * Computes the pixel representation of given coordinates.
     * 
     * @param margin The x and y margin.
     * @param basicCoordinates The coordinates <i>(<code>{y, x, errorMin, errorMax}</code>)</i>.
     * @param pointX The array that store the abscissa in pixel coordinates.
     * @param pointY The array that store the ordinate in pixel coordinates.
     * @param pointErrorMin The array that store the error minimum in pixel coordinates.
     * @param pointErrorMax The array that store the error maximum in pixel coordinates.
     * @param index The index at which to put the calculated pixel coordinates
     * @param minX The axis minimum X.
     * @param maxX The axis maximum X.
     * @param minY The axis minimum Y.
     * @param maxY The axis maximum Y.
     * @param xAxis The x axis.
     * @param yAxis The y axis.
     * @param width The width.
     * @param height The height.
     * @param xOrg The x origin.
     * @param yOrg The y origin.
     * @return whether a <code>NaN</code> value was encountered for x or y coordinate.
     */
    protected static boolean updatePointTables(Dimension margin, double[] basicCoordinates, int[] pointX, int[] pointY,
            int[] pointErrorMin, int[] pointErrorMax, int index, double minX, double maxX, double minY, double maxY,
            AbstractAxisView xAxis, AbstractAxisView yAxis, int width, int height, int xOrg, int yOrg) {
        boolean hasNaN = Double.isNaN(basicCoordinates[0]) || Double.isNaN(basicCoordinates[1]);
        double ratio = ChartUtils.getRatio(basicCoordinates[0], yOrg, height, margin.height, yAxis, xAxis);
        if (Double.isNaN(ratio)) {
            hasNaN = true;
        }
        pointY[index] = (int) Math.round(ratio);
        ratio = ChartUtils.getRatio(basicCoordinates[1], xOrg, width, margin.width, xAxis, yAxis);
        if (Double.isNaN(ratio)) {
            hasNaN = true;
        }
        pointX[index] = (int) Math.round(ratio);
        pointErrorMin[index] = (int) Math
                .round(ChartUtils.getRatio(basicCoordinates[2], yOrg, height, margin.height, yAxis, xAxis));
        pointErrorMax[index] = (int) Math
                .round(ChartUtils.getRatio(basicCoordinates[3], yOrg, height, margin.height, yAxis, xAxis));
        return hasNaN;
    }

    protected static IDataListContainer getDataListContainer(AbstractDataView v, AbstractDataView vx, boolean isXY) {
        return (isXY ? new DataXY(v.getData(), vx.getData()) : new DataListContainer(v.getData()));
    }

}
