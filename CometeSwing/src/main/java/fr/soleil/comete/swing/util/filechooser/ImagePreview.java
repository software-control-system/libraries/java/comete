/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util.filechooser;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jdesktop.swingx.JXImagePanel;

/**
 * This component displays a tiny preview for the selected image file in JFileChooser.
 *
 * @author MAINGUY
 *
 */
public class ImagePreview extends JPanel implements PropertyChangeListener {

    private static final long serialVersionUID = -8648677379236086193L;
    protected final static int IMAGE_SIZE = 95;
    protected final static int BORDER_SIZE = 7;

    protected File file = null;
    protected JXImagePanel imagePanel;

    public ImagePreview(JFileChooser fc) {
        fc.addPropertyChangeListener(this);

        imagePanel = new JXImagePanel();
        imagePanel.setPreferredSize(new Dimension(IMAGE_SIZE, IMAGE_SIZE));

        // JFileCHooser already has empty space on the right border
        setBorder(new EmptyBorder(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, 0));
        setPreferredSize(new Dimension(IMAGE_SIZE + BORDER_SIZE, IMAGE_SIZE + BORDER_SIZE));

        setLayout(new BorderLayout());
        add(imagePanel, BorderLayout.CENTER);
    }

    protected void loadImage() {
        if (file == null) {
            imagePanel.setImage(null);
        } else {
            try {
                Image img = ImageIO.read(file);
                // scale image if it is too big, but keep as is if it is too small
                // JXImagePanel always scale so we have to separate cases
                if (img != null) {
                    if (img.getWidth(null) > IMAGE_SIZE) {
                        imagePanel.setStyle(JXImagePanel.Style.SCALED_KEEP_ASPECT_RATIO);
                    } else {
                        imagePanel.setStyle(JXImagePanel.Style.CENTERED);
                    }
                }
                imagePanel.setImage(img);
            } catch (IOException e) {
                // we also arrive here when validating a new filename
                // e.printStackTrace();

                // setImage(null);
            }
        }
    }

    /* (non-Javadoc)
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent e) {
        boolean update = false;
        String prop = e.getPropertyName();

        // If the directory changed, don't show an image.
        if (JFileChooser.DIRECTORY_CHANGED_PROPERTY.equals(prop)) {
            file = null;
            update = true;
        }
        // If a file became selected, find out which one.
        else if (JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals(prop)) {
            file = (File) e.getNewValue();
            update = true;
        }

        // Update the preview accordingly.
        if (update) {
            if (isShowing()) {
                loadImage();
                repaint();
            }
        }
    }
}
