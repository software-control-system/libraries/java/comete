/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.ListIterator;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.plaf.metal.MetalButtonUI;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.IButtonListener;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IButton;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.comete.swing.util.IconConstants;
import fr.soleil.comete.swing.util.ImageTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;

public abstract class AbstractButton extends JButton implements IconConstants, IButton, ActionListener {

    private static final long serialVersionUID = -7756879897167282478L;

    private static final BasicButtonUI BASIC_UI = new BasicButtonUI();
    private static final MetalButtonUI METAL_UI = new MetalButtonUI();

    private final List<IButtonListener> buttonListeners;
    protected long oldEvent;
    private String actionName;
    private boolean buttonLook;
    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;

    public AbstractButton() {
        super();
        buttonListeners = new ArrayList<IButtonListener>();
        oldEvent = 0;
        actionName = ObjectUtils.EMPTY_STRING;
        buttonLook = true;
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        setSize(70, 25);
        setButtonLook(buttonLook);
        addActionListener(this);
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    @Override
    public void setEditable(boolean editable) {
        // Nothing to do: a button is not editable
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        setOpaque(color != null);
        super.setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(CometeFont font) {
        super.setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        super.setForeground(ColorTool.getColor(color));
    }

    @Override
    public boolean isButtonLook() {
        return buttonLook;
    }

    @Override
    public void setButtonLook(boolean buttonLook) {
        this.buttonLook = buttonLook;
        if (buttonLook) {
            setUI(METAL_UI);
        } else {
            setUI(BASIC_UI);
        }
        setBorderPainted(buttonLook);
        setForeground(Color.BLACK);
    }

    @Override
    public void addButtonListener(IButtonListener listener) {
        if (!buttonListeners.contains(listener)) {
            buttonListeners.add(listener);
        }
    }

    @Override
    public void removeButtonListener(IButtonListener listener) {
        if (buttonListeners.contains(listener)) {
            synchronized (buttonListeners) {
                buttonListeners.remove(listener);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        long actualEvent = e.getWhen();
        if (oldEvent != actualEvent) {
            oldEvent = actualEvent;
            fireActionPerformed(new EventObject(this));
        }
    }

    @Override
    public void fireActionPerformed(EventObject event) {
        ListIterator<IButtonListener> iterator = buttonListeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().actionPerformed(event);
        }
    }

    @Override
    public CometeImage getCometeImage() {
        CometeImage cometeImage = null;
        Icon currentIcon = getIcon();
        if (currentIcon instanceof ImageIcon) {
            cometeImage = ImageTool.getCometeImage((ImageIcon) currentIcon);
        }
        return cometeImage;
    }

    @Override
    public void setCometeImage(CometeImage image) {
        setIcon(ImageTool.getImage(image));
    }

    @Override
    public void execute() {
        ActionEvent event = new ActionEvent(this, this.hashCode(), actionName, System.currentTimeMillis(), 0);
        actionPerformed(event);
    }

    @Override
    public String getActionName() {
        return actionName;
    }

    @Override
    public void setActionName(String actionName) {
        this.actionName = actionName;
        setActionCommand(actionName);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to
     *            {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
