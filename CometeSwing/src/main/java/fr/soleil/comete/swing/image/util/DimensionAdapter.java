/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util;

import fr.soleil.comete.definition.event.ValueConvertorEvent;
import fr.soleil.comete.definition.listener.IValueConvertorListener;
import fr.soleil.comete.definition.util.AbstractValueConvertor;
import fr.soleil.comete.definition.util.IValueConvertor;

public class DimensionAdapter extends AbstractValueConvertor implements IValueConvertorListener {
    protected int dim;
    protected IValueConvertor convertor;
    protected boolean reduceIndex;

    public DimensionAdapter() {
        dim = 0;
        convertor = null;
        reduceIndex = false;
    }

    protected void checkIndexReduction() {
        boolean reduceIndex;
        IValueConvertor convertor = this.convertor;
        int dim = this.dim;
        if (convertor == null) {
            reduceIndex = false;
        } else if (convertor.isValid() && Double.isNaN(convertor.convertValue(dim))) {
            reduceIndex = true;
        } else {
            reduceIndex = false;
        }
        this.reduceIndex = reduceIndex;
    }

    public void setDim(int dim, boolean warn) {
        if (this.dim != dim) {
            this.dim = dim;
            checkIndexReduction();
            if (warn) {
                warnListeners();
            }
        }
    }

    public void setConvertor(IValueConvertor convertor, boolean warn) {
        if (this.convertor != convertor) {
            if (this.convertor != null) {
                this.convertor.removeValueConvertorListener(this);
            }
            this.convertor = convertor;
            checkIndexReduction();
            if (this.convertor != null) {
                this.convertor.addValueConvertorListener(this);
            }
            if (warn) {
                warnListeners();
            }
        }
    }

    @Override
    public boolean isValid() {
        return (dim > 0) && ((convertor == null) || convertor.isValid());
    }

    @Override
    public double convertValue(double value) {
        double result = dim - value;
        IValueConvertor convertor = this.convertor;
        if (convertor != null) {
            // When at exact limit, use previous value, to avoid having a NaN (solves GBS-123)
            if ((result == Math.floor(result)) && reduceIndex) {
                result--;
            }
            result = convertor.convertValue(result);
        }
        return result;
    }

    @Override
    public void convertorChanged(ValueConvertorEvent event) {
        checkIndexReduction();
        warnListeners();
    }

}
