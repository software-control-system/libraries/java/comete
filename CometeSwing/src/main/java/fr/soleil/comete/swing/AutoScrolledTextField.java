/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import fr.soleil.comete.definition.widget.IAutoScrolledTextField;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.FontUtils;

/**
 * A {@link TextField} for which the text is automatically scrolled if too long.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AutoScrolledTextField extends TextField
        implements IAutoScrolledTextField, ComponentListener, FocusListener {

    private static final long serialVersionUID = -7572718408367976124L;

    protected ScrollMode scrollMode;
    protected volatile int stepTime;
    protected volatile int waitingTime;
    protected int currentPosition;
    protected boolean reversed;
    protected ScrollThread scrollThread;

    /**
     * Constructor
     */
    public AutoScrolledTextField() {
        super();
        scrollMode = ScrollMode.LOOP;
        stepTime = 5;
        waitingTime = 2000;
        currentPosition = 0;
        reversed = false;
        scrollThread = null;
        addComponentListener(this);
        addFocusListener(this);
    }

    @Override
    protected void doSetText(String text) {
        super.doSetText(text);
        restartAutoScrollIfPossible();
    }

    @Override
    public void setEditable(boolean editable) {
        super.setEditable(editable);
        if (editable) {
            if (isEnabled()) {
                restartAutoScrollIfPossible();
            }
        } else {
            cancelAutoScroll();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            if (isEditable()) {
                restartAutoScrollIfPossible();
            }
        } else {
            cancelAutoScroll();
        }
    }

    /**
     * Cancels the scroll thread
     */
    protected void cancelAutoScroll() {
        if (scrollThread != null) {
            scrollThread.setCanceled(true);
            scrollThread = null;
        }
        currentPosition = 0;
        reversed = false;
    }

    /**
     * Restarts the scroll thread, if necessary, resetting text position and
     * scroll direction
     */
    protected void restartAutoScrollIfPossible() {
        cancelAutoScroll();
        if (isScrollingAllowed()) {
            scrollThread = new ScrollThread();
            scrollThread.start();
        } else {
            repaint();
        }
    }

    /**
     * Returns this textfield's text width
     * 
     * @return An <code>text</code>
     */
    protected int getTextWidth() {
        int textWidth = FontUtils.measureString(getText(), getFont()).width;
        return textWidth;
    }

    /**
     * Returns the width available to display some text
     * 
     * @return An <code>text</code>
     */
    protected int getDisplayableWidth() {
        int myWidth = getWidth();
        Insets insets = getInsets();
        if (insets != null) {
            myWidth -= (insets.left + insets.right);
        }
        Border border = getBorder();
        if (border != null) {
            insets = border.getBorderInsets(this);
            if (insets != null) {
                myWidth -= (insets.left + insets.right);
            }
        }
        if (myWidth < 0) {
            myWidth = 0;
        }
        return myWidth;
    }

    protected boolean isScrollingAllowed() {
        return ((stepTime > 0) && (!hasFocus()) && (getTextWidth() > getDisplayableWidth()));
    }

    @Override
    public ScrollMode getScrollMode() {
        return scrollMode;
    }

    @Override
    public void setScrollMode(ScrollMode scrollMode) {
        if (this.scrollMode != scrollMode) {
            if (scrollMode == null) {
                scrollMode = ScrollMode.LOOP;
            }
            this.scrollMode = scrollMode;
            restartAutoScrollIfPossible();
        }
    }

    @Override
    public int getScrollStepsTime() {
        return stepTime;
    }

    @Override
    public void setScrollStepsTime(int stepTime) {
        if (stepTime < 0) {
            stepTime = 0;
        }
        if (this.stepTime != stepTime) {
            boolean restartScroll = ((this.stepTime == 0) || (stepTime == 0));
            this.stepTime = stepTime;
            if (restartScroll) {
                restartAutoScrollIfPossible();
            }
        }
    }

    @Override
    public int getReachEndWaitingTime() {
        return waitingTime;
    }

    @Override
    public void setReachEndWaitingTime(int waitingTime) {
        if (waitingTime < 0) {
            waitingTime = 0;
        }
        this.waitingTime = waitingTime;
    }

    @Override
    public void paint(Graphics g) {
        if ((hasFocus() && isEditable() && isEnabled()) || !(g instanceof Graphics2D)) {
            super.paint(g);
        } else {
            CometeUtils.prepareGraphicsForText(g);
            int width = getWidth();
            int height = getHeight();
            if (isOpaque()) {
                g.setColor(getBackground());
                g.fillRect(0, 0, width, height);
            }
            String text = getText();
            if (text == null) {
                text = ObjectUtils.EMPTY_STRING;
            }
            FontRenderContext frc = ((Graphics2D) g).getFontRenderContext();
            if (isEnabled()) {
                g.setColor(getForeground());
            } else {
                Color disabledColor = getDisabledTextColor();
                if (disabledColor == null) {
                    disabledColor = getForeground();
                }
                g.setColor(disabledColor);
            }
            Rectangle2D textBounds = getFont().getStringBounds(text, frc);
            g.setFont(getFont());
            double ascent = getFont().getLineMetrics(text, frc).getAscent();
            int y = (int) ((height - textBounds.getHeight()) / 2.0 + ascent);
            int align = SwingConstants.LEFT;
            if (!isScrollingAllowed()) {
                align = getHorizontalAlignment();
            }
            int x;
            Insets margin = getMargin();
            Insets borderMargin = null;
            Border border = getBorder();
            if (border != null) {
                borderMargin = border.getBorderInsets(this);
            }
            switch (align) {
                case SwingConstants.CENTER:
                    x = (int) ((width - textBounds.getWidth()) / 2.0);
                    break;
                case SwingConstants.RIGHT:
                    x = (int) (width - textBounds.getWidth());
                    if (margin != null) {
                        x -= margin.right;
                    }
                    if (borderMargin != null) {
                        x -= borderMargin.right;
                    }
                    break;
                default:
                    x = 1;
                    if (margin != null) {
                        x += margin.left;
                    }
                    if (borderMargin != null) {
                        x += borderMargin.left;
                    }
            }
            g.drawString(text, x + currentPosition, y);
            paintBorder(g);
        }
    }

    @Override
    public void componentResized(ComponentEvent e) {
        if ((e != null) && (e.getSource() == this)) {
            restartAutoScrollIfPossible();
        }
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        // nothing to do
    }

    @Override
    public void componentShown(ComponentEvent e) {
        // nothing to do
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        // nothing to do
    }

    @Override
    public void focusGained(FocusEvent e) {
        if ((e != null) && (e.getSource() == this) && isEditable() && isEnabled()) {
            restartAutoScrollIfPossible();
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        if ((e != null) && (e.getSource() == this) && isEditable() && isEnabled()) {
            restartAutoScrollIfPossible();
        }
    }

    public static void main(String[] args) {
        final AutoScrolledTextField textField = new AutoScrolledTextField();
        JFrame frame = new JFrame(AutoScrolledTextField.class.getSimpleName() + " test");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel mainPanel = new JPanel(new BorderLayout());
        JLabel focusLabel = new JLabel("click here to loose focus");
        focusLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                ((JLabel) e.getSource()).grabFocus();
            }
        });
        JLabel modeLabel = new JLabel("Scrolling mode: " + textField.getScrollMode());
        modeLabel.setBorder(new LineBorder(Color.RED, 2));
        modeLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                ScrollMode scrollMode = null;
                switch (textField.getScrollMode()) {
                    case LOOP:
                        scrollMode = ScrollMode.PENDULAR;
                        break;
                    case PENDULAR:
                        scrollMode = ScrollMode.REACH_END;
                        break;
                    case REACH_END:
                        scrollMode = ScrollMode.LOOP;
                        break;
                }
                ((JLabel) e.getSource()).setText("Scrolling mode: " + scrollMode);
                textField.setScrollMode(scrollMode);
            }
        });

        JPanel speedPanel = new JPanel(new BorderLayout());
        JButton speedUpButton = new JButton("Speed up");
        speedUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int stepTime = textField.getScrollStepsTime();
                if (stepTime == 0) {
                    stepTime = 100;
                } else if (stepTime > 1) {
                    stepTime /= 2;
                }
                textField.setScrollStepsTime(stepTime);
            }
        });
        JButton slowDownButton = new JButton("Slow down");
        slowDownButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int stepTime = textField.getScrollStepsTime();
                if (stepTime > 0) {
                    stepTime *= 2;
                }
                if (stepTime >= 100) {
                    stepTime = 0;
                }
                textField.setScrollStepsTime(stepTime);
            }
        });
        textField.setText(
                "aaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbccccccccccccccccccccccddddddddddddddddddddeeeeeeeeeeeeeeeeeeeeeeeeeeee");
        textField.setEditable(false);
        speedPanel.add(speedUpButton, BorderLayout.NORTH);
        speedPanel.add(slowDownButton, BorderLayout.SOUTH);

        mainPanel.add(textField, BorderLayout.NORTH);
        mainPanel.add(focusLabel, BorderLayout.CENTER);
        mainPanel.add(modeLabel, BorderLayout.SOUTH);
        mainPanel.add(speedPanel, BorderLayout.EAST);
        frame.setContentPane(mainPanel);
        frame.pack();
        frame.setSize(600, frame.getHeight());
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * This is the {@link Thread} that calculates the text positions when
     * scrolling is activated
     */
    protected class ScrollThread extends Thread implements ICancelable {
        protected volatile boolean canceled;
        protected volatile int sleepTime;

        public ScrollThread() {
            canceled = false;
            sleepTime = stepTime;
        }

        @Override
        public void run() {
            while (!canceled) {
                try {
                    repaint();
                    if (sleepTime > 0) {
                        sleep(sleepTime);
                    }
                    sleepTime = stepTime;
                    if (reversed) {
                        currentPosition++;
                        switch (getScrollMode()) {
                            case PENDULAR:
                                if (currentPosition >= 0) {
                                    currentPosition = 0;
                                    reversed = false;
                                    sleepTime = waitingTime;
                                }
                                break;
                            case REACH_END:
                                if (currentPosition <= 0) {
                                    currentPosition = 0;
                                    reversed = false;
                                    sleepTime = waitingTime;
                                }
                                break;
                            case LOOP:
                                // nothing to do: loop mode is never reversed
                                break;
                        }
                    } else {
                        currentPosition--;
                        int textWidth = getTextWidth();
                        int myWidth = getDisplayableWidth();
                        switch (getScrollMode()) {
                            case LOOP:
                                if (currentPosition <= -textWidth) {
                                    currentPosition = textWidth;
                                }
                                break;
                            case PENDULAR:
                            case REACH_END:
                                if (currentPosition <= (myWidth - textWidth)) {
                                    reversed = true;
                                    sleepTime = waitingTime;
                                }
                        }
                    }
                    repaint();
                } catch (InterruptedException ie) {
                    // Nothing to do: ignore this exception
                }
            }
        }

        @Override
        public boolean isCanceled() {
            return canceled;
        }

        @Override
        public void setCanceled(boolean canceled) {
            this.canceled = canceled;
            if (canceled) {
                interrupt();
            }
        }
    }

}
