/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.data;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.IChartConst;
import fr.soleil.comete.definition.widget.util.IChartConst.FormatNAN;
import fr.soleil.comete.swing.chart.DataViewOption;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.util.CfFileReader;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.OFormat;
import fr.soleil.comete.swing.util.SwingFormat;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;

public abstract class AbstractDataView {

    public static final String NAME = "_name";
    public static final String AXIS = "_axis";
    protected static final String DISPLAYNAME = "_displayname";
    protected static final String LINECOLOR = "_linecolor";
    protected static final String LINEWIDTH = "_linewidth";
    protected static final String LINESTYLE = "_linestyle";
    protected static final String FILLCOLOR = "_fillcolor";
    protected static final String FILLMETHOD = "_fillmethod";
    protected static final String FILLSTYLE = "_fillstyle";
    protected static final String VIEWTYPE = "_viewtype";
    protected static final String BARWIDTH = "_barwidth";
    protected static final String MARKERCOLOR = "_markercolor";
    protected static final String MARKERSIZE = "_markersize";
    protected static final String MARKERSTYLE = "_markerstyle";
    protected static final String A0 = "_A0";
    protected static final String A1 = "_A1";
    protected static final String A2 = "_A2";
    protected static final String LABELVISIBLE = "_labelvisible";
    protected static final String CLICKABLE = "_clickable";
    protected static final String LABEL_COLOR = "_labelColor";

    protected static final String PERCENT = "%";
    protected static final String F = "f";
    protected static final String E = "e";
    protected static final String EXT_NAME_OPENER = " [";
    protected static final String EXT_NAME_CLOSER = "]";
    protected static final String PLUS = " + ";
    protected static final String MINUS = " - ";
    protected static final String TIMES_Y = "*y";
    protected static final String TIMES_Y_SQUARE = "*y^2";
    protected static final String VS_X = " VS [x";

    // Local declaration
    protected Color lineColor;
    protected Color fillColor;
    protected Color markerColor;
    protected Color errorColor;
    protected Color labelColor;
    protected int lineStyle;
    protected int lineWidth;
    protected int markerType;
    protected int markerSize;
    protected int barWidth;
    protected int fillStyle;
    protected int fillMethod;
    protected int type;
    protected double a0X;
    protected double a0;
    protected double a1;
    protected double a2;
    protected DataList theData;
    protected DataList theFilteredData;
    protected int dataLength;
    protected int filteredDataLength;
    protected DataList theDataEnd;
    protected DataList theFilteredDataEnd;
    protected double max;
    protected double min;
    protected double totalY;
    protected double maxXValue;
    protected double minXValue;
    protected String id;
    protected String displayName;
    protected String unit;
    protected boolean clickable;
    protected boolean labelVisible;
    protected boolean errorVisible;
    protected boolean samplingAllowed;
    protected String format;
    protected int interpMethod;
    protected int interpStep;
    protected double interpTension;
    protected double interpBias;
    protected int smoothMethod;
    protected double[] smoothCoefs;
    protected int smoothNeighbor;
    protected double smoothSigma;
    protected int smoothExtrapolation;
    protected int mathFunction;

    // A boolean to know whether data is supposed to be sorted on x
    protected boolean xDataSorted;

    // A boolean to know if datas must be reseted (true) or not (false).
    protected boolean fixed;

    protected boolean highlighted;
    protected int highlightMethod;
    protected double highlightCoefficient;
    protected boolean shouldComputeX;
    protected boolean shouldComputeY;

    public AbstractDataView() {
        this(null);
    }

    /**
     * DataView constructor.
     */
    public AbstractDataView(String id) {
        theData = null;
        theFilteredData = null;
        theDataEnd = null;
        dataLength = 0;
        if (id == null) {
            this.id = CometeUtils.generateIdForClass(getClass(), 50);
        } else {
            this.id = id;
        }
        displayName = id;
        unit = ObjectUtils.EMPTY_STRING;
        lineColor = Color.RED;
        fillColor = Color.LIGHT_GRAY;
        markerColor = Color.RED;
        errorColor = Color.RED;
        min = Double.MAX_VALUE;
        max = -Double.MAX_VALUE;
        totalY = 0;
        minXValue = Double.MAX_VALUE;
        maxXValue = -Double.MAX_VALUE;
        markerType = IChartViewer.MARKER_NONE;
        lineStyle = IChartViewer.STYLE_SOLID;
        type = IChartViewer.TYPE_LINE;
        fillStyle = IChartViewer.FILL_STYLE_NONE;
        fillMethod = IChartViewer.METHOD_FILL_FROM_BOTTOM;
        lineWidth = 1;
        barWidth = 10;
        markerSize = 6;
        a0 = 0;
        a1 = 1;
        a2 = 0;
        clickable = true;
        labelVisible = true;
        errorVisible = true;
        samplingAllowed = true;
        format = null;
        labelColor = Color.BLACK;
        interpMethod = IChartViewer.INTERPOLATE_NONE;
        interpStep = 10;
        interpTension = 0.0;
        interpBias = 0.0;
        smoothMethod = IChartViewer.SMOOTH_NONE;
        smoothCoefs = null;
        smoothNeighbor = 3;
        smoothSigma = 0.5;
        smoothExtrapolation = IChartViewer.SMOOTH_EXT_LINEAR;
        mathFunction = IChartViewer.MATH_NONE;
        xDataSorted = true;
        fixed = false;
        highlighted = false;
        highlightMethod = IChartViewer.HIGHLIGHT_MULTIPLY;
        highlightCoefficient = IChartConst.DEFAULT_HIGHLIGHT_COEFF;
        shouldComputeX = true;
        shouldComputeY = true;
    }

    /**
     * Expert usage. Gets the parent axis.
     * 
     * @return Parent axis
     */
    public abstract Object getAxis();

    // -----------------------------
    // ------- Global config -------
    // -----------------------------

    /**
     * Sets the graph type (Line or BarGraph).
     * 
     * @param s Type of graph
     * @see IChartViewer#TYPE_LINE
     * @see IChartViewer#TYPE_BAR
     * @see IChartViewer#TYPE_STAIRS
     */
    public void setViewType(int s) {
        type = s;
    }

    /**
     * Gets view type.
     * 
     * @return View type
     * @see #setViewType
     */
    public int getViewType() {
        return type;
    }

    /**
     * Sets the filling style of this view.
     * 
     * @param b Filling style
     * @see IChartViewer#FILL_STYLE_NONE
     * @see IChartViewer#FILL_STYLE_SOLID
     * @see IChartViewer#FILL_STYLE_LARGE_RIGHT_HATCH
     * @see IChartViewer#FILL_STYLE_LARGE_LEFT_HATCH
     * @see IChartViewer#FILL_STYLE_LARGE_CROSS_HATCH
     * @see IChartViewer#FILL_STYLE_SMALL_RIGHT_HATCH
     * @see IChartViewer#FILL_STYLE_SMALL_LEFT_HATCH
     * @see IChartViewer#FILL_STYLE_SMALL_CROSS_HATCH
     * @see IChartViewer#FILL_STYLE_DOT_PATTERN_1
     * @see IChartViewer#FILL_STYLE_DOT_PATTERN_2
     * @see IChartViewer#FILL_STYLE_DOT_PATTERN_3
     */

    public void setFillStyle(int b) {
        fillStyle = b;
    }

    /**
     * Gets the current filling style.
     * 
     * @return Filling style
     * @see #setFillStyle
     */
    public int getFillStyle() {
        return fillStyle;
    }

    /**
     * Sets the filling method for this view.
     * 
     * @param m Filling method
     * @see #METHOD_FILL_FROM_TOP
     * @see #METHOD_FILL_FROM_ZERO
     * @see #METHOD_FILL_FROM_BOTTOM
     */

    public void setFillMethod(int m) {
        fillMethod = m;
    }

    /**
     * Gets the current filling style.
     * 
     * @return Filling method
     * @see #setFillMethod
     */
    public int getFillMethod() {
        return fillMethod;
    }

    /**
     * Sets the filling color of this dataView.
     * 
     * @param c the filling color
     * @see #getFillColor
     */
    public void setFillColor(Color c) {
        fillColor = c;
    }

    /**
     * Gets the filling color.
     * 
     * @return the filling color
     * @see #setFillColor
     */
    public Color getFillColor() {
        return fillColor;
    }

    /**
     * Sets this view clickable or not. When set to false, this view is excluded from the search that occurs when the
     * user click on the chart. Default is rue.
     * 
     * @param b Clickable state
     */
    public void setClickable(boolean b) {
        clickable = b;
    }

    /**
     * Returns the clickable state.
     * 
     * @see #setClickable
     */
    public boolean isClickable() {
        return clickable;
    }

    // -----------------------------
    // -------- Line config --------
    // -----------------------------

    /**
     * Sets the color of the curve.
     * 
     * @param c
     *            Curve color
     * @see #getColor
     */
    public void setColor(Color c) {
        lineColor = c;
    }

    /**
     * Gets the curve color.
     * 
     * @return Curve color
     * @see #setColor
     */
    public Color getColor() {
        return lineColor;
    }

    /**
     * Provided for backward compatibility.
     * 
     * @see #setFillStyle
     * @return true if the view is filled, false otherwise
     */
    public boolean isFill() {
        return fillStyle != IChartViewer.FILL_STYLE_NONE;
    }

    /**
     * Provided for backward compatibility.
     * 
     * @param b true if the view is filled, false otherwise
     * @see #setFillStyle
     */
    public void setFill(boolean b) {
        if (!b) {
            setFillStyle(IChartViewer.FILL_STYLE_NONE);
        } else {
            setFillStyle(IChartViewer.FILL_STYLE_SOLID);
        }
    }

    /**
     * Set the plot line style.
     * 
     * @param c Line style
     * @see #STYLE_SOLID
     * @see #STYLE_DOT
     * @see #STYLE_DASH
     * @see #STYLE_LONG_DASH
     * @see #STYLE_DASH_DOT
     * @see #getStyle
     */
    public void setStyle(int c) {
        lineStyle = c;
    }

    /**
     * Gets the line style.
     * 
     * @return Line style
     * @see #setStyle
     */
    public int getStyle() {
        return lineStyle;
    }

    /**
     * Gets the line width.
     * 
     * @return Line width
     * @see #setLineWidth
     */
    public int getLineWidth() {
        return lineWidth;
    }

    /**
     * Sets the line width (pixel).
     * 
     * @param c Line width (pixel)
     * @see #getLineWidth
     */
    public void setLineWidth(int c) {
        lineWidth = c;
    }

    /**
     * Gets the line width.
     * 
     * @return Line width
     * @see #setLineWidth
     */
    public int getAdaptedLineWidth() {
        if (highlighted) {
            double temp = lineWidth;
            switch (highlightMethod) {
                case IChartViewer.HIGHLIGHT_ADD:
                    temp = lineWidth + highlightCoefficient;
                    break;
                case IChartViewer.HIGHLIGHT_MULTIPLY:
                    temp = lineWidth * highlightCoefficient;
                    break;
            }
            if (temp < 0) {
                temp = 0;
            } else if (temp > Integer.MAX_VALUE) {
                temp = Integer.MAX_VALUE;
            }
            return (int) Math.round(temp);
        } else {
            return lineWidth;
        }
    }

    // -----------------------------
    // ---- Interpolation config ---
    // -----------------------------

    /**
     * Set an interpolation on this dataview using the specified method. (Cubic interpolation requires constant x
     * interval)
     * 
     * @param method Interpolation method
     * @see IChartViewer#INTERPOLATE_NONE
     * @see IChartViewer#INTERPOLATE_LINEAR
     * @see IChartViewer#INTERPOLATE_COSINE
     * @see IChartViewer#INTERPOLATE_CUBIC
     * @see IChartViewer#INTERPOLATE_HERMITE
     */
    public void setInterpolationMethod(int method) {
        interpMethod = method;
        commitChange();
    }

    /**
     * Return current interpolation mode.
     * 
     * @see #setInterpolationMethod
     */
    public int getInterpolationMethod() {
        return interpMethod;
    }

    /**
     * Sets the interpolation step
     * 
     * @param step Interpolation step (must be >=2)
     * @see #setInterpolationMethod
     */
    public void setInterpolationStep(int step) {
        if (step < 2) {
            step = 2;
        }
        interpStep = step;
        updateFilters();
    }

    /**
     * Returns the interpolation step.
     * 
     * @see #setInterpolationStep
     */
    public int getInterpolationStep() {
        return interpStep;
    }

    /**
     * Set the Hermite interpolation tension coefficient
     * 
     * @param tension Hermite interpolation tension coefficient (1=>high, 0=>normal, -1=>low)
     */
    public void setHermiteTension(double tension) {
        if (tension < -1.0) {
            tension = -1.0;
        }
        if (tension > 1.0) {
            tension = 1.0;
        }
        interpTension = tension;
        updateFilters();
    }

    /**
     * Get the Hermite interpolation tension coefficient
     * 
     * @see #setHermiteTension
     */
    public double getHermiteTension() {
        return interpTension;
    }

    /**
     * Set the Hermite interpolation bias coefficient. 0 for no bias, positive value towards first segment, negative
     * value towards the others.
     * 
     * @param bias Hermite interpolation bias coefficient
     */
    public void setHermiteBias(double bias) {
        interpBias = bias;
        updateFilters();
    }

    /**
     * Set the Hermite interpolation bias coefficient.
     */
    public double getHermiteBias() {
        return interpBias;
    }

    // -----------------------------
    // ------ Smoothing config -----
    // -----------------------------

    /**
     * Sets the smoothing method (Convolution product). Requires constant x intervals.
     * 
     * @param method Smoothing filer type
     * @see IChartViewer#SMOOTH_NONE
     * @see IChartViewer#SMOOTH_FLAT
     * @see IChartViewer#SMOOTH_TRIANGULAR
     * @see IChartViewer#SMOOTH_GAUSSIAN
     */
    public void setSmoothingMethod(int method) {
        smoothMethod = method;
        updateSmoothCoefs();
        commitChange();
    }

    /**
     * Return the smoothing method.
     * 
     * @see #setSmoothingMethod
     */
    public int getSmoothingMethod() {
        return smoothMethod;
    }

    /**
     * Sets number of neighbors for smoothing
     * 
     * @param n Number of neighbors (Must be >=2)
     */
    public void setSmoothingNeighbors(int n) {
        smoothNeighbor = (n / 2) * 2 + 1;
        if (smoothNeighbor < 3) {
            smoothNeighbor = 3;
        }
        updateSmoothCoefs();
        updateFilters();
    }

    /**
     * Sets number of neighbors for smoothing
     * 
     * @see #setSmoothingNeighbors
     */
    public int getSmoothingNeighbors() {
        return smoothNeighbor - 1;
    }

    /**
     * Sets the standard deviation of the gaussian (Smoothing filter).
     * 
     * @param sigma
     *            Standard deviation
     * @see #setSmoothingMethod
     */
    public void setSmoothingGaussSigma(double sigma) {
        smoothSigma = sigma;
        updateSmoothCoefs();
        commitChange();
    }

    /**
     * Return the standard deviation of the gaussian (Smoothing filter).
     * 
     * @see #setSmoothingMethod
     */
    public double getSmoothingGaussSigma() {
        return smoothSigma;
    }

    /**
     * Sets the extrapolation method used in smoothing operation
     * 
     * @param extMode
     *            Extrapolation mode
     * @see IChartViewer#SMOOTH_EXT_NONE
     * @see IChartViewer#SMOOTH_EXT_FLAT
     * @see IChartViewer#SMOOTH_EXT_LINEAR
     */
    public void setSmoothingExtrapolation(int extMode) {
        smoothExtrapolation = extMode;
        updateFilters();
    }

    /**
     * Returns the extrapolation method used in smoothing operation.
     * 
     * @see #setSmoothingExtrapolation
     */
    public int getSmoothingExtrapolation() {
        return smoothExtrapolation;
    }

    // -----------------------------
    // -------- Math config --------
    // -----------------------------

    /**
     * Sets a mathematical function
     * 
     * @param function the mathematical Function
     * @see IChartViewer#MATH_NONE
     * @see IChartViewer#MATH_DERIVATIVE
     * @see IChartViewer#MATH_INTEGRAL
     * @see IChartViewer#MATH_FFT_MODULUS
     * @see IChartViewer#MATH_FFT_PHASE
     */
    public void setMathFunction(int function) {
        mathFunction = function;
        updateFilters();
        if (function == IChartViewer.MATH_NONE) {
            computeDataBounds();
        }
    }

    /**
     * Returns the current math function.
     * 
     * @see #setMathFunction
     */
    public int getMathFunction() {
        return mathFunction;
    }

    // -----------------------------
    // -------- BAR config ---------
    // -----------------------------

    /**
     * Sets the width of the bar in pixel (Bar graph mode). Pass 0 to have bar auto scaling.
     * 
     * @param w Bar width (pixel)
     * @see #getBarWidth
     */
    public void setBarWidth(int w) {
        barWidth = w;
    }

    /**
     * Gets the bar width.
     * 
     * @return Bar width (pixel)
     * @see #setBarWidth
     */
    public int getBarWidth() {
        return barWidth;
    }

    // -----------------------------
    // ------- Marker config -------
    // -----------------------------

    /**
     * Sets the marker color.
     * 
     * @param c Marker color
     * @see #getMarkerColor
     */
    public void setMarkerColor(Color c) {
        markerColor = c;
    }

    /**
     * Gets the marker color.
     * 
     * @return Marker color
     * @see #setMarkerColor
     */
    public Color getMarkerColor() {
        return markerColor;
    }

    /**
     * Display the label of this view when true. This has effects only if the parent chart has visible labels.
     * 
     * @param b visible state
     */
    public void setLabelVisible(boolean b) {
        labelVisible = b;
    }

    /**
     * Returns true when the label is visible.
     * 
     * @see #setLabelVisible
     */
    public boolean isLabelVisible() {
        return labelVisible;
    }

    public Color getLabelColor() {
        return labelColor;
    }

    public void setLabelColor(Color labelColor) {
        this.labelColor = labelColor;
    }

    /**
     * Returns whether sampling can be used to draw this {@link AbstractDataView}
     * 
     * @return A <code>boolean</code>
     */
    public boolean isSamplingAllowed() {
        return samplingAllowed;
    }

    /**
     * Sets whether sampling can be used to draw this {@link AbstractDataView}
     * 
     * @param samplingAllowed Whether sampling can be used to draw this {@link AbstractDataView}
     */
    public void setSamplingAllowed(boolean samplingAllowed) {
        this.samplingAllowed = samplingAllowed;
    }

    // -----------------------------
    // -------- Error config -------
    // -----------------------------

    /**
     * Returns the error color
     * 
     * @return Error color
     */
    public Color getErrorColor() {
        return errorColor;
    }

    /**
     * Sets the error color
     * 
     * @param errorColor
     *            The error color to set
     */
    public void setErrorColor(Color errorColor) {
        this.errorColor = errorColor;
    }

    /**
     * @return the errorVisible
     */
    public boolean isErrorVisible() {
        return errorVisible;
    }

    /**
     * @param errorVisible the errorVisible to set
     */
    public void setErrorVisible(boolean errorVisible) {
        this.errorVisible = errorVisible;
    }

    /**
     * Gets the marker size.
     * 
     * @return Marker size (pixel)
     * @see #setMarkerSize
     */
    public int getMarkerSize() {
        return markerSize;
    }

    /**
     * Sets the marker size (pixel).
     * 
     * @param c Marker size (pixel)
     * @see #getMarkerSize
     */
    public void setMarkerSize(int c) {
        markerSize = c;
    }

    /**
     * Returns the marker size adapted with highlighting
     * 
     * @return the marker size adapted with highlighting
     */
    public int getAdaptedMarkerSize() {
        if (highlighted) {
            double temp = markerSize;
            switch (highlightMethod) {
                case IChartViewer.HIGHLIGHT_ADD:
                    temp = markerSize + highlightCoefficient;
                    break;
                case IChartViewer.HIGHLIGHT_MULTIPLY:
                    temp = markerSize * highlightCoefficient;
                    break;
            }
            if (temp < 0) {
                temp = 0;
            } else if (temp > Integer.MAX_VALUE) {
                temp = Integer.MAX_VALUE;
            }
            return (int) Math.round(temp);
        } else {
            return markerSize;
        }
    }

    /**
     * Gets the view name. Used as key for the DataView.
     * 
     * @return Dataview name
     */
    public String getId() {
        return id;
    }

    /**
     * Gets the view displayName. Used as graphical name.
     * 
     * @return Dataview displayName
     * @see #setDisplayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the view displayName. Used as graphical name.
     * 
     * @return Dataview name
     * @see #getDisplayName
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @param fixed
     *            the fixed to set
     */
    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }

    /**
     * @return the fixed
     */
    public boolean isFixed() {
        return fixed;
    }

    /**
     * Set the dataView unit. (Used only for display)
     * 
     * @param s Dataview unit.
     * @see #getUnit
     */
    public void setUnit(String s) {
        unit = s == null ? ObjectUtils.EMPTY_STRING : s;
    }

    /**
     * Gets the dataView unit.
     * 
     * @return Dataview unit
     * @see #setUnit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Gets the extended name. (including transform description when used)
     * 
     * @return Extended name of this view.
     */
    public String getExtendedName() {
        return getExtendedName(null);
    }

    /**
     * Gets the extended name. (including transform description when used)
     * 
     * @return Extended name of this view.
     */
    public String getExtendedName(Object parent) {
        // Use String.valueOf to avoid NullPointerException
        StringBuilder result = new StringBuilder(String.valueOf(displayName));
        String t = ObjectUtils.EMPTY_STRING;
        if (hasTransform()) {
            result.append(EXT_NAME_OPENER);
            if (a0 != 0.0) {
                result.append(a0);
                t = PLUS;
            }
            if (a1 != 0.0) {
                result.append(t).append(a1).append(TIMES_Y);
                t = PLUS;
            }
            if (a2 != 0.0) {
                result.append(t).append(a2).append(TIMES_Y_SQUARE);
                t = PLUS;
            }
            result.append(EXT_NAME_CLOSER);
        }
        if (hasMathFunction()) {
            result.append(EXT_NAME_OPENER);
            if (mathFunction == IChartViewer.MATH_DERIVATIVE) {
                result.append(DataViewOption.DERIVATIVE);
            }
            if (mathFunction == IChartViewer.MATH_INTEGRAL) {
                result.append(DataViewOption.INTEGRAL);
            }
            if (mathFunction == IChartViewer.MATH_FFT_MODULUS) {
                result.append(DataViewOption.FFT_MODULUS);
            }
            if (mathFunction == IChartViewer.MATH_FFT_PHASE) {
                result.append(DataViewOption.FFT_PHASE);
            }
            result.append(EXT_NAME_CLOSER);
        }
        if (hasXTransform()) {
            result.append(VS_X);
            double absOffset = Math.abs(a0X);
            if (a0X < 0) {
                result.append(MINUS);
            } else {
                result.append(PLUS);
            }
            if ((parent != null) && isXTimeScale(parent)) {
                DateUtil.elapsedTimeToStringBuilder(result, (long) absOffset);
            } else {
                result.append(absOffset);
            }
            result.append(EXT_NAME_CLOSER);
        }
        return result.toString();
    }

    /**
     * Gets the marker type.
     * 
     * @return Marker type
     * @see #setMarker
     */
    public int getMarker() {
        return markerType;
    }

    /**
     * Sets the marker type.
     * 
     * @param m Marker type
     * @see #MARKER_NONE
     * @see #MARKER_DOT
     * @see #MARKER_BOX
     * @see #MARKER_TRIANGLE
     * @see #MARKER_DIAMOND
     * @see #MARKER_STAR
     * @see #MARKER_VERT_LINE
     * @see #MARKER_HORIZ_LINE
     * @see #MARKER_CROSS
     * @see #MARKER_CIRCLE
     * @see #MARKER_SQUARE
     */
    public void setMarker(int m) {
        markerType = m;
    }

    /**
     * Gets the A0 transformation coefficient.
     * 
     * @return A0 value
     * @see #setA0
     */
    public double getA0() {
        return a0;
    }

    /**
     * Gets the A1 transformation coefficient.
     * 
     * @return A1 value
     * @see #setA1
     */
    public double getA1() {
        return a1;
    }

    /**
     * Gets the A2 transformation coefficient.
     * 
     * @return A2 value
     * @see #setA2
     */
    public double getA2() {
        return a2;
    }

    /**
     * Set A0 transformation coefficient. The transformation computes new value = A0 + A1*v + A2*v*v. Transformation is
     * disabled when A0=A2=0 and A1=1.
     * 
     * @param d A0 value
     */
    public void setA0(double d) {
        a0 = d;
    }

    /**
     * Set A1 transformation coefficient. The transformation computes new value = A0 + A1*v + A2*v*v. Transformation is
     * disabled when A0=A2=0 and A1=1.
     * 
     * @param d A1 value
     */
    public void setA1(double d) {
        a1 = d;
    }

    /**
     * Set A2 transformation coefficient. The transformation computes new value = A0 + A1*v + A2*v*v. Transformation is
     * disabled when A0=A2=0 and A1=1.
     * 
     * @param d A2 value
     */
    public void setA2(double d) {
        a2 = d;
    }

    public double getA0X() {
        return a0X;
    }

    public void setA0X(double a0x) {
        a0X = a0x;
    }

    /**
     * Determines whether this view has a transformation.
     * 
     * @return false when A0=A2=0 and A1=1, true otherwise
     */
    public boolean hasTransform() {
        return !(a0 == 0 && a1 == 1 && a2 == 0);
    }

    /**
     * Determines whether this view has an X transformation.
     * 
     * @return false when A0X=0, true otherwise
     */
    public boolean hasXTransform() {
        return a0X != 0;
    }

    public boolean hasMathFunction() {
        boolean result = false;
        int function = mathFunction;
        if (function != IChartViewer.MATH_NONE) {
            result = true;
        }
        return result;
    }

    /**
     * Clears the filters applied to this view, without updating data <i>(without calling {@link #updateFilters()})</i>.
     * <p>
     * <big><b><u>Expert usage only!</u></b></big>
     * </p>
     */
    protected void clearFilter() {
        interpMethod = IChartViewer.INTERPOLATE_NONE;
        smoothMethod = IChartViewer.SMOOTH_NONE;
        mathFunction = IChartViewer.MATH_NONE;
    }

    /**
     * Determines whether this view is affected by a transform.
     * 
     * @return false when no filtering true otherwise.
     * @see #setInterpolationMethod
     * @see #setSmoothingMethod
     * @see #setMathFunction
     */
    public boolean hasFilter() {
        return (interpMethod != IChartViewer.INTERPOLATE_NONE) || (smoothMethod != IChartViewer.SMOOTH_NONE)
                || (mathFunction != IChartViewer.MATH_NONE);
    }

    /**
     * Expert usage. Gets the minimum (Y axis).
     * 
     * @return Minimum value
     */
    public double getMinimum() {
        return min;
    }

    /**
     * Expert usage. Gets the maximum (Y axis).
     * 
     * @return Maximum value
     */
    public double getMaximum() {
        return max;
    }

    /**
     * Expert usage. Gets the total on Y axis.
     * 
     * @return Total value
     */
    public double getTotalY() {
        return totalY;
    }

    /**
     * Expert usage. Gets the minimum on X axis (with TIME_ANNO).
     * 
     * @return Minimum time
     */
    public double getMinTime() {
        if (hasFilter()) {
            return minXValue;
        } else {
            DataList e = theData;
            if (e != null) {
                return e.getX();
            } else {
                return Double.MAX_VALUE;
            }
        }
    }

    /**
     * Expert usage. Gets the minimum on X axis.
     * 
     * @return Minimum x value
     */
    public double getMinXValue() {
        if (isXDataSorted()) {
            return getMinTime();
        }
        return minXValue;
    }

    /**
     * Expert usage. Get the positive minimum on X axis.
     * 
     * @return Minimum value strictly positive
     */
    public double getPositiveMinXValue() {
        double mi = Double.MAX_VALUE;
        DataList e = theData;
        if (hasFilter()) {
            e = theFilteredData;
        }
        while (e != null) {
            if (e.getX() > 0 && e.getX() < mi) {
                mi = e.getX();
            }
            e = e.next;
        }
        return mi;
    }

//    /**
//     * Expert usage. Get the positive minimum on X axis.
//     * 
//     * @return Minimum value strictly positive
//     */
//    public double getPositiveMinTime() {
//        DataList e = theData;
//        if (hasFilter()) {
//            e = theFilteredData;
//        }
//        boolean found = false;
//        while (e != null && !found) {
//            found = (e.getX() > 0);
//            if (!found) {
//                e = e.next;
//            }
//        }
//
//        if (e != null) {
//            return e.getX();
//        } else {
//            return Double.MAX_VALUE;
//        }
//    }

    /**
     * Expert usage. Get the maximum on X axis
     * 
     * @return Maximum x value
     */
    public double getMaxXValue() {
        if (isXDataSorted()) {
            return getMaxTime();
        }
        return maxXValue;
    }

    /**
     * Expert usage. Get the maximum on X axis (with TIME_ANNO)
     * 
     * @return Maximum value
     */
    public double getMaxTime() {
        if (hasFilter()) {
            return maxXValue;
        } else {
            DataList end = theDataEnd;
            if (end != null) {
                return end.getX();
            } else {
                return -Double.MAX_VALUE;
            }
        }
    }

    /**
     * Gets the number of data in this view.
     * 
     * @return Data length
     */
    public int getDataLength() {
        if (hasFilter()) {
            return filteredDataLength;
        } else {
            return dataLength;
        }
    }

    /**
     * Return a handle on the data. If you modify data, call commitChange() after the update. Expert usage.
     * 
     * @return A handle to the data.
     * @see #commitChange()
     */
    public DataList getData() {
        if (hasFilter()) {
            return theFilteredData;
        } else {
            return theData;
        }
    }

    /**
     * Commit change when some data has been in modified in the DataList (via getData())
     * 
     * @see #getData()
     */
    public void commitChange() {
        if (hasFilter()) {
            updateFilters();
        } else {
            computeDataBounds();
        }
    }

    /**
     * Add datum to the dataview. If you call this routine directly the graph will be updated only after a repaint and
     * your data won't be garbaged (if a display duration is specified). You should use ICharViewer.addData instead.
     * 
     * @param x
     *            x coordinates (real space)
     * @param y
     *            y coordinates (real space)
     */
    public void add(double x, double y) {
        add(x, y, 0, true);
    }

    /**
     * Add datum to the dataview. If you call this routine directly the graph will be updated only after a repaint and
     * your data won't be garbaged (if a display duration is specified). You should use ICharViewer.addData instead.
     * 
     * @param x x coordinates (real space)
     * @param y y coordinates (real space)
     * @param error error value (real space coordinates)
     */
    public void add(double x, double y, double error) {
        add(x, y, error, true);
    }

    /**
     * Add datum to the dataview. If you call this routine directly the graph will be updated only after a repaint and
     * your data won't be garbaged (if a display duration is specified). You should use ICharViewer.addData instead.
     * 
     * @param x x coordinates (real space)
     * @param y y coordinates (real space)
     * @param updateFilter update filter flag.
     */
    public void add(double x, double y, boolean updateFilter) {
        add(x, y, 0, updateFilter);
    }

    /**
     * Add data to the dataview. If you call this routine directly the graph will be updated only after a repaint and
     * your data won't be garbaged (if a display duration is specified). You should use ICharViewer.addData instead.
     * 
     * @param x x coordinates (real space)
     * @param y y coordinates (real space)
     * @param error error value (real space coordinates)
     * @param updateFilter update filter flag.
     */
    public synchronized void add(double x, double y, double error, boolean updateFilter) {
        addNoLock(x, y, error, updateFilter);
    }

    protected void addNoLock(double x, double y, double error, boolean updateFilter) {
        // Convert infinite value to NaN
        if (Double.isInfinite(y)) {
            if (y > 0) {
                y = FormatNAN.NAN_FOR_POSITIVE_INFINITY.getValue();
            } else {
                y = FormatNAN.NAN_FOR_NEGATIVE_INFINITY.getValue();
            }
        }
        DataList newData = new DataList(x, y, error);
        if (theData == null) {
            theData = newData;
            theDataEnd = theData;
        } else {
            theDataEnd.next = newData;
            theDataEnd = theDataEnd.next;
        }
        if (isValid(y)) {
            if (shouldComputeY) {
                shouldComputeY = false;
                min = Double.MAX_VALUE;
                max = -Double.MAX_VALUE;
            }
            if (y < min) {
                min = y;
            }
            if (y > max) {
                max = y;
            }
        }
        if (isValid(x)) {
            if (shouldComputeX) {
                shouldComputeX = false;
                minXValue = Double.MAX_VALUE;
                maxXValue = -Double.MAX_VALUE;
            }
            if (x < minXValue) {
                minXValue = x;
            }
            if (x > maxXValue) {
                maxXValue = x;
            }
        }
        dataLength++;
        if ((!Double.isNaN(y)) && (!Double.isInfinite(y))) {
            totalY += y;
        }
        if (updateFilter) {
            updateFilters();
        }
    }

    /**
     * Set data of this dataview.
     * 
     * @param x x values
     * @param y y values
     * @see #add
     */
    public void setData(double[] x, double[] y) {
        setData(x, y, null);
    }

    /**
     * Set data of this dataview.
     * 
     * @param x x values
     * @param y y values
     * @param error error value
     * @see #add
     */
    public synchronized void setData(double[] x, double[] y, double[] error) {
        if ((x == null) || (y == null)) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                    .info("Warning: Cannot set data, x array or y array is null");
        } else {
            reset();
            int dataLength = Math.min(x.length, y.length);
            int minLength;
            if (error == null) {
                minLength = 0;
            } else {
                minLength = Math.min(dataLength, error.length);
            }
            for (int i = 0; i < minLength; i++) {
                addNoLock(x[i], y[i], error[i], false);
            }
            for (int i = minLength; i < dataLength; i++) {
                addNoLock(x[i], y[i], 0, false);
            }
            updateFilters();
        }
    }

    public synchronized void setData(double[] xy) {
        if (xy == null) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).info("Warning: Cannot set data, xy array is null");
        } else if (xy.length % 2 != 0) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                    .info("Warning: Cannot set data, xy array length is not even");
        } else {
            reset();
            int length = xy.length / 2;
            for (int i = 0; i < length; i++) {
                addNoLock(xy[2 * i], xy[2 * i + 1], 0, false);
            }
            updateFilters();
        }
    }

    /**
     * Set data of this dataview and order value according to the x value
     * 
     * @param x x values
     * @param y y values
     * @see #add
     */
    public void setUnorderedData(double[] x, double[] y) {
        if ((x != null) && (y != null)) {
            double[] xOrd = x.clone();
            double[] yOrd = y.clone();
            mergeSort(xOrd, yOrd);
            setData(xOrd, yOrd);
        }
    }

    /**
     * Add datum to the dataview. If you call this routine directly the graph will be updated only after a repaint and
     * your data won't be garbaged (if a display duration is specified). You should use ICharViewer.addData instead.
     * 
     * @param p point (real space)
     */
    public void add(Point2D.Double p) {
        add(p.x, p.y);
    }

    /**
     * Garbage old data according to time.
     * 
     * @param garbageLimit Limit time (in millisec)
     * @return Number of deleted point.
     */
    public synchronized int garbagePointTime(double garbageLimit) {
        boolean need_to_recompute_max = false;
        boolean need_to_recompute_min = false;
        boolean need_to_recompute_max_x_value = false;
        boolean need_to_recompute_min_x_value = false;
        boolean found = false;
        int nbr = 0;
        DataList old;
        // Garbage old data
        if (theData != null) {
            while (theData != null && !found) {
                // Keep 3 seconds more to avoid complete curve
                found = (theData.getX() > (maxXValue - garbageLimit - 3000));
                if (!found) {
                    // Remove first element
                    double y = theData.getY();
                    need_to_recompute_max = need_to_recompute_max || (y == max);
                    need_to_recompute_min = need_to_recompute_min || (y == min);
                    need_to_recompute_max_x_value = need_to_recompute_max_x_value || (theData.getX() == maxXValue);
                    need_to_recompute_min_x_value = need_to_recompute_min_x_value || (theData.getX() == minXValue);
                    old = theData;
                    theData = theData.next;
                    old.next = null;
                    // To be sure that the JVM will clean the data list
                    old = null;
                    dataLength--;
                    if (isValid(y)) {
                        totalY -= y;
                    }
                    nbr++;
                }
            }
        }
        if (theData == null) {
            // All points has been removed
            reset();
        } else {
            if (hasFilter()) {
                updateFilters();
            } else {
                need_to_recompute_min_x_value = need_to_recompute_min_x_value && !isXDataSorted();
                need_to_recompute_max_x_value = need_to_recompute_max_x_value && !isXDataSorted();
                if (need_to_recompute_max) {
                    if (need_to_recompute_min || need_to_recompute_min_x_value || need_to_recompute_max_x_value) {
                        computeDataBounds();
                    } else {
                        computeMax();
                    }
                } else if (need_to_recompute_min) {
                    if (need_to_recompute_min_x_value || need_to_recompute_max_x_value) {
                        computeDataBounds();
                    } else {
                        computeMin();
                    }
                } else if (need_to_recompute_max_x_value) {
                    if (need_to_recompute_min_x_value) {
                        computeDataBounds();
                    } else {
                        computeMaxXValue();
                    }
                } else if (need_to_recompute_min_x_value) {
                    computeMinXValue();
                }
            }
        }
        return nbr;
    }

    /**
     * Garbage old data according to data length. It will remove the
     * (dataLength-garbageLimit) first points.
     * 
     * @param garbageLimit Index limit
     */
    public synchronized void garbagePointLimit(int garbageLimit) {
        boolean need_to_recompute_max = false;
        boolean need_to_recompute_min = false;
        boolean need_to_recompute_max_x_value = false;
        boolean need_to_recompute_min_x_value = false;
        DataList old;

        // Garbage old data
        int nb = dataLength - garbageLimit;
        for (int i = 0; i < nb; i++) {
            double y = theData.getY();
            need_to_recompute_max = need_to_recompute_max || (y == max);
            need_to_recompute_min = need_to_recompute_min || (y == min);
            need_to_recompute_max_x_value = need_to_recompute_max_x_value || (theData.getX() == maxXValue);
            need_to_recompute_min_x_value = need_to_recompute_min_x_value || (theData.getX() == minXValue);
            old = theData;
            theData = theData.next;
            old.next = null;
            dataLength--;
            if (isValid(y)) {
                totalY -= y;
            }
        }

        if (hasFilter()) {
            updateFilters();
        } else {
            need_to_recompute_min_x_value = need_to_recompute_min_x_value && !isXDataSorted();
            need_to_recompute_max_x_value = need_to_recompute_max_x_value && !isXDataSorted();

            if (need_to_recompute_max) {
                if (need_to_recompute_min || need_to_recompute_min_x_value || need_to_recompute_max_x_value) {
                    computeDataBounds();
                } else {
                    computeMax();
                }
            } else if (need_to_recompute_min) {
                if (need_to_recompute_min_x_value || need_to_recompute_max_x_value) {
                    computeDataBounds();
                } else {
                    computeMin();
                }
            } else if (need_to_recompute_max_x_value) {
                if (need_to_recompute_min_x_value) {
                    computeDataBounds();
                } else {
                    computeMaxXValue();
                }
            } else if (need_to_recompute_min_x_value) {
                computeMinXValue();
            }
        }
    }

    // Compute min
    protected void computeMin() {
        boolean done = false;
        min = Double.MAX_VALUE;
        DataList e = theData;
        if (hasFilter()) {
            e = theFilteredData;
        }
        while (e != null) {
            if (isValid(e.getY()) && e.getY() < min) {
                min = e.getY();
                done = true;
            }
            e = e.next;
        }
        if (!done) {
            max = 0;
        }
    }

    // Compute max
    protected void computeMax() {
        boolean done = false;
        max = -Double.MAX_VALUE;
        DataList e = theData;
        if (hasFilter()) {
            e = theFilteredData;
        }
        while (e != null) {
            if (isValid(e.getY()) && e.getY() > max) {
                max = e.getY();
                done = true;
            }
            e = e.next;
        }
        if (!done) {
            max = 0;
        }
    }

    // Compute minXValue
    protected void computeMinXValue() {
        boolean done = false;
        minXValue = Double.MAX_VALUE;
        DataList e = theData;
        if (hasFilter()) {
            e = theFilteredData;
        }
        while (e != null) {
            if (isValid(e.getX()) && e.getX() < minXValue) {
                minXValue = e.getX();
                done = true;
            }
            e = e.next;
        }
        if (!done) {
            minXValue = 0;
        }
    }

    // Compute maxXValue
    protected void computeMaxXValue() {
        boolean done = false;
        maxXValue = -Double.MAX_VALUE;
        DataList e = theData;
        if (hasFilter()) {
            e = theFilteredData;
        }
        while (e != null) {
            if (isValid(e.getX()) && e.getX() > maxXValue) {
                maxXValue = e.getX();
                done = true;
            }
            e = e.next;
        }
        if (!done) {
            maxXValue = 0;
        }
    }

    /**
     * Computes and stores min and max on x and y
     */
    public void computeDataBounds() {
        boolean doneX = false, doneY = false;
        minXValue = Double.MAX_VALUE;
        maxXValue = -Double.MAX_VALUE;
        min = Double.MAX_VALUE;
        max = -Double.MAX_VALUE;
        DataList e = theData;
        if (hasFilter()) {
            e = theFilteredData;
        }
        while (e != null) {
            if (isValid(e.getX())) {
                doneX = true;
                if (e.getX() < minXValue) {
                    minXValue = e.getX();
                }
                if (e.getX() > maxXValue) {
                    maxXValue = e.getX();
                }
            }
            if (isValid(e.getY())) {
                doneY = true;
                if (e.getY() < min) {
                    min = e.getY();
                }
                if (e.getY() > max) {
                    max = e.getY();
                }
            }
            e = e.next;
        }
        if (!doneX) {
            minXValue = 0;
            maxXValue = 0;
        }
        if (!doneY) {
            min = 0;
            max = 0;
        }
    }

    /**
     * Expert usage. Compute transformed min and max.
     * 
     * @param positive Whether to only use strictly positive values.
     * @return Transformed min and max
     */
    protected double[] computeTransformedMinMax(boolean positive, double a0, double a1, double a2) {
        double[] ret = new double[2];

        double mi = Double.MAX_VALUE;
        double ma = -Double.MAX_VALUE;

        DataList e = theData;
        if (hasFilter()) {
            e = theFilteredData;
        }

        while (e != null) {
            double v = a0 + a1 * e.getY() + a2 * e.getY() * e.getY();
            if (isValid(v, positive)) {
                if (v < mi) {
                    mi = v;
                }
                if (v > ma) {
                    ma = v;
                }
            }
            e = e.next;
        }

        if (mi == Double.MAX_VALUE) {
            mi = 0;
            if (positive) {
                mi = 1;
            }
        }
        if (ma == -Double.MAX_VALUE) {
            ma = 99;
        }

        ret[0] = mi;
        ret[1] = ma;

        return ret;
    }

    /**
     * Expert usage. Compute transformed min and max.
     * 
     * @return Transformed min and max
     */
    public double[] computeTransformedMinMax() {
        return computeTransformedMinMax(false, a0, a1, a2);
    }

    /**
     * Expert usage. Compute transformed strictly positive min and max.
     * 
     * @return Transformed min and max
     */
    public double[] computeTransformedPositiveMinMax() {
        return computeTransformedMinMax(true, a0, a1, a2);
    }

    /**
     * Expert usage. Compute strictly positive min and max.
     * 
     * @return min and max
     */
    public double[] computePositiveMinMax() {
        return computeTransformedMinMax(true, 0, 1, 0);
    }

    /**
     * Expert usage. Compute transformed x min and max.
     * 
     * @param positive Whether to only use strictly positive values.
     * @param offset The offset to use
     * @return Transformed x min and max
     */
    protected double[] computeTransformedMinMaxX(boolean positive, double offset) {
        double min = minXValue + offset, max = maxXValue + offset;
        if (positive && ((min <= 0) || (max <= 0))) {
            min = Double.MAX_VALUE;
            max = -Double.MAX_VALUE;

            DataList e = theData;
            if (hasFilter()) {
                e = theFilteredData;
            }

            while (e != null) {
                double v = e.getX() + offset;
                if (isValid(v, positive)) {
                    if (v < min) {
                        min = v;
                    }
                    if (v > max) {
                        max = v;
                    }
                }
                e = e.next;
            }
        }
        return new double[] { min, max };
    }

    /**
     * Expert usage. Compute transformed x min and max.
     * 
     * @return Transformed x min and max
     */
    public double[] computeTransformedMinMaxX() {
        return computeTransformedMinMaxX(false, a0X);
    }

    /**
     * Expert usage. Compute transformed strictly positive x min and max.
     * 
     * @return Transformed x min and max
     */
    public double[] computeTransformedPositiveMinMaxX() {
        return computeTransformedMinMaxX(true, a0X);
    }

    /**
     * Expert usage. Compute strictly positive x min and max.
     * 
     * @return Transformed x min and max
     */
    public double[] computePositiveMinMaxX() {
        return computeTransformedMinMaxX(true, 0);
    }

    /**
     * Compute minimum of positive value.
     * 
     * @return {@link Double#MAX_VALUE} when no positive value are found
     */
    public double computePositiveMin() {

        double mi = Double.MAX_VALUE;
        DataList e = theData;
        if (hasFilter()) {
            e = theFilteredData;
        }
        while (e != null) {
            if (isValid(e.getY()) && e.getY() > 0 && e.getY() < mi) {
                mi = e.getY();
            }
            e = e.next;
        }
        return mi;

    }

    protected boolean isValid(double value) {
        return isValid(value, false);
    }

    protected boolean isValid(double value, boolean positive) {
        boolean valid = ((!Double.isInfinite(value)) && (!Double.isNaN(value)));
        if (positive && valid) {
            valid = (value > 0);
        }
        return valid;
    }

    /**
     * Compute transformed value of v.
     * 
     * @param v Value to transform
     * @return transformed value (through A0,A1,A2 transformation)
     */
    public double getTransformedValue(double v) {
        return a0 + a1 * v + a2 * v * v;
    }

    /**
     * Compute offsetted x value of x.
     * 
     * @param x Value to offset
     * @return offsetted value (through A0X offset)
     */
    public double getOffsettedXValue(double x) {
        return x + a0X;
    }

    /**
     * Get last value.
     * 
     * @return Last value
     */
    public DataList getLastValue() {
        if (hasFilter()) {
            return theDataEnd;
        } else {
            return theFilteredDataEnd;
        }
    }

    /**
     * Clear all data in this view.
     */
    public synchronized void reset() {
        if (theData != null) {
            theData.reset();
        }
        theData = null;
        if (theDataEnd != null) {
            theDataEnd.reset();
        }
        theDataEnd = null;
        dataLength = 0;
        totalY = 0;
        updateFilters();
        computeDataBounds();
        shouldComputeX = true;
        shouldComputeY = true;
    }

    /**
     * Apply dataview configuration.
     * 
     * @param prefix settings prefix
     * @param f CfFileReader object which contains dataview parameters
     */
    public void applyConfiguration(String prefix, CfFileReader f) {
        List<String> p;
        // Dataview options
        p = f.getParam(prefix + DISPLAYNAME);
        if ((p != null) && (p.size() > 0)) {
            setDisplayName(p.get(0));
        }
        p = f.getParam(prefix + LINECOLOR);
        if (p != null) {
            setColor(OFormat.getColor(p));
        }
        p = f.getParam(prefix + LINEWIDTH);
        if (p != null) {
            setLineWidth(OFormat.getInt(p.get(0)));
        }
        p = f.getParam(prefix + LINESTYLE);
        if (p != null) {
            setStyle(OFormat.getInt(p.get(0)));
        }
        p = f.getParam(prefix + FILLCOLOR);
        if (p != null) {
            setFillColor(OFormat.getColor(p));
        }
        p = f.getParam(prefix + FILLMETHOD);
        if (p != null) {
            setFillMethod(OFormat.getInt(p.get(0)));
        }
        p = f.getParam(prefix + FILLSTYLE);
        if (p != null) {
            setFillStyle(OFormat.getInt(p.get(0)));
        }
        p = f.getParam(prefix + VIEWTYPE);
        if (p != null) {
            setViewType(OFormat.getInt(p.get(0)));
        }
        p = f.getParam(prefix + BARWIDTH);
        if (p != null) {
            setBarWidth(OFormat.getInt(p.get(0)));
        }
        p = f.getParam(prefix + MARKERCOLOR);
        if (p != null) {
            setMarkerColor(OFormat.getColor(p));
        }
        p = f.getParam(prefix + MARKERSIZE);
        if (p != null) {
            setMarkerSize(OFormat.getInt(p.get(0)));
        }
        p = f.getParam(prefix + MARKERSTYLE);
        if (p != null) {
            setMarker(OFormat.getInt(p.get(0)));
        }
        p = f.getParam(prefix + A0);
        if (p != null) {
            setA0(OFormat.getDouble(p.get(0)));
        }
        p = f.getParam(prefix + A1);
        if (p != null) {
            setA1(OFormat.getDouble(p.get(0)));
        }
        p = f.getParam(prefix + A2);
        if (p != null) {
            setA2(OFormat.getDouble(p.get(0)));
        }
        p = f.getParam(prefix + LABELVISIBLE);
        if (p != null) {
            setLabelVisible(OFormat.getBoolean(p.get(0)));
        }
        p = f.getParam(prefix + CLICKABLE);
        if (p != null) {
            setClickable(OFormat.getBoolean(p.get(0)));
        }
        p = f.getParam(prefix + LABEL_COLOR);
        if (p != null) {
            setLabelColor(OFormat.getColor(p));
        }
    }

    /**
     * Build a configuration string that can be write into a file and is compatible with CfFileReader.
     * 
     * @param prefix DataView prefix
     * @return A string containing param.
     * @see #applyConfiguration
     */
    public String getConfiguration(String prefix) {
        return appendConfiguration(null, prefix).toString();
    }

    /**
     * Appends to a {@link StringBuilder} a configuration string, that can be write into a file and is compatible with
     * CfFileReader
     * 
     * @param configurationBuilder The {@link StringBuilder}. If <code>null</code>, a new one will be created
     * @param prefix DataView prefix
     * @return A {@link StringBuilder} filled with the configuration
     */
    public StringBuilder appendConfiguration(StringBuilder configurationBuilder, String prefix) {
        StringBuilder builder = configurationBuilder;
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append(prefix).append(NAME).append(':').append(getId()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(DISPLAYNAME).append(':').append(getDisplayName()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(LINECOLOR).append(':').append(OFormat.color(getColor()))
                .append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(LINEWIDTH).append(':').append(getLineWidth()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(LINESTYLE).append(':').append(getStyle()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(FILLCOLOR).append(':').append(OFormat.color(getFillColor()))
                .append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(FILLMETHOD).append(':').append(getFillMethod()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(FILLSTYLE).append(':').append(getFillStyle()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(VIEWTYPE).append(':').append(getViewType()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(BARWIDTH).append(':').append(getBarWidth()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(MARKERCOLOR).append(':').append(OFormat.color(getMarkerColor()))
                .append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(MARKERSIZE).append(':').append(getMarkerSize()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(MARKERSTYLE).append(':').append(getMarker()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(A0).append(':').append(getA0()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(A1).append(':').append(getA1()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(A2).append(':').append(getA2()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(LABELVISIBLE).append(':').append(isLabelVisible()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(CLICKABLE).append(':').append(isClickable()).append(ObjectUtils.NEW_LINE);
        builder.append(prefix).append(LABEL_COLOR).append(':').append(OFormat.color(getLabelColor()))
                .append(ObjectUtils.NEW_LINE);
        return builder;
    }

    /**
     * Returns Y value according to index.
     * 
     * @param idx Value index
     * @return Y value
     */
    public double getYValueByIndex(int idx) {
        if (idx < 0 || idx >= getDataLength()) {
            return Double.NaN;
        }

        int i = 0;
        DataList e = theData;

        while (e != null && i < idx) {
            e = e.next;
            i++;
        }

        if (e != null) {
            return e.getY();
        } else {
            return Double.NaN;
        }

    }

    /**
     * Returns X value according to index.
     * 
     * @param idx Value index
     * @return X value
     */
    public double getXValueByIndex(int idx) {
        if (idx < 0 || idx >= getDataLength()) {
            return Double.NaN;
        }

        int i = 0;
        DataList e = theData;

        while (e != null && i < idx) {
            e = e.next;
            i++;
        }

        if (e != null) {
            return e.getX();
        } else {
            return Double.NaN;
        }

    }

    /**
     * Sets the format property for this dataview (C format). By default, it uses the Axis format.
     * 
     * @param format Format (C style)
     */
    public void setFormat(String format) {
        if ((format == null) || ((format.length() > 0) && SwingFormat.isNumberFormatOK(format))) {
            this.format = format;
        }
    }

    /**
     * Tests whether a given format is valid or not
     * 
     * @param format the format to tests
     * @return a boolean value : <code>true</code> if the format is valid, <code>false</code> otherwise.
     */
    public boolean isValidFormat(String format) {
        if (format.indexOf(PERCENT) == 0 && format.lastIndexOf(PERCENT) == format.indexOf(PERCENT)) {
            if (format.indexOf(ChartUtils.DOT) == -1) {
                // case %xd
                try {
                    int x = Integer.parseInt(format.substring(1, format.length() - 1));
                    if (x > 0) {
                        return true;
                    } else {
                        return false;
                    }
                } catch (Exception e) {
                    return false;
                }
            } else {
                if (format.indexOf(ChartUtils.DOT) == format.lastIndexOf(ChartUtils.DOT)) {
                    if (((format.toLowerCase().indexOf(F) == format.length() - 1)
                            && (format.toLowerCase().indexOf(F) > 0))
                            || ((format.toLowerCase().indexOf(E) == format.length() - 1)
                                    && (format.toLowerCase().indexOf(E) > 0))) {
                        // case %x.yf, %x.ye
                        try {
                            int x = Integer.parseInt(format.substring(1, format.indexOf(ChartUtils.DOT)));
                            int y = Integer.parseInt(
                                    format.substring(format.indexOf(ChartUtils.DOT) + 1, format.length() - 1));
                            if (x > y && x > 0 && y >= 0) {
                                return true;
                            } else {
                                return false;
                            }
                        } catch (Exception e) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    /**
     * Returns the current user format (null when none).
     */
    public String getFormat() {
        return format;
    }

    /**
     * Returns whether data is supposed to be sorted on x or not
     * 
     * @return a boolean value
     */
    public boolean isXDataSorted() {
        return xDataSorted;
    }

    /**
     * Set whether data is supposed to be sorted on x or not. <code>false</code> by default
     * 
     * @param dataSorted a boolean value
     */
    public void setXDataSorted(boolean dataSorted) {
        if (xDataSorted && !dataSorted) {
            computeDataBounds();
        }
        xDataSorted = dataSorted;
    }

    public synchronized double[] getSortedTimes() {
        double[] time = new double[dataLength];
        DataList currentData = theData;
        int i = 0;
        while (i < dataLength) {
            time[i++] = currentData.getX();
            currentData = currentData.next;
        }
        mergeSort(time, null);
        return time;
    }

    public synchronized double[] getSortedValues() {
        double[] value = new double[dataLength];
        DataList currentData = theData;
        int i = 0;
        while (i < dataLength) {
            value[i++] = currentData.getY();
            currentData = currentData.next;
        }
        mergeSort(value, null);
        return value;
    }

    public synchronized double[][] getDataAsDoubleMatrix() {
        return getDataAsDoubleMatrixNoLock();
    }

    protected final double[][] getDataAsDoubleMatrixNoLock() {
        double[][] result = new double[2][dataLength];
        double[] time = new double[dataLength];
        double[] value = new double[dataLength];
        DataList currentData = theData;
        for (int i = 0; i < dataLength; i++) {
            time[i] = currentData.getX();
            value[i] = currentData.getY();
            currentData = currentData.next;
        }
        result[IChartViewer.X_INDEX] = time;
        result[IChartViewer.Y_INDEX] = value;
        return result;
    }

    public synchronized double[][] getDataSortedByTimes() {
        double[][] result = getDataAsDoubleMatrixNoLock();
        mergeSort(result[IChartViewer.X_INDEX], result[IChartViewer.Y_INDEX]);
        return result;
    }

    public synchronized double[][] getDataSortedByValues() {
        double[][] result = getDataAsDoubleMatrixNoLock();
        mergeSort(result[IChartViewer.Y_INDEX], result[IChartViewer.X_INDEX]);
        return result;
    }

    /**
     * Applies merge sort on an array of double. If an associated array is given, its elements are moved the same way as
     * the array to sort.
     * 
     * @param array The array to sort
     * @param associated The associated array. Must be null or of the same length of <code>array</code>
     */
    public static void mergeSort(double[] array, double[] associated) {
        int length = array.length;
        if (length > 0) {
            mergeSort(array, associated, 0, length - 1);
        }
    }

    protected static void mergeSort(double[] array, double[] associated, int start, int end) {
        if (start != end) {
            int middle = (end + start) / 2;
            mergeSort(array, associated, start, middle);
            mergeSort(array, associated, middle + 1, end);
            merge(array, associated, start, middle, end);
        }
    }

    protected static void merge(double[] array, double[] associated, int start1, int end1, int end2) {
        int deb2 = end1 + 1;
        double[] tempArray = new double[end1 - start1 + 1];
        double[] tempAsso = (associated == null ? null : new double[end1 - start1 + 1]);
        for (int i = start1; i <= end1; i++) {
            tempArray[i - start1] = array[i];
            if (associated != null) {
                tempAsso[i - start1] = associated[i];
            }
        }
        int index1 = start1;
        int index2 = deb2;
        for (int i = start1; i <= end2; i++) {
            if (index1 == deb2) {
                break;
            } else if (index2 == (end2 + 1)) {
                array[i] = tempArray[index1 - start1];
                if (associated != null) {
                    associated[i] = tempAsso[index1 - start1];
                }
                index1++;
            } else if (tempArray[index1 - start1] < array[index2]) {
                array[i] = tempArray[index1 - start1];
                if (associated != null) {
                    associated[i] = tempAsso[index1 - start1];
                }
                index1++;
            } else {
                array[i] = array[index2];
                if (associated != null) {
                    associated[i] = associated[index2];
                }
                index2++;
            }
        }
    }

    // ----------------------------------------------------------------------------
    // Interpolation/Filtering stuff
    // ----------------------------------------------------------------------------

    /**
     * Returns an array of points.
     * 
     * @param nbExtra Number of extrapolated point
     * @param interpNan Interpolate NaN values when true, remove them otherwise
     */
    protected Point2D.Double[] getSource(DataList src, int nbExtra, boolean interpNan) {

        DataList f = src;

        // Interpolate NaN (When possible)
        List<Point2D.Double> pts = new ArrayList<Point2D.Double>();
        Point2D.Double lastValue = new Point2D.Double(Double.NaN, Double.NaN);
        Point2D.Double nextValue = new Point2D.Double(Double.NaN, Double.NaN);
        while (f != null) {
            if (!Double.isNaN(f.getY())) {
                pts.add(new Point.Double(f.getX(), f.getY()));
            } else {
                if (interpNan) {
                    if (!Double.isNaN(lastValue.y)) {
                        if (f.next != null && !Double.isNaN(f.next.getY())) {
                            // Linear interpolation possible
                            nextValue.x = f.next.getX();
                            nextValue.y = f.next.getY();
                            // Interpolate also x value, work around client side
                            // timestamps error.
                            pts.add(LinearInterpolate(lastValue, nextValue, 0.5));
                        } else {
                            // Duplicate last value
                            pts.add(new Point2D.Double(f.getX(), lastValue.y));
                        }
                    }
                }
            }
            lastValue.x = f.getX();
            lastValue.y = f.getY();
            f = f.next;
        }

        Point2D.Double[] ret = new Point2D.Double[pts.size() + 2 * nbExtra];
        for (int i = 0; i < pts.size(); i++) {
            ret[i + nbExtra] = pts.get(i);
        }

        return ret;

    }

    /* Mathematical operation (integral,derivative,fft) */
    protected void mathop() {
        if (mathFunction != IChartViewer.MATH_NONE) {

            // Get source data
            Point2D.Double[] source;
            if (interpMethod == IChartViewer.INTERPOLATE_NONE && smoothMethod == IChartViewer.SMOOTH_NONE) {
                source = getSource(theData, 0, false);
            } else {
                source = getSource(theFilteredData, 0, false);
            }

            // Reset filteredData
            theFilteredData = null;
            theFilteredDataEnd = null;
            filteredDataLength = 0;

            switch (mathFunction) {
                case IChartViewer.MATH_DERIVATIVE:
                    for (int i = 0; i < source.length - 1; i++) {
                        double d = (source[i + 1].y - source[i].y) / (source[i + 1].x - source[i].x);
                        addInt(source[i].x, d);
                    }
                    break;
                case IChartViewer.MATH_INTEGRAL:
                    double sum = 0.0;
                    if (source.length > 0) {
                        addInt(source[0].x, sum);
                        for (int i = 0; i < source.length - 1; i++) {
                            sum += ((source[i + 1].y + source[i].y) / 2.0) * (source[i + 1].x - source[i].x);
                            addInt(source[i + 1].x, sum);
                        }
                    }
                    break;
                case IChartViewer.MATH_FFT_MODULUS:
                    doFFT(source, 0);
                    break;
                case IChartViewer.MATH_FFT_PHASE:
                    doFFT(source, 1);
                    break;
            }
        }
    }

    /* Reverse bit of idx on p bits */
    protected int reverse(int idx, int p) {
        int i, rev;
        for (i = rev = 0; i < p; i++) {
            rev = (rev << 1) | (idx & 1);
            idx >>= 1;
        }
        return rev;
    }

    /**
     * Performs FFT of the input signal (Requires constant x intervals)
     * 
     * @param in Input signal
     * @param mode 0=>modulus 1=>argument
     */
    protected void doFFT(Point2D.Double[] in, int mode) {
        int nbSample = in.length;
        if (nbSample > 1) {
            int p = 0;
            int i, idx;
            // Get the power of 2 above
            if ((nbSample & (nbSample - 1)) == 0) {
                p = -1; // already a power of 2
            } else {
                p = 0;
            }
            while (nbSample != 0) {
                nbSample = nbSample >> 1;
                p++;
            }
            nbSample = 1 << p;

            // Create initial array
            double[] real = new double[nbSample];
            double[] imag = new double[nbSample];
            for (i = 0; i < in.length; i++) {
                idx = reverse(i, p);
                real[idx] = in[i].y;
                imag[idx] = 0.0;
            }
            for (; i < nbSample; i++) {
                idx = reverse(i, p);
                real[idx] = 0.0;
                imag[idx] = 0.0;
            }
            double fs = 1.0 / (in[1].x - in[0].x); // Sampling frequency

            // Do the FFT
            int blockEnd = 1;
            for (int blockSize = 2; blockSize <= nbSample; blockSize <<= 1) {

                double deltaAngle = 2.0 * Math.PI / blockSize;

                double sm2 = Math.sin(-2.0 * deltaAngle);
                double sm1 = Math.sin(-deltaAngle);
                double cm2 = Math.cos(-2.0 * deltaAngle);
                double cm1 = Math.cos(-deltaAngle);
                double w = 2.0 * cm1;
                double ar0, ar1, ar2, ai0, ai1, ai2;

                for (i = 0; i < nbSample; i += blockSize) {
                    ar2 = cm2;
                    ar1 = cm1;

                    ai2 = sm2;
                    ai1 = sm1;

                    for (int j = i, n = 0; n < blockEnd; j++, n++) {
                        ar0 = w * ar1 - ar2;
                        ar2 = ar1;
                        ar1 = ar0;

                        ai0 = w * ai1 - ai2;
                        ai2 = ai1;
                        ai1 = ai0;

                        int k = j + blockEnd;
                        double tr = ar0 * real[k] - ai0 * imag[k];
                        double ti = ar0 * imag[k] + ai0 * real[k];

                        real[k] = real[j] - tr;
                        imag[k] = imag[j] - ti;

                        real[j] += tr;
                        imag[j] += ti;
                    }
                }

                blockEnd = blockSize;
            }

            // Create modulus of arguments results
            double nS = nbSample;
            switch (mode) {
                case 0: // Modulus
                    for (i = 0; i < nbSample / 2; i++) {
                        double n = Math.sqrt(real[i] * real[i] + imag[i] * imag[i]);
                        addInt(i * fs / nS, n / nS);
                    }
                    break;
                case 1: // Arguments (in radians)
                    for (i = 0; i < nbSample; i++) {
                        double n = Math.sqrt(real[i] * real[i] + imag[i] * imag[i]);
                        double arg = Math.asin(imag[i] / n);
                        addInt(i * fs / nS, arg);
                    }
                    break;
            }
        }
    }

    /**
     * Update filter calculation. Call this function if you're using the add(x,y,false) method.
     */
    public void updateFilters() {
        theFilteredData = null;
        theFilteredDataEnd = null;
        filteredDataLength = 0;
        if (hasFilter()) {
            interpolate();
            convolution();
            mathop();
            computeDataBounds();
        }
    }

    /** Smoothing filter shapes */
    protected void updateSmoothCoefs() {
        if (smoothMethod == IChartViewer.SMOOTH_NONE) {
            smoothCoefs = null;
        } else {
            smoothCoefs = new double[smoothNeighbor];
            int nb = smoothNeighbor / 2;

            switch (smoothMethod) {

                case IChartViewer.SMOOTH_FLAT:
                    // Flat shape
                    for (int i = 0; i < smoothNeighbor; i++) {
                        smoothCoefs[i] = 1.0;
                    }
                    break;

                case IChartViewer.SMOOTH_TRIANGULAR:
                    // Triangular shape
                    double l = 1.0 / (nb + 1.0);
                    for (int i = 0; i < nb; i++) {
                        smoothCoefs[i] = (i - nb) * l + 1.0;
                    }
                    for (int i = 1; i <= nb; i++) {
                        smoothCoefs[i + nb] = (-i) * l + 1.0;
                    }
                    smoothCoefs[nb] = 1.0;
                    break;

                case IChartViewer.SMOOTH_GAUSSIAN:
                    // Gaussian shape
                    double A = 1.0 / (2.0 * Math.sqrt(Math.PI) * smoothSigma);
                    double B = 1.0 / (2.0 * smoothSigma * smoothSigma);
                    for (int i = 0; i < smoothNeighbor; i++) {
                        double x = (i - nb);
                        smoothCoefs[i] = A * Math.exp(-x * x * B);
                    }
                    break;

            }

            // Normalize coef
            double sum = 0.0;
            for (int i = 0; i < smoothNeighbor; i++) {
                sum += smoothCoefs[i];
            }
            for (int i = 0; i < smoothNeighbor; i++) {
                smoothCoefs[i] = smoothCoefs[i] / sum;
            }
        }
    }

    /** Convolution product */
    protected void convolution() {
        if (smoothMethod != IChartViewer.SMOOTH_NONE) {
            int nbG = smoothCoefs.length / 2;
            int nbF; // number of smoothed points
            int nbE; // number of exptrapoled points
            int start; // start index (input signal)
            int end; // end index (input signal)
            int i;
            double sum;

            // Number of added extrapolated points
            switch (smoothExtrapolation) {
                case IChartViewer.SMOOTH_EXT_NONE:
                    nbE = 0;
                    break;
                default:
                    nbE = nbG;
                    break;
            }

            // Get source data
            Point2D.Double[] source;
            if (interpMethod == IChartViewer.INTERPOLATE_NONE) {
                source = getSource(theData, nbE, true);
            } else {
                source = getSource(theFilteredData, nbE, true);
            }

            nbF = source.length - 2 * nbG; // number of smoothed points
            start = nbG; // start index
            end = nbF + nbG - 1; // end index

            // Too much neighbor or too much NaN
            if (nbF != nbE && start <= end) {
                // Extrapolation on boundaries
                // x values are there for testing purpose only.
                // They do not impact on calculation.
                switch (smoothExtrapolation) {

                    // Linearly extrpolate points based on the
                    // average direction of the 3 first (or last) vectors.
                    // TODO: Make this configurable
                    case IChartViewer.SMOOTH_EXT_LINEAR:

                        int maxPts = 3;
                        Point2D.Double vect = new Point2D.Double();

                        // Fisrt points
                        int nb = 0;
                        vect.x = 0.0; // Sum of vectors
                        vect.y = 0.0; // Sum of vectors
                        for (i = start; i < maxPts + start && i < nbF + start - 1; i++) {
                            vect.x += (source[i].x - source[i + 1].x);
                            vect.y += (source[i].y - source[i + 1].y);
                            nb++;
                        }
                        if (nb == 0) {
                            // Revert to flat
                            vect.x = source[start].x - nbE;
                            vect.y = source[start].y;
                        } else {
                            vect.x = (vect.x / nb) * nbE + source[start].x;
                            vect.y = (vect.y / nb) * nbE + source[start].y;
                        }
                        for (i = 0; i < nbE; i++) {
                            source[i] = LinearInterpolate(vect, source[start], (double) i / (double) nbE);
                        }

                        // Last points
                        nb = 0;
                        vect.x = 0.0; // Sum of vectors
                        vect.y = 0.0; // Sum of vectors
                        for (i = end - maxPts; i < end; i++) {
                            if (i >= start) {
                                vect.x += (source[i + 1].x - source[i].x);
                                vect.y += (source[i + 1].y - source[i].y);
                                nb++;
                            }
                        }
                        if (nb == 0) {
                            // Revert to flat
                            vect.x = source[end].x + nbE;
                            vect.y = source[end].y;
                        } else {
                            vect.x = (vect.x / nb) * nbE + source[end].x;
                            vect.y = (vect.y / nb) * nbE + source[end].y;
                        }
                        for (i = 1; i <= nbE; i++) {
                            source[end + i] = LinearInterpolate(source[end], vect, (double) i / (double) nbE);
                        }

                        break;

                    // Duplicate start and end values
                    case IChartViewer.SMOOTH_EXT_FLAT:
                        for (i = 0; i < nbE; i++) {
                            source[i] = new Point2D.Double(source[start].x - nbE + i, source[start].y);
                            source[i + end + 1] = new Point2D.Double(source[end].x + i + 1.0, source[end].y);
                        }
                        break;
                }

                // Reset filteredData
                theFilteredData = null;
                theFilteredDataEnd = null;
                filteredDataLength = 0;

                for (i = start; i <= end; i++) {
                    // Convolution product
                    sum = 0.0;
                    for (int j = 0; j < smoothCoefs.length; j++) {
                        sum += smoothCoefs[j] * source[i + j - nbG].y;
                    }
                    addInt(source[i].x, sum);
                }
            }

        }
    }

    protected void addInt(Point2D.Double p) {
        addInt(p.x, p.y);
    }

    protected void addInt(double x, double y) {

        DataList newData = new DataList(x, y);

        if (theFilteredData == null) {
            theFilteredData = newData;
        } else {
            theFilteredDataEnd.next = newData;
        }
        theFilteredDataEnd = newData;

        filteredDataLength++;

    }

    protected Point2D.Double[] getSegments(DataList l) {

        int nb = 0;
        DataList head = l;
        while (l != null && !Double.isNaN(l.getY())) {
            nb++;
            l = l.next;
        }
        l = head;
        Point2D.Double[] ret = new Point2D.Double[nb];
        for (int i = 0; i < nb; i++) {
            ret[i] = new Point2D.Double(l.getX(), l.getY());
            l = l.next;
        }
        return ret;

    }

    /**
     * Linear interpolation
     * 
     * @param p1 Start point (t=0)
     * @param p2 End point (t=1)
     * @param t 0=>p1 to 1=>p2
     */
    protected Point2D.Double LinearInterpolate(Point2D.Double p1, Point2D.Double p2, double t) {
        return new Point2D.Double(p1.x + (p2.x - p1.x) * t, p1.y + (p2.y - p1.y) * t);
    }

    /**
     * Cosine interpolation
     * 
     * @param p1 Start point (t=0)
     * @param p2 End point (t=1)
     * @param t 0=>p1 to 1=>p2
     */
    protected Point2D.Double CosineInterpolate(Point2D.Double p1, Point2D.Double p2, double t) {
        double t2;
        t2 = (1.0 - Math.cos(t * Math.PI)) / 2.0;
        return new Point2D.Double(p1.x + (p2.x - p1.x) * t, p1.y * (1.0 - t2) + p2.y * t2);
    }

    /**
     * Cubic interpolation (1D cubic interpolation, requires constant x intervals)
     * 
     * @param p0 neighbour point
     * @param p1 Start point (t=0)
     * @param p2 End point (t=1)
     * @param p3 neighbour point
     * @param t 0=>p1 to 1=>p2
     */
    protected Point2D.Double CubicInterpolate(Point2D.Double p0, Point2D.Double p1, Point2D.Double p2,
            Point2D.Double p3, double t) {
        double t2 = t * t;
        double t3 = t2 * t;

        double ay3 = p3.y - p2.y - p0.y + p1.y;
        double ay2 = p0.y - p1.y - ay3;
        double ay1 = p2.y - p0.y;
        double ay0 = p1.y;

        return new Point2D.Double(p1.x + (p2.x - p1.x) * t, ay3 * t3 + ay2 * t2 + ay1 * t + ay0);
    }

    /**
     * Hermite interpolation.
     * 
     * @param p0 neighbour point
     * @param p1 Start point (t=0)
     * @param p2 End point (t=1)
     * @param p3 neighbour point
     * @param mu 0=>p1 to 1=>p2
     * @param tension (1=>high, 0=>normal, -1=>low)
     * @param bias (0 for no bias, positive value towards first segment, negative value towards the others)
     */
    protected Point2D.Double HermiteInterpolate(Point2D.Double p0, Point2D.Double p1, Point2D.Double p2,
            Point2D.Double p3, double mu, double tension, double bias) {
        double ym0, ym1, xm0, xm1, mu2, mu3;
        double a0, a1, a2, a3;
        double t = (1.0 - tension) / 2.0;
        mu2 = mu * mu;
        mu3 = mu2 * mu;

        a0 = 2.0 * mu3 - 3.0 * mu2 + 1.0;
        a1 = mu3 - 2.0 * mu2 + mu;
        a2 = mu3 - mu2;
        a3 = -2.0 * mu3 + 3.0 * mu2;

        xm0 = (p1.x - p0.x) * (1.0 + bias) * t;
        xm0 += (p2.x - p1.x) * (1.0 - bias) * t;
        xm1 = (p2.x - p1.x) * (1.0 + bias) * t;
        xm1 += (p3.x - p2.x) * (1.0 - bias) * t;

        ym0 = (p1.y - p0.y) * (1.0 + bias) * t;
        ym0 += (p2.y - p1.y) * (1.0 - bias) * t;
        ym1 = (p2.y - p1.y) * (1.0 + bias) * t;
        ym1 += (p3.y - p2.y) * (1.0 - bias) * t;

        return new Point2D.Double(a0 * p1.x + a1 * xm0 + a2 * xm1 + a3 * p2.x,
                a0 * p1.y + a1 * ym0 + a2 * ym1 + a3 * p2.y);

    }

    /**
     * Interpolate the Dataview
     */
    protected void interpolate() {
        if (interpMethod != IChartViewer.INTERPOLATE_NONE) {
            double dt = 1.0 / interpStep;
            double t;
            int i;
            Point2D.Double fP;
            Point2D.Double lP;

            DataList l = theData;

            while (l != null) {

                // Get continuous segments
                Point2D.Double[] pts = getSegments(l);
                int nbPts = pts.length;
                int method = interpMethod;

                if (nbPts == 0) {
                    // NaN found
                    method = IChartViewer.INTERPOLATE_NONE; // No interpolation
                                                            // possible
                    addInt(new Point.Double(l.getX(), l.getY()));
                    l = l.next;
                } else if (nbPts == 1) {
                    method = IChartViewer.INTERPOLATE_NONE; // No interpolation
                                                            // possible
                    addInt(pts[0]);
                } else if (nbPts == 2) {
                    method = IChartViewer.INTERPOLATE_LINEAR; // Fallback to
                                                              // linear when
                                                              // less
                    // than 3 points
                }

                switch (method) {

                    // ---------------------------------------------------------------------
                    case IChartViewer.INTERPOLATE_LINEAR:

                        for (i = 0; i < nbPts - 2; i++) {
                            for (int j = 0; j < interpStep; j++) {
                                t = j * dt;
                                addInt(LinearInterpolate(pts[i], pts[i + 1], t));
                            }
                        }
                        for (int j = 0; j <= interpStep; j++) {
                            t = j * dt;
                            addInt(LinearInterpolate(pts[i], pts[i + 1], t));
                        }
                        break;

                    // ---------------------------------------------------------------------
                    case IChartViewer.INTERPOLATE_COSINE:

                        for (i = 0; i < nbPts - 2; i++) {
                            for (int j = 0; j < interpStep; j++) {
                                t = j * dt;
                                addInt(CosineInterpolate(pts[i], pts[i + 1], t));
                            }
                        }
                        for (int j = 0; j <= interpStep; j++) {
                            t = j * dt;
                            addInt(LinearInterpolate(pts[i], pts[i + 1], t));
                        }
                        break;

                    // ---------------------------------------------------------------------
                    case IChartViewer.INTERPOLATE_CUBIC:

                        // First segment (extend tangent)
                        fP = new Point.Double(2.0 * pts[0].x - pts[1].x, 2.0 * pts[0].y - pts[1].y);
                        for (int j = 0; j < interpStep; j++) {
                            t = j * dt;
                            addInt(CubicInterpolate(fP, pts[0], pts[1], pts[2], t));
                        }

                        // Middle segments
                        for (i = 1; i < nbPts - 2; i++) {
                            for (int j = 0; j < interpStep; j++) {
                                t = j * dt;
                                addInt(CubicInterpolate(pts[i - 1], pts[i], pts[i + 1], pts[i + 2], t));
                            }
                        }

                        // Last segment (extend tangent)
                        lP = new Point.Double(2.0 * pts[i + 1].x - pts[i].x, 2.0 * pts[i + 1].y - pts[i].y);
                        for (int j = 0; j <= interpStep; j++) {
                            t = j * dt;
                            addInt(CubicInterpolate(pts[i - 1], pts[i], pts[i + 1], lP, t));
                        }
                        break;

                    // ---------------------------------------------------------------------
                    case IChartViewer.INTERPOLATE_HERMITE:

                        // First segment (extend tangent)
                        fP = new Point.Double(2.0 * pts[0].x - pts[1].x, 2.0 * pts[0].y - pts[1].y);
                        for (int j = 0; j < interpStep; j++) {
                            t = j * dt;
                            addInt(HermiteInterpolate(fP, pts[0], pts[1], pts[2], t, interpTension, interpBias));
                        }

                        // Middle segments
                        for (i = 1; i < nbPts - 2; i++) {
                            for (int j = 0; j < interpStep; j++) {
                                t = j * dt;
                                addInt(HermiteInterpolate(pts[i - 1], pts[i], pts[i + 1], pts[i + 2], t, interpTension,
                                        interpBias));
                            }
                        }

                        // Last segment (extend tangent)
                        lP = new Point.Double(2.0 * pts[i + 1].x - pts[i].x, 2.0 * pts[i + 1].y - pts[i].y);
                        for (int j = 0; j <= interpStep; j++) {
                            t = j * dt;
                            addInt(HermiteInterpolate(pts[i - 1], pts[i], pts[i + 1], lP, t, interpTension,
                                    interpBias));
                        }
                        break;

                }

                // Fetch
                for (i = 0; i < nbPts; i++) {
                    l = l.next;
                }

            }
        }
    }

    public boolean isHighlighted() {
        return highlighted;
    }

    public void setHighlighted(boolean highlighted) {
        this.highlighted = highlighted;
    }

    public double getHighlightCoefficient() {
        return highlightCoefficient;
    }

    public void setHighlightCoefficient(double highlightFactor) {
        highlightCoefficient = highlightFactor;
    }

    public int getHighlightMethod() {
        return highlightMethod;
    }

    public void setHighlightMethod(int highlightMethod) {
        this.highlightMethod = highlightMethod;
    }

    /**
     * Format the given value according the userFormat or to the Axis format.
     * 
     * @param value Value to be formated
     * @param parent The parent that may know how to recover axis
     * @return A {@link String}, that represents the formatted value
     */
    public String formatValue(double value, Object parent) {
        String formattedValue;
        if (Double.isNaN(value)) {
            formattedValue = ChartUtils.formatNaNValue(value, null);
        } else if (format == null) {
            if (getAxis() == null) {
                formattedValue = Double.toString(value);
            } else {
                formattedValue = formatValueFromParentAxis(value, 0, parent);
            }
        } else {
            formattedValue = SwingFormat.formatValue(value, format, true);
        }
        return formattedValue;
    }

    /**
     * Formats the given value according to axis format and precision
     * 
     * @param value Value to be formated
     * @param precision The precision to use
     * @param parent The parent that may know how to recover axis
     * @return A {@link String}, that represents the formatted value
     */
    protected abstract String formatValueFromParentAxis(double value, int precision, Object parent);

    protected abstract boolean isXTimeScale(Object parent);

    @Override
    public String toString() {
        return getDisplayName();
    }

    @Override
    protected void finalize() {
        clearFilter();
        lineColor = null;
        fillColor = null;
        markerColor = null;
        errorColor = null;
        labelColor = null;
        theData = null;
        theFilteredData = null;
        theDataEnd = null;
        theFilteredDataEnd = null;
        id = null;
        displayName = null;
        unit = null;
        format = null;
        smoothCoefs = null;
    }

}
