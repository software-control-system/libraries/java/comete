/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.jdesktop.swingx.JXTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.listener.ITableListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.ITable;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.DefaultTableModelWithRowName;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.comete.swing.util.mask.MaskedTable;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.IDataComponent;
import fr.soleil.lib.project.awt.listener.DataComponentDelegate;
import fr.soleil.lib.project.date.IDateConstants;

public abstract class AbstractTable<T> extends JScrollPane
        implements ITable<T>, IDataComponent, MouseListener, CometeConstants {

    private static final long serialVersionUID = 4733725151540438338L;

    protected static final Logger LOGGER = LoggerFactory.getLogger(Mediator.LOGGER_ACCESS);
    protected static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter
            .ofPattern(IDateConstants.DATE_TIME_SHORT_FORMAT); // yyyy/MM/dd

    protected T matrix;
    protected T originalMatrix;
    protected T loadMatrix;
    protected final Collection<ITableListener<T>> listeners;
    protected final MaskedTable table;
    protected DefaultTableModelWithRowName tableModel;
    protected final JCheckBox loadDataVisibleCheckBox;
    protected boolean loadDataVisible;
    protected boolean saveColumnIndex;
    protected String valueSeparator;
    protected String[] customRowNames;
    protected String[] customColumnNames;
    protected boolean packAllOnInit;
    protected String format;
    protected boolean editable;
    protected int selectedColumn;
    protected int selectedRow;
    protected final double[] selectedPoint;
    private final TargetDelegate delegate;
    protected final DataComponentDelegate dataComponentDelegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;
    protected boolean editing;
    protected final boolean initialized;
    protected volatile boolean structureChanged, updateLoadCheckBox;

    public AbstractTable() {
        super();
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        editing = false;
        selectedColumn = -1;
        selectedRow = -1;

        selectedPoint = new double[2];
        selectedPoint[IChartViewer.X_INDEX] = selectedColumn;
        selectedPoint[IChartViewer.Y_INDEX] = selectedRow;

        editable = false;
        format = null;
        packAllOnInit = false;
        loadDataVisible = false;
        loadDataVisibleCheckBox = new JCheckBox("Load data visible");
        saveColumnIndex = false;
        valueSeparator = "\t";
        listeners = new ArrayList<>();
        originalMatrix = null;
        loadMatrix = null;
        matrix = null;
        table = generateMaskedTable();
        customRowNames = null;
        customColumnNames = null;
        tableModel = generateDefaultModel();
        table.setModel(tableModel);
        table.addMouseListener(this);
        table.setColumnControl(initColumnControl());
        table.setColumnControlVisible(true);
        table.setAutoResizeMode(JXTable.AUTO_RESIZE_OFF);
        table.setColumnSelectionAllowed(true);
        table.setCellSelectionEnabled(true);
        table.setSortable(false);
        table.setOpaque(false);
        setViewportView(table);
        getViewport().setBackground(getBackground());
        updateUI();
        dataComponentDelegate = new DataComponentDelegate();
        dataComponentDelegate.setupAndListenToComponent(this);
        dataComponentDelegate.setUpdateOnDataChanged(this, true);
        structureChanged = false;
        initialized = true;
    }

    protected MaskedTable generateMaskedTable() {
        // color hack needed for JAVAAPI-79
        return new MaskedTable() {

            private static final long serialVersionUID = -4064177300633445024L;

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component comp = super.prepareRenderer(renderer, row, column);
                if (comp instanceof JComponent) {
                    ((JComponent) comp).setOpaque(AbstractTable.this.isOpaque());
                }
                return comp;
            }
        };
    }

    public void setFullBackground(Color bg) {
        setBackground(bg);
        JViewport vp = getViewport();
        if (vp != null) {
            vp.setBackground(bg);
        }
        vp = getColumnHeader();
        if (vp != null) {
            vp.setBackground(bg);
        }
        vp = getRowHeader();
        if (vp != null) {
            vp.setBackground(bg);
        }
        JScrollBar scrollBar = getHorizontalScrollBar();
        if (scrollBar != null) {
            scrollBar.setBackground(bg);
        }
        scrollBar = getVerticalScrollBar();
        if (scrollBar != null) {
            scrollBar.setBackground(bg);
        }
        if (table != null) {
            table.setBackground(bg);
        }
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        super.setOpaque(isOpaque);
        if (initialized) {
            JViewport vp = getViewport();
            if (vp != null) {
                vp.setOpaque(isOpaque);
            }
            vp = getColumnHeader();
            if (vp != null) {
                vp.setOpaque(isOpaque);
            }
            vp = getRowHeader();
            if (vp != null) {
                vp.setOpaque(isOpaque);
            }
            JScrollBar scrollBar = getHorizontalScrollBar();
            if (scrollBar != null) {
                scrollBar.setOpaque(isOpaque);
            }
            scrollBar = getVerticalScrollBar();
            if (scrollBar != null) {
                scrollBar.setOpaque(isOpaque);
            }
        }
    }

    protected abstract DefaultTableModelWithRowName generateDefaultModel();

    public JComponent initColumnControl() {
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu(SUSPENSION_POINTS);
        menuBar.add(menu);
        menu.setToolTipText("Table control");
        JMenuItem packAll = new JMenuItem("Pack all");
        packAll.setToolTipText("Pack all");
        menu.add(packAll);
        packAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (table != null) {
                    table.packAll();
                }
            }
        });

        if (isMenuTrimTableVisible()) {
            JMenuItem trim = new JMenuItem("Show trim table");
            trim.setToolTipText("Show trim table");
            menu.add(trim);

            trim.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (matrix != null) {
                        T tmpMatrix = getCopy(matrix);
                        showTrimTable(tmpMatrix);
                    }
                }
            });
        }

        JMenuItem save = new JMenuItem("Save data file");
        save.setToolTipText("Save data file");
        menu.add(save);

        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveDataFile();
            }
        });

        JMenuItem loadDataFile = new JMenuItem("Load data file");
        loadDataFile.setToolTipText("Load data file");
        menu.add(loadDataFile);

        loadDataFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadDataFile();
            }
        });

        JMenuItem applySelection = new JMenuItem("Apply selection");
        applySelection.setToolTipText("Apply the selection on the image");
        menu.add(applySelection);

        applySelection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                applySelection();
            }
        });

        JMenuItem refresh = new JMenuItem("Refresh table");
        refresh.setToolTipText("Refresh the table");
        menu.add(refresh);

        refresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refresh();
            }
        });

        loadDataVisibleCheckBox.setToolTipText("Load data visible");
        loadDataVisibleCheckBox.setEnabled(loadDataVisible);
        menu.add(loadDataVisibleCheckBox);

        loadDataVisibleCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getModifiers() != 0) {
                    setLoadDataVisible(loadDataVisibleCheckBox.isSelected());
                }
            }
        });

        return menuBar;

    }

    protected boolean isMenuTrimTableVisible() {
        return true;
    }

    public synchronized void packAll() {
        table.packAll();
    }

    public boolean isLoadDataVisible() {
        return loadDataVisible;
    }

    public void setLoadDataVisible(boolean loadDataVisible) {
        this.loadDataVisible = loadDataVisible;

        if (loadMatrix == null) {
            loadDataVisibleCheckBox.setEnabled(false);
            loadDataVisible = false;
        } else {
            loadDataVisibleCheckBox.setEnabled(true);
        }

        updateLoadCheckBox = true;

        if (loadDataVisible) {
            if (loadMatrix != null) {
                if (matrix != null) {
                    originalMatrix = getCopy(matrix);
                }
                matrix = loadMatrix;
                structureChanged = true;
                dataComponentDelegate.setDataChanged(this, true);
            }
        } else if (originalMatrix != null) {
            matrix = getCopy(originalMatrix);
            originalMatrix = null;
            structureChanged = true;
            dataComponentDelegate.setDataChanged(this, true);
        }

    }

    public void loadDataFile() {
        String filename = CometeUtils.showFileChooserDialog(this, true);
        if (filename != null) {
            loadDataFile(filename);
        }
    }

    public abstract void loadDataFile(String loadDataFile);

    public void saveDataFile() {
        String filename = CometeUtils.showFileChooserDialog(this, false);
        saveDataFile(filename);
    }

    public abstract void saveDataFile(String fileName);

    protected abstract T getCopy(T toCopy);

    protected abstract void showTrimTable(final T matrix);

    protected abstract void applySelection();

    public String getValueSeparator() {
        return valueSeparator;
    }

    public void setValueSeparator(String valueSeparator) {
        this.valueSeparator = valueSeparator;
    }

    @Override
    public boolean dataChanged() {
        return dataComponentDelegate.dataChanged(this);
    }

    @Override
    public void updateFromData() {
        getDrawingThreadManager().runInDrawingThread(() -> {
            if (structureChanged) {
                structureChanged = false;
                if (tableModel != null) {
                    tableModel.fireTableStructureChanged();
                }
            } else {
                if (tableModel != null) {
                    tableModel.fireTableDataChanged();
                }
            }
            if (updateLoadCheckBox) {
                updateLoadCheckBox = false;
                loadDataVisibleCheckBox.setSelected(loadDataVisible);
            }
        });
    }

    @Override
    public void addTableListener(ITableListener<T> listener) {
        synchronized (listeners) {
            if (!listeners.contains(listener)) {
                listeners.add(listener);
            }
        }
    }

    @Override
    public void removeTableListener(ITableListener<T> listener) {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }

    @Override
    public void fireValueChanged(T matrix) {
        Collection<ITableListener<T>> listeners;
        synchronized (this.listeners) {
            listeners = new ArrayList<>(this.listeners);
        }
        try {
            for (ITableListener<T> tmpListener : listeners) {
                tmpListener.valueChanged(matrix);
            }
        } finally {
            listeners.clear();
        }
    }

    @Override
    public String[] getCustomColumnNames() {
        return customColumnNames;
    }

    @Override
    public synchronized void setCustomColumnNames(String[] customColumnNames) {
        if (this.customColumnNames != customColumnNames) {
            this.customColumnNames = customColumnNames;
            structureChanged = true;
            dataComponentDelegate.setDataChanged(this, true);
        }
    }

    @Override
    public String[] getCustomRowNames() {
        return customRowNames;
    }

    @Override
    public synchronized void setCustomRowNames(String[] customRowNames) {
        if (this.customRowNames != customRowNames) {
            this.customRowNames = customRowNames;
            structureChanged = true;
            dataComponentDelegate.setDataChanged(this, true);
        }
    }

    public boolean isPackAllOnInit() {
        return packAllOnInit;
    }

    public void setPackAllOnInit(boolean packAllOnInit) {
        this.packAllOnInit = packAllOnInit;
    }

    @Override
    public T getData() {
        return matrix;
    }

    @Override
    public void setData(T matrix) {
        if (!ObjectUtils.sameObject(matrix, this.matrix)) {
            boolean structureChanged = !isSameMatrixStructure(this.matrix, matrix);
            this.matrix = matrix;
            if (structureChanged) {
                this.structureChanged = true;
            }
            dataComponentDelegate.setDataChanged(this, true);
            fireValueChanged(matrix);
        }
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public void refresh() {
        tableModel.fireTableDataChanged();
        packAll();
    }

    protected abstract boolean isSameMatrixStructure(T matrix1, T matrix2);

    @Override
    public CometeColor getCometeBackground() {
        Color color = table.getBackground();
        if (color != null) {
            return ColorTool.getCometeColor(color);
        }
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        Font font = table.getFont();
        if (font != null) {
            return FontTool.getCometeFont(font);
        }
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        Color color = table.getForeground();
        if (color != null) {
            return ColorTool.getCometeColor(color);
        }
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        table.setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(CometeFont font) {
        table.setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        table.setForeground(ColorTool.getColor(color));
    }

    @Override
    public int getHorizontalAlignment() {
        return 0;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void setEditable(boolean editable) {
        if (this.editable != editable) {
            this.editable = editable;
            TableCellEditor editor = table.getCellEditor();
            if (editor != null) {
                editor.cancelCellEditing();
            }
            repaint();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        selectedColumn = table.getSelectedColumn();
        selectedRow = table.getSelectedRow();
        selectedPoint[IChartViewer.X_INDEX] = selectedColumn;
        selectedPoint[IChartViewer.Y_INDEX] = selectedRow;
        if (selectedRow != -1 && selectedColumn != -1 && !listeners.isEmpty()) {
            synchronized (listeners) {
                for (ITableListener<T> tmpListener : listeners) {
                    tmpListener.selectedColumnChanged(selectedColumn);
                    tmpListener.selectedRowChanged(selectedRow);
                    tmpListener.selectedPointChanged(selectedPoint);
                }
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public boolean hasFocus() {
        boolean focus;
        if ((table != null) && (table.getSelectedColumnCount() > 0)) {
            focus = true;
        } else {
            focus = false;
        }
        return focus;
    }

    public int getSelectedColumn() {
        return selectedColumn;
    }

    public void setSelectedColumn(int selectedColumn) {
        this.selectedColumn = selectedColumn;
        selectedPoint[IChartViewer.X_INDEX] = selectedColumn;
        selectedPoint[IChartViewer.Y_INDEX] = selectedRow;
    }

    public int getSelectedRow() {
        return selectedRow;
    }

    public void setSelectedRow(int selectedRow) {
        this.selectedRow = selectedRow;
        selectedPoint[IChartViewer.X_INDEX] = selectedColumn;
        selectedPoint[IChartViewer.Y_INDEX] = selectedRow;
    }

    public double[] getSelectedPoint() {
        return selectedPoint;
    }

    public boolean isSaveColumnIndex() {
        return saveColumnIndex;
    }

    public void setSaveColumnIndex(boolean saveColumnIndex) {
        this.saveColumnIndex = saveColumnIndex;
    }

    public Mask getMask() {
        return table.getMask();
    }

    public void setMask(Mask mask) {
        table.setMask(mask);
    }

    public Color getMaskColor() {
        return table.getMaskColor();
    }

    public void setMaskColor(Color maskColor) {
        table.setMaskColor(maskColor);
    }

    public Color getMaskSelectionColor() {
        return table.getMaskSelectionColor();
    }

    public void setMaskSelectionColor(Color maskSelectionColor) {
        table.setMaskSelectionColor(maskSelectionColor);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return table.isEditing();
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return false;
    }

}
