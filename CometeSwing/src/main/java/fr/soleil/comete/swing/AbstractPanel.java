/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;

public abstract class AbstractPanel extends JPanel implements IPanel, CometeConstants {

    private static final long serialVersionUID = -1931786755649577301L;

    private int horizontalAlignment;
    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;

    public AbstractPanel() {
        super();
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        horizontalAlignment = IComponent.CENTER;
    }

    @Override
    public void setPreferredSize(final int width, final int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void add(final IComponent component) {
        if (component instanceof java.awt.Component) {
            add((java.awt.Component) component);
        }
    }

    @Override
    public void addCenter(final IComponent component) {
        if (component instanceof java.awt.Component) {
            add((java.awt.Component) component, BorderLayout.CENTER);
        }
    }

    @Override
    public void add(final IComponent comp, final Object constraints) {
        if ((constraints != null) && (comp instanceof java.awt.Component)) {
            final LayoutManager layout = getLayout();
            if (layout == null || (layout instanceof FlowLayout)) {
                setLayout(new BorderLayout());
            }
            add((java.awt.Component) comp, constraints);
        }
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(getForeground());
    }

    @Override
    public void setCometeBackground(final CometeColor color) {
        setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(final CometeFont font) {
        setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(final CometeColor color) {
        setForeground(ColorTool.getColor(color));
    }

    @Override
    public void setLayout(final Object layout) {
        if (layout instanceof LayoutManager) {
            super.setLayout((LayoutManager) layout);
        }
    }

    @Override
    public int getHorizontalAlignment() {
        return horizontalAlignment;
    }

    @Override
    public void setHorizontalAlignment(final int halign) {
        horizontalAlignment = halign;
        switch (horizontalAlignment) {
            case IComponent.CENTER:
                setAlignmentX(CENTER_ALIGNMENT);
                break;
            case IComponent.LEFT:
                setAlignmentX(LEFT_ALIGNMENT);
                break;
            case IComponent.RIGHT:
                setAlignmentX(RIGHT_ALIGNMENT);
                break;
        }
    }

    @Override
    public void setTitledBorder(final String title) {
        setBorder(BorderFactory.createTitledBorder(title));
    }

    @Override
    public void addMediator(final Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(final Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to
     *            {@link Mediator}s
     */
    public <I, V extends TargetInformation<I>> void warnMediators(final V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void addMouseListener(final IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(final IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
