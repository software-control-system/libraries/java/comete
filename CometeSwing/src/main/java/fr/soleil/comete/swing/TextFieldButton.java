/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.listener.IButtonListener;
import fr.soleil.comete.definition.listener.ITextFieldListener;
import fr.soleil.comete.definition.widget.IButton;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.definition.widget.ITextField;
import fr.soleil.comete.definition.widget.ITextFieldButton;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.data.target.ICancelableTarget;
import fr.soleil.data.target.IErrorNotifiableTarget;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Swing implementation of {@link ITextFieldButton}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TextFieldButton extends Panel
        implements ITextFieldButton, ICancelableTarget, IErrorNotifiableTarget, ActionListener, DocumentListener {

    private static final long serialVersionUID = -6867375908629274404L;

    protected final TextField textField;
    protected final Button sendButton;
    protected boolean editable;
    protected boolean alwaysUpdateTextField;
    protected boolean disableButtonWithEmptyTextField;

    /**
     * Default constructor
     */
    public TextFieldButton() {
        this(false);
    }

    /**
     * Constructs this {@link TextFieldButton}
     * 
     * @param listenToTextFieldActions Whether actionPerformed on textfield should send data
     */
    public TextFieldButton(boolean listenToTextFieldActions) {
        super();
        editable = true;
        textField = initTextField(listenToTextFieldActions);
        sendButton = initButton();
        alwaysUpdateTextField = false;
        disableButtonWithEmptyTextField = false;
        initComponentsLayout();
    }

    @Override
    public void setToolTipText(String text) {
        textField.setToolTipText(text);
        sendButton.setToolTipText(text);
    }

    @Override
    public CometeColor getCometeBackground() {
        return getBackgroundTextField();
    }

    @Override
    public void setCometeBackground(CometeColor bg) {
        setBackgroundTextField(bg);
    }

    @Override
    public CometeColor getCometeForeground() {
        return getForegroundTextField();
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        setForegroundTextField(color);
    }

    /**
     * Initializes the "send" button
     * 
     * @return A {@link Button}
     */
    protected Button initButton() {
        Button button = new Button();
        button.setText("Send");
        button.setButtonLook(true);
        button.addActionListener(this);
        return button;
    }

    /**
     * Initializes the textfield
     * 
     * @param listenToTextFieldActions Whether actionPerformed on textfield should send data
     * @return A {@link TextField}
     */
    protected TextField initTextField(boolean listenToTextFieldActions) {
        TextField textField = new TextField();
        textField.removeActionListener(textField);
        textField.getDocument().addDocumentListener(this);
        textField.setSize(50, 20);
        if (listenToTextFieldActions) {
            textField.addActionListener(this);
        }
        return textField;
    }

    /**
     * Layouts the components
     */
    protected void initComponentsLayout() {
        add((ITextField) textField, BorderLayout.CENTER);
        add((IButton) sendButton, BorderLayout.EAST);
    }

    /**
     * Adds an {@link ITextFieldListener} to the {@link TextField}
     * 
     * @param listener The listener to add
     */
    public void addTextFieldListener(ITextFieldListener listener) {
        textField.addTextFieldListener(listener);
    }

    /**
     * Removes an {@link ITextFieldListener} from the {@link TextField}
     * 
     * @param listener The listener to remove
     */
    public void removeTextFieldListener(ITextFieldListener listener) {
        textField.removeTextFieldListener(listener);
    }

    /**
     * Adds an {@link IButtonListener} to the {@link Button}
     * 
     * @param listener The listener to add
     */
    public void addButtonListener(IButtonListener listener) {
        sendButton.addButtonListener(listener);
    }

    /**
     * Removes an {@link IButtonListener} from the {@link Button}
     * 
     * @param listener The listener to remove
     */
    public void removeButtonListener(IButtonListener listener) {
        sendButton.removeButtonListener(listener);
    }

    @Override
    public String getText() {
        return textField.getText();
    }

    @Override
    public void setText(String text) {
        if (!ObjectUtils.sameObject(text, textField.getText())) {
            if (isAlwaysUpdateTextField()) {
                textField.isEditingData = false;
            }
            textField.setText(text);
        }
    }

    /**
     * Returns the {@link Button}'s text
     * 
     * @return A {@link String}
     */
    public String getTextButton() {
        return sendButton.getText();
    }

    /**
     * Sets the {@link Button}'s text
     * 
     * @param textButton The text to set
     */
    public void setTextButton(String textButton) {
        sendButton.setText(textButton);
    }

    /**
     * Sets the {@link Button}'s horizontal alignment
     * 
     * @param alignment The alignment to set
     */
    public void setButtonHorizontalAlignment(int alignment) {
        sendButton.setHorizontalAlignment(alignment);
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void setEditable(boolean editable) {
        if (this.editable != editable) {
            this.editable = editable;
            textField.setEditable(editable);
            updateButtonAvailability();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        updateButtonAvailability();
        textField.setEnabled(enabled);
    }

    /**
     * Updates "send" button availability (enabled/disabled)
     */
    protected void updateButtonAvailability() {
        boolean active;
        if (isEnabled() && isEditable()) {
            if (isDisableButtonWithEmptyTextField()) {
                String text = textField.getText();
                if ((text != null) && (!text.trim().isEmpty())) {
                    active = true;
                } else {
                    active = false;
                }
            } else {
                active = true;
            }
        } else {
            active = false;
        }
        sendButton.setEnabled(active);
    }

    /**
     * Returns whether contained {@link TextField} text is always updated on {@link #setText(String)}
     * 
     * @return A <code>boolean</code> value. <code>false</code> by default.
     */
    public boolean isAlwaysUpdateTextField() {
        return alwaysUpdateTextField;
    }

    /**
     * Sets whether contained {@link TextField} text should always be updated on {@link #setText(String)}
     * 
     * @param alwaysUpdateText whether contained {@link TextField} text should always be updated on
     *            {@link #setText(String)}. If <code>true</code>, every call to {@link #setText(String)} will change
     *            {@link TextField}'s text. Otherwise, the text
     *            will change only if {@link TextField} considers no user is editing its text
     *            through keyboard
     */
    public void setAlwaysUpdateTextField(boolean alwaysUpdateText) {
        this.alwaysUpdateTextField = alwaysUpdateText;
    }

    /**
     * Returns whether "send" button will be disabled when text field is empty.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isDisableButtonWithEmptyTextField() {
        return disableButtonWithEmptyTextField;
    }

    /**
     * Sets whether "send" button should be disabled when text field is empty.
     * 
     * @param disableButtonWithEmptyTextField Whether "send" button should be disabled when text field is empty.
     */
    public void setDisableButtonWithEmptyTextField(boolean disableButtonWithEmptyTextField) {
        if (this.disableButtonWithEmptyTextField != disableButtonWithEmptyTextField) {
            this.disableButtonWithEmptyTextField = disableButtonWithEmptyTextField;
            updateButtonAvailability();
        }
    }

    /**
     * Returns the {@link Button}'s background
     * 
     * @return A {@link CometeColor}
     */
    public CometeColor getBackgroundButton() {
        return sendButton.getCometeBackground();
    }

    /**
     * Sets The {@link Button}'s background
     * 
     * @param backgroundButton The background to set
     */
    public void setBackgroundButton(CometeColor backgroundButton) {
        sendButton.setCometeBackground(backgroundButton);
        if (backgroundButton == null) {
            sendButton.setOpaque(!sendButton.isButtonLook());
        } else {
            sendButton.setOpaque(true);
        }
    }

    /**
     * Returns the {@link TextField}'s background
     * 
     * @return A {@link CometeColor}
     */
    public CometeColor getBackgroundTextField() {
        return textField.getCometeBackground();
    }

    /**
     * Sets the {@link TextField}'s background
     * 
     * @param color The background to set
     */
    public void setBackgroundTextField(CometeColor color) {
        if (textField != null) {
            textField.setCometeBackground(color);
        }
    }

    /**
     * Returns the {@link TextField}'s foreground
     * 
     * @return A {@link CometeColor}
     */
    public CometeColor getForegroundTextField() {
        return textField.getCometeForeground();
    }

    /**
     * Sets the {@link TextField}'s foreground
     * 
     * @param color The foreground to set
     */
    public void setForegroundTextField(CometeColor color) {
        if (textField != null) {
            textField.setCometeForeground(color);
        }
    }

    /**
     * Sets the {@link TextField}'s horizontal alignment
     * 
     * @param alignment The alignment to set
     */
    public void setTextFieldHorizontalAlignment(int alignment) {
        textField.setHorizontalAlignment(alignment);
    }

    /**
     * Returns the {@link TextField}'s columns
     * 
     * @return An <code>int</code>
     */
    public int getTextFieldColumns() {
        return textField.getColumns();
    }

    /**
     * Sets the {@link TextField}'s columns
     * 
     * @param columns The columns to set
     */
    public void setTextFieldColumns(int columns) {
        textField.setColumns(columns);
    }

    @Override
    public void setCometeFont(CometeFont arg0) {
        super.setCometeFont(arg0);
        if (sendButton != null) {
            sendButton.setCometeFont(arg0);
        }
        if (textField != null) {
            textField.setCometeFont(arg0);
        }
    }

    @Override
    public boolean hasFocus() {
        if (textField != null && sendButton != null) {
            return (textField.hasFocus() || sendButton.hasFocus());
        }
        return false;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event != null) {
            Object source = event.getSource();
            if ((source != null) && ((source == sendButton) || (source == textField))) {
                send();
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        textChanged(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        textChanged(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        textChanged(e);
    }

    protected void textChanged(DocumentEvent e) {
        if (isDisableButtonWithEmptyTextField() && (e != null) && (e.getDocument() == textField.getDocument())) {
            updateButtonAvailability();
        }
    }

    @Override
    public boolean isEditingData() {
        return ((textField != null) && textField.isEditingData());
    }

    @Override
    public synchronized void setSendButtonVisible(boolean visible) {
        sendButton.removeActionListener(this);
        sendButton.setEnabled(visible);
        sendButton.setVisible(visible);
        if (visible) {
            sendButton.addActionListener(this);
        }
    }

    /**
     * Enables/disables the {@link Button}
     * 
     * @param enabled Whether to activate the {@link Button}
     */
    public void setButtonEnabled(boolean enabled) {
        sendButton.setEnabled(enabled);
    }

    /**
     * Returns whether the {@link Button} is enabled
     * 
     * @return A <code>boolean</code>
     */
    public boolean isButtonEnabled() {
        return sendButton.isEnabled();
    }

    public void setTextFieldEditable(boolean editable) {
        textField.setEditable(editable && this.editable);
    }

    public boolean isTextFieldEditable() {
        return textField.isEditable();
    }

    @Override
    public void cancel() {
        textField.cancel();
    }

    @Override
    public void send() {
        String newValue = getText();
        if (newValue != null) {
            textField.isEditingData = false;
            warnMediators(new TextInformation(this, newValue));
        }
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        textField.notifyForError(message, error);
    }

    /**
     * Main class, so you can have an example.
     */
    public static void main(String[] args) {

        IPanel panel = new Panel();
        TextFieldButton widget = new TextFieldButton();
        widget.setText("truc de test");
        widget.setBackgroundButton(CometeColor.RED);
        widget.setTextFieldHorizontalAlignment(IComponent.RIGHT);
        widget.setBackgroundTextField(CometeColor.CYAN);

        widget.setEditable(false);

        panel.add(widget, BorderLayout.CENTER);

        IFrame frame = new Frame();
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setTitle("TextFieldButton test");
    }

}
