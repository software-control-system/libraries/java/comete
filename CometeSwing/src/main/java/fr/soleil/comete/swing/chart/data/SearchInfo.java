/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.data;

import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A class to handle search result. It is used when user clicks on the graph and when the nearest
 * value found is displayed
 */
public class SearchInfo implements CometeConstants {

    /**
     * Value tooltip TOPLEFT placement
     */
    public static final int TOPLEFT = 0;
    /**
     * Value tooltip TOPRIGHT placement
     */
    public static final int TOPRIGHT = 1;
    /**
     * Value tooltip BOTTOMLEFT placement
     */
    public static final int BOTTOMLEFT = 2;
    /**
     * Value tooltip BOTTOMRIGHT placement
     */
    public static final int BOTTOMRIGHT = 3;

    private boolean found;
    private int x;
    private int y;
    private int axis;
    private AbstractDataView dataView;
    private DataList value;
    private AbstractDataView xdataView;
    private DataList xvalue;
    private double dist;
    private int placement;
    private int clickIdx;

    public SearchInfo() {
        this(0, 0, null, -1, null, Integer.MAX_VALUE, 0, -1, false);
    }

    public SearchInfo(int x, int y, AbstractDataView dataView, int axis, DataList value, double dist, int placement,
            int idx) {
        this(x, y, dataView, axis, value, dist, placement, idx, true);
    }

    protected SearchInfo(int x, int y, AbstractDataView dataView, int axis, DataList value, double dist, int placement,
            int idx, boolean found) {
        this.found = found;
        this.x = x;
        this.y = y;
        this.dataView = dataView;
        this.value = value;
        this.dist = dist;
        this.placement = placement;
        this.axis = axis;
        xvalue = null;
        xdataView = null;
        clickIdx = idx;
    }

    public void setXValue(DataList d, AbstractDataView x) {
        xvalue = d;
        xdataView = x;
    }

    /**
     * Returns <code>true</code> when a point was found
     * 
     * @return A <code>boolean</code>
     */
    public boolean isFound() {
        return found;
    }

    /**
     * Returns the x pixel coordinates of the found point
     * 
     * @return An <code>int</code>
     */
    public int getX() {
        return x;
    }

    /**
     * Sets the x pixel coordinates of the found point
     * 
     * @param x The x pixel coordinates of the found point
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Returns the y pixel coordinates of the found point
     * 
     * @return An <code>int</code>
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the y pixel coordinates of the found point
     * 
     * @param y The y pixel coordinates of the found point
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Returns the axis on which the view containing the point is displayed
     * 
     * @return A <code>int</code>
     */
    public int getAxis() {
        return axis;
    }

    /**
     * Returns the Y DataView which contains the point
     * 
     * @return A {@link AbstractDataView}
     */
    public AbstractDataView getDataView() {
        return dataView;
    }

    /**
     * Returns the handle to the y value
     * 
     * @return A {@link DataList}
     */
    public DataList getValue() {
        return value;
    }

    /**
     * Returns the x DataView which contains the point (XY monitoring)
     * 
     * @return A {@link AbstractDataView}
     */
    public AbstractDataView getXDataView() {
        return xdataView;
    }

    /**
     * Returns the handle to the X value (XY monitoring)
     * 
     * @return A {@link DataList}
     */
    public DataList getXValue() {
        return xvalue;
    }

    /**
     * Returns the square distance from click to point (pixel)
     * 
     * @return A <code>double</code>
     */
    public double getDist() {
        return dist;
    }

    /**
     * Returns the placement of the tooltip panel
     * 
     * @return An <code>int</code>
     */
    public int getPlacement() {
        return placement;
    }

    /**
     * Returns the index in the dataView that contains the clicked point
     * 
     * @return An <code>int</code>
     */
    public int getClickIdx() {
        return clickIdx;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj.getClass().equals(getClass())) {
            SearchInfo info = (SearchInfo) obj;
            equals = (found == info.found) && (x == info.x) && (y == info.y) && (axis == info.axis)
                    && (dataView == info.dataView) && (xdataView == info.xdataView) && (xvalue == info.xvalue)
                    && (dist == info.dist) && (placement == info.placement) && (clickIdx == info.clickIdx);

        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0x5EA17C11;
        int mult = 0x12F0;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.generateHashCode(found);
        code = code * mult + x;
        code = code * mult + y;
        code = code * mult + axis;
        code = code * mult + ObjectUtils.getHashCode(dataView);
        code = code * mult + ObjectUtils.getHashCode(xdataView);
        code = code * mult + ObjectUtils.getHashCode(xvalue);
        code = code * mult + (int) dist;
        code = code * mult + placement;
        code = code * mult + clickIdx;
        return code;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("SearchInfo[ ");
        if (found) {
            builder.append("Hit (").append(x).append(COMMA).append(y);
            builder.append(") View name=").append(dataView.getId());
            builder.append(" Org (").append(value.getX()).append(COMMA).append(value.getY());
            builder.append(") Dist=").append(dist);
        } else {
            builder.append("No Hit]");
        }
        return builder.toString();
    }

}
