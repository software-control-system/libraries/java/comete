/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.util.EventObject;

import javax.swing.Icon;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.listener.IButtonListener;
import fr.soleil.comete.definition.listener.IFileBrowserListener;
import fr.soleil.comete.definition.listener.ITextFieldListener;
import fr.soleil.comete.definition.widget.IButton;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IFileBrowser;
import fr.soleil.comete.definition.widget.IFileBrowserTextFieldButton;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.definition.widget.ITextField;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.data.target.ICancelableTarget;
import fr.soleil.data.target.information.FileInformation;
import fr.soleil.lib.project.ObjectUtils;

public class FileBrowserFieldButton extends TextFieldButton implements IFileBrowserTextFieldButton, ICancelableTarget,
        ITextFieldListener, IButtonListener, IFileBrowserListener {

    private static final long serialVersionUID = 6346961320466027951L;

    protected FileBrowser fileBrowser;
    protected final Border previousBorder;
    protected final Border editingBorder;
    protected File lastSelectedFile;

    private boolean fromSource;

    public FileBrowserFieldButton() {
        super();
        initListeners();
        lastSelectedFile = null;
        fromSource = true;
        previousBorder = textField.getBorder();
        editingBorder = new LineBorder(Color.ORANGE, 2, true);
    }

    public void setBrowseText(String text) {
        boolean updateAllowed = fileBrowser.isUpdateAllowed();
        fileBrowser.setUpdateAllowed(true);
        fileBrowser.setText(text);
        fileBrowser.setUpdateAllowed(updateAllowed);
    }

    public void setBrowseIcon(Icon icon) {
        ((AbstractButton) fileBrowser).setIcon(icon);
    }

    @Override
    public void setText(String text) {
        setFile(text == null ? null : new File(text));
    }

    @Override
    public synchronized void setFile(File file) {
        if (!ObjectUtils.sameObject(file, fileBrowser.getFile())) {
            lastSelectedFile = file;
        }
        fileBrowser.setFile(file);
        File newFile = fileBrowser.getFile();
        textField.isEditingData = false;
        textField.setText(newFile == null ? ObjectUtils.EMPTY_STRING : newFile.getAbsolutePath());
        fromSource = true;
        updateBorder();
    }

    @Override
    public void addFileBrowserListener(IFileBrowserListener listener) {
        if (fileBrowser != null) {
            fileBrowser.addFileBrowserListener(listener);
        }
    }

    @Override
    public void removeFileBrowserListener(IFileBrowserListener listener) {
        fileBrowser.removeFileBrowserListener(listener);
    }

    @Override
    public int getBrowserType() {
        if (fileBrowser != null) {
            return fileBrowser.getBrowserType();
        }
        return IFileBrowser.FULLNAME;
    }

    @Override
    public void setBrowserType(int type) {
        if (fileBrowser != null) {
            fileBrowser.setBrowserType(type);
        }
    }

    @Override
    public boolean isDisplayHiddenFile() {
        if (fileBrowser != null) {
            return fileBrowser.isDisplayHiddenFile();
        }
        return false;
    }

    @Override
    public void setDisplayHiddenFile(boolean displayHiddenFile) {
        if (fileBrowser != null) {
            fileBrowser.setDisplayHiddenFile(displayHiddenFile);
        }
    }

    @Override
    public String getExtensionFile() {
        if (fileBrowser != null) {
            return fileBrowser.getExtensionFile();
        }
        return null;
    }

    @Override
    public void setExtensionFile(String extensionFile) {
        if (fileBrowser != null) {
            fileBrowser.setExtensionFile(extensionFile);
        }
    }

    @Override
    public String getTitle() {
        if (fileBrowser != null) {
            fileBrowser.getTitle();
        }
        return "File Browser";
    }

    @Override
    public void setTitle(String title) {
        if (fileBrowser != null) {
            fileBrowser.setTitle(title);
        }
    }

    @Override
    public void setCometeImage(CometeImage defaultImage) {
        if (fileBrowser != null) {
            fileBrowser.setCometeImage(defaultImage);
        }
    }

    @Override
    public CometeImage getCometeImage() {
        if (fileBrowser != null) {
            return fileBrowser.getCometeImage();
        }
        return null;
    }

    @Override
    public void setCometeBackground(CometeColor bg) {
        setBackgroundTextField(bg);
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        setForegroundTextField(color);
    }

    protected void initListeners() {
        addTextFieldListener(this);
        addFileBrowserListener(this);
    }

    public void setTooltipToBrowser(String tooltipText) {
        if (tooltipText != null) {
            fileBrowser.setToolTipText(tooltipText);
        }
    }

    @Override
    public void actionPerformed(EventObject event) {
        // nothing to do
    }

    protected FileBrowser generateFileBrowser() {
        return new FileBrowser();
    }

    @Override
    protected void initComponentsLayout() {
        fileBrowser = generateFileBrowser();

        setLayout(new GridBagLayout());
        textField.setPreferredSize(null);
        textField.setColumns(15);
        addTextFieldListener(this);

        GridBagConstraints textFieldConstraints = new GridBagConstraints();
        textFieldConstraints.fill = GridBagConstraints.BOTH;
        textFieldConstraints.gridx = 0;
        textFieldConstraints.gridy = 0;
        textFieldConstraints.weightx = 1;
        textFieldConstraints.weighty = 1;
        add((ITextField) textField, textFieldConstraints);
        textField.setCometeBackground(CometeColor.WHITE);

        GridBagConstraints fileSelectorConstraints = new GridBagConstraints();
        fileSelectorConstraints.fill = GridBagConstraints.VERTICAL;
        fileSelectorConstraints.gridx = 1;
        fileSelectorConstraints.gridy = 0;
        fileSelectorConstraints.weightx = 0;
        fileSelectorConstraints.weighty = 1;
        add((IFileBrowser) fileBrowser, fileSelectorConstraints);
        addFileBrowserListener(this);

        GridBagConstraints writeButtonConstraints = new GridBagConstraints();
        writeButtonConstraints.fill = GridBagConstraints.VERTICAL;
        writeButtonConstraints.gridx = 2;
        writeButtonConstraints.gridy = 0;
        writeButtonConstraints.weightx = 0;
        writeButtonConstraints.weighty = 1;
        writeButtonConstraints.insets = new Insets(0, 5, 0, 0);
        sendButton.setText("Send");
        sendButton.setButtonLook(true);
        addButtonListener(this);
        add((IButton) sendButton, writeButtonConstraints);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        fileBrowser.setEnabled(enabled && isEditable());
    }

    @Override
    public void setEditable(boolean editable) {
        super.setEditable(editable);
        fileBrowser.setEnabled(editable && isEnabled());
    }

    public CometeColor getBackgroundFileSelector() {
        return fileBrowser.getCometeBackground();
    }

    public void setBackgroundFileSelector(CometeColor backgroundFileSelector) {
        fileBrowser.setCometeBackground(backgroundFileSelector);
        if (backgroundFileSelector == null) {
            fileBrowser.setOpaque(!((IButton) fileBrowser).isButtonLook());
        } else {
            fileBrowser.setOpaque(true);
        }
    }

    public CometeColor getMainBackground() {
        return getCometeBackground();
    }

    public void setMainBackground(CometeColor color) {
        setCometeBackground(color);
    }

    @Override
    public void setCometeFont(CometeFont arg0) {
        super.setCometeFont(arg0);

        if (fileBrowser != null) {
            fileBrowser.setCometeFont(arg0);
        }
    }

    @Override
    public boolean hasFocus() {
        boolean result = super.hasFocus();
        if ((fileBrowser != null)) {
            result = (result || fileBrowser.hasFocus());
        }
        return result;
    }

    @Override
    public void cancel() {
        super.cancel();
        setText(textField.lastText);
    }

    @Override
    public synchronized void send() {
        File file = getFile();
        String fullName = (file == null ? null : file.getAbsolutePath());
        if (fullName != null) {
            fromSource = true;
            textField.isEditingData = false;
            setText(fullName);
            lastSelectedFile = new File(fullName);
            warnMediators(new TextInformation(this, getText()));
            warnMediators(new FileInformation(this, file));
            updateBorder();
        }
    }

    @Override
    public synchronized void textChanged(EventObject event) {
        if (textField.isEditingData) {
            fromSource = false;
            updateBorder();
            fileBrowser.setFile(new File(getText()));
        }
    }

    @Override
    public void selectedFileNameChange(String name) {
        if (getBrowserType() == IFileBrowser.FILENAME) {
            updateField(false);
        }
    }

    @Override
    public void selectedFullFileNameChange(String name) {
        if (getBrowserType() == IFileBrowser.FULLNAME || getBrowserType() == IFileBrowser.ALL) {
            updateField(false);
        }
    }

    @Override
    public void selectedPathChange(String name) {
        if (getBrowserType() == IFileBrowser.PATH) {
            updateField(false);
        }
    }

    protected synchronized void updateField(boolean fromSource) {
        this.fromSource = fromSource;
        String fullName = computeFileFullName();
        textField.setToolTipText(fullName);
        setText(fullName);
        updateBorder();
    }

    protected String computeFileFullName() {
        File file = getFile();
        return (file == null ? null : file.getAbsolutePath());
    }

    protected synchronized void updateBorder() {
        if (fromSource) {
            textField.setBorder(previousBorder);
        } else {
            textField.setBorder(editingBorder);
        }
    }

    @Override
    public boolean isEditingData() {
        return (super.isEditingData() || ((fileBrowser != null) && (fileBrowser.isEditingData())));
    }

    /**
     * Main class, so you can have an example. You can monitor your own
     * attribute by giving its full path name in argument
     */
    public static void main(String[] args) {

        IPanel panel = new Panel();
        FileBrowserFieldButton widget = new FileBrowserFieldButton();
        widget.setMainBackground(CometeColor.BLACK);
        widget.setBackgroundButton(CometeColor.RED);
        widget.setBackgroundFileSelector(CometeColor.YELLOW);
        widget.setBackgroundTextField(CometeColor.CYAN);
        widget.setTextFieldHorizontalAlignment(IComponent.RIGHT);

        panel.add(widget);

        IFrame frame = new Frame();
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setTitle(widget.getClass().getSimpleName() + " test");
    }

    @Override
    public void fireSelectedFileChange(EventObject event) {
        fileBrowser.fireSelectedFileChange(event);
    }

    @Override
    public File getFile() {
        return fileBrowser.getFile();
    }

    @Override
    public String getDirectory() {
        return fileBrowser.getDirectory();
    }

    @Override
    public String getDefaultDirectory() {
        return fileBrowser.getDefaultDirectory();
    }

    @Override
    public void setDefaultDirectory(String defaultDirectory) {
        fileBrowser.setDefaultDirectory(defaultDirectory);
    }

    @Override
    public void setDirectory(String directory) {
        fileBrowser.setDirectory(directory);
    }

    public boolean isAllowNonExistingFiles() {
        return fileBrowser.isAllowNonExistingFiles();
    }

    public void setAllowNonExistingFiles(boolean allowNonExistingFiles) {
        fileBrowser.setAllowNonExistingFiles(allowNonExistingFiles);
    }

}
