/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.text.Collator;
import java.util.Comparator;

import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.lib.project.ObjectUtils;

/**
 * An {@link AbstractDataView} {@link Comparator}, that compares {@link AbstractDataView}s display names
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DataViewComparator implements Comparator<AbstractDataView> {

    public DataViewComparator() {
        super();
    }

    @Override
    public int compare(AbstractDataView o1, AbstractDataView o2) {
        String name1 = (o1 == null ? null : o1.getDisplayName());
        String name2 = (o2 == null ? null : o2.getDisplayName());
        if (name1 == null) {
            name1 = ObjectUtils.EMPTY_STRING;
        }
        if (name2 == null) {
            name2 = ObjectUtils.EMPTY_STRING;
        }
        return Collator.getInstance().compare(name1, name2);
    }

}
