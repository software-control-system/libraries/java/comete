/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import javax.swing.ListCellRenderer;

import fr.soleil.lib.project.swing.renderer.AdaptedFontListCellRenderer;

/**
 * A {@link ListCellRenderer} that manages a {@link String} array to display some data
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ValueListCellRenderer extends AdaptedFontListCellRenderer<Object> {

    /**
     * Returns the displayable values managed by this {@link ValueListCellRenderer}
     * 
     * @return A {@link String}<code>[]</code>
     */
    public String[] getDisplayableValues();

    /**
     * Sets this {@link ValueListCellRenderer}'s displayable values
     * 
     * @param values The displayable values to set
     */
    public void setDisplayableValues(String[] values);

}
