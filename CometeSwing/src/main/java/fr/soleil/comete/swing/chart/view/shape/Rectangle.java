/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Stroke;

import fr.soleil.comete.definition.widget.util.IChartConst.ShapeType;

/**
 * {@link AbstractShape} specialized in rectangles
 * 
 * @author huriez
 */
public class Rectangle extends AbstractShape {

    private java.awt.Rectangle rectangle;
    private boolean fill;
    private BasicStroke stroke;
    private Paint pattern;
    private java.awt.Rectangle clip;
    private Color clipColor;

    public Rectangle(ShapeType type) {
        super(type);
    }

    public Rectangle(ShapeType type, java.awt.Rectangle rectangle, boolean fill, Color color) {
        this(type, rectangle, fill, color, null, null);
    }

    public Rectangle(ShapeType type, java.awt.Rectangle rectangle, boolean fill, Color color, java.awt.Rectangle clip,
            Color clipColor) {
        super(type);
        this.rectangle = rectangle;
        this.fill = fill;
        this.color = color;
        this.clip = clip;
        this.clipColor = clipColor;
    }

    /**
     * @return the fill
     */
    public boolean isFill() {
        return fill;
    }

    /**
     * @param fill the fill to set
     */
    public void setFill(boolean fill) {
        this.fill = fill;
    }

    /**
     * @return the stroke
     */
    public BasicStroke getStroke() {
        return stroke;
    }

    /**
     * @param stroke the stroke to set
     */
    public void setStroke(BasicStroke stroke) {
        this.stroke = stroke;
    }

    /**
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * @return the pattern
     */
    public Paint getPattern() {
        return pattern;
    }

    /**
     * @param pattern the pattern to set
     */
    public void setPattern(Paint pattern) {
        this.pattern = pattern;
    }

    /**
     * @return the rectangle
     */
    public java.awt.Rectangle getRectangle() {
        return rectangle;
    }

    @Override
    public void drawShape(Graphics2D g) {
        Stroke oldStroke = null;
        if (stroke != null) {
            oldStroke = g.getStroke();
            g.setStroke(stroke);
        }
        g.translate(x, y);
        if (color != null) {
            g.setColor(color);
            if (fill) {
                g.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
            } else {
                g.drawRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
            }
        }
        if (fill && (clip != null) && (clipColor != null) && (!clipColor.equals(color))) {
            g.setColor(clipColor);
            g.fillRect(clip.x, clip.y, clip.width, clip.height);
        }
        g.translate(-x, -y);
        if (oldStroke != null) {
            g.setStroke(oldStroke);
        }
    }

}
