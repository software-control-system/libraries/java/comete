/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.plugin;

import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.lib.project.ObjectUtils;
import ij.gui.Roi;
import ij.gui.ShapeRoi;
import ij.process.ImageProcessor;

/**
 * {@link CometePlugInFilter} used to make composite {@link Roi}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MakeShapePlugin extends CometePlugInFilter implements CometeConstants {

    public final static String COMMAND_NAME = "makeShape";

    @Override
    public void run(ImageProcessor ip) {
        try {
            if (argument != null) {
                String[] values;
                values = argument.replace("(", ObjectUtils.EMPTY_STRING).replace(")", ObjectUtils.EMPTY_STRING)
                        .split(COMMA);
                float[] shapePoints = new float[values.length];
                for (int i = 0; i < values.length; i++) {
                    shapePoints[i] = Float.parseFloat(values[i]);
                }
                IMaskedImageViewer imageViewer = recoverImageContext();
                if (imageViewer != null) {
                    IJRoiManager roiManager = imageViewer.getRoiManager();
                    if (roiManager != null) {
                        roiManager.setRoi(new ShapeRoi(shapePoints));
                    }
                }
            }
        } catch (Exception ex) {
            getLogger().error("Failed to make shape", ex);
        }
    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }

}
