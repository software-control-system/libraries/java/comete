/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.plugin;

import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.image.util.ij.roi.CircleRoi;
import fr.soleil.comete.swing.util.CometeConstants;
import ij.process.ImageProcessor;

/**
 * {@link CometePlugInFilter} used to make {@link CircleRoi}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MakeCirclePlugin extends CometePlugInFilter implements CometeConstants {

    public final static String COMMAND_NAME = "makeCircle";

    @Override
    public void run(ImageProcessor ip) {
        try {
            if (argument != null) {
                String arg = argument;
                int openIndex = arg.indexOf('(');
                int closeIndex = arg.lastIndexOf(')');
                String[] values;
                if ((openIndex > -1) && (closeIndex > -1) && (closeIndex > openIndex)) {
                    arg = arg.substring(openIndex + 1, closeIndex).trim();
                }
                values = arg.split(COMMA);
                if (values.length > 3) {
                    double centerX = Double.parseDouble(values[0]);
                    double centerZ = Double.parseDouble(values[1]);
                    float radius = Float.parseFloat(values[2]);
                    StringBuilder radiusBuilder = new StringBuilder(values[3]);
                    for (int i = 4; i < values.length; i++) {
                        radiusBuilder.append(',').append(values[i]);
                    }
                    IMaskedImageViewer imageViewer = recoverImageContext();
                    if (imageViewer != null) {
                        IJRoiManager roiManager = imageViewer.getRoiManager();
                        if (roiManager != null) {
                            roiManager.setRoi(new CircleRoi(centerX, centerZ, radius, radiusBuilder.toString()));
                        }
                    }
                }
            }
        } catch (Exception ex) {
            getLogger().error("Failed to make circle", ex);
        }
    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }

}
