/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import fr.soleil.comete.definition.listener.IPeriodicTableListener;
import fr.soleil.comete.definition.widget.IStringPeriodicTable;
import fr.soleil.comete.swing.periodictable.ElementButton;
import fr.soleil.comete.swing.periodictable.ElementModel;
import fr.soleil.comete.swing.periodictable.PeriodicTable;

public class StringPeriodicTable extends PeriodicTable
        implements IStringPeriodicTable, fr.soleil.comete.swing.periodictable.IPeriodicTableListener {

    private static final long serialVersionUID = 2994614504549814849L;
    private final List<IPeriodicTableListener<String>> listeners = new ArrayList<IPeriodicTableListener<String>>();

    public StringPeriodicTable() {
        addPeriodicTableListener(this);
    }

    @Override
    public void addPeriodicTableListener(IPeriodicTableListener<String> listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }

    }

    @Override
    public void fireValueChanged(String value) {
        ListIterator<IPeriodicTableListener<String>> iterator = listeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().valueChanged(value);
        }
    }

    @Override
    public void removePeriodicTableListener(IPeriodicTableListener<String> listener) {
        if (listeners.contains(listener)) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    @Override
    public void selectedAtomicNumber(String atomicNumber) {
        ListIterator<IPeriodicTableListener<String>> iterator = listeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().selectedAtomicNumber(atomicNumber);
        }
    }

    @Override
    public void selectedName(String name) {
        ListIterator<IPeriodicTableListener<String>> iterator = listeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().selectedName(name);
        }
    }

    @Override
    public void selectedSymbole(String symbole) {
        ListIterator<IPeriodicTableListener<String>> iterator = listeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().selectedSymbole(symbole);
        }
    }

    @Override
    public void selectedValueChanged(String value) {
        fireValueChanged(value);
    }

    @Override
    public String getText() {
        ElementButton elementButton = getSelectedElement();

        String result = null;
        if (elementButton != null) {
            int theMode = getMode();
            ElementModel model = elementButton.getElement();
            if (model != null) {
                switch (theMode) {
                    case PeriodicTable.ATOMIC_NUMBER_MODE:
                        result = model.getAtomicNumber();
                        break;
                    case PeriodicTable.SYMBOL_MODE:
                        result = model.getSymbole();
                        break;
                    case PeriodicTable.NAME_MODE:
                        result = model.getElementName();
                        break;
                }
            }
        }
        return result;
    }

    @Override
    public void setText(String text) {
        super.setValue(text);
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    @Override
    public void setEditable(boolean editable) {
        // Nothing to do: no button is editable
    }

}
