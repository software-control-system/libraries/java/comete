/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.axis.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.Map;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.swing.chart.axis.model.AxisAttributes;
import fr.soleil.comete.swing.chart.axis.model.AxisLabel;
import fr.soleil.comete.swing.chart.axis.scale.AbstractAxisScale.TickType;
import fr.soleil.lib.project.awt.ColorUtils;

/**
 * {@link AbstractAxisView} for horizontal axis
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class HorizontalAxisView extends AbstractAxisView {

    public HorizontalAxisView(AxisAttributes attributes, int tickAlignment, int labelAlignment) {
        super(attributes, tickAlignment, labelAlignment);
    }

    @Override
    protected Dimension computeAxisSize(FontRenderContext frc, int desiredLength, int maxLabelWidth,
            int maxLabelHeight) {
        return new Dimension(desiredLength, maxLabelHeight + getLabelHeight(frc));
    }

    @Override
    public int getThickness() {
        return getAxisHeight();
    }

    @Override
    public int getLength() {
        return getAxisWidth();
    }

    @Override
    public void applyTitleBounds(FontRenderContext frc, int x, int y, Rectangle bounds) {
        if (bounds != null) {
            Font labelFont = attributes.getLabelFont();
            if ((attributes.getTitle() == null) || attributes.getTitle().isEmpty() || (frc == null)
                    || (labelFont == null)) {
                bounds.x = 0;
                bounds.y = 0;
                bounds.width = 0;
                bounds.height = 0;
            } else {
                Rectangle2D titleBounds = labelFont.getStringBounds(attributes.getTitle(), frc);
                int titleFontAscent = (int) (labelFont.getLineMetrics(attributes.getTitle(), frc).getAscent() + 0.5f);
                // title coordinates
                int titleX, titleY;
                switch (attributes.getTitleAlignment()) {
                    case IComponent.LEFT:
                        titleX = x;
                        break;
                    case IComponent.RIGHT:
                        titleX = x + getLength() - (int) titleBounds.getWidth();
                        break;
                    // center aligned by default
                    case IComponent.CENTER:
                    default:
                        titleX = x + (getLength() - (int) titleBounds.getWidth()) / 2;
                        break;
                }
                if (titlePosition == TOP) {
                    titleY = y - (int) titleBounds.getHeight() - 2;
                } else {
                    titleY = y + (int) titleBounds.getHeight() + titleFontAscent;
                }
                bounds.x = titleX;
                bounds.y = titleY;
                bounds.width = (int) Math.round(titleBounds.getWidth());
                bounds.height = (int) Math.round(titleBounds.getHeight());
            }
        }
    }

    @Override
    public void paintAxis(Graphics2D g, FontRenderContext frc, Color back, int x, int y, int yOpposite,
            int oppositeTickAlignment, boolean drawOpposite, int gridMin, int gridMax) {
        Font labelFont = attributes.getLabelFont();
        Color subTickColor;
        if (back == null) {
            subTickColor = axisColor;
        } else {
            subTickColor = ColorUtils.blend(axisColor, back, 0.25);
        }
        int[] ticksAlignment = computeTicksY(y, tickAlignment);
        int[] oppositeTicksAlignment = computeTicksY(yOpposite, oppositeTickAlignment);
        int tickY = ticksAlignment[0], subTickY = ticksAlignment[1];
        int oppositeTickY = oppositeTicksAlignment[0], oppositeSubTickY = oppositeTicksAlignment[1];
        AxisLabel[] labels = scale.getLabels();
        int minLabelY = Math.min(y, tickY) - 2;
        int maxLabelY = Math.max(y, tickY + attributes.getTickLength()) + 2;
        if (visible && (labels != null)) {
            Color labelColor = attributes.getLabelColor();
            if (labelColor == null) {
                labelColor = Color.BLACK;
            }
            g.setColor(labelColor);
            g.setFont(labelFont);
            int width = getLength();
            // Draw labels
            for (AxisLabel label : labels) {
                int labelX = (int) Math.rint(label.getPos()) + x + label.getX() - label.getWidth() / 2;

                // JAVAAPI-520: label position correction, to make sure label is always readable.
                Rectangle2D bounds = g.getFont().getStringBounds(label.getValue(), g.getFontRenderContext());
                Rectangle clip = g.getClipBounds();
                // Center last label, but ensure its display according to current clip
                int labelMaxX = (int) Math.round(labelX + bounds.getWidth() / 2 - x);
                labelMaxX = Math.min(labelMaxX, (int) Math.floor(clip.width - bounds.getWidth()));
                // Center first label, but ensure its display according to current clip
                int minX = (int) Math.round(x - bounds.getWidth() / 2);
                minX = Math.max(minX, clip.x);
                if (labelMaxX > width) {
                    labelX -= labelMaxX - width;
                } else if (labelX < minX) {
                    labelX = minX;
                }

                int labelY;
                if (labelAlignment == TOP) {
                    labelY = minLabelY;
                } else {
                    // Bottom label alignment by default
                    labelY = maxLabelY + label.getTotalHeight();
                }
                g.drawString(label.getValue(), labelX, labelY);
            }
        }
        Map<TickType, int[]> tickMap = scale.computeTicksPositions(x, getLength());
        int[] ticks = tickMap.get(TickType.TICK);
        int[] subTicks = tickMap.get(TickType.SUB_TICK);
        if (attributes.getGridStroke() != null) {
            // Draw grid
            g.setStroke(attributes.getGridStroke());
            if ((subTicks != null) && attributes.isSubGridVisible()) {
                g.setColor(subTickColor);
                for (int subTick : subTicks) {
                    g.drawLine(subTick, gridMin, subTick, gridMax);
                }
            }
            if ((ticks != null) && attributes.isGridVisible()) {
                g.setColor(axisColor);
                for (int tick : ticks) {
                    g.drawLine(tick, gridMin, tick, gridMax);
                }
            }
        }
        g.setStroke(DEFAULT_AXIS_STROKE);
        if ((subTicks != null) && attributes.isSubTickVisible()) {
            // Draw sub-ticks
            g.setColor(subTickColor);
            for (int subTick : subTicks) {
                if (visible) {
                    g.drawLine(subTick, subTickY, subTick, subTickY + attributes.getSubtickLength());
                }
                if (drawOpposite) {
                    g.drawLine(subTick, oppositeSubTickY, subTick, oppositeSubTickY + attributes.getSubtickLength());
                }
            }
        }
        if (ticks != null) {
            // Draw ticks
            g.setColor(axisColor);
            for (int tick : ticks) {
                if (visible) {
                    g.drawLine(tick, tickY, tick, tickY + attributes.getTickLength());
                }
                if (drawOpposite) {
                    g.drawLine(tick, oppositeTickY, tick, oppositeTickY + attributes.getTickLength());
                }
            }
        }
        // Draw Axe
        g.setColor(axisColor);
        if (drawOpposite) {
            g.drawLine(x, yOpposite, x + getLength(), yOpposite);
        }
        if (visible) {
            g.drawLine(x, y, x + getLength(), y);
            if ((attributes.getTitle() != null) && (!attributes.getTitle().isEmpty())) {
                applyTitleBounds(frc, x, y, titleBounds);
                g.drawString(attributes.getTitle(), titleBounds.x, titleBounds.y);
            }
        }
    }

    /**
     * Computes ticks and sub-ticks y coordinate, depending on expected tick horizontal alignment
     * and axes y coordinate
     * 
     * @param y The axes y coordinate
     * @param alignment The expected tick horizontal alignment
     * @return An <code>int[]</code>: <code>{ tickY, subTickY }</code>
     */
    protected int[] computeTicksY(int y, int alignment) {
        int tickY, subTickY;
        switch (alignment) {
            case CENTER:
                tickY = y - attributes.getTickLength() / 2;
                subTickY = y - attributes.getSubtickLength() / 2;
                break;
            case BOTTOM:
                tickY = y;
                subTickY = y;
                break;
            // Top aligned by default
            case TOP:
            default:
                tickY = y - attributes.getTickLength();
                subTickY = y - attributes.getSubtickLength();
                break;
        }
        return new int[] { tickY, subTickY };
    }

    @Override
    public int[] computeLabelOffsetFromTickAlignment() {
        int offX = 0;
        int offY = (attributes.getTickLength() < 0) ? -attributes.getTickLength() : 0;
        int[] offset = new int[2];
        offset[IChartViewer.X_INDEX] = offX;
        offset[IChartViewer.Y_INDEX] = offY;
        return offset;
    }

}
