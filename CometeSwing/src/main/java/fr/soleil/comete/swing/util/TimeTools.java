/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.util.ArrayList;

import fr.soleil.lib.project.date.IDateConstants;

public class TimeTools {

    public static final java.util.GregorianCalendar calendar = new java.util.GregorianCalendar();
    public static final java.text.SimpleDateFormat genFormat = new java.text.SimpleDateFormat("dd/MM/yy HH:mm:ss.SSS");
    public static final java.text.SimpleDateFormat yearFormat = new java.text.SimpleDateFormat(
            IDateConstants.YEAR_LONG_FORMAT); // yyyy
    public static final java.text.SimpleDateFormat monthFormat = new java.text.SimpleDateFormat(
            IDateConstants.MONTH_VERY_LONG_FORMAT + IDateConstants.SECOND_FORMAT + IDateConstants.YEAR_SHORT_FORMAT); // MMMMM
                                                                                                                      // yy
    public static final java.text.SimpleDateFormat weekFormat = new java.text.SimpleDateFormat(
            IDateConstants.DATE_WEEK); // dd/MM/yy
    public static final java.text.SimpleDateFormat dayFormat = new java.text.SimpleDateFormat(IDateConstants.DATE_DAY); // EEE
                                                                                                                        // dd
    public static final java.text.SimpleDateFormat hour12Format = new java.text.SimpleDateFormat(
            IDateConstants.DAY_WEEK_FORMAT + IDateConstants.SPACE_SEPARATOR + IDateConstants.HOUR_MINUTE_FORMAT); // EEE
                                                                                                                  // HH:mm
    public static final java.text.SimpleDateFormat hourFormat = new java.text.SimpleDateFormat(
            IDateConstants.HOUR_MINUTE_FORMAT); // HH:mm
    public static final java.text.SimpleDateFormat secFormat = new java.text.SimpleDateFormat(
            IDateConstants.TIME_SHORT_FORMAT); // HH:mm:ss

    public static final double YEAR = 31536000000.0;
    public static final double MONTH = 2592000000.0;
    public static final double DAY = 86400000.0;
    public static final double HOUR = 3600000.0;
    public static final double MINUTE = 60000.0;
    public static final double SECOND = 1000.0;

    public static final ArrayList<TimePair> timeScalePrecision = new ArrayList<TimePair>();

    public static void initTimeScalePrecision() {

        timeScalePrecision.add(new TimePair(1 * SECOND, secFormat));
        timeScalePrecision.add(new TimePair(5 * SECOND, secFormat));
        timeScalePrecision.add(new TimePair(10 * SECOND, secFormat));
        timeScalePrecision.add(new TimePair(30 * SECOND, secFormat));
        timeScalePrecision.add(new TimePair(1 * MINUTE, secFormat));
        timeScalePrecision.add(new TimePair(5 * MINUTE, secFormat));
        timeScalePrecision.add(new TimePair(10 * MINUTE, secFormat));
        timeScalePrecision.add(new TimePair(30 * MINUTE, hourFormat));
        timeScalePrecision.add(new TimePair(1 * HOUR, hourFormat));
        timeScalePrecision.add(new TimePair(3 * HOUR, hourFormat));
        timeScalePrecision.add(new TimePair(6 * HOUR, hourFormat));
        timeScalePrecision.add(new TimePair(12 * HOUR, hour12Format));
        timeScalePrecision.add(new TimePair(1 * DAY, dayFormat));
        timeScalePrecision.add(new TimePair(7 * DAY, weekFormat));
        timeScalePrecision.add(new TimePair(1 * MONTH, monthFormat));
        timeScalePrecision.add(new TimePair(1 * YEAR, yearFormat));
        timeScalePrecision.add(new TimePair(5 * YEAR, yearFormat));
        timeScalePrecision.add(new TimePair(10 * YEAR, yearFormat));

    }

}
