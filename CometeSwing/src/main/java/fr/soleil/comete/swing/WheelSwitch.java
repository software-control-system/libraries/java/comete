/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import fr.soleil.comete.definition.data.information.NumberInformation;
import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.event.WheelSwitchEvent;
import fr.soleil.comete.definition.listener.IWheelSwitchListener;
import fr.soleil.comete.definition.widget.IWheelSwitch;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.ColorUtils;
import fr.soleil.lib.project.awt.FontUtils;
import fr.soleil.lib.project.awt.IBestFontSizeCalculator;
import fr.soleil.lib.project.awt.layout.VerticalFlowLayout;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.swing.ArrowButton;
import fr.soleil.lib.project.swing.listener.DynamicFontSizeManager;

/**
 * Swing implementation of {@link IWheelSwitch}. This is a {@link JComponent} that can edit a number value through arrow
 * buttons around each digit.
 */
public class WheelSwitch extends AbstractPanel
        implements IWheelSwitch, IBestFontSizeCalculator, PropertyChangeListener {

    private static final long serialVersionUID = 4511179785890395045L;

    protected static final String TEXT = "text";

    protected static final int UP_INDEX = 0;
    protected static final int DOWN_INDEX = 1;
    protected static final Font DEFAULT_FONT = new Font("Lucida Bright", Font.BOLD, 15);
    protected static final Color DEFAULT_SELECTION_COLOR = new Color(156, 154, 206);
    protected static final Color DEFAULT_BUTTON_BACKGROUND_COLOR = new Color(200, 200, 200);
    protected static final double DEFAULT_VALUE = Double.NaN;

    protected static final String NAN = "nan";
    protected static final String INFINITY = "infinity";
    protected static final String INFINITY2 = "inf";
    protected static final String INFINITY3 = "\u221E";
    protected static final String POSITIVE_INFINITY = "+infinity";
    protected static final String POSITIVE_INFINITY2 = "+inf";
    protected static final String POSITIVE_INFINITY3 = "+\u221E";
    protected static final String NEGATIVE_INFINITY = "-infinity";
    protected static final String NEGATIVE_INFINITY2 = "-inf";
    protected static final String NEGATIVE_INFINITY3 = "-\u221E";
    protected static final String ZERO = "0";
    protected static final String PLUS = "+";
    protected static final String MINUS = "-";
    protected static final String X = "X";
    protected static final String[] CHAR_SET = { ZERO, "1", "2", "3", "4", "5", "6", "7", "8", "9", "E", PLUS, MINUS,
            " ", INFINITY3, X };
    protected static final String REF = "+-.0123456789EX";
    protected static final String CHANGE_FORMAT = "Change format:";
    protected static final String WHEELSWITCH_ERROR_BOUND_VALUE = "Wheelswitch.Error.boundValue";
    protected static final String WHEELSWITCH_ERROR_BOUND_INCLUDED = "Wheelswitch.Error.bound.included";
    protected static final String WHEELSWITCH_ERROR_BOUND_EXCLUDED = "Wheelswitch.Error.bound.excluded";
    protected static final String WRONG_VALUE = "Wrong value";
    protected static final String PERCENT = "%";
    protected static final String PERCENT_PLUS = "%+";
    protected static final String FORMAT = "format";
    protected static final String NUMBER_VALUES = "[0-9]";

    /**
     * The default format to use
     */
    public static final String DEFAULT_FORMAT = "%+7.2f";
    /**
     * The default format to use when plus/minus arrows are not allowed
     */
    public static final String DEFAULT_FORMAT_NO_PLUS = "%7.2f";
    public static final ResourceBundle MESSAGES = ResourceBundle.getBundle("fr.soleil.comete.swing.util.messages");

    protected final AdaptedSpaceLabel theLabel; // contains the digit to display
    protected double value; // Current value
    protected double maxValue; // Maximum value
    protected double minValue; // Minimum value
    protected boolean minIncluded;
    protected boolean maxIncluded;
    protected boolean nanAllowed;
    protected boolean infinityAllowed;
    protected boolean editable;
    protected int xMargin;
    protected int yMargin;
    protected String format;
    protected int formatWidth;
    protected String formatedValue;
    protected final Collection<IWheelSwitchListener> listeners;

    // Arrow buttons
    protected ArrowButton[][] buttons;
    protected volatile int currentSelButton;
    protected volatile int nbButton;
    protected Color buttonBackgroundColor, buttonSelectionColor;
    protected boolean inversionLogic;
    protected boolean tenMultipleOnly;
    protected boolean lockBackgroundColor;
    protected boolean displayButtonsWithErrorValues;
    protected boolean usePlusMinusArrows;

    // dimension
    // Preferred dimension to display the wheelswitch
    protected Dimension prefDim;
    protected int buttonSize;
    protected int digitY;
    protected int digitHeight;

    // value edited
    protected boolean editMode;
    protected String editedValue;
    protected final KeyListener keyListener;
    protected final MouseListener arrowListener;
    protected final FocusListener focusListener;
    protected boolean applyValueOnFocusLost;

    // popup menu and format edition
    protected boolean formatEditable;
    protected JPopupMenu popupMenu;
    protected final JPanel formatPanel;
    protected final JLabel formatLabel;
    protected final JTextField formatField;
    protected final FormatUpdater formatUpdater;
    protected final ErrorNotificationDelegate errorNotificationDelegate;

    public WheelSwitch() {
        super();
        super.setFont(DEFAULT_FONT);
        setLayout(new FlowLayout(CENTER, 0, 0));
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        maxValue = Double.POSITIVE_INFINITY;
        minValue = Double.NEGATIVE_INFINITY;
        editable = true;
        xMargin = 5;
        yMargin = 5;
        usePlusMinusArrows = true;
        format = DEFAULT_FORMAT;
        formatWidth = 5;
        formatedValue = ObjectUtils.EMPTY_STRING;
        listeners = Collections.newSetFromMap(new ConcurrentHashMap<IWheelSwitchListener, Boolean>());
        buttons = new ArrowButton[2][0];
        currentSelButton = 0;
        nbButton = 0;
        buttonBackgroundColor = DEFAULT_BUTTON_BACKGROUND_COLOR;
        inversionLogic = true;
        tenMultipleOnly = true;
        lockBackgroundColor = true;
        displayButtonsWithErrorValues = false;
        prefDim = new Dimension(100, 100);
        buttonSize = 0;
        digitY = 0;
        digitHeight = 0;
        editMode = false;
        editedValue = ObjectUtils.EMPTY_STRING;
        applyValueOnFocusLost = false;
        keyListener = new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                processKey(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        };
        arrowListener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if ((evt != null) && (evt.getSource() instanceof ArrowButton)) {
                    clickArrow((ArrowButton) evt.getSource());
                }
            }
        };
        focusListener = new FocusAdapter() {

            @Override
            public void focusLost(FocusEvent e) {
                if (editMode) {
                    if (isApplyValueOnFocusLost()) {
                        applyValue(editedValue);
                    } else {
                        cancelEdition();
                    }
                }
            }

        };
        addFocusListener(focusListener);

        setLayout(null);
        setOpaque(true);
        theLabel = new AdaptedSpaceLabel();
        theLabel.setFont(DEFAULT_FONT);
        theLabel.setHorizontalAlignment(SwingConstants.LEFT);
        theLabel.addPropertyChangeListener(this);
        add(theLabel);

        // order is important here, setValue needs his listenersList
        initListeners();
        setValue(DEFAULT_VALUE, true, false);

        formatEditable = false;

        formatField = new JTextField(10);
        formatField.setText(getFormat());
        formatField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                WheelSwitch.this.getComponentPopupMenu().setVisible(false);
                setFormat(formatField.getText().trim());
            }
        });
        formatLabel = new JLabel(CHANGE_FORMAT);
        formatPanel = new JPanel(new BorderLayout(5, 5));
        formatPanel.add(formatLabel, BorderLayout.WEST);
        formatPanel.add(formatField, BorderLayout.CENTER);
        formatUpdater = new FormatUpdater();
        popupMenu = new JPopupMenu();
    }

    /**
     * Returns whether format edition through right click is enabled
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isFormatEditable() {
        return formatEditable;
    }

    /**
     * Enables or disables format edition through right click
     * 
     * @param formatEditable Whether to enable format edition
     */
    public void setFormatEditable(boolean formatEditable) {
        if (this.formatEditable != formatEditable) {
            this.formatEditable = formatEditable;
            updatePopupMenuForFormat();
        }
    }

    /**
     * Returns whether characters will all be drawn with the same width (or using the width calculated by the
     * {@link Font})
     * 
     * @return A <code>boolean</code>. <code>false</code> by default
     */
    public boolean isMonospaced() {
        return theLabel.isMonospaced();
    }

    /**
     * Sets whether characters should all be drawn with the same width (or using the width calculated by the
     * {@link Font})
     * 
     * @param monospaced Whether characters should all be drawn with the same width.
     *            <ul>
     *            <li>If <code>true</code>, all characters will use the same width and the arrow buttons will be adapted
     *            to this width</li>
     *            <li>If <code>false</code>, each character will use the width calculated by the {@link Font}, and the
     *            arrow buttons will try to adapt to the characters' width</li>
     *            </ul>
     */
    public void setMonospaced(boolean monospaced) {
        if (monospaced != isMonospaced()) {
            theLabel.setMonospaced(monospaced);
            computeSize();
            if (nbButton > 0) {
                placeButtons();
            }
            repaint();
        }
    }

    /**
     * Updates current popup menu according to {@link #isFormatEditable()}
     */
    protected void updatePopupMenuForFormat() {
        JPopupMenu currentPopupMenu = getComponentPopupMenu();
        if (formatEditable) {
            if (currentPopupMenu == null) {
                setComponentPopupMenu(popupMenu, false);
                currentPopupMenu = popupMenu;
            }
            if (!hasFormatPanel(currentPopupMenu)) {
                currentPopupMenu.add(formatPanel);
            }
        } else if (hasFormatPanel(currentPopupMenu)) {
            if (currentPopupMenu.getComponentCount() == 1) {
                setComponentPopupMenu(null, false);
                popupMenu = currentPopupMenu;
            } else {
                currentPopupMenu.remove(formatPanel);
            }
        }
    }

    @Override
    public void setComponentPopupMenu(JPopupMenu popup) {
        setComponentPopupMenu(popup, true);
    }

    protected void setComponentPopupMenu(JPopupMenu popup, boolean updateForFormat) {
        JPopupMenu currentPopupMenu = getComponentPopupMenu();
        if (currentPopupMenu != popup) {
            if (currentPopupMenu != null) {
                currentPopupMenu.removePopupMenuListener(formatUpdater);
            }
            super.setComponentPopupMenu(popup);
            if (popup != null) {
                popup.addPopupMenuListener(formatUpdater);
            }
        }
    }

    protected boolean hasFormatPanel(JPopupMenu menu) {
        boolean hasPanel = false;
        if (menu != null) {
            for (Component comp : menu.getComponents()) {
                if (comp == formatPanel) {
                    hasPanel = true;
                    break;
                }
            }
        }
        return hasPanel;
    }

    @Override
    public void setCometeFont(CometeFont font) {
        super.setCometeFont(font);
        theLabel.setFont(getFont());
    }

    protected void initListeners() {
        addKeyListener(keyListener);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                grabFocus();
            }
        });

        addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                repaint();
            }

            @Override
            public void focusLost(FocusEvent e) {
                repaint();
            }
        });
    }

    /**
     * If the value is NaN, only X will be displayed. If the value is not
     * between min and max value a message will be displayed.
     * 
     * @param val
     *            value to display.
     * @param needFormat
     *            whether associated string value needs to be formatted
     * @param notifyMediators
     *            whether to notify mediators for changes
     */
    public void setValue(double val, boolean needFormat, boolean notifyMediators) {
        boolean canNotify = false;
        Number numberValue = null;
        String text = null;
        double minValue = this.minValue;
        double maxValue = this.maxValue;
        boolean minIncluded = this.minIncluded;
        boolean maxIncluded = this.maxIncluded;
        if (isValidValue(val, minValue, minIncluded, maxValue, maxIncluded)) {
            value = val;
            applyChange(needFormat);
            canNotify = notifyMediators;
            numberValue = getNumberValue();
            text = getText();
        } else {
            // use string.valueOf to avoid .0000
            String min = String.valueOf(minValue);
            String max = String.valueOf(maxValue);
            JOptionPane.showMessageDialog(this,
                    String.format(MESSAGES.getString(WHEELSWITCH_ERROR_BOUND_VALUE), min,
                            getInclusionMessage(minIncluded), max, getInclusionMessage(maxIncluded)),
                    WRONG_VALUE, JOptionPane.ERROR_MESSAGE);
            // go back to previous value
            applyChange(false);
        }
        if (canNotify) {
            warnMediators(numberValue, text);
            // fireValueChange();
        }
    }

    public double getValue() {
        return value;
    }

    @Override
    public Number getNumberValue() {
        return value;
    }

    @Override
    public String getText() {
        return formatedValue;
    }

    protected boolean isOkAtMin(double value) {
        return ((value == minValue) && (minIncluded || (minValue == Double.NEGATIVE_INFINITY)));
    }

    protected boolean isOkAtMax(double value) {
        return ((value == maxValue) && (maxIncluded || (maxValue == Double.POSITIVE_INFINITY)));
    }

    protected boolean isInBounds(double value) {
        boolean ok;
        if (value > minValue) {
            if (value < maxValue) {
                ok = true;
            } else if (isOkAtMax(value)) {
                ok = true;
            } else {
                ok = false;
            }
        } else if (isOkAtMin(value)) {
            if (value < maxValue) {
                ok = true;
            } else if (isOkAtMax(value)) {
                ok = true;
            } else {
                ok = false;
            }
        } else {
            ok = false;
        }
        return ok;
    }

    @Override
    public void setText(String text) {
        double tempValue;
        if (text == null) {
            tempValue = MathConst.NAN_FOR_NULL;
        } else {
            try {
                tempValue = Double.parseDouble(text);
            } catch (Exception e) {
                tempValue = Double.NaN;
            }
        }
        if ((Double.isNaN(tempValue)) || isInBounds(tempValue)) {
            setValue(tempValue, true, false);
        }
    }

    /**
     * Sets this {@link WheelSwitch}'s format.
     * <ul>
     * <li>If the format is <code>null</code> or empty, the default format will be used</li>
     * <li>If the format is not correct, the former format will be used</li>
     * </ul>
     * 
     * @param format The format to use
     * @see Format#isNumberFormatOK(String)
     * @see #DEFAULT_FORMAT
     * @see #DEFAULT_FORMAT_NO_PLUS
     */
    @Override
    public void setFormat(String format) {
        if ((format == null) || (format.trim().isEmpty())) {
            doSetFormat(DEFAULT_FORMAT);
        } else if (Format.isNumberFormatOK(format)) {
            doSetFormat(format);
        }
    }

    /**
     * Updates this {@link WheelSwitch}'s format, one checked through {@link #setFormat(String)}
     * 
     * @param format The format to use
     */
    protected void doSetFormat(String format) {
        String formatToUse = format == null ? DEFAULT_FORMAT : format.toLowerCase();
        // we always want to show the sign
        boolean hasPlus = formatToUse.contains(PLUS);
        if (!hasPlus) {
            formatToUse = formatToUse.replaceFirst(PERCENT, PERCENT_PLUS);
        }
        if (!formatToUse.equals(this.format)) {
            String savedFormat = this.format;
            formatWidth = Format.getWidth(formatToUse);
            this.format = formatToUse;
            applyChange(true);
            firePropertyChange(FORMAT, savedFormat, formatToUse);
        }
    }

    @Override
    public String getFormat() {
        return format;
    }

    /**
     * Overrides setFont of JComponent because we need this font to compute wheelswitch size and we have to set it to
     * the label
     * 
     * @param font
     */
    @Override
    public void setFont(Font f) {
        super.setFont(f);
        if (theLabel != null) {
            theLabel.setFont(f);
            computeSize();

            if (nbButton > 0) {
                placeButtons();
            }
            repaint();
        }
    }

    /**
     * overrides setBorder of JComponent to be able to restore the former border
     * when the focus is lost.
     * 
     * @param border
     */
    @Override
    public void setBorder(Border border) {
        super.setBorder(border);
        if (theLabel != null) {
            computeSize();
            if (nbButton > 0) {
                placeButtons();
            }
            repaint();
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (hasFocus()) {
            // draw focus border (blue rectangle)
            int x = 0, y = 0, width = getWidth() - 1, height = getHeight() - 1;
            Border border = getBorder();
            if (border != null) {
                Insets insets = border.getBorderInsets(this);
                if (insets != null) {
                    if (insets.left > 0) {
                        x += insets.left;
                        width -= insets.left;
                    }
                    if (insets.right > 0) {
                        width -= insets.right;
                    }
                    if (insets.top > 0) {
                        y += insets.top;
                        height -= insets.top;
                    }
                    if (insets.bottom > 0) {
                        height -= insets.bottom;
                    }
                }
            }
            if (width < 3) {
                width = 3;
            }
            if (height < 3) {
                height = 3;
            }
            g.setColor(Color.BLUE);
            g.drawRect(x, y, width, height);
            g.drawRect(x + 1, y + 1, width - 2, height - 2);
        }

    }

    /**
     * Set if we can enter a number with keyboard
     * 
     * @param editable
     */
    @Override
    public void setEditable(boolean editable) {
        if (editable != this.editable) {
            this.editable = editable;
            setButtonsEnabled(editable && isEnabled());
            if (!editable) {
                cancelEdition();
            }
        }
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (enabled != isEnabled()) {
            setButtonsEnabled(enabled && isEditable());
            super.setEnabled(enabled);
            if (theLabel != null) {
                theLabel.setEnabled(enabled);
            }
            if (!enabled) {
                cancelEdition();
            }
        }
    }

    /**
     * Activates/deactivates the arrow buttons
     * 
     * @param enabled
     *            Whether to activate the buttons
     */
    protected void setButtonsEnabled(boolean enabled) {
        ArrowButton[][] toUpdate = buttons;
        int count = toUpdate[UP_INDEX].length;
        // we enabled the buttons
        for (int i = 0; i < count; i++) {
            toUpdate[UP_INDEX][i].setEnabled(enabled);
            toUpdate[DOWN_INDEX][i].setEnabled(enabled);
        }
        // we set a specific color
        setButtonColor(getBackground());
    }

    /**
     * Transforms the value according to the current format, and stores it to formattedValue
     */
    protected void formatValue() {
        formatedValue = formatValue(value, format);
    }

    /**
     * Transforms the value according to the current format
     * 
     * @param avalue The value
     * @param aformat The format
     * @return A {@link String}
     */
    protected String formatValue(double avalue, String aformat) {
        String formatedValue;
        if (Double.isInfinite(avalue)) {
            if (avalue > 0) {
                formatedValue = POSITIVE_INFINITY3;
            } else {
                formatedValue = NEGATIVE_INFINITY3;
            }
        } else {
            double tmpValue = avalue;
            String str;
            boolean isNaN = false;
            String format = aformat;
            if ((format == null) || format.trim().isEmpty()) {
                format = DEFAULT_FORMAT;
            }
            if (Double.isNaN(tmpValue)) {
                tmpValue = 0;
                isNaN = true;
            }
            try {
                str = Format.format(Locale.US, format, tmpValue);
            } catch (Exception e) {
                String defaultFormat = DEFAULT_FORMAT;
                if (defaultFormat.equals(format)) {
                    str = ObjectUtils.EMPTY_STRING;
                } else {
                    try {
                        str = Format.format(Locale.US, defaultFormat, tmpValue);
                    } catch (Exception e2) {
                        str = ObjectUtils.EMPTY_STRING;
                    }
                }
            }
            if (isNaN) {
                formatedValue = str.replaceAll(NUMBER_VALUES, X);
            } else {
                formatedValue = str;
            }
        }
        return formatedValue;
    }

    protected void applyChange(boolean needFormat) {
        if (needFormat) {
            formatValue();
            checkWidth();
        }
        computeNbButton();
        computeSize();
        prepareButton();
        theLabel.setText(formatedValue);
        repaint();
    }

    /**
     * Adds some "0" to the formated value if the width of the given format is
     * not respected
     */
    protected void checkWidth() {
        double value = this.value;
        if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
            int formatWidth = this.formatWidth;
            String formatedValue = this.formatedValue;
            // we find the number of digit before exp part in our formatedValue
            int nbDigit = 0;
            int cpt = 0;
            boolean expPartFound = false;
            char c;
            if (formatedValue == null) {
                formatedValue = ObjectUtils.EMPTY_STRING;
            }
            while (cpt < formatedValue.length() && !expPartFound) {
                c = formatedValue.toLowerCase().charAt(cpt);
                if (Character.isDigit(c)) {
                    nbDigit++;
                }
                if (c == 'e') {
                    expPartFound = true;
                }
                cpt++;
            }
            // we pad with 0 if there is not enough digit
            if (nbDigit < formatWidth) {
                StringBuilder str;
                // we have to remove and then add the sign
                // the sign has to be in first position
                if (value >= 0) {
                    str = new StringBuilder(PLUS);
                } else {
                    str = new StringBuilder(MINUS);
                }
                // we add the right number of 0
                for (int i = 0; i < (formatWidth - nbDigit); i++) {
                    str.append(ZERO);
                }
                // extract formatedValue without sign
                if (formatedValue.isEmpty()) {
                    str.append(formatedValue);
                } else {
                    c = formatedValue.charAt(0);
                    if ((c == '+') || (c == '-')) {
                        str.append(formatedValue.substring(1));
                    } else {
                        str.append(formatedValue);
                    }
                }
                this.formatedValue = str.toString();
            }
        }
    }

    @Override
    public void setMaxValue(double v) {
        if (!Double.isNaN(v)) {
            maxValue = v;
        }
    }

    @Override
    public void setMinValue(double v) {
        if (!Double.isNaN(v)) {
            minValue = v;
        }
    }

    @Override
    public double getMaxValue() {
        return maxValue;
    }

    @Override
    public double getMinValue() {
        return minValue;
    }

    /**
     * Returns whether minimum value is included.
     * 
     * @return A <code>boolean</code>. <code>false</code> by default.
     */
    public boolean isMinIncluded() {
        return minIncluded;
    }

    /**
     * Sets whether minimum value is included.
     * 
     * @param minIncluded Whether maximum value is included. If included, user can set this wheelswitch's value to its
     *            minimum.
     */
    public void setMinIncluded(boolean minIncluded) {
        this.minIncluded = minIncluded;
    }

    /**
     * Returns whether maximum value is included.
     * 
     * @return A <code>boolean</code>. <code>false</code> by default.
     */
    public boolean isMaxIncluded() {
        return maxIncluded;
    }

    /**
     * Sets whether maximum value is included.
     * 
     * @param maxIncluded Whether maximum value is included. If included, user can set this wheelswitch's value to its
     *            maximum.
     */
    public void setMaxIncluded(boolean maxIncluded) {
        this.maxIncluded = maxIncluded;
    }

    /**
     * Returns whether user can enter a <code>NaN</code> value through the wheelswitch
     * 
     * @return A <code>boolean</code>. <code>false</code> by default.
     */
    public boolean isNanAllowed() {
        return nanAllowed;
    }

    /**
     * Sets whether user can enter a <code>NaN</code> value through the wheelswitch.
     * 
     * @param nanAllowed Whether user can enter a <code>NaN</code> value through the wheelswitch. To enter a
     *            <code>NaN</code> value, user can either type "nan" (case insensitive), or any string containing only
     *            'x' characters (case insensitive).
     */
    public void setNanAllowed(boolean nanAllowed) {
        this.nanAllowed = nanAllowed;
    }

    /**
     * Returns whether user can enter a infinity values through the wheelswitch
     * 
     * @return A <code>boolean</code>. <code>false</code> by default.
     */
    public boolean isInfinityAllowed() {
        return infinityAllowed;
    }

    /**
     * Sets whether user can enter a infinity values through the wheelswitch
     * 
     * @param infinityAllowed Whether user can enter a infinity values through the wheelswitch. To enter infinity
     *            values, user can type any of these strings (case insensitive):
     *            <ul>
     *            <li>infinity</li>
     *            <li>-infinity</li>
     *            <li>inf</li>
     *            <li>-inf</li>
     *            </ul>
     */
    public void setInfinityAllowed(boolean infinityAllowed) {
        this.infinityAllowed = infinityAllowed;
    }

    protected boolean isUsableErrorCharacter(char c) {
        return (c == 'x' || c == 'X' || c == '\u221E' || c == 'N' || c == 'n' || c == 'A' || c == 'a' || c == 'I'
                || c == 'i' || c == 'F' || c == 'f' || c == 'T' || c == 't' || c == 'Y' || c == 'y');
    }

    protected void computeNbButton() {
        int buttonsNb = 0;
        double value = this.value;
        if (displayButtonsWithErrorValues || ((!Double.isNaN(value)) && (!Double.isInfinite(value)))) {
            String formatedValue = this.formatedValue;
            if (formatedValue != null) {
                for (int i = 0; i < formatedValue.length(); i++) {
                    char c = formatedValue.charAt(i);
                    if (Character.isDigit(c)) {
                        buttonsNb++;
                    } else if (c == '+' || c == '-') {
                        if ((i > 0) || isUsePlusMinusArrows()) {
                            buttonsNb++;
                        }
                    } else if (displayButtonsWithErrorValues && isUsableErrorCharacter(c)) {
                        buttonsNb++;
                    }
                }
            }
        }
        nbButton = buttonsNb;
    }

    protected void prepareButton() {
        // create buttons if need be
        // buttons_down and buttons_up have the same size
        ArrowButton[][] toCheck = buttons;
        int buttonCount = nbButton;
        if (toCheck[UP_INDEX].length != buttonCount) {
            // clean old buttons
            synchronized (this) {
                cleanButtons(toCheck[UP_INDEX]);
                cleanButtons(toCheck[DOWN_INDEX]);
                toCheck = new ArrowButton[2][buttonCount];
                // Remove all but theLabel
                List<Component> toRemove = new ArrayList<Component>();
                for (Component comp : getComponents()) {
                    if (comp != theLabel) {
                        toRemove.add(comp);
                    }
                }
                for (Component comp : toRemove) {
                    remove(comp);
                }
                toRemove.clear();
                // Add buttons
                for (int i = 0; i < buttonCount; i++) {
                    ArrowButton buttonDown = generateArrowButton(true);
                    add(buttonDown);
                    toCheck[DOWN_INDEX][i] = buttonDown;

                    ArrowButton buttonUp = generateArrowButton(false);
                    add(buttonUp);
                    toCheck[UP_INDEX][i] = buttonUp;
                }
            }
            buttons = toCheck;
        }
        updateButtonColor();
        // handle buttons position
        if (buttonCount > 0 && !editMode) {
            placeButtons();
        }
    }

    protected void cleanButtons(ArrowButton[] buttons) {
        if (buttons != null) {
            for (ArrowButton button : buttons) {
                if (button != null) {
                    remove(button);
                    button.removeKeyListener(keyListener);
                    button.removeMouseListener(arrowListener);
                }
            }
        }
    }

    protected ArrowButton generateArrowButton(boolean down) {
        ArrowButton arrowButton = new ArrowButton(down ? ArrowButton.DOWN : ArrowButton.UP);
        arrowButton.addMouseListener(arrowListener);
        arrowButton.setEnabled(isEnabled() && isEditable());
        arrowButton.addKeyListener(keyListener);
        return arrowButton;
    }

    // what we do when user clicked on an arrow button
    protected void clickArrow(ArrowButton button) {
        // Jira JAVAAPI-303: test whether button is enabled on clickArrow
        if ((button != null) && button.isEnabled()) {
            boolean up = (button.getOrientation() == ArrowButton.UP);
            int index = up ? UP_INDEX : DOWN_INDEX;
            ArrowButton[] toSearch = buttons[index];
            for (int i = 0; i < toSearch.length; i++) {
                if (ObjectUtils.sameObject(toSearch[i], button)) {
                    grabFocus();
                    currentSelButton = i;
                    computeValue(up);
                    break;
                }
            }
        }
    }

    // if we increase the value "up" will be true
    protected void computeValue(boolean up) {
        // we retrieve the position of the digit in the formated string
        int nbDigitRead = 0;
        int currentSelButton = this.currentSelButton;
        int realPos = currentSelButton;
        int cpt = 0;
        char c;
        char[] digits;
        String formatedValue = this.formatedValue;
        if ((formatedValue == null) || formatedValue.isEmpty()) {
            formatedValue = theLabel.getText();
            this.formatedValue = formatedValue;
        }
        while (nbDigitRead < (currentSelButton + 1)) {
            c = formatedValue.charAt(cpt);
            if (displayButtonsWithErrorValues && isUsableErrorCharacter(c)) {
                nbDigitRead++;
            } else if (!Character.isDigit(c) && c != '+' && c != '-') {
                realPos++;
            } else {
                if ((cpt == 0) && ((c == '-') || (c == '+')) && !isUsePlusMinusArrows()) {
                    realPos++;
                } else {
                    nbDigitRead++;
                }
            }
            cpt++;
        }
        digits = formatedValue.toCharArray();
        String str = ObjectUtils.EMPTY_STRING;
        int digit;
        char charDigit = digits[realPos];

        boolean updateContent = true;

        if (charDigit == '+' || charDigit == '-') {
            digits[realPos] = getInversedSign(charDigit);
        } else if (Character.isDigit(charDigit)) {
            digit = Integer.parseInt(Character.toString(charDigit));
            if (isInvertionLogic()) {
                int idx = currentSelButton;
                int max = idx;
                for (int i = 0; i < max; i++) {
                    if (digits[i] == '+' || digits[i] == '-') {
                        if (i == 0) {
                            if (isUsePlusMinusArrows()) {
                                idx--;
                            } else {
                                max++;
                            }
                        } else {
                            idx--;
                        }
                    } else if (digits[i] == '.' || digits[i] == 'e') {
                        max++;
                    }
                }
                changeValue(idx, up);
                updateContent = false;
            } else {
                if (up) {
                    // if no inversion mode, the up arrow always increase the value
                    digit = getNextDigit(digit);
                } else {
                    // if no inversion mode, the down arrow always decrease the value
                    digit = getPreviousDigit(digit);
                }
            }
            if (updateContent) {
                digits[realPos] = (char) ('0' + digit);
            }
        } else if (displayButtonsWithErrorValues && isUsableErrorCharacter(charDigit)) {
            updateContent = false;
        }
        if (updateContent) {
            str = String.copyValueOf(digits);
            // check if the new value is OK

            double newValue;
            switch (str) {
                case INFINITY:
                case INFINITY2:
                case INFINITY3:
                case POSITIVE_INFINITY:
                case POSITIVE_INFINITY2:
                case POSITIVE_INFINITY3:
                    newValue = Double.POSITIVE_INFINITY;
                    break;
                case NEGATIVE_INFINITY:
                case NEGATIVE_INFINITY2:
                case NEGATIVE_INFINITY3:
                    newValue = Double.NEGATIVE_INFINITY;
                    break;
                default:
                    newValue = Double.parseDouble(str);
                    break;
            }
            double minValue = this.minValue;
            double maxValue = this.maxValue;
            boolean minIncluded = this.minIncluded;
            boolean maxIncluded = this.maxIncluded;
            if (isValidValue(newValue, minValue, minIncluded, maxValue, maxIncluded)) {
                this.formatedValue = str;
                setValue(newValue, false, true);
                fireValueChange();
            } else {
                // use string.valueOf to avoid .0000
                String min = String.valueOf(minValue);
                String max = String.valueOf(maxValue);
                JOptionPane.showMessageDialog(this,
                        String.format(MESSAGES.getString(WHEELSWITCH_ERROR_BOUND_VALUE), min,
                                getInclusionMessage(minIncluded), max, getInclusionMessage(maxIncluded)),
                        WRONG_VALUE, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    protected String getInclusionMessage(boolean included) {
        return included ? MESSAGES.getString(WHEELSWITCH_ERROR_BOUND_INCLUDED)
                : MESSAGES.getString(WHEELSWITCH_ERROR_BOUND_EXCLUDED);
    }

    protected boolean isValidValue(double value, double minValue, boolean minIncluded, double maxValue,
            boolean maxIncluded) {
        boolean valid;
        if ((value > minValue) && (value < maxValue)) {
            valid = true;
        } else if (minIncluded && (value == minValue)) {
            valid = true;
        } else if (maxIncluded && (value == maxValue)) {
            valid = true;
        } else if (Double.isNaN(value)) {
            valid = true;
        } else {
            valid = false;
        }
        return valid;
    }

    protected double getExpPart() {
        double expPart = MathConst.NAN_FOR_NULL;
        String format = this.format;
        int e = (format == null ? -1 : format.indexOf('e'));
        if (e > -1) {
            String displayVal = formatValue(value, format);
            if (displayVal != null) {
                displayVal = displayVal.toLowerCase();
                try {
                    expPart = Double.parseDouble(displayVal.substring(displayVal.indexOf('e') + 1));
                } catch (Exception ex) {
                    expPart = MathConst.NAN_FOR_NULL;
                }
            }
        }
        return expPart;
    }

    protected double getFloatPart() {
        double floatPart = MathConst.NAN_FOR_NULL;
        String format = this.format;
        int e = (format == null ? -1 : format.indexOf('e'));
        if (e < 0) {
            floatPart = value;
        } else {
            String displayVal = formatValue(value, format);
            if (displayVal != null) {
                displayVal = displayVal.toLowerCase();
                try {
                    floatPart = Double.parseDouble(displayVal.substring(0, displayVal.indexOf('e')));
                } catch (Exception ex) {
                    floatPart = MathConst.NAN_FOR_NULL;
                }
            }
        }
        return floatPart;
    }

    /**
     * Increase/decrease value by 10^diz(idx)
     * 
     * @param idx The button index
     * @param up
     */
    protected void changeValue(int idx, boolean up) {
        int intNumber = 0;
        int fracNumber = 0;
        int expNumber = 0;
        int pointIndex = -1;
        int expIndex = -1;
        String toCheck = getText();
        if (toCheck != null) {
            toCheck = toCheck.replace(PLUS, ObjectUtils.EMPTY_STRING).replace(MINUS, ObjectUtils.EMPTY_STRING)
                    .toLowerCase().trim();
            if (!toCheck.isEmpty()) {
                pointIndex = toCheck.indexOf('.');
                expIndex = toCheck.indexOf('e');
                if (expIndex > -1) {
                    expNumber = toCheck.substring(expIndex + 1).trim().length();
                }
                if (pointIndex > -1) {
                    intNumber = toCheck.substring(0, pointIndex).trim().length();
                    fracNumber = toCheck.substring(pointIndex + 1, expIndex > -1 ? expIndex : toCheck.length()).trim()
                            .length();
                } else if (expIndex > -1) {
                    intNumber = toCheck.substring(0, expIndex).trim().length();
                } else {
                    intNumber = toCheck.length();
                }
            }
        }
        if (intNumber < 1) {
            intNumber = 1;
        }
        double[] dval = extractDValues(idx, intNumber, fracNumber, expNumber, up);
        double newValue = near(dval[0], fracNumber) * Math.pow(10, dval[1]);
        if (!tenMultipleOnly) {
            // special case to go, for example, from 02 to -12 instead of from 02 to -08
            double value = this.value;
            if ((value > 0 && newValue < 0) || (value < 0 && newValue > 0)) {
                // don't use this special case for scientific format
                if (expIndex < 0) {
                    this.value = -value;
                    dval = extractDValues(idx, intNumber, fracNumber, expNumber, up);
                    newValue = near(dval[0], fracNumber) * Math.pow(10, dval[1]);
                }
            }
        }
        if (!Double.isNaN(newValue)) {
            if (newValue <= maxValue && newValue >= minValue) {
                setValue(newValue, true, true);
                fireValueChange();
            }
        }
        updateButtonVisibility(intNumber);
    }

    protected double[] extractDValues(int idx, int intNumber, int fracNumber, int expNumber, boolean up) {
        double dval1 = 0;
        double dval2 = 0;
        if (format.indexOf('e') > 0) {
            dval1 = getFloatPart();
            dval2 = getExpPart();
        } else {
            dval1 = value;
        }
        if (idx < intNumber + fracNumber) {
            dval1 = calculateNewValue(dval1, idx, intNumber, up);
        } else {
            int idx2 = idx - intNumber - fracNumber;
            dval2 = calculateNewValue(dval2, idx2, expNumber, up);
        }
        return new double[] { dval1, dval2 };
    }

    protected void updateButtonVisibility(int intNumber) {
        double val = 0;
        // Update button visibility
        if (isEditable() && isEnabled()) {
            // Don't affect button in scientific format
            if (format.indexOf('e') == -1) {
                double minValue = this.minValue;
                double maxValue = this.maxValue;
                double value = this.value;
                ArrowButton[][] toCheck = buttons;
                for (int i = 1; i < toCheck[UP_INDEX].length; i++) {
                    val = calculateNewValue(value, isUsePlusMinusArrows() ? i + 1 : i, intNumber, true);
                    toCheck[UP_INDEX][i].setVisible(val <= maxValue);
                    val = calculateNewValue(value, isUsePlusMinusArrows() ? i + 1 : i, intNumber, false);
                    toCheck[DOWN_INDEX][i].setVisible(val >= minValue);
                }
                val = -val;
                boolean visible = ((val <= maxValue) && (val >= minValue));
                if (toCheck[UP_INDEX].length > 0) {
                    toCheck[UP_INDEX][0].setVisible(visible);
                    toCheck[DOWN_INDEX][0].setVisible(visible);
                }
            }
        }
    }

    protected double calculateNewValue(double val, int idx, int ref, boolean up) {
        double pow10 = Math.pow(10, (ref - idx - 1));
        double newValue = (up ? val + pow10 : val - pow10);
        return newValue;
    }

    // Round according to desired precision (fracNumber)
    protected double near(double d, int fracNumber) {
        double r = Math.pow(10, fracNumber);
        return Math.rint(d * r) / r;
    }

    protected char getInversedSign(char charDigit) {
        char digit = '+';
        if (charDigit == '+') {
            digit = '-';
        }
        return digit;
    }

    public int getNextDigit(int digit) {
        digit = (digit + 1) % 10;
        return digit;
    }

    public int getPreviousDigit(int digit) {
        digit--;
        if ((digit < 0)) {
            digit = 9;
        }
        return digit;
    }

    protected String getEditedValue() {
        return editedValue;
    }

    protected int computeButtonSize(Font font) {
        int buttonSize = 0;
        // compute a button size, - 2 is for a gap between buttons
        if (isMonospaced() && (!editMode)) {
            for (String s : CHAR_SET) {
                int w = FontUtils.measureString(s, font).width + 1;
                // We do want to test for strict equality as we known the exact array content
                if ((s != INFINITY3) && (w > buttonSize)) {
                    buttonSize = w;
                }
            }
            buttonSize -= 2;
        } else {
            buttonSize = theLabel.getFontMetrics(font).stringWidth(ZERO) - 2;
        }
        return buttonSize;
    }

    protected String getDisplayedText() {
        String str;
        if (editMode) {
            str = getEditedValue();
        } else {
            str = getText();
        }
        if (str == null) {
            str = ObjectUtils.EMPTY_STRING;
        }
        return str;
    }

    protected int getDigitHeight(String str, Font font) {
        int digitHeight;
        // during value edition an empty string may happen
        if (str.length() > 0) {
            FontMetrics fm = theLabel.getFontMetrics(font);
            digitHeight = fm.getAscent();
        } else {
            digitHeight = 0;
        }
        return digitHeight;
    }

    protected void computeSize() {
        // we compute the size with what we have to display
        String str = getDisplayedText();

        int prefY = 0;

        digitHeight = getDigitHeight(str, theLabel.getFont());

        if (nbButton > 0) {
            buttonSize = computeButtonSize(getFont());
            digitY = yMargin + buttonSize;
            prefY = digitY + digitHeight + buttonSize + yMargin;
        } else {
            digitY = yMargin;
            prefY = digitY + digitHeight + yMargin;
        }

        int strLength;
        if (isMonospaced() && (nbButton > 0) && (!editMode)) {
            int maxSmallWidth = buttonSize + 2;
            strLength = Double.isInfinite(value)
                    ? str.length() * Math.max(maxSmallWidth, FontUtils.measureString(INFINITY3, getFont()).width + 1)
                    : str.length() * maxSmallWidth;
        } else {
            strLength = theLabel.getFontMetrics(theLabel.getFont()).stringWidth(str);
        }
        int prefX = xMargin + strLength + xMargin;

        int tempX = xMargin, tempY = digitY;
        final int width = strLength, height = digitHeight;
        Border border = getBorder();
        if (border != null) {
            Insets insets = border.getBorderInsets(this);
            if (insets != null) {
                if (insets.left > 0) {
                    prefX += insets.left;
                    tempX += insets.left;
                }
                if (insets.right > 0) {
                    prefX += insets.right;
                }
                if (insets.top > 0) {
                    prefY += insets.top;
                    tempY += insets.top;
                }
                if (insets.bottom > 0) {
                    prefY += insets.bottom;
                }
            }
        }
        final int x = tempX, y = tempY;
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                theLabel.setBounds(x, y, width, height);
            }
        });
        prefDim = new Dimension(prefX, prefY);

        // call revalidate to take account of the new preferred size
        this.revalidate();
    }

    protected void placeButtons() {
        char c;
        int width;
        int posX = xMargin;
        int cpt = 0;
        int bottomY = digitY + digitHeight;
        // we place buttons with data compute by computeSize()
        ArrowButton[][] toCheck = buttons;
        String formatedValue = this.formatedValue;
        if (formatedValue == null) {
            formatedValue = ObjectUtils.EMPTY_STRING;
        }
        int yMargin = this.yMargin;
        int buttonSize = this.buttonSize;
        Border border = getBorder();
        if (border != null) {
            Insets insets = border.getBorderInsets(this);
            if (insets != null) {
                if (insets.left > 0) {
                    posX += insets.left;
                }
                if (insets.top > 0) {
                    yMargin += insets.top;
                    bottomY += insets.top;
                }
            }
        }
        for (int i = 0; i < formatedValue.length(); i++) {
            c = formatedValue.charAt(i);
            if (isMonospaced() && (nbButton > 0) && (!editMode)) {
                width = buttonSize + 2;
            } else {
                width = theLabel.getFontMetrics(theLabel.getFont()).stringWidth(c + ObjectUtils.EMPTY_STRING);
            }
            if ((i == 0) && (!isUsePlusMinusArrows()) && ((c == '-') || (c == '+'))) {
                posX += width;
                continue;
            }
            if (cpt < toCheck[UP_INDEX].length) {
                if (Character.isDigit(c)) {
                    toCheck[UP_INDEX][cpt].setBounds(posX + 1, yMargin, buttonSize, buttonSize);
                    toCheck[DOWN_INDEX][cpt].setBounds(posX + 1, bottomY, buttonSize, buttonSize);
                    cpt++;
                } else if (displayButtonsWithErrorValues && isUsableErrorCharacter(c)) {
                    toCheck[UP_INDEX][cpt].setBounds(posX + 1, yMargin, buttonSize, buttonSize);
                    toCheck[DOWN_INDEX][cpt].setBounds(posX + 1, bottomY, buttonSize, buttonSize);
                    cpt++;
                } else if (c == '+' || c == '-') {
                    toCheck[UP_INDEX][cpt].setBounds(posX + 1, yMargin, width - 2, buttonSize);
                    toCheck[DOWN_INDEX][cpt].setBounds(posX + 1, bottomY, width - 2, buttonSize);
                    cpt++;
                }
            }
            posX += width;
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return prefDim;
    }

    @Override
    public void addWheelSwitchListener(IWheelSwitchListener l) {
        if (l != null) {
            listeners.add(l);
        }
    }

    @Override
    public void removeWheelSwitchListener(IWheelSwitchListener l) {
        if (l != null) {
            listeners.remove(l);
        }
    }

    // handle Key Envent
    protected void processKey(KeyEvent e) {
        if (e != null) {
            if (e.getKeyCode() == KeyEvent.VK_F5) {
                applyChange(true);
            } else {
                // if we are editing a value, buttons are no longer visible
                // so we don't have to handle arrow key
                if (!editMode) {
                    handleArrowKey(e.getKeyCode());
                }
            }
            handleEdition(e.getKeyChar(), e.getKeyCode());
        }
    }

    protected void handleArrowKey(int keyCode) {
        if (nbButton > 0) {
            switch (keyCode) {
                // Button selection
                case KeyEvent.VK_RIGHT:
                    if (currentSelButton + 1 < nbButton) {
                        currentSelButton++;
                        updateButtonColor();
                    }
                    break;
                case KeyEvent.VK_LEFT:
                    if (currentSelButton - 1 >= 0) {
                        currentSelButton--;
                        updateButtonColor();
                    }
                    break;
                // digit value modification
                case KeyEvent.VK_UP:
                    computeValue(true);
                    break;
                case KeyEvent.VK_DOWN:
                    computeValue(false);
                    break;
            }
        }
    }

    protected void applyValue(String editedValue) {
        try {
            double newValue;
            String tmp = editedValue.toLowerCase();
            switch (tmp) {
                case INFINITY:
                case POSITIVE_INFINITY:
                case INFINITY2:
                case POSITIVE_INFINITY2:
                case INFINITY3:
                case POSITIVE_INFINITY3:
                    newValue = Double.POSITIVE_INFINITY;
                    break;
                case NEGATIVE_INFINITY:
                case NEGATIVE_INFINITY2:
                case NEGATIVE_INFINITY3:
                    newValue = Double.NEGATIVE_INFINITY;
                    break;
                case NAN:
                    newValue = Double.NaN;
                    break;
                case ObjectUtils.EMPTY_STRING:
                    newValue = getValue();
                    break;
                default:
                    newValue = Double.NaN;
                    boolean done = true;
                    for (int i = 0; i < tmp.length() && done; i++) {
                        if (tmp.charAt(i) != 'x') {
                            done = false;
                        }
                    }
                    if (!done) {
                        newValue = Double.parseDouble(editedValue);
                    }
                    break;
            }
            editedValue = ObjectUtils.EMPTY_STRING;
            this.editedValue = editedValue;
            editMode = false;
            // if the number of button doesn't change we have to show them because they won't be recreated
            // in prepareButton()
            setButtonVisible(true);
            setValue(newValue, true, true);
            fireValueChange();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, WRONG_VALUE, WRONG_VALUE, JOptionPane.ERROR_MESSAGE);
        }
    }

    protected void handleEdition(char c, int code) {
        if (isEditable() && isEnabled()) {
            boolean repaint = false;
            String editedValue = this.editedValue;
            if (editedValue == null) {
                editedValue = ObjectUtils.EMPTY_STRING;
            }
            char lc = Character.toLowerCase(c);
            // we save the typed characters to show them as they go along
            if ((c >= '0' && c <= '9') || (c == '.') || (c == '-')) {
                editedValue += c;
                this.editedValue = editedValue;
                editMode = true;
                theLabel.setText(editedValue);
                setButtonVisible(false);
                repaint = true;
            } else if (lc == 'e') {
                if (editedValue.toLowerCase().indexOf('e') == -1 && !editedValue.isEmpty()) {
                    editedValue += c;
                    this.editedValue = editedValue;
                    theLabel.setText(editedValue);
                    repaint = true;
                }
            } else {
                if (infinityAllowed) {
                    if ((lc == 'i') || (lc == 'n') || (lc == 'f') || (lc == 't') || (lc == 'y')) {
                        String tmp = editedValue + c;
                        String lower = tmp.toLowerCase();
                        if (INFINITY.startsWith(lower) || (POSITIVE_INFINITY.startsWith(lower))
                                || (NEGATIVE_INFINITY.startsWith(lower))) {
                            editedValue = tmp;
                            this.editedValue = editedValue;
                            editMode = true;
                            theLabel.setText(editedValue);
                            setButtonVisible(false);
                            repaint = true;
                        }
                    }
                }
                if (nanAllowed) {
                    if (lc == 'x') {
                        boolean strOk = true;
                        for (int i = 0; i < editedValue.length() && strOk; i++) {
                            char tmp = editedValue.charAt(i);
                            strOk = (tmp == 'x') || (tmp == 'X');
                        }
                        if (strOk) {
                            editedValue += c;
                            this.editedValue = editedValue;
                            editMode = true;
                            theLabel.setText(editedValue);
                            setButtonVisible(false);
                            repaint = true;
                        }
                    } else if ((lc == 'n') || (lc == 'a')) {
                        String tmp = editedValue + c;
                        if (NAN.startsWith(tmp.toLowerCase())) {
                            editedValue = tmp;
                            this.editedValue = editedValue;
                            editMode = true;
                            theLabel.setText(editedValue);
                            setButtonVisible(false);
                            repaint = true;
                        }
                    }
                }
            }
            if (repaint) {
                computeSize();
            }
            // user cancel his value edition
            if (code == KeyEvent.VK_CLEAR || code == KeyEvent.VK_CANCEL || code == KeyEvent.VK_ESCAPE) {
                cancelEdition();
            } else {
                boolean deleteChar;
                deleteChar = (editMode && editedValue.length() > 0
                        && (code == KeyEvent.VK_BACK_SPACE || code == KeyEvent.VK_DELETE));
                if (deleteChar) {
                    editedValue = editedValue.substring(0, editedValue.length() - 1);
                    this.editedValue = editedValue;
                    theLabel.setText(editedValue);
                    repaint = true;
                    computeSize();
                } else if (code == KeyEvent.VK_ENTER) {
                    applyValue(editedValue);
                }
            }
            if (repaint) {
                revalidate();
                repaint();
            }
        }
    }

    /**
     * Cancels value edition
     */
    protected void cancelEdition() {
        editedValue = ObjectUtils.EMPTY_STRING;
        editMode = false;
        setButtonVisible(true);
        computeSize();
        theLabel.setText(formatedValue);
        repaint();
    }

    // allow to hide or show all buttons in the wheelswitch
    protected void setButtonVisible(boolean b) {
        ArrowButton[][] toCheck = buttons;
        for (int i = 0; i < toCheck[UP_INDEX].length; i++) {
            toCheck[UP_INDEX][i].setVisible(b);
            toCheck[DOWN_INDEX][i].setVisible(b);
        }
    }

    public void setButtonColor(Color c) {
        if (c == null) {
            c = DEFAULT_BUTTON_BACKGROUND_COLOR;
        }
        buttonBackgroundColor = c;
        if (DEFAULT_BUTTON_BACKGROUND_COLOR.equals(c)) {
            buttonSelectionColor = DEFAULT_SELECTION_COLOR;
        } else if (Math.round(ColorUtils.getRealGray(c)) < 128) {
            buttonSelectionColor = ColorUtils.blend(c, Color.WHITE);
        } else {
            buttonSelectionColor = ColorUtils.blend(c, Color.BLACK);
        }
        updateButtonColor();
    }

    // Update button color according to the focus.
    protected void updateButtonColor() {
        ArrowButton[][] toCheck = buttons;
        if (isEnabled()) {
            for (int i = 0; i < toCheck[UP_INDEX].length; i++) {
                if (i == currentSelButton && toCheck[UP_INDEX][i].isEnabled()) {
                    toCheck[UP_INDEX][i].setBackground(buttonSelectionColor);
                    toCheck[DOWN_INDEX][i].setBackground(buttonSelectionColor);
                } else {
                    toCheck[UP_INDEX][i].setBackground(buttonBackgroundColor);
                    toCheck[DOWN_INDEX][i].setBackground(buttonBackgroundColor);
                }
            }
        }
    }

    // Fire WheelSwitchEvent to all registered listeners
    protected void fireValueChange() {
        Double dvalue = Double.valueOf(getValue());
        WheelSwitchEvent w = new WheelSwitchEvent(this, dvalue);
        for (IWheelSwitchListener listener : listeners) {
            if (listener != null) {
                listener.valueChange(w);
            }
        }
    }

    // Warns Mediators for some changes in this WheelSwitch
    protected void warnMediators(Number numberValue, String text) {
        warnMediators(new NumberInformation(this, numberValue));
        warnMediators(new TextInformation(this, text));
    }

    @Override
    public void setNumberValue(Number value) {
        double doubleValue = (value == null ? MathConst.NAN_FOR_NULL : value.doubleValue());
        setValue(doubleValue, true, false);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    /**
     * Whether to use inversionLogic.
     * 
     * @param invertionLogic
     *            the invertionLogic to set. If <code>TRUE</code> decreasing a
     *            digit under <code>0</code> or increasing a digit over <code>9</code> will impact other digits.
     *            Otherwise, decreasing
     *            a digit under <code>0</code> will set it back to <code>9</code>, and increasing a digit over
     *            <code>9</code> will set it back to <code>0</code>.
     */
    public void setInvertionLogic(boolean invertionLogic) {
        inversionLogic = invertionLogic;
    }

    /**
     * Whether invertionLogic is used
     * 
     * @return A <code>boolean</code> value
     * @see #setInvertionLogic(boolean)
     */
    public boolean isInvertionLogic() {
        return inversionLogic;
    }

    public void lockBackGroundColor(boolean lock) {
        lockBackgroundColor = lock;
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        if (!lockBackgroundColor) {
            super.setCometeBackground(color);
        }
    }

    public boolean isDisplayButtonsWithErrorValues() {
        return displayButtonsWithErrorValues;
    }

    public void setDisplayButtonsWithErrorValues(boolean displayButtonsWithErrorValues) {
        if (this.displayButtonsWithErrorValues != displayButtonsWithErrorValues) {
            this.displayButtonsWithErrorValues = displayButtonsWithErrorValues;
            applyChange(true);
        }
    }

    public boolean isUsePlusMinusArrows() {
        return usePlusMinusArrows;
    }

    public void setUsePlusMinusArrows(boolean usePlusMinusArrows) {
        if (this.usePlusMinusArrows != usePlusMinusArrows) {
            this.usePlusMinusArrows = usePlusMinusArrows;
            applyChange(true);
        }
    }

    /**
     * Returns whether edited value will be applied when this {@link WheelSwitch} looses focus.
     * 
     * @return A <code>boolean</code>. <code>false</code> by default.
     */
    public boolean isApplyValueOnFocusLost() {
        return applyValueOnFocusLost;
    }

    /**
     * Sets whether edited value should be applied when this {@link WheelSwitch} looses focus.
     * 
     * @param applyValueOnFocusLost Whether edited value should be applied when this {@link WheelSwitch} looses focus.
     */
    public void setApplyValueOnFocusLost(boolean applyValueOnFocusLost) {
        this.applyValueOnFocusLost = applyValueOnFocusLost;
    }

    /**
     * Returns the x margin used by this {@link WheelSwitch}
     * 
     * @return An <code>int</code>. <code>5</code> by default.
     */
    public int getXMargin() {
        return xMargin;
    }

    /**
     * Sets the x margin this {@link WheelSwitch} should use.
     * 
     * @param xMargin The x margin to use. Must be <code>&gt; -1</code>.
     */
    public void setXMargin(int xMargin) {
        if ((this.xMargin != xMargin) && (xMargin > -1)) {
            this.xMargin = xMargin;
            applyChange(false);
        }
    }

    /**
     * Returns the y margin used by this {@link WheelSwitch}
     * 
     * @return An <code>int</code>. <code>5</code> by default.
     */
    public int getYMargin() {
        return yMargin;
    }

    /**
     * Sets the y margin this {@link WheelSwitch} should use.
     * 
     * @param yMargin The y margin to use. Must be <code>&gt; -1</code>.
     */
    public void setYMargin(int yMargin) {
        if ((this.yMargin != yMargin) && (yMargin > -1)) {
            this.yMargin = yMargin;
            applyChange(false);
        }
    }

    // Recover the effective String bounds.
    // Algorithm found here: https://stackoverflow.com/questions/368295/how-to-get-real-string-height-in-java
    protected Rectangle getStringBounds(Graphics2D g2, float x, float y) {
        FontRenderContext frc = g2.getFontRenderContext();
        GlyphVector gv = getFont().createGlyphVector(frc, REF);
        return gv.getPixelBounds(null, x, y);
    }

    // JAVAAPI-578
    /**
     * Tries to recover best {@link Font} size (adapted to component height too), according to previously calculated
     * best font
     * size (height ignored).
     * 
     * @param font The {@link Font}.
     * @param refSize The previously calculated best font size.
     * @param borderMargin The margin to apply, according to current border.
     * @return An <code>int</code>.
     */
    protected int computeBestFontSize(final Font font, final int refSize, final Dimension borderMargin) {
        int bestSize = refSize;
        if (bestSize > 0) {
            int height;
            Font adaptedFont = font.deriveFont((float) bestSize);
            int buttonSize = computeButtonSize(adaptedFont);
            int digitHeight = getDigitHeight(getDisplayedText(), adaptedFont);
            if (nbButton > 0) {
                height = 2 * (yMargin + buttonSize) + digitHeight + borderMargin.height;
            } else {
                height = 2 * yMargin + digitHeight + borderMargin.height;
            }
            int currentHeight = getHeight();
            if ((currentHeight > 0) && (height > currentHeight)) {
                int delta = height - currentHeight;
                if (delta < 3) {
                    bestSize = computeBestFontSize(font, bestSize - 1, borderMargin);
                } else {
                    int logDelta = (int) Math.floor(Math.log10((delta) / 3));
                    int adaptedDelta = (int) Math.pow(10, logDelta);
                    int nexTry = bestSize - adaptedDelta;
                    bestSize = computeBestFontSize(font, nexTry, borderMargin);
                }
            }
        }
        return bestSize;
    }

    // JAVAAPI-578
    @Override
    public int computeBestFontSize() {
        String text = getDisplayedText();
        int textWidth;
        if (isMonospaced()) {
            // In case of monospaced wheelswitch, text width is calculated a different way.
            textWidth = (buttonSize + 2) * text.length();
        } else {
            // In case of classic wheelswitch, DynamicFontSizeManager may calculate text width.
            textWidth = -1;
        }
        int bestSize = DynamicFontSizeManager.computeBestFontSizeIgnoreHeight(theLabel, getFont(), text, textWidth,
                getWidth(), DynamicFontSizeManager.getBorderAndIconMargin(theLabel));
        return computeBestFontSize(getFont(), bestSize, DynamicFontSizeManager.getBorderAndIconMargin(this));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ((evt != null) && (evt.getSource() == theLabel) && TEXT.equals(evt.getPropertyName())) {
            firePropertyChange(TEXT, evt.getOldValue(), evt.getNewValue());
        }
    }

    public static void main(String... args) {
        JFrame f = new JFrame(WheelSwitch.class.getSimpleName() + " test");
        boolean useStress = false, dynamicFont = false, monospaced = false;
        final Thread stressThread;
        List<String> formats = new ArrayList<>();
        if ((args != null) && (args.length > 0)) {
            for (String arg : args) {
                if (arg == null) {
                    arg = ObjectUtils.EMPTY_STRING;
                } else {
                    arg = arg.trim();
                }
                String argLower = arg.toLowerCase();
                switch (argLower) {
                    case "stress":
                        useStress = true;
                        break;
                    case "dynamic":
                        dynamicFont = true;
                        break;
                    case "monospaced":
                        monospaced = true;
                        break;
                    default:
                        if (Format.isNumberFormatOK(arg)) {
                            formats.add(arg);
                        }
                        break;
                }
            }
        }
        if (dynamicFont) {
            // Dynamic font size test (JAVAAPI-578)
            stressThread = null;
            final String plain = "plain", italic = "italic", bold = "bold", boldItalic = "bold italic";
            final String fontProperty = "font";
            final String format = "<html><body><table><tr><th>Font family:</th><td>&ensp;%s</td></tr><tr><th>Font size:</th><td>&ensp;%s</td></tr><tr><th>Font style:</th><td>&ensp;%s</td></tr></table></body></html>";
            final JLabel fontLabel = new JLabel();
            fontLabel.setFont(fontLabel.getFont().deriveFont(Font.PLAIN));
            final JPanel mainPanel = new JPanel(new BorderLayout());
            final WheelSwitch wheelSwitch = new WheelSwitch();
            wheelSwitch.setMonospaced(monospaced);
            new DynamicFontSizeManager().setupAndListenToComponent(wheelSwitch);
            wheelSwitch.setFormat(formats.isEmpty() ? "%4.3e" : formats.get(0));
            wheelSwitch.setUsePlusMinusArrows(false);
            wheelSwitch.setInvertionLogic(true);
            wheelSwitch.setNumberValue(0);
            wheelSwitch.setBackground(new Color(255, 240, 230));
            wheelSwitch.addPropertyChangeListener((e) -> {
                if (fontProperty.equals(e.getPropertyName())) {
                    Font tmp = (Font) e.getNewValue();
                    if (tmp == null) {
                        fontLabel.setText(String.format(format, ObjectUtils.EMPTY_STRING, ObjectUtils.EMPTY_STRING,
                                ObjectUtils.EMPTY_STRING));
                    } else {
                        String tmpStyle;
                        if (tmp.isItalic()) {
                            if (tmp.isBold()) {
                                tmpStyle = boldItalic;
                            } else {
                                tmpStyle = italic;
                            }
                        } else if (tmp.isBold()) {
                            tmpStyle = bold;
                        } else {
                            tmpStyle = plain;
                        }
                        fontLabel.setText(String.format(format, tmp.getFontName(), tmp.getSize(), tmpStyle));
                    }
                }
            });
            mainPanel.add(wheelSwitch, BorderLayout.CENTER);
            String fontStyle;
            Font font = wheelSwitch.getFont();
            if (font.isItalic()) {
                if (font.isBold()) {
                    fontStyle = boldItalic;
                } else {
                    fontStyle = italic;
                }
            } else if (font.isBold()) {
                fontStyle = bold;
            } else {
                fontStyle = plain;
            }
            fontLabel.setText(String.format(format, font.getFontName(), font.getSize(), fontStyle));
            fontLabel.setBackground(new Color(230, 255, 240));
            fontLabel.setOpaque(true);
            mainPanel.add(fontLabel, BorderLayout.SOUTH);
            f.setContentPane(mainPanel);
        } else {
            // Classic test

            int i = 0;
            // scientific format
            WheelSwitch expWS = new WheelSwitch();
            expWS.setFormat(i < formats.size() ? formats.get(i++) : "%4.3e");
            expWS.setInvertionLogic(true);
            expWS.setButtonColor(new Color(200, 200, 200));
            expWS.setNumberValue(-42.9995);
            expWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed expWS:" + e.getValue());
                }
            });

            // float format
            WheelSwitch floatWS = new WheelSwitch();
            floatWS.setFormat(i < formats.size() ? formats.get(i++) : "%8.6f");
            floatWS.setInvertionLogic(true);
            floatWS.setBackground(new Color(250, 230, 255));
            floatWS.setButtonColor(Color.GREEN);
            floatWS.setNumberValue(5);
            // ws2.setMaxValue(75.0);
            // ws2.setMinValue(-25.0);
            // ws2.setFont(new Font("Dialog", Font.BOLD, 50));
            floatWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed floatWS:" + e.getValue());
                }
            });

            // integer format
            WheelSwitch intWS = new WheelSwitch();
            intWS.setFormat(i < formats.size() ? formats.get(i++) : "%6d");
            intWS.setButtonColor(Color.BLUE);
            intWS.setNumberValue(48.5);
            intWS.setFont(new Font("Lucida Bright", Font.BOLD, 30));
            intWS.setBorder(BorderFactory.createEtchedBorder());
            intWS.setInvertionLogic(true);
            intWS.setFormatEditable(true);
            intWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed intWS:" + e.getValue());
                }
            });

            // bad format
            WheelSwitch badWS = new WheelSwitch();
            badWS.setFormat(i < formats.size() ? formats.get(i++) : "%6.3s");
            badWS.setButtonColor(new Color(100, 200, 160));
            badWS.setNumberValue(28.1);
            badWS.setFont(new Font("Lucida Bright", Font.BOLD, 12));
            badWS.setBorder(BorderFactory.createEtchedBorder());
            badWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed badWS:" + e.getValue());
                }
            });

            // NaN with scientific format
            WheelSwitch nanExpWS = new WheelSwitch();
            nanExpWS.setFormat(i < formats.size() ? formats.get(i++) : "%6.3e");
            nanExpWS.setButtonColor(new Color(100, 0, 0));
            nanExpWS.setNumberValue(Double.NaN);
            nanExpWS.setFont(new Font("Dialog", Font.PLAIN, 16));
            nanExpWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed nanExpWS:" + e.getValue());
                }
            });

            // NaN with float format
            WheelSwitch nanFloatWS = new WheelSwitch();
            nanFloatWS.setFormat(i < formats.size() ? formats.get(i++) : "%5.2f");
            nanFloatWS.setButtonColor(new Color(0, 100, 0));
            nanFloatWS.setNumberValue(Double.NaN);
            nanFloatWS.setFont(new Font("Dialog", Font.PLAIN, 16));
            nanFloatWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed nanFloatWS:" + e.getValue());
                }
            });

            // NaN with integer format
            WheelSwitch nanIntWS = new WheelSwitch();
            nanIntWS.setFormat(i < formats.size() ? formats.get(i++) : "%+05d");
            nanIntWS.setButtonColor(new Color(0, 0, 100));
            nanIntWS.setNumberValue(Double.NaN);
            nanIntWS.setFont(new Font("Dialog", Font.PLAIN, 16));
            nanIntWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed nanIntWS:" + e.getValue());
                }
            });

            // NaN with integer format and arrows
            WheelSwitch nanIntArrowWS = new WheelSwitch();
            nanIntArrowWS.setDisplayButtonsWithErrorValues(true);
            nanIntArrowWS.setFormat(i < formats.size() ? formats.get(i++) : "%+05d");
            nanIntArrowWS.setButtonColor(new Color(0, 0, 100));
            nanIntArrowWS.setNumberValue(Double.NaN);
            nanIntArrowWS.setFont(new Font("Dialog", Font.PLAIN, 16));
            nanIntArrowWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed nanIntArrowWS:" + e.getValue());
                }
            });

            // Infinity with float format
            WheelSwitch infFloatWS = new WheelSwitch();
            infFloatWS.setMinIncluded(true);
            infFloatWS.setMaxIncluded(true);
            infFloatWS.setNanAllowed(true);
            infFloatWS.setInfinityAllowed(true);
            infFloatWS.setFormat(i < formats.size() ? formats.get(i++) : "%5.2f");
            infFloatWS.setButtonColor(new Color(0, 100, 0));
            infFloatWS.setNumberValue(Double.POSITIVE_INFINITY);
            infFloatWS.setFont(new Font("Dialog", Font.PLAIN, 14));
            infFloatWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed infFloatWS:" + e.getValue());
                }
            });

            // Infinity with integer format
            WheelSwitch infIntWS = new WheelSwitch();
            infIntWS.setMinIncluded(true);
            infIntWS.setMaxIncluded(true);
            infIntWS.setNanAllowed(true);
            infIntWS.setInfinityAllowed(true);
            infIntWS.setFormat(i < formats.size() ? formats.get(i++) : "%+05d");
            infIntWS.setButtonColor(new Color(0, 100, 0));
            infIntWS.setNumberValue(Double.NEGATIVE_INFINITY);
            infIntWS.setFont(new Font("Dialog", Font.PLAIN, 14));
            infIntWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed infIntWS:" + e.getValue());
                }
            });

            // Infinity with float format and arrows
            WheelSwitch infFloatArrowWS = new WheelSwitch();
            infFloatArrowWS.setDisplayButtonsWithErrorValues(true);
            infFloatArrowWS.setMinIncluded(true);
            infFloatArrowWS.setMaxIncluded(true);
            infFloatArrowWS.setNanAllowed(true);
            infFloatArrowWS.setInfinityAllowed(true);
            infFloatArrowWS.setFormat(i < formats.size() ? formats.get(i++) : "%5.2f");
            infFloatArrowWS.setButtonColor(new Color(0, 100, 0));
            infFloatArrowWS.setNumberValue(Double.POSITIVE_INFINITY);
            infFloatArrowWS.setFont(new Font("Dialog", Font.PLAIN, 14));
            infFloatArrowWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed infFloatArrowWS:" + e.getValue());
                }
            });

            // Infinity with integer format and arrows
            WheelSwitch infIntArrowWS = new WheelSwitch();
            infIntArrowWS.setDisplayButtonsWithErrorValues(true);
            infIntArrowWS.setMinIncluded(true);
            infIntArrowWS.setMaxIncluded(true);
            infIntArrowWS.setNanAllowed(true);
            infIntArrowWS.setInfinityAllowed(true);
            infIntArrowWS.setFormat(i < formats.size() ? formats.get(i++) : "%+05d");
            infIntArrowWS.setButtonColor(new Color(0, 100, 0));
            infIntArrowWS.setNumberValue(Double.NEGATIVE_INFINITY);
            infIntArrowWS.setFont(new Font("Dialog", Font.PLAIN, 14));
            infIntArrowWS.addWheelSwitchListener(new IWheelSwitchListener() {
                @Override
                public void valueChange(WheelSwitchEvent e) {
                    System.out.println("Value changed infIntArrowWS:" + e.getValue());
                }
            });

            JButton b = new JButton("OK");
            b.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    System.exit(0);
                }
            });

            JPanel mainPanel = new JPanel(new VerticalFlowLayout());

            JScrollPane scrollPane = new JScrollPane(mainPanel);

            f.setContentPane(scrollPane);
            f.setMinimumSize(new Dimension(0, 0));

            mainPanel.setBackground(Color.white);
            mainPanel.setLayout(new VerticalFlowLayout());
            mainPanel.add(expWS);
            mainPanel.add(floatWS);
            mainPanel.add(intWS);
            mainPanel.add(badWS);
            mainPanel.add(nanExpWS);
            mainPanel.add(nanFloatWS);
            mainPanel.add(nanIntWS);
            mainPanel.add(nanIntArrowWS);
            mainPanel.add(infFloatWS);
            mainPanel.add(infIntWS);
            mainPanel.add(infFloatArrowWS);
            mainPanel.add(infIntArrowWS);

            final WheelSwitch stress;
            if (useStress) {
                stress = new WheelSwitch();
                stress.setFormat(i < formats.size() ? formats.get(i++) : "%5.2e");
                stressThread = new Thread(WheelSwitch.class.getSimpleName() + " stress Thread") {
                    @Override
                    public void run() {
                        while (true) {
                            int total = new Random().nextInt(9) + 2;
                            int frac = new Random().nextInt(total - 1) + 1;
                            String format = PERCENT + total + "." + frac + "f";
                            double value = (Math.random() * 2) * (Math.random() < 0.5 ? -1 : 1);
                            if (Math.random() < 0.2) {
                                value = Double.NaN;
                            }
                            stress.setNumberValue(value);
                            stress.setFormat(format);
                            try {
                                sleep(new Random().nextInt(50) + 1);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                };
                mainPanel.add(stress);
            } else {
                stress = null;
                stressThread = null;
            }

            scrollPane.setColumnHeaderView(b);
        }
        formats.clear();
        f.pack();
        f.setVisible(true);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        if (useStress) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    stressThread.start();
                }
            });
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class FormatUpdater implements PopupMenuListener {
        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            formatField.setText(getFormat().trim().replace(PLUS, ObjectUtils.EMPTY_STRING));
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            // not managed
        }

        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
            // not managed
        }
    }

    /**
     * A {@link JLabel} that is able to adapt characters drawing, according to the wish to use monospaced characters or
     * not.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class AdaptedSpaceLabel extends JLabel {

        private static final long serialVersionUID = 6045337990303556062L;

        protected boolean monospaced;

        public AdaptedSpaceLabel() {
            super();
            setBorder(null);
        }

        public AdaptedSpaceLabel(Icon image, int horizontalAlignment) {
            super(image, horizontalAlignment);
            setBorder(null);
        }

        public AdaptedSpaceLabel(Icon image) {
            super(image);
            setBorder(null);
        }

        public AdaptedSpaceLabel(String text, Icon icon, int horizontalAlignment) {
            super(text, icon, horizontalAlignment);
            setBorder(null);
        }

        public AdaptedSpaceLabel(String text, int horizontalAlignment) {
            super(text, horizontalAlignment);
            setBorder(null);
        }

        public AdaptedSpaceLabel(String text) {
            super(text);
            setBorder(null);
        }

        /**
         * Returns whether characters will all be drawn with the same width (or using the width calculated by the
         * {@link Font})
         * 
         * @return A <code>boolean</code>. <code>false</code> by default
         */
        public boolean isMonospaced() {
            return monospaced;
        }

        /**
         * Sets whether characters should all be drawn with the same width (or using the width calculated by the
         * {@link Font})
         * 
         * @param monospaced Whether characters should all be drawn with the same width.
         *            <ul>
         *            <li>If <code>true</code>, all characters will use the same width and the arrow buttons will be
         *            adapted to this width</li>
         *            <li>If <code>false</code>, each character will use the width calculated by the {@link Font}, and
         *            the arrow buttons will try to adapt to the characters' width</li>
         *            </ul>
         */
        public void setMonospaced(boolean monospaced) {
            if (this.monospaced != monospaced) {
                this.monospaced = monospaced;
            }
        }

        @Override
        protected void paintComponent(Graphics g) {
            if (isMonospaced() && (nbButton > 0) && (!editMode)) {
                Graphics2D g2;
                if (g instanceof Graphics2D) {
                    g2 = (Graphics2D) g;
                    g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                    g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
                            RenderingHints.VALUE_FRACTIONALMETRICS_ON);
                } else {
                    g2 = null;
                }
                g.setFont(getFont());
                int width = buttonSize + 2;
                String txt = getText();
                FontMetrics fm = getFontMetrics(getFont());
                if (txt != null) {
                    char[] chars = new char[1];
                    int y = fm.getAscent();
                    if (g2 != null) {
                        int tempY = getStringBounds(g2, 0, y).height;
                        if (y > tempY) {
                            y -= Math.round((y - tempY) / 2.0f);
                        }
                    }
                    for (int i = 0; i < txt.length(); i++) {
                        chars[0] = txt.charAt(i);
                        int charWidth = fm.charWidth(chars[0]);
                        int offset = 0;
                        if (charWidth < width) {
                            offset = Math.round((width - charWidth) / 2.0f);
                        }
                        g.drawChars(chars, 0, 1, i * width + offset, y);
                    }
                }
            } else {
                super.paintComponent(g);
            }
        }

    }

}
