/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.data;

/**
 * Class to handle plot data (LinkedList).
 * 
 * @author JL Pons
 * @author Rapha&euml;l GIRARDOT
 */
public class DataList implements IDataList {

    private static final long serialVersionUID = -2346994193647073526L;

    // Original coordinates
    /** x value */
    private double x;

    /** y value */
    private double y;

    /** error value */
    private double error;

    // pointer to next item
    public DataList next;

    // Construct a node
    public DataList(double x, double y) {
        this(x, y, 0);
    }

    // Construct a node
    public DataList(double x, double y, double error) {
        this.x = x;
        this.y = y;
        this.error = Math.abs(error);
        next = null;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    /**
     * @return the error
     */
    public double getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(double error) {
        this.error = Math.abs(error);
    }

    public void reset() {
        // This algorithm was not made recursive to avoid some StackOverFlow
        DataList temp = this;
        DataList tempNext = next;
        while (temp != null) {
            temp.next = null;
            temp = tempNext;
            if (temp != null) {
                tempNext = temp.next;
            }
        }
    }

    @Override
    protected void finalize() {
        reset();
    }

}
