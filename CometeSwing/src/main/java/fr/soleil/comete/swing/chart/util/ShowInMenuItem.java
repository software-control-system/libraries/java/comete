/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.util.List;

import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.data.DataView;

public abstract class ShowInMenuItem extends AbstractChartCustomMenu {

    private static final long serialVersionUID = 3012042206758243805L;

    protected static ChartViewOptionDialog chartViewOption;

    public ShowInMenuItem() {
        super(5, "Show in new Chart", true, "Show in selection", "Select view...");
    }

    @Override
    public void manageDataViews(List<DataView> result, Chart chart) {
        if (chartViewOption == null) {
            chartViewOption = new ChartViewOptionDialog(this);
        }
        if (!chartViewOption.isVisible()) {
            chartViewOption.setLocationRelativeTo(this);
            chartViewOption.setVisible(true);
        }
        SingleChartView selectedView = chartViewOption.getSelectedView();
        if ((selectedView != null) && (result != null) && !result.isEmpty()) {
            selectedView.copyDataViews(result, chart);
            showSingleChartView(selectedView);
            result.clear();
        }
    }

    public abstract void showSingleChartView(SingleChartView selectedView);

}
