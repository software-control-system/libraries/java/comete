/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util.mask.sector;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.lang.ref.WeakReference;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.util.MathematicSector;
import fr.soleil.comete.definition.widget.util.MathematicSectorException;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

public class MathematicSectorEditor extends JPanel implements DocumentListener {

    private static final long serialVersionUID = -917349832404184928L;

    protected final static String TEXT_FIELD_KEY = "parent-text-field";
    protected final static Color DEFAULT_VALID_COLOR = Color.WHITE;
    protected final static Color DEFAULT_KO_COLOR = Color.ORANGE;
    protected final static String DEFAULT_KO_STRING = "Invalid value!";

    protected WeakReference<MathematicSector> sectorRef;
    protected JLabel[] parameterTitles;
    protected JLabel[] parameterUnits;
    protected JTextField[] parameterEditors;
    protected Color validColor = DEFAULT_VALID_COLOR;
    protected Color koColor = DEFAULT_KO_COLOR;
    protected String koString = DEFAULT_KO_STRING;

    public MathematicSectorEditor() {
        super(new GridBagLayout());
    }

    public synchronized void setSector(MathematicSector sector) {
        clean();
        if (sector != null) {
            setToolTipText(sector.getDescription());
            sector.setEditingMode();
            sectorRef = new WeakReference<MathematicSector>(sector);
            setBorder(new TitledBorder(sector.getDescription()));
            String[] names = sector.getParameterNames();
            if (names != null) {
                parameterEditors = new JTextField[names.length];
                parameterTitles = new JLabel[names.length];
                parameterUnits = new JLabel[names.length];
                for (int i = 0; i < names.length; i++) {
                    String name;
                    if (names[i] == null) {
                        name = ChartUtils.NULL;
                    } else {
                        name = names[i];
                    }
                    String displayName = sector.getDisplayableName(name);
                    if (displayName == null) {
                        displayName = ChartUtils.NULL;
                    }
                    String unit = sector.getParameterUnit(name);
                    if (unit == null) {
                        unit = ObjectUtils.EMPTY_STRING;
                    }
                    parameterTitles[i] = new JLabel(displayName + ":");
                    parameterTitles[i].setToolTipText(sector.getParameterDescription(name));
                    parameterTitles[i].setFont(CometeUtils.getLabelbFont());
                    parameterEditors[i] = new JTextField(10);
                    parameterEditors[i].setName(name);
                    parameterEditors[i].getDocument().putProperty(TEXT_FIELD_KEY, parameterEditors[i]);
                    parameterEditors[i].getDocument().addDocumentListener(this);
                    parameterUnits[i] = new JLabel(unit);
                    parameterUnits[i].setFont(CometeUtils.getLabelFont());
                    Number value = sector.getParameter(name);
                    if (value == null) {
                        parameterEditors[i].setText(ObjectUtils.EMPTY_STRING);
                    } else {
                        parameterEditors[i].setText(String.valueOf(value));
                    }
                    parameterEditors[i].setToolTipText(sector.getValidValuesDescription(name));
                    updateParentTextField(parameterEditors[i].getDocument());
                    GridBagConstraints titleConstraints = new GridBagConstraints();
                    titleConstraints.fill = GridBagConstraints.HORIZONTAL;
                    titleConstraints.gridx = 0;
                    titleConstraints.gridy = i;
                    titleConstraints.weightx = 0;
                    titleConstraints.weighty = 0;
                    titleConstraints.insets = new Insets(0, 5, 0, 5);
                    add(parameterTitles[i], titleConstraints);
                    GridBagConstraints editorConstraints = new GridBagConstraints();
                    editorConstraints.fill = GridBagConstraints.HORIZONTAL;
                    editorConstraints.gridx = 1;
                    editorConstraints.gridy = i;
                    editorConstraints.weightx = 1;
                    editorConstraints.weighty = 0;
                    add(parameterEditors[i], editorConstraints);
                    GridBagConstraints unitConstraints = new GridBagConstraints();
                    unitConstraints.fill = GridBagConstraints.HORIZONTAL;
                    unitConstraints.gridx = 2;
                    unitConstraints.gridy = i;
                    unitConstraints.weightx = 0;
                    unitConstraints.weighty = 0;
                    unitConstraints.insets = new Insets(0, 0, 0, 5);
                    add(parameterUnits[i], unitConstraints);
                } // end for (int i = 0; i < names.length; i++)
            } // end if (names != null)
        } // end if (sector != null)
    }

    public synchronized void clean() {
        removeAll();
        if (parameterEditors != null) {
            for (JTextField parameterEditor : parameterEditors) {
                if (parameterEditor != null) {
                    parameterEditor.getDocument().removeDocumentListener(this);
                    parameterEditor.getDocument().putProperty(TEXT_FIELD_KEY, null);
                }
            }
        }
        parameterEditors = null;
        parameterTitles = null;
        parameterUnits = null;
        sectorRef = null;
    }

    public Color getValidColor() {
        if (validColor == null) {
            validColor = DEFAULT_VALID_COLOR;
        }
        return validColor;
    }

    public void setValidColor(Color validColor) {
        this.validColor = validColor;
    }

    public Color getKoColor() {
        if (koColor == null) {
            koColor = DEFAULT_KO_COLOR;
        }
        return koColor;
    }

    public void setKoColor(Color koColor) {
        this.koColor = koColor;
    }

    public String getKoString() {
        if (koString == null) {
            koString = DEFAULT_KO_STRING;
        }
        return koString;
    }

    public void setKoString(String koString) {
        this.koString = koString;
    }

    /**
     * Updates the {@link JTextField} associated to a {@link Document}, to
     * display whether its text is a valid value.
     * 
     * @param doc
     *            The {@link Document} from which to recover the
     *            {@link JTextField}
     */
    protected synchronized void updateParentTextField(Document doc) {
        if (doc != null) {
            JTextField parent = (JTextField) doc.getProperty(TEXT_FIELD_KEY);
            if (parent != null) {
                if (sectorRef != null) {
                    MathematicSector sector = sectorRef.get();
                    if (sector == null) {
                        parent.setBackground(getValidColor());
                    } else {
                        String tooltip = sector.getValidValuesDescription(parent.getName());
                        if (sector.isValidParameterValue(parent.getName(), parent.getText())) {
                            parent.setBackground(getValidColor());
                            parent.setToolTipText(tooltip);
                            try {
                                sector.setParameterWithString(parent.getName(), parent.getText());
                            } catch (MathematicSectorException e) {
                                // should never happen
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to set sector parameter",
                                        e);
                            }
                        } else {
                            parent.setBackground(getKoColor());
                            if (tooltip == null) {
                                parent.setToolTipText(getKoString());
                            } else {
                                parent.setToolTipText(getKoString() + " - " + tooltip);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        if (e != null) {
            updateParentTextField(e.getDocument());
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        if (e != null) {
            updateParentTextField(e.getDocument());
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        if (e != null) {
            updateParentTextField(e.getDocument());
        }
    }

    /**
     * Sets the associated sector's parameters with the values in fields
     */
    public synchronized void setParameters() {
        if (parameterEditors != null) {
            if (sectorRef != null) {
                MathematicSector sector = sectorRef.get();
                if (sector != null) {
                    boolean ok = true;
                    for (JTextField parameterEditor : parameterEditors) {
                        if (parameterEditor != null) {
                            try {
                                sector.setParameterWithString(parameterEditor.getName(), parameterEditor.getText());
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(this, "An exception occured:\n" + ex.getMessage(),
                                        "Error", JOptionPane.ERROR_MESSAGE);
                                ok = false;
                                break;
                            }
                        }
                    }
                    if (ok) {
                        sector.validate();
                    } else {
                        sector.setEditingMode();
                    }
                }
            }
        }
    }

}
