/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Color;

import javax.swing.SwingUtilities;

import fr.soleil.comete.definition.data.target.matrix.ITextMatrixComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.util.Level;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * This is the component that displays history in a formatted way
 * 
 * @author Rapha&euml;l GIRARDOT
 * @deprecated You should use the LogViewer available in ApplicationUtilities project.
 */
@Deprecated
public class LogViewer extends HtmlFormattedTextViewer implements ITextMatrixComponent {

    private static final long serialVersionUID = 7491088087501996480L;

    protected static final String WARNING = "WARNING";
    protected static final String SEPARATOR = "-";
    protected static final String ZERO = "0";
    protected static final String DEFAULT_ERROR_TEXT = "<html><head></head><body><div color=\"red\"><b><u>No Data</u></b></div></body></html>";

    protected static final String HTML_PARAGRAPH_EMPTY = "<p></p>";
    protected static final String HTML_PARAGRAPH_START = "<p>";
    protected static final String HTML_PARAGRAPH_END = "</p>";
    protected static final String LINE_FEED = ObjectUtils.NEW_LINE;
    protected static final String HTML_LINE_FEED = "<br />";
    protected static final String HTML_LINE_FEED_START = "<br";
    protected static final String HTML_DIV_START = "<div";
    protected static final String HTML_DIV_COLOR = "<div color=\"#";
    protected static final String HTML_DIV_END = "</div>";
    protected static final String HTML_TITLE_START = "<h";
    protected static final String HTML_TAG_END = "\">";
    protected static final String HTML_EMPTY_PAGE = "<html><head></head><body>";
    protected static final String HTML_END = "</body></html>";

    // the type of level to display
    private Level filteredLevel;
    // last data source value
    protected String[] lastLines;
    protected boolean reversed;

    public LogViewer() {
        super();
        lastLines = null;
        setEditable(false);
        reversed = false;
        setDisabledTextColor(Color.RED);
    }

    public boolean isReversed() {
        return reversed;
    }

    public void setReversed(boolean reversed) {
        this.reversed = reversed;
    }

    protected StringBuilder appendHtmlLineToBuffer(StringBuilder buffer, String line) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        if ((line == null) || (line.trim().isEmpty())) {
            buffer.append(HTML_PARAGRAPH_EMPTY);
        } else {
            line = line.trim().replace(LINE_FEED, HTML_LINE_FEED);
            int index = line.indexOf(SEPARATOR);
            String lineToWrite = line;
            Color color = null;
            boolean canWrite = true;
            if (index > -1) {
                String level = line.substring(0, index).trim().toUpperCase();
                if ((filteredLevel == null) || Level.ALL.equals(filteredLevel) || filteredLevel.toString().equals(level)
                        || (Level.WARN.equals(filteredLevel) && WARNING.equals(level))) {
                    color = getColorForStringLevel(level);
                } else {
                    canWrite = false;
                }
            } else if ((filteredLevel != null) && (!Level.ALL.equals(filteredLevel))) {
                canWrite = false;
            }
            if (canWrite) {
                if (color == null) {
                    String ref = lineToWrite.toLowerCase().trim();
                    if (ref.startsWith(HTML_LINE_FEED_START) && ref.startsWith(HTML_DIV_START)
                            && ref.startsWith(HTML_TITLE_START)) {
                        // try to recognize HTML tags that already bring new lines
                        buffer.append(lineToWrite);
                    } else {
                        buffer.append(HTML_PARAGRAPH_START).append(lineToWrite).append(HTML_PARAGRAPH_END);
                    }
                    ref = null;
                } else {
                    lineToWrite = line.substring(index + 1);
                    buffer.append(HTML_DIV_COLOR);
                    appendHexStringToStringBuilder(buffer, color.getRed());
                    appendHexStringToStringBuilder(buffer, color.getGreen());
                    appendHexStringToStringBuilder(buffer, color.getBlue());
                    buffer.append(HTML_TAG_END).append(lineToWrite).append(HTML_DIV_END);
                }
            }
            color = null;
            lineToWrite = null;
        }
        return buffer;
    }

    /**
     * Sets text with previously registered dao content
     */
    protected void updateText() {
        final String text;
        String[] lines = lastLines;
        if (lines == null) {
            text = DEFAULT_ERROR_TEXT;
        } else {
            StringBuilder buffer = new StringBuilder(HTML_EMPTY_PAGE);
            if (reversed) {
                for (int i = lines.length - 1; i >= 0; i--) {
                    buffer = appendHtmlLineToBuffer(buffer, lines[i]);
                }
            } else {
                for (String line : lines) {
                    buffer = appendHtmlLineToBuffer(buffer, line);
                }
            }
            buffer.append(HTML_END);
            text = buffer.toString();
        }
        if (!isMyText(text)) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    setText(text, false);
                    if (reversed) {
                        setCaretPosition(0);
                    } else {
                        setCaretPosition(getDocument().getLength());
                    }
                }
            });
        }
    }

    // Appends integer hexadecimal string representation to StringBuilder
    private StringBuilder appendHexStringToStringBuilder(StringBuilder buffer, int i) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        String hexString = Integer.toHexString(i).toUpperCase().trim();
        if (hexString.length() == 1) {
            buffer.append(ZERO);
        }
        buffer.append(hexString);
        hexString = null;
        return buffer;
    }

    // returns the Color associated with a particular level
    private Color getColorForStringLevel(String level) {
        Color result = null;
        if (level != null) {
            if (Level.ERROR.toString().equals(level)) {
                result = Color.RED;
            } else if (Level.WARN.toString().equals(level) || WARNING.equals(level)) {
                result = new Color(238, 118, 33);
            } else if (Level.INFO.toString().equals(level)) {
                result = Color.BLUE;
            } else if (Level.DEBUG.toString().equals(level)) {
                result = Color.BLACK;
            }
        }
        return result;
    }

    /**
     * Returns the filtered message level
     * 
     * @return A {@link Level}
     */
    public Level getFilteredLevel() {
        return filteredLevel;
    }

    /**
     * Sets the message level to filter
     * 
     * @param filteredLevel A {@link String}. <code>null</code> to have no filter
     */
    public void setFilteredLevel(Level filteredLevel) {
        this.filteredLevel = filteredLevel;
        updateText();
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        // not allowed;
    }

    @Override
    public String[] getFlatStringMatrix() {
        return lastLines;
    }

    @Override
    public String[][] getStringMatrix() {
        // should not be used
        return (String[][]) ArrayUtils.convertArrayDimensionFrom1ToN(lastLines, getMatrixDataHeight(String.class),
                getMatrixDataWidth(String.class));
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        lastLines = value;
        updateText();
    }

    @Override
    public void setStringMatrix(String[][] value) {
        if (value == null) {
            lastLines = null;
        } else {
            lastLines = (String[]) ArrayUtils.convertArrayDimensionFromNTo1(value);
        }
        updateText();
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return (lastLines == null ? 0 : 1);
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        return (lastLines == null ? 0 : lastLines.length);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }
}
