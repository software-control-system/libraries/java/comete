/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.periodictable;

import java.util.ArrayList;

import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.lib.project.ObjectUtils;

/**
 * 
 * @author ADIOUF
 * 
 */
public class EnergyElement implements CometeConstants {

    private String energyType = null;
    private double energyValue;

    private ArrayList<Double> energyValues = new ArrayList<>();
    private ArrayList<String> energyNames = new ArrayList<>();

    public EnergyElement() {
    }

    public String getEnergyType() {
        return energyType;
    }

    public void setEnergyType(String energyType) {
        this.energyType = energyType;
    }

    public ArrayList<Double> getEnergyValues() {
        return energyValues;
    }

    public void setEnergyValues(ArrayList<Double> energyValues) {
        this.energyValues = energyValues;
    }

    public ArrayList<String> getEnergyNames() {
        return energyNames;
    }

    public void setEnergyNames(ArrayList<String> energyNames) {
        this.energyNames = energyNames;
    }

    public Double getEnergyValue() {
        return energyValue;
    }

    public void setEnergyValue(double energyValue) {
        this.energyValue = energyValue;
    }

    public void addEnergy(String name, Double value) {
        energyValues.add(value);
        energyNames.add(name);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(energyType);
        result.append(COMMA).append(energyValue).append(ObjectUtils.NEW_LINE);
        for (int i = 0; i < energyNames.size(); i++) {
            result.append(energyNames.get(i)).append(COMMA).append(energyValues.get(i)).append(ObjectUtils.NEW_LINE);
        }
        return result.toString();
    }
}
