/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.awt.Color;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JComponent;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.util.IntRotationTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.comete.definition.widget.properties.OffsetProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.DataView;
import fr.soleil.lib.project.awt.ColorUtils;

/**
 * Class that gives useful methods to interact with {@link PlotProperties}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class PlotPropertiesTool {

    /**
     * Reads (extracts) some {@link PlotProperties} from a given {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @return Some {@link PlotProperties}. <code>null</code> if <code>dataView</code> is <code>null</code>
     */
    public static PlotProperties readPlotProperties(AbstractDataView dataView) {
        PlotProperties result = null;
        if (dataView != null) {
            result = new PlotProperties(dataView.getViewType(), extractAxis(dataView), dataView.getFormat(),
                    dataView.getUnit(), readBarProperties(dataView), readCurveProperties(dataView),
                    readMarkerProperties(dataView), readErrorProperties(dataView),
                    readTransformationProperties(dataView), readXOffsetProperties(dataView),
                    readInterpolationProperties(dataView), readSmoothingProperties(dataView),
                    readMathProperties(dataView));
        }
        return result;
    }

    /**
     * Writes (applies) some {@link PlotProperties} in an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @param properties The {@link PlotProperties} to write
     */
    public static void writePlotProperties(AbstractDataView dataView, PlotProperties properties) {
        if ((dataView != null) && (properties != null)) {
            dataView.setViewType(properties.getViewType());
            setAxis(dataView, properties.getAxisChoice());
            dataView.setFormat(properties.getFormat());
            dataView.setUnit(properties.getUnit());
            writeBarProperties(dataView, properties.getBar());
            writeCurveProperties(dataView, properties.getCurve());
            writeMarkerProperties(dataView, properties.getMarker());
            writeErrorProperties(dataView, properties.getError());
            writeTransformationProperties(dataView, properties.getTransform());
            writeXOffsetProperties(dataView, properties.getXOffset());
            writeInterpolationProperties(dataView, properties.getInterpolation());
            writeSmoothingProperties(dataView, properties.getSmoothing());
            writeMathProperties(dataView, properties.getMath());
        }
    }

    /**
     * Reads (extracts) some {@link BarProperties} from a given {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @return Some {@link BarProperties}. <code>null</code> if <code>dataView</code> is <code>null</code>
     */
    public static BarProperties readBarProperties(AbstractDataView dataView) {
        BarProperties result = null;
        if (dataView != null) {
            result = new BarProperties(ColorTool.getCometeColor(dataView.getFillColor()), dataView.getBarWidth(),
                    dataView.getFillStyle(), dataView.getFillMethod());
        }
        return result;
    }

    /**
     * Writes (applies) some {@link writeBarProperties} in an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @param properties The {@link writeBarProperties} to write
     */
    public static void writeBarProperties(AbstractDataView dataView, BarProperties properties) {
        if ((dataView != null) && (properties != null)) {
            dataView.setFillColor(ColorTool.getColor(properties.getFillColor()));
            dataView.setBarWidth(properties.getWidth());
            dataView.setFillStyle(properties.getFillStyle());
            dataView.setFillMethod(properties.getFillingMethod());
        }
    }

    /**
     * Reads (extracts) some {@link CurveProperties} from a given {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @return Some {@link CurveProperties}. <code>null</code> if <code>dataView</code> is <code>null</code>
     */
    public static CurveProperties readCurveProperties(AbstractDataView dataView) {
        CurveProperties result = null;
        if (dataView != null) {
            result = new CurveProperties(ColorTool.getCometeColor(dataView.getColor()), dataView.getLineWidth(),
                    dataView.getStyle(), dataView.getDisplayName());
        }
        return result;
    }

    /**
     * Writes (applies) some {@link CurveProperties} in an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @param properties The {@link CurveProperties} to write
     */
    public static void writeCurveProperties(AbstractDataView dataView, CurveProperties properties) {
        if ((dataView != null) && (properties != null)) {
            dataView.setColor(ColorTool.getColor(properties.getColor()));
            dataView.setLineWidth(properties.getWidth());
            dataView.setStyle(properties.getLineStyle());
            dataView.setDisplayName(properties.getName());
        }
    }

    /**
     * Reads (extracts) some {@link MarkerProperties} from a given {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @return Some {@link MarkerProperties}. <code>null</code> if <code>dataView</code> is <code>null</code>
     */
    public static MarkerProperties readMarkerProperties(AbstractDataView dataView) {
        MarkerProperties result = null;
        if (dataView != null) {
            result = new MarkerProperties(ColorTool.getCometeColor(dataView.getMarkerColor()), dataView.getMarkerSize(),
                    dataView.getMarker(), dataView.isLabelVisible(), dataView.isSamplingAllowed());
        }
        return result;
    }

    /**
     * Writes (applies) some {@link MarkerProperties} in an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @param properties The {@link MarkerProperties} to write
     */
    public static void writeMarkerProperties(AbstractDataView dataView, MarkerProperties properties) {
        if ((dataView != null) && (properties != null)) {
            dataView.setMarkerColor(ColorTool.getColor(properties.getColor()));
            dataView.setMarkerSize(properties.getSize());
            dataView.setMarker(properties.getStyle());
            dataView.setLabelVisible(properties.isLegendVisible());
            dataView.setSamplingAllowed(properties.isSamplingAllowed());
        }
    }

    /**
     * Reads (extracts) some {@link ErrorProperties} from a given {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @return Some {@link ErrorProperties}. <code>null</code> if <code>dataView</code> is <code>null</code>
     */
    public static ErrorProperties readErrorProperties(AbstractDataView dataView) {
        ErrorProperties result = null;
        if (dataView != null) {
            result = new ErrorProperties(ColorTool.getCometeColor(dataView.getErrorColor()), dataView.isErrorVisible());
        }
        return result;
    }

    /**
     * Writes (applies) some {@link ErrorProperties} in an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @param properties The {@link ErrorProperties} to write
     */
    public static void writeErrorProperties(AbstractDataView dataView, ErrorProperties properties) {
        if ((dataView != null) && (properties != null)) {
            dataView.setErrorColor(ColorTool.getColor(properties.getColor()));
            dataView.setErrorVisible(properties.isVisible());
        }
    }

    /**
     * Reads (extracts) some {@link TransformationProperties} from a given {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @return Some {@link TransformationProperties}. <code>null</code> if <code>dataView</code> is <code>null</code>
     */
    public static TransformationProperties readTransformationProperties(AbstractDataView dataView) {
        TransformationProperties result = null;
        if (dataView != null) {
            result = new TransformationProperties(dataView.getA0(), dataView.getA1(), dataView.getA2());
        }
        return result;
    }

    /**
     * Reads (extracts) some x {@link OffsetProperties} from a given {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @return Some {@link OffsetProperties}. <code>null</code> if <code>dataView</code> is <code>null</code>
     */
    public static OffsetProperties readXOffsetProperties(AbstractDataView dataView) {
        OffsetProperties result = null;
        if (dataView != null) {
            result = new OffsetProperties(dataView.getA0X());
        }
        return result;
    }

    /**
     * Writes (applies) some {@link TransformationProperties} in an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @param properties The {@link TransformationProperties} to write
     */
    public static void writeTransformationProperties(AbstractDataView dataView, TransformationProperties properties) {
        if ((dataView != null) && (properties != null)) {
            dataView.setA0(properties.getA0());
            dataView.setA1(properties.getA1());
            dataView.setA2(properties.getA2());
        }
    }

    /**
     * Writes (applies) some x {@link OffsetProperties} in an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @param properties The {@link OffsetProperties} to write
     */
    public static void writeXOffsetProperties(AbstractDataView dataView, OffsetProperties properties) {
        if ((dataView != null) && (properties != null)) {
            dataView.setA0X(properties.getA0());
        }
    }

    /**
     * Reads (extracts) some {@link InterpolationProperties} from a given {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @return Some {@link InterpolationProperties}. <code>null</code> if <code>dataView</code> is <code>null</code>
     */
    public static InterpolationProperties readInterpolationProperties(AbstractDataView dataView) {
        InterpolationProperties result = null;
        if (dataView != null) {
            result = new InterpolationProperties(dataView.getInterpolationMethod(), dataView.getInterpolationStep(),
                    dataView.getHermiteBias(), dataView.getHermiteTension());
        }
        return result;
    }

    /**
     * Writes (applies) some {@link InterpolationProperties} in an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @param properties The {@link InterpolationProperties} to write
     */
    public static void writeInterpolationProperties(AbstractDataView dataView, InterpolationProperties properties) {
        if ((dataView != null) && (properties != null)) {
            dataView.setInterpolationMethod(properties.getInterpolationMethod());
            dataView.setInterpolationStep(properties.getInterpolationStep());
            dataView.setHermiteBias(properties.getHermiteBias());
            dataView.setHermiteTension(properties.getHermiteTension());
        }
    }

    /**
     * Reads (extracts) some {@link SmoothingProperties} from a given {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @return Some {@link SmoothingProperties}. <code>null</code> if <code>dataView</code> is <code>null</code>
     */
    public static SmoothingProperties readSmoothingProperties(AbstractDataView dataView) {
        SmoothingProperties result = null;
        if (dataView != null) {
            result = new SmoothingProperties(dataView.getSmoothingMethod(), dataView.getSmoothingNeighbors(),
                    dataView.getSmoothingGaussSigma(), dataView.getSmoothingExtrapolation());
        }
        return result;
    }

    /**
     * Writes (applies) some {@link SmoothingProperties} in an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @param properties The {@link SmoothingProperties} to write
     */
    public static void writeSmoothingProperties(AbstractDataView dataView, SmoothingProperties properties) {
        if ((dataView != null) && (properties != null)) {
            dataView.setSmoothingMethod(properties.getMethod());
            dataView.setSmoothingNeighbors(properties.getNeighbors());
            dataView.setSmoothingGaussSigma(properties.getGaussSigma());
            dataView.setSmoothingExtrapolation(properties.getExtrapolation());
        }
    }

    /**
     * Reads (extracts) some {@link MathProperties} from a given {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @return Some {@link MathProperties}. <code>null</code> if <code>dataView</code> is <code>null</code>
     */
    public static MathProperties readMathProperties(AbstractDataView dataView) {
        MathProperties result = null;
        if (dataView != null) {
            result = new MathProperties(dataView.getMathFunction());
        }
        return result;
    }

    /**
     * Writes (applies) some {@link MathProperties} in an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @param properties The {@link MathProperties} to write
     */
    public static void writeMathProperties(AbstractDataView dataView, MathProperties properties) {
        if ((dataView != null) && (properties != null)) {
            dataView.setMathFunction(properties.getFunction());
        }
    }

    /**
     * Tries to recover the axis associated with an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @return An <code>int</code>
     */
    @SuppressWarnings("deprecation")
    public static int extractAxis(AbstractDataView dataView) {
        int result = IChartViewer.Y1;
        if (dataView instanceof DataView) {
            Integer axis = (Integer) dataView.getAxis();
            if (axis != null) {
                result = axis.intValue();
            }
        } else if (dataView instanceof fr.soleil.comete.swing.chart.JLDataView) {
            fr.soleil.comete.swing.chart.JLAxis axis = (fr.soleil.comete.swing.chart.JLAxis) dataView.getAxis();
            if (axis != null) {
                JComponent parent = axis.getParent();
                if (parent instanceof fr.soleil.comete.swing.chart.JLChart) {
                    fr.soleil.comete.swing.chart.JLChart chart = (fr.soleil.comete.swing.chart.JLChart) parent;
                    if (axis == chart.getXAxis()) {
                        result = IChartViewer.X;
                    } else if (axis == chart.getY1Axis()) {
                        result = IChartViewer.Y1;
                    } else if (axis == chart.getY2Axis()) {
                        result = IChartViewer.Y2;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Tries to change the axis associated with an {@link AbstractDataView}
     * 
     * @param dataView The {@link AbstractDataView}
     * @param axis The axis to set
     */
    @SuppressWarnings("deprecation")
    public static void setAxis(AbstractDataView dataView, int axis) {
        if (dataView instanceof DataView) {
            ((DataView) dataView).setAxis(axis);
        } else if (dataView instanceof fr.soleil.comete.swing.chart.JLDataView) {
            fr.soleil.comete.swing.chart.JLAxis tmp = (fr.soleil.comete.swing.chart.JLAxis) dataView.getAxis();
            if (tmp != null) {
                JComponent parent = tmp.getParent();
                if (parent instanceof fr.soleil.comete.swing.chart.JLChart) {
                    fr.soleil.comete.swing.chart.JLChart chart = (fr.soleil.comete.swing.chart.JLChart) parent;
                    chart.addDataViewToAxis((fr.soleil.comete.swing.chart.JLDataView) dataView, axis, false);
                }
            }
        }
    }

    /**
     * Returns whether an {@link AbstractDataView} is displayed on y1 axis
     * 
     * @param dataView The {@link AbstractDataView}
     * @return A <code>boolean</code> value
     */
    @SuppressWarnings("deprecation")
    public static boolean isY1View(AbstractDataView dataView) {
        boolean y1;
        if (dataView instanceof DataView) {
            y1 = (((DataView) dataView).getAxis().intValue() == IChartViewer.Y1);
        } else if (dataView instanceof fr.soleil.comete.swing.chart.JLDataView) {
            fr.soleil.comete.swing.chart.JLAxis tmp = (fr.soleil.comete.swing.chart.JLAxis) dataView.getAxis();
            if (tmp == null) {
                y1 = false;
            } else {
                JComponent parent = tmp.getParent();
                if (parent instanceof fr.soleil.comete.swing.chart.JLChart) {
                    fr.soleil.comete.swing.chart.JLChart chart = (fr.soleil.comete.swing.chart.JLChart) parent;
                    y1 = (chart.getAxis(IChartViewer.Y1) == tmp);
                } else {
                    y1 = false;
                }
            }
        } else {
            y1 = false;
        }
        return y1;
    }

    /**
     * Recovers some {@link PlotProperties} from a {@link ConcurrentHashMap}, and initializes it and
     * add it in {@link ConcurrentHashMap} if not yet registered
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param displayName The display name to use in case of {@link PlotProperties} creation.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @param lineStyleRotationTool The {@link IntRotationTool} used to rotate line style
     * @param lineWidthRotationTool The {@link IntRotationTool} used to rotate line width
     * @param markerStyleRotationTool The {@link IntRotationTool} used to rotate marker style
     * @param markerSizeRotationTool The {@link IntRotationTool} used to rotate marker size
     * @param fillStyleRotationTool The {@link IntRotationTool} used to rotate fill style
     * @param barWidthRotationTool The {@link IntRotationTool} used to rotate bar width
     * @param viewTypeRotationTool The {@link IntRotationTool} used to rotate view type
     * @param axisRotationTool The {@link IntRotationTool} used to rotate view's axis
     * @param colorRotationTools The {@link ColorUtils}s that may be used to rotate the
     *            colors for {@link PlotProperties} initializations. Expected order is:
     *            <ol>
     *            <li>curve color {@link ColorUtils}</li>
     *            <li>bar fill color {@link ColorUtils}</li>
     *            <li>marker color {@link ColorUtils}</li>
     *            </ol>
     * @return Some {@link PlotProperties}
     */
    public static PlotProperties getInitializedPlotProperties(String id, String displayName,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap, IntRotationTool lineStyleRotationTool,
            IntRotationTool lineWidthRotationTool, IntRotationTool markerStyleRotationTool,
            IntRotationTool markerSizeRotationTool, IntRotationTool fillStyleRotationTool,
            IntRotationTool barWidthRotationTool, IntRotationTool viewTypeRotationTool,
            IntRotationTool axisRotationTool, ColorUtils... colorRotationTools) {
        return getInitializedPlotProperties(id, displayName, plotPropertiesMap, lineStyleRotationTool,
                lineWidthRotationTool, markerStyleRotationTool, markerSizeRotationTool, fillStyleRotationTool,
                barWidthRotationTool, viewTypeRotationTool, axisRotationTool, null, colorRotationTools);
    }

    /**
     * Recovers some {@link PlotProperties} from a {@link ConcurrentHashMap}, and initializes it and
     * add it in {@link ConcurrentHashMap} if not yet registered
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param displayName The display name to use in case of {@link PlotProperties} creation.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @param lineStyleRotationTool The {@link IntRotationTool} used to rotate line style
     * @param lineWidthRotationTool The {@link IntRotationTool} used to rotate line width
     * @param markerStyleRotationTool The {@link IntRotationTool} used to rotate marker style
     * @param markerSizeRotationTool The {@link IntRotationTool} used to rotate marker size
     * @param fillStyleRotationTool The {@link IntRotationTool} used to rotate fill style
     * @param barWidthRotationTool The {@link IntRotationTool} used to rotate bar width
     * @param viewTypeRotationTool The {@link IntRotationTool} used to rotate view type
     * @param axisRotationTool The {@link IntRotationTool} used to rotate view's axis
     * @param refColorToAvoid The {@link Color} from which generated colors should not be too near.
     * @param colorRotationTools The {@link ColorUtils}s that may be used to rotate the
     *            colors for {@link PlotProperties} initializations. Expected order is:
     *            <ol>
     *            <li>curve color {@link ColorUtils}</li>
     *            <li>bar fill color {@link ColorUtils}</li>
     *            <li>marker color {@link ColorUtils}</li>
     *            </ol>
     * @return Some {@link PlotProperties}
     */
    public static PlotProperties getInitializedPlotProperties(String id, String displayName,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap, IntRotationTool lineStyleRotationTool,
            IntRotationTool lineWidthRotationTool, IntRotationTool markerStyleRotationTool,
            IntRotationTool markerSizeRotationTool, IntRotationTool fillStyleRotationTool,
            IntRotationTool barWidthRotationTool, IntRotationTool viewTypeRotationTool,
            IntRotationTool axisRotationTool, Color refColorToAvoid, ColorUtils... colorRotationTools) {
        PlotProperties plotProperties;
        if (id == null) {
            plotProperties = null;
        } else {
            plotProperties = plotPropertiesMap.get(id);
            if (plotProperties == null) {
                if ((colorRotationTools == null) || (colorRotationTools.length == 0)) {
                    plotProperties = new PlotProperties();
                } else {
                    plotProperties = new PlotProperties(
                            ColorTool.getCometeColor(colorRotationTools[0].getNextColorFarEnoughFrom(refColorToAvoid)));
                    if (colorRotationTools.length > 1) {
                        if (colorRotationTools[1] != null) {
                            plotProperties.getBar().setFillColor(ColorTool
                                    .getCometeColor(colorRotationTools[1].getNextColorFarEnoughFrom(refColorToAvoid)));
                        }
                        if (colorRotationTools.length > 2) {
                            if (colorRotationTools[2] != null) {
                                plotProperties.getMarker().setColor(ColorTool.getCometeColor(
                                        colorRotationTools[2].getNextColorFarEnoughFrom(refColorToAvoid)));
                            }
                        }
                    }
                }
                CurveProperties curve = plotProperties.getCurve();
                curve.setLineStyle(getNextValue(lineStyleRotationTool, curve.getLineStyle()));
                curve.setWidth(getNextValue(lineWidthRotationTool, curve.getWidth()));
                MarkerProperties marker = plotProperties.getMarker();
                marker.setStyle(getNextValue(markerStyleRotationTool, marker.getStyle()));
                marker.setSize(getNextValue(markerSizeRotationTool, marker.getSize()));
                BarProperties bar = plotProperties.getBar();
                bar.setFillStyle(getNextValue(fillStyleRotationTool, bar.getFillStyle()));
                bar.setWidth(getNextValue(barWidthRotationTool, bar.getWidth()));
                plotProperties.setViewType(getNextValue(viewTypeRotationTool, plotProperties.getViewType()));
                plotProperties.setAxisChoice(getNextValue(axisRotationTool, plotProperties.getAxisChoice()));
                curve.setName(displayName == null ? id : displayName);
                PlotProperties temp = plotPropertiesMap.putIfAbsent(id, plotProperties);
                if (temp != null) {
                    plotProperties = temp;
                }
            }
        }
        return plotProperties;
    }

    protected static int getNextValue(IntRotationTool intRotationTool, int defaultValue) {
        int result;
        if (intRotationTool == null) {
            result = defaultValue;
        } else {
            result = intRotationTool.getNextInt(defaultValue);
        }
        return result;
    }

    /**
     * Recovers some {@link PlotProperties} from a {@link ConcurrentHashMap}, but does not try to
     * initialize it if not yet registered
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @return Some {@link PlotProperties}. <code>null</code> If no such one in {@link ConcurrentHashMap}
     */
    public static PlotProperties getNotInitializedPlotProperties(String id,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap) {
        return (id == null ? null : plotPropertiesMap.get(id));
    }

    /**
     * Calls {@link #getNotInitializedPlotProperties(String,ConcurrentHashMap)}, and extracts, if
     * possible, the associated {@link BarProperties}
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @return Some {@link BarProperties}. <code>null</code> If it could not be extracted from {@link ConcurrentHashMap}
     */
    public static BarProperties getNotInitializedBarProperties(String id,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap) {
        BarProperties properties = null;
        PlotProperties plotProperties = getNotInitializedPlotProperties(id, plotPropertiesMap);
        if (plotProperties != null) {
            properties = plotProperties.getBar();
        }
        return properties;
    }

    /**
     * Calls {@link #getNotInitializedPlotProperties(String,ConcurrentHashMap)}, and extracts, if
     * possible, the associated {@link CurveProperties}
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @return Some {@link CurveProperties}. <code>null</code> If it could not be extracted from
     *         {@link ConcurrentHashMap}
     */
    public static CurveProperties getNotInitializedCurveProperties(String id,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap) {
        CurveProperties properties = null;
        PlotProperties plotProperties = getNotInitializedPlotProperties(id, plotPropertiesMap);
        if (plotProperties != null) {
            properties = plotProperties.getCurve();
        }
        return properties;
    }

    /**
     * Calls {@link #getNotInitializedPlotProperties(String,ConcurrentHashMap)}, and extracts, if
     * possible, the associated {@link InterpolationProperties}
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @return Some {@link InterpolationProperties}. <code>null</code> If it could not be extracted
     *         from {@link ConcurrentHashMap}
     */
    public static InterpolationProperties getNotInitializedInterpolationProperties(String id,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap) {
        InterpolationProperties properties = null;
        PlotProperties plotProperties = getNotInitializedPlotProperties(id, plotPropertiesMap);
        if (plotProperties != null) {
            properties = plotProperties.getInterpolation();
        }
        return properties;
    }

    /**
     * Calls {@link #getNotInitializedPlotProperties(String,ConcurrentHashMap)}, and extracts, if
     * possible, the associated {@link MarkerProperties}
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @return Some {@link MarkerProperties}. <code>null</code> If it could not be extracted from
     *         {@link ConcurrentHashMap}
     */
    public static MarkerProperties getNotInitializedMarkerProperties(String id,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap) {
        MarkerProperties properties = null;
        PlotProperties plotProperties = getNotInitializedPlotProperties(id, plotPropertiesMap);
        if (plotProperties != null) {
            properties = plotProperties.getMarker();
        }
        return properties;
    }

    /**
     * Calls {@link #getNotInitializedPlotProperties(String,ConcurrentHashMap)}, and extracts, if
     * possible, the associated {@link ErrorProperties}
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @return Some {@link ErrorProperties}. <code>null</code> If it could not be extracted from
     *         {@link ConcurrentHashMap}
     */
    public static ErrorProperties getNotInitializedErrorProperties(String id,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap) {
        ErrorProperties properties = null;
        PlotProperties plotProperties = getNotInitializedPlotProperties(id, plotPropertiesMap);
        if (plotProperties != null) {
            properties = plotProperties.getError();
        }
        return properties;
    }

    /**
     * Calls {@link #getNotInitializedPlotProperties(String,ConcurrentHashMap)}, and extracts, if
     * possible, the associated {@link MathProperties}
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @return Some {@link MathProperties}. <code>null</code> If it could not be extracted from
     *         {@link ConcurrentHashMap}
     */
    public static MathProperties getNotInitializedMathProperties(String id,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap) {
        MathProperties properties = null;
        PlotProperties plotProperties = getNotInitializedPlotProperties(id, plotPropertiesMap);
        if (plotProperties != null) {
            properties = plotProperties.getMath();
        }
        return properties;
    }

    /**
     * Calls {@link #getNotInitializedPlotProperties(String,ConcurrentHashMap)}, and extracts, if
     * possible, the associated {@link SmoothingProperties}
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @return Some {@link SmoothingProperties}. <code>null</code> If it could not be extracted from
     *         {@link ConcurrentHashMap}
     */
    public static SmoothingProperties getNotInitializedSmoothingProperties(String id,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap) {
        SmoothingProperties properties = null;
        PlotProperties plotProperties = getNotInitializedPlotProperties(id, plotPropertiesMap);
        if (plotProperties != null) {
            properties = plotProperties.getSmoothing();
        }
        return properties;
    }

    /**
     * Calls {@link #getNotInitializedPlotProperties(String,ConcurrentHashMap)}, and extracts, if
     * possible, the associated {@link TransformationProperties}
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @return Some {@link TransformationProperties}. <code>null</code> If it could not be extracted
     *         from {@link ConcurrentHashMap}
     */
    public static TransformationProperties getNotInitializedTransformationProperties(String id,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap) {
        TransformationProperties properties = null;
        PlotProperties plotProperties = getNotInitializedPlotProperties(id, plotPropertiesMap);
        if (plotProperties != null) {
            properties = plotProperties.getTransform();
        }
        return properties;
    }

    /**
     * Calls {@link #getNotInitializedPlotProperties(String,ConcurrentHashMap)}, and extracts, if
     * possible, the associated {@link OffsetProperties}
     * 
     * @param id The key in {@link ConcurrentHashMap}. This key will most likely represent an {@link AbstractDataView}
     *            id.
     * @param plotPropertiesMap The {@link ConcurrentHashMap} that stores the {@link PlotProperties} . This
     *            {@link ConcurrentHashMap} must not be <code>null</code>.
     * @return Some {@link OffsetProperties}. <code>null</code> If it could not be extracted
     *         from {@link ConcurrentHashMap}
     */
    public static OffsetProperties getNotInitializedXOffsetProperties(String id,
            ConcurrentHashMap<String, PlotProperties> plotPropertiesMap) {
        OffsetProperties properties = null;
        PlotProperties plotProperties = getNotInitializedPlotProperties(id, plotPropertiesMap);
        if (plotProperties != null) {
            properties = plotProperties.getXOffset();
        }
        return properties;
    }

}
