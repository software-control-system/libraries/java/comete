/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import net.miginfocom.swing.MigLayout;

public class DateTimeFormatChooser extends JDialog {

    private static final long serialVersionUID = 2627928994281465132L;

    private static final String NONE = "NONE";
    protected static final String[] DATE_COMBO_CHOICE = { NONE, IDateConstants.DATE_CLASSIC_FORMAT,
            IDateConstants.YEAR_LONG_FORMAT, IDateConstants.DATE_SHORT_FORMAT, IDateConstants.DATE_DAY_MONTH_FORMAT,
            IDateConstants.DATE_DAY_FORMAT };
    protected static final String[] TIME_COMBO_CHOICE = { NONE, IDateConstants.TIME_LONG_FORMAT,
            IDateConstants.TIME_SHORT_FORMAT, IDateConstants.TIME_HOUR };

    protected JPanel mainPanel;
    protected JPanel panelCombo;
    protected JPanel panelBtn;

    protected JLabel labelDate;
    protected JLabel labelTime;
    protected JButton btnOk;
    protected JButton btnCancel;

    protected JComboBox<String> comboDate;
    protected JComboBox<String> comboTime;

    protected String[] selectedFormat;

    public DateTimeFormatChooser(Component owner) {
        super(WindowSwingUtils.getWindowForComponent(owner));
        setModal(true);
        setAlwaysOnTop(true);

        mainPanel = new JPanel(new BorderLayout());
        panelCombo = new JPanel(new MigLayout("fill, wrap 2, insets 2"));
        panelBtn = new JPanel();
        labelDate = new JLabel("Date : ");
        labelTime = new JLabel("Time : ");
        btnOk = new JButton("OK");
        btnCancel = new JButton("Cancel");

        this.setModal(true);
        this.setTitle("Choose Date & Time format");
        this.setSize(250, 250);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setLocationRelativeTo(getOwner());

        comboDate = new JComboBox<>(DATE_COMBO_CHOICE);
        comboTime = new JComboBox<>(TIME_COMBO_CHOICE);

        mainPanel.add(panelCombo, BorderLayout.CENTER);
        panelCombo.add(labelDate);
        panelCombo.add(comboDate);

        panelCombo.add(labelTime);
        panelCombo.add(comboTime);

        mainPanel.add(panelBtn, BorderLayout.SOUTH);
        panelBtn.add(btnOk);
        panelBtn.add(btnCancel);

        btnOk.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String selectedDateFormat = comboDate.getSelectedItem().toString();
                String selectedTimeFormat = comboTime.getSelectedItem().toString();
                if (selectedDateFormat.equals(NONE) && !selectedTimeFormat.equals(NONE)) {
                    selectedFormat = new String[] { selectedTimeFormat };
                }
                if (selectedDateFormat.equals(NONE) && selectedTimeFormat.equals(NONE)) {
                    selectedFormat = null;
                    setVisible(false);
                }
                if (selectedTimeFormat.equals(NONE) && !selectedDateFormat.equals(NONE)) {
                    selectedFormat = new String[] { selectedDateFormat };
                } else if (!selectedDateFormat.equals(NONE) && !selectedTimeFormat.equals(NONE)) {
                    selectedFormat = new String[] { selectedDateFormat, selectedTimeFormat };
                }
                setVisible(false);
            }
        });
        btnCancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                setSelectTimeFormat(NONE);
                setSelectDateFormat(NONE);
                selectedFormat = null;
                setVisible(false);
            }
        });

        this.setContentPane(mainPanel);

    }

    @Override
    public void setVisible(boolean b) {
        this.setLocationRelativeTo(getOwner());
        super.setVisible(b);
    }

    public String[] getSelectedFormat() {
        return selectedFormat == null ? null : selectedFormat.clone();
    }

    public void setSelectTimeFormat(String timeFormat) {
        if (timeFormat != null) {
            comboTime.setSelectedItem(timeFormat);
        }
    }

    public void setSelectDateFormat(String dateFormat) {
        if (dateFormat != null) {
            comboDate.setSelectedItem(dateFormat);
        }
    }

    public static void main(String[] args) {
        DateTimeFormatChooser dateTimeFormatChooser = new DateTimeFormatChooser(null);
        dateTimeFormatChooser.setSelectTimeFormat(IDateConstants.DATE_MONTH_FORMAT);
        dateTimeFormatChooser.setSelectDateFormat(IDateConstants.YEAR_LONG_FORMAT);
        dateTimeFormatChooser.setVisible(true);
        String[] result = dateTimeFormatChooser.getSelectedFormat();
        if (result != null) {
            System.out.println("result=" + Arrays.toString(result));
            System.out.println("Result length=" + result.length);
        }
        System.exit(0);
    }
}
