/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.periodictable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.data.information.NumberInformation;
import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.swing.AbstractPanel;
import fr.soleil.data.mediator.Mediator;

public abstract class PeriodicTable extends AbstractPanel {

    private static final long serialVersionUID = 703986527959854580L;

    private static final String PERIODIC_ELEMENTS_FILE = "PeriodicElements.txt";

    public static final int ATOMIC_NUMBER_MODE = 0;
    public static final int SYMBOL_MODE = 1;
    public static final int NAME_MODE = 2;

    private int mode = ATOMIC_NUMBER_MODE;

    private static String composantModel;

    private String accesPath;
    // private File fileCsv;
    private InputStream inputStreamCsv;
    private List<ElementButton> elements;
    private ElementButton selectedElement;

    private JPanel elementView;
    private ElementButton currentView;

    private final Collection<IPeriodicTableListener> listeners;

    protected final static String REF_ENERGY = "Energy";

    ////////////////////
    // Public methods //
    ////////////////////

    public PeriodicTable() {
        super();
        elements = new ArrayList<>();
        listeners = new ArrayList<>();
        initialize();
    }

    @Override
    public Font getFont() {
        Font font = super.getFont();
        if ((elements != null) && (elements.size() > 0)) {
            font = elements.get(0).getFont();
        }
        return font;
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        if (elements != null) {
            for (ElementButton element : elements) {
                element.setFont(font);
            }
        }
        if (currentView != null) {
            currentView.setFont(font);
        }
        setFont(elementView, font);
    }

    protected void setFont(Component comp, Font font) {
        if (comp instanceof JLabel) {
            comp.setFont(font);
        } else if (comp instanceof JPanel) {
            JPanel panel = (JPanel) comp;
            for (Component child : panel.getComponents()) {
                setFont(child, font);
            }
        }
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {

        initializeElementFile();
        initializeElementsTable();

        this.setLayout(new BorderLayout());
        this.add(new JPanel(), BorderLayout.NORTH);
        this.add(getElementView(), BorderLayout.CENTER);

    }

    private void initializeElementFile() {
        inputStreamCsv = PeriodicTable.class.getResourceAsStream(PERIODIC_ELEMENTS_FILE);
    }

    public void addPeriodicTableListener(IPeriodicTableListener e) {
        if (!listeners.contains(e)) {
            listeners.add(e);
        }
    }

    public void removePeriodicTableListener(IPeriodicTableListener e) {
        if (listeners.contains(e)) {
            synchronized (listeners) {
                listeners.remove(e);
            }
        }
    }

    public void setData(ElementModel data) {
        if (currentView != null) {
            currentView.setElement(data);
            currentView.refresh();
        }
    }

    private void initializeElementsTable() {
        if (inputStreamCsv != null) {
            try {
                BufferedReader cin = null;
                cin = new BufferedReader(new InputStreamReader(inputStreamCsv));
                String line;
                try {
                    if ((line = cin.readLine()) != null) {
                        composantModel = line;
                        while ((line = cin.readLine()) != null) {
                            final ElementModel datas = new ElementModel(line);
                            ElementButton button = new ElementButton(datas);
                            button.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {

                                    setData(datas);
                                    selectedElementChanged(datas);
                                }
                            });
                            elements.add(button);
                        }
                    }
                } catch (IOException ioe) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to read" + PERIODIC_ELEMENTS_FILE,
                            ioe);
                } finally {
                    cin.close();
                }
            } catch (Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to initialize elements table", e);
            }
        }
    }

    protected void selectedElementChanged(ElementModel datas) {
        if (datas != null) {
            String atomicNb = datas.getAtomicNumber();
            String symbol = datas.getSymbole();
            String name = datas.getElementName();

            String value = null;
            switch (mode) {
                case ATOMIC_NUMBER_MODE:
                    value = atomicNb;
                    warnMediators(new NumberInformation(this, Integer.parseInt(atomicNb)));
                    break;
                case SYMBOL_MODE:
                    value = symbol;
                    warnMediators(new TextInformation(this, symbol));
                    break;
                case NAME_MODE:
                    value = name;
                    warnMediators(new TextInformation(this, name));
                    break;
                default:
                    break;
            }

            for (IPeriodicTableListener listener : listeners) {
                listener.selectedAtomicNumber(atomicNb);
                listener.selectedName(name);
                listener.selectedSymbole(symbol);
                listener.selectedValueChanged(value);
            }

        }
    }

    public void setValue(String value) {
        ElementButton founded_element = searchElementFromValue(value);
        if (founded_element != null) {
            setData(founded_element.getElement());
        }
    }

    /**
     * 
     * @param text
     * @return JLabel
     */
    public JLabel formatLabel(String text) {
        JLabel label = new JLabel(text);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setBackground(Color.GREEN);
        label.setFont(getFont());
        return label;
    }

    /**
     * 
     * @return JPanel
     */
    public JPanel getElementView() {

        if (elements.isEmpty()) {
            JLabel label = new JLabel("No Element found");
            elementView = new JPanel();
            elementView.add(label);
            return elementView;
        }

        if (elementView == null) {
            elementView = new JPanel();
            JPanel linePanel1 = new JPanel(new GridLayout(1, 18));
            linePanel1.add(elements.get(0));

            for (int j = 0; j < 7; j++) {
                linePanel1.add(new JPanel());
            }

            currentView = new ElementButton(ElementModel.defaultElement());
            linePanel1.add(currentView);

            for (int j = 0; j < 8; j++) {
                linePanel1.add(new JPanel());
            }
            linePanel1.add(elements.get(1));

            // Line 2
            JPanel linePanel2 = new JPanel(new GridLayout(1, 18));
            for (int j = 2; j < 4; j++) {
                linePanel2.add(elements.get(j));
            }
            for (int j = 0; j < 10; j++) {
                linePanel2.add(new JPanel());
            }
            for (int j = 4; j < 10; j++) {
                linePanel2.add(elements.get(j));
            }

            // Line 3
            JPanel linePanel3 = new JPanel(new GridLayout(1, 18));
            for (int j = 10; j < 12; j++) {
                linePanel3.add(elements.get(j));
            }
            for (int i = 0; i < 10; i++) {
                linePanel3.add(new JPanel());
            }
            for (int j = 12; j < 18; j++) {
                linePanel3.add(elements.get(j));
            }

            // Line 4
            JPanel linePanel4 = new JPanel(new GridLayout(1, 18));
            for (int i = 0, j = 18; i < 18; i++, j++) {
                linePanel4.add(elements.get(j));
            }

            // Line 5
            JPanel linePanel5 = new JPanel(new GridLayout(1, 18));
            for (int i = 0, j = 36; i < 18; i++, j++) {
                linePanel5.add(elements.get(j));
            }

            // Line 6
            JPanel linePanel6 = new JPanel(new GridLayout(1, 18));
            for (int i = 0, j = 54; i < 3; i++, j++) {
                linePanel6.add(elements.get(j));
            }
            linePanel6.add(formatLabel("57 to 71"));
            for (int i = 0, j = 71; i < 15; i++, j++) {
                linePanel6.add(elements.get(j));
            }

            // Line 7
            JPanel linePanel7 = new JPanel(new GridLayout(1, 18));
            for (int i = 0, j = 86; i < 2; i++, j++) {
                linePanel7.add(elements.get(j));
            }
            linePanel7.add(formatLabel("89 to 103"));
            for (int i = 0, j = 103; i < 15; i++, j++) {
                if (j < elements.size() && elements.get(j) != null) {
                    linePanel7.add(elements.get(j));
                } else {
                    linePanel7.add(new JPanel());
                }
            }

            // Line 8
            JPanel linePanel8 = new JPanel(new GridLayout(1, 18));
            for (int i = 0; i < 2; i++) {
                linePanel8.add(new JPanel());
            }
            for (int i = 0, j = 56; i < 15; i++, j++) {
                linePanel8.add(elements.get(j));
            }
            linePanel8.add(new JPanel());

            // Line 9
            JPanel linePanel9 = new JPanel(new GridLayout(1, 18));
            for (int i = 0; i < 2; i++) {
                linePanel9.add(new JPanel());
            }
            for (int i = 0, j = 88; i < 15; i++, j++) {
                linePanel9.add(elements.get(j));
            }
            linePanel9.add(new JPanel());

            JPanel elementViewPanel = new JPanel();
            elementViewPanel.setLayout(new GridLayout(10, 1));
            elementViewPanel.add(linePanel1);
            elementViewPanel.add(linePanel2);
            elementViewPanel.add(linePanel3);
            elementViewPanel.add(linePanel4);
            elementViewPanel.add(linePanel5);
            elementViewPanel.add(linePanel6);
            elementViewPanel.add(linePanel7);
            elementViewPanel.add(new JLabel());
            elementViewPanel.add(linePanel8);
            elementViewPanel.add(linePanel9);

            // elementView.setLayout(new BorderLayout());
            // JPanel panel = new JPanel();
            // panel.add(getElementSelctedPanel());
            // panel.add(getSelctedEnergyView());
            // elementView.add(panel,BorderLayout.NORTH);

            elementView.add(elementViewPanel);
        }
        return elementView;
    }

    public ElementButton searchElementFromValue(String value) {
        ElementButton elementSource = null;
        if ((value != null) && (!value.isEmpty())) {
            for (ElementButton element : elements) {
                switch (mode) {
                    case ATOMIC_NUMBER_MODE:
                        try {
                            int atomicNumber = (int) Double.parseDouble(value);
                            if (Integer.parseInt(element.getElement().getAtomicNumber()) == atomicNumber) {
                                elementSource = element;
                                break;
                            }
                        } catch (NumberFormatException e) {
                            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                    .error("Not valide parameter. Please set a number or change settings mode.", e);
                        }
                        break;
                    case SYMBOL_MODE:
                        if (element.getElement().getSymbole().equalsIgnoreCase(value)) {
                            elementSource = element;
                        }
                        break;
                    case NAME_MODE:
                        if (element.getElement().getElementName().equalsIgnoreCase(value)) {
                            elementSource = element;
                        }
                        break;
                    default:
                        elementSource = new ElementButton(ElementModel.defaultElement());
                        break;
                }
            }

        }
        return elementSource;
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (elements != null) {
            for (ElementButton element : elements) {
                element.setEnabled(enabled);
            }
        }
        if (currentView != null) {
            currentView.setEnabled(enabled);
        }
    }

    @Override
    public void setBackground(Color color) {
        if (elements != null) {
            for (ElementButton element : elements) {
                element.setBackground(color);
            }
        }
        if (currentView != null) {
            currentView.setBackground(color);
        }
    }

    public void setEnabled(String[] valueList, boolean enabled) {
        for (int i = 0; i > valueList.length; ++i) {
            searchElementFromValue(valueList[i]).setEnabled(enabled);
        }
        if (currentView != null) {
            currentView.setEnabled(enabled);
        }
    }

    ///////////////////////
    // Getters & Setters //
    ///////////////////////

    public List<ElementButton> getElements() {
        return elements;
    }

    public void setElements(ArrayList<ElementButton> elements) {
        this.elements = elements;
    }

    public String getAccesPath() {
        return accesPath;
    }

    public void setAccesPath(String accesPath) {
        this.accesPath = accesPath;
    }

    public ElementButton getSelectedElement() {
        return selectedElement;
    }

    public void setSelectedElement(ElementButton selectedElement) {
        this.selectedElement = selectedElement;
    }

    protected static String getComposantModel() {
        return composantModel;
    }

    protected static void setComposantModel(String composantModel) {
        PeriodicTable.composantModel = composantModel;
    }

}
