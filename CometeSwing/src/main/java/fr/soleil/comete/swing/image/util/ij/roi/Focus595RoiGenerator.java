/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Cursor;

import javax.swing.ImageIcon;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.exception.ApplicationIdException;
import fr.soleil.lib.project.ij.FlatMatrixTool;
import ij.gui.Roi;

public class Focus595RoiGenerator extends AbstractContrastRoiGenerator {

    public Focus595RoiGenerator(String text, String description) {
        this(text, description, ImageViewer.CONTRAST_5_95);
    }

    protected Focus595RoiGenerator(String text, String description, ImageIcon icon) {
        super(text, description, icon);
    }

    @Override
    public Cursor getCursor() {
        return CURSOR;
    }

    @Override
    protected void contrast(Roi roi, ImageViewer viewer) throws ApplicationIdException {
        fivePercentContrast(roi, viewer);
    }

    /**
     * Applies the 5%-95% focus on an {@link ImageViewer}
     * 
     * @param viewer The {@link ImageViewer}
     * @param roi The {@link Roi} that determines the rectangle region for which to do the focus
     * @throws ApplicationIdException If the application id is not yet set for this {@link ImageViewer}
     * @see ImageViewer#setApplicationId(String)
     */
    public static void fivePercentContrast(Roi roi, ImageViewer viewer) throws ApplicationIdException {
        double[] minMaxMean = FlatMatrixTool.extractSubMatrixMinMaxAndMean(roi, viewer.getValue(), viewer.getDimX(),
                viewer.getDimY());
        double min = minMaxMean[0];
        if (!Double.isNaN(min)) {
            double max = minMaxMean[1];
            double fivePercentMean = minMaxMean[2] * 0.05;
            min += fivePercentMean;
            max -= fivePercentMean;
            if (min > max) {
                double temp = min;
                min = max;
                max = temp;
            }
            viewer.setFitMinMax(min, max);
        }
    }

}
