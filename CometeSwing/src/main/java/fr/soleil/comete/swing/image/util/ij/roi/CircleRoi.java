/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.geom.Ellipse2D;

import fr.soleil.lib.project.ObjectUtils;
import ij.gui.RoiHack;
import ij.gui.ShapeRoi;

/**
 * A {@link ShapeRoi} that represents a circle
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class CircleRoi extends ShapeRoi {

    private static final long serialVersionUID = 2949621832058186425L;

    public static final int TYPE = 11;

    protected double centerX;
    protected double centerY;
    protected float radius;
    protected String stringRadius;

    /**
     * Creates a new {@link CircleRoi}
     * 
     * @param centerX The X coordinate of the circle's center
     * @param centerY The Y coordinate of the circle's center
     * @param radius The circle's radius
     * @param stringRadius The string representation of the radius
     */
    public CircleRoi(double centerX, double centerY, float radius, String stringRadius) {
        super(buildShape(centerX, centerY, radius));
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.stringRadius = stringRadius == null ? ObjectUtils.EMPTY_STRING : stringRadius;
        this.type = TYPE;
    }

    /**
     * Creates the ellipse representing the circle at given center coordinates with given radius.
     * 
     * @param centerX The X coordinate of the circle's center
     * @param centerY The Y coordinate of the circle's center
     * @param radius The circle's radius
     * @return An {@link Ellipse2D.Double}
     */
    protected static Ellipse2D.Double buildShape(double centerX, double centerY, float radius) {
        return new Ellipse2D.Double(centerX - radius, centerY - radius, radius * 2f, radius * 2f);
    }

    /**
     * Creates a new {@link CircleRoi}
     * 
     * @param centerX The X coordinate of the circle's center
     * @param centerY The Y coordinate of the circle's center
     * @param radius The circle's radius
     */
    public CircleRoi(double centerX, double centerY, float radius) {
        this(centerX, centerY, radius, ObjectUtils.EMPTY_STRING);
    }

    /**
     * Changes the circle.
     * 
     * @param centerX The X coordinate of the circle's center
     * @param centerY The Y coordinate of the circle's center
     * @param radius The circle's radius
     * @param stringRadius The string representation of the radius
     */
    public void updateCircle(double centerX, double centerY, float radius, String stringRadius) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.stringRadius = stringRadius == null ? ObjectUtils.EMPTY_STRING : stringRadius;
        RoiHack.reshapeRoi(this, buildShape(centerX, centerY, radius), true);
    }

    // XXX We override this method to avoid a StackOverFlowException due to ImageJ
    /**
     * Returns the circle's perimeter
     * 
     * @return A <code>double</code>
     */
    @Override
    public double getLength() {
        return 2 * Math.PI * radius;
    }

    /**
     * Returns The X coordinate of the circle's center
     * 
     * @return A <code>double</code>
     */
    public double getCenterX() {
        return centerX;
    }

    /**
     * Returns The Z coordinate of the circle's center
     * 
     * @return A <code>double</code>
     */
    public double getCenterZ() {
        return centerY;
    }

    /**
     * Returns The circle's radius
     * 
     * @return A <code>float</code>
     */
    public float getRadius() {
        return radius;
    }

    public String getStringRadius() {
        return stringRadius;
    }

}
