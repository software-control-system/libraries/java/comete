/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.graphics;

import java.awt.Graphics;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.chart.view.shape.ShapeGenerator;

/**
 * A class that handles dataviews marker painting.
 * 
 * @author Rapha&euml;l GIRARDOT
 * @see ShapeGenerator
 */
public class MarkerGraphics {

    private MarkerGraphics() {
        // hide constructor
    }

    /**
     * Expert usage. Paints an equilateral triangle marker at the specified position.
     * 
     * @param g Graphics object
     * @param mSize Marker size
     * @param mSize2 Previously calculated marker half size
     * @param x x coordinates (pixel space)
     * @param y y coordinates (pixel space)
     */
    protected static void paintEquilateralTriangleMarker(Graphics g, int mSize, int mSize2, int x, int y) {
        int sqrt3Size3 = (int) (mSize * Math.sqrt(3) / 3);
        int sqrt3Size6 = (int) (mSize * Math.sqrt(3) / 6);
        g.fillPolygon(new int[] { x - mSize2, x, x + mSize2 },
                new int[] { y + sqrt3Size6, y - sqrt3Size3, y + sqrt3Size6 }, 3);
    }

    /**
     * Expert usage. Paints an isosceles triangle at the specified position.
     * 
     * @param g Graphics object
     * @param mSize Marker size
     * @param mSize2 Previously calculated marker half size
     * @param x x coordinates (pixel space)
     * @param y y coordinates (pixel space)
     */
    protected static void paintIsoscelesTriangleMarker(Graphics g, int mSize, int mSize2, int x, int y) {
        g.fillPolygon(new int[] { x - mSize2, x, x + mSize2 }, new int[] { y + mSize2, y - mSize2, y + mSize2 }, 3);
    }

    /**
     * Expert usage. Paints a marker at the specified position
     * 
     * @param g Graphics object
     * @param mType Marker type
     * @param mSize Marker size
     * @param x x coordinates (pixel space)
     * @param y y coordinates (pixel space)
     */
    public static void paintMarker(Graphics g, int mType, int mSize, int x, int y) {
        int mSize2 = mSize / 2;
        // RG: Uncomment to reuse former code
        // int mSize21 = mSize / 2 + 1;
        int mSizeSqrt2Div2 = (int) (mSize * Math.sqrt(2) / 2);

        switch (mType) {
            case IChartViewer.MARKER_DOT:
                g.fillOval(x - mSize2, y - mSize2, mSize, mSize);
                break;
            case IChartViewer.MARKER_BOX:
                g.fillRect(x - mSize2, y - mSize2, mSize, mSize);
                break;
            case IChartViewer.MARKER_TRIANGLE:
                paintIsoscelesTriangleMarker(g, mSize, mSize2, x, y);
                break;
            case IChartViewer.MARKER_DIAMOND:
                g.fillPolygon(new int[] { x - mSizeSqrt2Div2, x, x + mSizeSqrt2Div2, x },
                        new int[] { y, y - mSizeSqrt2Div2, y, y + mSizeSqrt2Div2 }, 4);
                break;
            case IChartViewer.MARKER_STAR:
                g.drawLine(x - mSize2, y + mSize2, x + mSize2, y - mSize2);
                g.drawLine(x + mSize2, y + mSize2, x - mSize2, y - mSize2);
                g.drawLine(x, y - mSize2, x, y + mSize2);
                g.drawLine(x - mSize2, y, x + mSize2, y);
                break;
            case IChartViewer.MARKER_VERT_LINE:
                g.drawLine(x, y - mSize2, x, y + mSize2);
                break;
            case IChartViewer.MARKER_HORIZ_LINE:
                g.drawLine(x - mSize2, y, x + mSize2, y);
                break;
            case IChartViewer.MARKER_CROSS:
                g.drawLine(x, y - mSize2, x, y + mSize2);
                g.drawLine(x - mSize2, y, x + mSize2, y);
                break;
            case IChartViewer.MARKER_CIRCLE:
                g.drawOval(x - mSize2, y - mSize2, mSize + 1, mSize + 1);
                break;
            case IChartViewer.MARKER_SQUARE:
                g.drawRect(x - mSize2, y - mSize2, mSize, mSize);
                break;
        }

    }

}
