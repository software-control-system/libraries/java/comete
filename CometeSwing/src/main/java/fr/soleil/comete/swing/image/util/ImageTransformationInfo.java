/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util;

import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.lib.project.data.FlatData;

/**
 * A class that contains all information to manage image transformations (rotations and symmetries).
 * <p>
 * To understand how to interact with this class, you should take a look at
 * {@link fr.soleil.comete.swing.image.ImageTransformationPanel}.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ImageTransformationInfo {

    protected volatile String xTitle, xTitleToUse;
    protected volatile String yTitle, yTitleToUse;
    protected volatile IValueConvertor xAxisConvertor, xAxisConvertorToUse;
    protected volatile IValueConvertor yAxisConvertor, yAxisConvertorToUse;
    protected volatile FlatData lastData;
    protected volatile String xAxisFormat, xAxisFormatToUse, yAxisFormat, yAxisFormatToUse;
    protected double[] beamPoint, beamPointToUse;

    /**
     * Creates a new {@link ImageTransformationInfo}.
     * 
     * @param xTitle The title to display on X axis before transformations.
     * @param yTitle The title to display on Y axis before transformations.
     * @param xAxisConvertor The {@link IValueConvertor} to use for X axis before transformations.
     * @param yAxisConvertor The {@link IValueConvertor} to use for X axis before transformations.
     * @param lastData The last displayed image data.
     * @param xAxisFormat The format to use on X axis before transformations.
     * @param yAxisFormat The format to use on Y axis before transformations.
     * @param beamPoint The known beam point before transformations.
     */
    public ImageTransformationInfo(String xTitle, String yTitle, IValueConvertor xAxisConvertor,
            IValueConvertor yAxisConvertor, FlatData lastData, String xAxisFormat, String yAxisFormat,
            double[] beamPoint) {
        super();
        this.xTitle = xTitle;
        this.xTitleToUse = xTitle;
        this.yTitle = yTitle;
        this.yTitleToUse = yTitle;
        this.xAxisConvertor = xAxisConvertor;
        this.xAxisConvertorToUse = xAxisConvertor;
        this.yAxisConvertor = yAxisConvertor;
        this.yAxisConvertorToUse = yAxisConvertor;
        this.lastData = lastData;
        this.xAxisFormat = xAxisFormat;
        this.xAxisFormatToUse = xAxisFormat;
        this.yAxisFormat = yAxisFormat;
        this.yAxisFormatToUse = yAxisFormat;
        this.beamPoint = beamPoint;
        this.beamPointToUse = beamPoint;
    }

    /**
     * Returns the title to display on X axis before transformations.
     * 
     * @return The title to display on X axis before transformations.
     */
    public String getXTitle() {
        return xTitle;
    }

    /**
     * Sets the title to display on X axis before transformations.
     * 
     * @param xTitle The title to display on X axis before transformations.
     */
    public void setXTitle(String xTitle) {
        this.xTitle = xTitle;
    }

    /**
     * Returns the effective title to display on X axis, once transformations are applied.
     * 
     * @return The effective title to display on X axis, once transformations are applied.
     */
    public String getXTitleToUse() {
        return xTitleToUse;
    }

    /**
     * Sets the effective title to display on X axis, once transformations are applied.
     * 
     * @param xTitleToUse The effective title to display on X axis, once transformations are applied.
     */
    public void setXTitleToUse(String xTitleToUse) {
        this.xTitleToUse = xTitleToUse;
    }

    /**
     * Returns the title to display on Y axis before transformations.
     * 
     * @return The title to display on Y axis before transformations.
     */
    public String getYTitle() {
        return yTitle;
    }

    /**
     * Sets the title to display on Y axis before transformations.
     * 
     * @param yTitle The title to display on Y axis before transformations.
     */
    public void setYTitle(String yTitle) {
        this.yTitle = yTitle;
    }

    /**
     * Returns the effective title to display on Y axis, once transformations are applied.
     * 
     * @return The effective title to display on Y axis, once transformations are applied.
     */
    public String getYTitleToUse() {
        return yTitleToUse;
    }

    /**
     * Sets the effective title to display on Y axis, once transformations are applied.
     * 
     * @param yTitleToUse The effective title to display on Y axis, once transformations are applied.
     */
    public void setYTitleToUse(String yTitleToUse) {
        this.yTitleToUse = yTitleToUse;
    }

    /**
     * Returns the {@link IValueConvertor} to use for X axis before transformations.
     * 
     * @return The {@link IValueConvertor} to use for X axis before transformations.
     */
    public IValueConvertor getXAxisConvertor() {
        return xAxisConvertor;
    }

    /**
     * Sets the {@link IValueConvertor} to use for X axis before transformations.
     * 
     * @param xAxisConvertor The {@link IValueConvertor} to use for X axis before transformations.
     */
    public void setXAxisConvertor(IValueConvertor xAxisConvertor) {
        this.xAxisConvertor = xAxisConvertor;
    }

    /**
     * Returns the {@link IValueConvertor} to effectively use for X axis, once transformations are applied.
     * 
     * @return The {@link IValueConvertor} to effectively use for X axis, once transformations are applied.
     */
    public IValueConvertor getXAxisConvertorToUse() {
        return xAxisConvertorToUse;
    }

    /**
     * Sets the {@link IValueConvertor} to effectively use for X axis, once transformations are applied.
     * 
     * @param xAxisConvertorToUse The {@link IValueConvertor} to effectively use for X axis, once transformations are
     *            applied.
     */
    public void setXAxisConvertorToUse(IValueConvertor xAxisConvertorToUse) {
        this.xAxisConvertorToUse = xAxisConvertorToUse;
    }

    /**
     * Returns the {@link IValueConvertor} to use for Y axis before transformations.
     * 
     * @return The {@link IValueConvertor} to use for Y axis before transformations.
     */
    public IValueConvertor getYAxisConvertor() {
        return yAxisConvertor;
    }

    /**
     * Sets the {@link IValueConvertor} to use for Y axis before transformations.
     * 
     * @param yAxisConvertor The {@link IValueConvertor} to use for Y axis before transformations.
     */
    public void setYAxisConvertor(IValueConvertor yAxisConvertor) {
        this.yAxisConvertor = yAxisConvertor;
    }

    /**
     * Returns the {@link IValueConvertor} to effectively use for Y axis, once transformations are applied.
     * 
     * @return The {@link IValueConvertor} to effectively use for Y axis, once transformations are applied.
     */
    public IValueConvertor getYAxisConvertorToUse() {
        return yAxisConvertorToUse;
    }

    /**
     * Sets the {@link IValueConvertor} to effectively use for Y axis, once transformations are applied.
     * 
     * @param yAxisConvertorToUse The {@link IValueConvertor} to effectively use for Y axis, once transformations are
     *            applied.
     */
    public void setYAxisConvertorToUse(IValueConvertor yAxisConvertorToUse) {
        this.yAxisConvertorToUse = yAxisConvertorToUse;
    }

    /**
     * Returns the last displayed image data.
     * 
     * @return A {@link FlatData}.
     */
    public FlatData getLastData() {
        return lastData;
    }

    /**
     * Sets the last displayed image data.
     * 
     * @param lastData The last displayed image data.
     */
    public void setLastData(FlatData lastData) {
        this.lastData = lastData;
    }

    /**
     * Returns the format to use on X axis before transformations.
     * 
     * @return The format to use on X axis before transformations.
     */
    public String getXAxisFormat() {
        return xAxisFormat;
    }

    /**
     * Sets the format to use on X axis before transformations.
     * 
     * @param xAxisFormat The format to use on X axis before transformations.
     */
    public void setXAxisFormat(String xAxisFormat) {
        this.xAxisFormat = xAxisFormat;
    }

    /**
     * Returns the format to effectively use on X axis, once transformations are applied.
     * 
     * @return The format to effectively use on X axis, once transformations are applied.
     */
    public String getXAxisFormatToUse() {
        return xAxisFormatToUse;
    }

    /**
     * Sets the format to effectively use on X axis, once transformations are applied.
     * 
     * @param xAxisFormatToUse The format to effectively use on X axis, once transformations are applied.
     */
    public void setXAxisFormatToUse(String xAxisFormatToUse) {
        this.xAxisFormatToUse = xAxisFormatToUse;
    }

    /**
     * Returns the format to use on Y axis before transformations.
     * 
     * @return The format to use on Y axis before transformations.
     */
    public String getYAxisFormat() {
        return yAxisFormat;
    }

    /**
     * Sets the format to use on Y axis before transformations.
     * 
     * @param yAxisFormat The format to use on Y axis before transformations.
     */
    public void setYAxisFormat(String yAxisFormat) {
        this.yAxisFormat = yAxisFormat;
    }

    /**
     * Returns the format to effectively use on Y axis, once transformations are applied.
     * 
     * @return The format to effectively use on Y axis, once transformations are applied.
     */
    public String getYAxisFormatToUse() {
        return yAxisFormatToUse;
    }

    /**
     * Sets the format to effectively use on Y axis, once transformations are applied.
     * 
     * @param yAxisFormatToUse The format to effectively use on Y axis, once transformations are applied.
     */
    public void setYAxisFormatToUse(String yAxisFormatToUse) {
        this.yAxisFormatToUse = yAxisFormatToUse;
    }

    /**
     * Returns the known beam point before transformations.
     * 
     * @return The known beam point before transformations.
     */
    public double[] getBeamPoint() {
        return beamPoint;
    }

    /**
     * Sets the known beam point before transformations.
     * 
     * @param beamPoint the known beam point before transformations.
     */
    public void setBeamPoint(double... beamPoint) {
        this.beamPoint = beamPoint;
    }

    /**
     * Returns the adapted beam point, once transformations are applied.
     * 
     * @return The adapted beam point, once transformations are applied.
     */
    public double[] getBeamPointToUse() {
        return beamPointToUse;
    }

    /**
     * Sets the adapted beam point, once transformations are applied.
     * 
     * @param beamPointToUse the adapted beam point, once transformations are applied.
     */
    public void setBeamPointToUse(double... beamPointToUse) {
        this.beamPointToUse = beamPointToUse;
    }

}
