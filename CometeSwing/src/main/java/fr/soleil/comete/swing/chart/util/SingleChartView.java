/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.ImageIcon;

import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.event.ChartViewerEvent.Reason;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.data.DataView;

public class SingleChartView implements IChartViewerListener {

    private final IChartViewer chart;
    private IChartViewer chartOrig;
    private String title;
    private ImageIcon image;
    private final Map<String, Object> subData;

    public SingleChartView() {
        chart = new Chart();
        chartOrig = null;
        subData = new HashMap<String, Object>();
    }

    public IChartViewer getChart() {
        return chart;
    }

    public String getTitle() {
        return title;
    }

    public ImageIcon getImage() {
        return image;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setIconImage(ImageIcon image) {
        this.image = image;

    }

    public void copyDataViews(List<DataView> result, Chart chartsource) {
        if ((result != null) && (chartsource != null)) {
            chartOrig = chartsource;
            chartsource.setNotifyListenersOnDataAdded(true);
            chartsource.addChartViewerListener(this);
            Map<String, Object> dataSource = chartsource.getData();
            if (dataSource != null) {
                Object value = null;
                String dataId = null;
                // Get the required data from the chart source
                for (DataView dataView : result) {
                    dataId = dataView.getId();
                    // System.out.println("dataViewId=" + dataId);
                    value = dataSource.get(dataId);
                    if (value != null) {
                        subData.put(dataId, value);
                    }

                    // Get the plotProperties of the DataView
                    PlotProperties dataViewPlotProperties = chartsource.getDataViewPlotProperties(dataId);
                    chart.setDataViewPlotProperties(dataId, dataViewPlotProperties);
                }
                chart.setData(subData);

            }
            ChartProperties chartProperties = chartsource.getChartProperties();
            chart.setChartProperties(chartProperties);

        }
    }

    @Override
    public void chartViewerChanged(ChartViewerEvent arg0) {
        if (arg0 != null) {
            Reason reason = arg0.getReason();
            IChartViewer source = arg0.getSource();
            String viewId = arg0.getViewId();
            Map<String, Object> concernedData = arg0.getConcernedData();
            switch (reason) {
                case HIGHLIGHT:
                    if (concernedData != null) {
                        Set<Entry<String, Object>> entrySet = concernedData.entrySet();
                        String key = null;
                        Object value = null;
                        for (Entry<String, Object> entry : entrySet) {
                            key = entry.getKey();
                            if (subData.containsKey(key)) {
                                value = entry.getValue();
                                if (value instanceof Boolean) {
                                    chart.setDataViewHighlighted(key, (Boolean) value);
                                }
                            }
                        }
                    }
                    break;
                case CONFIGURATION:
                    if (source != null) {
                        if ((viewId != null) && subData.containsKey(viewId)) {
                            chart.setDataViewPlotProperties(viewId, source.getDataViewPlotProperties(viewId));
                        } else {
                            chart.setChartProperties(source.getChartProperties());
                        }
                    }
                    break;
                case DATA:
                    if (concernedData != null) {
                        Set<Entry<String, Object>> entrySet = concernedData.entrySet();
                        String key = null;
                        Object value = null;
                        for (Entry<String, Object> entry : entrySet) {
                            key = entry.getKey();
                            if (subData.containsKey(key)) {
                                value = entry.getValue();
                                subData.put(key, value);
                            }
                        }
                        chart.setData(subData);
                    }
                    break;
                default:
                    // nothing to do for other cases
                    break;
            }
        }
    }

    public void cleanView() {
        if (chartOrig != null) {
            chartOrig.removeChartViewerListener(this);
        }
        chartOrig = null;
        title = null;
        image = null;
        subData.clear();
        chart.setData(subData);
    }

}