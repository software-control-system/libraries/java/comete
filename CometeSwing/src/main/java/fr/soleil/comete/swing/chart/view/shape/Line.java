/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Line2D;

import fr.soleil.comete.definition.widget.util.IChartConst.ShapeType;
import fr.soleil.comete.swing.util.CometeConstants;

/**
 * {@link AbstractShape} specialized in lines
 * 
 * @author huriez
 */
public class Line extends AbstractShape implements CometeConstants {

    private final Line2D line;
    private BasicStroke basicStroke;

    public Line(ShapeType type) {
        super(type);
        line = new Line2D.Double(0, 0, 0, 0);
        color = new Color(0, 0, 0);
        basicStroke = null;
    }

    public Line(ShapeType type, Line2D line) {
        super(type);
        this.line = line;
    }

    public Line(ShapeType type, Line2D line, Color color) {
        super(type);
        this.line = line;
        super.color = color;
    }

    public Line(ShapeType type, Line2D line, Color color, BasicStroke stroke) {
        super(type);
        this.line = line;
        super.color = color;
        basicStroke = stroke;
    }

    /**
     * @return the lineColor
     */
    public Color getLineColor() {
        return color;
    }

    /**
     * @param lineColor the lineColor to set
     */
    public void setLineColor(Color lineColor) {
        color = lineColor;
    }

    /**
     * @return the basicStroke
     */
    public BasicStroke getBasicStroke() {
        return basicStroke;
    }

    /**
     * @param basicStroke the basicStroke to set
     */
    public void setBasicStroke(BasicStroke basicStroke) {
        this.basicStroke = basicStroke;
    }

    /**
     * @return the line
     */
    public Line2D getLine() {
        return line;
    }

    @Override
    public void drawShape(Graphics2D g) {
        Stroke oldStroke = g.getStroke();
        if (basicStroke != null) {
            g.setStroke(basicStroke);
        }
        g.setColor(color);
        g.translate(x, y);
        g.drawLine((int) line.getX1(), (int) line.getY1(), (int) line.getX2(), (int) line.getY2());
        g.translate(-x, -y);
        if (oldStroke != null) {
            g.setStroke(oldStroke);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getClass().getSimpleName());
        builder.append(": {");
        builder.append("line: [").append((int) line.getX1()).append(COMMA).append((int) line.getY1()).append(COMMA)
                .append((int) line.getX2()).append(COMMA).append((int) line.getY2()).append("]|");
        builder.append("color: ").append(color).append("|");
        builder.append("basicStroke: ").append(basicStroke).append("}");
        return builder.toString();
    }

}
