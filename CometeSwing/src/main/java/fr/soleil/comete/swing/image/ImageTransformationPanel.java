/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.event.ImageViewerEvent;
import fr.soleil.comete.definition.event.ImageViewerEvent.Reason;
import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.listener.IImageViewerListener;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.listener.MaskListener;
import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.comete.definition.widget.util.IRoi;
import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MathematicSector;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.RotaryImageViewer;
import fr.soleil.comete.swing.image.util.DimensionAdapter;
import fr.soleil.comete.swing.image.util.ImageTransformationInfo;
import fr.soleil.comete.swing.image.util.Transformation;
import fr.soleil.data.adapter.NumberArrayToBooleanArrayAdapter;
import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.filter.FlatMatrixRightAngleRotation;
import fr.soleil.data.filter.FlatMatrixSymmetry;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.data.FlatData;
import fr.soleil.lib.project.map.ConcurrentWeakHashMap;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * A {@link JPanel} that manages images transformations
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ImageTransformationPanel extends JPanel {

    private static final long serialVersionUID = 329622087820753989L;

    protected static final String POSITIVE = "Positive";
    protected static final String NEGATIVE = "Negative";
    protected static final String NONE = "None";
    protected static final String ROTATION = "Rotation";
    protected static final String HORIZONTAL_AXIS = "Horizontal Axis";
    protected static final String VERTICAL_AXIS = "Vertical Axis";
    protected static final String SYMMETRY = "Symmetry";
    protected static final String TRANSFORMATION = "Transformation: ";
    protected static final String FILTER_SEPARATOR = ", ";

    protected final List<Transformation> filterList;
    protected final ConcurrentMap<Transformation, String> transformationTextMap;
    protected final ConcurrentMap<ImageViewer, ImageTransformationInfo> transformationMap;
    protected final ConcurrentMap<ImageViewer, ImageAdapter> adapterMap;
    protected Transformation lastRotationAction;

    protected final JPanel rotationPanel, symmetryPanel;
    protected final JRadioButton positiveRotationButton;
    protected final JRadioButton negativeRotationButton;
    protected final JRadioButton noRotationButton;
    protected final JCheckBox horizontalSymmetryCheckBox;
    protected final JCheckBox verticalSymmetryCheckBox;
    protected final JLabel transformationLabel;
    protected volatile boolean switchDim;

    public ImageTransformationPanel() {
        super(new BorderLayout());
        filterList = new ArrayList<>();
        transformationTextMap = new ConcurrentHashMap<>();
        transformationMap = new ConcurrentWeakHashMap<>();
        adapterMap = new ConcurrentWeakHashMap<>();

        // Rotation
        positiveRotationButton = new JRadioButton(POSITIVE, false);
        negativeRotationButton = new JRadioButton(NEGATIVE, false);
        noRotationButton = new JRadioButton(NONE, true);

        final ButtonGroup rotationGroup = new ButtonGroup();
        rotationGroup.add(positiveRotationButton);
        rotationGroup.add(negativeRotationButton);
        rotationGroup.add(noRotationButton);

        rotationPanel = new JPanel();
        final TitledBorder rotationTitle = BorderFactory.createTitledBorder(ROTATION);
        rotationPanel.setBorder(rotationTitle);
        rotationPanel.setLayout(new GridLayout(3, 1));
        rotationPanel.add(positiveRotationButton);
        rotationPanel.add(negativeRotationButton);
        rotationPanel.add(noRotationButton);

        final TransformationActionListener tal = new TransformationActionListener();
        positiveRotationButton.addActionListener(tal);
        noRotationButton.addActionListener(tal);
        negativeRotationButton.addActionListener(tal);

        // Symmetry
        horizontalSymmetryCheckBox = new JCheckBox(HORIZONTAL_AXIS);
        verticalSymmetryCheckBox = new JCheckBox(VERTICAL_AXIS);
        horizontalSymmetryCheckBox.addActionListener(tal);
        verticalSymmetryCheckBox.addActionListener(tal);

        symmetryPanel = new JPanel();
        final TitledBorder symmetryTitle = BorderFactory.createTitledBorder(SYMMETRY);
        symmetryPanel.setBorder(symmetryTitle);
        symmetryPanel.setLayout(new GridLayout(2, 1));
        symmetryPanel.add(horizontalSymmetryCheckBox);
        symmetryPanel.add(verticalSymmetryCheckBox);

        // Transformation panel
        JPanel rotationAndSymmetryPanel = new JPanel(new GridLayout(1, 2, 15, 15));
        rotationAndSymmetryPanel.add(rotationPanel);
        rotationAndSymmetryPanel.add(symmetryPanel);
        JLabel transformationTitleLabel = new JLabel(TRANSFORMATION);
        transformationLabel = new JLabel();
        transformationLabel.setFont(new Font(Font.DIALOG, Font.PLAIN, 12));
        JPanel transformationDescriptionPanel = new JPanel(new BorderLayout());
        transformationDescriptionPanel.add(transformationTitleLabel, BorderLayout.WEST);
        transformationDescriptionPanel.add(transformationLabel, BorderLayout.CENTER);
        transformationLabel.setHorizontalAlignment(JLabel.LEFT);
        add(transformationDescriptionPanel, BorderLayout.NORTH);
        add(rotationAndSymmetryPanel, BorderLayout.CENTER);
        applyTransformations();
    }

    /**
     * Registers an {@link ImageViewer} and returns the adapter that must be connected instead of the
     * {@link ImageViewer}.
     * 
     * @param imageViewer The {@link ImageViewer} to register.
     * @return An {@link IImageViewer}: the adapter that must be connected instead of the {@link ImageViewer}.
     * @see #unregisterImageViewer(ImageViewer)
     */
    public IImageViewer registerImageViewer(ImageViewer imageViewer) {
        ImageAdapter adapter;
        if (imageViewer == null) {
            adapter = null;
        } else {
            adapter = adapterMap.get(imageViewer);
            if (adapter == null) {
                ImageTransformationInfo transformationInfo = transformationMap.get(imageViewer);
                if (transformationInfo == null) {
                    transformationInfo = new ImageTransformationInfo(imageViewer.getXAxis().getAttributes().getTitle(),
                            imageViewer.getYAxis().getAttributes().getTitle(), imageViewer.getXAxisConvertor(),
                            imageViewer.getYAxisConvertor(),
                            imageViewer.getValue() == null ? null
                                    : new FlatData(new int[] { imageViewer.getDimY(), imageViewer.getDimX() },
                                            imageViewer.getValue()),
                            imageViewer.getXAxisFormat(), imageViewer.getYAxisFormat(), imageViewer.getBeamPoint());
                    ImageTransformationInfo tmp = transformationMap.putIfAbsent(imageViewer, transformationInfo);
                    if (tmp != null) {
                        transformationInfo = tmp;
                    }
                }
                adapter = new ImageAdapter(imageViewer);
                ImageAdapter tmp = adapterMap.putIfAbsent(imageViewer, adapter);
                if (tmp != null) {
                    adapter = tmp;
                }
            }
        }
        return adapter;
    }

    /**
     * Unregisters an {@link ImageViewer} and cancels all transformations in it.
     * 
     * @param imageViewer The {@link ImageViewer} to unregister.
     * @see #registerImageViewer(ImageViewer)
     */
    public void unregisterImageViewer(ImageViewer imageViewer) {
        if (imageViewer != null) {
            ImageTransformationInfo transformationInfo = transformationMap.remove(imageViewer);
            ImageAdapter adapter = adapterMap.remove(imageViewer);
            if (adapter != null) {
                imageViewer.removeImageViewerListener(adapter);
            }
            if (transformationInfo != null) {
                imageViewer.setXAxisConvertor(transformationInfo.getXAxisConvertor(), false);
                imageViewer.setYAxisConvertor(transformationInfo.getYAxisConvertor(), false);
                imageViewer.getXAxis().getAttributes().setTitle(transformationInfo.getXTitle());
                imageViewer.getYAxis().getAttributes().setTitle(transformationInfo.getYTitle());
                imageViewer.setXAxisFormat(transformationInfo.getXAxisFormat());
                imageViewer.setYAxisFormat(transformationInfo.getYAxisFormat());
                FlatData data = transformationInfo.getLastData();
                if ((data == null) || (data.getRealShape() == null) || (data.getRealShape().length != 2)) {
                    imageViewer.setFlatNumberMatrix(null, 0, 0);
                } else {
                    imageViewer.setFlatNumberMatrix(data.getValue(), data.getRealShape()[1], data.getRealShape()[0]);
                }
            }
        }
    }

    /**
     * Returns the adapter that must be connected instead of given {@link ImageViewer}.
     * 
     * @param imageViewer The {@link ImageViewer}.
     * @return An {@link IImageViewer}: the adapter that must be connected instead of given {@link ImageViewer}.
     * @see #registerImageViewer(ImageViewer)
     * @see #unregisterImageViewer(ImageViewer)
     */
    public IImageViewer getImageAdapter(ImageViewer imageViewer) {
        return imageViewer == null ? null : adapterMap.get(imageViewer);
    }

    /**
     * Returns the text displayed in the button for no rotation.
     * 
     * @return The text displayed in the button for no rotation.
     */
    public String getNoRotationText() {
        return noRotationButton.getText();
    }

    /**
     * Sets the text to display in the button for no rotation.
     * 
     * @param text The text to display in the button for no rotation.
     */
    public void setNoRotationText(String text) {
        noRotationButton.setText(text);
    }

    /**
     * Returns the text displayed in the button for positive angle rotation.
     * 
     * @return The text displayed in the button for positive angle rotation.
     */
    public String getPositiveRotationText() {
        return positiveRotationButton.getText();
    }

    /**
     * Sets the text to display in the button for positive angle rotation.
     * 
     * @param text The text to display in the button for positive angle rotation.
     */
    public void setPositiveRotationText(String text) {
        positiveRotationButton.setText(text);
    }

    /**
     * Returns the text displayed in the button for negative angle rotation.
     * 
     * @return The text displayed in the button for negative angle rotation.
     */
    public String getNegativeRotationText() {
        return negativeRotationButton.getText();
    }

    /**
     * Sets the text to display in the button for negative angle rotation.
     * 
     * @param text The text to display in the button for negative angle rotation.
     */
    public void setNegativeRotationText(String text) {
        negativeRotationButton.setText(text);
    }

    /**
     * Returns the title displayed in the rotation zone
     * 
     * @return The title displayed in the rotation zone
     */
    public String getRotationTitle() {
        return ((TitledBorder) rotationPanel.getBorder()).getTitle();
    }

    /**
     * Returns the title to display in the rotation zone
     * 
     * @return The title to display in the rotation zone
     */
    public void setRotationTitle(String title) {
        ((TitledBorder) rotationPanel.getBorder()).setTitle(title);
        repaint();
    }

    /**
     * Returns the title displayed in the symmetry zone
     * 
     * @return The title displayed in the symmetry zone
     */
    public String getSymmetryTitle() {
        return ((TitledBorder) symmetryPanel.getBorder()).getTitle();
    }

    /**
     * Returns the title to display in the symmetry zone
     * 
     * @return The title to display in the symmetry zone
     */
    public void setSymmetryTitle(String title) {
        ((TitledBorder) symmetryPanel.getBorder()).setTitle(title);
        repaint();
    }

    /**
     * Returns the text to displayed for given {@link Transformation}.
     * 
     * @param transformation The {@link Transformation}.
     * @return A {@link String}.
     */
    public String getTransformationText(Transformation transformation) {
        String text;
        if (transformation == null) {
            text = null;
        } else {
            text = transformationTextMap.get(transformation);
            if (text == null) {
                text = transformation.toString();
            }
        }
        return text;
    }

    /**
     * Sets the text to display for given {@link Transformation}..
     * 
     * @param transformation The {@link Transformation}.
     * @param text The text to display. If <code>null</code>, default text will be used.
     */
    public void setTransformationText(Transformation transformation, String text) {
        if (transformation != null) {
            if (text == null) {
                transformationTextMap.remove(transformation);
            } else {
                transformationTextMap.put(transformation, text);
            }
        }
    }

    /**
     * Returns this {@link RotaryImageViewer}'s current transformations
     * 
     * @return A {@link Transformation} array, never <code>null</code>.
     */
    public Transformation[] getTransformations() {
        Transformation[] transformations;
        synchronized (filterList) {
            transformations = filterList.toArray(new Transformation[filterList.size()]);
        }
        return transformations;
    }

    /**
     * Applies transformations to this {@link RotaryImageViewer}
     * 
     * @param applyImmediately Whether to apply filters once scale set
     * @param transformations The transformations to apply
     */
    public void setTransformations(boolean applyImmediately, Transformation... transformations) {
        synchronized (filterList) {
            filterList.clear();
            noRotationButton.setSelected(true);
            horizontalSymmetryCheckBox.setSelected(false);
            verticalSymmetryCheckBox.setSelected(false);
            lastRotationAction = Transformation.NO_ROTATION;
            for (Transformation transformation : transformations) {
                if (transformation != null) {
                    filterList.add(transformation);
                    switch (transformation) {
                        case HORIZONTAL_SYMMETRY:
                            horizontalSymmetryCheckBox.setSelected(true);
                            break;
                        case VERTICAL_SYMMETRY:
                            verticalSymmetryCheckBox.setSelected(true);
                            break;
                        case NO_ROTATION:
                            noRotationButton.setSelected(true);
                            lastRotationAction = transformation;
                            break;
                        case NEGATIVE_ROTATION:
                            negativeRotationButton.setSelected(true);
                            lastRotationAction = transformation;
                            break;
                        case POSITIVE_ROTATION:
                            positiveRotationButton.setSelected(true);
                            lastRotationAction = transformation;
                    }
                }
            }
            if (applyImmediately) {
                applyTransformations();
            }
        }
    }

    /**
     * Adds or removes vertical axis symmetry in/from the transformations and applies them.
     */
    protected void processVerticalSymmetry() {
        synchronized (filterList) {
            if (verticalSymmetryCheckBox.isSelected()) {
                filterList.add(Transformation.VERTICAL_SYMMETRY);
                applyTransformations();
            } else {
                filterList.remove(Transformation.VERTICAL_SYMMETRY);
                applyTransformations();
            }
        }
    }

    /**
     * Adds or removes horizontal axis symmetry in/from the transformations and applies them.
     */
    protected void processHorizontalSymmetry() {
        synchronized (filterList) {
            if (horizontalSymmetryCheckBox.isSelected()) {
                filterList.add(Transformation.HORIZONTAL_SYMMETRY);
                applyTransformations();
            } else {
                filterList.remove(Transformation.HORIZONTAL_SYMMETRY);
                applyTransformations();
            }
        }
    }

    /**
     * Adds positive angle rotation in the transformations and applies them.
     */
    protected void processPositiveRotation() {
        synchronized (filterList) {
            // test positive rotation is not already activated
            if (lastRotationAction != Transformation.POSITIVE_ROTATION) {
                // if the negative rotation was selected, we remove it
                if (lastRotationAction == Transformation.NEGATIVE_ROTATION) {
                    filterList.remove(Transformation.NEGATIVE_ROTATION);
                }
                filterList.add(Transformation.POSITIVE_ROTATION);
                applyTransformations();

                lastRotationAction = Transformation.POSITIVE_ROTATION;
            }
        }
    }

    /**
     * Adds negative angle rotation in the transformations and applies them.
     */
    protected void processNegativeRotation() {
        synchronized (filterList) {
            // test negative rotation is not already activated
            if (lastRotationAction != Transformation.NEGATIVE_ROTATION) {
                // if the positive rotation was selected, we remove it
                if (lastRotationAction == Transformation.POSITIVE_ROTATION) {
                    filterList.remove(Transformation.POSITIVE_ROTATION);
                }
                filterList.add(Transformation.NEGATIVE_ROTATION);
                applyTransformations();
                lastRotationAction = Transformation.NEGATIVE_ROTATION;
            }
        }
    }

    /**
     * Removes rotations from the transformations and applies them.
     */
    protected void processResetRotation() {
        synchronized (filterList) {
            // test none (rotation) is not already activated
            if (lastRotationAction != Transformation.NO_ROTATION) {
                // delete all filters
                filterList.remove(Transformation.NEGATIVE_ROTATION);
                filterList.remove(Transformation.POSITIVE_ROTATION);
                applyTransformations();
                lastRotationAction = Transformation.NO_ROTATION;
            }
        }
    }

    /**
     * Applies the transformations to all known {@link ImageViewer}s.
     * 
     * @return Whether axis where switched due to the transformations.
     */
    protected boolean applyTransformations() {
        boolean switchDim = false;
        String transformationText = generateTransformationText();
        for (Entry<ImageViewer, ImageTransformationInfo> entry : transformationMap.entrySet()) {
            ImageViewer viewer = entry.getKey();
            ImageTransformationInfo info = entry.getValue();
            if ((viewer != null) && (info != null)) {
                switchDim = applyTransformations(viewer, info);
            }
        }
        transformationLabel.setText(transformationText);
        transformationLabel.setToolTipText(transformationText);
        this.switchDim = switchDim;
        return switchDim;
    }

    /**
     * Generates the text to display for applied transformations.
     * 
     * @return A {@link String}.
     */
    protected String generateTransformationText() {
        StringBuilder transformationBuilder = new StringBuilder();
        synchronized (filterList) {
            // build the filter
            for (final Transformation filterType : filterList) {
                if (transformationBuilder.length() > 0) {
                    transformationBuilder.append(FILTER_SEPARATOR);
                }
                String text = transformationTextMap.get(filterType);
                switch (filterType) {
                    case POSITIVE_ROTATION:
                        transformationBuilder.append(text == null ? filterType : text);
                        break;
                    case NEGATIVE_ROTATION:
                        transformationBuilder.append(text == null ? filterType : text);
                        break;
                    case VERTICAL_SYMMETRY:
                        transformationBuilder.append(text == null ? filterType : text);
                        break;
                    case HORIZONTAL_SYMMETRY:
                        transformationBuilder.append(text == null ? filterType : text);
                        break;
                    default:
                        break;
                }
            }
        }
        if (transformationBuilder.length() == 0) {
            transformationBuilder.append(Transformation.NO_ROTATION);
        }
        String transformationText = transformationBuilder.toString();
        transformationBuilder.delete(0, transformationBuilder.length());
        return transformationText;
    }

    /**
     * Applies the transformations, stored in given {@link ImageTransformationInfo}, to given {@link ImageViewer}.
     * 
     * @param imageViewer The {@link ImageViewer}.
     * @param transformationInfo The {@link ImageTransformationInfo}.
     * @return Whether axis where switched due to the transformations.
     */
    protected boolean applyTransformations(ImageViewer imageViewer, ImageTransformationInfo transformationInfo) {
        boolean switchDim = false;
        if ((imageViewer != null) && (transformationInfo != null)) {
            // reset previous filter
            transformationInfo.setXTitleToUse(transformationInfo.getXTitle());
            transformationInfo.setXAxisFormatToUse(transformationInfo.getXAxisFormat());
            transformationInfo.setXAxisConvertorToUse(transformationInfo.getXAxisConvertor());
            transformationInfo.setYTitleToUse(transformationInfo.getYTitle());
            transformationInfo.setYAxisFormatToUse(transformationInfo.getYAxisFormat());
            transformationInfo.setYAxisConvertorToUse(transformationInfo.getYAxisConvertor());
            transformationInfo.setBeamPointToUse(transformationInfo.getBeamPoint());

            IDataContainer<FlatData> dataContainer = new IDataContainer<FlatData>() {
                @Override
                public FlatData getData() throws Exception {
                    return transformationInfo == null ? null : transformationInfo.getLastData();
                }

                @Override
                public void setData(FlatData data) throws Exception {
                }

                @Override
                public boolean isSettable() {
                    return false;
                }

                @Override
                public GenericDescriptor getDataType() {
                    return (transformationInfo.getLastData() == null ? null
                            : new GenericDescriptor(transformationInfo.getLastData().getClass()));
                }

                @Override
                public boolean isUnsigned() {
                    return false;
                }
            };

            synchronized (filterList) {
                // build the filter
                for (final Transformation filterType : filterList) {
                    switch (filterType) {
                        case POSITIVE_ROTATION:
                            dataContainer = new FlatMatrixRightAngleRotation<>(FlatData.class, dataContainer, true);
                            xToY(transformationInfo);
                            switchDim = !switchDim;
                            reverseY(switchDim, transformationInfo);
                            break;
                        case NEGATIVE_ROTATION:
                            dataContainer = new FlatMatrixRightAngleRotation<>(FlatData.class, dataContainer, false);
                            xToY(transformationInfo);
                            switchDim = !switchDim;
                            reverseX(switchDim, transformationInfo);
                            break;
                        case VERTICAL_SYMMETRY:
                            dataContainer = new FlatMatrixSymmetry<>(FlatData.class, dataContainer,
                                    FlatMatrixSymmetry.VERTICAL);
                            reverseX(switchDim, transformationInfo);
                            break;
                        case HORIZONTAL_SYMMETRY:
                            dataContainer = new FlatMatrixSymmetry<>(FlatData.class, dataContainer,
                                    FlatMatrixSymmetry.HORIZONTAL);
                            reverseY(switchDim, transformationInfo);
                            break;
                        default:
                            break;
                    }
                }
            }

            // apply the filter
            imageViewer.setXAxisConvertor(transformationInfo.getXAxisConvertorToUse(), false);
            imageViewer.setYAxisConvertor(transformationInfo.getYAxisConvertorToUse());
            imageViewer.getXAxis().getAttributes().setTitle(transformationInfo.getXTitleToUse());
            imageViewer.getYAxis().getAttributes().setTitle(transformationInfo.getYTitleToUse());
            imageViewer.setXAxisFormat(transformationInfo.getXAxisFormatToUse());
            imageViewer.setYAxisFormat(transformationInfo.getYAxisFormatToUse());
            try {
                FlatData containerData = dataContainer.getData();
                if (containerData == null) {
                    if (imageViewer.getValue() != null) {
                        imageViewer.setFlatNumberMatrix(null, 0, 0);
                    }
                } else {
                    Object value = containerData.getValue();
                    int[] shape = containerData.getRealShape();
                    if (value != imageViewer.getValue()) {
                        imageViewer.setFlatNumberMatrix(value, shape[1], shape[0]);
                    }
                }
            } catch (Exception e) {
                // Should not happen
                String applicationId = imageViewer.getApplicationId();
                LoggerFactory.getLogger(
                        applicationId == null || applicationId.isEmpty() ? Mediator.LOGGER_ACCESS : applicationId)
                        .error("Failed to apply image transformation", e);
            }
            if ((transformationInfo.getBeamPointToUse() == null)
                    || (transformationInfo.getBeamPointToUse().length != 2)) {
                imageViewer.clearBeamPoint();
            } else {
                if (switchDim) {
                    transformationInfo.setBeamPointToUse(transformationInfo.getBeamPointToUse()[1],
                            transformationInfo.getBeamPointToUse()[0]);
                }
                imageViewer.setBeamPoint(transformationInfo.getBeamPointToUse());
            }
        }
        return switchDim;
    }

    /**
     * Switches axis in given {@link ImageTransformationInfo}.
     * 
     * @param transformationInfo The {@link ImageTransformationInfo}.
     */
    protected void xToY(ImageTransformationInfo transformationInfo) {
        IValueConvertor tmpConvertor = transformationInfo.getXAxisConvertorToUse();
        String tmp = transformationInfo.getXTitleToUse();
        transformationInfo.setXAxisConvertorToUse(transformationInfo.getYAxisConvertorToUse());
        transformationInfo.setXTitleToUse(transformationInfo.getYTitleToUse());
        transformationInfo.setYAxisConvertorToUse(tmpConvertor);
        transformationInfo.setYTitleToUse(tmp);
        tmp = transformationInfo.getXAxisFormatToUse();
        transformationInfo.setXAxisFormatToUse(transformationInfo.getYAxisFormatToUse());
        transformationInfo.setYAxisFormatToUse(tmp);
    }

    /**
     * Reverses X axis in given {@link ImageTransformationInfo}.
     * 
     * @param switchDim Whether axis where previously switched.
     * @param transformationInfo The {@link ImageTransformationInfo}.
     */
    protected void reverseX(boolean switchDim, ImageTransformationInfo transformationInfo) {
        int[] size = transformationInfo.getLastData() == null ? null : transformationInfo.getLastData().getRealShape();
        int dimX, dimY;
        if ((size == null) || (size.length != 2)) {
            dimX = 0;
            dimY = 0;
        } else {
            dimX = size[1];
            dimY = size[0];
        }
        DimensionAdapter adapter = new DimensionAdapter();
        adapter.setConvertor(transformationInfo.getXAxisConvertorToUse(), false);
        adapter.setDim(switchDim ? dimY : dimX, false);
        transformationInfo.setXAxisConvertorToUse(adapter);
    }

    /**
     * Reverses Y axis in given {@link ImageTransformationInfo}.
     * 
     * @param switchDim Whether axis where previously switched.
     * @param transformationInfo The {@link ImageTransformationInfo}.
     */
    protected void reverseY(boolean switchDim, ImageTransformationInfo transformationInfo) {
        int[] size = transformationInfo.getLastData() == null ? null : transformationInfo.getLastData().getRealShape();
        int dimX, dimY;
        if ((size == null) || (size.length != 2)) {
            dimX = 0;
            dimY = 0;
        } else {
            dimX = size[1];
            dimY = size[0];
        }
        DimensionAdapter adapter = new DimensionAdapter();
        adapter.setConvertor(transformationInfo.getYAxisConvertorToUse(), false);
        adapter.setDim(switchDim ? dimX : dimY, false);
        transformationInfo.setYAxisConvertorToUse(adapter);
    }

    /**
     * Sets the X scale title.
     * 
     * @param applyImmediately Whether to apply filters once scale set.
     * @param name The X scale title.
     */
    public void setXScale(boolean applyImmediately, String name) {
        synchronized (filterList) {
            for (ImageTransformationInfo info : transformationMap.values()) {
                info.setXTitle(name);
            }
            if (applyImmediately) {
                applyTransformations();
            }
        }
    }

    /**
     * Sets the Y scale title.
     * 
     * @param applyImmediately Whether to apply filters once scale set.
     * @param name The Y scale title.
     */
    public void setYScale(boolean applyImmediately, String name) {
        synchronized (filterList) {
            for (ImageTransformationInfo info : transformationMap.values()) {
                info.setYTitle(name);
            }
            if (applyImmediately) {
                applyTransformations();
            }
        }
    }

    /**
     * Returns the X axis format.
     * 
     * @return A {@link String}.
     */
    public String getXAxisFormat() {
        String xAxisFormat = null;
        for (ImageTransformationInfo info : transformationMap.values()) {
            xAxisFormat = info.getXAxisFormat();
            break;
        }
        return xAxisFormat;
    }

    /**
     * Sets the X axis format.
     * 
     * @param applyImmediately Whether to apply filters once format set.
     * @param xAxisFormat The X axis format.
     */
    public void setXAxisFormat(boolean applyImmediately, String xAxisFormat) {
        synchronized (filterList) {
            for (ImageTransformationInfo info : transformationMap.values()) {
                info.setXAxisFormat(xAxisFormat);
            }
            if (applyImmediately) {
                applyTransformations();
            }
        }
    }

    /**
     * Returns the Y axis format.
     * 
     * @return A {@link String}.
     */
    public String getYAxisFormat() {
        String yAxisFormat = null;
        for (ImageTransformationInfo info : transformationMap.values()) {
            yAxisFormat = info.getYAxisFormat();
            break;
        }
        return yAxisFormat;
    }

    /**
     * Sets the Y axis format.
     * 
     * @param applyImmediately Whether to apply filters once format set.
     * @param yAxisFormat They axis format.
     */
    public void setYAxisFormat(boolean applyImmediately, String yAxisFormat) {
        synchronized (filterList) {
            for (ImageTransformationInfo info : transformationMap.values()) {
                info.setYAxisFormat(yAxisFormat);
            }
            if (applyImmediately) {
                applyTransformations();
            }
        }
    }

    // ////////////// //
    // Inner classes //
    // ////////////// //

    /**
     * Algorithm: we maintain a list which contains all transformations ordered to their order of
     * appearance. the first element is the transformation apply to target.
     * 
     * When user select a rotation transformation we remove the previous selected rotation from the
     * list and add at the end the new rotation.
     * 
     * When user select (resp. deselect) a symmetry. We add (resp. remove) it to the list
     * 
     */
    protected class TransformationActionListener implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            if (e.getSource() == positiveRotationButton) {
                processPositiveRotation();

            } else if (e.getSource() == negativeRotationButton) {
                processNegativeRotation();

            } else if (e.getSource() == noRotationButton) {
                processResetRotation();

            } else if (e.getSource() == horizontalSymmetryCheckBox) {
                processHorizontalSymmetry();

            } else if (e.getSource() == verticalSymmetryCheckBox) {
                processVerticalSymmetry();
            }
        }

    }

    /**
     * An {@link IImageViewer} that is able to automatically apply transformations to an {@link ImageViewer}
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class ImageAdapter implements IImageViewer, IImageViewerListener {

        protected ImageViewer imageViewer;
        protected final NumberArrayToBooleanArrayAdapter<Byte, byte[], boolean[]> arrayAdapter;
        protected final List<IImageViewerListener> listeners;

        /**
         * Constructs a new {@link ImageAdapter}.
         * 
         * @param imageViewer The {@link ImageViewer} linked with this {@link ImageAdapter}.
         */
        public ImageAdapter(ImageViewer imageViewer) {
            super();
            this.listeners = new ArrayList<>();
            this.imageViewer = imageViewer;
            if (this.imageViewer != null) {
                this.imageViewer.addImageViewerListener(this);
            }
            this.arrayAdapter = new NumberArrayToBooleanArrayAdapter<>(Byte.TYPE, Boolean.TYPE);
        }

        public ImageViewer getImageViewer() {
            return imageViewer;
        }

        protected ImageTransformationInfo getTransformationInfo() {
            return imageViewer == null ? null : transformationMap.get(imageViewer);
        }

        @Override
        public String getSnapshotDirectory() {
            return imageViewer == null ? null : imageViewer.getSnapshotDirectory();
        }

        @Override
        public void setSnapshotDirectory(String snapshotDirectory) {
            if (imageViewer != null) {
                imageViewer.setSnapshotDirectory(snapshotDirectory);
            }
        }

        @Override
        public String getSnapshotFile() {
            return imageViewer == null ? null : imageViewer.getSnapshotFile();
        }

        @Override
        public void addSnapshotListener(ISnapshotListener listener) {
            if (imageViewer != null) {
                imageViewer.addSnapshotListener(listener);
            }
        }

        @Override
        public void removeSnapshotListener(ISnapshotListener listener) {
            if (imageViewer != null) {
                imageViewer.removeSnapshotListener(listener);
            }
        }

        @Override
        public void loadDataFile(String fileName) {
            synchronized (filterList) {
                if (imageViewer != null) {
                    imageViewer.loadDataFile(fileName);
                    applyTransformations(imageViewer, getTransformationInfo());
                }
            }
        }

        @Override
        public void saveDataFile(String path) {
            if (imageViewer != null) {
                imageViewer.saveDataFile(path);
            }
        }

        @Override
        public String getDataDirectory() {
            return imageViewer == null ? null : imageViewer.getDataDirectory();
        }

        @Override
        public void setDataDirectory(String path) {
            if (imageViewer != null) {
                imageViewer.setDataDirectory(path);
            }
        }

        @Override
        public String getDataFile() {
            return imageViewer == null ? null : imageViewer.getDataFile();
        }

        @Override
        public void addDataTransferListener(IDataTransferListener listener) {
            if (imageViewer != null) {
                imageViewer.addDataTransferListener(listener);
            }
        }

        @Override
        public void removeDataTransferListener(IDataTransferListener listener) {
            if (imageViewer != null) {
                imageViewer.removeDataTransferListener(listener);
            }
        }

        @Override
        public void addMaskListener(MaskListener listener) {
            if (imageViewer != null) {
                imageViewer.addMaskListener(listener);
            }
        }

        @Override
        public void removeMaskListener(MaskListener listener) {
            if (imageViewer != null) {
                imageViewer.removeMaskListener(listener);
            }
        }

        @Override
        public void removeAllMaskListeners() {
            if (imageViewer != null) {
                imageViewer.removeAllMaskListeners();
            }
        }

        @Override
        public boolean setMask(Mask mask) {
            return imageViewer == null ? false : imageViewer.setMask(mask);
        }

        @Override
        public Mask getMask() {
            return imageViewer == null ? null : imageViewer.getMask();
        }

        // TODO maybe return original width/height
        @Override
        public int getImageWidth() {
            return imageViewer == null ? 0 : imageViewer.getImageWidth();
        }

        @Override
        public int getImageHeight() {
            return imageViewer == null ? 0 : imageViewer.getImageHeight();
        }

        @Override
        public String getImageName() {
            return imageViewer == null ? null : imageViewer.getImageName();
        }

        @Override
        public void setImageName(String name) {
            if (imageViewer != null) {
                imageViewer.setImageName(name);
            }
        }

        @Override
        public void addImageViewerListener(IImageViewerListener listener) {
            synchronized (listeners) {
                if (!listeners.contains(listener)) {
                    listeners.add(listener);
                }
            }
        }

        @Override
        public void removeImageViewerListener(IImageViewerListener listener) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }

        @Override
        public Gradient getGradient() {
            return imageViewer == null ? null : imageViewer.getGradient();
        }

        @Override
        public void setGradient(Gradient gradient) {
            if (imageViewer != null) {
                imageViewer.setGradient(gradient);
            }
        }

        @Override
        public boolean isUseMaskManagement() {
            return imageViewer == null ? false : imageViewer.isUseMaskManagement();
        }

        @Override
        public void setUseMaskManagement(boolean useMask) {
            if (imageViewer != null) {
                imageViewer.setUseMaskManagement(useMask);
            }
        }

        @Override
        public void loadMask(String path, boolean addToCurrentMask) {
            if (imageViewer != null) {
                imageViewer.loadMask(path, addToCurrentMask);
            }
        }

        @Override
        public void saveMask(String path) {
            if (imageViewer != null) {
                imageViewer.saveMask(path);
            }
        }

        @Override
        public boolean isAlwaysFitMaxSize() {
            return imageViewer == null ? false : imageViewer.isAlwaysFitMaxSize();
        }

        @Override
        public void setAlwaysFitMaxSize(boolean alwaysFitMaxSize) {
            if (imageViewer != null) {
                imageViewer.setAlwaysFitMaxSize(alwaysFitMaxSize);
            }
        }

        @Override
        public void setShowRoiInformationTable(boolean showTable) {
            if (imageViewer != null) {
                imageViewer.setShowRoiInformationTable(showTable);
            }
        }

        @Override
        public boolean isShowRoiInformationTable() {
            return imageViewer == null ? false : imageViewer.isShowRoiInformationTable();
        }

        @Override
        public boolean isDrawBeamPosition() {
            return imageViewer == null ? false : imageViewer.isDrawBeamPosition();
        }

        @Override
        public void setDrawBeamPosition(boolean drawBeam) {
            if (imageViewer != null) {
                imageViewer.setDrawBeamPosition(drawBeam);
            }
        }

        @Override
        public double[] getBeamPoint() {
            double[] adaptedBeamPoint;
            if (imageViewer == null) {
                adaptedBeamPoint = null;
            } else {
                double[] beamPoint = imageViewer.getBeamPoint();
                if ((beamPoint != null) && (beamPoint.length == 2) && switchDim) {
                    adaptedBeamPoint = new double[] { beamPoint[1], beamPoint[0] };
                } else {
                    adaptedBeamPoint = beamPoint;
                }
            }
            return adaptedBeamPoint;
        }

        @Override
        public void setBeamPoint(double... beamPoint) {
            synchronized (filterList) {
                ImageTransformationInfo transformationInfo = getTransformationInfo();
                if (transformationInfo != null) {
                    transformationInfo.setBeamPoint(beamPoint);
                    applyTransformations(imageViewer, transformationInfo);
                }
            }
        }

        @Override
        public void clearBeamPoint() {
            if (imageViewer != null) {
                imageViewer.clearBeamPoint();
            }
        }

        @Override
        public void registerSectorClass(Class<?> sectorClass) {
            if (imageViewer != null) {
                imageViewer.registerSectorClass(sectorClass);
            }
        }

        @Override
        public boolean isUseSectorManagement() {
            return imageViewer == null ? false : imageViewer.isUseSectorManagement();
        }

        @Override
        public void setUseSectorManagement(boolean useSector) {
            if (imageViewer != null) {
                imageViewer.setUseSectorManagement(useSector);
            }
        }

        @Override
        public MathematicSector getSector() {
            return imageViewer == null ? null : imageViewer.getSector();
        }

        @Override
        public void setSector(MathematicSector sector) {
            if (imageViewer != null) {
                imageViewer.setSector(sector);
            }
        }

        @Override
        public IValueConvertor getXAxisConvertor() {
            ImageTransformationInfo transformationInfo = getTransformationInfo();
            return transformationInfo == null ? null : transformationInfo.getXAxisConvertor();
        }

        @Override
        public void setXAxisConvertor(IValueConvertor xAxisConvertor) {
            setXAxisConvertor(true, xAxisConvertor);
        }

        public void setXAxisConvertor(boolean apply, IValueConvertor xAxisConvertor) {
            synchronized (filterList) {
                ImageTransformationInfo transformationInfo = getTransformationInfo();
                if ((transformationInfo != null) && (transformationInfo.getXAxisConvertor() != xAxisConvertor)) {
                    transformationInfo.setXAxisConvertor(xAxisConvertor);
                    if (apply) {
                        applyTransformations(imageViewer, transformationInfo);
                    }
                }
            }
        }

        @Override
        public IValueConvertor getYAxisConvertor() {
            ImageTransformationInfo transformationInfo = getTransformationInfo();
            return transformationInfo == null ? null : transformationInfo.getYAxisConvertor();
        }

        @Override
        public void setYAxisConvertor(IValueConvertor yAxisConvertor) {
            setYAxisConvertor(true, yAxisConvertor);
        }

        public void setYAxisConvertor(boolean apply, IValueConvertor yAxisConvertor) {
            synchronized (filterList) {
                ImageTransformationInfo transformationInfo = getTransformationInfo();
                if ((transformationInfo != null) && (transformationInfo.getYAxisConvertor() != yAxisConvertor)) {
                    transformationInfo.setYAxisConvertor(yAxisConvertor);
                    if (apply) {
                        applyTransformations(imageViewer, transformationInfo);
                    }
                }
            }
        }

        @Override
        public boolean isSingleRoiMode() {
            return imageViewer == null ? false : imageViewer.isSingleRoiMode();
        }

        @Override
        public void setSingleRoiMode(boolean singleRoiMode) {
            if (imageViewer != null) {
                imageViewer.setSingleRoiMode(singleRoiMode);
            }
        }

        @Override
        public boolean isCleanOnDataSetting() {
            return imageViewer == null ? false : imageViewer.isCleanOnDataSetting();
        }

        @Override
        public void setCleanOnDataSetting(boolean clean) {
            if (imageViewer != null) {
                imageViewer.setCleanOnDataSetting(clean);
            }
        }

        @Override
        public void addRoi(IRoi roi) {
            if (imageViewer != null) {
                imageViewer.addRoi(roi);
            }
        }

        @Override
        public void addRoi(IRoi roi, boolean centered) {
            if (imageViewer != null) {
                imageViewer.addRoi(roi, centered);
            }
        }

        @Override
        public CometeColor getNanColor() {
            return imageViewer == null ? null : imageViewer.getNanColor();
        }

        @Override
        public void setNanColor(CometeColor nanColor) {
            if (imageViewer != null) {
                imageViewer.setNanColor(nanColor);
            }
        }

        @Override
        public CometeColor getPositiveInfinityColor() {
            return imageViewer == null ? null : imageViewer.getPositiveInfinityColor();
        }

        @Override
        public void setPositiveInfinityColor(CometeColor positiveInfinityColor) {
            if (imageViewer != null) {
                imageViewer.setPositiveInfinityColor(positiveInfinityColor);
            }
        }

        @Override
        public CometeColor getNegativeInfinityColor() {
            return imageViewer == null ? null : imageViewer.getNegativeInfinityColor();
        }

        @Override
        public void setNegativeInfinityColor(CometeColor negativeInfinityColor) {
            if (imageViewer != null) {
                imageViewer.setNegativeInfinityColor(negativeInfinityColor);
            }
        }

        @Override
        public ImageProperties getImageProperties() {
            return imageViewer == null ? null : imageViewer.getImageProperties();
        }

        @Override
        public void setImageProperties(ImageProperties properties) {
            if (imageViewer != null) {
                imageViewer.setImageProperties(properties);
            }
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            // Not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // Not managed
        }

        @Override
        public String getText() {
            return imageViewer == null ? null : imageViewer.getText();
        }

        @Override
        public void setText(String text) {
            if (imageViewer != null) {
                imageViewer.setText(text);
            }
        }

        @Override
        public boolean hasFocus() {
            return imageViewer == null ? false : imageViewer.hasFocus();
        }

        @Override
        public void setOpaque(boolean opaque) {
            if (imageViewer != null) {
                imageViewer.setOpaque(opaque);
            }
        }

        @Override
        public boolean isOpaque() {
            return imageViewer == null ? false : imageViewer.isOpaque();
        }

        @Override
        public void setCometeBackground(CometeColor color) {
            if (imageViewer != null) {
                imageViewer.setCometeBackground(color);
            }
        }

        @Override
        public CometeColor getCometeBackground() {
            return imageViewer == null ? null : imageViewer.getCometeBackground();
        }

        @Override
        public void setCometeForeground(CometeColor color) {
            if (imageViewer != null) {
                imageViewer.setCometeForeground(color);
            }
        }

        @Override
        public CometeColor getCometeForeground() {
            return imageViewer == null ? null : imageViewer.getCometeForeground();
        }

        @Override
        public void setEnabled(boolean enabled) {
            if (imageViewer != null) {
                imageViewer.setEnabled(enabled);
            }
        }

        @Override
        public boolean isEnabled() {
            return imageViewer == null ? false : imageViewer.isEnabled();
        }

        @Override
        public void setCometeFont(CometeFont font) {
            if (imageViewer != null) {
                imageViewer.setCometeFont(font);
            }
        }

        @Override
        public CometeFont getCometeFont() {
            return imageViewer == null ? null : imageViewer.getCometeFont();
        }

        @Override
        public void setToolTipText(String text) {
            if (imageViewer != null) {
                imageViewer.setToolTipText(text);
            }
        }

        @Override
        public String getToolTipText() {
            return imageViewer == null ? null : imageViewer.getToolTipText();
        }

        @Override
        public void setVisible(boolean visible) {
            if (imageViewer != null) {
                imageViewer.setVisible(visible);
            }
        }

        @Override
        public boolean isVisible() {
            return imageViewer == null ? false : imageViewer.isVisible();
        }

        @Override
        public void setHorizontalAlignment(int halign) {
            if (imageViewer != null) {
                imageViewer.setHorizontalAlignment(halign);
            }
        }

        @Override
        public int getHorizontalAlignment() {
            return imageViewer == null ? CENTER : imageViewer.getHorizontalAlignment();
        }

        @Override
        public void setSize(int width, int height) {
            if (imageViewer != null) {
                imageViewer.setSize(width, height);
            }
        }

        @Override
        public void setPreferredSize(int width, int height) {
            if (imageViewer != null) {
                imageViewer.setPreferredSize(width, height);
            }
        }

        @Override
        public int getWidth() {
            return imageViewer == null ? 0 : imageViewer.getWidth();
        }

        @Override
        public int getHeight() {
            return imageViewer == null ? 0 : imageViewer.getHeight();
        }

        @Override
        public void setLocation(int x, int y) {
            if (imageViewer != null) {
                imageViewer.setLocation(x, y);
            }
        }

        @Override
        public int getX() {
            return imageViewer == null ? 0 : imageViewer.getX();
        }

        @Override
        public int getY() {
            return imageViewer == null ? 0 : imageViewer.getY();
        }

        @Override
        public boolean isEditingData() {
            return imageViewer == null ? false : imageViewer.isEditingData();
        }

        @Override
        public void setTitledBorder(String title) {
            if (imageViewer != null) {
                imageViewer.setTitledBorder(title);
            }
        }

        @Override
        public void addMouseListener(IMouseListener listener) {
            if (imageViewer != null) {
                imageViewer.addMouseListener(listener);
            }
        }

        @Override
        public void removeMouseListener(IMouseListener listener) {
            if (imageViewer != null) {
                imageViewer.removeMouseListener(listener);
            }
        }

        @Override
        public void removeAllMouseListeners() {
            if (imageViewer != null) {
                imageViewer.removeAllMouseListeners();
            }
        }

        @Override
        public void notifyForError(String message, Throwable error) {
            if (imageViewer != null) {
                imageViewer.notifyForError(message, error);
            }
        }

        @Override
        public IDrawingThreadManager getDrawingThreadManager() {
            return imageViewer == null ? null : imageViewer.getDrawingThreadManager();
        }

        @Override
        public boolean shouldReceiveDataInDrawingThread() {
            return imageViewer == null ? false : imageViewer.shouldReceiveDataInDrawingThread();
        }

        @Override
        public String getFormat() {
            return imageViewer == null ? null : imageViewer.getFormat();
        }

        @Override
        public void setFormat(String format) {
            if (imageViewer != null) {
                imageViewer.setFormat(format);
            }
        }

        @Override
        public String getXAxisFormat() {
            String format;
            if (imageViewer == null) {
                format = null;
            } else if (switchDim) {
                format = imageViewer.getYAxisFormat();
            } else {
                format = imageViewer.getXAxisFormat();
            }
            return format;
        }

        @Override
        public void setXAxisFormat(String format) {
            if (imageViewer != null) {
                ImageTransformationInfo info = transformationMap.get(imageViewer);
                if (info != null) {
                    info.setXAxisFormat(format);
                }
                if (switchDim) {
                    if (info != null) {
                        info.setYAxisFormatToUse(format);
                    }
                    imageViewer.setYAxisFormat(format);
                } else {
                    if (info != null) {
                        info.setXAxisFormatToUse(format);
                    }
                    imageViewer.setXAxisFormat(format);
                }
            }
        }

        @Override
        public String getYAxisFormat() {
            String format;
            if (imageViewer == null) {
                format = null;
            } else if (switchDim) {
                format = imageViewer.getXAxisFormat();
            } else {
                format = imageViewer.getYAxisFormat();
            }
            return format;
        }

        @Override
        public void setYAxisFormat(String format) {
            if (imageViewer != null) {
                ImageTransformationInfo info = transformationMap.get(imageViewer);
                if (info != null) {
                    info.setYAxisFormat(format);
                }
                if (switchDim) {
                    if (info != null) {
                        info.setXAxisFormatToUse(format);
                    }
                    imageViewer.setXAxisFormat(format);
                } else {
                    if (info != null) {
                        info.setYAxisFormatToUse(format);
                    }
                    imageViewer.setYAxisFormat(format);
                }
            }
        }

        @Override
        public Object[] getNumberMatrix() {
            ImageTransformationInfo transformationInfo = getTransformationInfo();
            FlatData data = transformationInfo == null ? null : transformationInfo.getLastData();
            return data == null ? null
                    : (Object[]) ArrayUtils.convertArrayDimensionFrom1ToN(data.getValue(), data.getRealShape());
        }

        @Override
        public void setNumberMatrix(Object[] value) {
            ImageTransformationInfo transformationInfo = getTransformationInfo();
            if (transformationInfo != null) {
                FlatData data;
                if (value == null) {
                    data = null;
                } else {
                    int[] shape = ArrayUtils.recoverShape(value);
                    data = new FlatData(shape, ArrayUtils.convertArrayDimensionFromNTo1(value));
                }
                transformationInfo.setLastData(data);
                applyTransformations(imageViewer, transformationInfo);
            }
        }

        @Override
        public Object getFlatNumberMatrix() {
            ImageTransformationInfo transformationInfo = getTransformationInfo();
            FlatData data = transformationInfo == null ? null : transformationInfo.getLastData();
            return data == null ? null : data.getValue();
        }

        @Override
        public void setFlatNumberMatrix(Object value, int width, int height) {
            ImageTransformationInfo transformationInfo = getTransformationInfo();
            if (transformationInfo != null) {
                FlatData data;
                if (value == null) {
                    data = null;
                } else {
                    data = new FlatData(new int[] { height, width }, value);
                }
                transformationInfo.setLastData(data);
                applyTransformations(imageViewer, transformationInfo);
            }
        }

        @Override
        public boolean isPreferFlatValues(Class<?> concernedDataClass) {
            return ObjectUtils.isNumberClass(concernedDataClass);
        }

        @Override
        public int getMatrixDataWidth(Class<?> concernedDataClass) {
            ImageTransformationInfo transformationInfo = getTransformationInfo();
            int result;
            if (transformationInfo == null) {
                result = 0;
            } else if (ObjectUtils.isNumberClass(concernedDataClass)) {
                FlatData data = transformationInfo.getLastData();
                if ((data == null) || (data.getRealShape() == null) || (data.getRealShape().length != 2)) {
                    result = 0;
                } else {
                    result = data.getRealShape()[1];
                }
            } else {
                result = 0;
            }
            return result;
        }

        @Override
        public int getMatrixDataHeight(Class<?> concernedDataClass) {
            ImageTransformationInfo transformationInfo = getTransformationInfo();
            int result;
            if (transformationInfo == null) {
                result = 0;
            } else if (ObjectUtils.isNumberClass(concernedDataClass)) {
                FlatData data = transformationInfo.getLastData();
                if ((data == null) || (data.getRealShape() == null) || (data.getRealShape().length != 2)) {
                    result = 0;
                } else {
                    result = data.getRealShape()[0];
                }
            } else {
                result = 0;
            }
            return result;
        }

        @Override
        public boolean[][] getBooleanMatrix() {
            // Not managed
            return null;
        }

        @Override
        public void setBooleanMatrix(boolean[][] value) {
            Object flatValue = null;
            int width = 0, height = 0;
            if (value != null) {
                height = value.length;
                int[] shape = ArrayUtils.recoverShape(value);
                if (shape != null && shape.length == 2) {
                    height = shape[0];
                    width = shape[1];
                    flatValue = ArrayUtils.convertArrayDimensionFromNTo1(value);
                }
            }
            setFlatBooleanMatrix((boolean[]) flatValue, width, height);
        }

        @Override
        public boolean[] getFlatBooleanMatrix() {
            // Not managed
            return null;
        }

        @Override
        public void setFlatBooleanMatrix(boolean[] value, int width, int height) {
            byte[] byteValue;
            try {
                byteValue = arrayAdapter.revertAdapt(value);
            } catch (DataAdaptationException e) {
                byteValue = null;
            }
            setFlatNumberMatrix(byteValue, width, height);
        }

        @Override
        public void setEditable(boolean editable) {
            if (imageViewer != null) {
                imageViewer.setEditable(editable);
            }
        }

        @Override
        public boolean isEditable() {
            return imageViewer == null ? false : imageViewer.isEditable();
        }

        @Override
        public void imageViewerChanged(ImageViewerEvent event) {
            // Only forward and adapt selection events
            if ((event != null) && (event.getSource() != null) && (event.getSource() == imageViewer)
                    && (event.getReason() == Reason.SELECTION)) {
                ImageViewerEvent newEvent = new ImageViewerEvent(this,
                        switchDim ? new double[] { event.getSelectedPoint()[1], event.getSelectedPoint()[0] }
                                : event.getSelectedPoint(),
                        event.getValue(), event.getReason());
                List<IImageViewerListener> copy;
                synchronized (listeners) {
                    copy = new ArrayList<>(listeners);
                }
                for (IImageViewerListener listener : copy) {
                    listener.imageViewerChanged(newEvent);
                }
                copy.clear();
            }
        }

        @Override
        protected void finalize() throws Throwable {
            if (imageViewer != null) {
                imageViewer.removeImageViewerListener(this);
            }
            imageViewer = null;
            listeners.clear();
            super.finalize();
        }

    }
}
