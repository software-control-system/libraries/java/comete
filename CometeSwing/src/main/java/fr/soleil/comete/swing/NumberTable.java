/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.lang.reflect.Array;
import java.util.Arrays;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.event.TableModelEvent;

import org.jdesktop.swingx.JXTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.data.target.matrix.INumberMatrixComponent;
import fr.soleil.comete.definition.util.FileArrayUtils;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.DefaultTableModelWithRowName;
import fr.soleil.comete.swing.util.mask.MaskedTable;
import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.adapter.AdapterUtils;
import fr.soleil.data.container.matrix.FlatMatrix;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.math.ArrayUtils;

public class NumberTable extends AbstractTable<Object> implements INumberMatrixComponent {

    private static final long serialVersionUID = 5027507657626850738L;

    protected int bufferredWidth = 0;
    protected int bufferredHeight = 0;
    protected int width;
    protected int height;
    protected AbstractAdapter<String, ? extends Number> converter;
    private final Logger LOGGER;

    public NumberTable() {
        super();
        LOGGER = LoggerFactory.getLogger(Mediator.LOGGER_ACCESS);
        updateConverter(Double.class);
    }

    private void updateConverter(Class<? extends Number> type) {
        converter = AdapterUtils.getAdapter(new GenericDescriptor(String.class), new GenericDescriptor(type));
    }

    @Override
    protected void applySelection() {
        if ((matrix != null) && (table.getSelectedColumnCount() > 0)) {
            int[] selectedColumn = table.getSelectedColumns();
            int[] selectedRow = table.getSelectedRows();
            Object newMatrix = null;
            if (matrix instanceof Number[]) {
                AbstractAdapter<String, ? extends Number> converter = this.converter;
                if (converter == null) {
                    converter = AdapterUtils.getAdapter(new GenericDescriptor(String.class),
                            new GenericDescriptor(Double.class));
                    if (this.converter == null) {
                        this.converter = converter;
                    }
                }
                Number[] newValues = (Number[]) Array.newInstance(matrix.getClass().getComponentType(),
                        selectedRow.length * selectedColumn.length);
                for (int y = 0; y < selectedRow.length; y++) {
                    for (int x = 0; x < selectedColumn.length; x++) {
                        Number value;
                        try {
                            value = converter.adapt(table.getValueAt(selectedRow[y], selectedColumn[x]).toString());
                            newValues[y * selectedColumn.length + x] = value;
                        } catch (DataAdaptationException e) {
                            LOGGER.error("Failed to parse data at y=" + y + ", x=" + x, e);
                            value = Double.NaN;
                        }
                    }
                }
                newMatrix = newValues;
            } else if (matrix instanceof byte[]) {
                byte[] newValues = new byte[selectedRow.length * selectedColumn.length];
                for (int y = 0; y < selectedRow.length; y++) {
                    for (int x = 0; x < selectedColumn.length; x++) {
                        byte value = Byte.parseByte(table.getValueAt(selectedRow[y], selectedColumn[x]).toString());
                        newValues[y * selectedColumn.length + x] = value;
                    }
                }
                newMatrix = newValues;
            } else if (matrix instanceof short[]) {
                short[] newValues = new short[selectedRow.length * selectedColumn.length];
                for (int y = 0; y < selectedRow.length; y++) {
                    for (int x = 0; x < selectedColumn.length; x++) {
                        short value = Short.parseShort(table.getValueAt(selectedRow[y], selectedColumn[x]).toString());
                        newValues[y * selectedColumn.length + x] = value;
                    }
                }
                newMatrix = newValues;
            } else if (matrix instanceof int[]) {
                int[] newValues = new int[selectedRow.length * selectedColumn.length];
                for (int y = 0; y < selectedRow.length; y++) {
                    for (int x = 0; x < selectedColumn.length; x++) {
                        int value = Integer.parseInt(table.getValueAt(selectedRow[y], selectedColumn[x]).toString());
                        newValues[y * selectedColumn.length + x] = value;
                    }
                }
                newMatrix = newValues;
            } else if (matrix instanceof long[]) {
                long[] newValues = new long[selectedRow.length * selectedColumn.length];
                for (int y = 0; y < selectedRow.length; y++) {
                    for (int x = 0; x < selectedColumn.length; x++) {
                        long value = Long.parseLong(table.getValueAt(selectedRow[y], selectedColumn[x]).toString());
                        newValues[y * selectedColumn.length + x] = value;
                    }
                }
                newMatrix = newValues;
            } else if (matrix instanceof float[]) {
                float[] newValues = new float[selectedRow.length * selectedColumn.length];
                for (int y = 0; y < selectedRow.length; y++) {
                    for (int x = 0; x < selectedColumn.length; x++) {
                        float value = Float.parseFloat(table.getValueAt(selectedRow[y], selectedColumn[x]).toString());
                        newValues[y * selectedColumn.length + x] = value;
                    }
                }
                newMatrix = newValues;
            } else if (matrix instanceof double[]) {
                double[] newValues = new double[selectedRow.length * selectedColumn.length];
                for (int y = 0; y < selectedRow.length; y++) {
                    for (int x = 0; x < selectedColumn.length; x++) {
                        double value = Double
                                .parseDouble(table.getValueAt(selectedRow[y], selectedColumn[x]).toString());
                        newValues[y * selectedColumn.length + x] = value;
                    }
                }
                newMatrix = newValues;
            }
            if (newMatrix != null) {
                matrix = newMatrix;
                width = selectedColumn.length;
                height = selectedRow.length;
                fireValueChanged(matrix);
            }
        }
    }

    @Override
    protected DefaultTableModelWithRowName generateDefaultModel() {
        return new DefaultTableModelWithRowName() {

            private static final long serialVersionUID = 3679109296557631430L;

            private int rows = 0;
            private int columns = 0;

            @Override
            public int getRowCount() {
                return rows;
            }

            @Override
            public int getColumnCount() {
                return columns;
            }

            @Override
            public String getColumnName(int column) {
                String[] customColumnNames = NumberTable.this.customColumnNames;
                if ((customColumnNames == null) || (column >= customColumnNames.length) || (column < 0)) {
                    return String.valueOf(column);
                } else {
                    return customColumnNames[column];
                }
            }

            @Override
            public String getRowName(int row) {
                String[] customRowNames = NumberTable.this.customRowNames;
                if ((customRowNames == null) || (row >= customRowNames.length) || (row < 0)) {
                    return String.valueOf(row);
                } else {
                    return customRowNames[row];
                }
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                if (loadDataVisible) {
                    return false;
                }
                return editable;
            }

            @Override
            public Object getValueAt(int row, int column) {
                String tmpValue = null;
                if (matrix != null) {
                    int index = (row * columns) + column;
                    if (Format.isNumberFormatOK(format)) {
                        if (matrix instanceof Number[]) {
                            Number nb = (Number) Array.get(matrix, index);
                            try {
                                tmpValue = Format.format(format, nb);
                            } catch (Exception e) {
                                tmpValue = String.valueOf(nb);
                            }
                        } else {
                            tmpValue = Format.formatValue(Array.getDouble(matrix, index), format);
                        }
                    } else {
                        tmpValue = Array.get(matrix, index).toString();
                    }
                }
                return tmpValue;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return String.class;
            }

            @Override
            public void setValueAt(Object aValue, int row, int column) {
                if (matrix != null) {
                    int index = (row * columns) + column;
                    if (index > -1 && index < Array.getLength(matrix) && (aValue instanceof String)) {
                        Object arrayCopy = getCopy(matrix);
                        String stringValue = (String) aValue;
                        if (arrayCopy instanceof Number[]) {
                            Number[] temp = (Number[]) arrayCopy;
                            Number value = temp[index];
                            if (value.getClass().isAssignableFrom(Byte.class)) {
                                try {
                                    temp[index] = Byte.valueOf(stringValue);
                                } catch (Exception e) {
                                    temp[index] = value;
                                }
                            } else if (value.getClass().isAssignableFrom(Short.class)) {
                                try {
                                    temp[index] = Short.valueOf(stringValue);
                                } catch (Exception e) {
                                    temp[index] = value;
                                }
                            } else if (value.getClass().isAssignableFrom(Integer.class)) {
                                try {
                                    temp[index] = Integer.valueOf(stringValue);
                                } catch (Exception e) {
                                    temp[index] = value;
                                }
                            } else if (value.getClass().isAssignableFrom(Long.class)) {
                                try {
                                    temp[index] = Long.valueOf(stringValue);
                                } catch (Exception e) {
                                    temp[index] = value;
                                }
                            } else if (value.getClass().isAssignableFrom(Float.class)) {
                                try {
                                    temp[index] = Float.valueOf(stringValue);
                                } catch (Exception e) {
                                    temp[index] = value;
                                }
                            } else if (value.getClass().isAssignableFrom(Double.class)) {
                                try {
                                    temp[index] = Double.valueOf(stringValue);
                                } catch (Exception e) {
                                    temp[index] = value;
                                }
                            }
                        } else if (arrayCopy instanceof byte[]) {
                            ((byte[]) arrayCopy)[index] = Byte.parseByte(stringValue);
                        } else if (arrayCopy instanceof short[]) {
                            ((short[]) arrayCopy)[index] = Short.parseShort(stringValue);
                        } else if (arrayCopy instanceof int[]) {
                            ((int[]) arrayCopy)[index] = Integer.parseInt(stringValue);
                        } else if (arrayCopy instanceof long[]) {
                            ((long[]) arrayCopy)[index] = Long.parseLong(stringValue);
                        } else if (arrayCopy instanceof float[]) {
                            ((float[]) arrayCopy)[index] = Float.parseFloat(stringValue);
                        } else if (arrayCopy instanceof double[]) {
                            ((double[]) arrayCopy)[index] = Double.parseDouble(stringValue);
                        }
                        if (arrayCopy != null) {
                            setData(arrayCopy);
                        }
                    }
                }
            }

            @Override
            public void fireTableStructureChanged() {
                rows = height;
                columns = width;
                super.fireTableStructureChanged();
            }
        };
    }

    @Override
    protected Object getCopy(Object toCopy) {
        Object result = null;
        if (toCopy instanceof Number[]) {
            result = Arrays.copyOf((Number[]) toCopy, Array.getLength(toCopy));
        } else if (toCopy instanceof byte[]) {
            result = Arrays.copyOf((byte[]) toCopy, Array.getLength(toCopy));
        } else if (toCopy instanceof short[]) {
            result = Arrays.copyOf((short[]) toCopy, Array.getLength(toCopy));
        } else if (toCopy instanceof int[]) {
            result = Arrays.copyOf((int[]) toCopy, Array.getLength(toCopy));
        } else if (toCopy instanceof long[]) {
            result = Arrays.copyOf((long[]) toCopy, Array.getLength(toCopy));
        } else if (toCopy instanceof float[]) {
            result = Arrays.copyOf((float[]) toCopy, Array.getLength(toCopy));
        } else if (toCopy instanceof double[]) {
            result = Arrays.copyOf((double[]) toCopy, Array.getLength(toCopy));
        }
        return result;
    }

    public void setBufferredWidth(int bufferredWidth) {
        if (bufferredWidth < 0) {
            bufferredWidth = 0;
        }
        this.bufferredWidth = bufferredWidth;
    }

    public void setBufferredHeight(int bufferredHeight) {
        if (bufferredHeight < 0) {
            bufferredHeight = 0;
        }
        this.bufferredHeight = bufferredHeight;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return height;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        return width;
    }

    @Override
    public void setFlatNumberMatrix(Object matrix, int width, int height) {
        setBufferredWidth(width);
        setBufferredHeight(height);
        setData(matrix);
    }

    @Override
    public void setNumberMatrix(Object[] matrix) {
        setData(matrix);
    }

    @Override
    public void setData(Object matrix) {
        if (matrix == null) {
            width = 0;
            height = 0;
            super.setData(matrix);
        } else {
            Class<?> contentType = ArrayUtils.recoverDataType(matrix);
            boolean canSetData = false;
            if (ObjectUtils.isNumberClass(contentType)) {
                int[] shape = ArrayUtils.recoverShape(matrix);
                if (shape.length == 1) {
                    int expectedLength = bufferredWidth * bufferredHeight;
                    if (expectedLength == shape[0]) {
                        width = bufferredWidth;
                        height = bufferredHeight;
                        canSetData = true;
                    }
                } else if (shape.length == 2) {
                    height = shape[0];
                    width = shape[1];
                    matrix = ArrayUtils.convertArrayDimensionFromNTo1(matrix);
                    canSetData = true;
                }
            }
            if (canSetData) {
                super.setData(matrix);
                if (Number.class.isAssignableFrom(contentType)) {
                    updateConverter(matchNumberClass(contentType));
                } else {
                    converter = null;
                }
            }
        }
    }

    private final Class<? extends Number> matchNumberClass(Class<?> toMatch) {
        Class<? extends Number> result = null;
        if (Number.class.equals(toMatch)) {
            result = Number.class;
        } else if (Byte.class.equals(toMatch)) {
            result = Byte.class;
        } else if (Short.class.equals(toMatch)) {
            result = Short.class;
        } else if (Integer.class.equals(toMatch)) {
            result = Integer.class;
        } else if (Long.class.equals(toMatch)) {
            result = Long.class;
        } else if (Float.class.equals(toMatch)) {
            result = Float.class;
        } else if (Double.class.equals(toMatch)) {
            result = Double.class;
        }
        return result;
    }

    @Override
    protected boolean isSameMatrixStructure(Object matrix1, Object matrix2) {
        boolean sameStructure = false;
        if (matrix1 == null) {
            sameStructure = (matrix2 == null);
        } else if (matrix2 != null) {
            sameStructure = (ArrayUtils.recoverDataType(matrix1).equals(ArrayUtils.recoverDataType(matrix2))
                    && Arrays.equals(ArrayUtils.recoverShape(matrix1), ArrayUtils.recoverShape(matrix2)));
        }
        return sameStructure;
    }

    @Override
    public void setLoadDataVisible(boolean loadDataVisible) {
        if (loadDataVisible && (loadMatrix != null)) {
            width = bufferredWidth;
            height = bufferredHeight;
        }
        super.setLoadDataVisible(loadDataVisible);
    }

    @Override
    public void loadDataFile(String loadDataFile) {
        FlatMatrix<?> flatMatrix = null;
        if (matrix instanceof Number[]) {
            flatMatrix = FileArrayUtils.loadFlatMatrixFromFile(loadDataFile, converter, getValueSeparator(),
                    isSaveColumnIndex());
        } else if (matrix instanceof byte[]) {
            flatMatrix = FileArrayUtils.loadTypeFlatMatrixFromFile(loadDataFile, Byte.TYPE, getValueSeparator(),
                    isSaveColumnIndex());
        } else if (matrix instanceof short[]) {
            flatMatrix = FileArrayUtils.loadTypeFlatMatrixFromFile(loadDataFile, Short.TYPE, getValueSeparator(),
                    isSaveColumnIndex());
        } else if (matrix instanceof int[]) {
            flatMatrix = FileArrayUtils.loadTypeFlatMatrixFromFile(loadDataFile, Integer.TYPE, getValueSeparator(),
                    isSaveColumnIndex());
        } else if (matrix instanceof long[]) {
            flatMatrix = FileArrayUtils.loadTypeFlatMatrixFromFile(loadDataFile, Long.TYPE, getValueSeparator(),
                    isSaveColumnIndex());
        } else if (matrix instanceof float[]) {
            flatMatrix = FileArrayUtils.loadTypeFlatMatrixFromFile(loadDataFile, Float.TYPE, getValueSeparator(),
                    isSaveColumnIndex());
        } else {
            flatMatrix = FileArrayUtils.loadTypeFlatMatrixFromFile(loadDataFile, Double.TYPE, getValueSeparator(),
                    isSaveColumnIndex());
        }
        setLoadedMatrix(flatMatrix);
    }

    public void setLoadedMatrix(FlatMatrix<?> flatMatrix) {
        if (flatMatrix != null) {
            bufferredWidth = flatMatrix.getColumns();
            loadMatrix = flatMatrix.getValue();
            if ((bufferredWidth == 0) || (loadMatrix == null)) {
                bufferredHeight = 0;
            } else {
                bufferredHeight = Array.getLength(loadMatrix) / bufferredWidth;
            }
        }
    }

    @Override
    public void saveDataFile(String fileName) {
        FileArrayUtils.saveFlatMatrixToFile(fileName, matrix, ArrayUtils.recoverDataType(matrix), width,
                getValueSeparator(), isSaveColumnIndex());
    }

    @Override
    protected void showTrimTable(final Object matrix) {

        final int[] commonTrimIndexes = recoverTrimTableIndexes(matrix, width);

        MaskedTable trimedTable = new MaskedTable() {

            private static final long serialVersionUID = 2222229128960837811L;

            private int[] trimIndexes = commonTrimIndexes;

            @Override
            public boolean isHiddenByMask(int row, int column) {
                boolean result;
                if ((trimIndexes[0] < 0) || (trimIndexes[1] < 0) || (trimIndexes[2] < 0) || (trimIndexes[3] < 0)) {
                    result = false;
                } else {
                    result = super.isHiddenByMask(row - trimIndexes[0], column - trimIndexes[2]);
                }
                return result;
            }

            @Override
            public void tableChanged(TableModelEvent e) {
                trimIndexes = recoverTrimTableIndexes(matrix, width);
                super.tableChanged(e);
            }
        };

        DefaultTableModelWithRowName trimedTableModel = new DefaultTableModelWithRowName() {

            private static final long serialVersionUID = -2716043722185345L;

            private final int[] trimIndexes = commonTrimIndexes;

            @Override
            public int getRowCount() {
                int count = 0;
                if ((trimIndexes[0] < 0) || (trimIndexes[1] < 0) || (trimIndexes[2] < 0) || (trimIndexes[3] < 0)) {
                    count = 0;
                } else {
                    count = trimIndexes[1] - trimIndexes[0] + 1;
                }
                if (count < 0) {
                    count = 0;
                }
                return count;
            }

            @Override
            public int getColumnCount() {
                int count = 0;
                if ((trimIndexes[0] < 0) || (trimIndexes[1] < 0) || (trimIndexes[2] < 0) || (trimIndexes[3] < 0)) {
                    count = 0;
                } else {
                    count = trimIndexes[3] - trimIndexes[2] + 1;
                }
                if (count < 0) {
                    count = 0;
                }
                return count;
            }

            @Override
            public String getColumnName(int column) {
                if (matrix != null) {
                    String[] customColumnNames = NumberTable.this.customColumnNames;
                    int index = trimIndexes[2] + column;
                    if ((customColumnNames == null) || (index >= customColumnNames.length) || (index < 0)) {
                        return String.valueOf(index);
                    } else {
                        return customColumnNames[index];
                    }
                }
                return ObjectUtils.EMPTY_STRING;
            }

            @Override
            public String getRowName(int row) {
                if (matrix != null) {
                    String[] customRowNames = NumberTable.this.customRowNames;
                    int index = trimIndexes[0] + row;
                    if ((customRowNames == null) || (index >= customRowNames.length) || (index < 0)) {
                        return String.valueOf(index);
                    } else {
                        return customRowNames[index];
                    }
                }
                return ObjectUtils.EMPTY_STRING;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;

            }

            @Override
            public Object getValueAt(int row, int column) {
                String tmpValue = null;
                if ((matrix != null) && (trimIndexes[0] > -1) && (trimIndexes[1] > -1) && (trimIndexes[2] > -1)
                        && (trimIndexes[3] > -1)) {
                    row += trimIndexes[0];
                    column += trimIndexes[2];
                    int index = (row * width) + column;
                    if (Format.isNumberFormatOK(format)) {
                        if (matrix instanceof Number[]) {
                            Number nb = (Number) Array.get(matrix, index);
                            try {
                                tmpValue = Format.format(format, nb);
                            } catch (Exception e) {
                                tmpValue = String.valueOf(nb);
                            }
                        } else {
                            tmpValue = Format.formatValue(Array.getDouble(matrix, index), format);
                        }
                    } else {
                        tmpValue = Array.get(matrix, index).toString();
                    }
                }
                return tmpValue;
            }

        };
        trimedTable.setModel(trimedTableModel);
        trimedTableModel.fireTableDataChanged();
        trimedTable.setColumnControlVisible(false);
        trimedTable.setAutoResizeMode(JXTable.AUTO_RESIZE_OFF);
        trimedTable.packAll();

        JScrollPane trimedScrollPane = new JScrollPane();
        trimedScrollPane.setViewportView(trimedTable);

        JDialog trimedDialog = new JDialog(CometeUtils.getFrameForComponent(this),
                "Matrix Trimed Data at " + DateUtil.milliToString(System.currentTimeMillis(), DATE_FORMAT), false);
        trimedDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        trimedDialog.setContentPane(trimedScrollPane);

        if (trimedDialog != null && !trimedDialog.isVisible()) {
            trimedDialog.setLocationRelativeTo(null);
            trimedDialog.setLocation(trimedDialog.getLocation().x, 0);
            trimedDialog.setSize(600, 600);
            trimedDialog.setPreferredSize(trimedDialog.getSize());
            trimedDialog.pack();
            trimedDialog.setVisible(true);
        }
    }

    private int[] recoverTrimTableIndexes(Object flatMatrix, int columns) {
        int trimLineStart, trimLineEnd, trimColumnStart, trimColumnEnd;
        if ((flatMatrix == null) || (columns < 1)) {
            trimLineStart = -1;
            trimLineEnd = -1;
            trimColumnStart = -1;
            trimColumnEnd = -1;
        } else {
            int rows = Array.getLength(flatMatrix) / columns;
            trimLineStart = rows - 1;
            trimLineEnd = 0;
            trimColumnStart = columns - 1;
            trimColumnEnd = 0;
            if (flatMatrix instanceof Integer[]) {
                for (int y = 0; y < rows; y++) {
                    for (int x = 0; x < columns; x++) {
                        if (((Number) Array.get(flatMatrix, y * columns + x)).doubleValue() != 0) {
                            if (y < trimLineStart) {
                                trimLineStart = y;
                            }
                            if (y > trimLineEnd) {
                                trimLineEnd = y;
                            }
                            if (x < trimColumnStart) {
                                trimColumnStart = x;
                            }
                            if (x > trimColumnEnd) {
                                trimColumnEnd = x;
                            }
                        }
                    }
                }
            } else {
                for (int y = 0; y < rows; y++) {
                    for (int x = 0; x < columns; x++) {
                        if (Array.getDouble(flatMatrix, y * columns + x) != 0) {
                            if (y < trimLineStart) {
                                trimLineStart = y;
                            }
                            if (y > trimLineEnd) {
                                trimLineEnd = y;
                            }
                            if (x < trimColumnStart) {
                                trimColumnStart = x;
                            }
                            if (x > trimColumnEnd) {
                                trimColumnEnd = x;
                            }
                        }
                    }
                }
            }
        }
        if ((trimLineStart < 0) || (trimColumnStart < 0) || (trimLineEnd < 0) || (trimColumnEnd < 0)) {
            trimLineStart = -1;
            trimLineEnd = -1;
            trimColumnStart = -1;
            trimColumnEnd = -1;
        }
        return new int[] { trimLineStart, trimLineEnd, trimColumnStart, trimColumnEnd };
    }

    @Override
    public Object[] getNumberMatrix() {
        Object[] numberMatrix = null;
        if (matrix != null) {
            try {
                numberMatrix = (Object[]) ArrayUtils.convertArrayDimensionFrom1ToN(matrix, height, width);
            } catch (Exception e) {
                numberMatrix = null;
            }
        }
        return numberMatrix;
    }

    @Override
    public Object getFlatNumberMatrix() {
        return matrix;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    public static void main(String args[]) {
        JFrame myFrame = new JFrame("Table");
        NumberTable table = new NumberTable();
        Double[][] data = new Double[][] { { new Double(1), new Double(2) }, { new Double(3), new Double(4) } };
        table.setData(data);
        myFrame.add(table);
        myFrame.setSize(800, 600);
        myFrame.setVisible(true);
        myFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

}
