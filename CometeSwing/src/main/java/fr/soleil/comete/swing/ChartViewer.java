/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.print.PrintException;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.WindowConstants;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.util.SnapshotEventDelegate;
import fr.soleil.comete.definition.util.TransferEventDelegate;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IComboBox;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.DragProperties;
import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.comete.definition.widget.properties.OffsetProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.chart.IJLChartActionListener;
import fr.soleil.comete.swing.chart.JLAxis;
import fr.soleil.comete.swing.chart.JLChart;
import fr.soleil.comete.swing.chart.JLChartActionEvent;
import fr.soleil.comete.swing.chart.JLDataView;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.chart.util.IDataViewManager;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.SnapshotUtil;
import fr.soleil.lib.project.swing.print.EasyPrinter;
import fr.soleil.lib.project.swing.ui.ColoredSplitPaneUI;

/**
 * @deprecated Use {@link Chart} instead
 */
@Deprecated
public class ChartViewer extends AbstractPanel
        implements IDataViewManager, ActionListener, IJLChartActionListener, IChartViewer {

    private static final long serialVersionUID = -8695565306235093589L;

    // DragProperties are not managed
    protected static final DragProperties NO_DRAG_PROPERTIES = new DragProperties(DRAG_NONE, DRAG_NONE, 0);

    protected final JLChart chart;
    protected final JComponent chartLegend;
    protected final JScrollPane chartLegendScrollPane;
    protected final JPanel chartPanel;
    protected JSplitPane chartSplitPane;

    protected boolean freezePanelVisible;
    protected boolean managementPanelVisible;

    protected JButton snapshotButton;
    protected JButton clearButton;

    protected final JLabel visibleLabel;
    protected final JLabel hiddenLabel;
    protected final IComboBox visibleDataViewCombo;
    protected final IComboBox hiddenDataViewCombo;
    protected final ComboBox addDataViewCombo;
    protected final JButton removeDataViewButton;
    protected final JButton addDataViewButton;
    protected final JPanel freezePanel;
    protected final JPanel managementPanel;
    protected boolean init;
    protected boolean axisSelectionVisible;

    protected int[] lineStyle;
    protected int[] axisList;
    protected int[] markerStyle;
    protected int[] markerSize;
    protected int[] lineWidth;
    protected int[] barWidth;
    protected int[] dataViewStyle;
    protected int[] fillStyle;
    protected int[] fillMethod;
    protected String[] formatList;

    protected final SnapshotEventDelegate snapshotEventDelegate;
    protected final TransferEventDelegate transferEventDelegate;

    protected CometeColor[] cometeColorList;
    protected CometeColor[] cometeMarkerColorList;
    protected CometeColor[] cometeFillColorList;
    protected final TargetDelegate delegate;
    protected int horizontalAlignment;
    protected boolean autoHideViews;

    public ChartViewer(boolean useNiceScale) {
        super();
        lineStyle = null;
        axisList = null;
        markerStyle = null;
        markerSize = null;
        lineWidth = null;
        barWidth = null;
        dataViewStyle = null;
        fillStyle = null;
        fillMethod = null;
        formatList = null;
        snapshotEventDelegate = new SnapshotEventDelegate(this, ChartUtils.DOT);
        transferEventDelegate = new TransferEventDelegate(this, ChartUtils.DOT);
        init = false;
        freezePanelVisible = false;
        managementPanelVisible = false;
        axisSelectionVisible = false;
        visibleLabel = new JLabel(" - Visible data views : ");
        hiddenLabel = new JLabel(" - Hidden data views : ");
        chart = generateChart(useNiceScale);
        chart.setTransferEventDelegate(transferEventDelegate);
        chartLegend = chart.generateLegend();
        chartLegend.setBackground(chart.getBackground());
        chart.addJLChartActionListener(this);
        chartLegendScrollPane = new JScrollPane(chartLegend);
        chartLegendScrollPane.setPreferredSize(new Dimension(0, 0));
        chartPanel = new JPanel(new BorderLayout());
        snapshotButton = new JButton("Freeze Values");
        snapshotButton.setMargin(new Insets(0, 0, 0, 0));
        snapshotButton.addActionListener(this);
        clearButton = new JButton("Clear Freezes");
        clearButton.setMargin(new Insets(0, 0, 0, 0));
        clearButton.addActionListener(this);
        freezePanel = new JPanel();

        addDataViewCombo = new ComboBox();
        addDataViewCombo.setValueList("Y1", "Y2");
        setAxisSelectionVisible(axisSelectionVisible);

        visibleDataViewCombo = chart.getVisibleDataViewCombo();
        hiddenDataViewCombo = chart.getHiddenDataViewCombo();

        removeDataViewButton = new JButton("Hide");
        removeDataViewButton.setIcon(new ImageIcon(getClass().getResource("/fr/soleil/comete/icons/hide-curve.png")));
        removeDataViewButton.setMargin(CometeUtils.getzInset());
        removeDataViewButton.setHorizontalAlignment(LEFT);
        removeDataViewButton.addActionListener(this);
        addDataViewButton = new JButton("Display");
        addDataViewButton.setIcon(new ImageIcon(getClass().getResource("/fr/soleil/comete/icons/show-curve.png")));
        addDataViewButton.setMargin(CometeUtils.getzInset());
        addDataViewButton.setHorizontalAlignment(LEFT);
        addDataViewButton.addActionListener(this);

        managementPanel = new JPanel(new GridBagLayout());
        cometeColorList = null;
        cometeMarkerColorList = null;
        cometeFillColorList = null;
        delegate = new TargetDelegate();
        horizontalAlignment = IComponent.CENTER;
        addComponents();
        updateChartLegend();
        setCometeBackground(ColorTool.getCometeColor(chart.getBackground()));
        autoHideViews = false;
    }

    public ChartViewer() {
        this(true);
    }

    @Override
    public Comparator<AbstractDataView> getDataViewComparator() {
        return chart.getDataViewComparator();
    }

    @Override
    public void setDataViewComparator(Comparator<AbstractDataView> comparator) {
        chart.setDataViewComparator(comparator);
    }

    /**
     * Generates an new {@link JLChart}. This method can be used to have a
     * custom {@link JLChart} implementation for {@link #chart}
     * 
     * @param useNiceScale
     *            Whether to use nice scale in {@link JLChart}
     * @return A {@link JLChart}
     */
    protected JLChart generateChart(boolean useNiceScale) {
        return new JLChart(this, useNiceScale);
    }

    @Override
    public boolean isAutoHideViews() {
        return autoHideViews;
    }

    @Override
    public void setAutoHideViews(final boolean hide) {
        this.autoHideViews = hide;
        chart.setAutoHideViews(isAutoHideViews() && isManagementPanelVisible());
    }

    @Override
    public String[][] getAllViewsIdsAndNames() {
        List<JLDataView> views = chart.getViews();
        Map<String, JLDataView> hiddenViews = chart.getHiddenDataViews();
        if (hiddenViews != null) {
            for (JLDataView view : hiddenViews.values()) {
                if (!views.contains(view)) {
                    views.add(view);
                }
            }
        }
        String[][] idAndName = new String[2][views.size()];
        int viewIndex = 0;
        for (JLDataView view : views) {
            idAndName[0][viewIndex] = view.getId();
            idAndName[1][viewIndex++] = view.getDisplayName();
        }
        return idAndName;
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        super.setOpaque(isOpaque);
        if (chart != null) {
            chart.setOpaque(isOpaque);
        }
        if (chartPanel != null) {
            chartPanel.setOpaque(isOpaque);
        }
        if (managementPanel != null) {
            managementPanel.setOpaque(isOpaque);
        }
        if (freezePanel != null) {
            freezePanel.setOpaque(isOpaque);
        }
        if (chartLegend != null) {
            chartLegend.setOpaque(isOpaque);
        }
        if (chartLegendScrollPane != null) {
            chartLegendScrollPane.setOpaque(isOpaque);
            chartLegendScrollPane.getViewport().setOpaque(isOpaque);
        }
        if (chartSplitPane != null) {
            chartSplitPane.setOpaque(isOpaque);
        }
    }

    protected void addComponents() {
        final GridBagConstraints snapBConstraint = new GridBagConstraints();
        snapBConstraint.fill = GridBagConstraints.NONE;
        snapBConstraint.gridx = 0;
        snapBConstraint.gridy = 0;
        snapBConstraint.weightx = 0;
        snapBConstraint.weighty = 0;

        final GridBagConstraints emptyConstraint = new GridBagConstraints();
        emptyConstraint.fill = GridBagConstraints.HORIZONTAL;
        emptyConstraint.gridx = 1;
        emptyConstraint.gridy = 0;
        emptyConstraint.weightx = 1;
        emptyConstraint.weighty = 0;

        final GridBagConstraints clearBConstraint = new GridBagConstraints();
        clearBConstraint.fill = GridBagConstraints.NONE;
        clearBConstraint.gridx = 2;
        clearBConstraint.gridy = 0;
        clearBConstraint.weightx = 0;
        clearBConstraint.weighty = 0;

        freezePanel.setLayout(new GridBagLayout());
        freezePanel.add(snapshotButton, snapBConstraint);
        freezePanel.add(Box.createGlue(), emptyConstraint);
        freezePanel.add(clearButton, clearBConstraint);

        final GridBagConstraints viewerConstraint = new GridBagConstraints();
        viewerConstraint.fill = GridBagConstraints.BOTH;
        viewerConstraint.gridx = 0;
        viewerConstraint.gridy = 0;
        viewerConstraint.weightx = 1;
        viewerConstraint.weighty = 1;

        final GridBagConstraints panelConstraint = new GridBagConstraints();
        panelConstraint.fill = GridBagConstraints.HORIZONTAL;
        panelConstraint.gridx = 0;
        panelConstraint.gridy = 1;
        panelConstraint.weightx = 1;
        panelConstraint.weighty = 0;

        final GridBagConstraints managementConstraint = new GridBagConstraints();
        managementConstraint.fill = GridBagConstraints.HORIZONTAL;
        managementConstraint.gridx = 0;
        managementConstraint.gridy = 2;
        managementConstraint.weightx = 1;
        managementConstraint.weighty = 0;

        // Hide visible views
        final GridBagConstraints visibleLabelConstraints = new GridBagConstraints();
        visibleLabelConstraints.fill = GridBagConstraints.BOTH;
        visibleLabelConstraints.gridx = 0;
        visibleLabelConstraints.gridy = 0;
        visibleLabelConstraints.weightx = 0;
        visibleLabelConstraints.weighty = 1;
        visibleLabelConstraints.insets = new Insets(5, 10, 0, 10);
        managementPanel.add(visibleLabel, visibleLabelConstraints);
        final GridBagConstraints visibleDataViewConstraints = new GridBagConstraints();
        visibleDataViewConstraints.fill = GridBagConstraints.BOTH;
        visibleDataViewConstraints.gridx = 1;
        visibleDataViewConstraints.gridy = 0;
        visibleDataViewConstraints.weightx = 1;
        visibleDataViewConstraints.weighty = 1;
        visibleDataViewConstraints.insets = new Insets(5, 10, 0, 10);
        managementPanel.add((JComboBox<?>) visibleDataViewCombo, visibleDataViewConstraints);
        final GridBagConstraints removeDataViewConstraints = new GridBagConstraints();
        removeDataViewConstraints.fill = GridBagConstraints.BOTH;
        removeDataViewConstraints.gridx = 2;
        removeDataViewConstraints.gridy = 0;
        removeDataViewConstraints.weightx = 0;
        removeDataViewConstraints.weighty = 1;
        removeDataViewConstraints.insets = new Insets(5, 10, 0, 10);
        managementPanel.add(removeDataViewButton, removeDataViewConstraints);

        // Show hidden views
        final GridBagConstraints hiddenLabelConstraints = new GridBagConstraints();
        hiddenLabelConstraints.fill = GridBagConstraints.BOTH;
        hiddenLabelConstraints.gridx = 0;
        hiddenLabelConstraints.gridy = 1;
        hiddenLabelConstraints.weightx = 0;
        hiddenLabelConstraints.weighty = 1;
        hiddenLabelConstraints.insets = new Insets(5, 10, 5, 10);
        managementPanel.add(hiddenLabel, hiddenLabelConstraints);
        final GridBagConstraints hiddenDataViewConstraints = new GridBagConstraints();
        hiddenDataViewConstraints.fill = GridBagConstraints.BOTH;
        hiddenDataViewConstraints.gridx = 1;
        hiddenDataViewConstraints.gridy = 1;
        hiddenDataViewConstraints.weightx = 0;
        hiddenDataViewConstraints.weighty = 1;
        hiddenDataViewConstraints.insets = new Insets(5, 10, 5, 10);
        managementPanel.add((JComboBox<?>) hiddenDataViewCombo, hiddenDataViewConstraints);
        final GridBagConstraints addButtonConstraints = new GridBagConstraints();
        addButtonConstraints.fill = GridBagConstraints.BOTH;
        addButtonConstraints.gridx = 2;
        addButtonConstraints.gridy = 1;
        addButtonConstraints.weightx = 0;
        addButtonConstraints.weighty = 1;
        addButtonConstraints.insets = new Insets(5, 10, 5, 10);
        managementPanel.add(addDataViewButton, addButtonConstraints);
        final GridBagConstraints addDataViewConstraints = new GridBagConstraints();
        addDataViewConstraints.fill = GridBagConstraints.NONE;
        addDataViewConstraints.gridx = 3;
        addDataViewConstraints.gridy = 1;
        addDataViewConstraints.weightx = 0;
        addDataViewConstraints.weighty = 1;
        addDataViewConstraints.insets = new Insets(5, 10, 5, 10);
        managementPanel.add(addDataViewCombo, addDataViewConstraints);

        this.setLayout(new GridBagLayout());
        this.add(chartPanel, viewerConstraint);
        this.add(freezePanel, panelConstraint);
        this.add(managementPanel, managementConstraint);

        this.repaint();
    }

    @Override
    public boolean isAxisSelectionVisible() {
        return axisSelectionVisible;
    }

    @Override
    public void setAxisSelectionVisible(final boolean axisSelectionVisible) {
        this.axisSelectionVisible = axisSelectionVisible;
        if (addDataViewCombo != null) {
            addDataViewCombo.setVisible(axisSelectionVisible);
        }
    }

    /**
     * Cleans the panel that contains the chart and its legend
     */
    protected void cleanChartPanel() {
        chartPanel.removeAll();
        chartPanel.repaint();
    }

    /**
     * Reinitializes the JSplitPane used to separate chart from its legend
     */
    protected void initChartSplitPane() {
        chartSplitPane = new JSplitPane();
        chartSplitPane.setUI(new ColoredSplitPaneUI());
        chartSplitPane.setOneTouchExpandable(true);
        chartSplitPane.setDividerSize(8);
        chartSplitPane.setBackground(chartPanel.getBackground());
        ((ColoredSplitPaneUI) chartSplitPane.getUI()).setDividerColor(chartPanel.getBackground());
        chartSplitPane.setOpaque(isOpaque());
    }

    /**
     * Does anything necessary to update chart's legend visibility and placement
     */
    public void updateChartLegend() {
        cleanChartPanel();
        if (chartSplitPane != null) {
            // clean former JSplitPane
            chartSplitPane.removeAll();
            chartSplitPane = null;
        }
        initChartSplitPane();
        if (chart.isLabelVisible()) {
            final int placement = chart.getLabelPlacement();
            final double placeForChartH = 0.5;
            final double placeForChartV = 0.8;
            switch (placement) {
                case IChartViewer.LABEL_UP:
                    chartSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
                    chartSplitPane.setTopComponent(chartLegendScrollPane);
                    chartSplitPane.setBottomComponent(chart);
                    chartSplitPane.setDividerLocation(1 - placeForChartV);
                    chartSplitPane.setResizeWeight(1 - placeForChartV);
                    break;
                case IChartViewer.LABEL_LEFT:
                    chartSplitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
                    chartSplitPane.setLeftComponent(chartLegendScrollPane);
                    chartSplitPane.setRightComponent(chart);
                    chartSplitPane.setDividerLocation(1 - placeForChartH);
                    chartSplitPane.setResizeWeight(1 - placeForChartH);
                    break;
                case IChartViewer.LABEL_RIGHT:
                    chartSplitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
                    chartSplitPane.setLeftComponent(chart);
                    chartSplitPane.setRightComponent(chartLegendScrollPane);
                    chartSplitPane.setDividerLocation(placeForChartH);
                    chartSplitPane.setResizeWeight(placeForChartH);
                    break;
                case IChartViewer.LABEL_ROW:
                case IChartViewer.LABEL_DOWN:
                    chartSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
                    chartSplitPane.setTopComponent(chart);
                    chartSplitPane.setBottomComponent(chartLegendScrollPane);
                    chartSplitPane.setDividerLocation(placeForChartV);
                    chartSplitPane.setResizeWeight(placeForChartV);
                    break;
            }
        }
        updateChartLegendVisibility();
        refresh();
    }

    /**
     * Sets chart legend visible or not, depending on chart's parameters
     */
    protected void updateChartLegendVisibility() {
        if (!isChartVisible()) {
            if (chart.isLabelVisible()) {
                chartPanel.add(chartSplitPane);
            } else {
                chartPanel.add(chart);
            }
        }
    }

    /**
     * Refreshes components and revalidates chart's legend when necessary
     */
    public void refresh() {
        if (isChartVisible()) {
            if (chart.isLabelVisible()) {
                chartLegend.revalidate();
                chartSplitPane.revalidate();
            }
        }
        chartPanel.revalidate();
        chartPanel.repaint();
    }

    protected boolean isChartVisible() {
        return (chartPanel.getComponentCount() > 0);
    }

    public JLChart getChart() {
        return chart;
    }

    @Override
    public void addChartViewerListener(final IChartViewerListener listener) {
        chart.addChartViewerListener(listener);
    }

    @Override
    public void removeChartViewerListener(final IChartViewerListener listener) {
        chart.removeChartViewerListener(listener);
    }

    @Override
    public void fireDataChanged(final Map<String, Object> data) {
        chart.fireDataChanged(data);
    }

    @Override
    public void fireConfigurationChanged(final String id) {
        chart.fireConfigurationChanged(id);
    }

    // /**
    // * remove all data view
    // */
    // public void removeAllDataViews() {
    // chart.removeAllDataViews();
    // // repaint forced to update legend
    // refresh();
    // }

    @Override
    public String getAxisName(int axis) {
        return chart.getAxisName(axis);
    }

    @Override
    public void setAxisName(final String name, final int axis) {
        chart.setAxisName(name, axis);
        chart.measureAndPaint();
    }

    @Override
    public int getAxisTitleAlignment(final int axis) {
        return chart.getAxisTitleAlignment(axis);
    }

    @Override
    public void setAxisTitleAlignment(final int align, final int axis) {
        chart.setAxisTitleAlignment(align, axis);
    }

    /**
     * set a color on the axis
     */
    public void setAxisColor(final Color color, final int axis) {
        chart.setAxisColor(color, axis);
    }

    @Override
    public void setGridStyle(final int style, final int axis) {
        chart.setAxisGridStyle(style, axis);
    }

    @Override
    public void setAxisDateFormat(final String format, final int axis) {
        chart.setAxisDateFormat(format, axis);
    }

    public void setAnnotation(final int annotation, final int axis) {
        chart.setAxisAnnotation(annotation, axis);
    }

    @Override
    public void setSubGridVisible(final boolean visible, final int axis) {
        chart.setAxisSubGridVisible(visible, axis);
    }

    @Override
    public void setGridVisible(final boolean visible, final int axis) {
        chart.setAxisGridVisible(visible, axis);
    }

    @Override
    public void setXAxisOnBottom(final boolean b) {
        chart.setXAxisOnBottom(b);
    }

    @Override
    public boolean isManagementPanelVisible() {
        return managementPanelVisible;
    }

    @Override
    public void setManagementPanelVisible(final boolean managementPanelVisible) {
        this.managementPanelVisible = managementPanelVisible;
        managementPanel.setVisible(managementPanelVisible);
        chart.setAutoHideViews(isAutoHideViews() && isManagementPanelVisible());
        chart.setInitDataViewComboBoxes(managementPanelVisible);
    }

    @Override
    public void setEditable(final boolean editable) {
        chart.setEditable(editable);
    }

    @Override
    public boolean isEditable() {
        return chart.isEditable();
    }

    public Color getChartBackground() {
        return chart.getChartBackground();
    }

    @Override
    public double getDisplayDuration() {
        return chart.getDisplayDuration();
    }

    @Override
    public String getHeader() {
        return chart.getHeader();
    }

    public Font getHeaderFont() {
        return chart.getHeaderFont();
    }

    public Color getHeaderColor() {
        return chart.getHeaderColor();
    }

    @Override
    public int getTimePrecision() {
        return chart.getTimePrecision();
    }

    @Override
    public boolean isFreezePanelVisible() {
        return freezePanelVisible;
    }

    @Override
    public boolean isLegendVisible() {
        return chart.isLabelVisible();
    }

    public Font getLabelFont() {
        return chart.getLabelFont();
    }

    @Override
    public boolean isXAxisOnBottom() {
        return chart.isXAxisOnBottom();
    }

    @Override
    public void loadDataFile(final String fileName) {
        chart.loadDataFile(fileName);
    }

    @Override
    public void saveDataFile(String path) {
        chart.saveDataFile(path);
    }

    @Override
    public String getDataDirectory() {
        return transferEventDelegate.getDataDirectory();
    }

    @Override
    public void setDataDirectory(String path) {
        transferEventDelegate.setDataDirectory(path);
    }

    @Override
    public String getDataFile() {
        return transferEventDelegate.getDataFile();
    }

    @Override
    public void addDataTransferListener(IDataTransferListener listener) {
        transferEventDelegate.addDataTransferListener(listener);
    }

    @Override
    public void removeDataTransferListener(IDataTransferListener listener) {
        transferEventDelegate.removeDataTransferListener(listener);
    }

    public void removeDataViewFromAxis(final String name, final int axis) {
        chart.removeDataViewFromAxis(name, axis);
        // repaint forced to update legend
        refresh();
    }

    @Override
    public void setAutoScale(final boolean autoscale, final int axis) {
        chart.setAxisAutoScale(autoscale, axis);
    }

    @Override
    public boolean isAutoScale(final int axis) {
        return chart.isAxisAutoScale(axis);
    }

    public void setChartBackground(final Color c) {
        chart.setChartBackground(c);
    }

    public void setChartMainBackground(final Color c) {
        chartLegend.setBackground(c);
        chartLegendScrollPane.setBackground(c);
        chartLegendScrollPane.getViewport().setBackground(c);
        chartLegendScrollPane.getHorizontalScrollBar().setBackground(c);
        chartLegendScrollPane.getVerticalScrollBar().setBackground(c);
        chartPanel.setBackground(c);
        if (chartSplitPane != null) {
            chartSplitPane.setBackground(c);
            ((ColoredSplitPaneUI) chartSplitPane.getUI()).setDividerColor(c);
        }
    }

    public void setManagementPanelBackground(final Color c) {
        managementPanel.setBackground(c);
    }

    public void setFreezeBackground(final Color c) {
        freezePanel.setBackground(c);
    }

    @Override
    public void setDataViewLineStyle(final String name, final int style) {
        chart.setDataViewLineStyle(name, style);
    }

    public void setDataViewColor(final String name, final Color color) {
        chart.setDataViewColor(name, color);
    }

    public void setDataViewFillColor(final String name, final Color color) {
        chart.setDataViewFillColor(name, color);
    }

    @Override
    public void setDataViewMarkerStyle(final String name, final int marker) {
        chart.setDataViewMarkerStyle(name, marker);
    }

    public void setDataViewMarkerColor(final String name, final Color color) {
        chart.setDataViewMarkerColor(name, color);
    }

    @Override
    public void setDataViewMarkerSize(final String name, final int size) {
        chart.setDataViewMarkerSize(name, size);
    }

    @Override
    public void setTimePrecision(final int precision) {
        chart.setTimePrecision(precision);
    }

    @Override
    public boolean isHeaderVisible() {
        return chart.isHeaderVisible();
    }

    @Override
    public void addData(final Map<String, Object> dataToAdd) {
        addData(dataToAdd, false);
    }

    @Override
    public void removeData(final Collection<String> idsToRemove) {
        if (idsToRemove != null) {
            chart.removeData(idsToRemove);
            refresh();
        }
    }

    @Override
    public Map<String, Object> getData() {
        return chart.copyLastDataToMap();
    }

    @Override
    public void setData(final Map<String, Object> data) {
        addData(data, true);
    }

    protected void addData(final Map<String, Object> data, boolean replace) {
        boolean dataValid;
        if (replace) {
            dataValid = (!ObjectUtils.sameObject(data, getData()));
        } else {
            dataValid = (data != null);
        }
        if (dataValid) {
            chart.addData(data, replace, true);
            if (!init) {
                initDataViews();
                init = true;
            }
            // repaint forced to update legend
            refresh();
        }
    }

    private void initDataViews() {
        chart.initDataViews(Y1, false);
        chart.initDataViews(Y2, true);
    }

    @Override
    public void setDataViewLineWidth(final String name, final int lineWidth) {
        chart.setDataViewLineWidth(name, lineWidth);
        refresh();
    }

    @Override
    public void setDataViewBarWidth(final String name, final int lineWidth) {
        chart.setDataViewBarWidth(name, lineWidth);
    }

    @Override
    public void setDataViewStyle(final String name, final int style) {
        chart.setDataViewStyle(name, style);
    }

    @Override
    public void setDataViewFillStyle(final String name, final int style) {
        chart.setDataViewFillStyle(name, style);
    }

    @Override
    public void setDataViewFillMethod(final String name, final int style) {
        chart.setDataViewFillMethod(name, style);
    }

    @Override
    public void setDataViewAxis(final String name, final int axis) {
        chart.setDataViewAxis(name, axis);
        refresh();
    }

    @Override
    public void setFormat(String id, String format) {
        chart.setDataViewFormat(id, format);
    }

    @Override
    public void setDataViewUnit(String id, String unit) {
        chart.setDataViewUnit(id, unit);
    }

    @Override
    public void setDateColumnName(final String dateColumnName) {
        chart.setDateColumnName(dateColumnName);
    }

    @Override
    public String getDateColumnName() {
        return chart.getDateColumnName();
    }

    @Override
    public void setFreezePanelVisible(final boolean freezePanelVisible) {
        this.freezePanelVisible = freezePanelVisible;
        freezePanel.setVisible(freezePanelVisible);
        chart.setFreezeItemsVisible(freezePanelVisible);
    }

    @Override
    public void setHeader(final String s) {
        chart.setHeader(s);
    }

    public void setHeaderColor(final Color c) {
        chart.setHeaderColor(c);
    }

    public void setHeaderFont(final Font f) {
        chart.setHeaderFont(f);
    }

    @Override
    public void setHeaderVisible(final boolean b) {
        chart.setHeaderVisible(b);
    }

    @Override
    public void setIndexColumnName(final String name) {
        chart.setIndexColumnName(name);
    }

    public void setLabelFont(final Font f) {
        chart.setLabelFont(f);
    }

    @Override
    public void setLegendVisible(final boolean b) {
        chart.setLabelVisible(b);
    }

    @Override
    public void setScale(final int scale, final int axis) {
        chart.setAxisScale(scale, axis);
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        if (e.getSource() == snapshotButton) {
            makeSnaphot();
        } else if (e.getSource() == clearButton) {
            clearSnapshots();
        } else if (e.getSource() == removeDataViewButton) {
            removeDataView();
        } else if (e.getSource() == addDataViewButton) {
            addDataView();
        }
    }

    private String getSelectedDataViewName(IComboBox comboBox) {
        String dataViewName;
        Object selected = null;
        if (comboBox != null) {
            selected = comboBox.getSelectedValue();
        }
        if (selected == null) {
            dataViewName = null;
        } else {
            dataViewName = selected.toString();
        }
        return dataViewName;
    }

    private void addDataView() {
        final String dataViewName = getSelectedDataViewName(hiddenDataViewCombo);
        if (dataViewName != null) {
            int axis = Y1;
            int selectedAxis;
            if (isAxisSelectionVisible()) {
                selectedAxis = addDataViewCombo.getSelectedIndex();
            } else {
                selectedAxis = getDataViewAxis(dataViewName);
            }
            if (selectedAxis == 1) {
                axis = Y2;
            }
            chart.addDataView(dataViewName, axis, true);
            // repaint forced to update legend
            refresh();
        }
    }

    @Override
    public void setPreferDialogForTable(final boolean preferDialog, final boolean modal) {
        chart.setPreferDialogForTable(preferDialog, modal);
    }

    private void removeDataView() {
        final String dataViewName = getSelectedDataViewName(visibleDataViewCombo);
        if (dataViewName != null) {
            chart.removeDataView(dataViewName, false);
            // repaint forced to update legend
            refresh();
        }
    }

    public void clearSnapshots() {
        chart.clearSnapshots();
        initDataViews();
        // repaint forced to update legend
        refresh();
    }

    public void makeSnaphot() {
        chart.makeSnaphot();
        initDataViews();
        // repaint forced to update legend
        refresh();
    }

    public int getAnnotation(final int axis) {
        return chart.getAxisAnnotation(axis);
    }

    public Color getAxisColor(final int axis) {
        return chart.getAxisColor(axis);
    }

    @Override
    public String getAxisDateFormat(final int axis) {
        return chart.getAxisDateFormat(axis);
    }

    @Override
    public boolean containsDataView(final String name) {
        boolean contains = false;
        if (name != null) {
            if (chart.getDataView(name) == null) {
                if ((hiddenDataViewCombo != null) && (hiddenDataViewCombo.getValueList() != null)) {
                    for (Object value : hiddenDataViewCombo.getValueList()) {
                        if (name.equals(value)) {
                            contains = true;
                            break;
                        }
                    }
                }
            } else {
                contains = true;
            }
        }
        return contains;
    }

    @Override
    public int getDataViewAxis(final String name) {
        return chart.getDataViewAxis(name);
    }

    @Override
    public String getFormat(String id) {
        return chart.getDataViewFormat(id);
    }

    @Override
    public String getDataViewUnit(String id) {
        return chart.getDataViewUnit(id);
    }

    public Color getDataViewColor(final String name) {
        return chart.getDataViewColor(name);
    }

    public Color getDataViewFillColor(final String name) {
        return chart.getDataViewFillColor(name);
    }

    @Override
    public String getDataViewDisplayName(final String name) {
        return chart.getDataViewDisplayName(name);
    }

    @Override
    public int getDataViewFillStyle(final String name) {
        return chart.getDataViewFillStyle(name);
    }

    @Override
    public int getDataViewFillMethod(final String name) {
        return chart.getDataViewFillMethod(name);
    }

    @Override
    public int getDataViewLineStyle(final String name) {
        return chart.getDataViewLineStyle(name);
    }

    @Override
    public int getDataViewLineWidth(final String name) {
        return chart.getDataViewLineWidth(name);
    }

    @Override
    public int getDataViewBarWidth(final String name) {
        return chart.getDataViewBarWidth(name);
    }

    public Color getDataViewMarkerColor(final String name) {
        return chart.getDataViewMarkerColor(name);
    }

    @Override
    public int getDataViewMarkerSize(final String name) {
        return chart.getDataViewMarkerSize(name);
    }

    @Override
    public int getDataViewMarkerStyle(final String name) {
        return chart.getDataViewMarkerStyle(name);
    }

    @Override
    public int getDataViewStyle(final String name) {
        return chart.getDataViewStyle(name);
    }

    @Override
    public int getGridStyle(final int axis) {
        return chart.getAxisGridStyle(axis);
    }

    @Override
    public String getIndexColumnName() {
        return chart.getIndexColumnName();
    }

    @Override
    public int getScale(final int axis) {
        return chart.getScale(axis);
    }

    @Override
    public boolean isGridVisible(final int axis) {
        return chart.isAxisGridVisible(axis);
    }

    @Override
    public boolean isSubGridVisible(final int axis) {
        return chart.isAxisSubGridVisible(axis);
    }

    @Override
    public void setDataViewDisplayName(final String name, final String displayName) {
        chart.setDataViewDisplayName(name, displayName);
        refresh();
    }

    @Override
    public void setDisplayDuration(final double duration) {
        chart.setDisplayDuration(duration);
    }

    @Override
    public boolean isMathExpressionEnabled() {
        return chart.isMathExpressionEnabled();
    }

    @Override
    public void setMathExpressionEnabled(final boolean enable) {
        chart.setMathExpressionEnabled(enable);
    }

    @Override
    public void setLabelPlacement(final int p) {
        chart.setLabelPlacement(p);
    }

    @Override
    public int getLabelPlacement() {
        return chart.getLabelPlacement();
    }

    @Override
    public double getLegendProportion() {
        // not managed
        return Double.NaN;
    }

    @Override
    public void setLegendProportion(double p) {
        // not managed
    }

    @Override
    public void setNoValueString(final String noValueString) {
        chart.setNoValueString(noValueString);
    }

    @Override
    public String getNoValueString() {
        return chart.getNoValueString();
    }

    @Override
    public void setAxisMaximum(final double maximum, final int axis) {
        chart.setAxisMaximum(maximum, axis);
    }

    @Override
    public double getAxisMaximum(final int axis) {
        return chart.getAxisMaximum(axis);
    }

    @Override
    public void setAxisMinimum(final double minimum, final int axis) {
        chart.setAxisMinimum(minimum, axis);
    }

    @Override
    public double getAxisMinimum(final int axis) {
        return chart.getAxisMinimum(axis);
    }

    @Override
    public void setAxisDrawOpposite(final boolean opposite, final int axis) {
        chart.setAxisDrawOpposite(opposite, axis);
    }

    @Override
    public boolean isAxisDrawOpposite(final int axis) {
        return chart.isAxisDrawOpposite(axis);
    }

    @Override
    public void setAxisLabelFormat(final int format, final int axis) {
        chart.setAxisLabelFormat(format, axis);
    }

    @Override
    public int getAxisLabelFormat(final int axis) {
        return chart.getAxisLabelFormat(axis);
    }

    @Override
    public void setAxisPosition(final int position, final int axis) {
        chart.setAxisPosition(position, axis);
    }

    @Override
    public int getAxisPosition(final int axis) {
        return chart.getAxisPosition(axis);
    }

    @Override
    public void setAxisVisible(final boolean visible, final int axis) {
        chart.setAxisVisible(visible, axis);
    }

    @Override
    public boolean isAxisVisible(final int axis) {
        return chart.isAxisVisible(axis);
    }

    @Override
    public boolean isTimeScale(int axis) {
        return getAnnotation(axis) == JLAxis.TIME_ANNO;
    }

    public void setCustomColor(final Color[] colorList) {
        chart.setCustomColor(colorList);
    }

    /**
     * Set the custom fill color map attribution
     */
    public void setCustomFillColor(final Color[] colorList) {
        chart.setCustomFillColor(colorList);
    }

    /**
     * Set the custom marker color map attribution
     */
    public void setCustomMarkerColor(final Color[] colorList) {
        chart.setCustomMarkerColor(colorList);
    }

    @Override
    public void setCustomLineStyle(final int[] lineStyleList) {
        lineStyle = lineStyleList;
        chart.setCustomLineStyle(lineStyleList);
    }

    @Override
    public int[] getCustomLineStyle() {
        return lineStyle;
    }

    @Override
    public void setCustomAxis(final int[] axisList) {
        this.axisList = axisList;
        chart.setCustomAxis(axisList);
    }

    @Override
    public int[] getCustomAxis() {
        return axisList;
    }

    @Override
    public void setCustomMarkerStyle(final int[] styleList) {
        markerStyle = styleList;
        chart.setCustomMarkerStyle(styleList);
    }

    @Override
    public int[] getCustomMarkerStyle() {
        return markerStyle;
    }

    @Override
    public void setCustomMarkerSize(final int[] sizeList) {
        markerSize = sizeList;
        chart.setCustomMarkerSize(sizeList);
    }

    @Override
    public int[] getCustomMarkerSize() {
        return markerSize;
    }

    @Override
    public void setCustomLineWidth(final int[] widthList) {
        lineWidth = widthList;
        chart.setCustomLineWidth(widthList);
    }

    @Override
    public void setCustomBarWidth(final int[] widthList) {
        barWidth = widthList;
        chart.setCustomBarWidth(widthList);
    }

    @Override
    public int[] getCustomLineWidth() {
        return lineWidth;
    }

    @Override
    public int[] getCustomBarWidth() {
        return barWidth;
    }

    @Override
    public void setCustomDataViewStyle(final int[] styleList) {
        dataViewStyle = styleList;
        chart.setCustomDataViewStyle(styleList);
    }

    @Override
    public int[] getCustomDataViewStyle() {
        return dataViewStyle;
    }

    @Override
    public void setCustomFillStyle(final int[] styleList) {
        fillStyle = styleList;
        chart.setCustomFillStyle(styleList);
    }

    @Override
    public int[] getCustomFillStyle() {
        return fillStyle;
    }

    @Override
    public double getDataViewTransformA0(final String name) {
        return chart.getDataViewTransformA0(name);
    }

    @Override
    public void setDataViewTransformA0(final String name, final double transform) {
        chart.setDataViewTransformA0(name, transform);
    }

    @Override
    public double getDataViewTransformA1(final String name) {
        return chart.getDataViewTransformA1(name);
    }

    @Override
    public void setDataViewTransformA1(final String name, final double transform) {
        chart.setDataViewTransformA1(name, transform);
    }

    @Override
    public double getDataViewTransformA2(final String name) {
        return chart.getDataViewTransformA2(name);
    }

    @Override
    public void setDataViewTransformA2(final String name, final double transform) {
        chart.setDataViewTransformA2(name, transform);
    }

    @Override
    public boolean isDataViewClickable(final String name) {
        return chart.isDataViewClickable(name);
    }

    @Override
    public void setDataViewClickable(final String name, final boolean clickable) {
        chart.setDataViewClickable(name, clickable);
    }

    @Override
    public boolean isDataViewLabelVisible(final String visible) {
        return chart.isDataViewLabelVisible(visible);
    }

    @Override
    public void setDataViewLabelVisible(final String name, final boolean visible) {
        chart.setDataViewLabelVisible(name, visible);
    }

    // sampling is transmitted, but not managed

    @Override
    public boolean isDataViewSamplingAllowed(String name) {
        return chart.isDataViewSamplingAllowed(name);
    }

    @Override
    public void setDataViewSamplingAllowed(String name, boolean allowed) {
        chart.setDataViewSamplingAllowed(name, allowed);
    }

    public void setCustomFillMethod(final int[] methodList) {
        fillMethod = methodList;
        chart.setCustomFillStyle(methodList);
    }

    @Override
    public int getDataViewInterpolationMethod(final String name) {
        return chart.getDataViewInterpolationMethod(name);
    }

    @Override
    public void setDataViewInterpolationMethod(final String name, final int method) {
        chart.setDataViewInterpolationMethod(name, method);
    }

    @Override
    public int getDataViewInterpolationStep(final String name) {
        return chart.getDataViewInterpolationStep(name);
    }

    @Override
    public void setDataViewInterpolationStep(final String name, final int step) {
        chart.setDataViewInterpolationStep(name, step);
    }

    @Override
    public double getDataViewHermiteBias(final String name) {
        return chart.getDataViewHermiteBias(name);
    }

    @Override
    public void setDataViewHermiteBias(final String name, final double bias) {
        chart.setDataViewHermiteBias(name, bias);
    }

    @Override
    public double getDataViewHermiteTension(final String name) {
        return chart.getDataViewHermiteTension(name);
    }

    @Override
    public void setDataViewHermiteTension(final String name, final double tension) {
        chart.setDataViewHermiteTension(name, tension);
    }

    @Override
    public int getDataViewSmoothingMethod(final String name) {
        return chart.getDataViewSmoothingMethod(name);
    }

    @Override
    public void setDataViewSmoothingMethod(final String name, final int method) {
        chart.setDataViewSmoothingMethod(name, method);
    }

    @Override
    public int getDataViewSmoothingNeighbors(final String name) {
        return chart.getDataViewSmoothingNeighbors(name);
    }

    @Override
    public void setDataViewSmoothingNeighbors(final String name, final int neighbors) {
        chart.setDataViewSmoothingNeighbors(name, neighbors);
    }

    @Override
    public double getDataViewSmoothingGaussSigma(final String name) {
        return chart.getDataViewSmoothingGaussSigma(name);
    }

    @Override
    public void setDataViewSmoothingGaussSigma(final String name, final double sigma) {
        chart.setDataViewSmoothingGaussSigma(name, sigma);
    }

    @Override
    public int getDataViewSmoothingExtrapolation(final String name) {
        return chart.getDataViewSmoothingExtrapolation(name);
    }

    @Override
    public void setDataViewSmoothingExtrapolation(final String name, final int extrapolation) {
        chart.setDataViewSmoothingExtrapolation(name, extrapolation);
    }

    @Override
    public int getDataViewMathFunction(final String name) {
        return chart.getDataViewMathFunction(name);
    }

    @Override
    public void setDataViewMathFunction(final String name, final int function) {
        chart.setDataViewMathFunction(name, function);
    }

    @Override
    public int getDataViewHighlightMethod(final String name) {
        return chart.getDataViewHighlightMethod(name);
    }

    @Override
    public void setDataViewHighlightMethod(final String name, final int method) {
        chart.setDataViewHighlightMethod(name, method);
    }

    @Override
    public double getDataViewHighlightCoefficient(final String name) {
        return chart.getDataViewHighlightCoefficient(name);
    }

    @Override
    public void setDataViewHighlightCoefficient(final String name, final double coef) {
        chart.setDataViewHighlightCoefficient(name, coef);
    }

    @Override
    public boolean isDataViewHighlighted(final String name) {
        return chart.isDataViewHighlighted(name);
    }

    @Override
    public void setDataViewHighlighted(final String name, final boolean highlighted) {
        chart.setDataViewHighlighted(name, highlighted);
    }

    @Override
    public void setAutoHighlightOnLegend(final boolean autoHighlightOnLegend) {
        chart.setAutoHighlightOnLegend(autoHighlightOnLegend);
    }

    @Override
    public boolean isAutoHighlightOnLegend() {
        return chart.isAutoHighlightOnLegend();
    }

    @Override
    public boolean isDataViewRemovingEnabled() {
        return chart.isDataViewRemovingEnabled();
    }

    @Override
    public void setDataViewRemovingEnabled(final boolean curveRemovingEnabled) {
        chart.setDataViewRemovingEnabled(curveRemovingEnabled);
    }

    @Override
    public boolean isCleanDataViewConfigurationOnRemoving() {
        return chart.isCleanDataViewConfigurationOnRemoving();
    }

    @Override
    public void setCleanDataViewConfigurationOnRemoving(final boolean cleanCurveConfigurationOnRemoving) {
        chart.setCleanDataViewConfigurationOnRemoving(cleanCurveConfigurationOnRemoving);
    }

    @Override
    public boolean hasDataViewProperties(final String name) {
        return chart.hasDataViewProperties(name);
    }

    @Override
    public void cleanDataViewProperties(final String name) {
        chart.cleanDataViewProperties(name);
    }

    @Override
    public PlotProperties getDataViewPlotProperties(final String name) {
        return chart.getDataViewPlotProperties(name);
    }

    @Override
    public void setDataViewPlotProperties(final String name, final PlotProperties properties) {
        chart.setDataViewPlotProperties(name, properties);
        refresh();
    }

    @Override
    public BarProperties getDataViewBarProperties(final String name) {
        return chart.getDataViewBarProperties(name);
    }

    @Override
    public void setDataViewBarProperties(final String name, final BarProperties properties) {
        chart.setDataViewBarProperties(name, properties);
        refresh();
    }

    @Override
    public CurveProperties getDataViewCurveProperties(final String name) {
        return chart.getDataViewCurveProperties(name);
    }

    @Override
    public void setDataViewCurveProperties(final String name, final CurveProperties properties) {
        chart.setDataViewCurveProperties(name, properties);
        refresh();
    }

    @Override
    public MarkerProperties getDataViewMarkerProperties(final String name) {
        return chart.getDataViewMarkerProperties(name);
    }

    @Override
    public void setDataViewMarkerProperties(final String name, final MarkerProperties properties) {
        chart.setDataViewMarkerProperties(name, properties);
        refresh();
    }

    @Override
    public ErrorProperties getDataViewErrorProperties(final String name) {
        // not supported
        return null;
    }

    @Override
    public void setDataViewErrorProperties(final String name, final ErrorProperties properties) {
        // not supported
    }

    @Override
    public boolean isDataViewErrorVisible(String id) {
        // not supported
        return false;
    }

    @Override
    public void setDataViewErrorVisible(String id, boolean visible) {
        // not supported
    }

    @Override
    public TransformationProperties getDataViewTransformationProperties(final String name) {
        return chart.getDataViewTransformationProperties(name);
    }

    @Override
    public void setDataViewTransformationProperties(final String name, final TransformationProperties properties) {
        chart.setDataViewTransformationProperties(name, properties);
        refresh();
    }

    @Override
    public double getDataViewXOffsetA0(String id) {
        return chart.getDataViewXOffsetA0(id);
    }

    @Override
    public void setDataViewXOffsetA0(String id, double a0x) {
        chart.setDataViewXOffsetA0(id, a0x);
        refresh();
    }

    @Override
    public OffsetProperties getDataViewXOffsetProperties(String name) {
        return chart.getDataViewXOffsetProperties(name);
    }

    @Override
    public void setDataViewXOffsetProperties(String id, OffsetProperties properties) {
        chart.setDataViewXOffsetProperties(id, properties);
        refresh();
    }

    @Override
    public InterpolationProperties getDataViewInterpolationProperties(final String name) {
        return chart.getDataViewInterpolationProperties(name);
    }

    @Override
    public void setDataViewInterpolationProperties(final String name, final InterpolationProperties properties) {
        chart.setDataViewInterpolationProperties(name, properties);
        refresh();
    }

    @Override
    public SmoothingProperties getDataViewSmoothingProperties(final String name) {
        return chart.getDataViewSmoothingProperties(name);
    }

    @Override
    public void setDataViewSmoothingProperties(final String name, final SmoothingProperties properties) {
        chart.setDataViewSmoothingProperties(name, properties);
        refresh();
    }

    @Override
    public MathProperties getDataViewMathProperties(final String name) {
        return chart.getDataViewMathProperties(name);
    }

    @Override
    public void setDataViewMathProperties(final String name, final MathProperties properties) {
        chart.setDataViewMathProperties(name, properties);
        refresh();
    }

    @Override
    public ChartProperties getChartProperties() {
        return chart.getChartProperties();
    }

    @Override
    public void setChartProperties(final ChartProperties properties) {
        chart.setChartProperties(properties);
        refresh();
    }

    @Override
    public AxisProperties getAxisProperties(final int axisChoice) {
        return chart.getAxisProperties(axisChoice);
    }

    @Override
    public void setAxisProperties(final AxisProperties properties, final int axisChoice) {
        chart.setAxisProperties(properties, axisChoice);
        refresh();
    }

    @Override
    public SamplingProperties getSamplingProperties() {
        // not managed
        return new SamplingProperties(false, 0, 0);
    }

    @Override
    public void setSamplingProperties(SamplingProperties properties) {
        // not managed
    }

    @Override
    public int getDataViewOffsetDragType() {
        // not managed
        return DRAG_NONE;
    }

    @Override
    public void setDataViewOffsetDragType(int dataViewOffsetDragType) {
        // not managed
    }

    @Override
    public int getScaleDragType() {
        // not managed
        return DRAG_NONE;
    }

    @Override
    public void setScaleDragType(int scaleDragType) {
        // not managed
    }

    @Override
    public int getScaleDragSensitivity() {
        // not managed
        return 0;
    }

    @Override
    public void setScaleDragSensitivity(int scaleDragSensitivity) {
        // not managed
    }

    @Override
    public DragProperties getDragProperties() {
        // not managed
        return NO_DRAG_PROPERTIES.clone();
    }

    @Override
    public void setDragProperties(DragProperties properties) {
        // not managed
    }

    @Override
    public void setAxisScaleEditionEnabled(final boolean enabled, final int axis) {
        chart.setAxisScaleEditionEnabled(enabled, axis);
    }

    public int[] getCustomFillMethod() {
        return fillMethod;
    }

    @Override
    public void setCustomUserFormat(final String[] formatList) {
        this.formatList = formatList;
        chart.setCustomUserFormat(formatList);
    }

    @Override
    public String[] getCustomUserFormat() {
        return formatList;
    }

    @Override
    public void resetAll() {
        chart.resetAll();
        // repaint forced to update legend
        refresh();
    }

    @Override
    public void resetAll(final boolean clearConfiguration) {
        chart.reset(false, true, clearConfiguration);
        refresh();
    }

    public void reset(boolean showConfirmDialog, boolean allViews, boolean clearConfiguration) {
        chart.reset(showConfirmDialog, allViews, clearConfiguration);
        refresh();
    }

    @Override
    public void addExpression(final String dataViewName, final String expression, final int axis,
            final String[] variables, final boolean x) {
        chart.addExpression(dataViewName, expression, axis, variables, x);
        // repaint forced to update legend
        refresh();
    }

    @Override
    public void addExpression(final String id, final String name, final String expression, final int axis,
            final String[] variables, final boolean x) {
        chart.addExpression(id, name, expression, axis, variables, x);
        // repaint forced to update legend
        refresh();
    }

    @Override
    public void actionPerformed(final JLChartActionEvent evt) {
        if ((evt != null) && (evt.getName() != null)) {
            if (JLChart.CHANGE_LABEL_PLACEMENT.equals(evt.getName())
                    || JLChart.CHANGE_LABEL_VISIBILITY.equals(evt.getName())) {
                updateChartLegend();
            } else if (JLChart.CHART_TO_IMAGE_FILE.equals(evt.getName())) {
                saveSnapshot();
            } else if (JLChart.PRINT_CHART.equals(evt.getName())) {
                printGraph();
            }
        }
    }

    @Override
    public boolean getActionState(final JLChartActionEvent evt) {
        // return default value
        return true;
    }

    @Override
    public boolean isCyclingCustomMap() {
        return chart.isCyclingCustomMap();
    }

    @Override
    public void setCyclingCustomMap(final boolean cyclingCustomMap) {
        chart.setCyclingCustomMap(cyclingCustomMap);
    }

    @Override
    public void setDataViewEditable(final String name, final boolean editable) {
        chart.setDataViewEditable(name, editable);
    }

    private void saveSnapshot() {
        snapshotEventDelegate
                .changeSnapshotLocation(SnapshotUtil.snapshot(this, snapshotEventDelegate.getSnapshotLocation(), true));
    }

    @Override
    public void addSnapshotListener(ISnapshotListener listener) {
        snapshotEventDelegate.addSnapshotListener(listener);
    }

    @Override
    public void removeSnapshotListener(ISnapshotListener listener) {
        snapshotEventDelegate.removeSnapshotListener(listener);
    }

    @Override
    public String getSnapshotDirectory() {
        return snapshotEventDelegate.getSnapshotDirectory();
    }

    @Override
    public void setSnapshotDirectory(String snapshotDirectory) {
        snapshotEventDelegate.setSnapshotDirectory(snapshotDirectory);
    }

    @Override
    public String getSnapshotFile() {
        return snapshotEventDelegate.getSnapshotFile();
    }

    public String getSnapshotLocation() {
        return snapshotEventDelegate.getSnapshotLocation();
    }

    /**
     * Prints out this graph.
     */
    public void printGraph() {
        try {
            EasyPrinter.print(this);
        } catch (PrintException e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to print chart", e);
        }
    }

    @Override
    public boolean isLimitDrawingZone() {
        return chart.isLimitDrawingZone();
    }

    @Override
    public void setLimitDrawingZone(final boolean limitDrawingZone) {
        chart.setLimitDrawingZone(limitDrawingZone);
    }

    @Override
    public void setAxisCometeColor(final CometeColor color, final int axis) {
        setAxisColor(ColorTool.getColor(color), axis);
    }

    @Override
    public void setDataViewMarkerCometeColor(final String name, final CometeColor color) {
        setDataViewMarkerColor(name, ColorTool.getColor(color));
    }

    @Override
    public CometeColor getHeaderCometeColor() {
        return ColorTool.getCometeColor(getHeaderColor());
    }

    @Override
    public CometeFont getHeaderCometeFont() {
        return FontTool.getCometeFont(getHeaderFont());
    }

    @Override
    public CometeFont getLabelCometeFont() {
        return FontTool.getCometeFont(getLabelFont());
    }

    @Override
    public void setHeaderCometeColor(final CometeColor c) {
        setHeaderColor(ColorTool.getColor(c));
    }

    @Override
    public void setHeaderCometeFont(final CometeFont f) {
        setHeaderFont(FontTool.getFont(f));
    }

    @Override
    public void setLabelCometeFont(final CometeFont f) {
        setLabelFont(FontTool.getFont(f));
    }

    @Override
    public void setDataViewCometeColor(final String name, final CometeColor color) {
        setDataViewColor(name, ColorTool.getColor(color));
    }

    @Deprecated
    public void setDataViewCometeFillColor(final String name, final CometeColor color) {
        setDataViewFillCometeColor(name, color);
    }

    @Override
    public void setDataViewFillCometeColor(final String name, final CometeColor color) {
        setDataViewFillColor(name, ColorTool.getColor(color));
    }

    @Override
    public CometeColor getAxisCometeColor(final int axis) {
        final Color color = getAxisColor(axis);
        if (color != null) {
            ColorTool.getCometeColor(color);
        }
        return null;
    }

    @Override
    public CometeColor getDataViewCometeColor(final String name) {
        final Color color = getDataViewColor(name);
        if (color != null) {
            ColorTool.getCometeColor(color);
        }
        return null;
    }

    @Override
    public CometeColor getDataViewFillCometeColor(final String name) {
        final Color color = getDataViewFillColor(name);
        if (color != null) {
            ColorTool.getCometeColor(color);
        }
        return null;
    }

    @Deprecated
    public CometeColor getDataViewCometeFillColor(final String name) {
        return getDataViewFillCometeColor(name);
    }

    @Override
    public CometeColor getDataViewMarkerCometeColor(final String name) {
        final Color color = getDataViewMarkerColor(name);
        if (color != null) {
            ColorTool.getCometeColor(color);
        }
        return null;
    }

    @Override
    public CometeColor getChartCometeBackground() {
        final Color color = getChartBackground();
        if (color != null) {
            return ColorTool.getCometeColor(color);
        }
        return null;
    }

    @Override
    public void setChartCometeBackground(final CometeColor c) {
        if (c != null) {
            setChartBackground(ColorTool.getColor(c));
        }
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(getForeground());
    }

    @Override
    public void setCometeBackground(final CometeColor c) {
        Color bg = null;
        if (c != null) {
            bg = ColorTool.getColor(c);
        }
        // apply background to children
        setBackground(bg);
        setChartMainBackground(bg);
        setManagementPanelBackground(bg);
        setFreezeBackground(bg);
    }

    @Override
    public void setCometeFont(final CometeFont font) {
        setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(final CometeColor color) {
        setForeground(ColorTool.getColor(color));
    }

    @Override
    public int getHorizontalAlignment() {
        return horizontalAlignment;
    }

    @Override
    public void setHorizontalAlignment(final int halign) {
        horizontalAlignment = halign;
        switch (horizontalAlignment) {
            case IComponent.CENTER:
                setAlignmentX(JPanel.CENTER_ALIGNMENT);
                break;
            case IComponent.LEFT:
                setAlignmentX(JPanel.LEFT_ALIGNMENT);
                break;
            case IComponent.RIGHT:
                setAlignmentX(JPanel.RIGHT_ALIGNMENT);
                break;
        }
    }

    @Override
    public void setCustomCometeColor(final CometeColor[] colorList) {
        if (colorList != null) {
            cometeColorList = colorList;
            final Color[] newColorList = new Color[cometeColorList.length];
            for (int i = 0; i < cometeColorList.length; i++) {
                newColorList[i] = ColorTool.getColor((cometeColorList[i]));
            }
            setCustomColor(newColorList);
        }
    }

    @Override
    public CometeColor[] getCustomCometeColor() {
        return cometeColorList;
    }

    @Override
    public void setCustomFillCometeColor(final CometeColor[] colorList) {
        if (colorList != null) {
            cometeFillColorList = colorList;
            final Color[] newColorList = new Color[cometeFillColorList.length];
            for (int i = 0; i < cometeFillColorList.length; i++) {
                newColorList[i] = ColorTool.getColor((cometeFillColorList[i]));
            }
            setCustomColor(newColorList);
        }
    }

    @Override
    public CometeColor[] getCustomFillCometeColor() {
        return cometeFillColorList;
    }

    @Override
    public void setCustomMarkerCometeColor(final CometeColor[] colorList) {
        if (colorList != null) {
            cometeMarkerColorList = colorList;
            final Color[] newColorList = new Color[cometeMarkerColorList.length];
            for (int i = 0; i < cometeMarkerColorList.length; i++) {
                newColorList[i] = ColorTool.getColor((cometeMarkerColorList[i]));
            }
            setCustomColor(newColorList);
        }
    }

    @Override
    public CometeColor[] getCustomMarkerCometeColor() {
        return cometeMarkerColorList;
    }

    @Override
    public void setPreferredSize(final int width, final int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void setDataViewErrorCometeColor(String id, CometeColor color) {
        // not supported
    }

    @Override
    public CometeColor getDataViewErrorCometeColor(String id) {
        // not supported
        return null;
    }

    // JIRA JAVAAPI-39
    @Override
    public void setLabels(int axis, String[] labels, double[] labelPositions) {
        chart.getAxis(axis).setLabels(labels, labelPositions);
    }

    @Override
    public void clearLabels(int axis) {
        setLabels(axis, null, null);
    }

    /**
     * Returns whether {@link AbstractDataView}s should be considered as sorted
     * on X
     * 
     * @return A <code>boolean</code> value;
     */
    public boolean isDataViewsSortedOnX() {
        return chart.isDataViewsSortedOnX();
    }

    /**
     * Sets whether {@link AbstractDataView}s should be considered as sorted on
     * X
     * 
     * @param dataViewsSortedOnX
     *            whether {@link AbstractDataView}s should be considered as
     *            sorted on X
     * @param applyOnExistingViews
     *            Whether to apply this rule on already existing views
     */
    public void setDataViewsSortedOnX(boolean dataViewsSortedOnX, boolean applyOnExistingViews) {
        chart.setDataViewsSortedOnX(dataViewsSortedOnX, applyOnExistingViews);
    }

    /**
     * Sets whether {@link AbstractDataView}s should be considered as sorted on
     * X
     * 
     * @param dataViewsSortedOnX
     *            whether {@link AbstractDataView}s should be considered as
     *            sorted on X
     */
    public void setDataViewsSortedOnX(boolean dataViewsSortedOnX) {
        setDataViewsSortedOnX(dataViewsSortedOnX, true);
    }

    @Override
    public void addMediator(final Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(final Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I>
     *            The type of Data the {@link TargetInformation} is able to
     *            manipulate
     * @param <V>
     *            The type of {@link TargetInformation} to transmit
     * @param targetInformation
     *            The {@link TargetInformation} that should be transmitted to {@link Mediator}s
     */
    @Override
    public <I, V extends TargetInformation<I>> void warnMediators(final V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    // This is a stress test to check our chart performance
    public static void main(final String[] args) {
        final ChartViewer chartViewer = new ChartViewer();
        chartViewer.setManagementPanelVisible(true);
        chartViewer.setAxisSelectionVisible(true);
        chartViewer.setAutoHighlightOnLegend(true);
        final Map<String, Object> data = new LinkedHashMap<String, Object>();
        // Create 1000 dataview
        String tmpName = null;
        double[] flatValues = null;
        final NumberFormat format = NumberFormat.getInstance();
        format.setMinimumIntegerDigits(3);
        for (int i = 0; i < 900; i++) {
            tmpName = "Curve-" + format.format(i);
            flatValues = new double[10];
            for (int j = 0; j < 10; j++) {
                flatValues[j] = j;
                flatValues[++j] = j + i;
            }
            System.out.println("DataView " + tmpName + " generated: " + Arrays.toString(flatValues));
            data.put(tmpName, flatValues);
        }

        chartViewer.setData(data);

        final JFrame testFrame = new JFrame(ChartViewer.class.getSimpleName() + " test");
        testFrame.setContentPane(chartViewer);
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setSize(600, 600);
        testFrame.setVisible(true);

    }

}
