/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.hidden;

import ij.ImageJ;

/**
 * Hide IJ main frame (with mode buttons and menus). We only need one, each image in IJ is
 * displayed in a separate frame.
 * 
 * @author MAINGUY
 */
public class HiddenImageJ extends ImageJ {

    private static final long serialVersionUID = 7717139574610621379L;

    public HiddenImageJ() {
        super();
    }

    @Override
    public void show() {
    }
}
