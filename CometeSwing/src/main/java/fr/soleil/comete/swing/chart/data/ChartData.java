/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.data;

/**
 * A class that stores data extracted from chart data file.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ChartData<D extends AbstractDataView> {

    private final D[] data;
    private final String fileLocation;

    public ChartData(D[] data, String fileLocation) {
        super();
        this.data = data;
        this.fileLocation = fileLocation;
    }

    /**
     * The recovered data.
     * 
     * @return An {@link AbstractDataView} array.
     */
    public D[] getData() {
        return data;
    }

    /**
     * The path of the file from which data was extracted
     * 
     * @return A {@link String}
     */
    public String getFileLocation() {
        return fileLocation;
    }

}
