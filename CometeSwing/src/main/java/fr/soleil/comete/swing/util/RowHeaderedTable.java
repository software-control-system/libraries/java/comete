/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.JXTable;

/**
 * A {@link JXTable} that manages its own row header when put in a {@link JScrollPane}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class RowHeaderedTable extends JXTable implements RowSorterListener {

    private static final long serialVersionUID = 219344720381766644L;

    protected JXTable rowHeaderTable;
    protected JPanel rowHeaderPanel;

    public RowHeaderedTable() {
        super();
    }

    public RowHeaderedTable(TableModel dm) {
        super(dm);
    }

    public RowHeaderedTable(TableModel dm, TableColumnModel cm) {
        super(dm, cm);
    }

    public RowHeaderedTable(int numRows, int numColumns) {
        super(numRows, numColumns);
    }

    public RowHeaderedTable(Vector<?> rowData, Vector<?> columnNames) {
        super(rowData, columnNames);
    }

    public RowHeaderedTable(Object[][] rowData, Object[] columnNames) {
        super(rowData, columnNames);
    }

    public RowHeaderedTable(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
        super(dm, cm, sm);
    }

    @Override
    public void setRowSorter(RowSorter<? extends TableModel> sorter) {
        RowSorter<?> previousSorter = getRowSorter();
        if (previousSorter != null) {
            previousSorter.removeRowSorterListener(this);
        }
        super.setRowSorter(sorter);
        RowSorter<?> currentSorter = getRowSorter();
        if (currentSorter != null) {
            currentSorter.addRowSorterListener(this);
        }
        updateHeaderModel();
    }

    @Override
    public void setModel(TableModel dataModel) {
        super.setModel(dataModel);
        if (rowHeaderTable == null) {
            rowHeaderTable = new JXTable() {

                private static final long serialVersionUID = 1516498701429156002L;

                @Override
                public Dimension getPreferredScrollableViewportSize() {
                    return getPreferredSize();
                }

            };
            int rowHeight = getRowHeight();
            if (rowHeight < 1) {
                rowHeight = 1;
            }
            rowHeaderTable.setRowHeight(rowHeight);
            rowHeaderTable.setRowMargin(getRowMargin());
            rowHeaderTable.setAutoResizeMode(getAutoResizeMode());
            rowHeaderTable.setSortable(false);
            rowHeaderTable.setSelectionModel(new DefaultListSelectionModel() {

                private static final long serialVersionUID = 2482486174943980670L;

                @Override
                public void setSelectionInterval(int index0, int index1) {
                    super.setSelectionInterval(index0, index1);
                    if (RowHeaderedTable.this.getColumnCount() > 0) {
                        RowHeaderedTable.this.setColumnSelectionInterval(0, RowHeaderedTable.this.getColumnCount() - 1);
                        RowHeaderedTable.this.setRowSelectionInterval(index0, index1);
                    }
                }

            });
            setRowHeaderTableRenderer();

            // The rowHeaderPanel contains the row header table + a glue that
            // pushes the table at the top of the panel (so that if the header
            // is too big, the row header table is still in front of its
            // corresponding table)
            rowHeaderPanel = new JPanel(new GridBagLayout());
            GridBagConstraints headerConstraints = new GridBagConstraints();
            headerConstraints.fill = GridBagConstraints.NONE;
            headerConstraints.gridx = 0;
            headerConstraints.gridy = 0;
            headerConstraints.weightx = 0;
            headerConstraints.weighty = 0;
            headerConstraints.anchor = GridBagConstraints.NORTHEAST;
            rowHeaderPanel.add(rowHeaderTable, headerConstraints);
            GridBagConstraints glueConstraints = new GridBagConstraints();
            glueConstraints.fill = GridBagConstraints.BOTH;
            glueConstraints.gridx = 0;
            glueConstraints.gridy = 1;
            glueConstraints.weightx = 1;
            glueConstraints.weighty = 1;
            rowHeaderPanel.add(Box.createGlue(), glueConstraints);
        }
        rowHeaderTable.setModel(generateRowHeaderTableModel(dataModel));
    }

    public void setFullBackground(Color bg) {
        setBackground(bg);
        if (rowHeaderTable != null) {
            rowHeaderTable.setBackground(bg);
        }
        if (rowHeaderPanel != null) {
            rowHeaderPanel.setBackground(bg);
        }
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        super.setOpaque(isOpaque);
        if (rowHeaderTable != null) {
            rowHeaderTable.setOpaque(isOpaque);
        }
        if (rowHeaderPanel != null) {
            rowHeaderPanel.setOpaque(isOpaque);
        }
    }

    protected DefaultTableModel generateRowHeaderTableModel(TableModel dataModel) {
        DefaultTableModel tm;
        if (dataModel == null) {
            tm = new DefaultTableModel();
        } else {
            tm = new DefaultTableModel() {

                private static final long serialVersionUID = -5805907979600281405L;

                @Override
                public int getColumnCount() {
                    return 1;
                }

                @Override
                public int getRowCount() {
                    return getModel().getRowCount();
                }

                @Override
                public Object getValueAt(int row, int column) {
                    if (getModel() instanceof DefaultTableModelWithRowName) {
                        return ((DefaultTableModelWithRowName) getModel()).getRowName(row);
                    }
                    return Integer.toString(row);
                }

                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }

            };
        }
        return tm;
    }

    @Override
    protected void initializeLocalVars() {
        super.initializeLocalVars();
        setRowHeaderTableRenderer();
    }

    @Override
    protected void configureEnclosingScrollPane() {
        super.configureEnclosingScrollPane();
        Container p = getParent();
        if (p instanceof JViewport) {
            Container gp = p.getParent();
            if (gp instanceof JScrollPane) {
                JScrollPane scrollPane = (JScrollPane) gp;
                // Make certain we are the viewPort's view and not, for
                // example, the rowHeaderView of the scrollPane -
                // an implementor of fixed columns might do this.
                JViewport viewport = scrollPane.getViewport();
                if (viewport != null && viewport.getView() == this) {
                    scrollPane.setRowHeaderView(rowHeaderPanel);
                }
            }
        }
    }

    @Override
    protected void unconfigureEnclosingScrollPane() {
        super.unconfigureEnclosingScrollPane();
        Container p = getParent();
        if (p instanceof JViewport) {
            Container gp = p.getParent();
            if (gp instanceof JScrollPane) {
                JScrollPane scrollPane = (JScrollPane) gp;
                // Make certain we are the viewPort's view and not, for
                // example, the rowHeaderView of the scrollPane -
                // an implementor of fixed columns might do this.
                JViewport viewport = scrollPane.getViewport();
                if (viewport != null && viewport.getView() == this) {
                    scrollPane.setRowHeaderView(null);
                }
            }
        }
    }

    @Override
    public void setRowHeight(int row, int rowHeight) {
        super.setRowHeight(row, rowHeight);
        if (rowHeaderTable != null && (rowHeaderTable.getRowHeight(row) != rowHeight)) {
            rowHeaderTable.setRowHeight(row, rowHeight);
        }
    }

    @Override
    public void setRowHeight(int rowHeight) {
        super.setRowHeight(rowHeight);
        if (rowHeaderTable != null && (rowHeaderTable.getRowHeight() != rowHeight)) {
            rowHeaderTable.setRowHeight(rowHeight);
        }
    }

    @Override
    public void setRowMargin(int rowMargin) {
        super.setRowMargin(rowMargin);
        if (rowHeaderTable != null && (rowHeaderTable.getRowMargin() != rowMargin)) {
            rowHeaderTable.setRowMargin(rowMargin);
        }
    }

    protected void setRowHeaderTableRenderer() {
        if (rowHeaderTable != null && getTableHeader() != null) {
            rowHeaderTable.setDefaultRenderer(Object.class, getTableHeader().getDefaultRenderer());
        }
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        super.tableChanged(e);
        if (e.getType() == TableModelEvent.HEADER_ROW || e.getType() == TableModelEvent.INSERT
                || e.getType() == TableModelEvent.DELETE) {
            updateHeaderModel();
        }
    }

    @Override
    public void sorterChanged(RowSorterEvent e) {
        super.sorterChanged(e);
        updateHeaderModel();
    }

    protected void updateHeaderModel() {
        if (rowHeaderTable != null) {
            DefaultTableModel tm = (DefaultTableModel) rowHeaderTable.getModel();
            tm.fireTableStructureChanged();
            Container p = getParent();
            if (p instanceof JViewport) {
                Container gp = p.getParent();
                if (gp instanceof JScrollPane) {
                    JScrollPane scrollPane = (JScrollPane) gp;
                    // Make certain we are the viewPort's view and not, for
                    // example, the rowHeaderView of the scrollPane -
                    // an implementor of fixed columns might do this.
                    JViewport viewport = scrollPane.getViewport();
                    if (viewport != null && viewport.getView() == this) {
                        scrollPane.revalidate();
                        scrollPane.repaint();
                    }
                }
            }
        }
    }

    @Override
    public void setAutoResizeMode(int mode) {
        super.setAutoResizeMode(mode);
        if (rowHeaderTable != null) {
            rowHeaderTable.setAutoResizeMode(mode);
        }
    }

    @Override
    public void packAll() {
        super.packAll();
        if (rowHeaderTable != null) {
            rowHeaderTable.packAll();
        }
    }

    @Override
    public void revalidate() {
        super.revalidate();
        if (rowHeaderTable != null) {
            rowHeaderTable.revalidate();
        }
    }

    @Override
    public void repaint() {
        super.repaint();
        if (rowHeaderTable != null) {
            rowHeaderTable.repaint();
        }
    }

    public JXTable getRowHeaderTable() {
        return rowHeaderTable;
    }
}
