/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.expression;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Window;
import java.util.Arrays;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * A {@link JDialog} that describes how to use expressions
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ParserHelpDialog extends JDialog {

    private static final long serialVersionUID = -300319278925591145L;

    /**
     * Represents the supported function names
     */
    private static final String[] FUNCTION_NAMES = { "Sine", "Cosine", "Tangent", "Arc Sine", "Arc Cosine",
            "Arc Tangent", "Arc Tangent (with 2 parameters)", "Hyperbolic Sine", "Hyperbolic Cosine",
            "Hyperbolic Tangent", "Inverse Hyperbolic Sine", "Inverse Hyperbolic Cosine", "Inverse Hyperblic Tangent",
            "Natural Logarithm", "Logarithm base 10", "Exponential (e^x)", "Absolute Value", "Modulus", "Square Root",
            "Sum", "If" };

    /**
     * Represents the supported function representations (the way to write them in formulas)
     */
    private static final String[] FUNCTION_REPRESENTATIONS = { "sin(x)", "cos(x)", "tan(x)", "asin(x)", "acos(x)",
            "atan(x)", "atan2(x2, x1)", "sinh(x)", "cosh(x)", "tanh(x)", "asinh(x)", "acosh(x)", "atanh(x)", "ln(x)",
            "log(x)", "exp(x)", "abs(x)", "mod(x1, x2) = x1 % x2", "sqrt(x)", "sum(x1, x2, x3)",
            "if(cond, trueval, falseval)" };

    /**
     * Represents the supported operator names
     */
    private static final String[] OPERATOR_NAMES = { "Power", "Boolean Not", "Unary Plus, Unary Minus", "Modulus",
            "Division", "Multiplication", "Addition, Substraction", "Less or Equal, More or Equal",
            "Less Than, Greater Than", "Equal, Not Equal", "Boolean And", "Boolean Or" };

    /**
     * Represents the supported operator representations (the way to write them in formulas)
     */
    private static final String[] OPERATOR_REPRESENTATIONS = { "^", "!", "+x, -x", "%", "/", "*", "+, -", "<=, >=",
            "<, >", "!=, ==", "&&", "||" };

    protected JLabel[] introLabel;
    protected JLabel operatorLabel, functionLabel;
    protected JTable operatorTable, functionTable;
    protected JScrollPane operatorScrollPane, functionScrollPane;
    protected JPanel mainPanel;
    protected final static Font expressionTitleFont = new Font("Times New Roman", Font.BOLD, 20);

    protected final static Dimension bestDim = new Dimension(610, 745);

    private boolean firstInit;

    public ParserHelpDialog(Window parent) {
        super(parent, "About Expressions");
        setModal(false);
        initComponents();
        addComponents();
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        firstInit = true;
    }

    protected void initComponents() {
        mainPanel = new JPanel();
        mainPanel.setBackground(Color.WHITE);
        mainPanel.setLayout(null);

        introLabel = new JLabel[6];
        introLabel[0] = generateLabel("How to evaluate an expression:", 5, 0, 605, 17, null, null);
        introLabel[1] = generateLabel("Enter your expression with variable names as parameter", 5, 22, 605, 17, null,
                null);
        introLabel[2] = generateLabel("Your expression must look like 'f(x)' if there is one variable,", 5, 39, 605, 17,
                null, null);
        introLabel[3] = generateLabel("or 'f(x1,...,xn)' if there are n variables", 5, 56, 605, 17, null, null);
        introLabel[4] = generateLabel(
                "After that, click on [Generate Variables] button to associate names with your variables.", 5, 73, 605,
                17, null, null);
        introLabel[5] = generateLabel(
                "Your expressions will then be evaluated following these supported operators and functions:", 5, 90,
                605, 17, null, null);
        operatorLabel = generateLabel("Supported Operators", 5, 110, 605, 25, Color.BLUE, expressionTitleFont);
        functionLabel = generateLabel("Supported Functions", 5, 360, 605, 25, Color.BLUE, expressionTitleFont);

        operatorTable = new JTable(new SupportedDataModel(OPERATOR_NAMES, OPERATOR_REPRESENTATIONS, "Operator"));
        operatorTable.setEnabled(false);
        operatorTable.getTableHeader().setResizingAllowed(false);
        operatorTable.getTableHeader().setReorderingAllowed(false);
        functionTable = new JTable(new SupportedDataModel(FUNCTION_NAMES, FUNCTION_REPRESENTATIONS, "Function"));
        functionTable.setEnabled(false);
        functionTable.getTableHeader().setResizingAllowed(false);
        functionTable.getTableHeader().setReorderingAllowed(false);

        operatorScrollPane = new JScrollPane(operatorTable);
        operatorScrollPane.setMaximumSize(new Dimension(Integer.MAX_VALUE, 216));
        operatorScrollPane.setBounds(5, 140, 605, 216);
        operatorScrollPane.setBackground(Color.WHITE);
        operatorScrollPane.getViewport().setBackground(Color.WHITE);
        functionScrollPane = new JScrollPane(functionTable);
        functionScrollPane.setMaximumSize(new Dimension(Integer.MAX_VALUE, 355));
        functionScrollPane.setPreferredSize(new Dimension(500, 360));
        functionScrollPane.setBounds(5, 390, 605, 360);
        functionScrollPane.setBackground(Color.WHITE);
        functionScrollPane.getViewport().setBackground(Color.WHITE);
    }

    /**
     * Generates a {@link JLabel}, setting its text, bounds, foreground color and font
     * 
     * @param text The text to set
     * @param x The bounds x
     * @param y The bounds y
     * @param width The bounds width
     * @param height The bounds height
     * @param fg The foreground color to set
     * @param fontThe font to set
     * @return A {@link JLabel}
     */
    protected JLabel generateLabel(String text, int x, int y, int width, int height, Color fg, Font font) {
        JLabel label = new JLabel(text);
        label.setBounds(x, y, width, height);
        if (fg != null) {
            label.setForeground(fg);
        }
        if (font != null) {
            label.setFont(font);
        }
        return label;
    }

    protected void addComponents() {
        for (JLabel element : introLabel) {
            mainPanel.add(element);
        }
        mainPanel.add(operatorLabel);
        mainPanel.add(operatorScrollPane);
        mainPanel.add(functionLabel);
        mainPanel.add(functionScrollPane);
    }

    public void resetBounds() {
        pack();
        setSize(getWidth(), getHeight() + 20);// use a margin for height
        setLocationRelativeTo(getOwner());
        Rectangle bounds = getBounds();
        Rectangle maxBounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        if (!maxBounds.contains(bounds)) {
            this.setSize(bestDim.width + 30, 600);
            if (getParent() == null) {
                setLocationRelativeTo(getOwner());
            } else {
                int x = getParent().getX() + getParent().getWidth() - (bestDim.width + 50);
                if (x < 0) {
                    x = 0;
                }
                int y = getParent().getY() + getParent().getHeight() - (bestDim.height + 50);
                if (y < 0) {
                    y = 0;
                }
                this.setLocation(x, y);
            }
        }
    }

    protected void initBounds() {
        mainPanel.setPreferredSize(bestDim);
        this.setContentPane(new JScrollPane(mainPanel));
        resetBounds();
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible && firstInit) {
            firstInit = false;
            initBounds();
        }
        super.setVisible(visible);
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    /**
     * A {@link DefaultTableModel} used to describes some supported data
     */
    protected static class SupportedDataModel extends DefaultTableModel {

        private static final long serialVersionUID = -7191169463346153635L;

        /**
         * Represents the supported data names
         */
        private final String[] names;
        /**
         * Represents the supported data representations (the way to write them in formulas)
         */
        private final String[] representations;

        /**
         * Represents the way the data should be called. Example: "Function"
         */
        private final String dataNaming;

        /**
         * Constructor
         * 
         * @param names The table that contains the data names
         * @param representations The table that contains the data representations (the way to write
         *            them in formulas)
         * @param dataNaming The way the data should be called. Example: "Function"
         */
        public SupportedDataModel(String[] names, String[] representations, String dataNaming) {
            super();
            // make sure both arrays are not null and of same length
            String[] tmpNames = names;
            String[] tmpRepresentations = representations;
            if (tmpNames == null) {
                tmpNames = new String[0];
            }
            if (tmpRepresentations == null) {
                tmpRepresentations = new String[0];
            }
            if (tmpNames.length > tmpRepresentations.length) {
                tmpNames = Arrays.copyOf(tmpNames, tmpRepresentations.length);
            } else if (tmpRepresentations.length > tmpNames.length) {
                tmpRepresentations = Arrays.copyOf(tmpRepresentations, tmpNames.length);
            }
            this.names = tmpNames;
            this.representations = tmpRepresentations;
            this.dataNaming = dataNaming + " Name";
        }

        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public int getRowCount() {
            return (names == null ? 0 : names.length);
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object value = null;
            if (rowIndex > -1) {
                if (columnIndex == 0) {
                    if ((names != null) && (rowIndex < names.length)) {
                        value = names[rowIndex];
                    }
                } else if (columnIndex == 1) {
                    if (rowIndex < representations.length) {
                        value = representations[rowIndex];
                    }
                }
            }
            return value;
        }

        @Override
        public String getColumnName(int columnIndex) {
            String name;
            switch (columnIndex) {
                case 0:
                    name = dataNaming;
                    break;
                case 1:
                    name = "Representation";
                    break;
                default:
                    name = null;
            }
            return name;
        }
    }

}
