package fr.soleil.comete.swing.border;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.data.target.scalar.ITextComponent;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.border.util.ColorTargetMode;
import fr.soleil.comete.swing.border.util.TextTargetMode;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.border.ColoredLineTitledBorder;

/**
 * A {@link ColoredLineTitledBorder} that may receive text and color.
 * <p>
 * <table border=1 style="border-collapse: collapse;">
 * <tr>
 * <td>
 * <b>Use case example with CometeBox and TangoDataSource projects:</b>
 * </td>
 * </tr>
 * <tr>
 * <td>
 * 
 * <pre>
 * JFrame testFrame = new JFrame("CometeTitledBorder test");
 * testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
 * String device = "tango/tangotest/1";
 * // Create and setup CometeTitledBorder
 * CometeTitledBorder border = new CometeTitledBorder(device);
 * border.setTextTargetMode(TextTargetMode.TEXT_AS_LABEL_TOOLTIP);
 * border.setColorTargetMode(ColorTargetMode.COMETE_BACKGROUND_AS_LABEL_FOREGROUND);
 * border.setTitleColorAsLineColor(true);
 * // Create a JTextArea that will receive the CometeTitledBorder as border
 * JTextArea area = new JTextArea("this is my text", 50, 50) {
 * 
 *     private static final long serialVersionUID = 2661690548419665918L;
 * 
 *     // Override getToolTipText(MouseEvent) to ensure displaying the tooltip dedicated to the CometeTitledBorder,
 *     // because by default, in Java, a Border can't have its own tooltip.
 *     &#64;Override
 *     public String getToolTipText(MouseEvent e) {
 *         // Ask CometeTitledBorderToolTipManager to compute best tooltip.
 *         return CometeTitledBorderToolTipManager.getToolTipText(e, super.getToolTipText(e));
 *     }
 * 
 * };
 * // Set a new CometeTitledBorderToolTipManager() as PropertyChangeListener to manage dedicated border tooltip.
 * area.addPropertyChangeListener(new CometeTitledBorderToolTipManager());
 * // Then Set CometeTitledBorder as border
 * area.setBorder(border);
 * testFrame.setContentPane(area);
 * testFrame.pack();
 * testFrame.setLocationRelativeTo(null);
 * testFrame.setVisible(true);
 * // Connect CometeTitledBorder to sources
 * StringScalarBox box = new StringScalarBox();
 * box.setErrorText(border, "UNKNOWN");
 * box.setErrorColor(border, StateAdapter.UNKNOWN);
 * box.setUserEnabled(border, true);
 * TangoKey key = new TangoKey();
 * TangoKeyTool.registerAttribute(key, device, TangoAttributeHelper.STATE);
 * box.connectWidget(border, key);
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class CometeTitledBorder extends ColoredLineTitledBorder implements ITextComponent {

    private static final long serialVersionUID = 6824555601720126442L;

    protected TextTargetMode textTargetMode;
    protected ColorTargetMode colorTargetMode;
    protected final TargetDelegate delegate;
    protected final ErrorNotificationDelegate errorNotificationDelegate;

    public CometeTitledBorder() {
        this(ObjectUtils.EMPTY_STRING, LEADING, DEFAULT_POSITION, null, null);
    }

    public CometeTitledBorder(String title) {
        this(title, LEADING, DEFAULT_POSITION, null, null);
    }

    public CometeTitledBorder(String title, int titleJustification, int titlePosition) {
        this(title, titleJustification, titlePosition, null, null);
    }

    public CometeTitledBorder(String title, int titleJustification, int titlePosition, Font titleFont) {
        this(title, titleJustification, titlePosition, titleFont, null);
    }

    public CometeTitledBorder(String title, int titleJustification, int titlePosition, Font titleFont,
            Color titleColor) {
        super(title, titleJustification, titlePosition, titleFont, titleColor);
        textTargetMode = TextTargetMode.TEXT_AS_LABEL_TEXT_AND_TOOLTIP_AS_LABEL_TOOLTIP;
        colorTargetMode = ColorTargetMode.COMETE_FOREGROUND_AS_LABEL_FOREGROUND_AND_COMETE_BACKGROUND_AS_LABEL_BACKGROUND;
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(label) {
            @Override
            public Component getSource() {
                return getLastComponent();
            }
        };
    }

    /**
     * Returns how this {@link CometeTitledBorder} will manage received text and tooltip.
     * 
     * @return A {@link TextTargetMode}.
     */
    public TextTargetMode getTextTargetMode() {
        return textTargetMode;
    }

    /**
     * Sets how this {@link CometeTitledBorder} should manage received text and tooltip.
     * 
     * @param textTargetMode The {@link TextTargetMode} that represents how this {@link CometeTitledBorder} should
     *            manage received text and tooltip.
     */
    public void setTextTargetMode(TextTargetMode textTargetMode) {
        this.textTargetMode = textTargetMode == null ? TextTargetMode.NO_TEXT_TARGET : textTargetMode;
    }

    /**
     * Returns how this {@link CometeTitledBorder} will manage received comete foreground and background.
     * 
     * @return A {@link ColorTargetMode}.
     */
    public ColorTargetMode getColorTargetMode() {
        return colorTargetMode;
    }

    /**
     * Sets how this {@link CometeTitledBorder} should manage received comete foreground and background.
     * 
     * @param colorTargetMode The {@link ColorTargetMode} that represents how this {@link CometeTitledBorder} should
     *            manage received comete foreground and background.
     */
    public void setColorTargetMode(ColorTargetMode colorTargetMode) {
        this.colorTargetMode = colorTargetMode == null ? ColorTargetMode.NO_COLOR_TARGET : colorTargetMode;
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean hasFocus() {
        // not managed
        return false;
    }

    @Override
    public void setOpaque(boolean opaque) {
        label.setOpaque(opaque);
    }

    @Override
    public boolean isOpaque() {
        return label.isOpaque();
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        ColorTargetMode mode = getColorTargetMode();
        switch (mode) {
            case COMETE_FOREGROUND_AS_LABEL_FOREGROUND_AND_COMETE_BACKGROUND_AS_LABEL_BACKGROUND:
            case COMETE_BACKGROUND_AS_LABEL_BACKGROUND:
                setLabelBackground(ColorTool.getColor(color));
                break;
            case COMETE_FOREGROUND_AS_LABEL_BACKGROUND_AND_COMETE_BACKGROUND_AS_LABEL_FOREGROUND:
            case COMETE_BACKGROUND_AS_LABEL_FOREGROUND:
                setTitleColor(ColorTool.getColor(color));
                break;
            default:
                // nothing to do
                break;
        }
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(label.getBackground());
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        ColorTargetMode mode = getColorTargetMode();
        switch (mode) {
            case COMETE_FOREGROUND_AS_LABEL_FOREGROUND_AND_COMETE_BACKGROUND_AS_LABEL_BACKGROUND:
            case COMETE_FOREGROUND_AS_LABEL_FOREGROUND:
                setTitleColor(ColorTool.getColor(color));
                break;
            case COMETE_FOREGROUND_AS_LABEL_BACKGROUND_AND_COMETE_BACKGROUND_AS_LABEL_FOREGROUND:
            case COMETE_FOREGROUND_AS_LABEL_BACKGROUND:
                setLabelBackground(ColorTool.getColor(color));
                break;
            default:
                // nothing to do
                break;
        }
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(getTitleColor());
    }

    @Override
    public void setEnabled(boolean enabled) {
        // not managed
    }

    @Override
    public boolean isEnabled() {
        // not managed
        return true;
    }

    @Override
    public void setCometeFont(CometeFont font) {
        setTitleFont(FontTool.getFont(font));
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(getTitleFont());
    }

    @Override
    public void setToolTipText(String text) {
        TextTargetMode mode = getTextTargetMode();
        if (mode == null) {
            mode = TextTargetMode.NO_TEXT_TARGET;
        }
        switch (mode) {
            case TOOLTIP_AS_LABEL_TEXT:
            case TEXT_AS_LABEL_TOOLTIP_AND_TOOLTIP_AS_LABEL_TEXT:
                setTitle(text);
                break;
            case TOOLTIP_AS_LABEL_TOOLTIP:
            case TEXT_AS_LABEL_TEXT_AND_TOOLTIP_AS_LABEL_TOOLTIP:
                setTitleToolTip(text);
                break;
            default:
                // do nothing
                break;
        }
    }

    @Override
    public String getToolTipText() {
        TextTargetMode mode = getTextTargetMode();
        if (mode == null) {
            mode = TextTargetMode.NO_TEXT_TARGET;
        }
        String tooltip;
        switch (mode) {
            case TOOLTIP_AS_LABEL_TEXT:
            case TEXT_AS_LABEL_TOOLTIP_AND_TOOLTIP_AS_LABEL_TEXT:
                tooltip = getTitle();
                break;
            case TOOLTIP_AS_LABEL_TOOLTIP:
            case TEXT_AS_LABEL_TEXT_AND_TOOLTIP_AS_LABEL_TOOLTIP:
                tooltip = getTitleToolTip();
                break;
            default:
                tooltip = null;
                break;
        }
        return tooltip;
    }

    @Override
    public void setVisible(boolean visible) {
        // not managed
    }

    @Override
    public boolean isVisible() {
        // not managed
        return false;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // not managed
    }

    @Override
    public int getHorizontalAlignment() {
        // not managed
        return 0;
    }

    @Override
    public void setSize(int width, int height) {
        // not managed
    }

    @Override
    public void setPreferredSize(int width, int height) {
        // not managed
    }

    @Override
    public int getWidth() {
        // not managed
        return 0;
    }

    @Override
    public int getHeight() {
        // not managed
        return 0;
    }

    @Override
    public void setLocation(int x, int y) {
        // not managed
    }

    @Override
    public int getX() {
        // not managed
        return 0;
    }

    @Override
    public int getY() {
        // not managed
        return 0;
    }

    @Override
    public boolean isEditingData() {
        // not managed
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        setTitle(title);
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        // not managed
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        // not managed
    }

    @Override
    public void removeAllMouseListeners() {
        // not managed
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        // not managed
        return false;
    }

    @Override
    public String getText() {
        return getTitle();
    }

    @Override
    public void setText(String text) {
        TextTargetMode mode = getTextTargetMode();
        if (mode == null) {
            mode = TextTargetMode.NO_TEXT_TARGET;
        }
        switch (mode) {
            case TEXT_AS_LABEL_TEXT:
            case TEXT_AS_LABEL_TEXT_AND_TOOLTIP_AS_LABEL_TOOLTIP:
                setTitle(text);
                break;
            case TEXT_AS_LABEL_TOOLTIP:
            case TEXT_AS_LABEL_TOOLTIP_AND_TOOLTIP_AS_LABEL_TEXT:
                label.setToolTipText(text);
                break;
            default:
                // do nothing
                break;
        }
    }

}
