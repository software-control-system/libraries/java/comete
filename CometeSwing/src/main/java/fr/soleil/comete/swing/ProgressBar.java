/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JProgressBar;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.listener.IProgressBarListener;
import fr.soleil.comete.definition.widget.IProgressBar;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;

public class ProgressBar extends JProgressBar implements IProgressBar, ChangeListener {

    private static final long serialVersionUID = 3309579046879472479L;

    private final List<IProgressBarListener> progressBarListeners;
    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;
    private String format;

    public ProgressBar() {
        super();
        progressBarListeners = new ArrayList<>();
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        format = null;
        super.setStringPainted(true);
        addChangeListener(this);
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void setValue(int n) {
        int oldValue = getValue();
        if (oldValue != n) {
            super.setValue(n);
            fireValueChanged(new EventObject(this));
        }
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(CometeFont font) {
        super.setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        super.setForeground(ColorTool.getColor(color));
    }

    @Override
    public int getHorizontalAlignment() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // TODO Auto-generated method stub

    }

    @Override
    public void stateChanged(ChangeEvent e) {
        fireValueChanged(new EventObject(this));
    }

    @Override
    public void addProgressBarListener(final IProgressBarListener listener) {
        if (!progressBarListeners.contains(listener)) {
            progressBarListeners.add(listener);
        }
    }

    @Override
    public void removeProgressBarListener(final IProgressBarListener listener) {
        if (progressBarListeners.contains(listener)) {
            synchronized (progressBarListeners) {
                progressBarListeners.remove(listener);
            }
        }
    }

    @Override
    public void fireValueChanged(EventObject event) {
        ListIterator<IProgressBarListener> iterator = progressBarListeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().valueChanged(event);
        }
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to
     *            {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public Number getNumberValue() {
        return super.getValue();
    }

    @Override
    public void setNumberValue(Number value) {
        super.setValue(value.intValue());
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public void setFormat(String format) {
        if (!ObjectUtils.sameObject(format, this.format)) {
            this.format = format;
            repaint();
        }
    }

    @Override
    public String getString() {
        String value;
        if ((format == null) || (!Format.isNumberFormatOK(format))) {
            value = super.getString();
        } else {
            StringBuilder builder = new StringBuilder();
            builder.append(Format.formatValue(getPercentComplete() * 100, format));
            builder.append(" %");
            value = builder.toString();
        }
        return value;
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
