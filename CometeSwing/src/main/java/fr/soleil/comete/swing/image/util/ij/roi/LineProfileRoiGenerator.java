/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Stroke;
import java.util.HashMap;
import java.util.Map;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.chart.axis.view.VerticalAxisView;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.image.ijviewer.components.ArrowStroke;
import fr.soleil.comete.swing.image.ijviewer.components.LineProfileDialog;
import fr.soleil.comete.swing.image.util.ProfileDataCalculator;
import fr.soleil.lib.project.swing.icons.Icons;
import ij.gui.Line;
import ij.gui.Roi;
import ij.gui.Toolbar;

public class LineProfileRoiGenerator extends DataRoiGenerator {

    protected static final Cursor CURSOR = Icons.generateCursorFromIcon(ImageViewer.LINE_PROFILE_MODE2_ICON,
            ImageViewer.MODE_LINE_PROFILE_ACTION);
    protected static final Stroke STROKE = new ArrowStroke(IJCanvas.DEFAULT_LINE_PROFILE_ROI_STROKE, ArrowStroke.END,
            12, Math.PI / 5);
    protected static final Stroke SELECTED_STROKE = new ArrowStroke(IJCanvas.DEFAULT_SELECTED_ROI_STROKE,
            ArrowStroke.END, 12, Math.PI / 5);

    public LineProfileRoiGenerator(String text, String description, ImageViewer viewer) {
        super(text, description, ImageViewer.LINE_PROFILE_MODE_ICON, viewer);
    }

    @Override
    public boolean isColorScaleDependent() {
        return true;
    }

    @Override
    public int getRoiMode() {
        return IJRoiManager.LINE_PROFILE_ROI_MODE;
    }

    @Override
    public Roi createNewRoi(int sx, int sy, IJRoiManager roiManager) {
        Line roi = new Line(sx, sy, roiManager);
        roi.setName(ImageViewer.LINE_PROFILE_NAME);
        return roi;
    }

    @Override
    public boolean isCompatibleWithRoi(Roi roi) {
        return ((roi instanceof Line) && ImageViewer.LINE_PROFILE_NAME.equals(roi.getName()));
    }

    @Override
    public Cursor getCursor() {
        return CURSOR;
    }

    @Override
    public String getActionCommand() {
        return ImageViewer.MODE_LINE_PROFILE_ACTION;
    }

    @Override
    protected Stroke generateRoiStroke() {
        return STROKE;
    }

    @Override
    protected Stroke generateSelectedRoiStroke() {
        return SELECTED_STROKE;
    }

    @Override
    protected Color generateRoiColor() {
        return IJCanvas.DEFAULT_LINE_PROFILE_ROI_COLOR;
    }

    @Override
    protected int getTool() {
        return Toolbar.LINE;
    }

    @Override
    protected boolean isDialogVisible(ImageViewer viewer) {
        LineProfileDialog dialog = viewer.getLineProfilerDialog();
        return (dialog != null) && dialog.isVisible();
    }

    @Override
    protected void doShowDataDialog(ImageViewer viewer) {
        if (viewer != null) {
            boolean updateBounds = (viewer.getLineProfiler(false) == null);
            LineProfileDialog dialog = viewer.getLineProfilerDialog();
            checkWindowAdapters(dialog, viewer);
            if (updateBounds) {
                updateDataBounds(viewer);
            }
            if ((dialog != null) && (!dialog.isVisible())) {
                dialog.setLocationRelativeTo(viewer);
                dialog.setVisible(true);
            }
            dialog.toFront();
        }
    }

    @Override
    protected void doHideDataDialog(ImageViewer viewer) {
        if (viewer != null) {
            LineProfileDialog dialog = viewer.getLineProfilerDialog();
            if (dialog != null) {
                dialog.setVisible(false);
            }
        }
    }

    @Override
    protected void updateDataBounds(ImageViewer viewer) {
        if (viewer != null) {
            Chart profiler = viewer.getLineProfiler(false);
            if (profiler != null) {
                VerticalAxisView axis = viewer.getGradientViewer().getAxis();
                profiler.setAxisMinimum(ChartUtils.computeLowTen(axis.getAttributes().getMinimum()), IChartViewer.Y1);
                double max = ChartUtils.computeLowTen(axis.getAttributes().getMaximum());
                if (max < axis.getAttributes().getMaximum()) {
                    int div = (int) Math.ceil(axis.getAttributes().getMaximum() / max);
                    max *= div;
                }
                profiler.setAxisMaximum(max, IChartViewer.Y1);
            }
        }
    }

    @Override
    protected void updateData(IMaskedImageViewer context) {
        ImageViewer viewer;
        if (context instanceof ImageViewer) {
            viewer = (ImageViewer) context;
        } else {
            viewer = null;
        }
        if (viewer != null) {
            Chart profiler = viewer.getLineProfiler(false);
            if (profiler != null) {
                Line line = (Line) viewer.getRoiManager().getSpecialRoi(getReferentRoiGenerator());
                if (line == null) {
                    profiler.setData(null);
                } else {
                    double[] pixels = ProfileDataCalculator.buildProfileData(viewer.getDimX(), viewer.getDimY(),
                            viewer.getValue(), line.x1, line.y1, line.x2, line.y2);
                    if (pixels == null) {
                        profiler.setData(null);
                    } else {
                        double[] indexes = new double[pixels.length];
                        for (int i = 0; i < indexes.length; i++) {
                            indexes[i] = i * viewer.getPixelSize();
                        }
                        profiler.setAxisMinimum(indexes[0], IChartViewer.X);
                        profiler.setAxisMaximum(indexes[indexes.length - 1], IChartViewer.X);
                        Map<String, Object> dataMap = new HashMap<String, Object>();
                        dataMap.put(ImageViewer.LINE_PROFILE_NAME, new double[][] { indexes, pixels });
                        profiler.setData(dataMap);
                    }
                }
            }
        }
    }

}
