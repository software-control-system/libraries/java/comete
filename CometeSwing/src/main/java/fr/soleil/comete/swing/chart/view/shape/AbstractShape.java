/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.shape;

import java.awt.Color;
import java.awt.Graphics2D;

import fr.soleil.comete.definition.widget.util.IChartConst.ShapeType;

/**
 * Class that describes a shape and is able to draw it
 * 
 * @author huriez
 */
public abstract class AbstractShape {

    private final ShapeType type;
    protected Color color;
    protected int x;
    protected int y;

    public AbstractShape(ShapeType type) {
        this.type = type;
        x = 0;
        y = 0;
    }

    /**
     * @return the type
     */
    public ShapeType getType() {
        return type;
    }

    public abstract void drawShape(Graphics2D g);

    /**
     * Returns the x coordinate of the shape
     * 
     * @return An <code>int</code>
     */
    public int getX() {
        return x;
    }

    /**
     * Sets the x coordinate of the shape
     * 
     * @param x the coordinate to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Returns the y coordinate of the shape
     * 
     * @return An <code>int</code>
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the y coordinate of the shape
     * 
     * @param y the coordinate to set
     */
    public void setY(int y) {
        this.y = y;
    }

}
