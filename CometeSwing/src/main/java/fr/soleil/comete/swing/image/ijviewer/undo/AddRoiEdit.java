/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.undo;

import java.util.ResourceBundle;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import ij.gui.Roi;

/**
 * 
 * 
 * @author MAINGUY
 * 
 */
public class AddRoiEdit extends AbstractUndoableEdit {

    private static final long serialVersionUID = -5350446673301156392L;

    protected static final ResourceBundle MESSAGES = ResourceBundle
            .getBundle("fr.soleil.comete.swing.image.ijviewer.messages");

    protected IJRoiManager roiManager;
    protected Roi roiAdded;

    /**
     * 
     */
    public AddRoiEdit(IJRoiManager aRoiManager, Roi aRoi) {
        roiManager = aRoiManager;
        roiAdded = aRoi;
    }

    /* (non-Javadoc)
     * @see javax.swing.undo.AbstractUndoableEdit#undo()
     */
    @Override
    public void undo() throws CannotUndoException {
        super.undo();

        roiManager.deleteRoi(roiAdded, true);
    }

    /* (non-Javadoc)
     * @see javax.swing.undo.AbstractUndoableEdit#redo()
     */
    @Override
    public void redo() throws CannotRedoException {
        super.redo();

        roiManager.addRoi(roiAdded, true);
    }

    /* (non-Javadoc)
     * @see javax.swing.undo.AbstractUndoableEdit#getPresentationName()
     */
    @Override
    public String getPresentationName() {
        return MESSAGES.getString("ImageJViewer.UndoManagement.AddRoi")/*+" "+roiAdded.getName()*/;
    }

}
