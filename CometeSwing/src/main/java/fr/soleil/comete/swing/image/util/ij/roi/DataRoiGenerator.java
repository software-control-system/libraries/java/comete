/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Cursor;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;

import javax.swing.ImageIcon;

import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.image.ijviewer.events.IRoiManagerListener;
import fr.soleil.comete.swing.image.ijviewer.events.RoiEvent;
import fr.soleil.lib.project.ObjectUtils;
import ij.gui.Roi;

public abstract class DataRoiGenerator extends AbstractRoiGenerator implements IRoiManagerListener {

    private final WeakReference<ImageViewer> reference;
    private final Map<ImageViewer, Roi> roiMap;
    private volatile boolean dataCleaned;
    private final WeakHashMap<ImageViewer, WindowAdapter> windowAdapters;

    public DataRoiGenerator(String text, String description, ImageIcon icon, ImageViewer viewer) {
        super(text, description, icon);
        this.reference = (viewer == null ? null : new WeakReference<>(viewer));
        roiMap = new WeakHashMap<>();
        windowAdapters = new WeakHashMap<>();
        dataCleaned = true;
    }

    protected ImageViewer getImageViewer() {
        return ObjectUtils.recoverObject(reference);
    }

    @Override
    public void roiAdded(Roi roi, IJRoiManager roiManager) {
        super.roiAdded(roi, roiManager);
        if (roiManager != null) {
            roiManager.addRoiManagerListener(this);
            updateData(roiManager.getImageViewer());
        }
    }

    @Override
    public void roiRemoved(Roi roi, IJRoiManager roiManager) {
        super.roiRemoved(roi, roiManager);
        if (roiManager == null) {
            updateData(null);
        } else {
            updateData(roiManager.getImageViewer());
            roiManager.removeRoiManagerListener(this);
        }
    }

    @Override
    public boolean isDataDependent() {
        return true;
    }

    @Override
    public void dataChanged(ImageViewer viewer) {
        update(viewer);
    }

    @Override
    public void axisChanged(ImageViewer viewer) {
        update(viewer);
    }

    @Override
    public void colorScaleChanged(ImageViewer viewer) {
        update(viewer);
    }

    @Override
    public void pixelSizeChanged(ImageViewer viewer) {
        update(viewer);
    }

    @Override
    public void zoomChanged(ImageViewer viewer) {
        update(viewer);
    }

    protected void update(ImageViewer viewer) {
        updateDataBounds(viewer);
        updateData(viewer);
    }

    @Override
    public boolean needsNoHandledRoi() {
        return false;
    }

    @Override
    public boolean isSilentRoi() {
        return false;
    }

    @Override
    public int getRoiModeMenu() {
        return ImageViewer.TYPE_POPUP_MENU;
    }

    @Override
    public int getPreferredPositionInMenu() {
        return 7;
    }

    @Override
    public Cursor getCursor() {
        return null;
    }

    @Override
    protected void treatData(ActionEvent e, ImageViewer viewer) {
        if (viewer == null) {
            if (e != null) {
                viewer = getImageViewer();
                applyCursor(viewer);
            }
        }
        super.treatData(e, viewer);
        dataCleaned = false;
        setDataDialogVisible(viewer, true);
        updateData(viewer);
    }

    @Override
    protected void cleanData(ImageViewer viewer) {
        super.cleanData(viewer);
        dataCleaned = true;
        setDataDialogVisible(viewer, false);
        updateData(viewer);
    }

    @Override
    protected boolean isTreatDataOnDifferentMode(ImageViewer viewer) {
        return isDialogVisible(viewer);
    }

    @Override
    protected boolean isCleanDataOnDifferentMode(ImageViewer viewer) {
        return (!dataCleaned) && (!isDialogVisible(viewer));
    }

    protected WindowAdapter generateDataCleaner(final ImageViewer viewer) {
        return new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (IJRoiManager.getRoiMode() != getRoiMode()) {
                    cleanData(viewer);
                }
            }
        };
    }

    protected void checkWindowAdapters(Window window, ImageViewer viewer) {
        WindowAdapter adapter = windowAdapters.get(viewer);
        if (adapter == null) {
            adapter = generateDataCleaner(viewer);
            windowAdapters.put(viewer, adapter);
            window.addWindowListener(adapter);
        }
    }

    protected final void setDataDialogVisible(ImageViewer viewer, boolean visible) {
        if (viewer != null) {
            if (visible) {
                Roi lastRoi = roiMap.get(viewer);
                if (lastRoi != null) {
                    viewer.getRoiManager().setSpecialRoi(getReferentRoiGenerator(), lastRoi);
                    viewer.getRoiManager().setHandledRoi(lastRoi);
                    viewer.repaint();
                } else if (!roiMap.containsKey(viewer)) {
                    roiMap.put(viewer, null);
                }
                doShowDataDialog(viewer);
            } else {
                viewer.getImagePanel().removeFocusListener(this);
                Roi deleted = viewer.getRoiManager().deleteSpecialRoi(getReferentRoiGenerator());
                if (deleted == null) {
                    RoiGenerator tmp = null;
                    for (RoiGenerator generator : viewer.getRoiManager().getRoiGenerators()) {
                        if ((generator != null) && (generator != getReferentRoiGenerator()) && ObjectUtils.sameObject(
                                generator.getActionCommand(), getReferentRoiGenerator().getActionCommand())) {
                            tmp = generator;
                            break;
                        }
                    }
                    if (tmp != null) {
                        deleted = viewer.getRoiManager().deleteSpecialRoi(tmp);
                    }
                }
                roiMap.put(viewer, deleted);
                doHideDataDialog(viewer);
                viewer.repaint();
            }
        }

    }

    protected String toString(Object o) {
        String result;
        if (o == null) {
            result = null;
        } else {
            result = o.getClass().getSimpleName() + "@" + o.hashCode();
        }
        return result;
    }

    protected abstract boolean isDialogVisible(ImageViewer viewer);

    protected abstract void doShowDataDialog(ImageViewer viewer);

    protected abstract void doHideDataDialog(ImageViewer viewer);

    protected abstract void updateDataBounds(ImageViewer viewer);

    protected abstract void updateData(IMaskedImageViewer viewer);

    @Override
    public void roiAdded(RoiEvent event) {
        // Nothing to do by default
    }

    @Override
    public void roiChanged(RoiEvent event) {
        if (isMyRoi(event)) {
            updateData(((IJRoiManager) event.getSource()).getImageViewer());
        }
    }

    @Override
    public void roiDeleted(RoiEvent event) {
        // Nothing to do by default
    }

    @Override
    public boolean keyPressed(KeyEvent e) {
        boolean treated = super.keyPressed(e);
        if (e != null) {
            ImageViewer viewer = recoverViewer(e.getComponent());
            if (viewer != null) {
                Roi roi = viewer.getRoiManager().getSpecialRoi(getReferentRoiGenerator());
                if (roi != null) {
                    switch (e.getKeyCode()) {
                        case KeyEvent.VK_UP:
                        case KeyEvent.VK_DOWN:
                        case KeyEvent.VK_LEFT:
                        case KeyEvent.VK_RIGHT:
                            roi.nudge(e.getKeyCode());
                            treated = true;
                            break;
                        case KeyEvent.VK_DELETE:
                        case KeyEvent.VK_BACK_SPACE:
                            viewer.getRoiManager().deleteRoi(roi);
                            treated = true;
                            break;
                    }
                }
                if (treated) {
                    updateData(viewer);
                }
            }
        }
        return treated;
    }

    @Override
    public boolean keyReleased(KeyEvent e) {
        boolean treated = super.keyReleased(e);
        if (e != null) {
            ImageViewer viewer = recoverViewer(e.getComponent());
            if (viewer != null) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_UP:
                    case KeyEvent.VK_DOWN:
                    case KeyEvent.VK_LEFT:
                    case KeyEvent.VK_RIGHT:
                    case KeyEvent.VK_DELETE:
                    case KeyEvent.VK_BACK_SPACE:
                        treated = true;
                        break;
                }
            }
        }
        return treated;
    }

    @Override
    public boolean keyTyped(KeyEvent e) {
        return keyReleased(e);
    }

}
