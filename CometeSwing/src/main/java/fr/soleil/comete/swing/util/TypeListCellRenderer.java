/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 * A {@link DefaultListCellRenderer} that is able knows some type represented by an id.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class TypeListCellRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = -8879097552581930945L;

    public TypeListCellRenderer() {
        super();
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
            boolean cellHasFocus) {
        JLabel comp = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        int type;
        if (value instanceof Integer) {
            type = ((Integer) value).intValue();
        } else {
            type = index;
        }
        applyType(type, comp);
        return comp;
    }

    /**
     * Applies the type representation to given component.
     * 
     * @param type The type.
     * @param comp The component.
     */
    protected void applyType(int type, JLabel comp) {
        comp.setText(getText(type));
        comp.setToolTipText(getToolTipText(type, getDefaultToolTipText(comp)));
    }

    /**
     * Given an id, returns a text that represents it.
     * 
     * @param id The id.
     * @return A {@link String}: the expected text.
     */
    protected abstract String getText(int id);

    /**
     * Returns the default tooltip text to use with given component.
     * 
     * @param comp the component.
     * @return A {@link String}.
     */
    protected String getDefaultToolTipText(JLabel comp) {
        return comp.getText();
    }

    /**
     * Given an id, returns a tooltip text that describes it.
     * 
     * @param id The id.
     * @param defaultTooltip The default tooltip to return if id is unknown.
     * @return A {@link String}.
     */
    protected String getToolTipText(int id, String defaultTooltip) {
        return defaultTooltip;
    }

}
