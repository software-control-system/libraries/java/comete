/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.axis.model;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.util.IChartConst.Orientation;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.awt.ColorUtils;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.date.IDateFormattable;

/**
 * Class that stores necessary information to calculate and draw an axis
 * 
 * @author huriez
 */
public class AxisAttributes implements IDateFormattable {

    private static final Font DEFAULT_LABEL_FONT = new Font("Dialog", Font.PLAIN, 11);

    private double maximum;
    private double minimum;

    private double labelInterval;

    private Font labelFont;
    private int labelFormat;
    private String dateFormat;
    private String timeFormat;
    private String valueFormat;

    private Orientation orientation;
    private int origin;
    private int scale;

    private String title;
    private int titleAlignment;

    private boolean autoScale;
    private boolean inverted;
    private boolean autoLabeling;
    private boolean subTickVisible;
    private boolean drawOpposite;
    private boolean gridVisible;
    private boolean subGridVisible;
    private boolean zeroAlwaysVisible;
    private boolean clipping;

    private BasicStroke gridStroke;
    private int gridStyle;
    private Color subGridColor;
    private Color labelColor;

    private String[] userLabel;
    private double[] userLabelPos;

    private double axisDuration;
    private boolean fitXAxisToDisplayDuration;
    private double percentScrollback;

    private int tickLength;
    private int subtickLength;
    private double minTickStep;

    public AxisAttributes(int origin, Color bgColor) {
        title = null;
        titleAlignment = IComponent.CENTER;
        minimum = 0;
        maximum = 100;
        labelInterval = Double.NaN;
        drawOpposite = true;
        subGridVisible = false;
        gridVisible = false;
        autoLabeling = true;
        subTickVisible = true;
        clipping = true;
        this.origin = origin;
        orientation = AxisAttributes.getOrientationFromOrigin(origin);
        scale = IChartViewer.LINEAR_SCALE;
        labelFormat = IChartViewer.AUTO_FORMAT;
        dateFormat = IDateConstants.DATE_LONG_FORMAT;
        timeFormat = IDateConstants.TIME_SHORT_FORMAT;
        valueFormat = null;
        labelFont = DEFAULT_LABEL_FONT;
        setGridStyle(IChartViewer.STYLE_DOT);
        setLabelColor(Color.black, bgColor);
        zeroAlwaysVisible = false;
        autoScale = true;
        inverted = (orientation != Orientation.HORIZONTAL);
        userLabel = null;
        userLabelPos = null;
        axisDuration = Double.POSITIVE_INFINITY;
        fitXAxisToDisplayDuration = true;
        percentScrollback = 0;
        setTickLength(4);
        minTickStep = 50.0;
    }

    public AxisAttributes(AxisAttributes attributes) {
        if (attributes.labelFont == null) {
            labelFont = DEFAULT_LABEL_FONT;
        } else {
            labelFont = new Font(attributes.labelFont.getAttributes());
        }
        if (attributes.title != null) {
            title = new String(attributes.title);
        }
        scale = attributes.scale;
        inverted = attributes.inverted;
        gridVisible = attributes.gridVisible;
        autoLabeling = attributes.autoLabeling;
        subTickVisible = attributes.subTickVisible;
        labelFormat = attributes.labelFormat;
        origin = attributes.origin;
        orientation = attributes.orientation;
        minimum = attributes.minimum;
        maximum = attributes.maximum;
        if (title != null) {
            title = new String(attributes.title);
        }
        inverted = attributes.inverted;
        zeroAlwaysVisible = attributes.zeroAlwaysVisible;
        autoScale = attributes.autoScale;
        axisDuration = attributes.axisDuration;
        fitXAxisToDisplayDuration = attributes.fitXAxisToDisplayDuration;
        percentScrollback = attributes.percentScrollback;
        tickLength = attributes.tickLength;
        subtickLength = attributes.subtickLength;
    }

    /**
     * Changes the axis origin
     * 
     * @param origin the origin to set
     */
    public void setOrigin(int origin) {
        if (AxisAttributes.getOrientationFromOrigin(origin) == orientation) {
            this.origin = origin;
        }
    }

    public static Orientation getOrientationFromOrigin(int origin) {
        Orientation orientation;
        switch (origin) {
            case IChartViewer.HORIZONTAL_DOWN:
            case IChartViewer.HORIZONTAL_ORGY1:
            case IChartViewer.HORIZONTAL_ORGY2:
            case IChartViewer.HORIZONTAL_UP:
                orientation = Orientation.HORIZONTAL;
                break;
            case IChartViewer.VERTICAL_ORGX:
            case IChartViewer.VERTICAL_RIGHT:
            case IChartViewer.VERTICAL_LEFT:
                orientation = Orientation.VERTICAL;
                break;
            default:
                orientation = null;
                break;
        }
        return orientation;
    }

    /**
     * @return the maximum
     */
    public double getMaximum() {
        return maximum;
    }

    /**
     * Changes the maximum
     * 
     * @param maximum the maximum to set
     */
    public void setMaximum(double maximum) {
        this.maximum = maximum;
    }

    /**
     * @return the minimum
     */
    public double getMinimum() {
        return minimum;
    }

    /**
     * Changes the minimum
     * 
     * @param minimum the minimum to set
     */
    public void setMinimum(double minimum) {
        this.minimum = minimum;
    }

    /**
     * @return the labelInterval
     */
    public double getLabelInterval() {
        return labelInterval;
    }

    /**
     * Changes the label interval
     * 
     * @param labelInterval the labelInterval to set
     */
    public void setLabelInterval(double labelInterval) {
        this.labelInterval = labelInterval;
    }

    /**
     * @return the labelFont
     */
    public Font getLabelFont() {
        return labelFont;
    }

    /**
     * Changes the label {@link Font}
     * 
     * @param labelFont the label {@link Font} to set
     */
    public void setLabelFont(Font labelFont) {
        this.labelFont = labelFont == null ? DEFAULT_LABEL_FONT : labelFont;
    }

    /**
     * @return the labelFormat
     */
    public int getLabelFormat() {
        return labelFormat;
    }

    /**
     * Changes the label format
     * 
     * @param labelFormat the label format to set
     */
    public void setLabelFormat(int labelFormat) {
        this.labelFormat = labelFormat;
    }

    /**
     * @return the orientation
     */
    public Orientation getOrientation() {
        return orientation;
    }

    /**
     * Changes the {@link Orientation}
     * 
     * @param orientation the {@link Orientation} to set
     */
    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    /**
     * @return the scale
     */
    public int getScale() {
        return scale;
    }

    /**
     * Changes the scale
     * 
     * @param scale the scale to set
     */
    public void setScale(int scale) {
        this.scale = scale;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Changes the title
     * 
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return whether zero should always be visible
     */
    public boolean isZeroAlwaysVisible() {
        return zeroAlwaysVisible;
    }

    /**
     * Sets whether zero should always be visible
     * 
     * @param zeroAlwaysVisible whether zero should always be visible
     */
    public void setZeroAlwaysVisible(boolean zeroAlwaysVisible) {
        this.zeroAlwaysVisible = zeroAlwaysVisible;
    }

    /**
     * @return the autoScale
     */
    public boolean isAutoScale() {
        return autoScale;
    }

    /**
     * Sets whether to use auto scale
     * 
     * @param autoScale whether to use auto scale
     */
    public void setAutoScale(boolean autoScale) {
        this.autoScale = autoScale;
        if (autoScale) {
            clearUserLabels();
        }
    }

    /**
     * @return the inverted
     */
    public boolean isInverted() {
        return inverted;
    }

    /**
     * Sets whether axis is inverted
     * 
     * @param inverted whether axis is inverted
     */
    public void setInverted(boolean inverted) {
        this.inverted = inverted;
    }

    /**
     * @return the drawOpposite
     */
    public boolean isDrawOpposite() {
        return drawOpposite;
    }

    /**
     * Sets whether to draw opposite axis
     * 
     * @param drawOpposite whether to draw opposite axis
     */
    public void setDrawOpposite(boolean drawOpposite) {
        this.drawOpposite = drawOpposite;
    }

    /**
     * @return the gridVisible
     */
    public boolean isGridVisible() {
        return gridVisible;
    }

    /**
     * Sets whether grid should be visible
     * 
     * @param gridVisible whether grid should be visible
     */
    public void setGridVisible(boolean gridVisible) {
        this.gridVisible = gridVisible;
    }

    /**
     * @return the subGridVisible
     */
    public boolean isSubGridVisible() {
        return subGridVisible;
    }

    /**
     * Sets whether subgrid should be visible
     * 
     * @param subGridVisible whether subgrid should be visible
     */
    public void setSubGridVisible(boolean subGridVisible) {
        this.subGridVisible = subGridVisible;
    }

    /**
     * @return the gridStroke
     */
    public BasicStroke getGridStroke() {
        return gridStroke;
    }

    /**
     * @return the gridStyle
     */
    public int getGridStyle() {
        return gridStyle;
    }

    /**
     * Changes the grid style
     * 
     * @param gridStyle the grid style to set
     */
    public void setGridStyle(int gridStyle) {
        this.gridStyle = gridStyle;
        gridStroke = ChartUtils.generateStroke(gridStyle);
    }

    /**
     * @return the subGridColor
     */
    public Color getSubGridColor() {
        return subGridColor;
    }

    protected void computeSubGridColor(Color bgColor) {
        if ((labelColor != null) && (bgColor != null)) {
            subGridColor = ColorUtils.blend(labelColor, bgColor, 0.25);
        }
    }

    /**
     * @return the labelColor
     */
    public Color getLabelColor() {
        return labelColor;
    }

    /**
     * Changes the label color
     * 
     * @param labelColor the label color to set
     * @param bgColor The known background color
     */
    public void setLabelColor(Color labelColor, Color bgColor) {
        this.labelColor = labelColor;
        computeSubGridColor(bgColor);
    }

    /**
     * @return the origin
     */
    public int getOrigin() {
        return origin;
    }

    /**
     * @return the autoLabeling
     */
    public boolean isAutoLabeling() {
        return autoLabeling;
    }

    /**
     * Sets whether to use auto labeling
     * 
     * @param autoLabeling whether to use auto labeling
     */
    public void setAutoLabeling(boolean autoLabeling) {
        this.autoLabeling = autoLabeling;
    }

    /**
     * @return the subTickVisible
     */
    public boolean isSubTickVisible() {
        return subTickVisible;
    }

    /**
     * Sets whether subticks should be visible
     * 
     * @param subTickVisible whether subticks should be visible
     */
    public void setSubTickVisible(boolean subTickVisible) {
        this.subTickVisible = subTickVisible;
    }

    /**
     * @return the clipping
     */
    public boolean isClipping() {
        return clipping;
    }

    /**
     * Sets whether to use clipping
     * 
     * @param clipping whether to use clipping
     */
    public void setClipping(boolean clipping) {
        this.clipping = clipping;
    }

    public void setAxisProperties(AxisProperties properties, Color bgColor) {
        if (properties != null) {
            setMinimum(properties.getScaleMin());
            setMaximum(properties.getScaleMax());
            setScale(properties.getScaleMode());
            setAutoScale(properties.isAutoScale());
            setDrawOpposite(properties.isDrawOpposite());
            setLabelFormat(properties.getLabelFormat());
            setTitle(properties.getTitle());
            setTitleAlignment(properties.getTitleAlignment());
            Color color = ColorTool.getColor(properties.getColor());
            setLabelColor(color, bgColor);
            setGridStyle(properties.getGridStyle());
            setGridVisible(properties.isGridVisible());
            setSubGridVisible(properties.isSubGridVisible());
            setOrigin(properties.getPosition());
            setLabelInterval(properties.getUserLabelInterval());
            setDateFormat(properties.getDateFormat());
            setTimeFormat(properties.getTimeFormat());
            setLabelFont(FontTool.getFont(properties.getLabelFont()));
        }
    }

    public AxisProperties getAxisProperties() {
        return new AxisProperties(getMinimum(), getMaximum(), getScale(), isAutoScale(), getLabelFormat(),
                FontTool.getCometeFont(getLabelFont()), getTitle(), ColorTool.getCometeColor(getLabelColor()),
                getGridStyle(), isGridVisible(), isSubGridVisible(), true, isDrawOpposite(), getOrigin(),
                getLabelInterval(), getTitleAlignment(), getDateFormat(), getTimeFormat());
    }

    /**
     * Returns the date format when chosen label format is DATE_FORMAT
     * 
     * @return A {@link String}
     * @see IDateFormattable#US_DATE_FORMAT
     * @see IDateFormattable#FR_DATE_FORMAT
     */
    @Override
    public String getDateFormat() {
        return dateFormat;
    }

    /**
     * Sets date format when chosen label format is DATE_FORMAT
     * 
     * @param dateFormat The date format to use
     * @see IDateFormattable#US_DATE_FORMAT
     * @see IDateFormattable#FR_DATE_FORMAT
     */
    @Override
    public void setDateFormat(String dateFormat) {
        if ((dateFormat != null) && (!dateFormat.trim().isEmpty())) {
            if (IDateFormattable.US_DATE_FORMAT.equalsIgnoreCase(dateFormat)
                    || IDateFormattable.FR_DATE_FORMAT.equalsIgnoreCase(dateFormat)) {
                int index = dateFormat.indexOf(' ');
                this.dateFormat = dateFormat.substring(0, index);
                setTimeFormat(dateFormat.substring(index + 1));
            } else {
                this.dateFormat = dateFormat;
            }
        }
    }

    public String getFullDateFormat() {
        String format = dateFormat;
        if (timeFormat != null) {
            format = format + " " + timeFormat;
        }
        return format;
    }

    public String getValueFormat() {
        return valueFormat;
    }

    public void setValueFormat(String valueFormat) {
        this.valueFormat = valueFormat;
    }

    public String getFormat() {
        String format;
        switch (labelFormat) {
            case IChartViewer.DATE_FORMAT:
                format = getFullDateFormat();
                break;
            case IChartViewer.TIME_FORMAT:
                format = timeFormat;
                if (format == null) {
                    format = IDateFormattable.TIME_SHORT_FORMAT;
                }
                break;
            default:
                format = valueFormat;
                break;
        }
        return format;
    }

    /**
     * Customize axis labels.
     * 
     * @param labels Label values
     * @param labelPos Label positions (in axis coordinates)
     */
    public void setLabels(String[] labels, double[] labelPos) {
        if ((labels == null) || (labelPos == null)) {
            clearUserLabels();
        } else if (labels.length != labelPos.length) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                    .info("AxisAttributes.setLabels() : labels and labelPos must have the same size");
        } else {
            String[] effectiveLabels = new String[labels.length];
            for (int i = 0; i < labels.length; i++) {
                if (labels[i] == null) {
                    effectiveLabels[i] = CometeConstants.NULL;
                } else {
                    effectiveLabels[i] = labels[i];
                }
            }
            userLabel = effectiveLabels;
            userLabelPos = labelPos.clone();
            autoLabeling = false;
        }
    }

    /**
     * Delete user labels and return to auto labeling.
     */
    public void clearUserLabels() {
        userLabel = null;
        userLabelPos = null;
        autoLabeling = true;
    }

    public String[] getUserLabel() {
        return userLabel;
    }

    // JIRA JAVAAPI-39
    public void setUserLabel(String[] userLabel) {
        this.userLabel = userLabel;
    }

    public double[] getUserLabelPos() {
        return userLabelPos;
    }

    // JIRA JAVAAPI-39
    public void setUserLabelPos(double[] userLabelPos) {
        this.userLabelPos = userLabelPos;
    }

    /**
     * @return the axisDuration
     */
    public double getAxisDuration() {
        return axisDuration;
    }

    /**
     * Changes the axis duration
     * 
     * @param axisDuration the axis duration to set
     */
    public void setAxisDuration(double axisDuration) {
        this.axisDuration = axisDuration;
    }

    /**
     * Fit the x axis to display duration (Horizontal axis only).
     * 
     * @param b <code>true</code> to fit x axis false otherwise
     */
    public void setFitXAxisToDisplayDuration(boolean b) {
        fitXAxisToDisplayDuration = b;
    }

    /**
     * Return true if the x axis fit to display duration.
     */
    public boolean isFitXAxisToDisplayDuration() {
        return fitXAxisToDisplayDuration;
    }

    /**
     * Sets the percent scrollback. When using TIME_ANNO mode for the horizontal
     * axis this property allows to avoid a full graph repaint for every new
     * data entered.
     * 
     * @param d Scrollback percent [0..100]
     */
    public void setPercentScrollback(double d) {
        percentScrollback = d / 100;
    }

    /**
     * Gets the percent scrollback
     * 
     * @return scrollback percent
     */
    public double getPercentScrollback() {
        return percentScrollback;
    }

    /**
     * Returns the subtick length (in pixel).
     */
    public int getSubtickLength() {
        return subtickLength;
    }

    /**
     * Returns the tick length (in pixel).
     */
    public int getTickLength() {
        return tickLength;
    }

    /**
     * Sets the tick length (in pixel).
     * 
     * @param lgth Length
     */
    public void setTickLength(int lgth) {
        tickLength = lgth;
        subtickLength = tickLength / 2;
    }

    /**
     * Returns the axis title alignment
     * 
     * @return an int value
     * 
     * @see IComponent#TOP
     * @see IComponent#LEFT
     * @see IComponent#BOTTOM
     * @see IComponent#RIGHT
     */
    public int getTitleAlignment() {
        return titleAlignment;
    }

    /**
     * Sets the axis title alignment
     * 
     * @param titleAlignment the alignment to set
     * 
     * @see IComponent#TOP
     * @see IComponent#LEFT
     * @see IComponent#BOTTOM
     * @see IComponent#RIGHT
     */
    public void setTitleAlignment(int titleAlignment) {
        this.titleAlignment = titleAlignment;
    }

    /**
     * Returns the current minimum tick spacing (in pixel).
     */
    public double getTickSpacing() {
        return minTickStep;
    }

    /**
     * Sets the minimum tick spacing (in pixel). Allows to control the number of
     * generated labels.
     * 
     * @param spacing Minimum tick spacing
     */
    public void setTickSpacing(double spacing) {
        minTickStep = spacing;
    }

    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
    }

    public String getTimeFormat() {
        return timeFormat;
    }

}
