/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.periodictable;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

/**
 * 
 * @author ADIOUF
 * 
 */
public class ElementModel implements CometeConstants {

    private String elementName;
    private String symbole;
    private String atomicNumber;

    private ArrayList<EnergyElement> energyElements = new ArrayList<>();

    private boolean isEmpty = true;

    protected final static String REF_ENERGY = "Energy";

    public ElementModel() {

    }

    public ElementModel(String model) {
        this.initFromStringModel(model);
    }

    public static ElementModel defaultElement() {
        ElementModel result = new ElementModel();
        result.setElementName("Name");
        result.setAtomicNumber("0");
        result.setSymbole("X");
        return result;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getSymbole() {
        return symbole;
    }

    public void setSymbole(String symbole) {
        this.symbole = symbole;
    }

    public String getAtomicNumber() {
        return atomicNumber;
    }

    public void setAtomicNumber(String atomicNumber) {
        this.atomicNumber = atomicNumber;
    }

    public ArrayList<EnergyElement> getEnergyElements() {
        return energyElements;
    }

    public void setEnergyElements(ArrayList<EnergyElement> energyElements) {
        this.energyElements = energyElements;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }

    public void initFromStringModel(String datas) {

        ArrayList<String> energyTokens = new ArrayList<>();
        ArrayList<String> modelTokens = new ArrayList<>();

        ArrayList<String> energyTypeNames = new ArrayList<>();
        ArrayList<Double> energyTypeValues = new ArrayList<>();

        try {

            StringTokenizer divider = new StringTokenizer(datas, ";");
            if (divider != null) {
                elementName = divider.nextToken();
                symbole = divider.nextToken();
                atomicNumber = divider.nextToken();

                while (divider.hasMoreTokens()) {
                    energyTokens.add(divider.nextToken());
                }

                divider = new StringTokenizer(PeriodicTable.getComposantModel(), ";");
                int index = 0;
                for (int i = 0; i < 3; ++i) {
                    divider.nextToken();
                }
                while (divider.hasMoreTokens()) {
                    String temp = divider.nextToken();
                    modelTokens.add(temp);
                    if (temp.contains(REF_ENERGY) && !(energyTokens.get(index).equals("NaN"))) {
                        String[] energyDivider = temp.split(COMMA);
                        energyTypeNames.add(energyDivider[1]);
                        energyTypeValues.add(new Double(energyTokens.get(index)));
                    }
                    ++index;
                }

                for (int i = 0; i < energyTypeNames.size(); ++i) {
                    String energyType = energyTypeNames.get(i);
                    EnergyElement elem = new EnergyElement();
                    elem.setEnergyType(energyType);
                    elem.setEnergyValue(energyTypeValues.get(i).doubleValue());

                    for (int j = 0; j < modelTokens.size(); ++j) {
                        String[] temp = modelTokens.get(j).split(COMMA);
                        if (energyType.equals(temp[0]) && !energyTokens.get(j).equals("NaN")) {
                            elem.addEnergy(temp[1], new Double(energyTokens.get(j)));
                        }
                    }
                    energyElements.add(elem);
                }
                this.setEmpty(false);
            }

        } catch (Exception e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to init from string model", e);
        }

    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(elementName);
        result.append(COMMA).append(symbole).append(COMMA).append(atomicNumber).append(ObjectUtils.NEW_LINE);
        for (int i = 0; i < energyElements.size(); ++i) {
            result.append(energyElements.get(i).toString()).append(ObjectUtils.NEW_LINE);
        }
        result.append(ObjectUtils.NEW_LINE);
        return result.toString();
    }

    public static void main(String[] args) {
        PeriodicTable.setComposantModel(
                "Nom;Symbole;Atomique_Number ;Energy,K;Energy,L-III;Energy,Mv;K,Alpha1;K,Beta1;K,Alpha2;K,Beta2;L-III,Alpha1;L-III,Beta1;L-III,Beta2;L-III,Gamma1;L-III,Iota ;L-III,Gamma3;Mv,Alpha1");
        new ElementModel("Hydrogen;H;1;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN").toString();
    }

}
