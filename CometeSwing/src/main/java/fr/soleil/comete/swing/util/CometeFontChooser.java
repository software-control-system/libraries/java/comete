/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Position;

import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.text.JSmoothLabel;

/**
 * A font chooser dialog box.
 * <p>
 * 
 * <pre>
 *  CometeFont defCometeFont = new CometeFont(&quot;Dialog&quot;, Font.BOLD, 12);
 *  CometeFont newCometeFont = CometeFontChooser.getNewCometeFont(this,&quot;Choose font&quot;,defCometeFont);
 *  if (newCometeFont != null) {
 *     ....
 *  }
 * </pre>
 */
public class CometeFontChooser extends JDialog implements ActionListener, ListSelectionListener, ChangeListener {

    private static final long serialVersionUID = 4079708525277596386L;

    private JPanel innerPanel;
    private CometeFont currentCometeFont;
    private int fontSize;
    private CometeFont result;
    private String[] allFamily;

    private DefaultListModel<String> listModel;
    private JScrollPane familyView;
    private JList<String> familyList;

    private JPanel infoPanel;

    private JCheckBox plainCheck;
    private JCheckBox boldCheck;
    private JCheckBox italicCheck;
    private JCheckBox italicboldCheck;

    private JTextField sizeText;
    private JLabel sizeLabel;
    private JSlider sizeSlider;

    private JSmoothLabel sampleLabel;

    private JButton okBtn;
    private JButton cancelBtn;

    private static CometeFont labelCometeFontBold = new CometeFont("Dialog", Font.BOLD, 12);
    private static CometeFont labelCometeFont = new CometeFont("Dialog", Font.PLAIN, 12);
    private static Color labelColor = new Color(85, 87, 140);

    // -----------------------------------------------
    // Contruction
    // -----------------------------------------------
    public CometeFontChooser(Window parent, String title, CometeFont initialCometeFont) {
        super(parent);
        setModal(true);
        initComponents(title, initialCometeFont);
    }

    private void initComponents(String title, CometeFont initialCometeFont) {

        innerPanel = new JPanel();
        innerPanel.setLayout(null);

        if (initialCometeFont == null) {
            currentCometeFont = labelCometeFont;
        } else {
            currentCometeFont = initialCometeFont;
        }

        fontSize = currentCometeFont.getSize();
        result = currentCometeFont;

        // Get all available family font
        allFamily = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();

        // Create Gui

        listModel = new DefaultListModel<>();
        familyList = new JList<>(listModel);
        familyList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        Font awtLabelCometeFont = FontTool.getFont(labelCometeFont);
        familyList.setFont(awtLabelCometeFont);

        for (String element : allFamily) {
            listModel.addElement(element);
        }

        familyView = new JScrollPane(familyList);
        familyView.setBorder(BorderFactory.createLoweredBevelBorder());

        infoPanel = new JPanel();
        infoPanel.setLayout(null);
        infoPanel.setBorder(createTitleBorder("Size and Style"));

        plainCheck = new JCheckBox("Plain");
        plainCheck.setForeground(labelColor);
        plainCheck.setFont(awtLabelCometeFont);
        plainCheck.addActionListener(this);

        boldCheck = new JCheckBox("Bold");
        boldCheck.setForeground(labelColor);
        boldCheck.setFont(awtLabelCometeFont);
        boldCheck.addActionListener(this);

        italicCheck = new JCheckBox("Italic");
        italicCheck.setForeground(labelColor);
        italicCheck.setFont(awtLabelCometeFont);
        italicCheck.addActionListener(this);

        italicboldCheck = new JCheckBox("Bold italic");
        italicboldCheck.setForeground(labelColor);
        italicboldCheck.setFont(awtLabelCometeFont);
        italicboldCheck.addActionListener(this);

        sizeText = new JTextField();
        sizeText.addActionListener(this);
        sizeText.setEditable(true);

        sizeLabel = new JLabel("Size");
        sizeLabel.setFont(awtLabelCometeFont);
        sizeLabel.setForeground(labelColor);

        sizeSlider = new JSlider(5, 72, fontSize);
        sizeSlider.setMinorTickSpacing(1);
        sizeSlider.setMajorTickSpacing(5);
        sizeSlider.setPaintTicks(true);
        sizeSlider.setPaintLabels(true);

        sizeSlider.addChangeListener(this);

        infoPanel.add(plainCheck);
        infoPanel.add(italicCheck);
        infoPanel.add(italicboldCheck);
        infoPanel.add(boldCheck);
        infoPanel.add(sizeLabel);
        infoPanel.add(sizeText);
        infoPanel.add(sizeSlider);

        plainCheck.setBounds(5, 20, 100, 25);
        italicCheck.setBounds(5, 45, 100, 25);
        boldCheck.setBounds(5, 70, 100, 25);
        italicboldCheck.setBounds(5, 95, 100, 25);
        sizeLabel.setBounds(130, 35, 80, 25);
        sizeText.setBounds(130, 60, 80, 25);
        sizeSlider.setBounds(5, 125, 240, 45);

        okBtn = new JButton("Apply");
        okBtn.setFont(awtLabelCometeFont);
        okBtn.addActionListener(this);

        cancelBtn = new JButton("Cancel");
        cancelBtn.setFont(awtLabelCometeFont);
        cancelBtn.addActionListener(this);

        sampleLabel = new JSmoothLabel();
        sampleLabel.setText("Sample 12.34");
        sampleLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        sampleLabel.setBackground(new Color(220, 220, 220));

        // Add and size
        innerPanel.add(familyView);
        innerPanel.add(infoPanel);
        innerPanel.add(okBtn);
        innerPanel.add(cancelBtn);
        innerPanel.add(sampleLabel);

        infoPanel.setBounds(178, 5, 254, 175);
        familyView.setBounds(5, 5, 170, 300);
        okBtn.setBounds(180, 280, 80, 25);
        cancelBtn.setBounds(350, 280, 80, 25);
        sampleLabel.setBounds(180, 185, 250, 90);

        setTitle(title);
        updateControl();

        // Select the currentCometeFont
        int selid = familyList.getNextMatch(awtLabelCometeFont.getFamily(), 0, Position.Bias.Forward);

        if (selid != -1) {
            familyList.setSelectedIndex(selid);
            familyList.ensureIndexIsVisible(selid);
        }

        familyList.addListSelectionListener(this);

        innerPanel.setPreferredSize(new Dimension(435, 312));
        setContentPane(innerPanel);
        setResizable(false);

    }

    // Update control according to the current font.
    private void updateControl() {
        Font awtCurrentCometeFont = FontTool.getFont(currentCometeFont);
        plainCheck.setSelected(awtCurrentCometeFont.isPlain());
        boldCheck.setSelected(awtCurrentCometeFont.isBold() && !awtCurrentCometeFont.isItalic());
        italicCheck.setSelected(awtCurrentCometeFont.isItalic() && !awtCurrentCometeFont.isBold());
        italicboldCheck.setSelected(awtCurrentCometeFont.isBold() && awtCurrentCometeFont.isItalic());

        sampleLabel.setFont(awtCurrentCometeFont);
        sizeText.setText(Integer.toString(fontSize));

    }

    // -----------------------------------------------
    // ActionListener
    // -----------------------------------------------
    @Override
    public void actionPerformed(ActionEvent e) {
        Font awtCurrentCometeFont = FontTool.getFont(currentCometeFont);
        if (e.getSource() == cancelBtn) {
            result = null;
            setVisible(false);
            dispose();
        } else if (e.getSource() == okBtn) {
            result = currentCometeFont;
            setVisible(false);
            dispose();
        } else if (e.getSource() == plainCheck) {
            CometeFont newCometeFont = new CometeFont(awtCurrentCometeFont.getFamily(), Font.PLAIN, fontSize);
            if (newCometeFont != null) {
                currentCometeFont = newCometeFont;
                updateControl();
            }
        } else if (e.getSource() == boldCheck) {
            CometeFont newCometeFont = new CometeFont(awtCurrentCometeFont.getFamily(), Font.BOLD, fontSize);
            if (newCometeFont != null) {
                currentCometeFont = newCometeFont;
                updateControl();
            }

        } else if (e.getSource() == italicCheck) {
            CometeFont newCometeFont = new CometeFont(awtCurrentCometeFont.getFamily(), Font.ITALIC, fontSize);
            if (newCometeFont != null) {
                currentCometeFont = newCometeFont;
                updateControl();
            }
        } else if (e.getSource() == italicboldCheck) {
            CometeFont newCometeFont = new CometeFont(awtCurrentCometeFont.getFamily(), Font.ITALIC + Font.BOLD,
                    fontSize);
            if (newCometeFont != null) {
                currentCometeFont = newCometeFont;
                updateControl();
            }
        } else if (e.getSource() == sizeText) {
            try {
                int nSize = Integer.parseInt(sizeText.getText());
                sizeSlider.setValue(nSize);
            } catch (NumberFormatException ex) {
                JOptionPane.showConfirmDialog(this, "Wrong integer format\n" + ex.getMessage());
            }
        }

    }

    // -----------------------------------------------
    // SelectionListListener
    // -----------------------------------------------
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getSource() == familyList) {

            String fName = listModel.get(familyList.getSelectedIndex());

            CometeFont newCometeFont = new CometeFont(fName, Font.PLAIN, fontSize);
            if (newCometeFont != null) {
                currentCometeFont = newCometeFont;
                updateControl();
            }

        }

    }

    // -----------------------------------------------
    // Change listener
    // -----------------------------------------------
    @Override
    public void stateChanged(ChangeEvent e) {
        Font awtCurrentCometeFont = FontTool.getFont(currentCometeFont);
        if (e.getSource() == sizeSlider) {
            fontSize = sizeSlider.getValue();
            CometeFont newCometeFont = FontTool.getCometeFont(awtCurrentCometeFont.deriveFont((float) fontSize));
            if (newCometeFont != null) {
                currentCometeFont = newCometeFont;
                updateControl();
            }
        }

    }

    /**
     * Display the CometeFont chooser dialog.
     * 
     * @param parent
     *            Parent component
     * @param dlgTitle
     *            Dialog title
     * @param defaultCometeFont
     *            Default font (can be null)
     * @return A handle to a new CometeFont, null when canceled.
     */
    public static CometeFont getNewCometeFont(Component parent, String dlgTitle, CometeFont defaultCometeFont) {
        Window pWindow = WindowSwingUtils.getWindowForComponent(parent);
        CometeFontChooser dlg;
        if (pWindow instanceof Dialog) {
            dlg = new CometeFontChooser(pWindow, dlgTitle, defaultCometeFont);
        } else if (pWindow instanceof Frame) {
            dlg = new CometeFontChooser(pWindow, dlgTitle, defaultCometeFont);
        } else {
            dlg = new CometeFontChooser((Frame) null, dlgTitle, defaultCometeFont);
        }
        dlg.setLocationRelativeTo(pWindow);
        dlg.setVisible(true);
        return dlg.result;

    }

    private Border createTitleBorder(String name) {
        return new TitledBorder(new EtchedBorder(), name, TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
                FontTool.getFont(labelCometeFontBold), labelColor);
    }

    public static void main(String[] args) {
        getNewCometeFont(null, "Choose font", null);
        System.exit(0);
    }

}
