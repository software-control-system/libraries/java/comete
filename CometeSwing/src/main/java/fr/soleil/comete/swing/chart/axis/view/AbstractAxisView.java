/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.axis.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.List;

import javax.swing.SwingConstants;

import fr.soleil.comete.swing.chart.axis.model.AxisAttributes;
import fr.soleil.comete.swing.chart.axis.model.AxisLabel;
import fr.soleil.comete.swing.chart.axis.scale.AbstractAxisScale;
import fr.soleil.comete.swing.chart.axis.scale.LinearScale;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Class that handles axis display
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AbstractAxisView implements SwingConstants {

    public static final int DEFAULT_MARGIN = 5;
    protected static final String STRING_TEMPLATE = "dummyStr0";

    protected static final Stroke DEFAULT_AXIS_STROKE = new BasicStroke(1);

    protected AxisAttributes attributes;
    protected AbstractAxisScale scale;
    protected int fontOverWidth;
    protected Dimension axisSize;
    protected boolean visible;
    protected Color axisColor;
    protected int tickAlignment;
    protected int labelAlignment;
    protected int titlePosition;
    protected String axeName;
    protected int maxLabelWidth, maxLabelHeight;
    protected Rectangle titleBounds;

    public AbstractAxisView(AxisAttributes attributes, int tickAlignment, int labelAlignment) {
        this.attributes = attributes;
        scale = new LinearScale(attributes);
        fontOverWidth = 0;
        axisSize = null;
        visible = true;
        this.tickAlignment = tickAlignment;
        this.labelAlignment = labelAlignment;
        axeName = ObjectUtils.EMPTY_STRING;
        maxLabelWidth = 0;
        maxLabelHeight = 0;
        setAxisColor(Color.BLACK);
        titleBounds = new Rectangle();
    }

    public AxisAttributes getAttributes() {
        return attributes;
    }

    /**
     * Returns the axis scale
     * 
     * @return An {@link AbstractAxisScale}
     */
    public AbstractAxisScale getScale() {
        return scale;
    }

    /**
     * Sets the axis scale
     * 
     * @param scale The axis scale to set
     */
    public void setScale(AbstractAxisScale scale) {
        if ((scale != null) && (!scale.equals(this.scale))) {
            this.scale = scale;
        }
    }

    /**
     * Expert usage. Computes labels and measures axis dimension.
     * 
     * @param frc Font render context
     * @param desiredLength Desired length
     * @param desiredHeight Desired height
     * @param views The views used to calculate scale
     */
    public void measureAxis(FontRenderContext frc, int desiredLength, List<? extends AbstractDataView> views) {
        int maxLabelWidth = 10; // Minimum width
        int maxLabelHeight = 0;
        scale.computeAxisItems(frc, desiredLength, computeLabelOffsetFromTickAlignment(), views);
        AxisLabel[] labels = scale.getLabels();
        for (AxisLabel label : labels) {
            if (label.getTotalWidth() > maxLabelWidth) {
                maxLabelWidth = label.getTotalWidth();
            }
            if (label.getTotalHeight() > maxLabelHeight) {
                maxLabelHeight = label.getTotalHeight();
            }
        }
        fontOverWidth = maxLabelWidth / 2 + 1;
        this.maxLabelWidth = maxLabelWidth;
        this.maxLabelHeight = maxLabelHeight;
        axisSize = computeAxisSize(frc, desiredLength, maxLabelWidth, maxLabelHeight);
    }

    /**
     * Calculates axis size (width and height)
     * 
     * @param frc Font render context
     * @param desiredLength Desired length
     * @param maxLabelWidth Maximum label width
     * @param maxLabelHeight Maximum label height
     * @return A {@link Dimension}
     */
    protected abstract Dimension computeAxisSize(FontRenderContext frc, int desiredLength, int maxLabelWidth,
            int maxLabelHeight);

    /**
     * Expert usage. Calculates label height, according to a {@link FontRenderContext}
     * 
     * @param frc The Font render context
     * @return Axis font height.
     */
    public int getLabelHeight(FontRenderContext frc) {
        int dimension;
        if ((!visible) || (frc == null) || (attributes == null)) {
            dimension = DEFAULT_MARGIN; // 5 pixel margin when axis not visible
        } else {
            Rectangle2D bounds = attributes.getLabelFont().getStringBounds(STRING_TEMPLATE, frc);
            dimension = (int) (Math.abs(bounds.getY()) + bounds.getHeight());
        }
        return dimension;
    }

    /**
     * Draws the axis at a given coordinate, knowing background color (mainly used to distinguish
     * ticks from sub-ticks). This method also draws axis grid and sub-grid, when available
     * 
     * @param g The {@link Graphics2D} in which to draw the axes
     * @param frc The labels {@link FontRenderContext}
     * @param back The background color
     * @param x The x coordinate
     * @param y The y coordinate
     * @param oppositeCoordinate The opposite axis coordinate
     * @param oppositeTickAlignment The opposite tick alignment
     * @param drawOpposite Whether to draw opposite axis
     * @param gridMin The minimum grid position
     * @param gridMax The maximum grid position
     */
    public abstract void paintAxis(Graphics2D g, FontRenderContext frc, Color back, int x, int y,
            int oppositeCoordinate, int oppositeTickAlignment, boolean drawOpposite, int gridMin, int gridMax);

    /**
     * Computes labels offset, based on tick alignment
     * 
     * @return An <code>int[]</code>: <code>{offX, offY}</code>
     */
    public abstract int[] computeLabelOffsetFromTickAlignment();

    public int getFontOverWidth() {
        return fontOverWidth;
    }

    public int getMaxLabelWidth() {
        return maxLabelWidth;
    }

    public int getMaxLabelHeight() {
        return maxLabelHeight;
    }

    /**
     * Expert usage. Returns axis thickness in pixel ( shorter side )
     * 
     * @return Axis thickness
     */
    public abstract int getThickness();

    /**
     * Expert usage. Returns axis lenght in pixel ( larger side ).
     * 
     * @return Axis lenght.
     */
    public abstract int getLength();

    /**
     * Returns the axis width
     * 
     * @return An <code>int</code>
     */
    protected int getAxisWidth() {
        return (axisSize == null ? 0 : axisSize.width);
    }

    /**
     * Returns the axis height
     * 
     * @return An <code>int</code>
     */
    protected int getAxisHeight() {
        return (axisSize == null ? 0 : axisSize.height);
    }

    /**
     * Displays or hides the axis.
     * 
     * @param b True to make the axis visible.
     */
    public void setVisible(boolean b) {
        visible = b;
    }

    /**
     * Returns <code>true</code> if the axis is visible, <code>false</code> otherwise
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Returns the axis line color
     * 
     * @return A {@link Color}
     */
    public Color getAxisColor() {
        return axisColor;
    }

    /**
     * Sets the axis line color
     * 
     * @param color The axis line color to use. If <code>null</code>, {@link Color#BLACK} will be
     *            used
     */
    public void setAxisColor(Color color) {
        this.axisColor = (color == null ? Color.BLACK : color);
    }

    /**
     * Returns the ticks alignment for this axis
     * 
     * @return An <code>int</code>
     * @see SwingConstants#TOP
     * @see SwingConstants#BOTTOM
     * @see SwingConstants#CENTER
     * @see SwingConstants#LEFT
     * @see SwingConstants#RIGHT
     */
    public int getTickAlignment() {
        return tickAlignment;
    }

    /**
     * Sets the ticks alignment
     * 
     * @param tickAlignment The ticks alignment to set
     * @see SwingConstants#TOP
     * @see SwingConstants#BOTTOM
     * @see SwingConstants#CENTER
     * @see SwingConstants#LEFT
     * @see SwingConstants#RIGHT
     */
    public void setTickAlignment(int tickAlignment) {
        this.tickAlignment = tickAlignment;
    }

    /**
     * Returns the label alignment for this axis
     * 
     * @return An <code>int</code>
     * @see SwingConstants#TOP
     * @see SwingConstants#BOTTOM
     * @see SwingConstants#CENTER
     * @see SwingConstants#LEFT
     * @see SwingConstants#RIGHT
     */
    public int getLabelAlignment() {
        return labelAlignment;
    }

    /**
     * Sets the labels alignment for this axis
     * 
     * @param labelAlignment The labels alignment to set
     * @see SwingConstants#TOP
     * @see SwingConstants#BOTTOM
     * @see SwingConstants#CENTER
     * @see SwingConstants#LEFT
     * @see SwingConstants#RIGHT
     */
    public void setLabelAlignment(int labelAlignment) {
        this.labelAlignment = labelAlignment;
    }

    /**
     * Returns the title position relative to this axis view
     * 
     * @return An <code>int</code>
     * @see SwingConstants#TOP
     * @see SwingConstants#BOTTOM
     * @see SwingConstants#CENTER
     * @see SwingConstants#LEFT
     * @see SwingConstants#RIGHT
     */
    public int getTitlePosition() {
        return titlePosition;
    }

    /**
     * Sets he title position relative to this axis view
     * 
     * @param titlePosition The title position to set
     * @see SwingConstants#TOP
     * @see SwingConstants#BOTTOM
     * @see SwingConstants#CENTER
     * @see SwingConstants#LEFT
     * @see SwingConstants#RIGHT
     */
    public void setTitlePosition(int titlePosition) {
        this.titlePosition = titlePosition;
    }

    /**
     * Gets the axis label.
     * 
     * @return Axis name.
     * @see #setAxeName(String)
     */
    public String getAxeName() {
        return axeName;
    }

    /**
     * Sets the axis name. Name is displayed in tooltips when clicking on the graph.
     * 
     * @param s Name of this axis.
     * @see #getAxeName()
     */
    public void setAxeName(String s) {
        axeName = s;
    }

    /**
     * Applies the title bounds, in given {@link FontRenderContext}, to a {@link Rectangle}
     * 
     * @param frc The {@link FontRenderContext}
     * @param x The horizontal start position
     * @param y The vertical start position
     * @param bounds The {@link Rectangle} to which to apply title bounds
     */
    public abstract void applyTitleBounds(FontRenderContext frc, int x, int y, Rectangle bounds);
}
