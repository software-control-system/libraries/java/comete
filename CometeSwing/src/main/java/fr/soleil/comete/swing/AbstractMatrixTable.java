/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JDialog;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.JXTable;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.listener.ITableListener;
import fr.soleil.comete.definition.util.FileArrayUtils;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.DefaultTableModelWithRowName;
import fr.soleil.comete.swing.util.mask.MaskedTable;
import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.matrix.IMatrixTarget;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;

public class AbstractMatrixTable<T> extends AbstractTable<AbstractMatrix<T>> implements IMatrixTarget {

    private static final long serialVersionUID = 5929713413680423712L;

    public AbstractMatrixTable() {
        super();
    }

    @Override
    protected DefaultTableModelWithRowName generateDefaultModel() {
        return new DefaultTableModelWithRowName() {

            private static final long serialVersionUID = -602956685825591777L;

            @Override
            public int getRowCount() {
                int count = 0;
                if (matrix != null) {
                    count = matrix.getHeight();
                }
                return count;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                Class<?> columnClass;
                if (Format.isNumberFormatOK(format)) {
                    columnClass = String.class;
                } else {
                    columnClass = Number.class;
                }
                return columnClass;
            }

            @Override
            public int getColumnCount() {
                int count = 0;
                if (matrix != null) {
                    count = matrix.getWidth();
                }
                return count;
            }

            @Override
            public String getColumnName(int column) {
                String name;
                String[] customColumnNames = AbstractMatrixTable.this.customColumnNames;
                if ((customColumnNames == null) || (column >= customColumnNames.length) || (column < 0)) {
                    name = String.valueOf(column);
                } else {
                    name = customColumnNames[column];
                }
                return name;
            }

            @Override
            public String getRowName(int row) {
                String name;
                String[] customRowNames = AbstractMatrixTable.this.customRowNames;
                if ((customRowNames == null) || (row >= customRowNames.length) || (row < 0)) {
                    name = String.valueOf(row);
                } else {
                    name = customRowNames[row];
                }
                return name;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return (editable && (!loadDataVisible));
            }

            @Override
            public Object getValueAt(int row, int column) {
                Object result = null;
                if (matrix != null) {
                    result = matrix.getValueAt(row, column);
                    if ((result instanceof Number) && Format.isNumberFormatOK(format)) {
                        Number nb = (Number) result;
                        try {
                            result = Format.format(format, nb);
                        } catch (Exception e) {
                            result = nb.toString();
                        }
                    }
                }
                return result;
            }

            @Override
            public void setValueAt(Object aValue, int row, int column) {
                if (matrix != null) {
                    AbstractMatrix<T> tmpMatrix = matrix.clone();
                    try {
                        AbstractAdapter<String, T> converter = matrix.getConverter();
                        T value = converter.adapt(aValue.toString());
                        tmpMatrix.setValueAt(value, row, column);
                        setData(tmpMatrix);
                    } catch (Exception e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .error(AbstractMatrixTable.this.getClass().getName() + "@"
                                        + AbstractMatrixTable.this.hashCode() + " failed to setValue at row=" + row
                                        + ", column=" + column, e);
                    }
                }
            }
        };
    }

    @Override
    public boolean isSaveColumnIndex() {
        if (matrix != null) {
            super.setSaveColumnIndex(matrix.isSaveColumnIndex());
        }
        return super.isSaveColumnIndex();
    }

    @Override
    public void setSaveColumnIndex(boolean saveColumnIndex) {
        super.setSaveColumnIndex(saveColumnIndex);
        if (matrix != null) {
            matrix.setSaveColumnIndex(saveColumnIndex);
        }
    }

    @Override
    public String getValueSeparator() {
        if (matrix != null) {
            super.setValueSeparator(matrix.getValueSeparator());
        }
        return super.getValueSeparator();
    }

    @Override
    public void setValueSeparator(String valueSeparator) {
        super.setValueSeparator(valueSeparator);
        if (matrix != null) {
            matrix.setValueSeparator(valueSeparator);
        }
    }

    @Override
    public void saveDataFile(String fileName) {
        if ((fileName != null) && (matrix != null)) {
            synchronized (matrix) {
                setSaveColumnIndex(saveColumnIndex);
                setValueSeparator(valueSeparator);
                FileArrayUtils.saveMatrixDataFile(matrix, fileName);
            }
        }
    }

    @Override
    public void loadDataFile(String loadDataFile) {
        loadMatrix = matrix.clone();
        FileArrayUtils.loadMatrixDataFile(loadMatrix, loadDataFile);
        setLoadDataVisible(true);
    }

    public T getValueAtPoint(double[] point) {
        T value = matrix.getValueAt((int) point[IChartViewer.Y_INDEX], (int) point[IChartViewer.X_INDEX]);
        if (!listeners.isEmpty() && value != null) {
            for (ITableListener<AbstractMatrix<T>> listener : listeners) {
                if (listener != null) {
                    listener.selectedRowChanged((int) (point[IChartViewer.Y_INDEX]));
                    listener.selectedColumnChanged((int) (point[IChartViewer.X_INDEX]));
                    listener.selectedPointChanged(point);
                }
            }
        }
        return value;
    }

    public T getValueAtIndex(int column) {
        T value = matrix.getValueAt(0, column);
        Collection<ITableListener<AbstractMatrix<T>>> listeners;
        synchronized (this.listeners) {
            listeners = new ArrayList<>();
        }
        try {
            if ((!listeners.isEmpty()) && (value != null)) {
                double[] point = { column, 0 };
                for (ITableListener<AbstractMatrix<T>> tmpListener : listeners) {
                    if (tmpListener != null) {
                        // tmpListener.selectedValueChanged(value);
                        tmpListener.selectedRowChanged(0);
                        tmpListener.selectedColumnChanged(column);
                        tmpListener.selectedPointChanged(point);
                    }
                }
            }
        } finally {
            listeners.clear();
        }
        return value;
    }

    @Override
    protected void applySelection() {
        if (matrix != null && table.getSelectedColumnCount() > 0) {
            int[] selectedColumn = table.getSelectedColumns();
            int[] selectedRow = table.getSelectedRows();

            AbstractMatrix<T> newMatrix = matrix.clone();
            for (int row = 0; row < selectedRow.length; row++) {
                for (int col = 0; col < selectedColumn.length; col++) {
                    T value = matrix.parseValue(table.getValueAt(selectedRow[row], selectedColumn[col]).toString());
                    newMatrix.setValueAt(value, row, col);
                }
            }

            matrix = newMatrix;
            fireValueChanged(matrix);
        }
    }

    @Override
    protected void showTrimTable(final AbstractMatrix<T> matrix) {

        MaskedTable trimedTable = new MaskedTable() {

            private static final long serialVersionUID = 6588341515040451454L;

            @Override
            public boolean isHiddenByMask(int row, int column) {
                return super.isHiddenByMask(row - matrix.getTrimStartLine(), column - matrix.getTrimStartColumn());
            }
        };

        DefaultTableModelWithRowName trimedTableModel = new DefaultTableModelWithRowName() {

            private static final long serialVersionUID = 5034187355187329609L;

            @Override
            public int getRowCount() {
                int count = 0;
                if (matrix != null) {
                    count = matrix.getTrimEndLine() - matrix.getTrimStartLine() + 1;
                }
                if (count < 0) {
                    count = 0;
                }
                return count;
            }

            @Override
            public int getColumnCount() {
                int count = 0;
                if (matrix != null) {
                    count = matrix.getTrimEndColumn() - matrix.getTrimStartColumn() + 1;
                }
                if (count < 0) {
                    count = 0;
                }
                return count;
            }

            @Override
            public String getColumnName(int column) {
                String name = ObjectUtils.EMPTY_STRING;
                if (matrix != null) {
                    String[] customColumnNames = AbstractMatrixTable.this.customColumnNames;
                    int index = matrix.getTrimStartColumn() + column;
                    if ((customColumnNames == null) || (index >= customColumnNames.length) || (index < 0)) {
                        name = String.valueOf(index);
                    } else {
                        name = customColumnNames[index];
                    }
                }
                return name;
            }

            @Override
            public String getRowName(int row) {
                String name = ObjectUtils.EMPTY_STRING;
                if (matrix != null) {
                    String[] customRowNames = AbstractMatrixTable.this.customRowNames;
                    int index = matrix.getTrimStartLine() + row;
                    if ((customRowNames == null) || (index >= customRowNames.length) || (index < 0)) {
                        name = String.valueOf(index);
                    } else {
                        name = customRowNames[index];
                    }
                }
                return name;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;

            }

            @Override
            public Object getValueAt(int row, int column) {
                Object value;
                if (matrix == null) {
                    value = ObjectUtils.EMPTY_STRING;
                } else {
                    try {
                        value = matrix.getValueAt(matrix.getTrimStartLine() + row,
                                matrix.getTrimStartColumn() + column);
                        if ((value instanceof Number) && Format.isNumberFormatOK(format)) {
                            value = Format.format(format, (Number) value);
                        }
                    } catch (Exception e) {
                        value = null;
                    }
                }
                return value;
            }

        };

        trimedTable.setModel(trimedTableModel);
        trimedTableModel.fireTableDataChanged();
        trimedTable.setColumnControlVisible(false);
        trimedTable.setAutoResizeMode(JXTable.AUTO_RESIZE_OFF);
        trimedTable.packAll();

        JScrollPane trimedScrollPane = new JScrollPane();
        trimedScrollPane.setViewportView(trimedTable);

        JDialog trimedDialog = new JDialog(CometeUtils.getFrameForComponent(this),
                "Matrix Trimed Data at " + DateUtil.milliToString(System.currentTimeMillis(), DATE_FORMAT), false);
        trimedDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        trimedDialog.setContentPane(trimedScrollPane);

        if (trimedDialog != null && !trimedDialog.isVisible()) {
            trimedDialog.setLocationRelativeTo(null);
            trimedDialog.setLocation(trimedDialog.getLocation().x, 0);
            trimedDialog.setSize(600, 600);
            trimedDialog.setPreferredSize(trimedDialog.getSize());
            trimedDialog.pack();
            trimedDialog.setVisible(true);
        }
    }

    @Override
    protected boolean isSameMatrixStructure(AbstractMatrix<T> matrix1, AbstractMatrix<T> matrix2) {
        boolean sameStructure = false;
        if (matrix1 == null) {
            sameStructure = (matrix2 == null);
        } else if (matrix2 != null) {
            sameStructure = ((matrix1.getHeight() == matrix2.getHeight())
                    && (matrix1.getWidth() == matrix2.getWidth()));
        }
        return sameStructure;
    }

    @Override
    protected AbstractMatrix<T> getCopy(AbstractMatrix<T> toCopy) {
        AbstractMatrix<T> copy;
        if (toCopy == null) {
            copy = null;
        } else {
            copy = toCopy.clone();
        }
        return copy;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        int height = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if (value != null) {
                height = value.length;
            }
        }
        return height;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int width = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if ((value != null) && (value.length > 0)) {
                width = Array.getLength(value[0]);
            }
        }
        return width;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return false;
    }

}
