/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Semaphore;

import fr.soleil.comete.definition.util.CometeDefinitionUtils;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.lib.project.ObjectUtils;
import ij.WindowManager;
import ij.gui.ImageWindow;

/**
 * A class that organizes ImageJ interactions
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ImageJManager {

    private static final ConcurrentMap<String, WeakReference<ImageViewer>> ACTIVATED_VIEWERS = new ConcurrentHashMap<String, WeakReference<ImageViewer>>();
    private static final ConcurrentMap<String, WeakReference<IImageJPluginListManager>> PLUGIN_MANAGERS = new ConcurrentHashMap<String, WeakReference<IImageJPluginListManager>>();
    private static final int TIMER_PERIOD = 5000; // 5s
    private static final String TIMER_NAME = new StringBuilder("ImageJManager references cleaner (every ")
            .append(TIMER_PERIOD / 1000).append("s)").toString(); // Timer name
    private static final Semaphore APPLICATION_LOCK = new Semaphore(1);
    private static String lastLock = null;
    private static final Timer TIMER;

    static {
        TIMER = new Timer(TIMER_NAME);
        TIMER.schedule(new ImageJCleanerTask(), 0, TIMER_PERIOD);
    }

    /**
     * Takes a global lock for an application. When such a lock is acquired, the concerned application is the only one
     * allowed to interact with ImageJ (i.e. its active {@link ImageViewer} and {@link IImageJPluginListManager} will
     * always be reactivated after each change in any {@link ImageViewer})
     * 
     * @param applicationId The application id
     * @throws InterruptedException If the current {@link Thread} was interrupted while trying to acquire the global
     *             lock
     * @see #setActivatedImageViewer(String, ImageViewer)
     * @see #setActivatedPluginListManager(String, IImageJPluginListManager)
     * @see #warnForContentChange(ImageViewer)
     */
    public static synchronized void grabLock(String applicationId) throws InterruptedException {
        APPLICATION_LOCK.acquire();
        lastLock = applicationId;
        updateImageJ(applicationId);
    }

    /**
     * Updates ImageJ to reconnect {@link ImageViewer} and {@link IImageJPluginListManager} that correspond to a given
     * application id
     * 
     * @param applicationId The application id
     */
    protected static void updateImageJ(String applicationId) {
        if (applicationId != null) {
            ImageViewer viewerToUse = getSelectedViewer(applicationId);
            IImageJPluginListManager pluginManagerToUse = getPluginListManager(applicationId);
            if ((viewerToUse != null) && (viewerToUse.getImageCanvas() != null)
                    && (viewerToUse.getImageCanvas().getImagePlus() != null)) {
                ImageWindow window = viewerToUse.getImageCanvas().getImagePlus().getWindow();
                WindowManager.setCurrentWindow(window);
                WindowManager.setWindow(window);
            }
            if (pluginManagerToUse != null) {
                pluginManagerToUse.registerPossibleActionsInIJ();
            }
        }
    }

    /**
     * Returns the previously registered {@link IImageJPluginListManager} for an application id
     * 
     * @param applicationId The application id
     * @return An {@link IImageJPluginListManager}
     */
    protected static IImageJPluginListManager getPluginListManager(String applicationId) {
        IImageJPluginListManager pluginManagerToUse = null;
        if (applicationId != null) {
            WeakReference<IImageJPluginListManager> managerRef = PLUGIN_MANAGERS.get(applicationId);
            if (managerRef != null) {
                pluginManagerToUse = managerRef.get();
            }
        }
        return pluginManagerToUse;
    }

    /**
     * Returns the previously registered {@link ImageViewer} for an application id
     * 
     * @param applicationId The application id
     * @return An {@link ImageViewer}
     */
    protected static ImageViewer getSelectedViewer(String applicationId) {
        return ObjectUtils.recoverObject(ACTIVATED_VIEWERS.get(applicationId));
    }

    /**
     * Releases the global lock, taken by an application
     * 
     * @param applicationId The application id. If the id does not match the id of the application that took the lock,
     *            nothing happens (i.e. the lock is not released).
     */
    public static synchronized void releaseLock(String applicationId) {
        if (ObjectUtils.sameObject(applicationId, lastLock)) {
            APPLICATION_LOCK.release();
        }
    }

    public static synchronized ImageViewer getActivatedImageViewer(String applicationId) {
        return getSelectedViewer(applicationId);
    }

    /**
     * Sets the {@link ImageViewer} that should be the target of ImageJ macros in an application
     * 
     * @param applicationId The application id
     * @param imageViewer The {@link ImageViewer}
     * @see #warnForContentChange(ImageViewer)
     */
    public static synchronized void setActivatedImageViewer(String applicationId, ImageViewer imageViewer) {
        changeReferenceTarget(applicationId, imageViewer, ACTIVATED_VIEWERS);
    }

    /**
     * Sets the {@link IImageJPluginListManager} that should be reactivated in an application after each
     * {@link ImageViewer} change
     * 
     * @param applicationId The application id
     * @param manager The {@link IImageJPluginListManager}
     * @see #warnForContentChange(ImageViewer)
     */
    public static synchronized void setActivatedPluginListManager(String applicationId,
            IImageJPluginListManager manager) {
        changeReferenceTarget(applicationId, manager, PLUGIN_MANAGERS);
    }

    private static <K, O> void changeReferenceTarget(K key, O target, Map<K, WeakReference<O>> map) {
        if ((key != null) && (target != null) && (map != null)) {
            synchronized (map) {
                O previousTarget = null;
                WeakReference<O> previous = map.get(key);
                if (previous != null) {
                    previousTarget = previous.get();
                }
                if (target != previousTarget) {
                    map.put(key, new WeakReference<O>(target));
                }
            }
        }
    }

    /**
     * <p>
     * Warns about some changes in an {@link ImageViewer}, i.e. about the fact that the {@link ImageViewer} may have
     * interacted with ImageJ and changed some static access.
     * </p>
     * 
     * <p>
     * This method is called by the {@link ImageViewer} itself.
     * </p>
     * 
     * <p>
     * When this method is called, the right {@link ImageViewer} and {@link IImageJPluginListManager} are reactivated
     * </p>
     * 
     * @param viewer The {@link ImageViewer} that changed
     */
    public static synchronized void warnForContentChange(ImageViewer viewer) {
        if (viewer != null) {
            String idToUse;
            if (APPLICATION_LOCK.availablePermits() == 0) {
                idToUse = lastLock;
            } else {
                idToUse = viewer.getApplicationId();
            }
            updateImageJ(idToUse);
        }
    }

    /**
     * Cleans a {@link ConcurrentMap} for useless {@link WeakReference}s
     * 
     * @param <K> The Map keys
     * @param <W> The type of {@link WeakReference}
     * @param map The {@link ConcurrentMap}
     */
    private static <K, W extends WeakReference<?>> void cleanDeadReferences(ConcurrentMap<K, W> map) {
        if (map != null) {
            for (Entry<K, W> entry : map.entrySet()) {
                K key = entry.getKey();
                W data = entry.getValue();
                if ((key != null) && (data != null)) {
                    Object value = data.get();
                    if (value == null) {
                        map.remove(key, data);
                    }
                }
            }
        }
    }

    /**
     * Generates a unique application id for an application name
     * 
     * @param applicationName The application name
     * @return A {@link String}
     */
    public static String generateApplicationId(String applicationName) {
        return CometeDefinitionUtils.generateRandomID(applicationName);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static class ImageJCleanerTask extends TimerTask {
        @Override
        public void run() {
            cleanDeadReferences(ACTIVATED_VIEWERS);
            cleanDeadReferences(PLUGIN_MANAGERS);
        }
    }

}
