/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import fr.soleil.comete.definition.data.information.BooleanInformation;
import fr.soleil.comete.definition.widget.IButton;

public class Button extends AbstractButton implements IButton, ActionListener {

    private static final long serialVersionUID = 1898841214057434804L;

    public Button() {
        super();
    }

    public Button(String text) {
        this();
        setText(text);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        long actualEvent = e.getWhen();
        if (oldEvent != actualEvent) {
            oldEvent = actualEvent;
            warnMediators(new BooleanInformation(this, true));
            fireActionPerformed(new EventObject(this));
        }
    }

}
