/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.plugin;

import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.ImageViewer;
import ij.process.ImageProcessor;

/**
 * A {@link CometePlugInFilter} that loads a {@link Mask} in an {@link ImageViewer}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LoadMaskAddPlugin extends CometePlugInFilter {

    public final static String COMMAND_NAME = "loadmaskAdd";

    @Override
    public void run(ImageProcessor imageProcessor) {
        if ((argument != null) && (!argument.trim().isEmpty())) {
            IMaskedImageViewer imageViewer = recoverImageContext();
            if (imageViewer != null) {
                imageViewer.loadMask(argument, true);
            }
        }
    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }

}
