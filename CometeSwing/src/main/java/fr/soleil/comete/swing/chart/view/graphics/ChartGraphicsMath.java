/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.graphics;

import fr.soleil.comete.swing.chart.axis.scale.AbstractAxisScale;
import fr.soleil.comete.swing.chart.axis.view.AbstractAxisView;
import fr.soleil.comete.swing.chart.data.DataList;
import fr.soleil.comete.swing.chart.data.DataXY;
import fr.soleil.comete.swing.chart.data.IDataListContainer;
import fr.soleil.lib.project.math.MathConst;

public class ChartGraphicsMath {

    private ChartGraphicsMath() {
        // hide constructor
    }

    public static double getTransformedValue(double value, double a0, double a1, double a2) {
        return a0 + a1 * value + a2 * value * value;
    }

    public static double getXValue(IDataListContainer l, double a0, double a1, double a2, boolean isXY) {
        double vtx;
        if (isXY) {
            vtx = getTransformedValue(((DataXY) l).getDataX().getY(), a0, a1, a2);
        } else {
            vtx = getTransformedValue(l.getDataY().getX(), a0, a1, a2);
        }
        return vtx;
    }

    public static double getYValue(IDataListContainer l, double a0, double a1, double a2) {
        double transformedValue;
        DataList list;
        if (l == null) {
            list = null;
        } else {
            list = l.getDataY();
        }
        if (list == null) {
            transformedValue = MathConst.NAN_FOR_NULL;
        } else {
            transformedValue = getTransformedValue(list.getY(), a0, a1, a2);
        }
        return transformedValue;
    }

    public static double getMaxError(double... coordinates) {
        double error;
        if ((coordinates == null) || coordinates.length < 4) {
            error = Double.NaN;
        } else if (Double.isNaN(coordinates[0]) || Double.isInfinite(coordinates[0])) {
            error = Double.NaN;
        } else if (Double.isNaN(coordinates[2]) || Double.isInfinite(coordinates[2])) {
            if (Double.isNaN(coordinates[3]) || Double.isInfinite(coordinates[3])) {
                error = Double.NaN;
            } else {
                error = Math.abs(coordinates[0] - coordinates[3]);
            }
        } else if (Double.isNaN(coordinates[3]) || Double.isInfinite(coordinates[3])) {
            error = Math.abs(coordinates[0] - coordinates[2]);
        } else {
            error = Math.max(Math.abs(coordinates[0] - coordinates[2]), Math.abs(coordinates[0] - coordinates[3]));
        }
        return error;
    }

    public static double extractError(DataList yData) {
        return yData == null ? MathConst.NAN_FOR_NULL : yData.getError();
    }

    /**
     * Fills an array with {@link IDataListContainer}'s basic coordinates
     * 
     * @param basicCoordinates The array of basic coordinates, expected to be of length 4, and organized that way:
     *            <ol start="0">
     *            <li>y</li>
     *            <li>x</li>
     *            <li>errorMin</li>
     *            <li>errorMax</li>
     *            </ol>
     * @param l The {@link IDataListContainer}
     * @param a0x transformation a0 on x scale
     * @param a1x transformation a1 on x scale
     * @param a2x transformation a2 on x scale
     * @param a0y transformation a0 on y scale
     * @param a1y transformation a1 on y scale
     * @param a2y transformation a2 on y scale
     * @param isXY whether to use xy mode
     */
    public static void computeBasicCoordinates(double[] basicCoordinates, IDataListContainer l, double a0x, double a1x,
            double a2x, double a0y, double a1y, double a2y, boolean isXY) {
        if (l == null) {
            basicCoordinates[0] = MathConst.NAN_FOR_NULL;
            basicCoordinates[1] = basicCoordinates[0];
            basicCoordinates[2] = basicCoordinates[0];
            basicCoordinates[3] = basicCoordinates[0];
        } else {
            DataList yData = l.getDataY();
            if (yData == null) {
                basicCoordinates[0] = MathConst.NAN_FOR_NULL;
                basicCoordinates[2] = basicCoordinates[0];
                basicCoordinates[3] = basicCoordinates[0];
            } else {
                basicCoordinates[0] = getTransformedValue(yData.getY(), a0y, a1y, a2y);
                // Consider a NaN (undefined) error as "no error".
                double error = yData.getError();
                if (Double.isNaN(error)) {
                    error = 0;
                }
                basicCoordinates[2] = getTransformedValue(yData.getY() - error, a0y, a1y, a2y);
                basicCoordinates[3] = getTransformedValue(yData.getY() + error, a0y, a1y, a2y);
            }
            if (isXY) {
                DataList xy = ((DataXY) l).getDataX();
                if (xy == null) {
                    basicCoordinates[1] = MathConst.NAN_FOR_NULL;
                } else {
                    basicCoordinates[1] = getTransformedValue(xy.getY(), a0x, a1x, a2x);
                }
            } else if (yData == null) {
                basicCoordinates[1] = MathConst.NAN_FOR_NULL;
            } else {
                basicCoordinates[1] = getTransformedValue(yData.getX(), a0x, a1x, a2x);
            }
        }
    }

    /**
     * Returns whether a value is valid for a scale. An invalid value, whether it is <code>NaN</code> or not, can always
     * be
     * associated to <code>NaN</code> for that scale.
     * 
     * @param value The value.
     * @param scale The scale.
     * @return A <code>boolean</code>: whether value is valid for given scale.
     */
    public static boolean isValidValue(double value, AbstractAxisScale scale) {
        return (!Double.isNaN(value)) && (value > scale.getMinimumCompatibleValue());
    }

    /**
     * Returns whether given coordinates and index are valid. If not valid, it can be associated to <code>NaN</code>.
     * 
     * @param basicCoordinates The coordinates <i>(<code>{y, x, errorMin, errorMax}</code>)</i>.
     * 
     * @param index The index.
     * @param nbPoint The number of points to compare index with.
     * @param xAxis The x axis.
     * @param yAxis The y axis.
     * @return A <code>boolean</code>: whether coordinates and index are valid.
     */
    public static boolean isValid(double[] basicCoordinates, int index, int nbPoint, AbstractAxisView xAxis,
            AbstractAxisView yAxis) {
        return isValidValue(basicCoordinates[0], yAxis.getScale())
                && isValidValue(basicCoordinates[1], xAxis.getScale()) && (index < nbPoint);
    }

    /**
     * Returns whether given coordinates and index are valid for given axis.
     * 
     * @param basicCoordinates The coordinates <i>(<code>{y, x, errorMin, errorMax}</code>)</i>.
     * @param index The index.
     * @param nbPoint The number of points to compare index with.
     * @param xAxis The x axis.
     * @param yAxis The y axis.
     * @return A <code>boolean</code>: whether coordinates and index are valid.
     */
    public static boolean isValidAllY(double[] basicCoordinates, int index, int nbPoint, AbstractAxisView xAxis,
            AbstractAxisView yAxis) {
        return (isValidValue(basicCoordinates[0], yAxis.getScale())
                || isValidValue(basicCoordinates[2], yAxis.getScale())
                || isValidValue(basicCoordinates[3], yAxis.getScale()))
                && isValidValue(basicCoordinates[1], xAxis.getScale()) && (index < nbPoint);
    }

}
