/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Color;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.widget.IChartPlayer;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.DragProperties;
import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.comete.definition.widget.properties.OffsetProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.util.IDataViewManager;
import fr.soleil.comete.swing.stackviewer.StackViewer;

/**
 * Swing implementation of {@link IChartPlayer}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ChartPlayer extends StackViewer<IChartViewer> implements IChartPlayer, IDataViewManager {

    private static final long serialVersionUID = 5622909585835345525L;

    public ChartPlayer() {
        super();
    }

    @Override
    public Comparator<AbstractDataView> getDataViewComparator() {
        Comparator<AbstractDataView> comparator;
        IChartViewer viewer = getViewer();
        if (viewer instanceof IDataViewManager) {
            comparator = ((IDataViewManager) viewer).getDataViewComparator();
        } else {
            comparator = null;
        }
        return comparator;
    }

    @Override
    public void setDataViewComparator(Comparator<AbstractDataView> comparator) {
        IChartViewer viewer = getViewer();
        if (viewer instanceof IDataViewManager) {
            ((IDataViewManager) viewer).setDataViewComparator(comparator);
        }
    }

    @Override
    public void addChartViewerListener(IChartViewerListener listener) {
    }

    @Override
    public void fireDataChanged(Map<String, Object> data) {
    }

    @Override
    public void fireConfigurationChanged(String id) {
    }

    @Override
    public void removeChartViewerListener(IChartViewerListener listener) {
    }

    @Override
    public String getAxisName(int axis) {
        return getViewer().getAxisName(axis);
    }

    @Override
    public void setAxisName(final String name, final int axis) {
        getViewer().setAxisName(name, axis);
    }

    @Override
    public int getAxisTitleAlignment(final int axis) {
        return getViewer().getAxisTitleAlignment(axis);
    }

    @Override
    public void setAxisTitleAlignment(final int align, final int axis) {
        getViewer().setAxisTitleAlignment(align, axis);
    }

    @Override
    public void setAxisCometeColor(final CometeColor color, final int axis) {
        getViewer().setAxisCometeColor(color, axis);
    }

    @Override
    public void setGridStyle(final int style, final int axis) {
        getViewer().setGridStyle(style, axis);
    }

    @Override
    public void setAxisDateFormat(final String format, final int axis) {
        getViewer().setAxisDateFormat(format, axis);
    }

    @Override
    public void setSubGridVisible(boolean visible, int axis) {
        getViewer().setSubGridVisible(visible, axis);
    }

    @Override
    public void setGridVisible(boolean visible, int axis) {
        getViewer().setGridVisible(visible, axis);
    }

    @Override
    public void setXAxisOnBottom(boolean b) {
        getViewer().setXAxisOnBottom(b);
    }

    @Override
    public boolean isManagementPanelVisible() {
        return getViewer().isManagementPanelVisible();
    }

    @Override
    public void setManagementPanelVisible(boolean managementPanelVisible) {
        getViewer().setManagementPanelVisible(managementPanelVisible);
    }

    @Override
    public void setEditable(boolean editable) {
        getViewer().setEditable(editable);
    }

    @Override
    public boolean isEditable() {
        return getViewer().isEditable();
    }

    @Override
    public double getDisplayDuration() {
        return getViewer().getDisplayDuration();
    }

    @Override
    public String getHeader() {
        return getViewer().getHeader();
    }

    @Override
    public CometeFont getHeaderCometeFont() {
        return getViewer().getHeaderCometeFont();
    }

    @Override
    public CometeColor getHeaderCometeColor() {
        return getViewer().getHeaderCometeColor();
    }

    @Override
    public int getTimePrecision() {
        return getViewer().getTimePrecision();
    }

    @Override
    public boolean isFreezePanelVisible() {
        return getViewer().isFreezePanelVisible();
    }

    @Override
    public boolean isLegendVisible() {
        return getViewer().isLegendVisible();
    }

    @Override
    public CometeFont getLabelCometeFont() {
        return getViewer().getLabelCometeFont();
    }

    @Override
    public boolean isXAxisOnBottom() {
        return getViewer().isXAxisOnBottom();
    }

    @Override
    public void setAutoScale(boolean autoscale, int axis) {
        getViewer().setAutoScale(autoscale, axis);
    }

    @Override
    public void setDataViewStyle(String name, int style) {
        getViewer().setDataViewStyle(name, style);
    }

    @Override
    public void setDataViewFillStyle(String name, int style) {
        getViewer().setDataViewFillStyle(name, style);
    }

    @Override
    public void setDataViewLineWidth(String name, int width) {
        getViewer().setDataViewLineWidth(name, width);
    }

    @Override
    public void setDataViewBarWidth(String name, int width) {
        getViewer().setDataViewBarWidth(name, width);
    }

    @Override
    public void setDataViewLineStyle(String name, int style) {
        getViewer().setDataViewLineStyle(name, style);
    }

    @Override
    public void setDataViewCometeColor(String name, CometeColor color) {
        getViewer().setDataViewCometeColor(name, color);
    }

    @Override
    public void setDataViewFillCometeColor(String name, CometeColor color) {
        getViewer().setDataViewFillCometeColor(name, color);
    }

    @Override
    public void setDataViewMarkerStyle(String name, int marker) {
        getViewer().setDataViewMarkerStyle(name, marker);
    }

    @Override
    public void setDataViewMarkerCometeColor(String name, CometeColor color) {
        getViewer().setDataViewMarkerCometeColor(name, color);
    }

    @Override
    public void setDataViewMarkerSize(String name, int size) {
        getViewer().setDataViewMarkerSize(name, size);
    }

    @Override
    public void setTimePrecision(int precision) {
        getViewer().setTimePrecision(precision);
    }

    @Override
    public boolean isHeaderVisible() {
        return getViewer().isHeaderVisible();
    }

    @Override
    public void setDataViewAxis(String name, int axis) {
        getViewer().setDataViewAxis(name, axis);
    }

    @Override
    public void setFormat(String id, String format) {
        getViewer().setFormat(id, format);
    }

    @Override
    public void setDataViewUnit(String id, String unit) {
        getViewer().setDataViewUnit(id, unit);
    }

    @Override
    public void setDateColumnName(String dateColumnName) {
        getViewer().setDateColumnName(dateColumnName);
    }

    @Override
    public String getDateColumnName() {
        return getViewer().getDateColumnName();
    }

    @Override
    public void setFreezePanelVisible(boolean freezePanelVisible) {
        getViewer().setFreezePanelVisible(freezePanelVisible);
    }

    @Override
    public void setHeader(String s) {
        getViewer().setHeader(s);
    }

    @Override
    public void setHeaderCometeColor(CometeColor c) {
        getViewer().setHeaderCometeColor(c);
    }

    @Override
    public void setHeaderCometeFont(CometeFont f) {
        getViewer().setHeaderCometeFont(f);
    }

    @Override
    public void setHeaderVisible(boolean b) {
        getViewer().setHeaderVisible(b);
    }

    @Override
    public void setIndexColumnName(String name) {
        getViewer().setIndexColumnName(name);
    }

    @Override
    public void setLabelCometeFont(CometeFont f) {
        getViewer().setLabelCometeFont(f);
    }

    @Override
    public void setLegendVisible(boolean b) {
        getViewer().setLegendVisible(b);
    }

    @Override
    public void setScale(int scale, int axis) {
        getViewer().setScale(scale, axis);
    }

    @Override
    public Map<String, Object> getData() {
        return getViewer().getData();
    }

    @Override
    public void setData(Map<String, Object> data) {
        getViewer().setData(data);
    }

    @Override
    public void addData(Map<String, Object> dataToAdd) {
        getViewer().addData(dataToAdd);
    }

    @Override
    public void removeData(Collection<String> idsToRemove) {
        getViewer().removeData(idsToRemove);
    }

    @Override
    public void setPreferDialogForTable(boolean preferDialog, boolean modal) {
        getViewer().setPreferDialogForTable(preferDialog, modal);
    }

    public Color getAxisColor(int axis) {
        return ColorTool.getColor(getViewer().getAxisProperties(axis).getColor());
    }

    @Override
    public String getAxisDateFormat(int axis) {
        return getViewer().getAxisDateFormat(axis);
    }

    @Override
    public int getDataViewAxis(String name) {
        return getViewer().getDataViewAxis(name);
    }

    @Override
    public String getFormat(String id) {
        return getViewer().getFormat(id);
    }

    @Override
    public String getDataViewUnit(String id) {
        return getViewer().getDataViewUnit(id);
    }

    public Color getDataViewColor(String name) {
        return ColorTool.getColor(getViewer().getDataViewCometeColor(name));
    }

    @Override
    public String getDataViewDisplayName(String name) {
        return getViewer().getDataViewDisplayName(name);
    }

    @Override
    public int getDataViewFillStyle(String name) {
        return getViewer().getDataViewFillStyle(name);
    }

    @Override
    public int getDataViewLineStyle(String name) {
        return getViewer().getDataViewLineStyle(name);
    }

    @Override
    public int getDataViewLineWidth(String name) {
        return getViewer().getDataViewLineWidth(name);
    }

    @Override
    public int getDataViewBarWidth(String name) {
        return getViewer().getDataViewBarWidth(name);
    }

    public Color getDataViewMarkerColor(String name) {
        return ColorTool.getColor(getViewer().getDataViewMarkerCometeColor(name));
    }

    @Override
    public int getDataViewMarkerSize(String name) {
        return getViewer().getDataViewMarkerSize(name);
    }

    @Override
    public int getDataViewMarkerStyle(String name) {
        return getViewer().getDataViewMarkerStyle(name);
    }

    @Override
    public int getDataViewStyle(String name) {
        return getViewer().getDataViewStyle(name);
    }

    @Override
    public int getGridStyle(int axis) {
        return getViewer().getGridStyle(axis);
    }

    @Override
    public String getIndexColumnName() {
        return getViewer().getIndexColumnName();
    }

    @Override
    public int getScale(int axis) {
        return getViewer().getScale(axis);
    }

    @Override
    public boolean isGridVisible(int axis) {
        return getViewer().isGridVisible(axis);
    }

    @Override
    public boolean isSubGridVisible(int axis) {
        return getViewer().isSubGridVisible(axis);
    }

    @Override
    public void setDataViewDisplayName(String name, String displayName) {
        getViewer().setDataViewDisplayName(name, displayName);
    }

    @Override
    public void setDisplayDuration(double duration) {
        getViewer().setDisplayDuration(duration);
    }

    @Override
    public CometeColor getAxisCometeColor(int axis) {
        return getViewer().getAxisCometeColor(axis);
    }

    @Override
    public CometeColor getDataViewCometeColor(String name) {
        return getViewer().getDataViewCometeColor(name);
    }

    @Override
    public CometeColor getDataViewFillCometeColor(String name) {
        return getViewer().getDataViewFillCometeColor(name);
    }

    @Override
    public CometeColor getDataViewMarkerCometeColor(String name) {
        return getViewer().getDataViewMarkerCometeColor(name);
    }

    @Override
    public boolean isMathExpressionEnabled() {
        return getViewer().isMathExpressionEnabled();
    }

    @Override
    public void setMathExpressionEnabled(boolean enable) {
        getViewer().setMathExpressionEnabled(enable);
    }

    @Override
    public CometeColor getChartCometeBackground() {
        return getViewer().getChartCometeBackground();
    }

    @Override
    public void setChartCometeBackground(CometeColor c) {
        getViewer().setChartCometeBackground(c);
    }

    @Override
    public void setLabelPlacement(int p) {
        getViewer().setLabelPlacement(p);
    }

    @Override
    public int getLabelPlacement() {
        return getViewer().getLabelPlacement();
    }

    @Override
    public double getLegendProportion() {
        return getViewer().getLegendProportion();
    }

    @Override
    public void setLegendProportion(double p) {
        getViewer().setLegendProportion(p);
    }

    @Override
    public void setNoValueString(String noValueString) {
        getViewer().setNoValueString(noValueString);
    }

    @Override
    public String getNoValueString() {
        return getViewer().getNoValueString();
    }

    @Override
    public void setAxisMaximum(final double maximum, final int axis) {
        getViewer().setAxisMaximum(maximum, axis);
    }

    @Override
    public double getAxisMaximum(final int axis) {
        return getViewer().getAxisMaximum(axis);
    }

    @Override
    public void setAxisMinimum(final double minimum, final int axis) {
        getViewer().setAxisMinimum(minimum, axis);
    }

    @Override
    public double getAxisMinimum(final int axis) {
        return getViewer().getAxisMinimum(axis);
    }

    @Override
    public void setAxisDrawOpposite(boolean opposite, final int axis) {
        getViewer().setAxisDrawOpposite(opposite, axis);
    }

    @Override
    public boolean isAxisDrawOpposite(final int axis) {
        return getViewer().isAxisDrawOpposite(axis);
    }

    @Override
    public void setAxisLabelFormat(int format, final int axis) {
        getViewer().setAxisLabelFormat(format, axis);
    }

    @Override
    public int getAxisLabelFormat(final int axis) {
        return getViewer().getAxisLabelFormat(axis);
    }

    @Override
    public void setAxisPosition(int position, final int axis) {
        getViewer().setAxisPosition(position, axis);
    }

    @Override
    public int getAxisPosition(final int axis) {
        return getViewer().getAxisPosition(axis);
    }

    @Override
    public void setAxisVisible(boolean visible, final int axis) {
        getViewer().setAxisVisible(visible, axis);
    }

    @Override
    public boolean isAxisVisible(final int axis) {
        return getViewer().isAxisVisible(axis);
    }

    @Override
    public boolean isTimeScale(int axis) {
        return getViewer().isTimeScale(axis);
    }

    @Override
    public void setDataViewFillMethod(String name, int style) {
        getViewer().setDataViewFillMethod(name, style);
    }

    @Override
    public int getDataViewFillMethod(String name) {
        return getViewer().getDataViewFillMethod(name);
    }

    @Override
    public void setCustomCometeColor(CometeColor[] colorList) {
        getViewer().setCustomCometeColor(colorList);
    }

    @Override
    public CometeColor[] getCustomCometeColor() {
        return getViewer().getCustomCometeColor();
    }

    @Override
    public void setCustomFillCometeColor(CometeColor[] colorList) {
        getViewer().setCustomFillCometeColor(colorList);
    }

    @Override
    public CometeColor[] getCustomFillCometeColor() {
        return getViewer().getCustomFillCometeColor();
    }

    @Override
    public void setCustomMarkerCometeColor(CometeColor[] colorList) {
        getViewer().setCustomMarkerCometeColor(colorList);
    }

    @Override
    public CometeColor[] getCustomMarkerCometeColor() {
        return getViewer().getCustomMarkerCometeColor();
    }

    @Override
    public double getDataViewTransformA0(String name) {
        return getViewer().getDataViewTransformA0(name);
    }

    @Override
    public void setDataViewTransformA0(String name, double transform) {
        getViewer().setDataViewTransformA0(name, transform);
    }

    @Override
    public double getDataViewTransformA1(String name) {
        return getViewer().getDataViewTransformA1(name);
    }

    @Override
    public void setDataViewTransformA1(String name, double transform) {
        getViewer().setDataViewTransformA1(name, transform);
    }

    @Override
    public double getDataViewTransformA2(String name) {
        return getViewer().getDataViewTransformA2(name);
    }

    @Override
    public void setDataViewTransformA2(String name, double transform) {
        getViewer().setDataViewTransformA2(name, transform);
    }

    @Override
    public boolean isDataViewClickable(String name) {
        return getViewer().isDataViewClickable(name);
    }

    @Override
    public void setDataViewClickable(String name, boolean clickable) {
        getViewer().setDataViewClickable(name, clickable);
    }

    @Override
    public boolean isDataViewLabelVisible(String visible) {
        return getViewer().isDataViewLabelVisible(visible);
    }

    @Override
    public void setDataViewLabelVisible(String name, boolean visible) {
        getViewer().setDataViewLabelVisible(name, visible);
    }

    @Override
    public void addExpression(String dataViewName, String expression, int axis, String[] variables, boolean x) {
        getViewer().addExpression(dataViewName, expression, axis, variables, x);
    }

    @Override
    public void addExpression(String id, String name, String expression, int axis, String[] variables, boolean x) {
        getViewer().addExpression(id, name, expression, axis, variables, x);
    }

    @Override
    public int getDataViewInterpolationMethod(String name) {
        return getViewer().getDataViewInterpolationMethod(name);
    }

    @Override
    public void setDataViewInterpolationMethod(String name, int method) {
        getViewer().setDataViewInterpolationMethod(name, method);
    }

    @Override
    public int getDataViewInterpolationStep(String name) {
        return getViewer().getDataViewInterpolationStep(name);
    }

    @Override
    public void setDataViewInterpolationStep(String name, int step) {
        getViewer().setDataViewInterpolationStep(name, step);
    }

    @Override
    public double getDataViewHermiteBias(String name) {
        return getViewer().getDataViewHermiteBias(name);
    }

    @Override
    public void setDataViewHermiteBias(String name, double bias) {
        getViewer().setDataViewHermiteBias(name, bias);
    }

    @Override
    public double getDataViewHermiteTension(String name) {
        return getViewer().getDataViewHermiteTension(name);
    }

    @Override
    public void setDataViewHermiteTension(String name, double tension) {
        getViewer().setDataViewHermiteTension(name, tension);
    }

    @Override
    public int getDataViewSmoothingMethod(String name) {
        return getViewer().getDataViewSmoothingMethod(name);
    }

    @Override
    public void setDataViewSmoothingMethod(String name, int method) {
        getViewer().setDataViewSmoothingMethod(name, method);
    }

    @Override
    public int getDataViewSmoothingNeighbors(String name) {
        return getViewer().getDataViewSmoothingNeighbors(name);
    }

    @Override
    public void setDataViewSmoothingNeighbors(String name, int neighbors) {
        getViewer().setDataViewSmoothingNeighbors(name, neighbors);
    }

    @Override
    public double getDataViewSmoothingGaussSigma(String name) {
        return getViewer().getDataViewSmoothingGaussSigma(name);
    }

    @Override
    public void setDataViewSmoothingGaussSigma(String name, double sigma) {
        getViewer().setDataViewSmoothingGaussSigma(name, sigma);
    }

    @Override
    public int getDataViewSmoothingExtrapolation(String name) {
        return getViewer().getDataViewSmoothingExtrapolation(name);
    }

    @Override
    public void setDataViewSmoothingExtrapolation(String name, int extrapolation) {
        getViewer().setDataViewSmoothingExtrapolation(name, extrapolation);
    }

    @Override
    public int getDataViewMathFunction(String name) {
        return getViewer().getDataViewMathFunction(name);
    }

    @Override
    public void setDataViewMathFunction(String name, int function) {
        getViewer().setDataViewMathFunction(name, function);
    }

    @Override
    public int getDataViewHighlightMethod(String name) {
        return getViewer().getDataViewHighlightMethod(name);
    }

    @Override
    public void setDataViewHighlightMethod(String name, int method) {
        getViewer().setDataViewHighlightMethod(name, method);
    }

    @Override
    public double getDataViewHighlightCoefficient(String name) {
        return getViewer().getDataViewHighlightCoefficient(name);
    }

    @Override
    public void setDataViewHighlightCoefficient(String name, double coef) {
        getViewer().setDataViewHighlightCoefficient(name, coef);
    }

    @Override
    public boolean isDataViewHighlighted(String name) {
        return getViewer().isDataViewHighlighted(name);
    }

    @Override
    public void setDataViewHighlighted(String name, boolean highlighted) {
        getViewer().setDataViewHighlighted(name, highlighted);
    }

    @Override
    public void setAutoHighlightOnLegend(boolean autoHighlightOnLegend) {
        getViewer().setAutoHighlightOnLegend(autoHighlightOnLegend);
    }

    @Override
    public boolean isAutoHighlightOnLegend() {
        return getViewer().isAutoHighlightOnLegend();
    }

    @Override
    public boolean isDataViewRemovingEnabled() {
        return getViewer().isDataViewRemovingEnabled();
    }

    @Override
    public void setDataViewRemovingEnabled(boolean curveRemovingEnabled) {
        getViewer().setDataViewRemovingEnabled(curveRemovingEnabled);
    }

    @Override
    public boolean isCleanDataViewConfigurationOnRemoving() {
        return getViewer().isCleanDataViewConfigurationOnRemoving();
    }

    @Override
    public void setCleanDataViewConfigurationOnRemoving(boolean cleanCurveConfigurationOnRemoving) {
        getViewer().setCleanDataViewConfigurationOnRemoving(cleanCurveConfigurationOnRemoving);
    }

    @Override
    public boolean hasDataViewProperties(String name) {
        return getViewer().hasDataViewProperties(name);
    }

    @Override
    public void cleanDataViewProperties(String name) {
        getViewer().cleanDataViewProperties(name);
    }

    @Override
    public PlotProperties getDataViewPlotProperties(String name) {
        return getViewer().getDataViewPlotProperties(name);
    }

    @Override
    public void setDataViewPlotProperties(String name, PlotProperties properties) {
        getViewer().setDataViewPlotProperties(name, properties);
    }

    @Override
    public BarProperties getDataViewBarProperties(String name) {
        return getViewer().getDataViewBarProperties(name);
    }

    @Override
    public void setDataViewBarProperties(String name, BarProperties properties) {
        getViewer().setDataViewBarProperties(name, properties);
    }

    @Override
    public CurveProperties getDataViewCurveProperties(String name) {
        return getViewer().getDataViewCurveProperties(name);
    }

    @Override
    public void setDataViewCurveProperties(String name, CurveProperties properties) {
        getViewer().setDataViewCurveProperties(name, properties);
    }

    @Override
    public MarkerProperties getDataViewMarkerProperties(String name) {
        return getViewer().getDataViewMarkerProperties(name);
    }

    @Override
    public void setDataViewMarkerProperties(String name, MarkerProperties properties) {
        getViewer().setDataViewMarkerProperties(name, properties);
    }

    @Override
    public ErrorProperties getDataViewErrorProperties(String name) {
        return getViewer().getDataViewErrorProperties(name);
    }

    @Override
    public void setDataViewErrorProperties(String name, ErrorProperties properties) {
        getViewer().setDataViewErrorProperties(name, properties);
    }

    @Override
    public TransformationProperties getDataViewTransformationProperties(String name) {
        return getViewer().getDataViewTransformationProperties(name);
    }

    @Override
    public void setDataViewTransformationProperties(String name, TransformationProperties properties) {
        getViewer().setDataViewTransformationProperties(name, properties);
    }

    @Override
    public InterpolationProperties getDataViewInterpolationProperties(String name) {
        return getViewer().getDataViewInterpolationProperties(name);
    }

    @Override
    public void setDataViewInterpolationProperties(String name, InterpolationProperties properties) {
        getViewer().setDataViewInterpolationProperties(name, properties);
    }

    @Override
    public SmoothingProperties getDataViewSmoothingProperties(String name) {
        return getViewer().getDataViewSmoothingProperties(name);
    }

    @Override
    public void setDataViewSmoothingProperties(String name, SmoothingProperties properties) {
        getViewer().setDataViewSmoothingProperties(name, properties);
    }

    @Override
    public MathProperties getDataViewMathProperties(String name) {
        return getViewer().getDataViewMathProperties(name);
    }

    @Override
    public void setDataViewMathProperties(String name, MathProperties properties) {
        getViewer().setDataViewMathProperties(name, properties);
    }

    @Override
    public ChartProperties getChartProperties() {
        return getViewer().getChartProperties();
    }

    @Override
    public void setChartProperties(ChartProperties properties) {
        getViewer().setChartProperties(properties);
    }

    @Override
    public AxisProperties getAxisProperties(int axisChoice) {
        return getViewer().getAxisProperties(axisChoice);
    }

    @Override
    public void setAxisProperties(AxisProperties properties, int axisChoice) {
        getViewer().setAxisProperties(properties, axisChoice);
    }

    @Override
    public SamplingProperties getSamplingProperties() {
        return getViewer().getSamplingProperties();
    }

    @Override
    public void setSamplingProperties(SamplingProperties properties) {
        getViewer().setSamplingProperties(properties);
    }

    @Override
    public int getDataViewOffsetDragType() {
        return getViewer().getDataViewOffsetDragType();
    }

    @Override
    public void setDataViewOffsetDragType(int dataViewOffsetDragType) {
        getViewer().setDataViewOffsetDragType(dataViewOffsetDragType);
    }

    @Override
    public int getScaleDragType() {
        return getViewer().getScaleDragType();
    }

    @Override
    public void setScaleDragType(int scaleDragType) {
        getViewer().setScaleDragType(scaleDragType);
    }

    @Override
    public int getScaleDragSensitivity() {
        return getViewer().getScaleDragSensitivity();
    }

    @Override
    public void setScaleDragSensitivity(int scaleDragSensitivity) {
        getViewer().setScaleDragSensitivity(scaleDragSensitivity);
    }

    @Override
    public DragProperties getDragProperties() {
        return getViewer().getDragProperties();
    }

    @Override
    public void setDragProperties(DragProperties properties) {
        getViewer().setDragProperties(properties);
    }

    @Override
    public void resetAll() {
        getViewer().resetAll();
    }

    @Override
    public void resetAll(boolean clearConfiguration) {
        getViewer().resetAll(clearConfiguration);
    }

    @Override
    public boolean isCyclingCustomMap() {
        return getViewer().isCyclingCustomMap();
    }

    @Override
    public void setCyclingCustomMap(boolean cyclingCustomMap) {
        getViewer().setCyclingCustomMap(cyclingCustomMap);
    }

    @Override
    public boolean isAutoScale(int axis) {
        return getViewer().isAutoScale(axis);
    }

    @Override
    public void setCustomUserFormat(String[] formatList) {
        getViewer().setCustomUserFormat(formatList);
    }

    @Override
    public String[] getCustomUserFormat() {
        return getViewer().getCustomUserFormat();
    }

    @Override
    public boolean isAxisSelectionVisible() {
        return getViewer().isAxisSelectionVisible();
    }

    @Override
    public void setAxisSelectionVisible(boolean axisSelectionVisible) {
        getViewer().setAxisSelectionVisible(axisSelectionVisible);
    }

    @Override
    public void setAxisScaleEditionEnabled(boolean enabled, int axis) {
        getViewer().setAxisScaleEditionEnabled(enabled, axis);
    }

    @Override
    public void setDataViewEditable(String name, boolean editable) {
        getViewer().setDataViewEditable(name, editable);
    }

    @Override
    public boolean containsDataView(String name) {
        return getViewer().containsDataView(name);
    }

    @Override
    public boolean isLimitDrawingZone() {
        return getViewer().isLimitDrawingZone();
    }

    @Override
    public void setLimitDrawingZone(boolean limitDrawingZone) {
        getViewer().setLimitDrawingZone(limitDrawingZone);
    }

    @Override
    public boolean isAutoHideViews() {
        return getViewer().isAutoHideViews();
    }

    @Override
    public void setAutoHideViews(boolean hide) {
        getViewer().setAutoHideViews(hide);
    }

    @Override
    public String[][] getAllViewsIdsAndNames() {
        return getViewer().getAllViewsIdsAndNames();
    }

    @Override
    protected IChartViewer generateComponent() {
        Chart chartViewer = new Chart();
        chartViewer.setFreezePanelVisible(true);
        chartViewer.setManagementPanelVisible(true);
        return chartViewer;
    }

    @Override
    public void loadDataFile(String fileName) {
        getViewer().loadDataFile(fileName);
    }

    @Override
    public void saveDataFile(String path) {
        getViewer().saveDataFile(path);
    }

    @Override
    public String getDataDirectory() {
        return getViewer().getDataDirectory();
    }

    @Override
    public void setDataDirectory(String path) {
        getViewer().setDataDirectory(path);
    }

    @Override
    public String getDataFile() {
        return getViewer().getDataFile();
    }

    @Override
    public void addDataTransferListener(IDataTransferListener listener) {
        getViewer().addDataTransferListener(listener);
    }

    @Override
    public void removeDataTransferListener(IDataTransferListener listener) {
        getViewer().removeDataTransferListener(listener);
    }

    @Override
    public void setDataViewErrorCometeColor(String id, CometeColor color) {
        getViewer().setDataViewErrorCometeColor(id, color);
    }

    @Override
    public CometeColor getDataViewErrorCometeColor(String id) {
        return getViewer().getDataViewErrorCometeColor(id);
    }

    @Override
    public boolean isDataViewErrorVisible(String id) {
        return getViewer().isDataViewErrorVisible(id);
    }

    @Override
    public void setDataViewErrorVisible(String id, boolean visible) {
        getViewer().setDataViewErrorVisible(id, visible);
    }

    @Override
    public boolean isDataViewSamplingAllowed(String name) {
        return getViewer().isDataViewSamplingAllowed(name);
    }

    @Override
    public void setDataViewSamplingAllowed(String name, boolean allowed) {
        getViewer().setDataViewSamplingAllowed(name, allowed);
    }

    @Override
    public void setCustomLineStyle(int[] lineStyleList) {
        getViewer().setCustomLineStyle(lineStyleList);
    }

    @Override
    public int[] getCustomLineStyle() {
        return getViewer().getCustomLineStyle();
    }

    @Override
    public void setCustomMarkerStyle(int[] styleList) {
        getViewer().setCustomMarkerStyle(styleList);
    }

    @Override
    public int[] getCustomMarkerStyle() {
        return getViewer().getCustomMarkerStyle();
    }

    @Override
    public void setCustomMarkerSize(int[] sizeList) {
        getViewer().setCustomMarkerSize(sizeList);
    }

    @Override
    public int[] getCustomMarkerSize() {
        return getViewer().getCustomMarkerSize();
    }

    @Override
    public void setCustomLineWidth(int[] widthList) {
        getViewer().setCustomLineWidth(widthList);
    }

    @Override
    public int[] getCustomLineWidth() {
        return getViewer().getCustomLineWidth();
    }

    @Override
    public void setCustomBarWidth(int[] widthList) {
        getViewer().setCustomBarWidth(widthList);
    }

    @Override
    public int[] getCustomBarWidth() {
        return getViewer().getCustomBarWidth();
    }

    @Override
    public void setCustomDataViewStyle(int[] styleList) {
        getViewer().setCustomDataViewStyle(styleList);
    }

    @Override
    public int[] getCustomDataViewStyle() {
        return getViewer().getCustomDataViewStyle();
    }

    @Override
    public void setCustomAxis(int[] axisList) {
        getViewer().setCustomAxis(axisList);
    }

    @Override
    public int[] getCustomAxis() {
        return getViewer().getCustomAxis();
    }

    @Override
    public void setCustomFillStyle(int[] styleList) {
        getViewer().setCustomFillStyle(styleList);
    }

    @Override
    public int[] getCustomFillStyle() {
        return getViewer().getCustomFillStyle();
    }

    @Override
    public void setLabels(int axis, String[] labels, double[] labelPositions) {
        getViewer().setLabels(axis, labels, labelPositions);

    }

    @Override
    public void clearLabels(int axis) {
        getViewer().clearLabels(axis);
    }

    @Override
    public double getDataViewXOffsetA0(String id) {
        return getViewer().getDataViewXOffsetA0(id);
    }

    @Override
    public void setDataViewXOffsetA0(String id, double a0x) {
        getViewer().setDataViewXOffsetA0(id, a0x);
    }

    @Override
    public OffsetProperties getDataViewXOffsetProperties(String name) {
        return getViewer().getDataViewXOffsetProperties(name);
    }

    @Override
    public void setDataViewXOffsetProperties(String id, OffsetProperties properties) {
        getViewer().setDataViewXOffsetProperties(id, properties);
    }

    /**
     * Main class, so you can have an example. You can monitor your own
     * attribute by giving its full path name in argument
     */
    public static void main(String[] args) {
        IPanel panel = new Panel();
        ChartPlayer component = new ChartPlayer();
        panel.add(component);
        IFrame frame = new Frame();
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setTitle("ChartPlayer test");
    }

}
