/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;

import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.file.MultiExtFileFilter;

//JFileChooser with at least one filter

public class JFileChooserFilter extends JFileChooser {

    private static final long serialVersionUID = -8423172784733034306L;

    public JFileChooserFilter(String title_p, String extension_p, String description_p) {
        super(ChartUtils.DOT);
        setDialogTitle(title_p);
        addFilter(extension_p, description_p);
    }

    /**
     * the extension has to be given without "."
     * 
     * @param extension_p the extension for the files
     * @param description_p the description seen in the chooser
     */
    public void addFilter(String extension_p, String description_p) {
        addChoosableFileFilter(new MultiExtFileFilter(description_p, extension_p));
    }

    public String showChooserDialog(Component parent_p) {
        String fileName = ObjectUtils.EMPTY_STRING;

        int returnVal = super.showOpenDialog(parent_p);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = getSelectedFile();
            if (f != null) {
                fileName = f.getAbsolutePath();
            }
        }

        return fileName;
    }
}
