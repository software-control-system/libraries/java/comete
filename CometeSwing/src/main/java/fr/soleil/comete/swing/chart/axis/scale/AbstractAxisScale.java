/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.axis.scale;

import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.IChartConst.Orientation;
import fr.soleil.comete.swing.chart.axis.model.AxisAttributes;
import fr.soleil.comete.swing.chart.axis.model.AxisLabel;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateFormattable;

/**
 * Class that does all the calculation for an axis, to be able to set its labels and ticks positions
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AbstractAxisScale {

    public static final double DEFAULT_MINIMUM = 0;
    public static final double DEFAULT_MAXIMUM = 100;
    protected static final double LABEL_INTERVAL = 5;
    protected static final double MINIMUM_DIFFERENCE = 1E-13;
    protected static final int MAX_LABELS = 30;

    protected static final String MEASURABLE_STRING = "0";

    protected double scaleMinimum;
    protected double scaleMaximum;
    protected AxisLabel[] labels;
    protected double userLabelInterval;
    protected double tickStep; // In pixel
    protected int subTickStep; // 0 => NONE, -1 => Log step, 1.. => Linear step
    protected AxisAttributes attributes;
    protected SimpleDateFormat usedFormat;
    protected boolean preciseLabel;
    protected double desiredPrecision;
    protected double startPrecision;
    protected boolean zoomed;
    protected boolean autoScale;
    protected final GregorianCalendar calendar;
    protected boolean timeScale;
    protected volatile boolean defaultScaleBounds;

    protected static final int DEFAULT_BAR_WIDTH = 20;

    public AbstractAxisScale(AxisAttributes attributes) {
        calendar = new GregorianCalendar();
        this.attributes = attributes;
        scaleMinimum = DEFAULT_MINIMUM;
        scaleMaximum = DEFAULT_MAXIMUM;
        labels = new AxisLabel[0];
        userLabelInterval = 0;
        usedFormat = ChartUtils.YEAR_FORMAT;
        preciseLabel = false;
        desiredPrecision = 0;
        startPrecision = 0;
        zoomed = false;
        updateScaleBoundsFromAttributes();
        defaultScaleBounds = true;
    }

    // JIRA JAVAAPI-39
    public void setLabels(String[] labels, double[] labelPos) {
        if ((labels == null) || (labelPos == null)) {
            attributes.clearUserLabels();
        } else {
            if (labels.length != labelPos.length) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .trace("AbstractAxisScale.setLabels() : labels and labelPos must have the same size");
            } else {
                attributes.setUserLabel(labels);
                // avoiding NullPointerExceptions
                if (attributes.getUserLabel() != null) {
                    for (int i = 0; i < attributes.getUserLabel().length; i++) {
                        if (attributes.getUserLabel()[i] == null) {
                            attributes.getUserLabel()[i] = String.valueOf(null);
                        }
                    }
                }
                attributes.setUserLabelPos(labelPos);
                attributes.setAutoLabeling(false);
            }
        }
    }

    public double getMinimumCompatibleValue() {
        return Double.NEGATIVE_INFINITY;
    }

    public void updateScaleBoundsFromAttributes() {
        autoScale = attributes.isAutoScale();
        if (!autoScale) {
            scaleMinimum = attributes.getMinimum();
            scaleMaximum = attributes.getMaximum();
            setLabelsFromUserInterval();
        }
    }

    protected boolean shouldUseStricltyPositiveValues() {
        return false;
    }

    /**
     * Extracts an {@link AbstractDataView}'s min and max x values
     * 
     * @param v
     *            The {@link AbstractDataView}
     * @return A <code>double[]</code>: <code>{min, max}</code>
     */
    protected double[] getMinMaxX(AbstractDataView v) {
        double[] mm;
        if (shouldUseStricltyPositiveValues()) {
            if (v.hasXTransform()) {
                mm = v.computeTransformedPositiveMinMaxX();
            } else {
                mm = v.computePositiveMinMaxX();
            }
        } else if (v.hasXTransform()) {
            mm = v.computeTransformedMinMaxX();
        } else {
            mm = new double[] { v.getMinXValue(), v.getMaxXValue() };
        }
        return mm;
    }

    /**
     * Extracts an {@link AbstractDataView}'s min and max y values
     * 
     * @param v The {@link AbstractDataView}
     * @return A <code>double[]</code>: <code>{min, max}</code>
     */
    protected double[] getMinMaxY(AbstractDataView v) {
        double[] mm;
        double min = v.getMinimum(), max = v.getMaximum();
        if (shouldUseStricltyPositiveValues()) {
            if (v.hasTransform()) {
                mm = v.computeTransformedPositiveMinMax();
            } else if ((min <= 0) || (max <= 0)) {
                mm = v.computePositiveMinMax();
            } else {
                mm = new double[] { min, max };
            }
        } else if (v.hasTransform()) {
            mm = v.computeTransformedMinMax();
        } else {
            mm = new double[] { min, max };
        }
        return mm;
    }

    /**
     * Expert usage. Computes labels and prepares ticks calculation.
     * 
     * @param frc Font render context
     * @param desiredLength Desired length
     * @param offset previously calculated label offset, according to tick alignment
     * @param views The views used to calculate scale
     */
    public void computeAxisItems(FontRenderContext frc, int desiredLength, int[] offset,
            Collection<? extends AbstractDataView> views) {
        computeAutoScale(views);
        if (attributes.isAutoLabeling()) {
            computeLabels(frc, desiredLength, offset);
        } else {
            computeUserLabels(frc, desiredLength, offset);
        }
    }

    public void setLabelsFromUserInterval() {
        double[] tableTest;
        String[] labels;
        if ((!autoScale) && (attributes.getLabelInterval() > 0)) {
            tableTest = CometeUtils.createLabelInterval(getScaleMaximum(), getScaleMinimum(),
                    attributes.getLabelInterval());
            labels = new String[tableTest.length];
            String format = attributes.getFormat();
            for (int i = 0; i < tableTest.length; i++) {
                labels[i] = ChartUtils.formatValue(tableTest[i], 0, attributes.getLabelFormat(), format);
            }
        } else {
            tableTest = null;
            labels = null;
        }
        attributes.setLabels(labels, tableTest);
    }

    /**
     * Computes ticks and sub-ticks positions, and stores them in a {@link Map}
     * 
     * @param axisOrigin The axis origin position
     * @param axisLength The axis length
     * @return A {@link Map} containing expected positions. Ticks or sub-ticks are identified by a {@link TickType}
     */
    public abstract Map<TickType, int[]> computeTicksPositions(int axisOrigin, int axisLength);

    /**
     * Expert usage. Compute X auto scale (HORIZONTAL axis only)
     * 
     * @param yViews All views displayed along all Y axis.
     * @param xViews All views displayed along X axis
     */
    public void computeXScale(Collection<? extends AbstractDataView> yViews, List<? extends AbstractDataView> xViews) {
        int viewCount = (yViews == null ? 0 : yViews.size());
        if (autoScale && (viewCount > 0)) {
            if ((xViews != null) && (xViews.size() > 0)) {
                // ******************************************************
                // XY monitoring
                computeAutoScale(xViews);
            } else {
                // ******************************************************
                // Classic monitoring
                scaleMinimum = Double.MAX_VALUE;
                scaleMaximum = -Double.MAX_VALUE;
                // Horizontal autoScale
                for (AbstractDataView v : yViews) {
                    double[] mm = getMinMaxX(v);
                    if (mm[1] > scaleMaximum) {
                        scaleMaximum = mm[1];
                    }
                    if (mm[0] < scaleMinimum) {
                        scaleMinimum = mm[0];
                    }
                }
                if ((scaleMinimum == Double.MAX_VALUE) && (scaleMaximum == -Double.MAX_VALUE)) {
                    // Only empty views !
                    computeDefaultXScaleBounds();
                    defaultScaleBounds = true;
                } else {
                    defaultScaleBounds = false;
                }
                analyseScaleType();
                if (isTimeScale()) {
                    if ((attributes.isFitXAxisToDisplayDuration()) && (!Double.isInfinite(attributes.getAxisDuration())
                            && (!Double.isNaN(attributes.getAxisDuration())))) {
                        scaleMinimum = scaleMaximum - attributes.getAxisDuration();
                    }
                    scaleMaximum += (scaleMaximum - scaleMinimum) * attributes.getPercentScrollback();
                }
                ensureMinimumDistanceBetweenMinAndMax();
            } // end if (isXY()) ... else
        } // end if (isHorizontal() && autoScale && viewCount > 0)
    }

    protected void analyseScaleType() {
        boolean calculatedTimeScale;
        switch (attributes.getLabelFormat()) {
            case IChartViewer.DATE_FORMAT:
            case IChartViewer.TIME_FORMAT:
                calculatedTimeScale = true;
                break;
            case IChartViewer.AUTO_FORMAT:
                calculatedTimeScale = (getUnScaledValue(scaleMinimum) >= IDateFormattable.REFERENCE_TIME);
                break;
            default:
                calculatedTimeScale = false;
                break;
        }
        this.timeScale = calculatedTimeScale;
    }

    public boolean isTimeScale() {
        return timeScale;
    }

    public boolean isDefaultScaleBounds() {
        return defaultScaleBounds;
    }

    /**
     * Computes X autoscale in case of empty views
     */
    protected abstract void computeDefaultXScaleBounds();

    /**
     * Calculates autoscale based an some {@link AbstractDataView} y values
     * 
     * @param views The {@link AbstractDataView}s
     */
    protected void computeAutoScale(Collection<? extends AbstractDataView> views) {
        int dataViewCount = (views == null ? 0 : views.size());
        if (autoScale && (dataViewCount > 0)) {
            scaleMinimum = Double.MAX_VALUE;
            scaleMaximum = -Double.MAX_VALUE;
            for (AbstractDataView v : views) {
                double[] mm = getMinMaxY(v);
                if (mm[1] > scaleMaximum) {
                    scaleMaximum = mm[1];
                }
                if (mm[0] < scaleMinimum) {
                    scaleMinimum = mm[0];
                }
            }
            // Check max and min
            if ((scaleMinimum == Double.MAX_VALUE) && (scaleMaximum == -Double.MAX_VALUE)) {
                // Only invalid data !!
                computeDefaultAutoScaleBounds();
                defaultScaleBounds = true;
            } else {
                if (attributes.isZeroAlwaysVisible()) {
                    if ((scaleMinimum < 0) && (scaleMaximum < 0)) {
                        scaleMaximum = 0;
                    } else if ((scaleMinimum > 0) && (scaleMaximum > 0)) {
                        scaleMinimum = 0;
                    }
                }
                defaultScaleBounds = false;
            }
            ensureMinimumDistanceBetweenMinAndMax();
            double precision = computeAutoScalePrecision();
            double tempMin = scaleMinimum, tempMax = scaleMaximum;
            scaleMinimum = Math.floor(tempMin / precision) * precision;
            scaleMaximum = Math.ceil(tempMax / precision) * precision;
            if (scaleMinimum == scaleMaximum) {
                scaleMinimum = (Math.floor(tempMin / precision) - 1) * precision;
                scaleMaximum = (Math.ceil(tempMax / precision) + 1) * precision;
            }
        }
        analyseScaleType();
    }

    /**
     * Calculates autoscale bounds in case of invalid or empty data
     */
    protected abstract void computeDefaultAutoScaleBounds();

    /**
     * Calculates the precision to use with autoscale
     * 
     * @return A <code>double</code> value
     */
    protected abstract double computeAutoScalePrecision();

    /**
     * Makes sure scale min and max are far enough from each other, to avoid
     * some dead locks in graphics
     */
    protected void ensureMinimumDistanceBetweenMinAndMax() {
        if (Math.abs(scaleMaximum - scaleMinimum) < 1e-100) {
            double delta = 0.999;
            if (scaleMaximum < scaleMinimum) {
                delta *= -1;
            }
            scaleMaximum += delta;
            scaleMinimum -= delta;
        }
    }

    protected void computeUserLabels(FontRenderContext frc, int length, int[] offset) {
        if (length > 0) {
            double scaleDelta = scaleMaximum - scaleMinimum;
            double precDelta = scaleDelta / length;
            AxisLabel[] temp = new AxisLabel[0];
            // Adjust labels offset according to tick
            int offX = offset[IChartViewer.X_INDEX];
            int offY = offset[IChartViewer.Y_INDEX];
            // Create labels
            if (attributes.getUserLabel() != null) {
                for (int i = 0; i < attributes.getUserLabel().length; i++) {
                    double upos = getUserLabelPosition(i);
                    if ((upos >= (scaleMinimum - precDelta)) && (upos <= (scaleMaximum + precDelta))) {
                        int pos = getPositionForCalculation(upos, length, scaleDelta);
                        if ((pos >= 0) && (pos < length)) {
                            Rectangle2D bounds = attributes.getLabelFont().getStringBounds(attributes.getUserLabel()[i],
                                    frc);
                            AxisLabel li = new AxisLabel(attributes.getUserLabel()[i], (int) bounds.getWidth(),
                                    (int) bounds.getHeight(), pos);
                            li.setOffset(offX, offY);
                            int previousLength = temp.length;
                            temp = Arrays.copyOf(temp, previousLength + 1);
                            temp[previousLength] = li;
                        }
                    }
                }
            }
            labels = temp;
        } // end if (length > 0)
    }

    /**
     * Recovers the user label position at a given index
     * 
     * @param index The index
     * @return A <code>double</code>
     */
    protected abstract double getUserLabelPosition(int index);

    // ****************************************************************
    // Compute labels
    // Expert usage
    protected void computeLabels(FontRenderContext frc, int length, int[] offset) {
        if (length > 0) {
            if (scaleMaximum < scaleMinimum) {
                double a = scaleMinimum;
                scaleMinimum = scaleMaximum;
                scaleMaximum = a;
            }
            labels = new AxisLabel[0];
            if (isTimeScale()) {
                computeLabelsForTimeAnnotation(frc, length);
            } else {
                computeLabelsForValueAnnotation(frc, length, offset);
            }
        }
    }

    // ***********************************************************

    protected boolean canAdaptFirstValueLabelToScaleMinimum() {
        return true;
    }

    protected void computeLabelsForValueAnnotation(final FontRenderContext frc, final int length, int[] offset) {
        AxisLabel[] temp = labels;
        if (length > 0) {
            String format = attributes.getFormat();
            double scaleDelta = scaleMaximum - scaleMinimum;
            double pos;
            String s = null;
            double startx;
            double prec;
            double precDelta = scaleDelta / length;
            Rectangle2D bounds;

            double fontAscent = (attributes.getLabelFont().getLineMetrics(MEASURABLE_STRING, frc).getAscent());
            prec = ChartUtils.computeLowTen(scaleDelta);

            // Anticipate label overlap
            int nbMaxLab = computeNbMaxLabel(frc, length);

            // Find the best precision
            double[] mathData = computeLabelDataForValueAnnotation(nbMaxLab, scaleDelta, precDelta, length);
            int subStep = (int) mathData[0];
            prec = mathData[1];
            startx = mathData[2];

            // If prec is too low, this might freeze application
            if (prec > Double.MIN_VALUE) {

                // Compute tickStep
                tickStep = length * prec / scaleDelta;
                if (attributes.isInverted()) {
                    tickStep = -tickStep;
                }
                subTickStep = subStep;

                // Adjust labels offset according to tick
                int offX = offset[IChartViewer.X_INDEX];
                int offY = offset[IChartViewer.Y_INDEX];

                // Build labels
                String lastLabelText = ObjectUtils.EMPTY_STRING;
                double lastDiff = Double.MAX_VALUE;
                AxisLabel lastLabel = null;

                // Find the value of the first label
                double firstShown = startx;
                double old;
                boolean infiniteLoop = false;
                double refMin = scaleMinimum - precDelta;
                while ((firstShown < refMin) && (!infiniteLoop)) {
                    old = firstShown;
                    firstShown += prec;
                    // Infinite loop detection (JAVAAPI-614)
                    if (firstShown <= old || Double.isInfinite(refMin) || Double.isNaN(refMin)
                            || Double.isNaN(firstShown)) {
                        infiniteLoop = true;
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .trace("AbstractAxisScale.computeLabelsForValueAnnotation(" + frc + ", " + length + ", "
                                        + Arrays.toString(offset)
                                        + "): infinite loop detected while searching for first shown label");
                        firstShown = scaleMinimum;
                    }
                }

                // Add a label if the first label is too far from the min
                if ((firstShown - scaleMinimum > 10 * scaleDelta / 100) && (!infiniteLoop)) {
                    double prec2 = prec / 10;

                    // Find the best value for the new first label
                    double firstLab = firstShown;
                    while ((firstLab - prec2 > scaleMinimum) && (!infiniteLoop)) {
                        old = firstLab;
                        firstLab -= prec2;
                        // Infinite loop detection (JAVAAPI-614)
                        if (firstLab >= old || Double.isInfinite(scaleMinimum) || Double.isNaN(scaleMinimum)
                                || Double.isNaN(firstLab)) {
                            infiniteLoop = true;
                            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                    .trace("AbstractAxisScale.computeLabelsForValueAnnotation(" + frc + ", " + length
                                            + ", " + Arrays.toString(offset)
                                            + "): infinite loop detected while searching for missing first labels");
                        }
                    }
                    if (infiniteLoop) {
                        preciseLabel = false;
                    } else {
                        pos = getPositionForCalculation(firstLab, length, scaleDelta);

                        double vt = getUnScaledValue(firstLab);

                        String tempValue = ChartUtils.formatValue(vt, getFormatPrecistion(vt, prec2),
                                attributes.getLabelFormat(), format);
                        double diff = 0;
                        if ((attributes.getLabelFormat() != IChartViewer.TIME_FORMAT)
                                && (attributes.getLabelFormat() != IChartViewer.DATE_FORMAT)) {
                            diff = Math.abs(Double.parseDouble(tempValue) - vt);
                        }
                        if (lastLabelText.equals(tempValue)) {
                            // Avoiding label duplication
                            if (diff < lastDiff) {
                                s = tempValue;
                            } else {
                                s = ObjectUtils.EMPTY_STRING;
                            }
                        } else {
                            s = tempValue;
                        }
                        lastDiff = diff;
                        lastLabelText = tempValue;
                        bounds = attributes.getLabelFont().getStringBounds(s, frc);

                        // DATAREDUC-468: Do not change first label in log scale
                        if (canAdaptFirstValueLabelToScaleMinimum() && (firstLab >= refMin) && (pos >= 0)
                                && (pos < length)) {
                            AxisLabel li = new AxisLabel(s, (int) bounds.getWidth(), (int) fontAscent, pos);
                            li.setOffset(offX, offY);
                            int formerLength = temp.length;
                            temp = Arrays.copyOf(temp, formerLength + 1);
                            temp[formerLength] = li;
                            lastLabel = li;
                        }

                        // Used to avoid subtick malfunction
                        preciseLabel = true;
                    }
                } else {
                    preciseLabel = false;
                }

                // There is no reason to go above scaleMaximum
                double refMax = scaleMaximum;
                boolean exitForLast = false;
                while ((startx <= refMax) && (!infiniteLoop) && (!exitForLast)) {
                    exitForLast = (startx == refMax);
                    pos = getPositionForCalculation(startx, length, scaleDelta);
                    double vt = getUnScaledValue(startx);
                    String tempValue = ChartUtils.formatValue(vt, exitForLast ? 0 : getFormatPrecistion(vt, prec),
                            attributes.getLabelFormat(), format);
                    if (tempValue == null) {
                        tempValue = ObjectUtils.EMPTY_STRING;
                    }
                    double diff = 0;
                    if ((attributes.getLabelFormat() != IChartViewer.TIME_FORMAT)
                            && (attributes.getLabelFormat() != IChartViewer.DATE_FORMAT) && (!exitForLast)
                            && !tempValue.isEmpty()) {
                        double val;
                        if ((attributes.getLabelFormat() == IChartViewer.HEXINT_FORMAT)
                                || (Format.isNumberFormatOK(format) && format.toLowerCase().indexOf('x') > 0)) {
                            val = Long.parseLong(tempValue, 16);
                        } else {
                            val = Double.parseDouble(tempValue);
                        }
                        if (val > refMax) {
                            // If val is above maximum, set it to maximum and display the exact maximum string value
                            val = refMax;
                            tempValue = ChartUtils.formatValue(vt, 0, attributes.getLabelFormat(), format);
                            if (tempValue == null) {
                                tempValue = ObjectUtils.EMPTY_STRING;
                            }
                        }
                        diff = Math.abs(val - vt);
                    }
                    if (lastLabelText.equals(tempValue)) {
                        // Avoiding label duplication
                        if (diff < lastDiff) {
                            s = tempValue;
                            if (lastLabel != null) {
                                lastLabel.setValue(ObjectUtils.EMPTY_STRING);
                            }
                        } else {
                            s = ObjectUtils.EMPTY_STRING;
                        }
                    } else {
                        s = tempValue;
                    }
                    lastDiff = diff;
                    lastLabelText = tempValue;
                    bounds = attributes.getLabelFont().getStringBounds(s, frc);

                    // Warning! Using (pos <= length) instead of (pos < length) is mandatory to build first tick
                    // in case of inverted vertical axis, and last tick in case of not inverted axis.
                    if ((startx >= refMin) && (pos >= 0) && (pos <= length)) {
                        AxisLabel li = new AxisLabel(s, (int) bounds.getWidth(), (int) fontAscent, pos);
                        li.setOffset(offX, offY);
                        int formerLength = temp.length;
                        temp = Arrays.copyOf(temp, formerLength + 1);
                        temp[formerLength] = li;
                        lastLabel = li;
                    }
                    old = startx;
                    startx += prec;
                    // Infinite loop detection (JAVAAPI-614)
                    if (startx <= old || Double.isInfinite(refMax) || Double.isNaN(refMax) || Double.isNaN(startx)) {
                        infiniteLoop = true;
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .trace("AbstractAxisScale.computeLabelsForValueAnnotation(" + frc + ", " + length + ", "
                                        + Arrays.toString(offset) + "): infinite loop detected while adding labels");
                    } else if (startx > refMax && !exitForLast) {
                        // Set label for maximum value if possible
                        double delta = refMax - old;
                        if (delta > 0.5 * prec) {
                            startx = refMax;
                            prec = delta;
                        }
                    }
                }

                // Extract 2 bounds when we didn't find a correct match.
                if (mathData[3] != 0) {
                    if (temp.length > 2) {
                        AxisLabel lis = temp[0];
                        AxisLabel lie = temp[temp.length - 1];
                        tickStep = lie.getPos() - lis.getPos();
                        subTickStep = temp.length - 1;
                        temp = new AxisLabel[2];
                        temp[0] = lis;
                        temp[1] = lie;
                    }
                }
                labels = temp;
            }
        }
    }

    public int getPosition(double value, int length) {
        return getPosition(value, length, false);
    }

    public int getPosition(double value, int length, boolean scaledValue) {
        int pos;
        double scaleDelta = scaleMaximum - scaleMinimum;
        double val;
        if (scaledValue) {
            val = value;
        } else {
            val = getScaledValue(value);
        }
        if (attributes.isInverted()) {
            pos = getInvertedPositionFromOrigin(val, length, scaleDelta);
        } else {
            pos = getStandardPosition(val, length, scaleDelta);
        }
        return pos;
    }

    protected int getPositionForCalculation(double scaledValue, int length, double scaleDelta) {
        int pos;
        if (attributes.isInverted()) {
            pos = getInvertedPositionForCalculation(scaledValue, length, scaleDelta);
        } else {
            pos = getStandardPosition(scaledValue, length, scaleDelta);
        }
        return pos;
    }

    protected int getStandardPosition(double scaledValue, int length, double scaleDelta) {
        return (int) Math.rint(length * ((scaledValue - scaleMinimum) / scaleDelta));
    }

    protected int getInvertedPositionForCalculation(double scaledValue, int length, double scaleDelta) {
        return (int) Math.rint(length * (1.0 - (scaledValue - scaleMinimum) / scaleDelta));
    }

    protected int getInvertedPositionFromOrigin(double scaledValue, int length, double scaleDelta) {
        return getInvertedPositionForCalculation(scaledValue, length, scaleDelta) - length;
    }

    protected double getFormatPrecistion(double value, double currentPrecision) {
        return currentPrecision;
    }

    /**
     * Calculates substep, precision, startX, and whether to extract label for
     * value annotation. Used in {@link #computeLabelsForValueAnnotation(FontRenderContext, double)}
     * 
     * @param nbMaxLab Maximum number of labels
     * @param scaleDelta Previously calculated distance between scale min and max
     * @param precDelta <code>scaleDelta</code> divided by expected axis length
     * @param length Expected axis length
     * 
     * @return A <code>double[]</code>: <code>{substep, prec, startX, extractLabel}</code>
     */
    protected abstract double[] computeLabelDataForValueAnnotation(int nbMaxLab, double scaleDelta, double precDelta,
            double length);

    /**
     * Adapts a value for scale reverse calculation
     * 
     * @param value The value to adapt
     * @return A <code>double</code>
     */
    public double getUnScaledValue(double value) {
        return value;
    }

    /**
     * Adapts a value for scale calculation
     * 
     * @param value The value to adapt
     * @return A <code>double</code>
     */
    public double getScaledValue(double value) {
        return value;
    }

    // ****************************************************************

    /**
     * Calculates the maximum number of labels for a given axis length
     * 
     * @param frc The {@link FontRenderContext} that helps calculating labels sizes
     * @param length The axis length
     * @return An <code>int</code> value
     */
    protected int computeNbMaxLabel(final FontRenderContext frc, final double length) {
        String s;
        double prec;
        Rectangle2D bounds;
        double fontAscent = (attributes.getLabelFont().getLineMetrics(MEASURABLE_STRING, frc).getAscent());
        prec = ChartUtils.computeLowTen(scaleMaximum - scaleMinimum);
        // Anticipate label overlap
        int nbMaxLab;
        if (attributes.getOrientation() == Orientation.HORIZONTAL) {
            // HORIZONTAL
            // The strategy is not obvious here as we can't estimate the max
            // label width
            // Do it with the number of digit in min and max, and convert it in
            // pixel size
            double minT = getUnScaledValue(scaleMinimum);
            double maxT = getUnScaledValue(scaleMaximum);
            double mW;
            String format = attributes.getFormat();
            s = ChartUtils.formatValue(minT, getFormatPrecistion(minT, prec), attributes.getLabelFormat(), format);
            bounds = attributes.getLabelFont().getStringBounds(s, frc);
            mW = bounds.getWidth();
            s = ChartUtils.formatValue(maxT, getFormatPrecistion(maxT, prec), attributes.getLabelFormat(), format);
            bounds = attributes.getLabelFont().getStringBounds(s, frc);
            if (bounds.getWidth() > mW) {
                mW = bounds.getWidth();
            }
            // consider the space between labels
            // mW = 1.5 * mW;
            mW = 3 * mW;
            nbMaxLab = (int) (length / mW);
        } else {
            // VERTICAL
            // nbMaxLab = (int) (length / (2.0 * fontAscent));
            nbMaxLab = (int) (length / (4.0 * fontAscent));
        }
        // Overrides maxLab
        int userMaxLab = (int) Math.rint(length / attributes.getTickSpacing());
        if (userMaxLab < nbMaxLab) {
            nbMaxLab = userMaxLab;
        }
        if (nbMaxLab < 1) {
            nbMaxLab = 1; // At least 1 label !
        }
        if (nbMaxLab > MAX_LABELS) {
            nbMaxLab = MAX_LABELS; // Maximum 30 labels
        }
        return nbMaxLab;
    }

    protected void computeLabelsForTimeAnnotation(final FontRenderContext frc, final int length) {
        if (length > 0) {
            AxisLabel[] temp = labels;
            double scaleDelta = scaleMaximum - scaleMinimum;
            double precDelta = scaleDelta / length;

            // Only for HORINZONTAL axis !
            // This has nothing to do with TIME_FORMAT
            // 10 labels maximum should be enough...
            computeDateFormat(10);

            // search for best 1st tick position
            double startx = Math.floor(scaleMinimum / startPrecision) * startPrecision;
            double pos;
            Date date;
            synchronized (calendar) {
                if ((usedFormat == ChartUtils.HOUR_12_FORMAT) || (usedFormat == ChartUtils.DAY_FORMAT)
                        || (usedFormat == ChartUtils.WEEK_FORMAT) || (usedFormat == ChartUtils.MONTH_FORMAT)
                        || (usedFormat == ChartUtils.YEAR_FORMAT)) {
                    // try to have first tick at day start (or at noon for HOUR_12_FORMAT)
                    calendar.setTimeInMillis((long) startx);
                    calendar.set(Calendar.MINUTE, 0);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    if (usedFormat == ChartUtils.HOUR_12_FORMAT) {
                        if (hour > 12) {
                            calendar.setTimeInMillis(calendar.getTimeInMillis() + ChartUtils.DAY);
                            calendar.set(Calendar.HOUR_OF_DAY, 0);
                        } else if (hour > 0) {
                            calendar.set(Calendar.HOUR_OF_DAY, 12);
                        }
                    } else if (hour > 0) {
                        calendar.setTimeInMillis(calendar.getTimeInMillis() + ChartUtils.DAY);
                        calendar.set(Calendar.HOUR_OF_DAY, 0);
                    }
                    if (usedFormat == ChartUtils.YEAR_FORMAT) {
                        int month = calendar.get(Calendar.MONTH);
                        int year = calendar.get(Calendar.YEAR);
                        if (month > Calendar.JUNE) {
                            // If over half of the year, start at the first day of next year
                            calendar.set(Calendar.YEAR, year + 1);
                            calendar.set(Calendar.MONTH, 0);
                            calendar.set(Calendar.DAY_OF_MONTH, 1);
                            calendar.set(Calendar.HOUR_OF_DAY, 0);
                            calendar.set(Calendar.MINUTE, 0);
                            calendar.set(Calendar.SECOND, 0);
                            calendar.set(Calendar.MILLISECOND, 0);
                        }
                    }
                    startx = calendar.getTimeInMillis();
                } else {
                    calendar.setTimeInMillis((long) startx);
                }
            }
            date = calendar.getTime();
            if (attributes.isInverted()) {
                pos = length * (1.0 - (startx - scaleMinimum) / scaleDelta);
            } else {
                pos = length * ((startx - scaleMinimum) / scaleDelta);
            }
            String s = getDateFormat(date);

            Rectangle2D bounds = attributes.getLabelFont().getStringBounds(s, frc);
            int w = (int) bounds.getWidth();
            int h = (int) bounds.getHeight();
            int formerLength;
            if ((pos >= 0) && (pos < length)) {
                formerLength = temp.length;
                temp = Arrays.copyOf(temp, formerLength + 1);
                temp[formerLength] = new AxisLabel(s, w, h, pos);
            }
            double minStep = w * 1.3;
            if (minStep < attributes.getTickSpacing()) {
                minStep = attributes.getTickSpacing();
            }
            double minPrec = (minStep / length) * scaleDelta;

            // Correct to avoid label overlap
            double prec = Math.ceil(minPrec / desiredPrecision) * desiredPrecision;

            tickStep = length * prec / scaleDelta;
            startx = getNextTimePosition(startx, prec);
            if (attributes.isInverted()) {
                tickStep = -tickStep;
            }
            subTickStep = 0;

            // Avoid infinite loop
            double max = (scaleMaximum + precDelta);
            if (Double.isInfinite(max)) {
                max = scaleMaximum;
            }
            if (!Double.isInfinite(max)) {
                double old;
                boolean infiniteLoop = false;
                // Build labels
                while ((startx <= max) && (!infiniteLoop)) {
                    if (attributes.isInverted()) {
                        pos = (int) Math.rint(length * (1.0 - (startx - scaleMinimum) / scaleDelta));
                    } else {
                        pos = (int) Math.rint(length * ((startx - scaleMinimum) / scaleDelta));
                    }
                    date = new Date((long) startx);
                    s = getDateFormat(date);
                    bounds = attributes.getLabelFont().getStringBounds(s, frc);

                    // Check limit
                    if ((pos > 0) && (pos <= length)) {
                        w = (int) bounds.getWidth();
                        h = (int) bounds.getHeight();
                        formerLength = temp.length;
                        temp = Arrays.copyOf(temp, formerLength + 1);
                        temp[formerLength] = new AxisLabel(s, w, h, pos);
                        // This code allows sub-ticks to be drawn
                        subTickStep = formerLength;
                    }
                    old = startx;
                    startx = getNextTimePosition(startx, prec);
                    // infinite loop detection (JAVAAPI-614)
                    if (startx <= old) {
                        infiniteLoop = true;
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                .trace("AbstractAxisScale.computeLabelsForTimeAnnotation(" + frc + ", " + length
                                        + "): infinite loop detected while adding labels");
                    }
                }
            }
            labels = temp;
        }
    }

    protected String getDateFormat(Date date) {
        String s = null;
        switch (attributes.getLabelFormat()) {
            case IChartViewer.DATE_FORMAT:
                String completeFormat = attributes.getFullDateFormat();
                s = new SimpleDateFormat(completeFormat).format(date);
                break;
            case IChartViewer.TIME_FORMAT:
                String timeFormat = IDateFormattable.TIME_SHORT_FORMAT;
                if (attributes.getTimeFormat() != null) {
                    timeFormat = attributes.getTimeFormat();
                }
                s = new SimpleDateFormat(timeFormat).format(date);
                break;
            default:
                s = usedFormat.format(date);
                break;
        }
        return s;
    }

    protected double getNextTimePosition(double startx, double prec) {
        if (usedFormat == ChartUtils.YEAR_FORMAT) {
            synchronized (calendar) {
                calendar.setTimeInMillis((long) startx);
                if (calendar.isLeapYear(calendar.get(Calendar.YEAR))) {
                    startx += prec + ChartUtils.DAY; // 1 more day in leap years
                } else {
                    startx += prec;
                }
            }
        } else {
            startx += prec;
        }
        return startx;
    }

    /**
     * Sets the appropriate time format for the range to display
     */
    protected void computeDateFormat(int maxLab) {
        // find optimal precision
        boolean found = false;
        int i = 0;
        while ((i < ChartUtils.TIME_STEP_PRECISIONS.length) && !found) {
            int n = (int) Math.floor((scaleMaximum - scaleMinimum) / ChartUtils.TIME_STEP_PRECISIONS[i]);
            found = (n <= maxLab);
            if (!found) {
                i++;
            }
        }
        if (found) {
            desiredPrecision = ChartUtils.TIME_STEP_PRECISIONS[i];
            startPrecision = ChartUtils.TIME_START_PRECISIONS[i];
            usedFormat = ChartUtils.TIME_FORMATS[i];
        } else {
            desiredPrecision = 10 * ChartUtils.YEAR;
            startPrecision = ChartUtils.YEAR;
            usedFormat = ChartUtils.YEAR_FORMAT;
        }
    }

    /**
     * Returns whether some new bounds, calculated for a zoom, do really apply a change.
     * 
     * @param minMax The new bounds.
     * @return A <code>boolean</code>.
     */
    protected boolean boundsChangedWithZoom(double... minMax) {
        boolean changed;
        if ((minMax != null) && (minMax.length > 1) && ((minMax[0] != scaleMinimum) || (minMax[1] != scaleMaximum))) {
            changed = true;
            scaleMinimum = minMax[0];
            scaleMaximum = minMax[1];
            autoScale = false;
            zoomed = true;
        } else {
            changed = false;
        }
        return changed;
    }

    /**
     * Zoom axis.
     * 
     * @param x1 New minimum value for this axis
     * @param x2 New maximum value for this axis
     * @param boundRect The {@link Rectangle} in which axis and dataviews are displayed
     * @return A <code>boolean</code>: whether the zoom was effective or not.
     */
    public boolean zoom(int x1, int x2, Rectangle boundRect) {
        return boundsChangedWithZoom(getNewBounds(x1, x2, boundRect));
    }

    /**
     * Calculates the bounds that correspond to a custom axis zoom
     * 
     * @param x1 New minimum position in current display coordinates
     * @param x2 New maximum position in current display coordinates
     * @param boundRect The {@link Rectangle} in which axis and dataviews are displayed
     * @return A <code>double[]</code>, never <code>null</code>: <code>{scaleMinimum, scaleMaximum}</code>
     */
    public double[] getNewBounds(int x1, int x2, Rectangle boundRect) {
        double scaleMinimum = this.scaleMinimum;
        double scaleMaximum = this.scaleMaximum;
        if (boundRect != null) {
            if (attributes.getOrientation() == Orientation.HORIZONTAL) {
                // Clip
                if (x1 < boundRect.x) {
                    x1 = boundRect.x;
                }
                if (x2 > (boundRect.x + boundRect.width)) {
                    x2 = boundRect.x + boundRect.width;
                }
                // Check for too small zoom
                if ((x2 - x1) >= 10) {
                    // Compute new min and max
                    double xr1 = (double) (x1 - boundRect.x) / (double) (boundRect.width);
                    double xr2 = (double) (x2 - boundRect.x) / (double) (boundRect.width);
                    double nmin = scaleMinimum + (scaleMaximum - scaleMinimum) * xr1;
                    double nmax = scaleMinimum + (scaleMaximum - scaleMinimum) * xr2;
                    // Check for too small zoom
                    double difference = nmax - nmin;
                    if (difference >= MINIMUM_DIFFERENCE) {
                        scaleMinimum = nmin;
                        scaleMaximum = nmax;
                    }
                }
            } else {
                // Clip
                if (x1 < boundRect.y) {
                    x1 = boundRect.y;
                }
                if (x2 > (boundRect.y + boundRect.height)) {
                    x2 = boundRect.y + boundRect.height;
                }
                // Check for too small zoom
                if ((x2 - x1) >= 10) {
                    // Compute new min and max
                    double yr1 = (double) (boundRect.y + boundRect.height - x2) / (double) (boundRect.height);
                    double yr2 = (double) (boundRect.y + boundRect.height - x1) / (double) (boundRect.height);
                    double nmin = scaleMinimum + (scaleMaximum - scaleMinimum) * yr1;
                    double nmax = scaleMinimum + (scaleMaximum - scaleMinimum) * yr2;
                    // Check for too small zoom
                    double difference = nmax - nmin;
                    if (difference >= MINIMUM_DIFFERENCE) {
                        scaleMinimum = nmin;
                        scaleMaximum = nmax;
                    }
                }
            }
        }
        return new double[] { scaleMinimum, scaleMaximum };
    }

    /**
     * Translates axis.
     * 
     * @param amount translation in current display coordinates.
     * @param boundRect The {@link Rectangle} in which axis and dataviews are displayed.
     * @return A <code>boolean</code>: whether the translation really occurred or not.
     */
    public boolean translate(int amount, Rectangle boundRect) {
        return boundsChangedWithZoom(getTranslationNewBounds(amount, boundRect));
    }

    /**
     * Calculates the bounds that correspond to a custom axis translation.
     * 
     * @param translation translation in current display coordinates.
     * @param boundRect The {@link Rectangle} in which axis and dataviews are displayed.
     */
    public double[] getTranslationNewBounds(int translation, Rectangle boundRect) {
        int pixelTranslation = attributes.isInverted() ? translation : -translation;
        double scaleMinimum = this.scaleMinimum;
        double scaleMaximum = this.scaleMaximum;
        if ((boundRect != null) && (boundRect.width >= 0) && (boundRect.height > 0) && (scaleMaximum > scaleMinimum)) {
            // Compute new min and max
            double dim = attributes.getOrientation() == Orientation.HORIZONTAL ? boundRect.width : boundRect.height;
            double axisTranslation = (pixelTranslation / dim) * (scaleMaximum - scaleMinimum);
            scaleMinimum += axisTranslation;
            scaleMaximum += axisTranslation;
        }
        return new double[] { scaleMinimum, scaleMaximum };
    }

    /**
     * Returns the value at given position in current display coordinates
     * 
     * @param pos The position in current display coordinates
     * @param boundRect The {@link Rectangle} in which axis and dataviews are displayed
     * @return A <code>double</code>
     */
    public double getValueAt(int pos, Rectangle boundRect) {
        double value;
        double scaleMinimum = this.scaleMinimum;
        double scaleMaximum = this.scaleMaximum;
        if (boundRect == null) {
            value = Double.NaN;
        } else {
            boolean inverted;
            if (attributes.getOrientation() == Orientation.HORIZONTAL) {
                value = (double) (pos - boundRect.x) / (boundRect.width);
                inverted = attributes.isInverted();
            } else {
                value = (double) (boundRect.y + boundRect.height - pos) / (boundRect.height);
                inverted = !attributes.isInverted();
            }
            if (inverted) {
                value = scaleMaximum - (scaleMaximum - scaleMinimum) * value;
            } else {
                value = scaleMinimum + (scaleMaximum - scaleMinimum) * value;
            }
            value = getUnScaledValue(value);
        }
        return value;
    }

    /**
     * Unzoom the axis and restores last state.
     */
    public void unzoom() {
        updateScaleBoundsFromAttributes();
        zoomed = false;
    }

    /**
     * Computes an {@link AbstractDataView} bar width depending on this scale
     * 
     * @param v The {@link AbstractDataView}
     * @param xView The {@link AbstractDataView} that may define x scale
     * @param axisLength The axis length
     * @return An <code>int</code>
     */
    public int computeBarWidth(AbstractDataView v, AbstractDataView xView, int axisLength) {
        int barWidth = v.getBarWidth();
        // No auto scale
        if (barWidth <= 0) {
            barWidth = computeAutoBarWidth(v, xView, axisLength);
        }
        return barWidth;
    }

    /**
     * Computes an {@link AbstractDataView} bar width depending on this scale, if its registered bar width is
     * <code>&lt;=0</code>
     * 
     * @param v The {@link AbstractDataView}
     * @param xView The {@link AbstractDataView} that may define x scale
     * @param axisLength The axis length
     * @return An <code>int</code>
     */
    protected int computeAutoBarWidth(AbstractDataView v, AbstractDataView xView, int axisLength) {
        return DEFAULT_BAR_WIDTH;
    }

    /**
     * Calculates the coordinate of "0" in this scale
     * 
     * @param axisOrigin The axis origin
     * @param axisLength The axis length
     * @return An <code>int</code>
     */
    public int getZeroPosition(int axisOrigin, double axisLength) {
        return axisOrigin;
    }

    /**
     * @return the preciseLabel
     */
    public boolean hasPreciseLabel() {
        return preciseLabel;
    }

    /**
     * Returns previously calculated axis labels
     * 
     * @return A {@link List} of {@link AxisLabel}
     */
    public AxisLabel[] getLabels() {
        return labels;
    }

    public double getScaleMinimum() {
        return scaleMinimum;
    }

    public double getScaleMaximum() {
        return scaleMaximum;
    }

    public boolean isZoomed() {
        return zoomed;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * An {@link Enum} used to identify a type of tick
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    public static enum TickType {
        /**
         * Represents the main ticks
         */
        TICK,
        /**
         * Represents the sub-ticks
         */
        SUB_TICK
    }

}
