/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.lang.reflect.Array;

import javax.activation.UnsupportedDataTypeException;
import javax.swing.JFrame;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.data.target.matrix.IBooleanMatrixComponent;
import fr.soleil.comete.definition.data.target.matrix.INumberMatrixComponent;
import fr.soleil.comete.definition.listener.ITableListener;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.BooleanMatrix;
import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.container.matrix.util.NumberMatrixGenerator;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

public class NumberMatrixArea extends AbstractMatrixArea implements INumberMatrixComponent, IBooleanMatrixComponent {

    private static final long serialVersionUID = -1651337818988366207L;

    public NumberMatrixArea() {
        super();
    }

    @Override
    public Object[] getNumberMatrix() {
        Object[] result;
        if (matrix instanceof AbstractNumberMatrix<?>) {
            result = getMatrix();
        } else {
            result = null;
        }
        return result;
    }

    @Override
    protected Object[] getMatrix() {
        Object[] result = null;
        if (matrix != null) {
            result = matrix.getValue();
        }
        return result;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return ObjectUtils.isNumberClass(concernedDataClass) || Boolean.class.equals(concernedDataClass)
                || Boolean.TYPE.equals(concernedDataClass);
    }

    @Override
    public void updateValue() {
        Object values = null;
        boolean[] valuesToBoolean = null;
        String[] flatValues = getFlatValue(false, false);
        int newCol = textAreaWidth;
        int newRow = textAreaHeight;
        if (flatValues != null) {
            if (textAreaWidth == 1) {// this is a spectrum
                newCol = textAreaHeight;
                newRow = textAreaWidth;
            }
        }
        if ((flatValues != null) && (flatValues.length > 0) && (matrix != null)) {
            Class<?> type = matrix.getType();
            if (type == double.class) {
                double[] doubleValue = new double[flatValues.length];
                for (int i = 0; i < flatValues.length; i++) {
                    doubleValue[i] = Double.parseDouble(flatValues[i]);
                }
                values = doubleValue;
            } else if (type == short.class) {
                short[] shortValue = new short[flatValues.length];
                for (int i = 0; i < flatValues.length; i++) {
                    shortValue[i] = Short.parseShort(flatValues[i]);
                }
                values = shortValue;
            } else if (type == long.class) {
                long[] longValue = new long[flatValues.length];
                for (int i = 0; i < flatValues.length; i++) {
                    longValue[i] = Long.parseLong(flatValues[i]);
                }
                values = longValue;
            } else if (type == byte.class) {
                byte[] byteValue = new byte[flatValues.length];
                for (int i = 0; i < flatValues.length; i++) {
                    byteValue[i] = Byte.parseByte(flatValues[i]);
                }
                values = byteValue;
            } else if (type == int.class) {
                int[] integerValue = new int[flatValues.length];
                for (int i = 0; i < flatValues.length; i++) {
                    integerValue[i] = Integer.parseInt(flatValues[i]);
                }
                values = integerValue;
            } else if (type == float.class) {
                float[] floatValue = new float[flatValues.length];
                for (int i = 0; i < flatValues.length; i++) {
                    floatValue[i] = Float.parseFloat(flatValues[i]);
                }
                values = floatValue;
            } else if (type == boolean.class) {
                boolean[] booleanValue = new boolean[flatValues.length];
                for (int i = 0; i < flatValues.length; i++) {
                    booleanValue[i] = Boolean.parseBoolean(flatValues[i]);
                }
                valuesToBoolean = booleanValue;
            }
            if (valuesToBoolean != null) {
                setFlatBooleanMatrix(valuesToBoolean, newCol, newRow);
            }
            if (values != null) {
                setFlatNumberMatrix(values, newCol, newRow);
            }
        } else {
            setNumberMatrix(null);
        }
        fireValueChanged();
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        if ((value != null) && value.length > 0) {
            int[] shape = ArrayUtils.recoverShape(value);
            int width, height;
            if ((shape != null) && (shape.length == 2)) {
                width = shape[1];
                height = shape[0];
            } else {
                width = 0;
                height = 0;
            }
            setFlatNumberMatrix(ArrayUtils.convertArrayDimensionFromNTo1(value), width, height);
        } else {
            matrix = null;
        }
        updateText();
    }

    @Override
    public Object getFlatNumberMatrix() {
        Object flatValue = null;
        if (matrix != null) {
            flatValue = matrix.getFlatValue();
        }
        return flatValue;
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        if (value != null) {
            Object[] tmp = (Object[]) Array.newInstance(value.getClass(), 1);
            tmp[0] = value;
            matrix = NumberMatrixGenerator.getNewMatrixFor(tmp);
            if (matrix == null) {
                updateSize(0, 0);
            } else {
                try {
                    matrix.setFlatValue(value, height, width);
                } catch (UnsupportedDataTypeException e) {
                    // Should not happen
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to set flat number matrix", e);
                }
                updateSize(matrix.getWidth(), matrix.getHeight());
            }
        } else {
            matrix = null;
            updateSize(0, 0);
        }
        updateText();
    }

    @Override
    protected void fillStringBuidler(Object array, StringBuilder builder) {
        if (array != null) {
            int col = 0;
            int width = viewerWidth;
            if (array instanceof boolean[]) {
                boolean[] flat = (boolean[]) array;
                for (int i = 0; i < flat.length; i++) {
                    builder.append(flat[i]);
                    if (width > 1) {
                        builder.append(separator);
                    }
                    col++;
                    if (col == width) {
                        builder.append(ObjectUtils.NEW_LINE);
                        col = 0;
                    }
                }
            } else if (array instanceof char[]) {
                char[] flat = (char[]) array;
                for (int i = 0; i < flat.length; i++) {
                    builder.append(flat[i]);
                    if (width > 1) {
                        builder.append(separator);
                    }
                    col++;
                    if (col == width) {
                        builder.append(ObjectUtils.NEW_LINE);
                        col = 0;
                    }
                }
            } else if (array instanceof byte[]) {
                byte[] flat = (byte[]) array;
                for (int i = 0; i < flat.length; i++) {
                    builder.append(flat[i]);
                    if (width > 1) {
                        builder.append(separator);
                    }
                    col++;
                    if (col == width) {
                        builder.append(ObjectUtils.NEW_LINE);
                        col = 0;
                    }
                }
            } else if (array instanceof short[]) {
                short[] flat = (short[]) array;
                for (int i = 0; i < flat.length; i++) {
                    builder.append(flat[i]);
                    if (width > 1) {
                        builder.append(separator);
                    }
                    col++;
                    if (col == width) {
                        builder.append(ObjectUtils.NEW_LINE);
                        col = 0;
                    }
                }
            } else if (array instanceof int[]) {
                int[] flat = (int[]) array;
                for (int i = 0; i < flat.length; i++) {
                    builder.append(flat[i]);
                    if (width > 1) {
                        builder.append(separator);
                    }
                    col++;
                    if (col == width) {
                        builder.append(ObjectUtils.NEW_LINE);
                        col = 0;
                    }
                }
            } else if (array instanceof long[]) {
                long[] flat = (long[]) array;
                for (int i = 0; i < flat.length; i++) {
                    builder.append(flat[i]);
                    if (width > 1) {
                        builder.append(separator);
                    }
                    col++;
                    if (col == width) {
                        builder.append(ObjectUtils.NEW_LINE);
                        col = 0;
                    }
                }
            } else if (array instanceof float[]) {
                float[] flat = (float[]) array;
                for (int i = 0; i < flat.length; i++) {
                    builder.append(flat[i]);
                    if (width > 1) {
                        builder.append(separator);
                    }
                    col++;
                    if (col == width) {
                        builder.append(ObjectUtils.NEW_LINE);
                        col = 0;
                    }
                }
            } else if (array instanceof double[]) {
                double[] flat = (double[]) array;
                for (int i = 0; i < flat.length; i++) {
                    builder.append(flat[i]);
                    if (width > 1) {
                        builder.append(separator);
                    }
                    col++;
                    if (col == width) {
                        builder.append(ObjectUtils.NEW_LINE);
                        col = 0;
                    }
                }
            }
        }
    }

    @Override
    protected Object getArrayValue() {
        Object array = null;
        if (matrix != null) {
            array = matrix.getFlatValue();
        }
        return array;
    }

    @Override
    public boolean[][] getBooleanMatrix() {
        boolean[][] values;
        if (matrix instanceof BooleanMatrix) {
            values = (boolean[][]) matrix.getValue();
        } else {
            values = null;
        }
        return values;
    }

    @Override
    public void setBooleanMatrix(boolean[][] value) {
        if (value != null) {
            int[] shape = ArrayUtils.recoverShape(value);
            int width, height;
            if ((shape != null) && (shape.length == 2)) {
                width = shape[1];
                height = shape[0];
            } else {
                width = 0;
                height = 0;
            }
            setFlatBooleanMatrix((boolean[]) ArrayUtils.convertArrayDimensionFromNTo1(value), width, height);
        }
        updateText();
    }

    @Override
    public boolean[] getFlatBooleanMatrix() {
        Object flatValue = null;
        if (matrix != null) {
            flatValue = ((BooleanMatrix) matrix).getFlatValue();
        }
        return (boolean[]) flatValue;
    }

    @Override
    public void setFlatBooleanMatrix(boolean[] value, int width, int height) {
        if (value != null) {
            if (!(matrix instanceof BooleanMatrix)) {
                matrix = new BooleanMatrix(Boolean.TYPE);
            }
            try {
                ((BooleanMatrix) matrix).setFlatValue(value, height, width);
            } catch (UnsupportedDataTypeException e) {
                // Should not happen
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to set flat boolean matrix", e);
            }
            updateSize(matrix.getWidth(), matrix.getHeight());
        } else {
            matrix = null;
            updateSize(0, 0);
        }
        updateText();

    }

    public static void main(String[] args) {

        NumberMatrixArea numberMatrixArea = new NumberMatrixArea();
        numberMatrixArea.setFlatNumberMatrix(new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, 3, 3);
        JFrame frame = new JFrame();
        frame.setContentPane(numberMatrixArea);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setTitle("NumberMatrixArea");
        frame.setAlwaysOnTop(true);
        frame.setLocationRelativeTo(null);
        frame.setSize(500, 500);
        frame.setVisible(true);
        frame.toFront();
        ITableListener<AbstractMatrix<?>> iTableListener = new ITableListener<AbstractMatrix<?>>() {

            @Override
            public void valueChanged(AbstractMatrix<?> matrix) {
                if (matrix instanceof StringMatrix) {

                }
            }

            @Override
            public void selectedPointChanged(double[] point) {

            }

            @Override
            public void selectedColumnChanged(int col) {

            }

            @Override
            public void selectedRowChanged(int col) {

            }

        };
        numberMatrixArea.addTableListener(iTableListener);
    }
}
