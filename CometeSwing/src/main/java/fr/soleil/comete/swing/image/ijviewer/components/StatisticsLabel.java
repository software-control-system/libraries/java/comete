/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.io.IOException;
import java.lang.ref.WeakReference;

import javax.swing.JLabel;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.events.RoiStatisticsListener;
import fr.soleil.comete.swing.image.util.ij.RoiAndPluginUtil;
import fr.soleil.lib.project.ObjectUtils;
import ij.gui.Roi;

/**
 * A {@link JLabel} that also is an {@link IStatisticsComponent}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class StatisticsLabel extends JLabel implements IStatisticsComponent {

    private static final long serialVersionUID = 3546804572170284209L;

    private static final String HTML_BODY = "<html><body>";
    private static final String BODY_HTML = "</body></html>";
    private static final String BR = "<br />";
    private static final String RANGES = "<b>Range: </b>";
    private static final String SEPARATOR = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    private static final String AVERAGE = "<b>Average: </b>";
    private static final String STD_DEV = "<b>Std deviation: </b>";
    private static final String SAMPLESTD_DEV = "<b>Sample std deviation: </b>";
    private static final String ROI = "<b>Roi: </b>";
    private static final String ITALIC_START = "<i>";
    private static final String ITALIC_END = "</i>";

    private final WeakReference<ImageViewer> imageViewerRef;
    private final RoiStatisticsListener roiListener;
    private final double[] statistics;
    private String valueFormat;

    /**
     * Constructor
     */
    public StatisticsLabel(ImageViewer imgViewer) {
        super();
        imageViewerRef = imgViewer == null ? null : new WeakReference<ImageViewer>(imgViewer);
        setFont(DEFAULT_FONT);
        statistics = IStatisticsComponent.generateStatisticsArray();
        fillLabel(null);
        roiListener = new RoiStatisticsListener(imgViewer, this);
    }

    @Override
    public RoiStatisticsListener getRoiListener() {
        return roiListener;
    }

    @Override
    public String getValueFormat() {
        return valueFormat;
    }

    @Override
    public void setValueFormat(String valueFormat) {
        if (!ObjectUtils.sameObject(valueFormat, this.valueFormat)) {
            this.valueFormat = valueFormat;
            computeStatisticsAndDisplayRoiInformation(ObjectUtils.recoverObject(imageViewerRef));
        }
    }

    protected String toString(double value, boolean format) {
        return IStatisticsComponent.toString(value, valueFormat, format);
    }

    protected void computeStatistics(ImageViewer imgViewer, Roi roi) {
        IStatisticsComponent.computeStatistics(imgViewer, roi, statistics);
    }

    protected void fillLabel(Roi roi) {
        if (statistics != null) {
            StringBuilder builder = new StringBuilder(HTML_BODY);
            builder.append(RANGES).append(toString(statistics[MIN_INDEX], false)).append(RANGES_SEPARATOR)
                    .append(toString(statistics[MAX_INDEX], false)).append(SEPARATOR).append(AVERAGE)
                    .append(toString(statistics[AVERAGE_INDEX], true)).append(BR).append(STD_DEV)
                    .append(toString(statistics[STDDEV_INDEX], true)).append(SEPARATOR).append(SAMPLESTD_DEV)
                    .append(toString(statistics[SAMPLESTDDEV_INDEX], true)).append(BR).append(ROI);
            if (roi != null) {
                builder.append('"').append(roi.getName()).append('"').append(SEPARATOR);
                if (checkType(roi)) {
                    builder.append(ITALIC_START);
                    try {
                        RoiAndPluginUtil.roiShapeToAppendable(roi, builder);
                    } catch (IOException e) {
                        // Won't happen as using a StringBuilder
                    }
                    builder.append(ITALIC_END);
                }
            }
            builder.append(BODY_HTML);
            setText(builder.toString());
        }

    }

    @Override
    public boolean checkType(Roi roi) {
        return IStatisticsComponent.validateType(roi, RECTANGLE_ROI_ONLY);
    }

    @Override
    public void computeStatisticsAndDisplayRoiInformation(ImageViewer imgViewer) {
        if (isVisible()) {
            Roi roi = IStatisticsComponent.getRoi(imgViewer, this::checkType);
            computeStatistics(imgViewer, roi);
            fillLabel(roi);
        }
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible != isVisible()) {
            super.setVisible(visible);
            if (visible) {
                computeStatisticsAndDisplayRoiInformation(ObjectUtils.recoverObject(imageViewerRef));
            } else {
                setText(ObjectUtils.EMPTY_STRING);
            }
        }
    }

}