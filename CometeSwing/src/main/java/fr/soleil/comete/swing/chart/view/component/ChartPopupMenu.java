/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.component;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXTitledSeparator;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.data.DataView;
import fr.soleil.comete.swing.chart.data.DataViewList;
import fr.soleil.comete.swing.chart.util.AbstractChartCustomMenu;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.chart.util.PrecisionPanel;

/**
 * {@link JPopupMenu} displayed in {@link Chart} on right mouse click
 * 
 * @author huriez
 * @author Rapha&euml;l GIRARDOT
 */
public class ChartPopupMenu extends JPopupMenu implements ActionListener {

    private static final long serialVersionUID = 8367543457246249806L;

    private static final String DATAVIEW = "Dataview #";

    private final Chart chart;

    private final JMenuItem optionMenuItem;
    private final JMenuItem saveFileMenuItem;
    private final JMenu tableMenu;
    private final JMenuItem tableAllMenuItem;
    private final JMenuItem tableDialogMenuItem;
    private final JMenuItem zoomBackMenuItem;
    private final JMenuItem printMenuItem;
    private final JMenuItem precisionMenuItem;
    private final JMenuItem saveSnapshotMenuItem;
    private final JMenuItem freezeMenuItem;
    private final JMenuItem clearFreezesMenuItem;
    private final JMenuItem loadDataMenuItem;
    private final AbstractButton dvDialogMenuItem;
    private final JMenu dvMenu;
    private final JMenu statMenu;
    private final JMenuItem statAllMenuItem;
    private final JMenuItem statDialogMenuItem;
    private final JSeparator expressionSeparator;
    private final JMenuItem expressionMenuItem;
    private final JMenuItem expressionClearMenuItem;
    private final JMenuItem settingsLoadMenuItem;
    private final JMenuItem settingsSaveMenuItem;
    private final JSeparator sepMenuItem;
    private final List<JMenuItem> userActionMenuItem;
    private final List<String> userAction;
    private final List<JMenuItem> tableSingleMenuItems;
    private final List<JMenuItem> statSingleMenuItems;
    private final List<JMenuItem> dvMenuItems;
    private boolean showAllMode;
    private final List<AbstractChartCustomMenu> customMenuList;

    private final int maxDataViewShown;

    private List<DataView> yViews;
    private List<DataView> clickableViews;

    private boolean expressionEnabled;

    public ChartPopupMenu(Chart chart) {
        super();
        this.chart = chart;
        optionMenuItem = new JMenuItem(Chart.MENU_CHART_PROPERTIES);
        optionMenuItem.addActionListener(this);
        saveFileMenuItem = new JMenuItem(Chart.MENU_SAVE_DATA_FILE);
        saveFileMenuItem.addActionListener(this);
        tableMenu = new JMenu(Chart.MENU_SHOW_TABLE);
        tableAllMenuItem = new JMenuItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.all"));
        tableAllMenuItem.addActionListener(this);
        tableDialogMenuItem = new JMenuItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.view.select"));
        tableDialogMenuItem.addActionListener(this);
        zoomBackMenuItem = new JMenuItem(Chart.MENU_EXIT_ZOOM);
        zoomBackMenuItem.addActionListener(this);
        printMenuItem = new JMenuItem(Chart.MENU_PRINT_CHART);
        printMenuItem.addActionListener(this);
        precisionMenuItem = new JMenuItem(Chart.MENU_ABSCISSA_MARGIN_ERROR);
        precisionMenuItem.addActionListener(this);
        saveSnapshotMenuItem = new JMenuItem(Chart.MENU_SAVE_SNAPSHOT);
        saveSnapshotMenuItem.addActionListener(this);
        freezeMenuItem = new JMenuItem(Chart.MENU_FREEZE_VALUES);
        freezeMenuItem.addActionListener(this);
        clearFreezesMenuItem = new JMenuItem(Chart.MENU_CLEAR_FREEZES);
        clearFreezesMenuItem.addActionListener(this);
        loadDataMenuItem = new JMenuItem(Chart.MENU_LOAD_DATA);
        loadDataMenuItem.addActionListener(this);
        dvMenu = new JMenu(Chart.MENU_DATAVIEW_PROPERTIES);
        dvDialogMenuItem = new JMenuItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.view.select"));
        dvDialogMenuItem.addActionListener(this);
        statMenu = new JMenu(Chart.MENU_SHOW_STATISTICS);
        statAllMenuItem = new JMenuItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.all"));
        statAllMenuItem.addActionListener(this);
        statDialogMenuItem = new JMenuItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.view.select"));
        statDialogMenuItem.addActionListener(this);
        expressionSeparator = new JSeparator();
        expressionMenuItem = new JMenuItem(Chart.MENU_EVALUATE_EXPRESSION);
        expressionMenuItem.addActionListener(this);
        expressionClearMenuItem = new JMenuItem(Chart.MENU_CLEAR_EXPRESSIONS);
        expressionClearMenuItem.addActionListener(this);
        settingsLoadMenuItem = new JMenuItem(Chart.MENU_LOAD_SETTINGS);
        settingsLoadMenuItem.addActionListener(this);
        settingsSaveMenuItem = new JMenuItem(Chart.MENU_SAVE_SETTINGS);
        settingsSaveMenuItem.addActionListener(this);

        userActionMenuItem = new ArrayList<>();
        userAction = new ArrayList<>();
        tableSingleMenuItems = new ArrayList<>();
        statSingleMenuItems = new ArrayList<>();
        dvMenuItems = new ArrayList<>();
        customMenuList = new ArrayList<>();

        add(zoomBackMenuItem);
        add(new JSeparator());
        add(optionMenuItem);
        add(dvMenu);
        add(tableMenu);
        add(statMenu);
        add(new JSeparator());
        add(saveFileMenuItem);
        add(loadDataMenuItem);
        add(printMenuItem);
        add(precisionMenuItem);
        add(new JSeparator());
        add(saveSnapshotMenuItem);
        add(freezeMenuItem);
        add(clearFreezesMenuItem);
        add(expressionSeparator);
        add(expressionMenuItem);
        add(expressionClearMenuItem);
        add(settingsLoadMenuItem);
        add(settingsSaveMenuItem);
        maxDataViewShown = 15;
        sepMenuItem = new JSeparator();
        add(sepMenuItem);
        sepMenuItem.setVisible(false);
        expressionEnabled = true;
    }

    public void addCustomMenu(AbstractChartCustomMenu menu) {
        int menuPosition = menu.getMenuPosition();
        if (menuPosition >= 0) {
            add(menu, menuPosition);
        } else {
            add(menu);
        }
        customMenuList.add(menu);
    }

    public void removeCustomMenu(AbstractChartCustomMenu menu) {
        customMenuList.remove(menu);
    }

    protected JMenuItem getMenuItem(String menuId) {
        JMenuItem item = null;
        if (menuId != null) {
            if (Chart.MENU_CHART_PROPERTIES.equals(menuId)) {
                item = optionMenuItem;
            } else if (Chart.MENU_SAVE_DATA_FILE.equals(menuId)) {
                item = saveFileMenuItem;
            } else if (Chart.MENU_SHOW_TABLE.equals(menuId)) {
                item = tableMenu;
            } else if (Chart.MENU_EXIT_ZOOM.equals(menuId)) {
                item = zoomBackMenuItem;
            } else if (Chart.MENU_PRINT_CHART.equals(menuId)) {
                item = printMenuItem;
            } else if (PrecisionPanel.VALUE_PRECISION_TITLE.equals(menuId)
                    || PrecisionPanel.TIME_PRECISION_TITLE.equals(menuId)) {
                item = precisionMenuItem;
            } else if (Chart.MENU_SAVE_SNAPSHOT.equals(menuId)) {
                item = saveSnapshotMenuItem;
            } else if (Chart.MENU_FREEZE_VALUES.equals(menuId)) {
                item = freezeMenuItem;
            } else if (Chart.MENU_CLEAR_FREEZES.equals(menuId)) {
                item = clearFreezesMenuItem;
            } else if (Chart.MENU_LOAD_DATA.equals(menuId)) {
                item = loadDataMenuItem;
            } else if (Chart.MENU_DATAVIEW_PROPERTIES.equals(menuId)) {
                item = dvMenu;
            } else if (Chart.MENU_SHOW_STATISTICS.equals(menuId)) {
                item = statMenu;
            } else if (Chart.MENU_EVALUATE_EXPRESSION.equals(menuId)) {
                item = expressionMenuItem;
            } else if (Chart.MENU_CLEAR_EXPRESSIONS.equals(menuId)) {
                item = expressionClearMenuItem;
            } else if (Chart.MENU_LOAD_SETTINGS.equals(menuId)) {
                item = settingsLoadMenuItem;
            } else if (Chart.MENU_SAVE_SETTINGS.equals(menuId)) {
                item = settingsSaveMenuItem;
            } else {
                item = null;
            }
        }
        return item;
    }

    public boolean isMenuVisible(String menu) {
        boolean visible;
        JMenuItem item = getMenuItem(menu);
        if (item == null) {
            visible = false;
        } else {
            visible = item.isVisible();
        }
        return visible;
    }

    public void setMenuVisible(String menu, boolean visible) {
        JMenuItem item = getMenuItem(menu);
        if (item != null) {
            item.setVisible(visible);
        }
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if ((evt != null) && (chart != null)) {
            Object src = evt.getSource();
            AbstractChartCustomMenu customMenu = null;
            if (src == getOptionMenuItem()) {
                chart.showChartOptionDialog();
            } else if (src == getZoomBackMenuItem()) {
                chart.getChartView().exitZoom();
                chart.refresh(true);
            } else if (src == getPrintMenuItem()) {
                chart.print();
                chart.fireActionPerformed(Chart.MENU_PRINT_CHART, true);
            } else if (src == getSaveSnapshotMenuItem()) {
                setVisible(false);
                chart.saveSnapshot();
            } else if (src == getFreezeMenuItem()) {
                chart.freeze();
            } else if (src == getCleaFreezesMenuItem()) {
                chart.clearFreezes();
            } else if (src == getPrecisionMenuItem()) {
                chart.displayPrecisionDialog();
            } else if (src == getLoadDataMenuItem()) {
                chart.showLoadFileDialog();
            } else if (src == getTableAllMenuItem()) {
                chart.showTableAll();
            } else if (src == getStatAllMenuItem()) {
                chart.showStatAll();
            } else if (src == getTableDialogMenuItem()) {
                chart.showTableDialog();
            } else if (src == getStatDialogMenuItem()) {
                chart.showStatsDialog();
            } else if (src == getDvDialogMenuItem()) {
                chart.showDataViewDialog();
            } else if (src == getExpressionMenuItem()) {
                chart.showExpressionDialog();
            } else if (src == getExpressionClearMenuItem()) {
                chart.clearExpressions();
            } else if (src == getSettingsLoadMenuItem()) {
                chart.loadSettings();
            } else if (src == getSettingsSaveMenuItem()) {
                chart.saveSettings();
            } else if (src == getSaveFileMenuItem()) {
                chart.showSaveFileDialog();
            } else if ((customMenu = findCustomMenuForSource(src)) != null) {
                customMenu.actionPerformed(src, chart);
            } else if (src instanceof AbstractChartCustomMenu) {
                ((AbstractChartCustomMenu) src).showDataViewDialog(chart);
            } else {
                fireUserActionItemFound(src);
                String item = null;
                if ((item = dataViewItemFound(src)) != null) {
                    chart.showDataOptions(item);
                } else {
                    setShowAllMode(false);
                    if ((item = tableItemFound(src)) != null) {
                        chart.showTableSingle(item);
                    } else if ((item = statItemFound(src)) != null) {
                        chart.showStatSingle(item);
                    }
                }
            }
            repaint();
        }
    }

    protected String getItemName(Object source, List<JMenuItem> itemList) {
        String result = null;
        for (JMenuItem element : itemList) {
            if (element == source) {
                result = element.getName();
                break;
            }
        }
        return result;
    }

    private String statItemFound(Object source) {
        return getItemName(source, statSingleMenuItems);
    }

    private String tableItemFound(Object source) {
        return getItemName(source, tableSingleMenuItems);
    }

    private String dataViewItemFound(Object source) {
        return getItemName(source, dvMenuItems);
    }

    private void fireUserActionItemFound(Object source) {
        JMenuItem foundItem = null;
        for (JMenuItem item : userActionMenuItem) {
            if (item == source) {
                foundItem = item;
                break;
            }
        }
        if (foundItem != null) {
            if (foundItem instanceof JCheckBoxMenuItem) {
                JCheckBoxMenuItem c = (JCheckBoxMenuItem) foundItem;
                chart.fireActionPerformed(c.getText(), c.getState());
            } else {
                chart.fireActionPerformed(foundItem.getText(), false);
            }
        }
    }

    @Override
    public void show(Component invoker, int x, int y) {
        if (chart != null) {
            precisionMenuItem.setText(chart.isTimeScale(IChartViewer.X) ? PrecisionPanel.TIME_PRECISION_TITLE
                    : PrecisionPanel.VALUE_PRECISION_TITLE);
            zoomBackMenuItem.setEnabled(chart.getChartView().isZoomed());
            // Gets user action state
            for (JMenuItem item : userActionMenuItem) {
                if (item instanceof JCheckBoxMenuItem) {
                    JCheckBoxMenuItem b = (JCheckBoxMenuItem) item;
                    b.setSelected(chart.fireGetActionState(b.getText(), listenerList));
                }
            }
            // Add dataView table item
            tableMenu.removeAll();
            tableMenu.add(tableAllMenuItem);
            // Add dataView option menu
            dvMenu.removeAll();
            // DataView Statistics item
            statMenu.removeAll();
            statMenu.add(statAllMenuItem);

            clearCustomMenu();

            // Forget previous menu items
            for (JMenuItem item : tableSingleMenuItems) {
                item.removeActionListener(this);
            }
            tableSingleMenuItems.clear();
            for (JMenuItem item : dvMenuItems) {
                item.removeActionListener(this);
            }
            dvMenuItems.clear();
            for (JMenuItem item : statSingleMenuItems) {
                item.removeActionListener(this);
            }
            statSingleMenuItems.clear();
            // -------- //
            List<DataView> y1Views, y2Views;
            DataViewList dataviews = null;
            dataviews = chart.getDataViewList();
            if (dataviews == null) {
                y1Views = new ArrayList<>();
                y2Views = new ArrayList<>();
            } else {
                List<DataView>[] views = dataviews.getDataViewsByAxis(chart.getDataViewComparator(), IChartViewer.Y1,
                        IChartViewer.Y2);
                y1Views = views[0];
                y2Views = views[1];
            }
            List<DataView> possibleViews = new ArrayList<>();
            List<DataView> allViews = new ArrayList<>();
            for (DataView view : y1Views) {
                if (view.isClickable()) {
                    possibleViews.add(view);
                }
                allViews.add(view);
            }
            for (DataView view : y2Views) {
                if (view.isClickable()) {
                    possibleViews.add(view);
                }
                allViews.add(view);
            }
            List<DataView> views = clickableViews;
            if (maxDataViewShown > -1) {
                if (possibleViews.size() > maxDataViewShown) {
                    tableMenu.addSeparator();
                    statMenu.addSeparator();
                    tableMenu.add(tableDialogMenuItem);
                    dvMenu.add(dvDialogMenuItem);
                    statMenu.add(statDialogMenuItem);
                    addSelectMenuToCustomMenu();
                    clickableViews = possibleViews;
                    yViews = allViews;
                } else {
                    buildMenus(possibleViews, allViews);
                }
            } else {
                buildMenus(possibleViews, allViews);
            }
            if (views != null) {
                views.clear();
            }
            super.show(invoker, x, y);
        }
    }

    private void buildMenus(List<DataView> possibleViews, List<DataView> allViews) {
        boolean noNeedToBuildDvMenu = ((maxDataViewShown > -1) && (allViews.size() > maxDataViewShown));
        List<DataView> views = yViews;
        if (noNeedToBuildDvMenu) {
            dvMenu.add(dvDialogMenuItem);
            yViews = allViews;
        } else {
            yViews = null;
        }
        views = yViews;
        if (views != null) {
            views.clear();
        }
        int unamedDv = 1;
        boolean y2Added = false;
        boolean y1Added = false;
        JMenuItem item;
        Map<String, String> nameMap = new HashMap<>();
        for (DataView view : possibleViews) {
            if (view.getAxis() == IChartViewer.Y1) {
                if (!y1Added) {
                    tableMenu.add(createTitleSeparator(ChartLegend.Y1_NAME, SwingConstants.CENTER));
                    statMenu.add(createTitleSeparator(ChartLegend.Y1_NAME, SwingConstants.CENTER));
                    addComponentToCustomMenu(createTitleSeparator(ChartLegend.Y1_NAME, SwingConstants.CENTER));
                }
                y1Added = true;
            } else if (view.getAxis() == IChartViewer.Y2) {
                if (!y2Added) {
                    tableMenu.add(createTitleSeparator(ChartLegend.Y2_NAME, SwingConstants.CENTER));
                    statMenu.add(createTitleSeparator(ChartLegend.Y2_NAME, SwingConstants.CENTER));
                    addComponentToCustomMenu(createTitleSeparator(ChartLegend.Y2_NAME, SwingConstants.CENTER));
                }
                y2Added = true;
            }
            String dvName = view.getDisplayName();
            if ((dvName == null) || dvName.trim().isEmpty()) {
                dvName = DATAVIEW + unamedDv++;
                nameMap.put(view.getId(), dvName);
            }
            item = createRefreshedMenuItem(dvName, view.getId());
            tableMenu.add(item);
            tableSingleMenuItems.add(item);
            item = createRefreshedMenuItem(dvName, view.getId());
            statMenu.add(item);
            statSingleMenuItems.add(item);
            item = createRefreshedMenuItem(dvName, view.getId());
            addComponentToCustomMenu(item);
        }
        if (!noNeedToBuildDvMenu) {
            y1Added = false;
            y2Added = false;
            for (DataView view : allViews) {
                if (view.getAxis() == IChartViewer.Y1) {
                    if (!y1Added) {
                        dvMenu.add(createTitleSeparator(ChartLegend.Y1_NAME, SwingConstants.CENTER));
                    }
                    y1Added = true;
                } else if (view.getAxis() == IChartViewer.Y2) {
                    if (!y2Added) {
                        dvMenu.add(createTitleSeparator(ChartLegend.Y2_NAME, SwingConstants.CENTER));
                    }
                    y2Added = true;
                }
                String dvName = view.getDisplayName();
                if ((dvName == null) || dvName.trim().isEmpty()) {
                    dvName = nameMap.get(view.getId());
                    if (dvName == null) {
                        dvName = DATAVIEW + unamedDv++;
                    }
                }
                item = createRefreshedMenuItem(dvName, view.getId());
                dvMenu.add(item);
                dvMenuItems.add(item);
            }
        }
    }

    private void addSelectMenuToCustomMenu() {
        for (AbstractChartCustomMenu customMenu : customMenuList) {
            customMenu.addSelectMenu();
        }
    }

    private void addComponentToCustomMenu(Component comp) {
        for (AbstractChartCustomMenu customMenu : customMenuList) {
            customMenu.add(comp);
        }
    }

    private void clearCustomMenu() {
        for (AbstractChartCustomMenu customMenu : customMenuList) {
            customMenu.clearItemMenu(this);
        }
    }

    protected JXTitledSeparator createTitleSeparator(String title, int alignment) {
        JXTitledSeparator separator = new JXTitledSeparator(title, alignment);
        separator.setOpaque(false);
        return separator;
    }

    private JMenuItem createRefreshedMenuItem(String name, String id) {
        JMenuItem item = new JMenuItem(name);
        item.setName(id);
        item.addActionListener(this);
        return item;
    }

    public AbstractChartCustomMenu findCustomMenuForSource(Object src) {
        AbstractChartCustomMenu foundObject = null;
        for (AbstractChartCustomMenu customMenu : customMenuList) {
            if (customMenu.containsComponent(src)) {
                foundObject = customMenu;
                break;
            }
        }
        return foundObject;
    }

    /**
     * @return the optionMenuItem
     */
    public JMenuItem getOptionMenuItem() {
        return optionMenuItem;
    }

    /**
     * @return the saveFileMenuItem
     */
    public JMenuItem getSaveFileMenuItem() {
        return saveFileMenuItem;
    }

    /**
     * @return the tableMenu
     */
    public JMenu getTableMenu() {
        return tableMenu;
    }

    /**
     * @return the tableAllMenuItem
     */
    public JMenuItem getTableAllMenuItem() {
        return tableAllMenuItem;
    }

    /**
     * @return the tableDialogMenuItem
     */
    public JMenuItem getTableDialogMenuItem() {
        return tableDialogMenuItem;
    }

    /**
     * @return the zoomBackMenuItem
     */
    public JMenuItem getZoomBackMenuItem() {
        return zoomBackMenuItem;
    }

    /**
     * @return the printMenuItem
     */
    public JMenuItem getPrintMenuItem() {
        return printMenuItem;
    }

    /**
     * @return the precisionMenuItem
     */
    public JMenuItem getPrecisionMenuItem() {
        return precisionMenuItem;
    }

    /**
     * @return the saveSnapshotMenuItem
     */
    public JMenuItem getSaveSnapshotMenuItem() {
        return saveSnapshotMenuItem;
    }

    /**
     * @return the freezeMenuItem
     */
    public JMenuItem getFreezeMenuItem() {
        return freezeMenuItem;
    }

    /**
     * @return the cleaFreezesMenuItem
     */
    public JMenuItem getCleaFreezesMenuItem() {
        return clearFreezesMenuItem;
    }

    /**
     * @return the loadDataMenuItem
     */
    public JMenuItem getLoadDataMenuItem() {
        return loadDataMenuItem;
    }

    /**
     * @return the dvDialogMenuItem
     */
    public AbstractButton getDvDialogMenuItem() {
        return dvDialogMenuItem;
    }

    /**
     * @return the dvMenu
     */
    public JMenu getDvMenu() {
        return dvMenu;
    }

    /**
     * @return the statMenu
     */
    public JMenu getStatMenu() {
        return statMenu;
    }

    /**
     * @return the statAllMenuItem
     */
    public JMenuItem getStatAllMenuItem() {
        return statAllMenuItem;
    }

    /**
     * @return the statDialogMenuItem
     */
    public JMenuItem getStatDialogMenuItem() {
        return statDialogMenuItem;
    }

    /**
     * @return the expressionSeparator
     */
    public JSeparator getExpressionSeparator() {
        return expressionSeparator;
    }

    /**
     * @return the expressionMenuItem
     */
    public JMenuItem getExpressionMenuItem() {
        return expressionMenuItem;
    }

    /**
     * @return the expressionClearMenuItem
     */
    public JMenuItem getExpressionClearMenuItem() {
        return expressionClearMenuItem;
    }

    /**
     * @return the settingsLoadMenuItem
     */
    public JMenuItem getSettingsLoadMenuItem() {
        return settingsLoadMenuItem;
    }

    public JMenuItem getSettingsSaveMenuItem() {
        return settingsSaveMenuItem;
    }

    /**
     * @return the sepMenuItem
     */
    public JSeparator getSepMenuItem() {
        return sepMenuItem;
    }

    public void setShowAllMode(boolean b) {
        showAllMode = b;
    }

    /**
     * @return the tableSingleMenuItems
     */
    public List<JMenuItem> getTableSingleMenuItems() {
        return tableSingleMenuItems;
    }

    /**
     * @return the statSingleMenuItems
     */
    public List<JMenuItem> getStatSingleMenuItems() {
        return statSingleMenuItems;
    }

    /**
     * @return the dvMenuItems
     */
    public List<JMenuItem> getDvMenuItems() {
        return dvMenuItems;
    }

    /**
     * @return the showAllMode
     */
    public boolean isShowAllMode() {
        return showAllMode;
    }

    /**
     * @return the userActionMenuItem
     */
    public Collection<JMenuItem> getUserActionMenuItem() {
        return userActionMenuItem;
    }

    /**
     * @return the userAction
     */
    public List<String> getUserAction() {
        return userAction;
    }

    public List<DataView> getYViews() {
        return yViews;
    }

    /**
     * @return the dialogListSave
     */
    public List<DataView> getClickableViews() {
        return clickableViews;
    }

    /**
     * @return the expressionEnabled
     */
    public boolean isExpressionEnabled() {
        return expressionEnabled;
    }

    /**
     * @param expressionEnabled
     *            the expressionEnabled to set
     */
    public void setExpressionEnabled(boolean expressionEnabled) {
        if (expressionEnabled != this.expressionEnabled) {
            this.expressionEnabled = expressionEnabled;
            expressionSeparator.setVisible(expressionEnabled);
            expressionMenuItem.setVisible(expressionEnabled);
            expressionClearMenuItem.setVisible(expressionEnabled);
        }
    }

    /**
     * Returns whether freeze actions are enabled
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isFreezeEnabled() {
        return freezeMenuItem.isVisible();
    }

    /**
     * Sets whether freeze actions should be enabled
     * 
     * @param enabled Whether freeze actions should be enabled
     */
    public void setFreezeEnabled(boolean enabled) {
        freezeMenuItem.setVisible(enabled);
        clearFreezesMenuItem.setVisible(enabled);
    }

    /**
     * Returns whether data file actions are enabled
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isDataFileEnabled() {
        return loadDataMenuItem.isEnabled();
    }

    /**
     * Sets whether data file actions should enabled
     * 
     * @param enabled Whether data file actions should enabled
     */
    public void setDataFileEnabled(boolean enabled) {
        loadDataMenuItem.setEnabled(enabled);
        saveFileMenuItem.setEnabled(enabled);
    }

}
