/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;

import fr.soleil.comete.definition.data.target.matrix.IMatrixComponent;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.listener.ITableListener;
import fr.soleil.comete.definition.listener.ITextAreaListener;
import fr.soleil.comete.definition.widget.ITextArea;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.MatrixInformation;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;

public abstract class AbstractMatrixArea extends JPanel implements IMatrixComponent, ITextArea, CometeConstants {

    private static final long serialVersionUID = -7372587479648057742L;

    private static final String NEW_LINE = ObjectUtils.NEW_LINE;

    protected TextArea textArea;
    protected JScrollPane scrollPane;
    private JPanel buttonPanel;
    private Button updateButton;
    private Button writeButton;
    private Button cancelButton;
    private JTable lineNumber;
    protected AbstractTableModel tableModel;
    protected String separator;
    protected String[] separatorList;
    protected AbstractMatrix<?> matrix;
    private List<ITableListener<AbstractMatrix<?>>> listeners;
    protected int viewerWidth;
    protected int viewerHeight;
    protected int dataWidth;
    protected int dataHeight;
    protected int textAreaHeight;
    protected int textAreaWidth;
    protected final TargetDelegate delegate;
    protected final ErrorNotificationDelegate errorNotificationDelegate;
    protected boolean caretAtTop;

    public AbstractMatrixArea() {
        super();
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        listeners = new ArrayList<ITableListener<AbstractMatrix<?>>>();
        tableModel = new AbstractTableModel() {

            private static final long serialVersionUID = -6005742308410525704L;

            @Override
            public Object getValueAt(int row, int col) {
                return row;
            }

            @Override
            public int getRowCount() {
                return viewerHeight;
            }

            @Override
            public int getColumnCount() {
                return 1;
            }
        };
        caretAtTop = false;
        textArea = null;
        updateButton = null;
        writeButton = null;
        cancelButton = null;
        lineNumber = null;
        separator = " ";
        separatorList = new String[] { COMMA, " ", ";", "\t", "\n\n" };
        matrix = null;
        viewerWidth = 0;
        viewerHeight = 0;
        textAreaHeight = 0;
        textAreaWidth = 0;
        initGUI();
    }

    public boolean isCaretAtTop() {
        return caretAtTop;
    }

    public void setCaretAtTop(boolean caretAtTop) {
        this.caretAtTop = caretAtTop;
    }

    private void initGUI() {
        textArea = new TextArea();
        textArea.setLineWripe(false);
        textArea.setPopupMenuVisible(false);

        updateButton = new Button("Update");
        writeButton = new Button("Write");
        cancelButton = new Button("Cancel");

        lineNumber = new JTable();
        lineNumber.setPreferredSize(new Dimension(50, lineNumber.getPreferredSize().height));

        setLayout(new BorderLayout());
        setSize(4, 600);
        JTextArea centerComponent = null;
        for (Component comp : textArea.getComponents()) {
            if (comp instanceof JScrollPane) {
                centerComponent = (JTextArea) ((JScrollPane) comp).getViewport().getView();
                break;
            }
        }
        if (centerComponent == null) {
            textArea.setRowHeader(lineNumber);
            add(textArea, BorderLayout.CENTER);
        } else {
            JPanel textAreaPanel = new JPanel(new BorderLayout());
            textArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            textArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            textAreaPanel.add(centerComponent, BorderLayout.CENTER);
            textAreaPanel.add(lineNumber, BorderLayout.WEST);
            scrollPane = new JScrollPane(textAreaPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            add(scrollPane, BorderLayout.CENTER);
        }
        buttonPanel = new JPanel();
        buttonPanel.add(updateButton);
        buttonPanel.add(writeButton);
        buttonPanel.add(cancelButton);
        add(buttonPanel, BorderLayout.SOUTH);

        updateButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                updateValue();
            }
        });

        writeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                updateValue();
                warnMediators();
            }
        });

        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                Window window = SwingUtilities.windowForComponent(getParent());
                if (window instanceof JFrame) {
                    JFrame frame = (JFrame) window;
                    frame.setVisible(false);
                    frame.dispose();
                }
            }
        });

        lineNumber.setEnabled(false);
        lineNumber.setBackground(Color.LIGHT_GRAY);
        lineNumber.setModel(tableModel);
    }

    public boolean isLineNumberVisible() {
        return lineNumber.isVisible();
    }

    public void setLineNumberVisible(boolean visible) {
        lineNumber.setVisible(visible);
        revalidate();
        repaint();
    }

    public boolean isButtonPanelVisible() {
        return buttonPanel.isVisible();
    }

    public void setButtonPanelVisible(boolean visible) {
        buttonPanel.setVisible(visible);
        revalidate();
        repaint();
    }

    public void addTableListener(ITableListener<AbstractMatrix<?>> listener) {
        synchronized (listeners) {
            if (!listeners.contains(listener)) {
                listeners.add(listener);
            }
        }
    }

    public void removeTableListener(ITableListener<AbstractMatrix<?>> listener) {
        synchronized (listeners) {
            if (listeners.contains(listener)) {
                listeners.remove(listener);
            }
        }
    }

    public void fireValueChanged() {
        synchronized (listeners) {
            for (ITableListener<AbstractMatrix<?>> tmpListener : listeners) {
                tmpListener.valueChanged(matrix);
            }
        }
    }

    public String[] getSeparatorList() {
        return separatorList;
    }

    public void setSeparatorList(String[] separatorList) {
        this.separatorList = separatorList;
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    protected abstract Object[] getMatrix();

    protected void updateSize(int width, int height) {
        dataHeight = height;
        dataWidth = width;
        // if is image
        if (height > 1) {
            this.viewerWidth = width;
            this.viewerHeight = height;
        }
        // If is spectrum
        else if (height == 1) {
            this.viewerWidth = height;
            this.viewerHeight = width;
        } else {
            this.viewerWidth = 0;
            this.viewerHeight = 0;
        }
    }

    protected Object getArrayValue() {
        Object array = null;
        if (matrix instanceof AbstractMatrix<?>) {
            array = ((AbstractMatrix<?>) matrix).getFlatValue();
        }
        return array;
    }

    protected void fillStringBuidler(Object array, StringBuilder builder) {
        if (array != null) {
            int col = 0;
            int row = 0;
            if (array instanceof Object[]) {
                Object[] flat = (Object[]) array;
                for (int i = 0; i < flat.length; i++) {
                    builder.append(flat[i]);
                    if (viewerWidth > 1) {
                        builder.append(separator);
                    }
                    col++;
                    if (col == viewerWidth) {
                        builder.append(NEW_LINE);
                        col = 0;
                        row++;
                    }
                }
            }
            if (row > 0) {
                builder.deleteCharAt(builder.length() - 1);
            }
        }
    }

    protected void updateText() {
        StringBuilder builder = new StringBuilder();
        Object array = getArrayValue();
        fillStringBuidler(array, builder);
        textArea.doSetText(builder.toString());
        tableModel.fireTableStructureChanged();
        lineNumber.setPreferredSize(null);
        Dimension preferredSize;
        if (lineNumber.getColumnCount() > 0) {
            preferredSize = new Dimension(lineNumber.getColumnModel().getColumn(0).getPreferredWidth(),
                    lineNumber.getPreferredSize().height);
        } else {
            preferredSize = new Dimension(50, lineNumber.getPreferredSize().height);
        }
        lineNumber.setPreferredSize(preferredSize);
        lineNumber.revalidate();
        ((JComponent) lineNumber.getParent()).revalidate();
        ((JComponent) lineNumber.getParent().getParent()).revalidate();
        if (caretAtTop) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    String text = textArea.getText();
                    if ((text != null) && (!text.isEmpty())) {
                        textArea.setCaretPosition(0);
                    }
                }
            };
            SwingUtilities.invokeLater(runnable);
        }
    }

    /**
     * Returns The {@link String} array this area is displaying
     * 
     * @param trim Whether to trim each {@link String}
     * @param allowEmptyLines Whether to allow empty {@link String}s
     * @return A {@link String} array
     */
    protected String[] getFlatValue(boolean trim, boolean allowEmptyLines) {
        String text = textArea.getText();
        String[] flatValues = null;
        if (text != null) {
            // FIRST REPLACE ALL SEPARATOR WITH \n
            for (String sep : separatorList) {
                if (!sep.equals(separator)) {
                    text = text.replaceAll(sep, NEW_LINE);
                }
            }
            textAreaHeight = 0;
            textAreaWidth = 0;
            // Split the text with \n to obtains lines number
            String[] lines = text.split(NEW_LINE);
            String[] col = null;
            if ((lines != null) && (lines.length > 0)) {
                textAreaHeight = lines.length;
                // Split the text with separator to obtains columns number
                col = lines[0].split(separator);
                textAreaWidth = col.length;
            }

            text = text.replaceAll(separator, NEW_LINE);
            flatValues = text.split(NEW_LINE);
            if (trim) {
                for (int i = 0; i < flatValues.length; i++) {
                    flatValues[i] = flatValues[i].trim();
                }
            }
            if (!allowEmptyLines) {
                List<String> list = new ArrayList<String>(flatValues.length);
                for (String value : flatValues) {
                    if (!value.isEmpty()) {
                        list.add(value);
                    }
                }
                flatValues = list.toArray(new String[list.size()]);
            }
        }
        return flatValues;
    }

    protected abstract void updateValue();

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return false;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        if (matrix != null) {
            dataWidth = matrix.getWidth();
        }
        return dataWidth;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        if (matrix != null) {
            dataHeight = matrix.getWidth();
        }
        return dataHeight;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    public void warnMediators() {
        Object[] object = getMatrix();
        warnMediators(new MatrixInformation(textArea, object));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        textArea.addMouseListener(listener);
    }

    @Override
    public CometeColor getCometeBackground() {
        return textArea.getCometeBackground();
    }

    @Override
    public CometeFont getCometeFont() {
        return textArea.getCometeFont();
    }

    @Override
    public CometeColor getCometeForeground() {
        return textArea.getCometeForeground();
    }

    @Override
    public int getHorizontalAlignment() {
        return textArea.getHorizontalAlignment();
    }

    @Override
    public String getToolTipText() {
        return textArea.getToolTipText();
    }

    @Override
    public boolean isEditingData() {
        return textArea.isEditingData();
    }

    @Override
    public void removeAllMouseListeners() {
        textArea.removeAllMouseListeners();
    }

    @Override
    public void removeMouseListener(IMouseListener mouseListener) {
        textArea.removeMouseListener(mouseListener);
    }

    @Override
    public void setCometeBackground(CometeColor colorBackGround) {
        textArea.setCometeBackground(colorBackGround);
    }

    @Override
    public void setCometeFont(CometeFont font) {
        textArea.setCometeFont(font);
    }

    @Override
    public void setCometeForeground(CometeColor colorForeground) {
        textArea.setCometeForeground(colorForeground);
    }

    @Override
    public void setHorizontalAlignment(int horizontalAlignement) {
        textArea.setHorizontalAlignment(horizontalAlignement);
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        setTextAreaOpaque(isOpaque);
        setMeOpaque(isOpaque);
        setButtonPanelOpaque(isOpaque);
    }

    protected boolean amIOpaque() {
        return super.isOpaque();
    }

    protected void setMeOpaque(boolean opaque) {
        super.setOpaque(opaque);
    }

    protected boolean isTextAreaOpaque() {
        return textArea == null ? false : textArea.isOpaque();
    }

    protected void setTextAreaOpaque(boolean opaque) {
        if (textArea != null) {
            textArea.setOpaque(opaque);
        }
    }

    protected boolean isButtonPanelOpaque() {
        return buttonPanel == null ? false : buttonPanel.isOpaque();
    }

    protected void setButtonPanelOpaque(boolean opaque) {
        if (buttonPanel != null) {
            buttonPanel.setOpaque(opaque);
        }
    }

    @Override
    public void setPreferredSize(int width, int height) {
        textArea.setPreferredSize(width, height);
    }

    @Override
    public void setTitledBorder(String titleBorder) {
        setBorder(new TitledBorder(titleBorder));
    }

    @Override
    public void setToolTipText(String toolTipText) {
        super.setToolTipText(toolTipText);
        if (buttonPanel != null) {
            buttonPanel.setToolTipText(toolTipText);
        }
        if (textArea != null) {
            textArea.setToolTipText(toolTipText);
        }
        if (scrollPane != null) {
            scrollPane.setToolTipText(toolTipText);
        }
    }

    @Override
    public String getText() {
        return textArea.getText();
    }

    @Override
    public void setText(String text) {
        textArea.setText(text);
    }

    @Override
    public void setEditable(boolean editable) {
        textArea.setEditable(editable);
    }

    @Override
    public boolean isEditable() {
        return textArea.isEditable();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (textArea != null) {
            textArea.setEnabled(enabled);
        }
        if (scrollPane != null) {
            scrollPane.setEnabled(enabled);
            scrollPane.getHorizontalScrollBar().setEnabled(enabled);
            scrollPane.getVerticalScrollBar().setEnabled(enabled);
            scrollPane.getViewport().setEnabled(enabled);
        }
        if (updateButton != null) {
            updateButton.setEnabled(enabled);
        }
        if (writeButton != null) {
            writeButton.setEnabled(enabled);
        }
        if (cancelButton != null) {
            cancelButton.setEnabled(enabled);
        }
    }

    @Override
    public void addTextAreaListener(ITextAreaListener textAreaListener) {
        textArea.addTextAreaListener(textAreaListener);
    }

    @Override
    public void removeTextAreaListener(ITextAreaListener removeTextAreaListener) {
        textArea.removeTextAreaListener(removeTextAreaListener);
    }

    @Override
    public void fireTextChanged(EventObject event) {
        textArea.fireTextChanged(event);
    }

    @Override
    public void fireActionPerformed(EventObject event) {
        textArea.fireActionPerformed(event);
    }

    @Override
    public void setColumns(int nbColumns) {
        textArea.setColumns(nbColumns);
    }

    @Override
    public void setRows(int nbRows) {
        textArea.setRows(nbRows);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    public JButton getUpdateButton() {
        return updateButton;
    }

    public JButton getWriteButton() {
        return writeButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public TextArea getTextArea() {
        return textArea;
    }

    public void setTextArea(TextArea textArea) {
        this.textArea = textArea;
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
