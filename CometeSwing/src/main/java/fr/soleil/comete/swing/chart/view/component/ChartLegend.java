/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.component;

import java.awt.Dimension;
import java.awt.Font;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collection;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.DataView;

/**
 * {@link AbstractChartLegend} for a {@link Chart}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ChartLegend extends AbstractChartLegend {

    private static final long serialVersionUID = 7901552659959788353L;

    public static final String Y1_NAME = "Y1";
    public static final String Y2_NAME = "Y2";

    private final WeakReference<Chart> chartReference;

    private boolean removingEnabled;

    private Font noSamplingFont;

    public ChartLegend(Chart chart) {
        super();
        this.chartReference = (chart == null ? null : new WeakReference<Chart>(chart));
        removingEnabled = false;
        computeNoSamplingFont();
    }

    protected void computeNoSamplingFont() {
        Font font = getLegendFont();
        int style;
        if (font == null) {
            noSamplingFont = null;
        } else {
            if (font.isBold()) {
                if (font.isItalic()) {
                    style = Font.PLAIN;
                } else {
                    style = Font.ITALIC;
                }
            } else if (font.isItalic()) {
                style = Font.BOLD;
            } else {
                style = Font.BOLD | Font.ITALIC;
            }
            noSamplingFont = font.deriveFont(style);
        }
    }

    @Override
    protected boolean isSamplingEnabled() {
        boolean enabled;
        Chart chart = getChart();
        if (chart == null) {
            enabled = false;
        } else {
            enabled = chart.getChartView().isSamplingAllowed();
        }
        return enabled;
    }

    @Override
    protected void computePreferredSize() {
        computeNoSamplingFont();
        super.computePreferredSize();
    }

    @Override
    protected Font getNoSamplingFont() {
        return noSamplingFont;
    }

    protected Chart getChart() {
        return chartReference == null ? null : chartReference.get();
    }

    @Override
    protected void repaintChartIfNecessary() {
        Chart chart = getChart();
        if (chart != null) {
            if (chart.isAutoHighlightOnLegend()) {
                chart.getChartView().repaint();
            }
        }
    }

    @Override
    protected void computeY1AndY2ViewLists() {
        Chart chart = getChart();
        if (chart == null) {
            y1Views = null;
            y2Views = null;
        } else {
            Collection<DataView>[] views = chart.getDataViewList().getDataViewsByAxis(chart.getDataViewComparator(),
                    IChartViewer.Y1, IChartViewer.Y2);
            if ((views == null) || (views.length != 2)) {
                y1Views = null;
                y2Views = null;
            } else {
                y1Views = views[0];
                y2Views = views[1];
            }
        }
    }

    @Override
    protected boolean isChartLegendVisible() {
        Chart chart = getChart();
        return chart == null ? true : chart.isLegendVisible();
    }

    @Override
    protected Font getLegendFont() {
        return getFont();
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        computeNoSamplingFont();
    }

    @Override
    protected int getChartLegendPlacement() {
        Chart chart = getChart();
        return chart == null ? IChartViewer.LABEL_ROW : chart.getLabelPlacement();
    }

    @Override
    protected Dimension getMarginSize() {
        return new Dimension(5, 5);
    }

    @Override
    protected String getY1Name() {
        return Y1_NAME;
    }

    @Override
    protected String getY2Name() {
        return Y2_NAME;
    }

    @Override
    protected void deleteDataView() {
        Chart chart = getChart();
        if ((chart != null) && (currentView != null)) {
            String id = currentView.getId();
            chart.removeData(Arrays.asList(id));
        }
    }

    @Override
    public boolean isDataViewRemovingEnabled() {
        return removingEnabled;
    }

    public void setDataViewRemovingEnabled(boolean removingEnabled) {
        this.removingEnabled = removingEnabled;
    }

    @Override
    protected boolean isDataViewClickable(String id) {
        return ((id != null) && (getChart() != null));
    }

    @Override
    protected void showDataOptionDialog(AbstractDataView view) {
        Chart chart = getChart();
        if (chart != null) {
            chart.showDataOptions(view);
        }
    }

    @Override
    protected boolean isAutoHighlightOnLegend() {
        Chart chart = getChart();
        return chart == null ? true : chart.isAutoHighlightOnLegend();
    }

    @Override
    protected boolean isDataViewHighlighted(String idToHighlight) {
        Chart chart = getChart();
        return chart == null ? false : chart.isDataViewHighlighted(idToHighlight);
    }

    @Override
    protected void setDataViewHighlighted(String idToHighlight, boolean highlighted) {
        Chart chart = getChart();
        if (chart != null) {
            chart.setDataViewHighlighted(idToHighlight, highlighted);
        }
    }

}
