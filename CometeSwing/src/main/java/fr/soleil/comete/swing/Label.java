/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.ILabel;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.comete.swing.util.ImageIconCometeImage;
import fr.soleil.comete.swing.util.ImageTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.IUnitTarget;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.swing.text.AntiAliasingDelegate;
import fr.soleil.lib.project.swing.text.DynamicForegroundLabel;

/**
 * Swing implementation of {@link ILabel}
 */
public class Label extends DynamicForegroundLabel implements ILabel, IUnitTarget {

    private static final long serialVersionUID = -5967068745657733796L;

    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;
    private String unit;
    private final Object bgLock;
    private String lastText;

    public Label() {
        super();
        lastText = ObjectUtils.EMPTY_STRING;
        bgLock = new Object();
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        unit = null;
        setSize(70, 25);
    }

    @Override
    protected AntiAliasingDelegate generateAntiAliasingDelegate() {
        return new AntiAliasingDelegate(true, this);
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void setSize(int width, int height) {
        super.setSize(width, height);
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        synchronized (bgLock) {
            if (color == null) {
                setOpaque(false);
            } else {
                setOpaque(true);
                setBackground(ColorTool.getColor(color));
            }
        }
    }

    @Override
    public void setCometeFont(CometeFont font) {
        super.setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        setForeground(ColorTool.getColor(color));
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public String getUnit() {
        return this.unit;
    }

    @Override
    public void setUnit(String unit) {
        this.unit = unit;
        applyUnit();
    }

    /**
     * Applies text with unit
     */
    protected void applyUnit() {
        String text = lastText;
        if (unit != null) {
            text = text + IDateConstants.SPACE_SEPARATOR + unit;
        }
        if (!ObjectUtils.sameObject(text, getText())) {
            super.setText(text);
        }
    }

    @Override
    public void setText(String text) {
        if (text == null) {
            text = ObjectUtils.EMPTY_STRING;
        }
        lastText = text;
        applyUnit();
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void setCometeImage(CometeImage image) {
        setIcon(ImageTool.getImage(image));
        revalidate();
    }

    @Override
    public CometeImage getCometeImage() {
        CometeImage cometeImage;
        Icon icon = getIcon();
        if (icon == null) {
            cometeImage = null;
        } else if (icon instanceof ImageIcon) {
            cometeImage = ImageTool.getCometeImage((ImageIcon) icon);
        } else {
            BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(),
                    BufferedImage.TYPE_INT_ARGB);
            icon.paintIcon(this, image.getGraphics(), 0, 0);
            cometeImage = new ImageIconCometeImage(new ImageIcon(image));
        }
        return cometeImage;
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
