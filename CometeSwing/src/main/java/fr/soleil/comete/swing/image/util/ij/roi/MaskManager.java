/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Rectangle;
import java.awt.Shape;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MaskException;
import fr.soleil.comete.definition.widget.util.MathematicSector;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.data.mediator.Mediator;
import ij.gui.Roi;
import ij.gui.ShapeRoi;

/**
 * 
 * A class to Handle Masks, using logger in case of problem.
 * 
 * @author pilif
 *
 */
public class MaskManager {

    public static final MaskManager DEFAULT_INSTANCE = new MaskManager();

    public static final String MASK = "mask_";
    public static final String ERROR_REMOVE_MASK = "Failed to remove from mask";
    public static final String ERROR_ADD_MASK = "Failed to add to mask";
    public static final String ERROR_SET_MASK = "Failed to set as mask";
    public static final String ERROR_LOAD_MASK = "Failed to load as mask";

    public MaskManager() {
        super();
    }

    /**
     * Removes the mask represented by the current validated Roi from existing mask
     * 
     * @param roiManager The {@link IJRoiManager} that knows the validated Roi
     * @param dimX The known image width
     * @param dimY The known image height
     * @param maskId The mask id to use
     * @param maskValue The current mask
     * @return A {@link Mask}
     */
    public Mask removeFromMask(final IJRoiManager roiManager, final int dimX, final int dimY, final int maskId,
            final boolean[] maskValue) {
        Mask mask = null;
        if ((roiManager != null) && (maskValue != null)) {
            Roi roi = roiManager.getValidatedRoi();
            if (roi != null) {
                mask = removeFromMask(roi, roiManager.getInnerRois().contains(roi), dimX, dimY, maskId, maskValue);
            }
        }
        return mask;
    }

    /**
     * Removes the mask represented by a {@link Roi} from existing mask
     * 
     * @param roi The {@link Roi}
     * @param innerRoi Whether the {@link Roi} should be considered as an inner Roi
     * @param dimX The known image width
     * @param dimY The known image height
     * @param maskId The mask id to use
     * @param maskValue The current mask
     * @return A {@link Mask}
     */
    public Mask removeFromMask(Roi roi, boolean innerRoi, final int dimX, final int dimY, final int maskId,
            final boolean[] maskValue) {
        Mask mask = null;
        if ((maskValue != null) && (roi != null)) {
            boolean[] toTreat = new boolean[dimY * dimX];
            Rectangle roiBounds = roi.getBounds();
            int[] delta = extractShapeTranslation(roi, roiBounds);
            Shape roiShape = getShape(roi);
            int minYtop = Math.max(roiBounds.y, 0);
            int maxYtop = Math.min(minYtop, dimY);
            int maxYroi = Math.min(roiBounds.y + roiBounds.height, dimY);
            int minXleft = Math.max(roiBounds.x, 0);
            int maxXleft = Math.min(minXleft, dimX);
            int maxXroi = Math.min(roiBounds.x + roiBounds.width, dimX);
            if (innerRoi) {
                // fill top rectangle
                for (int y = 0; y < maxYtop; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = maskValue[y * dimX + x];
                    }
                }
                for (int y = maxYtop; y < maxYroi; y++) {
                    // fill left rectangle
                    for (int x = 0; x < maxXleft; x++) {
                        toTreat[y * dimX + x] = maskValue[y * dimX + x];
                    }
                    // fill roi rectangle
                    for (int x = maxXleft; x < maxXroi; x++) {
                        if (contains(roi, roiShape, x, y, delta[0], delta[1])) {
                            toTreat[y * dimX + x] = true;
                        } else {
                            toTreat[y * dimX + x] = maskValue[y * dimX + x];
                        }
                    }
                    // fill right rectangle
                    for (int x = maxXroi; x < dimX; x++) {
                        toTreat[y * dimX + x] = maskValue[y * dimX + x];
                    }
                }
                // fill bottom rectangle
                for (int y = maxYroi; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = maskValue[y * dimX + x];
                    }
                }
            } // end if (getRoiManager().getInnerRois().contains(roi))
            else {
                // fill top rectangle
                for (int y = 0; y < maxYtop; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = true;
                    }
                }
                for (int y = maxYtop; y < maxYroi; y++) {
                    // fill left rectangle
                    for (int x = 0; x < maxXleft; x++) {
                        toTreat[y * dimX + x] = true;
                    }
                    // fill roi rectangle
                    for (int x = maxXleft; x < maxXroi; x++) {
                        if (contains(roi, roiShape, x, y, delta[0], delta[1])) {
                            toTreat[y * dimX + x] = maskValue[y * dimX + x];
                        } else {
                            toTreat[y * dimX + x] = true;
                        }
                    }
                    // fill right rectangle
                    for (int x = maxXroi; x < dimX; x++) {
                        toTreat[y * dimX + x] = true;
                    }
                }
                // fill bottom rectangle
                for (int y = maxYroi; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = true;
                    }
                }
            } // end if (innerRoi)...else

            try {
                mask = new Mask(toTreat, dimX, dimY, MASK + maskId);
            } catch (MaskException me) {
                // shall never happen
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(ERROR_REMOVE_MASK, me);
            }
        }
        return mask;
    }

    /**
     * Adds the mask represented by the current validated Roi to existing mask
     * 
     * @param roiManager The {@link IJRoiManager} that knows the validated Roi
     * @param dimX The known image width
     * @param dimY The known image height
     * @param maskId The mask id to use
     * @param maskValue The current mask
     * @return A {@link Mask}
     */
    public Mask addToMask(final IJRoiManager roiManager, final int dimX, final int dimY, final int maskId,
            final boolean[] maskValue) {
        Mask mask = null;
        if (roiManager != null && maskValue != null) {
            Roi roi = roiManager.getValidatedRoi();
            if (roi != null) {
                mask = addToMask(roi, roiManager.getInnerRois().contains(roi), dimX, dimY, maskId, maskValue);
            }
        }
        return mask;
    }

    /**
     * Adds the mask represented by a {@link Roi} to existing mask
     * 
     * @param roi The {@link Roi}
     * @param innerRoi Whether the {@link Roi} should be considered as an inner Roi
     * @param dimX The known image width
     * @param dimY The known image height
     * @param maskId The mask id to use
     * @param maskValue The current mask
     * @return A {@link Mask}
     */
    public Mask addToMask(Roi roi, boolean innerRoi, final int dimX, final int dimY, final int maskId,
            final boolean[] maskValue) {
        Mask mask = null;
        if ((maskValue != null) && (roi != null)) {
            boolean[] toTreat = new boolean[dimY * dimX];
            Rectangle roiBounds = roi.getBounds();
            int[] delta = extractShapeTranslation(roi, roiBounds);
            Shape roiShape = getShape(roi);
            int minYtop = Math.max(roiBounds.y, 0);
            int maxYtop = Math.min(minYtop, dimY);
            int maxYroi = Math.min(roiBounds.y + roiBounds.height, dimY);
            int minXleft = Math.max(roiBounds.x, 0);
            int maxXleft = Math.min(minXleft, dimX);
            int maxXroi = Math.min(roiBounds.x + roiBounds.width, dimX);
            if (innerRoi) {
                // fill top rectangle
                for (int y = 0; y < maxYtop; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = false;
                    }
                }
                for (int y = maxYtop; y < maxYroi; y++) {
                    // fill left rectangle
                    for (int x = 0; x < maxXleft; x++) {
                        toTreat[y * dimX + x] = false;
                    }
                    // fill roi rectangle
                    for (int x = maxXleft; x < maxXroi; x++) {
                        if (contains(roi, roiShape, x, y, delta[0], delta[1])) {
                            toTreat[y * dimX + x] = maskValue[y * dimX + x];
                        } else {
                            toTreat[y * dimX + x] = false;
                        }
                    }
                    // fill right rectangle
                    for (int x = maxXroi; x < dimX; x++) {
                        toTreat[y * dimX + x] = false;
                    }
                }
                // fill bottom rectangle
                for (int y = maxYroi; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = false;
                    }
                }
            } // end if (getRoiManager().getInnerRois().contains(roi))
            else {
                // fill top rectangle
                for (int y = 0; y < maxYtop; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = maskValue[y * dimX + x];
                    }
                }
                for (int y = maxYtop; y < maxYroi; y++) {
                    // fill left rectangle
                    for (int x = 0; x < maxXleft; x++) {
                        toTreat[y * dimX + x] = maskValue[y * dimX + x];
                    }
                    // fill roi rectangle
                    for (int x = maxXleft; x < maxXroi; x++) {
                        if (contains(roi, roiShape, x, y, delta[0], delta[1])) {
                            toTreat[y * dimX + x] = false;
                        } else {
                            toTreat[y * dimX + x] = maskValue[y * dimX + x];
                        }
                    }
                    // fill right rectangle
                    for (int x = maxXroi; x < dimX; x++) {
                        toTreat[y * dimX + x] = maskValue[y * dimX + x];
                    }
                }
                // fill bottom rectangle
                for (int y = maxYroi; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = maskValue[y * dimX + x];
                    }
                }
            } // end if (innerRoi)...else
            try {
                mask = new Mask(toTreat, dimX, dimY, MASK + maskId);
            } catch (MaskException me) {
                // shall never happen
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(ERROR_ADD_MASK, me);
            }
        }
        return mask;
    }

    /**
     * Sets the mask to use from current validated Roi
     * 
     * @param roiManager The {@link IJRoiManager} that knows the validated Roi
     * @param dimX The known image width
     * @param dimY The known image height
     * @param maskId The mask id to use
     * @return A {@link Mask}
     */
    public Mask setAsMask(final IJRoiManager roiManager, final int dimX, final int dimY, final int maskId) {
        Mask mask = null;
        if (roiManager != null && dimX > 0 && dimY > 0) {
            Roi roi = roiManager.getValidatedRoi();
            if (roi != null) {
                mask = setAsMask(roi, roiManager.getInnerRois().contains(roi), dimX, dimY, maskId);
            }
        }
        return mask;
    }

    /**
     * Sets the mask to use from a {@link Roi}
     * 
     * @param roi The {@link Roi}
     * @param innerRoi Whether the {@link Roi} should be considered as an inner Roi
     * @param dimX The known image width
     * @param dimY The known image height
     * @param maskId The mask id to use
     * @return A {@link Mask}
     */
    public Mask setAsMask(Roi roi, boolean innerRoi, final int dimX, final int dimY, final int maskId) {
        Mask mask = null;
        if (roi != null) {
            boolean[] toTreat = new boolean[dimY * dimX];
            Rectangle roiBounds = roi.getBounds();
            int[] delta = extractShapeTranslation(roi, roiBounds);
            Shape roiShape = getShape(roi);
            int minYtop = Math.max(roiBounds.y, 0);
            int maxYtop = Math.min(minYtop, dimY);
            int maxYroi = Math.min(roiBounds.y + roiBounds.height, dimY);
            int minXleft = Math.max(roiBounds.x, 0);
            int maxXleft = Math.min(minXleft, dimX);
            int maxXroi = Math.min(roiBounds.x + roiBounds.width, dimX);
            if (innerRoi) {
                // fill top rectangle
                for (int y = 0; y < maxYtop; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = false;
                    }
                }
                for (int y = maxYtop; y < maxYroi; y++) {
                    // fill left rectangle
                    for (int x = 0; x < maxXleft; x++) {
                        toTreat[y * dimX + x] = false;
                    }
                    // fill roi rectangle
                    for (int x = maxXleft; x < maxXroi; x++) {
                        if (contains(roi, roiShape, x, y, delta[0], delta[1])) {
                            toTreat[y * dimX + x] = true;
                        } else {
                            toTreat[y * dimX + x] = false;
                        }
                    }
                    // fill right rectangle
                    for (int x = maxXroi; x < dimX; x++) {
                        toTreat[y * dimX + x] = false;
                    }
                }
                // fill bottom rectangle
                for (int y = maxYroi; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = false;
                    }
                }
            } // end if (imp.getInnerRois().contains(roi))
            else {
                // fill top rectangle
                for (int y = 0; y < maxYtop; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = true;
                    }
                }
                for (int y = maxYtop; y < maxYroi; y++) {
                    // fill left rectangle
                    for (int x = 0; x < maxXleft; x++) {
                        toTreat[y * dimX + x] = true;
                    }
                    // fill roi rectangle
                    for (int x = maxXleft; x < maxXroi; x++) {
                        if (contains(roi, roiShape, x, y, delta[0], delta[1])) {
                            toTreat[y * dimX + x] = false;
                        } else {
                            toTreat[y * dimX + x] = true;
                        }
                    }
                    // fill right rectangle
                    for (int x = maxXroi; x < dimX; x++) {
                        toTreat[y * dimX + x] = true;
                    }
                }
                // fill bottom rectangle
                for (int y = maxYroi; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        toTreat[y * dimX + x] = true;
                    }
                }
            } // end if (innerRoi)...else
            try {
                mask = new Mask(toTreat, dimX, dimY, MASK + maskId);
            } catch (MaskException me) {
                // shall never happen
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(ERROR_SET_MASK, me);
            }
        }
        return mask;
    }

    /**
     * Loads a {@link Mask}, adding it or not to previous {@link Mask}
     * 
     * @param formerMask The previous {@link Mask}
     * @param addToCurrentMask Whether to add loaded {@link Mask} to previous {@link Mask}
     * @param maskPath Path of the file that contains the representation of the {@link Mask}
     * @return A {@link Mask}
     */
    public Mask loadMask(final Mask formerMask, final boolean addToCurrentMask, final Mask maskPath) {
        Mask mask = null;
        if (addToCurrentMask && (formerMask != null) && (formerMask.getValue().length == maskPath.getValue().length)) {
            boolean[] newMaskTab = new boolean[maskPath.getValue().length];
            for (int i = 0; i < newMaskTab.length; i++) {
                newMaskTab[i] = formerMask.getValue()[i] && maskPath.getValue()[i];
            }
            try {
                mask = new Mask(newMaskTab, maskPath.getWidth(), maskPath.getHeight(), maskPath.getName());
            } catch (MaskException e) {
                // Should not happen;
            }
        }
        return mask;
    }

    /**
     * Extracts the {@link Mask} from a {@link MathematicSector}
     * 
     * @param sector The {@link MathematicSector}
     * @param dimX The known image width
     * @param dimY The known image height
     * @param previewSector Whether to generate the {@link Mask} when {@link MathematicSector} is still under
     *            construction
     * @return A {@link Mask}
     */
    public Mask updateSectorMask(final MathematicSector sector, final int dimX, final int dimY,
            final boolean previewSector) {
        Mask sectorMask = null;
        if (sector != null) {
            if (sector.isConstructing()) {
                if (previewSector) {
                    sectorMask = sector.generateMask(dimX, dimY);
                }
            } else {
                sectorMask = sector.generateMask(dimX, dimY);
            }
        }
        return sectorMask;
    }

    protected static Shape extractShape(Roi roi) {
        Shape shape;
        if (roi instanceof ShapeRoi) {
            shape = ((ShapeRoi) roi).getShape();
        } else {
            shape = roi.getPolygon();
        }
        return shape;
    }

    protected static int[] extractShapeTranslation(Roi roi, Rectangle roiBounds) {
        int deltaX, deltaY;
        if (roi instanceof ShapeRoi) {
            deltaX = roiBounds.x;
            deltaY = roiBounds.y;
        } else {
            deltaX = 0;
            deltaY = 0;
        }
        return new int[] { deltaX, deltaY };
    }

    protected Shape getShape(Roi roi) {
        // don't extract shape by default as it costs something to create it
        return null;
    }

    protected boolean contains(Roi roi, Shape roiShape, int x, int y, int deltaX, int deltaY) {
        boolean contains = roi.contains(x, y);
        return contains;
    }

}
