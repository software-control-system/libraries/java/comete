/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util.mask;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import fr.soleil.comete.swing.util.CometeConstants;

public class MaskedTableRenderer extends DefaultTableCellRenderer implements CometeConstants {

    private static final long serialVersionUID = 2404228240930208815L;

    protected final JLabel maskedLabel;

    public MaskedTableRenderer() {
        super();
        maskedLabel = new JLabel();
        maskedLabel.setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        JLabel component = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                column);
        boolean maskedZone = false;
        if (table instanceof MaskedTable) {
            maskedZone = ((MaskedTable) table).isHiddenByMask(row, column);
        }
        if (maskedZone) {
            maskedLabel.setText(component.getText());
            maskedLabel.setBorder(component.getBorder());
            maskedLabel.setFont(component.getFont());
            if (isSelected) {
                maskedLabel.setBackground(((MaskedTable) table).getMaskSelectionColor());
            } else {
                maskedLabel.setBackground(((MaskedTable) table).getMaskColor());
            }
            maskedLabel.setForeground(component.getForeground());
            maskedLabel.setFocusable(component.isFocusable());
            maskedLabel.setToolTipText(NAN);
            component = maskedLabel;
        } else {
            component.setToolTipText(component.getText());
        }
        return component;
    }
}
