/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.util.CometeDefinitionUtils;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.IChartConst.FormatNAN;
import fr.soleil.comete.definition.widget.util.IChartConst.Orientation;
import fr.soleil.comete.swing.chart.axis.view.AbstractAxisView;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.ChartData;
import fr.soleil.comete.swing.chart.data.DataList;
import fr.soleil.comete.swing.chart.data.LineInfo;
import fr.soleil.comete.swing.chart.data.ParsedLineInfo;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.swing.util.SwingFormat;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.file.log.CsvLogFileWriter;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.math.NumberArrayUtils;
import fr.soleil.lib.project.math.NumberUtils;
import fr.soleil.lib.project.resource.Utf8PropertyResourceBundle;
import fr.soleil.lib.project.swing.WindowSwingUtils;

/**
 * Class that contains some useful methods and constants for charts.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ChartUtils implements CometeConstants {

    public static final ResourceBundle MESSAGES;
    static {
        String baseName = "fr.soleil.comete.swing.chart.messages";
        Utf8PropertyResourceBundle bundle;
        try {
            bundle = new Utf8PropertyResourceBundle(baseName, Locale.getDefault(), ChartUtils.class.getClassLoader());
        } catch (Exception e) {
            try {
                bundle = new Utf8PropertyResourceBundle(baseName, Locale.US, ChartUtils.class.getClassLoader());
            } catch (IOException e1) {
                throw new IllegalArgumentException("Chart messages not found", e);
            }
        }
        MESSAGES = bundle;
    }

    protected static final String PRECISION_FORMAT = MESSAGES.getString("fr.soleil.comete.swing.chart.precision.set");
    protected static final String[] LABEL_FORMATS = { "%g", ObjectUtils.EMPTY_STRING, "%02d:%02d:%02d", "%d", "%X",
            "%b" };
    protected static final GregorianCalendar CALENDAR = new GregorianCalendar();

    protected static final String POSITIVE_INFINITY = "+Infinity";
    protected static final String NEGATIVE_INFINITY = "+Infinity";
    protected static final String NX_EXTRACTOR_COMMENT = "#";
    protected static final String NX_EXTRACTOR_INDEX = "#index";
    public static final String SPACES = " +";
    public static final String CSV_SEPARATOR = ";";
    protected static final String DOUBLE_CSV_SEPARATOR = ";;";
    public static final String TAB = "\t";
    public static final String TABS = "\t+";
    protected static final String SEPARATOR = "-";
    protected static final String ZERO = "0";
    protected static final String FLOAT_FORMAT_START = "%.";
    protected static final String FLOAT_FORMAT_END = "f";
    protected static final String SCIENTIFIC_FORMAT = "%1$.2fe%2$d";
    protected static final String SCIENTIFIC_INT_FORMAT = "%1$de%2$d";
    protected static final String FAILED_TO_READ_FILE = "Failed to read file: ";
    protected static final String OPEN_PARENTHESIS = "(";
    protected static final String CLOSE_PARENTHESIS = ")";
    protected static final String S = "s";
    public static final String DOT = ".";
    protected static final String STATISTICS = "Statistics: ";
    protected static final String HTML_STATISTICS_START = MESSAGES
            .getString("fr.soleil.comete.swing.chart.statistics.start");
    protected static final String SPAN_END = "</span>";
    protected static final String SPAN_OBTAINED_AT_X = MESSAGES
            .getString("fr.soleil.comete.swing.chart.statistics.ontained.at.x");
    protected static final String SPAN_LI_END = "</span></li>";
    protected static final String HTML_MAXIMUM = MESSAGES.getString("fr.soleil.comete.swing.chart.statistics.maximum");
    protected static final String HTML_AVERAGE = MESSAGES.getString("fr.soleil.comete.swing.chart.statistics.average");
    protected static final String HTML_STANDARD_DEVIATION = MESSAGES
            .getString("fr.soleil.comete.swing.chart.statistics.stddev");
    protected static final String HTML_SAMPLE_STANDARD_DEVIATION = MESSAGES
            .getString("fr.soleil.comete.swing.chart.statistics.sstddev");
    protected static final String HTML_STATISTICS_END = "</span></li></body></html>";
    protected static final String FAILED_TO_OBTAIN_STATISTICS_ON = MESSAGES
            .getString("fr.soleil.comete.swing.chart.statistics.failed");
    protected static final String TIME = MESSAGES.getString("fr.soleil.comete.swing.chart.time");
    protected static final String DATE = MESSAGES.getString("fr.soleil.comete.swing.chart.date");
    protected static final String ERROR = MESSAGES.getString("fr.soleil.comete.swing.chart.error");
    protected static final String ERROR_IN_DATA_COUNT = MESSAGES
            .getString("fr.soleil.comete.swing.chart.error.data.count");
    protected static final String FAILED_TO_LOAD_FILE = MESSAGES
            .getString("fr.soleil.comete.swing.chart.file.load.failed");
    protected static final String FAILED_TO_CLOSE_FILE = MESSAGES
            .getString("fr.soleil.comete.swing.chart.file.close.failed");
    public static final String NEW_LINE = ObjectUtils.NEW_LINE;
    protected static final String GRAPH_DATA = MESSAGES.getString("fr.soleil.comete.swing.chart.graph.data");
    protected static final String OK = "ok";

    public static final long YEAR = 31536000000l;
    public static final long MONTH = 2592000000l;
    public static final long DAY = 86400000l;
    public static final long HOUR = 3600000l;
    public static final long MINU = 60000l;
    public static final long SECO = 1000l;
    public static final long MILLI = 1l;
    /**
     * @deprecated You should use {@link MathConst#PRECISION_LIMIT}
     */
    @Deprecated
    public static final double AXIS_MINIMUM_SIZE = MathConst.PRECISION_LIMIT;
    protected static final String DATE_YEAR = IDateConstants.YEAR_LONG_FORMAT; // yyyy
    protected static final String DATE_MONTH = IDateConstants.DATE_MONTH_FORMAT; // MMMM yy
    protected static final String DATE_WEEK = IDateConstants.DATE_WEEK; // dd/MM/yy
    protected static final String DATE_DAY = IDateConstants.DATE_DAY; // EE dd
    protected static final String TIME_HOUR_12 = IDateConstants.TIME_HOUR_12; // EEE HH:mm
    protected static final String TIME_HOUR = IDateConstants.TIME_HOUR; // HH:mm
    protected static final String TIME_SECOND = IDateConstants.TIME_SECOND;// HH:mm:ss
    protected static final String TIME_MILLI_SECOND = IDateConstants.TIME_MILLI_SECOND; // HH:mm:ss.SSS
    public static final SimpleDateFormat YEAR_FORMAT = new SimpleDateFormat(DATE_YEAR);
    public static final SimpleDateFormat MONTH_FORMAT = new SimpleDateFormat(DATE_MONTH);
    public static final SimpleDateFormat WEEK_FORMAT = new SimpleDateFormat(DATE_WEEK);
    public static final SimpleDateFormat DAY_FORMAT = new SimpleDateFormat(DATE_DAY);
    public static final SimpleDateFormat HOUR_12_FORMAT = new SimpleDateFormat(TIME_HOUR_12);
    public static final SimpleDateFormat HOUR_FORMAT = new SimpleDateFormat(TIME_HOUR);
    public static final SimpleDateFormat SECOND_FORMAT = new SimpleDateFormat(TIME_SECOND);
    public static final SimpleDateFormat MILLI_SECOND_FORMAT = new SimpleDateFormat(TIME_MILLI_SECOND);
    protected static final SimpleDateFormat GENERAL_DATE_FORMAT = new SimpleDateFormat(
            IDateConstants.DATE_TIME_LONG_FORMAT);
    protected static final SimpleDateFormat GENERAL_SHORT_DATE_FORMAT = new SimpleDateFormat(
            IDateConstants.DATE_TIME_FORMAT);

    public static final double[] TIME_STEP_PRECISIONS = { MILLI, MILLI * 10, MILLI * 100, SECO, 5 * SECO, 10 * SECO,
            30 * SECO, MINU, 5 * MINU, 10 * MINU, 30 * MINU, HOUR, 3 * HOUR, 6 * HOUR, 12 * HOUR, DAY, 7 * DAY, MONTH,
            YEAR, 5 * YEAR, 10 * YEAR };
    public static final double[] TIME_START_PRECISIONS = { MILLI, MILLI, MILLI, SECO, SECO, SECO, SECO, MINU, MINU,
            MINU, MINU, HOUR, HOUR, HOUR, HOUR, DAY, DAY, MONTH, YEAR, YEAR, YEAR };
    public static final SimpleDateFormat[] TIME_FORMATS = { MILLI_SECOND_FORMAT, MILLI_SECOND_FORMAT,
            MILLI_SECOND_FORMAT, SECOND_FORMAT, SECOND_FORMAT, SECOND_FORMAT, SECOND_FORMAT, SECOND_FORMAT,
            SECOND_FORMAT, SECOND_FORMAT, HOUR_FORMAT, HOUR_FORMAT, HOUR_FORMAT, HOUR_FORMAT, HOUR_12_FORMAT,
            DAY_FORMAT, WEEK_FORMAT, MONTH_FORMAT, YEAR_FORMAT, YEAR_FORMAT, YEAR_FORMAT };

    public static final String[] DATE_FORMATS = { DATE_YEAR, DATE_MONTH, DATE_WEEK, DATE_DAY };
    public static final String[] TIMER_FORMATS = { TIME_MILLI_SECOND, TIME_SECOND, TIME_HOUR };

    /**
     * Returns a representation of the double according to the format
     * 
     * @param valueToFormat double to convert
     * @param precision Desired precision (Pass 0 to not perform precision rounding).
     * @param labelFormat The {@link IChartViewer}'s label format to use
     * @param valueFormat The date format to use, in case of date
     * @return A string containing a formated representation of the given double.
     */
    public static String formatValue(double valueToFormat, double precision, int labelFormat, String valueFormat) {
        String formattedValue;
        if (Double.isNaN(valueToFormat)) {
            formattedValue = NAN;
        } else {
            valueToFormat = NumberUtils.getDouble(valueToFormat, true);
            if (Format.isNumberFormatOK(valueFormat)) {
                formattedValue = Format.formatValue(valueToFormat, valueFormat);
            } else {
                formattedValue = null;
            }
        }
        if ((formattedValue == null) || (formattedValue.trim().isEmpty())) {
            // Round value to nearest multiple of precision
            if (precision != 0) {
                boolean isNegative = (valueToFormat < 0.0);
                if (isNegative) {
                    valueToFormat = -valueToFormat;
                }
                // Find multiple
                double i = Math.floor(0.5d + valueToFormat / precision);
                valueToFormat = i * precision;
                valueToFormat = NumberUtils.getDouble(valueToFormat, true);
                if (isNegative) {
                    valueToFormat = -valueToFormat;
                }
            }
            switch (labelFormat) {
                case IChartViewer.SCIENTIFIC_FORMAT:
                    formattedValue = toScientific(valueToFormat);
                    break;
                case IChartViewer.SCIENTIFICINT_FORMAT:
                    formattedValue = toScientificInt(valueToFormat);
                    break;
                case IChartViewer.BININT_FORMAT:
                    formattedValue = Integer.toBinaryString((int) valueToFormat);
                    break;
                case IChartViewer.DECINT_FORMAT:
                case IChartViewer.HEXINT_FORMAT:
                    Object[] o2 = { Integer.valueOf((int) (Math.abs(valueToFormat) + 0.5)) };
                    if (valueToFormat < 0.0) {
                        formattedValue = SEPARATOR + SwingFormat.sprintf(LABEL_FORMATS[labelFormat], o2);
                    } else {
                        formattedValue = SwingFormat.sprintf(LABEL_FORMATS[labelFormat], o2);
                    }
                    break;
                case IChartViewer.TIME_FORMAT:
                    int sec = (int) (Math.abs(valueToFormat));
                    Object[] o3 = { Integer.valueOf(sec / 3600), Integer.valueOf((sec % 3600) / 60),
                            Integer.valueOf(sec % 60) };
                    if (valueToFormat < 0.0) {
                        formattedValue = SEPARATOR + SwingFormat.sprintf(LABEL_FORMATS[labelFormat], o3);
                    } else {
                        formattedValue = SwingFormat.sprintf(LABEL_FORMATS[labelFormat], o3);
                    }
                    break;
                case IChartViewer.DATE_FORMAT:
                    SimpleDateFormat format = new SimpleDateFormat(valueFormat);
                    long millisec = (long) (Math.abs(valueToFormat) * 1000);
                    formattedValue = format.format(new Date(millisec));
                    break;
                default:
                    // Auto format
                    if (valueToFormat == 0.0) {
                        formattedValue = ZERO;
                    } else if (Math.abs(valueToFormat) <= 1.0E-4) {
                        formattedValue = toScientific(valueToFormat);
                    } else {
                        int nbDigit = -(int) Math.floor(Math.log10(precision));
                        if (nbDigit <= 0) {
                            formattedValue = suppressZero(Double.toString(valueToFormat));
                        } else {
                            // add one more digit, just in case... (JAVAAPI-251)
                            nbDigit++;
                            String dFormat = FLOAT_FORMAT_START + nbDigit + FLOAT_FORMAT_END;
                            // remove useless zeros
                            formattedValue = suppressZero(Format.formatValue(valueToFormat, dFormat));
                        }
                    }
                    break;
            }
        }
        return formattedValue;
    }

    /**
     * Return a scientific (exponential) representation of the double.
     * 
     * @param d double to convert
     * @return A string continaing a scientific representation of the given double.
     */
    public static String toScientific(double d) {
        double a = Math.abs(d);
        int e = 0;
        String f = SCIENTIFIC_FORMAT;
        if (a != 0) {
            if (a < 1) {
                while (a < 1) {
                    a = a * 10;
                    e--;
                }
            } else {
                while (a >= 10) {
                    a = a / 10;
                    e++;
                }
            }
        }
        if (a >= 9.999999999) {
            a = a / 10;
            e++;
        }
        if (d < 0) {
            a = -a;
        }
        Object o[] = { Double.valueOf(a), Integer.valueOf(e) };
        return String.format(Locale.US, f, o);
    }

    public static String toScientificInt(double d) {
        double a = Math.abs(d);
        int e = 0;
        String f = SCIENTIFIC_INT_FORMAT;
        if (a != 0) {
            if (a < 1) {
                while (a < 1) {
                    a = a * 10;
                    e--;
                }
            } else {
                while (a >= 10) {
                    a = a / 10;
                    e++;
                }
            }
        }
        if (a >= 9.999999999) {
            a = a / 10;
            e++;
        }
        if (d < 0) {
            a = -a;
        }
        Object o[] = { Integer.valueOf((int) Math.rint(a)), Integer.valueOf(e) };
        return String.format(Locale.US, f, o);
    }

    /**
     * Suppresses last non significant zeros
     * 
     * @param n String representing a floating number
     */
    public static String suppressZero(String n) {
        String result;
        boolean hasDecimal = n.indexOf('.') != -1;
        if (hasDecimal) {
            StringBuilder str = new StringBuilder(n);
            int eIndex = n.indexOf('e');
            if (eIndex < 0) {
                eIndex = n.indexOf('E');
            }
            if (eIndex < 0) {
                eIndex = str.length();
            }
            int i = eIndex - 1;
            while ((i > -1) && (str.charAt(i) == '0')) {
                str.deleteCharAt(i);
                i--;
            }
            if (str.charAt(i) == '.') {
                // Remove unwanted decimal
                str.deleteCharAt(i);
            }
            result = str.toString();

        } else {
            result = n;
        }
        return result;
    }

    /**
     * Returns a representation of the double in time format "EEE, d MMM yyyy HH:mm:ss".
     * 
     * @param valueToFormat number of milliseconds since epoch
     * @return A string containing a time representation of the given double.
     */
    public static String formatTimeValue(double valueToFormat) {
        return formatTimeValue((long) valueToFormat);
    }

    /**
     * Returns a representation of the long in time format
     * "EEE, d MMM yyyy HH:mm:ss".
     * 
     * @param valueToFormat number of milliseconds since epoch
     * @return A string containing a time representation of the given long.
     */
    public static String formatTimeValue(long valueToFormat) {
        Date date = parseTimeLong(String.valueOf(valueToFormat));
        return GENERAL_DATE_FORMAT.format(date);
    }

    /**
     * Parses a formatted time {@link String} to produce a {@link Date}
     * 
     * @param formattedTime The formatted time {@link String}
     * @return A {@link Date}. <code>null</code> if the {@link String} could not be parsed
     */
    public static Date parseTimeString(String formattedTime) {
        Date date;
        try {
            date = GENERAL_DATE_FORMAT.parse(formattedTime);
        } catch (Exception e) {
            try {
                // backward compatibility with former date format
                date = GENERAL_SHORT_DATE_FORMAT.parse(formattedTime);
            } catch (Exception e2) {
                date = null;
            }
        }
        if (date == null) {
            // backward compatibility with former date format
            double time;
            try {
                time = Double.parseDouble(formattedTime);
                long timeInMillis = (long) (time * 1000);
                synchronized (CALENDAR) {
                    CALENDAR.setTimeInMillis(timeInMillis);
                    date = CALENDAR.getTime();
                }
            } catch (Exception e) {
                date = null;
            }
        }
        return date;
    }

    /**
     * Parse the given date with the given userFormat
     * 
     * @param formattedTime , the date to parse
     * @param userFormat , the user format
     * @return the date object
     */
    public static Date parseTimeString(String formattedTime, String userFormat) {
        Date date = null;
        if (userFormat == null || userFormat.trim().isEmpty() || userFormat.trim().equalsIgnoreCase(S)) {
            date = parseTimeLong(formattedTime);
        } else {// There is a format
            try {
                SimpleDateFormat userDateFormat = new SimpleDateFormat(userFormat);
                date = userDateFormat.parse(formattedTime);
            } catch (Exception e4) {
                date = parseTimeLong(formattedTime);
            }
        }

        if (date == null) {// Should not happen
            date = parseTimeString(formattedTime);
        }

        return date;
    }

    private static Date parseTimeLong(String time) {
        Date date = null;
        if ((time != null) && (!time.trim().isEmpty())) {
            try {
                double doubleValue = Double.parseDouble(time.trim());
                if (time.contains(DOT)) {
                    doubleValue = doubleValue * 1000;
                }
                date = new Date((long) doubleValue);
            } catch (NumberFormatException e) {
                date = null;
            }
        }
        return date;

    }

    /**
     * Changes an {@link AbstractDataView}'s data
     * 
     * @param dataView The {@link AbstractDataView}
     * @param value The data to set
     */
    public static void updateDataView(AbstractDataView dataView, Object value) {
        if (value != null) {
            if (value instanceof double[]) {
                updateDataView(dataView, (double[]) value);
            } else if ((value instanceof byte[]) || (value instanceof short[]) || (value instanceof int[])
                    || (value instanceof long[]) || (value instanceof float[]) || (value instanceof Number[])) {
                updateDataView(dataView, NumberArrayUtils.extractDoubleArray(value));
            } else if (value instanceof Object[]) {
                updateDataView(dataView, (Object[]) value);
            }
        }
    }

    protected static double extractDouble(Number n) {
        return n == null ? MathConst.NAN_FOR_NULL : n.doubleValue();
    }

    private static void updateDataView(AbstractDataView dataView, double[] value) {
        if (dataView != null) {
            if ((value != null) && (value.length > 0) && (value.length % 2 == 0)) {
                dataView.setData(value);
            } else {
                dataView.reset();
            }
        }
    }

    private static void updateDataView(AbstractDataView dataView, Object[] value) {
        if ((dataView != null) && (value != null) && (value.length >= 2) && (value[0] != null) && (value[1] != null)) {
            double[] x = NumberArrayUtils.extractDoubleArray(value[IChartViewer.X_INDEX]);
            double[] y = NumberArrayUtils.extractDoubleArray(value[IChartViewer.Y_INDEX]);
            double[] error = null;
            if (value.length > 2) {
                error = NumberArrayUtils.extractDoubleArray(value[IChartViewer.ERROR_INDEX]);
            }
            if ((x != null) && (y != null)) {
                dataView.setData(x, y, error);
            }
        }
    }

    /**
     * Extract the values of an {@link AbstractDataView}, returning it as a <code>double[][]</code>;
     * 
     * @param dataView The {@link AbstractDataView}.
     * @return A <code>double[][]</code>: <code>{ {x values}, {y values}, {errors} }</code>.
     */
    public static double[][] extractData(AbstractDataView dataView) {
        double[][] result;
        if (dataView == null) {
            result = null;
        } else {
            int length = dataView.getDataLength();
            DataList data = dataView.getData();
            result = new double[3][length];
            int index = 0;
            while (data != null) {
                result[IChartViewer.X_INDEX][index] = data.getX();
                result[IChartViewer.Y_INDEX][index] = data.getY();
                result[IChartViewer.ERROR_INDEX][index] = data.getError();
                data = data.next;
                index++;
            }
        }
        return result;
    }

    /**
     * Displays a popup containing the statistics of some {@link AbstractDataView}s
     * 
     * @param views The {@link AbstractDataView}s
     * @param name The name to display in statistics title
     * @param timeScale Whether to use time scale
     * @param labelFormat The used label format
     * @param dateFormat The used date format
     * @param component The {@link Component} relatively to which the popup should be displayed
     */
    public static void showStatAll(List<? extends AbstractDataView> views, String name, boolean timeScale,
            int labelFormat, String dateFormat, Component component) {
        int dataCount = 0;
        double[][] values;
        synchronized (views) {
            for (AbstractDataView view : views) {
                dataCount += view.getDataLength();
            }
            values = new double[dataCount][2];
            int index = 0;
            for (AbstractDataView view : views) {
                DataList data = view.getData();
                while (data != null) {
                    double y = data.getY();
                    double x = data.getX();
                    if (Double.isNaN(y)) {
                        long l = Double.doubleToRawLongBits(data.getY());
                        if (l == Double.doubleToRawLongBits(FormatNAN.NAN_FOR_NEGATIVE_INFINITY.getValue())) {
                            y = Double.NEGATIVE_INFINITY;
                        } else if (l == Double.doubleToRawLongBits(FormatNAN.NAN_FOR_POSITIVE_INFINITY.getValue())) {
                            y = Double.POSITIVE_INFINITY;
                        }
                    }
                    if (Double.isNaN(x)) {
                        long l = Double.doubleToRawLongBits(data.getX());
                        if (l == Double.doubleToRawLongBits(FormatNAN.NAN_FOR_NEGATIVE_INFINITY.getValue())) {
                            x = Double.NEGATIVE_INFINITY;
                        } else if (l == Double.doubleToRawLongBits(FormatNAN.NAN_FOR_POSITIVE_INFINITY.getValue())) {
                            x = Double.POSITIVE_INFINITY;
                        }
                    }
                    values[index][0] = y;
                    values[index++][1] = x;
                    data = data.next;
                }
            }
        }
        showStatistics(values, name, timeScale, labelFormat, dateFormat, component);
    }

    private static void showStatistics(double[][] values, String name, boolean timeScale, int labelFormat,
            String dateFormat, Component component) {
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        double minX = Double.NaN;
        double maxX = Double.NaN;
        double average = 0;
        double standardDeviation = 0;
        double sampleStandardDeviation = 0;
        double squareSum = 0;
        // Standard deviation = sqrt( (1/N)*sum(xi^2) - ( (1/N)*sum(xi) )^2 )
        // Sample standard deviation = sqrt( (N/(N-1)) * ( (1/N)*sum(xi^2) - ( (1/N)*sum(xi) )^2 ) )
        // (AbstractDataViews may be considered as samples of points)
        try {
            for (double[] value : values) {
                if (value[0] < min) {
                    min = value[0];
                    minX = value[1];
                }
                if (value[0] > max) {
                    max = value[0];
                    maxX = value[1];
                }
                average += value[0];
                double square;
                if (Double.isInfinite(value[0])) {
                    square = Double.POSITIVE_INFINITY;
                } else if (Double.isNaN(value[0])) {
                    square = Double.NaN;
                } else {
                    square = value[0] * value[0];
                }
                squareSum += square;
            }
            if (Double.isInfinite(min) && min > 0) {
                min = Double.NaN;
            }
            if (Double.isInfinite(max) && max < 0) {
                max = Double.NaN;
            }
            if (values.length > 0) {
                // -------------------calculate average---------------------------
                average = average / values.length;
                // ---------------calculate standard deviation--------------------
                squareSum = squareSum / values.length;
                standardDeviation = Math.sqrt(squareSum - (average * average));
                if (values.length > 1) {
                    sampleStandardDeviation = Math.sqrt((((double) values.length) / (double) (values.length - 1))
                            * (squareSum - (average * average)));
                } else {
                    sampleStandardDeviation = Double.NaN;
                }
            } else {
                average = Double.NaN;
                standardDeviation = Double.NaN;
                sampleStandardDeviation = Double.NaN;
                min = Double.NaN;
                max = Double.NaN;
            }
            StringBuilder titleBuffer = new StringBuilder(STATISTICS);
            titleBuffer.append(name);
            StringBuilder messageBuilder = new StringBuilder(HTML_STATISTICS_START);
            messageBuilder.append(min).append(SPAN_END);
            if (!Double.isNaN(minX)) {
                String xStringValue = ObjectUtils.EMPTY_STRING;
                if (timeScale) {
                    xStringValue = formatTimeValue(minX);
                } else {
                    xStringValue = formatValue(minX, 0, labelFormat, dateFormat);
                }
                messageBuilder.append(SPAN_OBTAINED_AT_X);
                messageBuilder.append(xStringValue).append(SPAN_LI_END);
            }
            messageBuilder.append(HTML_MAXIMUM).append(max).append(SPAN_END);
            if (!Double.isNaN(maxX)) {
                String xStringValue = ObjectUtils.EMPTY_STRING;
                if (timeScale) {
                    xStringValue = formatTimeValue(maxX);
                } else {
                    xStringValue = formatValue(maxX, 0, labelFormat, dateFormat);
                }
                messageBuilder.append(SPAN_OBTAINED_AT_X);
                messageBuilder.append(xStringValue).append(SPAN_LI_END);
            }
            messageBuilder.append(HTML_AVERAGE).append(average).append(SPAN_LI_END);
            messageBuilder.append(HTML_STANDARD_DEVIATION);
            messageBuilder.append(standardDeviation).append(SPAN_LI_END);
            messageBuilder.append(HTML_SAMPLE_STANDARD_DEVIATION);
            messageBuilder.append(sampleStandardDeviation).append(HTML_STATISTICS_END);
            JOptionPane.showMessageDialog(component, messageBuilder.toString(), titleBuffer.toString(),
                    JOptionPane.PLAIN_MESSAGE, null);
        } catch (Exception e) {
            String errorMessage = FAILED_TO_OBTAIN_STATISTICS_ON + name;
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(errorMessage, e);
            JOptionPane.showMessageDialog(component, errorMessage, ERROR, JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Generates a new {@link TabbedLine} based on a {@link List} of {@link AbstractDataView}s
     * 
     * @param views The {@link AbstractDataView} {@link List}
     * @param indexColumnName The name to display when horizontal axis is in value annotation
     * @param dateColumnName The name to display when horizontal axis is in time annotation
     * @param timePrecision The time precision to use
     * @param noValueString The string to display when there is no data
     * @return A {@link TabbedLine}. <code>null</code> if <code>views</code> is <code>null</code>
     */
    public static TabbedLine generateTabbedLine(String indexColumnName, String dateColumnName, int timePrecision,
            String noValueString, AbstractDataView... views) {
        TabbedLine tl;
        if (views == null) {
            tl = null;
        } else {
            tl = new TabbedLine(views.length);
            updateTabbedLine(tl, indexColumnName, dateColumnName, timePrecision, noValueString);
            for (int v = 0; v < views.length; v++) {
                tl.add(v, views[v]);
            }
        }
        return tl;
    }

    /**
     * Updates a {@link TabbedLine} to set its index column name, time column name, time precision and no value string
     * 
     * @param tl The {@link TabbedLine}
     * @param indexColumnName The name to display when horizontal axis is in value annotation
     * @param dateColumnName The name to display when horizontal axis is in time annotation
     * @param timePrecision The time precision to use
     * @param noValueString The string to display when there is no data
     */
    protected static void updateTabbedLine(TabbedLine tl, String indexColumnName, String dateColumnName,
            int timePrecision, String noValueString) {
        if (tl != null) {
            if (indexColumnName != null) {
                tl.setIndexColumnName(indexColumnName);
            }
            if (dateColumnName != null) {
                tl.setDateColumnName(dateColumnName);
            }
            tl.setPrecision(timePrecision);
            if (noValueString != null) {
                tl.setNoValueString(noValueString);
            }
        }
    }

    /**
     * Saves some {@link AbstractDataView}s data in a file
     * 
     * @param fileName The file name
     * @param indexColumnName The name to display when horizontal axis is in value annotation
     * @param dateColumnName The name to display when horizontal axis is in time annotation
     * @param timePrecision The time precision to use
     * @param noValueString The string to display when there is no data
     * @param horizontalTimeScale Whether horizontal axis uses time scale
     * @param useDisplayName Whether to use dataviews display name instead of id
     * @param views The {@link AbstractDataView}s
     * @throws IOException If a problem occurred during file writing
     */
    public static void saveDataFile(String fileName, String indexColumnName, String dateColumnName, int timePrecision,
            String noValueString, boolean horizontalTimeScale, boolean useDisplayName, AbstractDataView... views)
            throws IOException {
        saveDataFile(fileName, indexColumnName, dateColumnName, timePrecision, noValueString, horizontalTimeScale,
                useDisplayName, CsvLogFileWriter.CSV.equalsIgnoreCase(FileUtils.getExtension(fileName)), views);
    }

    /**
     * Saves some {@link AbstractDataView}s data in a file
     * 
     * @param fileName The file name
     * @param indexColumnName The name to display when horizontal axis is in value annotation
     * @param dateColumnName The name to display when horizontal axis is in time annotation
     * @param timePrecision The time precision to use
     * @param noValueString The string to display when there is no data
     * @param horizontalTimeScale Whether horizontal axis uses time scale
     * @param useDisplayName Whether to use dataviews display name instead of id
     * @param csv Whether to use CSV format
     * @param views The {@link AbstractDataView}s
     * @throws IOException If a problem occurred during file writing
     */
    // CSV compatibility (JAVAAPI-573)
    public static void saveDataFile(String fileName, String indexColumnName, String dateColumnName, int timePrecision,
            String noValueString, boolean horizontalTimeScale, boolean useDisplayName, boolean csv,
            AbstractDataView... views) throws IOException {
        if ((fileName != null) && (!fileName.trim().isEmpty())) {
            FileWriter fw = null;
            try {
                fw = new FileWriter(fileName);
                String dateColumnName2 = new StringBuilder(dateColumnName).append(' ').append(OPEN_PARENTHESIS)
                        .append(GENERAL_DATE_FORMAT.toPattern()).append(CLOSE_PARENTHESIS).toString();
                TabbedLine tl = generateTabbedLine(indexColumnName, dateColumnName2, timePrecision, noValueString,
                        views);
                if (tl != null) {
                    tl.setUseDisplayNameInFiles(useDisplayName);
                    String s;
                    for (int v = 0; v < views.length; v++) {
                        tl.add(v, views[v]);
                    }
                    s = tl.getFirstLine(horizontalTimeScale, csv);
                    while (s != null) {
                        fw.write(s);
                        s = tl.getNextLine(csv);
                    }
                }
            } finally {
                if (fw != null) {
                    fw.close();
                }
            }
        }
    }

    /**
     * Loads a data file and add the corresponding data to Y1 axis
     * 
     * @param dataViewType The type of {@link AbstractDataView} to use
     * @param xDataSorted Whether {@link AbstractDataView}s should be considered as sorted on x on creation
     * @param fileName the full path of the data file
     * @param indexColumnName The known index column name
     * @param dateColumnName The known date column name
     * @param parent The {@link Component} relatively to which error popups should be displayed
     * @return A {@link ChartData}. <code>null</code> if data file loading failed
     */
    public static <D extends AbstractDataView> ChartData<D> loadDataFile(Class<D> dataViewType, boolean xDataSorted,
            final String fileName, final String indexColumnName, final String dateColumnName, Component parent) {
        return loadDataFile(dataViewType, xDataSorted, fileName, indexColumnName, dateColumnName, false, parent);
    }

    /**
     * Loads a data file and add the corresponding data to Y1 axis
     * 
     * @param dataViewType The type of {@link AbstractDataView} to use
     * @param xDataSorted Whether {@link AbstractDataView}s should be considered as sorted on x on creation
     * @param fileName the full path of the data file
     * @param indexColumnName The known index column name
     * @param dateColumnName The known date column name
     * @param randomIDs Whether to generate random IDs for loading data views (instead of using their name as ID).
     * @param parent The {@link Component} relatively to which error popups should be displayed
     * @return A {@link ChartData}. <code>null</code> if data file loading failed
     */
    public static <D extends AbstractDataView> ChartData<D> loadDataFile(Class<D> dataViewType, boolean xDataSorted,
            final String fileName, final String indexColumnName, final String dateColumnName, boolean randomIDs,
            Component parent) {
        return loadDataFile(dataViewType, xDataSorted, fileName, indexColumnName, dateColumnName, randomIDs,
                CsvLogFileWriter.CSV.equalsIgnoreCase(FileUtils.getExtension(fileName)), parent);
    }

    /**
     * Reads a file and returns the {@link LineInfo} that contains the first readable line and whether the file follows
     * NxExtractor format.
     * 
     * @param reader The {@link BufferedReader} that reads the file.
     * @param csv Whether the file has a CSV extension.
     * @return A {@link LineInfo}.
     * @throws IOException If a problem occurred during file reading.
     */
    protected static LineInfo getFirstLine(BufferedReader reader, boolean csv) throws IOException {
        String line = ObjectUtils.EMPTY_STRING;
        boolean nxExtractorFormat = false;
        while ((line != null) && line.isEmpty()) {
            line = reader.readLine();
            if (line != null) {
                line = line.trim();
                // JAVAAPI-518: compatibility with NxExtractor format
                if ((!csv) && line.startsWith(NX_EXTRACTOR_COMMENT)) {
                    nxExtractorFormat = true;
                    if (line.startsWith(NX_EXTRACTOR_INDEX)) {
                        line = line.replaceFirst(NX_EXTRACTOR_INDEX, TabbedLine.DEFAULT_INDEX_COLUMN_NAME)
                                .replaceAll(SPACES, TAB).replaceAll(TABS, TAB);
                    } else {
                        line = ObjectUtils.EMPTY_STRING;
                    }
                }
            }
        }
        return new LineInfo(line, nxExtractorFormat);
    }

    /**
     * Loads a data file and add the corresponding data to Y1 axis
     * 
     * @param dataViewType The type of {@link AbstractDataView} to use
     * @param xDataSorted Whether {@link AbstractDataView}s should be considered as sorted on x on creation
     * @param fileName the full path of the data file
     * @param indexColumnName The known index column name
     * @param dateColumnName The known date column name
     * @param randomIDs Whether to generate random IDs for loading data views (instead of using their name as ID).
     * @param csv Whether the file follows the CSV format
     * @param parent The {@link Component} relatively to which error popups should be displayed
     * @return A {@link ChartData}. <code>null</code> if data file loading failed
     */
    public static <D extends AbstractDataView> ChartData<D> loadDataFile(Class<D> dataViewType, boolean xDataSorted,
            final String fileName, final String indexColumnName, final String dateColumnName, boolean randomIDs,
            boolean csv, Component parent) {
        ChartData<D> result = null;
        String xUserDateFormat = null;
        if ((fileName != null) && (!fileName.trim().isEmpty())) {
            BufferedReader reader = null;
            try {
                boolean canLoad = true;
                File dataFile = new File(fileName);
                reader = new BufferedReader(new FileReader(dataFile));
                LineInfo lineInfo = getFirstLine(reader, csv);
                String line = lineInfo.getLine();
                boolean nxExtractorFormat = lineInfo.isNxExtractorFormat();
                if (line == null) {
                    throw new IOException(FAILED_TO_READ_FILE + fileName);
                } else {
                    ParsedLineInfo parsedLineInfo = new ParsedLineInfo(line, csv, nxExtractorFormat);
                    String[] parsedLine = parsedLineInfo.getParsedLine();
                    csv = parsedLineInfo.isCSV();
                    String parsedLine0 = parsedLine[0].trim();
                    String lowerParsedLine0 = parsedLine0.toLowerCase();
                    boolean timeScale = (parsedLine0.equalsIgnoreCase(dateColumnName) || lowerParsedLine0.contains(TIME)
                            || lowerParsedLine0.contains(DATE));
                    if (timeScale) {
                        if (parsedLine0.contains(OPEN_PARENTHESIS)) {// There is a date format specified
                            int indexOf = parsedLine0.indexOf(OPEN_PARENTHESIS);
                            int lastIndexOf = parsedLine0.lastIndexOf(CLOSE_PARENTHESIS);
                            xUserDateFormat = parsedLine0.substring(indexOf + 1, lastIndexOf);
                        }
                    }
                    if (canLoad) {
                        String lastDataFileLocation = dataFile.getParentFile().getAbsolutePath();
                        int viewCount = parsedLine.length - 1;
                        if (viewCount < 0) {
                            throw new ArrayIndexOutOfBoundsException();
                        }
                        @SuppressWarnings("unchecked")
                        D[] views = (D[]) Array.newInstance(dataViewType, viewCount);
                        D dataView = null;
                        String dvName = null;
                        String dvID = null;
                        Constructor<D> dataViewConstructor = dataViewType.getConstructor(String.class);
                        for (int i = 0; i < views.length; i++) {
                            dvName = parsedLine[i + 1].trim();
                            if (randomIDs) {
                                // JAVAAPI-536: generate random ID
                                dvID = CometeDefinitionUtils.generateRandomID(dvName);
                            } else {
                                dvID = dvName;
                            }
                            dataView = dataViewConstructor.newInstance(dvID);
                            dataView.setDisplayName(dvName);
                            dataView.setFixed(true);
                            dataView.setXDataSorted(xDataSorted);
                            views[i] = dataView;
                            dataView.setViewType(IChartViewer.TYPE_LINE);
                        }
                        double time = 0;
                        // double minTime = Double.MAX_VALUE, maxTime = -Double.MAX_VALUE;
                        while ((line = reader.readLine()) != null) {
                            if (nxExtractorFormat) {
                                // JAVAAPI-518: compatibility with NxExtractor format
                                parsedLine = line.trim().replaceAll(SPACES, TAB).replaceAll(TABS, TAB).split(TAB);
                            } else if (csv) {
                                // JAVAAPI-573: compatibility with csv format
                                // Here, we don't use String.split as a csv line may contain ";;" and may end with ";"
                                Collection<String> stringList = new ArrayList<>();
                                StringBuilder builder = new StringBuilder(line.length() + 1);
                                for (int i = 0; i < line.length(); i++) {
                                    char c = line.charAt(i);
                                    if (c == ';') {
                                        stringList.add(builder.toString());
                                        builder.delete(0, builder.length());
                                    } else {
                                        builder.append(c);
                                    }
                                }
                                stringList.add(builder.toString());
                                parsedLine = stringList.toArray(new String[stringList.size()]);
                                stringList.clear();
                                builder.delete(0, builder.length());
                            } else {
                                parsedLine = line.split(TAB);
                            }
                            if (parsedLine.length - 1 != viewCount) {
                                throw new IOException(ERROR_IN_DATA_COUNT);
                            }
                            if (timeScale) {
                                Date date = ChartUtils.parseTimeString(parsedLine[0], xUserDateFormat);
                                if (date == null) {
                                    // error on this line, try to read the other ones
                                    continue;
                                } else {
                                    time = date.getTime();
                                }
                            } else {
                                try {
                                    time = Double.parseDouble(parsedLine[0]);
                                } catch (NumberFormatException nfe) {
                                    if (NAN.equalsIgnoreCase(parsedLine[0])) {
                                        time = Double.NaN;
                                    } else {
                                        // error on this line, try to read the other ones
                                        continue;
                                    }
                                }
                            }
                            // if (time > maxTime) maxTime = time;
                            // if (time < minTime) minTime = time;
                            for (int i = 0; i < views.length; i++) {
                                try {
                                    views[i].add(time, Double.parseDouble(parsedLine[i + 1]));
                                } catch (NumberFormatException nfe) {
                                    if (NULL.equalsIgnoreCase(parsedLine[i + 1].trim())) {
                                        // case null
                                        views[i].add(time, MathConst.NAN_FOR_NULL);
                                    } else {
                                        // no data at this time
                                        continue;
                                    }
                                }
                            } // end for (int i = 0; i < views.length; i++)
                        } // end while ((line = reader.readLine()) != null)
                        result = new ChartData<D>(views, lastDataFileLocation);
                    } // end if (canLoad)
                } // end if (line == null) ... else
            } catch (Exception e) {
                StringBuilder messageBuilder = new StringBuilder(FAILED_TO_LOAD_FILE);
                messageBuilder.append(fileName);
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(messageBuilder.toString(), e);
                messageBuilder.append(NEW_LINE).append(e.getMessage());
                JOptionPane.showMessageDialog(parent, messageBuilder.toString(), ERROR, JOptionPane.ERROR_MESSAGE);
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(FAILED_TO_CLOSE_FILE + fileName, e);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns whether a file is potential a chart data file.
     * 
     * @param reader The {@link BufferedReader} that reads the file.
     * @param csv Whether the file has a csv extension.
     * @return A <code>boolean</code>.
     * @throws IOException If a problem occurred during file reading.
     */
    protected static boolean isPotentialDataFile(BufferedReader reader, boolean csv) throws IOException {
        LineInfo info = getFirstLine(reader, csv);
        ParsedLineInfo parsedLineInfo = new ParsedLineInfo(info.getLine(), csv, info.isNxExtractorFormat());
        return parsedLineInfo.getParsedLine().length > 1;
    }

    /**
     * Returns whether a file is potential a chart data file.
     * 
     * @param fileName The file path.
     * @return A <code>boolean</code>.
     */
    public static boolean isPotentialDataFile(String fileName) {
        boolean dataFile;
        if ((fileName == null) || fileName.trim().isEmpty()) {
            dataFile = false;
        } else {
            try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
                dataFile = isPotentialDataFile(reader,
                        CsvLogFileWriter.CSV.equalsIgnoreCase(FileUtils.getExtension(fileName)));
            } catch (IOException e) {
                dataFile = false;
            }
        }
        return dataFile;
    }

    /**
     * Returns whether a file is potential a chart data file.
     * 
     * @param file The {@link File}.
     * @return A <code>boolean</code>.
     */
    public static boolean isPotentialDataFile(File file) {
        boolean dataFile;
        try {
            if ((file == null) || file.isDirectory()) {
                dataFile = false;
            } else {
                try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                    dataFile = isPotentialDataFile(reader,
                            CsvLogFileWriter.CSV.equalsIgnoreCase(FileUtils.getExtension(file)));
                } catch (IOException e) {
                    dataFile = false;
                }
            }
        } catch (SecurityException e) {
            // potentially thrown by file.isDirectory(): read access denied
            dataFile = false;
        }
        return dataFile;
    }

    /**
     * Updates a {@link JTableRow}'s content with some {@link AbstractDataView}s
     * 
     * @param table The {@link JTableRow}
     * @param timePrecision The time precision to use
     * @param horizontalTimeScale Whether horizontal axis uses time scale
     * @param xy Whether horizontal axis is in xy mode
     * @param indexColumnName The "index" column name
     * @param dateColumnName The "date" column name
     * @param views The {@link AbstractDataView}s
     */
    public static void updateTableContent(JTableRow table, int timePrecision, boolean horizontalTimeScale, boolean xy,
            String indexColumnName, String dateColumnName, AbstractDataView... views) {
        updateTableContent(table, horizontalTimeScale, xy,
                generateTabbedLine(indexColumnName, dateColumnName, timePrecision, null, views));
    }

    /**
     * Updates a {@link JTableRow}'s content with a {@link TabbedLine}
     * 
     * @param table The {@link JTableRow}
     * @param horizontalTimeScale Whether horizontal axis is in time scale
     * @param xy Whether horizontal axis is in xy mode
     * @param tl The {@link TabbedLine}
     */
    public static void updateTableContent(JTableRow table, boolean horizontalTimeScale, boolean xy, TabbedLine tl) {
        if ((table != null) && (tl != null)) {
            List<Object[]> data = new ArrayList<Object[]>();
            String[] cols = tl.getFirstFields(horizontalTimeScale, !xy);
            String[] s = tl.getNextFields(null);
            while (s != null) {
                data.add(s);
                s = tl.getNextFields(null);
            }
            int y = data.size();
            int x = cols.length;
            Object[][] dv = new Object[y][x];
            for (int j = 0; j < y; j++) {
                Object[] ln = data.get(j);
                for (int i = 0; i < x; i++) {
                    dv[j][i] = ln[i];
                }
            }

            if (x == 1 && y == 0) {
                // no data
                dv = null;
                cols = null;
            }
            table.setData(dv, cols);
        }
    }

    /**
     * Creates, if necessary, a new {@link Window} for a {@link JTableRow}, depending on user preferences
     * 
     * @param tableWindow The previously created {@link Window}, if any
     * @param table The {@link JTableRow}
     * @param preferDialog Whether user prefers having a {@link JDialog}. <code>true</code> to use a {@link JDialog},
     *            <code>false</code> to use a {@link JFrame}
     * @param modalDialog Whether the {@link JDialog} should be modal, if concerned
     * @param referent The {@link Component} that asks for initialization (in case of {@link JDialog}, to recover owner)
     * @return A {@link Window}
     */
    public static Window getInitializedTableWindow(Window tableWindow, JTableRow table, boolean preferDialog,
            boolean modalDialog, Component referent) {
        String title = GRAPH_DATA;
        boolean shouldClose = false;
        boolean shouldInit = false;
        Window initializedWindow = tableWindow;
        if (initializedWindow == null) {
            shouldInit = true;
        } else {
            if (preferDialog) {
                shouldClose = !(initializedWindow instanceof JDialog);
            } else {
                shouldClose = !(initializedWindow instanceof JFrame);
            }
            shouldInit = shouldClose;
        }
        if (shouldClose) {
            initializedWindow.setVisible(false);
            initializedWindow = null;
        }
        if (shouldInit) {
            if (preferDialog) {
                JDialog dialog = new JDialog(WindowSwingUtils.getWindowForComponent(referent), title);
                dialog.setContentPane(table);
                dialog.setModal(modalDialog);
                initializedWindow = dialog;
            } else {
                JFrame frame = new JFrame(title);
                frame.setContentPane(table);
                initializedWindow = frame;
            }
        }
        return initializedWindow;
    }

    /**
     * Centers and limits size of a {@link Window} containing a {@link JTableRow}
     * 
     * @param tableWindow The {@link Window}
     * @param table The {@link JTableRow}
     */
    public static void centerTableWindow(Window tableWindow, JTableRow table) {
        if ((tableWindow != null) && (table != null)) {
            table.adjustColumnSize();
            table.adjustSize();

            // Center the frame and saturate to 800*600
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension scrsize = toolkit.getScreenSize();
            tableWindow.pack();
            Dimension appsize = tableWindow.getPreferredSize();
            if (appsize.height > 600) {
                appsize.height = 600;
                if (appsize.width < 800) {
                    // When we saturate the height
                    // it is better to reserver space for
                    // the vertical scrollbar
                    appsize.width += 16;
                }
            }
            if (appsize.width > 800) {
                appsize.width = 800;
            }

            int x = (scrsize.width - appsize.width) / 2;
            int y = (scrsize.height - appsize.height) / 2;
            tableWindow.setBounds(x, y, appsize.width, appsize.height);
        }
    }

    /**
     * Calculates a <code>NaN</code> string representation, depending of <code>NaN</code> encoding method
     * 
     * @param v The <code>NaN</code> value
     * @param nullString The string to display when the encoded <code>NaN</code> represents a <code>null</code> value
     * @return A {@link String}
     */
    public static String formatNaNValue(double v, String nullString) {
        if ((nullString == null) || (nullString.isEmpty())) {
            nullString = NULL;
        }
        String result;
        if (Double.isNaN(v)) {
            if (MathConst.isNaNForNull(v)) {
                result = nullString;
            } else {
                long l = Double.doubleToRawLongBits(v);
                if (l == Double.doubleToRawLongBits(FormatNAN.NAN_FOR_NEGATIVE_INFINITY.getValue())) {
                    result = NEGATIVE_INFINITY;
                } else if (l == Double.doubleToRawLongBits(FormatNAN.NAN_FOR_POSITIVE_INFINITY.getValue())) {
                    result = POSITIVE_INFINITY;
                } else {
                    result = NAN;
                }
            }
        } else {
            result = Double.toString(v);
        }
        return result;
    }

    /**
     * Calculates the {@link String} re^resentation of a value, depending on a format and a <code>NaN</code> encoding
     * method
     * 
     * @param v The value
     * @param nullString The string to display when the encoded <code>NaN</code> represents a <code>null</code> value
     * @param format The format
     * @return A {@link String}
     */
    public static String formatValue(double v, String nullString, String format) {
        String result;
        if (Double.isNaN(v)) {
            result = formatNaNValue(v, nullString);
        } else if ((format == null) || format.isEmpty()) {
            result = Double.toString(v);
        } else {
            result = SwingFormat.formatValue(v, format);
        }
        return result;
    }

    /**
     * Appends the chart configuration file help string to a {@link StringBuilder}
     * 
     * @param builder The {@link StringBuilder}
     * @return The given {@link StringBuilder}, or a new one if <code>null</code>, with the help string appended to it
     */
    public static StringBuilder appendChartConfigurationHelpString(StringBuilder builder) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append("-- Global chart settings --\n\n");
        builder.append("graph_title:'title'   Chart title ('null' to disable)\n");
        builder.append("label_visible:true or false  Show legend\n");
        builder.append("label_placement:value   (0 Down,1 Up,2 Right, 3 Left)\n");
        builder.append("label_font:name,style(0 Plain,1 Bold,2 italic),size \n");
        builder.append("graph_background:r,g,b   Component background \n");
        builder.append("chart_background:r,g,b   Graph area background \n");
        builder.append("title_font:name,style(0 Plain,1 Bold,2 italic),size\n");
        builder.append("display_duration:milliSec   X axis duration (time monitoring)\n\n");
        return appendDataViewConfigurationHelpString(appendAxisConfigurationHelpString(builder).append(NEW_LINE));
    }

    /**
     * Appends the axis configuration file help string to a {@link StringBuilder}
     * 
     * @param builder The {@link StringBuilder}
     * @return The given {@link StringBuilder}, or a new one if <code>null</code>, with the help string appended to it
     */
    public static StringBuilder appendAxisConfigurationHelpString(StringBuilder builder) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append("-- Axis settings --\n");
        builder.append("Parameter name is preceded by the axis name.\n\n");
        builder.append("visible:true or false   Displays the axis\n");
        builder.append("grid:true or false   Displays the grid\n");
        builder.append("subgrid:true or false   Displays the sub grid\n");
        builder.append("grid_style:style   (0 Solid,1 Dot, 2 Dash, 3 Long Dash)\n");
        builder.append("min:value Axis minimum\n" + "max:value Axis maximum\n");
        builder.append("autoscale:true or false Axis autoscale\n");
        builder.append("scale:s   Axis scale (0 Linear ,1 Log)\n");
        builder.append("format:format   Axis format (0 Auto,1 Sci,2 Time,3 Dec,4 Hex,5 Bin))\n");
        builder.append("title:'title'   Axis title ('null' to disable)\n" + "color:r,g,b   Axis color\n");
        builder.append("label_font:name,style(0 Plain,1 Bold,2 italic),size\n");
        return builder;
    }

    /**
     * Appends the dataview configuration file help string to a {@link StringBuilder}
     * 
     * @param builder The {@link StringBuilder}
     * @return The given {@link StringBuilder}, or a new one if <code>null</code>, with the help string appended to it
     */
    public static StringBuilder appendDataViewConfigurationHelpString(StringBuilder builder) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append("-- DataView settings --\n");
        builder.append("Parameter name is preceded by the dataview id.\n\n");
        builder.append("linecolor:r,g,b  Curve color\n");
        builder.append("linewidth:width  Curve width\n");
        builder.append("linestyle:style  Line style (0 Solid,1 Dot, 2 Dash, 3 Long Dash,...)\n");
        builder.append("fillcolor:r,g,b  Curve fill color\n");
        builder.append("fillmethod:m   Bar filling method (0 Top,1 Zero,2 Bottom)\n");
        builder.append("fillstyle:style  Curve filling style (0 No fill,1 Solid,...)\n");
        builder.append("viewtype:type   Type of plot (0 Line, 1 Bar)\n");
        builder.append("barwidth:width   Bar width in pixel (0 autoscale)\n");
        builder.append("markercolor:r,g,b   Marker color\n");
        builder.append("markersize:size   Marker size\n");
        builder.append("markerstyle:style  Marker style (0 No marker,1 Dot,2 Box,...)\n");
        builder.append("A0,A1,A2:value   Vertical transfrom Y = A0 + A1*y + A2*y*y\n");
        builder.append("labelvisible:true or false   Displays legend of this view\n");
        builder.append("clickable:true or false  Shows tooltip on mouse click\n");
        return builder;
    }

    public static double getRatio(double value, double org, int axisSize, int margin, AbstractAxisView axis,
            AbstractAxisView orthogonalAxis) {
        double v = axis.getScale().getScaledValue(value);
        double ratio;
        if (Double.isNaN(v)) {
            ratio = Double.NaN;
        } else {
            if (axis.getAttributes().isInverted()) {
                // TODO check if algorithm is ok for inverted x axis
                if (axis.getAttributes().getOrientation() == Orientation.HORIZONTAL) {
                    org = org + axisSize;
                }
            } else if (axis.getAttributes().getOrientation() == Orientation.VERTICAL) {
                // GIRARDOT: don't ask me why, but this this the right transformation
                org = axisSize + (margin * 2) - org;
                if (orthogonalAxis.getAttributes().getOrigin() == IChartViewer.HORIZONTAL_UP) {
                    org += orthogonalAxis.getThickness() + 1;
                }
            }

            // ask position to axis scale
            ratio = axis.getScale().getPosition(v, axis.getLength(), true);

            // saturate position
            ratio = getSaturation(ratio);

            // make position start from axis effective origin
            ratio += org;
        }
        return ratio;
    }

    /**
     * Calculates some value saturation. Mainly used in axis sales
     * 
     * @param toSaturate The value to saturate
     * @return A <code>double</code>
     */
    public static double getSaturation(double toSaturate) {
        if (toSaturate < -32000) {
            toSaturate = -32000;
        }
        if (toSaturate > 32000) {
            toSaturate = 32000;
        }
        return toSaturate;
    }

    /**
     * Generates a {@link BasicStroke} based on a chart line style
     * 
     * @param lineStyle The line style
     * @return A {@link BasicStroke}
     */
    public static BasicStroke generateStroke(int lineStyle) {
        BasicStroke bs;
        switch (lineStyle) {
            case IChartViewer.STYLE_SOLID:
                bs = new BasicStroke();
                break;
            case IChartViewer.STYLE_DOT:
                bs = new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0f,
                        new float[] { 0.0f, 3.0f }, 0.0f);
                break;
            case IChartViewer.STYLE_DASH:
                bs = new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0f, new float[] { 3.0f },
                        0.0f);
                break;
            case IChartViewer.STYLE_LONG_DASH:
                bs = new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0f, new float[] { 6.0f },
                        0.0f);
                break;
            case IChartViewer.STYLE_DASH_DOT:
                bs = new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0f,
                        new float[] { 3.0f, 3.0f, 0.0f, 3.0f }, 0.0f);
                break;
            default:
                bs = null;
                break;
        }
        return bs;
    }

    /**
     * Displays a dialog in which user can set precision (for table construction)
     * 
     * @param chart The {@link IChartViewer} for which to change the precision
     */
    public static void displayPrecisionDialog(final IChartViewer chart) {
        if (chart != null) {
            Component comp = (chart instanceof Component ? (Component) chart : null);
            final PrecisionPanel precisionPanel = new PrecisionPanel();
            precisionPanel.setTimeScale(chart.isTimeScale(IChartViewer.X));
            precisionPanel.setPrecision(chart.getTimePrecision());
            final JDialog precisionDialog = new JDialog(WindowSwingUtils.getWindowForComponent(comp),
                    String.format(PRECISION_FORMAT,
                            precisionPanel.isTimeScale() ? precisionPanel.getTimePrecisionTitle().toLowerCase()
                                    : precisionPanel.getValuePrecisionTitle().toLowerCase()));
            precisionDialog.setModal(true);
            precisionDialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            precisionDialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    precisionPanel.setPrecision(chart.getTimePrecision());
                }
            });
            JButton ok = new JButton(OK);
            ok.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    precisionDialog.setVisible(false);
                }
            });
            JPanel mainPanel = new JPanel(new BorderLayout(5, 5));
            mainPanel.add(precisionPanel, BorderLayout.CENTER);
            mainPanel.add(ok, BorderLayout.EAST);
            precisionDialog.setContentPane(mainPanel);
            precisionDialog.pack();
            precisionDialog.setResizable(false);
            precisionDialog.setLocationRelativeTo(comp);
            precisionDialog.setVisible(true);
            chart.setTimePrecision(precisionPanel.getPrecision());
        }
    }

    /**
     * Creates, if possible, a copy of an {@link AbstractDataView}
     * 
     * @param original The {@link AbstractDataView} to copy
     * @param viewClass The {@link AbstractDataView} {@link Class}, used to create a copy
     * @param id The copy id to use
     * @return An {@link AbstractDataView}. <code>null</code> if a problem occurred
     */
    public static <DV extends AbstractDataView> DV duplicateDataView(DV original, Class<DV> viewClass, String id) {
        DV copy = null;
        if ((original != null) && (viewClass != null)) {
            try {
                Constructor<DV> constructor = viewClass.getConstructor(String.class);
                copy = constructor.newInstance(id);
            } catch (Exception e) {
                copy = null;
            }
            if (copy != null) {
                copy.setMarkerSize(original.getMarkerSize());
                copy.setMarker(original.getMarker());
                copy.setLineWidth(original.getLineWidth());
                copy.setViewType(original.getViewType());
                copy.setStyle(original.getStyle());
                copy.setFillStyle(original.getFillStyle());
                copy.setFillMethod(original.getFillMethod());
                copy.setUnit(original.getUnit());
                if (original.getFormat() != null) {
                    copy.setFormat(original.getFormat());
                }
                copy.setBarWidth(original.getBarWidth());
                copy.setA0(original.getA0());
                copy.setA1(original.getA1());
                copy.setA2(original.getA2());
                copy.setA0X(original.getA0X());
                copy.setXDataSorted(original.isXDataSorted());

                DataList data = original.getData();
                while (data != null) {
                    copy.add(data.getX(), data.getY());
                    data = data.next;
                }
            }
        }
        return copy;
    }

    /**
     * Calculates best 10 exponent for a given value
     * 
     * @param d The value
     * @return A <code>double</code>
     */
    public static double computeLowTen(double d) {
        int p;
        double dAbs = Math.abs(d);
        if (dAbs < 1) {
            p = (int) Math.floor(Math.log10(dAbs));
        } else {
            p = (int) Math.log10(dAbs);
        }
        return Math.pow(10.0, p);
    }

}
