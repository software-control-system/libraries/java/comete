/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

import fr.soleil.comete.swing.Chart;

public class ShowInFrameMenu extends ShowInMenuItem implements WindowListener {

    private static final long serialVersionUID = -360039644183540876L;

    private final Map<String, JFrame> chartViewFrameMap;
    private final Map<String, SingleChartView> singleChartViewMap;

    public ShowInFrameMenu() {
        super();
        chartViewFrameMap = new HashMap<String, JFrame>();
        singleChartViewMap = new HashMap<String, SingleChartView>();
    }

    @Override
    public void showSingleChartView(SingleChartView selectedView) {
        if (selectedView != null) {
            String title = selectedView.getTitle();
            JFrame frame = chartViewFrameMap.get(title);
            if (frame == null) {
                frame = new JFrame();
                frame.setSize(400, 400);
                frame.setLocationRelativeTo(this);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                frame.setVisible(true);
                frame.setTitle(title);
                frame.setContentPane((Chart) selectedView.getChart());

                frame.addWindowListener(this);
                chartViewFrameMap.put(title, frame);
                singleChartViewMap.put(title, selectedView);
            }
            frame.setIconImage(selectedView.getImage().getImage());
            frame.toFront();
            frame.setVisible(true);
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
        // System.out.println("windowClosed");
        Object source = e.getSource();
        if (source instanceof JFrame) {
            String title = ((JFrame) source).getTitle();
            chartViewOption.removeSingleChartView(title);
            chartViewFrameMap.remove(title);
            SingleChartView singleChartView = singleChartViewMap.get(title);
            if (singleChartView != null) {
                singleChartView.cleanView();
                singleChartViewMap.remove(title);
            }
        }
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

}
