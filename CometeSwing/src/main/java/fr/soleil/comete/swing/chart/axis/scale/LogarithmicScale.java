/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.axis.scale;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.swing.chart.axis.model.AxisAttributes;
import fr.soleil.comete.swing.chart.axis.model.AxisLabel;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.data.mediator.Mediator;

public class LogarithmicScale extends AbstractAxisScale {

    /**
     * Constant used to calculate sub-tick positions
     */
    private static final double[] LOG_STEP = { 0.301, 0.477, 0.602, 0.699, 0.778, 0.845, 0.903, 0.954 };
    /**
     * Constant used to avoid some application freezes
     */
    private static final double MINIMUM_COMPATIBLE_VALUE = 1e-100;

    public LogarithmicScale(AxisAttributes attributes) {
        super(attributes);
    }

    @Override
    public double getMinimumCompatibleValue() {
        return MINIMUM_COMPATIBLE_VALUE;
    }

    @Override
    public void updateScaleBoundsFromAttributes() {
        autoScale = attributes.isAutoScale();
        if (!autoScale) {
            double min = attributes.getMinimum();
            if (min <= 0) {
                min = 1;
            }
            scaleMinimum = Math.log10(min);
            double max = attributes.getMaximum();
            if (max <= 0) {
                max = min * 10.0;
            }
            scaleMaximum = Math.log10(max);
            setLabelsFromUserInterval();
        }
    }

    @Override
    protected boolean shouldUseStricltyPositiveValues() {
        return true;
    }

    @Override
    protected double[] getMinMaxX(AbstractDataView v) {
        return updateMinMax(super.getMinMaxX(v));
    }

    @Override
    protected double[] getMinMaxY(AbstractDataView v) {
        return updateMinMax(super.getMinMaxY(v));
    }

    protected double[] updateMinMax(double[] mm) {
        if (mm[0] != Double.MAX_VALUE) {
            mm[0] = Math.log10(mm[0]);
        }
        if (mm[1] <= 0) {
            mm[1] = -Double.MAX_VALUE;
        } else {
            mm[1] = Math.log10(mm[1]);
        }
        return mm;
    }

    @Override
    public Map<TickType, int[]> computeTicksPositions(int axisOrigin, int axisLength) {
        Map<TickType, int[]> tickMap = new HashMap<AbstractAxisScale.TickType, int[]>();
        AxisLabel[] temp = labels;
        int size = temp.length;
        if (size > 0) {
            AxisLabel firstLabel = temp[0];
            AxisLabel lastLabel = temp[size - 1];
            int[] firstSubTicks = new int[0];
            int[] lastSubTicks = new int[0];
            int firstLength = LOG_STEP.length;
            int lastLength = LOG_STEP.length;
            int max = axisOrigin + axisLength;
            double firstOrigin = axisOrigin + firstLabel.getPos() - tickStep;
            double lastOrigin = axisOrigin + lastLabel.getPos();
            for (double step : LOG_STEP) {
                int firstPos = (int) Math.rint(firstOrigin + tickStep * step);
                int lastPos = (int) Math.rint(lastOrigin + tickStep * step);
                if ((firstPos <= axisOrigin) || (firstPos >= max)) {
                    firstLength--;
                } else {
                    firstSubTicks = Arrays.copyOf(firstSubTicks, firstSubTicks.length + 1);
                    firstSubTicks[firstSubTicks.length - 1] = firstPos;
                }
                if ((lastPos <= axisOrigin) || (lastPos >= max)) {
                    lastLength--;
                } else {
                    lastSubTicks = Arrays.copyOf(lastSubTicks, lastSubTicks.length + 1);
                    lastSubTicks[lastSubTicks.length - 1] = lastPos;
                }
            }
            int[] tickPositions = new int[size];
            int[] subTickPositions = new int[firstLength + lastLength + (size - 1) * LOG_STEP.length];
            int tickIndex = 0;
            int subTickIndex = 0;
            // First subTicks
            for (int subtick : firstSubTicks) {
                subTickPositions[subTickIndex++] = subtick;
            }
            // Other subTicks
            for (AxisLabel label : labels) {
                tickPositions[tickIndex++] = (int) Math.rint(label.getPos()) + axisOrigin;
                if (label != lastLabel) {
                    for (double step : LOG_STEP) {
                        if (subTickIndex < subTickPositions.length) {
                            subTickPositions[subTickIndex++] = (int) Math
                                    .rint(label.getPos() + axisOrigin + tickStep * step);
                        }
                    }
                }
            }
            // Last subTicks
            for (int subtick : lastSubTicks) {
                subTickPositions[subTickIndex++] = subtick;
            }
            tickMap.put(TickType.TICK, tickPositions);
            tickMap.put(TickType.SUB_TICK, subTickPositions);
        } // end if (size > 0)
        return tickMap;
    }

    @Override
    protected void computeDefaultXScaleBounds() {
        scaleMinimum = 0;
        scaleMaximum = 1;
    }

    @Override
    protected void computeDefaultAutoScaleBounds() {
        scaleMinimum = 0;
        scaleMaximum = 1;
    }

    @Override
    protected double computeAutoScalePrecision() {
        double precision = ChartUtils.computeLowTen(scaleMaximum - scaleMinimum);
        // Avoid unlabeled axis when log scale
        if (precision < 1.0) {
            precision = 1.0;
        }
        return precision;
    }

    @Override
    protected void ensureMinimumDistanceBetweenMinAndMax() {
        if (Math.abs(scaleMaximum - scaleMinimum) < MINIMUM_COMPATIBLE_VALUE) {
            scaleMaximum = scaleMinimum * 10;
            if (scaleMaximum < scaleMinimum) {
                scaleMaximum *= -1;
            }
        }
    }

    @Override
    protected double getUserLabelPosition(int index) {
        return Math.log10(attributes.getUserLabelPos()[index]);
    }

    // DATAREDUC-468: Do not change first label in log scale
    @Override
    protected boolean canAdaptFirstValueLabelToScaleMinimum() {
        return false;
    }

    @Override
    protected double[] computeLabelDataForValueAnnotation(int nbMaxLab, double scaleDelta, double precDelta,
            double length) {
        int step = 0;
        int subStep = 0;
        double prec = 1; // Decade
        step = -1; // Logarithm sub-grid
        double startx = Math.rint(scaleMinimum);
        int n = (int) Math.rint(scaleDelta);
        int old;
        boolean infiniteLoop = false;
        // Adjust step
        while (n > nbMaxLab && !infiniteLoop) {
            old = n;
            prec *= 2;
            step = 2;
            n = (int) Math.rint(scaleDelta / prec);
            if (n > nbMaxLab) {
                prec *= 5;
                step = 10;
                n = (int) Math.rint(scaleDelta / prec);
            }
            // Infinite loop detection (JAVAAPI-614)
            if (n >= old) {
                infiniteLoop = true;
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .trace("LogarithmicScale.computeLabelDataForValueAnnotation(" + nbMaxLab + ", " + scaleDelta
                                + ", " + precDelta + "): infinite loop detected");
            }
        }
        subStep = step;
        return new double[] { subStep, prec, startx, 0 };
    }

    @Override
    protected double getFormatPrecistion(double value, double currentPrecision) {
        double precision = currentPrecision;
        // XXX RG: While studying DATAREDUC-745, I discovered the need to use "value < 10" instead of "value < 1",
        // to avoid a Logarithmic scale starting at 2 instead of 1
        if ((currentPrecision > 0) && (value < 10)) {
            double log = Math.floor(Math.log10(value));
            precision = Math.pow(10.0, log);
        }
        return precision;
    }

    @Override
    public double getUnScaledValue(double value) {
        return Double.isNaN(value) ? Double.NaN : Math.pow(10.0, value);
    }

    @Override
    public double getScaledValue(double value) {
        double result;
        if (Double.isNaN(value)) {
            result = Double.NaN;
        } else if (value <= 0) {
            // Special case for negative values to avoid data tracing bug
            result = Double.NEGATIVE_INFINITY;
        } else {
            result = Math.log10(value);
        }
        return result;
    }

}
