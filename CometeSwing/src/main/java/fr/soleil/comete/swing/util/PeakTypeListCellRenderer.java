/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import javax.swing.ListCellRenderer;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.chart.util.ChartUtils;

/**
 * A {@link ListCellRenderer} for sampling peak type selection.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class PeakTypeListCellRenderer extends TypeListCellRenderer {

    private static final long serialVersionUID = -6712039808507213180L;

    private static final String PEAK_TYPE_GREATEST_TEXT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.peak.greatest.text");
    private static final String PEAK_TYPE_GREATEST_TOOLTIP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.peak.greatest.tooltip");
    private static final String PEAK_TYPE_MIN_TEXT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.peak.minimum.text");
    private static final String PEAK_TYPE_MIN_TOOLTIP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.peak.minimum.tooltip");
    private static final String PEAK_TYPE_MAX_TEXT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.peak.maximum.text");
    private static final String PEAK_TYPE_MAX_TOOLTIP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.peak.maximum.tooltip");
    private static final String PEAK_TYPE_LOWEST_TEXT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.peak.lowest.text");
    private static final String PEAK_TYPE_LOWEST_TOOLTIP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.peak.lowest.tooltip");
    private static final String PEAK_TYPE_CLOSEST_TEXT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.peak.closest.text");
    private static final String PEAK_TYPE_CLOSEST_TOOLTIP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.peak.closest.tooltip");

    public PeakTypeListCellRenderer() {
        super();
    }

    @Override
    protected String getText(int id) {
        String text;
        switch (id) {
            case IChartViewer.PEAK_TYPE_GREATEST:
                text = PEAK_TYPE_GREATEST_TEXT;
                break;
            case IChartViewer.PEAK_TYPE_MIN:
                text = PEAK_TYPE_MIN_TEXT;
                break;
            case IChartViewer.PEAK_TYPE_MAX:
                text = PEAK_TYPE_MAX_TEXT;
                break;
            case IChartViewer.PEAK_TYPE_LOWEST:
                text = PEAK_TYPE_LOWEST_TEXT;
                break;
            case IChartViewer.PEAK_TYPE_CLOSEST:
                text = PEAK_TYPE_CLOSEST_TEXT;
                break;
            default:
                text = Integer.toString(id);
                break;
        }
        return text;
    }

    @Override
    protected String getToolTipText(int id, String defaultTooltip) {
        String text;
        switch (id) {
            case IChartViewer.PEAK_TYPE_GREATEST:
                text = PEAK_TYPE_GREATEST_TOOLTIP;
                break;
            case IChartViewer.PEAK_TYPE_MIN:
                text = PEAK_TYPE_MIN_TOOLTIP;
                break;
            case IChartViewer.PEAK_TYPE_MAX:
                text = PEAK_TYPE_MAX_TOOLTIP;
                break;
            case IChartViewer.PEAK_TYPE_LOWEST:
                text = PEAK_TYPE_LOWEST_TOOLTIP;
                break;
            case IChartViewer.PEAK_TYPE_CLOSEST:
                text = PEAK_TYPE_CLOSEST_TOOLTIP;
                break;
            default:
                text = Integer.toString(id);
                break;
        }
        return text;
    }

}
