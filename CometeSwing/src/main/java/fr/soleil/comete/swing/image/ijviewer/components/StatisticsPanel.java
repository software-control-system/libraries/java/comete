/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.WeakReference;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.events.RoiStatisticsListener;
import fr.soleil.comete.swing.image.util.ij.RoiAndPluginUtil;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.text.StateFocusTextField;
import ij.gui.Line;
import ij.gui.Roi;
import ij.gui.RoiHack;

/**
 * A {@link JPanel} that also is an {@link IStatisticsComponent}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class StatisticsPanel extends JPanel
        implements IStatisticsComponent, CometeConstants, ActionListener, DocumentListener {

    private static final long serialVersionUID = -3546894616939150267L;

    private static final Font TITLE_FONT = DEFAULT_FONT.deriveFont(Font.BOLD);
    private static final Color FIELD_BORDER_COLOR = new Color(150, 150, 150, 200);

    private static final String RANGES = "Range:";
    private static final String AVERAGE = "Average:";
    private static final String STD_DEV = "Std deviation:";
    private static final String SAMPLESTD_DEV = "Sample std deviation:";
    private static final String ROI = "Roi:";

    private final WeakReference<ImageViewer> imageViewerRef;
    private WeakReference<Roi> roiRef;
    private final RoiStatisticsListener roiListener;
    private final double[] statistics;
    private String valueFormat;
    private volatile boolean nameEditing, roiEditing;

    private final JLabel rangeLabel, averageLabel, stdDeviationLabel, sampleStdDeviationLabel, roiLabel;
    private final JLabel roiStartLabel, roiEndLabel;
    private final JTextField rangeField, averageField, stdDeviationField, sampleStdDeviationField;
    private final StateFocusTextField roiNameField, roiConstructionField;

    public StatisticsPanel(ImageViewer imageViewer) {
        super(new GridBagLayout());
        imageViewerRef = imageViewer == null ? null : new WeakReference<>(imageViewer);
        roiRef = null;
        statistics = IStatisticsComponent.generateStatisticsArray();
        roiListener = new RoiStatisticsListener(imageViewer, this);
        rangeLabel = generateTitleLabel(RANGES);
        averageLabel = generateTitleLabel(AVERAGE);
        stdDeviationLabel = generateTitleLabel(STD_DEV);
        sampleStdDeviationLabel = generateTitleLabel(SAMPLESTD_DEV);
        roiLabel = generateTitleLabel(ROI);
        roiStartLabel = generateValueLabel();
        roiStartLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        roiEndLabel = generateValueLabel();
        rangeField = generateValueField();
        averageField = generateValueField();
        stdDeviationField = generateValueField();
        sampleStdDeviationField = generateValueField();
        roiNameField = generateStateValueField();
        roiConstructionField = generateStateValueField();
        roiConstructionField.setVisible(false);
        int x = 0, y = 0;
        x = addComponent(rangeLabel, rangeField, 0, 5, 0, x, y);
        x = addComponent(averageLabel, averageField, 10, 5, 0, x, y++);
        x = 0;
        x = addComponent(stdDeviationLabel, stdDeviationField, 0, 5, 5, x, y);
        x = addComponent(sampleStdDeviationLabel, sampleStdDeviationField, 30, 5, 5, x, y++);
        x = 0;
        x = addComponent(roiLabel, roiNameField, 0, 5, 5, x, y);
        x = addComponent(roiStartLabel, roiConstructionField, 10, 0, 5, x, y);
        x = addComponent(roiEndLabel, null, 0, 0, 5, x, y++);
    }

    @Override
    public RoiStatisticsListener getRoiListener() {
        return roiListener;
    }

    @Override
    public String getValueFormat() {
        return valueFormat;
    }

    @Override
    public void setValueFormat(String valueFormat) {
        if (!ObjectUtils.sameObject(valueFormat, this.valueFormat)) {
            this.valueFormat = valueFormat;
            computeStatisticsAndDisplayRoiInformation(ObjectUtils.recoverObject(imageViewerRef));
        }
    }

    @Override
    public void computeStatisticsAndDisplayRoiInformation(ImageViewer imageViewer) {
        if (isVisible()) {
            Roi roi = IStatisticsComponent.getRoi(imageViewer, this::checkType);
            IStatisticsComponent.computeStatistics(imageViewer, roi, statistics);
            roiRef = roi == null ? null : new WeakReference<>(roi);
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                updateInformation(imageViewer);
            });
        }
    }

    @Override
    public boolean checkType(Roi roi) {
        return IStatisticsComponent.validateType(roi, RECTANGLE_ROI_ONLY);
    }

    protected String getRoiText(Roi roi, StringBuilder builder) {
        String roiText;
        StringBuilder tmp = builder == null ? new StringBuilder() : builder;
        if (roi == null) {
            roiText = ObjectUtils.EMPTY_STRING;
        } else {
            try {
                RoiAndPluginUtil.roiShapeToAppendable(roi, tmp);
                roiText = tmp.toString();
                tmp.delete(0, tmp.length());
            } catch (Exception e) {
                // Should not happen as we use a StringBuilder
                roiText = ObjectUtils.EMPTY_STRING;
            }
        }
        return roiText;
    }

    protected void setValue(StateFocusTextField field, String value) {
        boolean mayTreat = true;
        if (field == roiNameField) {
            mayTreat = !nameEditing;
        } else if (field == roiConstructionField) {
            mayTreat = !roiEditing;
        }
        if (mayTreat) {
            mayTreat = (!ObjectUtils.sameObject(field.getText(), value)) || field.isEditing()
                    || field.isErrorOrNoValue();
        }
        if (mayTreat) {
            field.setValidValue(value);
            field.setToolTipText(value == null || value.isEmpty() ? null : value);
            if (field == roiNameField) {
                nameEditing = false;
            } else if (field == roiConstructionField) {
                roiEditing = false;
            }
        }
    }

    protected void updateInformation(ImageViewer imageViewer) {
        Roi roi = ObjectUtils.recoverObject(roiRef);
        StringBuilder builder = new StringBuilder();
        builder.append(IStatisticsComponent.toString(statistics[MIN_INDEX], valueFormat, false));
        builder.append(RANGES_SEPARATOR);
        builder.append(IStatisticsComponent.toString(statistics[MAX_INDEX], valueFormat, false));
        rangeField.setText(builder.toString());
        builder.delete(0, builder.length());
        averageField.setText(IStatisticsComponent.toString(statistics[AVERAGE_INDEX], valueFormat, true));
        stdDeviationField.setText(IStatisticsComponent.toString(statistics[STDDEV_INDEX], valueFormat, true));
        sampleStdDeviationField
                .setText(IStatisticsComponent.toString(statistics[SAMPLESTDDEV_INDEX], valueFormat, true));
        boolean nameEditable, roiEditable;
        String roiText;
        if (roi == null) {
            roiText = ObjectUtils.EMPTY_STRING;
            setValue(roiNameField, ObjectUtils.EMPTY_STRING);
            nameEditable = false;
            roiEditable = false;
        } else {
            setValue(roiNameField, roi.getName());
            roiEditable = (roi.getType() == Roi.RECTANGLE) || (roi.getType() == Roi.LINE);
            if (imageViewer == null) {
                nameEditable = true;
            } else {
                nameEditable = imageViewer.isEditableRoiName(roi);
            }
            roiText = getRoiText(roi, builder);
        }
        setFieldEditable(roiNameField, nameEditable);
        setFieldEditable(roiConstructionField, roiEditable);
        int startIndex = roiText.indexOf('('), endIndex = roiText.lastIndexOf(')');
        if (startIndex > -1 && endIndex > startIndex) {
            roiStartLabel.setText(roiText.substring(0, ++startIndex));
            setValue(roiConstructionField, roiText.substring(startIndex, endIndex));
            roiEndLabel.setText(RoiAndPluginUtil.SHAPE_CLOSER);
        } else {
            roiStartLabel.setText(ObjectUtils.EMPTY_STRING);
            roiEndLabel.setText(ObjectUtils.EMPTY_STRING);
            setValue(roiConstructionField, ObjectUtils.EMPTY_STRING);
        }
        if (!roiEditing) {
            roiConstructionField.setVisible(!roiConstructionField.getText().isEmpty());
        }
        roiStartLabel.revalidate();
        roiEndLabel.revalidate();
        revalidate();
        repaint();
    }

    protected JLabel generateTitleLabel(String title) {
        JLabel label = new JLabel(title);
        label.setHorizontalAlignment(SwingConstants.RIGHT);
        label.setFont(TITLE_FONT);
        return label;
    }

    protected JLabel generateValueLabel() {
        JLabel label = new JLabel();
        label.setFont(DEFAULT_FONT);
        return label;
    }

    protected JTextField generateValueField() {
        JTextField field = new JTextField(15);
        field.setBorder(new LineBorder(FIELD_BORDER_COLOR));
        field.setEditable(false);
        return field;
    }

    protected void setFieldEditable(StateFocusTextField field, boolean editable) {
        field.setEditable(editable);
        field.setOpaque(editable);
    }

    protected StateFocusTextField generateStateValueField() {
        StateFocusTextField field = new StateFocusTextField(15, null);
        field.setNoValueText(ObjectUtils.EMPTY_STRING);
        field.setValidValue(ObjectUtils.EMPTY_STRING);
        field.setBorder(new LineBorder(FIELD_BORDER_COLOR));
        setFieldEditable(field, false);
        field.setEditable(false);
        field.setOpaque(false);
        field.addActionListener(this);
        field.getDocument().addDocumentListener(this);
        return field;
    }

    protected int addComponent(JLabel title, JComponent editor, int titleGap, int editorGap, int topGap, int x, int y) {
        GridBagConstraints titleConstraints = new GridBagConstraints();
        titleConstraints.fill = GridBagConstraints.BOTH;
        titleConstraints.gridx = x++;
        titleConstraints.gridy = y;
        titleConstraints.weightx = 0;
        titleConstraints.weighty = 0;
        titleConstraints.insets = new Insets(topGap, titleGap, 0, 0);
        if (title != null) {
            add(title, titleConstraints);
        }
        GridBagConstraints editorConstraints = new GridBagConstraints();
        editorConstraints.fill = GridBagConstraints.HORIZONTAL;
        editorConstraints.gridx = x++;
        editorConstraints.gridy = y;
        editorConstraints.weightx = 0.5;
        editorConstraints.weighty = 0;
        editorConstraints.insets = new Insets(topGap, editorGap, 0, 0);
        if (editor != null) {
            add(editor, editorConstraints);
        }
        return x;
    }

    protected String getText(JTextField field) {
        String text = field.getText();
        if (text == null) {
            text = ObjectUtils.EMPTY_STRING;
        } else {
            text = text.trim();
        }
        return text;
    }

    protected void resetRoiConstructionField(Roi roi) {
        if (roi == null) {
            setValue(roiConstructionField, ObjectUtils.EMPTY_STRING);
        } else {
            String roiText = getRoiText(roi, null);
            int startIndex = roiText.indexOf('('), endIndex = roiText.lastIndexOf(')');
            if (startIndex > -1 && endIndex > startIndex) {
                setValue(roiConstructionField, roiText.substring(++startIndex, endIndex));
                roiConstructionField.setVisible(true);
            } else {
                setValue(roiConstructionField, ObjectUtils.EMPTY_STRING);
                roiConstructionField.setVisible(false);
            }
        }
    }

    protected void treadDocumentEvent(DocumentEvent e) {
        if (e != null) {
            if (e.getDocument() == roiNameField.getDocument()) {
                nameEditing = true;
            } else if (e.getDocument() == roiConstructionField.getDocument()) {
                roiEditing = true;
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e != null) {
            if (e.getSource() == roiNameField) {
                if (roiNameField.isEditable()) {
                    String name = getText(roiNameField);
                    nameEditing = false;
                    setValue(roiNameField, name);
                    ImageViewer imageViewer = ObjectUtils.recoverObject(imageViewerRef);
                    Roi roi = ObjectUtils.recoverObject(roiRef);
                    if ((roi == null) || ((imageViewer != null) && !imageViewer.isEditableRoiName(roi))) {
                        setValue(roiNameField, roi == null ? ObjectUtils.EMPTY_STRING : roi.getName());
                    } else {
                        roi.setName(name);
                        if (imageViewer != null) {
                            imageViewer.getImageCanvas().roiChanged(roi);
                            imageViewer.repaint();
                        }
                    }
                } else {
                    nameEditing = false;
                }
            } else if (e.getSource() == roiConstructionField) {
                if (roiConstructionField.isEditable()) {
                    Roi roi = ObjectUtils.recoverObject(roiRef);
                    ImageViewer imageViewer = ObjectUtils.recoverObject(imageViewerRef);
                    String[] roiData = getText(roiConstructionField).split(COMMA);
                    roiEditing = false;
                    if ((roi != null) && ((roi.getType() == Roi.RECTANGLE) || (roi.getType() == Roi.LINE))) {
                        boolean changed = true;
                        try {
                            if (roi.getType() == Roi.RECTANGLE) {
                                if (roiData.length == 4) {
                                    Rectangle bounds = roi.getBounds();
                                    int x = Integer.parseInt(roiData[0].trim());
                                    int y = Integer.parseInt(roiData[1].trim());
                                    int width = Integer.parseInt(roiData[2].trim());
                                    int height = Integer.parseInt(roiData[3].trim());
                                    roi.setLocation(x, y);
                                    try {
                                        RoiHack.resizeRoi(roi, width, height);
                                    } catch (Exception ex) {
                                        changed = false;
                                        roi.setLocation(bounds.x, bounds.y);
                                        throw ex;
                                    }
                                } else {
                                    changed = false;
                                }
                            } else if (roi instanceof Line) {
                                if (roiData.length == 4) {
                                    int x1 = Integer.parseInt(roiData[0].trim());
                                    int y1 = Integer.parseInt(roiData[1].trim());
                                    int x2 = Integer.parseInt(roiData[2].trim());
                                    int y2 = Integer.parseInt(roiData[3].trim());
                                    changed = RoiHack.setCoordinates((Line) roi, x1, y1, x2, y2);
                                } else {
                                    changed = false;
                                }
                            } else {
                                changed = false;
                            }
                        } catch (Exception ex) {
                            changed = false;
                        }
                        if (changed && (imageViewer != null)) {
                            imageViewer.getImageCanvas().roiChanged(roi);
                            imageViewer.repaint();
                            // JAVAAPI-643: update statistics values
                            computeStatisticsAndDisplayRoiInformation(imageViewer);
                        } else {
                            SwingUtilities.invokeLater(() -> {
                                resetRoiConstructionField(roi);
                            });
                        }
                    }
                } else {
                    roiEditing = false;
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        treadDocumentEvent(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        treadDocumentEvent(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        // nothing to do
    }

}
