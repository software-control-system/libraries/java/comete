/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;

import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.DataView;

public class DialogList<T extends AbstractDataView> extends JDialog implements ComponentListener {

    private static final long serialVersionUID = 1252312868267768473L;

    private static final int ADD = 0;
    private static final int REMOVE = 1;
    private static final int ADD_ALL = 2;
    private static final int REMOVE_ALL = 3;

    private JPanel mainPanel;
    private JPanel southPanel;
    private JPanel northPanel;
    private JPanel centerPanel;

    private JButton add;
    private JButton remove;
    private JButton addAll;
    private JButton removeAll;
    private JButton valide;
    private JButton cancel;

    private JLabel titleLabel;
    private GridBagConstraints titleLabelConstraints;
    private JTextField searchField;
    private GridBagConstraints searchFieldConstraints;

    private JList<T> list1;
    private JScrollPane selector;
    private JList<T> list2;
    private JScrollPane selected;

    private final List<T> data1;
    private final List<T> data2;
    private final List<T> dataCopy;
    private final Class<T> dataClass;

    private List<T> result;

    private final boolean isSingle;

    public DialogList(Window parent, String title, boolean modal, List<T> views, boolean mode, Class<T> dataClass) {
        super(parent, title);
        setModal(modal);

        data1 = (views == null ? new ArrayList<>() : new ArrayList<>(views));
        dataCopy = new ArrayList<>();
        data2 = new ArrayList<>();

        result = new ArrayList<>();
        isSingle = mode;

        this.dataClass = dataClass;

        initialize();
    }

    private void initialize() {
        mainPanel = new JPanel(new BorderLayout());

        initializeCenterPanel();
        initializeNorthPanel();
        initializeSouthPanel();

        mainPanel.add(northPanel, BorderLayout.NORTH);
        mainPanel.add(centerPanel, BorderLayout.CENTER);
        mainPanel.add(southPanel, BorderLayout.SOUTH);

        setContentPane(mainPanel);

        updateLists();

        mainPanel.addComponentListener(this);
    }

    /**
     * Creates an array, never <code>null</code>, from a {@link Collection}
     * 
     * @param list The {@link Collection}. If <code>null</code>, the resulting array is of length <b>0</b>.
     * @return An array.
     */
    @SuppressWarnings("unchecked")
    protected final T[] toArray(Collection<T> list) {
        T[] result;
        if (list == null) {
            result = (T[]) Array.newInstance(dataClass, 0);
        } else {
            result = list.toArray((T[]) Array.newInstance(dataClass, list.size()));
        }
        return result;
    }

    private void initializeCenterPanel() {
        centerPanel = new JPanel(new GridBagLayout());

        list1 = new JList<>(toArray(data1));
        selector = new JScrollPane(list1);
        if (isSingle) {
            list1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        }

        GridBagConstraints selectorConstraints = new GridBagConstraints();
        selectorConstraints.fill = GridBagConstraints.BOTH;
        selectorConstraints.gridx = 0;
        selectorConstraints.gridy = 0;
        selectorConstraints.weighty = 1;
        if (isSingle) {
            selectorConstraints.weightx = 1;
            selectorConstraints.gridwidth = GridBagConstraints.REMAINDER;
        } else {
            selectorConstraints.weightx = 0.5;
        }
        centerPanel.add(selector, selectorConstraints);

        if (!isSingle) {

            JPanel buttonPanel = new JPanel();
            buttonPanel.setLayout(new GridLayout(5, 1));

            add = new JButton("Add >");
            remove = new JButton("< Remove");
            addAll = new JButton("Add All >>");
            removeAll = new JButton("<< Remove All");

            add.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    moveItem(ADD);
                }
            });

            remove.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    moveItem(REMOVE);
                }
            });

            addAll.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    moveItem(ADD_ALL);
                }
            });

            removeAll.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    moveItem(REMOVE_ALL);
                }
            });

            buttonPanel.add(add);
            buttonPanel.add(remove);
            buttonPanel.add(new JSeparator());
            buttonPanel.add(addAll);
            buttonPanel.add(removeAll);

            list2 = new JList<>(toArray(data2));
            selected = new JScrollPane(list2);

            GridBagConstraints buttonPanelConstraints = new GridBagConstraints();
            buttonPanelConstraints.fill = GridBagConstraints.NONE;
            buttonPanelConstraints.gridx = 1;
            buttonPanelConstraints.gridy = 0;
            buttonPanelConstraints.weightx = 0;
            buttonPanelConstraints.weighty = 0;
            centerPanel.add(buttonPanel, buttonPanelConstraints);

            GridBagConstraints selectedConstraints = new GridBagConstraints();
            selectedConstraints.fill = GridBagConstraints.BOTH;
            selectedConstraints.gridx = 2;
            selectedConstraints.gridy = 0;
            selectedConstraints.weightx = 0.5;
            selectedConstraints.weighty = 1;
            centerPanel.add(selected, selectedConstraints);

        }

    }

    private void initializeNorthPanel() {

        northPanel = new JPanel(new GridBagLayout());
        titleLabel = new JLabel("Select the view you want to see / save and add it to the list below", JLabel.CENTER);

        titleLabelConstraints = new GridBagConstraints();
        titleLabelConstraints.fill = GridBagConstraints.BOTH;
        titleLabelConstraints.gridx = 0;
        titleLabelConstraints.gridy = 0;
        titleLabelConstraints.weightx = 0;
        titleLabelConstraints.weighty = 0;
        northPanel.add(titleLabel, titleLabelConstraints);

        searchField = new JTextField(20);
        searchField.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                searchView(searchField.getText());
            }
        });

        searchFieldConstraints = new GridBagConstraints();
        searchFieldConstraints.fill = GridBagConstraints.BOTH;
        searchFieldConstraints.gridx = 1;
        searchFieldConstraints.gridy = 0;
        searchFieldConstraints.weightx = 1;
        searchFieldConstraints.weighty = 0;
        northPanel.add(searchField, searchFieldConstraints);

    }

    private void initializeSouthPanel() {

        southPanel = new JPanel();

        valide = new JButton("OK");
        cancel = new JButton("Cancel");

        valide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                valideChange();
            }
        });
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        southPanel.add(valide);
        southPanel.add(cancel);

    }

    private void moveItem(int mode) {
        T[] listView;
        switch (mode) {
            case ADD:
                listView = toArray(list1.getSelectedValuesList());
                break;
            case ADD_ALL:
                listView = toArray(data1);
                break;
            case REMOVE:
                listView = toArray(list2.getSelectedValuesList());
                break;
            case REMOVE_ALL:
                listView = toArray(data2);
                break;
            default:
                listView = toArray(null);
                break;
        }
        if (listView.length != 0) {
            for (T i : listView) {
                if ((mode == ADD) || (mode == ADD_ALL)) {
                    if (!data2.contains(i)) {
                        data2.add(i);
                        data1.remove(i);
                    }
                } else if ((mode == REMOVE) || (mode == REMOVE_ALL)) {
                    if (!data1.contains(i)) {
                        data1.add(i);
                        data2.remove(i);
                    }
                }
            }
            updateLists();
        }
    }

    public void updateLists() {
        list1.setListData(toArray(data1));
        adaptScrollPaneSize(selector, list1);
        if (!isSingle) {
            list2.setListData(toArray(data2));
            adaptScrollPaneSize(selected, list2);
        }
    }

    private void adaptScrollPaneSize(JScrollPane scrollPane, JList<?> source) {
        Dimension sourceSize = source.getPreferredSize();
        Dimension preferredSize = new Dimension(150, sourceSize.height + 4);
        if (preferredSize.height > 500) {
            preferredSize.height = 500;
        }
        scrollPane.setPreferredSize(preferredSize);
    }

    public void searchView(String src) {
        if (src.isEmpty()) {
            data1.addAll(dataCopy);
            dataCopy.clear();
            // System.out.println("do clear");
        } else {
            ArrayList<T> toRemove = new ArrayList<>();
            for (T v1 : data1) {
                if (!v1.getDisplayName().contains(src)) {
                    dataCopy.add(v1);
                    toRemove.add(v1);
                }
            }
            data1.removeAll(toRemove);
            toRemove.clear();

            for (T v2 : dataCopy) {
                if (v2.getDisplayName().contains(src)) {
                    data1.add(v2);
                    toRemove.add(v2);
                }
            }
            dataCopy.removeAll(toRemove);

        }
        updateLists();

        // System.out.println("data 1 :");
        // for (T v2 : data1) {
        // System.out.println(v2);
        // }
        // System.out.println("\ndata Copy :");
        // for (T v2 : dataCopy) {
        // System.out.println(v2);
        // }

    }

    public List<T> showOptionDialog(Component caller) {
        pack();
        setLocationRelativeTo(caller);
        this.setVisible(true);
        return result;
    }

    public void valideChange() {
        result = new ArrayList<>();
        if (isSingle) {
            result.add(list1.getSelectedValue());
        } else {
            result.addAll(data2);
        }
        this.setVisible(false);
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        // nothing to do
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        // nothing to do
    }

    @Override
    public void componentResized(ComponentEvent e) {
        if (e.getSource() == mainPanel) {
            int prefWidth = titleLabel.getPreferredSize().width + searchField.getPreferredSize().width;
            if (getWidth() < prefWidth) {
                searchFieldConstraints.gridx = 0;
                searchFieldConstraints.gridy = 1;
            } else {
                searchFieldConstraints.gridx = 1;
                searchFieldConstraints.gridy = 0;
            }
            northPanel.remove(searchField);
            northPanel.add(searchField, searchFieldConstraints);
            northPanel.revalidate();
            mainPanel.revalidate();
        }
    }

    @Override
    public void componentShown(ComponentEvent e) {
        // nothing to do
    }

    @Override
    protected void finalize() {
        data1.clear();
        data2.clear();
        dataCopy.clear();
        result = null;
    }

    public static void main(String[] args) {
        JFrame f = new JFrame();

        final JButton openDialog = new JButton("Open Dialog");
        openDialog.setPreferredSize(new Dimension(200, 100));
        openDialog.addActionListener((e) -> {
            List<DataView> arg = new ArrayList<>();
            for (int l = 0; l < 10; ++l) {
                DataView dataview = new DataView("Courbe " + l);
                for (int i = 0; i < 750; i++) {
                    double t = i / 25.0;
                    dataview.add(t, Math.pow(t, 2) + t + 1);
                }
                arg.add(dataview);
            }
            DialogList<DataView> dialog = new DialogList<>(null, "test dialog", true, arg, false, DataView.class);
            List<DataView> res = dialog.showOptionDialog(openDialog);
            if (res != null) {
                for (int i = 0; i < res.size(); ++i) {
                    System.out.println(res.get(i));
                }
            }
            res.clear();
        });

        JPanel panel = new JPanel();
        panel.add(openDialog);
        f.add(panel);

        f.setSize(400, 200);
        f.setVisible(true);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

}
