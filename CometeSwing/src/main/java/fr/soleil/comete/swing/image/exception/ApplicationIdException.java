/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.exception;

/**
 * This exception is thrown by an ImageViewer when you try to set its image or data and you did not
 * set its application id
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ApplicationIdException extends RuntimeException {

    private static final long serialVersionUID = 890318506147996494L;

    /**
     * Constructs this {@link ApplicationIdException} with a message
     * 
     * @param message The message to use.
     */
    public ApplicationIdException(String message) {
        super(message);
    }

}
