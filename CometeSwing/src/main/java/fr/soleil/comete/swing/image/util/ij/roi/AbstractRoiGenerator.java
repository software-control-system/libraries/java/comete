/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.ImageIcon;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.action.AbstractActionExt;
import org.jdesktop.swingx.action.ActionManager;

import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.image.ijviewer.events.IRoiModeListener;
import fr.soleil.comete.swing.image.ijviewer.events.RoiEvent;
import ij.gui.Roi;
import ij.gui.Toolbar;

public abstract class AbstractRoiGenerator extends MouseAdapter
        implements RoiGenerator, MouseMotionListener, FocusListener, IRoiModeListener {

    private String text;
    private String description;
    private final AbstractActionExt action;
    private final Stroke stroke, hoveredStroke, selectedStroke;
    private final Color color, hoveredColor, selectedColor;
    private final Map<ImageViewer, Collection<Integer>> keyCodeMap;
    private volatile int lastMode;

    public AbstractRoiGenerator(String text, String description, ImageIcon icon) {
        this.text = text;
        this.description = description;
        AbstractActionExt tmpAction = (AbstractActionExt) ActionManager.getInstance().getAction(getActionCommand());
        if (tmpAction == null) {
            tmpAction = generateAction();
            if (text != null) {
                tmpAction.setName(text);
            }
            if (description != null) {
                tmpAction.setShortDescription(description);
            }
            if (icon != null) {
                tmpAction.setSmallIcon(icon);
            }
            tmpAction.setActionCommand(getActionCommand());
        }
        action = tmpAction;
        ActionManager.getInstance().addAction(action);
        stroke = generateRoiStroke();
        hoveredStroke = getValue(generateHoveredRoiStroke(), stroke);
        selectedStroke = getValue(generateSelectedRoiStroke(), stroke);
        color = generateRoiColor();
        hoveredColor = getValue(generateHoveredRoiColor(), color);
        selectedColor = getValue(generateSelectedRoiColor(), color);
        keyCodeMap = new WeakHashMap<>();
        lastMode = IJRoiManager.getRoiMode();
        IJRoiManager.addRoiModeListener(this);
    }

    /**
     * This method is only used to eventually delegate the work to another {@link RoiGenerator}.
     * 
     * @return The {@link RoiGenerator} to which work is delegated. By default, it is <code>this</code>.
     */
    protected RoiGenerator getReferentRoiGenerator() {
        return this;
    }

    protected boolean isMyRoi(RoiEvent event) {
        boolean myRoi;
        IJRoiManager manager = null;
        if (event != null) {
            if (event.getSource() instanceof IJRoiManager) {
                manager = (IJRoiManager) event.getSource();
            }
        }
        if (manager == null) {
            myRoi = false;
        } else {
            myRoi = event.getRoi() == manager.getSpecialRoi(getReferentRoiGenerator());
        }
        return myRoi;
    }

    public String getText() {
        return text;
    }

    protected void setText(String text) {
        this.text = text;
        action.setName(text);
    }

    public String getDescription() {
        return description;
    }

    protected void setDescription(String description) {
        this.description = description;
        action.setShortDescription(description);
    }

    private Collection<Integer> prepareKeyCodeMap(ImageViewer viewer) {
        Collection<Integer> codes;
        if (viewer == null) {
            codes = null;
        } else {
            codes = keyCodeMap.get(viewer);
            if (codes == null) {
                codes = Collections.newSetFromMap(new ConcurrentHashMap<>());
                keyCodeMap.put(viewer, codes);
            }
        }
        return codes;
    }

    protected boolean isAltKeyCodes(ImageViewer viewer) {
        boolean alt = false;
        if (viewer == null) {
            alt = false;
        } else {
            alt = isAltKeyCodes(keyCodeMap.get(viewer));
        }
        return alt;
    }

    @Override
    public boolean canHoverOtherRois() {
        return !needsNoHandledRoi();
    }

    protected void applyCursor(ImageViewer viewer) {
        if (viewer != null) {
            IJCanvas canvas = viewer.getImageCanvas();
            if (canvas != null) {
                Cursor cursor = getCursor();
                if (cursor != null) {
                    canvas.doSetCursor(cursor);
                }
            }
        }
    }

    protected static IMaskedImageViewer recoverViewer(IJRoiManager roiManager) {
        return roiManager == null ? null : roiManager.getImageViewer();
    }

    protected static ImageViewer recoverViewer(Component comp) {
        ImageViewer result;
        if (comp == null) {
            result = null;
        } else if (comp instanceof ImageViewer) {
            result = (ImageViewer) comp;
        } else if (comp instanceof JPopupMenu) {
            result = recoverViewer(((JPopupMenu) comp).getInvoker());
        } else {
            result = recoverViewer(comp.getParent());
        }
        return result;
    }

    protected static <T> T getValue(T value, T defaultValue) {
        T result = value;
        if (result == null) {
            result = defaultValue;
        }
        return result;
    }

    protected abstract int getTool();

    protected final AbstractActionExt generateAction() {
        AbstractActionExt action = new AbstractActionExt() {

            private static final long serialVersionUID = 699909092908912004L;

            @Override
            public void actionPerformed(ActionEvent e) {
                Toolbar.getInstance().setTool(getTool());
                IJRoiManager.setRoiMode(getRoiMode());
                ActionManager.getInstance().setSelected(AbstractRoiGenerator.this.getActionCommand(), true);
                ImageViewer src = (e == null ? null : recoverViewer((Component) e.getSource()));
                if (e != null) {
                    applyCursor(src);
                }
                treatData(e, src);
            }
        };
        action.setStateAction();
        action.setGroup(ImageViewer.IJ_ACTION_GROUP);
        return action;
    }

    protected void treatData(ActionEvent e, ImageViewer viewer) {
        if (viewer != null) {
            viewer.getImagePanel().addFocusListener(this);
        }
    }

    private void clearKeyCodes(ImageViewer viewer) {
        Collection<Integer> codes = prepareKeyCodeMap(viewer);
        if (codes != null) {
            codes.clear();
        }
    }

    protected void cleanData(ImageViewer viewer) {
        if (viewer != null) {
            viewer.getImagePanel().removeFocusListener(this);
            clearKeyCodes(viewer);
        }
    }

    protected abstract Stroke generateRoiStroke();

    protected boolean isAltAction(InputEvent e) {
        return (e != null) && (e.isControlDown() || e.isShiftDown() || e.isAltDown() || e.isAltGraphDown());
    }

    protected boolean isAltKeyCode(int keyCode) {
        return (keyCode == KeyEvent.VK_CONTROL) || (keyCode == KeyEvent.VK_SHIFT) || (keyCode == KeyEvent.VK_ALT)
                || (keyCode == KeyEvent.VK_ALT_GRAPH) || (keyCode == KeyEvent.VK_WINDOWS);
    }

    protected boolean isAltKeyCodes(Collection<Integer> keyCodes) {
        boolean alt = false;
        if (keyCodes != null) {
            for (Integer keyCode : keyCodes) {
                if ((keyCode != null) && isAltKeyCode(keyCode.intValue())) {
                    alt = true;
                    break;
                }
            }
        }
        return alt;
    }

    protected Stroke generateHoveredRoiStroke() {
        return null;
    }

    protected Stroke generateSelectedRoiStroke() {
        return null;
    }

    protected abstract Color generateRoiColor();

    protected Color generateHoveredRoiColor() {
        return null;
    }

    protected Color generateSelectedRoiColor() {
        return null;
    }

    @Override
    public boolean isDataDependent() {
        return false;
    }

    @Override
    public boolean isAxisDependent() {
        return false;
    }

    @Override
    public boolean isColorScaleDependent() {
        return false;
    }

    @Override
    public boolean isPixelSizeDependent() {
        return false;
    }

    @Override
    public boolean isZoomDependent() {
        return false;
    }

    @Override
    public void dataChanged(ImageViewer viewer) {
        // Nothing to do by default
    }

    @Override
    public void axisChanged(ImageViewer viewer) {
        // Nothing to do by default
    }

    @Override
    public void colorScaleChanged(ImageViewer viewer) {
        // Nothing to do by default
    }

    @Override
    public void pixelSizeChanged(ImageViewer viewer) {
        // Nothing to do by default
    }

    @Override
    public void zoomChanged(ImageViewer viewer) {
        // Nothing to do by default
    }

    @Override
    public void roiAdded(Roi roi, IJRoiManager roiManager) {
        ImageViewer viewer;
        IMaskedImageViewer context = recoverViewer(roiManager);
        if (context instanceof ImageViewer) {
            viewer = (ImageViewer) context;
        } else {
            viewer = null;
        }
        if (viewer != null) {
            viewer.getImagePanel().addMouseListener(this);
            viewer.getImagePanel().addMouseMotionListener(this);
        }
    }

    @Override
    public void roiRemoved(Roi roi, IJRoiManager roiManager) {
        ImageViewer viewer;
        IMaskedImageViewer context = recoverViewer(roiManager);
        if (context instanceof ImageViewer) {
            viewer = (ImageViewer) context;
        } else {
            viewer = null;
        }
        if (viewer != null) {
            viewer.getImagePanel().removeMouseListener(this);
            viewer.getImagePanel().removeMouseMotionListener(this);
        }
    }

    @Override
    public AbstractActionExt getRoiModeAction() {
        return action;
    }

    @Override
    public int getPreferredPositionInMenu() {
        return -1;
    }

    @Override
    public Stroke getRoiStroke() {
        return stroke;
    }

    @Override
    public Color getRoiColor() {
        return color;
    }

    @Override
    public Stroke getHoveredRoiStroke() {
        return hoveredStroke;
    }

    @Override
    public Color getHoveredRoiColor() {
        return hoveredColor;
    }

    @Override
    public Stroke getSelectedRoiStroke() {
        return selectedStroke;
    }

    @Override
    public Color getSelectedRoiColor() {
        return selectedColor;
    }

    @Override
    public MouseEvent adaptMousePressedEvent(MouseEvent event) {
        return event;
    }

    @Override
    public MouseEvent adaptMouseReleasedEvent(MouseEvent event) {
        return event;
    }

    @Override
    public MouseEvent adaptMouseClickedEvent(MouseEvent event) {
        return event;
    }

    @Override
    public MouseEvent adaptMouseDraggedEvent(MouseEvent event) {
        return event;
    }

    @Override
    public MouseEvent adaptMouseMovedEvent(MouseEvent event) {
        return event;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // Nothing to do by default
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // Nothing to do by default
    }

    @Override
    public boolean keyPressed(KeyEvent e) {
        boolean treated = false;
        if (e != null) {
            ImageViewer viewer = recoverViewer(e.getComponent());
            Collection<Integer> codes = prepareKeyCodeMap(viewer);
            if (codes != null) {
                codes.add(Integer.valueOf(e.getKeyCode()));
            }
        }
        return treated;
    }

    @Override
    public boolean keyReleased(KeyEvent e) {
        boolean treated = false;
        if (e != null) {
            ImageViewer viewer = recoverViewer(e.getComponent());
            Collection<Integer> codes = prepareKeyCodeMap(viewer);
            if (codes != null) {
                codes.remove(Integer.valueOf(e.getKeyCode()));
            }
        }
        return treated;
    }

    @Override
    public boolean keyTyped(KeyEvent e) {
        return keyReleased(e);
    }

    @Override
    public void focusGained(FocusEvent e) {
        // Nothing to do
    }

    @Override
    public void focusLost(FocusEvent e) {
        // We might miss "key released" events --> clear key code list
        if (e != null) {
            clearKeyCodes(recoverViewer(e.getComponent()));
        }
    }

    protected final int getLastMode() {
        return lastMode;
    }

    protected boolean isCleanDataOnDifferentMode(ImageViewer viewer) {
        return lastMode == getRoiMode();
    }

    protected boolean isTreatDataOnDifferentMode(ImageViewer viewer) {
        return false;
    }

    @Override
    public void roiModeChanged(RoiEvent event) {
        if (IJRoiManager.getRoiMode() == getRoiMode()) {
            for (ImageViewer viewer : keyCodeMap.keySet()) {
                treatData(null, viewer);
            }
        } else {
            for (ImageViewer viewer : keyCodeMap.keySet()) {
                if (isCleanDataOnDifferentMode(viewer)) {
                    cleanData(viewer);
                } else if (isTreatDataOnDifferentMode(viewer)) {
                    treatData(null, viewer);
                }
            }
        }
        lastMode = IJRoiManager.getRoiMode();
    }

}
