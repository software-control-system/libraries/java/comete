/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.axis.scale;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.swing.chart.axis.model.AxisAttributes;
import fr.soleil.comete.swing.chart.axis.model.AxisLabel;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.DataList;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.data.mediator.Mediator;

public class LinearScale extends AbstractAxisScale {

    public LinearScale(AxisAttributes attributes) {
        super(attributes);
    }

    @Override
    public Map<TickType, int[]> computeTicksPositions(int axisOrigin, int axisLength) {
        Map<TickType, int[]> tickMap = new HashMap<AbstractAxisScale.TickType, int[]>();
        fillTickMap(tickMap, labels, axisOrigin, axisLength, tickStep, subTickStep);
        return tickMap;
    }

    /**
     * Calculates ticks and sub-ticks positions and fills a {@link Map} with them
     * 
     * @param tickMap The {@link Map} to fill
     * @param labels The previously calculated {@link AxisLabel}s
     * @param axisOrigin The axis origin position
     * @param axisLength The axis length
     * @param tickStep The previously calculated tick step
     * @param subTickStep The previously calculated sub-tick step
     */
    public static void fillTickMap(Map<TickType, int[]> tickMap, AxisLabel[] labels, int axisOrigin, int axisLength,
            double tickStep, int subTickStep) {
        if ((tickMap != null) && (labels != null)) {
            int size = labels.length;
            if (size > 0) {
                int subTickLength;
                double r = 1.0 / subTickStep;
                int firstLength;
                int lastLength;
                double firstOrigin;
                AxisLabel lastLabel = labels[size - 1];
                int[] firstSubTicks = new int[0];
                int[] lastSubTicks = new int[0];
                int max = axisOrigin + axisLength;
                if (subTickStep > 0) {
                    AxisLabel firstLabel = labels[0];
                    firstLength = subTickStep;
                    lastLength = subTickStep;
                    firstOrigin = axisOrigin + firstLabel.getPos() - tickStep;
                    double lastOrigin = axisOrigin + lastLabel.getPos();
                    for (int currentStep = 0; currentStep < subTickStep; currentStep++) {
                        int firstPos = (int) Math.rint(firstOrigin + tickStep * r * currentStep);
                        int lastPos = (int) Math.rint(lastOrigin + tickStep * r * currentStep);
                        if ((firstPos <= axisOrigin) || (firstPos >= max)) {
                            firstLength--;
                        } else {
                            firstSubTicks = Arrays.copyOf(firstSubTicks, firstSubTicks.length + 1);
                            firstSubTicks[firstSubTicks.length - 1] = firstPos;
                        }
                        if ((lastPos <= axisOrigin) || (lastPos >= max)) {
                            lastLength--;
                        } else {
                            lastSubTicks = Arrays.copyOf(lastSubTicks, lastSubTicks.length + 1);
                            lastSubTicks[lastSubTicks.length - 1] = lastPos;
                        }
                    }
                    subTickLength = firstLength + lastLength + (size - 1) * subTickStep;
                } else {
                    subTickLength = 0;
                    r = 0;
                    firstLength = 0;
                    lastLength = 0;
                    firstOrigin = 0;
                }
                int[] tickPositions = new int[size];
                int[] subTickPositions = new int[subTickLength];
                int tickIndex = 0;
                int subTickIndex = 0;
                // First subTicks
                for (int subtick : firstSubTicks) {
                    subTickPositions[subTickIndex++] = subtick;
                }
                // Other subTicks
                for (AxisLabel label : labels) {
                    tickPositions[tickIndex++] = (int) Math.rint(label.getPos()) + axisOrigin;
                    if ((subTickLength > 0) && (label != lastLabel)) {
                        for (int currentStep = 1; currentStep < subTickStep; currentStep++) {
                            if (subTickIndex < subTickPositions.length) {
                                int position = (int) Math
                                        .rint(label.getPos() + axisOrigin + tickStep * r * currentStep);
                                if ((position < max) && (position >= axisOrigin)) {
                                    subTickPositions[subTickIndex++] = position;
                                }
                            }
                        }
                    }
                }
                // Last subTicks
                for (int subtick : lastSubTicks) {
                    subTickPositions[subTickIndex++] = subtick;
                }

                // Double check subticks to eliminate unset positions
                if (subTickPositions.length > 0) {
                    int start = 0, end = subTickPositions.length - 1;
                    while ((start < subTickPositions.length) && (subTickPositions[start] < axisOrigin)) {
                        start++;
                    }
                    while ((end > -1) && (subTickPositions[end] < axisOrigin)) {
                        end--;
                    }
                    if ((start > end) || (start >= subTickPositions.length) || (end < 0)) {
                        subTickPositions = new int[0];
                    } else {
                        end++;
                        boolean hole = false;
                        for (int i = start; (i < end) && (!hole); i++) {
                            hole = (subTickPositions[i] < axisOrigin);
                        }
                        if (hole) {
                            int index = 0;
                            int[] adaptedSubTicks = new int[end + 1 - start];
                            for (int i = start; i < end; i++) {
                                int subtick = subTickPositions[i];
                                if (subtick >= axisOrigin) {
                                    adaptedSubTicks[index] = subtick;
                                }
                                index++;
                            }
                            subTickPositions = Arrays.copyOf(adaptedSubTicks, index);
                        } else if ((start > 0) || (end < subTickPositions.length)) {
                            subTickPositions = Arrays.copyOfRange(subTickPositions, start, end);
                        }
                    }
                }
                tickMap.put(TickType.TICK, tickPositions);
                tickMap.put(TickType.SUB_TICK, subTickPositions);
            } // end if (size > 0)
        } // end if ((tickMap != null) && (labels != null) && (subTickStep > 0))
    }

    @Override
    protected void computeDefaultXScaleBounds() {
        if (isTimeScale()) {
            scaleMinimum = System.currentTimeMillis() - ChartUtils.HOUR;
            scaleMaximum = System.currentTimeMillis();
        } else {
            scaleMinimum = 0;
            scaleMaximum = 99.99;
        }
    }

    @Override
    protected void computeDefaultAutoScaleBounds() {
        scaleMinimum = 0;
        scaleMaximum = 99.99;
    }

    @Override
    protected double computeAutoScalePrecision() {
        return ChartUtils.computeLowTen(scaleMaximum - scaleMinimum);
    }

    @Override
    protected double getUserLabelPosition(int index) {
        return attributes.getUserLabelPos()[index];
    }

    @Override
    protected double[] computeLabelDataForValueAnnotation(int nbMaxLab, double scaleDelta, double precDelta,
            double length) {
        double extractLabel = 0;
        int step = 5;
        int subStep = 0;
        // The interval between two labels is calculated with the axis size
        double prec = scaleDelta / nbMaxLab;

        // We adjust prec with the wanted gap
        double space = LABEL_INTERVAL;
        if (prec <= 1) {
            space *= ChartUtils.computeLowTen(prec);
            // Avoid a division by 0
            if (Math.rint(prec / space) == 0) {
                space /= 10;
            }
        } else if (prec < space) {
            prec = space;
        }
        // Calculate the real prec
        prec = Math.rint(prec / space) * space;
        // Set the step for the tick calculation
        if (prec / 10 == (int) prec / 10) {
            step = 10;
        } else {
            step = 5;
        }

        // Round to multiple of prec (last not visible label)
        long round = (long) Math.floor(scaleMinimum / prec);
        double startx = round * prec;
        // Compute real number of label
        double sx = startx;
        int nbL = 0;
        double old;
        boolean infiniteLoop = false;
        double refMax = scaleMaximum + precDelta, refMin = scaleMinimum - precDelta;
        while ((sx <= refMax) && (!infiniteLoop)) {
            old = sx;
            if (sx >= refMin) {
                nbL++;
            }
            sx += prec;
            // Infinite loop detection (JAVAAPI-614)
            if (sx <= old || Double.isInfinite(refMin) || Double.isInfinite(refMax) || Double.isNaN(refMin)
                    || Double.isNaN(refMax) || Double.isNaN(sx)) {
                infiniteLoop = true;
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .trace("LinearScale.computeLabelDataForValueAnnotation(" + nbMaxLab + ", " + nbMaxLab + ", "
                                + precDelta + ", " + length
                                + "): infinite loop detected while computing real number of labels");
            }
        }

        if (nbL < 2) {
            // Only one label
            // Go backward and extract the 2 extremities
            if (step == 10) {
                step = 5;
                prec = prec / 2.0;
            } else {
                step = 10;
                prec = prec / 5.0;
            }
            extractLabel = 1;
        }
        // Compute tick spacing
        double tickSpacing = Math.abs(((prec / scaleDelta) * length) / step);
        subStep = step;
        int oldSubStep;
        while ((tickSpacing < 10.0) && (subStep > 1) && (!infiniteLoop)) {
            oldSubStep = subStep;
            old = tickSpacing;
            switch (subStep) {
                case 10:
                    subStep = 5;
                    tickSpacing *= 2;
                    break;
                case 5:
                    subStep = 2;
                    tickSpacing *= 2.5;
                    break;
                case 2:
                    // No sub tick
                    subStep = 1;
                    break;
            }
            // Infinite loop detection (JAVAAPI-614)
            if ((tickSpacing <= old) && (subStep <= oldSubStep)) {
                infiniteLoop = true;
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .trace("LinearScale.computeLabelDataForValueAnnotation(" + nbMaxLab + ", " + nbMaxLab + ", "
                                + precDelta + ", " + length
                                + "): infinite loop detected while computing real tick spacing");
            }
        }
        return new double[] { subStep, prec, startx, extractLabel };
    }

    @Override
    protected int computeAutoBarWidth(AbstractDataView v, AbstractDataView xView, int axisLength) {
        int barWidth;
        double minx = getScaleMinimum();
        double maxx = getScaleMaximum();
        double minI = Double.MAX_VALUE;
        if (xView == null) {
            // Look for the minimum interval
            DataList d = v.getData();
            if (d != null) {
                double x = d.getX();
                d = d.next;
                while (d != null) {
                    double diff = Math.abs(d.getX() - x);
                    if (diff < minI) {
                        minI = diff;
                    }
                    x = d.getX();
                    d = d.next;
                }
            }
        } else {
            // Look for the minimum interval
            DataList d = xView.getData();
            if (d != null) {
                double x = d.getY();
                d = d.next;
                while (d != null) {
                    double diff = Math.abs(d.getY() - x);
                    if (diff < minI) {
                        minI = diff;
                    }
                    x = d.getY();
                    d = d.next;
                }
            }
        }
        if (minI == Double.MAX_VALUE) {
            barWidth = DEFAULT_BAR_WIDTH;
        } else {
            barWidth = (int) Math.floor(minI / (maxx - minx) * axisLength) - 2;

            // Make width multiple of 2 and saturate
            barWidth = barWidth / 2;
            barWidth = barWidth * 2;
            if (barWidth < 0) {
                barWidth = 0;
            }
        }
        return barWidth;
    }

    @Override
    public int getZeroPosition(int axisOrigin, double axisLength) {
        return (int) (getScaleMinimum() / (getScaleMaximum() - getScaleMinimum()) * axisLength) + axisOrigin;
    }

}
