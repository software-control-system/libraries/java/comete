/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.plugin;

import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import ij.gui.Roi;
import ij.process.ImageProcessor;

/**
 * A {@link CometePlugInFilter} that selects the last created {@link Roi}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class RoiSelection extends CometePlugInFilter {

    public final static String COMMAND_NAME = "selection";

    @Override
    public boolean isSetupArgumentMandatory() {
        return false;
    }

    @Override
    public void run(ImageProcessor ip) {
        try {
            IMaskedImageViewer imageViewer = recoverImageContext();
            if (imageViewer != null) {
                IJRoiManager roiManager = imageViewer.getRoiManager();
                if (roiManager != null) {
                    roiManager.selectRoi(roiManager.getLastCreatedRoi());
                }
            }
        } catch (Exception ex) {
            getLogger().error("Failed to apply roi selection", ex);
        }
    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }
}
