/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;

public class ErrorNotificationDelegate {

    private static final Font LABEL_FONT = new Font(Font.DIALOG, Font.PLAIN, 12);
    private static final Font AREA_FONT = new Font(Font.DIALOG, Font.ITALIC, 11);
    private static final Font AREA_TITLE_FONT = new Font(Font.DIALOG, Font.BOLD | Font.ITALIC, 12);
    private static final String TITLE = "Error!";
    private static final String SCROLL_TITLE = "Error details";
    private static final String OK = "OK";
    private static final int MAX_WIDTH = 750, MAX_HEIGHT = 350;
    private static final ImageIcon ERROR_ICON = new ImageIcon(
            ErrorNotificationDelegate.class.getResource("/com/famfamfam/silk/exclamation.png"));

    private Component source;
    private JDialog messageDialog;
    private final JLabel messageLabel;
    private final JTextArea stackTraceArea;
    private final JScrollPane stackTraceAreaScrollPane;
    private final Object messageLock;

    public ErrorNotificationDelegate(Component source) {
        this.source = source;
        messageLabel = new JLabel(ERROR_ICON);
        messageLabel.setForeground(Color.RED);
        messageLabel.setFont(LABEL_FONT);
        messageLabel.setHorizontalAlignment(SwingConstants.LEFT);
        stackTraceArea = new JTextArea(10, 50);
        stackTraceArea.setFont(AREA_FONT);
        stackTraceArea.setLineWrap(true);
        stackTraceArea.setWrapStyleWord(true);
        stackTraceArea.setEditable(false);
        stackTraceArea.setOpaque(false);
        stackTraceAreaScrollPane = new JScrollPane(stackTraceArea);
        stackTraceAreaScrollPane.setBorder(new TitledBorder(null, SCROLL_TITLE, TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, AREA_TITLE_FONT));
        messageLock = new Object();
    }

    protected void hideMessageDialog() {
        messageDialog.setVisible(false);
        messageLabel.setText(ObjectUtils.EMPTY_STRING);
        stackTraceArea.setText(ObjectUtils.EMPTY_STRING);
    }

    public void notifyForError(final String message, final Throwable error) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (messageDialog == null) {
                    synchronized (messageLock) {
                        if (messageDialog == null) {
                            messageDialog = new JDialog(WindowSwingUtils.getWindowForComponent(source), TITLE,
                                    JDialog.DEFAULT_MODALITY_TYPE);
                            messageDialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                            messageDialog.addWindowListener(new WindowAdapter() {
                                @Override
                                public void windowClosing(java.awt.event.WindowEvent e) {
                                    hideMessageDialog();
                                }
                            });
                            JPanel mainPanel = new JPanel(new GridBagLayout());
                            JButton okButton = new JButton(OK);
                            okButton.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    hideMessageDialog();
                                }
                            });
                            GridBagConstraints messageLabelConstraints = new GridBagConstraints();
                            messageLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
                            messageLabelConstraints.gridx = 0;
                            messageLabelConstraints.gridy = 0;
                            messageLabelConstraints.weightx = 1;
                            messageLabelConstraints.weighty = 0;
                            messageLabelConstraints.insets = new Insets(10, 5, 10, 5);
                            messageLabelConstraints.anchor = GridBagConstraints.LINE_START;
                            mainPanel.add(messageLabel, messageLabelConstraints);
                            GridBagConstraints stackTraceAreaScrollPaneConstraints = new GridBagConstraints();
                            stackTraceAreaScrollPaneConstraints.fill = GridBagConstraints.BOTH;
                            stackTraceAreaScrollPaneConstraints.gridx = 0;
                            stackTraceAreaScrollPaneConstraints.gridy = 1;
                            stackTraceAreaScrollPaneConstraints.weightx = 1;
                            stackTraceAreaScrollPaneConstraints.weighty = 1;
                            mainPanel.add(stackTraceAreaScrollPane, stackTraceAreaScrollPaneConstraints);
                            GridBagConstraints okButtonConstraints = new GridBagConstraints();
                            okButtonConstraints.fill = GridBagConstraints.NONE;
                            okButtonConstraints.gridx = 0;
                            okButtonConstraints.gridy = 2;
                            okButtonConstraints.weightx = 1;
                            okButtonConstraints.weighty = 0;
                            okButtonConstraints.insets = new Insets(5, 5, 5, 5);
                            okButtonConstraints.anchor = GridBagConstraints.CENTER;
                            mainPanel.add(okButton, okButtonConstraints);

                            messageDialog.setContentPane(mainPanel);
                        }
                    }
                } // end if (messageDialog == null)
                messageLabel.setText(message == null ? ObjectUtils.EMPTY_STRING : message);
                String stackTrace = (error == null ? ObjectUtils.EMPTY_STRING : ObjectUtils.printStackTrace(error));
                stackTraceArea.setText(stackTrace);
                stackTraceAreaScrollPane.revalidate();
                if (!stackTrace.isEmpty()) {
                    stackTraceArea.setCaretPosition(0);
                }
                messageDialog.pack();
                int width = messageDialog.getWidth(), height = messageDialog.getHeight();
                boolean changed = false;
                if (width > MAX_WIDTH) {
                    Dimension prefSize = messageLabel.getPreferredSize();
                    width = Math.min(width, Math.max(prefSize.width + 15, MAX_WIDTH));
                    changed = (width != messageDialog.getWidth());
                }
                if (height > MAX_HEIGHT) {
                    height = MAX_HEIGHT;
                    changed = true;
                }
                if (changed) {
                    messageDialog.setSize(width, height);
                }
                messageDialog.setLocationRelativeTo(getSource());
                messageDialog.setVisible(true);
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    public Component getSource() {
        return source;
    }

    public void setSource(Component source) {
        this.source = source;
    }

}
