/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.data;

import fr.soleil.comete.swing.chart.util.ChartUtils;

/**
 * A class used to parse the first line in a file and to confirm whether that file is a CSV file.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ParsedLineInfo {

    private final String[] parsedLine;
    private final boolean csv;

    /**
     * Constructs a new {@link ParsedLineInfo}.
     * 
     * @param line The first read line in the file.
     * @param csv Whether the file has a csv extension.
     * @param nxExtractorFormat Whether the file follows NxExtractor format.
     */
    public ParsedLineInfo(String line, boolean csv, boolean nxExtractorFormat) {
        if (nxExtractorFormat) {
            // JAVAAPI-518: compatibility with NxExtractor format
            parsedLine = line.trim().replaceAll(ChartUtils.SPACES, ChartUtils.TAB)
                    .replaceAll(ChartUtils.TABS, ChartUtils.TAB).split(ChartUtils.TAB);
            this.csv = csv;
        } else {
            if (csv) {
                // CONTROLGUI-378: search for fake CSV case.
                if (line.indexOf(ChartUtils.CSV_SEPARATOR) < 0 && line.indexOf(ChartUtils.TAB) > -1) {
                    this.csv = false;
                } else {
                    this.csv = csv;
                }
            } else {
                this.csv = csv;
            }
            if (isCSV()) {
                parsedLine = line.split(ChartUtils.CSV_SEPARATOR);
            } else {
                parsedLine = line.split(ChartUtils.TAB);
            }
        }
    }

    /**
     * Returns the parsed first line, as a {@link String} array.
     * 
     * @return The parsed first line.
     */
    public String[] getParsedLine() {
        return parsedLine;
    }

    /**
     * Returns whether the file is really a csv file.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isCSV() {
        return csv;
    }

}