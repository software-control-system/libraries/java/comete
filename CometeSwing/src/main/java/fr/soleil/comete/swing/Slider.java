/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Dictionary;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.SliderUI;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.data.information.NumberInformation;
import fr.soleil.comete.definition.event.ValueConvertorEvent;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.listener.ISliderListener;
import fr.soleil.comete.definition.listener.IValueConvertorListener;
import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.comete.definition.widget.ISlider;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.EqualityContentTable;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateFormattable;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.math.NumberArrayUtils;
import fr.soleil.lib.project.swing.ui.ColoredTickMetalSliderUI;
import fr.soleil.lib.project.swing.ui.ToolTipSliderUI;

/**
 * Swing implementation of {@link ISlider}
 */
public class Slider extends JSlider implements ISlider, ChangeListener, IValueConvertorListener, IDateFormattable {

    private static final long serialVersionUID = -6588229312796622679L;

    // Default minimum distance in pixels between 2 ticks (in pixels) in auto scale mode
    private static final int DEFAULT_DISTANCE_BETWEEN_TICKS = 50;
    // Default maximum number of ticks in auto scale mode
    private static final int DEFAULT_MAXIMUM_TICKS = 20;
    // Default minimum number of ticks in auto scale mode
    private static final int DEFAULT_MINIMUM_TICKS = 2;

    private static final String DEFAULT_DATE_FORMAT = IDateFormattable.DATE_LONG_FORMAT + " "
            + IDateFormattable.HOUR_MINUTE_FORMAT;

    private SimpleDateFormat usedFormat;
    private final List<ISliderListener> sliderListeners;
    private final Map<String, SimpleDateFormat> dateFormatMap;
    private double step;
    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;
    private boolean autoToolTip;
    private IValueConvertor valueConvertor;
    private Color preferedMinorTickColor;
    private Color preferedMajorTickColor;
    private String format;
    private boolean formatOnlyConvertedValue;
    private double minimumDouble;
    private double maximumDouble;
    private double value;
    private boolean allowOutOfBoundsValues;
    private boolean autoScale;
    private boolean autoScaleOnMinorTicksToo;
    private final Object labelTableLock;
    private boolean canUpdateLabels;
    private boolean dateCompatibility;
    private String dateFormat;
    private int distanceBetweenTicks, minimumTickCount, maximumTickCount;
    // boolean used to avoid undesired events (detected during JAVAAPI-485)
    protected volatile boolean cancelEvents;

    public Slider() {
        super();
        cancelEvents = false;
        usedFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
        sliderListeners = new ArrayList<ISliderListener>();
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        step = 1;
        maximumDouble = 100;
        minimumDouble = 0;
        value = 0;
        preferedMinorTickColor = null;
        preferedMajorTickColor = null;
        autoToolTip = true;
        valueConvertor = null;
        formatOnlyConvertedValue = false;
        autoScale = true;
        autoScaleOnMinorTicksToo = true;
        labelTableLock = new Object();
        dateCompatibility = false;
        dateFormat = DEFAULT_DATE_FORMAT;
        dateFormatMap = new HashMap<String, SimpleDateFormat>();
        distanceBetweenTicks = DEFAULT_DISTANCE_BETWEEN_TICKS;
        minimumTickCount = DEFAULT_MINIMUM_TICKS;
        maximumTickCount = DEFAULT_MAXIMUM_TICKS;
        setUI(new ToolTipSliderUI());
        addChangeListener(this);
        canUpdateLabels = true;
        if (!dateFormatMap.containsKey(DEFAULT_DATE_FORMAT)) {
            dateFormatMap.put(DEFAULT_DATE_FORMAT, new SimpleDateFormat(DEFAULT_DATE_FORMAT));
        }
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                if (isAutoScale()) {
                    updateBounds();
                }
            }
        });
    }

    /**
     * Returns whether ticks spacing will be automatically calculated
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isAutoScale() {
        return autoScale;
    }

    /**
     * Sets whether ticks spacing should be automatically calculated
     * 
     * @param autoScale whether ticks spacing should be automatically calculated
     */
    public void setAutoScale(boolean autoScale) {
        if (this.autoScale != autoScale) {
            this.autoScale = autoScale;
            updateBounds();
        }
    }

    /**
     * Returns whether autoscale mode will change minor ticks spacing too
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isAutoScaleOnMinorTicksToo() {
        return autoScaleOnMinorTicksToo;
    }

    /**
     * Sets whether autoscale mode should change minor ticks spacing too. Will only work is
     * autoscale is activated (i.e. {@link #isAutoScale()} is <code>true</code>)
     * 
     * @param autoScaleOnMinorTicksToo Whether autoscale mode should change minor ticks spacing too
     * @see #isAutoScaleOnMinorTicksToo()
     * @see #setAutoScale(boolean)
     * @see #isAutoScale()
     */
    public void setAutoScaleOnMinorTicksToo(boolean autoScaleOnMinorTicksToo) {
        this.autoScaleOnMinorTicksToo = autoScaleOnMinorTicksToo;
    }

    /**
     * Returns the minimum distance, in pixels, between 2 major ticks.
     * 
     * @return An <code>int</code>.
     */
    public int getDistanceBetweenTicks() {
        return distanceBetweenTicks;
    }

    /**
     * Sets the minimum distance, in pixels, between 2 major ticks.
     * 
     * @param distanceBetweenTicks The minimum distance between 2 major ticks.
     */
    public void setDistanceBetweenTicks(int distanceBetweenTicks) {
        if ((distanceBetweenTicks > -1) && (this.distanceBetweenTicks != distanceBetweenTicks)) {
            this.distanceBetweenTicks = distanceBetweenTicks;
            if (isAutoScale()) {
                updateBounds();
            }
        }
    }

    /**
     * Returns the minimum number of major ticks that will be displayed in this {@link Slider}
     * 
     * @return An <code>int</code>.
     */
    public int getMinimumTickCount() {
        return minimumTickCount;
    }

    /**
     * Sets the minimum number of major ticks that should be displayed in this {@link Slider}
     * 
     * @param minimumTickCount The minimum number of major ticks that should be displayed in this {@link Slider}
     */
    public void setMinimumTickCount(int minimumTickCount) {
        if ((minimumTickCount > 1) && (this.minimumTickCount != minimumTickCount)) {
            this.minimumTickCount = minimumTickCount;
            if (isAutoScale()) {
                updateBounds();
            }
        }
    }

    /**
     * Returns the maximum number of major ticks that will be displayed in this {@link Slider}
     * 
     * @return An <code>int</code>.
     */
    public int getMaximumTickCount() {
        return maximumTickCount;
    }

    /**
     * Sets the maximum number of major ticks that should be displayed in this {@link Slider}
     * 
     * @param maximumTickCount The maximum number of major ticks that should be displayed in this {@link Slider}
     */
    public void setMaximumTickCount(int maximumTickCount) {
        if ((maximumTickCount > 1) && (this.maximumTickCount != maximumTickCount)) {
            this.maximumTickCount = maximumTickCount;
            if (isAutoScale()) {
                updateBounds();
            }
        }
    }

    @Override
    public void setMajorTickSpacing(final int space) {
        super.setMajorTickSpacing(space);
        updateLabels();
    }

    @Override
    public void setMinorTickSpacing(final int space) {
        super.setMinorTickSpacing(space);
        updateLabels();
    }

    /**
     * Returns whether this Slider uses format only when its {@link IValueConvertor} is set
     * 
     * @return A <code>boolean</code> value
     * @see #setFormatOnlyConvertedValue(boolean)
     * @see #setValueConvertor(IValueConvertor)
     * @see #getValueConvertor()
     */
    public boolean isFormatOnlyConvertedValue() {
        return formatOnlyConvertedValue;
    }

    /**
     * Sets whether this Slider should use format only when its {@link IValueConvertor} is set
     * 
     * @param formatOnlyConvertedValue A <code>boolean</code> value. <code>TRUE</code> to format
     *            only when there is an {@link IValueConvertor}
     * @see #setValueConvertor(IValueConvertor)
     * @see #getValueConvertor()
     */
    public void setFormatOnlyConvertedValue(boolean formatOnlyConvertedValue) {
        if (this.formatOnlyConvertedValue != formatOnlyConvertedValue) {
            this.formatOnlyConvertedValue = formatOnlyConvertedValue;
            updateLabels();
        }
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public void setFormat(String format) {
        if (!ObjectUtils.sameObject(format, this.format)) {
            this.format = format;
            updateLabels();
        }
    }

    /**
     * Returns the preferred color for minor ticks drawing
     * 
     * @return A {@link Color}
     */
    public Color getPreferedMinorTickColor() {
        return preferedMinorTickColor;
    }

    /**
     * Sets the preferred color for minor ticks drawing
     * 
     * @param preferedMinorTickColor The {@link Color} to use
     */
    public void setPreferedMinorTickColor(Color preferedMinorTickColor) {
        this.preferedMinorTickColor = preferedMinorTickColor;
        updateSliderUI();
    }

    /**
     * Returns the preferred color for major ticks drawing
     * 
     * @return A {@link Color}
     */
    public Color getPreferedMajorTickColor() {
        return preferedMajorTickColor;
    }

    /**
     * Sets the preferred color for major ticks drawing
     * 
     * @param preferedMajorTickColor The {@link Color} to use
     */
    public void setPreferedMajorTickColor(Color preferedMajorTickColor) {
        this.preferedMajorTickColor = preferedMajorTickColor;
        updateSliderUI();
    }

    @Override
    public void setUI(SliderUI ui) {
        super.setUI(ui);
        updateSliderUI();
    }

    protected void updateSliderUI() {
        SliderUI ui = getUI();
        if (ui instanceof ColoredTickMetalSliderUI) {
            ColoredTickMetalSliderUI coloredUI = (ColoredTickMetalSliderUI) ui;
            coloredUI.setCustomMajorTickColor(getPreferedMajorTickColor());
            coloredUI.setCustomMinorTickColor(getPreferedMinorTickColor());
        }
    }

    @Override
    public String getToolTipText() {
        String tooltip = null;
        if (autoToolTip) {
            Dictionary<?, ?> labels = getLabelTable();
            int index = getValue();
            if (labels != null) {
                Object obj = labels.get(index);
                if (obj instanceof JLabel) {
                    JLabel label = (JLabel) obj;
                    if (label.getToolTipText() == null) {
                        tooltip = label.getText();
                    } else {
                        tooltip = label.getToolTipText();
                    }
                }
            }
            if ((tooltip == null) || tooltip.trim().isEmpty()) {
                double value = getDoubleValueForIndex(index);
                tooltip = (valueConvertor == null) || !valueConvertor.isValid() ? basicToString(value)
                        : basicToString(valueConvertor.convertValue(value));
            }
        } else {
            tooltip = super.getToolTipText();
        }
        return tooltip;
    }

    @Override
    public void setToolTipText(String text) {
        autoToolTip = text == null;
        super.setToolTipText(text);
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(CometeFont font) {
        setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        setForeground(ColorTool.getColor(color));
    }

    @Override
    public int getHorizontalAlignment() {
        return 0;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        value = getMinimumDouble() + getStep() * (getValue() - getMinimum());
        if (!getValueIsAdjusting()) {
            // Detected during JAVAAPI-485: do not warn mediators while value is adjusting.
            warnMediators();
        }
        fireValueChanged(new EventObject(this));
    }

    @Override
    public void addSliderListener(final ISliderListener listener) {
        if (listener != null) {
            synchronized (sliderListeners) {
                if (!sliderListeners.contains(listener)) {
                    sliderListeners.add(listener);
                }
            }
        }
    }

    @Override
    public void removeSliderListener(final ISliderListener listener) {
        if (listener != null) {
            synchronized (sliderListeners) {
                sliderListeners.remove(listener);
            }
        }
    }

    @Override
    public void fireValueChanged(EventObject event) {
        Collection<ISliderListener> listeners;
        synchronized (sliderListeners) {
            listeners = new ArrayList<>(sliderListeners);
        }
        for (ISliderListener listener : listeners) {
            listener.valueChanged(event);
        }
        listeners.clear();
    }

    @Override
    public void setLabelTable(String[] labels, int[] values) {
        if ((values != null) && (values.length > 0)) {
            Dictionary<Integer, JLabel> dictionnary = new EqualityContentTable<Integer, JLabel>();
            Integer key = null;
            String label = null;
            for (int i = 0; i < values.length; i++) {
                key = values[i];
                label = key.toString();
                if ((labels != null) && (labels.length > i)) {
                    label = labels[i];
                }
                dictionnary.put(key, generateLabel(label));
            }
            setLabelTable(dictionnary);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setLabelTable(Dictionary labels) {
        synchronized (labelTableLock) {
            if (!ObjectUtils.sameObject(labels, getLabelTable())) {
                canUpdateLabels = ((labels == null) || (labels instanceof ValueLabelTable));
                super.setLabelTable(labels);
            }
        }
    }

    public boolean isDateCompatibility() {
        return dateCompatibility;
    }

    public void setDateCompatibility(boolean dateCompatibility) {
        this.dateCompatibility = dateCompatibility;
    }

    @Override
    public boolean isPaintLabels() {
        return getPaintLabels();
    }

    @Override
    public boolean isPaintTicks() {
        return getPaintTicks();
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Warns Mediators for some changes int this {@link Slider}
     */
    public void warnMediators() {
        warnMediators(new NumberInformation(this, getNumberValue()));
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public Number getNumberValue() {
        return Double.valueOf(value);
    }

    protected boolean isValidValue(double value) {
        boolean valid;
        if (Double.isNaN(value)) {
            valid = false;
        } else if (isAllowOutOfBoundsValues()) {
            valid = true;
        } else {
            valid = (value >= getMinimumDouble()) && (value <= getMaximumDouble());
        }
        return valid;
    }

    protected void updateValueWithoutListening() {
        synchronized (this) {
            removeChangeListener(this);
            updateValue();
            addChangeListener(this);
        }
    }

    protected void updateValue() {
        int index = NumberArrayUtils.getIndexInArray(value, getMinimumDouble(), getStep());
        if (index > -1) {
            index += getMinimum();
            if (index <= getMaximum()) {
                setValue(index);
            } else if (isAllowOutOfBoundsValues()) {
                setValue(getMaximum());
            }
        } else if (isAllowOutOfBoundsValues()) {
            setValue(getMinimum());
        }
    }

    @Override
    public void setNumberValue(Number value) {
        if (value == null) {
            value = MathConst.NAN_FOR_NULL;
        }
        double val = value.doubleValue();
        if (isValidValue(val)) {
            this.value = val;
            updateValueWithoutListening();
        }
    }

    @Override
    public boolean isEditingData() {
        return (!cancelEvents) && getValueIsAdjusting();
    }

    public IValueConvertor getValueConvertor() {
        return valueConvertor;
    }

    public void setValueConvertor(IValueConvertor valueConvertor) {
        if (this.valueConvertor != null) {
            this.valueConvertor.removeValueConvertorListener(this);
        }
        this.valueConvertor = valueConvertor;
        if (this.valueConvertor != null) {
            this.valueConvertor.addValueConvertorListener(this);
        }
        updateLabels();
    }

    protected void updateLabels() {
        if (canUpdateLabels && (getMajorTickSpacing() > 0)) {
            setLabelTable(createStandardLabels(getMajorTickSpacing()));
        }
    }

    @Override
    public Hashtable<?, ?> createStandardLabels(int increment, int start) {
        if ((start > getMaximum()) || (start < getMinimum())) {
            throw new IllegalArgumentException("Slider label start point out of range.");
        }
        if (increment <= 0) {
            throw new IllegalArgumentException("Label incremement must be > 0");
        }
        return new ValueLabelTable(increment, start);
    }

    @Override
    public void convertorChanged(ValueConvertorEvent event) {
        if ((event != null) && (event.getSource() == valueConvertor)) {
            updateLabels();
        }
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    protected void updateLabelUIs() {
        // XXX deactivated due to AWT dead locks.
        // Anyway, labels size is calculated on creation
    }

    /**
     * Generates a new {@link JLabel} with a given text, and sets its size
     * 
     * @param text The {@link JLabel}'s text
     * @return A {@link JLabel}
     */
    protected JLabel generateLabel(String text) {
        JLabel label = new TextEqualityLabel(text);
        label.setSize(label.getPreferredSize());
        return label;
    }

    private int getNbOfTicks() {
        int nbTick = getMaximum();
        if (step != 0) {
            nbTick = (int) Math.ceil(Math.abs((maximumDouble - minimumDouble) / step));
        }
        return nbTick;
    }

    private double getDoubleValueForIndex(int index) {
        double value = index * step + minimumDouble;
        return value;
    }

    @Override
    public double getDoubleValue() {
        int index = super.getValue();
        double value = getDoubleValueForIndex(index);
        return value;
    }

    @Override
    public void setDoubleValue(double value) {
        int index = super.getValue();
        if (step > 0) {
            int newIndex = Double.valueOf(((value - minimumDouble) / step)).intValue();
            if (newIndex != index) {
                setValue(newIndex);
            }
        }
    }

    @Override
    public double getStep() {
        return step;
    }

    @Override
    public void setStep(double astep) {
        step = astep;
        updateBounds();
    }

    @Override
    public double getMinimumDouble() {
        return minimumDouble;
    }

    @Override
    public void setMinimumDouble(double min) {
        minimumDouble = min;
        updateBounds();
    }

    @Override
    public double getMaximumDouble() {
        return maximumDouble;
    }

    @Override
    public void setMaximumDouble(double max) {
        maximumDouble = max;
        updateBounds();
    }

    /**
     * Returns whether this {@link Slider} will allow out of bounds values.
     * 
     * @return A <code>boolean</code>. <code>false</code> by default.
     */
    public boolean isAllowOutOfBoundsValues() {
        return allowOutOfBoundsValues;
    }

    /**
     * Sets whether this {@link Slider} should allow out of bounds values.
     * 
     * @param allowOutOfBoundsValues
     */
    public void setAllowOutOfBoundsValues(boolean allowOutOfBoundsValues) {
        this.allowOutOfBoundsValues = allowOutOfBoundsValues;
    }

    protected void updateBounds() {
        synchronized (this) {
            removeChangeListener(this);
            setMaximum(getNbOfTicks());
            if (isAutoScale()) {
                int width = getWidth();
                double tickCount = width / (double) distanceBetweenTicks;
                if (tickCount > maximumTickCount) {
                    tickCount = maximumTickCount;
                }
                if (tickCount < minimumTickCount) {
                    tickCount = minimumTickCount;
                }
                int spacing = Math.max(1, (int) Math.ceil(Math.abs((getMaximum() - getMinimum()) / tickCount)));
                if (isAutoScaleOnMinorTicksToo()) {
                    if (spacing == 1) {
                        setMinorTickSpacing(0);
                    } else {
                        setMinorTickSpacing(1);
                    }
                }
                setMajorTickSpacing(spacing);
                updateValue();
            } else {
                updateLabels();
            }
            addChangeListener(this);
        } // end synchronized (this)
        repaint();
    }

    @Override
    public void setEditable(boolean state) {
        super.setEnabled(state);
    }

    @Override
    public boolean isEditable() {
        return super.isEnabled();
    }

    private boolean isADate(double value) {
        return value >= IDateFormattable.REFERENCE_TIME;
    }

    protected String basicToString(double value) {
        String result = null;
        if (dateCompatibility && isADate(value)) {
            result = usedFormat.format(new Date(Double.valueOf(value).longValue()));
        } else {
            if (value == Math.round(value)) {
                result = Format.formatValue(value, "%d");
            } else {
                result = Double.toString(value);
            }
        }
        return result;
    }

    @Override
    public String getDateFormat() {
        return dateFormat;
    }

    @Override
    public void setDateFormat(String dateText) {
        if (!ObjectUtils.sameObject(dateFormat, dateText)) {
            dateFormat = dateText;
            if ((dateText != null) && (!dateText.trim().isEmpty())) {
                SimpleDateFormat usedFormat = dateFormatMap.get(dateText);
                if (usedFormat == null) {
                    usedFormat = new SimpleDateFormat(dateText);
                    dateFormatMap.put(dateText, usedFormat);
                }
                this.usedFormat = usedFormat;
            } else {
                usedFormat = dateFormatMap.get(DEFAULT_DATE_FORMAT);
            }
            repaint();
        }
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        // Detected during JAVAAPI-485:
        // When notifying for error, avoid any value update event from SliderUI.
        // To do so, consider value is adjusting during error notification.
        cancelEvents = true;
        setValueIsAdjusting(true);
        errorNotificationDelegate.notifyForError(message, error);
        cancelEvents = false;
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

    @Override
    protected void processMouseEvent(MouseEvent e) {
        // Detected during JAVAAPI-485:
        // When notifying for error, avoid any value update event from SliderUI.
        // To do so, consider value is adjusting during error notification.
        if (!cancelEvents) {
            super.processMouseEvent(e);
        }
    }

    @Override
    public void setValueIsAdjusting(boolean b) {
        // Detected during JAVAAPI-485:
        // When notifying for error, avoid any value update event from SliderUI.
        // To do so, consider value is adjusting during error notification.
        if ((!cancelEvents) || b) {
            super.setValueIsAdjusting(b);
        }
    }

    @Override
    protected void processKeyEvent(KeyEvent e) {
        // Detected during JAVAAPI-485:
        // When notifying for error, avoid any value update event from SliderUI.
        // To do so, consider value is adjusting during error notification.
        // However, This should not cancel key released events.
        cancelEvents = false;
        if ((!e.isConsumed()) && (e.getID() == KeyEvent.KEY_RELEASED)
                && ((e.getKeyCode() == KeyEvent.VK_UP) || (e.getKeyCode() == KeyEvent.VK_DOWN)
                        || (e.getKeyCode() == KeyEvent.VK_LEFT) || (e.getKeyCode() == KeyEvent.VK_RIGHT)
                        || (e.getKeyCode() == KeyEvent.VK_EQUALS))) {
            setValueIsAdjusting(false);
        }
        super.processKeyEvent(e);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link JLabel} that only uses its text for equality comparison
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class TextEqualityLabel extends JLabel {

        private static final long serialVersionUID = 3923813448094484130L;

        public TextEqualityLabel() {
            super();
        }

        public TextEqualityLabel(Icon image, int horizontalAlignment) {
            super(image, horizontalAlignment);
        }

        public TextEqualityLabel(Icon image) {
            super(image);
        }

        public TextEqualityLabel(String text, Icon icon, int horizontalAlignment) {
            super(text, icon, horizontalAlignment);
        }

        public TextEqualityLabel(String text, int horizontalAlignment) {
            super(text, horizontalAlignment);
        }

        public TextEqualityLabel(String text) {
            super(text);
        }

        // XXX hack to avoid label displaying "..." instead of its value (JAVAAPI-302)
        @Override
        public int getWidth() {
            Dimension size = getPreferredSize();
            return size == null ? super.getWidth() : size.width;
        }

        // XXX hack to avoid label displaying "..." instead of its value (JAVAAPI-302)
        @Override
        public int getHeight() {
            Dimension size = getPreferredSize();
            return size == null ? super.getHeight() : size.height;
        }

        @Override
        public boolean equals(Object obj) {
            boolean equals;
            if (obj == null) {
                equals = false;
            } else if (obj == this) {
                equals = false;
            } else if (getClass().equals(obj.getClass())) {
                equals = ObjectUtils.sameObject(getText(), ((TextEqualityLabel) obj).getText());
            } else {
                equals = false;
            }
            return equals;
        }

        @Override
        public int hashCode() {
            int code = 0x7E47;
            int mult = 0x1ABE1;
            code = code * mult + getClass().hashCode();
            code = code * mult + ObjectUtils.getHashCode(getText());
            return code;
        }

    }

    protected class ValueLabel extends TextEqualityLabel {

        private static final long serialVersionUID = -4739103525332918075L;

        protected int index;

        public ValueLabel(int index) {
            super();
            setHorizontalAlignment(CENTER);
            this.index = index;
            setName("Slider.label");
            setSize(getPreferredSize());
        }

        @Override
        public String getToolTipText() {
            double value = getDoubleValueForIndex(index);
            return (valueConvertor == null) || !valueConvertor.isValid() ? basicToString(value)
                    : basicToString(valueConvertor.convertValue(value));
        }

        @Override
        public String getText() {
            String text;
            double value = getDoubleValueForIndex(index);
            if ((valueConvertor == null) || !valueConvertor.isValid()) {
                if (isFormatOnlyConvertedValue() || (format == null) || (!Format.isNumberFormatOK(format))) {
                    text = basicToString(value);
                } else {
                    text = Format.format(Locale.US, format, value);
                }
            } else {
                double newValue = valueConvertor.convertValue(value);
                if ((format == null) || !Format.isNumberFormatOK(format)) {
                    text = basicToString(newValue);
                } else {
                    text = Format.format(Locale.US, format, newValue);
                }
            }
            return text;
        }
    }

    // copy of class SmartHashtable in JSlider.createStandardLabels method
    protected class ValueLabelTable extends EqualityContentTable<Integer, JLabel> {

        private static final long serialVersionUID = 7016390574931321949L;

        protected int increment = 0;
        protected int start = 0;
        protected boolean startAtMin = false;

        public ValueLabelTable(int increment, int start) {
            super();
            this.increment = increment;
            this.start = start;
            startAtMin = (start == getMinimum());
            createLabels();
        }

        protected void createLabels() {
            int labelIndex = start;
            for (labelIndex = start; labelIndex <= getMaximum(); labelIndex += increment) {
                put(Integer.valueOf(labelIndex), new ValueLabel(labelIndex));
            }
            if ((getMaximum() >= start) && (labelIndex != getMaximum() + increment)) {
                put(Integer.valueOf(getMaximum()), new ValueLabel(getMaximum()));
            }
        }
    }
}
