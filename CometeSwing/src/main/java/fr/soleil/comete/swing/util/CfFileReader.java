/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;

import fr.soleil.data.mediator.Mediator;

/**
 * A class to parse configuration file
 * 
 * @author JL Pons
 */
public class CfFileReader implements CometeConstants {

    private static final String SEPARATOR = ":";

    private static final String CF_FILE_READER_PARSE_IS_MISSING = "CfFileReader.parse: '\'' is missing";
    private static final String CF_FILE_READER_PARSE_EXPECTED_INSTEAD_OF = "CfFileReader.parse: ':' expected instead of ";

    // ----------------------------------------------------
    // Class variable
    // ----------------------------------------------------
    protected List<Item> prop;
    protected FileReader file;
    protected String cfStr, filePath;
    protected char currentChar;
    protected BufferedReader stream;
    protected boolean whiteSpaceAsSeparator;

    // ----------------------------------------------------
    // General constructor
    // ----------------------------------------------------
    public CfFileReader() {
        whiteSpaceAsSeparator = true;
        prop = new ArrayList<>();
        cfStr = null;
        file = null;
        filePath = null;
        stream = null;
    }

    /**
     * @return the whiteSpaceAsSeparator
     */
    public boolean isWhiteSpaceAsSeparator() {
        return whiteSpaceAsSeparator;
    }

    /**
     * @param whiteSpaceAsSeparator
     *            the whiteSpaceAsSeparator to set
     */
    public void setWhiteSpaceAsSeparator(boolean whiteSpaceAsSeparator) {
        this.whiteSpaceAsSeparator = whiteSpaceAsSeparator;
    }

    /**
     * Returns the path of the last read file.
     * 
     * @return A {@link String}.
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Get the current char
     * 
     * @return The current char
     * @throws IOException
     *             If a read error occurred
     */
    protected char getCurrentChar() throws IOException {
        char c = 0;
        if (file != null) {
            c = (char) file.read();
        } else if (cfStr != null) {
            c = cfStr.charAt(0);
            cfStr = cfStr.substring(1);
        } else if (stream != null) {
            c = (char) stream.read();
        }
        return c;
    }

    /**
     * Return <code>true</code> when EOF
     * 
     * @return A <code>boolean</code>
     * @throws IOException in case of read error
     */
    protected boolean eof() throws IOException {
        boolean eof = true;
        if (file != null) {
            eof = (!file.ready());
        } else if (cfStr != null) {
            eof = (cfStr.length() == 0);
        } else if (stream != null) {
            eof = (!stream.ready());
        }
        return eof;
    }

    /**
     * Read the file word by word
     * 
     * @return The file content
     * @throws IOException If a read error occurred
     */
    protected String readWord() throws IOException {
        boolean found = (currentChar > 32);
        StringBuilder ret = new StringBuilder();
        // Jump space
        while (!eof() && !found) {
            currentChar = getCurrentChar();
            found = (currentChar > 32);
        }
        if (found) {
            // Treat strings
            if (currentChar == '\'') {
                found = false;
                while (!eof() && !found) {
                    currentChar = getCurrentChar();
                    found = (currentChar == '\'');
                    if (!found) {
                        ret.append(currentChar);
                    }
                }
                if (found) {
                    // System.out.println("ReadWord:" + ret);
                    currentChar = getCurrentChar();
                } else {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn(CF_FILE_READER_PARSE_IS_MISSING);
                    ret = null;
                }

            } else {
                // Read the next word
                ret.append(currentChar);
                if ((currentChar == ',') || (currentChar == ':')) {
                    currentChar = getCurrentChar();
                    // System.out.println("ReadWord:" + ret);
                } else {
                    found = false;
                    while (!eof() && !found) {
                        currentChar = getCurrentChar();
                        found = (currentChar == ',') || (currentChar == ':') || (currentChar < 32);
                        if (isWhiteSpaceAsSeparator() && (!found)) {
                            found = (currentChar == 32);
                        }
                        if (!found) {
                            ret.append(currentChar);
                        }
                    }
                    // System.out.println("ReadWord:" + ret);
                }
            }
        } else {
            ret = null;
        }
        return ret == null ? null : ret.toString();
    }

    /**
     * Reads the configuration file and fills properties vector
     * 
     * @return <code>true</code> when successfully browsed
     * @throws IOException If a read error occurred
     */
    protected boolean parse() throws IOException {
        boolean parsed = true;
        prop.clear();
        currentChar = 0;
        String word = readWord();
        boolean sameItem;
        while (word != null) {
            // Create new item
            Item it = new Item(word);
            // Jump ':'
            word = readWord();
            if (!SEPARATOR.equals(word)) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).warn(CF_FILE_READER_PARSE_EXPECTED_INSTEAD_OF + word);
                parsed = false;
                break;
            }
            // Read values
            sameItem = true;
            while (sameItem) {
                sameItem = false;
                word = readWord();
                if (word != null) {
                    it.addProp(word);
                }
                word = readWord();
                if (word != null) {
                    sameItem = word.equals(COMMA);
                }
            }
            prop.add(it);
        }
        return parsed;
    }

    /**
     * Parses the given string and fill property vector.
     * 
     * @param text String containing text to parse
     * @return Return true when text successfully parsed
     */

    public boolean parseText(String text) {
        boolean ok = false;
        try {
            cfStr = text;
            ok = parse();
        } catch (Exception e) {
            ok = false;
        }
        return ok;
    }

    /**
     * Parses the given file and fill property vector.
     * 
     * @param filename File to parse
     * @return Return true when file successfully parsed
     */

    public boolean readFile(String filename) {
        boolean ok = false;
        boolean parsed = false;
        try {
            file = new FileReader(filename);
            filePath = filename;
            ok = parse();
            parsed = true;
            file.close();
        } catch (Exception e) {
            if (!parsed) {
                ok = false;
            }
        }
        return ok;
    }

    /**
     * Parses the given file and fill property vector.
     * 
     * @param filename File to parse
     * @return Return true when file successfully parsed
     */

    public boolean readFile(File file) {
        boolean ok = false;
        boolean parsed = false;
        try {
            this.file = new FileReader(file);
            filePath = file.getAbsolutePath();
            ok = parse();
            parsed = true;
            this.file.close();
        } catch (Exception e) {
            if (!parsed) {
                ok = false;
            }
        }
        return ok;
    }

    public boolean readStream(BufferedReader stream) {
        boolean ok = false;
        boolean parsed = false;
        try {
            this.stream = stream;
            ok = parse();
            parsed = true;
            this.stream.close();
        } catch (Exception e) {
            if (!parsed) {
                ok = false;
            }
        }
        return ok;
    }

    /**
     * Returns all parameter names found in the config file.
     * 
     * @return Returns a vector of String.
     */
    public List<String> getNames() {
        List<String> v = new ArrayList<>();
        for (Item item : prop) {
            v.add(item.toString());
        }
        return v;
    }

    /**
     * Returns parameter value, one parameter can have multiple fields separated by a colon.
     * 
     * @param name Parameter name
     * @return Returns a list of String. (1 string per field)
     */
    public List<String> getParam(String name) {
        List<String> param = null;
        for (Item item : prop) {
            if (name.equals(item.toString())) {
                param = item.getItems();
                break;
            }
        }
        return param;
    }

    public static void main(String args[]) {
        final CfFileReader cf = new CfFileReader();
        if (cf.readFile("test.cfg")) {
            List<String> names = cf.getNames();
            System.out.println("Read " + names.size() + " params");
            for (int i = 0; i < names.size(); i++) {
                System.out.println(names.get(i).toString());
                List<String> values = cf.getParam(names.get(i).toString());
                for (int j = 0; j < values.size(); j++) {
                    System.out.println("   " + values.get(j).toString());
                }
            }
        } else {
            System.out.println("Error while reading config file");
        }
    }

    // ///////////// //
    // Inner Classes //
    // ///////////// //

    /**
     * Inner class Item. Handles one property in the config file
     */
    protected class Item {

        private List<String> items;
        private String name;

        public Item(String name) {
            items = new ArrayList<>();
            this.name = name;
        }

        public void addProp(String value) {
            items.add(value);
        }

        public List<String> getItems() {
            return items;
        }

        @Override
        public String toString() {
            return name;
        }

    }

}
