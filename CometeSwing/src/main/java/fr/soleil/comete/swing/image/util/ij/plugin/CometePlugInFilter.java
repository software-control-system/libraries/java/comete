/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.plugin;

import java.lang.ref.WeakReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.util.ij.CometeIJMathTool;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;
import ij.ImagePlus;
import ij.Macro;
import ij.gui.ImageCanvas;
import ij.plugin.filter.PlugInFilter;

/**
 * A {@link PlugInFilter} that can be identified by a command
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class CometePlugInFilter implements PlugInFilter {

    // Whether to allow the call to Macro.getOptions() in setup method
    private static boolean allowSettingArgumentThroughMacroOptions = true;

    /**
     * Return whether {@link CometePlugInFilter}s are allowed to search for a potential argument in
     * {@link Macro#getOptions()} when argument is <code>null</code> or empty in {@link #setup(String, ImagePlus)}.
     * 
     * @return A <code>boolean</code>. <code>true</code> by default.
     */
    public static boolean isAllowSettingArgumentThroughMacroOptions() {
        return allowSettingArgumentThroughMacroOptions;
    }

    /**
     * Sets whether {@link CometePlugInFilter}s are allowed to search for a potential argument in
     * {@link Macro#getOptions()} when argument is <code>null</code> or empty in {@link #setup(String, ImagePlus)}.
     * 
     * @param allowSettingArgumentThroughMacroOptions Whether {@link CometePlugInFilter}s are allowed to search for a
     *            potential argument in {@link Macro#getOptions()} when argument is <code>null</code> or empty in
     *            {@link #setup(String, ImagePlus)}.
     */
    public static void setAllowSettingArgumentThroughMacroOptions(boolean allowSettingArgumentThroughMacroOptions) {
        CometePlugInFilter.allowSettingArgumentThroughMacroOptions = allowSettingArgumentThroughMacroOptions;
    }

    /**
     * This plugin's last argument
     */
    protected String argument;

    /**
     * The {@link IJCanvas} that called this plugin
     */
    private WeakReference<IJCanvas> canvasRef;

    /**
     * Returns the command that identifies this plugin
     * 
     * @return A {@link String}
     */
    public abstract String getCommandName();

    protected IJCanvas getImageCanvas() {
        return ObjectUtils.recoverObject(canvasRef);
    }

    /**
     * Returns whether the first argument is mandatory (i.e. must'nt be null or empty) in
     * {@link #setup(String, ImagePlus)}
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isSetupArgumentMandatory() {
        return true;
    }

    @Override
    public int setup(String arg, ImagePlus imagePlus) {
        int baseCapabilities;
        try {
            if (isSetupArgumentMandatory() && isAllowSettingArgumentThroughMacroOptions()
                    && ((arg == null) || arg.trim().isEmpty())) {
                argument = Macro.getOptions();
            } else {
                argument = arg;
            }
            if (argument != null) {
                argument = argument.trim();
            }
            canvasRef = null;
            if (imagePlus != null) {
                imagePlus.unlock();
                ImageCanvas tempCanvas = imagePlus.getCanvas();
                if (tempCanvas instanceof IJCanvas) {
                    canvasRef = new WeakReference<>((IJCanvas) tempCanvas);
                }
            }
            baseCapabilities = DOES_ALL;
        } catch (Exception ex) {
            getLogger().error(getClass().getName() + " failed to setup", ex);
            baseCapabilities = 0;
        }
        return baseCapabilities;
    }

    /**
     * Recovers the {@link IMaskedImageViewer} that called this plugin
     * 
     * @return An {@link IMaskedImageViewer}
     */
    protected IMaskedImageViewer recoverImageContext() {
        return recoverImageViewer();
    }

    protected Logger getLogger() {
        Logger logger;
        try {
            String applicationId;
            IMaskedImageViewer viewer = recoverImageContext();
            applicationId = viewer == null ? null : viewer.getApplicationId();
            if ((applicationId == null) || applicationId.isEmpty()) {
                logger = LoggerFactory.getLogger(Mediator.LOGGER_ACCESS);
            } else {
                logger = LoggerFactory.getLogger(applicationId);
            }
        } catch (Exception e) {
            logger = LoggerFactory.getLogger(Mediator.LOGGER_ACCESS);
        }
        return logger;
    }

    /**
     * Recovers the {@link ImageViewer} that called this plugin
     * 
     * @return An {@link ImageViewer}
     */
    public ImageViewer recoverImageViewer() {
        ImageViewer result = null;
        IJCanvas canvas = getImageCanvas();
        if (canvas != null) {
            result = canvas.getImageViewer();
        }
        return result;
    }

    /**
     * Calculates the masked matrix
     * 
     * @return A <code>double[][]</code>
     */
    protected double[][] getMatriceValuesFromROI() {
        return CometeIJMathTool.getMatriceValuesFromROI(recoverImageContext());
    }

    /**
     * Calculates the masked flat matrix
     * 
     * @return An {@link AbstractNumberMatrix}
     */
    protected AbstractNumberMatrix<Double> getFlatMatrixValuesFromROI() {
        return CometeIJMathTool.getFlatMatrixValuesFromROI(recoverImageContext());
    }

}
