/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer;

import java.awt.Image;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JTable;
import javax.swing.event.EventListenerList;
import javax.swing.event.UndoableEditListener;
import javax.swing.table.TableModel;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CompoundEdit;
import javax.swing.undo.UndoableEditSupport;

import org.apache.commons.lang3.StringUtils;

import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.events.IRoiManagerListener;
import fr.soleil.comete.swing.image.ijviewer.events.IRoiModeListener;
import fr.soleil.comete.swing.image.ijviewer.events.IRoiSelectionListener;
import fr.soleil.comete.swing.image.ijviewer.events.RoiEvent;
import fr.soleil.comete.swing.image.ijviewer.hidden.HiddenImageWindow;
import fr.soleil.comete.swing.image.ijviewer.undo.AddRoiEdit;
import fr.soleil.comete.swing.image.ijviewer.undo.CombineRoiEdit;
import fr.soleil.comete.swing.image.ijviewer.undo.DeleteRoiEdit;
import fr.soleil.comete.swing.image.util.ij.roi.CircleRoi;
import fr.soleil.comete.swing.image.util.ij.roi.ManagerDependentRoiGenerator;
import fr.soleil.comete.swing.image.util.ij.roi.RoiGenerator;
import fr.soleil.lib.project.ObjectUtils;
import ij.ImagePlus;
import ij.gui.FreehandRoi;
import ij.gui.ImageWindow;
import ij.gui.Line;
import ij.gui.OvalRoi;
import ij.gui.PointRoi;
import ij.gui.PolygonRoi;
import ij.gui.Roi;
import ij.gui.ShapeRoi;
import ij.gui.TextRoi;
import ij.gui.Toolbar;
import ij.process.ColorProcessor;

// TODO undo/redo mechanism shows up some limitation with single click ROI creation: ROI is created and deleted, so we can undo the delete and have a 0-sized ROI we can't do anything with.
// TODO Check why ROI combination (and, or, not, xor) does not work  well with Line and Point ROIs
// XXX Roi.equals returns true for 2 ROIs of the same type, dimensions and position. This becomes a problem because IJ can't differentiate 2 Rois that perfectly match each other shape, and that are above each other.
// TODO Is it useful to do a "Not" on many ROIs ? Ok for other operations.
// TODO Maybe think about a way to move Rois through Roi List, in order to select a Roi under another one...
/**
 * This class provides a ROI support extended to multiple ROIs, with a selection
 * state. It basically behaves like a ROI model. Initially, IJ only manages a
 * single ROI, the handled ROI. It can be modified when the mouse is over. We
 * introduce a list to manage multiple ROIs, and a list for selected ROIs. We
 * can fool IJ by changing on the fly its handled ROI, according to the position
 * of the mouse (see ImageCanvas). We also introduce notifications for creation,
 * deletion, selection and deselection.
 * 
 * Selection mode is a modified rectangular ROI mode.
 * 
 * @author MAINGUY, Rapha&euml;l GIRARDOT
 */
public class IJRoiManager extends ImagePlus implements IRoiModeListener, Cloneable {

    /**
     * This is used to use the same code for all operations
     */
    public enum RoiOperation {
        OR, AND, NOT, XOR
    }

    /**
     * This is used to distinguish standard IJ modes from specific ones
     */
    public static final int CLASSIC_ROI_MODE = 0;
    public static final int SELECTION_ROI_MODE = 1;
    public static final int ZOOM_ROI_MODE = 2;
    public static final int LINE_PROFILE_ROI_MODE = 3;
    public static final int CALIBRATION_ROI_MODE = 4;
    public static final int FOCUS_ROI_MODE = 5;
    public static final int DUAL_PROFILE_ROI_MODE = 6;
    public static final int HISTOGRAM_ROI_MODE = 7;

    protected static final int CASE_ADD = 0;
    protected static final int CASE_DELETE = 1;

    // This set is used to be sure that Roi modes are transmitted to all IJRoiManagers
    // (so that actions that should interact together in ImageViewer do it the right way)
    protected static final Collection<IRoiModeListener> ROI_MODE_LISTENERS = Collections
            .newSetFromMap(new WeakHashMap<IRoiModeListener, Boolean>());

    /**
     * The current roi mode
     */
    protected static int roiMode = CLASSIC_ROI_MODE;
    protected static int storedRoiMode = CLASSIC_ROI_MODE;

    /**
     * List containing all ROIs. It also contains the selection ROI.
     */
    protected List<Roi> allRois;

    /**
     * List containing selected Rois
     */
    protected List<Roi> selectedRois;

    /**
     * The list of ROI listeners
     */
    protected EventListenerList listenerList;

    /**
     * List containing all interior {@link Roi}s
     */
    protected List<Roi> innerRois;

    /**
     * List containing all exterior {@link Roi}s
     */
    protected List<Roi> outerRois;

    /**
     * The only selected interior/exterior {@link Roi}s
     */
    protected Roi validatedRoi;

    protected boolean lockValidatedRois;

    /**
     * This is used to avoid firing add and delete events when drawing the
     * rectangular selection ROI on selection mode (external stands for
     * operation that doesn't depend on IJ's mode)
     */
    protected boolean external = false;

    /**
     * Manages undoable edit listeners
     */
    protected UndoableEditSupport undoSupport;
    protected CompoundEdit multipleEdit;

    /**
     * this is used to set a name for a new roi
     */
    protected int cpt;

    protected boolean singleRoiMode;
    protected Object windowLock;

    protected ConcurrentHashMap<Integer, RoiGenerator> roiGenerators;
    protected ConcurrentHashMap<RoiGenerator, Roi> specialRois;

    protected WeakReference<IMaskedImageViewer> imageViewerReference;

    public IJRoiManager() {
        super();
        listenerList = new EventListenerList();
        allRois = new ArrayList<>();
        selectedRois = new ArrayList<>();
        innerRois = new ArrayList<>();
        outerRois = new ArrayList<>();
        validatedRoi = null;
        lockValidatedRois = true;
        cpt = 1;
        singleRoiMode = false;
        windowLock = new Object();
        roiGenerators = new ConcurrentHashMap<Integer, RoiGenerator>();
        specialRois = new ConcurrentHashMap<RoiGenerator, Roi>();

        // undo management
        undoSupport = new UndoableEditSupport();
        addRoiModeListener(this);
        imageViewerReference = null;

        // Initialize with an empty ColorProcessor, so that plugins can work when no image is loaded
        setProcessor(new ColorProcessor(0, 0));
    }

    // no synchronized to avoid efficiency problems (DATAREDUC-881)
    @Override
    public void unlock() {
        locked = false;
    }

    // no synchronized to avoid efficiency problems (DATAREDUC-881)
    @Override
    public boolean lock() {
        locked = true;
        return true;
    }

    // no synchronized to avoid efficiency problems (DATAREDUC-881)
    @Override
    public boolean lockSilently() {
        locked = true;
        return true;
    }

    public IMaskedImageViewer getImageViewer() {
        return ObjectUtils.recoverObject(imageViewerReference);
    }

    public void setImageViewer(IMaskedImageViewer imageViewer) {
        if (imageViewer == null) {
            imageViewerReference = null;
        } else {
            imageViewerReference = new WeakReference<IMaskedImageViewer>(imageViewer);
        }
    }

    /**
     * Called when creating a new ROI. We add the ROI to our list. We don't kill
     * previous existing ROI, we are in a multi-ROIs mode.
     * 
     * @see ij.ImagePlus#createNewRoi(int, int)
     */
    @Override
    public void createNewRoi(int sx, int sy) {
        Roi roi = null;
        RoiGenerator roiGenerator = null;
        if (roiMode == CLASSIC_ROI_MODE) {
            // don't call super because of the killRoi
            switch (Toolbar.getToolId()) {
                case Toolbar.RECTANGLE:
                    roi = new Roi(sx, sy, this);
                    break;
                case Toolbar.OVAL:
                    roi = new OvalRoi(sx, sy, this);
                    break;
                case Toolbar.POLYGON:
                case Toolbar.POLYLINE:
                case Toolbar.ANGLE:
                    roi = new PolygonRoi(sx, sy, this);
                    break;
                case Toolbar.FREEROI:
                case Toolbar.FREELINE:
                    roi = new FreehandRoi(sx, sy, this);
                    break;
                case Toolbar.LINE:
                    roi = new Line(sx, sy, this);
                    break;
                case Toolbar.TEXT:
                    roi = new TextRoi(sx, sy, this);
                    break;
                case Toolbar.POINT:
                    roi = new PointRoi(sx, sy, this);
                    break;
            }
        } else {
            roiGenerator = getCurrentRoiGenerator();
            if (roiGenerator != null) {
                roi = roiGenerator.createNewRoi(sx, sy, this);
                if (roi != null) {
                    deleteRoi(specialRois.get(roiGenerator));
                    specialRois.put(roiGenerator, roi);
                }
            }
        }
        if (roi != null) {
            // "this.roi" is the handled ROI
            this.roi = roi;
            addRoi(roi);
            if (roiGenerator != null) {
                roiGenerator.roiAdded(roi, this);
            }
        }
    }

    // this one is needed for the Wand tool, that sets the ROI
    @Override
    public void setRoi(Roi newRoi) {
        addRoi(newRoi);
        newRoi.setImage(this);
    }

    /**
     * Adds a new ROI to the list. In ROI mode, fires an event.
     * 
     * @param roiToAdd
     *            the new ROI to add
     */
    public void addRoi(Roi roiToAdd) {
        addRoi(roiToAdd, false);
    }

    /**
     * Adds a new ROI to the list. In ROI mode, fires an event.
     * 
     * @param roiToAdd
     *            the new ROI to add
     * @param isUndo
     *            true if it is an undo or redo action
     */
    public void addRoi(Roi roiToAdd, boolean isUndo) {
        if (roiToAdd != null) {
            if (!allRois.contains(roiToAdd)) {
                AddRoiEdit undoableEdit = generateAddRoiEdit(roiToAdd);
                RoiEvent roiEvent = generateRoiEvent(roiToAdd);
                processAddRoi(roiToAdd);
                // used not to take care of "volatile" ROIs (selection, zoom...)
                boolean nonVolatileRoi = shouldFireEvent() || external;

                // don't change name of deleted ROIs brought back to life by undo
                if (nonVolatileRoi && !isUndo) {
                    setRoiName(roiToAdd);
                }

                // add an undo edit only on regular actions (the ones from the
                // user), not on undo/redo actions
                if (nonVolatileRoi && !isUndo) {
                    storeUndoableEdit(undoableEdit, external);
                }

                // don't fire the event on special modes, not to consider the ROI
                if (nonVolatileRoi || isUndo) {
                    fireRoiAdded(roiEvent);
                }
            }
        }
    }

    protected AddRoiEdit generateAddRoiEdit(Roi roiToAdd) {
        return new AddRoiEdit(this, roiToAdd);
    }

    protected void processAddRoi(Roi roiToAdd) {
        if (isSingleRoiMode() && (!isSpecialRoiMode())) {
            clearPreviousNotSpecialRois();
        }
        allRois.add(roiToAdd);
        roiToAdd.setImage(this);
    }

    /**
     * @param roiToAdd
     */
    protected void setRoiName(Roi roiToAdd) {
        // JAVAAPI-13
        if (StringUtils.isEmpty(roiToAdd.getName())) {
            if (roiToAdd instanceof CircleRoi) {
                roiToAdd.setName("[" + cpt + "] " + ((CircleRoi) roiToAdd).getStringRadius());
                cpt++;
            } else {
                roiToAdd.setName(ObjectUtils.EMPTY_STRING + cpt);
                cpt++;
            }
        }
    }

    protected void storeUndoableEdit(AbstractUndoableEdit undoableEdit, boolean multiple) {
        if (multiple) {
            multipleEdit.addEdit(undoableEdit);
        } else {
            postEdit(undoableEdit);
        }
    }

    protected boolean shouldFireEvent() {
        // TODO do not forget these cases
        // return (isClassicRoiMode() || isLineProfileRoiMode() || isCalibrationRoiMode());
        boolean fireEvent;
//        if (isCalibrationRoiMode()) {
//            fireEvent = true;
//        } else {
//        }
        RoiGenerator generator = getCurrentRoiGenerator();
        fireEvent = ((generator == null) || (!generator.isSilentRoi()));
        return fireEvent;
    }

    /**
     * Sets the ROI that should be considered for processing. This is how we
     * fool IJ.
     * 
     * @param handledRoi
     *            the new ROI to be handled
     */
    public void setHandledRoi(Roi handledRoi) {
        if (handledRoi == null) {
            doHandleRoi(handledRoi);
        } else if (isLockValidatedRois()) {
            if ((!isInner(handledRoi)) && (!isOuter(handledRoi))) {
                doHandleRoi(handledRoi);
            }
        } else {
            doHandleRoi(handledRoi);
        }
    }

    protected void doHandleRoi(Roi handledRoi) {
        // TODO see if previousRoi is not involved in multi-points ROI loss (since modifyRoi uses it)
        roi = handledRoi;
        if (ip != null) {
            try {
                ip.setRoi(handledRoi);
            } catch (Exception e) {
                // Nothing to do. We don't really care about the ImageProcessor having a problem with the Roi.
                // If there is a problem, it should be treated ImageJ's development side
            }
        }
    }

    public void clearHandledRoi() {
        setHandledRoi(null);
    }

    /**
     * Destroy the handled ROI.
     * 
     * @see ij.ImagePlus#killRoi()
     */
    @Override
    public void killRoi() {
        // this is useful on mouseReleased to kill the created ROI that is 0-sized (single click)
        deleteRoi(roi);
    }

    @Override
    public void setImage(Image image) {
        ImageWindow temp = win;
        Roi formerRoi = roi;
        // Here, we use a hack to avoid undesired popups but keep drawing Rois.
        synchronized (windowLock) {
            // Hack start: set window to null
            win = null;
            super.setImage(image);
            // Hack end: restore window
            win = temp;
        }
        setHandledRoi(formerRoi);
    }

    @Override
    public void setWindow(ImageWindow win) {
        // XXX previous should not be cleaned inside synchronized block, as it may cause freezes (JAVAAPI-622)
        ImageWindow previous;
        synchronized (windowLock) {
            previous = getWindow();
            super.setWindow(win);
        }
        if ((previous != null) && (previous != win)) {
            HiddenImageWindow.clean(previous);
        }
    }

    public static void setRoiMode(int newMode) {
        if (newMode != roiMode) {
            roiMode = newMode;
            warnRoiModeListeners();
        }
    }

    public static void storeRoiMode() {
        storedRoiMode = roiMode;
    }

    public static void restoreRoiMode() {
        setRoiMode(storedRoiMode);
    }

    /**
     * @return the roiMode
     */
    public static int getRoiMode() {
        return roiMode;
    }

    public static void setClassicRoiMode() {
        setRoiMode(CLASSIC_ROI_MODE);
    }

    /**
     * Convenience method
     * 
     * @return true if current mode is classic, false otherwise
     */
    public static boolean isClassicRoiMode() {
        return (roiMode == CLASSIC_ROI_MODE);
    }

    /**
     * Returns whether inner and outer {@link Roi}s are locked. Locking a {@link Roi} means you can't move or resize it.
     * 
     * @return a boolean value
     */
    public boolean isLockValidatedRois() {
        return lockValidatedRois;
    }

    /**
     * Sets whether inner and outer {@link Roi}s are locked. Locking a {@link Roi} means you can't move or resize it.
     * 
     * @param lockValidatedRois
     *            the boolean value to lock or not the inner and outer {@link Roi}s. <code>true</code> to lock.
     */
    public void setLockValidatedRois(boolean lockValidatedRois) {
        this.lockValidatedRois = lockValidatedRois;
        if (lockValidatedRois) {
            // apply lock
            if (roi != null) {
                if (isInner(roi) || isOuter(roi)) {
                    clearHandledRoi();
                }
            }
        }
    }

    /**
     * Sets a {@link Roi} as an inner {@link Roi}. Setting a {@link Roi} as
     * inner means that you are interested in what is inside this {@link Roi}.
     * Once done, you won't be able to resize or move this {@link Roi}, until
     * you invalidate it.
     * 
     * @param roi
     *            the {@link Roi} to set as inner.
     */
    public void setInner(Roi roi) {
        if (roi != null && allRois.contains(roi)) {
            if (!innerRois.contains(roi)) {
                outerRois.remove(roi);
                innerRois.add(roi);
                if (selectedRois.contains(roi)) {
                    if (roi != validatedRoi) {
                        if (validatedRoi != null) {
                            deselectRoi(validatedRoi);
                        }
                        validatedRoi = roi;
                    }
                }
            }
        }
        if (isLockValidatedRois()) {
            if (this.roi != null && (this.roi == roi)) {
                clearHandledRoi();
            }
        }
    }

    /**
     * Sets a {@link Roi} as an outer {@link Roi}. Setting a {@link Roi} as
     * outer means that you are interested in what is outside of this {@link Roi}. Once done, you won't be able to
     * resize or move this {@link Roi}, until you invalidate it.
     * 
     * @param roi
     *            the {@link Roi} to set as outer.
     */
    public void setOuter(Roi roi) {
        if (roi != null && allRois.contains(roi)) {
            if (!outerRois.contains(roi)) {
                innerRois.remove(roi);
                outerRois.add(roi);
                if (selectedRois.contains(roi)) {
                    if (roi != validatedRoi) {
                        if (validatedRoi != null) {
                            deselectRoi(validatedRoi);
                        }
                        validatedRoi = roi;
                    }
                }
            }
        }
        if (isLockValidatedRois()) {
            if (this.roi != null && (this.roi == roi)) {
                clearHandledRoi();
            }
        }
    }

    /**
     * Returns a {@link List} containing all inner {@link Roi}s
     * 
     * @return a {@link List} of {@link Roi}
     */
    public List<Roi> getInnerRois() {
        return new ArrayList<Roi>(innerRois);
    }

    /**
     * Returns a {@link List} containing all outer {@link Roi}s
     * 
     * @return a {@link List} of {@link Roi}
     */
    public List<Roi> getOuterRois() {
        return new ArrayList<Roi>(outerRois);
    }

    /**
     * Returns the last created {@link Roi}
     * 
     * @return The last created {@link Roi}
     */
    public Roi getLastCreatedRoi() {
        Roi roi;
        if (allRois == null || allRois.isEmpty()) {
            roi = null;
        } else {
            roi = allRois.get(allRois.size() - 1);
        }
        return roi;
    }

    /**
     * Returns whether a {@link Roi} is inner
     * 
     * @param roi
     *            the {@link Roi} to test
     * @return a boolean value
     */
    public boolean isInner(Roi roi) {
        return innerRois.contains(roi);
    }

    /**
     * Returns whether a {@link Roi} is outer
     * 
     * @param roi
     *            the {@link Roi} to test
     * @return a boolean value
     */
    public boolean isOuter(Roi roi) {
        return outerRois.contains(roi);
    }

    /**
     * Invalidates a {@link Roi}. This means: sets a {@link Roi} as neither
     * inner nor outer
     * 
     * @param roi
     *            the {@link Roi} to invalidate
     */
    public void invalidateRoi(Roi roi) {
        if ((roi != null) && allRois.contains(roi)) {
            innerRois.remove(roi);
            outerRois.remove(roi);
            if (validatedRoi == roi) {
                validatedRoi = null;
            }
        }
    }

    /**
     * Use this to reverse inner <-> outer without un-selecting a roi
     * 
     * @param roi the {@link Roi} to reverse
     */
    public void reverseValidatedRoi(Roi roi) {
        if (validatedRoi == roi) {
            if (innerRois.contains(roi) && !outerRois.contains(roi)) {
                innerRois.remove(roi);
                outerRois.add(roi);
            } else if (outerRois.contains(roi) && !innerRois.contains(roi)) {
                outerRois.remove(roi);
                innerRois.add(roi);
            }
        }
    }

    /**
     * Returns the only {@link Roi} that is selected and inner or outer
     * 
     * @return a {@link Roi}
     */
    public Roi getValidatedRoi() {
        return validatedRoi;
    }

    public UndoableEditSupport getUndoSupport() {
        return undoSupport;
    }

    public void setUndoSupport(UndoableEditSupport undoSupport) {
        if (undoSupport != null) {
            this.undoSupport = undoSupport;
        }
    }

    /**
     * Returns the list of all ROIs
     * 
     * @return a list of all ROIs
     */
    public List<Roi> getAllRois() {
        return new ArrayList<Roi>(allRois);
    }

    /**
     * Returns the list of selected ROIs
     * 
     * @return a list of selected ROIs
     */
    public List<Roi> getSelectedRois() {
        return new ArrayList<Roi>(selectedRois);
    }

    protected void clearPreviousNotSpecialRois() {
        ArrayList<Roi> toDelete = new ArrayList<Roi>();
        for (Roi roi : allRois) {
//            if ((roi != lineProfileRoi) && (roi != calibrationRoi) && (roi != getRoi())) {
            if ((!isSpecialRoi(roi)) && (roi != getRoi())) {
                toDelete.add(roi);
            }
        }
        allRois.removeAll(toDelete);
        innerRois.removeAll(toDelete);
        outerRois.removeAll(toDelete);
        selectedRois.removeAll(toDelete);
        for (Roi roi : toDelete) {
            if (roi == validatedRoi) {
                validatedRoi = null;
            }
        }
        toDelete.clear();
    }

    /**
     * Returns true if the ROI is selected
     * 
     * @param roiToTest
     *            the ROI we want to know if it is selected
     * @return true if the ROI is selected, false otherwise
     */
    public boolean isSelectedRoi(Roi roiToTest) {
        return selectedRois.contains(roiToTest);
    }

    /**
     * Selects a ROI. Fires an event if it was not already selected.
     * 
     * @param roiToSelect
     *            the ROI to select
     */
    public void selectRoi(Roi roiToSelect) {
        if (roiToSelect != null) {
            if (allRois.contains(roiToSelect) && !selectedRois.contains(roiToSelect)) {
                // If there is a selected inner/outer ROI, deselect it first
                if (isInner(roiToSelect) || isOuter(roiToSelect)) {
                    if (validatedRoi != roiToSelect) {
                        if (validatedRoi != null) {
                            deselectRoi(validatedRoi);
                        }
                        validatedRoi = roiToSelect;
                    }
                }
                RoiEvent roiEvent = generateRoiEvent(roiToSelect);
                selectedRois.add(roiToSelect);
                fireRoiSelected(roiEvent);
            }
        }
    }

    /**
     * Deselects a ROI. Fires an event if it was selected.
     * 
     * @param roiToDeselect
     *            the ROI to deselect
     */
    public void deselectRoi(Roi roiToDeselect) {
        if (roiToDeselect != null) {
            if (selectedRois.contains(roiToDeselect)) {
                if (validatedRoi == roiToDeselect) {
                    validatedRoi = null;
                }
                RoiEvent roiEvent = generateRoiEvent(roiToDeselect);
                selectedRois.remove(roiToDeselect);
                fireRoiDeselected(roiEvent);
            }
        }
    }

    public void selectAllRois() {
        for (Roi roiToSelect : allRois) {
            selectRoi(roiToSelect);
        }
    }

    /**
     * Clear the ROI selection. All ROIs get deselected.
     */
    @SuppressWarnings("unchecked")
    public void clearRoiSelection() {
        ArrayList<Roi> copy = (ArrayList<Roi>) ((ArrayList<Roi>) selectedRois).clone();
        for (Roi selectedRoi : copy) {
            deselectRoi(selectedRoi);
        }
    }

    /**
     * Deletes a ROI. Fires an event in ROI mode.
     * 
     * @param roiToDelete
     */
    public void deleteRoi(Roi roiToDelete) {
        deleteRoi(roiToDelete, false);
    }

    /**
     * Deletes a ROI. Fires an event in ROI mode.
     * 
     * @param roiToDelete
     * @param isUndo
     */
    public void deleteRoi(Roi roiToDelete, boolean isUndo) {
        if (roiToDelete != null) {
            if (allRois.contains(roiToDelete)) {
                // give access to ROI's status before deleting it
                DeleteRoiEdit undoableEdit = generateDeleteRoiEdit(roiToDelete);
                RoiEvent roiEvent = generateRoiEvent(roiToDelete);

                processDeleteRoi(roiToDelete);

                // used not to take care of "volatile" ROIs (selection, zoom...)
                boolean nonVolatileRoi = shouldFireEvent() || external;

                // add an undo edit only on regular actions (the ones from the
                // user), not on undo/redo actions
                if (nonVolatileRoi && !isUndo) {
                    storeUndoableEdit(undoableEdit, external);
                }

                // don't fire the event on special modes, not to consider the ROI
                if (nonVolatileRoi || isUndo) {
                    fireRoiDeleted(roiEvent);
                }
            }
        }
    }

    protected DeleteRoiEdit generateDeleteRoiEdit(Roi roiToDelete) {
        return new DeleteRoiEdit(this, roiToDelete, isInner(roiToDelete), isOuter(roiToDelete),
                isSelectedRoi(roiToDelete));
    }

    protected void processDeleteRoi(Roi roiToDelete) {
        if (roiToDelete != null) {
            deselectRoi(roiToDelete);
            allRois.remove(roiToDelete);
            innerRois.remove(roiToDelete);
            outerRois.remove(roiToDelete);
            // if we delete the handled ROI, there is no more handled ROI
            if (roiToDelete.equals(roi)) {
                clearHandledRoi();
            }
            RoiGenerator toRemove = null;
            for (Entry<RoiGenerator, Roi> entry : specialRois.entrySet()) {
                // Here, we do want to use == instead of Roi.equals
                if (roiToDelete == entry.getValue()) {
                    toRemove = entry.getKey();
                    break;
                }
            }
            if (toRemove != null) {
                specialRois.remove(toRemove);
                toRemove.roiRemoved(roiToDelete, this);
            }
        }
    }

    /**
     * Deletes the selected ROIs. ROIs are previously deselected.
     */
    public synchronized void deleteSelectedRois() {
        external = true;

        // undo management
        multipleEdit = new CompoundEdit();

        // use a clone of the list
        List<Roi> copy = getSelectedRois();
        for (Roi roiToDelete : copy) {
            deleteRoi(roiToDelete);
        }
        copy.clear();

        // undo management
        multipleEdit.end();
        postEdit(multipleEdit);
        multipleEdit = null;

        external = false;
    }

    public synchronized void deleteAllRoisButSpecial(int mode, boolean resetRoiCounter) {
        RoiGenerator generator = roiGenerators.get(Integer.valueOf(mode));
        deleteAllRoisBut(resetRoiCounter, generator == null ? null : specialRois.get(generator));
    }

    public synchronized void deleteAllRoisButSpecials(boolean resetRoiCounter) {
        Collection<Roi> rois = getSpecialRois();
        deleteAllRoisBut(resetRoiCounter, rois.toArray(new Roi[rois.size()]));
    }

    protected void deleteAllRoisBut(boolean resetRoiCounter, Roi... toKeep) {
        external = true;
        deleteAllRoisBut(toKeep);
        external = false;
        if (resetRoiCounter) {
            // TODO manage in undo... otherwise, we might have ROIs that share the same name
            cpt = 1;
        }
    }

    protected void deleteAllRoisBut(Roi... toKeep) {
        // undo management
        multipleEdit = new CompoundEdit();

        // use a clone of the list
        List<Roi> copy = getAllRois();
        if ((toKeep != null) && (toKeep.length > 0)) {
            for (Roi roi : toKeep) {
                if (roi != null) {
                    copy.remove(roi);
                }
            }
        }
        for (Roi roiToDelete : copy) {
            deleteRoi(roiToDelete);
        }
        copy.clear();

        // undo management
        multipleEdit.end();
        postEdit(multipleEdit);
        multipleEdit = null;
    }

    /**
     * Deletes all ROIs. ROIs are previously deselected.
     * 
     * @param resetRoiCounter
     *            A boolean to say whether to reset Roi counter
     */
    public synchronized void deleteAllRois(boolean resetRoiCounter) {
        external = true;
        deleteAllRoisBut((Roi[]) null);
        external = false;
        if (resetRoiCounter) {
            // TODO manage in undo... otherwise, we might have ROIs that share the same name
            cpt = 1;
        }
    }

    /**
     * Let the canvas notify the manager on ROI moving and growing (key and
     * mouse)
     */
    public void fireRoiChanged() {
        if (roi != null) {
            fireRoiChanged(generateRoiEvent(roi));
        }
    }

    public void addRoiGenerator(RoiGenerator generator) {
        if (generator != null) {
            Integer id = Integer.valueOf(generator.getRoiMode());
            removeRoiGenerator(roiGenerators.get(id));
            roiGenerators.put(id, generator);
            if (generator instanceof ManagerDependentRoiGenerator) {
                ((ManagerDependentRoiGenerator) generator).registerRoiManager(this);
            }
        }
    }

    public void removeRoiGenerator(RoiGenerator generator) {
        if (generator != null) {
            deleteRoi(specialRois.remove(generator));
            roiGenerators.remove(Integer.valueOf(generator.getRoiMode()));
            if (generator instanceof IRoiManagerListener) {
                removeRoiManagerListener((IRoiManagerListener) generator);
            }
            if (generator instanceof ManagerDependentRoiGenerator) {
                ((ManagerDependentRoiGenerator) generator).unregisterRoiManager(this);
            }
        }
    }

    public void removeRoiGenerator(int roiMode) {
        deleteSpecialRoi(roiGenerators.remove(Integer.valueOf(roiMode)));
    }

    public RoiGenerator getRoiGenerator(int roiMode) {
        return roiGenerators.get(Integer.valueOf(roiMode));
    }

    public Collection<RoiGenerator> getRoiGenerators() {
        return roiGenerators.values();
    }

    public Roi deleteSpecialRoi(RoiGenerator generator) {
        Roi previous;
        if (generator == null) {
            previous = null;
        } else {
            previous = specialRois.get(generator);
            deleteRoi(previous);
        }
        return previous;
    }

    public Roi getSpecialRoi(RoiGenerator generator) {
        Roi roi = null;
        if (generator != null) {
            roi = specialRois.get(generator);
        }
        return roi;
    }

    public void setSpecialRoi(RoiGenerator generator, Roi roi) {
        if ((generator != null) && roiGenerators.values().contains(generator)) {
            Roi previous = specialRois.get(generator);
            if (roi == null) {
                deleteRoi(previous);
            } else if (generator.isCompatibleWithRoi(roi)) {
                if (previous != roi) {
                    deleteRoi(specialRois.get(generator));
                    specialRois.put(generator, roi);
                }
                addRoi(roi);
                generator.roiAdded(roi, this);
            }
        }
    }

    // -----------------------------------------------------------
    // Roi Management listener
    // -----------------------------------------------------------
    public void addRoiManagerListener(IRoiManagerListener l) {
        listenerList.add(IRoiManagerListener.class, l);
    }

    public void removeRoiManagerListener(IRoiManagerListener l) {
        listenerList.remove(IRoiManagerListener.class, l);
    }

    public boolean hasRoiManagerListener(IRoiManagerListener l) {
        boolean contains = false;
        if (l != null) {
            IRoiManagerListener[] listeners = listenerList.getListeners(IRoiManagerListener.class);
            for (IRoiManagerListener listener : listeners) {
                if (l.equals(listener)) {
                    contains = true;
                    break;
                }
            }
        }
        return contains;
    }

    protected static RoiEvent generateDefaultRoiEvent() {
        return new RoiEvent(null, null, false);
    }

    public RoiEvent generateRoiEvent(Roi theRoi) {
        return new RoiEvent(this, theRoi, isSelectedRoi(theRoi));
    }

    protected void fireRoiAdded(RoiEvent event) {
        IRoiManagerListener[] listeners = listenerList.getListeners(IRoiManagerListener.class);
        for (IRoiManagerListener listener : listeners) {
            listener.roiAdded(event);
        }
    }

    protected void fireRoiChanged(RoiEvent event) {
        IRoiManagerListener[] listeners = listenerList.getListeners(IRoiManagerListener.class);
        for (IRoiManagerListener listener : listeners) {
            listener.roiChanged(event);
        }
    }

    protected void fireRoiDeleted(RoiEvent event) {
        IRoiManagerListener[] listeners = listenerList.getListeners(IRoiManagerListener.class);
        for (IRoiManagerListener listener : listeners) {
            listener.roiDeleted(event);
        }
    }

    protected void fireRoiModeChanged(RoiEvent event) {
        IRoiManagerListener[] listeners = listenerList.getListeners(IRoiManagerListener.class);
        for (IRoiManagerListener listener : listeners) {
            listener.roiModeChanged(event);
        }
    }

    // -----------------------------------------------------------
    // Roi Selection listener
    // -----------------------------------------------------------
    public void addRoiSelectionListener(IRoiSelectionListener l) {
        listenerList.add(IRoiSelectionListener.class, l);
    }

    public void removeRoiSelectionListener(IRoiSelectionListener l) {
        listenerList.remove(IRoiSelectionListener.class, l);
    }

    protected void fireRoiSelected(RoiEvent event) {
        IRoiSelectionListener[] listeners = listenerList.getListeners(IRoiSelectionListener.class);
        for (IRoiSelectionListener listener : listeners) {
            listener.roiSelected(event);
        }
    }

    protected void fireRoiDeselected(RoiEvent event) {
        IRoiSelectionListener[] listeners = listenerList.getListeners(IRoiSelectionListener.class);
        for (IRoiSelectionListener listener : listeners) {
            listener.roiDeselected(event);
        }
    }

    @Override
    public synchronized IJRoiManager clone() {
        IJRoiManager clone;
        clone = (IJRoiManager) super.clone();
//        clone.win = win;
        clone.allRois = new ArrayList<Roi>(allRois);
        clone.selectedRois = new ArrayList<Roi>(selectedRois);
        clone.listenerList = new EventListenerList();
        IRoiManagerListener[] managerListeners = listenerList.getListeners(IRoiManagerListener.class);
        if (managerListeners != null) {
            for (IRoiManagerListener listener : managerListeners) {
                clone.listenerList.add(IRoiManagerListener.class, listener);
            }
        }
        IRoiSelectionListener[] selectionListeners = listenerList.getListeners(IRoiSelectionListener.class);
        if (selectionListeners != null) {
            for (IRoiSelectionListener listener : selectionListeners) {
                clone.listenerList.add(IRoiSelectionListener.class, listener);
            }
        }
        clone.innerRois = new ArrayList<Roi>(innerRois);
        clone.outerRois = new ArrayList<Roi>(outerRois);
        clone.undoSupport = new UndoableEditSupport();
        clone.multipleEdit = null;
        clone.windowLock = new Object();
        clone.roiGenerators = new ConcurrentHashMap<Integer, RoiGenerator>();
        for (Entry<Integer, RoiGenerator> entry : roiGenerators.entrySet()) {
            Integer key = entry.getKey();
            RoiGenerator value = entry.getValue();
            if ((key != null) && (value != null)) {
                clone.roiGenerators.put(key, value);
            }
        }
        clone.specialRois = new ConcurrentHashMap<RoiGenerator, Roi>();
        for (Entry<RoiGenerator, Roi> entry : specialRois.entrySet()) {
            RoiGenerator key = entry.getKey();
            Roi value = entry.getValue();
            if ((key != null) && (value != null)) {
                clone.specialRois.put(key, value);
            }
        }

        IMaskedImageViewer context = ObjectUtils.recoverObject(imageViewerReference);
        if (context instanceof ImageViewer) {
            JTable roiInfoTable = ((ImageViewer) context).getRoiInfoTable();
            if (roiInfoTable != null) {
                TableModel model = roiInfoTable.getModel();
                if (roiInfoTable instanceof IRoiSelectionListener) {
                    clone.removeRoiSelectionListener((IRoiSelectionListener) roiInfoTable);
                }
                if (model instanceof IRoiManagerListener) {
                    clone.removeRoiManagerListener((IRoiManagerListener) model);
                }
            }

        }
        clone.imageViewerReference = (context == null ? null : new WeakReference<IMaskedImageViewer>(context));
        addRoiModeListener(clone);
        return clone;
    }

    // ---------------
    // Roi operations
    // ---------------

    public void orSelectedRois() {
        roiCombination(RoiOperation.OR);
    }

    public void andSelectedRois() {
        roiCombination(RoiOperation.AND);
    }

    public void notSelectedRois() {
        // TODO do it only when there are 2 selected ROIs
        roiCombination(RoiOperation.NOT);
    }

    public void xorSelectedRois() {
        roiCombination(RoiOperation.XOR);
    }

    protected synchronized void roiCombination(RoiOperation op) {
        // use a clone of the list
        List<Roi> roisToCombine = filterAllowedRois(getSelectedRois());

        // we need at least 2 ROIs
        if (roisToCombine.size() > 1) {
            external = true;

            // undo management
            multipleEdit = new CombineRoiEdit(op);

            Roi roiToCombine = roisToCombine.get(0);
            ShapeRoi mainShape = null;
            if (roiToCombine instanceof ShapeRoi) {
                mainShape = (ShapeRoi) roiToCombine;
            } else {
                mainShape = new ShapeRoi(roiToCombine);
            }
            deleteRoi(roiToCombine);

            ShapeRoi shapeToCombine;
            for (int i = 1; i < roisToCombine.size(); i++) {
                roiToCombine = roisToCombine.get(i);
                if (roiToCombine instanceof ShapeRoi) {
                    shapeToCombine = (ShapeRoi) roiToCombine;
                } else {
                    shapeToCombine = new ShapeRoi(roiToCombine);
                }

                switch (op) {
                    case OR:
                        mainShape = mainShape.or(shapeToCombine);
                        break;

                    case AND:
                        mainShape = mainShape.and(shapeToCombine);
                        break;

                    case NOT:
                        mainShape = mainShape.not(shapeToCombine);
                        break;

                    case XOR:
                        mainShape = mainShape.xor(shapeToCombine);
                        break;
                }

                deleteRoi(roiToCombine);
            }

            // NOT op can give a 0-sized result if 2 ROIs are equals
            if (!mainShape.getBounds().isEmpty()) {
                addRoi(mainShape);
                selectRoi(mainShape);
            }

            // undo management
            multipleEdit.end();
            postEdit(multipleEdit);
            multipleEdit = null;

            external = false;
        }
    }

    protected List<Roi> filterAllowedRois(List<Roi> roisToFilter) {
        Iterator<Roi> iterator = roisToFilter.iterator();
        while (iterator.hasNext()) {
            Roi roiToFilter = iterator.next();
            // we remove lines and points since it is not well managed by shape operations
            if (roiToFilter.getType() == Roi.LINE || roiToFilter.getType() == Roi.POINT) {
                iterator.remove();
            }
            // normally useless because lines are already removed
            else if (isSpecialRoi(roiToFilter)) {
                iterator.remove();
            }
        }
        return roisToFilter;
    }

    public boolean isSpecialRoiMode() {
        return roiMode != CLASSIC_ROI_MODE;
    }

    public Collection<Roi> getSpecialRois() {
        return specialRois.values();
    }

    public boolean isSpecialRoi(Roi roi) {
        boolean special = false;
        if (roi != null) {
            Collection<Roi> specialRois = getSpecialRois();
            for (Roi specialRoi : specialRois) {
                // We do want to test for strict equality
                if (specialRoi == roi) {
                    special = true;
                    break;
                }
            }
        }
        return special;
    }

    public RoiGenerator getRoiGenerator(Roi roi) {
        RoiGenerator generator = null;
        if (roi != null) {
            for (Entry<RoiGenerator, Roi> entry : specialRois.entrySet()) {
                // We do want to test for strict equality
                if (entry.getValue() == roi) {
                    generator = entry.getKey();
                    break;
                }
            }
        }
        return generator;
    }

    public RoiGenerator getCurrentRoiGenerator() {
        return roiGenerators.get(Integer.valueOf(roiMode));
    }

    @Override
    public void roiModeChanged(RoiEvent event) {
        // The Roi mode changed: treat the new mode
        RoiGenerator generator = getCurrentRoiGenerator();
        if ((generator != null) && generator.needsNoHandledRoi()) {
            clearHandledRoi();
        }
        // Should fire Roi change, not roi mode change
        // fireRoiModeChanged(generateRoiEvent(null));
        fireRoiChanged(generateRoiEvent(null));
    }

    public boolean shouldHaveNoHandledRoi() {
        RoiGenerator generator = getCurrentRoiGenerator();
        return ((generator != null) && generator.needsNoHandledRoi());
    }

    public void addUndoableEditListener(UndoableEditListener undoListener) {
        if (undoSupport != null) {
            undoSupport.addUndoableEditListener(undoListener);
        }
    }

    public void removeUndoableEditListener(UndoableEditListener undoListener) {
        if (undoSupport != null) {
            undoSupport.removeUndoableEditListener(undoListener);
        }
    }

    protected void postEdit(AbstractUndoableEdit edit) {
        if (undoSupport != null) {
            undoSupport.postEdit(edit);
        }
    }

    public static void addRoiModeListener(IRoiModeListener listener) {
        if (listener != null) {
            synchronized (ROI_MODE_LISTENERS) {
                ROI_MODE_LISTENERS.add(listener);
            }
        }
    }

    public static void removeRoiModeListener(IRoiModeListener listener) {
        if (listener != null) {
            synchronized (ROI_MODE_LISTENERS) {
                ROI_MODE_LISTENERS.remove(listener);
            }
        }
    }

    // Here, we make sure every IJRoiManager updates its Roi mode
    protected static void warnRoiModeListeners() {
        List<IRoiModeListener> copy = new ArrayList<IRoiModeListener>();
        synchronized (ROI_MODE_LISTENERS) {
            copy.addAll(ROI_MODE_LISTENERS);
        }
        for (IRoiModeListener litener : copy) {
            litener.roiModeChanged(null);
        }
        copy.clear();
    }

    public boolean isSingleRoiMode() {
        return singleRoiMode;
    }

    public void setSingleRoiMode(boolean singleRoiMode) {
        this.singleRoiMode = singleRoiMode;
    }

}
