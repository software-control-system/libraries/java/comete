/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JComponent;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.DataList;
import fr.soleil.comete.swing.chart.data.DataListContainer;
import fr.soleil.comete.swing.chart.data.DataXY;
import fr.soleil.comete.swing.chart.data.IDataListContainer;
import fr.soleil.comete.swing.chart.data.SearchInfo;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.chart.view.graphics.MarkerGraphics;
import fr.soleil.comete.swing.util.CfFileReader;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.OFormat;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateFormattable;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.math.NiceScale;

/**
 * Class which handles chart axis.
 * 
 * @author JL Pons
 */
@Deprecated
public class JLAxis implements Serializable {

    private static final long serialVersionUID = -163887200057919365L;

    /**
     * Draw time annotation for x axis.
     */
    public static final int TIME_ANNO = 1;
    /**
     * Draw formated annotation
     */
    public static final int VALUE_ANNO = 2;

    // Constants
    public static final GregorianCalendar CALENDAR = new GregorianCalendar();
    private static final double[] LOG_STEP = { 0.301, 0.477, 0.602, 0.699, 0.778, 0.845, 0.903, 0.954 };

    // Local declaration
    private boolean visible;
    private double scaleMinimum = 0.0;
    private double scaleMaximum = 100.0;
    private double minimum = 0.0;
    private double maximum = 100.0;
    private boolean autoScale = false;
    private int scale = IChartViewer.LINEAR_SCALE;
    private Color labelColor;
    private Font labelFont;
    private int labelFormat;
    private double labelInterval = 5;
    private double userLabelInterval = 0;
    private LabelInfo[] labels;
    private boolean hasPreciseLabel = false;
    private int orientation; // Axis orientation/position
    private int dOrientation; // Default orientation (cannot be _ORG)
    private final boolean subtickVisible;
    private Dimension csize = null;
    private String title;
    private int titleAlignment;
    private int annotation = VALUE_ANNO;
    private final List<JLDataView> dataViews;
    private boolean gridVisible;
    private boolean subGridVisible;
    private int gridStyle;
    private final Rectangle boundRect;
    private boolean lastAutoScale;
    private boolean isZoomed;
    private double percentScrollback;
    private double axisDuration = Double.POSITIVE_INFINITY;
    private String axeName;
    private SimpleDateFormat useFormat;
    private double desiredPrec;
    private boolean drawOpposite;
    private int tickLength;
    private int subtickLength;
    private int fontOverWidth;
    private boolean inverted = false;
    private double tickStep; // In pixel
    private double minTickStep;
    private int subTickStep; // 0 => NONE, -1 => Log step, 1.. => Linear step
    private boolean fitXAxisToDisplayDuration;

    private boolean zeroAlwaysVisible = false;
    private boolean autoLabeling = true;
    private String[] userLabel = null;
    private double[] userLabelPos = null;

    private String dateFormat = IDateFormattable.US_DATE_FORMAT;

    private NiceScale niceScale = null;
    private boolean useNiceScale;
    private final JComponent parent;

    private final int axisType;
    private String labelValueFormat;

    public JLAxis(JComponent parent, int orientation) {
        this(parent, orientation, -1, true);
    }

    /**
     * Axis constructor (Expert usage).
     * 
     * @param orientation Default Axis placement (cannot be ..._ORG).
     * @param parent (deprecated, not used).
     * @see IChartViewer#HORIZONTAL_DOWN
     * @see IChartViewer#HORIZONTAL_UP
     * @see IChartViewer#VERTICAL_LEFT
     * @see IChartViewer#VERTICAL_RIGHT
     * @see JLAxis#setPosition
     */
    public JLAxis(JComponent parent, int orientation, boolean useNiceScale) {
        this(parent, orientation, -1, useNiceScale);
    }

    /**
     * Axis constructor (Expert usage).
     * 
     * @param orientation Default Axis placement (cannot be ..._ORG).
     * @param parent (deprecated, not used).
     * @see IChartViewer#HORIZONTAL_DOWN
     * @see IChartViewer#HORIZONTAL_UP
     * @see IChartViewer#VERTICAL_LEFT
     * @see IChartViewer#VERTICAL_RIGHT
     * @see JLAxis#setPosition
     */
    public JLAxis(JComponent parent, int orientation, int axisType, boolean useNiceScale) {
        this.parent = parent;
        labels = new LabelInfo[0];
        labelFont = new Font("Dialog", Font.PLAIN, 11);
        labelColor = Color.black;
        title = null;
        titleAlignment = IComponent.CENTER;
        this.orientation = orientation;
        this.axisType = axisType;
        dOrientation = orientation;
        inverted = !isHorizontal();
        dataViews = new ArrayList<JLDataView>();
        gridVisible = false;
        subGridVisible = false;
        gridStyle = IChartViewer.STYLE_DOT;
        labelFormat = IChartViewer.AUTO_FORMAT;
        labelValueFormat = ObjectUtils.EMPTY_STRING;
        subtickVisible = true;
        boundRect = new Rectangle(0, 0, 0, 0);
        isZoomed = false;
        percentScrollback = 0.0;
        axeName = ObjectUtils.EMPTY_STRING;
        visible = true;
        drawOpposite = true;
        tickLength = 4;
        subtickLength = tickLength / 2;
        fontOverWidth = 0;
        minTickStep = 50.0;
        fitXAxisToDisplayDuration = true;
        this.useNiceScale = useNiceScale;
    }

    /**
     * Sets the percent scrollback. When using {@link JLChart#addData(JLDataView , double , double ) JLChart.addData}
     * and TIME_ANNO mode for the horizontal axis this property allows to avoid a full graph repaint for every new data
     * entered.
     * 
     * @param d Scrollback percent [0..100]
     */
    public void setPercentScrollback(double d) {
        percentScrollback = d / 100;
    }

    /**
     * Gets the percent scrollback
     * 
     * @return scrollback percent
     */
    public double getPercentScrollback() {
        return percentScrollback;
    }

    /**
     * Sets the axis color.
     * 
     * @param c Axis color
     * @see JLAxis#getAxisColor
     */
    public void setAxisColor(Color c) {
        labelColor = c;
    }

    /**
     * Returns the axis color.
     * 
     * @return Axis color
     * @see JLAxis#setAxisColor
     */
    public Color getAxisColor() {
        return labelColor;
    }

    /**
     * Sets the axis label format.
     * 
     * @param l Format of values displayed on axis and in tooltips.
     * @see IChartViewer#AUTO_FORMAT
     * @see IChartViewer#SCIENTIFIC_FORMAT
     * @see IChartViewer#TIME_FORMAT
     * @see IChartViewer#DECINT_FORMAT
     * @see IChartViewer#HEXINT_FORMAT
     * @see IChartViewer#BININT_FORMAT
     * @see IChartViewer#SCIENTIFICINT_FORMAT
     * @see IChartViewer#DATE_FORMAT
     * @see JLAxis#getLabelFormat
     */
    public void setLabelFormat(int l) {
        labelFormat = l;
    }

    /**
     * Returns the axis label format.
     * 
     * @return Axis value format
     * @see JLAxis#setLabelFormat
     */
    public int getLabelFormat() {
        return labelFormat;
    }

    public String getLabelValueFormat() {
        return labelValueFormat;
    }

    public void setLabelValueFormat(String labelValueFormat) {
        this.labelValueFormat = (labelValueFormat == null ? ObjectUtils.EMPTY_STRING : labelValueFormat.trim());
    }

    /**
     * Shows the grid.
     * 
     * @param b true to make the grid visible; false to hide it
     * @see JLAxis#isGridVisible
     */
    public void setGridVisible(boolean b) {
        gridVisible = b;
    }

    /**
     * Fit the x axis to display duration (Horizontal axis only).
     * 
     * @param b
     *            true to fit x axis false otherwise
     */
    public void setFitXAxisToDisplayDuration(boolean b) {
        fitXAxisToDisplayDuration = b;
    }

    /**
     * Return true if the x axis fit to display duration.
     */
    public boolean isFitXAxisToDisplayDuration() {
        return fitXAxisToDisplayDuration;
    }

    /**
     * Determines whether the axis is showing the grid
     * 
     * @return true if the grid is visible, false otherwise
     * @see JLAxis#setGridVisible
     */
    public boolean isGridVisible() {
        return gridVisible;
    }

    /**
     * Draw a second axis at the opposite side.
     * 
     * @param b
     *            true to enable the opposite axis.
     */
    public void setDrawOpposite(boolean b) {
        drawOpposite = b;
    }

    /**
     * Determines whether the axis at the opposite side is visible
     * 
     * @return true if opposite axis is visible.
     * @see JLAxis#setDrawOpposite
     */
    public boolean isDrawOpposite() {
        return drawOpposite;
    }

    /**
     * Shows the sub grid. More accurate grid displayed with a soft color.
     * 
     * @param b true to make the subgrid visible; false to hide it
     * @see JLAxis#isSubGridVisible
     */
    public void setSubGridVisible(boolean b) {
        subGridVisible = b;
    }

    /**
     * Determines whether the axis is showing the sub grid
     * 
     * @return true if the subgrid is visible, false otherwise
     * @see JLAxis#setSubGridVisible
     */
    public boolean isSubGridVisible() {
        return subGridVisible;
    }

    /**
     * Sets the grid style.
     * 
     * @param s Style of the grid. Can be one of the following:
     * @see IChartViewer#STYLE_SOLID
     * @see IChartViewer#STYLE_DOT
     * @see IChartViewer#STYLE_DASH
     * @see IChartViewer#STYLE_LONG_DASH
     * @see IChartViewer#STYLE_DASH_DOT
     * @see JLAxis#getGridStyle
     */
    public void setGridStyle(int s) {
        gridStyle = s;
    }

    /**
     * Returns the current grid style.
     * 
     * @return the current grid style
     * @see JLAxis#setGridStyle
     */
    public int getGridStyle() {
        return gridStyle;
    }

    /**
     * Sets the label font
     * 
     * @param f Sets the font for this components
     * @see JLAxis#getFont
     */
    public void setFont(Font f) {
        labelFont = f;
    }

    /**
     * Gets the label font
     * 
     * @return The current label font
     * @see JLAxis#setFont
     */
    public Font getFont() {
        return labelFont;
    }

    /**
     * Set the annotation method
     * 
     * @param a Annotation for this axis
     * @see IChartViewer#TIME_ANNO
     * @see IChartViewer#VALUE_ANNO
     */
    public void setAnnotation(int a) {
        annotation = a;
    }

    /**
     * Returns the annotation method
     * 
     * @see JLAxis#setAnnotation
     */
    public int getAnnotation() {
        return annotation;
    }

    /**
     * Display or hide the axis.
     * 
     * @param b
     *            True to make the axis visible.
     */
    public void setVisible(boolean b) {
        visible = b;
    }

    /**
     * Returns true if the axis is visble, false otherwise
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Determines whether the axis is zoomed.
     * 
     * @return true if the axis is zoomed, false otherwise
     * @see JLAxis#zoom
     */
    public boolean isZoomed() {
        return isZoomed;
    }

    /**
     * Determines whether the axis is in XY mode. Use only with HORIZONTAL axis.
     * 
     * @return true if the axis is in XY mode, false otherwise
     * @see JLAxis#addDataView
     */
    public boolean isXY() {
        return (dataViews.size() > 0);
    }

    /**
     * Sets minimum axis value. This value is ignored when using autoscale.
     * 
     * @param d Minimum value for this axis. Must be strictly positive for LOG_SCALE.
     * @see JLAxis#getMinimum
     */
    public void setMinimum(double d) {
        minimum = d;

        if (!autoScale) {
            if (scale == IChartViewer.LOG_SCALE) {
                if (d <= 0) {
                    d = 1;
                }
                scaleMinimum = Math.log10(d);
            } else {
                scaleMinimum = d;
            }

            if (useNiceScale) {
                computeUserLabelInterval();
            }
        }

    }

    /**
     * Gets minimum axis value
     * 
     * @return The minimum value for this axis.
     * @see JLAxis#setMinimum
     */
    public double getMinimum() {
        return minimum;
    }

    /**
     * Sets maximum axis value. This value is ignored when using autoscale.
     * 
     * @param d Maximum value for this axis. Must be strictly positive for LOG_SCALE.
     * @see JLAxis#getMaximum
     */
    public void setMaximum(double d) {
        maximum = d;

        if (!autoScale) {
            if (scale == IChartViewer.LOG_SCALE) {
                if (scaleMaximum <= 0) {
                    scaleMaximum = scaleMinimum * 10.0;
                }
                scaleMaximum = Math.log10(d);
            } else {
                scaleMaximum = d;
            }
            if (useNiceScale) {
                computeUserLabelInterval();
            }
        }

    }

    private double getNiceUserLabelInterval() {
        double newUserInterval = 0;
        // Calculate a default user interval
        Rectangle bound = this.getBoundRect();
        int length = bound.height;

        if (orientation == IChartViewer.HORIZONTAL_DOWN || orientation == IChartViewer.HORIZONTAL_UP
                || orientation == IChartViewer.HORIZONTAL_ORGY1 || orientation == IChartViewer.HORIZONTAL_ORGY2) {
            length = bound.width;
        }
        if (length > 0) {
            if (niceScale == null) {
                niceScale = new NiceScale(0, length);
            }
            // set 10 pixels for visibility
            niceScale.setMaxTicks(length / 10);
            double minTickSpacing = niceScale.getTickSpacing();

            double realLength = scaleMaximum - scaleMinimum;
            newUserInterval = (realLength * minTickSpacing) / length;
            // newUserInterval = Math.(newValue);
            // System.out.println("newUserInterval=" + newUserInterval);
        }
        return newUserInterval;
    }

    private void computeUserLabelInterval() {
        double newUserInterval = getNiceUserLabelInterval();
        // System.out.println("computeUserLabelInterval=" + newUserInterval);
        if (userLabelInterval < newUserInterval) {
            setUserLabelInterval(newUserInterval);
        }
    }

    /**
     * Gets maximum axis value
     * 
     * @return The maximum value for this axis.
     * @see JLAxis#setMaximum
     */
    public double getMaximum() {
        return maximum;
    }

    /**
     * Expert usage. Get minimum axis value (according to auto scale transformation).
     * 
     * @return The minimum value for this axis.
     */
    public double getScaleMinimum() {
        return scaleMinimum;
    }

    /**
     * Expert usage. Get maximum axis value (according to auto scale transformation).
     * 
     * @return The maximum value for this axis.
     */
    public double getScaleMaximum() {
        return scaleMaximum;
    }

    /**
     * Determines whether the axis is autoscaled.
     * 
     * @return true if the axis is autoscaled, false otherwise
     * @see JLAxis#setAutoScale
     */
    public boolean isAutoScale() {
        return autoScale;
    }

    /**
     * Sets the autoscale mode for this axis.
     * 
     * @param b true if the axis is autoscaled, false otherwise
     * @see JLAxis#isAutoScale
     */
    public void setAutoScale(boolean b) {
        autoScale = b;
        if (useNiceScale) {
            setUserLabelInterval(userLabelInterval);
        }
    }

    /**
     * @return the useNiceScale
     */
    public boolean isUseNiceScale() {
        return useNiceScale;
    }

    /**
     * @param useNiceScale the useNiceScale to set
     */
    public void setUseNiceScale(boolean useNiceScale) {
        this.useNiceScale = useNiceScale;
    }

    /**
     * Gets the scale mdoe for this axis.
     * 
     * @return scale mdoe
     * @see JLAxis#setScale
     */
    public int getScale() {
        return scale;
    }

    /**
     * Sets scale mode
     * 
     * @param s Scale mode for this axis
     * @see IChartViewer#LINEAR_SCALE
     * @see IChartViewer#LOG_SCALE
     * @see IChartViewer#getScale
     */
    public void setScale(int s) {

        scale = s;

        if (scale == IChartViewer.LOG_SCALE) {
            // Check min and max SEPARATELY.
            if (maximum <= 0) {
                maximum = 10;
            } else {
                scaleMaximum = Math.log10(maximum);
            }

            if (minimum <= 0) {
                scaleMinimum = 0.0001;
            } else {
                scaleMinimum = Math.log10(minimum);
            }

        } else {
            scaleMinimum = minimum;
            scaleMaximum = maximum;
        }
    }

    // public void setScale(int s) {
    //
    // scale = s;
    //
    // if (scale == IChartViewer.LOG_SCALE) {
    // // Check min and max
    // if (minimum <= 0 || maximum <= 0) {
    // minimum = 1;
    // maximum = 10;
    // }
    // scaleMinimum = Math.log10(minimum);
    // scaleMaximum = Math.log10(maximum);
    // } else {
    // scaleMinimum = minimum;
    // scaleMaximum = maximum;
    // }
    //
    // }

    /**
     * Sets the axis orientation and reset position.
     * 
     * @param orientation Orientation value
     * @see IChartViewer#HORIZONTAL_DOWN
     * @see IChartViewer#HORIZONTAL_UP
     * @see IChartViewer#VERTICAL_LEFT
     * @see IChartViewer#VERTICAL_RIGHT
     * @see #setPosition
     */
    public void setOrientation(int orientation) {
        this.orientation = orientation;
        dOrientation = orientation;
    }

    /**
     * Returns the orientation of this axis.
     * 
     * @see #setOrientation
     */
    public int getOrientation() {
        return orientation;
    }

    /**
     * Zoom axis.
     * 
     * @param x1 New minimum value for this axis
     * @param x2 New maximum value for this axis
     * @see JLAxis#isZoomed
     * @see JLAxis#unzoom
     */
    public void zoom(int x1, int x2) {
        boolean canZoom = true;

        if (!isZoomed) {
            lastAutoScale = autoScale;
        }

        if (isHorizontal()) {

            // Clip
            if (x1 < boundRect.x) {
                x1 = boundRect.x;
            }
            if (x2 > (boundRect.x + boundRect.width)) {
                x2 = boundRect.x + boundRect.width;
            }

            // Too small zoom
            if ((x2 - x1) < 10) {
                canZoom = false;
            } else {
                // Compute new min and max
                double xr1 = (double) (x1 - boundRect.x) / (double) (boundRect.width);
                double xr2 = (double) (x2 - boundRect.x) / (double) (boundRect.width);
                double nmin = scaleMinimum + (scaleMaximum - scaleMinimum) * xr1;
                double nmax = scaleMinimum + (scaleMaximum - scaleMinimum) * xr2;

                // Too small zoom
                double difference = nmax - nmin;
                if (difference < 1E-13) {
                    canZoom = false;
                } else {
                    scaleMinimum = nmin;
                    scaleMaximum = nmax;
                }
            }
        } else {
            // Clip
            if (x1 < boundRect.y) {
                x1 = boundRect.y;
            }
            if (x2 > (boundRect.y + boundRect.height)) {
                x2 = boundRect.y + boundRect.height;
            }

            // Too small zoom
            if ((x2 - x1) < 10) {
                canZoom = false;
            } else {
                // Compute new min and max
                double yr1 = (double) (boundRect.y + boundRect.height - x2) / (double) (boundRect.height);
                double yr2 = (double) (boundRect.y + boundRect.height - x1) / (double) (boundRect.height);
                double nmin = scaleMinimum + (scaleMaximum - scaleMinimum) * yr1;
                double nmax = scaleMinimum + (scaleMaximum - scaleMinimum) * yr2;

                // Too small zoom
                double difference = nmax - nmin;
                if (difference < 1E-13) {
                    canZoom = false;
                } else {
                    scaleMinimum = nmin;
                    scaleMaximum = nmax;
                }
            }
        }
        if (canZoom) {
            autoScale = false;
            isZoomed = true;
        }
    }

    /**
     * Unzoom the axis and restores last state.
     * 
     * @see JLAxis#isZoomed
     * @see JLAxis#unzoom
     */
    public void unzoom() {
        autoScale = lastAutoScale;
        if (!lastAutoScale) {
            setMinimum(getMinimum());
            setMaximum(getMaximum());
        }
        isZoomed = false;
    }

    /**
     * @deprecated Use getTickSpacing
     * @see JLAxis#getTickSpacing
     */
    @Deprecated
    public int getTick() {
        return (int) minTickStep;
    }

    /**
     * Returns the current minimum tick spacing (in pixel).
     */
    public double getTickSpacing() {
        return minTickStep;
    }

    /**
     * Sets the minimum tick spacing (in pixel). Allows to control the number of generated labels.
     * 
     * @param spacing Minimum tick spacing
     */
    public void setTickSpacing(double spacing) {
        minTickStep = spacing;
    }

    /**
     * Sets the tick length (in pixel).
     * 
     * @param lgth Length
     */
    public void setTickLength(int lgth) {
        tickLength = lgth;
        subtickLength = tickLength / 2;
    }

    /**
     * Returns the tick length (in pixel).
     */
    public int getTickLength() {
        return tickLength;
    }

    /**
     * Gets the axis label.
     * 
     * @return Axis name.
     * @see JLAxis#setName
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the axis title. Title is displayed along or above the axis.
     * 
     * @param title title of this axis.
     * @see JLAxis#getName
     */
    public void setTitle(String title) {

        int z = 0;
        if (title != null) {
            z = title.length();
        }

        if (z > 0) {
            this.title = title;
        } else {
            this.title = null;
        }

    }

    /**
     * Returns the title alignment of this axis
     * 
     * @return an int value
     * 
     * @see IComponent#TOP
     * @see IComponent#LEFT
     * @see IComponent#BOTTOM
     * @see IComponent#RIGHT
     */
    public int getTitleAlignment() {
        return titleAlignment;
    }

    /**
     * Sets the title alignment of this axis
     * 
     * @param titleAlignment
     *            the alignment to set
     * 
     * @see IComponent#TOP
     * @see IComponent#LEFT
     * @see IComponent#BOTTOM
     * @see IComponent#RIGHT
     */
    public void setTitleAlignment(int titleAlignment) {
        this.titleAlignment = titleAlignment;
    }

    /**
     * Sets the axis position
     * 
     * @param o
     *            Axis position
     * @see JLAxis#VERTICAL_LEFT
     * @see JLAxis#VERTICAL_RIGHT
     * @see JLAxis#VERTICAL_ORG
     * @see JLAxis#HORIZONTAL_DOWN
     * @see JLAxis#HORIZONTAL_UP
     * @see JLAxis#HORIZONTAL_ORG1
     * @see JLAxis#HORIZONTAL_ORG2
     */
    public void setPosition(int o) {
        if (isHorizontal()) {
            if (o >= IChartViewer.HORIZONTAL_DOWN && o <= IChartViewer.HORIZONTAL_ORGY2) {
                orientation = o;
            }
        } else {
            if (o >= IChartViewer.VERTICAL_RIGHT && o <= IChartViewer.VERTICAL_ORGX) {
                orientation = o;
            }
        }
    }

    /**
     * Returns the axis position
     * 
     * @return Axis position
     * @see JLAxis#setPosition
     */
    int getPosition() {
        return orientation;
    }

    /**
     * Gets the axis label.
     * 
     * @return Axis name.
     * @see JLAxis#setAxeName
     */
    public String getAxeName() {
        return axeName;
    }

    /**
     * Sets the axis name. Name is displayed in tooltips when clicking on the graph.
     * 
     * @param s Name of this axis.
     * @see JLAxis#getName
     */
    public void setAxeName(String s) {
        axeName = s;
    }

    /**
     * Displays a DataView on this axis. The graph switches in XY monitoring mode when adding a dataView to X axis. Only
     * one view is allowed on HORIZONTAL Axis. In case of a view plotted along this horizontal axis doesn't have the
     * same number of point as this x view, points are correlated according to their x values.
     * 
     * @param v The dataview to map along this axis.
     * @see JLAxis#removeDataView
     * @see JLAxis#clearDataView
     * @see JLAxis#getViews
     */
    public void addDataView(JLDataView v) {
        if ((v != null) && (dataViews != null)) {
            synchronized (dataViews) {
                if (!dataViews.contains(v)) {
                    if (isHorizontal()) {
                        // Switch to XY mode
                        // Only one view on X
                        dataViews.clear();
                        dataViews.add(v);
                        v.setAxis(this);
                        setAnnotation(VALUE_ANNO);
                    } else {
                        dataViews.add(v);
                        v.setAxis(this);
                    }
                }
            }
        }

    }

    /**
     * Add the given dataView at the specified index.
     * 
     * @param index insertion position
     * @param v DataView to add
     * @see JLAxis#addDataView
     */
    public void addDataViewAt(int index, JLDataView v) {
        if ((v != null) && (dataViews != null)) {
            synchronized (dataViews) {
                if (!dataViews.contains(v)) {
                    if (isHorizontal()) {
                        addDataView(v);
                    } else {
                        dataViews.add(index, v);
                        v.setAxis(this);
                    }
                }
            }
        }

    }

    /**
     * Get the dataView of this axis at the specified index.
     * 
     * @param index DataView index
     * @return Null if index out of bounds.
     */
    public JLDataView getDataView(int index) {
        List<JLDataView> copy = getViewsCopy();
        JLDataView result = null;
        if (copy != null) {
            if (index > -1 & index < copy.size()) {
                result = copy.get(index);
            }
            copy.clear();
            copy = null;
        }
        return result;
    }

    /**
     * Removes dataview from this axis
     * 
     * @param v dataView to remove from this axis.
     * @see JLAxis#addDataView
     * @see JLAxis#clearDataView
     * @see JLAxis#getViews
     */
    public void removeDataView(JLDataView v) {
        if (v != null) {
            if (dataViews != null) {
                synchronized (dataViews) {
                    dataViews.remove(v);
                }
            }
            v.setAxis(null);
            if (isHorizontal()) {
                // Restore TIME_ANNO and Liner scale
                setAnnotation(TIME_ANNO);
                if (scale != IChartViewer.LINEAR_SCALE) {
                    setScale(IChartViewer.LINEAR_SCALE);
                }
            }
        }
    }

    /**
     * Removes dataview from this axis and returns true if the dataview has been
     * found for this Axis
     * 
     * @param v dataView to remove from this axis.
     * @return true if Axis contained the dataview and false if this dataview did not belong to the axis
     * @see JLAxis#addDataView
     * @see JLAxis#clearDataView
     * @see JLAxis#getViews
     */
    public boolean checkRemoveDataView(JLDataView v) {
        boolean contained = false;
        if (v != null) {
            if (dataViews != null) {
                synchronized (dataViews) {
                    contained = dataViews.remove(v);
                }
            }
            if (contained == true) {
                v.setAxis(null);
                if (isHorizontal()) {
                    // Restore TIME_ANNO and Liner scale
                    setAnnotation(TIME_ANNO);
                    if (scale != IChartViewer.LINEAR_SCALE) {
                        setScale(IChartViewer.LINEAR_SCALE);
                    }
                }
            }
        }
        return contained;
    }

    /**
     * Clear all dataview from this axis
     * 
     * @see JLAxis#removeDataView
     * @see JLAxis#addDataView
     * @see JLAxis#getViews
     */
    public void clearDataView() {
        if (dataViews != null) {
            synchronized (dataViews) {
                List<JLDataView> copy = getViewsCopy();
                if (copy != null) {
                    for (JLDataView v : copy) {
                        v.setAxis(null);
                    }
                    copy.clear();
                    copy = null;
                }
            }
            dataViews.clear();
        }
    }

    /**
     * Gets all dataViews displayed on this axis.
     * 
     * @return {@link List} of {@link JLDataView}.
     * @see JLAxis#addDataView
     * @see JLAxis#removeDataView
     * @see JLAxis#clearDataView
     */
    public List<JLDataView> getViewsCopy() {
        List<JLDataView> views = new ArrayList<JLDataView>();
        if (dataViews != null) {
            synchronized (dataViews) {
                views.addAll(dataViews);
            }
        }
        return views;
    }

    /**
     * Returns the number if dataview in this axis.
     * 
     * @return DataView number.
     */
    public int getViewNumber() {
        return dataViews.size();
    }

    /**
     * Invert this axis.
     * 
     * @param i
     *            true to invert the axis
     */
    public void setInverted(boolean i) {
        inverted = i;
    }

    /**
     * Returns true if this axis is inverted.
     */
    public boolean isInverted() {
        return inverted;
    }

    /**
     * Returns the bounding rectangle of this axis.
     * 
     * @return The bounding rectangle
     */
    public Rectangle getBoundRect() {
        return boundRect;
    }

    /**
     * Return a scientific (exponential) representation of the double.
     * 
     * @param d double to convert
     * @return A string containing a scientific representation of the given double.
     * @deprecated Use {@link ChartUtils#toScientific(double)} instead
     */
    @Deprecated
    public String toScientific(double d) {
        return ChartUtils.toScientific(d);
    }

    /**
     * @deprecated Use {@link ChartUtils#toScientificInt(double)} instead
     */
    @Deprecated
    public String toScientificInt(double d) {
        return ChartUtils.toScientificInt(d);
    }

    /**
     * Returns a representation of the double in time format
     * "EEE, d MMM yyyy HH:mm:ss".
     * 
     * @param vt number of millisec since epoch
     * @return A string continaing a time representation of the given double.
     * @deprecated Use {@link ChartUtils#formatTimeValue(double)} instead
     */
    @Deprecated
    public static String formatTimeValue(double vt) {
        return ChartUtils.formatTimeValue(vt);
    }

    /**
     * Returns a representation of the long in time format "EEE, d MMM yyyy HH:mm:ss".
     * 
     * @param vt number of millisec since epoch
     * @return A string continaing a time representation of the given long.
     * @deprecated Use {@link ChartUtils#formatTimeValue(long)} instead
     */
    @Deprecated
    public static String formatTimeValue(long vt) {
        return ChartUtils.formatTimeValue(vt);
    }

    /**
     * Parses a formatted time {@link String} to produce a {@link Date}
     * 
     * @param formattedTime The formatted time {@link String}
     * @return A {@link Date}. <code>null</code> if the {@link String} could not be parsed
     * @deprecated Use {@link ChartUtils#parseTimeString(String)} instead
     */
    @Deprecated
    public static Date parseTimeString(String formattedTime) {
        return ChartUtils.parseTimeString(formattedTime);
    }

    /**
     * Sets the appropriate time format for the range to display
     */
    private void computeDateformat(int maxLab) {

        // find optimal precision
        boolean found = false;
        int i = 0;
        while (i < ChartUtils.TIME_STEP_PRECISIONS.length && !found) {
            int n = (int) ((scaleMaximum - scaleMinimum) / ChartUtils.TIME_STEP_PRECISIONS[i]);
            found = (n <= maxLab);
            if (!found) {
                i++;
            }
        }

        if (!found) {
            // TODO Year Linear scale
            i--;
            desiredPrec = 10 * ChartUtils.YEAR;
            useFormat = ChartUtils.YEAR_FORMAT;
        } else {
            desiredPrec = ChartUtils.TIME_STEP_PRECISIONS[i];
            useFormat = ChartUtils.TIME_FORMATS[i];
        }

    }

    /**
     * Customize axis labels.
     * 
     * @param labels Label values
     * @param labelPos Label positions (in axis coordinates)
     */
    public void setLabels(String[] labels, double[] labelPos) {
        if (labels == null || labelPos == null) {
            clearUserLabels();
        } else {
            if (labels.length != labelPos.length) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .trace("JLAxis.setLabels() : labels and labelPos must have the same size");
            } else {
                userLabel = labels;
                // avoiding NullPointerExceptions
                if (userLabel != null) {
                    for (int i = 0; i < userLabel.length; i++) {
                        if (userLabel[i] == null) {
                            userLabel[i] = String.valueOf(null);
                        }
                    }
                }
                userLabelPos = labelPos;
                autoLabeling = false;
            }
        }
    }

    /**
     * Delete user labels and return to auto labeling.
     */
    public void clearUserLabels() {
        userLabel = null;
        userLabelPos = null;
        autoLabeling = true;
    }

    public void setLabelsFromUserInterval() {
        double[] tableTest;
        String[] labels;
        if ((!autoScale) && (getUserLabelInterval() > 0)) {
            tableTest = CometeUtils.createLabelInterval(getScaleMaximum(), getScaleMinimum(), getUserLabelInterval());
            labels = CometeUtils.doubleListToString(tableTest);
        } else {
            tableTest = null;
            labels = null;
        }
        setLabels(labels, tableTest);
    }

    protected String formatValue(double vt, double prec, String format) {
        String result;
        if (Format.isNumberFormatOK(format)) {
            result = Format.formatValue(vt, format);
        } else {
            result = formatValue(vt, prec);
        }
        return result;
    }

    /**
     * Returns a representation of the double according to the format
     * 
     * @param vt double to convert
     * @param prec Desired precision (Pass 0 to not perform prec rounding).
     * @return A string containing a formated representation of the given double.
     */
    public String formatValue(double vt, double prec) {
        // XXX GIRARDOT: following correction should be done in computeLabels methods
        // bug 23947:
        // This code forces precision recalculation in order to avoid bad rounding problems
        // into formatValue (the log precision is around 1 instead of 0.00.....N)
        double formatPrec = prec;
        if (scale == IChartViewer.LOG_SCALE) {
            if (vt < 1) {
                double log = Math.floor(Math.log10(vt));
                formatPrec = Math.pow(10.0, log);
            }
        }
        return ChartUtils.formatValue(vt, formatPrec, labelFormat, dateFormat);
    }

    private boolean isHorizontal() {
        return (dOrientation == IChartViewer.HORIZONTAL_DOWN) || (dOrientation == IChartViewer.HORIZONTAL_UP);
    }

    // *****************************************************
    // AutoScaling stuff
    // Expert usage

    private void computeAutoScale() {
        List<JLDataView> copy = getViewsCopy();
        int dataViewCount = copy.size();
        double mi = 0, ma = 0;
        if (autoScale && dataViewCount > 0) {
            scaleMinimum = Double.MAX_VALUE;
            scaleMaximum = -Double.MAX_VALUE;
            for (JLDataView v : copy) {
                if (v.hasTransform()) {
                    double[] mm = v.computeTransformedMinMax();
                    mi = mm[0];
                    ma = mm[1];
                } else {
                    mi = v.getMinimum();
                    ma = v.getMaximum();
                }
                if (scale == IChartViewer.LOG_SCALE) {
                    if (mi <= 0) {
                        mi = v.computePositiveMin();
                    }
                    if (mi != Double.MAX_VALUE) {
                        mi = Math.log10(mi);
                    }
                    if (ma <= 0) {
                        ma = -Double.MAX_VALUE;
                    } else {
                        ma = Math.log10(ma);
                    }
                }
                if (ma > scaleMaximum) {
                    scaleMaximum = ma;
                }
                if (mi < scaleMinimum) {
                    scaleMinimum = mi;
                }
            }
            // Check max and min
            if ((scaleMinimum == Double.MAX_VALUE) && (scaleMaximum == -Double.MAX_VALUE)) {
                // Only invalid data !!
                if (scale == IChartViewer.LOG_SCALE) {
                    scaleMinimum = 0;
                    scaleMaximum = 1;
                } else {
                    scaleMinimum = 0;
                    scaleMaximum = 99.99;
                }
            } else if (zeroAlwaysVisible) {
                if (scaleMinimum < 0 && scaleMaximum < 0) {
                    scaleMaximum = 0;
                } else if (scaleMinimum > 0 && scaleMaximum > 0) {
                    scaleMinimum = 0;
                }
            }
            ensureMinimumDistanceBetweenMinAndMax();
            double prec = ChartUtils.computeLowTen(scaleMaximum - scaleMinimum);
            // Avoid unlabeled axis when log scale
            if (scale == IChartViewer.LOG_SCALE && prec < 1.0) {
                prec = 1.0;
            }
            // System.out.println("ComputeAutoScale: Prec= " + prec );
            double tempMin = scaleMinimum, tempMax = scaleMaximum;
            scaleMinimum = Math.floor(tempMin / prec) * prec;
            scaleMaximum = Math.ceil(tempMax / prec) * prec;
            if (scaleMinimum == scaleMaximum) {
                scaleMinimum = (Math.floor(tempMin / prec) - 1) * prec;
                scaleMaximum = (Math.ceil(tempMax / prec) + 1) * prec;
            }
            // System.out.println("ComputeAutoScale: " + min + "," + max );
        } // end ( if autoScale )
        copy.clear();
        copy = null;
    }

    /**
     * Expert usage. Sets the preferred scale for time axis (HORIZONTAL axis only)
     * 
     * @param d Duration (millisec)
     */
    public void setAxisDuration(double d) {
        axisDuration = d;
    }

    /**
     * Expert usage. Compute X auto scale (HORIZONTAL axis only)
     * 
     * @param views All views displayed along all Y axis.
     */
    public void computeXScale(List<JLDataView> views) {
        int i = 0;
        int viewCount = views.size();
        double mi, ma;

        if (isHorizontal() && autoScale && viewCount > 0) {
            if (isXY()) {
                // ******************************************************
                // XY monitoring
                computeAutoScale();
            } else {
                // ******************************************************
                // Classic monitoring
                JLDataView v;
                scaleMinimum = Double.MAX_VALUE;
                scaleMaximum = -Double.MAX_VALUE;
                // Horizontal autoScale
                for (i = 0; i < viewCount; i++) {
                    v = views.get(i);
                    ma = v.getMaxXValue();
                    mi = v.getMinXValue();
                    if (scale == IChartViewer.LOG_SCALE) {
                        if (mi <= 0) {
//                            if (annotation == VALUE_ANNO) {
//                                mi = v.getPositiveMinXValue();
//                            } else {
//                                mi = v.getPositiveMinTime();
//                            }
                            mi = v.getPositiveMinXValue();
                        }
                        if (mi != Double.MAX_VALUE) {
                            mi = Math.log10(mi);
                        }
                        if (ma <= 0) {
                            ma = -Double.MAX_VALUE;
                        } else {
                            ma = Math.log10(ma);
                        }
                    }
                    if (ma > scaleMaximum) {
                        scaleMaximum = ma;
                    }
                    if (mi < scaleMinimum) {
                        scaleMinimum = mi;
                    }
                }
                if (scaleMinimum == Double.MAX_VALUE && scaleMaximum == -Double.MAX_VALUE) {
                    // Only empty views !
                    if (scale == IChartViewer.LOG_SCALE) {
                        scaleMinimum = 0;
                        scaleMaximum = 1;
                    } else {
                        if (annotation == TIME_ANNO) {
                            scaleMinimum = System.currentTimeMillis() - ChartUtils.HOUR;
                            scaleMaximum = System.currentTimeMillis();
                        } else {
                            scaleMinimum = 0;
                            scaleMaximum = 99.99;
                        }
                    }
                }
                if (annotation == TIME_ANNO) {
                    if (axisDuration != Double.POSITIVE_INFINITY && fitXAxisToDisplayDuration) {
                        scaleMinimum = scaleMaximum - axisDuration;
                    }
                    scaleMaximum += (scaleMaximum - scaleMinimum) * percentScrollback;

                }
                ensureMinimumDistanceBetweenMinAndMax();
            } // end if (isXY()) ... else
        } // end if (isHorizontal() && autoScale && viewCount > 0)
    }

    protected void ensureMinimumDistanceBetweenMinAndMax() {
        if (Math.abs(scaleMaximum - scaleMinimum) < 1e-100) {
            double delta = 0.999;
            if (scaleMaximum < scaleMinimum) {
                delta *= -1;
            }
            scaleMaximum += delta;
            scaleMinimum -= delta;
        }
    }

    // *****************************************************
    // Measurements stuff

    /**
     * Expert usage.
     * 
     * @return Axis font dimension.
     */
    public int getLabelFontDimension(FontRenderContext frc, boolean doubleIfHorizontal) {
        if ((!visible) || (frc == null)) {
            return 5; // 5 pixel margin when axis not visible
        }
        int hFont = (int) (labelFont.getLineMetrics("dummyStr0", frc).getHeight() + 0.5);
        if (isHorizontal() && doubleIfHorizontal) {
            if (title != null) {
                return 2 * hFont + 5;
            } else {
                return hFont + 5;
            }
        } else {
            if (title != null) {
                return hFont + 5;
            } else {
                return 5;
            }
        }
    }

    public int getFontOverWidth() {
        return fontOverWidth;
    }

    /**
     * Expert usage. Returns axis tichkness in pixel ( shorter side )
     * 
     * @return Axis tichkness
     * @see JLAxis#getLength
     */
    public int getThickness() {
        if (isHorizontal()) {
            return getCHeight();
        } else {
            return getCWidth();
        }
    }

    /**
     * Expert usage. Returns axis lenght in pixel ( larger side ).
     * 
     * @return Axis lenght.
     * @see JLAxis#getThickness
     */
    public int getLength() {
        if (isHorizontal()) {
            return getCWidth();
        } else {
            return getCHeight();
        }
    }

    /**
     * Expert usage. Computes labels and measures axis dimension.
     * 
     * @param frc Font render context
     * @param desiredWidth Desired width
     * @param desiredHeight Desired height
     */
    public void measureAxis(FontRenderContext frc, int desiredWidth, int desiredHeight) {
        int max_width = 10; // Minimum width
        int max_height = 0;

        computeAutoScale();

        if (autoLabeling) {
            if (isHorizontal()) {
                computeLabels(frc, desiredWidth);
            } else {
                computeLabels(frc, desiredHeight);
            }
        } else {
            if (isHorizontal()) {
                computeUserLabels(frc, desiredWidth);
            } else {
                computeUserLabels(frc, desiredHeight);
            }
        }
        LabelInfo[] temp = labels;
        for (LabelInfo li : temp) {
            if (li.getWidth() > max_width) {
                max_width = li.getWidth();
            }
            if (li.getHeight() > max_height) {
                max_height = li.getHeight();
            }
        }

        fontOverWidth = max_width / 2 + 1;

        if (isHorizontal()) {
            csize = new Dimension(desiredWidth, max_height + getLabelFontDimension(frc, false));
        } else {
            csize = new Dimension(max_width + getLabelFontDimension(frc, false), desiredHeight);
        }

    }

    // ****************************************************************
    // search nearest point stuff

    /**
     * Expert usage. Transfrom given coordinates (real space) into pixel coordinates
     * 
     * @param x The x coordinates (Real space)
     * @param y The y coordinates (Real space)
     * @param xAxis The axis corresponding to x coordinates.
     * @return Point(-100,-100) when cannot transform
     */
    public Point transform(double x, double y, JLAxis xAxis) {

        // The graph must have been measured before
        // we can transform
        if (csize == null) {
            return new Point(-100, -100);
        }

        double xlength = (xAxis.getScaleMaximum() - xAxis.getScaleMinimum());
        int xOrg = boundRect.x;
        int yOrg = boundRect.y + getLength();
        double vx, vy;

        // Check validity
        if (Double.isNaN(y) || Double.isNaN(x)) {
            return new Point(-100, -100);
        }

        if (xAxis.getScale() == IChartViewer.LOG_SCALE) {
            if (x <= 0) {
                return new Point(-100, -100);
            } else {
                vx = Math.log10(x);
            }
        } else {
            vx = x;
        }

        if (scale == IChartViewer.LOG_SCALE) {
            if (y <= 0) {
                return new Point(-100, -100);
            } else {
                vy = Math.log10(y);
            }

        } else {
            vy = y;
        }

        double xratio = (vx - xAxis.getScaleMinimum()) / (xlength) * (xAxis.getLength());
        double yratio = -(vy - scaleMinimum) / (scaleMaximum - scaleMinimum) * getCHeight();

        // Saturate
        if (xratio < -32000) {
            xratio = -32000;
        }
        if (xratio > 32000) {
            xratio = 32000;
        }
        if (yratio < -32000) {
            yratio = -32000;
        }
        if (yratio > 32000) {
            yratio = 32000;
        }

        return new Point((int) (xratio) + xOrg, (int) (yratio) + yOrg);

    }

    // Return the square distance
    private int distance2(int x1, int y1, int x2, int y2) {
        return (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
    }

    /**
     * Search the nearest point in the dataViews.
     * 
     * @param x The x coordinates (Real space)
     * @param y The y coordinates (Real space)
     * @param xAxis The axis corresponding to x coordinates.
     * @return A structure containing search result.
     */
    public SearchInfo searchNearest(int x, int y, JLAxis xAxis) {
        SearchInfo si;
        // Search only in graph area
        if (x <= boundRect.x - 10 || x >= boundRect.x + boundRect.width + 10 || y <= boundRect.y - 10
                || y >= boundRect.y + boundRect.height + 10) {
            si = new SearchInfo();
        } else {
            List<? extends AbstractDataView> copy = getViewsCopy();
            int idx = 0;
            int norme2;
            DataList minP = null;
            DataList minXP = null;
            Point minPt = null;
            int minNorme = Integer.MAX_VALUE;
            AbstractDataView minDataView = null;
            int minPl = 0;
            int minIdx = -1;
            boolean xy = xAxis.isXY();
            AbstractDataView w = xy ? xAxis.getViewsCopy().get(0) : null;
            Rectangle boundRect2 = new Rectangle();
            boundRect2.setBounds(boundRect.x - 2, boundRect.y - 2, boundRect.width + 4, boundRect.height + 4);
            for (AbstractDataView v : copy) {
                if (v.isClickable()) {
                    IDataListContainer e = xy ? new DataXY(v.getData(), w.getData())
                            : new DataListContainer(v.getData());
                    if (e.isValid()) {
                        e.init();
                    }
                    idx = 0;
                    while (e.isValid()) {
                        Point p;
                        if (xy) {
                            p = transform(w.getTransformedValue(((DataXY) e).getDataX().getY()),
                                    v.getTransformedValue(e.getDataY().getY()), xAxis);
                        } else {
                            p = transform(e.getDataY().getX(), v.getTransformedValue(e.getDataY().getY()), xAxis);
                        }
                        if (boundRect2.contains(p)) {
                            norme2 = distance2(x, y, p.x, p.y);
                            if (norme2 < minNorme) {

                                minNorme = norme2;
                                minP = e.getDataY();
                                if (xy) {
                                    minXP = ((DataXY) e).getDataX();
                                } else {
                                    minIdx = idx;
                                }
                                minDataView = v;
                                minPt = p;

                                // Compute placement for the value info window
                                if (p.x < (boundRect.x + boundRect.width / 2)) {
                                    if (p.y < (boundRect.y + boundRect.height / 2)) {
                                        minPl = SearchInfo.BOTTOMRIGHT;
                                    } else {
                                        minPl = SearchInfo.TOPRIGHT;
                                    }
                                } else {
                                    if (p.y < (boundRect.y + boundRect.height / 2)) {
                                        minPl = SearchInfo.BOTTOMLEFT;
                                    } else {
                                        minPl = SearchInfo.TOPLEFT;
                                    }
                                }
                            }
                        }
                        e.next();
                        idx++;
                    }
                }
            }
            copy.clear();
            copy = null;
            if (minNorme == Integer.MAX_VALUE) {
                si = new SearchInfo(); // No item found
            } else if (xy) {
                si = new SearchInfo(minPt.x, minPt.y, minDataView, axisType, minP, minNorme, minPl, -1);
                si.setXValue(minXP, w);
            } else {
                si = new SearchInfo(minPt.x, minPt.y, minDataView, axisType, minP, minNorme, minPl, minIdx);
            }
        }
        return si;
    }

    private void computeUserLabels(FontRenderContext frc, double length) {
        if (length > 0) {
            double sz = scaleMaximum - scaleMinimum;
            double precDelta = sz / length;

            LabelInfo[] temp = new LabelInfo[0];

            // Adjust labels offset according to tick

            int offX = 0;
            int offY = 0;
            switch (dOrientation) {
                case IChartViewer.VERTICAL_LEFT:
                    offX = (tickLength < 0) ? tickLength : 0;
                    break;
                case IChartViewer.VERTICAL_RIGHT:
                    offX = (tickLength < 0) ? -tickLength : 0;
                    break;
                default: // HORIZONTAL_DOWN
                    offY = (tickLength < 0) ? -tickLength : 0;
                    break;
            }

            // Create labels

            if (userLabel != null) {
                for (int i = 0; i < userLabel.length; i++) {

                    double upos;
                    if (scale == IChartViewer.LOG_SCALE) {
                        upos = Math.log10(userLabelPos[i]);
                    } else {
                        upos = userLabelPos[i];
                    }

                    if (upos >= (scaleMinimum - precDelta) && upos <= (scaleMaximum + precDelta)) {

                        int pos;

                        if (inverted) {
                            pos = (int) Math.rint(length * (1.0 - (upos - scaleMinimum) / sz));
                        } else {
                            pos = (int) Math.rint(length * ((upos - scaleMinimum) / sz));
                        }

                        Rectangle2D bounds = labelFont.getStringBounds(userLabel[i], frc);
                        LabelInfo li = new LabelInfo(userLabel[i], (int) bounds.getWidth(), (int) bounds.getHeight(),
                                pos);
                        li.setOffset(offX, offY);
                        int formerLength = temp.length;
                        temp = Arrays.copyOf(temp, formerLength + 1);
                        temp[formerLength] = li;
                    }
                }

            }
            labels = temp;
        }
    }

    // ****************************************************************
    // Compute labels
    // Expert usage
    private void computeLabels(FontRenderContext frc, double length) {
        if (length > 0) {
            // XXX Vincent Hardion max < min ???
            if (scaleMaximum < scaleMinimum) {
                // StringBuilder errorBuffer = new
                // StringBuilder("Error found in JLAxis");
                // errorBuffer.append("\nMethod computeLabels(FontRenderContext,double)");
                // errorBuffer.append("\nmax < min : ");
                // errorBuffer.append("max = ").append(max);
                // errorBuffer.append(", min = ").append(min);
                // errorBuffer.append("\nInverting min and max values to avoid problems");
                // System.err.println( errorBuffer.toString() );
                double a = scaleMinimum;
                scaleMinimum = scaleMaximum;
                scaleMaximum = a;
            }
            double sz = scaleMaximum - scaleMinimum;
            labels = new LabelInfo[0];
            // check if the axis is too zoomed
            if (sz > MathConst.PRECISION_LIMIT) {
                switch (annotation) {
                    case TIME_ANNO:
                        computeLabelsForTimeAnnotation(frc, length);
                        break;
                    case VALUE_ANNO:
                        computeLabelsForValueAnnotation(frc, length);
                        break;
                }
            }
        }
    }

    // ***********************************************************

    private void computeLabelsForValueAnnotation(final FontRenderContext frc, final double length) {
        if (length > 0) {
            LabelInfo[] temp = labels;
            double scaleDelta = scaleMaximum - scaleMinimum;
            long round;
            double pos;
            String s;
            double startx;
            double prec;
            double precDelta = scaleDelta / length;
            Rectangle2D bounds;

            double fontAscent = (labelFont.getLineMetrics("0", frc).getAscent());
            prec = ChartUtils.computeLowTen(scaleDelta);
            boolean extractLabel = false;

            // Anticipate label overlap
            int nbMaxLab = computeNbMaxLabel(frc, length);

            String format = labelValueFormat;

            int n;
            int step = 0;
            int subStep = 0;
            // Find the best precision
            if (scale == IChartViewer.LOG_SCALE) {
                prec = 1; // Decade
                step = -1; // Logarithm subgrid
                startx = Math.rint(scaleMinimum);
                n = (int) Math.rint(scaleDelta / prec);
                while (n > nbMaxLab) {
                    prec *= 2;
                    step = 2;
                    n = (int) Math.rint(scaleDelta / prec);
                    if (n > nbMaxLab) {
                        prec *= 5;
                        step = 10;
                        n = (int) Math.rint(scaleDelta / prec);
                    }
                }
                subStep = step;
            } else {
                // Linear scale
                // the interval between two labels is calculated with the axe
                // size
                prec = scaleDelta / nbMaxLab;
                // we adjust prec with the gap wanted
                double space = labelInterval;
                if (prec <= 1) {
                    space *= ChartUtils.computeLowTen(prec);
                    // avoid a division by 0
                    if (Math.rint(prec / space) == 0) {
                        space /= 10;
                    }
                } else if (prec < space) {
                    prec = space;
                }
                // calculate the real prec
                prec = Math.rint(prec / space) * space;
                // set the step for the tick calculation
                if (prec / 10 == (int) prec / 10) {
                    step = 10;
                } else {
                    step = 5;
                }
                // round to multiple of prec (last not visible label)
                round = (long) Math.floor(scaleMinimum / prec);
                startx = round * prec;
                // Compute real number of label
                double sx = startx;
                int nbL = 0;
                while (sx <= (scaleMaximum + precDelta)) {
                    if (sx >= (scaleMinimum - precDelta)) {
                        nbL++;
                    }
                    sx += prec;
                }

                if (nbL < 2) {
                    // Only one label
                    // Go backward and extract the 2 extremity
                    if (step == 10) {
                        step = 5;
                        prec = prec / 2.0;
                    } else {
                        step = 10;
                        prec = prec / 5.0;
                    }
                    extractLabel = true;
                }
                // Compute tick spacing
                double tickSpacing = Math.abs(((prec / scaleDelta) * length) / step);
                subStep = step;
                while (tickSpacing < 10.0 && subStep > 1) {
                    switch (subStep) {
                        case 10:
                            subStep = 5;
                            tickSpacing *= 2;
                            break;
                        case 5:
                            subStep = 2;
                            tickSpacing *= 2.5;
                            break;
                        case 2:
                            // No sub tick
                            subStep = 1;
                            break;
                    }
                }
            }
            // Compute tickStep
            tickStep = length * prec / scaleDelta;
            if (inverted) {
                tickStep = -tickStep;
            }
            subTickStep = subStep;

            // Adjust labels offset according to tick

            int offX = 0;
            int offY = 0;
            switch (dOrientation) {
                case IChartViewer.VERTICAL_LEFT:
                    offX = (tickLength < 0) ? tickLength : 0;
                    break;
                case IChartViewer.VERTICAL_RIGHT:
                    offX = (tickLength < 0) ? -tickLength : 0;
                    break;
                default: // HORIZONTAL_DOWN
                    offY = (tickLength < 0) ? -tickLength : 0;
                    break;
            }

            // Build labels

            String lastLabelText = ObjectUtils.EMPTY_STRING;
            double lastDiff = Double.MAX_VALUE;
            LabelInfo lastLabel = null;

            // find the value of the first label
            double firstShown = startx;
            if (firstShown < scaleMinimum - precDelta) {
                while (firstShown < scaleMinimum) {
                    firstShown += prec;
                }
            }

            // add a label if the first label is too far from the min
            if (firstShown - scaleMinimum > (10 * scaleDelta / 100)) {
                double prec2 = prec / 10;

                // find the best value for the new first label
                double firstLab = firstShown;
                while ((firstLab - prec2) > scaleMinimum) {
                    firstLab -= prec2;
                }

                if (inverted) {
                    pos = (int) Math.rint(length * (1.0 - (firstLab - scaleMinimum) / scaleDelta));
                } else {
                    pos = (int) Math.rint(length * ((firstLab - scaleMinimum) / scaleDelta));
                }

                double vt;
                if (scale == IChartViewer.LOG_SCALE) {
                    vt = Math.pow(10.0, firstLab);
                } else {
                    vt = firstLab;
                }

                String tempValue = formatValue(vt, prec2, format);
                double diff = 0;
                if (labelFormat != IChartViewer.TIME_FORMAT && labelFormat != IChartViewer.DATE_FORMAT) {
                    diff = Math.abs(Double.parseDouble(tempValue) - vt);
                }
                if (lastLabelText.equals(tempValue)) {
                    // avoiding label duplication
                    if (diff < lastDiff) {
                        s = tempValue;
                    } else {
                        s = ObjectUtils.EMPTY_STRING;
                    }
                } else {
                    s = tempValue;
                }
                lastDiff = diff;
                lastLabelText = tempValue;
                bounds = labelFont.getStringBounds(s, frc);

                if (firstLab >= (scaleMinimum - precDelta)) {
                    LabelInfo li = new LabelInfo(s, (int) bounds.getWidth(), (int) fontAscent, pos);
                    li.setOffset(offX, offY);
                    int formerLength = temp.length;
                    temp = Arrays.copyOf(temp, formerLength + 1);
                    temp[formerLength] = li;
                    lastLabel = li;
                }

                // used to avoid subtick malfunction
                hasPreciseLabel = true;
            } else {
                hasPreciseLabel = false;
            }

            while (startx <= (scaleMaximum + precDelta)) {

                if (inverted) {
                    pos = (int) Math.rint(length * (1.0 - (startx - scaleMinimum) / scaleDelta));
                } else {
                    pos = (int) Math.rint(length * ((startx - scaleMinimum) / scaleDelta));
                }

                double vt;
                if (scale == IChartViewer.LOG_SCALE) {
                    vt = Math.pow(10.0, startx);
                } else {
                    vt = startx;
                }

                String tempValue = formatValue(vt, prec, format);
                if (tempValue == null) {
                    tempValue = ObjectUtils.EMPTY_STRING;
                }
                double diff = 0;
                if (labelFormat != IChartViewer.TIME_FORMAT && labelFormat != IChartViewer.DATE_FORMAT) {
                    double val;
                    if (labelFormat == IChartViewer.HEXINT_FORMAT) {
                        val = Long.parseLong(tempValue, 16);
                    } else {
                        val = Double.parseDouble(tempValue);
                    }
                    diff = Math.abs(val - vt);
                }
                if (lastLabelText.equals(tempValue)) {
                    // avoiding label duplication
                    if (diff < lastDiff) {
                        s = tempValue;
                        if (lastLabel != null) {
                            lastLabel.value = ObjectUtils.EMPTY_STRING;
                        }
                    } else {
                        s = ObjectUtils.EMPTY_STRING;
                    }
                } else {
                    s = tempValue;
                }
                lastDiff = diff;
                lastLabelText = tempValue;
                bounds = labelFont.getStringBounds(s, frc);

                if (startx >= (scaleMinimum - precDelta)) {
                    LabelInfo li = new LabelInfo(s, (int) bounds.getWidth(), (int) fontAscent, pos);
                    li.setOffset(offX, offY);
                    int formerLegnth = temp.length;
                    temp = Arrays.copyOf(temp, formerLegnth + 1);
                    temp[formerLegnth] = li;
                    lastLabel = li;
                }

                startx += prec;

            }

            // Extract 2 bounds when we didn't found a correct match.
            if (extractLabel) {
                if (temp.length > 2) {
                    LabelInfo lis = temp[0];
                    LabelInfo lie = temp[temp.length - 1];
                    tickStep = lie.pos - lis.pos;
                    subTickStep = labels.length - 1;
                    temp = new LabelInfo[2];
                    temp[0] = lis;
                    temp[1] = lie;
                }
            }
            labels = temp;
        }
    }

    // ****************************************************************

    private int computeNbMaxLabel(final FontRenderContext frc, final double length) {
        String s;
        double prec;
        Rectangle2D bounds;
        double fontAscent = (labelFont.getLineMetrics("0", frc).getAscent());
        prec = ChartUtils.computeLowTen(scaleMaximum - scaleMinimum);
        // Anticipate label overlap
        int nbMaxLab;
        if (isHorizontal()) {
            // HORIZONTAL
            // The strategy is not obvious here as we can't estimate the max
            // label width
            // Do it with the number of digit in min and max, and convert it in
            // pixel size
            double minT;
            double maxT;
            if (scale == IChartViewer.LOG_SCALE) {
                minT = Math.pow(10.0, scaleMinimum);
                maxT = Math.pow(10.0, scaleMaximum);
            } else {
                minT = scaleMinimum;
                maxT = scaleMaximum;
            }
            double mW;
            s = formatValue(minT, prec);
            bounds = labelFont.getStringBounds(s, frc);
            mW = bounds.getWidth();
            s = formatValue(maxT, prec);
            bounds = labelFont.getStringBounds(s, frc);
            if (bounds.getWidth() > mW) {
                mW = bounds.getWidth();
            }
            // consider the space between labels
            mW = 1.5 * mW;
            nbMaxLab = (int) (length / mW);
        } else {
            // VERTICAL
            nbMaxLab = (int) (length / (2.0 * fontAscent));
        }
        // Overrides maxLab
        int userMaxLab = (int) Math.rint(length / minTickStep);
        if (userMaxLab < nbMaxLab) {
            nbMaxLab = userMaxLab;
        }
        if (nbMaxLab < 1) {
            nbMaxLab = 1; // At least 1 label !
        }
        return nbMaxLab;
    }

    private void computeLabelsForTimeAnnotation(final FontRenderContext frc, final double length) {
        if (length > 0) {
            LabelInfo[] temp = labels;
            double sz = scaleMaximum - scaleMinimum;
            int lgth = (int) length;
            double startx;
            double precDelta = sz / length;
            double pos;

            // Only for HORINZONTAL axis !
            // This has nothing to do with TIME_FORMAT
            // 10 labels maximum should be enough...
            computeDateformat(10);

            // round to multiple of prec
            long round;
            round = (long) (scaleMinimum / desiredPrec);
            startx = (round + 1) * desiredPrec;

            if (inverted) {
                pos = length * (1.0 - (startx - scaleMinimum) / sz);
            } else {
                pos = length * ((startx - scaleMinimum) / sz);
            }
            Date date;
            synchronized (CALENDAR) {
                CALENDAR.setTimeInMillis((long) startx);
                date = CALENDAR.getTime();
            }
            String s;
            if (labelFormat == IChartViewer.DATE_FORMAT) {
                s = new SimpleDateFormat(dateFormat).format(date);
            } else {
                s = useFormat.format(date);
            }

            Rectangle2D bounds = labelFont.getStringBounds(s, frc);
            int w = (int) bounds.getWidth();
            int h = (int) bounds.getHeight();
            int formerLength = temp.length;
            temp = Arrays.copyOf(temp, formerLength + 1);
            temp[formerLength] = new LabelInfo(s, w, h, pos);

            double minStep = w * 1.3;
            if (minStep < minTickStep) {
                minStep = minTickStep;
            }
            double minPrec = (minStep / length) * sz;

            // Correct to avoid label overlap
            double prec = desiredPrec;
            while (prec < minPrec) {
                prec += desiredPrec;
            }

            tickStep = length * prec / sz;
            startx += prec;
            if (inverted) {
                tickStep = -tickStep;
            }
            subTickStep = 0;

            // Build labels
            while (startx <= (scaleMaximum + precDelta)) {

                if (inverted) {
                    pos = (int) Math.rint(length * (1.0 - (startx - scaleMinimum) / sz));
                } else {
                    pos = (int) Math.rint(length * ((startx - scaleMinimum) / sz));
                }
                synchronized (CALENDAR) {
                    CALENDAR.setTimeInMillis((long) startx);
                    date = CALENDAR.getTime();
                }
                if (labelFormat == IChartViewer.DATE_FORMAT) {
                    s = new SimpleDateFormat(dateFormat).format(date);
                } else {
                    s = useFormat.format(date);
                }
                bounds = labelFont.getStringBounds(s, frc);

                // Check limit
                if (pos > 0 && pos < lgth) {
                    w = (int) bounds.getWidth();
                    h = (int) bounds.getHeight();
                    formerLength = temp.length;
                    temp = Arrays.copyOf(temp, formerLength + 1);
                    temp[formerLength] = new LabelInfo(s, w, h, pos);
                    // This code allows sub-ticks to be drawn
                    subTickStep = formerLength;
                }

                startx += prec;

            }
            labels = temp;
        }
    }

    // ****************************************************************
    // use in case of linear and value axis
    // the interval between two labels will use labelInterval
    public void setLabelInterval(double d) {
        if (d != 0) {
            labelInterval = d;
        }
    }

    public double getLabelInterval() {
        return labelInterval;
    }

    public double getUserLabelInterval() {
        return userLabelInterval;
    }

    public void setUserLabelInterval(double userLabelInterval) {
        if (userLabelInterval != this.userLabelInterval) {
            if (useNiceScale) {
                clearUserLabels();
                double newUserInterval = getNiceUserLabelInterval();
                if (userLabelInterval <= 0) {
                    newUserInterval = userLabelInterval;
                }
                if (userLabelInterval >= newUserInterval) {
                    this.userLabelInterval = userLabelInterval;
                }
            } else {
                this.userLabelInterval = userLabelInterval;
            }
            if (!autoScale) {
                setLabelsFromUserInterval();
            }
        }
    }

    // ****************************************************************
    // Painting stuff

    /**
     * Expert Usage. Paint last point of a dataView.
     * 
     * @param g Graphics object
     * @param lp last point
     * @param p new point
     * @param v view containing the lp and p.
     */
    public void drawFast(Graphics g, Point lp, Point p, JLDataView v) {

        if (lp != null) {
            if (boundRect.contains(lp)) {

                Graphics2D g2 = (Graphics2D) g;
                Stroke old = g2.getStroke();
                BasicStroke bs = CometeUtils.createStrokeForLine(v.getAdaptedLineWidth(), v.getStyle());
                if (bs != null) {
                    g2.setStroke(bs);
                }

                // Draw
                g.setColor(v.getColor());
                g.drawLine(lp.x, lp.y, p.x, p.y);

                // restore default stroke
                g2.setStroke(old);
            }
        }

        // Paint marker
        Color oc = g.getColor();
        g.setColor(v.getMarkerColor());
        paintMarker(g, v.getMarker(), v.getAdaptedMarkerSize(), p.x, p.y);
        g.setColor(oc);

    }

    /**
     * Expert usage. Paints a marker at the specified position
     * 
     * @param g Graphics object
     * @param mType Marker type
     * @param mSize Marker size
     * @param x x coordinates (pixel space)
     * @param y y coordinates (pixel space)
     */
    public static void paintMarker(Graphics g, int mType, int mSize, int x, int y) {
        MarkerGraphics.paintMarker(g, mType, mSize, x, y);
    }

    private void paintBarBorder(Graphics g, int barWidth, int y0, int x, int y) {
        g.drawLine(x - barWidth / 2, y, x + barWidth / 2, y);
        g.drawLine(x + barWidth / 2, y, x + barWidth / 2, y0);
        g.drawLine(x + barWidth / 2, y0, x - barWidth / 2, y0);
        g.drawLine(x - barWidth / 2, y0, x - barWidth / 2, y);
    }

    private void paintBar(Graphics2D g, Paint pattern, int barWidth, Color background, int fillStyle, int y0, int x,
            int y) {
        if (fillStyle != IChartViewer.FILL_STYLE_NONE) {
            if (pattern != null) {
                g.setPaint(pattern);
            } else {
                g.setColor(background);
            }
            if (y > y0) {
                g.fillRect(x - barWidth / 2, y0, barWidth, y - y0);
            } else {
                g.fillRect(x - barWidth / 2, y, barWidth, (y0 - y));
            }
        }
    }

    /**
     * Expert usage. Draw a sample line of a dataview
     * 
     * @param g Graphics object
     * @param x x coordinates (pixel space)
     * @param y y coordinates (pixel space)
     * @param v dataview
     */
    public static void drawSampleLine(Graphics g, int x, int y, JLDataView v) {

        Color oldColor = g.getColor();
        Graphics2D g2 = (Graphics2D) g;
        Stroke oldStroke = g2.getStroke();
        BasicStroke bs = CometeUtils.createStrokeForLine(v.getAdaptedLineWidth(), v.getStyle());

        // Draw
        if ((v.getViewType() == IChartViewer.TYPE_LINE) || (v.getViewType() == IChartViewer.TYPE_STAIRS)) {
            g.setColor(v.getMarkerColor());
            paintMarker(g, v.getMarker(), v.getAdaptedMarkerSize(), x + 20, y);
            if (v.getAdaptedLineWidth() > 0) {
                g.setColor(v.getColor());
                if (bs != null) {
                    g2.setStroke(bs);
                }
                g.drawLine(x, y, x + 40, y);
            }

        } else if (v.getViewType() == IChartViewer.TYPE_BAR) {

            if (v.getFillStyle() != IChartViewer.FILL_STYLE_NONE) {
                g.setColor(v.getFillColor());
                g.fillRect(x + 16, y - 4, 8, 8);
            }

            g.setColor(v.getColor());
            if (bs != null) {
                g2.setStroke(bs);
            }
            if (v.getAdaptedLineWidth() > 0) {
                g.drawLine(x + 16, y - 4, x + 24, y - 4);
                g.drawLine(x + 24, y - 4, x + 24, y + 4);
                g.drawLine(x + 24, y + 4, x + 16, y + 4);
                g.drawLine(x + 16, y + 4, x + 16, y - 4);
            }

        }

        // Restore
        g2.setStroke(oldStroke);
        g.setColor(oldColor);

    }

    /**
     * Expert usage. Paints dataviews along the given axis.
     * 
     * @param g
     *            The graphics in which to paint dataviews
     * @param xAxis
     *            horizonbtal axis of the graph
     * @param xOrg
     *            x origin (pixel space)
     * @param yOrg
     *            y origin (pixel space)
     * @param useCliping
     *            a boolean to limit or not drawing zone. <code>TRUE</code> to
     *            limit the drawing zone.
     */
    public void paintDataViews(Graphics g, JLAxis xAxis, int xOrg, int yOrg, boolean useCliping) {
        boolean isXY = xAxis.isXY();
        AbstractDataView vx = null;
        // -------- Clipping
        int xClip = xOrg + 1;
        int yClip = yOrg - getLength() + 1;
        int wClip = xAxis.getLength() - 1;
        int hClip = getLength() - 1;
        if (wClip > 1 && hClip > 1) {
            if (useCliping) {
                g.clipRect(xClip, yClip, wClip, hClip);
            }
            // -------- Draw dataView
            if (isXY) {
                vx = xAxis.getViewsCopy().get(0);
            }
            List<JLDataView> copy = getViewsCopy();
            if (copy != null) {
                for (AbstractDataView v : copy) {
                    IDataListContainer l = (isXY ? new DataXY(v.getData(), vx.getData())
                            : new DataListContainer(v.getData()));
                    if (l.isValid()) {
                        // Max number of point
                        int nbPoint = v.getDataLength();
                        if (isXY) {
                            nbPoint += vx.getDataLength();
                        }
                        int pointX[] = new int[nbPoint];
                        int pointY[] = new int[nbPoint];
                        // Transform points
                        double minx, maxx, lx;
                        double miny, maxy, ly;
                        double xratio;
                        double yratio;
                        double vtx;
                        double vty;
                        double A0y = v.getA0();
                        double A1y = v.getA1();
                        double A2y = v.getA2();
                        double A0x;
                        double A1x;
                        double A2x;
                        if (isXY) {
                            A0x = vx.getA0();
                            A1x = vx.getA1();
                            A2x = vx.getA2();
                        } else {
                            A0x = 0;
                            A1x = 1;
                            A2x = 0;
                        }
                        int y0;
                        minx = xAxis.getScaleMinimum();
                        maxx = xAxis.getScaleMaximum();
                        lx = xAxis.getLength();
                        int sx = xAxis.getScale();
                        miny = scaleMinimum;
                        maxy = scaleMaximum;
                        ly = getLength();
                        int j = 0;
                        boolean valid = true;
                        // Set the stroke mode for dashed line
                        Graphics2D g2 = (Graphics2D) g;
                        BasicStroke bs = CometeUtils.createStrokeForLine(v.getAdaptedLineWidth(), v.getStyle());
                        // Create the filling pattern
                        Paint fPattern = CometeUtils.createPatternForFilling(v.getFillStyle(), v.getFillColor(),
                                v.getColor(), getParentBackground(), false);
                        // Compute zero vertical offset
                        switch (v.getFillMethod()) {
                            case IChartViewer.METHOD_FILL_FROM_TOP:
                                y0 = yOrg - (int) ly;
                                break;
                            case IChartViewer.METHOD_FILL_FROM_ZERO:
                                if (scale == IChartViewer.LOG_SCALE) {
                                    y0 = yOrg;
                                } else {
                                    y0 = (int) (miny / (maxy - miny) * ly) + yOrg;
                                }
                                break;
                            default:
                                y0 = yOrg;
                                break;
                        }
                        int barWidth = computeBarWidth(v, xAxis);
                        while (l.isValid()) {
                            // Go to starting time position
                            l.init();
                            while (valid && l.isValid()) {
                                // Compute transform here for performance
                                vty = A0y + A1y * l.getDataY().getY() + A2y * l.getDataY().getY() * l.getDataY().getY();
                                if (isXY) {
                                    vtx = A0x + A1x * ((DataXY) l).getDataX().getY()
                                            + A2x * ((DataXY) l).getDataX().getY() * ((DataXY) l).getDataX().getY();
                                } else {
                                    vtx = l.getDataY().getX();
                                }
                                valid = !Double.isNaN(vtx) && !Double.isNaN(vty)
                                        && (sx != IChartViewer.LOG_SCALE || vtx > 1e-100)
                                        && (scale != IChartViewer.LOG_SCALE || vty > 1e-100);
                                if (valid) {
                                    if (sx == IChartViewer.LOG_SCALE) {
                                        xratio = (Math.log10(vtx) - minx) / (maxx - minx) * lx;
                                    } else {
                                        xratio = (vtx - minx) / (maxx - minx) * lx;
                                    }
                                    if (scale == IChartViewer.LOG_SCALE) {
                                        yratio = -(Math.log10(vty) - miny) / (maxy - miny) * ly;
                                    } else {
                                        yratio = -(vty - miny) / (maxy - miny) * ly;
                                    }
                                    // Saturate
                                    if (xratio < -32000) {
                                        xratio = -32000;
                                    }
                                    if (xratio > 32000) {
                                        xratio = 32000;
                                    }
                                    if (yratio < -32000) {
                                        yratio = -32000;
                                    }
                                    if (yratio > 32000) {
                                        yratio = 32000;
                                    }
                                    if (j < nbPoint) {
                                        pointX[j] = (int) (xratio) + xOrg;
                                        pointY[j] = (int) (yratio) + yOrg;

                                        // Draw marker
                                        if (v.getMarker() > IChartViewer.MARKER_NONE) {
                                            g.setColor(v.getMarkerColor());
                                            paintMarker(g, v.getMarker(), v.getAdaptedMarkerSize(), pointX[j],
                                                    pointY[j]);
                                        }

                                        // Draw bar
                                        paintDataViewBar(g2, v, barWidth, bs, fPattern, y0, pointX[j], pointY[j]);
                                        j++;
                                    }
                                    // Go to next pos
                                    l.next();
                                } // end if (valid)
                            } // end while (valid && l.isValid())

                            // Draw the polyline
                            paintDataViewPolyline(g2, v, bs, fPattern, j, y0, pointX, pointY);
                            j = 0;
                            if (!valid) {
                                // Go to next pos
                                l.next();
                                valid = true;
                            }
                        } // End (while l!=null)
                    } // End (if l!=null)
                } // End for (JLDataView v : dataViews)
                copy.clear();
            } // end if (copy != null)
        } // end if (wClip > 1 && hClip > 1)
    }

    private int computeBarWidth(AbstractDataView v, JLAxis xAxis) {

        int defaultWidth = 20;
        double minx = xAxis.getScaleMinimum();
        double maxx = xAxis.getScaleMaximum();
        int bw = v.getBarWidth();
        double minI = Double.MAX_VALUE;

        // No auto scale
        if (bw > 0) {
            return bw;
        }

        // No autoScale when horizontal axis is logarithmic
        if (xAxis.getScale() == IChartViewer.LOG_SCALE) {
            return defaultWidth;
        }

        if (xAxis.isXY()) {

            AbstractDataView vx = xAxis.getViewsCopy().get(0);

            // Look for the minimun interval
            DataList d = vx.getData();
            if (d != null) {
                double x = d.getY();
                d = d.next;
                while (d != null) {
                    double diff = Math.abs(d.getY() - x);
                    if (diff < minI) {
                        minI = diff;
                    }
                    x = d.getY();
                    d = d.next;
                }
            }

        } else {

            // Look for the minimun interval
            DataList d = v.getData();
            if (d != null) {
                double x = d.getX();
                d = d.next;
                while (d != null) {
                    double diff = Math.abs(d.getX() - x);
                    if (diff < minI) {
                        minI = diff;
                    }
                    x = d.getX();
                    d = d.next;
                }
            }

        }

        if (minI == Double.MAX_VALUE) {
            return defaultWidth;
        }

        bw = (int) Math.floor(minI / (maxx - minx) * xAxis.getLength()) - 2;

        // Make width multiple of 2 and saturate
        bw = bw / 2;
        bw = bw * 2;
        if (bw < 0) {
            bw = 0;
        }

        return bw;
    }

    private void paintDataViewBar(Graphics2D g, AbstractDataView v, int barWidth, BasicStroke bs, Paint fPattern,
            int y0, int x, int y) {
        if (v.getViewType() == IChartViewer.TYPE_BAR) {
            paintBar(g, fPattern, barWidth, v.getFillColor(), v.getFillStyle(), y0, x, y);
            // Draw bar border
            if (v.getAdaptedLineWidth() > 0) {
                Stroke old = g.getStroke();
                if (bs != null) {
                    g.setStroke(bs);
                }
                g.setColor(v.getColor());
                paintBarBorder(g, barWidth, y0, x, y);
                g.setStroke(old);
            }
        }
    }

    private void paintDataViewPolyline(Graphics2D g, AbstractDataView v, BasicStroke bs, Paint fPattern, int nb,
            int yOrg, int[] pointX, int[] pointY) {
        if (nb > 0) {
            if (v.getViewType() == IChartViewer.TYPE_LINE) {
                paintDataViewPolylineNoCheck(g, v, bs, fPattern, nb, yOrg, pointX, pointY);
            } else if (v.getViewType() == IChartViewer.TYPE_STAIRS) {
                int nb2 = 2 * nb - 1;
                int[] pointX2, pointY2;
                pointX2 = new int[nb2];
                pointY2 = new int[nb2];
                for (int i = 0; i < nb; i++) {
                    pointX2[2 * i] = pointX[i];
                    pointY2[2 * i] = pointY[i];
                    if (i < nb - 1) {
                        pointX2[2 * i + 1] = pointX[i + 1];
                        pointY2[2 * i + 1] = pointY[i];
                    }
                }
                paintDataViewPolylineNoCheck(g, v, bs, fPattern, nb2, yOrg, pointX2, pointY2);
            }
        }
    }

    private void paintDataViewPolylineNoCheck(Graphics2D g, AbstractDataView v, BasicStroke bs, Paint fPattern, int nb,
            int yOrg, int[] pointX, int[] pointY) {
        // Ensure drawing at least a pixel
        int nb2 = nb;
        int[] pointX2 = pointX;
        int[] pointY2 = pointY;
        if (nb == 1) {
            nb2 = 2;
            pointX2 = new int[] { pointX[0], pointX[0] };
            pointY2 = new int[] { pointY[0], pointY[0] };
        }
        // Draw surface
        if (v.getFillStyle() != IChartViewer.FILL_STYLE_NONE) {
            int[] Xs = new int[nb2 + 2];
            int[] Ys = new int[nb2 + 2];
            for (int i = 0; i < nb2; i++) {
                Xs[i + 1] = pointX2[i];
                Ys[i + 1] = pointY2[i];
            }
            Xs[0] = Xs[1];
            Ys[0] = yOrg;
            Xs[nb2 + 1] = Xs[nb2];
            Ys[nb2 + 1] = yOrg;
            if (fPattern != null) {
                g.setPaint(fPattern);
            }
            g.fillPolygon(Xs, Ys, nb2 + 2);
        }
        if (v.getAdaptedLineWidth() > 0) {
            Stroke old = g.getStroke();
            if (bs != null) {
                g.setStroke(bs);
            }
            g.setColor(v.getColor());
            g.drawPolyline(pointX2, pointY2, nb2);
            g.setStroke(old);
        }
    }

    public JComponent getParent() {
        return parent;
    }

    private Color getParentBackground() {
        Color bg;
        if (parent == null) {
            bg = null;
        } else if (parent instanceof JLChart) {
            bg = ((JLChart) parent).getChartBackground();
        } else {
            bg = parent.getBackground();
        }
        return bg;
    }

    // Paint sub tick outside label limit
    // Expert usage
    private void paintYOutTicks(Graphics g, int x0, double ys, int y0, int la, BasicStroke bs, int tr, int off,
            boolean grid) {

        int j, h;
        Graphics2D g2 = (Graphics2D) g;
        Stroke old = g2.getStroke();

        if (subtickVisible) {
            if (subTickStep == -1) {

                for (j = 0; j < LOG_STEP.length; j++) {
                    h = (int) Math.rint(ys + tickStep * LOG_STEP[j]);
                    if (h >= y0 && h <= (y0 + getCHeight())) {
                        g.drawLine(x0 + tr + off, h, x0 + tr + off + subtickLength, h);
                        if (gridVisible && subGridVisible && grid) {
                            if (bs != null) {
                                g2.setStroke(bs);
                            }
                            g.drawLine(x0, h, x0 + la, h);
                            g2.setStroke(old);
                        }
                    }
                }

            } else if (subTickStep > 0) {

                double r = 1.0 / subTickStep;
                for (j = 0; j < subTickStep; j++) {
                    h = (int) Math.rint(ys + tickStep * r * j);
                    if (h >= y0 && h <= (y0 + getCHeight())) {
                        g.drawLine(x0 + tr + off, h, x0 + tr + off + subtickLength, h);
                        if ((j > 0) && gridVisible && subGridVisible && grid) {
                            if (bs != null) {
                                g2.setStroke(bs);
                            }
                            g.drawLine(x0, h, x0 + la, h);
                            g2.setStroke(old);
                        }
                    }
                }

            }

        }

    }

    // Paint sub tick outside label limit
    // Expert usage
    private void paintXOutTicks(Graphics g, int y0, double xs, int x0, int la, BasicStroke bs, int tr, int off,
            boolean grid) {

        int j, w;
        Graphics2D g2 = (Graphics2D) g;
        Stroke old = g2.getStroke();

        if (subtickVisible) {

            if (subTickStep == -1) {

                for (j = 0; j < LOG_STEP.length; j++) {
                    w = (int) Math.rint(xs + tickStep * LOG_STEP[j]);
                    if (w >= x0 && w <= (x0 + getCWidth())) {
                        g.drawLine(w, y0 + tr + off, w, y0 + tr + off + subtickLength);
                        if (gridVisible && subGridVisible && grid) {
                            if (bs != null) {
                                g2.setStroke(bs);
                            }
                            g.drawLine(w, y0, w, y0 + la);
                            g2.setStroke(old);
                        }
                    }
                }

            } else if (subTickStep > 0) {

                double r = 1.0 / subTickStep;
                for (j = 0; j < subTickStep; j++) {
                    w = (int) Math.rint(xs + tickStep * r * j);
                    if (w >= x0 && w <= (x0 + getCWidth())) {
                        g.drawLine(w, y0 + tr + off, w, y0 + tr + off + subtickLength);
                        if ((j > 0) && gridVisible && subGridVisible && grid) {
                            if (bs != null) {
                                g2.setStroke(bs);
                            }
                            g.drawLine(w, y0, w, y0 + la);
                            g2.setStroke(old);
                        }
                    }
                }

            }

        }

    }

    private int getTickShift(int width) {

        // Calculate position
        int off = 0;
        switch (dOrientation) {
            case IChartViewer.VERTICAL_LEFT:
                if (orientation == IChartViewer.VERTICAL_ORGX) {
                    off = -width / 2;
                }
                break;
            case IChartViewer.VERTICAL_RIGHT:
                if (orientation == IChartViewer.VERTICAL_ORGX) {
                    off = -width / 2;
                } else {
                    off = -width;
                }
                break;
            case IChartViewer.HORIZONTAL_DOWN:
            case IChartViewer.HORIZONTAL_UP:
                if (orientation == IChartViewer.HORIZONTAL_ORGY1 || orientation == IChartViewer.HORIZONTAL_ORGY2) {
                    off = -width / 2;
                } else if (orientation == IChartViewer.HORIZONTAL_UP) {
                    off = 0;
                } else {
                    off = -width;
                }
                break;
        }

        return off;
    }

    private int getTickShiftOpposite(int width) {

        // Calculate position
        int off = 0;
        switch (dOrientation) {
            case IChartViewer.VERTICAL_RIGHT:
                if (orientation == IChartViewer.VERTICAL_ORGX) {
                    off = -width / 2;
                }
                break;
            case IChartViewer.VERTICAL_LEFT:
                if (orientation == IChartViewer.VERTICAL_ORGX) {
                    off = -width / 2;
                } else {
                    off = -width;
                }
                break;
            case IChartViewer.HORIZONTAL_DOWN:
            case IChartViewer.HORIZONTAL_UP:
                if (orientation == IChartViewer.HORIZONTAL_ORGY1 || orientation == IChartViewer.HORIZONTAL_ORGY2) {
                    off = -width / 2;
                } else if (orientation == IChartViewer.HORIZONTAL_UP) {
                    off = -width;
                } else {
                    off = 0;
                }
                break;
        }

        return off;
    }

    // Paint Y sub tick and return tick spacing
    // Expert usage
    private void paintYTicks(Graphics g, int i, int x0, double y, int la, BasicStroke bs, int tr, int off,
            boolean grid) {

        int j, h;
        Graphics2D g2 = (Graphics2D) g;
        Stroke old = g2.getStroke();
        int size = labels.length;
        if (subtickVisible && i < (size - 1)) {

            // Draw ticks

            if (subTickStep == -1) { // Logarithmic step

                for (j = 0; j < LOG_STEP.length; j++) {
                    h = (int) Math.rint(y + tickStep * LOG_STEP[j]);
                    g.drawLine(x0 + tr + off, h, x0 + tr + off + subtickLength, h);
                    if (gridVisible && subGridVisible && grid) {
                        if (bs != null) {
                            g2.setStroke(bs);
                        }
                        g.drawLine(x0, h, x0 + la, h);
                        g2.setStroke(old);
                    }
                }

            } else if (subTickStep > 0) { // Linear step

                double r = 1.0 / subTickStep;
                for (j = 0; j < subTickStep; j++) {
                    h = (int) Math.rint(y + tickStep * r * j);
                    g.drawLine(x0 + tr + off, h, x0 + tr + off + subtickLength, h);
                    if ((j > 0) && gridVisible && subGridVisible && grid) {
                        if (bs != null) {
                            g2.setStroke(bs);
                        }
                        g.drawLine(x0, h, x0 + la, h);
                        g2.setStroke(old);
                    }
                }

            }

        }

    }

    // Paint X sub tick and return tick spacing
    // Expert usage
    private void paintXTicks(Graphics g, int i, int y0, double x, int la, BasicStroke bs, int tr, int off,
            boolean grid) {

        int j, w;
        Graphics2D g2 = (Graphics2D) g;
        Stroke old = g2.getStroke();
        int size = labels.length;
        if (subtickVisible && i < (size - 1)) {

            if (subTickStep == -1) { // Logarithmic step

                for (j = 0; j < LOG_STEP.length; j++) {
                    w = (int) Math.rint(x + tickStep * LOG_STEP[j]);
                    g.drawLine(w, y0 + tr + off, w, y0 + tr + off + subtickLength);
                    if (gridVisible && subGridVisible && grid) {
                        if (bs != null) {
                            g2.setStroke(bs);
                        }
                        g.drawLine(w, y0, w, y0 + la);
                        g2.setStroke(old);
                    }
                }

            } else if (subTickStep > 0) { // Linear step

                double r = 1.0 / subTickStep;
                for (j = 0; j < subTickStep; j++) {
                    w = (int) Math.rint(x + tickStep * r * j);
                    g.drawLine(w, y0 + tr + off, w, y0 + tr + off + subtickLength);
                    if ((j > 0) && gridVisible && subGridVisible && grid) {
                        if (bs != null) {
                            g2.setStroke(bs);
                        }
                        g.drawLine(w, y0, w, y0 + la);
                        g2.setStroke(old);
                    }
                }

            }

        }

    }

    /**
     * Compute the medium color of c1,c2
     * 
     * @param c1 Color 1
     * @param c2 Color 2
     * @return Averaged color.
     */
    public Color computeMediumColor(Color c1, Color c2) {
        return new Color((c1.getRed() + 3 * c2.getRed()) / 4, (c1.getGreen() + 3 * c2.getGreen()) / 4,
                (c1.getBlue() + 3 * c2.getBlue()) / 4);
    }

    /**
     * Expert usage. Paint the axis and its DataView at the specified position along the given axis.
     * 
     * @param g Graphics object
     * @param frc Font render context
     * @param x0 Axis x position (pixel space)
     * @param y0 Axis y position (pixel space)
     * @param xAxis Horizontal axis of the graph
     * @param xOrg X origin for transformation (pixel space)
     * @param yOrg Y origin for transformation (pixel space)
     * @param back Background color
     * @param oppositeVisible Opposite axis is visible.
     * 
     */
    public void paintAxis(Graphics g, FontRenderContext frc, int x0, int y0, JLAxis xAxis, int xOrg, int yOrg,
            Color back, boolean oppositeVisible) {

        int la = 0;
        int tr = 0;
        Point p0 = null;

        // Do not paint vertical axis without data
        if (isHorizontal() || dataViews.size() > 0) {
            // Do not paint when too small
            if (getLength() <= 1) {
                boundRect.setRect(0, 0, 0, 0);
            } else {
                la = xAxis.getLength();

                // Do not paint when out of bounds
                if (la <= 0) {
                    boundRect.setRect(0, 0, 0, 0);
                } else {
                    // Update bounding rectangle
                    boolean canPaint = true;
                    switch (dOrientation) {

                        case IChartViewer.VERTICAL_LEFT:
                            boundRect.setRect(x0 + getThickness(), y0, la, getCHeight());

                            if (orientation == IChartViewer.VERTICAL_ORGX) {
                                p0 = transform(0.0, 1.0, xAxis);
                                if ((p0.x >= (x0 + getCWidth())) && (p0.x <= (x0 + getCWidth() + la))) {
                                    tr = p0.x - (x0 + getCWidth());
                                } else {
                                    // Do not display axe ot of bounds !!
                                    canPaint = false;
                                }
                            }
                            break;

                        case IChartViewer.VERTICAL_RIGHT:
                            boundRect.setRect(x0 - la - 1, y0, la, getCHeight());
                            if (orientation == IChartViewer.VERTICAL_ORGX) {
                                p0 = transform(0.0, 1.0, xAxis);
                                if ((p0.x >= (x0 - la - 1)) && (p0.x <= x0)) {
                                    tr = p0.x - x0;
                                } else {
                                    // Do not display axe ot of bounds !!
                                    canPaint = false;
                                }
                            }
                            break;

                        case IChartViewer.HORIZONTAL_DOWN:
                        case IChartViewer.HORIZONTAL_UP:
                            boundRect.setRect(x0, y0 - la, getCWidth(), la);
                            if (orientation == IChartViewer.HORIZONTAL_ORGY1
                                    || orientation == IChartViewer.HORIZONTAL_ORGY2) {
                                p0 = xAxis.transform(1.0, 0.0, this);
                                if ((p0.y >= (y0 - la)) && (p0.y <= y0)) {
                                    tr = p0.y - y0;
                                } else {
                                    // Do not display axe out of bounds !!
                                    canPaint = false;
                                }
                            }
                            break;

                        default:
                            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                                    .trace("JLChart warning: Wrong axis position");
                            break;

                    }
                    if (canPaint && visible) {
                        paintAxisDirect(g, frc, x0, y0, back, tr, xAxis.getLength());
                        if (drawOpposite && oppositeVisible) {
                            if (orientation == IChartViewer.VERTICAL_ORGX
                                    || orientation == IChartViewer.HORIZONTAL_ORGY1
                                    || orientation == IChartViewer.HORIZONTAL_ORGY2) {
                                paintAxisOppositeDouble(g, frc, x0, y0, back, tr, xAxis.getLength());
                            } else {
                                paintAxisOpposite(g, frc, x0, y0, back, tr, xAxis.getLength());
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Paint this axis.
     * 
     * @param g
     *            Graphics context
     * @param frc
     *            Font render context
     * @param x0
     *            Axis position
     * @param y0
     *            Axis position
     * @param back
     *            background Color (used to compute subTick color)
     * @param tr
     *            Translation from x0 to axis.
     * @param la
     *            Translation to opposite axis (used by grid).
     */
    public void paintAxisDirect(Graphics g, FontRenderContext frc, int x0, int y0, Color back, int tr, int la) {

        int i, x, y, tickOff, subTickOff;
        BasicStroke bs = null;
        Graphics2D g2 = (Graphics2D) g;

        Color subgridColor = computeMediumColor(labelColor, back);
        if (gridVisible) {
            bs = CometeUtils.createStrokeForLine(1, gridStyle);
        }
        g.setFont(labelFont);

        tickOff = getTickShift(tickLength);
        subTickOff = getTickShift(subtickLength);
        LabelInfo[] temp = labels;
        switch (dOrientation) {

            case IChartViewer.VERTICAL_LEFT:

                // Draw extra sub ticks (outside labels limit)
                if (temp.length > 0 && autoLabeling) {
                    LabelInfo lis = temp[0];
                    LabelInfo lie = temp[temp.length - 1];
                    g.setColor(subgridColor);
                    paintYOutTicks(g, x0 + getCWidth(), y0 + lis.pos - tickStep, y0, la, bs, tr, subTickOff, true);
                    paintYOutTicks(g, x0 + getCWidth(), y0 + lie.pos, y0, la, bs, tr, subTickOff, true);
                }

                for (i = 0; i < temp.length; i++) {

                    // Draw labels
                    g.setColor(labelColor);
                    LabelInfo li = temp[i];

                    x = x0 + (getCWidth() - 4) - li.size.width;
                    y = (int) Math.rint(li.pos) + y0;
                    g.drawString(li.value, x + tr + li.offset.x, y + li.offset.y + li.size.height / 3);

                    // Draw the grid
                    if (gridVisible) {
                        Stroke old = g2.getStroke();
                        if (bs != null) {
                            g2.setStroke(bs);
                        }
                        g.drawLine(x0 + (getCWidth() + 1), y, x0 + (getCWidth() + 1) + la, y);
                        g2.setStroke(old);
                    }

                    if ((autoLabeling && !hasPreciseLabel) || (autoLabeling && hasPreciseLabel && i > 0)) {
                        // Draw sub tick
                        g.setColor(subgridColor);
                        paintYTicks(g, i, x0 + getCWidth(), li.pos + y0, la, bs, tr, subTickOff, true);
                    }

                    // Draw tick
                    g.setColor(labelColor);
                    g.drawLine(x0 + tr + getCWidth() + tickOff, y, x0 + tr + getCWidth() + tickOff + tickLength, y);

                }

                // Draw Axe
                g.setColor(labelColor);
                g.drawLine(x0 + tr + getCWidth(), y0, x0 + tr + getCWidth(), y0 + getCHeight());

                if (title != null) {
                    // Draw vertical title
                    Rectangle2D bounds = labelFont.getStringBounds(title, frc);
                    int fontAscent = (int) (labelFont.getLineMetrics("0", frc).getAscent() + 0.5f);
                    // title coordinates in rotated graphics
                    int rotatedTitleX, rotatedTitleY;
                    switch (titleAlignment) {
                        case IComponent.TOP:
                            rotatedTitleX = -y0 - (int) (bounds.getWidth());
                            break;
                        case IComponent.BOTTOM:
                            rotatedTitleX = -y0 - getCHeight();
                            break;
                        case IComponent.CENTER:
                        default:
                            rotatedTitleX = -y0 - (getCHeight() + (int) (bounds.getWidth())) / 2;
                    }
                    rotatedTitleY = x0 + tr + fontAscent - 2;

                    g2.rotate(-Math.PI / 2.0);
                    g.drawString(title, rotatedTitleX, rotatedTitleY);
                    g2.rotate(Math.PI / 2.0);
                }
                break;

            case IChartViewer.VERTICAL_RIGHT:

                // Draw extra sub ticks (outside labels limit)
                if (temp.length > 0 && autoLabeling) {
                    LabelInfo lis = temp[0];
                    LabelInfo lie = temp[temp.length - 1];
                    g.setColor(subgridColor);
                    paintYOutTicks(g, x0, y0 + lis.pos - tickStep, y0, -la, bs, tr, subTickOff, true);
                    paintYOutTicks(g, x0, y0 + lie.pos, y0, -la, bs, tr, subTickOff, true);
                }

                for (i = 0; i < temp.length; i++) {

                    // Draw labels
                    g.setColor(labelColor);
                    LabelInfo li = temp[i];

                    y = (int) Math.rint(li.pos) + y0;
                    g.drawString(li.value, x0 + tr + li.offset.x + 6, y + li.offset.y + li.size.height / 3);

                    // Draw the grid
                    if (gridVisible) {
                        Stroke old = g2.getStroke();
                        if (bs != null) {
                            g2.setStroke(bs);
                        }
                        g.drawLine(x0, y, x0 - la, y);
                        g2.setStroke(old);
                    }

                    // Draw sub tick
                    if ((autoLabeling && !hasPreciseLabel) || (autoLabeling && hasPreciseLabel && i > 0)) {
                        g.setColor(subgridColor);
                        paintYTicks(g, i, x0, li.pos + y0, -la, bs, tr, subTickOff, true);
                    }

                    // Draw tick
                    g.setColor(labelColor);
                    g.drawLine(x0 + tr + tickOff, y, x0 + tr + tickOff + tickLength, y);
                }

                // Draw Axe
                g.setColor(labelColor);
                g.drawLine(x0 + tr, y0, x0 + tr, y0 + getCHeight());

                if (title != null) {
                    // Draw vertical title
                    Rectangle2D bounds = labelFont.getStringBounds(title, frc);
                    int fontAscent = (int) (labelFont.getLineMetrics(title, frc).getAscent() + 0.5f);
                    // title coordinates in rotated graphics
                    int rotatedTitleX, rotatedTitleY;
                    switch (titleAlignment) {
                        case IComponent.TOP:
                            rotatedTitleX = y0;
                            break;
                        case IComponent.BOTTOM:
                            rotatedTitleX = y0 + getCHeight() - (int) bounds.getWidth();
                            break;
                        case IComponent.CENTER:
                        default:
                            rotatedTitleX = y0 + (getCHeight() - (int) (bounds.getWidth())) / 2;
                    }
                    rotatedTitleY = -x0 - tr - getCWidth() + fontAscent;

                    g2.rotate(Math.PI / 2.0);
                    g.drawString(title, rotatedTitleX, rotatedTitleY);
                    g2.rotate(-Math.PI / 2.0);
                }

                break;

            case IChartViewer.HORIZONTAL_UP:
            case IChartViewer.HORIZONTAL_DOWN:
                if (orientation == IChartViewer.HORIZONTAL_UP) {
                    tr = -la;
                }

                // Draw extra sub ticks (outside labels limit)
                if (temp.length > 0 && autoLabeling) {
                    LabelInfo lis = temp[0];
                    LabelInfo lie = temp[temp.length - 1];
                    g.setColor(subgridColor);
                    paintXOutTicks(g, y0, x0 + lis.pos - tickStep, x0, -la, bs, tr, subTickOff, true);
                    paintXOutTicks(g, y0, x0 + lie.pos, x0, -la, bs, tr, subTickOff, true);
                }

                for (i = 0; i < temp.length; i++) {

                    // Draw labels
                    g.setColor(labelColor);
                    LabelInfo li = temp[i];

                    x = (int) Math.rint(li.pos) + x0;
                    y = y0;
                    if (orientation == IChartViewer.HORIZONTAL_UP) {
                        g.drawString(li.value, x + li.offset.x - li.size.width / 2, y + tr - 2);
                    } else {
                        g.drawString(li.value, x + li.offset.x - li.size.width / 2,
                                y + tr + li.offset.y + li.size.height);
                    }

                    // Draw sub tick
                    if ((autoLabeling && !hasPreciseLabel) || (autoLabeling && hasPreciseLabel && i > 0)) {
                        g.setColor(subgridColor);
                        paintXTicks(g, i, y, li.pos + x0, -la, bs, tr, subTickOff, true);
                    }

                    // Draw the grid
                    g.setColor(labelColor);
                    if (gridVisible) {
                        Stroke old = g2.getStroke();
                        if (bs != null) {
                            g2.setStroke(bs);
                        }
                        g.drawLine(x, y0, x, y0 - la);
                        g2.setStroke(old);
                    }

                    // Draw tick
                    g.setColor(labelColor);
                    g.drawLine(x, y0 + tr + tickOff, x, y0 + tr + tickOff + tickLength);

                }

                // Draw Axe
                g.setColor(labelColor);
                if (csize != null) {
                    g.drawLine(x0, y0 + tr, x0 + csize.width, y0 + tr);
                }

                if (title != null) {
                    // Draw title
                    Rectangle2D bounds = labelFont.getStringBounds(title, frc);
                    int fontAscent = (int) (labelFont.getLineMetrics(title, frc).getAscent() + 0.5f);
                    // title coordinates
                    int titleX, titleY;
                    switch (titleAlignment) {
                        case IComponent.LEFT:
                            titleX = x0;
                            break;
                        case IComponent.RIGHT:
                            titleX = x0 + ((getCWidth()) - (int) bounds.getWidth());
                            break;
                        case IComponent.CENTER:
                        default:
                            titleX = x0 + ((getCWidth()) - (int) bounds.getWidth()) / 2;
                            break;
                    }
                    if (orientation == IChartViewer.HORIZONTAL_UP) {
                        titleY = y0 + tr - (int) bounds.getHeight() - 2;
                    } else {
                        titleY = y0 + tr + (int) bounds.getHeight() + fontAscent;
                    }
                    g.drawString(title, titleX, titleY);
                }

                break;

        }
    }

    public void paintAxisOpposite(Graphics g, FontRenderContext frc, int x0, int y0, Color back, int tr, int la) {

        int i, x, y, tickOff, subTickOff;
        BasicStroke bs = null;

        Color subgridColor = computeMediumColor(labelColor, back);

        tickOff = getTickShiftOpposite(tickLength);
        subTickOff = getTickShiftOpposite(subtickLength);
        LabelInfo[] temp = labels;
        switch (dOrientation) {

            case IChartViewer.VERTICAL_RIGHT:

                int nX0 = x0 - la - getCWidth();

                // Draw extra sub ticks (outside labels limit)
                if (temp.length > 0 && autoLabeling) {
                    LabelInfo lis = temp[0];
                    LabelInfo lie = temp[temp.length - 1];
                    g.setColor(subgridColor);
                    paintYOutTicks(g, nX0 + getCWidth(), y0 + lis.pos - tickStep, y0, la, bs, tr, subTickOff, false);
                    paintYOutTicks(g, nX0 + getCWidth(), y0 + lie.pos, y0, la, bs, tr, subTickOff, false);
                }

                for (i = 0; i < temp.length; i++) {

                    LabelInfo li = temp[i];
                    x = nX0 + (getCWidth() - 4) - li.size.width;
                    y = (int) Math.rint(li.pos) + y0;

                    // Draw sub tick
                    if ((autoLabeling && !hasPreciseLabel) || (autoLabeling && hasPreciseLabel && i > 0)) {
                        g.setColor(subgridColor);
                        paintYTicks(g, i, nX0 + getCWidth(), li.pos + y0, la, bs, tr, subTickOff, false);
                    }

                    // Draw tick
                    g.setColor(labelColor);
                    g.drawLine(nX0 + tr + getCWidth() + tickOff, y, nX0 + tr + getCWidth() + tickOff + tickLength, y);

                }

                // Draw Axe
                g.setColor(labelColor);
                g.drawLine(nX0 + tr + getCWidth(), y0, nX0 + tr + getCWidth(), y0 + getCHeight());
                break;

            case IChartViewer.VERTICAL_LEFT:

                x = x0 + la + getCWidth();

                // Draw extra sub ticks (outside labels limit)
                if (temp.length > 0 && autoLabeling) {
                    LabelInfo lis = temp[0];
                    LabelInfo lie = temp[temp.length - 1];
                    g.setColor(subgridColor);
                    paintYOutTicks(g, x, y0 + lis.pos - tickStep, y0, -la, bs, tr, subTickOff, false);
                    paintYOutTicks(g, x, y0 + lie.pos, y0, -la, bs, tr, subTickOff, false);
                }

                for (i = 0; i < temp.length; i++) {

                    LabelInfo li = temp[i];
                    y = (int) Math.rint(li.pos) + y0;

                    // Draw sub tick
                    if ((autoLabeling && !hasPreciseLabel) || (autoLabeling && hasPreciseLabel && i > 0)) {
                        g.setColor(subgridColor);
                        paintYTicks(g, i, x, li.pos + y0, -la, bs, tr, subTickOff, false);
                    }

                    // Draw tick
                    g.setColor(labelColor);
                    g.drawLine(x + tr + tickOff, y, x + tr + tickOff + tickLength, y);
                }

                // Draw Axe
                g.setColor(labelColor);
                g.drawLine(x + tr, y0, x + tr, y0 + getCHeight());
                break;

            case IChartViewer.HORIZONTAL_UP:
            case IChartViewer.HORIZONTAL_DOWN:

                if (orientation == IChartViewer.HORIZONTAL_DOWN) {
                    tr = -la;
                }

                // Draw extra sub ticks (outside labels limit)
                if (temp.length > 0 && autoLabeling) {
                    LabelInfo lis = temp[0];
                    LabelInfo lie = temp[temp.length - 1];
                    g.setColor(subgridColor);
                    paintXOutTicks(g, y0, x0 + lis.pos - tickStep, x0, -la, bs, tr, subTickOff, false);
                    paintXOutTicks(g, y0, x0 + lie.pos, x0, -la, bs, tr, subTickOff, false);
                }

                for (i = 0; i < temp.length; i++) {

                    LabelInfo li = temp[i];
                    x = (int) Math.rint(li.pos) + x0;
                    y = y0;

                    // Draw sub tick
                    if ((autoLabeling && !hasPreciseLabel) || (autoLabeling && hasPreciseLabel && i > 0)) {
                        g.setColor(subgridColor);
                        paintXTicks(g, i, y, li.pos + x0, -la, bs, tr, subTickOff, false);
                    }

                    // Draw tick
                    g.setColor(labelColor);
                    g.drawLine(x, y0 + tr + tickOff, x, y0 + tr + tickOff + tickLength);

                }

                // Draw Axe
                g.setColor(labelColor);
                g.drawLine(x0, y0 + tr, x0 + getCWidth(), y0 + tr);
                break;

        }

    }

    protected int getCWidth() {
        return (csize == null ? 0 : csize.width);
    }

    protected int getCHeight() {
        return (csize == null ? 0 : csize.height);
    }

    public void paintAxisOppositeDouble(Graphics g, FontRenderContext frc, int x0, int y0, Color back, int tr, int la) {

        int nX0;

        switch (dOrientation) {

            case IChartViewer.VERTICAL_RIGHT:

                nX0 = x0 - la;

                // Draw Axe
                g.setColor(labelColor);
                g.drawLine(nX0, y0, nX0, y0 + getCHeight());

                // Draw Axe
                g.drawLine(nX0 + la, y0, nX0 + la, y0 + getCHeight());
                break;

            case IChartViewer.VERTICAL_LEFT:

                nX0 = x0 + getCWidth();

                // Draw Axe
                g.setColor(labelColor);
                g.drawLine(nX0, y0, nX0, y0 + getCHeight());

                // Draw Axe
                g.drawLine(nX0 + la, y0, nX0 + la, y0 + getCHeight());

                break;

            case IChartViewer.HORIZONTAL_UP:
            case IChartViewer.HORIZONTAL_DOWN:

                // Draw Axe
                g.setColor(labelColor);
                g.drawLine(x0, y0 - la, x0 + getCWidth(), y0 - la);

                // Draw Axe
                g.drawLine(x0, y0, x0 + getCWidth(), y0);
                break;

        }

    }

    /**
     * Apply axis configuration.
     * 
     * @param prefix Axis settings prefix
     * @param f CfFileReader object which contains axis parameters
     * @see JLChart#applyConfiguration
     * @see JLDataView#applyConfiguration
     */
    public void applyConfiguration(String prefix, CfFileReader f) {
        List<String> p;
        p = f.getParam(prefix + "visible");
        if (p != null) {
            setVisible(OFormat.getBoolean(p.get(0).toString()));
        }
        p = f.getParam(prefix + "grid");
        if (p != null) {
            setGridVisible(OFormat.getBoolean(p.get(0).toString()));
        }
        p = f.getParam(prefix + "subgrid");
        if (p != null) {
            setSubGridVisible(OFormat.getBoolean(p.get(0).toString()));
        }
        p = f.getParam(prefix + "grid_style");
        if (p != null) {
            setGridStyle(OFormat.getInt(p.get(0).toString()));
        }
        p = f.getParam(prefix + "min");
        if (p != null) {
            setMinimum(OFormat.getDouble(p.get(0).toString()));
        }
        p = f.getParam(prefix + "max");
        if (p != null) {
            setMaximum(OFormat.getDouble(p.get(0).toString()));
        }
        p = f.getParam(prefix + "autoscale");
        if (p != null) {
            setAutoScale(OFormat.getBoolean(p.get(0).toString()));
        }
        p = f.getParam(prefix + "scale");
        if (p != null) {
            setScale(OFormat.getInt(p.get(0).toString()));
        } else {
            // To handle a bug in older version
            p = f.getParam(prefix + "cale");
            if (p != null) {
                setScale(OFormat.getInt(p.get(0).toString()));
            }
        }
        p = f.getParam(prefix + "format");
        if (p != null) {
            setLabelFormat(OFormat.getInt(p.get(0).toString()));
        }
        p = f.getParam(prefix + "title");
        if (p != null) {
            setTitle(OFormat.getName(p.get(0).toString()));
        }
        p = f.getParam(prefix + "color");
        if (p != null) {
            setAxisColor(OFormat.getColor(p));
        }
        p = f.getParam(prefix + "label_font");
        if (p != null) {
            setFont(OFormat.getFont(p));
        }
        p = f.getParam(prefix + "fit_display_duration");
        if (p != null) {
            this.setFitXAxisToDisplayDuration(OFormat.getBoolean(p.get(0).toString()));
        }

    }

    /**
     * Builds a configuration string that can be write into a file and is compatible with CfFileReader.
     * 
     * @param prefix Axis settings prefix
     * @return A string containing param
     * @see JLAxis#applyConfiguration
     * @see JLChart#getConfiguration
     * @see JLDataView#getConfiguration
     */
    public String getConfiguration(String prefix) {

        String to_write = ObjectUtils.EMPTY_STRING;

        to_write += prefix + "visible:" + isVisible() + ObjectUtils.NEW_LINE;
        to_write += prefix + "grid:" + isGridVisible() + ObjectUtils.NEW_LINE;
        to_write += prefix + "subgrid:" + isSubGridVisible() + ObjectUtils.NEW_LINE;
        to_write += prefix + "grid_style:" + getGridStyle() + ObjectUtils.NEW_LINE;
        to_write += prefix + "min:" + getMinimum() + ObjectUtils.NEW_LINE;
        to_write += prefix + "max:" + getMaximum() + ObjectUtils.NEW_LINE;
        to_write += prefix + "autoscale:" + isAutoScale() + ObjectUtils.NEW_LINE;
        to_write += prefix + "scale:" + getScale() + ObjectUtils.NEW_LINE;
        to_write += prefix + "format:" + getLabelFormat() + ObjectUtils.NEW_LINE;
        to_write += prefix + "title:\'" + getTitle() + "\'\n";
        to_write += prefix + "color:" + OFormat.color(getAxisColor()) + ObjectUtils.NEW_LINE;
        to_write += prefix + "label_font:" + OFormat.font(getFont()) + ObjectUtils.NEW_LINE;
        to_write += prefix + "fit_display_duration:" + isFitXAxisToDisplayDuration() + ObjectUtils.NEW_LINE;

        return to_write;
    }

    /**
     * Allows user to know if the 0 value will always be visible in case of auto
     * scale
     * 
     * @return a boolean value
     */
    public boolean isZeroAlwaysVisible() {
        return zeroAlwaysVisible;
    }

    /**
     * Sets if 0 must always be visible in case of auto scale or not
     * 
     * @param zeroAlwaysVisible
     *            a boolean value. True for always visible, false otherwise.
     */
    public void setZeroAlwaysVisible(boolean zeroAlwaysVisible) {
        this.zeroAlwaysVisible = zeroAlwaysVisible;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    /**
     * Sets date format chen chosen label format is DATE_FORMAT
     * 
     * @param dateFormat
     * @see #US_DATE_FORMAT
     * @see #FR_DATE_FORMAT
     */
    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public void setAutoLabeling(boolean autoLabeling) {
        this.autoLabeling = autoLabeling;
    }

    /**
     * @deprecated Use {@link ChartUtils#appendAxisConfigurationHelpString(StringBuilder)} instead
     */
    @Deprecated
    public static String getHelpString() {
        return ChartUtils.appendAxisConfigurationHelpString(null).toString();
    }

}

// ///////////// //
// Inner classes //
// ///////////// //

// Inner class to handle label info
class LabelInfo implements Serializable {

    private static final long serialVersionUID = 2237705800113238437L;

    public String value;
    public Dimension size;
    public double pos;
    public Point offset;

    public LabelInfo(String lab, int w, int h, double pos) {
        value = lab;
        size = new Dimension(w, h);
        this.pos = pos;
        offset = new Point(0, 0);
    }

    public void setOffset(int x, int y) {
        offset.x = x;
        offset.y = y;
    }

    public int getWidth() {
        return size.width + Math.abs(offset.x);
    }

    public int getHeight() {
        return size.height + Math.abs(offset.y);
    }

}
