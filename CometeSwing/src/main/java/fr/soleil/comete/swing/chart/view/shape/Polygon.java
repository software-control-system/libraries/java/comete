/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.shape;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;

import fr.soleil.comete.definition.widget.util.IChartConst.ShapeType;

/**
 * {@link AbstractShape} specialized in polygons
 * 
 * @author huriez
 */
public class Polygon extends AbstractShape {

    private int[] xPoint;
    private int[] yPoint;
    private int nbPoint;
    private boolean fill;
    private Paint pattern;

    public Polygon(ShapeType type) {
        super(type);
    }

    public Polygon(ShapeType type, int[] point, int[] point2, int nbPoint, boolean fill, Color color) {
        super(type);
        xPoint = point;
        yPoint = point2;
        this.nbPoint = nbPoint;
        this.fill = fill;
        super.color = color;
    }

    /**
     * @return the nbPoint
     */
    public int getNbPoint() {
        return nbPoint;
    }

    /**
     * @param nbPoint the nbPoint to set
     */
    public void setNbPoint(int nbPoint) {
        this.nbPoint = nbPoint;
    }

    /**
     * @return the fill
     */
    public boolean isFill() {
        return fill;
    }

    /**
     * @param fill the fill to set
     */
    public void setFill(boolean fill) {
        this.fill = fill;
    }

    /**
     * @return the xPoint
     */
    public int[] getXPoint() {
        return xPoint;
    }

    /**
     * @return the yPoint
     */
    public int[] getYPoint() {
        return yPoint;
    }

    /**
     * @param pattern the pattern to set
     */
    public void setPattern(Paint pattern) {
        this.pattern = pattern;
    }

    /**
     * @return the pattern
     */
    public Paint getPattern() {
        return pattern;
    }

    @Override
    public void drawShape(Graphics2D g) {
        Paint oldPaint = null;
        g.setColor(color);
        if (pattern != null) {
            oldPaint = g.getPaint();
            g.setPaint(pattern);
        }
        g.translate(x, y);
        if (fill) {
            g.fillPolygon(xPoint, yPoint, nbPoint);
        } else {
            g.drawPolygon(xPoint, yPoint, nbPoint);
        }
        g.translate(-x, -y);
        if (oldPaint != null) {
            g.setPaint(oldPaint);
        }
    }

}
