/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.undo;

import java.util.ResourceBundle;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import ij.gui.Roi;

/**
 * 
 * 
 * @author MAINGUY
 * 
 */
public class DeleteRoiEdit extends AbstractUndoableEdit {

    private static final long serialVersionUID = 6298508946639923089L;

    protected static final ResourceBundle MESSAGES = ResourceBundle
            .getBundle("fr.soleil.comete.swing.image.ijviewer.messages");

    protected IJRoiManager roiManager;
    protected Roi roiDeleted;
    protected boolean isInner = false;
    protected boolean isOuter = false;
    protected boolean isSelected = false;

    /**
     * 
     */
    public DeleteRoiEdit(IJRoiManager aRoiManager, Roi aRoi, boolean inner, boolean outer, boolean selected) {
        roiManager = aRoiManager;
        roiDeleted = aRoi;
        if (roiManager != null && aRoi != null) {
            isInner = inner;
            isOuter = outer;
            isSelected = selected;
        }
    }

    @Override
    public void undo() throws CannotUndoException {
        super.undo();
        roiManager.addRoi(roiDeleted, true);
        if (isInner) {
            roiManager.setInner(roiDeleted);
        }
        if (isOuter) {
            roiManager.setOuter(roiDeleted);
        }
        if (isSelected) {
            roiManager.selectRoi(roiDeleted);
        }
    }

    @Override
    public void redo() throws CannotRedoException {
        super.redo();
        roiManager.deleteRoi(roiDeleted, true);
    }

    @Override
    public String getPresentationName() {
        return MESSAGES.getString("ImageJViewer.UndoManagement.DeleteRoi")/*
                                                                           * +" "
                                                                           * +
                                                                           * roiDeleted
                                                                           * .
                                                                           * getName
                                                                           * ()
                                                                           */;
    }

}
