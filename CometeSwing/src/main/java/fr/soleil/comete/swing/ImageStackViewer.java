/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.lang.reflect.Array;
import java.util.Arrays;

import javax.swing.JComponent;

import fr.soleil.comete.definition.data.target.matrix.INumberMatrixComponent;
import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.data.target.cube.INumberCubeTarget;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

public class ImageStackViewer extends AbstractPanel implements INumberMatrixComponent, INumberCubeTarget, ITextTarget {

    private static final long serialVersionUID = 5605313812073677666L;

    protected final ImageViewer imageViewer;
    protected final PositionView positionView;

    protected String imageName;
    protected Object[] stackData;
    protected IValueConvertor[] scaleConvertors;

    public ImageStackViewer() {
        scaleConvertors = null;
        imageViewer = new ImageViewer();
        positionView = new PositionView() {
            private static final long serialVersionUID = 6397011055864780667L;

            @Override
            protected void notifySubPositionChanged(int dimIndex, int position) {
                super.notifySubPositionChanged(dimIndex, position);
                applyPosition();
            }

            @Override
            protected void notifyFullPositionChanged(int... position) {
                super.notifyFullPositionChanged(position);
                applyPosition();
            }
        };
        setLayout(new BorderLayout());
        add((JComponent) imageViewer, BorderLayout.CENTER);
        add((JComponent) positionView, BorderLayout.SOUTH);
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setCometeBackground(color);
        positionView.setCometeBackground(color);
        imageViewer.setCometeBackground(color);
    }

    public IValueConvertor[] getScaleConvertors() {
        return scaleConvertors == null ? null : scaleConvertors.clone();
    }

    public void setScaleConvertors(IValueConvertor[] scaleConvertors) {
        this.scaleConvertors = scaleConvertors;
        if ((scaleConvertors == null) || (scaleConvertors.length < 2)) {
            positionView.setValueConvertors(null);
            imageViewer.setXAxisConvertor(null, false);
            imageViewer.setYAxisConvertor(null, true);
        } else if (scaleConvertors.length == 2) {
            imageViewer.setYAxisConvertor(scaleConvertors[0], false);
            imageViewer.setXAxisConvertor(scaleConvertors[1], true);
        } else {
            IValueConvertor[] positionConvertors = Arrays.copyOf(scaleConvertors, scaleConvertors.length - 2);
            imageViewer.setYAxisConvertor(scaleConvertors[scaleConvertors.length - 2], false);
            imageViewer.setXAxisConvertor(scaleConvertors[scaleConvertors.length - 1], false);
            positionView.setValueConvertors(positionConvertors);
            repaint();
        }
    }

    public ImageViewer getImageViewer() {
        return imageViewer;
    }

    @Override
    public Object[] getNumberMatrix() {
        // not managed
        return null;
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        int[] shape = ArrayUtils.recoverShape(value);
        stackData = value;
        if (shape == null) {
            positionView.setStackShape(new int[0]);
            imageViewer.setNumberMatrix(null);
            imageViewer.setImageName(imageName == null ? ObjectUtils.EMPTY_STRING : imageName);
        } else if (shape.length < 3) {
            positionView.setStackShape(new int[0]);
            imageViewer.setNumberMatrix(value);
            imageViewer.setImageName(imageName == null ? ObjectUtils.EMPTY_STRING : imageName);
        } else {
            shape = Arrays.copyOf(shape, shape.length - 2);
            boolean same = ObjectUtils.sameObject(shape, positionView.getStackShape());
            positionView.setStackShape(shape);
            if (same) {
                applyPosition();
            }
        }
    }

    protected void applyPosition() {
        int[] position = positionView.getPositions();
        Object[] imageData = stackData;
        if (position != null) {
            try {
                for (int index : position) {
                    imageData = (Object[]) Array.get(imageData, index);
                }
            } catch (Exception e) {
                imageData = null;
            }
        }
        imageViewer.setNumberMatrix(imageData);
        applyName(position);
    }

    protected void applyName(int[] position) {
        StringBuilder nameBuilder = new StringBuilder();
        String name = imageName;
        if (name != null) {
            nameBuilder.append(name);
        }
        if ((position != null) && (position.length > 0)) {
            nameBuilder.append(Arrays.toString(position));
        }
        imageViewer.setImageName(nameBuilder.toString());
    }

    @Override
    public Object getFlatNumberMatrix() {
        // not managed
        return null;
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        Object[] convertedData;
        if (value == null) {
            convertedData = null;
        } else {
            try {
                convertedData = (Object[]) ArrayUtils.convertArrayDimensionFrom1ToN(value, height, width);
            } catch (Exception e) {
                convertedData = null;
            }
        }
        setNumberMatrix(convertedData);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        // prefer multi-dimension arrays to flat arrays
        return false;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        return imageViewer.getMatrixDataWidth(concernedDataClass);
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return imageViewer.getMatrixDataHeight(concernedDataClass);
    }

    @Override
    public Object[][] getNumberCube() {
        // not managed
        return null;
    }

    @Override
    public void setNumberCube(Object[][] data) {
        setNumberMatrix(data);
    }

    @Override
    public String getText() {
        return imageName;
    }

    @Override
    public void setText(String text) {
        this.imageName = text;
        applyName(positionView.getPositions());
    }

    public String getStackName() {
        return positionView.getTitle();
    }

    public void setStackName(String name) {
        positionView.setTitle(name);
    }

}
