/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.util.IllegalFormatConversionException;
import java.util.Locale;

import fr.soleil.lib.project.Format;

public class SwingFormat extends Format {

    public static String sprintf(String format, Object... data) {
        String formattedValue = null;
        try {
            formattedValue = String.format(Locale.US, format, data);
        } catch (IllegalFormatConversionException e) {
            formattedValue = null;
        }
        return formattedValue;
    }

}
