/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import javax.swing.JOptionPane;

import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IDialog;

public class Dialog extends Component implements IDialog {

    private static final long serialVersionUID = -7897754366219271606L;

    @Override
    public int showConfirmDialog(IComponent parent, String message, String title, int option) {
        if ((parent == null) || (parent instanceof java.awt.Component)) {
            return JOptionPane.showConfirmDialog((java.awt.Component) parent, message, title, option);
        }
        return JOptionPane.NO_OPTION;
    }

    @Override
    public void showMessageDialog(IComponent parent, String message) {
        if ((parent == null) || (parent instanceof java.awt.Component)) {
            JOptionPane.showMessageDialog((java.awt.Component) parent, message);
        }
    }

    @Override
    public String showInputDialog(IComponent parent, String message, String title, int messageType) {
        if ((parent == null) || (parent instanceof java.awt.Component)) {
            return JOptionPane.showInputDialog((java.awt.Component) parent, message, title, messageType);
        }
        return null;
    }
}
