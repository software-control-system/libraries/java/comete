/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Component;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.lib.project.swing.panel.LoadingPanel;

/**
 * A {@link LoadingPanel} that is also an {@link IBooleanTarget}, in order to be set as loading through Comete
 * architecture
 * 
 * @param <C> The type of {@link Component} this panel contains
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TargetLoadingPanel<C extends Component> extends LoadingPanel<C> implements IBooleanTarget {

    private static final long serialVersionUID = -5114171122537409499L;

    public TargetLoadingPanel() {
        super();
    }

    public TargetLoadingPanel(C component) {
        super(component);
    }

    @Override
    public boolean isSelected() {
        return isLoading();
    }

    @Override
    public void setSelected(boolean selected) {
        setLoading(selected);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed: no mediator to notify
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed: no mediator to notify
    }

}
