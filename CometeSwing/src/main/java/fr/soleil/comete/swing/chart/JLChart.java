/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JToolTip;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.EventListenerList;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.awt.util.ColorRotationTool;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.event.ChartViewerEvent.Reason;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.util.TransferEventDelegate;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IComboBox;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.comete.definition.widget.properties.OffsetProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.comete.definition.widget.util.IChartConst;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.DataList;
import fr.soleil.comete.swing.chart.data.SearchInfo;
import fr.soleil.comete.swing.chart.expression.ExpressionParser;
import fr.soleil.comete.swing.chart.expression.ExpressionParserOption;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.chart.util.DataViewComparator;
import fr.soleil.comete.swing.chart.util.IDataViewManager;
import fr.soleil.comete.swing.chart.util.ITableRowListener;
import fr.soleil.comete.swing.chart.util.JTableRow;
import fr.soleil.comete.swing.chart.util.JTableRowEvent;
import fr.soleil.comete.swing.chart.util.TabbedLine;
import fr.soleil.comete.swing.util.CfFileReader;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.JFileChooserFilter;
import fr.soleil.comete.swing.util.OFormat;
import fr.soleil.comete.swing.util.SwingFormat;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.expression.IExpressionParser;
import fr.soleil.lib.project.expression.jeval.JEvalExpressionParser;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.swing.file.MultiExtFileFilter;

/**
 * A class to handle 2D graphics plot.
 * 
 * @author JL Pons
 */
@Deprecated
public class JLChart extends JComponent implements IDataViewManager, MouseListener, MouseMotionListener, ActionListener,
        ITableRowListener, ComponentListener {

    private static final long serialVersionUID = -8356672717704151215L;

    // constant
    public final static String CHANGE_LABEL_PLACEMENT = "chkChange Label Placement";
    public final static String CHANGE_LABEL_VISIBILITY = "chkChange Label Visibility";
    public final static String CHART_TO_IMAGE_FILE = "Save a snapshot of this chart";
    public final static String PRINT_CHART = "Print chart";

    /* Chart properties menu item */
    public static final int MENU_CHARTPROP = 0;
    /* Data view properties menu */
    public static final int MENU_DVPROP = 1;
    /* Show table menu */
    public static final int MENU_TABLE = 2;
    /* Save data file menu item */
    public static final int MENU_DATASAVE = 3;
    /* print graph menu item */
    public static final int MENU_PRINT = 4;
    /* Statistics menu */
    public static final int MENU_STAT = 5;

    /* Labels for math expressions functions */
    protected final static String SHOW_DATE = "chkShow date";

    /*
     * Maximum of dataviews shown in the contextual menu before using DialogList
     * to choose them
     */
    private int maxDataViewShown = 15;

    private String header = null;
    private boolean headerVisible = false;
    private Font headerFont;
    private Color headerColor;

    private boolean labelVisible = true;
    private int labelMode = IChartViewer.LABEL_ROW;
    private Font labelFont;

    private boolean ipanelVisible = false;
    private boolean paintAxisFirst = false;
    private Color chartBackground;

    private double displayDuration;
    protected double maxDisplayDuration;

    protected JPopupMenu chartMenu;
    private final JMenuItem optionMenuItem;
    private final JMenuItem loadDataMenuItem;
    private final JMenuItem saveFileMenuItem;
    private final JMenuItem zoomBackMenuItem;
    private final JMenuItem printMenuItem;
    private final JSeparator sepMenuItem;

    private final JSeparator expressionSeparator;
    private final JMenuItem expressionMenuItem;
    private final JMenuItem expressionClearMenuItem;
    private final JMenuItem configurationLoadMenuItem;

    private final JMenu tableMenu;
    private final JMenuItem tableAllMenuItem;
    private JMenuItem[] tableSingleMenuItem = new JMenuItem[0];
    private final JMenuItem tableDialogMenuItem;

    private final JMenu dvMenu;
    private JMenuItem[] dvMenuItem = new JMenuItem[0];
    private final JMenuItem dvDialogMenuItem;

    private final JMenu statMenu;
    private final JMenuItem statAllMenuItem;
    private JMenuItem[] statSingleMenuItem = new JMenuItem[0];
    private final JMenuItem statDialogMenuItem;

    private JMenuItem[] userActionMenuItem;
    private String[] userAction;

    private boolean zoomDrag;
    private boolean zoomDragAllowed;

    private boolean areaZoom;
    private boolean isZoomed;
    private boolean oldAutoScaleY1;
    private boolean oldAutoScaleY2;
    private double oldMinY1;
    private double oldMaxY1;
    private double oldMinY2;
    private double oldMaxY2;
    private double oldTime;

    private int zoomX;
    private int zoomY;
    private final JButton zoomButton;

    private int lastX;
    private int lastY;
    private SearchInfo lastSearch;

    // Measurements stuff
    private final Rectangle headerR;
    private final Rectangle viewR;
    private Dimension margin;
    private int headerWidth;
    private int axisHeight;
    private int axisWidth;
    private int y1AxisThickness;
    private int y2AxisThickness;
    private int xAxisThickness;
    private int xAxisUpMargin;

    // Axis
    private final JLAxis xAxis;
    private final JLAxis y1Axis;
    private final JLAxis y2Axis;

    // Custom DataView information
    private boolean cyclingCustomMap = true;
    private final LinkedList<Color> customColor = new LinkedList<Color>();
    private final LinkedList<Color> customDataViewFillColor = new LinkedList<Color>();
    private final LinkedList<Color> customDataViewMarkerColor = new LinkedList<Color>();
    private final LinkedList<Integer> customDataViewLineStyle = new LinkedList<Integer>();
    private final LinkedList<Integer> customDataViewAxis = new LinkedList<Integer>();
    private final LinkedList<Integer> customDataViewMarkerStyle = new LinkedList<Integer>();
    private final LinkedList<Integer> customDataViewMarkerSize = new LinkedList<Integer>();
    private final LinkedList<Integer> customDataViewLineWidth = new LinkedList<Integer>();
    private final LinkedList<Integer> customDataViewBarWidth = new LinkedList<Integer>();
    private final LinkedList<Integer> customDataViewStyle = new LinkedList<Integer>();
    private final LinkedList<Integer> customDataFillStyle = new LinkedList<Integer>();
    private final LinkedList<Integer> customDataFillMethod = new LinkedList<Integer>();
    private final LinkedList<String> customDataViewUserFormat = new LinkedList<String>();
    private final LinkedList<String> customDataViewUnit = new LinkedList<String>();

    // DataView informations
    private final Map<String, Color> dataViewColor = new HashMap<String, Color>();
    private final Map<String, Color> dataViewFillColor = new HashMap<String, Color>();
    private final Map<String, Color> dataViewMarkerColor = new HashMap<String, Color>();
    private final Map<String, String> dataViewDisplayName = new HashMap<String, String>();
    private final Map<String, Integer> dataViewLineStyle = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewAxis = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewMarker = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewMarkerSize = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewLineWidth = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewBarWidth = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewStyle = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewFillStyle = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewFillMethod = new HashMap<String, Integer>();
    private final Map<String, Double> dataViewTransformA0 = new HashMap<String, Double>();
    private final Map<String, Double> dataViewTransformA1 = new HashMap<String, Double>();
    private final Map<String, Double> dataViewTransformA2 = new HashMap<String, Double>();
    private final Map<String, Double> dataViewOffsetXA0 = new HashMap<String, Double>();
    private final Map<String, Integer> dataViewInterpolationMethod = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewInterpolationStep = new HashMap<String, Integer>();
    private final Map<String, Double> dataViewHermiteBias = new HashMap<String, Double>();
    private final Map<String, Double> dataViewHermiteTension = new HashMap<String, Double>();
    private final Map<String, Integer> dataViewSmoothingMethod = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewSmoothingNeighbors = new HashMap<String, Integer>();
    private final Map<String, Double> dataViewSmoothingGaussSigma = new HashMap<String, Double>();
    private final Map<String, Integer> dataViewSmoothingExtrapolation = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewMathFunction = new HashMap<String, Integer>();
    private final Map<String, Integer> dataViewHighlightMethod = new HashMap<String, Integer>();
    private final Map<String, Double> dataViewHighlightCoefficient = new HashMap<String, Double>();
    private final Map<String, Boolean> dataViewEditable = new HashMap<String, Boolean>();
    private final Map<String, Boolean> dataViewClickable = new HashMap<String, Boolean>();
    private final Map<String, Boolean> dataViewLabelVisible = new HashMap<String, Boolean>();
    private final Map<String, Boolean> dataViewSamplingAllowed = new HashMap<String, Boolean>();
    private final Map<String, Boolean> dataViewHighlighted = new HashMap<String, Boolean>();
    private final Map<String, String> dataViewUserFormat = new HashMap<String, String>();
    private final Map<String, String> dataViewUnit = new HashMap<String, String>();
    private final Map<String, JLDataView> loadDataView = new HashMap<String, JLDataView>();

    protected final Map<String, JLDataView> visibleDataView = new HashMap<String, JLDataView>();
    protected final Map<String, JLDataView> hiddenDataView = new HashMap<String, JLDataView>();
    private final IComboBox hiddenDataViewCombo = new ComboBox();
    private final IComboBox visibleDataViewCombo = new ComboBox();

    private boolean mathExpressionEnabled = true;

    /* Used to allow or not the use of X axis JLDataView with expressions */
    private boolean useXViewsWithExpressions = true;

    /* Used to allow or not to put expressions on X Axis */
    private boolean canPutExpressionOnX = true;

    // Used to save expressions in files, but can be used to dynamically update
    // the expression JLDataViews.
    // Key : The expression JLDataView
    // Associated value : Object[]
    // 0 : Integer <-> axis
    // 1 : String <-> expression
    // 2 : Boolean <-> x
    // 3..length-1 : variables
    protected HashMap<JLDataView, Object[]> expressionMap;

    // Listeners
    private final List<IChartViewerListener> chartListeners = new ArrayList<IChartViewerListener>();

    // Table
    private JTableRow theTable = null;

    // Timestamp precision to build the table. Positive int. The lower, the more
    // precise
    private int timePrecision = 0;

    // Menu item to allow user to set time precision
    private final JMenuItem precisionMenuItem;

    // Menu item to save a snapshot of this chart
    private final JMenuItem saveSnapshotMenuItem;
    private final JMenuItem makeSnapshotMenuItem;
    private final JMenuItem clearSnapshotMenuItem;

    // Elements to save snapshots of the first DataView
    private final List<JLDataView> snapshots;

    // Elements to save for using DialogList option
    private List<JLDataView> dialogListSave;

    private boolean showAllMode = false;
    private JLDataView currentViewShown = null;
    private ITableRowListener tableListener = null;
    protected boolean preferDialog = false, modalDialog = false;
    protected Window tableWindow = null;

    protected String noValueString = "*";

    private String indexColumnName = TabbedLine.DEFAULT_INDEX_COLUMN_NAME;

    private String dateColumnName = TabbedLine.DEFAULT_DATE_COLUMN_NAME;

    private boolean editable = false;

    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    protected Map<String, Object> lastDatas = null;
    protected JDialog dataViewEditor;
    protected JLDataViewOption dataViewOption;
    protected JDialog expressionEditor;
    protected ExpressionParserOption expressionParserOption;
    protected JDialog chartEditor;
    protected JLChartOption chartOption;
    protected Comparator<AbstractDataView> dataViewComparator;

    protected boolean autoHighlightOnLegend = false;
    protected boolean dataViewRemovingEnabled = false;
    protected boolean cleanDataViewConfigurationOnRemoving = false;

    private String displayableTooltipText = null;
    private MouseEvent displayableTooltipEvent = null;
    private final JToolTip infoTooltip;
    private Popup infoPopup = null;
    private final static Color INFO_PANEL_BACKGROUND = new Color(255, 247, 200);
    private final static Border INFO_PANEL_BORDER = new LineBorder(Color.BLACK, 1);
    private final static Insets TOOLTIP_INSETS = new Insets(2, 2, 2, 2);

    private ArrayList<String> selectedIds;

    protected boolean limitDrawingZone = true;
    private boolean autoHideViews;
    private IChartViewer parent;

    private boolean initDataViewComboBoxes;

    private final ColorRotationTool colorRotationTool;

    private boolean dataViewsSortedOnX;

    private FontRenderContext lastFontRenderContext;
    private int lastWidth;
    private int lastHeight;

    private TransferEventDelegate transferEventDelegate;
    protected IExpressionParser expressionParser;

    /**
     * Graph constructor.
     */
    public JLChart(IChartViewer parent) {
        this(parent, true);
    }

    public JLChart(IChartViewer parent, boolean useNiceScale) {
        super();
        dataViewComparator = new DataViewComparator();
        lastFontRenderContext = null;
        lastWidth = 0;
        lastHeight = 0;
        this.parent = parent;
        initDataViewComboBoxes = true;
        colorRotationTool = new ColorRotationTool();
        infoTooltip = new JToolTip() {

            private static final long serialVersionUID = 7496937450964735288L;

            @Override
            public Insets getInsets() {
                return TOOLTIP_INSETS;
            }

        };
        infoTooltip.setComponent(this);
        infoTooltip.setBackground(INFO_PANEL_BACKGROUND);
        infoTooltip.setBorder(INFO_PANEL_BORDER);
        Color defColor = Color.WHITE;
        setBackground(defColor);
        setChartBackground(defColor);
        setForeground(Color.black);
        setOpaque(true);
        setFont(new Font("Dialog", Font.PLAIN, 12));

        headerFont = getFont();
        headerColor = getForeground();
        labelFont = getFont();

        margin = new Dimension(5, 5);
        headerR = new Rectangle(0, 0, 0, 0);
        viewR = new Rectangle(0, 0, 0, 0);

        xAxis = new JLAxis(this, IChartViewer.HORIZONTAL_DOWN, IChartViewer.X, useNiceScale);
        xAxis.setAnnotation(JLAxis.TIME_ANNO);
        xAxis.setAutoScale(true);
        xAxis.setAxeName("(X)");
        y1Axis = new JLAxis(this, IChartViewer.VERTICAL_LEFT, IChartViewer.Y1, useNiceScale);
        y1Axis.setAxeName("(Y1)");
        y2Axis = new JLAxis(this, IChartViewer.VERTICAL_RIGHT, IChartViewer.Y2, useNiceScale);
        y2Axis.setAxeName("(Y2)");
        displayDuration = Double.POSITIVE_INFINITY;
        maxDisplayDuration = Double.POSITIVE_INFINITY;

        zoomDrag = false;
        zoomDragAllowed = false;

        chartMenu = new JPopupMenu();

        optionMenuItem = new JMenuItem("Chart properties");
        optionMenuItem.addActionListener(this);

        saveFileMenuItem = new JMenuItem("Save data File");
        saveFileMenuItem.addActionListener(this);

        tableMenu = new JMenu("Show table");
        tableAllMenuItem = new JMenuItem("All");
        tableAllMenuItem.addActionListener(this);
        tableDialogMenuItem = new JMenuItem("Select View...");
        tableDialogMenuItem.addActionListener(this);

        zoomBackMenuItem = new JMenuItem("Zoom back");
        zoomBackMenuItem.addActionListener(this);

        printMenuItem = new JMenuItem(PRINT_CHART);
        printMenuItem.addActionListener(this);

        precisionMenuItem = new JMenuItem("Abscissa error margin");
        precisionMenuItem.addActionListener(this);

        saveSnapshotMenuItem = new JMenuItem(CHART_TO_IMAGE_FILE);
        saveSnapshotMenuItem.addActionListener(this);

        makeSnapshotMenuItem = new JMenuItem("Freeze values");
        makeSnapshotMenuItem.addActionListener(this);

        clearSnapshotMenuItem = new JMenuItem("Clear Freezes");
        clearSnapshotMenuItem.addActionListener(this);

        loadDataMenuItem = new JMenuItem("Load data");
        loadDataMenuItem.addActionListener(this);

        dvMenu = new JMenu("Data View properties");
        dvDialogMenuItem = new JMenuItem("Select View...");
        dvDialogMenuItem.addActionListener(this);

        statMenu = new JMenu("Show statistics");
        statAllMenuItem = new JMenuItem("All");
        statAllMenuItem.addActionListener(this);
        statDialogMenuItem = new JMenuItem("Select View...");
        statDialogMenuItem.addActionListener(this);

        expressionSeparator = new JSeparator();

        expressionMenuItem = new JMenuItem("Evaluate an expression");
        expressionMenuItem.addActionListener(this);

        expressionClearMenuItem = new JMenuItem("Clear all expressions");
        expressionClearMenuItem.addActionListener(this);

        configurationLoadMenuItem = new JMenuItem("Load configuration");
        configurationLoadMenuItem.addActionListener(this);

        /*
         * infoMenuItem = new JMenuItem("Chart menu");
         * infoMenuItem.setEnabled(false); chartMenu.add(infoMenuItem);
         * chartMenu.add(new JSeparator());
         */

        chartMenu.add(zoomBackMenuItem);
        chartMenu.add(new JSeparator());
        chartMenu.add(optionMenuItem);
        chartMenu.add(dvMenu);
        chartMenu.add(tableMenu);
        chartMenu.add(statMenu);
        chartMenu.add(new JSeparator());
        chartMenu.add(saveFileMenuItem);
        chartMenu.add(loadDataMenuItem);
        chartMenu.add(printMenuItem);
        chartMenu.add(precisionMenuItem);
        chartMenu.add(new JSeparator());
        chartMenu.add(saveSnapshotMenuItem);
        chartMenu.add(makeSnapshotMenuItem);
        chartMenu.add(clearSnapshotMenuItem);
        if (this.isMathExpressionEnabled()) {
            chartMenu.add(expressionSeparator);
            chartMenu.add(expressionMenuItem);
            chartMenu.add(expressionClearMenuItem);
            chartMenu.add(configurationLoadMenuItem);
        }

        sepMenuItem = new JSeparator();
        chartMenu.add(sepMenuItem);

        userActionMenuItem = new JMenuItem[0];
        userAction = new String[0];
        sepMenuItem.setVisible(false);

        // Set up listeners
        addMouseListener(this);
        addMouseMotionListener(this);

        listenerList = new EventListenerList();

        zoomButton = new JButton("Zoom back");
        zoomButton.setFont(labelFont);
        zoomButton.setMargin(new Insets(2, 2, 1, 1));
        zoomButton.setVisible(false);
        zoomButton.addActionListener(this);
        add(zoomButton);

        areaZoom = false;
        isZoomed = false;

        snapshots = new ArrayList<JLDataView>();
        dialogListSave = new ArrayList<JLDataView>();

        expressionMap = new HashMap<JLDataView, Object[]>();
        // this.addJLChartActionListener(this);

        transferEventDelegate = null;

        initComponent();
        dataViewsSortedOnX = true;
        addComponentListener(this);
    }

    @Override
    public Comparator<AbstractDataView> getDataViewComparator() {
        return dataViewComparator;
    }

    @Override
    public void setDataViewComparator(Comparator<AbstractDataView> dataViewComparator) {
        this.dataViewComparator = (dataViewComparator == null ? new DataViewComparator() : dataViewComparator);
        initComboList();
    }

    /**
     * Returns the {@link IExpressionParser} used by this {@link JLChart} to parse expressions
     * 
     * @return An {@link IExpressionParser}
     */
    public IExpressionParser getExpressionParser() {
        return expressionParser;
    }

    /**
     * Sets the {@link IExpressionParser} this {@link JLChart} can use to parse expressions
     * 
     * @param expressionParser The {@link IExpressionParser} this {@link JLChart} can use to parse expressions
     */
    public void setExpressionParser(IExpressionParser expressionParser) {
        this.expressionParser = (expressionParser == null ? new JEvalExpressionParser() : expressionParser);
    }

    /**
     * @return the transferEventDelegate
     */
    public TransferEventDelegate getTransferEventDelegate() {
        return transferEventDelegate;
    }

    /**
     * @param transferEventDelegate
     *            the transferEventDelegate to set
     */
    public void setTransferEventDelegate(TransferEventDelegate transferEventDelegate) {
        this.transferEventDelegate = transferEventDelegate;
    }

    /**
     * @return the initDataViewComboBoxes
     */
    public boolean isInitDataViewComboBoxes() {
        return initDataViewComboBoxes;
    }

    /**
     * @param initDataViewComboBoxes
     *            the initDataViewComboBoxes to set
     */
    public void setInitDataViewComboBoxes(boolean initDataViewComboBoxes) {
        if (initDataViewComboBoxes != this.initDataViewComboBoxes) {
            this.initDataViewComboBoxes = initDataViewComboBoxes;
            initComboList();
        }
    }

    public boolean isAutoHideViews() {
        return autoHideViews;
    }

    public void setAutoHideViews(boolean autoHideViews) {
        this.autoHideViews = autoHideViews;
    }

    public boolean isCyclingCustomMap() {
        return cyclingCustomMap;
    }

    /**
     * Set if the custom data view properties map are cyclic or not
     */
    public void setCyclingCustomMap(boolean cyclingCustomMap) {
        this.cyclingCustomMap = cyclingCustomMap;
    }

    /**
     * Set the custom color map attribution
     */
    public void setCustomColor(Color[] colorList) {
        if (colorList != null && colorList.length > 0) {
            for (Color element : colorList) {
                customColor.addFirst(element);
            }
        }
    }

    /**
     * Set the custom fill color map attribution
     */
    public void setCustomFillColor(Color[] colorList) {
        if (colorList != null && colorList.length > 0) {
            for (Color element : colorList) {
                customDataViewFillColor.addFirst(element);
            }
        }
    }

    /**
     * Set the custom marker color map attribution
     */
    public void setCustomMarkerColor(Color[] colorList) {
        if (colorList != null && colorList.length > 0) {
            for (Color element : colorList) {
                customDataViewMarkerColor.addFirst(element);
            }
        }
    }

    /**
     * Set the custom line style map attribution
     */
    public void setCustomLineStyle(int[] lineStyle) {
        if (lineStyle != null && lineStyle.length > 0) {
            for (int element : lineStyle) {
                customDataViewLineStyle.addFirst(element);
            }
        }
    }

    /**
     * Set the custom line style map attribution
     */
    public void setCustomUserFormat(String[] formatList) {
        if (formatList != null && formatList.length > 0) {
            for (String element : formatList) {
                customDataViewUserFormat.addFirst(element);
            }
        }
    }

    /**
     * Set the custom line style map attribution
     */
    public void setCustomUserUnit(String[] unitList) {
        if (unitList != null && unitList.length > 0) {
            for (String element : unitList) {
                customDataViewUnit.addFirst(element);
            }
        }
    }

    /**
     * Set the custom axis map attribution
     */
    public void setCustomAxis(int[] axis) {
        if (axis != null && axis.length > 0) {
            for (int element : axis) {
                customDataViewAxis.addFirst(element);
            }
        }
    }

    /**
     * Set the custom marker map attribution
     */
    public void setCustomMarkerStyle(int[] marker) {
        if (marker != null && marker.length > 0) {
            for (int element : marker) {
                customDataViewMarkerStyle.addFirst(element);
            }
        }
    }

    /**
     * Set the custom marker size map attribution
     */
    public void setCustomMarkerSize(int[] sizeList) {
        if (sizeList != null && sizeList.length > 0) {
            for (int element : sizeList) {
                customDataViewMarkerSize.addFirst(element);
            }
        }
    }

    /**
     * Set the custom line width map attribution
     */
    public void setCustomLineWidth(int[] widthList) {
        if (widthList != null && widthList.length > 0) {
            for (int element : widthList) {
                customDataViewLineWidth.addFirst(element);
            }
        }
    }

    /**
     * Set the custom bar width map attribution
     */
    public void setCustomBarWidth(int[] widthList) {
        if (widthList != null && widthList.length > 0) {
            for (int element : widthList) {
                customDataViewBarWidth.addFirst(element);
            }
        }
    }

    /**
     * Set the custom data view style map attribution
     */
    public void setCustomDataViewStyle(int[] styleList) {
        if (styleList != null && styleList.length > 0) {
            for (int element : styleList) {
                customDataViewStyle.addFirst(element);
            }
        }
    }

    /**
     * Set the custom data view fill style map attribution
     */
    public void setCustomFillStyle(int[] styleList) {
        if (styleList != null && styleList.length > 0) {
            for (int element : styleList) {
                customDataFillStyle.addFirst(element);
            }
        }
    }

    /**
     * Set the custom data view fill style map attribution
     */
    public void setCustomFillMethod(int[] methodList) {
        if (methodList != null && methodList.length > 0) {
            for (int element : methodList) {
                customDataFillMethod.addFirst(element);
            }
        }
    }

    public IComboBox getHiddenDataViewCombo() {
        return hiddenDataViewCombo;
    }

    public IComboBox getVisibleDataViewCombo() {
        return visibleDataViewCombo;
    }

    private void initComboList() {
        if (initDataViewComboBoxes) {
            // System.out.println("initComboList");
            Collection<JLDataView> collection = null;
            // JLDataView jlDataView = null;

            Comparator<AbstractDataView> viewComparator = dataViewComparator;
            int nbOfItem = visibleDataViewCombo.getItemCount();
            Map<String, JLDataView> visibleViews = getVisibleDataViews();
            int nbOfValues = visibleViews.size();

            if (nbOfValues == 0) {
                visibleDataViewCombo.setValueList((Object[]) null);
                visibleDataViewCombo.setDisplayedList((String[]) null);
            } else if (!visibleDataViewCombo.isEditingData()/*
                                                             * && nbOfItem !=
                                                             * nbOfValues
                                                             */) {
                // XXX RG: "nbOfItem != nbOfValues" is in comments because it
                // brought diplayName a
                // bug
                // --> Mantis #21443

                // System.out.println("nbOfValues visible = " + nbOfValues);
                // System.out.println("nbOfItem visible = " + nbOfItem);

                collection = visibleViews.values();
                ArrayList<JLDataView> sorted = new ArrayList<JLDataView>(collection);
                Collections.sort(sorted, viewComparator);
                String[] optionList = new String[nbOfValues];
                Object[] valueList = new String[nbOfValues];
                int index = 0;
                for (JLDataView jlDataView : sorted) {
                    // jlDataView = iterator.next();
                    optionList[index] = jlDataView.getDisplayName();
                    // System.out.println("optionList[" + index + "]=" +
                    // optionList[index]);
                    valueList[index] = jlDataView.getId();
                    // System.out.println("valueList[" + index + "]=" +
                    // valueList[index]);
                    index++;
                }
                sorted.clear();
                sorted = null;
                visibleDataViewCombo.setValueList(valueList);
                visibleDataViewCombo.setDisplayedList(optionList);
            }

            nbOfItem = hiddenDataViewCombo.getItemCount();
            Map<String, JLDataView> hiddenViews = getHiddenDataViews();
            nbOfValues = hiddenViews.size();
            if (!hiddenDataViewCombo.isEditingData() && nbOfItem != nbOfValues) {
                collection = hiddenViews.values();
                ArrayList<JLDataView> sorted = new ArrayList<JLDataView>(collection);
                Collections.sort(sorted, viewComparator);
                String[] optionList = new String[nbOfValues];
                Object[] valueList = new String[nbOfValues];
                int index = 0;
                for (JLDataView jlDataView : sorted) {
                    optionList[index] = jlDataView.getDisplayName();
                    valueList[index] = jlDataView.getId();
                    index++;
                }
                sorted.clear();
                sorted = null;
                hiddenDataViewCombo.setValueList(valueList);
                hiddenDataViewCombo.setDisplayedList(optionList);
            }
            visibleViews.clear();
            visibleViews = null;
            hiddenViews.clear();
            hiddenViews = null;
        } else {
            if (visibleDataViewCombo.getValueList() != null) {
                visibleDataViewCombo.setValueList((Object[]) null);
                visibleDataViewCombo.setDisplayedList((String[]) null);
            }
            if (hiddenDataViewCombo.getValueList() != null) {
                hiddenDataViewCombo.setValueList((Object[]) null);
                hiddenDataViewCombo.setDisplayedList((String[]) null);
            }
        }
    }

    public void removeDataView(String dataViewName, boolean definitively) {
        JLDataView jlDataView = getDataView(dataViewName);
        synchronized (hiddenDataView) {
            if (definitively) {
                if (hiddenDataView.containsKey(dataViewName)) {
                    hiddenDataView.remove(dataViewName);
                }
                if (isCleanDataViewConfigurationOnRemoving()) {
                    cleanDataViewProperties(dataViewName);
                }
            } else {
                if (jlDataView != null && !hiddenDataView.containsKey(dataViewName)) {
                    hiddenDataView.put(dataViewName, jlDataView);
                }
            }
        }
        removeDataView(jlDataView);

        // XXX GIRARDOT: temporary deactivated this code, because it avoided to
        // correctly hide a
        // dataview
        // if (lastDatas != null) {
        // setData(lastDatas);
        // }

    }

    public void addDataView(String dataViewName, int axis, boolean forceVisible) {
        setDataViewAxis(dataViewName, axis);

        JLDataView dataView = getDataView(dataViewName);
        if (dataView == null && hiddenDataView.containsKey(dataViewName)) {
            dataView = hiddenDataView.get(dataViewName);
        }

        addDataViewToAxis(dataView, axis, (autoHideViews && !forceVisible));
        measureAndPaint();
    }

    public void initDataViews(int axis, boolean computeCalculation) {
        List<JLDataView> dataViews = getDataView(axis);
        if (dataViews != null) {
            for (JLDataView jlDataView : dataViews) {
                synchronized (visibleDataView) {
                    if (jlDataView != null && !visibleDataView.containsKey(jlDataView.getId())) {
                        visibleDataView.put(jlDataView.getId(), jlDataView);
                    }
                }
            }
            dataViews.clear();
            dataViews = null;
        }
        if (computeCalculation) {
            initComboList();
            measureAndPaint();
        }
    }

    public void measureAndPaint() {
        measureGraphItems();
        repaint();
    }

    public Map<String, JLDataView> getVisibleDataViews() {
        HashMap<String, JLDataView> resultMap = new HashMap<String, JLDataView>();
        synchronized (visibleDataView) {
            resultMap.putAll(visibleDataView);
        }
        return resultMap;
    }

    public Map<String, JLDataView> getHiddenDataViews() {
        HashMap<String, JLDataView> resultMap = new HashMap<String, JLDataView>();
        synchronized (hiddenDataView) {
            resultMap.putAll(hiddenDataView);
        }
        return resultMap;
    }

    public void clearSnapshots() {
        synchronized (snapshots) {
            for (JLDataView snapshot : snapshots) {
                snapshot.setFixed(false);
                removeDataView(snapshot.getId(), true);
            }
            snapshots.clear();
        }
    }

    protected List<JLDataView> makeSnaphot(int axis) {
        List<JLDataView> copy = new ArrayList<JLDataView>();
        List<JLDataView> dataviews = getDataView(axis);
        if (dataviews != null) {
            synchronized (snapshots) {
                for (JLDataView jlDataView : dataviews) {
                    if ((jlDataView != null) && (!snapshots.contains(jlDataView))) {
                        copy.add(jlDataView);
                        if (jlDataView.isClickable()) {
                            JLDataView snapshot = duplicateDataView(jlDataView);
                            snapshot.setFixed(true);
                            snapshots.add(snapshot);
                            addDataViewToAxis(snapshot, axis, false);
                        }
                    }
                }
            }
            dataviews.clear();
        }
        return copy;
    }

    public void makeSnaphot() {
        List<JLDataView> toBringToFront = new ArrayList<JLDataView>();
        synchronized (snapshots) {
            toBringToFront.addAll(makeSnaphot(IChartViewer.Y1));
            toBringToFront.addAll(makeSnaphot(IChartViewer.Y2));
            for (JLDataView dataView : toBringToFront) {
                JLAxis axis = dataView.getAxis();
                if (axis != null) {
                    axis.removeDataView(dataView);
                    axis.addDataView(dataView);
                }
            }
        }
        toBringToFront.clear();
        measureAndPaint();
    }

    private JLDataView duplicateDataView(JLDataView original) {
        String freezeString = "- Freeze " + dateFormat.format(new Date(System.currentTimeMillis()));
        String id = original.getId() + freezeString + "_" + (Math.random() * 1000);
        String displayName = original.getDisplayName() + freezeString;
        JLDataView copy = new JLDataView(id);
        copy.setDisplayName(displayName);
        setDataViewMarkerSize(id, original.getMarkerSize());
        setDataViewMarkerStyle(id, original.getMarker());
        setDataViewLineWidth(id, original.getLineWidth());
        setDataViewStyle(id, original.getViewType());
        setDataViewLineStyle(id, original.getStyle());
        setDataViewFillStyle(id, original.getFillStyle());
        setDataViewFillMethod(id, original.getFillMethod());
        JLAxis jlAxis = original.getAxis();
        if (jlAxis.equals(getXAxis())) {
            setDataViewAxis(id, IChartViewer.X);
        }
        if (jlAxis.equals(getY1Axis())) {
            setDataViewAxis(id, IChartViewer.Y1);
        }
        if (jlAxis.equals(getY2Axis())) {
            setDataViewAxis(id, IChartViewer.Y2);
        }
        copy.setUnit(original.getUnit());
        if (original.getFormat() != null) {
            copy.setFormat(original.getFormat());
        }
        copy.setBarWidth(original.getBarWidth());
        copy.setA0(original.getA0());
        copy.setA1(original.getA1());
        copy.setA2(original.getA2());

        DataList data = original.getData();
        while (data != null) {
            copy.add(data.getX(), data.getY());
            data = data.next;
        }
        return copy;
    }

    public void setIndexColumnName(String name) {
        indexColumnName = name;
    }

    public String getIndexColumnName() {
        return indexColumnName;
    }

    public String getDateColumnName() {
        return dateColumnName;
    }

    public void setDateColumnName(String dateColumnName) {
        this.dateColumnName = dateColumnName;
    }

    // public void removeAllDataViews() {
    // removeAllDataViews(Y1);
    // removeAllDataViews(Y2);
    // removeAllDataViews(X);
    // }
    //
    // public void removeAllDataViews(int axis) {
    // JLAxis jlaxis = getAxis(axis);
    // jlaxis.clearDataView();
    // }

    /**
     * Return a handle to the x axis
     * 
     * @return Axis handle
     */
    public JLAxis getXAxis() {
        return xAxis;
    }

    /**
     * Return a handle to the left y axis
     * 
     * @return Axis handle
     */
    public JLAxis getY1Axis() {
        return y1Axis;
    }

    /**
     * Return a handle to the right y axis
     * 
     * @return Axis handle
     */
    public JLAxis getY2Axis() {
        return y2Axis;
    }

    public String getAxisName(int axis) {
        String name;
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis == null) {
            name = null;
        } else {
            name = jlaxis.getTitle();
        }
        return name;
    }

    /**
     * Sets the axis name
     */
    public void setAxisName(final String name, final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setTitle(name);
        }
    }

    /**
     * Returns the axis title alignment
     * 
     * @param axis
     *            the axis
     * 
     * @return an int value
     * 
     * @see IComponent#TOP
     * @see IComponent#LEFT
     * @see IComponent#BOTTOM
     * @see IComponent#RIGHT
     */
    public int getAxisTitleAlignment(final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.getTitleAlignment();
        }
        return IComponent.CENTER;
    }

    /**
     * Sets the axis title alignment
     * 
     * @param titleAlignment
     *            the alignment to set
     * @param axis
     *            the axis to set
     * 
     * @see IComponent#TOP
     * @see IComponent#LEFT
     * @see IComponent#BOTTOM
     * @see IComponent#RIGHT
     */
    public void setAxisTitleAlignment(final int align, final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setTitleAlignment(align);
        }
    }

    /**
     * set a color on the axis
     */
    public void setAxisColor(final Color color, final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setAxisColor(color);
        }
    }

    /**
     * Draw a second axis at the opposite side.
     * 
     * @param b
     *            true to enable the opposite axis.
     */
    public void setAxisDrawOpposite(boolean opposite, final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setDrawOpposite(opposite);
        }
    }

    public boolean isAxisDrawOpposite(final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.isDrawOpposite();
        }
        return false;
    }

    /**
     * Sets the axis position
     * 
     * @param o
     *            Axis position
     * @see JLAxis#VERTICAL_LEFT
     * @see JLAxis#VERTICAL_RIGHT
     * @see JLAxis#VERTICAL_ORG
     * @see JLAxis#HORIZONTAL_DOWN
     * @see JLAxis#HORIZONTAL_UP
     * @see JLAxis#HORIZONTAL_ORG1
     * @see JLAxis#HORIZONTAL_ORG2
     */
    public void setAxisPosition(int position, final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setPosition(position);
            measureAndPaint();
        }
    }

    public int getAxisPosition(final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.getPosition();
        }
        return IChartViewer.HORIZONTAL_DOWN;
    }

    /**
     * Display or hide the axis.
     * 
     * @param b
     *            True to make the axis visible.
     */
    public void setAxisVisible(boolean visible, final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setVisible(visible);
            measureAndPaint();
        }
    }

    public boolean isAxisVisible(final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.isVisible();
        }
        return false;
    }

    /**
     * set a maximum on the axis
     */
    public void setAxisMaximum(final double maximum, final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setMaximum(maximum);
            measureAndPaint();
        }
    }

    /**
     * get the maximum on the axis
     */
    public double getAxisMaximum(final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.getMaximum();
        }
        return Double.NaN;
    }

    /**
     * set a minimum on the axis
     */
    public void setAxisMinimum(final double minimum, final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setMinimum(minimum);
            measureAndPaint();
        }
    }

    /**
     * get the minimum on the axis
     */
    public double getAxisMinimum(final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.getMinimum();
        }
        return Double.NaN;
    }

    /**
     * set a date format on the axis if it is TIME ANNOTATION
     */
    public void setAxisDateFormat(final String format, final int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setDateFormat(format);
        }
    }

    /**
     * Sets weather x Axis is on bottom of screen or not
     * 
     * @param b
     *            boolean to know weather x Axis is on bottom of screen or not
     */
    public void setXAxisOnBottom(boolean b) {
        if (b) {
            getXAxis().setPosition(IChartViewer.HORIZONTAL_DOWN);
        } else {
            getXAxis().setPosition(IChartViewer.HORIZONTAL_ORGY1);
        }
    }

    /**
     * tells weather x Axis is on bottom of screen or not
     * 
     * @return [code]true[/code] if x Axis is on bottom of screen,
     *         [code]false[/code] otherwise
     */
    public boolean isXAxisOnBottom() {
        return getXAxis().getPosition() == IChartViewer.HORIZONTAL_DOWN;
    }

    /**
     * Sets header font
     * 
     * @param f
     *            Header font
     * @see JLChart#getHeaderFont
     */
    public void setHeaderFont(Font f) {
        setHeaderFont(f, true);
    }

    protected void setHeaderFont(Font f, boolean fireChartEvent) {
        headerFont = f;
        if (fireChartEvent) {
            fireConfigurationChanged(null);
        }
    }

    /**
     * Gets the header font
     * 
     * @return Header font
     * @see JLChart#setHeaderFont
     */
    public Font getHeaderFont() {
        return headerFont;
    }

    /**
     * Sets component margin
     * 
     * @param d
     *            Margin
     * @see JLChart#getMargin
     */
    public void setMargin(Dimension d) {
        margin = d;
    }

    /**
     * Gets the current margin
     * 
     * @return Margin
     * @see JLChart#setMargin
     */
    public Dimension getMargin() {
        return margin;
    }

    @Override
    public void setBackground(Color c) {
        super.setBackground(c);
        // setChartBackground(c);
    }

    /**
     * Sets the chart background (curve area)
     * 
     * @param c
     *            Background color
     */
    public void setChartBackground(Color c) {
        setChartBackground(c, true);
    }

    protected void setChartBackground(Color c, boolean fireChartEvent) {
        chartBackground = c;
        if (fireChartEvent) {
            fireConfigurationChanged(null);
        }
    }

    /**
     * 
     * Gets the chart background (curve area)
     * 
     * @return Background color
     */
    public Color getChartBackground() {
        return chartBackground;
    }

    /**
     * Paints axis under curve when true
     * 
     * @param b
     *            Painting order
     */
    public void setPaintAxisFirst(boolean b) {
        paintAxisFirst = b;
    }

    /**
     * Return painting order between axis and curve
     * 
     * @return true if axis are painted under curve
     */
    public boolean isPaintAxisFirst() {
        return paintAxisFirst;
    }

    /**
     * Displays or hides header.
     * 
     * @param b
     *            true if the header is visible, false otherwise
     * @see JLChart#setHeader
     */
    public void setHeaderVisible(boolean b) {
        headerVisible = b;
    }

    public boolean isHeaderVisible() {
        return headerVisible;
    }

    /**
     * Sets the header and displays it.
     * 
     * @param s
     *            Graph header
     * @see JLChart#getHeader
     */
    public void setHeader(String s) {
        setHeader(s, true);
    }

    protected void setHeader(String s, boolean fireChartEvent) {
        header = s;
        if (s != null) {
            if (s.length() == 0) {
                header = null;
            }
        }
        setHeaderVisible(header != null);
        if (fireChartEvent) {
            fireConfigurationChanged(null);
        }
    }

    /**
     * Gets the current header
     * 
     * @return Graph header
     * @see JLChart#setHeader
     */
    public String getHeader() {
        return header;
    }

    /**
     * Sets the display duration.This will garbage old data in all displayed
     * data views. Garbaging occurs when addData is called.
     * 
     * @param v
     *            Displauy duration (millisec). Pass Double.POSITIVE_INFINITY to
     *            disable.
     * @see JLChart#addData
     */
    public void setDisplayDuration(double v) {
        setDisplayDuration(v, true);
    }

    protected void setDisplayDuration(double v, boolean fireChartEvent) {
        if (v <= maxDisplayDuration) {
            // accept displayDuration
            displayDuration = v;
            getXAxis().setAxisDuration(v);
            if (fireChartEvent) {
                fireConfigurationChanged(null);
            }
        } else {
            // refuse displayDuration
            StringBuilder result = new StringBuilder("Duration refused : can not be greater than ");
            if (JLAxis.TIME_ANNO == getXAxis().getAnnotation()
                    || IChartViewer.TIME_FORMAT == getXAxis().getLabelFormat()) {
                // in case of time, convert maxDisplayDuration to readable time
                double days, hours, minutes, seconds, milliseconds;
                int dayTime = 1000 * 60 * 60 * 24;
                int hourTime = 1000 * 60 * 60;
                int minuteTime = 1000 * 60;
                int secondTime = 1000;
                double totalTime = maxDisplayDuration;

                days = totalTime / dayTime;
                totalTime -= days * dayTime;

                hours = totalTime / hourTime;
                totalTime -= hours * hourTime;

                minutes = totalTime / minuteTime;
                totalTime -= minutes * minuteTime;

                seconds = totalTime / secondTime;

                totalTime -= seconds * secondTime;
                milliseconds = totalTime;

                if (days > 0) {
                    String dayString = Double.toString(days);
                    if (dayString.endsWith(".0")) {
                        dayString = dayString.substring(0, dayString.indexOf(ChartUtils.DOT));
                    }
                    result.append(dayString).append("day(s) ");
                    dayString = null;
                }

                if (hours > 0) {
                    String hourString = Double.toString(hours);
                    if (hourString.endsWith(".0")) {
                        hourString = hourString.substring(0, hourString.indexOf(ChartUtils.DOT));
                    }
                    result.append(hours).append("hr ");
                    hourString = null;
                }

                if (minutes > 0) {
                    String minuteString = Double.toString(minutes);
                    if (minuteString.endsWith(".0")) {
                        minuteString = minuteString.substring(0, minuteString.indexOf(ChartUtils.DOT));
                    }
                    result.append(minuteString).append("mn ");
                    minuteString = null;
                }

                if (seconds > 0) {
                    String secondString = Double.toString(seconds);
                    if (secondString.endsWith(".0")) {
                        secondString = secondString.substring(0, secondString.indexOf(ChartUtils.DOT));
                    }
                    result.append(secondString).append("s ");
                    secondString = null;
                }

                if (milliseconds > 0) {
                    String millisecondString = Double.toString(milliseconds);
                    if (millisecondString.endsWith(".0")) {
                        millisecondString = millisecondString.substring(0, millisecondString.indexOf(ChartUtils.DOT));
                    }
                    result.append(millisecondString).append("ms ");
                    millisecondString = null;
                } else if (days == 0 && hours == 0 && minutes == 0 && seconds == 0 && milliseconds == 0) {
                    String millisecondString = Double.toString(milliseconds);
                    if (millisecondString.endsWith(".0")) {
                        millisecondString = millisecondString.substring(0, millisecondString.indexOf(ChartUtils.DOT));
                    }
                    result.append(millisecondString).append("ms ");
                    millisecondString = null;
                }
            } else {
                // otherwise, maxDisplayDuration does not have to be converted
                // to readable time
                String maxString = Double.toString(maxDisplayDuration);
                if (maxString.endsWith(".0")) {
                    maxString = maxString.substring(0, maxString.indexOf(ChartUtils.DOT));
                }
                result.append(maxString);
                maxString = null;
            }
            JOptionPane.showMessageDialog(this, result.toString(), "Warning !", JOptionPane.WARNING_MESSAGE);
            result = null;
        }
    }

    /**
     * Gets the display duration.
     * 
     * @return Display duration
     * @see JLChart#setDisplayDuration
     */
    public double getDisplayDuration() {
        return displayDuration;
    }

    /**
     * Gets the maximum allowed for a display duration
     * 
     * @return Maximum allowed for a display duration
     * @see JLChart#setMaxDisplayDuration
     * @see JLChart#setDisplayDuration
     */
    public double getMaxDisplayDuration() {
        return maxDisplayDuration;
    }

    /**
     * Sets the maximum allowed for a display duration
     * 
     * @param maxDisplayDuration
     *            The maximum allowed for a display duration
     * @see JLChart#getMaxDisplayDuration
     * @see JLChart#getDisplayDuration
     * @see JLChart#setDisplayDuration
     */
    public void setMaxDisplayDuration(double maxDisplayDuration) {
        this.maxDisplayDuration = maxDisplayDuration;
    }

    /**
     * Sets the header color
     * 
     * @param c
     *            Header color
     */
    public void setHeaderColor(Color c) {
        headerColor = c;
        setHeaderVisible(true);
    }

    public Color getHeaderColor() {
        return headerColor;
    }

    /**
     * Displays or hide labels.
     * 
     * @param b
     *            true if labels are visible, false otherwise
     * @see JLChart#isLabelVisible
     */
    public void setLabelVisible(boolean b) {
        setLabelVisible(b, true);
    }

    protected void setLabelVisible(boolean b, boolean fireChartEvent) {
        labelVisible = b;
        fireActionPerformed(CHANGE_LABEL_VISIBILITY, true);
        if (fireChartEvent) {
            fireConfigurationChanged(null);
        }
    }

    /**
     * Determines if labels are visible or not.
     * 
     * @return true if labels are visible, false otherwise
     */
    public boolean isLabelVisible() {
        return labelVisible;
    }

    /**
     * Set the label placement.
     * 
     * @param p
     *            Placement
     * @see JLChart#LABEL_UP
     * @see JLChart#LABEL_DOWN
     * @see JLChart#LABEL_ROW
     * @see JLChart#LABEL_LEFT
     * @see JLChart#LABEL_RIGHT
     */
    public void setLabelPlacement(int p) {
        setLabelPlacement(p, true);
    }

    protected void setLabelPlacement(int p, boolean fireChartEvent) {
        labelMode = p;
        fireActionPerformed(CHANGE_LABEL_PLACEMENT, true);
        if (fireChartEvent) {
            fireConfigurationChanged(null);
        }
    }

    /**
     * Returns the current label placement.
     * 
     * @return Label placement
     * @see JLChart#setLabelPlacement
     */
    public int getLabelPlacement() {
        return labelMode;
    }

    /**
     * Sets the label font
     * 
     * @param f
     */
    public void setLabelFont(Font f) {
        setLabelFont(f, true);
    }

    protected void setLabelFont(Font f, boolean fireChartEvent) {
        labelFont = f;
        if (fireChartEvent) {
            fireConfigurationChanged(null);
        }
    }

    public int getAxisLabelFormat(int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.getLabelFormat();
        }
        return IChartViewer.AUTO_FORMAT;
    }

    public void setAxisLabelFormat(int format, int axis) {
        JLAxis tmp_axis = this.getAxis(axis);
        if (tmp_axis != null) {
            tmp_axis.setLabelFormat(format);
        }
    }

    public int getMaxDataViewShown() {
        return maxDataViewShown;
    }

    public void setMaxDataViewShown(int maxDataViewShown) {
        this.maxDataViewShown = maxDataViewShown;
    }

    /**
     * Returns the label font
     * 
     * @see #setLabelFont
     */
    public Font getLabelFont() {
        return labelFont;
    }

    protected void cleanChartEditor() {
        if (chartEditor != null) {
            chartEditor.setVisible(false);
            chartEditor.setContentPane(new JPanel());
            chartEditor.dispose();
            chartEditor = null;
        }
    }

    /**
     * Display the global graph option dialog.
     */
    public void showOptionDialog() {

        cleanChartEditor();
        chartEditor = new JDialog(CometeUtils.getWindowForComponent(this));
        chartEditor.setModal(true);
        chartEditor.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        chartEditor.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cleanChartEditor();
            }
        });
        chartEditor.setTitle(chartOption.getName());
        chartEditor.setContentPane(chartOption);
        chartOption.setChart(this);
        chartEditor.pack();
        chartEditor.setLocationRelativeTo(this);
        chartEditor.setVisible(true);

    }

    protected void cleanDataViewEditor() {
        if (dataViewEditor != null) {
            if (dataViewEditor.isVisible()) {
                dataViewEditor.setVisible(false);
                dataViewEditor.dispose();
            }
            dataViewEditor.setContentPane(new JPanel());
            dataViewEditor = null;
        }
    }

    /**
     * Display the data view option dialog.
     */
    public void showDataOptionDialog(JLDataView v) {
        if (v != null) {
            cleanDataViewEditor();
            dataViewOption.setDataViewId(v.getId());
            Window parent = CometeUtils.getWindowForComponent(this);
            dataViewEditor = new JDialog(parent);
            dataViewEditor.setTitle(dataViewOption.getName());
            dataViewEditor.setModal(true);
            dataViewEditor.setContentPane(dataViewOption);
            dataViewEditor.pack();
            dataViewEditor.setResizable(false);
            dataViewEditor.setLocationRelativeTo(parent);
            parent = null;
            dataViewEditor.setVisible(true);
        }
    }

    /**
     * Determines whether the graph is zoomed.
     * 
     * @return true if the , false otherwise
     */
    public boolean isZoomed() {
        // isZoomed is in the case of an area Zoom
        return isZoomed || xAxis.isZoomed() || y1Axis.isZoomed() || y2Axis.isZoomed();
    }

    /**
     * Enter zoom mode. This happens when you hold the left mouse button down
     * and drag the mouse.
     */
    public void enterZoom() {
        if (!zoomDragAllowed) {
            zoomDragAllowed = true;
            setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        }
    }

    /**
     * Set the specified JLChart Listener
     * 
     * @param l
     *            JLChart listener.
     */
    public void addChartViewerListener(IChartViewerListener listener) {
        if (listener != null) {
            synchronized (chartListeners) {
                if (!chartListeners.contains(listener)) {
                    chartListeners.add(listener);
                }
            }
        }
    }

    /**
     * removes the specified JLChart Listener
     * 
     * @param l
     *            JLChart listener. If set to null the listener will be removed.
     */
    public void removeChartViewerListener(IChartViewerListener listener) {
        if (listener != null) {
            synchronized (chartListeners) {
                if (chartListeners.contains(listener)) {
                    chartListeners.remove(listener);
                }
            }
        }
    }

    // save the listener, it will be added to the table
    // when the table will be created
    public void addTableRowListener(ITableRowListener l) {
        tableListener = l;
        if (theTable != null) {
            theTable.addTableRowListener(l);
        }
    }

    public void removeTableRowListener(ITableRowListener l) {
        if (theTable != null) {
            theTable.removeTableRowListener(l);
        }
    }

    /**
     * Adds a user action. It will be available from the contextual chart menu.
     * All JLChartActionListener are triggered when a user action is executed.
     * Hint: If the action name starts with 'chk' , it will be displayed as
     * check box menu item. Each time the chart menu is shown, a
     * getActionState() is executed on all listener, if several listener handle
     * the same action, the result will be a logical and of all results.
     * 
     * @param name
     *            Action name
     */
    public void addUserAction(String name) {
        int i;

        String[] action = new String[userAction.length + 1];
        boolean[] enable = new boolean[userAction.length + 1];
        for (i = 0; i < userAction.length; i++) {
            action[i] = userAction[i];
            enable[i] = userActionMenuItem[i].isEnabled();
        }
        action[i] = name;
        enable[i] = true;

        // Build the menu
        for (i = 0; i < userActionMenuItem.length; i++) {
            chartMenu.remove(userActionMenuItem[i]);
            userActionMenuItem[i].removeActionListener(this);
            userActionMenuItem[i] = null;
        }

        JMenuItem[] actionMenu = new JMenuItem[action.length];
        for (i = 0; i < action.length; i++) {
            if (action[i].startsWith("chk")) {
                actionMenu[i] = new JCheckBoxMenuItem(action[i].substring(3));
            } else {
                actionMenu[i] = new JMenuItem(action[i]);
            }
            actionMenu[i].addActionListener(this);
            actionMenu[i].setEnabled(enable[i]);
            chartMenu.add(actionMenu[i]);
        }

        userActionMenuItem = actionMenu;
        userAction = action;
        sepMenuItem.setVisible(true);

    }

    /**
     * Removes a user action from chart menu.
     * 
     * @param name
     *            Action name
     */
    public void removeUserAction(String name) {
        int i;

        int correspondingIndex = -1;

        for (i = 0; i < userAction.length; i++) {
            if (userAction[i].equals(name)) {
                correspondingIndex = i;
                break;
            }
        }
        if (correspondingIndex != -1) {
            String[] action = new String[userAction.length - 1];
            for (i = 0; i < correspondingIndex; i++) {
                action[i] = userAction[i];
            }
            for (i = correspondingIndex + 1; i < userAction.length; i++) {
                action[i - 1] = userAction[i];
            }

            // Build the menu
            for (i = 0; i < userActionMenuItem.length; i++) {
                chartMenu.remove(userActionMenuItem[i]);
                userActionMenuItem[i].removeActionListener(this);
                userActionMenuItem[i] = null;
            }

            JMenuItem[] actionMenu = new JMenuItem[action.length];
            for (i = 0; i < action.length; i++) {
                if (action[i].startsWith("chk")) {
                    actionMenu[i] = new JCheckBoxMenuItem(action[i].substring(3));
                } else {
                    actionMenu[i] = new JMenuItem(action[i]);
                }
                actionMenu[i].addActionListener(this);
                chartMenu.add(actionMenu[i]);
            }

            userActionMenuItem = actionMenu;
            userAction = action;
            sepMenuItem.setVisible(true);
        }

    }

    public void enableUserAction(boolean b, String name) {
        for (int i = 0; i < userAction.length; i++) {
            if (userAction[i].equals(name)) {
                userActionMenuItem[i].setEnabled(false);
            }
        }
    }

    /**
     * Add the specified JLChartAction listener to the list
     * 
     * @param l
     *            Listener to add
     */
    public void addJLChartActionListener(IJLChartActionListener l) {
        listenerList.add(IJLChartActionListener.class, l);
    }

    /**
     * Exit zoom mode.
     */
    public void exitZoom() {

        if (areaZoom) {

            y1Axis.setAutoScale(oldAutoScaleY1);
            y2Axis.setAutoScale(oldAutoScaleY2);

            if (!oldAutoScaleY1) {
                y1Axis.setMaximum(oldMaxY1);
                y1Axis.setMinimum(oldMinY1);
            }

            if (!oldAutoScaleY2) {
                y2Axis.setMaximum(oldMaxY2);
                y2Axis.setMinimum(oldMinY2);
            }

            xAxis.setAutoScale(true);

            areaZoom = false;
        } else {
            xAxis.unzoom();
            y1Axis.unzoom();
            y2Axis.unzoom();
        }

        zoomDragAllowed = false;
        zoomButton.setVisible(false);
        isZoomed = false;
        setCursor(Cursor.getDefaultCursor());
        measureAndPaint();
    }

    /**
     * Method to remove item of the contextual menu.
     * 
     * @param menu
     *            Item to remove
     * @see #MENU_CHARTPROP
     * @see #MENU_DVPROP
     * @see #MENU_TABLE
     * @see #MENU_DATASAVE
     * @see #MENU_PRINT
     * @see #MENU_STAT
     */
    public void removeMenuItem(int menu) {

        switch (menu) {
            /* Chart properties menu item */
            case MENU_CHARTPROP:
                chartMenu.remove(optionMenuItem);
                break;
            /* Data view properties menu item */
            case MENU_DVPROP:
                chartMenu.remove(dvMenu);
                break;
            /* Show table menu item */
            case MENU_TABLE:
                chartMenu.remove(tableMenu);
                break;
            /* Save data file menu item */
            case MENU_DATASAVE:
                chartMenu.remove(saveFileMenuItem);
                break;
            /* print graph menu item */
            case MENU_PRINT:
                chartMenu.remove(printMenuItem);
                break;
            /* Statistics menu */
            case MENU_STAT:
                chartMenu.remove(statMenu);
                break;
        }
    }

    /**
     * Method to add item to the contextual menu.
     * 
     * @param menu
     *            MenuItem to add
     */
    public void addMenuItem(JMenuItem menu) {
        chartMenu.add(menu);
    }

    /**
     * Method to add a separator to the contextual menu.
     */
    public void addSeparator() {
        chartMenu.addSeparator();
    }

    /**
     * Remove the specified JLChartAction Listener
     * 
     * @param l
     *            Listener to remove
     */
    public void removeJLChartActionListener(IJLChartActionListener l) {
        listenerList.remove(IJLChartActionListener.class, l);
    }

    /**
     * Apply graph configuration. This includes all global settings. The
     * CfFileReader object must have been filled by the caller.
     * 
     * @param f
     *            Handle to CfFileReader object that contains global graph param
     * @see CfFileReader#parseText
     * @see CfFileReader#readFile
     * @see JLAxis#applyConfiguration
     * @see JLDataView#applyConfiguration
     */
    public void applyConfiguration(CfFileReader f) {

        List<String> p;

        // General settings
        p = f.getParam("graph_title");
        if (p != null) {
            setHeader(OFormat.getName(p.get(0).toString()));
        }
        // if (p != null) {
        // setHeader(OFormat.getName(p.get(0).toString()));
        // }
        p = f.getParam("label_visible");
        if (p != null) {
            setLabelVisible(OFormat.getBoolean(p.get(0).toString()));
        }
        p = f.getParam("label_placement");
        if (p != null) {
            setLabelPlacement(OFormat.getInt(p.get(0).toString()));
        }
        p = f.getParam("label_font");
        if (p != null) {
            setLabelFont(OFormat.getFont(p));
        }
        p = f.getParam("graph_background");
        if (p != null) {
            setBackground(OFormat.getColor(p));
        }
        p = f.getParam("chart_background");
        if (p != null) {
            setChartBackground(OFormat.getColor(p));
        }
        p = f.getParam("title_font");
        if (p != null) {
            setHeaderFont(OFormat.getFont(p));
        }
        p = f.getParam("display_duration");
        if (p != null) {
            setDisplayDuration(OFormat.getDouble(p.get(0).toString()));
        }
        p = f.getParam("precision");
        if (p != null) {
            setTimePrecision(OFormat.getInt(p.get(0).toString()));
        }
        // window size is no more supported by JLChart
        // p = f.getParam("window_size");
        // int x = 400;
        // int y = 400;
        // if (p != null) {
        // x = OFormat.getPoint(p).x;
        // y = OFormat.getPoint(p).y;
        // }
        // this.setSize(x, y);
        // this.setPreferredSize(this.getSize());

        if (this.isMathExpressionEnabled()) {
            // Expression settings
            p = f.getParam("expressions");
            int expressionSize = 0;
            if (p != null) {
                expressionSize = OFormat.getInt(p.get(0).toString());
            }
            for (int i = 0; i < expressionSize; i++) {
                String id = null;
                p = f.getParam("expression_" + i + AbstractDataView.NAME);
                if (p != null) {
                    id = p.get(0).toString();
                }
                JLDataView expressionView = new JLDataView(id);
                expressionView.applyConfiguration("expression_" + i, f);

                int axis = -1;
                p = f.getParam("expression_" + i + AbstractDataView.AXIS);
                if (p != null) {
                    axis = OFormat.getInt(p.get(0).toString());
                }

                String expression = ObjectUtils.EMPTY_STRING;
                p = f.getParam("expression_" + i + "_expression");
                if (p != null) {
                    expression = p.get(0).toString();
                }

                boolean x_abs = false;
                p = f.getParam("expression_" + i + "_isX");
                if (p != null) {
                    x_abs = OFormat.getBoolean(p.get(0).toString());
                }

                int variablesCount = 0;
                p = f.getParam("expression_" + i + "_variables");
                if (p != null) {
                    variablesCount = OFormat.getInt(p.get(0).toString());
                }

                String[] variables = new String[variablesCount];
                for (int j = 0; j < variablesCount; j++) {
                    String variable = ObjectUtils.EMPTY_STRING;
                    p = f.getParam("expression_" + i + "_variable_" + j);
                    if (p != null) {
                        variable = p.get(0).toString();
                    }
                    variables[j] = new String(variable);
                    variable = null;
                }

                if (axis != -1) {
                    applyExpressionToChart(expression, expressionView, axis, variables, x_abs);
                }
            }

            // axis configuration
            getXAxis().applyConfiguration("x", f);
            getY1Axis().applyConfiguration("y1", f);
            getY2Axis().applyConfiguration("y2", f);

            // set views on the correct axis
            setViewsOnAxis(f);
        }
    }

    // set the dataViews on the correct axis when applying configuration
    private void setViewsOnAxis(CfFileReader f) {

        List<String> p;

        // all views are on Y1 at the beginning
        List<JLDataView> views = getY1Axis().getViewsCopy();

        // look down the views: set on the correct axis with correct
        // configuration
        String prefix;
        JLDataView currentView = null;

        for (int i = 0; i < views.size(); i++) {

            currentView = views.get(i);
            prefix = "dv" + Integer.toString(i);
            currentView.applyConfiguration(prefix, f);

            p = f.getParam(prefix + "_selected");
            // if we can't find the correct axis, leave the view on Y1 i.e 2
            int axis = 2;
            if (p != null) {
                axis = OFormat.getInt(p.get(0).toString());
            }

            switch (axis) {
                // X axis
                case 1:
                    getY1Axis().removeDataView(currentView);
                    getXAxis().addDataView(currentView);
                    break;

                // Y1 axis
                case 2: // nothing to do, the view is on Y1
                    break;

                // Y2 axis
                case 3:
                    getY1Axis().removeDataView(currentView);
                    getY2Axis().addDataView(currentView);
                    break;

                default:
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .error("Can't correctly set the view on axis. There is no axis with this number");
            }
        }
    }

    /**
     * Build a configuration string that can be write into a file and is
     * compatible with CfFileReader.
     * 
     * @return A string containing param.
     * @see JLChart#applyConfiguration
     * @see JLDataView#getConfiguration
     * @see JLAxis#getConfiguration
     */
    public String getConfiguration() {
        StringBuilder builder = new StringBuilder();
        builder.append("graph_title:\'").append(getHeader()).append("\'\n");
        builder.append("label_visible:").append(isLabelVisible()).append(ObjectUtils.NEW_LINE);
        builder.append("label_placement:").append(getLabelPlacement()).append(ObjectUtils.NEW_LINE);
        builder.append("label_font:").append(OFormat.font(getLabelFont())).append(ObjectUtils.NEW_LINE);
        builder.append("graph_background:").append(OFormat.color(getBackground())).append(ObjectUtils.NEW_LINE);
        builder.append("chart_background:").append(OFormat.color(getChartBackground())).append(ObjectUtils.NEW_LINE);
        builder.append("title_font:").append(OFormat.font(getHeaderFont())).append(ObjectUtils.NEW_LINE);
        builder.append("display_duration:").append(getDisplayDuration()).append(ObjectUtils.NEW_LINE);
        builder.append("precision:").append(getTimePrecision()).append(ObjectUtils.NEW_LINE);
        if (this.isMathExpressionEnabled()) {
            builder.append("expressions:").append(expressionMap.size()).append(ObjectUtils.NEW_LINE);
            int i = 0;
            for (Entry<JLDataView, Object[]> entry : expressionMap.entrySet()) {
                JLDataView keyView = entry.getKey();
                Object[] expressionData = entry.getValue();
                builder.append("expression_").append(i).append("_name:\'").append(keyView.getId()).append("\'\n");
                builder.append("expression_").append(i).append("_axis:")
                        .append(((Integer) expressionData[0]).intValue()).append(ObjectUtils.NEW_LINE);
                builder.append("expression_").append(i).append("_expression:\'").append(((String) expressionData[1]))
                        .append("\'\n");
                builder.append("expression_").append(i).append("_isX:")
                        .append(((Boolean) expressionData[2]).booleanValue()).append(ObjectUtils.NEW_LINE);
                builder.append("expression_").append(i).append("_variables:").append((expressionData.length - 3))
                        .append(ObjectUtils.NEW_LINE);
                for (int j = 0; j < expressionData.length - 3; j++) {
                    builder.append("expression_").append(i).append("_variable_").append(j).append(":\'")
                            .append(((String) expressionData[j + 3])).append("\'\n");
                }
                keyView.appendConfiguration(builder, "expression_" + i);
                i++;
            }
        }
        return builder.toString();
    }

    /**
     * Returns a string containing the configuration file help.
     * 
     * @deprecated Use {@link ChartUtils#appendChartConfigurationHelpString(StringBuilder)} instead
     */
    @Deprecated
    public String getHelpString() {
        return ChartUtils.appendChartConfigurationHelpString(null).toString();
    }

    /**
     * Remove all dataview from the graph.
     */
    public void unselectAll() {
        getY1Axis().clearDataView();
        getY2Axis().clearDataView();
        getXAxis().clearDataView();
    }

    // -----------------------------------------------------

    // Fire JLChartActionEvent to all registered IJLChartActionListener
    private void fireActionPerformed(String name, boolean state) {
        IJLChartActionListener[] list = (listenerList.getListeners(IJLChartActionListener.class));
        JLChartActionEvent w = new JLChartActionEvent(this, name, state);
        for (IJLChartActionListener element : list) {
            element.actionPerformed(w);
        }
    }

    // Fire JLChartActionEvent to all registered IJLChartActionListener
    private boolean fireGetActionState(String name) {
        IJLChartActionListener[] list = (listenerList.getListeners(IJLChartActionListener.class));
        JLChartActionEvent w = new JLChartActionEvent(this, name);
        boolean ret = true;
        for (IJLChartActionListener element : list) {
            ret = element.getActionState(w) && ret;
        }

        return ret;
    }

    // Make a snapshot of data in a TAB separated field
    public void saveDataFile(String fileName) {
        List<JLDataView> views = new ArrayList<JLDataView>();
        if (xAxis.isXY()) {
            views.addAll(xAxis.getViewsCopy());
        }
        views.addAll(y1Axis.getViewsCopy());
        views.addAll(y2Axis.getViewsCopy());
        AbstractDataView[] viewArray = views.toArray(new AbstractDataView[views.size()]);
        views.clear();
        try {
            ChartUtils.saveDataFile(fileName, indexColumnName, dateColumnName, timePrecision, noValueString,
                    xAxis.getAnnotation() == JLAxis.TIME_ANNO, false, viewArray);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error during saving file.\n" + e.getMessage());
        } finally {
            if (transferEventDelegate != null) {
                transferEventDelegate.changeDataLocation(fileName, true);
            }
        }
    }

    /**
     * Loads a data file and add the corresponding data to Y1 axis
     * 
     * @param fileName
     *            the full path of the data file
     */
    public void loadDataFile(final String fileName) {
        if ((fileName != null) && (!fileName.trim().isEmpty())) {
            String lastDataFileLocation = null;
            BufferedReader reader = null;
            try {
                boolean canLoad = true;
                File dataFile = new File(fileName);
                reader = new BufferedReader(new FileReader(dataFile));
                String line = ObjectUtils.EMPTY_STRING;
                while ((line != null) && line.trim().isEmpty()) {
                    line = reader.readLine();
                }
                if (line == null) {
                    throw new Exception("Failed to read file: " + fileName);
                } else {
                    String[] parsedLine = line.split("\t");
                    String parsedLine0 = parsedLine[0].trim().toLowerCase();
                    int annotationType = -1;
                    if (parsedLine0.equalsIgnoreCase(dateColumnName) || parsedLine0.contains("time")
                            || parsedLine0.contains("date")) {
                        annotationType = JLAxis.TIME_ANNO;
                    } else if (parsedLine0.equalsIgnoreCase(indexColumnName) || parsedLine0.contains("index")) {
                        annotationType = JLAxis.VALUE_ANNO;
                    } else {
                        throw new Exception("Failed to read X Axis annotation type");
                    }
                    List<JLDataView> existingViews = new ArrayList<JLDataView>();
                    if (xAxis.isXY()) {
                        existingViews.addAll(xAxis.getViewsCopy());
                    }
                    existingViews.addAll(y1Axis.getViewsCopy());
                    existingViews.addAll(y2Axis.getViewsCopy());
                    if (existingViews.size() != 0 && annotationType != getXAxis().getAnnotation()) {
                        existingViews.clear();
                        existingViews = null;
                        String warning = "Loading this file will change X Axis annotation type.\n"
                                + "Your component may not work any more.\n" + "Are you sure to load this file ?";
                        int choice = JOptionPane.showConfirmDialog(this, warning, "Risk of breaking component",
                                JOptionPane.WARNING_MESSAGE);
                        if (choice != JOptionPane.OK_OPTION) {
                            canLoad = false;
                        }
                    }
                    if (canLoad) {
                        lastDataFileLocation = dataFile.getParentFile().getAbsolutePath();
                        int viewCount = parsedLine.length - 1;
                        if (viewCount < 0) {
                            throw new Exception();
                        }
                        JLDataView[] views = new JLDataView[viewCount];
                        JLDataView dataView = null;
                        String dvName = null;

                        for (int i = 0; i < views.length; i++) {
                            dvName = parsedLine[i + 1].trim();
                            dataView = new JLDataView(dvName);
                            dataView.setFixed(true);
                            views[i] = dataView;
                            loadDataView.put(dvName, dataView);
                            dataView.setViewType(IChartViewer.TYPE_LINE);
                        }
                        double time = 0;
                        // double minTime = Double.MAX_VALUE, maxTime =
                        // -Double.MAX_VALUE;
                        while (true) {
                            line = reader.readLine();
                            if (line == null) {
                                break;
                            }
                            parsedLine = line.split("\t");
                            if (parsedLine.length - 1 != viewCount) {
                                throw new Exception();
                            }
                            if (annotationType == JLAxis.TIME_ANNO) {
                                Date date = ChartUtils.parseTimeString(parsedLine[0]);
                                if (date == null) {
                                    // error on this line, try to read the other
                                    // ones
                                    continue;
                                } else {
                                    time = date.getTime();
                                }
                            } else {
                                try {
                                    time = Double.parseDouble(parsedLine[0]);
                                } catch (NumberFormatException nfe) {
                                    if ("NaN".equalsIgnoreCase(parsedLine[0])) {
                                        time = Double.NaN;
                                    } else {
                                        // error on this line, try to read the
                                        // other ones
                                        continue;
                                    }
                                }
                            }
                            // if (time > maxTime) maxTime = time;
                            // if (time < minTime) minTime = time;
                            for (int i = 0; i < views.length; i++) {
                                try {
                                    views[i].add(time, Double.parseDouble(parsedLine[i + 1]));
                                } catch (NumberFormatException nfe) {
                                    if ("null".equalsIgnoreCase(parsedLine[i + 1].trim())) {
                                        // case null
                                        views[i].add(time, MathConst.NAN_FOR_NULL);
                                    } else {
                                        // no data at this time
                                        continue;
                                    }
                                }
                            }
                        }
                        for (JLDataView view : views) {
                            int axis = IChartViewer.Y1;
                            if (dataViewAxis.containsKey(view.getId())) {
                                axis = dataViewAxis.get(view.getId());
                            }
                            addDataViewToAxis(view, axis, false);
                        }
                        setAxisAnnotation(annotationType, IChartViewer.X);
                        measureAndPaint();
                    }
                }
            } catch (Exception e) {
                String errorMessage = "Failed to load file: " + fileName + ObjectUtils.NEW_LINE + e.getMessage();
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(errorMessage, e);
                JOptionPane.showMessageDialog(this, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
            } finally {
                if (reader != null) {
                    if (transferEventDelegate != null) {
                        transferEventDelegate.changeDataLocation(lastDataFileLocation, false);
                    }
                    try {
                        reader.close();
                    } catch (IOException e) {
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to close file: " + fileName, e);
                    }
                }
            }
        }
    }

    public void setColEditable(List<Integer> colEditable) {
        if (theTable != null) {
            theTable.setColEditable(colEditable);
        }
    }

    private void computeColEditable() {
        if (theTable != null) {
            List<Integer> colEditable = null;
            if (isEditable()) {
                colEditable = new ArrayList<Integer>();
                Set<String> keySet = dataViewEditable.keySet();
                Iterator<String> key = keySet.iterator();
                String columnName = null;
                boolean editable = false;
                int index = -1;
                while (key.hasNext()) {
                    columnName = key.next();
                    editable = dataViewEditable.get(columnName);
                    if (editable) {
                        index = theTable.getColumnIndexForName(columnName);
                        colEditable.add(index);
                    }
                }
            }
            theTable.setColEditable(colEditable);
        }
    }

    public List<Integer> getColEditable() {
        List<Integer> result;
        if (theTable != null) {
            result = theTable.getColEditable();
        } else {
            result = null;
        }
        return result;
    }

    /**
     * @return a vector of all JLDataview set on the axis
     */
    public List<JLDataView> getViews() {
        return getViews(false);
    }

    /**
     * @return a vector of all JLDataview set on the axis
     */
    protected List<JLDataView> getViews(boolean yOnly) {
        List<JLDataView> views;
        if ((!yOnly) && xAxis.isXY()) {
            views = xAxis.getViewsCopy();
            views.addAll(y1Axis.getViewsCopy());
        } else {
            views = y1Axis.getViewsCopy();
        }
        views.addAll(y2Axis.getViewsCopy());
        return views;
    }

    /**
     * @return a vector of all JLDataview set on the axis
     */
    public List<JLDataView> getClickableViews() {
        List<JLDataView> y1Views = y1Axis.getViewsCopy();
        List<JLDataView> y2Views = y2Axis.getViewsCopy();
        int separatorIndex = y1Views.size();
        if (separatorIndex == 0) {
            separatorIndex = -1;
        }
        List<JLDataView> possibleViews = new ArrayList<JLDataView>();
        for (JLDataView view : y1Views) {
            if (view.isClickable()) {
                possibleViews.add(view);
            }
        }
        y1Views.clear();
        y1Views = null;
        for (JLDataView view : y2Views) {
            if (view.isClickable()) {
                possibleViews.add(view);
            }
        }
        y2Views.clear();
        y2Views = null;
        if (xAxis.isXY()) {
            List<JLDataView> xViews = xAxis.getViewsCopy();
            for (JLDataView view : xViews) {
                if (view.isClickable()) {
                    possibleViews.add(view);
                }
            }
            xViews.clear();
            xViews = null;
        }

        return possibleViews;
    }

    public void refreshTableAll() {
        if ((theTable != null) && (tableWindow != null) && (tableWindow.isVisible()) && showAllMode) {
            // we get all the views to refresh and updateTable with it
            List<JLDataView> clickableViews = getClickableViews();
            List<JLDataView> toRefresh;
            synchronized (this) {
                if (selectedIds == null) {
                    toRefresh = clickableViews;
                } else {
                    toRefresh = new ArrayList<JLDataView>();
                    for (String id : selectedIds) {
                        JLDataView view = getDataView(id);
                        if (view != null) {
                            toRefresh.add(view);
                        }
                    }
                }
            }
            clickableViews = null;
            updateTableData(toRefresh);
        }
    }

    private void updateTableData(List<JLDataView> views) {
        JLDataView[] array = views.toArray(new JLDataView[views.size()]);
        views.clear();
        ChartUtils.updateTableContent(theTable, timePrecision, xAxis.getAnnotation() == JLAxis.TIME_ANNO, xAxis.isXY(),
                getIndexColumnName(), getDateColumnName(), array);
    }

    private void updateTableDataSingle(String id) {
        ChartUtils.updateTableContent(theTable, timePrecision, xAxis.getAnnotation() == JLAxis.TIME_ANNO, xAxis.isXY(),
                getIndexColumnName(), getDateColumnName(), getDataView(id));
    }

    // Display a JTable containing data of a single dataView
    private void showTableSingle(String id) {
        JLDataView v = getDataView(id);
        if (v != null) {
            initTable();

            initTableWindow();

            // save the view which is displayed in the table
            currentViewShown = v;

            updateTableDataSingle(id);

            ChartUtils.centerTableWindow(tableWindow, theTable);
            tableWindow.setVisible(true);

            tableWindow.repaint();

            computeColEditableSingle(id);
        }
    }

    protected void initTable() {
        if (theTable == null) {
            theTable = new JTableRow();
            theTable.setEditable(editable);
            theTable.addTableRowListener(this);
            if (tableListener != null) {
                theTable.addTableRowListener(tableListener);
            }
        }
    }

    protected void initTableWindow() {
        tableWindow = ChartUtils.getInitializedTableWindow(tableWindow, theTable, preferDialog, modalDialog, this);
    }

    private void computeColEditableSingle(final String colName) {
        if (dataViewEditable.containsKey(colName)) {
            if (dataViewEditable.get(colName)) {
                List<Integer> editable = new ArrayList<Integer>();
                editable.add(1);
                setColEditable(editable);
            } else {
                setColEditable(null);
            }
        }
    }

    // Displays a dialog in which user can set precision (for table
    // construction)
    private void displayPrecisionDialog() {
        ChartUtils.displayPrecisionDialog(parent);
    }

    // Extract the date to display in order to overwrite it in extends class
    protected void showTableAll(List<JLDataView> views) {
        TabbedLine tl;

        initTable();

        tl = new TabbedLine(views.size());
        // -------precision-------//
        tl.setPrecision(timePrecision);
        // -------precision-------//
        for (int v = 0; v < views.size(); v++) {
            tl.add(v, views.get(v));
        }

        initTableWindow();

        // Build data
        ChartUtils.updateTableContent(theTable, xAxis.getAnnotation() == JLAxis.TIME_ANNO, xAxis.isXY(), tl);

        ChartUtils.centerTableWindow(tableWindow, theTable);
        tableWindow.setVisible(true);
    }

    // Display a JTable containing all data of the chart
    protected void showTableAll() {
        showAllMode = true;
        currentViewShown = null;
        synchronized (this) {
            if (selectedIds != null) {
                selectedIds.clear();
            }
            selectedIds = null;
        }
        showTableAll(getClickableViews());
        computeColEditable();
    }

    protected void showStatAll(List<JLDataView> views) {
        ChartUtils.showStatAll(views, "all", xAxis.getAnnotation() == JLAxis.TIME_ANNO, xAxis.getLabelFormat(),
                xAxis.getDateFormat(), this);

    }

    // Display a Dialog containing all statistics of the chart
    protected void showStatAll() {
        showStatAll(getClickableViews());
    }

    // Display a Dialog containing The dataView statistics
    private void showStatSingle(String id) {
        JLDataView v = getDataView(id);
        if (v != null) {
            ChartUtils.showStatAll(Arrays.asList(v), v.getDisplayName(), xAxis.getAnnotation() == JLAxis.TIME_ANNO,
                    xAxis.getLabelFormat(), xAxis.getDateFormat(), this);
        }
    }

    // -----------------------------------------------------
    // Action listener
    // -----------------------------------------------------
    @Override
    public void actionPerformed(ActionEvent evt) {

        Object src = evt.getSource();

        if (src == optionMenuItem) {
            showOptionDialog();
        } else if (src == zoomBackMenuItem || src == zoomButton) {
            exitZoom();
        } else if (src == printMenuItem) {
            fireActionPerformed(PRINT_CHART, true);
        } else if (src == saveSnapshotMenuItem) {
            chartMenu.setVisible(false);
            fireActionPerformed(CHART_TO_IMAGE_FILE, true);
        } else if (src == makeSnapshotMenuItem) {
            makeSnaphot();
        } else if (src == clearSnapshotMenuItem) {
            clearSnapshots();
        } else if (src == precisionMenuItem) {
            displayPrecisionDialog();
        } else if (src == loadDataMenuItem) {
            String filename = CometeUtils.showFileChooserDialog(this, true);
            loadDataFile(filename);
        } else if (src == tableAllMenuItem) {
            showTableAll();
        } else if (src == statAllMenuItem) {
            showStatAll();
        } else if (src == tableDialogMenuItem) {

            DialogList<JLDataView> dialog = new DialogList<JLDataView>(CometeUtils.getWindowForComponent(this),
                    "Table selection", true, dialogListSave, false, JLDataView.class);
            List<JLDataView> result = dialog.showOptionDialog(CometeUtils.getWindowForComponent(this));
            if (result.size() == 1) {
                synchronized (this) {
                    if (selectedIds != null) {
                        selectedIds.clear();
                    }
                    selectedIds = null;
                }
                showTableSingle(result.get(0).getId());
            } else if (result.size() > 1) {
                synchronized (this) {
                    if (selectedIds == null) {
                        selectedIds = new ArrayList<String>();
                    } else {
                        selectedIds.clear();
                    }
                    for (JLDataView view : result) {
                        selectedIds.add(view.getId());
                    }
                }
                showTableAll(result);
            }
        } else if (src == statDialogMenuItem) {

            DialogList<JLDataView> dialog = new DialogList<JLDataView>(CometeUtils.getWindowForComponent(this),
                    "Stats selection", true, dialogListSave, false, JLDataView.class);
            List<JLDataView> result = dialog.showOptionDialog(CometeUtils.getWindowForComponent(this));
            if (result.size() == 1) {
                showStatSingle(result.get(0).getId());
            } else if (result.size() > 1) {
                showStatAll(result);
            }
        } else if (src == dvDialogMenuItem) {

            DialogList<JLDataView> dialog = new DialogList<JLDataView>(CometeUtils.getWindowForComponent(this),
                    "Stats selection", true, dialogListSave, true, JLDataView.class);
            List<JLDataView> result = dialog.showOptionDialog(CometeUtils.getWindowForComponent(this));
            if (result.size() == 1) {
                showDataOptionDialog(result.get(0));
            }
        } else if (src == expressionMenuItem) {
            showExpressionDialog();
        } else if (src == expressionClearMenuItem) {
            clearExpressions();
        } else if (src == configurationLoadMenuItem) {

            CfFileReader r = new CfFileReader();

            // Read and browse the file
            JFileChooserFilter chooser = new JFileChooserFilter(configurationLoadMenuItem.getText(), "txt",
                    "text file");
            String fileName = chooser.showChooserDialog(this);

            // fileName equals "" when user clicks on cancel
            if (!fileName.isEmpty()) {
                if (r.readFile(fileName)) {
                    this.applyConfiguration(r);
                } else {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .warn("Failed to read given text " + fileName + "\nThe configuration won't be applied");
                }
            }
        } else if (src == saveFileMenuItem) {

            String lastDataFileLocation;
            if (transferEventDelegate == null) {
                lastDataFileLocation = ChartUtils.DOT;
            } else {
                lastDataFileLocation = transferEventDelegate.getDataDirectory();
            }
            int ok = JOptionPane.YES_OPTION;
            JFileChooser chooser = new JFileChooser(lastDataFileLocation);
            chooser.addChoosableFileFilter(new MultiExtFileFilter("Text files", "txt"));
            chooser.setDialogTitle("Save Graph Data (Text file with TAB separated fields)");
            int returnVal = chooser.showSaveDialog(this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File f = chooser.getSelectedFile();
                if (f != null) {
                    if (FileUtils.getExtension(f) == null) {
                        f = new File(f.getAbsolutePath() + ".txt");
                    }
                    if (f.exists()) {
                        ok = JOptionPane.showConfirmDialog(this, "Do you want to overwrite " + f.getName() + " ?",
                                "Confirm overwrite", JOptionPane.YES_NO_OPTION);
                    }
                    if (ok == JOptionPane.YES_OPTION) {
                        lastDataFileLocation = f.getParentFile().getAbsolutePath();
                        saveDataFile(f.getAbsolutePath());
                    }
                }
            }
        } else {

            // Search in user action
            boolean found = false;
            int i = 0;
            while (i < userActionMenuItem.length && !found) {
                found = userActionMenuItem[i] == evt.getSource();
                if (!found) {
                    i++;
                }
            }
            if (found) {
                if (userActionMenuItem[i] instanceof JCheckBoxMenuItem) {
                    JCheckBoxMenuItem c = (JCheckBoxMenuItem) userActionMenuItem[i];
                    fireActionPerformed(c.getText(), c.getState());
                } else {
                    fireActionPerformed(userActionMenuItem[i].getText(), false);
                }
            } else {
                // Search in show Data View option menu
                String id = null;
                for (JMenuItem item : dvMenuItem) {
                    if (item == evt.getSource()) {
                        id = item.getName();
                    }
                }
                if (id == null) {
                    showAllMode = false;
                    // Search in show table single menu item
                    for (JMenuItem item : tableSingleMenuItem) {
                        if (item == evt.getSource()) {
                            id = item.getName();
                        }
                    }
                    if (id == null) {
                        // Search in show stat single menu item
                        for (JMenuItem item : statSingleMenuItem) {
                            if (item == evt.getSource()) {
                                id = item.getName();
                            }
                        }
                        if (id != null) {
                            showStatSingle(id);
                        }
                    } else {
                        showTableSingle(id);
                    }
                } else {
                    showDataOptionDialog(getDataView(id));
                }
            }
        }
    }

    // paint Header
    private void paintHeader(Graphics2D g) {
        // Draw header
        if (headerR.width > 0) {
            g.setFont(headerFont);
            int xpos = ((headerR.width - headerWidth) / 2);
            g.setColor(headerColor);
            g.drawString(header, xpos, headerR.y + g.getFontMetrics(headerFont).getAscent() - 1);
        }

    }

    // Compute size of graph items (Axe,label,header,....
    private void measureGraphItems() {
        // we manually handle the time progression
        if (areaZoom) {
            double current = System.currentTimeMillis();
            double time = current - oldTime;
            oldTime = current;
            xAxis.setMinimum(xAxis.getMinimum() + time);
            xAxis.setMaximum(xAxis.getMaximum() + time);
        }

        List<JLDataView> views = getViews(true);
        Rectangle2D bounds = null;
        int MX = margin.width;
        int MY = margin.height;

        // Reset sizes ------------------------------------------------------
        headerR.setBounds(0, 0, 0, 10);
        viewR.setBounds(0, 0, 0, 0);
        headerWidth = 0;
        axisWidth = 0;
        axisHeight = 0;
        y1AxisThickness = 0;
        y2AxisThickness = 0;
        if (lastFontRenderContext != null) {
            // Measure header
            // ------------------------------------------------------
            if (headerVisible && (header != null) && (headerFont != null)) {
                bounds = headerFont.getStringBounds(header, lastFontRenderContext);
                headerWidth = (int) bounds.getWidth();
                headerR.setBounds(MX, MY, lastWidth - 2 * MX, (int) bounds.getHeight() + 5);
            }

            // Measure view Rectangle
            // --------------------------------------------
            switch (labelMode) {
                case IChartViewer.LABEL_UP:
                    viewR.setBounds(MX, MY + headerR.height, lastWidth, lastHeight - headerR.height);
                    break;
                case IChartViewer.LABEL_DOWN:
                case IChartViewer.LABEL_ROW:
                    viewR.setBounds(MX, MY + headerR.height, lastWidth - 2 * MX, lastHeight - 2 * MY - headerR.height);
                    break;
                case IChartViewer.LABEL_RIGHT:
                    viewR.setBounds(MX, MY + headerR.height, lastWidth - 2 * MX, lastHeight - 2 * MY - headerR.height);
                    break;
                case IChartViewer.LABEL_LEFT:
                    viewR.setBounds(MX, MY + headerR.height, lastWidth - 2 * MX, lastHeight - 2 * MY - headerR.height);
                    break;
            }

            // Measure Axis
            // ------------------------------------------------------
            xAxisThickness = xAxis.getLabelFontDimension(lastFontRenderContext, true);
            if (xAxis.getOrientation() == IChartViewer.HORIZONTAL_UP) {
                xAxisUpMargin = xAxisThickness / 2;
            } else {
                xAxisUpMargin = 0;
            }

            axisHeight = viewR.height - xAxisThickness;

            xAxis.computeXScale(views);
            y1Axis.measureAxis(lastFontRenderContext, 0, axisHeight);
            y2Axis.measureAxis(lastFontRenderContext, 0, axisHeight);
            y1AxisThickness = y1Axis.getThickness();
            if (y1AxisThickness == 0) {
                y1AxisThickness = 5;
            }
            y2AxisThickness = y2Axis.getThickness();
            if (y2AxisThickness == 0) {
                y2AxisThickness = 5;
            }

            axisWidth = viewR.width - (y1AxisThickness + y2AxisThickness);

            xAxis.measureAxis(lastFontRenderContext, axisWidth, 0);
        }
    }

    // Paint the zoom mode label
    private void paintZoomButton(int x, int y) {
        if (isZoomed()) {
            int w = zoomButton.getPreferredSize().width;
            int h = zoomButton.getPreferredSize().height;
            zoomButton.setBounds(x + 7, y + 5, w, h);
            zoomButton.setVisible(w < (axisWidth - 7) && h < (axisHeight - 5));
        } else {
            zoomButton.setVisible(false);
        }
    }

    // Paint the zoom rectangle
    private void paintZoomSelection(Graphics g) {

        if (zoomDrag) {
            g.setColor(Color.black);
            // Draw rectangle
            Rectangle r = buildRect(zoomX, zoomY, lastX, lastY);
            g.drawRect(r.x, r.y, r.width, r.height);
        }

    }

    /**
     * Paint the components. Use the repaint method to repaint the graph.
     * 
     * @param g
     *            Graphics object.
     */
    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        boolean measure = false;
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        if (lastFontRenderContext == null) {
            lastFontRenderContext = g2.getFontRenderContext();
            measure = true;
        }

        g.setPaintMode();

        if (isOpaque()) {
            g.setColor(getBackground());
            g.fillRect(0, 0, lastWidth, lastHeight);
        }

        if (measure) {
            lastWidth = getWidth();
            lastHeight = getHeight();
            // Compute bounds of label and graph
            measureGraphItems();
        }

        // Draw label and header
        paintHeader(g2);

        // Paint chart background
        int xOrg = viewR.x + y1AxisThickness;
        int yOrg = viewR.y + axisHeight + xAxisUpMargin;

        int xOrgY1 = viewR.x;
        int xOrgY2 = viewR.x + y1AxisThickness + axisWidth;
        int yOrgY = viewR.y + xAxisUpMargin;

        if (((!isOpaque()) || (!chartBackground.equals(getBackground()))) && axisWidth > 0 && axisHeight > 0) {
            g.setColor(chartBackground);
            g.fillRect(xOrg, yOrg - axisHeight, axisWidth, axisHeight);
        }

        // Paint zoom stuff
        paintZoomSelection(g);
        paintZoomButton(xOrg, yOrgY);

        if (paintAxisFirst) {

            // Draw axes
            drawAxes(g, lastFontRenderContext, xOrg, yOrg, xOrgY1, xOrgY2, yOrgY);

            // Draw data
            drawData(g, xOrg, yOrg);
        } else {
            // Draw data
            drawData(g, xOrg, yOrg);

            // Draw axes
            drawAxes(g, lastFontRenderContext, xOrg, yOrg, xOrgY1, xOrgY2, yOrgY);
        }

        redrawPanel(g);

        // Paint swing stuff
        paintComponents(g);
        paintBorder(g);
    }

    private void drawAxes(Graphics g, FontRenderContext frc, int xOrg, int yOrg, int xOrgY1, int xOrgY2, int yOrgY) {
        y1Axis.paintAxis(g, frc, xOrgY1, yOrgY, xAxis, xOrg, yOrg, getBackground(),
                !y2Axis.isVisible() || y2Axis.getViewNumber() == 0);
        y2Axis.paintAxis(g, frc, xOrgY2, yOrgY, xAxis, xOrg, yOrg, getBackground(),
                !y1Axis.isVisible() || y1Axis.getViewNumber() == 0);
        if (xAxis.getPosition() == IChartViewer.HORIZONTAL_ORGY2) {
            xAxis.paintAxis(g, frc, xOrg, yOrg, y2Axis, 0, 0, getBackground(), true);
        } else {
            xAxis.paintAxis(g, frc, xOrg, yOrg, y1Axis, 0, 0, getBackground(), true);
        }
    }

    private void drawData(Graphics g, int xOrg, int yOrg) {
        Rectangle clipRect = g.getClipBounds();
        y1Axis.paintDataViews(g, xAxis, xOrg, yOrg, isLimitDrawingZone());
        y2Axis.paintDataViews(g, xAxis, xOrg, yOrg, isLimitDrawingZone());
        if (isLimitDrawingZone()) {
            if (clipRect != null) {
                g.setClip(clipRect.x, clipRect.y, clipRect.width, clipRect.height);
            } else {
                g.setClip(null);
            }
        }
        clipRect = null;
    }

    // Build a valid rectangle with the given coordinates
    private Rectangle buildRect(int x1, int y1, int x2, int y2) {

        Rectangle r = new Rectangle();

        if (x1 < x2) {
            if (y1 < y2) {
                r.setRect(x1, y1, x2 - x1, y2 - y1);
            } else {
                r.setRect(x1, y2, x2 - x1, y1 - y2);
            }
        } else {
            if (y1 < y2) {
                r.setRect(x2, y1, x1 - x2, y2 - y1);
            } else {
                r.setRect(x2, y2, x1 - x2, y1 - y2);
            }
        }

        return r;
    }

    // a zoom selection must be contained in the graph zone and big enough
    private boolean zoomSelectionOK(Rectangle r) {

        // check the position of the selection
        int limitUP = viewR.y + xAxisUpMargin;
        int limitDOWN = y1Axis.getLength() + limitUP;
        int limitLEFT = y1AxisThickness + viewR.x;
        int limitRIGHT = xAxis.getLength() + limitLEFT;

        // if the whole selection is not contained in the graphe zone OK = false
        // selection in the left margin
        if ((r.x < limitLEFT) && (r.x + r.width < limitLEFT)) {
            return false;
        }

        // selection in the right margin
        if ((r.x > limitRIGHT)) {
            return false;
        }

        // selection in the upper margin
        if ((r.y < limitUP) && (r.y + r.height < limitUP)) {
            return false;
        }

        // selection in the lower margin
        if (r.y > limitDOWN) {
            return false;
        }

        // if it's just a part, we adjust
        if (r.x < limitLEFT) {
            int newWidth = r.width - (limitLEFT - r.x);
            r.setBounds(limitLEFT, r.y, newWidth, r.height);
        }

        if (r.y < limitUP) {
            int newHeight = r.height - (limitUP - r.y);
            r.setBounds(r.x, limitUP, r.width, newHeight);
        }

        if (r.x + r.width > limitRIGHT) {
            r.setBounds(r.x, r.y, limitRIGHT - r.x, r.height);
        }

        if (r.y + r.height > limitDOWN) {
            r.setBounds(r.x, r.y, r.width, limitDOWN - r.y);
        }

        // test if the selection is not too small
        if (r.width < 10 || r.height < 10) {
            return false;
        }

        // if everything is OK or adjust
        return true;
    }

    // we do the zoom
    private void applyZoom(int x, int y) {
        // we set the zoom rectangle (size in px)
        Rectangle zoomRect = buildRect(zoomX, zoomY, x, y);

        // if the zoom is not too small and well-positioned we apply it
        if (zoomSelectionOK(zoomRect)) {

            // there are 2 types of zoom: areaZoom , fixed a zone ; zoom, fixed
            // a span of time
            if (areaZoom) {

                // if it's the first zoom, we save information to be able to get
                // back
                if (!isZoomed) {
                    oldAutoScaleY1 = y1Axis.isAutoScale();
                    oldAutoScaleY2 = y2Axis.isAutoScale();
                    oldMinY1 = y1Axis.getMinimum();
                    oldMaxY1 = y1Axis.getMaximum();
                    oldMinY2 = y2Axis.getMinimum();
                    oldMaxY2 = y2Axis.getMaximum();
                }

                // we set the min and the max for the Axis
                // we compute in comparison with the Y origin (upper left)
                // for the min we need to transform the height of the zoom into
                // value
                int yOrgY = viewR.y + xAxisUpMargin;

                double valLength1 = y1Axis.getScaleMaximum() - y1Axis.getScaleMinimum();
                double maxY1 = y1Axis.getScaleMaximum() - (((zoomRect.y - yOrgY) * valLength1) / y1Axis.getLength());
                double minY1 = maxY1 - (zoomRect.height * valLength1 / y1Axis.getLength());

                y1Axis.setAutoScale(false);

                y1Axis.setMaximum(maxY1);
                y1Axis.setMinimum(minY1);

                double valLength2 = y2Axis.getScaleMaximum() - y2Axis.getScaleMinimum();
                double maxY2 = y2Axis.getScaleMaximum() - (((zoomRect.y - yOrgY) * valLength2) / y2Axis.getLength());
                double minY2 = maxY2 - (zoomRect.height * valLength2 / y2Axis.getLength());

                y2Axis.setAutoScale(false);
                y2Axis.setMaximum(maxY2);
                y2Axis.setMinimum(minY2);

                // we convert the width in px into duration
                xAxis.setAutoScale(false);
                double duration = (zoomRect.width * getDisplayDuration()) / xAxis.getLength();
                double maxX = xAxis.getScaleMaximum();

                xAxis.setMaximum(maxX);
                xAxis.setMinimum(maxX - duration);

                oldTime = System.currentTimeMillis();

            } else {
                xAxis.zoom(zoomRect.x, zoomRect.x + zoomRect.width);
                y1Axis.zoom(zoomRect.y, zoomRect.y + zoomRect.height);
                y2Axis.zoom(zoomRect.y, zoomRect.y + zoomRect.height);
            }

            // we set that the graph is zoomed
            isZoomed = true;
        }
    }

    // allow us to know if we are doing a fixed area zoom or a fixed time zoom
    public void setAreaZoom(boolean b) {
        areaZoom = b;
    }

    // ************************************************************************
    // Mouse Listener
    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (zoomDrag) {
            int repaintMaxX;
            int repaintMinX = Math.min(e.getX(), Math.min(lastX, zoomX));
            int repaintMinY = Math.min(e.getY(), Math.min(lastY, zoomY));
            int repaintMaxY = Math.max(e.getY(), Math.max(lastY, zoomY));

            // if we do a fixed area zoom, maxX is right graph border
            if (areaZoom) {
                int limitLEFT = y1AxisThickness + viewR.x;
                int limitRIGHT = xAxis.getLength() + limitLEFT;

                repaintMaxX = limitRIGHT;
                lastX = repaintMaxX;

            } else {
                repaintMaxX = Math.max(e.getX(), Math.max(lastX, zoomX));
                lastX = e.getX();
            }

            // create repaint rectangle
            final Rectangle repaintRect = buildRect(repaintMinX, repaintMinY, repaintMaxX, repaintMaxY);
            repaintRect.width += 1;
            repaintRect.height += 1;

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    repaint(repaintRect);
                }
            });

            // we save current coordinate
            lastY = e.getY();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        boolean measure = false;
        if (zoomDrag) {
            measure = true;
            zoomDrag = false;

            // if we are in the area zoom mode lastX contains
            // the position of the right graph border
            if (areaZoom) {
                applyZoom(lastX, e.getY());
            } else {
                applyZoom(e.getX(), e.getY());
            }
        }

        SearchInfo search = lastSearch;
        if (search != null) {
            AbstractDataView view = search.getDataView();
            DataList value = search.getValue();
            if (value != null) {
                fireSelectionChanged(value.getX(), value.getY(), search.getClickIdx(),
                        (view == null ? null : view.getId()));
            }
        }

        displayableTooltipText = null;
        if (displayableTooltipEvent != null) {
            displayableTooltipEvent.consume();
            displayableTooltipEvent = null;
        }
        if (infoPopup != null) {
            infoPopup.hide();
            infoPopup = null;
        }
        ipanelVisible = false;
        displayableTooltipText = null;

        if (measure) {
            measureGraphItems();
        }
        repaint();
    }

    protected void fireSelectionChanged(double x, double y, int index, String viewId) {
        ChartViewerEvent event = new ChartViewerEvent(parent, new double[] { x, y }, index, viewId, null,
                Reason.SELECTION);
        List<IChartViewerListener> copy = new ArrayList<IChartViewerListener>();
        synchronized (chartListeners) {
            copy.addAll(chartListeners);
        }
        double[] point = new double[2];
        point[IChartViewer.X_INDEX] = x;
        point[IChartViewer.Y_INDEX] = y;
        for (IChartViewerListener listener : copy) {
            if (listener != null) {
                listener.chartViewerChanged(event);
            }
        }
        copy.clear();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            // Left button click

            // Zoom management
            if (e.isControlDown() || zoomDragAllowed) {
                zoomDrag = true;
                zoomX = e.getX();
                zoomY = e.getY();
                lastX = e.getX();
                lastY = e.getY();
            } else {
                SearchInfo axisSearch;

                // Look for the nearest value on each dataView
                lastSearch = y1Axis.searchNearest(e.getX(), e.getY(), xAxis);
                axisSearch = y2Axis.searchNearest(e.getX(), e.getY(), xAxis);
                if (axisSearch.isFound() && axisSearch.getDist() < lastSearch.getDist()) {
                    lastSearch = axisSearch;
                }

                if ((lastSearch != null) && lastSearch.isFound()) {
                    if (displayableTooltipEvent == null) {
                        displayableTooltipEvent = new MouseEvent(this, e.getID(), e.getWhen(), e.getModifiers(),
                                e.getX(), e.getY(), e.getClickCount(), e.isPopupTrigger());
                    }
                    Graphics g = getGraphics();
                    showPanel(g, lastSearch);
                    g.dispose();
                }
            }
        } else if (e.getButton() == MouseEvent.BUTTON3) {
            // Right button click
            int i;

            zoomBackMenuItem.setEnabled(isZoomed());
            // Gets user action state
            for (i = 0; i < userActionMenuItem.length; i++) {
                if (userActionMenuItem[i] instanceof JCheckBoxMenuItem) {
                    JCheckBoxMenuItem b = (JCheckBoxMenuItem) userActionMenuItem[i];
                    b.setSelected(fireGetActionState(b.getText()));
                }
            }

            // Add dataView table item
            tableMenu.removeAll();
            tableMenu.add(tableAllMenuItem);
            // Add dataView option menu
            dvMenu.removeAll();
            // DataView Statistics item
            statMenu.removeAll();
            statMenu.add(statAllMenuItem);

            // -------- //
            List<JLDataView> y1Views = y1Axis.getViewsCopy();
            List<JLDataView> y2Views = y2Axis.getViewsCopy();
            int separatorIndex = y1Views.size();
            if (separatorIndex == 0) {
                separatorIndex = -1;
            }
            List<JLDataView> possibleViews = new ArrayList<JLDataView>();
            for (JLDataView view : y1Views) {
                if (view.isClickable()) {
                    possibleViews.add(view);
                }
            }
            y1Views.clear();
            y1Views = null;
            for (JLDataView view : y2Views) {
                if (view.isClickable()) {
                    possibleViews.add(view);
                }
            }
            y2Views.clear();
            y2Views = null;
            if (possibleViews.size() > 0) {
                tableMenu.addSeparator();
                statMenu.addSeparator();
            }

            if (possibleViews.size() > maxDataViewShown && maxDataViewShown != -1) {

                tableMenu.add(tableDialogMenuItem);
                dvMenu.add(dvDialogMenuItem);
                statMenu.add(statDialogMenuItem);
                dialogListSave = possibleViews;
            } else {

                for (JMenuItem item : tableSingleMenuItem) {
                    item.removeActionListener(this);
                }
                for (JMenuItem item : dvMenuItem) {
                    item.removeActionListener(this);
                }
                for (JMenuItem item : statSingleMenuItem) {
                    item.removeActionListener(this);
                }

                tableSingleMenuItem = new JMenuItem[possibleViews.size()];
                dvMenuItem = new JMenuItem[possibleViews.size()];
                statSingleMenuItem = new JMenuItem[possibleViews.size()];

                int unamedDv = 1;
                for (i = 0; i < possibleViews.size(); i++) {
                    if (i == separatorIndex) {
                        tableMenu.addSeparator();
                        dvMenu.addSeparator();
                        statMenu.addSeparator();
                    }
                    String dvName = possibleViews.get(i).getDisplayName();
                    if ((dvName == null) || dvName.isEmpty()) {
                        dvName = "Dataview #" + unamedDv;
                        unamedDv++;
                    }
                    tableSingleMenuItem[i] = new JMenuItem(dvName);
                    tableSingleMenuItem[i].setName(possibleViews.get(i).getId());
                    tableSingleMenuItem[i].addActionListener(this);
                    tableMenu.add(tableSingleMenuItem[i]);
                    dvMenuItem[i] = new JMenuItem(dvName);
                    dvMenuItem[i].setName(possibleViews.get(i).getId());
                    dvMenuItem[i].addActionListener(this);
                    dvMenu.add(dvMenuItem[i]);
                    statSingleMenuItem[i] = new JMenuItem(dvName);
                    statSingleMenuItem[i].setName(possibleViews.get(i).getId());
                    statSingleMenuItem[i].addActionListener(this);
                    statMenu.add(statSingleMenuItem[i]);
                }
            }
            chartMenu.show(this, e.getX(), e.getY());
        }

    }

    // ****************************************
    // component listener

    @Override
    public void componentResized(ComponentEvent e) {
        lastWidth = getWidth();
        lastHeight = getHeight();
        measureGraphItems();
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        // Nothing to do
    }

    @Override
    public void componentShown(ComponentEvent e) {
        if ((lastWidth == 0) && (lastHeight == 0)) {
            componentResized(e);
        }
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        // Nothing to do
    }

    // ****************************************
    // redraw the panel
    private void redrawPanel(Graphics g) {
        if (ipanelVisible) {
            // Udpate serachInfo
            Point p;
            AbstractDataView vy = lastSearch.getDataView();
            AbstractDataView vx = lastSearch.getXDataView();
            DataList dy = lastSearch.getValue();
            DataList dx = lastSearch.getXValue();
            JLAxis yaxis = getAxis(lastSearch.getAxis());
            if (xAxis.isXY()) {
                p = yaxis.transform(vx.getTransformedValue(dx.getY()), vy.getTransformedValue(dy.getY()), xAxis);
            } else {
                p = yaxis.transform(dy.getX(), vy.getTransformedValue(dy.getY()), xAxis);
            }
            lastSearch.setX(p.x);
            lastSearch.setY(p.y);
            showPanel(g, lastSearch);
        }
    }

    protected String[] buildPanelString(SearchInfo si) {
        String[] str = null;
        if (si != null) {
            String xValue;
            double y, ytrans;
            DataList dataList = si.getValue();
            AbstractDataView dataView = si.getDataView();
            if ((dataList != null) && (dataView != null)) {
                String format = dataView.getFormat();
                y = dataList.getY();
                ytrans = dataView.getTransformedValue(y);
                boolean diffY = (y != ytrans);
                int strCount;
                if (xAxis.getAnnotation() == JLAxis.TIME_ANNO) {
                    xValue = "Time = " + ChartUtils.formatTimeValue(dataList.getX());
                } else {
                    xValue = "Index = " + dataList.getX();
                }
                int index = 0;
                StringBuilder nameBuilder = new StringBuilder(dataView.getExtendedName());
                JLAxis axis = getAxis(si.getAxis());
                if (axis != null) {
                    nameBuilder.append(" ").append(axis.getAxeName());
                }
                if (xAxis.isXY()) {
                    double x, xtrans;
                    x = si.getXValue().getY();
                    xtrans = si.getXDataView().getTransformedValue(x);
                    boolean diffX = (x != xtrans);
                    strCount = 4;
                    if (diffX) {
                        strCount++;
                    }
                    if (diffY) {
                        strCount++;
                    }
                    str = new String[strCount];
                    str[index++] = nameBuilder.toString();
                    str[index++] = xValue;
                    str[index++] = "X = " + x;
                    if (diffX) {
                        str[index++] = "X transformed = " + xtrans;
                    }
                } else {
                    strCount = 3;
                    if (diffY) {
                        strCount++;
                    }
                    str = new String[strCount];
                    str[index++] = nameBuilder.toString();
                    str[index++] = xValue;
                }
                str[index++] = "Y = " + (format == null ? y : SwingFormat.formatValue(y, format, true)) + " "
                        + dataView.getUnit();
                if (diffY) {
                    str[index++] = "Y transformed = "
                            + (format == null ? ytrans : SwingFormat.formatValue(ytrans, format, true)) + " "
                            + dataView.getUnit();
                }
            }
        }
        return str;
    }

    @Override
    public String getToolTipText() {
        if (displayableTooltipText == null) {
            return super.getToolTipText();
        } else {
            return displayableTooltipText;
        }
    }

    /**
     * Display the value tooltip.
     * 
     * @param g
     *            Graphics object
     * @param si
     *            SearchInfo structure.
     * @see JLAxis#searchNearest
     */
    public void showPanel(Graphics g, SearchInfo si) {
        Graphics2D g2 = (Graphics2D) g;
        Rectangle2D bounds;
        int maxh = 0;
        // int h = 0;
        int maxw = 0;
        int x0 = 0, y0 = 0;
        String[] str = buildPanelString(si);

        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        FontRenderContext frc = g2.getFontRenderContext();

        g.setPaintMode();
        g.setFont(labelFont);

        // Do not show panel if no text
        if ((str != null) && (str.length > 0)) {
            // Compute panel size
            bounds = g.getFont().getStringBounds(str[0], frc);
            maxw = (int) bounds.getWidth();
            // h = maxh = (int) bounds.getHeight();

            for (int i = 1; i < str.length; i++) {
                bounds = g.getFont().getStringBounds(str[i], frc);
                if ((int) bounds.getWidth() > maxw) {
                    maxw = (int) bounds.getWidth();
                }
                maxh += bounds.getHeight();
            }

            maxw += 10;
            maxh += 10;

            g.setColor(Color.black);

            Point location = null;
            if (displayableTooltipEvent != null) {
                location = displayableTooltipEvent.getLocationOnScreen();
            }

            if (location != null) {
                Point myLocation = getLocationOnScreen();
                if (myLocation != null) {
                    switch (si.getPlacement()) {
                        case SearchInfo.BOTTOMRIGHT:
                            x0 = si.getX() + 10;
                            y0 = si.getY() + 10;
                            g.drawLine(si.getX(), si.getY(), si.getX() + 10, si.getY() + 10);
                            break;
                        case SearchInfo.BOTTOMLEFT:
                            x0 = si.getX() - 10 - maxw;
                            y0 = si.getY() + 10;
                            g.drawLine(si.getX(), si.getY(), si.getX() - 10, si.getY() + 10);
                            break;
                        case SearchInfo.TOPRIGHT:
                            x0 = si.getX() + 10;
                            y0 = si.getY() - 10 - maxh;
                            g.drawLine(si.getX(), si.getY(), si.getX() + 10, si.getY() - 10);
                            break;
                        case SearchInfo.TOPLEFT:
                            x0 = si.getX() - 10 - maxw;
                            y0 = si.getY() - 10 - maxh;
                            g.drawLine(si.getX(), si.getY(), si.getX() - 10, si.getY() - 10);
                            break;
                    }
                    x0 += myLocation.x;
                    y0 += myLocation.y;

                    StringBuilder buffer = new StringBuilder();
                    buffer.append("<html><body>");
                    for (String info : str) {
                        buffer.append("<p>").append(info).append("</p>");
                    }
                    buffer.append("</body></html>");
                    displayableTooltipText = buffer.toString();
                    infoTooltip.setFont(getFont());
                    infoTooltip.setTipText(displayableTooltipText);
                    if (infoPopup == null) {
                        infoPopup = PopupFactory.getSharedInstance().getPopup(this, infoTooltip, x0, y0);
                    }
                    location = null;
                    myLocation = null;
                    infoPopup.show();

                    lastSearch = si;
                    ipanelVisible = true;
                }
            }
        }
    }

    /**
     * Remove points that exceed displayDuration.
     * 
     * @param v
     *            DataView containing points
     * @return Number of deleted points
     */
    public int garbageData(JLDataView v) {

        int nb = 0;

        if (displayDuration != Double.POSITIVE_INFINITY) {
            nb = v.garbagePointTime(displayDuration);
        }

        return nb;
    }

    /**
     * Add data to dataview , perform fast update when possible and garbage old
     * data (if a display duration is specified).
     * 
     * @param v
     *            The dataview
     * @param x
     *            x coordinates (real space)
     * @param y
     *            y coordinates (real space)
     * @see JLChart#setDisplayDuration
     */
    public void addData(JLDataView v, double x, double y) {

        DataList lv = null;
        boolean need_repaint = false;

        // Get the last value
        if (v.getDataLength() > 0) {
            lv = v.getLastValue();
        }

        // Add data
        v.add(x, y);

        // Garbage
        int nb = garbageData(v);
        if (nb > 0 && v.getAxis() != null) {
            need_repaint = true;
        }

        // Does not repaint if zoom drag
        if (!zoomDrag) {
            if (xAxis.isXY()) {
                // Perform full update in XY
                measureAndPaint();
            } else {
                // Compute update
                JLAxis yaxis = v.getAxis();

                if (yaxis != null) {

                    Point lp = null;
                    Point p = yaxis.transform(x, v.getTransformedValue(y), xAxis);
                    if (lv != null) {
                        lp = yaxis.transform(lv.getX(), v.getTransformedValue(lv.getY()), xAxis);
                    }

                    if (yaxis.getBoundRect().contains(p) && !need_repaint) {
                        // We can perform fast update
                        yaxis.drawFast(getGraphics(), lp, p, v);
                    } else {
                        // Full update needed
                        measureAndPaint();
                    }

                }
            }
        }
    }

    /**
     * Sets the allowed margin to make a projection on a line on data show.
     * 
     * @param milliseconds
     *            the margin, in milliseconds
     */
    public void setTimePrecision(int milliseconds) {
        setTimePrecision(milliseconds, true);
    }

    protected void setTimePrecision(int milliseconds, boolean fireChartEvent) {
        timePrecision = milliseconds;
        if (fireChartEvent) {
            fireConfigurationChanged(null);
        }
    }

    /**
     * Returns the allowed margin to make a projection on a line on data show
     * (default: 0).
     * 
     * @return The allowed margin to make a projection on a line on data show
     *         (default: 0).
     */
    public int getTimePrecision() {
        return timePrecision;
    }

    /**
     * Used with saveDataFile(). Returns the String used to represent "no data"
     * (default : "*").
     * 
     * @return The String used to represent "no data"
     */
    public String getNoValueString() {
        return noValueString;
    }

    /**
     * Used with saveDataFile(). Sets the String used to represent "no data"
     * (default : "*").
     * 
     * @param noValueString
     *            The String used to represent "no data"
     */
    public void setNoValueString(String noValueString) {
        setNoValueString(noValueString, true);
    }

    protected void setNoValueString(String noValueString, boolean fireChartEvent) {
        this.noValueString = noValueString;
        if (fireChartEvent) {
            fireConfigurationChanged(null);
        }
    }

    /**
     * Sets if you prefer to use a JDialog with showTableXXX() methods instead
     * of the classic JFrame
     * 
     * @param preferDialog
     *            Prefer to have a dialog or not
     * @param modal
     *            The dialog should be modal or not
     */
    public void setPreferDialogForTable(boolean preferDialog, boolean modal) {
        this.preferDialog = preferDialog;
        modalDialog = modal;
    }

    private void removeDataView(JLDataView view) {
        removeDataView(view, false);
    }

    protected void removeDataView(JLDataView view, boolean fromDAOToo) {
        if (view != null) {
            synchronized (visibleDataView) {
                if (visibleDataView.containsKey(view.getId())) {
                    visibleDataView.remove(view.getId());
                }
            }
            JLAxis axis = view.getAxis();
            if (axis != null) {
                axis.removeDataView(view);
            }
            if (expressionMap.containsKey(view)) {
                expressionMap.remove(view);
                view.reset();
                updateExpressions(view);
            }
            if (fromDAOToo) {
                fireConfigurationChanged(IChartViewer.DATAVIEW_DELETE_ACTION_NAME + view.getId());
            }
            initComboList();
            measureAndPaint();
        }
    }

    /**
     * remove all data views and set the default axis and chart configuration
     */
    public void resetAll() {
        reset(false, true, true);
    }

    public void reset() {
        reset(true);
    }

    protected void reset(boolean showConfirmDialog) {
        reset(showConfirmDialog, false, true);
    }

    public void reset(boolean showConfirmDialog, boolean allViews, boolean clearConfiguration) {
        if (allViews) {
            clearExpressions();
        }
        List<JLDataView> existingViews = new ArrayList<JLDataView>();
        if (xAxis.isXY()) {
            existingViews.addAll(xAxis.getViewsCopy());
        }
        existingViews.addAll(y1Axis.getViewsCopy());
        existingViews.addAll(y2Axis.getViewsCopy());
        int choice = JOptionPane.OK_OPTION;
        if (existingViews.size() != 0 && showConfirmDialog) {
            String warning = "Reseting chart will remove all the existing dataviews.\n"
                    + "Your component may not work any more.\n" + "Are you sure to reset chart ?";
            choice = JOptionPane.showConfirmDialog(this, warning, "Risk of breaking component",
                    JOptionPane.WARNING_MESSAGE);
            if (choice != JOptionPane.OK_OPTION) {
                existingViews.clear();
                existingViews = null;
            }
        }
        if (choice == JOptionPane.OK_OPTION) {
            JLDataView dataView = null;
            for (int i = 0; i < existingViews.size(); i++) {
                dataView = existingViews.get(i);
                if (!dataView.isFixed() || allViews) {
                    removeDataView(dataView);
                    dataView.reset();
                }
            }

            existingViews.clear();
            existingViews = null;

            if (allViews) {
                loadDataView.clear();
                synchronized (snapshots) {
                    snapshots.clear();
                }
                synchronized (hiddenDataView) {
                    hiddenDataView.clear();
                }
                synchronized (visibleDataView) {
                    visibleDataView.clear();
                }
            }

            if (clearConfiguration) {
                maxDisplayDuration = Double.POSITIVE_INFINITY;
                displayDuration = Double.POSITIVE_INFINITY;

                dataViewDisplayName.clear();
                dataViewColor.clear();
                dataViewFillColor.clear();
                dataViewMarkerColor.clear();
                dataViewLineStyle.clear();
                dataViewAxis.clear();
                dataViewMarker.clear();
                dataViewMarkerSize.clear();
                dataViewLineWidth.clear();
                dataViewBarWidth.clear();
                dataViewStyle.clear();
                dataViewFillStyle.clear();
                dataViewFillMethod.clear();
                dataViewInterpolationMethod.clear();
                dataViewInterpolationStep.clear();
                dataViewHermiteBias.clear();
                dataViewHermiteTension.clear();
                dataViewTransformA0.clear();
                dataViewTransformA1.clear();
                dataViewTransformA2.clear();
                dataViewSmoothingMethod.clear();
                dataViewSmoothingNeighbors.clear();
                dataViewSmoothingGaussSigma.clear();
                dataViewSmoothingExtrapolation.clear();
                dataViewMathFunction.clear();
                dataViewHighlightMethod.clear();
                dataViewHighlightCoefficient.clear();
                dataViewHighlighted.clear();
                dataViewEditable.clear();
                dataViewClickable.clear();
                dataViewLabelVisible.clear();
                dataViewSamplingAllowed.clear();

                getY1Axis().setLabels(null, null);
                getY1Axis().setScale(IChartViewer.LINEAR_SCALE);
                getY1Axis().setAutoScale(true);

                getY2Axis().setLabels(null, null);
                getY2Axis().setScale(IChartViewer.LINEAR_SCALE);
                getY2Axis().setAutoScale(true);

                getXAxis().setLabels(null, null);
                getXAxis().setScale(IChartViewer.LINEAR_SCALE);
                getXAxis().setAutoScale(true);
            }
        }
    }

    /**
     * @return true if the table with all dataviews was displayed, false
     *         otherwise
     */
    public boolean getShowAllMode() {
        return showAllMode;
    }

    /**
     * @return the JLDataView shown by the table. null if none or all views are
     *         shown in the table
     */
    public JLDataView getCurrentViewShown() {
        return currentViewShown;
    }

    public void setDataViewDisplayName(String name, String displayName) {
        setDataViewDisplayName(name, displayName, true);
    }

    protected <T> boolean storeData(String name, T data, Map<String, T> dataMap) {
        boolean result = false;
        if ((name != null) && (!name.trim().isEmpty()) && (dataMap != null)
                && (!ObjectUtils.sameObject(data, dataMap.get(name)))) {
            dataMap.put(name, data);
            result = true;
        }
        return result;
    }

    protected void setDataViewDisplayName(String name, String displayName, boolean fireEvent) {
        if (storeData(name, displayName, dataViewDisplayName)) {
            dataViewDisplayName.put(name, displayName);
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setDisplayName(displayName);
                initComboList();
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewColor(String name, Color color) {
        setDataViewColor(name, color, true);
    }

    protected void setDataViewColor(String name, Color color, boolean fireEvent) {
        if (storeData(name, color, dataViewColor)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setColor(color);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewFillColor(String name, Color color) {
        setDataViewFillColor(name, color, true);
    }

    protected void setDataViewFillColor(String name, Color color, boolean fireEvent) {
        if (storeData(name, color, dataViewFillColor)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setFillColor(color);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewEditable(String name, boolean editable) {
        if ((name != null) && (!name.trim().isEmpty())) {
            dataViewEditable.put(name, editable);
        }
    }

    public void setDataViewClickable(String name, boolean clickable) {
        if (storeData(name, clickable, dataViewClickable)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setClickable(clickable);
            }
        }
    }

    public void setDataViewLabelVisible(String name, boolean visible) {
        setDataViewLabelVisible(name, visible, true);
    }

    public void setDataViewLabelVisible(String name, boolean visible, boolean fireEvent) {
        if (storeData(name, visible, dataViewLabelVisible)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setLabelVisible(visible);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewSamplingAllowed(String name, boolean allowed) {
        setDataViewSamplingAllowed(name, allowed, true);
    }

    public void setDataViewSamplingAllowed(String name, boolean allowed, boolean fireEvent) {
        if (storeData(name, allowed, dataViewSamplingAllowed)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setSamplingAllowed(allowed);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewMarkerStyle(String name, int marker) {
        setDataViewMarkerStyle(name, marker, true);
    }

    protected void setDataViewMarkerStyle(String name, int marker, boolean fireEvent) {
        if (storeData(name, marker, dataViewMarker)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setMarker(marker);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewLineWidth(String name, int lineWidth) {
        setDataViewLineWidth(name, lineWidth, true);
    }

    protected void setDataViewLineWidth(String name, int lineWidth, boolean fireEvent) {
        if (storeData(name, lineWidth, dataViewLineWidth)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setLineWidth(lineWidth);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewBarWidth(String name, int barWidth) {
        setDataViewBarWidth(name, barWidth, true);
    }

    protected void setDataViewBarWidth(String name, int barWidth, boolean fireEvent) {
        if (storeData(name, barWidth, dataViewBarWidth)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setBarWidth(barWidth);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewLineStyle(String name, int style) {
        setDataViewLineStyle(name, style, true);
    }

    protected void setDataViewLineStyle(String name, int style, boolean fireEvent) {
        if (storeData(name, style, dataViewLineStyle)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setStyle(style);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewMarkerSize(String name, int size) {
        setDataViewMarkerSize(name, size, true);
    }

    protected void setDataViewMarkerSize(String name, int size, boolean fireEvent) {
        if (storeData(name, size, dataViewMarkerSize)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setMarkerSize(size);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewMarkerColor(String name, Color color) {
        setDataViewMarkerColor(name, color, true);
    }

    protected void setDataViewMarkerColor(String name, Color color, boolean fireEvent) {
        if (storeData(name, color, dataViewMarkerColor)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setMarkerColor(color);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewAxis(String name, int axis) {
        setDataViewAxis(name, axis, true);
    }

    protected void setDataViewAxis(String name, int axis, boolean fireEvent) {
        if (storeData(name, axis, dataViewAxis)) {
            boolean hidden = false;
            if (autoHideViews) {
                synchronized (hiddenDataView) {
                    hidden = hiddenDataView.containsKey(name);
                }
            }
            if ((!isDataViewBelongToAxis(name, axis)) && (!hidden)) {
                // int actualAxis = getDataViewAxis(name);
                JLDataView dataview = getDataView(name);
                // getDataView(name, axis);
                // ystem.out.println("dataview=" + dataview);
                removeDataView(dataview);
                // removeDataViewFromAxis(name, actualAxis);
                addDataViewToAxis(dataview, axis, false);
                measureGraphItems();
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewFormat(String name, String userFormat) {
        if ((name != null) && (!name.trim().isEmpty())) {
            if (!ObjectUtils.sameObject(userFormat, dataViewUserFormat.get(name))) {
                dataViewUserFormat.put(name, userFormat);
                JLDataView dataView = getDataView(name);
                if (dataView != null) {
                    dataView.setFormat(userFormat);
                }
            }
        }
    }

    public void setDataViewUnit(String name, String unit) {
        if ((name != null) && (!name.trim().isEmpty())) {
            if (!ObjectUtils.sameObject(unit, dataViewUnit.get(name))) {
                dataViewUnit.put(name, unit);
                JLDataView dataView = getDataView(name);
                if (dataView != null) {
                    dataView.setUnit(unit);
                }
            }
        }
    }

    public void setDataViewStyle(String name, int style) {
        setDataViewStyle(name, style, true);
    }

    protected void setDataViewStyle(String name, int style, boolean fireEvent) {
        if (storeData(name, style, dataViewStyle)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setViewType(style);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewFillStyle(String name, int style) {
        setDataViewFillStyle(name, style, true);
    }

    protected void setDataViewFillStyle(String name, int style, boolean fireEvent) {
        if (storeData(name, style, dataViewFillStyle)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setFillStyle(style);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    /**
     * Sets the filling method for this view.
     * 
     * @param m
     *            Filling method
     * @see JLDataView#METHOD_FILL_FROM_TOP
     * @see JLDataView#METHOD_FILL_FROM_ZERO
     * @see JLDataView#METHOD_FILL_FROM_BOTTOM
     */
    public void setDataViewFillMethod(String name, int m) {
        setDataViewFillMethod(name, m, true);
    }

    protected void setDataViewFillMethod(String name, int m, boolean fireEvent) {
        if (storeData(name, m, dataViewFillMethod)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setFillMethod(m);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public JLDataView getDataView(String name) {
        JLDataView dataview = getDataView(name, IChartViewer.Y1);
        if (dataview == null) {
            dataview = getDataView(name, IChartViewer.Y2);
        }
        if (dataview == null) {
            synchronized (hiddenDataView) {
                dataview = hiddenDataView.get(name);
            }
        }
        return dataview;
    }

    public JLDataView getDataView(String name, int axis) {
        JLDataView result = null;
        List<JLDataView> dataviews = getDataView(axis);
        if (dataviews != null) {
            for (JLDataView jlDataView : dataviews) {
                if (jlDataView != null && jlDataView.getId().equalsIgnoreCase(name)) {
                    result = jlDataView;
                    break;
                }
            }
            dataviews.clear();
            dataviews = null;
        }
        return result;
    }

    public void setAxisAnnotation(int annotation, int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setAnnotation(annotation);
        }
    }

    public void setAxisSubGridVisible(boolean visible, int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setSubGridVisible(visible);
        }
    }

    public void setAxisGridVisible(boolean visible, int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setGridVisible(visible);
        }
    }

    public void setAxisGridStyle(int style, int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setGridStyle(style);
        }
    }

    public void setAxisScale(int scale, int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setScale(scale);
            measureAndPaint();
        }
    }

    public void setAxisAutoScale(boolean autoscale, int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            jlaxis.setAutoScale(autoscale);
            measureAndPaint();
        }
    }

    public JLAxis getAxis(int axis) {
        JLAxis result;
        switch (axis) {
            case IChartViewer.Y1:
                result = getY1Axis();
                break;

            case IChartViewer.Y2:
                result = getY2Axis();
                break;

            default:
                result = getXAxis();
                break;
        }
        return result;
    }

    public boolean isDataViewBelongToAxis(String name, int axis) {
        return (getDataView(name, axis) != null);
    }

    protected boolean getValue(String name, Map<String, Boolean> valueMap, boolean defaultValue) {
        boolean result;
        if (name == null) {
            result = defaultValue;
        } else {
            Boolean value = valueMap.get(name);
            if (value == null) {
                result = defaultValue;
            } else {
                result = value.booleanValue();
            }
        }
        return result;
    }

    protected int getValue(String name, Map<String, Integer> valueMap, int defaultValue) {
        int result;
        if (name == null) {
            result = defaultValue;
        } else {
            Integer value = valueMap.get(name);
            if (value == null) {
                result = defaultValue;
            } else {
                result = value.intValue();
            }
        }
        return result;
    }

    protected double getValue(String name, Map<String, Double> valueMap, double defaultValue) {
        double result;
        if (name == null) {
            result = defaultValue;
        } else {
            Double value = valueMap.get(name);
            if (value == null) {
                result = defaultValue;
            } else {
                result = value.doubleValue();
            }
        }
        return result;
    }

    protected <K, T> T getValue(K key, Map<K, T> valueMap, T defaultValue) {
        T result;
        if (key == null) {
            result = defaultValue;
        } else {
            T value = valueMap.get(key);
            if (value == null) {
                result = defaultValue;
            } else {
                result = value;
            }
        }
        return result;
    }

    protected boolean getNext(String name, Map<String, Boolean> valueMap, LinkedList<Boolean> valueList,
            boolean defaultValue) {
        boolean result = defaultValue;
        Boolean tmp = valueMap.get(name);
        if (tmp != null) {
            result = tmp.booleanValue();
        } else if (!valueList.isEmpty()) {
            result = valueList.pollLast();
            if (cyclingCustomMap) {
                valueList.addFirst(result);
            }
            valueMap.put(name, result);
        }
        return result;
    }

    protected int getNext(String name, Map<String, Integer> valueMap, LinkedList<Integer> valueList, int defaultValue) {
        int result = defaultValue;
        Integer tmp = valueMap.get(name);
        if (tmp != null) {
            result = tmp.intValue();
        } else if (!valueList.isEmpty()) {
            result = valueList.pollLast();
            if (cyclingCustomMap) {
                valueList.addFirst(result);
            }
            valueMap.put(name, result);
        }
        return result;
    }

    protected double getNext(String name, Map<String, Double> valueMap, LinkedList<Double> valueList,
            double defaultValue) {
        double result = defaultValue;
        Double tmp = valueMap.get(name);
        if (tmp != null) {
            result = tmp.doubleValue();
        } else if (!valueList.isEmpty()) {
            result = valueList.pollLast();
            if (cyclingCustomMap) {
                valueList.addFirst(result);
            }
            valueMap.put(name, result);
        }
        return result;
    }

    protected <K, T> T getNext(K key, Map<K, T> valueMap, LinkedList<T> valueList, T defaultValue) {
        T result = defaultValue;
        T tmp = valueMap.get(key);
        if (tmp != null) {
            result = tmp;
        } else if (!valueList.isEmpty()) {
            result = valueList.pollLast();
            if (cyclingCustomMap) {
                valueList.addFirst(result);
            }
            valueMap.put(key, result);
        }
        return result;
    }

    private Color getColorWithRotation(String name, Color defaultColor, Map<String, Color> colorMap,
            LinkedList<Color> colorList) {
        Color color = colorMap.get(name);
        if (color == null) {
            color = defaultColor;
            if (color == null) {
                if (colorList == null) {
                    color = colorRotationTool.getNextColor();
                } else {
                    synchronized (colorList) {
                        if (colorList.isEmpty()) {
                            color = colorRotationTool.getNextColor();
                        } else {
                            color = colorList.pollLast();
                            // set to the beginning of the list
                            if (cyclingCustomMap) {
                                colorList.addFirst(color);
                            }
                        }
                    }
                }
            }
            colorMap.put(name, color);
        }
        return color;
    }

    private Color getNextColor(String name, Color defaultColor) {
        return getColorWithRotation(name, defaultColor, dataViewColor, customColor);
    }

    private Color getNextMarkerColor(String name, Color defaultColor) {
        return getColorWithRotation(name, defaultColor, dataViewMarkerColor, customDataViewMarkerColor);
    }

    private Color getNextFillColor(String name, Color defaultColor) {
        return getColorWithRotation(name, defaultColor, dataViewFillColor, customDataViewFillColor);
    }

    private int getNextLineWidth(String name) {
        return getNext(name, dataViewLineWidth, customDataViewLineWidth, 1);
    }

    private int getNextBarWidth(String name) {
        return getNext(name, dataViewBarWidth, customDataViewBarWidth, 1);
    }

    private int getNextMarkerSize(String name) {
        return getNext(name, dataViewMarkerSize, customDataViewMarkerSize, 5);
    }

    private int getNextMarkerStyle(String name) {
        return getNext(name, dataViewMarker, customDataViewMarkerStyle, IChartViewer.MARKER_NONE);
    }

    private int getNextAxis(String name) {
        return getNext(name, dataViewAxis, customDataViewAxis, IChartViewer.Y1);
    }

    private int getNextLineStyle(String name) {
        return getNext(name, dataViewLineStyle, customDataViewLineStyle, IChartViewer.STYLE_SOLID);
    }

    private int getNextFillStyle(String name) {
        return getNext(name, dataViewFillStyle, customDataFillStyle, IChartViewer.FILL_STYLE_NONE);
    }

    private String getNextUserFormat(String name) {
        return getNext(name, dataViewUserFormat, customDataViewUserFormat, null);
    }

    private String getNextUnit(String name) {
        return getNext(name, dataViewUnit, customDataViewUnit, ObjectUtils.EMPTY_STRING);
    }

    private int getNextFillMethod(String name) {
        return getNext(name, dataViewFillMethod, customDataFillMethod, IChartViewer.METHOD_FILL_FROM_BOTTOM);
    }

    private int getNextDataViewStyle(String name) {
        return getNext(name, dataViewStyle, customDataViewStyle, IChartViewer.TYPE_LINE);
    }

    public void addDataViewToAxis(JLDataView dataview, int axis, boolean hideView) {
        if (dataview != null) {
            String dataViewName = dataview.getId();
            synchronized (hiddenDataView) {
                if (hideView) {
                    if (!hiddenDataView.containsKey(dataViewName)) {
                        hiddenDataView.put(dataViewName, dataview);
                    }
                } else if (hiddenDataView.containsKey(dataViewName)) {
                    hiddenDataView.remove(dataViewName);
                }
            }
            synchronized (visibleDataView) {
                if (hideView) {
                    visibleDataView.remove(dataViewName);
                } else if (!visibleDataView.containsKey(dataViewName)) {
                    visibleDataView.put(dataViewName, dataview);
                }
            }

            String name = dataview.getId();
            String displayName = dataViewDisplayName.get(name);
            if (displayName == null) {
                displayName = dataview.getDisplayName();
            }

            Color color = getNextColor(name, null);

            Color markerColor = getNextMarkerColor(name, color);

            Color fillColor = getNextFillColor(name, color);

            Integer lineWidth = getNextLineWidth(name);

            Integer barWidth = getNextBarWidth(name);

            Integer markerSize = getNextMarkerSize(name);

            Integer markerStyle = getNextMarkerStyle(name);

            Integer lineStyle = getNextLineStyle(name);

            Integer fillStyle = getNextFillStyle(name);

            Integer fillMethod = getNextFillMethod(name);

            Integer viewStyle = getNextDataViewStyle(name);

            String format = getNextUserFormat(name);
            String unit = getNextUnit(name);

            dataview.setDisplayName(displayName);
            dataViewDisplayName.put(name, displayName);
            dataview.setColor(color);
            dataViewColor.put(name, color);
            dataview.setMarkerColor(markerColor);
            dataViewMarkerColor.put(name, markerColor);
            dataview.setViewType(viewStyle.intValue());
            dataViewStyle.put(name, viewStyle);
            dataview.setFillColor(fillColor);
            dataViewFillColor.put(name, fillColor);
            dataview.setFillStyle(fillStyle.intValue());
            dataViewFillStyle.put(name, fillStyle);
            dataview.setFillMethod(fillMethod);
            dataViewFillMethod.put(name, fillMethod);
            dataview.setMarker(markerStyle.intValue());
            dataViewMarker.put(name, markerStyle);
            dataview.setMarkerSize(markerSize.intValue());
            dataViewMarkerSize.put(name, markerSize);
            dataview.setLineWidth(lineWidth.intValue());
            dataViewLineWidth.put(name, lineWidth);
            dataview.setBarWidth(barWidth.intValue());
            dataViewBarWidth.put(name, barWidth);
            dataview.setStyle(lineStyle.intValue());
            dataViewLineStyle.put(name, lineStyle);
            dataview.setFormat(format);
            dataViewUserFormat.put(name, format);
            dataview.setUnit(unit);
            dataViewUnit.put(name, unit);

            // no rotation for these parameters
            dataview.setA0(getDataViewTransformA0(name));
            dataview.setA1(getDataViewTransformA1(name));
            dataview.setA2(getDataViewTransformA2(name));
            dataview.setA0X(getDataViewXOffsetA0(name));
            dataview.setClickable(isDataViewClickable(name));
            dataview.setLabelVisible(isDataViewLabelVisible(name));
            dataview.setInterpolationMethod(getDataViewInterpolationMethod(name));
            dataview.setInterpolationStep(getDataViewInterpolationStep(name));
            dataview.setHermiteBias(getDataViewHermiteBias(name));
            dataview.setHermiteTension(getDataViewHermiteTension(name));
            dataview.setSmoothingMethod(getDataViewSmoothingMethod(name));
            dataview.setSmoothingNeighbors(getDataViewSmoothingNeighbors(name));
            dataview.setSmoothingGaussSigma(getDataViewSmoothingGaussSigma(name));
            dataview.setSmoothingExtrapolation(getDataViewSmoothingExtrapolation(name));
            dataview.setMathFunction(getDataViewMathFunction(name));
            dataview.setHighlightMethod(getDataViewHighlightMethod(name));
            dataview.setHighlightCoefficient(getDataViewHighlightCoefficient(name));
            dataview.setHighlighted(isDataViewHighlighted(name));

            if (!hideView) {
                JLAxis jlAxis = getAxis(axis);
                if (jlAxis != null) {
                    jlAxis.addDataView(dataview);
                }
            }
            initComboList();
        }
    }

    public void removeDataViewFromAxis(String name, int axis) {
        JLDataView dataview = getDataView(name, axis);
        if (dataview != null) {
            JLAxis jlAxis = getAxis(axis);
            if (jlAxis != null) {
                jlAxis.removeDataView(dataview);
            }
        }
    }

    public List<JLDataView> getDataView(int axis) {
        JLAxis jlaxis = getAxis(axis);

        if (jlaxis != null) {
            return jlaxis.getViewsCopy();
        }

        return null;
    }

    public void initComponent() {
        setAxisScale(IChartViewer.LINEAR_SCALE, IChartViewer.Y1);
        setAxisScale(IChartViewer.LINEAR_SCALE, IChartViewer.Y2);
        setAxisScale(IChartViewer.LINEAR_SCALE, IChartViewer.X);
        setAxisAutoScale(true, IChartViewer.Y1);
        setAxisAutoScale(true, IChartViewer.Y2);
        setAxisAutoScale(true, IChartViewer.X);
        setAxisAnnotation(JLAxis.VALUE_ANNO, IChartViewer.X);

        dataViewOption = new JLDataViewOption();
        dataViewOption.getCloseButton().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cleanDataViewEditor();
            }

        });
        dataViewOption.setChart(this);
        expressionParserOption = new ExpressionParserOption();
        expressionParserOption.getCloseButton().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                applyExpression();
            }

        });
        expressionParserOption.getCancelButton().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cleanExpressionEditor();
            }
        });
        chartOption = new JLChartOption();
        chartOption.setChart(this);
        chartOption.getCloseButton().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cleanChartEditor();
            }
        });
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        if (theTable != null) {
            theTable.setEditable(editable);
        }
    }

    public boolean isEditable() {
        if (theTable != null) {
            theTable.isEditable();
        }
        return editable;
    }

    @Override
    public void columnChanged(JTableRowEvent evt) {
        int columnIndex = evt.getColumnIndex();
        Object[] yPoint = evt.getColumnChanged();
        if (theTable != null) {
            Object[] xPoint = theTable.getColumnValues(0);
            String dataArrayName = theTable.getColumnName(columnIndex);
            if (xPoint.length == yPoint.length) {
                Map<String, Object> dataMap = new HashMap<String, Object>();
                double[][] value = new double[2][];
                double[] xValue = new double[xPoint.length];
                double[] yValue = new double[yPoint.length];
                for (int i = 0; i < yPoint.length; i++) {
                    xValue[i] = Double.parseDouble(xPoint[i].toString());
                    yValue[i] = Double.parseDouble(yPoint[i].toString());
                }
                value[IChartViewer.X_INDEX] = xValue;
                value[IChartViewer.Y_INDEX] = yValue;
                dataMap.put(dataArrayName, value);
                fireDataChanged(dataMap);
            }
        }
    }

    public void fireDataChanged(Map<String, Object> writeData) {
        if (writeData != null) {
            ChartViewerEvent event = new ChartViewerEvent(parent, null, -1, null, writeData, Reason.DATA);
            List<IChartViewerListener> copy = new ArrayList<IChartViewerListener>();
            synchronized (chartListeners) {
                copy.addAll(chartListeners);
            }
            for (IChartViewerListener listener : copy) {
                if (listener != null) {
                    listener.chartViewerChanged(event);
                }
            }
            copy.clear();
        }
    }

    public void fireConfigurationChanged(String id) {
        List<IChartViewerListener> copy = new ArrayList<IChartViewerListener>();
        ChartViewerEvent event = new ChartViewerEvent(parent, null, -1, id, null, Reason.CONFIGURATION);
        synchronized (chartListeners) {
            copy.addAll(chartListeners);
        }
        for (IChartViewerListener listener : copy) {
            if (listener != null) {
                listener.chartViewerChanged(event);
            }
        }
        copy.clear();
    }

    protected JLDataView recoverCleanedDataView(String id) {
        JLDataView dataView = getDataView(id);
        if (dataView == null) {
            dataView = new JLDataView(id);
            dataView.setXDataSorted(dataViewsSortedOnX);
            addDataViewToAxis(dataView, getNextAxis(id), autoHideViews);
        } else {
            dataView.reset();
        }
        return dataView;
    }

    public void addData(Map<String, Object> dataToAdd, boolean replaceData, boolean checkHiddenViews) {
        if (replaceData) {
            List<String> toRemove = new ArrayList<String>();
            if (getData() != null) {
                toRemove.addAll(getData().keySet());
                if (dataToAdd != null) {
                    toRemove.removeAll(dataToAdd.keySet());
                }
            }
            removeData(toRemove);
        }
        if (dataToAdd != null) {
            // Prepare a Map to update lastDatas. Use LinkedHashMap to keep
            // DataArray order
            Map<String, Object> dataMap;
            if (replaceData) {
                dataMap = new LinkedHashMap<String, Object>();
            } else {
                dataMap = copyLastDataToMap();
            }
            // update dataviews
            for (String id : dataToAdd.keySet()) {
                Object value = dataToAdd.get(id);
                JLDataView dataView = recoverCleanedDataView(id);
                ChartUtils.updateDataView(dataView, value);
                dataMap.put(id, value);
                if (isMathExpressionEnabled()) {
                    updateExpressions(dataView);
                }
            }
            // Update lastDatas
            setLastData(dataMap);
        }
        initComboList();
        measureAndPaint();
        refreshTableAll();
    }

    public void removeData(Collection<String> dataToRemove) {
        if ((dataToRemove != null) && (!dataToRemove.isEmpty())) {
            // Prepare a Map to update lastDatas. Use LinkedHashMap to keep
            // DataArray order
            Map<String, Object> dataMap = copyLastDataToMap();
            // Update dataviews
            for (String id : dataToRemove) {
                removeDataView(id, true);
                dataMap.remove(id);
            }
            // Update lastDatas
            setLastData(dataMap);
        }
    }

    public Map<String, Object> copyLastDataToMap() {
        Map<String, Object> toCopy = lastDatas;
        Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
        if (toCopy != null) {
            dataMap.putAll(toCopy);
        }
        return dataMap;
    }

    protected void setLastData(Map<String, Object> dataMap) {
        lastDatas = dataMap;
    }

    public Map<String, Object> getData() {
        return lastDatas;
    }

    public boolean isMathExpressionEnabled() {
        return mathExpressionEnabled;
    }

    public void setMathExpressionEnabled(boolean mathExpressionEnabled) {
        this.mathExpressionEnabled = mathExpressionEnabled;

        expressionSeparator.setVisible(mathExpressionEnabled);
        expressionMenuItem.setVisible(mathExpressionEnabled);
        expressionClearMenuItem.setVisible(mathExpressionEnabled);
    }

    protected void cleanExpressionEditor() {
        if (expressionEditor != null) {
            if (expressionEditor.isVisible()) {
                expressionEditor.setVisible(false);
                expressionEditor.dispose();
            }
            expressionEditor.setContentPane(new JPanel());
            expressionEditor = null;
        }
    }

    protected void applyExpression() {
        expressionParserOption.commit();
        cleanExpressionEditor();
        JLDataView expressionDataView = new JLDataView(expressionParserOption.dataViewId);
        String expression = expressionParserOption.getExpression();
        String[] variables = expressionParserOption.getVariables();
        if ((variables != null) && (variables.length > 0) && (!expression.isEmpty())) {
            applyExpressionToChart(expression, expressionDataView, expressionParserOption.getSelectedAxis(), variables,
                    expressionParserOption.isX());
        }
    }

    /**
     * Display the expression dialog.
     */
    public void showExpressionDialog() {
        cleanExpressionEditor();
        expressionParserOption.setAlwaysCommit(false);
        expressionParserOption.setChart(null);
        expressionEditor = new JDialog(CometeUtils.getWindowForComponent(this), expressionParserOption.getName());
        expressionEditor.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        expressionEditor.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cleanExpressionEditor();
            }
        });
        expressionEditor.setContentPane(expressionParserOption);
        expressionEditor.pack();
        JLDataView expressionDataView = new JLDataView();
        expressionParserOption.setDataViewId(expressionDataView.getId());
        expressionParserOption.setChart(this);
        CometeUtils.centerDialog(expressionEditor);
        expressionEditor.setVisible(true);
    }

    /**
     * Call this method to evaluate an expression and have the result
     * represented by a DataView you previously parametered
     * 
     * @param dataViewName
     *            the name of your new dataView.
     * 
     * @param expression
     *            The String representing your expression. It must not be null.
     *            Example: "cos(x1) + 2*sin(x2)/exp(x3)"
     * @param selectedAxis
     *            The axis on which you want to put your DataView. It can be <code>X_AXIS</code>, <code>Y1_AXIS</code>
     *            or <code>Y2_AXIS</code>
     * @param variables
     *            A String[] representing the dataview names associated with
     *            your variables in order of the variables index. Example : You
     *            have two variables x1 and x2 in your expression. x1 is
     *            associated with the JLDataView named "theCurve", and x2 with
     *            the JLDataView named "theBar". Then, variables must be
     *            {"theCurve", "theBar"}.
     * @param x
     *            A boolean to know whether your expression looks like "f(x)".
     *            If your expression looks like "f(x1,...,xn)" then set x to <code>false</code>. If it looks like
     *            "f(x)", set x to <code>true</code>.
     * @see #X_AXIS
     * @see #Y1_AXIS
     * @see #Y2_AXIS
     * @return The JLDataView used to draw the expression result (the one given
     *         in parameter if not null, the automatically created one
     *         otherwise). Returns <code>null</code> if you try to put an
     *         expression on x axis when it is not allowed
     */
    public JLDataView addExpressionToChart(JLDataView expressionDataView, String expression, int selectedAxis,
            String[] variables, boolean x) {

        if (selectedAxis == IChartViewer.X && !isCanPutExpressionOnX()) {
            return null;
        }

        JLDataView resultView = null;

        // --Registering Expression View--//
        resultView = applyExpression(expression, expressionDataView, variables, x);
        Object[] parameters = new Object[variables.length + 3];
        parameters[0] = new Integer(selectedAxis);
        parameters[1] = expression;
        parameters[2] = new Boolean(x);
        for (int i = 0; i < variables.length; i++) {
            parameters[i + 3] = variables[i];
        }

        resultView.setFixed(true);
        synchronized (expressionMap) {
            expressionMap.put(resultView, parameters);
        } // end synchronized(expressionMap)

        // --Adding Expression View to Axis--//
        addDataViewToAxis(resultView, selectedAxis, false);
        // --END Adding Expression View to Axis--//

        measureAndPaint();
        return resultView;

    }

    /**
     * Call this method to evaluate an expression and have the result
     * represented by a DataView you previously parametered
     * 
     * @param dataViewName
     *            the name of your new dataView.
     * 
     * @param expression
     *            The String representing your expression. It must not be null.
     *            Example: "cos(x1) + 2*sin(x2)/exp(x3)"
     * @param selectedAxis
     *            The axis on which you want to put your DataView. It can be <code>X_AXIS</code>, <code>Y1_AXIS</code>
     *            or <code>Y2_AXIS</code>
     * @param variables
     *            A String[] representing the dataview names associated with
     *            your variables in order of the variables index. Example : You
     *            have two variables x1 and x2 in your expression. x1 is
     *            associated with the JLDataView named "theCurve", and x2 with
     *            the JLDataView named "theBar". Then, variables must be
     *            {"theCurve", "theBar"}.
     * @param x
     *            A boolean to know whether your expression looks like "f(x)".
     *            If your expression looks like "f(x1,...,xn)" then set x to <code>false</code>. If it looks like
     *            "f(x)", set x to <code>true</code>.
     * @see #X_AXIS
     * @see #Y1_AXIS
     * @see #Y2_AXIS
     */
    public void addExpression(String dataViewName, String expression, int selectedAxis, String[] variables, boolean x) {
        addExpression(dataViewName, dataViewName, expression, selectedAxis, variables, x);
    }

    /**
     * Call this method to evaluate an expression and have the result
     * represented by a DataView you previously parametered
     * 
     * @param dataViewId
     *            the id of your new dataView
     * 
     * @param dataViewName
     *            the name of your new dataView.
     * 
     * @param expression
     *            The String representing your expression. It must not be null.
     *            Example: "cos(x1) + 2*sin(x2)/exp(x3)"
     * @param selectedAxis
     *            The axis on which you want to put your DataView. It can be <code>X_AXIS</code>, <code>Y1_AXIS</code>
     *            or <code>Y2_AXIS</code>
     * @param variables
     *            A String[] representing the dataview names associated with
     *            your variables in order of the variables index. Example : You
     *            have two variables x1 and x2 in your expression. x1 is
     *            associated with the JLDataView named "theCurve", and x2 with
     *            the JLDataView named "theBar". Then, variables must be
     *            {"theCurve", "theBar"}.
     * @param x
     *            A boolean to know whether your expression looks like "f(x)".
     *            If your expression looks like "f(x1,...,xn)" then set x to <code>false</code>. If it looks like
     *            "f(x)", set x to <code>true</code>.
     * @see #X_AXIS
     * @see #Y1_AXIS
     * @see #Y2_AXIS
     */
    public void addExpression(String dataViewId, String dataViewName, String expression, int selectedAxis,
            String[] variables, boolean x) {

        JLDataView expressionDataView = new JLDataView(dataViewId);
        expressionDataView.setDisplayName(dataViewName);

        addExpressionToChart(expressionDataView, expression, selectedAxis, variables, x);
    }

    /**
     * Call this method to evaluate an expression and have the result
     * represented by a DataView you previously parametered
     * 
     * @param expression
     *            The String representing your expression. It must not be null.
     *            Example: "cos(x1) + 2*sin(x2)/exp(x3)"
     * @param expressionDataView
     *            The JLDataView in which you want to put your expression
     *            evaluation result. It can be null. In this case, a JLDataView
     *            is automatically created.
     * @param selectedAxis
     *            The axis on which you want to put your DataView. It can be <code>X_AXIS</code>, <code>Y1_AXIS</code>
     *            or <code>Y2_AXIS</code>
     * @param variables
     *            A String[] representing the dataview names associated with
     *            your variables in order of the variables index. Example : You
     *            have two variables x1 and x2 in your expression. x1 is
     *            associated with the JLDataView named "theCurve", and x2 with
     *            the JLDataView named "theBar". Then, variables must be
     *            {"theCurve", "theBar"}.
     * @param x
     *            A boolean to know whether your expression looks like "f(x)".
     *            If your expression looks like "f(x1,...,xn)" then set x to <code>false</code>. If it looks like
     *            "f(x)", set x to <code>true</code>.
     * @see #X_AXIS
     * @see #Y1_AXIS
     * @see #Y2_AXIS
     * @return The JLDataView used to draw the expression result (the one given
     *         in parameter if not null, the automatically created one
     *         otherwise). Returns <code>null</code> if you try to put an
     *         expression on x axis when it is not allowed
     */
    public JLDataView applyExpressionToChart(String expression, JLDataView expressionDataView, int selectedAxis,
            String[] variables, boolean x) {

        Object[] parameters = getValue(expressionDataView, expressionMap, null);
        // --Cleaning when necessary--//
        if (parameters != null) {
            Integer axis = (Integer) parameters[0];
            removeDataViewFromAxis(expressionDataView.getId(), axis);
            synchronized (expressionMap) {
                expressionMap.remove(expressionDataView);
            }
            expressionDataView.reset();
        }
        // --Registering Expression View--//
        JLDataView resultView = addExpressionToChart(expressionDataView, expression, selectedAxis, variables, x);
        return resultView;
    }

    // Calculates the expression dataView.
    // Can be usefull to update an already existing expression dataview.
    // If you want to do so, you should clean your dataview first.
    protected JLDataView applyExpression(String expression, JLDataView expressionDataView, String[] variables,
            boolean x) {
        JLDataView resultView = expressionDataView;
        if (variables != null) {
            List<JLDataView> views = getViews();
            ExpressionParser parser = new ExpressionParser(variables.length, expression, getExpressionParser());
            parser.setX(x);
            parser.setPrecision(getTimePrecision());
            for (int i = 0; i < variables.length; i++) {
                for (int v = 0; v < views.size(); v++) {
                    if (variables[i].equals(views.get(v).getId())) {
                        parser.add(i, views.get(v));
                        break;
                    }
                }
            }
            views.clear();
            resultView = parser.buildDataView(expressionDataView, JLDataView.class);
            parser.clean();
        }
        return resultView;
    }

    public synchronized void clearExpressions() {
        Set<JLDataView> keySet = expressionMap.keySet();
        Iterator<JLDataView> keyIterator = keySet.iterator();
        while (keyIterator.hasNext()) {
            JLDataView data = keyIterator.next();
            Object[] parameters = expressionMap.get(data);
            Integer axis = (Integer) parameters[0];
            switch (axis.intValue()) {
                case IChartViewer.X:
                    getXAxis().removeDataView(data);
                    break;
                case IChartViewer.Y1:
                    getY1Axis().removeDataView(data);
                    break;
                case IChartViewer.Y2:
                    getY2Axis().removeDataView(data);
                    break;
            }
        }
        expressionMap.clear();
        measureAndPaint();
    }

    // Tries to update all the expression JLDataViews associated with the
    // JLDataView given in parameter
    protected synchronized void updateExpressions(JLDataView view) {
        List<JLDataView> expressions = new ArrayList<JLDataView>();
        expressions.addAll(expressionMap.keySet());
        for (int i = 0; i < expressions.size(); i++) {
            JLDataView expressionView = expressions.get(i);
            Object[] parameters = expressionMap.get(expressionView);
            if (parameters != null) {
                for (int j = 3; j < parameters.length; j++) {
                    if (parameters[j].equals(view.getId())) {
                        String expression = (String) parameters[1];
                        Boolean x = (Boolean) parameters[2];
                        String[] variables = new String[parameters.length - 3];
                        for (int k = 0; k < variables.length; k++) {
                            variables[k] = (String) parameters[k + 3];
                        }
                        // Commit Change
                        expressionView.reset();
                        applyExpression(expression, expressionView, variables, x.booleanValue());
                        updateExpressions(expressionView);
                        break;
                    }
                }
            }
        }
    }

    /**
     * @return the canSetExpressionOnX
     */
    public boolean isCanPutExpressionOnX() {
        return canPutExpressionOnX;
    }

    /**
     * @param canSetExpressionOnX
     *            the canSetExpressionOnX to set
     */
    public void setCanPutExpressionOnX(boolean canSetExpressionOnX) {
        canPutExpressionOnX = canSetExpressionOnX;
    }

    /**
     * @return the useXViewsWithExpressions
     */
    public boolean isUseXViewsWithExpressions() {
        return useXViewsWithExpressions;
    }

    /**
     * @param useXViewsWithExpressions
     *            the useXViewsWithExpressions to set
     */
    public void setUseXViewsWithExpressions(boolean useXViewsWithExpressions) {
        this.useXViewsWithExpressions = useXViewsWithExpressions;
    }

    public boolean isLimitDrawingZone() {
        return limitDrawingZone;
    }

    public void setLimitDrawingZone(boolean limitDrawingZone) {
        this.limitDrawingZone = limitDrawingZone;
    }

    public int getScale(int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.getScale();
        }
        return IChartViewer.LINEAR_SCALE;
    }

    public int getDataViewFillStyle(String name) {
        return getValue(name, dataViewFillStyle, IChartViewer.FILL_STYLE_NONE);
    }

    public int getDataViewFillMethod(String name) {
        return getValue(name, dataViewFillMethod, IChartViewer.METHOD_FILL_FROM_BOTTOM);
    }

    public int getDataViewLineStyle(String name) {
        return getValue(name, dataViewLineStyle, IChartViewer.STYLE_SOLID);
    }

    public int getDataViewLineWidth(String name) {
        return getValue(name, dataViewLineWidth, 1);
    }

    public int getDataViewBarWidth(String name) {
        return getValue(name, dataViewBarWidth, 10);
    }

    public Color getDataViewColor(String name) {
        return getValue(name, dataViewColor, null);
    }

    public Color getDataViewFillColor(String name) {
        return getValue(name, dataViewFillColor, null);
    }

    public int getDataViewMarkerStyle(String name) {
        return getValue(name, dataViewMarker, IChartViewer.MARKER_NONE);
    }

    public Color getDataViewMarkerColor(String name) {
        return getValue(name, dataViewMarkerColor, null);
    }

    public int getDataViewMarkerSize(String name) {
        return getValue(name, dataViewMarkerSize, 6);
    }

    public int getDataViewAxis(String name) {
        return getValue(name, dataViewAxis, IChartViewer.Y1);
    }

    public String getDataViewFormat(String name) {
        return getValue(name, dataViewUserFormat, null);
    }

    public String getDataViewUnit(String name) {
        return getValue(name, dataViewUnit, ObjectUtils.EMPTY_STRING);
    }

    public int getDataViewStyle(String name) {
        return getValue(name, dataViewStyle, IChartViewer.TYPE_LINE);
    }

    public void setDataViewTransformA0(String name, double transform) {
        setDataViewTransformA0(name, transform, true);
    }

    protected void setDataViewTransformA0(String name, double transform, boolean fireEvent) {
        if (storeData(name, transform, dataViewTransformA0)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setA0(transform);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewTransformA1(String name, double transform) {
        setDataViewTransformA1(name, transform, true);
    }

    protected void setDataViewTransformA1(String name, double transform, boolean fireEvent) {
        if (storeData(name, transform, dataViewTransformA1)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setA1(transform);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewTransformA2(String name, double transform) {
        setDataViewTransformA2(name, transform, true);
    }

    protected void setDataViewTransformA2(String name, double transform, boolean fireEvent) {
        if (storeData(name, transform, dataViewTransformA2)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setA2(transform);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public void setDataViewXOffsetA0(String name, double transform) {
        setDataViewXOffsetA0(name, transform, true);
    }

    protected void setDataViewXOffsetA0(String name, double offset, boolean fireEvent) {
        if (storeData(name, offset, dataViewOffsetXA0)) {
            JLDataView dataView = getDataView(name);
            if (dataView != null) {
                dataView.setA0X(offset);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public double getDataViewTransformA0(String name) {
        return getValue(name, dataViewTransformA0, 0);
    }

    public double getDataViewTransformA1(String name) {
        return getValue(name, dataViewTransformA1, 1);
    }

    public double getDataViewTransformA2(String name) {
        return getValue(name, dataViewTransformA2, 0);
    }

    public double getDataViewXOffsetA0(String name) {
        return getValue(name, dataViewOffsetXA0, 0);
    }

    public String getDataViewDisplayName(String name) {
        JLDataView dataView = getDataView(name);
        if (dataView != null) {
            return dataView.getDisplayName();
        }
        return null;
    }

    public boolean isDataViewClickable(String name) {
        return getValue(name, dataViewClickable, true);
    }

    public boolean isDataViewLabelVisible(String name) {
        return getValue(name, dataViewLabelVisible, true);
    }

    public boolean isDataViewSamplingAllowed(String name) {
        return getValue(name, dataViewSamplingAllowed, true);
    }

    public int getDataViewInterpolationMethod(String name) {
        return getValue(name, dataViewInterpolationMethod, 0);
    }

    public void setDataViewInterpolationMethod(String name, int method) {
        setDataViewInterpolationMethod(name, method, true);
    }

    protected void setDataViewInterpolationMethod(String name, int method, boolean fireEvent) {
        if (storeData(name, method, dataViewInterpolationMethod)) {
            JLDataView view = getDataView(name);
            if (view != null) {
                view.setInterpolationMethod(method);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public int getDataViewInterpolationStep(String name) {
        return getValue(name, dataViewInterpolationStep, 10);
    }

    public void setDataViewInterpolationStep(String name, int step) {
        setDataViewInterpolationStep(name, step, true);
    }

    protected void setDataViewInterpolationStep(String name, int step, boolean fireEvent) {
        if (storeData(name, step, dataViewInterpolationStep)) {
            JLDataView view = getDataView(name);
            if (view != null) {
                view.setInterpolationStep(step);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public double getDataViewHermiteBias(String name) {
        return getValue(name, dataViewHermiteBias, 0);
    }

    public void setDataViewHermiteBias(String name, double bias) {
        setDataViewHermiteBias(name, bias, true);
    }

    protected void setDataViewHermiteBias(String name, double bias, boolean fireEvent) {
        if (storeData(name, bias, dataViewHermiteBias)) {
            JLDataView view = getDataView(name);
            if (view != null) {
                view.setHermiteBias(bias);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public double getDataViewHermiteTension(String name) {
        return getValue(name, dataViewHermiteTension, 0);
    }

    public void setDataViewHermiteTension(String name, double tension) {
        setDataViewHermiteTension(name, tension, true);
    }

    protected void setDataViewHermiteTension(String name, double tension, boolean fireEvent) {
        if (storeData(name, tension, dataViewHermiteTension)) {
            dataViewHermiteTension.put(name, tension);
            JLDataView view = getDataView(name);
            if (view != null) {
                view.setHermiteTension(tension);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public int getDataViewSmoothingMethod(String name) {
        return getValue(name, dataViewSmoothingMethod, 0);
    }

    public void setDataViewSmoothingMethod(String name, int method) {
        setDataViewSmoothingMethod(name, method, true);
    }

    protected void setDataViewSmoothingMethod(String name, int method, boolean fireEvent) {
        if (storeData(name, method, dataViewSmoothingMethod)) {
            JLDataView view = getDataView(name);
            if (view != null) {
                view.setSmoothingMethod(method);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public int getDataViewSmoothingNeighbors(String name) {
        return getValue(name, dataViewSmoothingNeighbors, 3);
    }

    public void setDataViewSmoothingNeighbors(String name, int neighbors) {
        setDataViewSmoothingNeighbors(name, neighbors, true);
    }

    protected void setDataViewSmoothingNeighbors(String name, int neighbors, boolean fireEvent) {
        if (storeData(name, neighbors, dataViewSmoothingNeighbors)) {
            dataViewSmoothingNeighbors.put(name, neighbors);
            JLDataView view = getDataView(name);
            if (view != null) {
                view.setSmoothingNeighbors(neighbors);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public double getDataViewSmoothingGaussSigma(String name) {
        double result;
        Double sigma = dataViewSmoothingGaussSigma.get(name);
        if (sigma == null) {
            result = 0.5;
        } else {
            result = sigma.doubleValue();
        }
        return result;
    }

    public void setDataViewSmoothingGaussSigma(String name, double sigma) {
        setDataViewSmoothingGaussSigma(name, sigma, true);
    }

    protected void setDataViewSmoothingGaussSigma(String name, double sigma, boolean fireEvent) {
        if (storeData(name, sigma, dataViewSmoothingGaussSigma)) {
            JLDataView view = getDataView(name);
            if (view != null) {
                view.setSmoothingGaussSigma(sigma);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public int getDataViewSmoothingExtrapolation(String name) {
        return getValue(name, dataViewSmoothingExtrapolation, 0);
    }

    public void setDataViewSmoothingExtrapolation(String name, int extrapolation) {
        setDataViewSmoothingExtrapolation(name, extrapolation, true);
    }

    protected void setDataViewSmoothingExtrapolation(String name, int extrapolation, boolean fireEvent) {
        if (storeData(name, extrapolation, dataViewSmoothingExtrapolation)) {
            JLDataView view = getDataView(name);
            if (view != null) {
                view.setSmoothingExtrapolation(extrapolation);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public int getDataViewMathFunction(String name) {
        return getValue(name, dataViewMathFunction, 0);
    }

    public void setDataViewMathFunction(String name, int function) {
        setDataViewMathFunction(name, function, true);
    }

    protected void setDataViewMathFunction(String name, int function, boolean fireEvent) {
        if (storeData(name, function, dataViewMathFunction)) {
            JLDataView view = getDataView(name);
            if (view != null) {
                view.setMathFunction(function);
            }
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public int getDataViewHighlightMethod(String name) {
        return getValue(name, dataViewHighlightMethod, IChartViewer.HIGHLIGHT_MULTIPLY);
    }

    public void setDataViewHighlightMethod(String name, int method) {
        dataViewHighlightMethod.put(name, method);
        JLDataView view = getDataView(name);
        if (view != null) {
            view.setHighlightMethod(method);
        }
    }

    public double getDataViewHighlightCoefficient(String name) {
        return getValue(name, dataViewHighlightCoefficient, IChartConst.DEFAULT_HIGHLIGHT_COEFF);
    }

    public void setDataViewHighlightCoefficient(String name, double coef) {
        dataViewHighlightCoefficient.put(name, coef);
        JLDataView view = getDataView(name);
        if (view != null) {
            view.setHighlightCoefficient(coef);
        }
    }

    public boolean isDataViewHighlighted(String name) {
        return getValue(name, dataViewHighlighted, false);
    }

    public void setDataViewHighlighted(String name, boolean highlighted) {
        dataViewHighlighted.put(name, highlighted);
        JLDataView view = getDataView(name);
        if (view != null) {
            view.setHighlighted(highlighted);
        }
    }

    public boolean isAutoHighlightOnLegend() {
        return autoHighlightOnLegend;
    }

    public void setAutoHighlightOnLegend(boolean autoHighlightOnLegend) {
        this.autoHighlightOnLegend = autoHighlightOnLegend;
    }

    public boolean isDataViewRemovingEnabled() {
        return dataViewRemovingEnabled;
    }

    public void setDataViewRemovingEnabled(boolean curveRemovingEnabled) {
        dataViewRemovingEnabled = curveRemovingEnabled;
    }

    public boolean isCleanDataViewConfigurationOnRemoving() {
        return cleanDataViewConfigurationOnRemoving;
    }

    public void setCleanDataViewConfigurationOnRemoving(boolean cleanCurveConfigurationOnRemoving) {
        cleanDataViewConfigurationOnRemoving = cleanCurveConfigurationOnRemoving;
    }

    public boolean hasDataViewProperties(String name) {
        if (name == null) {
            return false;
        } else {
            return
            // general
            dataViewStyle.containsKey(name) || dataViewAxis.containsKey(name) ||
            // bar
                    dataViewBarWidth.containsKey(name) || dataViewFillColor.containsKey(name)
                    || dataViewFillMethod.containsKey(name) || dataViewFillStyle.containsKey(name) ||
                    // curve
                    dataViewLineWidth.containsKey(name) || dataViewLineStyle.containsKey(name)
                    || dataViewColor.containsKey(name) || dataViewDisplayName.containsKey(name) ||
                    // marker
                    dataViewMarkerColor.containsKey(name) || dataViewMarkerSize.containsKey(name)
                    || dataViewMarker.containsKey(name) || dataViewLabelVisible.containsKey(name)
                    || dataViewSamplingAllowed.containsKey(name) ||
                    // transformation
                    dataViewTransformA0.containsKey(name) || dataViewTransformA1.containsKey(name)
                    || dataViewTransformA2.containsKey(name) ||
                    // interpolation
                    dataViewInterpolationMethod.containsKey(name) || dataViewInterpolationStep.containsKey(name)
                    || dataViewHermiteBias.containsKey(name) || dataViewHermiteTension.containsKey(name) ||
                    // smoothing
                    dataViewSmoothingMethod.containsKey(name) || dataViewSmoothingNeighbors.containsKey(name)
                    || dataViewSmoothingGaussSigma.containsKey(name) || dataViewSmoothingExtrapolation.containsKey(name)
                    ||
                    // math
                    dataViewMathFunction.containsKey(name);
        }
    }

    public void cleanDataViewProperties(String name) {
        if (name != null) {
            // general
            dataViewStyle.remove(name);
            dataViewAxis.remove(name);
            // bar
            dataViewBarWidth.remove(name);
            dataViewFillColor.remove(name);
            dataViewFillMethod.remove(name);
            dataViewFillStyle.remove(name);
            // curve
            dataViewLineWidth.remove(name);
            dataViewLineStyle.remove(name);
            dataViewColor.remove(name);
            dataViewDisplayName.remove(name);
            // marker
            dataViewMarkerColor.remove(name);
            dataViewMarkerSize.remove(name);
            dataViewMarker.remove(name);
            dataViewLabelVisible.remove(name);
            dataViewSamplingAllowed.remove(name);
            // transformation
            dataViewTransformA0.remove(name);
            dataViewTransformA1.remove(name);
            dataViewTransformA2.remove(name);
            // offset
            dataViewOffsetXA0.remove(name);
            // interpolation
            dataViewInterpolationMethod.remove(name);
            dataViewInterpolationStep.remove(name);
            dataViewHermiteBias.remove(name);
            dataViewHermiteTension.remove(name);
            // smoothing
            dataViewSmoothingMethod.remove(name);
            dataViewSmoothingNeighbors.remove(name);
            dataViewSmoothingGaussSigma.remove(name);
            dataViewSmoothingExtrapolation.remove(name);
            // math
            dataViewMathFunction.remove(name);
        }
    }

    public PlotProperties getDataViewPlotProperties(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        } else {
            return new PlotProperties(getDataViewStyle(name), getDataViewAxis(name), getDataViewFormat(name),
                    getDataViewUnit(name), getDataViewBarProperties(name), getDataViewCurveProperties(name),
                    getDataViewMarkerProperties(name), null, getDataViewTransformationProperties(name),
                    getDataViewXOffsetProperties(name), getDataViewInterpolationProperties(name),
                    getDataViewSmoothingProperties(name), getDataViewMathProperties(name));
        }
    }

    public void setDataViewPlotProperties(String name, PlotProperties properties) {
        if ((name != null) && (!name.isEmpty()) && (properties != null)) {
            setDataViewStyle(name, properties.getViewType(), false);
            setDataViewAxis(name, properties.getAxisChoice(), false);
            setDataViewBarProperties(name, properties.getBar(), false);
            setDataViewCurveProperties(name, properties.getCurve(), false);
            setDataViewMarkerProperties(name, properties.getMarker(), false);
            setDataViewTransformationProperties(name, properties.getTransform(), false);
            setDataViewXOffsetProperties(name, properties.getXOffset(), false);
            setDataViewInterpolationProperties(name, properties.getInterpolation(), false);
            setDataViewSmoothingProperties(name, properties.getSmoothing(), false);
            setDataViewMathProperties(name, properties.getMath(), false);
            fireConfigurationChanged(name);
        }
    }

    public BarProperties getDataViewBarProperties(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        } else {
            return new BarProperties(ColorTool.getCometeColor(getDataViewFillColor(name)), getDataViewBarWidth(name),
                    getDataViewFillStyle(name), getDataViewFillMethod(name));
        }
    }

    public void setDataViewBarProperties(String name, BarProperties properties) {
        setDataViewBarProperties(name, properties, true);
    }

    protected void setDataViewBarProperties(String name, BarProperties properties, boolean fireEvent) {
        if ((name != null) && (!name.isEmpty()) && (properties != null)) {
            setDataViewBarWidth(name, properties.getWidth(), false);
            setDataViewFillColor(name, ColorTool.getColor(properties.getFillColor()), false);
            setDataViewFillMethod(name, properties.getFillingMethod(), false);
            setDataViewFillStyle(name, properties.getFillStyle(), false);
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public CurveProperties getDataViewCurveProperties(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        } else {
            return new CurveProperties(ColorTool.getCometeColor(getDataViewColor(name)), getDataViewLineWidth(name),
                    getDataViewLineStyle(name), getDataViewDisplayName(name));
        }
    }

    public void setDataViewCurveProperties(String name, CurveProperties properties) {
        setDataViewCurveProperties(name, properties, true);
    }

    protected void setDataViewCurveProperties(String name, CurveProperties properties, boolean fireEvent) {
        if ((name != null) && (!name.isEmpty()) && (properties != null)) {
            setDataViewLineWidth(name, properties.getWidth(), false);
            setDataViewLineStyle(name, properties.getLineStyle(), false);
            setDataViewColor(name, ColorTool.getColor(properties.getColor()), false);
            setDataViewDisplayName(name, properties.getName(), false);
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public MarkerProperties getDataViewMarkerProperties(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        } else {

            return new MarkerProperties(ColorTool.getCometeColor(getDataViewMarkerColor(name)),
                    getDataViewMarkerSize(name), getDataViewMarkerStyle(name), isDataViewLabelVisible(name));
        }
    }

    public void setDataViewMarkerProperties(String name, MarkerProperties properties) {
        setDataViewMarkerProperties(name, properties, true);
    }

    protected void setDataViewMarkerProperties(String name, MarkerProperties properties, boolean fireEvent) {
        if ((name != null) && (!name.isEmpty()) && (properties != null)) {
            setDataViewMarkerColor(name, ColorTool.getColor(properties.getColor()), false);
            setDataViewMarkerSize(name, properties.getSize(), false);
            setDataViewMarkerStyle(name, properties.getStyle(), false);
            setDataViewLabelVisible(name, properties.isLegendVisible(), false);
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public TransformationProperties getDataViewTransformationProperties(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        } else {
            return new TransformationProperties(getDataViewTransformA0(name), getDataViewTransformA1(name),
                    getDataViewTransformA2(name));
        }
    }

    public void setDataViewTransformationProperties(String name, TransformationProperties properties) {
        setDataViewTransformationProperties(name, properties, true);
    }

    protected void setDataViewTransformationProperties(String name, TransformationProperties properties,
            boolean fireEvent) {
        if ((name != null) && (!name.isEmpty()) && (properties != null)) {
            setDataViewTransformA0(name, properties.getA0(), false);
            setDataViewTransformA1(name, properties.getA1(), false);
            setDataViewTransformA2(name, properties.getA2(), false);
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public OffsetProperties getDataViewXOffsetProperties(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        } else {
            return new OffsetProperties(getDataViewXOffsetA0(name));
        }
    }

    public void setDataViewXOffsetProperties(String name, OffsetProperties properties) {
        setDataViewXOffsetProperties(name, properties, true);
    }

    protected void setDataViewXOffsetProperties(String name, OffsetProperties properties, boolean fireEvent) {
        if ((name != null) && (!name.isEmpty()) && (properties != null)) {
            setDataViewXOffsetA0(name, properties.getA0(), false);
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public InterpolationProperties getDataViewInterpolationProperties(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        } else {
            return new InterpolationProperties(getDataViewInterpolationMethod(name), getDataViewInterpolationStep(name),
                    getDataViewHermiteBias(name), getDataViewHermiteTension(name));
        }
    }

    public void setDataViewInterpolationProperties(String name, InterpolationProperties properties) {
        setDataViewInterpolationProperties(name, properties, true);
    }

    protected void setDataViewInterpolationProperties(String name, InterpolationProperties properties,
            boolean fireEvent) {
        if ((name != null) && (!name.isEmpty()) && (properties != null)) {
            setDataViewInterpolationMethod(name, properties.getInterpolationMethod(), false);
            setDataViewInterpolationStep(name, properties.getInterpolationStep(), false);
            setDataViewHermiteBias(name, properties.getHermiteBias(), false);
            setDataViewHermiteTension(name, properties.getHermiteTension(), false);
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public SmoothingProperties getDataViewSmoothingProperties(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        } else {
            return new SmoothingProperties(getDataViewSmoothingMethod(name), getDataViewSmoothingNeighbors(name),
                    getDataViewSmoothingGaussSigma(name), getDataViewSmoothingExtrapolation(name));
        }
    }

    public void setDataViewSmoothingProperties(String name, SmoothingProperties properties) {
        setDataViewSmoothingProperties(name, properties, true);
    }

    protected void setDataViewSmoothingProperties(String name, SmoothingProperties properties, boolean fireEvent) {
        if ((name != null) && (!name.isEmpty()) && (properties != null)) {
            setDataViewSmoothingMethod(name, properties.getMethod(), false);
            setDataViewSmoothingNeighbors(name, properties.getNeighbors(), false);
            setDataViewSmoothingGaussSigma(name, properties.getGaussSigma(), false);
            setDataViewSmoothingExtrapolation(name, properties.getExtrapolation(), false);
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public MathProperties getDataViewMathProperties(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        } else {
            return new MathProperties(getDataViewMathFunction(name));
        }
    }

    public void setDataViewMathProperties(String name, MathProperties properties) {
        setDataViewMathProperties(name, properties, true);
    }

    protected void setDataViewMathProperties(String name, MathProperties properties, boolean fireEvent) {
        if ((name != null) && (!name.isEmpty()) && (properties != null)) {
            setDataViewMathFunction(name, properties.getFunction(), false);
            if (fireEvent) {
                fireConfigurationChanged(name);
            }
        }
    }

    public ChartProperties getChartProperties() {
        return new ChartProperties(isLabelVisible(), getLabelPlacement(),
                ColorTool.getCometeColor(getChartBackground()), FontTool.getCometeFont(getHeaderFont()),
                FontTool.getCometeFont(getLabelFont()), getHeader(), getDisplayDuration(), getTimePrecision(),
                getNoValueString(), isAutoHighlightOnLegend(), isLimitDrawingZone(), true,
                getAxisProperties(IChartViewer.Y1), getAxisProperties(IChartViewer.Y2),
                getAxisProperties(IChartViewer.X), null, null);
    }

    public void setChartProperties(ChartProperties properties) {
        if (properties != null) {
            if (isZoomed()) {
                exitZoom();
            }
            setLabelVisible(properties.isLegendVisible(), false);
            setLabelPlacement(properties.getLegendPlacement(), false);
            setChartBackground(ColorTool.getColor(properties.getBackgroundColor()), false);
            setBackground(ColorTool.getColor(properties.getGlobalBackgroundColor()));
            setHeaderFont(FontTool.getFont(properties.getHeaderFont()), false);
            setLabelFont(FontTool.getFont(properties.getLabelFont()), false);
            setHeader(properties.getTitle(), false);
            setDisplayDuration(properties.getDisplayDuration(), false);
            setTimePrecision(properties.getTimePrecision(), false);
            setNoValueString(properties.getNoValueString(), false);
            setAxisProperties(properties.getXAxisProperties(), IChartViewer.X, false);
            setAxisProperties(properties.getY1AxisProperties(), IChartViewer.Y1, false);
            setAxisProperties(properties.getY2AxisProperties(), IChartViewer.Y2, false);
            setAutoHighlightOnLegend(properties.isAutoHighlightOnLegend());
            measureAndPaint();
            fireConfigurationChanged(null);
        }
    }

    public AxisProperties getAxisProperties(int axisChoice) {
        JLAxis axis = getAxis(axisChoice);
        AxisProperties properties;
        if (axis == null) {
            properties = null;
        } else {
            properties = new AxisProperties(axis.getMinimum(), axis.getMaximum(), axis.getScale(), axis.isAutoScale(),
                    axis.getLabelFormat(), axis.getTitle(), ColorTool.getCometeColor(axis.getAxisColor()),
                    axis.getGridStyle(), axis.isGridVisible(), axis.isSubGridVisible(), axis.isVisible(),
                    axis.isDrawOpposite(), axis.getPosition(), axis.getUserLabelInterval(), IComponent.CENTER);
        }
        return properties;
    }

    public void setAxisProperties(AxisProperties properties, int axisChoice) {
        setAxisProperties(properties, axisChoice, true);
    }

    protected void setAxisProperties(AxisProperties properties, int axisChoice, boolean fireChartEvent) {
        JLAxis axis = getAxis(axisChoice);
        if (axis != null) {
            if (isZoomed()) {
                exitZoom();
            }
            double min = properties.getScaleMin();
            double max = properties.getScaleMax();
            if (min > max) {
                double temp = min;
                min = max;
                max = temp;
            }
            axis.setAutoScale(properties.isAutoScale());
            axis.setMinimum(min);
            axis.setMaximum(max);
            axis.setUserLabelInterval(properties.getUserLabelInterval());
            properties.setUserLabelInterval(axis.getUserLabelInterval());
            axis.setScale(properties.getScaleMode());
            axis.setLabelFormat(properties.getLabelFormat());
            axis.setTitle(properties.getTitle());
            axis.setAxisColor(ColorTool.getColor(properties.getColor()));
            axis.setGridStyle(properties.getGridStyle());
            axis.setGridVisible(properties.isGridVisible());
            axis.setSubGridVisible(properties.isSubGridVisible());
            axis.setVisible(properties.isVisible());
            axis.setDrawOpposite(properties.isDrawOpposite());
            axis.setPosition(properties.getPosition());
            axis.setTitleAlignment(properties.getTitleAlignment());
            if (fireChartEvent) {
                measureAndPaint();
                fireConfigurationChanged(null);
            }
        }
    }

    public int getAxisAnnotation(int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.getAnnotation();
        }
        return JLAxis.VALUE_ANNO;
    }

    public boolean isAxisAutoScale(int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.isAutoScale();
        }
        return false;
    }

    public Color getAxisColor(int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.getAxisColor();
        }
        return null;
    }

    public String getAxisDateFormat(int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.getDateFormat();
        }
        return null;
    }

    public int getAxisGridStyle(int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.getGridStyle();
        }
        return IChartViewer.STYLE_DASH;
    }

    public boolean isAxisGridVisible(int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.isGridVisible();
        }
        return false;
    }

    public boolean isAxisSubGridVisible(int axis) {
        JLAxis jlaxis = getAxis(axis);
        if (jlaxis != null) {
            return jlaxis.isSubGridVisible();
        }
        return false;
    }

    public void setAxisScaleEditionEnabled(boolean enabled, int axis) {
        if (chartOption != null) {
            chartOption.setScalePanelEnabled(enabled, axis);
        }
    }

    public JComponent generateLegend() {
        return new JLChartLegend(this);
    }

    public String getFileDirectoryPath() {
        return (transferEventDelegate == null ? ChartUtils.DOT : transferEventDelegate.getDataDirectory());
    }

    public void setFileDirectoryPath(String path) {
        if ((transferEventDelegate != null) && (path != null)) {
            transferEventDelegate.setDataDirectory(path);
        }
    }

    /**
     * Returns whether {@link JLDataView}s should be considered as sorted on X
     * 
     * @return A <code>boolean</code> value;
     */
    public boolean isDataViewsSortedOnX() {
        return dataViewsSortedOnX;
    }

    /**
     * Sets whether {@link JLDataView}s should be considered as sorted on X
     * 
     * @param dataViewsSortedOnX
     *            whether {@link JLDataView}s should be considered as sorted on
     *            X
     * @param applyOnExistingViews
     *            Whether to apply this rule on already existing views
     */
    public void setDataViewsSortedOnX(boolean dataViewsSortedOnX, boolean applyOnExistingViews) {
        this.dataViewsSortedOnX = dataViewsSortedOnX;
        if (applyOnExistingViews) {
            for (JLDataView dataView : getViews()) {
                dataView.setXDataSorted(dataViewsSortedOnX);
            }
            measureAndPaint();
        }
    }

    public void setFreezeItemsVisible(boolean visible) {
        makeSnapshotMenuItem.setVisible(visible);
        clearSnapshotMenuItem.setVisible(visible);
    }

    // //// //
    // Main //
    // //// //

    public static void main(String args[]) {

        final JFrame f = new JFrame();
        final JPanel mainPanel = new JPanel(new BorderLayout());
        final JLChart chart = new JLChart(null);
        f.setContentPane(mainPanel);

        JLDataView dataview = new JLDataView("2x\u00B2+x+1");
        for (int i = 0; i < 750; i++) {
            double t = i / 25.0;
            dataview.add(t, Math.pow(t, 2) + t + 1);
        }
        dataview.setFixed(true);
        chart.addDataViewToAxis(dataview, IChartViewer.Y1, false);
        final JComponent legend = chart.generateLegend();
        final JScrollPane scroll = new JScrollPane(legend);
        chart.addJLChartActionListener(new IJLChartActionListener() {

            private static final long serialVersionUID = 8450918095813542063L;

            @Override
            public boolean getActionState(JLChartActionEvent evt) {
                return true;
            }

            @Override
            public void actionPerformed(JLChartActionEvent evt) {
                if (JLChart.CHANGE_LABEL_PLACEMENT.equals(evt.getName())
                        || JLChart.CHANGE_LABEL_VISIBILITY.equals(evt.getName())) {
                    mainPanel.remove(legend);
                    if (chart.isLabelVisible()) {
                        switch (chart.getLabelPlacement()) {
                            case IChartViewer.LABEL_UP:
                                mainPanel.add(legend, BorderLayout.NORTH);
                                break;
                            case IChartViewer.LABEL_LEFT:
                                mainPanel.add(legend, BorderLayout.WEST);
                                break;
                            case IChartViewer.LABEL_RIGHT:
                                mainPanel.add(legend, BorderLayout.EAST);
                                break;
                            case IChartViewer.LABEL_DOWN:
                            case IChartViewer.LABEL_ROW:
                                mainPanel.add(legend, BorderLayout.SOUTH);
                                break;
                        }
                        legend.revalidate();
                        scroll.revalidate();
                    }
                    mainPanel.revalidate();
                    mainPanel.repaint();
                }
            }
        });
        scroll.setBorder(null);
        mainPanel.add(chart, BorderLayout.CENTER);
        mainPanel.add(scroll, BorderLayout.SOUTH);

        f.setContentPane(mainPanel);
        f.setSize(400, 300);
        legend.revalidate();
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

}
