/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.data;

import java.util.List;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.util.CfFileReader;
import fr.soleil.comete.swing.util.OFormat;
import fr.soleil.lib.project.ObjectUtils;

/**
 * An {@link AbstractDataView} that stores its axis as an <code>int</code>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DataView extends AbstractDataView {

    private int axis;

    public DataView() {
        this(null);
    }

    public DataView(String id) {
        super(id);
        axis = IChartViewer.Y1;
    }

    public void setAxis(int axis) {
        this.axis = axis;
    }

    @Override
    public Integer getAxis() {
        return Integer.valueOf(axis);
    }

    @Override
    protected String formatValueFromParentAxis(double value, int precision, Object parent) {
        String result;
        if (parent instanceof IChartViewer) {
            IChartViewer chart = (IChartViewer) parent;
            result = ChartUtils.formatValue(value, precision, chart.getAxisLabelFormat(axis),
                    chart.getAxisDateFormat(axis));
        } else {
            result = Double.toString(value);
        }
        return result;
    }

    @Override
    protected boolean isXTimeScale(Object parent) {
        boolean time;
        if (parent instanceof IChartViewer) {
            IChartViewer chart = (IChartViewer) parent;
            time = chart.isTimeScale(IChartViewer.X);
        } else {
            time = false;
        }
        return time;
    }

    @Override
    public StringBuilder appendConfiguration(StringBuilder configurationBuilder, String prefix) {
        StringBuilder builder = super.appendConfiguration(configurationBuilder, prefix);
        builder.append(prefix).append(AXIS).append(':').append(axis).append(ObjectUtils.NEW_LINE);
        return builder;
    }

    @Override
    public void applyConfiguration(String prefix, CfFileReader f) {
        super.applyConfiguration(prefix, f);
        List<String> p = f.getParam(prefix + AXIS);
        if (p != null) {
            setAxis(OFormat.getInt(p.get(0)));
        }
    }

}
