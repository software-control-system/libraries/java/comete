/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.shape;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import fr.soleil.comete.definition.widget.util.IChartConst.ShapeType;

/**
 * {@link AbstractShape} specialized in ellipses
 * 
 * @author huriez
 */
public class Oval extends Rectangle {

    public Oval(ShapeType type) {
        super(type);
    }

    public Oval(ShapeType type, java.awt.Rectangle rectangle, boolean fill, Color color) {
        super(type, rectangle, fill, color);
    }

    @Override
    public void drawShape(Graphics2D g) {
        Stroke oldStroke = null;
        if (getStroke() != null) {
            oldStroke = g.getStroke();
            g.setStroke(getStroke());
        }
        g.setColor(color);
        g.translate(x, y);
        if (isFill()) {
            g.fillOval(getRectangle().x, getRectangle().y, getRectangle().width, getRectangle().height);
        } else {
            g.drawOval(+getRectangle().x, getRectangle().y, getRectangle().width, getRectangle().height);
        }
        g.translate(-x, -y);
        if (oldStroke != null) {
            g.setStroke(oldStroke);
        }
    }
}
