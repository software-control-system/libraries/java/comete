/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Shape;

import ij.gui.Roi;

public class PixelCenterMaskManager extends MaskManager {

    public static final PixelCenterMaskManager DEFAULT_INSTANCE = new PixelCenterMaskManager();

    public PixelCenterMaskManager() {
        super();
    }

    @Override
    protected Shape getShape(Roi roi) {
        // in this manager, we want to use the shape, so we do extract it
        return extractShape(roi);
    }

    @Override
    protected boolean contains(Roi roi, Shape roiShape, int x, int y, int deltaX, int deltaY) {
        boolean contains = roiShape.contains(x + 0.5d - deltaX, y + 0.5d - deltaY);
        return contains;
    }
}
