/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util;

import java.util.Map;
import java.util.TreeMap;

import fr.soleil.comete.definition.util.IValueConvertor;

/**
 * A class that is able to convert screen/axis coordinates to image coordinates.
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 */
public class ImageScreenPositionCalculator {

    private static final double DEFAULT_AXIS_MIN = 0.0;
    private static final double DEFAULT_AXIS_MAX = 100.0;

    // axes' limits to remember
    protected double xAxisMin = DEFAULT_AXIS_MIN;
    protected double xAxisMax = DEFAULT_AXIS_MAX;
    protected double yAxisMin = DEFAULT_AXIS_MIN;
    protected double yAxisMax = DEFAULT_AXIS_MAX;

    // axes' limits set by user, used when not in autoscale mode
    protected double xAxisMinUser = DEFAULT_AXIS_MIN;
    protected double xAxisMaxUser = DEFAULT_AXIS_MAX;
    protected double yAxisMinUser = DEFAULT_AXIS_MIN;
    protected double yAxisMaxUser = DEFAULT_AXIS_MAX;

    // conversion ratio from image coordinates to axis coordinates
    protected double xRatio = 0;
    protected double yRatio = 0;

    // axis position convertors
    protected IValueConvertor xConvertor = null;
    protected IValueConvertor yConvertor = null;

    public double getXAxisMin() {
        return xAxisMin;
    }

    public void setXAxisMin(double xAxisMin) {
        this.xAxisMin = xAxisMin;
    }

    public double getXAxisMax() {
        return xAxisMax;
    }

    public void setXAxisMax(double xAxisMax) {
        this.xAxisMax = xAxisMax;
    }

    public double getYAxisMin() {
        return yAxisMin;
    }

    public void setYAxisMin(double yAxisMin) {
        this.yAxisMin = yAxisMin;
    }

    public double getYAxisMax() {
        return yAxisMax;
    }

    public void setYAxisMax(double yAxisMax) {
        this.yAxisMax = yAxisMax;
    }

    public double getXAxisMinUser() {
        return xAxisMinUser;
    }

    public void setXAxisMinUser(double xAxisMinUser) {
        this.xAxisMinUser = xAxisMinUser;
    }

    public double getXAxisMaxUser() {
        return xAxisMaxUser;
    }

    public void setXAxisMaxUser(double xAxisMaxUser) {
        this.xAxisMaxUser = xAxisMaxUser;
    }

    public double getYAxisMinUser() {
        return yAxisMinUser;
    }

    public void setYAxisMinUser(double yAxisMinUser) {
        this.yAxisMinUser = yAxisMinUser;
    }

    public double getYAxisMaxUser() {
        return yAxisMaxUser;
    }

    public void setYAxisMaxUser(double yAxisMaxUser) {
        this.yAxisMaxUser = yAxisMaxUser;
    }

    public double getXRatio() {
        return xRatio;
    }

    public void updateAxisRatioX(int dimX) {
        xRatio = (xAxisMax - xAxisMin) / dimX;
    }

    public double getYRatio() {
        return yRatio;
    }

    public void updateAxisRatioY(int dimY) {
        yRatio = (yAxisMax - yAxisMin) / dimY;
    }

    public IValueConvertor getXConvertor() {
        return xConvertor;
    }

    public void setXConvertor(IValueConvertor xConvertor) {
        this.xConvertor = xConvertor;
    }

    public IValueConvertor getYConvertor() {
        return yConvertor;
    }

    public void setYConvertor(IValueConvertor yConvertor) {
        this.yConvertor = yConvertor;
    }

    public Map<Double, Double> getCustomXPositions(double min, double max) {
        return getCustomPositions(min, max, xAxisMin, xAxisMax, xRatio, xConvertor);
    }

    public Map<Double, Double> getCustomYPositions(double min, double max) {
        return getCustomPositions(min, max, yAxisMin, yAxisMax, yRatio, yConvertor);
    }

    protected Map<Double, Double> getCustomPositions(double desiredMin, double desiredMax, double min, double max,
            double ratio, IValueConvertor convertor) {
        Map<Double, Double> labels;
        if ((convertor == null) || (!convertor.isValid())) {
            labels = null;
        } else {
            labels = new TreeMap<>();
            double step = (desiredMax - desiredMin) / 4;
            labels.put(Double.valueOf(desiredMin),
                    Double.valueOf(imagePixelToAxis(desiredMin, min, ratio, convertor, false)));
            boolean done = false;
            double value = desiredMin;
            if (step < 0) {
                if (min > 0 && min < 12) {
                    value = desiredMin - 1;
                    while (value > desiredMax) {
                        labels.put(Double.valueOf(value),
                                Double.valueOf(imagePixelToAxis(value, min, ratio, convertor, false)));
                        value -= 1;
                    }
                    value = desiredMin - 1;
                    done = true;
                }
            } else {
                if (max > 0 && max < 12) {
                    value = desiredMin + 1;
                    while (value < desiredMax) {
                        labels.put(Double.valueOf(value),
                                Double.valueOf(imagePixelToAxis(value, min, ratio, convertor, false)));
                        value += 1;
                    }
                    done = true;
                }
            }
            if (!done) {
                for (int i = 1; i < 4; i++) {
                    double tmp = value;
                    value = desiredMin + i * step;
                    labels.put(Double.valueOf(value),
                            Double.valueOf(imagePixelToAxis(value, min, ratio, convertor, false)));
                    if ((step < 0) && (i > 1)) {
                        value = tmp;
                    }
                }
            }
            labels.put(Double.valueOf(desiredMax),
                    Double.valueOf(imagePixelToAxis(desiredMax, min, ratio, convertor, false)));
            if (step > 0) {
                checkMax(value, desiredMax, min, max, ratio, convertor, labels);
            } else if (step < 0) {
                checkMax(value, desiredMin, min, min, ratio, convertor, labels);
            }
        }
        return labels;
    }

    protected void checkMax(double lastPosition, double desiredMax, double min, double max, double ratio,
            IValueConvertor convertor, Map<Double, Double> valueMap) {
        // check extreme case
        if (desiredMax == max) {
            double beforeMax = desiredMax - 1;
            if (beforeMax > lastPosition) {
                valueMap.remove(Double.valueOf(desiredMax));
                valueMap.put(Double.valueOf(beforeMax),
                        Double.valueOf(imagePixelToAxis(beforeMax, min, ratio, convertor, false)));
            }
        }
    }

    /**
     * Convert a full image pixel position to an axis position
     * 
     * @param ox image pixel position
     * @return A <code>double</code>
     */
    public double imagePixelToAxisXD(double ox) {
        return imagePixelToAxis(ox, xAxisMin, xRatio, xConvertor, true);
    }

    /**
     * Convert a full image pixel position to an axis position
     * 
     * @param oy image pixel position
     * @return A <code>double</code>
     */
    public double imagePixelToAxisYD(double oy) {
        return imagePixelToAxis(oy, yAxisMin, yRatio, yConvertor, true);
    }

    /**
     * Converts an x axis position to an image x pixel position
     * 
     * @param ax the axis position
     * @return A <code>double</code>
     */
    public double axisToImagePixelXD(double ax) {
        return axisToImagePixel(ax, xAxisMin, xAxisMax, xRatio, xConvertor);
    }

    /**
     * Converts an y axis position to an image y pixel position
     * 
     * @param ay the axis position
     * @return A <code>double</code>
     */
    public double axisToImagePixelYD(double ay) {
        return axisToImagePixel(ay, yAxisMin, yAxisMax, yRatio, yConvertor);
    }

    /**
     * Convert an image pixel position to an axis position
     * 
     * @param position The image position
     * @param min The axis minimum
     * @param ratio The axis ratio
     * @param convertor The axis value convertor
     * @param checkConvertor Whether to check convertor's validity
     * @return A <code>double</code>
     */
    protected double imagePixelToAxis(double position, double min, double ratio, IValueConvertor convertor,
            boolean checkConvertor) {
        double value = min + position * ratio;
        if ((!checkConvertor) || ((convertor != null) && convertor.isValid())) {
            value = convertor.convertValue(value);
        }
        return value;
    }

    /**
     * Converts an axis position to an image pixel position
     * 
     * @param position The axis position
     * @param min The axis minimum
     * @param max The axis maximum
     * @param ratio The axis ratio
     * @param convertor The axis value convertor
     * @return A <code>double</code>
     */
    protected double axisToImagePixel(double position, double min, double max, double ratio,
            IValueConvertor convertor) {
        double value = (position - min) / ratio;
        if ((convertor != null) && convertor.isValid()) {
            boolean found = false;
            double beforePrevious = Double.NaN;
            int beforePreviousIndex = -1;
            double previous = Double.NaN;
            int previousIndex = -1;
            double effective = Double.NaN;
            int effectiveIndex = -1;
            for (int index = 0; index < max && !found; index++) {
                double tmp = imagePixelToAxis(index, min, ratio, convertor, false);
                if (!Double.isNaN(tmp)) {
                    if (Double.isNaN(previous)) {
                        // go to next pixel coordinate --> buffer current pixel coordinate
                        previous = tmp;
                        previousIndex = index;
                        // Warning! We do want to test for <= instead of < in case of 2 subsequent identical values
                    } else if (Math.abs(tmp - position) <= Math.abs(previous - position)) {
                        // go to next pixel coordinate --> buffer current pixel coordinate and previous pixel coordinate
                        beforePrevious = previous;
                        beforePreviousIndex = previousIndex;
                        previous = tmp;
                        previousIndex = index;
                    } else {
                        // don't go any further. previous pixel coordinate is the nearest pixel coordinate
                        found = true;
                        effective = previous;
                        effectiveIndex = previousIndex;
                        if (!Double.isNaN(beforePrevious)) {
                            if (Math.abs(beforePrevious - position) < Math.abs(tmp - position)) {
                                // before previous pixel coordinate is nearer than current pixel coordinate:
                                // --> before previous pixel coordinate becomes previous pixel coordinate
                                previous = beforePrevious;
                                previousIndex = beforePreviousIndex;
                            } else {
                                // current pixel coordinate is nearer than before previous pixel coordinate:
                                // --> current pixel coordinate becomes previous pixel coordinate
                                previous = tmp;
                                previousIndex = index;
                            }
                        }
                    }
                }
            }
            if (Double.isNaN(effective)) {
                if (Double.isNaN(previous)) {
                    // no pixel coordinate found
                    value = Double.NaN;
                } else {
                    // consider previous pixel coordinate as the nearest one
                    value = previousIndex;
                }
            } else {
                if (Double.isNaN(previous) || (previousIndex == effectiveIndex) || (position == effective)) {
                    // if no previous pixel coordinate, consider effective one as the only possibility
                    value = effectiveIndex;
                } else {
                    // use linear equation to compute finest pixel coordinate
                    double[] affine = getLinearCoefficients(previousIndex, previous, effectiveIndex, effective);
                    if (affine[0] == 0) {
                        value = effectiveIndex;
                    } else {
                        value = (position - affine[1]) / affine[0];
                    }
                }
            }
        }
        return value;
    }

    /**
     * Calculates <code>a</code> and <code>b</code> coefficients of equation <code>y = ax + b</code>, knowing two points
     * 
     * @param x1 first point x coordinate
     * @param y1 first point y coordinate
     * @param x2 second point x coordinate
     * @param y2 second point y coordinate
     * @return a <code>double[]</code>:
     *         <ol start="0">
     *         <li><code>a</code></li>
     *         <li><code>b</code></li>
     *         </ol>
     */
    protected static double[] getLinearCoefficients(double x1, double y1, double x2, double y2) {
        double a, b;
        if (Double.isNaN(y1) || Double.isNaN(x1) || Double.isNaN(y2) || Double.isNaN(x2) || (x1 == x2)) {
            // Not an y = ax + b equation
            a = Double.NaN;
            b = Double.NaN;
        } else {
            // Solve y = ax + b
            a = (y1 - y2) / (x1 - x2);
            b = y1 - a * x1;
        }
        return new double[] { a, b };
    }

}
