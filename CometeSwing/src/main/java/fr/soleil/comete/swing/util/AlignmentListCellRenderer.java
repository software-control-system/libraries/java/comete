/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.swing.chart.util.ChartUtils;

public class AlignmentListCellRenderer extends TypeListCellRenderer {

    private static final long serialVersionUID = -8189266153625164577L;

    private static final String TXT_CENTER = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.center");
    private static final String TXT_TOP = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.top");
    private static final String TXT_LEFT = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.left");
    private static final String TXT_BOTTOM = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.bottom");
    private static final String TXT_RIGHT = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.right");

    public AlignmentListCellRenderer() {
        super();
    }

    @Override
    protected String getText(int id) {
        String text;
        switch (id) {
            case IComponent.CENTER:
                text = TXT_CENTER;
                break;
            case IComponent.TOP:
                text = TXT_TOP;
                break;
            case IComponent.LEFT:
                text = TXT_LEFT;
                break;
            case IComponent.BOTTOM:
                text = TXT_BOTTOM;
                break;
            case IComponent.RIGHT:
                text = TXT_RIGHT;
                break;
            default:
                text = Integer.toString(id);
                break;
        }
        return text;
    }

}
