/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.util.Comparator;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;

import fr.soleil.comete.definition.event.TreeNodeEvent;
import fr.soleil.comete.definition.event.TreeNodeEvent.NodeEventReason;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.model.SortTreeModel;

/**
 * The Default implementation of {@link CometeTreeModel}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DefaultCometeTreeModel extends SortTreeModel implements CometeTreeModel {

    private static final long serialVersionUID = 7687617937935036199L;

    /**
     * Constructor
     * 
     * @param root The root {@link ITreeNode} to use
     */
    public DefaultCometeTreeModel(ITreeNode root) {
        super(new DefaultMutableTreeNode());
        setRoot(root);
    }

    @Override
    protected Comparator<DefaultMutableTreeNode> getDefaultComparator(DefaultMutableTreeNode parent) {
        ITreeNodeIndexComparator comparator = new ITreeNodeIndexComparator(parent);
        if ((comparator.getChildren() == null) || comparator.getChildren().isEmpty()) {
            comparator = null;
        }
        return comparator;
    }

    @Override
    public void nodeChanged(final TreeNodeEvent event) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (event != null) {
                    ITreeNode treeNode = event.getSource();
                    DefaultMutableTreeNode node = recoverNode(treeNode);
                    if (node != null) {
                        NodeEventReason reason = event.getReason();
                        switch (reason) {
                            case DATA:
                            case NAME:
                            case IMAGE:
                                if (node.isRoot()) {
                                    reload();
                                } else {
                                    DefaultMutableTreeNode parent = (DefaultMutableTreeNode) node.getParent();
                                    try {
                                        fireTreeNodesChanged(parent, parent.getPath(),
                                                new int[] { parent.getIndex(node) },
                                                new DefaultMutableTreeNode[] { node });
                                    } catch (Exception e) {
                                        // XXX GIRARDOT: ugly hack to avoid some tree thread safety problems
                                        reload();
                                    }
                                }
                                break;
                            case INSERTED:
                                if (treeNode instanceof DefaultMutableTreeNode) {
                                    // XXX GIRARDOT: ugly hack to avoid some tree thread safety problems
                                    try {
                                        reload(node);
                                    } catch (Exception e) {
                                        reload();
                                    }
                                } else {
                                    int[] indexes = event.getIndexes();
                                    ITreeNode[] children = event.getChildren();
                                    if ((indexes != null) && (children != null)
                                            && (indexes.length == children.length)) {
                                        for (int i = 0; i < indexes.length; i++) {
                                            addTreeNode(node, children[i], indexes[i]);
                                        }
                                    }
                                }
                                break;
                            case REMOVED:
                            case REMOVED_TEMPORARY:
                                if (treeNode instanceof DefaultMutableTreeNode) {
                                    // XXX GIRARDOT: ugly hack to avoid some tree thread safety problems
                                    try {
                                        reload(node);
                                    } catch (Exception e) {
                                        reload();
                                    }
                                } else {
                                    int[] indexes = event.getIndexes();
                                    ITreeNode[] children = event.getChildren();
                                    if ((indexes != null) && (children != null)
                                            && (indexes.length == children.length)) {
                                        DefaultMutableTreeNode[] toRemove = new DefaultMutableTreeNode[indexes.length];
                                        for (int i = 0; i < indexes.length; i++) {
                                            if (i < node.getChildCount()) {
                                                DefaultMutableTreeNode childToRemove = (DefaultMutableTreeNode) node
                                                        .getChildAt(i);
                                                // We check for strict equality, because we don't want a similar
                                                // node: we want exactly that node
                                                if (getTreeNode(childToRemove) != children[i]) {
                                                    // If the node does not match expected one, search for it
                                                    childToRemove = recoverNode(children[i]);
                                                }
                                                toRemove[i] = childToRemove;
                                            }
                                        }
                                        removeNodes(toRemove);
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    @Override
    public void setRoot(ITreeNode root) {
        boolean reload = false;
        synchronized (sortLock) {
            ITreeNode rootNode = getTreeNode(getRoot());
            if (!ObjectUtils.sameObject(root, rootNode)) {
                if (rootNode != null) {
                    rootNode.removeTreeNodeListener(this);
                }
                doRemoveNodes(getRoot());
                if (root != null) {
                    if (root instanceof DefaultMutableTreeNode) {
                        super.doSetRoot((DefaultMutableTreeNode) root, true);
                    } else {
                        DefaultMutableTreeNode rootMutableTreeNode = new DefaultMutableTreeNode(root);
                        super.doSetRoot(rootMutableTreeNode, false);
                        List<ITreeNode> children = root.getChildren();
                        if (children != null) {
                            for (ITreeNode child : children) {
                                doAddTreeNode(getRoot(), child, -1);
                            }
                        }
                    }
                    root.addTreeNodeListener(this);
                }
                reload = true;
            }
        }
        if (reload) {
            reload();
        }
    }

    @Override
    public ITreeNode getTreeNode(DefaultMutableTreeNode node) {
        ITreeNode result = null;
        if (node != null) {
            if (node instanceof ITreeNode) {
                result = (ITreeNode) node;
            } else if (node.getUserObject() instanceof ITreeNode) {
                result = (ITreeNode) node.getUserObject();
            }
        }
        return result;
    }

    @Override
    public void removeNodes(DefaultMutableTreeNode... nodes) {
        boolean reload;
        synchronized (sortLock) {
            reload = doRemoveNodes(nodes);
        }
        if (reload) {
            reload();
        }
    }

    protected boolean doRemoveNodes(DefaultMutableTreeNode... nodes) {
        boolean reload = false;
        if (nodes != null) {
            for (DefaultMutableTreeNode node : nodes) {
                if (node != null) {
                    ITreeNode treeNode = getTreeNode(node);
                    if (treeNode != null) {
                        treeNode.removeTreeNodeListener(this);
                    }
                    while (node.getChildCount() > 0) {
                        reload = doRemoveNodes((DefaultMutableTreeNode) node.getChildAt(0)) || reload;
                    }
                    if ((node.getParent() != null) && (!(node instanceof ITreeNode))) {
                        try {
                            removeNodeFromParent(node);
                        } catch (Exception e) {
                            // XXX GIRARDOT: ugly hack to avoid some tree thread safety problems
                            reload = true;
                        }
                    }
                }
            }
        }
        return reload;
    }

    @Override
    public void addTreeNode(DefaultMutableTreeNode parent, ITreeNode treeNode, int index) {
        boolean reload;
        synchronized (sortLock) {
            reload = doAddTreeNode(parent, treeNode, index);
        }
        if (reload) {
            reload();
        }
    }

    protected boolean doAddTreeNode(DefaultMutableTreeNode parent, ITreeNode treeNode, int index) {
        boolean reload = false;
        if ((treeNode != null) && (parent != null)) {
            treeNode.removeTreeNodeListener(this);
            if (index < 0 || index > parent.getChildCount()) {
                index = parent.getChildCount();
            }
            DefaultMutableTreeNode child;
            if (treeNode instanceof DefaultMutableTreeNode) {
                child = (DefaultMutableTreeNode) treeNode;
            } else {
                child = new DefaultMutableTreeNode(treeNode);
            }
            boolean canInsert = true;
            if (child.getParent() == parent) {
                if (index == parent.getChildCount()) {
                    index--;
                }
                if (parent.getChildAt(index) == child) {
                    canInsert = false;
                }
            }
            if (canInsert) {
                try {
                    insertNodeInto(child, parent, index);
                } catch (Exception e) {
                    // XXX GIRARDOT: ugly hack to avoid some tree thread safety problems
                    reload = true;
                }
            }
            List<ITreeNode> children = treeNode.getChildren();
            if (children != null) {
                for (ITreeNode toAdd : children) {
                    reload = doAddTreeNode(child, toAdd, -1) || reload;
                }
            }
            treeNode.addTreeNodeListener(this);
        }
        return reload;
    }

    @Override
    public void removeTreeNodes(ITreeNode... iTreeNodes) {
        if (iTreeNodes != null) {
            DefaultMutableTreeNode[] nodes = new DefaultMutableTreeNode[iTreeNodes.length];
            for (int i = 0; i < iTreeNodes.length; i++) {
                nodes[i] = recoverNode(iTreeNodes[i]);
            }
            removeNodes(nodes);
        }
    }

    @Override
    public DefaultMutableTreeNode recoverNode(ITreeNode treeNode) {
        return recoverNode(treeNode, getRoot());
    }

    @Override
    public DefaultMutableTreeNode recoverNode(ITreeNode treeNode, DefaultMutableTreeNode startNode) {
        DefaultMutableTreeNode result = null;
        if ((treeNode != null) && (startNode != null)) {
            if (treeNode instanceof DefaultMutableTreeNode) {
                result = (DefaultMutableTreeNode) treeNode;
            } else {
                if (treeNode == startNode.getUserObject()) {
                    result = startNode;
                } else {
                    for (int i = 0; i < startNode.getChildCount(); i++) {
                        result = recoverNode(treeNode, (DefaultMutableTreeNode) startNode.getChildAt(i));
                        if (result != null) {
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }

    protected class ITreeNodeIndexComparator implements Comparator<DefaultMutableTreeNode> {
        protected final List<ITreeNode> children;

        public List<ITreeNode> getChildren() {
            return children;
        }

        public ITreeNodeIndexComparator(DefaultMutableTreeNode parent) {
            super();
            ITreeNode tmp = getTreeNode(parent);
            children = (tmp == null ? null : tmp.getChildren());
        }

        @Override
        public int compare(DefaultMutableTreeNode o1, DefaultMutableTreeNode o2) {
            int result;
            if (children == null) {
                result = 0;
            } else {
                ITreeNode itn1 = getTreeNode(o1), itn2 = getTreeNode(o2);
                if (itn1 == null) {
                    if (itn2 == null) {
                        result = 0;
                    } else {
                        result = -1;
                    }
                } else if (itn2 == null) {
                    result = 1;
                } else {
                    int i1 = children.indexOf(itn1), i2 = children.indexOf(itn2);
                    if (i1 == i2) {
                        result = 0;
                    } else if (i1 > i2) {
                        result = 1;
                    } else {
                        result = -1;
                    }
                }
            }
            return result;
        }

    }

}
