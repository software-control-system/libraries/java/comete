/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.ResourceBundle;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.definition.event.WheelSwitchEvent;
import fr.soleil.comete.definition.listener.IWheelSwitchListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IWheelSwitch;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;

/**
 * A {@link JDialog} to display and configure histogram.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class HistogramDialog extends JDialog implements PropertyChangeListener, ItemListener, IWheelSwitchListener {

    private static final long serialVersionUID = -8695656421618376371L;

    protected static final ResourceBundle MESSAGES = ResourceBundle
            .getBundle("fr.soleil.comete.swing.image.ijviewer.messages");

    protected final WeakReference<ImageViewer> imageViewerReference;

    protected final JPanel mainPanel;
    protected final Chart histogramViewer;
    protected final JLabel minLabel, maxLabel, stepLabel;
    protected final WheelSwitch minSwitch, maxSwitch, stepSwitch;
    protected final ConstrainedCheckBox autoScaleBox, autoStepBox;

    public HistogramDialog(Window owner, String title, ImageViewer imageViewer) {
        super(owner, title);
        setModal(false);
        mainPanel = new JPanel(new GridBagLayout());
        this.imageViewerReference = imageViewer == null ? null : new WeakReference<>(imageViewer);
        histogramViewer = createHistogramViewer();
        int x = 0, y = 0;
        GridBagConstraints histoConstraints = new GridBagConstraints();
        histoConstraints.fill = GridBagConstraints.BOTH;
        histoConstraints.gridx = x;
        histoConstraints.gridy = y++;
        histoConstraints.gridwidth = GridBagConstraints.REMAINDER;
        histoConstraints.weightx = 1;
        histoConstraints.weighty = 1;
        mainPanel.add(histogramViewer, histoConstraints);
        minLabel = createLabel(MESSAGES.getString("ImageJViewer.Action.HistogramMode.Min"));
        maxLabel = createLabel(MESSAGES.getString("ImageJViewer.Action.HistogramMode.Max"));
        stepLabel = createLabel(MESSAGES.getString("ImageJViewer.Action.HistogramMode.Step"));
        if (imageViewer == null) {
            minSwitch = createWheelSwitch(0, "%5d");
            maxSwitch = createWheelSwitch(65535, "%5d");
            stepSwitch = createWheelSwitch(1, "%5d");
        } else {
            minSwitch = createWheelSwitch(imageViewer.getHistogramMin(), imageViewer.getFormat());
            maxSwitch = createWheelSwitch(imageViewer.getHistogramMax(), imageViewer.getFormat());
            stepSwitch = createWheelSwitch(imageViewer.getHistogramStep(), imageViewer.getFormat());
        }
        autoScaleBox = createCheckBox(MESSAGES.getString("ImageJViewer.Action.HistogramMode.AutoScale"));
        autoStepBox = createCheckBox(MESSAGES.getString("ImageJViewer.Action.HistogramMode.AutoStep"));
        x = 0;
        x = addComponents(minLabel, minSwitch, x, y);
        x = addComponents(maxLabel, maxSwitch, x, y);
        addCheckBox(autoScaleBox, x, y++);
        x = 0;
        x = addComponents(stepLabel, stepSwitch, x, y);
        addCheckBox(autoStepBox, x, y++);
        setContentPane(mainPanel);
        pack();
    }

    protected JLabel createLabel(String text) {
        JLabel label = new JLabel(text);
        label.setFont(CometeUtils.getLabelbFont());
        return label;
    }

    protected WheelSwitch createWheelSwitch(double value, String format) {
        WheelSwitch wheelSwitch = new WheelSwitch();
        wheelSwitch.setFont(CometeUtils.getLabelFont());
        if (format != null) {
            wheelSwitch.setFormat(format);
        }
        wheelSwitch.setNumberValue(value);
        wheelSwitch.setEnabled(false);
        wheelSwitch.addWheelSwitchListener(this);
        return wheelSwitch;
    }

    protected ConstrainedCheckBox createCheckBox(String text) {
        ConstrainedCheckBox checkBox = new ConstrainedCheckBox(text);
        checkBox.setFont(CometeUtils.getLabelbFont());
        checkBox.setSelected(true);
        checkBox.addItemListener(this);
        return checkBox;
    }

    protected Chart createHistogramViewer() {
        Chart histogram = new Chart();
        histogram.setRefreshLater(true);
        histogram.setPreferredSize(500, 300);
        SamplingProperties samplingProperties = histogram.getSamplingProperties();
        samplingProperties.setEnabled(false);
        histogram.setSamplingProperties(samplingProperties);
        histogram.setAxisName(MESSAGES.getString("ImageJViewer.Action.HistogramMode.Values"), IChartViewer.X);
        histogram.setAxisName(MESSAGES.getString("ImageJViewer.Action.HistogramMode.Count"), IChartViewer.Y1);
        histogram.setHeader(ImageViewer.HISTOGRAM_NAME);
        histogram.setManagementPanelVisible(false);
        histogram.setFreezePanelVisible(false);
        histogram.setAxisLabelFormat(IChartViewer.DECINT_FORMAT, IChartViewer.Y1);
        PlotProperties plotProperties = new PlotProperties(CometeColor.RED);
        plotProperties.getCurve().setName(ImageViewer.HISTOGRAM_NAME);
        plotProperties.setFormat("%d");
        plotProperties.setAxisChoice(IChartViewer.Y1);
        plotProperties.setViewType(IChartViewer.TYPE_BAR);
        plotProperties.getBar().setFillStyle(IChartViewer.FILL_STYLE_SOLID);
        plotProperties.getBar().setFillingMethod(IChartViewer.METHOD_FILL_FROM_ZERO);
        histogram.setDataViewPlotProperties(ImageViewer.HISTOGRAM_NAME, plotProperties);
        return histogram;
    }

    public Chart getHistogramViewer() {
        return histogramViewer;
    }

    protected int addComponents(JLabel label, WheelSwitch editor, int x, int y) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = x++;
        constraints.gridy = y;
        constraints.weightx = 0;
        constraints.weightx = 0;
        constraints.insets = new Insets(5, 5, 5, 0);
        mainPanel.add(label, constraints);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = x++;
        constraints.gridy = y;
        constraints.weightx = 0;
        constraints.weightx = 0;
        mainPanel.add(editor, constraints);
        return x;
    }

    protected void addCheckBox(ConstrainedCheckBox box, int x, int y) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = x++;
        constraints.gridy = y;
        constraints.weightx = 1;
        constraints.weightx = 0;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.insets = new Insets(5, 20, 5, 5);
        mainPanel.add(box, constraints);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ((evt != null) && (evt.getSource() instanceof ImageViewer)) {
            ImageViewer viewer = (ImageViewer) evt.getSource();
            if (viewer == ObjectUtils.recoverObject(imageViewerReference)) {
                switch (evt.getPropertyName()) {
                    case ImageViewer.IMAGE_NAME:
                        String newName = (String) evt.getNewValue();
                        if ((newName == null) || newName.trim().isEmpty()) {
                            newName = ImageViewer.HISTOGRAM_NAME;
                        }
                        histogramViewer.setDataViewDisplayName(ImageViewer.HISTOGRAM_NAME, newName);
                        break;
                    case ImageViewer.FORMAT:
                        String format = (String) evt.getNewValue();
                        EDTManager.INSTANCE.runInDrawingThread(() -> {
                            minSwitch.setFormat(format);
                            maxSwitch.setFormat(format);
                            stepSwitch.setFormat(format);
                        });
                        break;
                    case ImageViewer.HISTOGRAM_MIN:
                        EDTManager.INSTANCE.runInDrawingThread(() -> {
                            minSwitch.setNumberValue((Number) evt.getNewValue());
                        });
                        break;
                    case ImageViewer.HISTOGRAM_MAX:
                        EDTManager.INSTANCE.runInDrawingThread(() -> {
                            maxSwitch.setNumberValue((Number) evt.getNewValue());
                        });
                        break;
                    case ImageViewer.HISTOGRAM_STEP:
                        EDTManager.INSTANCE.runInDrawingThread(() -> {
                            stepSwitch.setNumberValue((Number) evt.getNewValue());
                        });
                        break;
                    case ImageViewer.HISTOGRAM_AUTO_STEP:
                        EDTManager.INSTANCE.runInDrawingThread(() -> {
                            autoStepBox.setSelected((Boolean) evt.getNewValue());
                        });
                        break;
                    case ImageViewer.HISTOGRAM_AUTO_SCALE:
                        EDTManager.INSTANCE.runInDrawingThread(() -> {
                            autoScaleBox.setSelected((Boolean) evt.getNewValue());
                        });
                        break;
                    default:
                        break;
                }
            }
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e != null) {
            ImageViewer viewer = ObjectUtils.recoverObject(imageViewerReference);
            if ((viewer != null)
                    && (e.getStateChange() == ItemEvent.SELECTED || e.getStateChange() == ItemEvent.DESELECTED)) {
                if (e.getSource() == autoScaleBox) {
                    boolean enabled = !autoScaleBox.isSelected();
                    viewer.setHistogramAutoScale(autoScaleBox.isSelected());
                    EDTManager.INSTANCE.runInDrawingThread(() -> {
                        minSwitch.setEnabled(enabled);
                        maxSwitch.setEnabled(enabled);
                    });
                } else if (e.getSource() == autoStepBox) {
                    boolean enabled = !autoStepBox.isSelected();
                    viewer.setHistogramAutoStep(autoStepBox.isSelected());
                    EDTManager.INSTANCE.runInDrawingThread(() -> {
                        stepSwitch.setEnabled(enabled);
                    });
                }
            }
        }
    }

    @Override
    public void valueChange(WheelSwitchEvent evt) {
        if (evt != null) {
            ImageViewer viewer = ObjectUtils.recoverObject(imageViewerReference);
            if (viewer != null) {
                IWheelSwitch source = evt.getSource();
                if (source == minSwitch) {
                    viewer.setHistogramMin(evt.getValue());
                } else if (source == maxSwitch) {
                    viewer.setHistogramMax(evt.getValue());
                } else if (source == stepSwitch) {
                    viewer.setHistogramStep(evt.getValue());
                }
            }
        }
    }

}
