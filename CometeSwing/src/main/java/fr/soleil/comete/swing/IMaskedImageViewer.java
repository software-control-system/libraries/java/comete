/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import fr.soleil.comete.definition.data.target.IBeamPointTarget;
import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MathematicSector;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.util.mask.MaskController;
import fr.soleil.data.target.scalar.ITextTarget;

public interface IMaskedImageViewer extends ITextTarget, IBeamPointTarget {

    public Object getValue();

    public int getDimX();

    public int getDimY();

    public IJRoiManager getRoiManager();

    public void setRoiManager(IJRoiManager roiManager);

    public MaskController getMaskController();

    public void setSector(MathematicSector sector);

    public MathematicSector getSector();

    public Mask getSectorMask();

    public String getApplicationId();

    public Mask getMask();

    public boolean setMask(Mask mask);

    public void loadMask(String path, boolean addToCurrentMask);

    public void setAsMask();

    public boolean isCleanOnDataSetting();

    /**
     * In this method, {@link IMaskedImageViewer} is expected to return image name, if any
     */
    @Override
    public String getText();
}
