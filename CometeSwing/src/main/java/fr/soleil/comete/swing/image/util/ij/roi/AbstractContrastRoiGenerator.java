/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.exception.ApplicationIdException;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.lib.project.swing.icons.Icons;
import ij.gui.Roi;

public abstract class AbstractContrastRoiGenerator extends AutoDeleteSimpleRoiGenerator {

    protected static final Cursor CURSOR = Icons.generateCursorFromIcon(ImageViewer.CONTRAST_DEFAULT,
            ImageViewer.MODE_FOCUS_ACTION);

    public AbstractContrastRoiGenerator(String text, String description, ImageIcon icon) {
        super(text, description, icon);
    }

    @Override
    public int getRoiMode() {
        return IJRoiManager.FOCUS_ROI_MODE;
    }

    @Override
    protected BasicStroke generateRoiStroke() {
        return IJCanvas.DEFAULT_FOCUS_ROI_STROKE;
    }

    @Override
    protected Color generateRoiColor() {
        return IJCanvas.DEFAULT_FOCUS_ROI_COLOR;
    }

    @Override
    public int getRoiModeMenu() {
        return ImageViewer.TYPE_SELECTION_MENU;
    }

    @Override
    public String getActionCommand() {
        return ImageViewer.MODE_FOCUS_ACTION;
    }

    @Override
    public Cursor getCursor() {
        return CURSOR;
    }

    @Override
    protected void doExpectedWork(MouseEvent e, ImageViewer viewer, Roi focusRoi, IJCanvas canvas) {
        if (focusRoi != null) {
            contrast(focusRoi, viewer);
        }
    }

    /**
     * Applies the contrast algorithm on an {@link ImageViewer}
     * 
     * @param roi The {@link Roi} that determines the rectangle region for which to compute the contrast
     * @param viewer The {@link ImageViewer}
     * @throws ApplicationIdException If the application id is not yet set for this {@link ImageViewer}
     * @see ImageViewer#setApplicationId(String)
     */
    protected abstract void contrast(Roi roi, ImageViewer viewer) throws ApplicationIdException;

}
