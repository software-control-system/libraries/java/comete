/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import javax.activation.UnsupportedDataTypeException;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.data.target.matrix.IBooleanMatrixComponent;
import fr.soleil.data.container.matrix.BooleanMatrix;
import fr.soleil.data.mediator.Mediator;

public class BooleanMatrixTable extends AbstractMatrixTable<Boolean> implements IBooleanMatrixComponent {

    private static final long serialVersionUID = 7703897540610548835L;

    @Override
    protected boolean isMenuTrimTableVisible() {
        return false;
    }

    @Override
    public boolean[][] getBooleanMatrix() {
        boolean[][] result = null;
        if (matrix != null) {
            Object[] value = matrix.getValue();
            if (value instanceof boolean[][]) {
                result = (boolean[][]) value;
            } else if (value instanceof Boolean[][]) {
                Boolean[][] bValue = (Boolean[][]) value;
                result = new boolean[matrix.getHeight()][matrix.getWidth()];
                for (int row = 0; row < result.length; row++) {
                    for (int col = 0; col < result[row].length; col++) {
                        result[row][col] = (bValue[row][col] == null ? false : bValue[row][col]);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public void setBooleanMatrix(boolean[][] value) {
        BooleanMatrix result = new BooleanMatrix(Boolean.TYPE);
        try {
            result.setValue(value);
            setData(result);
        } catch (UnsupportedDataTypeException e) {
            // Should not happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to set boolean matrix", e);
        }
    }

    @Override
    public boolean[] getFlatBooleanMatrix() {
        boolean[] result = null;
        if (matrix != null) {
            Object flatValue = matrix.getFlatValue();
            if (flatValue instanceof boolean[]) {
                result = (boolean[]) flatValue;
            } else if (flatValue instanceof Boolean[]) {
                Boolean[] bValue = (Boolean[]) flatValue;
                result = new boolean[bValue.length];
                for (int i = 0; i < bValue.length; i++) {
                    result[i] = (bValue[i] == null ? false : bValue[i]);
                }
            }
        }
        return result;
    }

    @Override
    public void setFlatBooleanMatrix(boolean[] value, int width, int height) {
        BooleanMatrix bMatrix = new BooleanMatrix(Boolean.TYPE);
        try {
            bMatrix.setFlatValue(value, height, width);
            setData(bMatrix);
        } catch (UnsupportedDataTypeException e) {
            // Should not happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to set flat boolean matrix", e);
        }
    }
}
