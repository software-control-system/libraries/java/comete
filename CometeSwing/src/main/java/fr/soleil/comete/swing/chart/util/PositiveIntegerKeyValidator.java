/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.awt.event.KeyEvent;

public class PositiveIntegerKeyValidator {

    protected static final char[] ALLOWED_PRECISION_CHARACTERS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

    protected static final int[] ALLOWED_PRECISION_KEY_CODES = { KeyEvent.VK_BACK_SPACE, KeyEvent.VK_DELETE,
            KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT, KeyEvent.VK_KP_LEFT, KeyEvent.VK_KP_RIGHT, KeyEvent.VK_ENTER,
            KeyEvent.VK_TAB, KeyEvent.VK_SHIFT };

    public static boolean isValid(KeyEvent evt) {
        boolean validated = false;
        if (evt != null) {
            char c = evt.getKeyChar();
            for (char element : ALLOWED_PRECISION_CHARACTERS) {
                if (c == element) {
                    validated = true;
                    break;
                }
            }
            if (!validated) {
                int code = evt.getKeyCode();
                for (int element : ALLOWED_PRECISION_KEY_CODES) {
                    if (code == element) {
                        validated = true;
                        break;
                    }
                }
            }
        }
        return validated;
    }

}
