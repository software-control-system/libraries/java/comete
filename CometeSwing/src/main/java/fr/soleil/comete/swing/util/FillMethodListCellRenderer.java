/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.chart.util.ChartUtils;

public class FillMethodListCellRenderer extends TypeListCellRenderer {

    private static final long serialVersionUID = 18289851742679171L;

    protected static final String TOP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.method.up");
    protected static final String BOTTOM = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.method.bottom");
    protected static final String ZERO = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.fill.method.zero");

    public FillMethodListCellRenderer() {
        super();
    }

    @Override
    protected String getText(int id) {
        String text;
        switch (id) {
            case IChartViewer.METHOD_FILL_FROM_TOP:
                text = TOP;
                break;
            case IChartViewer.METHOD_FILL_FROM_ZERO:
                text = ZERO;
                break;
            case IChartViewer.METHOD_FILL_FROM_BOTTOM:
                text = BOTTOM;
                break;
            default:
                text = Integer.toString(id);
                break;
        }
        return text;
    }

}
