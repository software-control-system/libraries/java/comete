/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.print.PrintException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.EventListenerList;
import javax.swing.filechooser.FileFilter;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.event.ChartViewerEvent.Reason;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.util.IntRotationTool;
import fr.soleil.comete.definition.util.SnapshotEventDelegate;
import fr.soleil.comete.definition.util.TransferEventDelegate;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.DragProperties;
import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.comete.definition.widget.properties.OffsetProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.comete.definition.widget.util.ChartActionEvent;
import fr.soleil.comete.definition.widget.util.ChartActionListener;
import fr.soleil.comete.definition.widget.util.ChartEventDelegate;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.definition.widget.util.IChartConst;
import fr.soleil.comete.swing.chart.ChartOption;
import fr.soleil.comete.swing.chart.DataViewOption;
import fr.soleil.comete.swing.chart.DialogList;
import fr.soleil.comete.swing.chart.axis.model.AxisAttributes;
import fr.soleil.comete.swing.chart.axis.scale.AbstractAxisScale;
import fr.soleil.comete.swing.chart.axis.view.AbstractAxisView;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.ChartData;
import fr.soleil.comete.swing.chart.data.DataView;
import fr.soleil.comete.swing.chart.data.DataViewList;
import fr.soleil.comete.swing.chart.expression.ExpressionOption;
import fr.soleil.comete.swing.chart.expression.ExpressionParser;
import fr.soleil.comete.swing.chart.util.AbstractChartCustomMenu;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.chart.util.DataViewComparator;
import fr.soleil.comete.swing.chart.util.IDataViewManager;
import fr.soleil.comete.swing.chart.util.ITableRowListener;
import fr.soleil.comete.swing.chart.util.JTableRow;
import fr.soleil.comete.swing.chart.util.JTableRowEvent;
import fr.soleil.comete.swing.chart.util.PlotPropertiesTool;
import fr.soleil.comete.swing.chart.util.PrecisionPanel;
import fr.soleil.comete.swing.chart.util.TabbedLine;
import fr.soleil.comete.swing.chart.view.component.ChartLegend;
import fr.soleil.comete.swing.chart.view.component.ChartPopupMenu;
import fr.soleil.comete.swing.chart.view.component.ChartView;
import fr.soleil.comete.swing.util.CfFileReader;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.OFormat;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.ColorUtils;
import fr.soleil.lib.project.awt.IDataComponent;
import fr.soleil.lib.project.awt.listener.DataComponentDelegate;
import fr.soleil.lib.project.expression.IExpressionParser;
import fr.soleil.lib.project.expression.jeval.JEvalExpressionParser;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.file.log.CsvLogFileWriter;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.swing.SnapshotUtil;
import fr.soleil.lib.project.swing.SplitPaneUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.file.ExtensionFileFilter;
import fr.soleil.lib.project.swing.print.EasyPrinter;
import fr.soleil.lib.project.swing.ui.ColoredSplitPaneUI;

/**
 * CometeSwing alternative implementation of {@link IChartViewer}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class Chart extends Panel implements IChartViewer, IDataViewManager, IDataComponent, ITableRowListener,
        ActionListener, PropertyChangeListener {

    private static final long serialVersionUID = 4994034770616228049L;

    protected static final String GRAPH_TITLE = "graph_title";
    protected static final String LABEL_VISIBLE = "label_visible";
    protected static final String LABEL_PLACEMENT = "label_placement";
    protected static final String LABEL_FONT = "label_font";
    protected static final String LEGEND_PROPORTION = "legend_proportion";
    protected static final String GRAPH_BACKGROUND = "graph_background";
    protected static final String CHART_BACKGROUND = "chart_background";
    protected static final String TITLE_FONT = "title_font";
    protected static final String DISPLAY_DURATION = "display_duration";
    protected static final String PRECISION = "precision";
    protected static final String EXPRESSIONS = "expressions";
    protected static final String EXPRESSION_PREFIX = "expression_";
    protected static final String EXPRESSION_SUFFIX = "_expression";
    protected static final String IS_X = "_isX";
    protected static final String VARIABLES = "_variables";
    protected static final String VARIABLE = "_variable_";
    protected static final String X_PREFIX = "x";
    protected static final String Y1_PREFIX = "y1";
    protected static final String Y2_PREFIX = "y2";
    protected static final String DV_NUMBER = "dv_number";
    protected static final String DATAVIEW_PREFIX = "dv";

    protected static final ImageIcon LOADING_ICON = new ImageIcon(
            Chart.class.getResource("/fr/soleil/lib/project/swing/icons/loading.gif"));

    protected static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");
    protected static final DataViewComparator VIEW_COMPARATOR = new DataViewComparator();

    public static final String MENU_CHART_PROPERTIES = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.properties");
    public static final String MENU_SAVE_DATA_FILE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.file.data.save");
    public static final String MENU_SHOW_TABLE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.show.table");
    public static final String MENU_SHOW_IN = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.show.in");
    public static final String MENU_EXIT_ZOOM = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.zoom.back");
    public static final String MENU_PRINT_CHART = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.print");
    public static final String MENU_ABSCISSA_MARGIN_ERROR = PrecisionPanel.VALUE_PRECISION_TITLE;
    public static final String MENU_SAVE_SNAPSHOT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.snapshot.save");
    public static final String MENU_FREEZE_VALUES = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.freeze.values");
    public static final String MENU_CLEAR_FREEZES = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.freeze.clear");
    public static final String MENU_LOAD_DATA = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.file.data.load");
    public static final String MENU_DATAVIEW_PROPERTIES = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.properties.dataview");
    public static final String MENU_SHOW_STATISTICS = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.statistics.show");
    public static final String MENU_EVALUATE_EXPRESSION = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.expression.evaluate");
    public static final String MENU_CLEAR_EXPRESSIONS = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.expression.clear");
    public static final String MENU_LOAD_SETTINGS = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.settings.load");
    public static final String MENU_SAVE_SETTINGS = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.settings.save");

    // The minimum difference between 2 proportion values to consider a change occurred
    private static final double MIN_PROPORTION_DELTA = 5e-3;

    protected final ConcurrentMap<String, Object> data;

    protected final ChartLegend chartLegend;
    protected final ChartView chartView;
    protected final ChartPopupMenu chartMenu;
    protected final JPanel chartPanel;
    protected JSplitPane chartSplitPane;
    protected final JScrollPane chartLegendScrollPane;

    protected final ConcurrentHashMap<String, PlotProperties> plotPropertiesMap;
    protected final ColorUtils lineColorRotationTool;
    protected ColorUtils barColorRotationTool;
    protected ColorUtils markerColorRotationTool;
    protected IntRotationTool lineStyleRotationTool;
    protected IntRotationTool markerStyleRotationTool;
    protected IntRotationTool markerSizeRotationTool;
    protected IntRotationTool lineWidthRotationTool;
    protected IntRotationTool barWidthRotationTool;
    protected IntRotationTool viewStyleRotationTool;
    protected IntRotationTool fillStyleRotationTool;
    protected IntRotationTool axisRotationTool;
    protected boolean cyclingCustomMap;
    protected DataViewOption dataViewOption;
    protected JDialog dataViewOptionDialog;
    protected ChartOption chartOption;
    protected JDialog chartOptionDialog;
    protected ExpressionOption expressionOption;
    protected JDialog expressionOptionDialog;

    protected boolean managementPanelVisible;
    protected final JPanel managementPanel;
    protected final ComboBox visibleViews;
    protected final ComboBox hiddenViews;
    protected final ComboBox axisSelectionCombo;
    protected final JLabel visibleLabel;
    protected final JLabel hiddenLabel;
    protected final JButton hideDataViewButton;
    protected final JButton showDataViewButton;
    protected final Object visibilityLock;

    protected boolean freezePanelVisible;
    protected final JPanel freezePanel;
    protected final JButton freezeButton;
    protected final JButton clearFreezesButton;

    protected final JPanel southPanel;
    protected final JLabel loadDataLabel;

    // Listeners
    protected final ChartEventDelegate chartEventDelegate;
    protected final SnapshotEventDelegate snapshotEventDelegate;
    protected final TransferEventDelegate transferEventDelegate;

    protected boolean legendVisible;
    protected int legendPlacement;
    protected double legendProportion;

    protected boolean autoHighlight;
    protected boolean axisSelectionVisible;
    protected boolean autoHide;
    protected boolean cleanOnRemoving;
    protected boolean generateRandomIDsForLoadedData;

    protected final List<String> freezeIds;

    protected String noValueString;

    protected String preferredSnapshotExtension;

    // Used to save expressions in files, but can be used to dynamically update
    // the expression DataViews.
    // Key : The expression DataView
    // Associated value : Object[]
    // 0 : Integer <-> axis
    // 1 : String <-> expression
    // 2 : Boolean <-> x
    // 3..length-1 : variables
    protected ConcurrentHashMap<DataView, Object[]> expressionMap;

    protected volatile boolean dataViewsSortedOnX;
    protected final DataViewList dataViewList;
    protected Comparator<AbstractDataView> dataViewComparator;
    // Timestamp precision to build the table. Positive int. The lower, the more precise.
    protected int timePrecision;

    // Data table management
    protected final JTableRow dataTable;
    protected Window dataTableWindow;
    protected String[] displayedIds;
    protected boolean showAll;
    protected boolean preferDialog;
    protected boolean modalDialog;
    protected boolean editable;

    protected String indexColumnName;
    protected String dateColumnName;

    // settings
    protected final ExtensionFileFilter settingsFileFilter;
    protected String settingsDirectory;

    protected boolean refreshLater;
    protected boolean useDisplayNameForDataSaving;
    protected boolean notifyListenersOnDataAdded;
    protected boolean autoDeactivateDataViewComboBoxes;
    protected boolean autoFillDataViewComboBoxes;
    protected IExpressionParser expressionParser;
    private Component northComponent;

    // IDataComponent specific (JAVAAPI-621)
    protected final DataComponentDelegate dataComponentDelegate;
    protected volatile Boolean shouldMeasureItems, shouldRevalidate;
    protected volatile List<DataView>[] updatedViews;

    public Chart() {
        super();
        dataViewComparator = VIEW_COMPARATOR;
        loadDataLabel = new JLabel("loading data...", JLabel.CENTER);
        loadDataLabel.setIcon(LOADING_ICON);
        chartEventDelegate = new ChartEventDelegate(this);
        dataViewList = new DataViewList();
        axisSelectionVisible = false;
        autoHighlight = false;
        autoHide = false;
        cleanOnRemoving = false;
        legendVisible = true;
        generateRandomIDsForLoadedData = false;
        noValueString = ChartProperties.DEFAULT_NO_VALUE_STRING;
        preferredSnapshotExtension = null;
        legendPlacement = LABEL_ROW;
        legendProportion = Double.NaN;
        chartView = generateChartView();
        chartLegend = generateChartLegend();
        chartLegendScrollPane = new JScrollPane(chartLegend);
        plotPropertiesMap = new ConcurrentHashMap<>();
        lineColorRotationTool = new ColorUtils();
        barColorRotationTool = null;
        markerColorRotationTool = null;
        lineStyleRotationTool = null;
        markerStyleRotationTool = null;
        markerSizeRotationTool = null;
        lineWidthRotationTool = null;
        barWidthRotationTool = null;
        viewStyleRotationTool = null;
        fillStyleRotationTool = null;
        axisRotationTool = null;
        cyclingCustomMap = true;
        settingsFileFilter = new ExtensionFileFilter("txt", "Text files");
        settingsDirectory = ChartUtils.DOT;
        refreshLater = false;
        useDisplayNameForDataSaving = false;
        notifyListenersOnDataAdded = false;
        autoDeactivateDataViewComboBoxes = false;
        autoFillDataViewComboBoxes = false;
        setLayout(new BorderLayout());
        chartPanel = new JPanel(new BorderLayout());
        add(chartPanel, BorderLayout.CENTER);
        initDataViewOption();
        initChartOption();
        initExpressionOption();
        managementPanelVisible = true;
        visibilityLock = new Object();
        managementPanel = new JPanel();
        visibleViews = new ComboBox();
        visibleViews.setMinimumSize(new Dimension(30, visibleViews.getPreferredSize().height));
        hiddenViews = new ComboBox();
        hiddenViews.setMinimumSize(new Dimension(30, hiddenViews.getPreferredSize().height));
        axisSelectionCombo = new ComboBox();
        axisSelectionCombo.setValueList("Y1", "Y2");
        visibleLabel = new JLabel("Visible data views: ");
        hiddenLabel = new JLabel("Hidden data views: ");
        hideDataViewButton = new JButton(
                new ImageIcon(getClass().getResource("/fr/soleil/comete/icons/hide-curve.png")));
        hideDataViewButton.setMargin(CometeUtils.getzInset());
        hideDataViewButton.setMinimumSize(hideDataViewButton.getPreferredSize());
        hideDataViewButton.setText("Hide");
        hideDataViewButton.setHorizontalAlignment(LEFT);
        hideDataViewButton.addActionListener(this);
        showDataViewButton = new JButton(
                new ImageIcon(getClass().getResource("/fr/soleil/comete/icons/show-curve.png")));
        showDataViewButton.setMargin(CometeUtils.getzInset());
        showDataViewButton.setMinimumSize(showDataViewButton.getPreferredSize());
        showDataViewButton.setText("Display");
        showDataViewButton.setHorizontalAlignment(LEFT);
        showDataViewButton.addActionListener(this);
        initManagementPanel();
        managementPanel.setVisible(managementPanelVisible);
        freezePanelVisible = true;
        freezePanel = new JPanel();
        freezeButton = new JButton("Freeze Values");
        freezeButton.setMargin(new Insets(0, 0, 0, 0));
        freezeButton.addActionListener(this);
        clearFreezesButton = new JButton("Clear Freezes");
        clearFreezesButton.setMargin(new Insets(0, 0, 0, 0));
        clearFreezesButton.addActionListener(this);
        initFreezePanel();
        freezePanel.setVisible(freezePanelVisible);
        southPanel = new JPanel(new BorderLayout());
        southPanel.add(managementPanel, BorderLayout.SOUTH);
        southPanel.add(freezePanel, BorderLayout.NORTH);
        add(southPanel, BorderLayout.SOUTH);
        dataViewOptionDialog = null;
        chartOptionDialog = null;
        expressionOptionDialog = null;
        data = new ConcurrentHashMap<>();
        freezeIds = new ArrayList<>();
        expressionMap = new ConcurrentHashMap<>();
        updateChartLegend();
        dataViewsSortedOnX = true;
        chartMenu = new ChartPopupMenu(this);
        chartView.setComponentPopupMenu(chartMenu);
        timePrecision = 0;
        editable = false;
        dataTable = new JTableRow();
        dataTable.addTableRowListener(this);
        dataTable.setEditable(editable);
        dataTableWindow = null;
        displayedIds = null;
        showAll = false;
        preferDialog = true;
        modalDialog = false;
        indexColumnName = TabbedLine.DEFAULT_INDEX_COLUMN_NAME;
        dateColumnName = TabbedLine.DEFAULT_DATE_COLUMN_NAME;
        setAxisSelectionVisible(axisSelectionVisible);
        setCometeBackground(CometeColor.WHITE);
        setChartCometeBackground(CometeColor.WHITE);
        updateFreezePanelAvailability();

        snapshotEventDelegate = new SnapshotEventDelegate(this, ChartUtils.DOT);
        transferEventDelegate = new TransferEventDelegate(this, ChartUtils.DOT);
        dataComponentDelegate = new DataComponentDelegate();
        dataComponentDelegate.setupAndListenToComponent(this);
        dataComponentDelegate.setUpdateOnDataChanged(this, true);
        shouldMeasureItems = shouldRevalidate = null;
        updatedViews = null;
    }

    protected void initDataViewOption() {
        dataViewOption = new DataViewOption();
        dataViewOption.setChart(this);
    }

    protected void initChartOption() {
        chartOption = new ChartOption();
        chartOption.setChart(this);
    }

    protected void initExpressionOption() {
        expressionOption = new ExpressionOption();
        expressionOption.setChart(this);
    }

    protected void initManagementPanel() {
        managementPanel.setLayout(new GridBagLayout());
        // Hide visible views
        final GridBagConstraints visibleLabelConstraints = new GridBagConstraints();
        visibleLabelConstraints.fill = GridBagConstraints.BOTH;
        visibleLabelConstraints.gridx = 0;
        visibleLabelConstraints.gridy = 0;
        visibleLabelConstraints.weightx = 0;
        visibleLabelConstraints.weighty = 1;
        visibleLabelConstraints.insets = new Insets(5, 10, 0, 0);
        managementPanel.add(visibleLabel, visibleLabelConstraints);
        final GridBagConstraints visibleDataViewConstraints = new GridBagConstraints();
        visibleDataViewConstraints.fill = GridBagConstraints.BOTH;
        visibleDataViewConstraints.gridx = 1;
        visibleDataViewConstraints.gridy = 0;
        visibleDataViewConstraints.weightx = 1;
        visibleDataViewConstraints.weighty = 1;
        visibleDataViewConstraints.insets = new Insets(5, 10, 0, 0);
        managementPanel.add(visibleViews, visibleDataViewConstraints);
        final GridBagConstraints removeDataViewConstraints = new GridBagConstraints();
        removeDataViewConstraints.fill = GridBagConstraints.BOTH;
        removeDataViewConstraints.gridx = 2;
        removeDataViewConstraints.gridy = 0;
        removeDataViewConstraints.weightx = 0;
        removeDataViewConstraints.weighty = 1;
        removeDataViewConstraints.insets = new Insets(5, 10, 0, 10);
        managementPanel.add(hideDataViewButton, removeDataViewConstraints);

        // Show hidden views
        final GridBagConstraints hiddenLabelConstraints = new GridBagConstraints();
        hiddenLabelConstraints.fill = GridBagConstraints.BOTH;
        hiddenLabelConstraints.gridx = 0;
        hiddenLabelConstraints.gridy = 1;
        hiddenLabelConstraints.weightx = 0;
        hiddenLabelConstraints.weighty = 1;
        hiddenLabelConstraints.insets = new Insets(5, 10, 5, 0);
        managementPanel.add(hiddenLabel, hiddenLabelConstraints);
        final GridBagConstraints hiddenDataViewConstraints = new GridBagConstraints();
        hiddenDataViewConstraints.fill = GridBagConstraints.BOTH;
        hiddenDataViewConstraints.gridx = 1;
        hiddenDataViewConstraints.gridy = 1;
        hiddenDataViewConstraints.weightx = 0;
        hiddenDataViewConstraints.weighty = 1;
        hiddenDataViewConstraints.insets = new Insets(5, 10, 5, 0);
        managementPanel.add(hiddenViews, hiddenDataViewConstraints);
        final GridBagConstraints addButtonConstraints = new GridBagConstraints();
        addButtonConstraints.fill = GridBagConstraints.BOTH;
        addButtonConstraints.gridx = 2;
        addButtonConstraints.gridy = 1;
        addButtonConstraints.weightx = 0;
        addButtonConstraints.weighty = 1;
        addButtonConstraints.insets = new Insets(5, 10, 5, 10);
        managementPanel.add(showDataViewButton, addButtonConstraints);
        final GridBagConstraints axisSelectionConstraints = new GridBagConstraints();
        axisSelectionConstraints.fill = GridBagConstraints.BOTH;
        axisSelectionConstraints.gridx = 3;
        axisSelectionConstraints.gridy = 1;
        axisSelectionConstraints.weightx = 0;
        axisSelectionConstraints.weighty = 1;
        axisSelectionConstraints.insets = new Insets(5, 0, 5, 10);
        managementPanel.add(axisSelectionCombo, axisSelectionConstraints);
    }

    protected void initFreezePanel() {
        freezePanel.setLayout(new BorderLayout());
        freezePanel.add(freezeButton, BorderLayout.WEST);
        freezePanel.add(clearFreezesButton, BorderLayout.EAST);
    }

    /**
     * Returns the {@link IExpressionParser} this {@link Chart} uses to parse expressions
     * 
     * @return An {@link IExpressionParser}
     */
    public IExpressionParser getExpressionParser() {
        return expressionParser;
    }

    /**
     * Sets the {@link IExpressionParser} that can be used to parse expressions
     * 
     * @param expressionParser The {@link IExpressionParser} that can be used to parse expressions
     */
    public void setExpressionParser(IExpressionParser expressionParser) {
        this.expressionParser = (expressionParser == null ? new JEvalExpressionParser() : expressionParser);
    }

    public JDialog getDataViewOptionDialog() {
        if (dataViewOptionDialog == null) {
            dataViewOptionDialog = new JDialog(WindowSwingUtils.getWindowForComponent(this), dataViewOption.getName());
            dataViewOptionDialog.setContentPane(dataViewOption);
            dataViewOptionDialog.setModal(true);
        }
        return dataViewOptionDialog;
    }

    public JDialog getChartOptionDialog() {
        if (chartOptionDialog == null) {
            chartOptionDialog = new JDialog(WindowSwingUtils.getWindowForComponent(this), chartOption.getName());
            chartOptionDialog.setContentPane(chartOption);
            chartOptionDialog.setModal(true);
        }
        return chartOptionDialog;
    }

    public JDialog getExpressionOptionDialog() {
        if (expressionOptionDialog == null) {
            expressionOptionDialog = new JDialog(WindowSwingUtils.getWindowForComponent(this),
                    expressionOption.getName());
            expressionOptionDialog.setContentPane(expressionOption);
            expressionOptionDialog.setModal(true);
        }
        return expressionOptionDialog;
    }

    public void showChartOptionDialog() {
        chartOption.setProperties(getChartProperties());
        getChartOptionDialog().pack();
        getChartOptionDialog().setResizable(false);
        getChartOptionDialog().setLocationRelativeTo(this);
        getChartOptionDialog().setVisible(true);
    }

    public void freeze() {
        synchronized (freezeIds) {
            List<DataView> views = getClickableViews(false);
            for (DataView view : views) {
                DataView copy = duplicateDataView(view);
                copy.setClickable(false);
                freezeIds.add(copy.getId());
                dataViewList.addDataView(copy);
            }
        }
        refreshVisibilityBoxes(true, null);
        updateFreezePanelAvailability();
    }

    public void clearFreezes() {
        synchronized (freezeIds) {
            for (String id : freezeIds) {
                dataViewList.removeDataView(id);
                plotPropertiesMap.remove(id);
            }
            freezeIds.clear();
        }
        refreshVisibilityBoxes(true, null);
        updateFreezePanelAvailability();
    }

    protected DataView duplicateDataView(DataView original) {
        String freezeString = " - Freeze " + DATE_FORMAT.format(new Date(System.currentTimeMillis()));
        String id = original.getId() + freezeString + "_" + (Math.random() * 1000);
        String displayName = original.getDisplayName() + freezeString;
        DataView copy = ChartUtils.duplicateDataView(original, DataView.class, id);
        if (copy != null) {
            PlotProperties formerProperties = getPlotPropertiesForNewDataView(original.getId(), displayName);
            if (lineColorRotationTool != null) {
                CometeColor freezeColor = ColorTool.getCometeColor(
                        lineColorRotationTool.getNextColorFarEnoughFrom(chartView.getChartBackground()));
                Color color = ColorTool.getColor(freezeColor);
                copy.setColor(color);
                copy.setMarkerColor(color);
                copy.setFillColor(color);
                copy.setErrorColor(color);
            }
            copy.setAxis(formerProperties.getAxisChoice());
            copy.setDisplayName(displayName);
            PlotProperties newProperties = PlotPropertiesTool.readPlotProperties(copy);
            plotPropertiesMap.put(id, newProperties);
        }
        return copy;
    }

    protected DataView generateDataView(String id) {
        return new DataView(id);
    }

    public void displayPrecisionDialog() {
        ChartUtils.displayPrecisionDialog(this);
    }

    public void showExpressionDialog() {
        expressionOption.setAlwaysCommit(false);
        expressionOption.setDataViewId(generateDataView(null).getId());
        expressionOption.setPlotProperties(new PlotProperties());
        expressionOption.setChart(this);
        getExpressionOptionDialog().pack();
        getExpressionOptionDialog().setLocationRelativeTo(this);
        getExpressionOptionDialog().setVisible(true);
    }

    public void clearExpressions() {
        List<String> toRemove = new ArrayList<>();
        for (DataView data : expressionMap.keySet()) {
            toRemove.add(data.getId());
        }
        removeData(toRemove);
        expressionMap.clear();
        if (isShowing()) {
            repaint();
        }
    }

    // Tries to update all the expression DataViews associated with the DataView given in parameter
    protected boolean updateExpressions(DataView view) {
        boolean expressionChanged = false;
        if (!expressionMap.isEmpty()) {
            List<DataView> expressions = new ArrayList<>();
            expressions.addAll(expressionMap.keySet());
            for (int i = 0; i < expressions.size(); i++) {
                DataView expressionView = expressions.get(i);
                Object[] parameters = expressionMap.get(expressionView);
                if (parameters != null) {
                    for (int j = 3; j < parameters.length; j++) {
                        if (parameters[j].equals(view.getId())) {
                            expressionChanged = true;
                            String expression = (String) parameters[1];
                            Boolean x = (Boolean) parameters[2];
                            String[] variables = new String[parameters.length - 3];
                            for (int k = 0; k < variables.length; k++) {
                                variables[k] = (String) parameters[k + 3];
                            }
                            // Commit Change
                            expressionView.reset();
                            applyExpression(expression, expressionView, variables, x.booleanValue());
                            updateExpressions(expressionView);
                            break;
                        }
                    }
                }
            }
        }
        return expressionChanged;
    }

    // Calculates the expression dataView.
    // Can be usefull to update an already existing expression dataview.
    // If you want to do so, you should clean your dataview first.
    protected DataView applyExpression(String expression, DataView expressionDataView, String[] variables, boolean x) {
        DataView resultView = expressionDataView;
        if (variables != null) {
            List<DataView> views = dataViewList.getListCopy(false);
            ExpressionParser parser = new ExpressionParser(variables.length, expression, getExpressionParser());
            parser.setX(x);
            parser.setPrecision(getTimePrecision());
            for (int i = 0; i < variables.length; i++) {
                for (DataView view : views) {
                    if (variables[i].equals(view.getId())) {
                        parser.add(i, view);
                        break;
                    }
                }
            }
            views.clear();
            resultView = parser.buildDataView(expressionDataView, DataView.class);
            parser.clean();
        }
        return resultView;
    }

    /**
     * Call this method to evaluate an expression and have the result represented by a DataView you previously
     * parametered
     * 
     * @param dataViewName the name of your new dataView.
     * 
     * @param expression The String representing your expression. It must not be null.
     *            Example: "cos(x1) + 2*sin(x2)/exp(x3)"
     * @param selectedAxis The axis on which you want to put your DataView. It can be <code>X_AXIS</code>,
     *            <code>Y1_AXIS</code> or <code>Y2_AXIS</code>
     * @param variables A String[] representing the dataview names associated with your variables in order of the
     *            variables index.
     *            Example : You have two variables x1 and x2 in your expression. x1 is associated with the DataView
     *            named "theCurve", and x2 with the DataView named "theBar". Then, variables must be {"theCurve",
     *            "theBar"}.
     * @param x A boolean to know whether your expression looks like "f(x)". If your expression looks like
     *            "f(x1,...,xn)" then set x to <code>false</code>. If it looks like "f(x)", set x to <code>true</code>.
     * @see #X_AXIS
     * @see #Y1_AXIS
     * @see #Y2_AXIS
     */
    protected void addExpressionToChart(DataView expressionDataView, String expression, int selectedAxis,
            String[] variables, boolean x) {
        DataView resultView = applyExpression(expression, expressionDataView, variables, x);
        Object[] parameters = new Object[variables.length + 3];
        parameters[0] = Integer.valueOf(selectedAxis);
        parameters[1] = expression;
        parameters[2] = new Boolean(x);
        for (int i = 0; i < variables.length; i++) {
            parameters[i + 3] = variables[i];
        }
        resultView.setFixed(true);
        expressionMap.put(resultView, parameters);
        resultView.setAxis(-1);
        resultView = dataViewList.addDataViewIfAbsent(resultView);
        PlotPropertiesTool.writePlotProperties(resultView, getDataViewPlotProperties(resultView.getId()));
        resultView.setAxis(selectedAxis);
        getDrawingThreadManager().runInDrawingThread(() -> {
            refreshVisibilityBoxes(true, null);
        });
    }

    protected DataView getDataView(String id, boolean yOnly) {
        return dataViewList.getDataView(id, yOnly);
    }

    protected boolean hasClickableViews() {
        boolean clickable = false;
        List<DataView>[] yViews = dataViewList.getDataViewsByAxis(false, Y1, Y2);
        for (List<DataView> views : yViews) {
            if (!clickable) {
                for (DataView view : views) {
                    if (view.isClickable()) {
                        clickable = true;
                        break;
                    }
                }
            }
            views.clear();
        }
        return clickable;
    }

    protected List<DataView> getClickableViews(boolean sorted) {
        List<DataView> clickableViews = new ArrayList<>();
        List<DataView>[] yViews = dataViewList.getDataViewsByAxis(sorted ? dataViewComparator : null, Y1, Y2);
        for (List<DataView> views : yViews) {
            for (DataView view : views) {
                if (view.isClickable()) {
                    clickableViews.add(view);
                }
            }
            views.clear();
        }
        return clickableViews;
    }

    public void showDataOptions(String item) {
        DataView dataview = getDataView(item, true);
        if (dataview != null) {
            showDataOptions(dataview);
        }
    }

    public void showDataOptions(AbstractDataView view) {
        dataViewOption.setDataViewId(view.getId());
        dataViewOption.setPlotProperties(PlotPropertiesTool.readPlotProperties(view));
        getDataViewOptionDialog().pack();
        getDataViewOptionDialog().setResizable(false);
        getDataViewOptionDialog().setLocationRelativeTo(this);
        getDataViewOptionDialog().setVisible(true);
    }

    public boolean isMenuVisible(String menu) {
        return chartMenu.isMenuVisible(menu);
    }

    public void setMenuVisible(String menu, boolean visible) {
        chartMenu.setMenuVisible(menu, visible);
    }

    public void addMenuSeparator() {
        chartMenu.addSeparator();
    }

    public void addMenuItem(JMenuItem item) {
        if (item != null) {
            if (item instanceof AbstractChartCustomMenu) {
                chartMenu.addCustomMenu((AbstractChartCustomMenu) item);
            } else {
                chartMenu.add(item);
            }
        }
    }

    public void removeMenuItem(JMenuItem item) {
        if (item != null) {
            if (item instanceof AbstractChartCustomMenu) {
                chartMenu.removeCustomMenu((AbstractChartCustomMenu) item);
            } else {
                chartMenu.remove(item);
            }
        }
    }

    protected void updateLastDataFileLocation(String path, boolean saved) {
        transferEventDelegate.changeDataLocation(path, saved);
    }

    public void showTableSingle(String item) {
        displayedIds = new String[] { item };
        showAll = false;
        updateTableContent();
        initTableWindow();
        displayDataTable();
    }

    public void showTableAll(List<DataView> result) {
        if ((result != null) && (!result.isEmpty())) {
            String[] temp = new String[result.size()];
            int i = 0;
            for (DataView view : result) {
                temp[i++] = view.getId();
            }
            displayedIds = temp;
            showAll = false;
            updateTableContent();
            initTableWindow();
            displayDataTable();
        }
    }

    public void showTableAll() {
        displayedIds = null;
        showAll = true;
        updateTableContent();
        initTableWindow();
        displayDataTable();
    }

    public void showShowInSingle(String item) {
        displayedIds = new String[] { item };
        showAll = false;
        updateTableContent();
        initTableWindow();
        displayDataTable();
    }

    public void showShowInAll(List<DataView> result) {
        if ((result != null) && (!result.isEmpty())) {
            String[] temp = new String[result.size()];
            int i = 0;
            for (DataView view : result) {
                temp[i++] = view.getId();
            }
            displayedIds = temp;
            showAll = false;
            updateTableContent();
            initTableWindow();
            displayDataTable();
        }
    }

    public void showShowInAll() {
        displayedIds = null;
        showAll = true;
        updateTableContent();
        initTableWindow();
        displayDataTable();
    }

    public DataView showSingleDataViewDialog(String item) {
        DataView dataView = getDataView(item, true);
        return dataView;
    }

    public void showStatSingle(String item) {
        DataView dataView = getDataView(item, true);
        if (dataView != null) {
            AxisProperties xAxisProperties = getChartProperties().getXAxisProperties();
            ChartUtils.showStatAll(Arrays.asList(dataView), dataView.getDisplayName(),
                    chartView.getAxis(X).getScale().isTimeScale(), xAxisProperties.getLabelFormat(),
                    chartView.getAxis(X).getAttributes().getFullDateFormat(), this);
        }
    }

    public void showStatAll(List<DataView> result) {
        AxisProperties xAxisProperties = getChartProperties().getXAxisProperties();
        ChartUtils.showStatAll(result, "all", chartView.getAxis(X).getScale().isTimeScale(),
                xAxisProperties.getLabelFormat(), chartView.getAxis(X).getAttributes().getFullDateFormat(), this);
    }

    public void showStatAll() {
        showStatAll(getClickableViews(false));
    }

    /**
     * Returns the selection area {@link Color}
     * 
     * @return A {@link Color}
     */
    public Color getSelectionAreaColor() {
        return chartView.getSelectionAreaColor();
    }

    /**
     * Sets the selection area {@link Color}
     * 
     * @param selectionColor The {@link Color} to sets
     */
    public void setSelectionAreaColor(Color selectionColor) {
        chartView.setSelectionAreaColor(selectionColor);
    }

    /**
     * Returns whether this {@link Chart} may use a bounds selection through an area
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isUseSelectionArea() {
        return chartView.isUseSelectionArea();
    }

    /**
     * Sets whether this {@link Chart} can use a bounds selection through an area
     * 
     * @param useSelectionArea Whether this {@link Chart} can use a bounds selection through an area
     */
    public void setUseSelectionArea(boolean useSelectionArea) {
        chartView.setUseSelectionArea(useSelectionArea);
    }

    /**
     * Returns the selected coordinates
     * 
     * @return A <code>double[]</code>: <code>null</code> or <code>{minX, maxX, minY1, maxY1, minY2, maxY2}</code>
     * @see #selectCoordinates(double, double, double, double, double, double)
     * @see #clearSelectedCoordinates()
     */
    public double[] getSelectedCoordinates() {
        return chartView.getSelectedCoordinates();
    }

    /**
     * Changes the selected coordinates
     * 
     * @param minX x minimum
     * @param maxX x maximum
     * @param minY1 y1 minimum
     * @param maxY1 y1 maximum
     * @param minY2 y2 minimum
     * @param maxY2 y2 maximum
     */
    public void selectCoordinates(double minX, double maxX, double minY1, double maxY1, double minY2, double maxY2) {
        chartView.selectCoordinates(minX, maxX, minY1, maxY1, minY2, maxY2);
    }

    /**
     * Clears selected coordinates
     */
    public void clearSelectedCoordinates() {
        chartView.clearSelectedCoordinates();
    }

    /**
     * Appends, in a {@link StringBuilder}, a configuration string that can be written into a file and is compatible
     * with {@link CfFileReader}.
     * 
     * @param builder. The {@link StringBuilder}.
     * @return The {@link StringBuilder} to which the string was appended: <code>builder</code> if it was not
     *         <code>null</code> or a new one if <code>builder</code> was <code>null</code>.
     */
    public StringBuilder appendConfiguration(StringBuilder builder) {
        StringBuilder result = builder == null ? new StringBuilder() : builder;
        result.append(GRAPH_TITLE).append(':').append('\'').append(getHeader()).append('\'')
                .append(ObjectUtils.NEW_LINE);
        result.append(LABEL_VISIBLE).append(':').append(isLegendVisible()).append(ObjectUtils.NEW_LINE);
        result.append(LABEL_PLACEMENT).append(':').append(getLabelPlacement()).append(ObjectUtils.NEW_LINE);
        result.append(LEGEND_PROPORTION).append(':').append(getLegendProportion()).append(ObjectUtils.NEW_LINE);
        result.append(LABEL_FONT).append(':').append(OFormat.font(chartLegend.getFont())).append(ObjectUtils.NEW_LINE);
        result.append(GRAPH_BACKGROUND).append(':').append(OFormat.color(chartView.getBackground()))
                .append(ObjectUtils.NEW_LINE);
        result.append(CHART_BACKGROUND).append(':').append(OFormat.color(chartView.getChartBackground()))
                .append(ObjectUtils.NEW_LINE);
        result.append(TITLE_FONT).append(':').append(OFormat.font(chartView.getFont())).append(ObjectUtils.NEW_LINE);
        result.append(DISPLAY_DURATION).append(':').append(getDisplayDuration()).append(ObjectUtils.NEW_LINE);
        result.append(PRECISION).append(':').append(getTimePrecision()).append(ObjectUtils.NEW_LINE);
        if (this.isMathExpressionEnabled()) {
            result.append(EXPRESSIONS).append(':').append(expressionMap.size()).append(ObjectUtils.NEW_LINE);
            int i = 0;
            for (Entry<DataView, Object[]> entry : expressionMap.entrySet()) {
                DataView keyView = entry.getKey();
                Object[] expressionData = entry.getValue();
                result.append(EXPRESSION_PREFIX).append(i).append(EXPRESSION_SUFFIX).append(':').append('\'')
                        .append(((String) expressionData[1])).append('\'').append(ObjectUtils.NEW_LINE);
                result.append(EXPRESSION_PREFIX).append(i).append(IS_X).append(':')
                        .append(((Boolean) expressionData[2]).booleanValue()).append(ObjectUtils.NEW_LINE);
                result.append(EXPRESSION_PREFIX).append(i).append(VARIABLES).append(':')
                        .append((expressionData.length - 3)).append(ObjectUtils.NEW_LINE);
                for (int j = 0; j < expressionData.length - 3; j++) {
                    result.append(EXPRESSION_PREFIX).append(i).append(VARIABLE).append(j).append(':').append('\'')
                            .append(((String) expressionData[j + 3])).append('\'').append(ObjectUtils.NEW_LINE);
                }
                keyView.appendConfiguration(result, EXPRESSION_PREFIX + i);
                i++;
            }
        }
        chartView.appendSamplingConfiguration(result);
        chartView.appendAxisConfiguration(result, X_PREFIX, X);
        chartView.appendAxisConfiguration(result, Y1_PREFIX, Y1);
        chartView.appendAxisConfiguration(result, Y2_PREFIX, Y2);
        List<DataView> views = dataViewList.getListCopy(dataViewComparator);
        if (!views.isEmpty()) {
            result.append(DV_NUMBER).append(':').append(views.size()).append(ObjectUtils.NEW_LINE);
            for (int i = 0; i < views.size(); i++) {
                views.get(i).appendConfiguration(result, DATAVIEW_PREFIX + Integer.toString(i));
            }
        }
        return result;
    }

    /**
     * Build a configuration string that can be written into a file and is compatible with {@link CfFileReader}.
     * 
     * @return A string containing param.
     */
    public String getConfiguration() {
        return appendConfiguration(null).toString();
    }

    public void applyConfiguration(CfFileReader f) {
        List<String> p;
        // General settings
        p = f.getParam(GRAPH_TITLE);
        if (p != null) {
            setHeader(OFormat.getName(p.get(0)));
        }
        // if (p != null) {
        // setHeader(OFormat.getName(p.get(0)));
        // }
        p = f.getParam(LABEL_VISIBLE);
        if (p != null) {
            setLegendVisible(OFormat.getBoolean(p.get(0)));
        }
        p = f.getParam(LABEL_FONT);
        if (p != null) {
            setLegendFont(OFormat.getFont(p), false);
        }
        p = f.getParam(LABEL_PLACEMENT);
        if (p != null) {
            setLabelPlacement(OFormat.getInt(p.get(0)));
        }
        p = f.getParam(LEGEND_PROPORTION);
        if (p != null) {
            setLegendProportion(OFormat.getDouble(p.get(0)));
        }
        p = f.getParam(GRAPH_BACKGROUND);
        if (p != null) {
            Color bg = OFormat.getColor(p);
            setGlobalBackground(bg);
        }
        p = f.getParam(CHART_BACKGROUND);
        if (p != null) {
            chartView.setChartBackground(OFormat.getColor(p));
        }
        p = f.getParam(TITLE_FONT);
        if (p != null) {
            chartView.setFont(OFormat.getFont(p));
        }
        p = f.getParam(DISPLAY_DURATION);
        if (p != null) {
            setDisplayDuration(OFormat.getDouble(p.get(0)));
        }
        p = f.getParam(PRECISION);
        if (p != null) {
            setTimePrecision(OFormat.getInt(p.get(0)));
        }
        if (this.isMathExpressionEnabled()) {
            // Expression settings
            p = f.getParam(EXPRESSIONS);
            int expressionSize = 0;
            if (p != null) {
                expressionSize = OFormat.getInt(p.get(0));
            }
            for (int i = 0; i < expressionSize; i++) {
                String id;
                p = f.getParam(EXPRESSION_PREFIX + i + AbstractDataView.NAME);
                if (p == null) {
                    id = null;
                } else {
                    id = p.get(0);
                }
                DataView expressionView = generateDataView(id);
                expressionView.setXDataSorted(isDataViewsSortedOnX());
                expressionView.applyConfiguration(EXPRESSION_PREFIX + i, f);
                int axis = -1;
                p = f.getParam(EXPRESSION_PREFIX + i + AbstractDataView.AXIS);
                if (p != null) {
                    axis = OFormat.getInt(p.get(0));
                }

                String expression = ObjectUtils.EMPTY_STRING;
                p = f.getParam(EXPRESSION_PREFIX + i + EXPRESSION_SUFFIX);
                if (p != null) {
                    expression = p.get(0);
                }

                boolean x_abs = false;
                p = f.getParam(EXPRESSION_PREFIX + i + IS_X);
                if (p != null) {
                    x_abs = OFormat.getBoolean(p.get(0));
                }

                int variablesCount = 0;
                p = f.getParam(EXPRESSION_PREFIX + i + VARIABLES);
                if (p != null) {
                    variablesCount = OFormat.getInt(p.get(0));
                }

                String[] variables = new String[variablesCount];
                for (int j = 0; j < variablesCount; j++) {
                    String variable = ObjectUtils.EMPTY_STRING;
                    p = f.getParam(EXPRESSION_PREFIX + i + VARIABLE + j);
                    if (p != null) {
                        variable = p.get(0);
                    }
                    variables[j] = new String(variable);
                    variable = null;
                }

                if (axis != -1) {
                    addExpressionToChart(expressionView, expression, axis, variables, x_abs);
                }
            }

            // sampling configuration
            chartView.applySamplingConfiguration(f);

            // axis configuration
            chartView.applyAxisConfiguration(X_PREFIX, X, f);
            chartView.applyAxisConfiguration(Y1_PREFIX, Y1, f);
            chartView.applyAxisConfiguration(Y2_PREFIX, Y2, f);

            // look down the views: set on the correct axis with correct
            // configuration
            p = f.getParam(DV_NUMBER);
            if (p != null) {
                String prefix;
                DataView currentView = null;
                int count = OFormat.getInt(p.get(0));
                for (int i = 0; i < count; i++) {
                    prefix = DATAVIEW_PREFIX + Integer.toString(i);
                    p = f.getParam(prefix + AbstractDataView.NAME);
                    if (p != null) {
                        String id = p.get(0);
                        currentView = dataViewList.getDataView(id, false);
                        if (currentView != null) {
                            currentView.applyConfiguration(prefix, f);
                            PlotProperties properties = PlotPropertiesTool.readPlotProperties(currentView);
                            plotPropertiesMap.put(currentView.getId(), properties);
                        }
                    }
                }
            }
            // measure items because axis lengths might have changed due to titles
            refresh(true);
        }
    }

    public void loadSettings() {
        JFileChooser chooser = new JFileChooser(settingsDirectory);
        chooser.setDialogTitle(MENU_LOAD_SETTINGS);
        chooser.addChoosableFileFilter(settingsFileFilter);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            String path = chooser.getSelectedFile().getAbsolutePath();
            CfFileReader r = new CfFileReader();
            r.setWhiteSpaceAsSeparator(false);
            if (r.readFile(path)) {
                settingsDirectory = chooser.getCurrentDirectory().getAbsolutePath();
                applyConfiguration(r);
            } else {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).info("Failed to read settings from " + path);
            }
        }
    }

    public void saveSettings() {
        JFileChooser chooser = new JFileChooser(settingsDirectory);
        chooser.setDialogTitle(MENU_SAVE_SETTINGS);
        chooser.addChoosableFileFilter(settingsFileFilter);
        if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            int ok = JOptionPane.YES_OPTION;
            File selected = chooser.getSelectedFile();
            if (chooser.getFileFilter() instanceof ExtensionFileFilter) {
                ExtensionFileFilter filter = (ExtensionFileFilter) chooser.getFileFilter();
                if (!ObjectUtils.sameObject(filter.getExtension(), FileUtils.getExtension(selected))) {
                    selected = new File(selected.getAbsolutePath() + ChartUtils.DOT + filter.getExtension());
                    chooser.setSelectedFile(selected);
                }
            }
            if (selected.exists()) {
                ok = JOptionPane.showConfirmDialog(this, selected.getName() + " exists. Overwrite?",
                        "Confirm overwrite", JOptionPane.YES_NO_OPTION);
            }
            if (ok == JOptionPane.YES_OPTION) {
                try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(selected),
                        StandardCharsets.UTF_8)) {
                    writer.write(getConfiguration());
                    writer.flush();
                    settingsDirectory = chooser.getCurrentDirectory().getAbsolutePath();
                } catch (Exception e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .error("Failed to write settings to file " + selected.getAbsolutePath(), e);
                }
            }
        }
    }

    // Fire ChartActionEvent to all registered ChartActionListener
    public boolean fireGetActionState(String name, EventListenerList eventListenerList) {
        ChartActionListener[] list = (eventListenerList.getListeners(ChartActionListener.class));
        ChartActionEvent w = new ChartActionEvent(this, name);
        boolean ret = true;
        for (ChartActionListener element : list) {
            ret = element.getActionState(w) && ret;
        }
        return ret;
    }

    public DataViewList getDataViewList() {
        return dataViewList;
    }

    public ChartView getChartView() {
        return chartView;
    }

    public ChartLegend getChartLegend() {
        return chartLegend;
    }

    protected Color getNextColor() {
        return lineColorRotationTool.getNextColorFarEnoughFrom(chartView.getChartBackground());
    }

    public void setChartMainBackground(final Color c) {
        chartView.setBackground(c);
        chartLegend.setBackground(c);
        chartLegendScrollPane.setBackground(c);
        chartLegendScrollPane.getViewport().setBackground(c);
        chartLegendScrollPane.getHorizontalScrollBar().setBackground(c);
        chartLegendScrollPane.getVerticalScrollBar().setBackground(c);
        chartLegendScrollPane.setMinimumSize(new Dimension(0, 0));
        chartLegendScrollPane.setPreferredSize(new Dimension(0, 0));
        chartPanel.setBackground(c);
        if (chartSplitPane != null) {
            chartSplitPane.setBackground(c);
            ((ColoredSplitPaneUI) chartSplitPane.getUI()).setDividerColor(c);
        }
    }

    public void setManagementPanelBackground(final Color c) {
        managementPanel.setBackground(c);
    }

    public void setFreezeBackground(final Color c) {
        freezePanel.setBackground(c);
    }

    protected void setGlobalBackground(Color bg) {
        // apply background to children
        setBackground(bg);
        setManagementPanelBackground(bg);
        setFreezeBackground(bg);
        setChartMainBackground(bg);
    }

    protected void mayUpdateTableContent() {
        Window dataWindow = dataTableWindow;
        if ((dataWindow != null) && (dataWindow.isVisible())) {
            updateTableContent();
        }
    }

    protected void cleanViews(Collection<DataView>[] toClean) {
        if (toClean != null) {
            for (Collection<DataView> tmp : toClean) {
                if (tmp != null) {
                    try {
                        tmp.clear();
                    } catch (ConcurrentModificationException e) {
                        // This is a thread safety problem: lists are read while clearing
                        LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).trace("Could not clear views", e);
                    }
                }
            }
        }
    }

    protected void addData(Map<String, Object> dataToAdd, boolean forceGlobalRepaint, boolean updateTable) {
        boolean shouldRepaint = forceGlobalRepaint;
        final List<DataView>[] dataViews;
        if ((dataToAdd != null) && (!dataToAdd.isEmpty())) {
            shouldRepaint = shouldRepaint || dataViewList.isEmpty();
            List<DataView> y1Views = new ArrayList<>(dataToAdd.size());
            List<DataView> y2Views = new ArrayList<>(dataToAdd.size());
            List<DataView> xViews = new ArrayList<>(1);
            List<DataView> hiddenViews = new ArrayList<>(dataToAdd.size());
            @SuppressWarnings("unchecked")
            List<DataView>[] views = new List[] { y1Views, y2Views, xViews, hiddenViews };
            for (Entry<String, Object> entry : dataToAdd.entrySet()) {
                DataView view = addSingleData(entry.getKey(), entry.getValue());
                if (view == null) {
                    shouldRepaint = true;
                } else {
                    int axis = view.getAxis().intValue();
                    switch (axis) {
                        case Y1:
                            y1Views.add(view);
                            break;
                        case Y2:
                            y2Views.add(view);
                            break;
                        case X:
                            xViews.add(view);
                            break;
                        default:
                            hiddenViews.add(view);
                            break;
                    }
                }
            }
            if (shouldRepaint) {
                xViews.clear();
                y1Views.clear();
                y2Views.clear();
                hiddenViews.clear();
                dataViews = null;
            } else {
                dataViews = views;
            }
        } else {
            dataViews = null;
        }

        // refresh visible content only when not showing (JAVAAPI-621)
        if (shouldRepaint) {
            shouldMeasureItems = Boolean.TRUE;
        } else if (shouldMeasureItems == null) {
            shouldMeasureItems = Boolean.FALSE;
        }
        shouldMeasureItems = Boolean.valueOf(shouldRepaint);
        Collection<DataView>[] tmpViews = updatedViews;
        updatedViews = dataViews;
        cleanViews(tmpViews);
        dataComponentDelegate.setDataChanged(this, true);

        // dataTableWindow might be visible even when this Chart is not showing
        getDrawingThreadManager().runInDrawingThread(() -> {
            if (updateTable) {
                mayUpdateTableContent();
            }
        });
        if (isNotifyListenersOnDataAdded()) {
            fireDataChanged(dataToAdd);
        }
    }

    protected DataView addSingleData(String id, Object value) {
        DataView view = null;
        if ((id != null) && (!id.trim().isEmpty())) {
            if (value == null) {
                removeSingleView(id);
            } else {
                boolean globalRepaint = true;
                data.put(id, value);
                view = getDataView(id, false);
                if (view == null) {
                    globalRepaint = false;
                    view = generateDataView(id);
                    view.setXDataSorted(isDataViewsSortedOnX());
                    PlotPropertiesTool.writePlotProperties(view,
                            getPlotPropertiesForNewDataView(id, view.getDisplayName()));
                    if (autoHide) {
                        view.setAxis(-1);
                    }
                    view = dataViewList.addDataViewIfAbsent(view);
                }
                ChartUtils.updateDataView(view, value);
                if (!globalRepaint) {
                    int axis = view.getAxis().intValue();
                    AbstractAxisView axisView = chartView.getAxis(axis);
                    globalRepaint = needRepaint(view, axisView, false);
                    if ((axis != X) && (!globalRepaint)) {
                        globalRepaint = needRepaint(view, chartView.getAxis(X), true);
                    }
                }
                globalRepaint = updateExpressions(view) || globalRepaint;
                if (globalRepaint) {
                    view = null;
                }
            }
        }
        return view;
    }

    protected boolean needRepaint(DataView view, AbstractAxisView axisView, boolean x) {
        boolean globalRepaint;
        if ((view == null) || (axisView == null)) {
            globalRepaint = false;
        } else {
            AbstractAxisScale scale = axisView.getScale();
            AxisAttributes attributes = axisView.getAttributes();
            if ((scale == null) || (scale.isZoomed()) || (attributes == null) || (!attributes.isAutoScale())) {
                globalRepaint = false;
            } else if (scale.isDefaultScaleBounds()) {
                globalRepaint = true;
            } else {
                double scaleMin = scale.getScaleMinimum();
                double scaleMax = scale.getScaleMaximum();
                double viewMin, viewMax;
                if (x) {
                    if (view.isXDataSorted()) {
                        viewMin = scale.getScaledValue(view.getMinTime());
                        viewMax = scale.getScaledValue(view.getMaxTime());
                    } else {
                        viewMin = scale.getScaledValue(view.getMinXValue());
                        viewMax = scale.getScaledValue(view.getMaxXValue());
                    }
                } else {
                    viewMin = scale.getScaledValue(view.getMinimum());
                    viewMax = scale.getScaledValue(view.getMaximum());
                }
                if ((viewMin < scaleMin) || (viewMax > scaleMax)) {
                    globalRepaint = true;
                } else {
                    globalRepaint = false;
                }
            }
        }
        return globalRepaint;
    }

    /**
     * Returns the {@link PlotProperties} for a newly created {@link DataView}
     * 
     * @param id The {@link DataView} id
     * @param displayName The {@link DataView} display name
     * @return Some {@link PlotProperties}
     */
    protected PlotProperties getPlotPropertiesForNewDataView(String id, String displayName) {
        return PlotPropertiesTool.getInitializedPlotProperties(id, displayName, plotPropertiesMap,
                lineStyleRotationTool, lineWidthRotationTool, markerStyleRotationTool, markerSizeRotationTool,
                fillStyleRotationTool, barWidthRotationTool, viewStyleRotationTool, axisRotationTool,
                lineColorRotationTool, barColorRotationTool, markerColorRotationTool);
    }

    /**
     * Applies loaded data to this {@link Chart}
     * 
     * @param chartData The loaded data
     * @param wasEnabled Whether data loading was enabled before calling {@link #loadDataFile(String)}
     */
    protected final void addChartData(ChartData<DataView> chartData, boolean wasEnabled) {
        if (chartData == null) {
            threadedAddDataViews(null, 0, wasEnabled);
        } else {
            updateLastDataFileLocation(chartData.getFileLocation(), false);
            threadedAddDataViews(chartData.getData(), 0, wasEnabled);
        }
    }

    protected void finalActionOnceDataLoaded() {
        // nothing to do by default
        // This method is useful for CONTROLGUI-379
    }

    /**
     * Uses {@link SwingWorker}s to add previously loaded {@link DataView}s
     * 
     * @param views The {@link DataView}s, loaded from a file
     * @param index The current {@link DataView} index to add
     * @param wasEnabled Whether data loading was enabled before calling {@link #loadDataFile(String)}
     */
    protected final void threadedAddDataViews(final DataView[] views, final int index, final boolean wasEnabled) {
        if ((views == null) || (index < 0) || (index >= views.length)) {
            // refresh visible content only when not showing (JAVAAPI-621)
            shouldRevalidate = Boolean.TRUE;
            shouldMeasureItems = Boolean.TRUE;
            Collection<DataView>[] tmpViews = updatedViews;
            updatedViews = null;
            cleanViews(tmpViews);
            getDrawingThreadManager().runInDrawingThread(() -> {
                chartMenu.setDataFileEnabled(wasEnabled);
            });
            dataComponentDelegate.setDataChanged(this, true);
            finalActionOnceDataLoaded();
        } else {
            SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    DataView view = views[index];
                    PlotProperties properties = getPlotPropertiesForNewDataView(view.getId(), view.getDisplayName());
                    PlotPropertiesTool.writePlotProperties(view, properties);
                    double[][] viewData = view.getDataAsDoubleMatrix();
                    data.put(view.getId(), viewData);
                    dataViewList.addDataView(view);
                    updateExpressions(view);
                    return null;
                }

                @Override
                protected void done() {
                    threadedAddDataViews(views, index + 1, wasEnabled);
                }
            };
            worker.execute();
        }
    }

    protected void initTableWindow() {
        dataTableWindow = ChartUtils.getInitializedTableWindow(dataTableWindow, dataTable, preferDialog, modalDialog,
                this);
    }

    protected void updateTableContent() {
        AbstractDataView[] views;
        if (showAll) {
            List<DataView> tmp = getClickableViews(true);
            views = tmp.toArray(new AbstractDataView[tmp.size()]);
            tmp.clear();
        } else {
            String[] ids = displayedIds;
            views = new AbstractDataView[ids.length];
            for (int i = 0; i < ids.length; i++) {
                views[i] = getDataView(ids[i], false);
            }
        }
        updateTableContent(views);
    }

    protected void updateTableContent(AbstractDataView... views) {
        if ((views != null) && (views.length > 0)) {
            AbstractDataView xView = dataViewList.getXView();
            ChartUtils.updateTableContent(dataTable, timePrecision, chartView.getAxis(X).getScale().isTimeScale(),
                    xView != null, getIndexColumnName(), getDateColumnName(), views);
        }
    }

    protected void displayDataTable() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ChartUtils.centerTableWindow(dataTableWindow, dataTable);
                dataTableWindow.setVisible(true);
            }
        });
    }

    /**
     * Cleans the panel that contains the chart and its legend
     */
    protected void cleanChartPanel() {
        chartPanel.removeAll();
        chartPanel.repaint();
    }

    /**
     * Reinitializes the JSplitPane used to separate chart from its legend
     */
    protected void initChartSplitPane() {
        JSplitPane splitPane = new JSplitPane();
        splitPane.setContinuousLayout(true);
        splitPane.setUI(new ColoredSplitPaneUI());
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerSize(8);
        splitPane.setBackground(chartPanel.getBackground());
        ((ColoredSplitPaneUI) splitPane.getUI()).setDividerColor(chartPanel.getBackground());
        splitPane.setOpaque(isOpaque());
        splitPane.addPropertyChangeListener(this);
        chartSplitPane = splitPane;
    }

    /**
     * Does anything necessary to update chart's legend visibility and placement
     */
    protected void updateChartLegend() {
        cleanChartPanel();
        if (chartSplitPane != null) {
            // clean former JSplitPane
            chartSplitPane.removeAll();
            chartSplitPane.removePropertyChangeListener(this);
            chartSplitPane = null;
        }
        initChartSplitPane();
        if (isLegendVisible()) {
            final int placement = getLabelPlacement();
            final double placeForChartH = 0.5, placeForLegendH = 1 - placeForChartH;
            final double placeForChartV = 0.8, placeForLegendV = 1 - placeForChartV;
            chartSplitPane.removePropertyChangeListener(this);
            double legendProportion = this.legendProportion;
            switch (placement) {
                case LABEL_UP:
                    chartSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
                    chartSplitPane.setTopComponent(chartLegendScrollPane);
                    chartSplitPane.setBottomComponent(chartView);
                    if (Double.isNaN(legendProportion)) {
                        chartSplitPane.setDividerLocation(placeForLegendV);
                        chartSplitPane.setResizeWeight(placeForLegendV);
                        this.legendProportion = placeForLegendV;
                    } else {
                        chartSplitPane.setDividerLocation(legendProportion);
                        chartSplitPane.setResizeWeight(legendProportion);
                    }
                    break;
                case LABEL_LEFT:
                    chartSplitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
                    chartSplitPane.setLeftComponent(chartLegendScrollPane);
                    chartSplitPane.setRightComponent(chartView);
                    if (Double.isNaN(legendProportion)) {
                        chartSplitPane.setDividerLocation(placeForLegendH);
                        chartSplitPane.setResizeWeight(placeForLegendH);
                        this.legendProportion = placeForLegendH;
                    } else {
                        chartSplitPane.setDividerLocation(legendProportion);
                        chartSplitPane.setResizeWeight(legendProportion);
                    }
                    break;
                case LABEL_RIGHT:
                    chartSplitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
                    chartSplitPane.setLeftComponent(chartView);
                    chartSplitPane.setRightComponent(chartLegendScrollPane);
                    if (Double.isNaN(legendProportion)) {
                        chartSplitPane.setDividerLocation(placeForChartH);
                        chartSplitPane.setResizeWeight(placeForChartH);
                        this.legendProportion = placeForLegendH;
                    } else {
                        chartSplitPane.setDividerLocation(1 - legendProportion);
                        chartSplitPane.setResizeWeight(1 - legendProportion);
                    }
                    break;
                case LABEL_ROW:
                case LABEL_DOWN:
                    chartSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
                    chartSplitPane.setTopComponent(chartView);
                    chartSplitPane.setBottomComponent(chartLegendScrollPane);
                    if (Double.isNaN(legendProportion)) {
                        chartSplitPane.setDividerLocation(placeForChartV);
                        chartSplitPane.setResizeWeight(placeForChartV);
                        this.legendProportion = placeForLegendV;
                    } else {
                        chartSplitPane.setDividerLocation(1 - legendProportion);
                        chartSplitPane.setResizeWeight(1 - legendProportion);
                    }
                    break;
            } // end switch (placement)
            chartSplitPane.addPropertyChangeListener(this);
        } // end if (isLegendVisible())
        updateChartLegendVisibility();
        refresh(false);
    }

    /**
     * Sets chart legend visible or not, depending on chart's parameters
     */
    protected void updateChartLegendVisibility() {
        if (!isChartVisible()) {
            if (isLegendVisible()) {
                chartPanel.add(chartSplitPane, BorderLayout.CENTER);
            } else {
                chartPanel.add(chartView, BorderLayout.CENTER);
            }
        }
    }

    protected void refreshVisibilityBoxes(final boolean measureItems, final List<DataView>[] views) {
        if (isManagementPanelVisible()) {
            synchronized (visibilityLock) {
                if (views == null) {
                    List<DataView>[] viewsToUpdate = dataViewList.getDataViewsByBlock(dataViewComparator, -1, Y1, Y2);
                    updateVisibilityBox(visibleViews, extractIdsAndNames(viewsToUpdate[1]));
                    updateVisibilityBox(hiddenViews, extractIdsAndNames(viewsToUpdate[0]));
                } else {
                    List<DataView> yViews = new ArrayList<>(views[0].size() + views[1].size());
                    yViews.addAll(views[0]);
                    yViews.addAll(views[1]);
                    updateVisibilityBox(visibleViews, extractIdsAndNames(yViews));
                    updateVisibilityBox(hiddenViews, extractIdsAndNames(views[3]));
                }
                checkButtonsAvailability();
            }
        }
        refresh(measureItems, views);
    }

    protected void checkButtonsAvailability() {
        if (hideDataViewButton.isEnabled() != visibleViews.isEnabled()) {
            hideDataViewButton.setEnabled(visibleViews.isEnabled());
        }
        if (showDataViewButton.isEnabled() != hiddenViews.isEnabled()) {
            showDataViewButton.setEnabled(hiddenViews.isEnabled());
        }
    }

    protected void clearVisibilityBoxes() {
        synchronized (visibilityLock) {
            if (visibleViews.getValueList() != null) {
                visibleViews.setValueList((Object[]) null);
            }
            if (visibleViews.getDisplayedList() != null) {
                visibleViews.setDisplayedList((String[]) null);
            }
            if (hiddenViews.getValueList() != null) {
                hiddenViews.setValueList((Object[]) null);
            }
            if (hiddenViews.getDisplayedList() != null) {
                hiddenViews.setDisplayedList((String[]) null);
            }
        }
    }

    protected String[][] extractIdsAndNames(List<DataView> views) {
        Collections.sort(views, dataViewComparator);
        String[][] result = new String[2][views.size()];
        int index = 0;
        for (DataView view : views) {
            result[0][index] = view.getId();
            result[1][index++] = view.getDisplayName();
        }
        return result;
    }

    protected void updateVisibilityBox(ComboBox box, String[][] idsAndNames) {
        box.setValueList((Object[]) idsAndNames[0]);
        box.setDisplayedList(idsAndNames[1]);
        int count = box.getItemCount();
        if (count > 0) {
            if (isAutoDeactivateDataViewComboBoxes() && (!box.isEnabled())) {
                box.setEnabled(true);
            }
            if (isAutoFillDataViewComboBoxes() && (box.getSelectedIndex() < 0)) {
                box.setSelectedIndex(0);
            }
        } else if (isAutoDeactivateDataViewComboBoxes()) {
            box.setEnabled(false);
        }
    }

    /**
     * Refreshes components and revalidates chart's legend when necessary
     * 
     * @param measureItems
     *            Whether to force {@link ChartView} to measure graph items
     */
    public void refresh(final boolean measureItems) {
        refresh(measureItems, null);
    }

    protected void refresh(final boolean measureItems, final List<DataView>[] views) {
        Runnable refreshRunnable = new Runnable() {
            @Override
            public void run() {
                doRefresh(measureItems, views);
            }
        };
        if (isRefreshLater()) {
            SwingUtilities.invokeLater(refreshRunnable);
        } else {
            refreshRunnable.run();
        }
    }

    protected void doRefresh(boolean measureItems, List<DataView>[] views) {
        boolean emptyViews = (views == null);
        if (measureItems && emptyViews) {
            chartView.measureGraphItems();
        }
        if (isChartVisible()) {
            if (isLegendVisible()) {
                chartLegend.revalidate();
                if (emptyViews && (chartSplitPane != null)) {
                    chartSplitPane.revalidate();
                }
            }
        }
        if (emptyViews) {
            chartPanel.revalidate();
            if (isShowing()) {
                repaint();
            }
        } else {
            if (chartLegend.isShowing()) {
                chartLegend.repaint();
            }
            try {
                chartView.paintDataViews(views);
            } catch (ConcurrentModificationException e) {
                // This is a thread safety problem: lists are read while clearing
                // We can ignore this because this means data is being updated and views will be painted again later.
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).trace("Could not paint views", e);
            }
        }
        cleanViews(views);
    }

    protected boolean isChartVisible() {
        return (chartPanel.getComponentCount() > 0);
    }

    public void setDataViewVisible(String id, boolean visible) {
        setDataViewVisible(id, visible, -1);
    }

    protected void setDataViewVisible(String id, boolean visible, int axis) {
        if ((id != null) && (!id.trim().isEmpty())) {
            DataView dataView = getDataView(id, false);
            if (dataView != null) {
                if (visible) {
                    if (axis < 0) {
                        dataView.setAxis(
                                getPlotPropertiesForNewDataView(id, dataView.getDisplayName()).getAxisChoice());
                    } else {
                        dataView.setAxis(axis);
                    }
                } else {
                    dataView.setAxis(-1);
                }
                refreshVisibilityBoxes(true, null);
                updateFreezePanelAvailability();
            }
        }
    }

    public void showDataViewDialog() {
        List<DataView> result = showDialog("Dataview selection", true);
        if (result.size() == 1) {
            showDataOptions(result.get(0));
        }
        result.clear();
    }

    public void showSaveFileDialog() {
        int ok = JOptionPane.YES_OPTION;
        JFileChooser chooser = new JFileChooser(transferEventDelegate.getDataDirectory());
        chooser.addChoosableFileFilter(new ExtensionFileFilter("txt", "Text files"));
        chooser.addChoosableFileFilter(new ExtensionFileFilter(CsvLogFileWriter.CSV));
        chooser.setDialogTitle("Save Graph Data (Text file with TAB separated fields)");
        if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            if (f != null) {
                FileFilter filter = chooser.getFileFilter();
                if (filter instanceof ExtensionFileFilter) {
                    ExtensionFileFilter ext = (ExtensionFileFilter) filter;
                    if (!ext.getExtension().equalsIgnoreCase(FileUtils.getExtension(f))) {
                        f = new File(f.getAbsolutePath() + '.' + ext.getExtension());
                    }
                }
                if (FileUtils.getExtension(f) == null) {
                    f = new File(f.getAbsolutePath() + ".txt");
                }
                if (f.exists()) {
                    ok = JOptionPane.showConfirmDialog(this, "Do you want to overwrite " + f.getName() + " ?",
                            "Confirm overwrite", JOptionPane.YES_NO_OPTION);
                }
                if (ok == JOptionPane.YES_OPTION) {
                    saveDataFile(f.getAbsolutePath());
                }
            }
        }
    }

    public void showLoadFileDialog() {
        JFileChooser chooser = new JFileChooser(transferEventDelegate.getDataDirectory());
        chooser.addChoosableFileFilter(new ExtensionFileFilter("txt", "Text files"));
        chooser.addChoosableFileFilter(new ExtensionFileFilter(CsvLogFileWriter.CSV));
        chooser.setDialogTitle("Load Graph Data (Text file with TAB separated fields)");
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            if (f != null) {
                loadDataFile(f.getAbsolutePath());
            }
        }
    }

    public void showTableDialog() {
        List<DataView> result = showDialog("Table selection", false);
        if (result.size() == 1) {
            showTableSingle(result.get(0).getId());
        } else if (result.size() > 1) {
            showTableAll(result);
        }
        result.clear();
    }

    public List<DataView> showDataViewSelectionDialog(String title) {
        return showDialog(title, false);
    }

    public void showStatsDialog() {
        List<DataView> result = showDialog("Stats selection", false);
        if (result.size() == 1) {
            showStatSingle(result.get(0).getId());
        } else if (result.size() > 1) {
            showStatAll(result);
        }
        result.clear();
    }

    private List<DataView> showDialog(String dialogTitle, boolean all) {
        DialogList<DataView> dialog = new DialogList<>(WindowSwingUtils.getWindowForComponent(this), dialogTitle, true,
                all ? chartMenu.getYViews() : chartMenu.getClickableViews(), false, DataView.class);
        return dialog.showOptionDialog(WindowSwingUtils.getWindowForComponent(this));
    }

    /**
     * Prints out this chart.
     */
    public void print() {
        try {
            EasyPrinter.print(this);
        } catch (PrintException e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to print chart", e);
        }
    }

    /**
     * Saves a snapshot of this {@link Chart}
     */
    public void saveSnapshot() {
        snapshotEventDelegate.changeSnapshotLocation(SnapshotUtil.snapshot(this,
                snapshotEventDelegate.getSnapshotLocation(), getPreferredSnapshotExtension(), true));
    }

    /**
     * Returns whether {@link AbstractDataView}s should be considered as sorted on X
     * 
     * @return A <code>boolean</code> value;
     */
    public boolean isDataViewsSortedOnX() {
        return dataViewsSortedOnX;
    }

    /**
     * Sets whether {@link AbstractDataView}s should be considered as sorted on X
     * 
     * @param dataViewsSortedOnX Whether {@link AbstractDataView}s should be considered as sorted on X
     * @param applyOnExistingViews Whether to apply this rule on already existing views
     */
    public void setDataViewsSortedOnX(boolean dataViewsSortedOnX, boolean applyOnExistingViews) {
        this.dataViewsSortedOnX = dataViewsSortedOnX;
        if (applyOnExistingViews) {
            for (AbstractDataView dataView : dataViewList.getListCopy(false)) {
                dataView.setXDataSorted(dataViewsSortedOnX);
            }
            if (isShowing()) {
                repaint();
            }
        }
    }

    /**
     * Sets whether {@link AbstractDataView}s should be considered as sorted on X
     * 
     * @param dataViewsSortedOnX whether {@link AbstractDataView}s should be considered as sorted on X
     */
    public void setDataViewsSortedOnX(boolean dataViewsSortedOnX) {
        setDataViewsSortedOnX(dataViewsSortedOnX, true);
    }

    /**
     * Highlights or unhighlights many views
     * 
     * @param highlighted Whether to highlight the views
     * @param repaint Whether to repaint once done
     * @param ids The id of the views to highlight/unhighlight
     */
    public void setDataViewsHighlighted(boolean highlighted, boolean repaint, String... ids) {
        setDataViewsHighlighted(highlighted, repaint, false, ids);
    }

    /**
     * Highlights or unhighlights many views
     * 
     * @param highlighted Whether to highlight the views
     * @param repaint Whether to repaint once done
     * @param exclusive Whether the previous highlighted curves are unhighlighted or not
     * @param ids The id of the views to highlight/unhighlight
     */
    public void setDataViewsHighlighted(boolean highlighted, boolean repaint, boolean exclusive, String... ids) {
        if (ids != null) {
            Map<String, Object> highlightMap = null;
            if (exclusive) {
                highlightMap = unhighlightAllCurves(false);
            }
            if (highlightMap == null) {
                highlightMap = new LinkedHashMap<>();
            }
            Boolean value = Boolean.valueOf(highlighted);
            for (String id : ids) {
                DataView dataView = getDataView(id, false);
                if (dataView != null) {
                    dataView.setHighlighted(highlighted);
                }
                highlightMap.put(id, value);
            }
            doFireHighlightChange(highlightMap);
            if (repaint && isShowing()) {
                repaint();
            }
        }
    }

    /**
     * Unhighlights all curves, and returns a highlight {@link Map}
     * 
     * @param fireHighlightEvent Whether to notify listeners for highlight change
     * @return A {@link Map}
     */
    protected Map<String, Object> unhighlightAllCurves(boolean fireHighlightEvent) {
        Map<String, Object> highlightMap = new LinkedHashMap<>();
        if ((dataViewList != null) && !dataViewList.isEmpty()) {
            for (DataView dataView : dataViewList.getListCopy()) {
                if ((dataView != null) && dataView.isHighlighted()) {
                    dataView.setHighlighted(false);
                    highlightMap.put(dataView.getId(), Boolean.FALSE);
                }
            }
            if (fireHighlightEvent) {
                doFireHighlightChange(highlightMap);
            }
        }
        return highlightMap;
    }

    /**
     * Unhighlights all curves
     */
    public void unhighlightAllCurves() {
        unhighlightAllCurves(true);
    }

    /**
     * Sets whether {@link #refresh()} method is done asynchronously on the AWT event dispatching thread.
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isRefreshLater() {
        return refreshLater;
    }

    /**
     * Sets whether {@link #refresh()} method should be done asynchronously on the AWT event dispatching thread.
     * 
     * @param refreshLater Whether {@link #refresh()} method should be done asynchronously on the AWT event dispatching
     *            thread.
     */
    public void setRefreshLater(boolean refreshLater) {
        this.refreshLater = refreshLater;
    }

    /**
     * Returns whether dataview display name is used in output data files
     * 
     * @return A <code>boolean</code> value. <code>false</code> by default;
     */
    public boolean isUseDisplayNameForDataSaving() {
        return useDisplayNameForDataSaving;
    }

    /**
     * Sets whether to use dataview display name in output data files
     * 
     * @param usLabelForDataSaving Whether to use dataview display name in output data files
     */
    public void setUseDisplayNameForDataSaving(boolean usLabelForDataSaving) {
        this.useDisplayNameForDataSaving = usLabelForDataSaving;
    }

    /**
     * Returns whether listeners are notified for data change when some data is added in this chart
     * 
     * @return A <code>boolean</code>
     */
    public boolean isNotifyListenersOnDataAdded() {
        return notifyListenersOnDataAdded;
    }

    /**
     * Sets whether listeners should be notified for data change when some data is added in this chart
     * 
     * @param notifyListenersOnDataAdded Whether listeners should be notified for data change when some data is added in
     *            this chart
     */
    public void setNotifyListenersOnDataAdded(boolean notifyListenersOnDataAdded) {
        this.notifyListenersOnDataAdded = notifyListenersOnDataAdded;
    }

    /**
     * Returns whether the show/hide dataview comboboxes, in management panel, will be automatically deactivated when
     * containing no item.
     * 
     * @return A <code>boolean</code>
     */
    public boolean isAutoDeactivateDataViewComboBoxes() {
        return autoDeactivateDataViewComboBoxes;
    }

    /**
     * Sets whether the show/hide dataview comboboxes, in management panel, should be automatically deactivated when
     * containing no item.
     * 
     * @param autoDeactivateDataViewComboBoxes Whether the show/hide dataview comboboxes, in management panel, should be
     *            automatically deactivated when containing no item.
     */
    public void setAutoDeactivateDataViewComboBoxes(boolean autoDeactivateDataViewComboBoxes) {
        if (this.autoDeactivateDataViewComboBoxes != autoDeactivateDataViewComboBoxes) {
            this.autoDeactivateDataViewComboBoxes = autoDeactivateDataViewComboBoxes;
            refreshVisibilityBoxes(false, null);
        }
    }

    /**
     * Returns whether the show/hide dataview comboboxes, in management panel, will always have a selected item when
     * they are not empty.
     * 
     * @return A <code>boolean</code>
     */
    public boolean isAutoFillDataViewComboBoxes() {
        return autoFillDataViewComboBoxes;
    }

    /**
     * Sets whether the show/hide dataview comboboxes, in management panel, should always have a selected item when they
     * are not empty.
     * 
     * @param autoFillDataViewComboBoxes Whether the show/hide dataview comboboxes, in management panel, should always
     *            have a selected item when they are not empty.
     */
    public void setAutoFillDataViewComboBoxes(boolean autoFillDataViewComboBoxes) {
        if (this.autoFillDataViewComboBoxes != autoFillDataViewComboBoxes) {
            this.autoFillDataViewComboBoxes = autoFillDataViewComboBoxes;
            refreshVisibilityBoxes(false, null);
        }
    }

    public String getPreferredSnapshotExtension() {
        return preferredSnapshotExtension;
    }

    public void setPreferredSnapshotExtension(String preferredSnapshotExtension) {
        this.preferredSnapshotExtension = preferredSnapshotExtension;
    }

    /**
     * Recovers the custom {@link CometeColor}s used with a {@link ColorUtils}
     * 
     * @param colorRotationTool The {@link ColorUtils}
     * @return A {@link CometeColor} array.
     */
    protected CometeColor[] getCustomCometeColors(ColorUtils colorRotationTool) {
        return colorRotationTool == null ? null : ColorTool.getCometeColors(colorRotationTool.getCustomColors());
    }

    /**
     * Creates or updates a {@link ColorUtils} with custom {@link CometeColor}s.
     * 
     * @param colorList The custom {@link CometeColor}s
     * @param savedColorRotationTool The known {@link ColorUtils}
     * @return The initialized/updated {@link ColorUtils}. <code>null</code> If <code>colorList</code> is
     *         <code>null</code>
     */
    protected ColorUtils getColorRotationTool(CometeColor[] colorList, ColorUtils savedColorRotationTool) {
        ColorUtils colorRotationTool = savedColorRotationTool;
        if (colorList == null) {
            colorRotationTool = null;
        } else {
            if (colorRotationTool == null) {
                colorRotationTool = new ColorUtils();
            }
            colorRotationTool.setCustomColors(ColorTool.getColors(colorList));
        }
        return colorRotationTool;
    }

    /**
     * Creates or updates an {@link IntRotationTool} with custom <code>int[]</code>.
     * 
     * @param valueList The custom <code>int[]</code>
     * @param savedRotationTool The known {@link IntRotationTool}
     * @return The initialized/updated {@link IntRotationTool}. <code>null</code> If <code>savedRotationTool</code> is
     *         <code>null</code>
     */
    protected IntRotationTool getIntRotationTool(int[] valueList, IntRotationTool savedRotationTool) {
        IntRotationTool intRotationTool = savedRotationTool;
        if (valueList == null) {
            intRotationTool = null;
        } else {
            if (intRotationTool == null) {
                intRotationTool = new IntRotationTool();
            }
            intRotationTool.setCycle(cyclingCustomMap);
            intRotationTool.setData(valueList);
        }
        return intRotationTool;
    }

    protected void setCyclingCustomMap(IntRotationTool intRotationTool) {
        if (intRotationTool != null) {
            intRotationTool.setCycle(cyclingCustomMap);
        }
    }

    protected int[] getValues(IntRotationTool intRotationTool) {
        return intRotationTool == null ? null : intRotationTool.getData();
    }

    /**
     * Returns whether legend proportion should be considered as the the opposite (1 - proportion) of chartSplitpane's
     * divider location.
     * 
     * @return A <code>boolean</code>.
     */
    protected boolean shouldRevertProportion() {
        return legendPlacement == LABEL_DOWN || legendPlacement == LABEL_ROW || legendPlacement == LABEL_RIGHT;
    }

    /**
     * Returns whether given proportion should be considered as a different legend proportion
     * 
     * @param proportion The proportion to check.
     * @return A <code>boolean</code>.
     */
    protected boolean legendProportionChanged(double proportion) {
        // limit proportion precision to MIN_PROPORTION_DELTA
        return Math.abs(this.legendProportion - proportion) > MIN_PROPORTION_DELTA
                || (Double.isNaN(legendProportion) && !Double.isNaN(proportion))
                || (Double.isNaN(proportion) && !Double.isNaN(legendProportion));
    }

    // ///////// //
    // Listeners //
    // ///////// //

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e != null) {
            if (e.getSource() == showDataViewButton) {
                setDataViewVisible((String) hiddenViews.getSelectedValue(), true,
                        axisSelectionVisible ? axisSelectionCombo.getSelectedIndex() : -1);
            } else if (e.getSource() == hideDataViewButton) {
                setDataViewVisible((String) visibleViews.getSelectedValue(), false);
            } else if (e.getSource() == freezeButton) {
                freeze();
            } else if (e.getSource() == clearFreezesButton) {
                clearFreezes();
            }
        }
    }

    @Override
    public void columnChanged(JTableRowEvent evt) {
        if (evt != null) {
            int columnIndex = evt.getColumnIndex();
            Object[] yPoint = evt.getColumnChanged();
            if (dataTable != null) {
                Object[] xPoint = dataTable.getColumnValues(0);
                String dataArrayName = dataTable.getColumnName(columnIndex);
                if (xPoint.length == yPoint.length) {
                    Map<String, Object> dataMap = new HashMap<>();
                    double[][] value = new double[2][];
                    double[] xValue = new double[xPoint.length];
                    double[] yValue = new double[yPoint.length];
                    for (int i = 0; i < yPoint.length; i++) {
                        xValue[i] = Double.parseDouble(xPoint[i].toString());
                        yValue[i] = Double.parseDouble(yPoint[i].toString());
                    }
                    value[X_INDEX] = xValue;
                    value[Y_INDEX] = yValue;
                    dataMap.put(dataArrayName, value);
                    addData(dataMap, true, false);
                    fireDataChanged(dataMap);
                } // end if (xPoint.length == yPoint.length)
            } // end if (dataTable != null)
        } // end if (evt != null)
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ((evt != null) && (evt.getSource() == chartSplitPane)
                && JSplitPane.DIVIDER_LOCATION_PROPERTY.equals(evt.getPropertyName())) {
            double proportion = SplitPaneUtils.getDividerLocation(chartSplitPane);
            if (proportion < 0) {
                proportion = 0;
            } else if (proportion > 1) {
                proportion = 1;
            }
            if (shouldRevertProportion() && !Double.isNaN(proportion)) {
                proportion = 1 - proportion;
            }
            if (legendProportionChanged(proportion)) {
                this.legendProportion = proportion;
                if (!Double.isNaN(proportion)) {
                    chartSplitPane.setResizeWeight(shouldRevertProportion() ? 1 - proportion : proportion);
                }
                fireConfigurationChanged(null);
            }
        }
    }

    // //////////////// //
    // IDataViewManager //
    // //////////////// //

    @Override
    public Comparator<AbstractDataView> getDataViewComparator() {
        return dataViewComparator;
    }

    @Override
    public void setDataViewComparator(Comparator<AbstractDataView> dataViewComparator) {
        this.dataViewComparator = (dataViewComparator == null ? VIEW_COMPARATOR : dataViewComparator);
        refreshVisibilityBoxes(false, null);
    }

    // //////////// //
    // IChartViewer //
    // //////////// //

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return false;
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        super.setOpaque(isOpaque);
        CometeUtils.setOpaque(isOpaque, chartView, chartPanel, managementPanel, freezePanel, chartLegend,
                chartLegendScrollPane, chartSplitPane, southPanel);
    }

    @Override
    public Map<String, Object> getData() {
        Map<String, Object> copy = new LinkedHashMap<>();
        copy.putAll(data);
        return copy;
    }

    @Override
    public void setData(Map<String, Object> data) {
        List<String> toRemove = new ArrayList<>();
        toRemove.addAll(this.data.keySet());
        if (data != null) {
            toRemove.removeAll(data.keySet());
        }
        this.data.clear();
        for (String id : toRemove) {
            dataViewList.removeDataView(id);
        }
        toRemove.clear();
        addData(data, true, true);
    }

    @Override
    public void addData(Map<String, Object> dataToAdd) {
        addData(dataToAdd, false, true);
    }

    @Override
    public void removeData(Collection<String> idsToRemove) {
        if ((idsToRemove != null) && (!idsToRemove.isEmpty())) {
            boolean removed = false;
            for (String id : idsToRemove) {
                removed = removeSingleView(id) || removed;
            }
            if (removed) {
                // refresh visible content only when not showing (JAVAAPI-621)
                shouldMeasureItems = Boolean.TRUE;
                Collection<DataView>[] tmpViews = updatedViews;
                updatedViews = null;
                cleanViews(tmpViews);
                dataComponentDelegate.setDataChanged(this, true);
            }
        }
    }

    protected boolean removeSingleView(String id) {
        boolean removed = false;
        if (id != null) {
            data.remove(id);
            DataView view = dataViewList.removeDataView(id);
            removed = (view != null);
            if (removed) {
                expressionMap.remove(view);
                view.reset();
            }
            updateExpressions(view);
            if (isCleanDataViewConfigurationOnRemoving()) {
                plotPropertiesMap.remove(id);
            }
        }
        return removed;
    }

    @Override
    public void setCometeBackground(final CometeColor c) {
        setGlobalBackground(ColorTool.getColor(c));
    }

    @Override
    public void setChartCometeBackground(CometeColor c) {
        if (c != null) {
            chartView.setChartBackground(ColorTool.getColor(c));
            fireConfigurationChanged(null);
        }
    }

    @Override
    public CometeColor getChartCometeBackground() {
        return ColorTool.getCometeColor(chartView.getChartBackground());
    }

    @Override
    public void addChartViewerListener(IChartViewerListener listener) {
        chartEventDelegate.addChartViewerListener(listener);
    }

    @Override
    public void removeChartViewerListener(IChartViewerListener listener) {
        chartEventDelegate.removeChartViewerListener(listener);
    }

    public void addChartActionListener(ChartActionListener listener) {
        chartEventDelegate.addChartActionListener(listener);
    }

    public void removeChartActionListener(ChartActionListener listener) {
        chartEventDelegate.removeChartActionListener(listener);
    }

    @Override
    public void addSnapshotListener(ISnapshotListener listener) {
        snapshotEventDelegate.addSnapshotListener(listener);
    }

    @Override
    public void removeSnapshotListener(ISnapshotListener listener) {
        snapshotEventDelegate.removeSnapshotListener(listener);
    }

    @Override
    public void fireDataChanged(Map<String, Object> data) {
        chartEventDelegate.fireChartEvent(new ChartViewerEvent(this, null, -1, null, data, Reason.DATA));
    }

    @Override
    public void fireConfigurationChanged(String id) {
        chartEventDelegate.fireChartEvent(new ChartViewerEvent(this, null, -1, id, null, Reason.CONFIGURATION));
    }

    public void fireHighlightChange(Map<String, Boolean> highlighted) {
        Map<String, Object> data = new LinkedHashMap<>();
        if (highlighted != null) {
            data.putAll(highlighted);
        }
        doFireHighlightChange(data);
    }

    protected void doFireHighlightChange(Map<String, Object> highlighted) {
        chartEventDelegate.fireChartEvent(new ChartViewerEvent(this, null, -1, null, highlighted, Reason.HIGHLIGHT));
    }

    public void fireActionPerformed(String name, boolean state) {
        chartEventDelegate.fireChartActionEvent(new ChartActionEvent(this, name, state));
    }

    protected void fireSelectionChanged(int index, String viewId, double... coordinates) {
        chartEventDelegate
                .fireChartEvent(new ChartViewerEvent(this, coordinates, index, viewId, null, Reason.SELECTION));
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void setEditable(boolean editable) {
        this.editable = editable;
        if (dataTable != null) {
            dataTable.setEditable(editable);
        }
    }

    @Override
    public String getAxisName(int axis) {
        String title = null;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            title = axisView.getAttributes().getTitle();
        }
        return title;
    }

    @Override
    public void setAxisName(String name, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.getAttributes().setTitle(name);
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public int getAxisTitleAlignment(int axis) {
        int alignment = IChartViewer.CENTER;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            alignment = axisView.getAttributes().getTitleAlignment();
        }
        return alignment;
    }

    @Override
    public void setAxisTitleAlignment(int align, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.getAttributes().setTitleAlignment(align);
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public CometeColor getAxisCometeColor(int axis) {
        CometeColor color = null;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            color = ColorTool.getCometeColor(axisView.getAxisColor());
        }
        return color;
    }

    @Override
    public void setAxisCometeColor(CometeColor color, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.setAxisColor(ColorTool.getColor(color));
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public int getGridStyle(int axis) {
        int style = STYLE_DASH;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            style = axisView.getAttributes().getGridStyle();
        }
        return style;
    }

    @Override
    public void setGridStyle(int style, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.getAttributes().setGridStyle(style);
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public String getAxisDateFormat(int axis) {
        String format = null;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            format = axisView.getAttributes().getDateFormat();
        }
        return format;
    }

    @Override
    public void setAxisDateFormat(String format, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.getAttributes().setDateFormat(format);
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public boolean isSubGridVisible(int axis) {
        boolean visible = false;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            visible = axisView.getAttributes().isSubGridVisible();
        }
        return visible;
    }

    @Override
    public void setSubGridVisible(boolean visible, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.getAttributes().setSubGridVisible(visible);
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public boolean isGridVisible(int axis) {
        boolean visible = false;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            visible = axisView.getAttributes().isGridVisible();
        }
        return visible;
    }

    @Override
    public void setGridVisible(boolean visible, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.getAttributes().setGridVisible(visible);
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public boolean isXAxisOnBottom() {
        return (chartView.getAxis(X).getAttributes().getOrigin() == HORIZONTAL_DOWN);
    }

    @Override
    public void setXAxisOnBottom(boolean b) {
        AbstractAxisView axisView = chartView.getAxis(X);
        axisView.getAttributes().setOrigin(b ? HORIZONTAL_DOWN : HORIZONTAL_ORGY1);
        if (isShowing()) {
            chartView.repaint();
        }
    }

    @Override
    public boolean isManagementPanelVisible() {
        return managementPanelVisible;
    }

    @Override
    public void setManagementPanelVisible(boolean visible) {
        if (visible != managementPanelVisible) {
            managementPanelVisible = visible;
            managementPanel.setVisible(visible);
            if (visible) {
                refreshVisibilityBoxes(false, null);
            } else {
                clearVisibilityBoxes();
            }
        }
    }

    @Override
    public boolean isMathExpressionEnabled() {
        return chartMenu.isExpressionEnabled();
    }

    @Override
    public void setMathExpressionEnabled(boolean enable) {
        chartMenu.setExpressionEnabled(enable);
    }

    @Override
    public double getDisplayDuration() {
        return chartView.getDisplayDuration();
    }

    @Override
    public void setDisplayDuration(double duration) {
        if (chartView.setDisplayDuration(duration)) {
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public String getHeader() {
        return chartView.getTitle();
    }

    @Override
    public CometeColor getHeaderCometeColor() {
        return ColorTool.getCometeColor(chartView.getForeground());
    }

    @Override
    public CometeFont getHeaderCometeFont() {
        return FontTool.getCometeFont(chartView.getFont());
    }

    @Override
    public boolean isHeaderVisible() {
        return chartView.isTitleVisible();
    }

    @Override
    public int getTimePrecision() {
        return timePrecision;
    }

    @Override
    public void setTimePrecision(int precision) {
        if (precision != this.timePrecision) {
            this.timePrecision = precision;
            fireConfigurationChanged(null);
        }
    }

    @Override
    public String getDateColumnName() {
        return dateColumnName;
    }

    @Override
    public void setDateColumnName(String dateColumnName) {
        this.dateColumnName = dateColumnName;
    }

    @Override
    public String getIndexColumnName() {
        return indexColumnName;
    }

    @Override
    public void setIndexColumnName(String name) {
        this.indexColumnName = name;
    }

    @Override
    public boolean isLegendVisible() {
        return legendVisible;
    }

    @Override
    public void setLegendVisible(boolean visible) {
        if (legendVisible != visible) {
            legendVisible = visible;
            updateChartLegend();
            fireConfigurationChanged(null);
        }
    }

    /**
     * Returns whether loaded data (from file) will have random ID instead of curve name as ID.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isGenerateRandomIDsForLoadedData() {
        return generateRandomIDsForLoadedData;
    }

    /**
     * Sets whether loaded data (from file) should have random ID instead of curve name as ID.
     * 
     * @param generateRandomIDsForLoadedData Whether loaded data (from file) should have random ID instead of curve name
     *            as ID.
     */
    public void setGenerateRandomIDsForLoadedData(boolean generateRandomIDsForLoadedData) {
        this.generateRandomIDsForLoadedData = generateRandomIDsForLoadedData;
    }

    @Override
    public void setAutoScale(boolean autoscale, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            if (autoscale != axisView.getAttributes().isAutoScale()) {
                axisView.getAttributes().setAutoScale(autoscale);
                axisView.getScale().updateScaleBoundsFromAttributes();
                if (isShowing()) {
                    chartView.repaint();
                }
            }
        }
    }

    @Override
    public boolean isAutoScale(int axis) {
        boolean autoscale = false;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            autoscale = axisView.getAttributes().isAutoScale();
        }
        return autoscale;
    }

    @Override
    public void setDataViewStyle(String id, int style) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            PlotProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            properties.setViewType(style);
            if ((dataView != null) && (style != dataView.getViewType())) {
                dataView.setViewType(style);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public int getDataViewStyle(String id) {
        PlotProperties properties = PlotPropertiesTool.getNotInitializedPlotProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getViewType());
    }

    @Override
    public void setDataViewFillStyle(String id, int style) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            BarProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getBar();
            properties.setFillStyle(style);
            if ((dataView != null) && (style != dataView.getFillStyle())) {
                dataView.setFillStyle(style);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewFillCometeColor(String id, CometeColor color) {
        if ((id != null) && (color != null)) {
            DataView dataView = getDataView(id, false);
            BarProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getBar();
            properties.setFillColor(color);
            Color fillColor = ColorTool.getColor(color);
            if ((dataView != null) && (!ObjectUtils.sameObject(fillColor, dataView.getFillColor()))) {
                dataView.setFillColor(fillColor);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public CometeColor getDataViewFillCometeColor(String id) {
        CometeColor color = null;
        BarProperties properties = PlotPropertiesTool.getNotInitializedBarProperties(id, plotPropertiesMap);
        if (properties != null) {
            color = properties.getFillColor();
        }
        return color;
    }

    @Override
    public void setDataViewLineStyle(String id, int style) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            CurveProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getCurve();
            properties.setLineStyle(style);
            if ((dataView != null) && (style != dataView.getStyle())) {
                dataView.setStyle(style);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewLineWidth(String id, int lineWidth) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            CurveProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getCurve();
            properties.setWidth(lineWidth);
            if ((dataView != null) && (lineWidth != dataView.getLineWidth())) {
                dataView.setLineWidth(lineWidth);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewBarWidth(String id, int barWidth) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            BarProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getBar();
            properties.setWidth(barWidth);
            if ((dataView != null) && (barWidth != dataView.getBarWidth())) {
                dataView.setBarWidth(barWidth);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewCometeColor(String id, CometeColor color) {
        if ((id != null) && (color != null)) {
            DataView dataView = getDataView(id, false);
            CurveProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getCurve();
            properties.setColor(color);
            Color lineColor = ColorTool.getColor(color);
            if ((dataView != null) && (!ObjectUtils.sameObject(lineColor, dataView.getColor()))) {
                dataView.setColor(lineColor);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewMarkerStyle(String id, int marker) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            MarkerProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getMarker();
            properties.setStyle(marker);
            if ((dataView != null) && (marker != dataView.getMarker())) {
                dataView.setMarker(marker);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewMarkerCometeColor(String id, CometeColor color) {
        if ((id != null) && (color != null)) {
            DataView dataView = getDataView(id, false);
            MarkerProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getMarker();
            properties.setColor(color);
            Color markerColor = ColorTool.getColor(color);
            if ((dataView != null) && (!ObjectUtils.sameObject(markerColor, dataView.getMarkerColor()))) {
                dataView.setMarkerColor(markerColor);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewMarkerSize(String id, int size) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            MarkerProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getMarker();
            properties.setSize(size);
            if ((dataView != null) && (size != dataView.getMarkerSize())) {
                dataView.setMarkerSize(size);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewErrorCometeColor(String id, CometeColor color) {
        if ((id != null) && (color != null)) {
            DataView dataView = getDataView(id, false);
            ErrorProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getError();
            properties.setColor(color);
            Color errorColor = ColorTool.getColor(color);
            if ((dataView != null) && (!ObjectUtils.sameObject(errorColor, dataView.getErrorColor()))) {
                dataView.setErrorColor(errorColor);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewErrorVisible(String id, boolean visible) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            ErrorProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getError();
            properties.setVisible(visible);
            if ((dataView != null) && (visible != dataView.isErrorVisible())) {
                dataView.setErrorVisible(visible);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewAxis(String id, int axis) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            PlotProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            properties.setAxisChoice(axis);
            if ((dataView != null) && (!ObjectUtils.sameObject(Integer.valueOf(axis), dataView.getAxis()))) {
                // don't show hidden views
                if (dataView.getAxis() > -1) {
                    dataView.setAxis(axis);
                }
                refresh(true);
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setFormat(String id, String format) {
        if (id != null) {
            if ((format != null) && format.trim().isEmpty()) {
                format = null;
            }
            DataView dataView = getDataView(id, false);
            PlotProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            properties.setFormat(format);
            if ((dataView != null) && (!ObjectUtils.sameObject(format, dataView.getFormat()))) {
                dataView.setFormat(format);
                refresh(false);
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewUnit(String id, String unit) {
        if (id != null) {
            if (unit == null) {
                unit = ObjectUtils.EMPTY_STRING;
            }
            DataView dataView = getDataView(id, false);
            PlotProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            properties.setUnit(unit);
            if ((dataView != null) && (!ObjectUtils.sameObject(unit, dataView.getUnit()))) {
                dataView.setUnit(unit);
                refresh(false);
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewDisplayName(String id, String displayName) {
        setDataViewDisplayName(id, displayName, true);
    }

    public void setDataViewDisplayName(String id, String displayName, boolean repaint) {
        if (id != null) {
            String name = ((displayName == null) || displayName.trim().isEmpty() ? id : displayName);
            DataView dataView = getDataView(id, false);
            CurveProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getCurve();
            properties.setName(name);
            if ((dataView != null) && (!ObjectUtils.sameObject(name, dataView.getDisplayName()))) {
                dataView.setDisplayName(name);
                if (repaint) {
                    refreshVisibilityBoxes(false, null);
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public boolean isFreezePanelVisible() {
        return freezePanelVisible;
    }

    @Override
    public void setFreezePanelVisible(boolean freezePanelVisible) {
        if (this.freezePanelVisible != freezePanelVisible) {
            this.freezePanelVisible = freezePanelVisible;
            freezePanel.setVisible(freezePanelVisible);
            chartMenu.setFreezeEnabled(freezePanelVisible);
            refresh(false);
        }
    }

    protected void updateFreezePanelAvailability() {
        boolean freezeEnabled = hasClickableViews();
        boolean clearFreezesEnabled;
        synchronized (freezeIds) {
            clearFreezesEnabled = !freezeIds.isEmpty();
        }

        freezeButton.setEnabled(freezeEnabled);
        chartMenu.getFreezeMenuItem().setEnabled(freezeEnabled);
        clearFreezesButton.setEnabled(clearFreezesEnabled);
        chartMenu.getCleaFreezesMenuItem().setEnabled(clearFreezesEnabled);
    }

    @Override
    public void setHeader(String s) {
        chartView.setTitle(s);
        if (isShowing()) {
            chartView.repaint();
        }
    }

    @Override
    public void setHeaderCometeColor(CometeColor c) {
        chartView.setForeground(ColorTool.getColor(c));
        if (isShowing()) {
            chartView.repaint();
        }
    }

    @Override
    public void setHeaderCometeFont(CometeFont f) {
        chartView.setFont(FontTool.getFont(f));
        if (isShowing()) {
            chartView.repaint();
        }
    }

    @Override
    public void setHeaderVisible(boolean visible) {
        chartView.setTitleVisible(visible);
        if (isShowing()) {
            chartView.repaint();
        }
    }

    protected void setLegendFont(Font labelFont, boolean repaint) {
        chartLegend.setFont(labelFont);
        if (isShowing() && repaint) {
            repaint();
        }
    }

    @Override
    public void setLabelCometeFont(CometeFont f) {
        setLegendFont(FontTool.getFont(f), true);
    }

    @Override
    public CometeFont getLabelCometeFont() {
        return FontTool.getCometeFont(chartLegend.getFont());
    }

    @Override
    public void setScale(int scale, int axis) {
        AxisProperties properties = getAxisProperties(axis);
        if (properties != null) {
            properties.setScaleMode(scale);
            setAxisProperties(properties, axis);
        }
    }

    @Override
    public int getScale(int axis) {
        int scale = -1;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            scale = axisView.getAttributes().getScale();
        }
        return scale;
    }

    @Override
    public int getDataViewFillStyle(String id) {
        BarProperties properties = PlotPropertiesTool.getNotInitializedBarProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getFillStyle());
    }

    @Override
    public int getDataViewLineStyle(String id) {
        CurveProperties properties = PlotPropertiesTool.getNotInitializedCurveProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getLineStyle());
    }

    @Override
    public int getDataViewLineWidth(String id) {
        CurveProperties properties = PlotPropertiesTool.getNotInitializedCurveProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getWidth());
    }

    @Override
    public int getDataViewBarWidth(String id) {
        BarProperties properties = PlotPropertiesTool.getNotInitializedBarProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getWidth());
    }

    @Override
    public CometeColor getDataViewCometeColor(String id) {
        CurveProperties properties = PlotPropertiesTool.getNotInitializedCurveProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.getColor());
    }

    @Override
    public int getDataViewMarkerStyle(String id) {
        MarkerProperties properties = PlotPropertiesTool.getNotInitializedMarkerProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getStyle());
    }

    @Override
    public CometeColor getDataViewMarkerCometeColor(String id) {
        MarkerProperties properties = PlotPropertiesTool.getNotInitializedMarkerProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.getColor());
    }

    @Override
    public int getDataViewMarkerSize(String id) {
        MarkerProperties properties = PlotPropertiesTool.getNotInitializedMarkerProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getSize());
    }

    @Override
    public CometeColor getDataViewErrorCometeColor(String id) {
        ErrorProperties properties = PlotPropertiesTool.getNotInitializedErrorProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.getColor());
    }

    @Override
    public boolean isDataViewErrorVisible(String id) {
        ErrorProperties properties = PlotPropertiesTool.getNotInitializedErrorProperties(id, plotPropertiesMap);
        return (properties == null ? true : properties.isVisible());
    }

    @Override
    public int getDataViewAxis(String id) {
        PlotProperties properties = PlotPropertiesTool.getNotInitializedPlotProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getAxisChoice());
    }

    @Override
    public String getFormat(String id) {
        PlotProperties properties = PlotPropertiesTool.getNotInitializedPlotProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.getFormat());
    }

    @Override
    public String getDataViewUnit(String id) {
        PlotProperties properties = PlotPropertiesTool.getNotInitializedPlotProperties(id, plotPropertiesMap);
        return (properties == null ? ObjectUtils.EMPTY_STRING : properties.getUnit());
    }

    @Override
    public String getDataViewDisplayName(String id) {
        CurveProperties properties = PlotPropertiesTool.getNotInitializedCurveProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.getName());
    }

    @Override
    public boolean containsDataView(String id) {
        return getDataView(id, false) != null;
    }

    @Override
    public void setPreferDialogForTable(boolean preferDialog, boolean modal) {
        this.preferDialog = preferDialog;
        this.modalDialog = modal;
    }

    @Override
    public void setLabelPlacement(int p) {
        if (legendPlacement != p) {
            legendPlacement = p;
            updateChartLegend();
            fireConfigurationChanged(null);
        }
    }

    @Override
    public int getLabelPlacement() {
        return legendPlacement;
    }

    @Override
    public double getLegendProportion() {
        return legendProportion;
    }

    @Override
    public void setLegendProportion(double legendProportion) {
        if (legendProportionChanged(legendProportion)) {
            this.legendProportion = legendProportion;
            updateChartLegend();
            fireConfigurationChanged(null);
        }
    }

    @Override
    public String getNoValueString() {
        return noValueString;
    }

    @Override
    public void setNoValueString(String noValueString) {
        if (noValueString == null) {
            noValueString = ChartProperties.DEFAULT_NO_VALUE_STRING;
        }
        if (!ObjectUtils.sameObject(noValueString, this.noValueString)) {
            this.noValueString = noValueString;
            fireConfigurationChanged(null);
        }
    }

    @Override
    public double getAxisMaximum(int axis) {
        double max = Double.NaN;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            max = axisView.getAttributes().getMaximum();
        }
        return max;
    }

    @Override
    public void setAxisMaximum(double maximum, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.getAttributes().setMaximum(maximum);
            axisView.getScale().updateScaleBoundsFromAttributes();
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public double getAxisMinimum(int axis) {
        double min = Double.NaN;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            min = axisView.getAttributes().getMinimum();
        }
        return min;
    }

    @Override
    public void setAxisMinimum(double minimum, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.getAttributes().setMinimum(minimum);
            axisView.getScale().updateScaleBoundsFromAttributes();
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public void setDataViewFillMethod(String id, int m) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            BarProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getBar();
            properties.setFillingMethod(m);
            if ((dataView != null) && (m != dataView.getFillMethod())) {
                dataView.setFillMethod(m);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public int getDataViewFillMethod(String id) {
        BarProperties properties = PlotPropertiesTool.getNotInitializedBarProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getFillingMethod());
    }

    @Override
    public boolean isAxisDrawOpposite(int axis) {
        boolean opposite = false;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            opposite = axisView.getAttributes().isDrawOpposite();
        }
        return opposite;
    }

    @Override
    public void setAxisDrawOpposite(boolean opposite, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.getAttributes().setDrawOpposite(opposite);
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public int getAxisLabelFormat(int axis) {
        int format = AUTO_FORMAT;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            format = axisView.getAttributes().getLabelFormat();
        }
        return format;
    }

    @Override
    public void setAxisLabelFormat(int format, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.getAttributes().setLabelFormat(format);
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public int getAxisPosition(int axis) {
        int position = AUTO_FORMAT;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            position = axisView.getAttributes().getOrigin();
        }
        return position;
    }

    @Override
    public void setAxisPosition(int position, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.getAttributes().setOrigin(position);
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public boolean isAxisVisible(int axis) {
        boolean visible = false;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            visible = axisView.isVisible();
        }
        return visible;
    }

    @Override
    public void setAxisVisible(boolean visible, int axis) {
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            axisView.setVisible(visible);
            if (isShowing()) {
                chartView.repaint();
            }
        }
    }

    @Override
    public boolean isTimeScale(int axis) {
        boolean time = false;
        AbstractAxisView axisView = chartView.getAxis(axis);
        if (axisView != null) {
            AbstractAxisScale scale = axisView.getScale();
            if (scale != null) {
                time = scale.isTimeScale();
            }
        }
        return time;
    }

    @Override
    public void setCustomCometeColor(CometeColor[] colorList) {
        lineColorRotationTool.setCustomColors(ColorTool.getColors(colorList));
    }

    @Override
    public CometeColor[] getCustomCometeColor() {
        return ColorTool.getCometeColors(lineColorRotationTool.getCustomColors());
    }

    @Override
    public void setCustomFillCometeColor(CometeColor[] colorList) {
        barColorRotationTool = getColorRotationTool(colorList, barColorRotationTool);
    }

    @Override
    public CometeColor[] getCustomFillCometeColor() {
        return getCustomCometeColors(barColorRotationTool);
    }

    @Override
    public void setCustomMarkerCometeColor(CometeColor[] colorList) {
        markerColorRotationTool = getColorRotationTool(colorList, markerColorRotationTool);
    }

    @Override
    public CometeColor[] getCustomMarkerCometeColor() {
        return getCustomCometeColors(markerColorRotationTool);
    }

    @Override
    public void setCustomUserFormat(String[] formatList) {
        // XXX not managed
    }

    @Override
    public String[] getCustomUserFormat() {
        // XXX not managed
        return null;
    }

    @Override
    public void setCustomLineStyle(final int[] lineStyleList) {
        lineStyleRotationTool = getIntRotationTool(lineStyleList, lineStyleRotationTool);
    }

    @Override
    public int[] getCustomLineStyle() {
        return getValues(lineStyleRotationTool);
    }

    @Override
    public void setCustomMarkerStyle(final int[] styleList) {
        markerStyleRotationTool = getIntRotationTool(styleList, markerStyleRotationTool);
    }

    @Override
    public int[] getCustomMarkerStyle() {
        return getValues(markerStyleRotationTool);
    }

    @Override
    public void setCustomMarkerSize(final int[] sizeList) {
        markerSizeRotationTool = getIntRotationTool(sizeList, markerSizeRotationTool);
    }

    @Override
    public int[] getCustomMarkerSize() {
        return getValues(markerSizeRotationTool);
    }

    @Override
    public void setCustomLineWidth(final int[] widthList) {
        lineWidthRotationTool = getIntRotationTool(widthList, lineWidthRotationTool);
    }

    @Override
    public int[] getCustomLineWidth() {
        return getValues(lineWidthRotationTool);
    }

    @Override
    public void setCustomBarWidth(final int[] widthList) {
        barWidthRotationTool = getIntRotationTool(widthList, barWidthRotationTool);
    }

    @Override
    public int[] getCustomBarWidth() {
        return getValues(barWidthRotationTool);
    }

    @Override
    public void setCustomDataViewStyle(final int[] styleList) {
        viewStyleRotationTool = getIntRotationTool(styleList, viewStyleRotationTool);
    }

    @Override
    public int[] getCustomDataViewStyle() {
        return getValues(viewStyleRotationTool);
    }

    @Override
    public void setCustomAxis(int[] axisList) {
        axisRotationTool = getIntRotationTool(axisList, axisRotationTool);
    }

    @Override
    public int[] getCustomAxis() {
        return getValues(axisRotationTool);
    }

    @Override
    public void setCustomFillStyle(final int[] styleList) {
        fillStyleRotationTool = getIntRotationTool(styleList, fillStyleRotationTool);
    }

    @Override
    public int[] getCustomFillStyle() {
        return getValues(fillStyleRotationTool);
    }

    @Override
    public void setCyclingCustomMap(boolean cyclingCustomMap) {
        this.cyclingCustomMap = cyclingCustomMap;
        setCyclingCustomMap(lineStyleRotationTool);
        setCyclingCustomMap(markerStyleRotationTool);
        setCyclingCustomMap(markerSizeRotationTool);
        setCyclingCustomMap(lineWidthRotationTool);
        setCyclingCustomMap(barWidthRotationTool);
        setCyclingCustomMap(viewStyleRotationTool);
        setCyclingCustomMap(fillStyleRotationTool);
    }

    @Override
    public boolean isCyclingCustomMap() {
        return cyclingCustomMap;
    }

    @Override
    public double getDataViewXOffsetA0(String id) {
        OffsetProperties properties = PlotPropertiesTool.getNotInitializedXOffsetProperties(id, plotPropertiesMap);
        return (properties == null ? MathConst.NAN_FOR_NULL : properties.getA0());
    }

    @Override
    public void setDataViewXOffsetA0(String id, double a0x) {
        if (id != null) {
            setDataViewXOffsetProperties(id, new OffsetProperties(a0x));
        }
    }

    @Override
    public double getDataViewTransformA0(String id) {
        TransformationProperties properties = PlotPropertiesTool.getNotInitializedTransformationProperties(id,
                plotPropertiesMap);
        return (properties == null ? MathConst.NAN_FOR_NULL : properties.getA0());
    }

    @Override
    public void setDataViewTransformA0(String id, double transform) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            TransformationProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getTransform().clone();
            properties.setA0(transform);
            setDataViewTransformationProperties(id, properties);
        }
    }

    @Override
    public double getDataViewTransformA1(String id) {
        TransformationProperties properties = PlotPropertiesTool.getNotInitializedTransformationProperties(id,
                plotPropertiesMap);
        return (properties == null ? MathConst.NAN_FOR_NULL : properties.getA1());
    }

    @Override
    public void setDataViewTransformA1(String id, double transform) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            TransformationProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getTransform().clone();
            properties.setA1(transform);
            setDataViewTransformationProperties(id, properties);
        }
    }

    @Override
    public double getDataViewTransformA2(String id) {
        TransformationProperties properties = PlotPropertiesTool.getNotInitializedTransformationProperties(id,
                plotPropertiesMap);
        return (properties == null ? MathConst.NAN_FOR_NULL : properties.getA2());
    }

    @Override
    public void setDataViewTransformA2(String id, double transform) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            TransformationProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getTransform().clone();
            properties.setA2(transform);
            setDataViewTransformationProperties(id, properties);
        }
    }

    @Override
    public boolean isDataViewClickable(String id) {
        // TODO Manage clickable in PlotProperties
        boolean clickable = false;
        DataView dataView = getDataView(id, false);
        if (dataView != null) {
            clickable = dataView.isClickable();
        }
        return clickable;
    }

    @Override
    public void setDataViewClickable(String id, boolean clickable) {
        // TODO Manage clickable in PlotProperties
        DataView dataView = getDataView(id, false);
        if (dataView != null) {
            dataView.setClickable(clickable);
            if (isShowing()) {
                repaint();
            }
        }
    }

    @Override
    public boolean isDataViewLabelVisible(String id) {
        MarkerProperties properties = PlotPropertiesTool.getNotInitializedMarkerProperties(id, plotPropertiesMap);
        return (properties == null ? false : properties.isLegendVisible());
    }

    @Override
    public boolean isDataViewSamplingAllowed(String id) {
        MarkerProperties properties = PlotPropertiesTool.getNotInitializedMarkerProperties(id, plotPropertiesMap);
        return (properties == null ? false : properties.isSamplingAllowed());
    }

    @Override
    public int getDataViewInterpolationMethod(String id) {
        InterpolationProperties properties = PlotPropertiesTool.getNotInitializedInterpolationProperties(id,
                plotPropertiesMap);
        return (properties == null ? -1 : properties.getInterpolationMethod());
    }

    @Override
    public void setDataViewInterpolationMethod(String id, int method) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            InterpolationProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getInterpolation();
            properties.setInterpolationMethod(method);
            if ((dataView != null) && (method != dataView.getInterpolationMethod())) {
                dataView.setInterpolationMethod(method);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public int getDataViewInterpolationStep(String id) {
        InterpolationProperties properties = PlotPropertiesTool.getNotInitializedInterpolationProperties(id,
                plotPropertiesMap);
        return (properties == null ? -1 : properties.getInterpolationStep());
    }

    @Override
    public void setDataViewInterpolationStep(String id, int step) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            InterpolationProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getInterpolation();
            properties.setInterpolationStep(step);
            if ((dataView != null) && (step != dataView.getInterpolationStep())) {
                dataView.setInterpolationStep(step);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public double getDataViewHermiteBias(String id) {
        InterpolationProperties properties = PlotPropertiesTool.getNotInitializedInterpolationProperties(id,
                plotPropertiesMap);
        return (properties == null ? MathConst.NAN_FOR_NULL : properties.getHermiteBias());
    }

    @Override
    public void setDataViewHermiteBias(String id, double bias) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            InterpolationProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getInterpolation();
            properties.setHermiteBias(bias);
            if ((dataView != null) && (bias != dataView.getHermiteBias())) {
                dataView.setHermiteBias(bias);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public double getDataViewHermiteTension(String id) {
        InterpolationProperties properties = PlotPropertiesTool.getNotInitializedInterpolationProperties(id,
                plotPropertiesMap);
        return (properties == null ? MathConst.NAN_FOR_NULL : properties.getHermiteTension());
    }

    @Override
    public void setDataViewHermiteTension(String id, double tension) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            InterpolationProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getInterpolation();
            properties.setHermiteTension(tension);
            if ((dataView != null) && (tension != dataView.getHermiteTension())) {
                dataView.setHermiteTension(tension);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public int getDataViewSmoothingMethod(String id) {
        SmoothingProperties properties = PlotPropertiesTool.getNotInitializedSmoothingProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getMethod());
    }

    @Override
    public void setDataViewSmoothingMethod(String id, int method) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            SmoothingProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getSmoothing();
            properties.setMethod(method);
            if ((dataView != null) && (method != dataView.getSmoothingMethod())) {
                dataView.setSmoothingMethod(method);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public int getDataViewSmoothingNeighbors(String id) {
        SmoothingProperties properties = PlotPropertiesTool.getNotInitializedSmoothingProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getNeighbors());
    }

    @Override
    public void setDataViewSmoothingNeighbors(String id, int neighbors) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            SmoothingProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getSmoothing();
            properties.setNeighbors(neighbors);
            if ((dataView != null) && (neighbors != dataView.getSmoothingNeighbors())) {
                dataView.setSmoothingNeighbors(neighbors);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public double getDataViewSmoothingGaussSigma(String id) {
        SmoothingProperties properties = PlotPropertiesTool.getNotInitializedSmoothingProperties(id, plotPropertiesMap);
        return (properties == null ? MathConst.NAN_FOR_NULL : properties.getGaussSigma());
    }

    @Override
    public void setDataViewSmoothingGaussSigma(String id, double sigma) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            SmoothingProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getSmoothing();
            properties.setGaussSigma(sigma);
            if ((dataView != null) && (sigma != dataView.getSmoothingGaussSigma())) {
                dataView.setSmoothingGaussSigma(sigma);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public int getDataViewSmoothingExtrapolation(String id) {
        SmoothingProperties properties = PlotPropertiesTool.getNotInitializedSmoothingProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getExtrapolation());
    }

    @Override
    public void setDataViewSmoothingExtrapolation(String id, int extrapolation) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            SmoothingProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getSmoothing();
            properties.setExtrapolation(extrapolation);
            if ((dataView != null) && (extrapolation != dataView.getSmoothingExtrapolation())) {
                dataView.setSmoothingExtrapolation(extrapolation);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public int getDataViewMathFunction(String id) {
        MathProperties properties = PlotPropertiesTool.getNotInitializedMathProperties(id, plotPropertiesMap);
        return (properties == null ? -1 : properties.getFunction());
    }

    @Override
    public void setDataViewMathFunction(String id, int function) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            MathProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getMath().clone();
            properties.setFunction(function);
            setDataViewMathProperties(id, properties);
        }
    }

    @Override
    public void setDataViewLabelVisible(String id, boolean visible) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            MarkerProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getMarker();
            properties.setLegendVisible(visible);
            if ((dataView != null) && (visible != dataView.isLabelVisible())) {
                dataView.setLabelVisible(visible);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public void setDataViewSamplingAllowed(String id, boolean allowed) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            MarkerProperties properties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName()).getMarker();
            properties.setSamplingAllowed(allowed);
            if ((dataView != null) && (allowed != dataView.isSamplingAllowed())) {
                dataView.setSamplingAllowed(allowed);
                if (isShowing()) {
                    if (isLegendVisible()) {
                        chartLegend.revalidate();
                        chartSplitPane.revalidate();
                    }
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public int getDataViewHighlightMethod(String id) {
        int method = HIGHLIGHT_MULTIPLY;
        DataView view = getDataView(id, false);
        if (view != null) {
            method = view.getHighlightMethod();
        }
        return method;
    }

    @Override
    public void setDataViewHighlightMethod(String id, int method) {
        DataView view = getDataView(id, false);
        if (view != null) {
            view.setHighlightMethod(method);
            if (isShowing()) {
                repaint();
            }
        }
    }

    @Override
    public double getDataViewHighlightCoefficient(String id) {
        double coeff = IChartConst.DEFAULT_HIGHLIGHT_COEFF;
        DataView view = getDataView(id, false);
        if (view != null) {
            coeff = view.getHighlightCoefficient();
        }
        return coeff;
    }

    @Override
    public void setDataViewHighlightCoefficient(String id, double coef) {
        DataView view = getDataView(id, false);
        if (view != null) {
            view.setHighlightCoefficient(coef);
            if (isShowing()) {
                repaint();
            }
        }
    }

    @Override
    public boolean isDataViewHighlighted(String id) {
        boolean highlighted = false;
        DataView view = getDataView(id, false);
        if (view != null) {
            highlighted = view.isHighlighted();
        }
        return highlighted;
    }

    @Override
    public void setDataViewHighlighted(String id, boolean highlighted) {
        setDataViewsHighlighted(highlighted, true, id);
    }

    @Override
    public void setAutoHighlightOnLegend(boolean autoHighlightOnLegend) {
        if (this.autoHighlight != autoHighlightOnLegend) {
            this.autoHighlight = autoHighlightOnLegend;
            chartView.measureGraphItems();
            if (isShowing()) {
                repaint();
            }
            fireConfigurationChanged(null);
        }
    }

    @Override
    public boolean isAutoHighlightOnLegend() {
        return autoHighlight;
    }

    @Override
    public boolean isDataViewRemovingEnabled() {
        return chartLegend.isDataViewRemovingEnabled();
    }

    @Override
    public void setDataViewRemovingEnabled(boolean curveRemovingEnabled) {
        chartLegend.setDataViewRemovingEnabled(curveRemovingEnabled);
    }

    @Override
    public boolean isCleanDataViewConfigurationOnRemoving() {
        return cleanOnRemoving;
    }

    @Override
    public void setCleanDataViewConfigurationOnRemoving(boolean cleanCurveConfigurationOnRemoving) {
        this.cleanOnRemoving = cleanCurveConfigurationOnRemoving;
    }

    @Override
    public boolean hasDataViewProperties(String id) {
        return PlotPropertiesTool.getNotInitializedPlotProperties(id, plotPropertiesMap) != null;
    }

    @Override
    public void cleanDataViewProperties(String id) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            plotPropertiesMap.remove(id);
            if (dataView != null) {
                plotPropertiesMap.putIfAbsent(id, PlotPropertiesTool.readPlotProperties(dataView));
            }
        }
    }

    @Override
    public PlotProperties getDataViewPlotProperties(String id) {
        PlotProperties properties = PlotPropertiesTool.getNotInitializedPlotProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.clone());
    }

    @Override
    public void setDataViewPlotProperties(String id, PlotProperties properties) {
        if (id != null) {
            DataView dataView = getDataView(id, false);
            boolean shouldComputeAxis = false;
            PlotProperties previous = PlotPropertiesTool.getNotInitializedPlotProperties(id, plotPropertiesMap);
            if (!ObjectUtils.sameObject(properties, previous)) {
                if (properties == null) {
                    if (dataView == null) {
                        plotPropertiesMap.remove(id);
                    }
                } else {
                    plotPropertiesMap.put(id, properties.clone());
                    if (previous != null) {
                        shouldComputeAxis = (!ObjectUtils.sameObject(properties.getMath(), previous.getMath()))
                                || (!ObjectUtils.sameObject(properties.getTransform(), previous.getTransform()));
                    }
                }
                if ((dataView != null) && (properties != null)) {
                    PlotPropertiesTool.writePlotProperties(dataView, properties);
                    refreshVisibilityBoxes(shouldComputeAxis, null);
                    fireConfigurationChanged(id);
                }
            }
        }
    }

    @Override
    public BarProperties getDataViewBarProperties(String id) {
        BarProperties properties = PlotPropertiesTool.getNotInitializedBarProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.clone());
    }

    @Override
    public void setDataViewBarProperties(String id, BarProperties properties) {
        if ((id != null) && (properties != null)) {
            DataView dataView = getDataView(id, false);
            PlotProperties plotProperties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            BarProperties previous = plotProperties.getBar();
            plotProperties.setBar(properties.clone());
            if ((dataView != null) && (!properties.equals(previous))) {
                PlotPropertiesTool.writeBarProperties(dataView, properties);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public CurveProperties getDataViewCurveProperties(String id) {
        CurveProperties properties = PlotPropertiesTool.getNotInitializedCurveProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.clone());
    }

    @Override
    public void setDataViewCurveProperties(String id, CurveProperties properties) {
        if ((id != null) && (properties != null)) {
            DataView dataView = getDataView(id, false);
            PlotProperties plotProperties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            CurveProperties previous = plotProperties.getCurve();
            plotProperties.setCurve(properties.clone());
            if ((dataView != null) && (!properties.equals(previous))) {
                PlotPropertiesTool.writeCurveProperties(dataView, properties);
                refreshVisibilityBoxes(false, null);
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public MarkerProperties getDataViewMarkerProperties(String id) {
        MarkerProperties properties = PlotPropertiesTool.getNotInitializedMarkerProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.clone());
    }

    @Override
    public void setDataViewMarkerProperties(String id, MarkerProperties properties) {
        if ((id != null) && (properties != null)) {
            DataView dataView = getDataView(id, false);
            PlotProperties plotProperties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            MarkerProperties previous = plotProperties.getMarker();
            plotProperties.setMarker(properties.clone());
            if ((dataView != null) && (!properties.equals(previous))) {
                PlotPropertiesTool.writeMarkerProperties(dataView, properties);
                if (isShowing()) {
                    if (isLegendVisible()) {
                        chartLegend.revalidate();
                        chartSplitPane.revalidate();
                    }
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public ErrorProperties getDataViewErrorProperties(String id) {
        ErrorProperties properties = PlotPropertiesTool.getNotInitializedErrorProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.clone());
    }

    @Override
    public void setDataViewErrorProperties(String id, ErrorProperties properties) {
        if ((id != null) && (properties != null)) {
            DataView dataView = getDataView(id, false);
            PlotProperties plotProperties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            ErrorProperties previous = plotProperties.getError();
            plotProperties.setError(properties.clone());
            if ((dataView != null) && (!properties.equals(previous))) {
                PlotPropertiesTool.writeErrorProperties(dataView, properties);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public TransformationProperties getDataViewTransformationProperties(String id) {
        TransformationProperties properties = PlotPropertiesTool.getNotInitializedTransformationProperties(id,
                plotPropertiesMap);
        return (properties == null ? null : properties.clone());
    }

    @Override
    public void setDataViewTransformationProperties(String id, TransformationProperties properties) {
        if ((id != null) && (properties != null)) {
            DataView dataView = getDataView(id, false);
            PlotProperties plotProperties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            TransformationProperties previous = plotProperties.getTransform();
            plotProperties.setTransform(properties.clone());
            if ((dataView != null) && (!properties.equals(previous))) {
                PlotPropertiesTool.writeTransformationProperties(dataView, properties);
                refresh(true);
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public OffsetProperties getDataViewXOffsetProperties(String id) {
        OffsetProperties properties = PlotPropertiesTool.getNotInitializedXOffsetProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.clone());
    }

    @Override
    public void setDataViewXOffsetProperties(String id, OffsetProperties properties) {
        if ((id != null) && (properties != null)) {
            DataView dataView = getDataView(id, false);
            PlotProperties plotProperties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            OffsetProperties previous = plotProperties.getXOffset();
            plotProperties.setXOffset(properties.clone());
            if ((dataView != null) && (!properties.equals(previous))) {
                PlotPropertiesTool.writeXOffsetProperties(dataView, properties);
                refresh(true);
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public InterpolationProperties getDataViewInterpolationProperties(String id) {
        InterpolationProperties properties = PlotPropertiesTool.getNotInitializedInterpolationProperties(id,
                plotPropertiesMap);
        return (properties == null ? null : properties.clone());
    }

    @Override
    public void setDataViewInterpolationProperties(String id, InterpolationProperties properties) {
        if ((id != null) && (properties != null)) {
            DataView dataView = getDataView(id, false);
            PlotProperties plotProperties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            InterpolationProperties previous = plotProperties.getInterpolation();
            plotProperties.setInterpolation(properties.clone());
            if ((dataView != null) && (!properties.equals(previous))) {
                PlotPropertiesTool.writeInterpolationProperties(dataView, properties);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public SmoothingProperties getDataViewSmoothingProperties(String id) {
        SmoothingProperties properties = PlotPropertiesTool.getNotInitializedSmoothingProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.clone());
    }

    @Override
    public void setDataViewSmoothingProperties(String id, SmoothingProperties properties) {
        if ((id != null) && (properties != null)) {
            DataView dataView = getDataView(id, false);
            PlotProperties plotProperties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            SmoothingProperties previous = plotProperties.getSmoothing();
            plotProperties.setSmoothing(properties.clone());
            if ((dataView != null) && (!properties.equals(previous))) {
                PlotPropertiesTool.writeSmoothingProperties(dataView, properties);
                if (isShowing()) {
                    repaint();
                }
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public MathProperties getDataViewMathProperties(String id) {
        MathProperties properties = PlotPropertiesTool.getNotInitializedMathProperties(id, plotPropertiesMap);
        return (properties == null ? null : properties.clone());
    }

    @Override
    public void setDataViewMathProperties(String id, MathProperties properties) {
        if ((id != null) && (properties != null)) {
            DataView dataView = getDataView(id, false);
            PlotProperties plotProperties = getPlotPropertiesForNewDataView(id,
                    dataView == null ? null : dataView.getDisplayName());
            MathProperties previous = plotProperties.getMath();
            plotProperties.setMath(properties.clone());
            if ((dataView != null) && (!properties.equals(previous))) {
                PlotPropertiesTool.writeMathProperties(dataView, properties);
                refresh(true);
                fireConfigurationChanged(id);
            }
        }
    }

    @Override
    public ChartProperties getChartProperties() {
        ChartProperties properties = chartView.getChartProperties();
        properties.setLabelFont(getLabelCometeFont());
        properties.setGlobalBackgroundColor(getCometeBackground());
        properties.setAutoHighlightOnLegend(autoHighlight);
        properties.setLegendVisible(legendVisible);
        properties.setLegendPlacement(legendPlacement);
        properties.setLegendProportion(legendProportion);
        properties.setNoValueString(noValueString);
        properties.setTimePrecision(timePrecision);
        return properties;
    }

    @Override
    public void setChartProperties(ChartProperties properties) {
        if (properties != null) {
            ChartProperties chartProperties = getChartProperties();
            if (!ObjectUtils.sameObject(chartProperties, properties)) {
                autoHighlight = properties.isAutoHighlightOnLegend();
                legendVisible = properties.isLegendVisible();
                legendPlacement = properties.getLegendPlacement();
                legendProportion = properties.getLegendProportion();
                noValueString = properties.getNoValueString();
                timePrecision = properties.getTimePrecision();
                setCometeBackground(properties.getGlobalBackgroundColor());
                chartView.setChartProperties(properties);
                Font labelFont = FontTool.getFont(properties.getLabelFont());
                setLegendFont(labelFont, false);
                // update chartSplitPane only when necessary (CONTROLGUI-390)
                if ((chartProperties.getLegendPlacement() != legendPlacement)
                        || (chartProperties.isLegendVisible() != legendVisible)
                        || legendProportionChanged(chartProperties.getLegendProportion())) {
                    updateChartLegend();
                }
                fireConfigurationChanged(null);
            }
        }
    }

    @Override
    public AxisProperties getAxisProperties(int axisChoice) {
        return chartView.getAxisProperties(axisChoice);
    }

    @Override
    public void setAxisProperties(AxisProperties properties, int axisChoice) {
        AxisProperties axisProperties = getAxisProperties(axisChoice);
        if (!ObjectUtils.sameObject(axisProperties, properties)) {
            chartView.setAxisProperties(axisChoice, properties);
            fireConfigurationChanged(null);
        }
    }

    @Override
    public SamplingProperties getSamplingProperties() {
        return chartView.getSamplingProperties();
    }

    @Override
    public void setSamplingProperties(SamplingProperties properties) {
        if (!ObjectUtils.sameObject(properties, getSamplingProperties())) {
            chartView.setSamplingProperties(properties);
            if (isShowing()) {
                chartLegend.revalidate();
                chartLegend.repaint();
            }
            fireConfigurationChanged(null);
        }
    }

    @Override
    public int getDataViewOffsetDragType() {
        return chartView.getDataViewOffsetDragType();
    }

    @Override
    public void setDataViewOffsetDragType(int dataViewOffsetDragType) {
        chartView.setDataViewOffsetDragType(dataViewOffsetDragType);
    }

    @Override
    public int getScaleDragType() {
        return chartView.getScaleDragType();
    }

    @Override
    public void setScaleDragType(int scaleDragType) {
        chartView.setScaleDragType(scaleDragType);
    }

    @Override
    public int getScaleDragSensitivity() {
        return chartView.getScaleDragSensitivity();
    }

    @Override
    public void setScaleDragSensitivity(int scaleDragSensitivity) {
        chartView.setScaleDragSensitivity(scaleDragSensitivity);
    }

    @Override
    public DragProperties getDragProperties() {
        return chartView.getDragProperties();
    }

    @Override
    public void setDragProperties(DragProperties properties) {
        if (!ObjectUtils.sameObject(properties, getDragProperties())) {
            chartView.setDragProperties(properties);
            fireConfigurationChanged(null);
        }
    }

    @Override
    public boolean isAxisSelectionVisible() {
        return axisSelectionVisible;
    }

    @Override
    public void setAxisSelectionVisible(boolean axisSelectionVisible) {
        this.axisSelectionVisible = axisSelectionVisible;
        if (axisSelectionCombo != null) {
            axisSelectionCombo.setVisible(axisSelectionVisible);
        }
    }

    @Override
    public void setAxisScaleEditionEnabled(boolean enabled, int axis) {
        if (chartOption != null) {
            chartOption.setScalePanelEnabled(enabled, axis);
        }
    }

    @Override
    public void resetAll() {
        resetAll(true);
    }

    @Override
    public void resetAll(boolean clearConfiguration) {
        // Axis should be refreshed only when there was some data.
        boolean shouldRefresh = !dataViewList.isEmpty();
        data.clear();
        expressionMap.clear();
        dataViewList.clearDataView();
        synchronized (freezeIds) {
            if (!clearConfiguration) {
                // always forget freeze configurations
                for (String id : freezeIds) {
                    plotPropertiesMap.remove(id);
                }
            }
            freezeIds.clear();
        }
        if (clearConfiguration) {
            plotPropertiesMap.clear();
        }
        // refresh visible content only when not showing (JAVAAPI-621)
        if (shouldRefresh) {
            shouldMeasureItems = Boolean.TRUE;
        } else if (shouldMeasureItems == null) {
            shouldMeasureItems = Boolean.FALSE;
        }
        Collection<DataView>[] tmpViews = updatedViews;
        updatedViews = null;
        cleanViews(tmpViews);
        dataComponentDelegate.setDataChanged(this, true);
    }

    @Override
    public void addExpression(String name, String expression, int axis, String[] variables, boolean x) {
        addExpression(name, name, expression, axis, variables, x);
    }

    @Override
    public void addExpression(String id, String name, String expression, int axis, String[] variables, boolean x) {
        DataView expressionDataView = generateDataView(id);
        expressionDataView.setXDataSorted(isDataViewsSortedOnX());
        expressionDataView.setDisplayName(name);
        addExpressionToChart(expressionDataView, expression, axis, variables, x);
    }

    @Override
    public void setDataViewEditable(String id, boolean editable) {
        // TODO manage editable in PlotProperties or remove from IChartViewer
    }

    @Override
    public boolean isLimitDrawingZone() {
        return chartView.isLimitDrawingZone();
    }

    @Override
    public void setLimitDrawingZone(boolean limitDrawingZone) {
        chartView.setLimitDrawingZone(limitDrawingZone);
        if (isShowing()) {
            chartView.repaint();
        }
    }

    @Override
    public boolean isAutoHideViews() {
        return autoHide;
    }

    @Override
    public void setAutoHideViews(boolean hide) {
        this.autoHide = hide;
    }

    @Override
    public String[][] getAllViewsIdsAndNames() {
        List<DataView> views = dataViewList.getListCopy(dataViewComparator);
        String[][] idAndName = new String[2][views.size()];
        int viewIndex = 0;
        for (DataView view : views) {
            idAndName[0][viewIndex] = view.getId();
            idAndName[1][viewIndex++] = view.getDisplayName();
        }
        return idAndName;
    }

    @Override
    public String getSnapshotDirectory() {
        return snapshotEventDelegate.getSnapshotDirectory();
    }

    @Override
    public void setSnapshotDirectory(String snapshotDirectory) {
        snapshotEventDelegate.setSnapshotDirectory(snapshotDirectory);
    }

    @Override
    public String getSnapshotFile() {
        return snapshotEventDelegate.getSnapshotFile();
    }

    @Override
    public void addDataTransferListener(IDataTransferListener listener) {
        transferEventDelegate.addDataTransferListener(listener);
    }

    @Override
    public void removeDataTransferListener(IDataTransferListener listener) {
        transferEventDelegate.removeDataTransferListener(listener);
    }

    @Override
    public String getDataDirectory() {
        return transferEventDelegate.getDataDirectory();
    }

    @Override
    public void setDataDirectory(String path) {
        transferEventDelegate.setDataDirectory(path);
    }

    @Override
    public String getDataFile() {
        return transferEventDelegate.getDataFile();
    }

    @Override
    public void loadDataFile(final String fileName) {
        BorderLayout layout = (BorderLayout) getLayout();
        northComponent = layout.getLayoutComponent(BorderLayout.NORTH);
        if (northComponent != null) {
            remove(northComponent);
        }
        final boolean wasEnabled = chartMenu.isDataFileEnabled();
        chartMenu.setDataFileEnabled(false);
        add(loadDataLabel, BorderLayout.NORTH);
        if (isShowing()) {
            revalidate();
            repaint();
        }
        SwingWorker<ChartData<DataView>, Void> worker = new SwingWorker<ChartData<DataView>, Void>() {
            @Override
            protected ChartData<DataView> doInBackground() {
                return ChartUtils.loadDataFile(DataView.class, isDataViewsSortedOnX(), fileName, indexColumnName,
                        dateColumnName, isGenerateRandomIDsForLoadedData(), Chart.this);
            }

            @Override
            protected void done() {
                remove(loadDataLabel);
                if (northComponent != null) {
                    add(northComponent, BorderLayout.NORTH);
                }
                ChartData<DataView> data;
                try {
                    data = get();
                } catch (Exception e) {
                    data = null;
                }
                addChartData(data, wasEnabled);
            }
        };
        worker.execute();
    }

    @Override
    public void saveDataFile(String path) {
        // Save only visible views (JAVAAPI-530, PROBLEM-1568)
        List<? extends AbstractDataView> views = dataViewList.getDataViews(dataViewComparator, X, Y1, Y2);

        AbstractDataView[] viewArray = views.toArray(new AbstractDataView[views.size()]);
        views.clear();
        try {
            ChartUtils.saveDataFile(path, getIndexColumnName(), getDateColumnName(), getTimePrecision(),
                    getNoValueString(), chartView.getAxis(X).getScale().isTimeScale(), isUseDisplayNameForDataSaving(),
                    viewArray);
            updateLastDataFileLocation(path, true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error during saving file.\n" + e.getMessage());
        }
    }

    protected ChartView generateChartView() {
        return new ChartView(chartEventDelegate, dataViewList);
    }

    protected ChartLegend generateChartLegend() {
        return new ChartLegend(this);
    }

    // JIRA JAVAAPI-39
    @Override
    public void setLabels(int axis, String[] labels, double[] labelPositions) {
        chartView.getAxis(axis).getScale().setLabels(labels, labelPositions);
    }

    @Override
    public void clearLabels(int axis) {
        setLabels(axis, null, null);
    }

    @Override
    protected void finalize() {
        data.clear();
        expressionMap.clear();
        dataViewList.clearDataView();
        plotPropertiesMap.clear();
        freezeIds.clear();
        if (displayedIds != null) {
            Arrays.fill(displayedIds, null);
            displayedIds = null;
        }
        barColorRotationTool = null;
        markerColorRotationTool = null;
        lineStyleRotationTool = null;
        markerStyleRotationTool = null;
        markerSizeRotationTool = null;
        lineWidthRotationTool = null;
        barWidthRotationTool = null;
        viewStyleRotationTool = null;
        fillStyleRotationTool = null;
        axisRotationTool = null;
        dataViewOption = null;
        dataViewOptionDialog = null;
        chartOption = null;
        chartOptionDialog = null;
        expressionOption = null;
        expressionOptionDialog = null;
        noValueString = null;
        preferredSnapshotExtension = null;
        dataViewComparator = null;
        dataTableWindow = null;
        indexColumnName = null;
        dateColumnName = null;
        settingsDirectory = null;
        expressionParser = null;
        northComponent = null;
    }

    @Override
    public boolean dataChanged() {
        return dataComponentDelegate.dataChanged(this);
    }

    @Override
    public void updateFromData() {
        Boolean shouldMeasureItems = this.shouldMeasureItems;
        this.shouldMeasureItems = null;
        Boolean shouldRevalidate = this.shouldRevalidate;
        List<DataView>[] updatedViews = this.updatedViews;
        this.updatedViews = null;
        boolean shouldRefresh = shouldMeasureItems == null ? false : shouldMeasureItems.booleanValue();
        boolean revalidate = shouldRevalidate == null ? false : shouldRevalidate.booleanValue();
        getDrawingThreadManager().runInDrawingThread(() -> {
            if (revalidate) {
                if (isLegendVisible()) {
                    chartLegend.revalidate();
                    chartSplitPane.revalidate();
                }
                revalidate();
            }
            refreshVisibilityBoxes(shouldRefresh, updatedViews);
            updateFreezePanelAvailability();
        });
    }

}
