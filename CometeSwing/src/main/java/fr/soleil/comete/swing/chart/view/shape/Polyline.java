/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import fr.soleil.comete.definition.widget.util.IChartConst.ShapeType;

/**
 * {@link AbstractShape} specialized in poly-lines
 * 
 * @author huriez
 */
public class Polyline extends Polygon {

    private BasicStroke basicStroke;

    public Polyline(ShapeType type) {
        super(type);
    }

    public Polyline(ShapeType type, int[] point, int[] point2, int nbPoint, Color color) {
        super(type, point, point2, nbPoint, false, color);
    }

    /**
     * @param basicStroke the basicStroke to set
     */
    public void setBasicStroke(BasicStroke basicStroke) {
        this.basicStroke = basicStroke;
    }

    /**
     * @return the basicStroke
     */
    public BasicStroke getBasicStroke() {
        return basicStroke;
    }

    @Override
    public void drawShape(Graphics2D g) {
        Stroke oldStroke = null;
        if (basicStroke != null) {
            oldStroke = g.getStroke();
            g.setStroke(basicStroke);
        }
        g.setColor(color);
        g.translate(x, y);
        g.drawPolyline(getXPoint(), getYPoint(), getNbPoint());
        g.translate(-x, -y);
        if (oldStroke != null) {
            g.setStroke(oldStroke);
        }
    }

}
