/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.ChartObservable;
import fr.soleil.comete.definition.widget.util.ChartObserver;
import fr.soleil.comete.definition.widget.util.IChartConst.UpdateType;
import fr.soleil.comete.swing.chart.util.DataViewComparator;
import fr.soleil.data.mediator.Mediator;

/**
 * Class that contains {@link AbstractDataView}'s coordinates
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DataViewList implements ChartObservable {

    private final ConcurrentHashMap<String, DataView> viewMap;

    private final Collection<ChartObserver> listeners;

    public DataViewList() {
        // Use a ConcurrentHashMap for Thread safety
        viewMap = new ConcurrentHashMap<>();
        listeners = Collections.newSetFromMap(new ConcurrentHashMap<ChartObserver, Boolean>());
    }

    public DataViewList(DataView singleton) {
        this();
        addDataView(singleton);
    }

    /**
     * Returns the number of {@link DataView}s contained in this {@link DataViewList}
     * 
     * @return An <code>int</code>
     */
    public int size() {
        return viewMap.size();
    }

    /**
     * Adds a DataView in this list
     * 
     * @param view
     *            The dataview to add.
     */
    public void addDataView(DataView view) {
        if (view != null) {
            viewMap.put(view.getId(), view);
        }
    }

    /**
     * Adds a DataView in this list, if no similar one already present.
     * 
     * @param view The dataview to add.
     * @return The added {@link DataView}, or its predecessor if already present
     */
    public DataView addDataViewIfAbsent(DataView view) {
        DataView result = view;
        if (view != null) {
            result = viewMap.putIfAbsent(view.getId(), view);
            if (result == null) {
                result = view;
            }
        }
        return result;
    }

    /**
     * Adds all the {@link DataView}s of a {@link DataViewList} in this {@link DataViewList}
     * 
     * @param list The {@link DataViewList}
     */
    public void addDataViews(DataViewList list) {
        if (list != this) {
            addDataViews(list.getListCopy(false));
        }
    }

    /**
     * Adds all the {@link DataView}s of a {@link Collection} in this {@link DataViewList}
     * 
     * @param list The {@link DataView} {@link Collection}
     */
    public void addDataViews(Collection<? extends DataView> list) {
        if (list != null) {
            for (DataView view : list) {
                viewMap.put(view.getId(), view);
            }
        }
    }

    /**
     * Recovers a {@link DataView}.
     * 
     * @param id The {@link DataView} id
     * @param yOnly Whether to search only for a {@link DataView} on an y axis. If <code>true</code>, the matching
     *            {@link DataView} won't be returned if not on an y axis.
     * @return A {@link DataView}
     */
    public DataView getDataView(String id, boolean yOnly) {
        DataView dataview = null;
        if (yOnly) {
            dataview = getDataView(id, IChartViewer.Y1, IChartViewer.Y2);
        } else {
            dataview = viewMap.get(id);
        }
        return dataview;
    }

    /**
     * Recovers a {@link DataView} that has a given id and is associated with some axis
     * 
     * @param id The {@link DataView} id
     * @param axis The axis identifiers
     * @return A {@link DataView}. <code>null</code> if no such {@link DataView} found
     */
    public DataView getDataView(String id, int... axis) {
        DataView result = viewMap.get(id);
        if (result != null) {
            boolean axisOk = false;
            for (int currentAxis : axis) {
                if (currentAxis == result.getAxis().intValue()) {
                    axisOk = true;
                    break;
                }
            }
            if (!axisOk) {
                result = null;
            }
        }
        return result;
    }

    /**
     * Recovers all dataviews associated with some axis
     * 
     * @param axis The axis identifiers
     * @return A {@link DataViewList}
     */
    public DataViewList getDataViewList(int... axis) {
        DataViewList result = null;
        if (axis != null) {
            result = new DataViewList();
            boolean added = false;
            for (DataView view : viewMap.values()) {
                for (int axisToCheck : axis) {
                    if (view.getAxis().intValue() == axisToCheck) {
                        if (axisToCheck == IChartViewer.X) {
                            if (!added) {
                                result.addDataView(view);
                                added = true;
                            }
                        } else {
                            result.addDataView(view);
                        }
                        break;
                    }
                }
            }
        }
        return result;
    }

    protected boolean tryToAddDataView(DataView view, Collection<DataView> views, int axis, boolean previousAdded) {
        boolean added = previousAdded;
        if (view.getAxis().intValue() == axis) {
            if (axis == IChartViewer.X) {
                if (!added) {
                    views.add(view);
                    added = true;
                }
            } else {
                views.add(view);
            }
        }
        return added;
    }

    /**
     * Extracts the {@link DataView}s that are associated to some axis, and returns an array of {@link List}s, in the
     * same order as the given axis array.
     * 
     * @param sorted Whether the {@link DataView}s should be sorted by display name in {@link List}s
     * @param axis The desired axis
     * @return A <code>{@link List}[]</code>
     */
    public List<DataView>[] getDataViewsByAxis(boolean sorted, int... axis) {
        return getDataViewsByAxis(sorted ? new DataViewComparator() : null, axis);
    }

    /**
     * Extracts the {@link DataView}s that are associated to some axis, and returns an array of {@link List}s, in the
     * same order as the given axis array.
     * 
     * @param comparator The comparator that defines how {@link DataView}s should be sorted. <code>null</code> to avoid
     *            sorting.
     * @param axis The desired axis
     * @return A <code>{@link List}[]</code>
     */
    public List<DataView>[] getDataViewsByAxis(Comparator<AbstractDataView> comparator, int... axis) {
        List<DataView>[] result;
        if (axis == null) {
            result = null;
        } else {
            @SuppressWarnings("unchecked")
            List<DataView>[] array = new List[axis.length];
            result = array;
            for (int i = 0; i < axis.length; i++) {
                result[i] = new ArrayList<>();
            }
            boolean added = false;
            for (DataView view : viewMap.values()) {
                int axisIndex = 0;
                for (int axisToCheck : axis) {
                    added = tryToAddDataView(view, result[axisIndex++], axisToCheck, added);
                }
            }
            if (comparator != null) {
                try {
                    for (final List<DataView> list : result) {
                        Collections.sort(list, comparator);
                    }
                } catch (OutOfMemoryError oome) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to sort dataviews");
                }
            }
        }
        return result;
    }

    /**
     * Computes an array of 2 {@link List}s of {@link DataView}s. The first {@link List} contains the {@link DataView}s
     * that are associated to a given axis. The second one contains the {@link DataView}s that are associated to some
     * ot.her axis
     * 
     * @param sorted Whether the {@link DataView}s should be sorted by display name in {@link List}s
     * @param separatedAxis The axis to match in first {@link List}
     * @param axis The axis to match in second {@link List}. If <code>null</code>, the resulting array will be of length
     *            1.
     * @return A <code>{@link List}[]</code> of length 2. This array will be of length 1 only if <code>axis</code> is
     *         <code>null</code>.
     */
    public List<DataView>[] getDataViewsByBlock(boolean sorted, int separatedAxis, int... axis) {
        return getDataViewsByBlock(sorted ? new DataViewComparator() : null, separatedAxis, axis);
    }

    /**
     * Computes an array of 2 {@link List}s of {@link DataView}s. The first {@link List} contains the {@link DataView}s
     * that are associated to a given axis. The second one contains the {@link DataView}s that are associated to some
     * ot.her axis
     * 
     * @param comparator The comparator that defines how {@link DataView}s should be sorted. <code>null</code> to avoid
     *            sorting.
     * @param separatedAxis The axis to match in first {@link List}
     * @param axis The axis to match in second {@link List}. If <code>null</code>, the resulting array will be of length
     *            1.
     * @return A <code>{@link List}[]</code> of length 2. This array will be of length 1 only if <code>axis</code> is
     *         <code>null</code>.
     */
    public List<DataView>[] getDataViewsByBlock(Comparator<AbstractDataView> comparator, int separatedAxis,
            int... axis) {
        List<DataView>[] result;
        if (axis == null) {
            @SuppressWarnings("unchecked")
            List<DataView>[] array = new List[] { new ArrayList<>() };
            result = array;
            boolean added = false;
            for (DataView view : viewMap.values()) {
                added = tryToAddDataView(view, result[0], separatedAxis, added);
            }
        } else {
            @SuppressWarnings("unchecked")
            List<DataView>[] array = new List[] { new ArrayList<>(), new ArrayList<>() };
            result = array;
            boolean added = false;
            for (DataView view : viewMap.values()) {
                added = tryToAddDataView(view, result[0], separatedAxis, added);
                List<DataView> list = result[1];
                for (int axisToCheck : axis) {
                    added = tryToAddDataView(view, list, axisToCheck, added);
                }
            }
        }
        if (comparator != null) {
            try {
                for (final List<DataView> list : result) {
                    Collections.sort(list, comparator);
                }
            } catch (OutOfMemoryError oome) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to sort dataviews");
            }
        }
        return result;
    }

    /**
     * Recovers all {@link DataView}s associated with some axis.
     * 
     * @param sorted Whether the {@link DataView}s should be sorted by display name in {@link List}.
     * @param axis The axis identifiers.
     * @return A {@link DataView} {@link List}.
     */
    public List<DataView> getDataViews(boolean sorted, int... axis) {
        return getDataViews(sorted ? new DataViewComparator() : null, axis);
    }

    /**
     * Recovers all {@link DataView}s associated with some axis.
     * 
     * @param comparator The comparator that defines how {@link DataView}s should be sorted. <code>null</code> to avoid
     *            sorting.
     * @param axis The axis identifiers.
     * @return A {@link DataView} {@link List}.
     */
    public List<DataView> getDataViews(Comparator<AbstractDataView> comparator, int... axis) {
        List<DataView> result = new ArrayList<>();
        if (axis != null) {
            boolean added = false;
            for (DataView view : viewMap.values()) {
                for (int axisToCheck : axis) {
                    added = tryToAddDataView(view, result, axisToCheck, added);
                }
            }
        }
        if (comparator != null) {
            try {
                Collections.sort(result, comparator);
            } catch (OutOfMemoryError oome) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to sort dataviews");
            }
        }
        return result;
    }

    /**
     * Removes {@link DataView} from this axis
     * 
     * @param view {@link DataView} to remove from this axis.
     * @return The removed DataView.
     */
    public DataView removeDataView(String id) {
        DataView removed = null;
        if ((id != null) && (!viewMap.isEmpty())) {
            removed = viewMap.remove(id);
        }
        return removed;
    }

    /**
     * Clears all dataviews
     */
    public void clearDataView() {
        for (DataView view : viewMap.values()) {
            if (view.hasFilter()) {
                view.clearFilter();
            }
            view.reset();
        }
        viewMap.clear();
    }

    /**
     * Returns the {@link DataView} associated to x axis, if any.
     * 
     * @return A {@link DataView}.
     */
    public DataView getXView() {
        DataView result = null;
        for (DataView view : viewMap.values()) {
            if (view.getAxis().intValue() == IChartViewer.X) {
                result = view;
                break;
            }
        }
        return result;
    }

    public boolean isEmpty() {
        return viewMap.isEmpty();
    }

    /**
     * Returns a new {@link List} containing all the {@link DataView}s contained in this {@link DataViewList}.
     * 
     * @return A {@link List}, never <code>null</code>.
     */
    public List<DataView> getListCopy() {
        return getListCopy(false);
    }

    /**
     * Returns a new {@link List} containing all the {@link DataView}s contained in this {@link DataViewList}.
     * 
     * @param sorted Whether the {@link DataView}s should be sorted by display name in {@link List}.
     * @return A {@link List}.
     */
    public List<DataView> getListCopy(boolean sorted) {
        return getListCopy(sorted ? new DataViewComparator() : null);
    }

    /**
     * Returns a new {@link List} containing all the {@link DataView}s contained in this {@link DataViewList}
     * 
     * @param comparator The comparator that defines how {@link DataView}s should be sorted. <code>null</code> to avoid
     *            sorting.
     * @return A {@link List}.
     */
    public List<DataView> getListCopy(Comparator<AbstractDataView> comparator) {
        List<DataView> copy = new ArrayList<>();
        copy.addAll(viewMap.values());
        if (comparator != null) {
            Collections.sort(copy, comparator);
        }
        return copy;
    }

    @Override
    public void addObserver(ChartObserver obs) {
        listeners.add(obs);
    }

    @Override
    public void notifyObserver(UpdateType updateType) {
        for (ChartObserver obs : listeners) {
            obs.update(updateType);
        }
    }

    @Override
    public void removeObserver() {
        listeners.clear();
    }

}
