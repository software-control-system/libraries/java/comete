/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import fr.soleil.comete.swing.TextArea;

/**
 * A {@link TextArea} for which scrollpane preferred size can be recalculated on user demand
 * 
 * @author Rapha&euml;l GIRARDOT
 *
 */
public class DynamicSizeTextArea extends TextArea {

    private static final long serialVersionUID = 5155075743087977317L;

    public DynamicSizeTextArea() {
        super();
    }

    /**
     * Recalculates scrollpane preferred size
     */
    public void updateScrollPane() {
        updateScrollPane(true);
    }

}
