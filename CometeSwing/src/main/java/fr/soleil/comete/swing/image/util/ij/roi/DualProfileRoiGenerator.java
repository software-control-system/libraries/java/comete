/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.image.ijviewer.components.ArrowStroke;
import fr.soleil.comete.swing.image.util.ProfileDataCalculator;
import fr.soleil.lib.project.swing.icons.Icons;
import ij.gui.Roi;
import ij.gui.Toolbar;

public class DualProfileRoiGenerator extends DataRoiGenerator implements SwingConstants {

    protected static final Cursor CURSOR = Icons.generateCursorFromIcon(ImageViewer.DUAL_PROFILE_ICON,
            ImageViewer.DUAL_PROFILE_ACTION);
    protected static final Stroke STROKE = new ArrowStroke(IJCanvas.DEFAULT_DUAL_PROFILE_ROI_STROKE, ArrowStroke.END,
            12, Math.PI / 5);

    protected int horizontalBarAlignment, verticalBarAlignment;

    public DualProfileRoiGenerator(String text, String description, ImageViewer viewer) {
        super(text, description, ImageViewer.DUAL_PROFILE_ICON, viewer);
        horizontalBarAlignment = CENTER;
        verticalBarAlignment = CENTER;
    }

    @Override
    public boolean isAxisDependent() {
        return true;
    }

    @Override
    public boolean isPixelSizeDependent() {
        return true;
    }

    @Override
    public boolean isZoomDependent() {
        return true;
    }

    @Override
    public int getRoiMode() {
        return IJRoiManager.DUAL_PROFILE_ROI_MODE;
    }

    @Override
    public Roi createNewRoi(int sx, int sy, IJRoiManager roiManager) {
        Roi cross = new CrossRoi(sx, sy, horizontalBarAlignment, verticalBarAlignment, roiManager);
        cross.setName(ImageViewer.DUAL_PROFILE_NAME);
        return cross;
    }

    @Override
    public boolean isCompatibleWithRoi(Roi roi) {
        return ((roi instanceof CrossRoi) && ImageViewer.DUAL_PROFILE_NAME.equals(roi.getName()));
    }

    @Override
    public Cursor getCursor() {
        return CURSOR;
    }

    @Override
    protected int getTool() {
        return Toolbar.RECTANGLE;
    }

    @Override
    protected boolean isDialogVisible(ImageViewer viewer) {
        return false;
    }

    @Override
    protected void doShowDataDialog(ImageViewer viewer) {
        if (viewer != null) {
            if ((getVerticalProfiler(viewer) == null) || (!viewer.isHorizontalDataForVerticalProfiler())) {
                viewer.getImageScrollPane().setColumnHeaderView(viewer.getHorizontalProfilerPanel());
                viewer.getImageScrollPane().setRowHeaderView(viewer.getVerticalProfilerPanel());
                viewer.getHorizontalProfilerPanel().setBackground(viewer.getImageScrollPane().getBackground());
                viewer.getVerticalProfilerPanel().setBackground(viewer.getImageScrollPane().getBackground());
                viewer.getImageScrollPane().setCorner(ScrollPaneConstants.UPPER_LEFT_CORNER,
                        viewer.getProfilerSizeButtonPanel());
                viewer.getProfilerSizeButtonPanel().setBackground(viewer.getImageScrollPane().getBackground());
            }
        }
    }

    @Override
    protected void doHideDataDialog(ImageViewer viewer) {
        if (viewer != null) {
            JScrollPane scrollPane = viewer.getImageScrollPane();
            if ((scrollPane.getColumnHeader() != null) && (scrollPane.getColumnHeader().getView() != null)) {
                scrollPane.setColumnHeaderView(null);
            }
            if ((scrollPane.getRowHeader() != null) && (scrollPane.getRowHeader().getView() != null)) {
                scrollPane.setRowHeaderView(null);
            }
        }
    }

    @Override
    protected void updateDataBounds(ImageViewer viewer) {
        // Nothing to do here
    }

    protected void updateAxisRange(ImageViewer viewer) {
        if (viewer != null) {
            CrossRoi roi = (CrossRoi) viewer.getRoiManager().getSpecialRoi(getReferentRoiGenerator());
            boolean forceWidth, forceHeight;
            if (roi == null) {
                forceWidth = true;
                forceHeight = true;
            } else {
                Rectangle bounds = roi.getBounds();
                forceWidth = (((bounds.x == 0) && (bounds.width == viewer.getDimX()))
                        || bounds.contains(new Rectangle(0, 0, viewer.getDimX(), 1)));
                forceHeight = (((bounds.y == 0) && (bounds.height == viewer.getDimY()))
                        || bounds.contains(new Rectangle(0, 0, 1, viewer.getDimY())));
            }
            Chart verticalProfiler = getVerticalProfiler(viewer, false);
            if (verticalProfiler != null) {
                int axisType = viewer.isHorizontalDataForVerticalProfiler() ? IChartViewer.Y1 : IChartViewer.X;
                double[] range = forceHeight ? viewer.getAxisRangeY() : null;
                updateRange(verticalProfiler, range, axisType);
            }
            Chart horizontalProfiler = getHorizontalProfiler(viewer, false);
            if (horizontalProfiler != null) {
                int axisType = IChartViewer.Y1;
                double[] range = forceWidth ? viewer.getAxisRangeX() : null;
                updateRange(horizontalProfiler, range, axisType);
            }
            viewer.profilesFollowImage();
        }
    }

    protected void updateRange(Chart profiler, double[] range, int axisType) {
        profiler.setAutoScale(false, axisType);
        if (range != null) {
            updateAxisProperties(profiler, axisType, range[0], range[1]);
        }
        double[][] data = null;
        for (Object tmp : profiler.getData().values()) {
            if (tmp instanceof double[][]) {
                data = (double[][]) tmp;
                break;
            }
        }

        double xMin, xMax, yMin, yMax;
        if (data == null) {
            xMin = 0;
            xMax = 1;
            yMin = 0;
            yMax = 1;
        } else {
            xMin = Double.POSITIVE_INFINITY;
            xMax = Double.NEGATIVE_INFINITY;
            yMin = Double.POSITIVE_INFINITY;
            yMax = Double.NEGATIVE_INFINITY;
            int length = data[0].length;
            for (int i = 0; i < length; i++) {
                double xValue = data[IChartViewer.X_INDEX][i], yValue = data[IChartViewer.Y_INDEX][i];
                if ((!Double.isNaN(xValue)) && (!Double.isInfinite(xValue))) {
                    if (xValue > xMax) {
                        xMax = xValue;
                    }
                    if (xValue < xMin) {
                        xMin = xValue;
                    }
                }
                if ((!Double.isNaN(yValue)) && (!Double.isInfinite(yValue))) {
                    if (yValue > yMax) {
                        yMax = yValue;
                    }
                    if (yValue < yMin) {
                        yMin = yValue;
                    }
                }
            }
        }
        if (xMax < xMin) {
            xMin = 0;
            xMax = 1;
        } else if (xMax == xMin) {
            xMax++;
            xMin--;
        } else {
            xMin *= 0.999;
            xMax *= 1.001;
        }
        if (yMax < yMin) {
            yMin = 0;
            yMax = 1;
        } else if (yMax == yMin) {
            yMax++;
            yMin--;
        } else {
            yMin *= 0.999;
            yMax *= 1.001;
        }

        if (range == null) {
            if (axisType == IChartViewer.Y1) {
                updateAxisProperties(profiler, IChartViewer.Y1, yMin, yMax);
            } else {
                updateAxisProperties(profiler, IChartViewer.X, xMin, xMax);
            }
        }
        if (axisType == IChartViewer.Y1) {
            updateAxisProperties(profiler, IChartViewer.X, xMin, xMax);
        } else {
            updateAxisProperties(profiler, IChartViewer.Y1, yMin, yMax);
        }
    }

    protected void updateAxisProperties(Chart profiler, int axis, double min, double max) {
        AxisProperties properties = profiler.getAxisProperties(axis);
        properties.setAutoScale(false);
        properties.setScaleMin(min);
        properties.setScaleMax(max);
        profiler.setAxisProperties(properties, axis);
    }

    protected int getDataIndex(int axis) {
        int index;
        if (axis == IChartViewer.X) {
            index = IChartViewer.X_INDEX;
        } else {
            index = IChartViewer.Y_INDEX;
        }
        return index;
    }

    protected Chart getVerticalProfiler(ImageViewer viewer) {
        return getVerticalProfiler(viewer, true);
    }

    protected Chart getVerticalProfiler(ImageViewer viewer, boolean constructionAllowed) {
        return viewer.getVerticalProfiler(true, constructionAllowed);
    }

    protected Chart getHorizontalProfiler(ImageViewer viewer) {
        return getHorizontalProfiler(viewer, true);
    }

    protected Chart getHorizontalProfiler(ImageViewer viewer, boolean constructionAllowed) {
        return viewer.getHorizontalProfiler(constructionAllowed);
    }

    @Override
    protected void updateData(IMaskedImageViewer context) {
        ImageViewer viewer;
        if (context instanceof ImageViewer) {
            viewer = (ImageViewer) context;
        } else {
            viewer = null;
        }
        if (viewer != null) {
            Object value = viewer.getValue();
            CrossRoi roi = (CrossRoi) viewer.getRoiManager().getSpecialRoi(getReferentRoiGenerator());
            if ((roi == null) || (value == null)) {
                Chart horizontalProfiler = getHorizontalProfiler(viewer, false);
                if (horizontalProfiler != null) {
                    horizontalProfiler.setData(null);
                }
                Chart verticalProfiler = getVerticalProfiler(viewer, false);
                if (verticalProfiler != null) {
                    verticalProfiler.setData(null);
                }
            } else {
                Rectangle hBounds = roi.getHorizontalBarBounds();
                Rectangle vBounds = roi.getVerticalBarBounds();
                double[][] profileData = ProfileDataCalculator.getProfileTables(viewer.getDimX(), viewer.getDimY(),
                        hBounds.x, vBounds.x, hBounds.y, vBounds.y, hBounds.width, vBounds.height, value,
                        viewer.getImageScreenPositionCalculator());
                double[] profileV = profileData[0];
                double[] profileH = profileData[1];
                double[] yValues = profileData[2];
                double[] xValues = profileData[3];
                if (getVerticalProfiler(viewer) != null) {
                    Map<String, Object> verticalProfileMap = new HashMap<String, Object>();
                    double[][] verticalProfileData = new double[2][];
                    if (viewer.isHorizontalDataForVerticalProfiler()) {
                        verticalProfileData[IChartViewer.X_INDEX] = yValues;
                        verticalProfileData[IChartViewer.Y_INDEX] = profileV;
                    } else {
                        verticalProfileData[IChartViewer.X_INDEX] = profileV;
                        verticalProfileData[IChartViewer.Y_INDEX] = yValues;
                    }
                    verticalProfileMap.put(ImageViewer.VERTICAL_PROFILE_NAME, verticalProfileData);
                    getVerticalProfiler(viewer).setData(verticalProfileMap);
                }
                if (getHorizontalProfiler(viewer) != null) {
                    Map<String, Object> horizontalProfileMap = new HashMap<String, Object>();
                    horizontalProfileMap.put(ImageViewer.HORIZONTAL_PROFILE_NAME, new double[][] { xValues, profileH });
                    getHorizontalProfiler(viewer).setData(horizontalProfileMap);
                }
            }
        }
        updateAxisRange(viewer);
    }

    @Override
    public String getActionCommand() {
        return ImageViewer.DUAL_PROFILE_ACTION;
    }

    @Override
    protected Stroke generateRoiStroke() {
        return STROKE;
    }

    @Override
    protected Color generateRoiColor() {
        return ImageViewer.DEFAULT_CROSSHAIR_COLOR;
    }

    @Override
    public boolean keyPressed(KeyEvent e) {
        boolean treated = false;
        if (e != null) {
            ImageViewer viewer = recoverViewer(e.getComponent());
            if (viewer != null) {
                CrossRoi roi = (CrossRoi) viewer.getRoiManager().getSpecialRoi(getReferentRoiGenerator());
                if (isAltKeyCodes(viewer)) {
                    switch (e.getKeyCode()) {
                        case KeyEvent.VK_UP:
                            if (horizontalBarAlignment == BOTTOM) {
                                horizontalBarAlignment = CENTER;
                            } else if (horizontalBarAlignment == CENTER) {
                                horizontalBarAlignment = TOP;
                            }
                            treated = true;
                            break;
                        case KeyEvent.VK_DOWN:
                            if (horizontalBarAlignment == TOP) {
                                horizontalBarAlignment = CENTER;
                            } else if (horizontalBarAlignment == CENTER) {
                                horizontalBarAlignment = BOTTOM;
                            }
                            treated = true;
                            break;
                        case KeyEvent.VK_LEFT:
                            if (verticalBarAlignment == RIGHT) {
                                verticalBarAlignment = CENTER;
                            } else if (verticalBarAlignment == CENTER) {
                                verticalBarAlignment = LEFT;
                            }
                            treated = true;
                            break;
                        case KeyEvent.VK_RIGHT:
                            if (verticalBarAlignment == LEFT) {
                                verticalBarAlignment = CENTER;
                            } else if (verticalBarAlignment == CENTER) {
                                verticalBarAlignment = RIGHT;
                            }
                            treated = true;
                            break;
                        case KeyEvent.VK_A:
                            viewer.getRoiManager().deleteRoi(roi);
                            roi = new CrossRoi(0, 0, viewer.getDimX(), viewer.getDimY(), horizontalBarAlignment,
                                    verticalBarAlignment, viewer.getRoiManager());
                            roi.setName(ImageViewer.DUAL_PROFILE_NAME);
                            viewer.getRoiManager().setSpecialRoi(getReferentRoiGenerator(), roi);
                            viewer.getRoiManager().setHandledRoi(roi);
                            viewer.repaint();
                            treated = true;
                            break;
                    }
                } else {
                    treated = super.keyPressed(e);
                }
                if (roi != null) {
                    roi.setHorizontalBarAlignment(horizontalBarAlignment);
                    roi.setVerticalBarAlignment(verticalBarAlignment);
                }
                if (treated) {
                    updateData(viewer);
                }
            }
        }
        return treated;
    }

    @Override
    public boolean keyReleased(KeyEvent e) {
        boolean treated = super.keyReleased(e);
        if (!treated) {
            if (isAltKeyCodes(recoverViewer(e.getComponent()))) {
                treated = (e.getKeyCode() == KeyEvent.VK_A);
            }
        }
        return treated;
    }
}
