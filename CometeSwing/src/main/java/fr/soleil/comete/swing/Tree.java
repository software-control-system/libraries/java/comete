/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Dimension;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.ToolTipManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.event.TreeNodeSelectionEvent;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.listener.ITreeNodeSelectionListener;
import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.util.CometeTreeCellRenderer;
import fr.soleil.comete.swing.util.CometeTreeModel;
import fr.soleil.comete.swing.util.DefaultCometeTreeModel;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.tree.ExpandableTree;

/**
 * CometeSwing implementation of {@link ITree}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class Tree extends ExpandableTree implements ITree, TreeSelectionListener {

    private static final long serialVersionUID = 8923067130498378916L;

    protected final TargetDelegate targetDelegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;
    protected final List<WeakReference<ITreeNodeSelectionListener>> selectionListeners;
    protected ITreeNode rootNode;
    protected int selectionMode;

    /**
     * Default constructor
     */
    public Tree() {
        this(new DefaultCometeTreeModel(null));
    }

    /**
     * Constructs this {@link Tree} with a {@link CometeTreeModel}
     * 
     * @param model The {@link CometeTreeModel} to use
     */
    public Tree(CometeTreeModel model) {
        super(model == null ? new DefaultCometeTreeModel(null) : model);
        selectionMode = ITree.MULTI_SELECTION;
        targetDelegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        selectionListeners = new ArrayList<WeakReference<ITreeNodeSelectionListener>>();
        addTreeSelectionListener(this);
        setCellRenderer(new CometeTreeCellRenderer());
        // Call this method to manage the tooltip set on ITreeNode
        ToolTipManager.sharedInstance().registerComponent(this);
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        if (e != null) {
            TreePath[] paths = e.getPaths();
            if (paths != null) {
                List<TreePath> added = new ArrayList<TreePath>();
                List<TreePath> removed = new ArrayList<TreePath>();
                for (int i = 0; i < paths.length; i++) {
                    if (e.isAddedPath(i)) {
                        added.add(paths[i]);
                    } else {
                        removed.add(paths[i]);
                    }
                }
                List<ITreeNode> nodes = extractNodes(removed);
                if (!nodes.isEmpty()) {
                    warnSelectionListeners(
                            new TreeNodeSelectionEvent(this, false, nodes.toArray(new ITreeNode[nodes.size()])));
                    nodes.clear();
                }
                nodes = extractNodes(added);
                if (!nodes.isEmpty()) {
                    warnSelectionListeners(
                            new TreeNodeSelectionEvent(this, true, nodes.toArray(new ITreeNode[nodes.size()])));
                    nodes.clear();
                }
            }
        }
    }

    /**
     * Extracts the {@link ITreeNode}s that correspond to some {@link TreePath}s
     * 
     * @param paths The {@link TreePath}s from which to extract the {@link ITreeNode}s
     * @return an {@link ITreeNode} {@link List}
     */
    protected List<ITreeNode> extractNodes(List<TreePath> paths) {
        List<ITreeNode> nodes = new ArrayList<ITreeNode>();
        CometeTreeModel model = getModel();
        if ((paths != null) && (!paths.isEmpty()) && (model != null)) {
            for (TreePath path : paths) {
                if (path.getLastPathComponent() instanceof DefaultMutableTreeNode) {
                    nodes.add(model.getTreeNode((DefaultMutableTreeNode) path.getLastPathComponent()));
                }
            }
        }
        return nodes;
    }

    @Override
    public void setModel(TreeModel newModel) {
        if (newModel instanceof CometeTreeModel) {
            super.setModel(newModel);
            getModel().setRoot(rootNode);
        }
    }

    @Override
    public CometeTreeModel getModel() {
        return (CometeTreeModel) super.getModel();
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        setBackground(ColorTool.getColor(color));
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(getBackground());
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        setForeground(ColorTool.getColor(color));
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(getForeground());
    }

    @Override
    public void setCometeFont(CometeFont font) {
        setFont(FontTool.getFont(font));
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(getFont());
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // not managed
    }

    @Override
    public int getHorizontalAlignment() {
        return 0;
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        targetDelegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        targetDelegate.removeMediator(mediator);
    }

    @Override
    public ITreeNode getRootNode() {
        return rootNode;
    }

    @Override
    public void setRootNode(ITreeNode rootNode) {
        if (!ObjectUtils.sameObject(rootNode, this.rootNode)) {
            this.rootNode = rootNode;
            CometeTreeModel model = getModel();
            if (model != null) {
                model.setRoot(rootNode);
            }
        }
    }

    @Override
    public ITreeNode getNodeAt(int x, int y) {
        ITreeNode result = null;
        CometeTreeModel model = getModel();
        if (model != null) {
            TreePath path = getPathForLocation(x, y);
            if (path != null) {
                Object comp = path.getLastPathComponent();
                if (comp instanceof DefaultMutableTreeNode) {
                    result = model.getTreeNode((DefaultMutableTreeNode) comp);
                }
            }
        }
        return result;
    }

    @Override
    public void selectNodes(boolean keepPreviousSelection, ITreeNode... nodes) {
        TreePath[] toSelect = getPaths(nodes);
        if (toSelect != null) {
            if (keepPreviousSelection) {
                addSelectionPaths(toSelect);
            } else {
                setSelectionPaths(toSelect);
            }
        }
    }

    @Override
    public void deselectNodes(ITreeNode... nodes) {
        TreePath[] toSelect = getPaths(nodes);
        if (toSelect != null) {
            removeSelectionPaths(toSelect);
        }
    }

    @Override
    public ITreeNode[] getSelectedNodes() {
        return getSelectedNodes(false);
    }

    public ITreeNode[] getSelectedNodes(boolean sortBytTreeOrder) {
        ITreeNode[] selection = null;
        CometeTreeModel model = getModel();
        if (model != null) {
            TreePath[] paths = getSelectionPaths();
            if (paths != null) {
                if (sortBytTreeOrder) {
                    List<TreePath> toSort = new ArrayList<TreePath>(paths.length);
                    for (TreePath path : paths) {
                        toSort.add(path);
                    }
                    Collections.sort(toSort, new Comparator<TreePath>() {
                        @Override
                        public int compare(TreePath o1, TreePath o2) {
                            int comp;
                            if (o1 == null) {
                                if (o2 == null) {
                                    comp = 0;
                                } else {
                                    comp = -1;
                                }
                            } else if (o2 == null) {
                                comp = 1;
                            } else {
                                comp = Double.compare(getRowForPath(o1), getRowForPath(o2));
                            }
                            return comp;
                        }
                    });
                    paths = toSort.toArray(paths);
                }
                List<ITreeNode> nodeList = new ArrayList<ITreeNode>();
                for (TreePath path : paths) {
                    Object comp = path.getLastPathComponent();
                    if (comp instanceof DefaultMutableTreeNode) {
                        ITreeNode node = model.getTreeNode((DefaultMutableTreeNode) comp);
                        if (node != null) {
                            nodeList.add(node);
                        }
                    }
                }
                selection = nodeList.toArray(new ITreeNode[nodeList.size()]);
                nodeList.clear();
            }
        }
        return selection;
    }

    /**
     * Recovers the {@link TreePath}s that correspond to some give {@link ITreeNode}s
     * 
     * @param nodes The {@link ITreeNode}s
     * @return A {@link TreePath} array. <code>null</code> If model or <code>nodes</code> is <code>null</code>
     * @see #getModel()
     */
    protected TreePath[] getPaths(ITreeNode... nodes) {
        TreePath[] result = null;
        if (nodes != null) {
            CometeTreeModel model = getModel();
            if (model != null) {
                DefaultMutableTreeNode[] realNodes = new DefaultMutableTreeNode[nodes.length];
                for (int i = 0; i < nodes.length; i++) {
                    realNodes[i] = model.recoverNode(nodes[i]);
                }
                List<TreePath> treePaths = new ArrayList<TreePath>();
                for (DefaultMutableTreeNode node : realNodes) {
                    if (node != null) {
                        treePaths.add(new TreePath(node.getPath()));
                    }
                }
                result = treePaths.toArray(new TreePath[treePaths.size()]);
            }
        }
        return result;
    }

    @Override
    public void addTreeNodeSelectionListener(ITreeNodeSelectionListener listener) {
        if (listener != null) {
            synchronized (selectionListeners) {
                boolean canAdd = true;
                List<WeakReference<ITreeNodeSelectionListener>> toRemove = new ArrayList<WeakReference<ITreeNodeSelectionListener>>();
                for (WeakReference<ITreeNodeSelectionListener> ref : selectionListeners) {
                    ITreeNodeSelectionListener selectionListener = ref.get();
                    if (selectionListener == null) {
                        toRemove.add(ref);
                    } else if (selectionListener.equals(listener)) {
                        canAdd = false;
                    }
                }
                selectionListeners.removeAll(toRemove);
                toRemove.clear();
                if (canAdd) {
                    selectionListeners.add(new WeakReference<ITreeNodeSelectionListener>(listener));
                }
            }
        }
    }

    @Override
    public void removeTreeNodeSelectionListener(ITreeNodeSelectionListener listener) {
        if (listener != null) {
            synchronized (selectionListeners) {
                List<WeakReference<ITreeNodeSelectionListener>> toRemove = new ArrayList<WeakReference<ITreeNodeSelectionListener>>();
                for (WeakReference<ITreeNodeSelectionListener> ref : selectionListeners) {
                    ITreeNodeSelectionListener selectionListener = ref.get();
                    if ((selectionListener == null) || selectionListener.equals(listener)) {
                        toRemove.add(ref);
                    }
                }
                selectionListeners.removeAll(toRemove);
                toRemove.clear();
            }
        }
    }

    @Override
    public void removeAllTreeNodeSelectionListeners() {
        synchronized (selectionListeners) {
            selectionListeners.clear();
        }
    }

    @Override
    public boolean hasTreeNodeSelectionListener(ITreeNodeSelectionListener listener) {
        boolean contains = false;
        if (listener != null) {
            synchronized (selectionListeners) {
                List<WeakReference<ITreeNodeSelectionListener>> toRemove = new ArrayList<WeakReference<ITreeNodeSelectionListener>>();
                for (WeakReference<ITreeNodeSelectionListener> ref : selectionListeners) {
                    ITreeNodeSelectionListener selectionListener = ref.get();
                    if (selectionListener == null) {
                        toRemove.add(ref);
                    } else if (selectionListener.equals(listener)) {
                        contains = true;
                    }
                }
                selectionListeners.removeAll(toRemove);
                toRemove.clear();
            }
        }
        return contains;
    }

    /**
     * Warns {@link ITreeNodeSelectionListener}s for some changes in node selection
     * 
     * @param event The {@link TreeNodeSelectionEvent} that describes the changes
     */
    protected void warnSelectionListeners(TreeNodeSelectionEvent event) {
        if (event != null) {
            synchronized (selectionListeners) {
                List<WeakReference<ITreeNodeSelectionListener>> toRemove = new ArrayList<WeakReference<ITreeNodeSelectionListener>>();
                for (WeakReference<ITreeNodeSelectionListener> ref : selectionListeners) {
                    ITreeNodeSelectionListener selectionListener = ref.get();
                    if (selectionListener == null) {
                        toRemove.add(ref);
                    } else {
                        selectionListener.selectionChanged(event);
                    }
                }
                selectionListeners.removeAll(toRemove);
                toRemove.clear();
            }
        }
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void setSelectionMode(int selectionMode) {
        this.selectionMode = selectionMode;
        int swingSelectionMode = TreeSelectionModel.SINGLE_TREE_SELECTION;
        if (selectionMode == ITree.MULTI_SELECTION) {
            swingSelectionMode = TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION;
        }
        getSelectionModel().setSelectionMode(swingSelectionMode);
    }

    @Override
    public int getSelectionMode() {
        return selectionMode;
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
