/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util.mask;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MaskHandler;

public class MatrixMaskUndoableEdit extends AbstractUndoableEdit {

    private static final long serialVersionUID = 7133520601783278708L;
    protected MaskHandler handler;
    protected Mask undoMask;
    protected Mask redoMask;
    protected String presentationName;

    public MatrixMaskUndoableEdit(MaskHandler handler, Mask undoMask) {
        super();
        this.handler = handler;
        this.undoMask = undoMask;
        redoMask = handler.getMask();
    }

    public void setPresentationName(String presentationName) {
        this.presentationName = presentationName;
    }

    @Override
    public String getPresentationName() {
        return presentationName;
    }

    @Override
    public boolean canUndo() {
        return super.canUndo() && handler != null;
    }

    @Override
    public boolean canRedo() {
        return super.canRedo() && handler != null;
    }

    @Override
    public void undo() throws CannotUndoException {
        super.undo();
        redoMask = handler.getMask();
        handler.setMask(undoMask);
    }

    @Override
    public void redo() throws CannotRedoException {
        super.redo();
        handler.setMask(redoMask);
    }

    @Override
    public void die() {
        super.die();
        handler = null;
        undoMask = null;
        redoMask = null;
    }

}
