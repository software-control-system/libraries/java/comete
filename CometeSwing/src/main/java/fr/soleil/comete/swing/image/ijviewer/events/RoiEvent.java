/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.events;

import java.util.EventObject;

import ij.gui.Roi;

/**
 * 
 * 
 * @author MAINGUY
 * 
 */
public class RoiEvent extends EventObject {

    private static final long serialVersionUID = 7735099023527447394L;

    protected Roi roi;
    protected boolean isSelected;

    public RoiEvent(Object theSource, Roi theRoi, boolean selected) {
        super(theSource);
        roi = theRoi;
        isSelected = selected;
    }

    public Roi getRoi() {
        return roi;
    }

    public boolean isSelected() {
        return isSelected;
    }

}
