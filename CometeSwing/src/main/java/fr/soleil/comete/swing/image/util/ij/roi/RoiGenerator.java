/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Stroke;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import org.jdesktop.swingx.action.AbstractActionExt;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import ij.gui.Roi;

/**
 * An interface for something that can create new {@link Roi}s. Used by ImageViewer for special {@link Roi}s creation.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface RoiGenerator {

    /**
     * Returns the action command that identifies this {@link RoiGenerator}
     * 
     * @return a {@link String}
     */
    public String getActionCommand();

    /**
     * Returns the Roi mode ID used by this {@link RoiGenerator}
     * 
     * @return An <code>int</code>
     */
    public int getRoiMode();

    /**
     * Creates a new {@link Roi}
     * 
     * @param sx screen x coordinate for the new Roi
     * @param sy screen y coordinate for the new Roi
     * @param roiManager The {@link IJRoiManager} that will manage the created Roi
     * @return A {@link Roi}.
     */
    public Roi createNewRoi(int sx, int sy, IJRoiManager roiManager);

    public boolean isCompatibleWithRoi(Roi roi);

    /**
     * Warns this {@link IJRoiManager} that its previously created {@link Roi} was added in an {@link IJRoiManager}
     * 
     * @param roi The previously created {@link Roi} (obtained by {@link #createNewRoi(int, int, IJRoiManager)})
     * @param roiManager The {@link IJRoiManager} in which the {@link Roi} was added.
     */
    public void roiAdded(Roi roi, IJRoiManager roiManager);

    /**
     * Warns this {@link IJRoiManager} that its previously created {@link Roi} was removed from an {@link IJRoiManager}
     * 
     * @param roi The previously created {@link Roi} (obtained by {@link #createNewRoi(int, int, IJRoiManager)})
     * @param roiManager The {@link IJRoiManager} from which the {@link Roi} was removed.
     */
    public void roiRemoved(Roi roi, IJRoiManager roiManager);

    /**
     * Returns whether this {@link RoiGenerator} needs having no handled {@link Roi} before {@link Roi} creation
     * 
     * @return A <code>boolean</code>
     */
    public boolean needsNoHandledRoi();

    /**
     * Returns whether this {@link RoiGenerator} accepts other {@link Roi}s to be hovered, even if
     * {@link #needsNoHandledRoi()} returns <code>false</code>
     * 
     * @return A <code>boolean</code>
     */
    public boolean canHoverOtherRois();

    /**
     * Returns whether the {@link Roi} created by this {@link RoiGenerator} should not be cause of events
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isSilentRoi();

    /**
     * Returns the {@link Stroke} used to draw generated {@link Roi}s
     * 
     * @return A {@link Stroke}
     */
    public Stroke getRoiStroke();

    /**
     * Returns the {@link Color} used to draw generated {@link Roi}s
     * 
     * @return A {@link Color}
     */
    public Color getRoiColor();

    /**
     * Returns the {@link Stroke} used to draw generated {@link Roi}s when it is under mouse position
     * 
     * @return A {@link Stroke}
     */
    public Stroke getHoveredRoiStroke();

    /**
     * Returns the {@link Color} used to draw generated {@link Roi}s when it is under mouse position
     * 
     * @return A {@link Color}
     */
    public Color getHoveredRoiColor();

    /**
     * Returns the {@link Stroke} used to draw generated {@link Roi}s when it is selected
     * 
     * @return A {@link Stroke}
     */
    public Stroke getSelectedRoiStroke();

    /**
     * Returns the {@link Color} used to draw generated {@link Roi}s when it is selected
     * 
     * @return A {@link Color}
     */
    public Color getSelectedRoiColor();

    /**
     * Returns the {@link AbstractActionExt} used to activate associated Roi mode
     * 
     * @return An {@link AbstractActionExt}
     */
    public AbstractActionExt getRoiModeAction();

    /**
     * Returns an <code>int</code> that identifies in which menu the action should appear
     * 
     * @return An <code>int</code>
     * @see #getRoiModeAction()
     */
    public int getRoiModeMenu();

    /**
     * Returns an <code>int</code> that indicates the preferred position of the action in the menu
     * 
     * @return An <code>int</code>
     */
    public int getPreferredPositionInMenu();

    /**
     * Returns the {@link Cursor} used to represent associated roi mode
     * 
     * @return A {@link Cursor}
     */
    public Cursor getCursor();

    /**
     * Returns whether this {@link RoiGenerator} should be warned for data changes in {@link ImageViewer}
     * 
     * @return A <code>boolean</code>
     */
    public boolean isDataDependent();

    /**
     * Returns whether this {@link RoiGenerator} should be warned for axis changes in {@link ImageViewer}
     * 
     * @return A <code>boolean</code>
     */
    public boolean isAxisDependent();

    /**
     * Returns whether this {@link RoiGenerator} should be warned for color scale changes in {@link ImageViewer}
     * 
     * @return A <code>boolean</code>
     */
    public boolean isColorScaleDependent();

    /**
     * Returns whether this {@link RoiGenerator} should be warned for pixel size changes in {@link ImageViewer}
     * 
     * @return A <code>boolean</code>
     */
    public boolean isPixelSizeDependent();

    /**
     * Returns whether this {@link RoiGenerator} should be warned for zoom changes in {@link ImageViewer}
     * 
     * @return A <code>boolean</code>
     */
    public boolean isZoomDependent();

    /**
     * Warns this {@link RoiGenerator} for data changes in an {@link ImageViewer}
     * 
     * @param viewer The {@link ImageViewer}
     */
    public void dataChanged(ImageViewer viewer);

    /**
     * Warns this {@link RoiGenerator} for axis changes in an {@link ImageViewer}
     * 
     * @param viewer The {@link ImageViewer}
     */
    public void axisChanged(ImageViewer viewer);

    /**
     * Warns this {@link RoiGenerator} for color scale changes in an {@link ImageViewer}
     * 
     * @param viewer The {@link ImageViewer}
     */
    public void colorScaleChanged(ImageViewer viewer);

    /**
     * Warns this {@link RoiGenerator} for pixel size changes in an {@link ImageViewer}
     * 
     * @param viewer The {@link ImageViewer}
     */
    public void pixelSizeChanged(ImageViewer viewer);

    /**
     * Warns this {@link RoiGenerator} for zoom changes in an {@link ImageViewer}
     * 
     * @param viewer The {@link ImageViewer}
     */
    public void zoomChanged(ImageViewer viewer);

    /**
     * Adapts a mouse pressed event
     * 
     * @param event The {@link MouseEvent}
     * @return A {@link MouseEvent}. If <code>null</code>, this means that the event should be ignored by image canvas
     */
    public MouseEvent adaptMousePressedEvent(MouseEvent event);

    /**
     * Adapts a mouse released event
     * 
     * @param event The {@link MouseEvent}
     * @return A {@link MouseEvent}. If <code>null</code>, this means that the event should be ignored by image canvas
     */
    public MouseEvent adaptMouseReleasedEvent(MouseEvent event);

    /**
     * Adapts a mouse clicked event
     * 
     * @param event The {@link MouseEvent}
     * @return A {@link MouseEvent}. If <code>null</code>, this means that the event should be ignored by image canvas
     */
    public MouseEvent adaptMouseClickedEvent(MouseEvent event);

    /**
     * Adapts a mouse dragged event
     * 
     * @param event The {@link MouseEvent}
     * @return A {@link MouseEvent}. If <code>null</code>, this means that the event should be ignored by image canvas
     */
    public MouseEvent adaptMouseDraggedEvent(MouseEvent event);

    /**
     * Adapts a mouse moved event
     * 
     * @param event The {@link MouseEvent}
     * @return A {@link MouseEvent}. If <code>null</code>, this means that the event should be ignored by image canvas
     */
    public MouseEvent adaptMouseMovedEvent(MouseEvent event);

    /**
     * Notifies this {@link RoiGenerator} that a key was pressed in an {@link ImageViewer}
     * 
     * @param e The {@link KeyEvent} that notifies for the key pressed
     * @return Whether this {@link RoiGenerator} did treat the event.
     */
    public boolean keyPressed(KeyEvent e);

    /**
     * Notifies this {@link RoiGenerator} that a key was released in an {@link ImageViewer}
     * 
     * @param e The {@link KeyEvent} that notifies for the key released
     * @return Whether this {@link RoiGenerator} did treat the event.
     */
    public boolean keyReleased(KeyEvent e);

    /**
     * Notifies this {@link RoiGenerator} that a key was typed in an {@link ImageViewer}
     * 
     * @param e The {@link KeyEvent} that notifies for the key typed
     * @return Whether this {@link RoiGenerator} did treat the event.
     */
    public boolean keyTyped(KeyEvent e);

}
