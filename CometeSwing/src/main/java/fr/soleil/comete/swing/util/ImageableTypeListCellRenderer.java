/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * A {@link DefaultListCellRenderer} that is able to associate an image to some type represented by an id.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class ImageableTypeListCellRenderer extends TypeListCellRenderer {

    private static final long serialVersionUID = 2960826607792413638L;

    public ImageableTypeListCellRenderer() {
        super();
    }

    @Override
    protected void applyType(int type, JLabel comp) {
        super.applyType(type, comp);
        Image image = getImage(type, comp.getBackground(), comp.getForeground());
        comp.setIcon(image == null ? null : new ImageIcon(image));
    }

    /**
     * Given an id and a component's background and foreground colors, creates an image that represents it.
     * 
     * @param id The id.
     * @param bg The component's background color.
     * @param fg The component's foreground color.
     * @return A {@link BufferedImage}: the expected image. May be <code>null</code>.
     */
    protected abstract BufferedImage getImage(int id, Color bg, Color fg);

}
