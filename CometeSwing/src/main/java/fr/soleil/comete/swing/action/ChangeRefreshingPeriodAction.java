/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.action;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.WeakHashMap;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import fr.soleil.comete.swing.chart.util.PrecisionPanel;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.IRefreshingStrategy;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.data.target.IKeyTarget;
import fr.soleil.data.target.IKeysTarget;
import fr.soleil.lib.project.swing.WindowSwingUtils;

/**
 * An {@link AbstractAction} used to change the refreshing period of an {@link IKey}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ChangeRefreshingPeriodAction extends AbstractAction implements IKeyTarget, IKeysTarget {

    private static final long serialVersionUID = 274111079363484245L;

    protected static final String REFRESHING_PERIOD_TEXT = "Change refreshing period";
    protected static final String REFRESHING_PERIOD_TOOLTIP = "Desired refreshing period";
    protected static final ImageIcon ICON = new ImageIcon(ChangeRefreshingPeriodAction.class
            .getResource("/org/tango-project/tango-icon-theme/16x16/actions/appointment-new.png"));

    private final Map<Component, JDialog> dialogMap;

    protected IKey[] keys;

    protected String dialogTitle, textFieldTitle, textFieldTooltip;

    public ChangeRefreshingPeriodAction() {
        super(REFRESHING_PERIOD_TEXT, ICON);
        dialogTitle = REFRESHING_PERIOD_TEXT;
        textFieldTitle = REFRESHING_PERIOD_TEXT;
        textFieldTooltip = REFRESHING_PERIOD_TOOLTIP;
        dialogMap = new WeakHashMap<Component, JDialog>();
    }

    protected RefreshingPeriodPanel generateRefreshingPeriodPanel() {
        return new RefreshingPeriodPanel();
    }

    protected String getDialogTitle() {
        return dialogTitle;
    }

    public void setDialogTitle(String dialogTitle) {
        this.dialogTitle = dialogTitle;
    }

    public String getTextFieldTitle() {
        return textFieldTitle;
    }

    public void setTextFieldTitle(String textFieldTitle) {
        this.textFieldTitle = textFieldTitle;
        for (RefreshingPeriodPanel panel : getInstanciatedRefreshingPeriodPanels()) {
            panel.setTimePrecisionTitle(getTextFieldTitle());
        }
    }

    public String getTextFieldTooltip() {
        return textFieldTooltip;
    }

    public void setTextFieldTooltip(String textFieldTooltip) {
        this.textFieldTooltip = textFieldTooltip;
        for (RefreshingPeriodPanel panel : getInstanciatedRefreshingPeriodPanels()) {
            panel.setDefaultTooltip(getTextFieldTooltip());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e != null) {
            Object src = e.getSource();
            if (src instanceof Component) {
                Component comp = (Component) src;
                JDialog editionDialog;
                synchronized (dialogMap) {
                    editionDialog = dialogMap.get(comp);
                    if (editionDialog == null) {
                        final RefreshingPeriodPanel panel = generateRefreshingPeriodPanel();
                        editionDialog = new JDialog(WindowSwingUtils.getWindowForComponent(comp), getDialogTitle());
                        Object icon = getValue(SMALL_ICON);
                        if (icon instanceof ImageIcon) {
                            editionDialog.setIconImage(((ImageIcon) icon).getImage());
                        }
                        editionDialog.setContentPane(panel);
                        editionDialog.setModal(false);
                        dialogMap.put(comp, editionDialog);
                    }
                }
                if (!editionDialog.isVisible()) {
                    ((RefreshingPeriodPanel) editionDialog.getContentPane()).cancel();
                    editionDialog.pack();
                    editionDialog.setLocationRelativeTo(comp.isShowing() ? comp : editionDialog.getOwner());
                }
                editionDialog.setVisible(true);
                editionDialog.toFront();
            }
        }
    }

    protected Collection<RefreshingPeriodPanel> getInstanciatedRefreshingPeriodPanels() {
        Collection<RefreshingPeriodPanel> panels = new ArrayList<ChangeRefreshingPeriodAction.RefreshingPeriodPanel>();
        synchronized (dialogMap) {
            for (JDialog dialog : dialogMap.values()) {
                if (dialog != null) {
                    Container content = dialog.getContentPane();
                    if (content instanceof RefreshingPeriodPanel) {
                        panels.add((RefreshingPeriodPanel) content);
                    }
                }
            }
        }
        return panels;
    }

    protected IKey[] getConnectedKeys() {
        return keys;
    }

    protected PolledRefreshingStrategy buildRefreshingStrategy(IKey key, int period) {
        return new PolledRefreshingStrategy(period);
    }

    @Override
    public void addMediator(Mediator<?> mdtr) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mdtr) {
        // not managed
    }

    @Override
    public void setKey(IKey key) {
        setKeys(key);
    }

    @Override
    public void setKeys(IKey... keys) {
        this.keys = keys;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class RefreshingPeriodPanel extends PrecisionPanel {

        private static final long serialVersionUID = -8802895315709205841L;

        protected JPanel southPanel;
        protected JPanel buttonPanel;
        protected JButton okButton;
        protected JButton applyButton;
        protected JButton cancelButton;

        public RefreshingPeriodPanel() {
            super();
            setTimeScale(true);
            okButton = new JButton("Ok");
            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    apply();
                    closeParent();
                }
            });
            applyButton = new JButton("Apply");
            applyButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    apply();
                }
            });
            cancelButton = new JButton("Cancel");
            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    cancel();
                    closeParent();
                }
            });
            buttonPanel = new JPanel(new GridLayout(1, 3, 5, 5));
            buttonPanel.add(okButton);
            buttonPanel.add(cancelButton);
            buttonPanel.add(applyButton);
            southPanel = new JPanel(new BorderLayout());
            southPanel.add(buttonPanel, BorderLayout.EAST);
            add(southPanel, BorderLayout.SOUTH);
            setTimePrecisionTitle(getTextFieldTitle());
            setDefaultTooltip(getTextFieldTooltip());
        }

        public void apply() {
            if (computePrecision()) {
                IKey[] keys = getConnectedKeys();
                if (keys != null) {
                    for (IKey key : keys) {
                        DataSourceProducerProvider.setRefreshingStrategy(key,
                                buildRefreshingStrategy(key, getPrecision()));
                    }
                }
            }
        }

        public void cancel() {
            IKey[] keys = getConnectedKeys();
            if (keys != null) {
                for (IKey key : keys) {
                    IRefreshingStrategy strategy = DataSourceProducerProvider.getRefreshingStrategy(key);
                    if (strategy instanceof PolledRefreshingStrategy) {
                        setPrecision(((PolledRefreshingStrategy) strategy).getRefreshingPeriod());
                        break;
                    }
                }
            }
        }

        protected void closeParent() {
            Window parent = WindowSwingUtils.getWindowForComponent(this);
            if (parent instanceof JDialog) {
                parent.setVisible(false);
            }
        }

    }

}
