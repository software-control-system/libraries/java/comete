/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.hidden;

import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.util.concurrent.atomic.AtomicInteger;

import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.ImageCanvas;
import ij.gui.ImageWindow;

/**
 * This class hides the frame that IJ uses to display the image. We cannot set it to null as
 * image always link to a window in IJ.
 * 
 * @author MAINGUY, Rapha&euml;l GIRARDOT
 */
public class HiddenImageWindow extends ImageWindow {

    private static final long serialVersionUID = 1141685747752173089L;

    // Call Garbage Collector every 10 ImageWindow cleaned...
    private static final int MAX_CLEAN_BEFORE_GC = 10;
    // ... or every 30s.
    private static final long MAX_DELAY_BEFORE_GC = 30000;
    private static final AtomicInteger CLEANED_COUNT = new AtomicInteger(0);
    private static long lastGC = System.currentTimeMillis();

    protected volatile boolean ignoreResizeEvents;

    public HiddenImageWindow(ImagePlus imp, ImageCanvas ic) {
        super(imp, ic);
        setResizable(false);
        ignoreResizeEvents = false;
    }

    public boolean isIgnoreResizeEvents() {
        return ignoreResizeEvents;
    }

    public void setIgnoreResizeEvents(boolean ignoreResizeEvents) {
        this.ignoreResizeEvents = ignoreResizeEvents;
    }

    @Override
    public boolean close() {
        return true;
    }

    @Override
    public void show() {
    }

    // XXX This is an ugly hack, but there were no other solution to avoid undesired IJCanvas resized
    @Deprecated
    @Override
    public void layout() {
        // do not fit max size while user is scrolling image
        if (!isIgnoreResizeEvents()) {
            try {
                super.layout();
            } catch (NullPointerException npe) {
                // ignore NPE as it is just impossible to override resizeCanvas method in IJCanvas to try to solve it
            }
        }
    }

    public static void clean(ImageWindow window) {
        if (window != null) {
            // 1/ Close ImageWidow
            window.dispose();
            ImageWindow previous = WindowManager.getCurrentWindow();
            window.close();
            if (previous != window) {
                WindowManager.setCurrentWindow(previous);
            }
            // 2/ Remove it from ImagePlus if necessary
            ImagePlus imp = window.getImagePlus();
            if ((imp != null) && (imp.getWindow() == window)) {
                imp.setWindow(null);
            }
            // 3/ Remove its references in WindowManager
            if (window == WindowManager.getFrontWindow()) {
                WindowManager.setWindow(null);
            }
            WindowManager.removeWindow(window);
            // 4/ Clean ImageWidow
            window.removeAll();
            window.setMenuBar(null);
            WindowFocusListener[] focusListeners = window.getWindowFocusListeners();
            if (focusListeners != null) {
                for (WindowFocusListener listener : focusListeners) {
                    window.removeWindowFocusListener(listener);
                }
            }
            WindowStateListener[] stateListeners = window.getWindowStateListeners();
            if (stateListeners != null) {
                for (WindowStateListener listener : stateListeners) {
                    window.removeWindowStateListener(listener);
                }
            }
            WindowListener[] listeners = window.getWindowListeners();
            if (listeners != null) {
                for (WindowListener listener : listeners) {
                    window.removeWindowListener(listener);
                }
            }
            // 5/ Once ImageWidow cleaned, call Garbage Collector if useful.
            long now = System.currentTimeMillis();
            if ((CLEANED_COUNT.incrementAndGet() > MAX_CLEAN_BEFORE_GC) || (now - lastGC > MAX_DELAY_BEFORE_GC)) {
                lastGC = now;
                CLEANED_COUNT.set(0);
                System.gc();
            }
        }
    }

}
