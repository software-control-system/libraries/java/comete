/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.chart.util.FontChooser;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.LineStyleListCellRenderer;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.text.JSmoothLabel;

/**
 * A class to display global graph settings dialog.
 * 
 * @deprecated use {@link ChartOption} instead
 */
@Deprecated
public class JLChartOption extends JPanel
        implements ActionListener, MouseListener, ChangeListener, KeyListener, AxisPanelListener {

    private static final long serialVersionUID = 5344157119337579259L;

    // Local declaration
    protected JLChart chart;
    protected JTabbedPane tabPane;
    protected JButton closeButton;
    protected ChartProperties properties;

    // general panel
    protected JPanel generalPanel;

    // Legends
    protected JPanel gLegendPanel;
    protected JCheckBox generalLabelVisibleCheck;
    protected JCheckBox autoHighlightCheck;

    // Colors & Fonts
    protected JPanel gColorFontPanel;
    protected JLabel generalFontHeaderLabel;
    protected JSmoothLabel generalFontHeaderSampleLabel;
    protected JButton generalFontHeaderBtn;
    protected JLabel generalFontLabelLabel;
    protected JSmoothLabel generalFontLabelSampleLabel;
    protected JButton generalFontLabelBtn;
    protected JLabel chartBackColorLabel;
    protected JLabel chartBackColorView;
    protected JButton chartBackColorBtn;
    protected JLabel generalBackColorLabel;
    protected JLabel generalBackColorView;
    protected JButton generalBackColorBtn;

    // Axis grid
    protected JPanel gGridPanel;
    protected JComboBox<String> generalGridCombo;
    protected JComboBox<String> generalLabelPCombo;
    protected JLabel generalLabelPLabel;
    protected JComboBox<Integer> generalGridStyleCombo;
    protected JLabel generalGridStyleLabel;

    // Misc
    protected JPanel gMiscPanel;
    protected JLabel generalLegendLabel;
    protected JTextField generalLegendText;
    protected JLabel generalDurationLabel;
    protected JTextField generalDurationText;
    protected JLabel timePrecisionLabel;
    protected JTextField timePrecisionText;

    // Axis panels
    protected AxisPanel y1Panel;
    protected AxisPanel y2Panel;
    protected AxisPanel xPanel;

    protected final static int ORIGIN_Y1 = 0;
    protected final static int ORIGIN_Y2 = 1;
    protected final static int ORIGIN_X = 2;
    protected final static int ORIGIN_GENERAL_PROPERTIES = 3;

    /**
     * JLChartOption constructor.
     * 
     * @param parent
     *            Parent dialog
     * @param chart
     *            Chart to be edited.
     */
    public JLChartOption() {
        super();
        initComponents();
    }

    public void setChart(JLChart chart) {
        this.chart = chart;
        updatePropertiesFromChart();
    }

    protected void updatePropertiesFromChart() {
        if (chart != null) {
            setProperties(chart.getChartProperties());
        }
    }

    protected void initComponents() {

        setLayout(new GridBagLayout());

        setName("Chart properties");

        tabPane = new JTabbedPane();

        // **********************************************
        // General panel construction
        // **********************************************

        generalPanel = new JPanel(new GridBagLayout());

        gLegendPanel = new JPanel(new GridBagLayout());
        gLegendPanel.setBorder(CometeUtils.createTitleBorder("Legends"));

        gColorFontPanel = new JPanel(new GridBagLayout());
        gColorFontPanel.setBorder(CometeUtils.createTitleBorder("Colors & Fonts"));

        gGridPanel = new JPanel(new GridBagLayout());
        gGridPanel.setBorder(CometeUtils.createTitleBorder("Axis grid"));

        gMiscPanel = new JPanel(new GridBagLayout());
        gMiscPanel.setBorder(CometeUtils.createTitleBorder("Misc"));

        generalLegendLabel = new JLabel("Chart title");
        generalLegendLabel.setFont(CometeUtils.getLabelFont());
        generalLegendLabel.setForeground(CometeUtils.getfColor());
        generalLegendText = new JTextField(8);
        generalLegendText.setMargin(CometeUtils.getTextFieldInsets());
        generalLegendText.setEditable(true);
        generalLegendText.setText(ObjectUtils.EMPTY_STRING);
        generalLegendText.addKeyListener(this);

        generalLabelVisibleCheck = new JCheckBox();
        generalLabelVisibleCheck.setFont(CometeUtils.getLabelFont());
        generalLabelVisibleCheck.setForeground(CometeUtils.getfColor());
        generalLabelVisibleCheck.setText("Visible");
        generalLabelVisibleCheck.setSelected(false);
        generalLabelVisibleCheck.addActionListener(this);

        autoHighlightCheck = new JCheckBox();
        autoHighlightCheck.setFont(CometeUtils.getLabelFont());
        autoHighlightCheck.setForeground(CometeUtils.getfColor());
        autoHighlightCheck.setText("Auto highlighting");
        autoHighlightCheck.setSelected(false);
        autoHighlightCheck.addActionListener(this);

        chartBackColorLabel = new JLabel("Chart background");
        chartBackColorLabel.setFont(CometeUtils.getLabelFont());
        chartBackColorLabel.setForeground(CometeUtils.getfColor());
        chartBackColorView = new JLabel(ObjectUtils.EMPTY_STRING);
        chartBackColorView.setOpaque(true);
        chartBackColorView.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        chartBackColorView.setBackground(Color.WHITE);
        chartBackColorView.setPreferredSize(CometeUtils.getViewLabelSize());
        chartBackColorView.setMinimumSize(chartBackColorView.getPreferredSize());
        chartBackColorBtn = new JButton("...");
        chartBackColorBtn.setMargin(CometeUtils.getBtnInsets());
        chartBackColorBtn.addMouseListener(this);

        generalBackColorLabel = new JLabel("Main background");
        generalBackColorLabel.setFont(CometeUtils.getLabelFont());
        generalBackColorLabel.setForeground(CometeUtils.getfColor());
        generalBackColorView = new JLabel(ObjectUtils.EMPTY_STRING);
        generalBackColorView.setOpaque(true);
        generalBackColorView.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        generalBackColorView.setBackground(Color.WHITE);
        generalBackColorView.setPreferredSize(CometeUtils.getViewLabelSize());
        generalBackColorView.setMinimumSize(generalBackColorView.getPreferredSize());
        generalBackColorBtn = new JButton("...");
        generalBackColorBtn.setMargin(CometeUtils.getBtnInsets());
        generalBackColorBtn.addMouseListener(this);

        generalLabelPLabel = new JLabel("Placement");
        generalLabelPLabel.setHorizontalAlignment(JLabel.RIGHT);
        generalLabelPLabel.setFont(CometeUtils.getLabelFont());
        generalLabelPLabel.setForeground(CometeUtils.getfColor());

        generalLabelPCombo = new JComboBox<>();
        generalLabelPCombo.setFont(CometeUtils.getLabelFont());
        generalLabelPCombo.addItem("Bottom");
        generalLabelPCombo.addItem("Top");
        generalLabelPCombo.addItem("Right");
        generalLabelPCombo.addItem("Left");
        generalLabelPCombo.addItem("Row");
        generalLabelPCombo.setSelectedIndex(IChartViewer.LABEL_ROW);
        generalLabelPCombo.addActionListener(this);

        generalGridCombo = new JComboBox<>();
        generalGridCombo.setFont(CometeUtils.getLabelFont());
        generalGridCombo.addItem("None");
        generalGridCombo.addItem("On X");
        generalGridCombo.addItem("On Y1");
        generalGridCombo.addItem("On Y2");
        generalGridCombo.addItem("On X and Y1");
        generalGridCombo.addItem("On X and Y2");

        generalGridCombo.setSelectedIndex(0);
        generalGridCombo.addActionListener(this);

        generalGridStyleLabel = new JLabel("Style");
        generalGridStyleLabel.setFont(CometeUtils.getLabelFont());
        generalGridStyleLabel.setHorizontalAlignment(JLabel.RIGHT);
        generalGridStyleLabel.setForeground(CometeUtils.getfColor());

        generalGridStyleCombo = new JComboBox<>();
        generalGridStyleCombo.setFont(CometeUtils.getLabelFont());
        generalGridStyleCombo.addItem(IChartViewer.STYLE_SOLID);
        generalGridStyleCombo.addItem(IChartViewer.STYLE_DOT);
        generalGridStyleCombo.addItem(IChartViewer.STYLE_DASH);
        generalGridStyleCombo.addItem(IChartViewer.STYLE_LONG_DASH);
        generalGridStyleCombo.addItem(IChartViewer.STYLE_DASH_DOT);
        generalGridStyleCombo.setSelectedIndex(0);
        generalGridStyleCombo.setRenderer(new LineStyleListCellRenderer());
        generalGridStyleCombo.addActionListener(this);

        generalDurationLabel = new JLabel("Display duration (s)");
        generalDurationLabel.setFont(CometeUtils.getLabelFont());
        generalDurationLabel.setForeground(CometeUtils.getfColor());
        generalDurationText = new JTextField(8);
        generalDurationText.setMargin(CometeUtils.getTextFieldInsets());
        generalDurationText.setEditable(true);
        generalDurationText.setToolTipText("Type Infinity to disable");
        generalDurationText.setText(Double.toString(Double.POSITIVE_INFINITY));
        generalDurationText.addKeyListener(this);

        timePrecisionLabel = new JLabel("Time precision (ms)");
        timePrecisionLabel.setFont(CometeUtils.getLabelFont());
        timePrecisionLabel.setForeground(CometeUtils.getfColor());
        timePrecisionText = new JTextField(8);
        timePrecisionText.setMargin(CometeUtils.getTextFieldInsets());
        timePrecisionText.setEditable(true);
        timePrecisionText.setToolTipText("Precision to consider 2 X points to be the same. 0 by default");
        timePrecisionText.setText("0");
        timePrecisionText.addKeyListener(this);

        generalFontHeaderLabel = new JLabel("Header font");
        generalFontHeaderLabel.setFont(CometeUtils.getLabelFont());
        generalFontHeaderLabel.setForeground(CometeUtils.getfColor());
        generalFontHeaderSampleLabel = new JSmoothLabel();
        generalFontHeaderSampleLabel.setText("Sample text");
        generalFontHeaderSampleLabel.setForeground(CometeUtils.getfColor());
        generalFontHeaderSampleLabel.setOpaque(false);
        generalFontHeaderSampleLabel.setFont(CometeUtils.getLabelFont());
        generalFontHeaderBtn = new JButton("...");
        generalFontHeaderBtn.setMargin(CometeUtils.getBtnInsets());
        generalFontHeaderBtn.addMouseListener(this);

        generalFontLabelLabel = new JLabel("Label font");
        generalFontLabelLabel.setFont(CometeUtils.getLabelFont());
        generalFontLabelLabel.setForeground(CometeUtils.getfColor());
        generalFontLabelSampleLabel = new JSmoothLabel();
        generalFontLabelSampleLabel.setText("Sample 0123456789");
        generalFontLabelSampleLabel.setForeground(CometeUtils.getfColor());
        generalFontLabelSampleLabel.setOpaque(false);
        generalFontLabelSampleLabel.setFont(CometeUtils.getLabelFont());
        generalFontLabelBtn = new JButton("...");
        generalFontLabelBtn.setMargin(CometeUtils.getBtnInsets());
        generalFontLabelBtn.addMouseListener(this);

        // Legends
        GridBagConstraints generalLabelVisibleCheckConstraints = new GridBagConstraints();
        generalLabelVisibleCheckConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalLabelVisibleCheckConstraints.gridx = 0;
        generalLabelVisibleCheckConstraints.gridy = 0;
        generalLabelVisibleCheckConstraints.weightx = 0;
        generalLabelVisibleCheckConstraints.weighty = 0;
        gLegendPanel.add(generalLabelVisibleCheck, generalLabelVisibleCheckConstraints);
        GridBagConstraints generalLabelLabelPLabelConstraints = new GridBagConstraints();
        generalLabelLabelPLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalLabelLabelPLabelConstraints.gridx = 1;
        generalLabelLabelPLabelConstraints.gridy = 0;
        generalLabelLabelPLabelConstraints.weightx = 0.5;
        generalLabelLabelPLabelConstraints.weighty = 0;
        generalLabelLabelPLabelConstraints.insets = new Insets(0, 30, 0, 5);
        gLegendPanel.add(generalLabelPLabel, generalLabelLabelPLabelConstraints);
        GridBagConstraints generalLabelLabelPComboConstraints = new GridBagConstraints();
        generalLabelLabelPComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalLabelLabelPComboConstraints.gridx = 2;
        generalLabelLabelPComboConstraints.gridy = 0;
        generalLabelLabelPComboConstraints.weightx = 0.5;
        generalLabelLabelPComboConstraints.weighty = 0;
        gLegendPanel.add(generalLabelPCombo, generalLabelLabelPComboConstraints);
        GridBagConstraints autoHighlightCheckConstraints = new GridBagConstraints();
        autoHighlightCheckConstraints.fill = GridBagConstraints.HORIZONTAL;
        autoHighlightCheckConstraints.gridx = 0;
        autoHighlightCheckConstraints.gridy = 1;
        autoHighlightCheckConstraints.weightx = 0;
        autoHighlightCheckConstraints.weighty = 0;
        gLegendPanel.add(autoHighlightCheck, autoHighlightCheckConstraints);

        GridBagConstraints gLegendPanelConstraints = new GridBagConstraints();
        gLegendPanelConstraints.fill = GridBagConstraints.BOTH;
        gLegendPanelConstraints.gridx = 0;
        gLegendPanelConstraints.gridy = 0;
        gLegendPanelConstraints.weightx = 1;
        gLegendPanelConstraints.weighty = 0.25;
        generalPanel.add(gLegendPanel, gLegendPanelConstraints);

        // Colors & Fonts
        GridBagConstraints chartBackColorLabelConstraints = new GridBagConstraints();
        chartBackColorLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        chartBackColorLabelConstraints.gridx = 0;
        chartBackColorLabelConstraints.gridy = 0;
        chartBackColorLabelConstraints.weightx = 0;
        chartBackColorLabelConstraints.weighty = 0;
        chartBackColorLabelConstraints.insets = new Insets(0, 5, 5, 30);
        gColorFontPanel.add(chartBackColorLabel, chartBackColorLabelConstraints);
        GridBagConstraints chartBackColorViewConstraints = new GridBagConstraints();
        chartBackColorViewConstraints.fill = GridBagConstraints.HORIZONTAL;
        chartBackColorViewConstraints.gridx = 1;
        chartBackColorViewConstraints.gridy = 0;
        chartBackColorViewConstraints.weightx = 1;
        chartBackColorViewConstraints.weighty = 0;
        chartBackColorViewConstraints.insets = new Insets(0, 0, 5, 5);
        gColorFontPanel.add(chartBackColorView, chartBackColorViewConstraints);
        GridBagConstraints chartBackColorBtnConstraints = new GridBagConstraints();
        chartBackColorBtnConstraints.fill = GridBagConstraints.NONE;
        chartBackColorBtnConstraints.gridx = 2;
        chartBackColorBtnConstraints.gridy = 0;
        chartBackColorBtnConstraints.weightx = 0;
        chartBackColorBtnConstraints.weighty = 0;
        chartBackColorBtnConstraints.insets = new Insets(0, 0, 5, 5);
        gColorFontPanel.add(chartBackColorBtn, chartBackColorBtnConstraints);

        GridBagConstraints generalBackColorLabelConstraints = new GridBagConstraints();
        generalBackColorLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalBackColorLabelConstraints.gridx = 0;
        generalBackColorLabelConstraints.gridy = 1;
        generalBackColorLabelConstraints.weightx = 0;
        generalBackColorLabelConstraints.weighty = 0;
        generalBackColorLabelConstraints.insets = new Insets(0, 5, 5, 30);
        gColorFontPanel.add(generalBackColorLabel, generalBackColorLabelConstraints);
        GridBagConstraints generalBackColorViewConstraints = new GridBagConstraints();
        generalBackColorViewConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalBackColorViewConstraints.gridx = 1;
        generalBackColorViewConstraints.gridy = 1;
        generalBackColorViewConstraints.weightx = 1;
        generalBackColorViewConstraints.weighty = 0;
        generalBackColorViewConstraints.insets = new Insets(0, 0, 5, 5);
        gColorFontPanel.add(generalBackColorView, generalBackColorViewConstraints);
        GridBagConstraints generalBackColorBtnConstraints = new GridBagConstraints();
        generalBackColorBtnConstraints.fill = GridBagConstraints.NONE;
        generalBackColorBtnConstraints.gridx = 2;
        generalBackColorBtnConstraints.gridy = 1;
        generalBackColorBtnConstraints.weightx = 0;
        generalBackColorBtnConstraints.weighty = 0;
        generalBackColorBtnConstraints.insets = new Insets(0, 0, 5, 5);
        gColorFontPanel.add(generalBackColorBtn, generalBackColorBtnConstraints);

        GridBagConstraints generalFontHeaderLabelConstraints = new GridBagConstraints();
        generalFontHeaderLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalFontHeaderLabelConstraints.gridx = 0;
        generalFontHeaderLabelConstraints.gridy = 2;
        generalFontHeaderLabelConstraints.weightx = 0;
        generalFontHeaderLabelConstraints.weighty = 0;
        generalFontHeaderLabelConstraints.insets = new Insets(0, 5, 5, 30);
        gColorFontPanel.add(generalFontHeaderLabel, generalFontHeaderLabelConstraints);
        GridBagConstraints generalFontHeaderSampleLabelConstraints = new GridBagConstraints();
        generalFontHeaderSampleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalFontHeaderSampleLabelConstraints.gridx = 1;
        generalFontHeaderSampleLabelConstraints.gridy = 2;
        generalFontHeaderSampleLabelConstraints.weightx = 1;
        generalFontHeaderSampleLabelConstraints.weighty = 0;
        generalFontHeaderSampleLabelConstraints.insets = new Insets(0, 0, 5, 5);
        gColorFontPanel.add(generalFontHeaderSampleLabel, generalFontHeaderSampleLabelConstraints);
        GridBagConstraints generalFontHeaderBtnConstraints = new GridBagConstraints();
        generalFontHeaderBtnConstraints.fill = GridBagConstraints.NONE;
        generalFontHeaderBtnConstraints.gridx = 2;
        generalFontHeaderBtnConstraints.gridy = 2;
        generalFontHeaderBtnConstraints.weightx = 0;
        generalFontHeaderBtnConstraints.weighty = 0;
        generalFontHeaderBtnConstraints.insets = new Insets(0, 0, 5, 5);
        gColorFontPanel.add(generalFontHeaderBtn, generalFontHeaderBtnConstraints);

        GridBagConstraints generalFontLabelLabelConstraints = new GridBagConstraints();
        generalFontLabelLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalFontLabelLabelConstraints.gridx = 0;
        generalFontLabelLabelConstraints.gridy = 3;
        generalFontLabelLabelConstraints.weightx = 0;
        generalFontLabelLabelConstraints.weighty = 0;
        generalFontLabelLabelConstraints.insets = new Insets(0, 5, 0, 30);
        gColorFontPanel.add(generalFontLabelLabel, generalFontLabelLabelConstraints);
        GridBagConstraints generalFontLabelSampleLabelConstraints = new GridBagConstraints();
        generalFontLabelSampleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalFontLabelSampleLabelConstraints.gridx = 1;
        generalFontLabelSampleLabelConstraints.gridy = 3;
        generalFontLabelSampleLabelConstraints.weightx = 1;
        generalFontLabelSampleLabelConstraints.weighty = 0;
        generalFontLabelSampleLabelConstraints.insets = new Insets(0, 0, 0, 5);
        gColorFontPanel.add(generalFontLabelSampleLabel, generalFontLabelSampleLabelConstraints);
        GridBagConstraints generalFontLabelBtnConstraints = new GridBagConstraints();
        generalFontLabelBtnConstraints.fill = GridBagConstraints.NONE;
        generalFontLabelBtnConstraints.gridx = 2;
        generalFontLabelBtnConstraints.gridy = 3;
        generalFontLabelBtnConstraints.weightx = 0;
        generalFontLabelBtnConstraints.weighty = 0;
        generalFontLabelBtnConstraints.insets = new Insets(0, 0, 0, 5);
        gColorFontPanel.add(generalFontLabelBtn, generalFontLabelBtnConstraints);

        GridBagConstraints gColorFontPanelConstraints = new GridBagConstraints();
        gColorFontPanelConstraints.fill = GridBagConstraints.BOTH;
        gColorFontPanelConstraints.gridx = 0;
        gColorFontPanelConstraints.gridy = 1;
        gColorFontPanelConstraints.weightx = 1;
        gColorFontPanelConstraints.weighty = 0.25;
        generalPanel.add(gColorFontPanel, gColorFontPanelConstraints);

        // Axis Grid
        GridBagConstraints generalGridComboConstraints = new GridBagConstraints();
        generalGridComboConstraints.fill = GridBagConstraints.NONE;
        generalGridComboConstraints.gridx = 0;
        generalGridComboConstraints.gridy = 0;
        generalGridComboConstraints.weightx = 0;
        generalGridComboConstraints.weighty = 0;
        generalGridComboConstraints.insets = new Insets(5, 0, 5, 0);
        gGridPanel.add(generalGridCombo, generalGridComboConstraints);
        GridBagConstraints generalGridStyleLabelConstraints = new GridBagConstraints();
        generalGridStyleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalGridStyleLabelConstraints.gridx = 1;
        generalGridStyleLabelConstraints.gridy = 0;
        generalGridStyleLabelConstraints.weightx = 1;
        generalGridStyleLabelConstraints.weighty = 0;
        generalGridStyleLabelConstraints.insets = new Insets(5, 30, 5, 5);
        gGridPanel.add(generalGridStyleLabel, generalGridStyleLabelConstraints);
        GridBagConstraints generalGridStyleComboConstraints = new GridBagConstraints();
        generalGridStyleComboConstraints.fill = GridBagConstraints.NONE;
        generalGridStyleComboConstraints.gridx = 2;
        generalGridStyleComboConstraints.gridy = 0;
        generalGridStyleComboConstraints.weightx = 0;
        generalGridStyleComboConstraints.weighty = 0;
        generalGridStyleComboConstraints.insets = new Insets(5, 0, 5, 0);
        gGridPanel.add(generalGridStyleCombo, generalGridStyleComboConstraints);

        GridBagConstraints gGridPanelConstraints = new GridBagConstraints();
        gGridPanelConstraints.fill = GridBagConstraints.BOTH;
        gGridPanelConstraints.gridx = 0;
        gGridPanelConstraints.gridy = 2;
        gGridPanelConstraints.weightx = 2;
        gGridPanelConstraints.weighty = 0.25;
        generalPanel.add(gGridPanel, gGridPanelConstraints);

        // Misc
        GridBagConstraints generalLegendLabelConstraints = new GridBagConstraints();
        generalLegendLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalLegendLabelConstraints.gridx = 0;
        generalLegendLabelConstraints.gridy = 0;
        generalLegendLabelConstraints.weightx = 0;
        generalLegendLabelConstraints.weighty = 0;
        generalLegendLabelConstraints.insets = new Insets(0, 5, 5, 10);
        gMiscPanel.add(generalLegendLabel, generalLegendLabelConstraints);
        GridBagConstraints generalLegendTextConstraints = new GridBagConstraints();
        generalLegendTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalLegendTextConstraints.gridx = 1;
        generalLegendTextConstraints.gridy = 0;
        generalLegendTextConstraints.weightx = 1;
        generalLegendTextConstraints.weighty = 0;
        generalLegendTextConstraints.insets = new Insets(0, 5, 5, 5);
        gMiscPanel.add(generalLegendText, generalLegendTextConstraints);

        GridBagConstraints generalDurationConstraints = new GridBagConstraints();
        generalDurationConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalDurationConstraints.gridx = 0;
        generalDurationConstraints.gridy = 1;
        generalDurationConstraints.weightx = 0;
        generalDurationConstraints.weighty = 0;
        generalDurationConstraints.insets = new Insets(0, 5, 5, 10);
        gMiscPanel.add(generalDurationLabel, generalDurationConstraints);
        GridBagConstraints generalDurationTextConstraints = new GridBagConstraints();
        generalDurationTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        generalDurationTextConstraints.gridx = 1;
        generalDurationTextConstraints.gridy = 1;
        generalDurationTextConstraints.weightx = 1;
        generalDurationTextConstraints.weighty = 0;
        generalDurationTextConstraints.insets = new Insets(0, 5, 5, 5);
        gMiscPanel.add(generalDurationText, generalDurationTextConstraints);

        GridBagConstraints timePrecisionConstraints = new GridBagConstraints();
        timePrecisionConstraints.fill = GridBagConstraints.HORIZONTAL;
        timePrecisionConstraints.gridx = 0;
        timePrecisionConstraints.gridy = 2;
        timePrecisionConstraints.weightx = 0;
        timePrecisionConstraints.weighty = 0;
        timePrecisionConstraints.insets = new Insets(0, 5, 0, 10);
        gMiscPanel.add(timePrecisionLabel, timePrecisionConstraints);
        GridBagConstraints timePrecisionTextConstraints = new GridBagConstraints();
        timePrecisionTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        timePrecisionTextConstraints.gridx = 1;
        timePrecisionTextConstraints.gridy = 2;
        timePrecisionTextConstraints.weightx = 1;
        timePrecisionTextConstraints.weighty = 0;
        timePrecisionTextConstraints.insets = new Insets(0, 5, 0, 5);
        gMiscPanel.add(timePrecisionText, timePrecisionTextConstraints);

        GridBagConstraints gMiscPanelConstraints = new GridBagConstraints();
        gMiscPanelConstraints.fill = GridBagConstraints.BOTH;
        gMiscPanelConstraints.gridx = 0;
        gMiscPanelConstraints.gridy = 3;
        gMiscPanelConstraints.weightx = 2;
        gMiscPanelConstraints.weighty = 0.25;
        generalPanel.add(gMiscPanel, gMiscPanelConstraints);

        // **********************************************
        // Axis panel construction
        // **********************************************
        y1Panel = new AxisPanel(AxisPanel.Y1_TYPE);
        y1Panel.addAxisPanelListener(this);
        y2Panel = new AxisPanel(AxisPanel.Y2_TYPE);
        y2Panel.addAxisPanelListener(this);
        xPanel = new AxisPanel(AxisPanel.X_TYPE);
        xPanel.addAxisPanelListener(this);

        // Global frame construction

        tabPane.add("General", generalPanel);
        tabPane.add("X axis", xPanel);
        tabPane.add("Y1 axis", y1Panel);
        tabPane.add("Y2 axis", y2Panel);

        GridBagConstraints tabPaneConstraints = new GridBagConstraints();
        tabPaneConstraints.fill = GridBagConstraints.BOTH;
        tabPaneConstraints.gridx = 0;
        tabPaneConstraints.gridy = 0;
        tabPaneConstraints.weightx = 1;
        tabPaneConstraints.weighty = 1;
        tabPaneConstraints.gridwidth = GridBagConstraints.REMAINDER;
        add(tabPane, tabPaneConstraints);

        GridBagConstraints mainGlueConstraints = new GridBagConstraints();
        mainGlueConstraints.fill = GridBagConstraints.HORIZONTAL;
        mainGlueConstraints.gridx = 0;
        mainGlueConstraints.gridy = 1;
        mainGlueConstraints.weightx = 1;
        mainGlueConstraints.weighty = 0;
        add(Box.createGlue(), mainGlueConstraints);
        closeButton = new JButton("Close");
        GridBagConstraints closeButtonConstraints = new GridBagConstraints();
        closeButtonConstraints.fill = GridBagConstraints.NONE;
        closeButtonConstraints.gridx = 1;
        closeButtonConstraints.gridy = 1;
        closeButtonConstraints.weightx = 0;
        closeButtonConstraints.weighty = 0;
        add(closeButton, closeButtonConstraints);

        // chartBackColorLabel.setBounds(10, 20, 140, 25);
        // chartBackColorView.setBounds(155, 20, 95, 25);
        // chartBackColorBtn.setBounds(255, 20, 30, 25);
        // generalFontHeaderLabel.setBounds(10, 50, 90, 25);
        // generalFontHeaderSampleLabel.setBounds(105, 50, 145, 25);
        // generalFontHeaderBtn.setBounds(255, 50, 30, 25);
        // generalFontLabelLabel.setBounds(10, 80, 90, 25);
        // generalFontLabelSampleLabel.setBounds(105, 80, 145, 25);
        // generalFontLabelBtn.setBounds(255, 80, 30, 25);
        // gColorFontPanel.setBounds(5, 70, 290, 115);
        //
        // generalGridCombo.setBounds(10, 20, 120, 25);
        // generalGridStyleLabel.setBounds(135, 20, 45, 25);
        // generalGridStyleCombo.setBounds(185, 20, 100, 25);
        // gGridPanel.setBounds(5, 190, 290, 55);
        //
        // generalLegendLabel.setBounds(10, 20, 70, 25);
        // generalLegendText.setBounds(85, 20, 200, 25);
        // generalDurationLabel.setBounds(10, 50, 120, 25);
        // generalDurationText.setBounds(135, 50, 150, 25);
        // gMiscPanel.setBounds(5, 250, 290, 110);

        // tabPane.setBounds(5, 5, 300, 390);
        // closeButton.setBounds(225, 400, 80, 25);
        //
        // setPreferredSize(new Dimension(310, 430));

        initProperties();
    }

    protected void commit(int origin) {
        if (chart != null) {
            switch (origin) {
                case ORIGIN_Y1:
                    AxisProperties y1axisProperties = getProperties().getY1AxisProperties();
                    chart.setAxisProperties(y1axisProperties, IChartViewer.Y1);
                    y1Panel.setUserLabelInterval(y1axisProperties.getUserLabelInterval());
                    break;
                case ORIGIN_Y2:
                    AxisProperties y2axisProperties = getProperties().getY2AxisProperties();
                    chart.setAxisProperties(y2axisProperties, IChartViewer.Y2);
                    y2Panel.setUserLabelInterval(y2axisProperties.getUserLabelInterval());
                    break;
                case ORIGIN_X:
                    AxisProperties xaxisProperties = getProperties().getXAxisProperties();
                    chart.setAxisProperties(xaxisProperties, IChartViewer.X);
                    xPanel.setUserLabelInterval(xaxisProperties.getUserLabelInterval());
                    break;
                case ORIGIN_GENERAL_PROPERTIES:
                    chart.setChartProperties(getProperties());
                    break;
            }
            chart.repaint();
        }
    }

    // Mouse Listener
    @Override
    public void mouseClicked(MouseEvent e) {
        // ------------------------------
        if (e.getSource() == chartBackColorBtn) {
            Color c = JColorChooser.showDialog(this, "Choose background Color",
                    ColorTool.getColor(getProperties().getBackgroundColor()));
            CometeColor cc = ColorTool.getCometeColor(c);
            if ((c != null) && (cc != null)) {
                properties.setBackgroundColor(cc);
                chartBackColorView.setBackground(c);
                commit(ORIGIN_GENERAL_PROPERTIES);
            }
        } else if (e.getSource() == generalBackColorBtn) {
            Color c = JColorChooser.showDialog(this, "Choose background Color",
                    ColorTool.getColor(getProperties().getGlobalBackgroundColor()));
            CometeColor cc = ColorTool.getCometeColor(c);
            if ((c != null) && (cc != null)) {
                properties.setGlobalBackgroundColor(cc);
                generalBackColorView.setBackground(c);
                commit(ORIGIN_GENERAL_PROPERTIES);
            }
        } else if (e.getSource() == generalFontHeaderBtn) {

            Font f = FontChooser.getNewFont(this, "Choose Header Font",
                    FontTool.getFont(getProperties().getHeaderFont()));
            CometeFont cf = FontTool.getCometeFont(f);
            if ((f != null) && (cf != null)) {
                properties.setHeaderFont(cf);
                generalFontHeaderSampleLabel.setFont(f);
                commit(ORIGIN_GENERAL_PROPERTIES);
            }

        } else if (e.getSource() == generalFontLabelBtn) {

            Font f = FontChooser.getNewFont(this, "Choose label Font",
                    FontTool.getFont(getProperties().getLabelFont()));
            CometeFont cf = FontTool.getCometeFont(f);
            if ((f != null) && (cf != null)) {
                properties.setLabelFont(cf);
                generalFontLabelSampleLabel.setFont(f);
                commit(ORIGIN_GENERAL_PROPERTIES);
            }

        }

    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    // ***************************************************************
    // Action listener
    // ***************************************************************
    @Override
    public void actionPerformed(ActionEvent e) {

        // General ----------------------------------------------------
        if (e.getSource() == generalLabelVisibleCheck) {

            properties.setLegendVisible(generalLabelVisibleCheck.isSelected());
            commit(ORIGIN_GENERAL_PROPERTIES);

            // ------------------------------------------------------------
        } else if (e.getSource() == autoHighlightCheck) {
            properties.setAutoHighlightOnLegend(autoHighlightCheck.isSelected());
            commit(ORIGIN_GENERAL_PROPERTIES);
        } else if (e.getSource() == generalGridCombo) {

            int sel = generalGridCombo.getSelectedIndex();

            switch (sel) {
                case 1: // On X
                    properties.getXAxisProperties().setGridVisible(true);
                    properties.getY1AxisProperties().setGridVisible(false);
                    properties.getY2AxisProperties().setGridVisible(false);
                    break;
                case 2: // On Y1
                    properties.getXAxisProperties().setGridVisible(false);
                    properties.getY1AxisProperties().setGridVisible(true);
                    properties.getY2AxisProperties().setGridVisible(false);
                    break;
                case 3: // On Y2
                    properties.getXAxisProperties().setGridVisible(false);
                    properties.getY1AxisProperties().setGridVisible(false);
                    properties.getY2AxisProperties().setGridVisible(true);
                    break;
                case 4: // On X,Y1
                    properties.getXAxisProperties().setGridVisible(true);
                    properties.getY1AxisProperties().setGridVisible(true);
                    properties.getY2AxisProperties().setGridVisible(false);
                    break;
                case 5: // On X,Y2
                    properties.getXAxisProperties().setGridVisible(true);
                    properties.getY1AxisProperties().setGridVisible(false);
                    properties.getY2AxisProperties().setGridVisible(true);
                    break;
                default: // None
                    properties.getXAxisProperties().setGridVisible(false);
                    properties.getY1AxisProperties().setGridVisible(false);
                    properties.getY2AxisProperties().setGridVisible(false);
                    break;
            }
//            y1Panel.setProperties(properties.getY1AxisProperties(),
//                    properties.getY1AxisProperties().getAnnotation() == JLAxis.TIME_ANNO);
//            y2Panel.setProperties(properties.getY2AxisProperties(),
//                    properties.getY2AxisProperties().getAnnotation() == JLAxis.TIME_ANNO);
//            xPanel.setProperties(properties.getXAxisProperties(),
//                    properties.getXAxisProperties().getAnnotation() == JLAxis.TIME_ANNO);
            commit(ORIGIN_GENERAL_PROPERTIES);

            // ------------------------------------------------------------
        } else if (e.getSource() == generalGridStyleCombo) {

            int s = generalGridStyleCombo.getSelectedIndex();
            properties.getXAxisProperties().setGridStyle(s);
            properties.getY1AxisProperties().setGridStyle(s);
            properties.getY2AxisProperties().setGridStyle(s);
            commit(ORIGIN_GENERAL_PROPERTIES);

            // ------------------------------------------------------------
        } else if (e.getSource() == generalLabelPCombo) {

            int s = generalLabelPCombo.getSelectedIndex();
            properties.setLegendPlacement(s);
            commit(ORIGIN_GENERAL_PROPERTIES);

        }
    }

    // ***************************************************************
    // Change listener
    // ***************************************************************
    @Override
    public void stateChanged(ChangeEvent e) {
    }

    // ***************************************************************
    // Key listener
    // ***************************************************************
    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {

        // General ------------------------------------------------------------
        if (e.getSource() == generalLegendText) {
            String title = generalLegendText.getText().trim();
            if (title.isEmpty()) {
                title = null;
            }
            properties.setTitle(title);
            commit(ORIGIN_GENERAL_PROPERTIES);
        } else if (e.getSource() == generalDurationText) {
            if (generalDurationText.getText().equalsIgnoreCase("infinty")) {
                properties.setDisplayDuration(Double.POSITIVE_INFINITY);
                commit(ORIGIN_GENERAL_PROPERTIES);
            } else {
                try {
                    double d = Double.parseDouble(generalDurationText.getText());
                    properties.setDisplayDuration(d * 1000);
                    generalDurationText.setBackground(Color.WHITE);
                    generalDurationText.setToolTipText("Type Infinity to disable");
                    commit(ORIGIN_GENERAL_PROPERTIES);
                } catch (NumberFormatException err) {
                    generalDurationText.setBackground(Color.ORANGE);
                    generalDurationText.setToolTipText("Display duration: malformed number.");
                }
            }
        } else if (e.getSource() == timePrecisionText) {
            try {
                int t = Integer.parseInt(timePrecisionText.getText());
                properties.setTimePrecision(t);
                timePrecisionText.setBackground(Color.WHITE);
                timePrecisionText.setToolTipText("Precision to consider 2 X points to be the same. 0 by default");
                commit(ORIGIN_GENERAL_PROPERTIES);
            } catch (NumberFormatException err) {
                timePrecisionText.setBackground(Color.ORANGE);
                timePrecisionText.setToolTipText("Time precision: malformed number.");
            }
        }
    } // End keyReleased

    // Error message
    protected void error(String m) {
        JOptionPane.showMessageDialog(this, m, "Chart options error", JOptionPane.ERROR_MESSAGE);
    }

    protected void initProperties() {
        setProperties(null);
    }

    public ChartProperties getProperties() {
        if (properties == null) {
            properties = new ChartProperties();
        }
        return properties.clone();
    }

    public void setProperties(ChartProperties chartProperties) {
        if (chartProperties == null) {
            properties = new ChartProperties();
        } else {
            properties = chartProperties.clone();
        }
        applyProperties();
    }

    protected void applyProperties() {
        ChartProperties tempProperties = getProperties();

        // Axis properties START
        y1Panel.removeAxisPanelListener(this);
        y2Panel.removeAxisPanelListener(this);
        xPanel.removeAxisPanelListener(this);
//        y1Panel.setProperties(tempProperties.getY1AxisProperties(), tempProperties.getY1AxisProperties()
//                .getAnnotation() == JLAxis.TIME_ANNO);
//        y2Panel.setProperties(tempProperties.getY2AxisProperties(), tempProperties.getY2AxisProperties()
//                .getAnnotation() == JLAxis.TIME_ANNO);
//        xPanel.setProperties(tempProperties.getXAxisProperties(),
//                tempProperties.getXAxisProperties().getAnnotation() == JLAxis.TIME_ANNO);
        properties.setY1AxisProperties(y1Panel.getProperties());
        properties.setY2AxisProperties(y2Panel.getProperties());
        properties.setXAxisProperties(xPanel.getProperties());
        y1Panel.addAxisPanelListener(this);
        y2Panel.addAxisPanelListener(this);
        xPanel.addAxisPanelListener(this);
        // Axis properties END

        tempProperties = getProperties();
        generalLegendText
                .setText(tempProperties.getTitle() == null ? ObjectUtils.EMPTY_STRING : tempProperties.getTitle());
        chartBackColorView.setBackground(ColorTool.getColor(tempProperties.getBackgroundColor()));
        generalBackColorView.setBackground(ColorTool.getColor(tempProperties.getGlobalBackgroundColor()));
        generalFontHeaderSampleLabel.setFont(FontTool.getFont(tempProperties.getHeaderFont()));
        generalFontLabelSampleLabel.setFont(FontTool.getFont(tempProperties.getLabelFont()));
        generalLabelVisibleCheck.setSelected(tempProperties.isLegendVisible());

        // Legend placement START
        generalLabelPCombo.removeActionListener(this);
        generalLabelPCombo.setSelectedIndex(tempProperties.getLegendPlacement());
        generalLabelPCombo.addActionListener(this);
        // Legend placement END

        generalDurationText.setText(Double.toString(tempProperties.getDisplayDuration()));
        timePrecisionText.setText(Integer.toString(tempProperties.getTimePrecision()));
        autoHighlightCheck.setSelected(tempProperties.isAutoHighlightOnLegend());

        // Axis grid selection

        // default: None
        int sel = 0;

        if ((tempProperties.getXAxisProperties().isGridVisible())
                && (!tempProperties.getY1AxisProperties().isGridVisible())
                && (!tempProperties.getY2AxisProperties().isGridVisible())) {
            // On X
            sel = 1;
        } else if ((!tempProperties.getXAxisProperties().isGridVisible())
                && (tempProperties.getY1AxisProperties().isGridVisible())
                && (!tempProperties.getY2AxisProperties().isGridVisible())) {
            // On Y1
            sel = 2;
        } else if ((!tempProperties.getXAxisProperties().isGridVisible())
                && (!tempProperties.getY1AxisProperties().isGridVisible())
                && (tempProperties.getY2AxisProperties().isGridVisible())) {
            // On Y2
            sel = 3;
        } else if ((tempProperties.getXAxisProperties().isGridVisible())
                && (tempProperties.getY1AxisProperties().isGridVisible())
                && (!tempProperties.getY2AxisProperties().isGridVisible())) {
            // On X,Y1
            sel = 4;
        } else if ((tempProperties.getXAxisProperties().isGridVisible())
                && (!tempProperties.getY1AxisProperties().isGridVisible())
                && (tempProperties.getY2AxisProperties().isGridVisible())) {
            // On X,Y2
            sel = 5;
        }

        // Grid position START
        generalGridCombo.removeActionListener(this);
        generalGridCombo.setSelectedIndex(sel);
        generalGridCombo.addActionListener(this);
        // Grid position END

        // Grid style START
        generalGridStyleCombo.removeActionListener(this);
        generalGridStyleCombo.setSelectedIndex(tempProperties.getXAxisProperties().getGridStyle());
        generalGridStyleCombo.addActionListener(this);
        // Grid style END
    }

    @Override
    public void axisPanelChanged(AxisPanelEvent event) {
        if (event.getSource() == y1Panel) {
            properties.setY1AxisProperties(y1Panel.getProperties());
            commit(ORIGIN_Y1);
        } else if (event.getSource() == y2Panel) {
            properties.setY2AxisProperties(y2Panel.getProperties());
            commit(ORIGIN_Y2);
        } else if (event.getSource() == xPanel) {
            properties.setXAxisProperties(xPanel.getProperties());
            commit(ORIGIN_X);
        }
    }

    public JButton getCloseButton() {
        return closeButton;
    }

    public void setScalePanelEnabled(boolean enabled, int axis) {
        AxisPanel axisPanel = getAxisPanel(axis);
        if (axisPanel != null) {
            axisPanel.setScalePanelEnabled(enabled);
        }
    }

    private AxisPanel getAxisPanel(int axis) {
        switch (axis) {
            case AxisPanel.Y1_TYPE:
                return y1Panel;
            case AxisPanel.Y2_TYPE:
                return y2Panel;
            case AxisPanel.X_TYPE:
                return xPanel;
            default:
                return null;
        }
    }

}
