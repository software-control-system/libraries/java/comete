/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.Color;
import java.awt.Font;
import java.util.concurrent.Semaphore;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;

import org.slf4j.LoggerFactory;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.renderer.AdaptedFontDefaultListCellRenderer;

/**
 * Default implementation of {@link ValueListCellRenderer}
 *
 * @author Rapha&euml;l GIRARDOT
 */
public class DefaultValueListCellRenderer extends AdaptedFontDefaultListCellRenderer implements ValueListCellRenderer {

    private static final long serialVersionUID = 3125974732530238268L;

    private String[] displayableValues;
    private final Semaphore displaySema = new Semaphore(1);
    protected boolean sameTextAndTooltip;

    public DefaultValueListCellRenderer(JComponent comp) {
        super(comp);
        displayableValues = null;
        sameTextAndTooltip = true;
    }

    /**
     * Returns whether this renderer will use text as tooltip
     * 
     * @return A <code>boolean</code>
     */
    public boolean isSameTextAndTooltip() {
        return sameTextAndTooltip;
    }

    /**
     * Sets whether this renderer should use text as tooltip
     * 
     * @param sameTextAndTooltip Whether this renderer should use text as tooltip
     */
    public void setSameTextAndTooltip(boolean sameTextAndTooltip) {
        this.sameTextAndTooltip = sameTextAndTooltip;
    }

    @Override
    public String[] getDisplayableValues() {
        return displayableValues;
    }

    @Override
    public void setDisplayableValues(String[] values) {
        try {
            displaySema.acquire();
            this.displayableValues = values;
            displaySema.release();
        } catch (InterruptedException e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                    .warn(getClass().getName() + ".setDisplayableValues canceled");
        }
    }

    @Override
    protected void applyToComponent(JComponent comp, Font font, Color foreground, String tooltipText,
            JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus,
            boolean derive) {
        try {
            displaySema.acquire();
            // Selected value index recovering START
            if ((index < 0) && (value != null) && (list != null) && (list.getModel() != null)) {
                for (int i = 0; i < list.getModel().getSize(); i++) {
                    try {
                        if (areSameValues(value, list.getModel().getElementAt(i))) {
                            index = i;
                            break;
                        }
                    } catch (ArrayIndexOutOfBoundsException aioobe) {
                        // Simply ignore this case. The model has a concurrent change
                    }
                }
            }
            // Selected value index recovering END
            if ((displayableValues != null) && (index > -1) && (index < displayableValues.length)) {
                if (comp instanceof JLabel) {
                    JLabel component = (JLabel) comp;
                    component.setText(displayableValues[index]);
                    if (isSameTextAndTooltip()) {
                        tooltipText = component.getText();
                    }
                }
            }
            displaySema.release();
        } catch (InterruptedException e) {
            // Should never happen
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                    .warn(getClass().getName() + ".getListCellRendererComponent canceled");
        }
        super.applyToComponent(comp, font, foreground, tooltipText, list, value, index, isSelected, cellHasFocus,
                derive);
    }

    /**
     * Compares 2 {@link Object}s to check whether they represent the same value
     *
     * @param o1 The 1st {@link Object}
     * @param o2 The 2nd {@link Object}
     * @return A <code>boolean</code> value
     */
    public static boolean areSameValues(Object o1, Object o2) {
        boolean sameValues = false;
        if (ObjectUtils.sameObject(o1, o2)) {
            sameValues = true;
        } else if ((o1 instanceof Number) && (o2 instanceof Number)) {
            double d1 = ((Number) o1).doubleValue();
            double d2 = ((Number) o2).doubleValue();
            if (d1 == d2) {
                sameValues = true;
            } else if (Double.isNaN(d1) && Double.isNaN(d2)) {
                sameValues = true;
            }
        }
        return sameValues;
    }

}
