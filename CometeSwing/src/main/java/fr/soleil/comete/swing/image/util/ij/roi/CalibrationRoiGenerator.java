/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Stroke;

import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.image.ijviewer.components.ArrowStroke;
import fr.soleil.lib.project.swing.icons.Icons;
import ij.gui.Line;
import ij.gui.Roi;
import ij.gui.Toolbar;

public class CalibrationRoiGenerator extends DataRoiGenerator {

    protected static final String CALIBRATION = "Calibration";
    protected static final Cursor CURSOR = Icons.generateCursorFromIcon(ImageViewer.CALIBRATION_MODE_ICON,
            ImageViewer.MODE_CALIBRATION_ACTION);
    protected static final Stroke STROKE = new ArrowStroke(IJCanvas.DEFAULT_CALIBRATION_ROI_STROKE, ArrowStroke.BOTH, 8,
            Math.PI / 2);
    protected static final Stroke SELECTED_STROKE = new ArrowStroke(IJCanvas.DEFAULT_SELECTED_ROI_STROKE,
            ArrowStroke.BOTH, 8, Math.PI / 2);

    public CalibrationRoiGenerator(String text, String description, ImageViewer viewer) {
        super(text, description, ImageViewer.CALIBRATION_MODE_ICON, viewer);
    }

    @Override
    public boolean isPixelSizeDependent() {
        return true;
    }

    @Override
    public int getRoiMode() {
        return IJRoiManager.CALIBRATION_ROI_MODE;
    }

    @Override
    public Roi createNewRoi(int sx, int sy, IJRoiManager roiManager) {
        Line roi = new Line(sx, sy, roiManager);
        roi.setName(CALIBRATION);
        return roi;
    }

    @Override
    public boolean isCompatibleWithRoi(Roi roi) {
        return ((roi instanceof Line) && CALIBRATION.equals(roi.getName()));
    }

    @Override
    public Cursor getCursor() {
        return CURSOR;
    }

    @Override
    protected int getTool() {
        return Toolbar.LINE;
    }

    @Override
    protected boolean isDialogVisible(ImageViewer viewer) {
        return viewer.getCalibrationDialog().isVisible();
    }

    @Override
    protected void doShowDataDialog(ImageViewer viewer) {
        if (viewer != null) {
            checkWindowAdapters(viewer.getCalibrationDialog(), viewer);
            viewer.getCalibrationDialog().setLocationRelativeTo(viewer);
            viewer.getCalibrationDialog().setVisible(true);
        }
    }

    @Override
    protected void doHideDataDialog(ImageViewer viewer) {
        if (viewer != null) {
            viewer.getCalibrationDialog().setVisible(false);
        }
    }

    @Override
    protected void updateDataBounds(ImageViewer viewer) {
        // nothing to do
    }

    @Override
    protected void updateData(IMaskedImageViewer viewer) {
        if (viewer instanceof ImageViewer) {
            ((ImageViewer) viewer).getCalibrationDialog().updateFields();
        }
    }

    @Override
    public String getActionCommand() {
        return ImageViewer.MODE_CALIBRATION_ACTION;
    }

    @Override
    protected Stroke generateRoiStroke() {
        return STROKE;
    }

    @Override
    protected Stroke generateSelectedRoiStroke() {
        return SELECTED_STROKE;
    }

    @Override
    protected Color generateRoiColor() {
        return IJCanvas.DEFAULT_CALIBRATION_ROI_COLOR;
    }

}
