/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.util.Comparator;

import fr.soleil.comete.swing.chart.data.AbstractDataView;

/**
 * An interface for somethng that manages {@link AbstractDataView}s and uses a {@link Comparator} for them.
 * 
 * @author Rapha&euml;l GIRARDOT
 *
 */
public interface IDataViewManager {

    /**
     * Returns the used {@link AbstractDataView} {@link Comparator}.
     * 
     * @return A {@link Comparator}.
     */
    public Comparator<AbstractDataView> getDataViewComparator();

    /**
     * Sets the {@link AbstractDataView} {@link Comparator} to use.
     * 
     * @param comparator The {@link AbstractDataView} {@link Comparator} to use.
     */
    public void setDataViewComparator(Comparator<AbstractDataView> comparator);

}
