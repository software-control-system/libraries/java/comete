/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.data;

/**
 * {@link IDataListContainer} for XY data
 * 
 * @author huriez
 */
public class DataXY implements IDataListContainer {

    private static final long serialVersionUID = -8539541590762577386L;

    private DataList dataY;
    private DataList dataX; // View plotted on the xAxis

    public DataXY(DataList dy, DataList dx) {
        dataY = dy;
        dataX = dx;
    }

    // Find the next point for XY mode
    @Override
    public void next() {
        // Correlation mode
        dataY = dataY.next;
        while ((dataY != null) && (dataX != null) && (dataX.next != null) && (dataX.next.getX() <= dataY.getX())) {
            dataX = dataX.next;
        }
    }

    // Go to starting time position
    @Override
    public void init() {
        if (dataY.getX() < dataX.getX()) {
            while ((dataY != null) && (dataY.getX() < dataX.getX())) {
                dataY = dataY.next;
            }
        } else {
            while ((dataX != null) && (dataX.next != null) && (dataX.next.getX() < dataY.getX())) {
                dataX = dataX.next;
            }
        }
    }

    @Override
    public boolean isValid() {
        return ((dataY != null) && (dataX != null));
    }

    @Override
    public DataList getDataY() {
        return dataY;
    }

    public DataList getDataX() {
        return dataX;
    }

}
