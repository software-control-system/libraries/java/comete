/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.component;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JToolTip;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.event.ChartViewerEvent.Reason;
import fr.soleil.comete.definition.event.CometeEvent;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.DragProperties;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.definition.widget.util.ChartEventDelegate;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.axis.model.AxisAttributes;
import fr.soleil.comete.swing.chart.axis.scale.AbstractAxisScale;
import fr.soleil.comete.swing.chart.axis.scale.LinearScale;
import fr.soleil.comete.swing.chart.axis.scale.LogarithmicScale;
import fr.soleil.comete.swing.chart.axis.view.AbstractAxisView;
import fr.soleil.comete.swing.chart.axis.view.HorizontalAxisView;
import fr.soleil.comete.swing.chart.axis.view.VerticalAxisView;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.data.DataList;
import fr.soleil.comete.swing.chart.data.DataListContainer;
import fr.soleil.comete.swing.chart.data.DataView;
import fr.soleil.comete.swing.chart.data.DataViewList;
import fr.soleil.comete.swing.chart.data.DataXY;
import fr.soleil.comete.swing.chart.data.IDataListContainer;
import fr.soleil.comete.swing.chart.data.SearchInfo;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.chart.view.graphics.ChartGraphics;
import fr.soleil.comete.swing.util.CfFileReader;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.swing.util.OFormat;
import fr.soleil.comete.swing.util.SwingFormat;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.FontUtils;
import fr.soleil.lib.project.awt.adapter.MacOSXCompatibleMouseAdapter;
import fr.soleil.lib.project.date.DateUtil;

/**
 * {@link JComponent} that handles axis and data drawing for a chart. A part of data drawing is delegated to
 * {@link ChartGraphics} class.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ChartView extends JComponent
        implements MouseListener, MouseMotionListener, ActionListener, ComponentListener {

    private static final long serialVersionUID = -3732766685325389941L;

    protected static final String VISIBLE = "visible";
    protected static final String GRID = "grid";
    protected static final String SUBGRID = "subgrid";
    protected static final String GRID_STYLE = "grid_style";
    protected static final String MIN = "min";
    protected static final String MAX = "max";
    protected static final String AUTOSCALE = "autoscale";
    protected static final String SCALE = "scale";
    protected static final String FORMAT = "format";
    protected static final String TITLE = "title";
    protected static final String COLOR = "color";
    protected static final String LABEL_FONT = "label_font";
    protected static final String FIT_DISPLAY_DURATION = "fit_display_duration";
    protected static final String SAMPLING_ENABLED = "sampling_enabled";
    protected static final String SAMPLING_MEAN = "sampling_mean";
    protected static final String SAMPLING_PEAK_TYPE = "sampling_peak_type";

    protected static final String KO_MESSAGE = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.render.ko");
    protected static final Color INFO_PANEL_BACKGROUND = new Color(255, 247, 200);
    protected static final Border INFO_PANEL_BORDER = new LineBorder(Color.BLACK, 1);
    protected static final Insets TOOLTIP_INSETS = new Insets(2, 2, 2, 2);
    protected static final Color DEFAULT_AREA_SELECTION_COLOR = new Color(80, 255, 160, 150);

    // Listeners
    protected final ChartEventDelegate chartEventDelegate;
    protected final MacOSXCompatibleMouseAdapter macOSXAdapter;

    protected final DataViewList dataViewList;
    protected Color chartBackground;
    protected HorizontalAxisView xAxis;
    protected VerticalAxisView y1Axis;
    protected VerticalAxisView y2Axis;
    protected String title;
    protected final Rectangle titleR;
    protected final Rectangle viewR;
    protected Dimension margin;
    protected int titleWidth;
    protected int axisHeight;
    protected int axisWidth;
    protected int y1AxisThickness;
    protected int y2AxisThickness;
    protected int xAxisThickness;
    protected int xAxisUpMargin;
    protected int xOrg;
    protected int yOrg;
    protected int yOrgX;
    protected int xOrgY1;
    protected int xOrgY2;
    protected int yOrgY;
    protected boolean titleVisible;
    protected Font labelFont;
    protected boolean areaSelection;
    protected boolean useSelectionArea;
    protected double[] selectedCoordinates;
    protected Color selectionAreaColor;
    protected boolean zoomed;
    protected boolean oldAutoScaleY1;
    protected boolean oldAutoScaleY2;
    protected double oldMinY1;
    protected double oldMaxY1;
    protected double oldMinY2;
    protected double oldMaxY2;
    protected double oldTime;
    protected boolean paintAxisFirst;
    protected boolean zoomDrag;
    protected boolean potentialTranslateDrag, translateDrag;
    protected boolean moveDataView;
    protected int zoomX;
    protected int zoomY;
    protected int lastX;
    protected int lastY;
    protected SearchInfo lastSearch;
    protected boolean lastSearchHighlighted;
    protected double lastSearchXStart, lastSearchYStart;
    protected double lastSearchA0X, lastSearchA0;
    protected final JButton zoomButton;
    protected int zoomButtonX, zoomButtonY, zoomButtonDx, zoomButtonDy;
    protected volatile boolean ignoreZoomButtonAction;
    protected boolean zoomButtonMoved;
    protected boolean useControlKeyToZoom;
    protected boolean limitDrawingZone;
    protected double displayDuration;
    protected double maxDisplayDuration;
    protected String displayableTooltipText;
    protected MouseEvent displayableTooltipEvent;
    protected boolean ipanelVisible;
    protected final JToolTip infoTooltip;
    protected Popup infoPopup;

    protected FontRenderContext lastFontRenderContext;
    protected int lastWidth;
    protected int lastHeight;

    protected boolean transparencyAllowed;
    protected SamplingProperties samplingProperties;
    protected DragProperties dragProperties;
    protected volatile boolean canWarnForSampling;
    protected boolean fireSelectionEventOnAnyClick;

    public ChartView(ChartEventDelegate chartEventDelegate, DataViewList dataViewList) {
        super();
        this.chartEventDelegate = chartEventDelegate;
        this.dataViewList = (dataViewList == null ? new DataViewList() : dataViewList);
        this.macOSXAdapter = new MacOSXCompatibleMouseAdapter();
        addKeyListener(macOSXAdapter);
        setBackground(Color.WHITE);
        chartBackground = Color.WHITE;
        xAxis = new HorizontalAxisView(new AxisAttributes(IChartViewer.HORIZONTAL_DOWN, chartBackground),
                SwingConstants.TOP, SwingConstants.BOTTOM);
        xAxis.setAxeName("X");
        y1Axis = new VerticalAxisView(new AxisAttributes(IChartViewer.VERTICAL_LEFT, chartBackground),
                SwingConstants.RIGHT, SwingConstants.LEFT);
        y1Axis.setAxeName("Y1");
        y1Axis.setTitlePosition(SwingConstants.LEFT);
        y2Axis = new VerticalAxisView(new AxisAttributes(IChartViewer.VERTICAL_RIGHT, chartBackground),
                SwingConstants.LEFT, SwingConstants.RIGHT);
        y2Axis.setAxeName("Y2");
        y2Axis.setTitlePosition(SwingConstants.RIGHT);
        title = null;
        margin = new Dimension(5, 5);
        titleR = new Rectangle(0, 0, 0, 0);
        viewR = new Rectangle(0, 0, 0, 0);
        titleWidth = 0;
        axisHeight = 0;
        axisWidth = 0;
        y1AxisThickness = 0;
        y2AxisThickness = 0;
        xAxisThickness = 0;
        xAxisUpMargin = 0;
        xOrg = 0;
        yOrg = 0;
        xOrgY1 = 0;
        xOrgY2 = 0;
        yOrgY = 0;
        title = null;
        titleVisible = true;
        labelFont = getFont();
        zoomed = false;
        areaSelection = false;
        useSelectionArea = false;
        selectedCoordinates = null;
        selectionAreaColor = DEFAULT_AREA_SELECTION_COLOR;
        oldAutoScaleY1 = false;
        oldAutoScaleY2 = false;
        oldMinY1 = 0;
        oldMaxY1 = 0;
        oldMinY2 = 0;
        oldMaxY2 = 0;
        oldTime = 0;
        paintAxisFirst = false;
        zoomDrag = false;
        potentialTranslateDrag = false;
        translateDrag = false;
        zoomX = 0;
        zoomY = 0;
        lastX = 0;
        lastY = 0;
        useControlKeyToZoom = true;
        limitDrawingZone = true;
        displayDuration = Double.POSITIVE_INFINITY;
        maxDisplayDuration = Double.POSITIVE_INFINITY;
        displayableTooltipText = null;
        displayableTooltipEvent = null;
        ipanelVisible = false;
        infoTooltip = new JToolTip() {
            private static final long serialVersionUID = 8324071641315629883L;

            @Override
            public Insets getInsets() {
                return TOOLTIP_INSETS;
            }
        };
        infoTooltip.setComponent(this);
        infoTooltip.setBackground(INFO_PANEL_BACKGROUND);
        infoTooltip.setBorder(INFO_PANEL_BORDER);
        infoPopup = null;

        zoomButton = new JButton(Chart.MENU_EXIT_ZOOM);
        zoomButton.setFont(labelFont);
        zoomButton.setMargin(new Insets(2, 2, 1, 1));
        zoomButton.setVisible(false);
        zoomButton.addActionListener(this);
        zoomButton.addMouseMotionListener(this);
        zoomButton.addMouseListener(this);
        zoomButtonX = 0;
        zoomButtonY = 0;
        zoomButtonDx = 0;
        zoomButtonDy = 0;
        ignoreZoomButtonAction = false;
        zoomButtonMoved = false;

        lastWidth = 0;
        lastHeight = 0;
        lastFontRenderContext = null;

        samplingProperties = new SamplingProperties();
        dragProperties = new DragProperties();

        add(zoomButton);
        // Set up listeners
        addMouseListener(this);
        addMouseMotionListener(this);
        addComponentListener(this);
        canWarnForSampling = true;
        fireSelectionEventOnAnyClick = true;
    }

    protected <V extends AbstractDataView> void paintDataViews(Graphics2D g2, List<V>[] views, boolean forceAxisPaint) {
        if ((g2 != null) && (views != null)) {
            List<V> y1Views = views[0];
            List<V> y2Views = views[1];
            List<V> xViews = views[2];
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            boolean measure = false;
            if (lastFontRenderContext == null) {
                lastFontRenderContext = g2.getFontRenderContext();
                measure = true;
            }
            g2.setPaintMode();
            // Paint chart background
            if (isOpaque() && forceAxisPaint) {
                g2.setColor(getBackground());
                g2.fillRect(0, 0, lastWidth, lastHeight);
            }
            if (measure) {// Compute bounds of label and graph
                measureGraphItems(y1Views, y2Views, xViews);
            }
            if (forceAxisPaint) {
                // Draw label and title
                ChartGraphics.paintTitle(title, g2, titleR, titleWidth, getFont(), getForeground());
            }
            if (forceAxisPaint) {
                xOrg = viewR.x + y1AxisThickness;
                yOrg = viewR.y + axisHeight + xAxisUpMargin;
                xOrgY1 = xOrg;
                xOrgY2 = xOrg + axisWidth;
                yOrgY = viewR.y + xAxisUpMargin;
                // Translate origins to calculated ones
                Point zeroY1 = transform(0, 0, xOrg, yOrg, y1Axis);
                Point zeroY2 = transform(0, 0, xOrg, yOrg, y2Axis);
                if (y1Axis.getAttributes().getOrigin() == IChartViewer.VERTICAL_ORGX) {
                    xOrgY1 = zeroY1.x;
                }
                if (y2Axis.getAttributes().getOrigin() == IChartViewer.VERTICAL_ORGX) {
                    xOrgY2 = zeroY2.x;
                }
                switch (xAxis.getAttributes().getOrigin()) {
                    case IChartViewer.HORIZONTAL_ORGY1:
                        yOrgX = zeroY1.y;
                        break;
                    case IChartViewer.HORIZONTAL_ORGY2:
                        yOrgX = zeroY2.y;
                        break;
                    case IChartViewer.HORIZONTAL_UP:
                        yOrgX = yOrgY;
                        break;
                    default:
                        yOrgX = yOrg;
                        break;
                }
            }
            // Fill chart background
            if (((!isOpaque()) || (!chartBackground.equals(getBackground()))) && axisWidth > 0 && axisHeight > 0) {
                if (forceAxisPaint) {
                    g2.setColor(chartBackground);
                    g2.fillRect(xOrg, yOrg - axisHeight, axisWidth, axisHeight);
                }
            }
            // Paint zoom stuff
            paintZoomSelection(g2);
            paintZoomButton(xOrg, yOrgY);
            // Paint selection stuff
            paintAreaSelection(g2);
            if (isPaintAxisFirst()) {
                if (forceAxisPaint) {
                    // Draw axes
                    drawAxes(g2, lastFontRenderContext, xOrg, yOrgX, xOrgY1, xOrgY2, yOrgY, y1Views, y2Views, xViews);
                }
                // Draw data
                drawData(g2, xOrg, yOrg, y1Views, y2Views, xViews);
            } else {
                // Draw data
                drawData(g2, xOrg, yOrg, y1Views, y2Views, xViews);
                if (forceAxisPaint) {
                    // Draw axes
                    drawAxes(g2, lastFontRenderContext, xOrg, yOrgX, xOrgY1, xOrgY2, yOrgY, y1Views, y2Views, xViews);
                }
            }
            if (forceAxisPaint) {
                redrawPanel(g2, xOrg, yOrg);
            }
        }

    }

    /**
     * Paint the components. Use the repaint method to repaint the graph.
     * 
     * @param g
     *            Graphics object.
     */
    @Override
    public void paint(Graphics g) {
        canWarnForSampling = true;
        lastWidth = getWidth();
        lastHeight = getHeight();
        if (isVisible() && isShowing()) {
            if (g instanceof Graphics2D) {
                // Create a vector containing all views
                List<DataView>[] views = dataViewList.getDataViewsByAxis(false, IChartViewer.Y1, IChartViewer.Y2,
                        IChartViewer.X);
                Graphics2D g2 = (Graphics2D) g;
                paintDataViews(g2, views, true);
            } else {
                // Unsupported Graphics
                Font font = getFont();
                g.setColor(getForeground());
                Dimension fontSize = FontUtils.measureString(KO_MESSAGE, font);
                int x = 0, y = 0;
                if (fontSize != null) {
                    x = Math.max((lastWidth - fontSize.width) / 2, 0);
                    y = Math.max((lastHeight - fontSize.height) / 2, 0);
                }
                g.drawString(KO_MESSAGE, x, y);
            }
        }
        // Paint swing stuff
        paintComponents(g);
        paintBorder(g);
    }

    public void paintDataViews(List<DataView>[] views) {
        if (isVisible() && isShowing()) {
            if ((lastWidth > 0) && (lastHeight > 0)) {
                Graphics g = getGraphics();
                if (g instanceof Graphics2D) {
                    Graphics2D g2 = (Graphics2D) g;
                    paintDataViews(g2, views, false);
                }
            } else {
                repaint();
            }
        }
    }

    // ****************************************
    // redraw the panel
    protected void redrawPanel(Graphics g, int xOrg, int yOrg) {
        if (ipanelVisible) {
            // Udpate serachInfo
            Point p;
            AbstractDataView vy = lastSearch.getDataView();
            AbstractDataView vx = lastSearch.getXDataView();
            DataList dy = lastSearch.getValue();
            DataList dx = lastSearch.getXValue();
            AbstractAxisView yaxis = getAxis(lastSearch.getAxis());
            if (vx == null) {
                p = transform(dy.getX(), vy.getTransformedValue(dy.getY()), xOrg, yOrg, yaxis);
            } else {
                p = transform(vx.getTransformedValue(dx.getY()), vy.getTransformedValue(dy.getY()), xOrg, yOrg, yaxis);
            }
            lastSearch.setX(p.x);
            lastSearch.setY(p.y);
            showPanel(g, lastSearch);
        }
    }

    protected String[] buildPanelString(SearchInfo si) {
        return buildPanelString(si, false);
    }

    protected String[] buildPanelString(SearchInfo si, boolean nameOnly) {
        String[] str = null;
        if (si != null) {
            DataList dataList = si.getValue();
            AbstractDataView dataView = si.getDataView();
            if ((dataList != null) && (dataView != null)) {
                String format = dataView.getFormat();
                int axisId = IChartViewer.Y1;
                Object tmp = dataView.getAxis();
                if (tmp instanceof Number) {
                    axisId = ((Number) tmp).intValue();
                }
                StringBuilder nameBuilder = new StringBuilder(dataView.getExtendedName(chartEventDelegate.getChart()));
                AbstractAxisView axis = getAxis(si.getAxis());
                if (axis != null) {
                    nameBuilder.append(" (").append(axis.getAxeName()).append(")");
                }
                if (nameOnly) {
                    str = new String[] { nameBuilder.toString() };
                } else {
                    int strCount;
                    String xValue;
                    double y, ytrans;
                    y = dataList.getY();
                    ytrans = dataView.getTransformedValue(y);
                    boolean diffY = (y != ytrans);
                    boolean hasError = (dataList.getError() != 0) && (!Double.isNaN(dataList.getError()));
                    boolean hasXOffset = (dataView.getA0X() != 0);
                    boolean xTime = xAxis.getScale().isTimeScale();
                    if (xTime) {
                        xValue = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.time.title")
                                + ChartUtils.formatTimeValue(dataList.getX());
                    } else {
                        xValue = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.index.title")
                                + dataList.getX();
                    }
                    int index = 0;
                    if (si.getXDataView() == null) {
                        strCount = 3;
                        if (diffY) {
                            strCount++;
                        }
                        if (hasError) {
                            strCount++;
                        }
                        if (hasXOffset) {
                            strCount += 2;
                        }
                        str = new String[strCount];
                        str[index++] = nameBuilder.toString();
                        str[index++] = xValue;
                        if (hasXOffset) {
                            str[index++] = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.x.offset.title")
                                    + (xTime ? DateUtil.elapsedTimeToStringBuilder(null, (long) dataView.getA0X())
                                            : dataView.getA0X());
                            double xtrans = dataView.getOffsettedXValue(dataList.getX());
                            str[index++] = ChartUtils.MESSAGES
                                    .getString("fr.soleil.comete.swing.chart.x.offsetted.title")
                                    + (xTime ? ChartUtils.formatTimeValue(xtrans) : xtrans);
                        }
                    } else {
                        double x, xtrans;
                        x = si.getXValue().getY();
                        xtrans = si.getXDataView().getTransformedValue(x);
                        boolean diffX = (x != xtrans);
                        strCount = 4;
                        if (diffX) {
                            strCount++;
                        }
                        if (diffY) {
                            strCount++;
                        }
                        if (hasError) {
                            strCount++;
                        }
                        str = new String[strCount];
                        str[index++] = nameBuilder.toString();
                        str[index++] = xValue;
                        str[index++] = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.x.title") + x;
                        if (diffX) {
                            str[index++] = ChartUtils.MESSAGES
                                    .getString("fr.soleil.comete.swing.chart.x.transformed.title") + xtrans;
                        }
                    }
                    str[index++] = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.y.title")
                            + getYStringValue(y, axisId, format) + CometeConstants.SPACE + dataView.getUnit();
                    if (diffY) {
                        str[index++] = ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.y.transformed.title")
                                + getYStringValue(ytrans, axisId, format) + CometeConstants.SPACE + dataView.getUnit();
                    }
                    if (hasError) {
                        str[index++] = ChartUtils.MESSAGES
                                .getString("fr.soleil.comete.swing.chart.error.moreless.title") + dataList.getError()
                                + CometeConstants.SPACE + dataView.getUnit();
                    }
                }
            }
        }
        return str;
    }

    protected String getYStringValue(double y, int axisId, String format) {
        String result;
        AbstractAxisView axis = getAxis(axisId);
        if (axis.getScale().isTimeScale()) {
            result = ChartUtils.formatTimeValue(y);
        } else if ((format == null) || format.isEmpty()) {
            result = Double.toString(y);
        } else {
            result = SwingFormat.formatValue(y, format, true);
        }
        return result;
    }

    @Override
    public String getToolTipText() {
        return (displayableTooltipText == null ? super.getToolTipText() : displayableTooltipText);
    }

    protected void cleanInfoPopup() {
        if (infoPopup != null) {
            infoPopup.hide();
            infoPopup = null;
        }
    }

    protected Popup getInfoPopup(int x, int y) {
        if (infoPopup == null) {
            infoPopup = PopupFactory.getSharedInstance().getPopup(this, infoTooltip, x, y);
        }
        return infoPopup;
    }

    /**
     * Display the value tooltip.
     * 
     * @param g Graphics object
     * @param si SearchInfo structure.
     * @see #searchNearest(int, int, int)
     */
    public void showPanel(Graphics g, SearchInfo si) {
        showPanel(g, si, moveDataView);
    }

    protected void showPanel(Graphics g, SearchInfo si, boolean nameOnly) {
        if (g instanceof Graphics2D) {
            Graphics2D g2 = (Graphics2D) g;
            Rectangle2D bounds;
            int maxh = 0;
            int maxw = 0;
            int x0 = 0, y0 = 0;
            String[] str = buildPanelString(si, nameOnly);

            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            FontRenderContext frc = g2.getFontRenderContext();

            g.setPaintMode();
            g.setFont(labelFont);

            // Do not show panel if no text
            if ((str != null) && (str.length > 0)) {
                // Compute panel size
                bounds = g.getFont().getStringBounds(str[0], frc);
                maxw = (int) bounds.getWidth();
                // h = maxh = (int) bounds.getHeight();

                for (int i = 1; i < str.length; i++) {
                    bounds = g.getFont().getStringBounds(
                            str[i].replace("<b>", ObjectUtils.EMPTY_STRING).replace("</b>", ObjectUtils.EMPTY_STRING),
                            frc);
                    if ((int) bounds.getWidth() > maxw) {
                        maxw = (int) bounds.getWidth();
                    }
                    maxh += bounds.getHeight();
                }

                maxw += 10;
                maxh += 10;

                g.setColor(Color.black);

                Point location = null;
                if (displayableTooltipEvent != null) {
                    location = displayableTooltipEvent.getLocationOnScreen();
                }

                if (location != null) {
                    Point myLocation = getLocationOnScreen();
                    if (myLocation != null) {
                        switch (si.getPlacement()) {
                            case SearchInfo.BOTTOMRIGHT:
                                x0 = si.getX() + 10;
                                y0 = si.getY() + 10;
                                if (!nameOnly) {
                                    g.drawLine(si.getX(), si.getY(), si.getX() + 10, si.getY() + 10);
                                }
                                break;
                            case SearchInfo.BOTTOMLEFT:
                                x0 = si.getX() - 10 - maxw;
                                y0 = si.getY() + 10;
                                if (!nameOnly) {
                                    g.drawLine(si.getX(), si.getY(), si.getX() - 10, si.getY() + 10);
                                }
                                break;
                            case SearchInfo.TOPRIGHT:
                                x0 = si.getX() + 10;
                                y0 = si.getY() - 10 - maxh;
                                if (!nameOnly) {
                                    g.drawLine(si.getX(), si.getY(), si.getX() + 10, si.getY() - 10);
                                }
                                break;
                            case SearchInfo.TOPLEFT:
                                x0 = si.getX() - 10 - maxw;
                                y0 = si.getY() - 10 - maxh;
                                if (!nameOnly) {
                                    g.drawLine(si.getX(), si.getY(), si.getX() - 10, si.getY() - 10);
                                }
                                break;
                        }
                        x0 += myLocation.x;
                        y0 += myLocation.y;

                        StringBuilder buffer = new StringBuilder("<html><body>");
                        for (String info : str) {
                            buffer.append("<p>").append(info).append("</p>");
                        }
                        buffer.append("</body></html>");
                        displayableTooltipText = buffer.toString();
                        infoTooltip.setFont(getFont());
                        infoTooltip.setTipText(displayableTooltipText);
                        getInfoPopup(x0, y0).show();

                        lastSearch = si;
                        ipanelVisible = true;
                    }
                }
            }
        }
    }

    protected void drawAxes(Graphics2D g, FontRenderContext frc, int xOrg, int yOrg, int xOrgY1, int xOrgY2, int yOrgY,
            List<? extends AbstractDataView> y1Views, List<? extends AbstractDataView> y2Views,
            List<? extends AbstractDataView> xViews) {
        int gridXMin = xOrg;
        int gridXMax = xOrg + axisWidth;
        int gridYMin = yOrgY;
        int gridYMax = yOrgY + axisHeight;
        boolean y1AxisVisible = y1Axis.isVisible() && !y1Views.isEmpty();
        boolean y2AxisVisible = y2Axis.isVisible() && !y2Views.isEmpty();
        if (y1AxisVisible) {
            y1Axis.paintAxis(g, frc, chartBackground, xOrgY1, yOrgY, viewR.x + y1AxisThickness + axisWidth,
                    SwingConstants.LEFT, (!y2AxisVisible) && y1Axis.getAttributes().isDrawOpposite(), gridXMin,
                    gridXMax);
        }
        if (y2AxisVisible) {
            y2Axis.paintAxis(g, frc, chartBackground, xOrgY2, yOrgY, viewR.x + y1AxisThickness, SwingConstants.RIGHT,
                    (!y1AxisVisible) && y2Axis.getAttributes().isDrawOpposite(), gridXMin, gridXMax);
        }
        if (xAxis.getAttributes().getOrigin() == IChartViewer.HORIZONTAL_UP) {
            xAxis.setTickAlignment(SwingConstants.BOTTOM);
            xAxis.setLabelAlignment(SwingConstants.TOP);
            xAxis.paintAxis(g, frc, chartBackground, xOrg, yOrg, yOrg + axisHeight, SwingConstants.TOP,
                    xAxis.getAttributes().isDrawOpposite(), gridYMin, gridYMax);
        } else {
            xAxis.setTickAlignment(SwingConstants.TOP);
            xAxis.setLabelAlignment(SwingConstants.BOTTOM);
            xAxis.paintAxis(g, frc, chartBackground, xOrg, yOrg, viewR.y, SwingConstants.BOTTOM,
                    xAxis.getAttributes().isDrawOpposite(), gridYMin, gridYMax);
        }
    }

    protected <V extends AbstractDataView> List<V> extractedHighlightedViews(List<V> views) {
        List<V> highlightedViews = null;
        if (views != null) {
            for (V view : views) {
                if (view != null && view.isHighlighted()) {
                    if (highlightedViews == null) {
                        highlightedViews = new ArrayList<>();
                    }
                    highlightedViews.add(view);
                }
            }
        }
        return highlightedViews;
    }

    protected <V extends AbstractDataView> List<V> extractedClassicViews(List<V> views, List<V> highlightedViews) {
        List<V> classicViews = views;
        if ((views != null) && (highlightedViews != null) && !highlightedViews.isEmpty()) {
            classicViews = new ArrayList<>(views);
            classicViews.removeAll(highlightedViews);
        }
        return classicViews;
    }

    protected <V extends AbstractDataView> void drawData(Graphics2D g, int xOrg, int yOrg, List<V> y1Views,
            List<V> y2Views, List<V> xViews) {
        Rectangle clipRect = g.getClipBounds();
        List<V> classicY1Views, highlightedY1Views;
        highlightedY1Views = extractedHighlightedViews(y1Views);
        classicY1Views = extractedClassicViews(y1Views, highlightedY1Views);
        List<V> classicY2Views, highlightedY2Views;
        highlightedY2Views = extractedHighlightedViews(y2Views);
        classicY2Views = extractedClassicViews(y2Views, highlightedY2Views);
        canWarnForSampling = (!ChartGraphics.paintDataViews(g, margin, classicY1Views, xViews, y1Axis, xAxis, xOrg,
                yOrg, isLimitDrawingZone(), chartBackground, samplingProperties, canWarnForSampling,
                isTransparencyAllowed())) && canWarnForSampling;
        canWarnForSampling = (!ChartGraphics.paintDataViews(g, margin, classicY2Views, xViews, y2Axis, xAxis, xOrg,
                yOrg, isLimitDrawingZone(), chartBackground, samplingProperties, canWarnForSampling,
                isTransparencyAllowed())) && canWarnForSampling;
        if (highlightedY1Views != null && !highlightedY1Views.isEmpty()) {
            canWarnForSampling = (!ChartGraphics.paintDataViews(g, margin, highlightedY1Views, xViews, y1Axis, xAxis,
                    xOrg, yOrg, isLimitDrawingZone(), chartBackground, samplingProperties, canWarnForSampling,
                    isTransparencyAllowed())) && canWarnForSampling;
            highlightedY1Views.clear();
        }
        if (highlightedY2Views != null && !highlightedY2Views.isEmpty()) {
            canWarnForSampling = (!ChartGraphics.paintDataViews(g, margin, highlightedY2Views, xViews, y2Axis, xAxis,
                    xOrg, yOrg, isLimitDrawingZone(), chartBackground, samplingProperties, canWarnForSampling,
                    isTransparencyAllowed())) && canWarnForSampling;
            highlightedY2Views.clear();
        }
        if (isLimitDrawingZone()) {
            if (clipRect == null) {
                g.setClip(null);
            } else {
                g.setClip(clipRect.x, clipRect.y, clipRect.width, clipRect.height);
            }
        }
    }

    public void measureGraphItems() {
        List<DataView>[] views = dataViewList.getDataViewsByAxis(false, IChartViewer.Y1, IChartViewer.Y2,
                IChartViewer.X);
        measureGraphItems(views[0], views[1], views[2]);
    }

    // Compute size of graph items (Axes, labels, title, ....)
    protected void measureGraphItems(List<? extends AbstractDataView> y1Views, List<? extends AbstractDataView> y2Views,
            List<? extends AbstractDataView> xViews) {
        canWarnForSampling = true;
        List<AbstractDataView> yViews = new ArrayList<AbstractDataView>();
        if (y1Views != null) {
            yViews.addAll(y1Views);
        }
        if (y2Views != null) {
            yViews.addAll(y2Views);
        }
        Rectangle2D bounds = null;
        int marginWidth = margin.width;
        int marginHeight = margin.height;
        // Reset sizes ------------------------------------------------------
        titleR.setBounds(0, 0, 0, 0);
        viewR.setBounds(0, 0, 0, 0);
        titleWidth = 0;
        axisWidth = 0;
        axisHeight = 0;
        y1AxisThickness = 0;
        y2AxisThickness = 0;
        if (lastFontRenderContext != null) {
            int halfWidth = marginWidth / 2;
            // Measure title
            // ------------------------------------------------------
            if (titleVisible && (title != null) && (getFont() != null)) {
                int halfHeight = marginHeight / 2;
                bounds = getFont().getStringBounds(title, lastFontRenderContext);
                titleWidth = (int) bounds.getWidth();
                titleR.setBounds(marginWidth, halfHeight, lastWidth - 2 * marginWidth,
                        (marginHeight - halfHeight) + (int) Math.round(Math.abs(bounds.getMaxY() - bounds.getMinY())));
            }
            // Measure view Rectangle
            // --------------------------------------------
            viewR.setBounds(marginWidth, marginHeight + titleR.height, lastWidth - 2 * marginWidth,
                    lastHeight - 2 * marginHeight - titleR.height);

            // Measure Axis
            // ------------------------------------------------------
            xAxisThickness = xAxis.getLabelHeight(lastFontRenderContext);
            if (xAxis.isVisible() && (xAxis.getAttributes().getTitle() != null)
                    && (!xAxis.getAttributes().getTitle().isEmpty()) && (lastFontRenderContext != null)) {
                xAxisThickness *= 2;
            }
            if (xAxis.getAttributes().getOrigin() == IChartViewer.HORIZONTAL_UP) {
                xAxisUpMargin = xAxis.getLabelHeight(lastFontRenderContext);
            } else {
                xAxisUpMargin = 0;
            }
            axisHeight = viewR.height - xAxisThickness;
            xAxis.getScale().computeXScale(yViews, xViews);
            y1Axis.measureAxis(lastFontRenderContext, axisHeight, y1Views);
            y2Axis.measureAxis(lastFontRenderContext, axisHeight, y2Views);
            y1AxisThickness = y1Axis.getThickness();
            if (y1AxisThickness == 0) {
                y1AxisThickness = AbstractAxisView.DEFAULT_MARGIN;
            }
            if ((!y1Axis.isVisible()) || (y1Views == null) || y1Views.isEmpty()) {
                y1AxisThickness = halfWidth;
            }
            y2AxisThickness = y2Axis.getThickness();
            if (y2AxisThickness == 0) {
                y2AxisThickness = AbstractAxisView.DEFAULT_MARGIN;
            }
            if ((!y2Axis.isVisible()) || (y2Views == null) || y2Views.isEmpty()) {
                y2AxisThickness = halfWidth;
            }
            axisWidth = viewR.width - (y1AxisThickness + y2AxisThickness);
            xAxis.measureAxis(lastFontRenderContext, axisWidth, xViews);
        }
    }

    // Paint the zoom mode label
    protected void paintZoomButton(int x, int y) {
        if (isZoomed()) {
            Dimension size = zoomButton.getPreferredSize();
            int w = size.width;
            int h = size.height;
            int newX = x + 7;
            int newY = y + 5;
            zoomButtonX = getAdaptedX(zoomButtonX, x, w);
            zoomButtonY = getAdaptedY(zoomButtonY, y, h);
            if (zoomButtonMoved) {
                newX = zoomButtonX;
                newY = zoomButtonY;
            } else {
                newX = getAdaptedX(newX, x, w);
                newY = getAdaptedY(newY, y, h);
            }
            zoomButton.setBounds(newX, newY, w, h);
            zoomButton.setVisible((w < axisWidth - 7) && (h < axisHeight - 5));
        } else {
            zoomButton.setVisible(false);
        }
    }

    protected int getAdaptedX(int xToCheck, int xRef, int w) {
        if (xToCheck + w >= xRef + axisWidth) {
            xToCheck = xRef + axisWidth - w;
        }
        if (xToCheck < 0) {
            xToCheck = 0;
        }
        return xToCheck;
    }

    protected int getAdaptedY(int yToCheck, int yRef, int h) {
        if (yToCheck + h >= yRef + axisHeight) {
            yToCheck = yRef + axisHeight - h;
        }
        if (yToCheck < 0) {
            yToCheck = 0;
        }
        return yToCheck;
    }

    // Paint the zoom rectangle
    protected void paintZoomSelection(Graphics g) {
        if (zoomDrag) {
            g.setColor(Color.black);
            // Draw rectangle
            Rectangle r = buildRect(zoomX, zoomY, lastX, lastY);
            g.drawRect(r.x, r.y, r.width, r.height);
        }
    }

    // Paint the selection rectangle
    protected void paintAreaSelection(Graphics g) {
        if (useSelectionArea) {
            if (areaSelection) {
                // Draw selection rectangle
                g.setColor(selectionAreaColor);
                Rectangle r = buildRect(zoomX, zoomY, lastX, lastY);
                g.fillRect(r.x, r.y, r.width, r.height);
            } else {
                // recover pixel coordinates from selection axis coordinates
                double[] selectedCoordinates = this.selectedCoordinates;
                if ((selectedCoordinates != null) && (selectedCoordinates.length == 6)) {
                    int xOrg = viewR.x + y1AxisThickness;
                    int yOrg = viewR.y + axisHeight + xAxisUpMargin;
                    double minX = selectedCoordinates[0];
                    double maxX = selectedCoordinates[1];
                    if (Double.isNaN(minX)) {
                        minX = xAxis.getScale().getScaleMinimum();
                    }
                    if (Double.isNaN(maxX)) {
                        maxX = xAxis.getScale().getScaleMaximum();
                    }
                    if (minX > maxX) {
                        double temp = minX;
                        minX = maxX;
                        maxX = temp;
                    }
                    Point pY1Min = null;
                    Point pY1Max = null;
                    Point pY2Min = null;
                    Point pY2Max = null;
                    double minY1 = selectedCoordinates[2];
                    double maxY1 = selectedCoordinates[3];
                    double minY2 = selectedCoordinates[4];
                    double maxY2 = selectedCoordinates[5];
                    if (Double.isNaN(minY1) || Double.isNaN(maxY1)) {
                        if (Double.isNaN(minY2) || Double.isNaN(maxY2)) {
                            if (y1Axis.isVisible()) {
                                minY1 = y1Axis.getScale().getUnScaledValue(y1Axis.getScale().getScaleMinimum());
                                maxY1 = y1Axis.getScale().getUnScaledValue(y1Axis.getScale().getScaleMaximum());
                            } else if (y2Axis.isVisible()) {
                                minY2 = y2Axis.getScale().getUnScaledValue(y2Axis.getScale().getScaleMinimum());
                                maxY2 = y2Axis.getScale().getUnScaledValue(y2Axis.getScale().getScaleMaximum());
                            }
                        }
                    }
                    int[] coordinates = null;
                    if ((!Double.isNaN(minY1)) && (!Double.isNaN(maxY1))) {
                        if (minY1 > maxY1) {
                            double temp = minY1;
                            minY1 = maxY1;
                            maxY1 = temp;
                        }
                        pY1Min = transform(minX, minY1, xOrg, yOrg, y1Axis);
                        pY1Max = transform(maxX, maxY1, xOrg, yOrg, y1Axis);
                        if ((pY1Min != null) && (pY1Max != null)) {
                            coordinates = new int[4];
                            if (pY1Min.x < pY1Max.x) {
                                coordinates[0] = pY1Min.x;
                                coordinates[1] = pY1Max.x;
                            } else {
                                coordinates[0] = pY1Max.x;
                                coordinates[1] = pY1Min.x;
                            }
                            if (pY1Min.y < pY1Max.y) {
                                coordinates[2] = pY1Min.y;
                                coordinates[3] = pY1Max.y;
                            } else {
                                coordinates[2] = pY1Max.y;
                                coordinates[3] = pY1Min.y;
                            }
                        }
                    }
                    if ((!Double.isNaN(minY2)) && (!Double.isNaN(maxY2))) {
                        if (minY2 > maxY2) {
                            double temp = minY2;
                            minY2 = maxY2;
                            maxY2 = temp;
                        }
                        pY2Min = transform(minX, minY2, xOrg, yOrg, y2Axis);
                        pY2Max = transform(maxX, maxY2, xOrg, yOrg, y2Axis);
                        int[] tempCoordinates = new int[4];
                        if (pY2Min.x < pY2Max.x) {
                            tempCoordinates[0] = pY2Min.x;
                            tempCoordinates[1] = pY2Max.x;
                        } else {
                            tempCoordinates[0] = pY2Max.x;
                            tempCoordinates[1] = pY2Min.x;
                        }
                        if (pY2Min.y < pY2Max.y) {
                            tempCoordinates[2] = pY2Min.y;
                            tempCoordinates[3] = pY2Max.y;
                        } else {
                            tempCoordinates[2] = pY2Max.y;
                            tempCoordinates[3] = pY2Min.y;
                        }
                        if (coordinates == null) {
                            coordinates = tempCoordinates;
                        } else {
                            if (tempCoordinates[2] < coordinates[2]) {
                                coordinates[2] = tempCoordinates[2];
                            }
                            if (tempCoordinates[3] > coordinates[3]) {
                                coordinates[3] = tempCoordinates[3];
                            }
                        }
                    }
                    if (coordinates != null) {
                        // Draw selection rectangle
                        g.setColor(selectionAreaColor);
                        Rectangle r = buildRect(coordinates[0], coordinates[2], coordinates[1], coordinates[3]);
                        g.fillRect(r.x, r.y, r.width, r.height);
                    }
                }
            }
        }
    }

    // Build a valid rectangle with the given coordinates
    protected Rectangle buildRect(int x1, int y1, int x2, int y2) {
        Rectangle r = new Rectangle();
        if (x1 < x2) {
            if (y1 < y2) {
                r.setRect(x1, y1, x2 - x1, y2 - y1);
            } else {
                r.setRect(x1, y2, x2 - x1, y1 - y2);
            }
        } else {
            if (y1 < y2) {
                r.setRect(x2, y1, x1 - x2, y2 - y1);
            } else {
                r.setRect(x2, y2, x1 - x2, y1 - y2);
            }
        }
        return r;
    }

    // a zoom selection must be contained in the graph zone and big enough
    protected boolean zoomSelectionOK(Rectangle r) {
        boolean zoomOk;
        // check the position of the selection
        int limitUP = viewR.y + xAxisUpMargin;
        int limitDOWN = y1Axis.getLength() + limitUP;
        int limitLEFT = y1AxisThickness + viewR.x;
        int limitRIGHT = xAxis.getLength() + limitLEFT;
        // if the whole selection is not contained in the graph zone OK = false
        if ((r.x < limitLEFT) && (r.x + r.width < limitLEFT)) {
            // selection in the left margin
            zoomOk = false;
        } else if ((r.x > limitRIGHT)) {
            // selection in the right margin
            zoomOk = false;
        } else if ((r.y < limitUP) && (r.y + r.height < limitUP)) {
            // selection in the upper margin
            zoomOk = false;
        } else if (r.y > limitDOWN) {
            // selection in the lower margin
            zoomOk = false;
        } else {
            // if it's just a part, we adjust
            if (r.x < limitLEFT) {
                int newWidth = r.width - (limitLEFT - r.x);
                r.setBounds(limitLEFT, r.y, newWidth, r.height);
            }
            if (r.y < limitUP) {
                int newHeight = r.height - (limitUP - r.y);
                r.setBounds(r.x, limitUP, r.width, newHeight);
            }
            if (r.x + r.width > limitRIGHT) {
                r.setBounds(r.x, r.y, limitRIGHT - r.x, r.height);
            }
            if (r.y + r.height > limitDOWN) {
                r.setBounds(r.x, r.y, r.width, limitDOWN - r.y);
            }
            // test if the selection is not too small
            if (r.width < 10 || r.height < 10) {
                zoomOk = false;
            } else {
                // everything is OK or adjusted
                zoomOk = true;
            }
        }
        return zoomOk;
    }

    protected Rectangle getVisibleArea() {
        int xOrg = viewR.x + y1AxisThickness;
        int yOrg = viewR.y + xAxisUpMargin;
        return new Rectangle(xOrg, yOrg, xAxis.getLength(), y1Axis.getLength());
    }

    // we do the zoom
    protected boolean applyZoom(int x, int y) {
        boolean canZoom = false;
        // we set the zoom rectangle (size in px)
        Rectangle zoomRect = buildRect(zoomX, zoomY, x, y);
        // if the zoom is not too small and well-positioned we apply it
        if (zoomSelectionOK(zoomRect)) {
            Rectangle visibleRect = getVisibleArea();
            boolean xZoom = xAxis.getScale().zoom(zoomRect.x, zoomRect.x + zoomRect.width, visibleRect);
            boolean y1Zoom = y1Axis.getScale().zoom(zoomRect.y, zoomRect.y + zoomRect.height, visibleRect);
            boolean y2Zoom = y2Axis.getScale().zoom(zoomRect.y, zoomRect.y + zoomRect.height, visibleRect);
            canZoom = xZoom || y1Zoom || y2Zoom;
            if (canZoom) {
                // we set that the graph is zoomed
                zoomed = true;
                // A zoom is applied: forget last search
                lastSearch = null;
                cleanInfoPopup();
                ipanelVisible = false;
            }
        }
        return canZoom;
    }

    // we do the translation
    protected void applyTranslation(int x, int y) {
        boolean canTranslate;
        Rectangle visibleRect = getVisibleArea();
        boolean xTranslate = xAxis.getScale().translate(x, visibleRect);
        boolean y1Translate = y1Axis.getScale().translate(y, visibleRect);
        boolean y2Translate = y2Axis.getScale().translate(y, visibleRect);
        canTranslate = xTranslate || y1Translate || y2Translate;
        if (canTranslate) {
            // we set that the graph is zoomed
            zoomed = true;
            // A translation is applied: forget last search
            lastSearch = null;
            cleanInfoPopup();
            ipanelVisible = false;
            measureGraphItems();
        }
    }

    /**
     * Exit zoom mode.
     */
    public void exitZoom() {
        // exitZoom only when zoomed to avoid an OutOfMemoryError
        if (isZoomed()) {
            xAxis.getScale().unzoom();
            y1Axis.getScale().unzoom();
            y2Axis.getScale().unzoom();
            zoomButton.setVisible(false);
            zoomed = false;
            setCursor(Cursor.getDefaultCursor());
        }
    }

    /**
     * Expert usage. Transforms given coordinates (real space) into pixel coordinates
     * 
     * @param x The x coordinates (Real space)
     * @param y The y coordinates (Real space)
     * @param yAxis The axis corresponding to y coordinates.
     * @return Point(-100,-100) when cannot transform
     */
    protected Point transform(double x, double y, int xOrg, int yOrg, AbstractAxisView yAxis) {
        Point transformation;
        // The graph must have been measured before we can transform
        if (viewR.isEmpty()) {
            transformation = new Point(-100, -100);
        } else {
            double xRatio = ChartUtils.getRatio(x, xOrg, axisWidth, margin.width, xAxis, yAxis);
            double yRatio = ChartUtils.getRatio(y, yOrg, axisHeight, margin.height, yAxis, xAxis);
            if (Double.isNaN(xRatio) || Double.isNaN(yRatio)) {
                xRatio = -100;
                yRatio = -100;
            }
            transformation = new Point((int) xRatio, (int) yRatio);
        }
        return transformation;
    }

    /**
     * Expert usage. Transforms given coordinates (real space) into pixel coordinates
     * 
     * @param xy The x and y coordinates (Real space)
     * @param yAxis The axis corresponding to y coordinates.
     * @return replaces xy by expected coordinates
     */
    protected void transform(double[] xy, int xOrg, int yOrg, AbstractAxisView yAxis) {
        double xRatio, yRatio;
        // The graph must have been measured before we can transform
        if (viewR.isEmpty()) {
            xRatio = Double.NaN;
            yRatio = Double.NaN;
        } else {
            xRatio = ChartUtils.getRatio(xy[0], xOrg, axisWidth, margin.width, xAxis, yAxis);
            yRatio = ChartUtils.getRatio(xy[1], yOrg, axisHeight, margin.height, yAxis, xAxis);
        }
        xy[0] = xRatio;
        xy[1] = yRatio;
    }

    /**
     * Search the nearest point in the dataViews.
     * 
     * @param x The x coordinates (Real space)
     * @param y The y coordinates (Real space)
     * @param xAxis The axis corresponding to x coordinates.
     * @return A structure containing search result.
     */
    public SearchInfo searchNearest(int x, int y, int axisType) {
        int xOrg = viewR.x + y1AxisThickness;
        int yOrg = viewR.y + axisHeight + xAxisUpMargin;
        SearchInfo si;
        // Search only in graph area
        if ((x <= viewR.x - 10) || (x >= viewR.x + viewR.width + 10) || (y <= viewR.y - 10)
                || (y >= viewR.y + viewR.height + 10)) {
            si = new SearchInfo();
        } else {
            AbstractAxisView yAxis = getAxis(axisType);
            List<? extends AbstractDataView>[] views = dataViewList.getDataViewsByAxis(false, axisType, IChartViewer.X);
            List<? extends AbstractDataView> yViews = views[0];
            List<? extends AbstractDataView> xViews = views[1];
            int idx = 0;
            int norme2;
            DataList minP = null;
            DataList minXP = null;
            Point minPt = null;
            int minNorme = Integer.MAX_VALUE;
            AbstractDataView minDataView = null;
            int minPl = 0;
            int minIdx = -1;
            boolean xy;
            AbstractDataView w;
            if ((xViews == null) || xViews.isEmpty()) {
                xy = false;
                w = null;
            } else {
                xy = true;
                w = xViews.get(0);
            }
            Rectangle boundRect2 = new Rectangle();
            boundRect2.setBounds(viewR.x - 2, viewR.y - 2, viewR.width + 4, viewR.height + 4);
            for (AbstractDataView v : yViews) {
                if (v.isClickable()) {
                    IDataListContainer e = xy ? new DataXY(v.getData(), w.getData())
                            : new DataListContainer(v.getData());
                    if (e.isValid()) {
                        e.init();
                    }
                    idx = 0;
                    while (e.isValid()) {
                        Point p;
                        if (xy) {
                            p = transform(w.getTransformedValue(((DataXY) e).getDataX().getY()),
                                    v.getTransformedValue(e.getDataY().getY()), xOrg, yOrg, yAxis);
                        } else {
                            p = transform(v.getOffsettedXValue(e.getDataY().getX()),
                                    v.getTransformedValue(e.getDataY().getY()), xOrg, yOrg, yAxis);
                        }
                        if (boundRect2.contains(p)) {
                            norme2 = distance2(x, y, p.x, p.y);
                            if (norme2 < minNorme) {

                                minNorme = norme2;
                                minP = e.getDataY();
                                if (xy) {
                                    minXP = ((DataXY) e).getDataX();
                                } else {
                                    minIdx = idx;
                                }
                                minDataView = v;
                                minPt = p;

                                // Compute placement for the value info window
                                if (p.x < (viewR.x + viewR.width / 2)) {
                                    if (p.y < (viewR.y + viewR.height / 2)) {
                                        minPl = SearchInfo.BOTTOMRIGHT;
                                    } else {
                                        minPl = SearchInfo.TOPRIGHT;
                                    }
                                } else {
                                    if (p.y < (viewR.y + viewR.height / 2)) {
                                        minPl = SearchInfo.BOTTOMLEFT;
                                    } else {
                                        minPl = SearchInfo.TOPLEFT;
                                    }
                                }
                            }
                        }
                        e.next();
                        idx++;
                    }
                }
            }
            yViews.clear();
            if (minNorme == Integer.MAX_VALUE) {
                si = new SearchInfo(); // No item found
            } else if (xy) {
                si = new SearchInfo(minPt.x, minPt.y, minDataView, axisType, minP, minNorme, minPl, -1);
                si.setXValue(minXP, w);
            } else {
                si = new SearchInfo(minPt.x, minPt.y, minDataView, axisType, minP, minNorme, minPl, minIdx);
            }
        }
        return si;
    }

    /**
     * Returns the {@link Collection} of {@link DataView}s that are visible in current display area (i.e. that are
     * effectively traced at current zoom)
     * 
     * @param onlyClickable Whether you only want clickable {@link DataView}s
     * @return A {@link Collection}, never <code>null</code>.
     */
    public Collection<DataView> getVisibleDataViews(boolean onlyClickable) {
        Collection<DataView> visibleViews = new ArrayList<DataView>();
        int xOrg = viewR.x + y1AxisThickness;
        int yOrg = viewR.y + axisHeight + xAxisUpMargin;
        List<DataView>[] views = dataViewList.getDataViewsByAxis(false, IChartViewer.Y1, IChartViewer.Y2,
                IChartViewer.X);
        List<DataView> y1Views = views[0];
        List<DataView> y2Views = views[1];
        List<DataView> xViews = views[2];
        boolean xy;
        DataView w;
        if ((xViews == null) || xViews.isEmpty()) {
            xy = false;
            w = null;
        } else {
            xy = true;
            w = xViews.get(0);
        }
        fillViewsForVisibility(y1Views, xOrg, yOrg, viewR, xy, w, visibleViews, true, onlyClickable);
        fillViewsForVisibility(y2Views, xOrg, yOrg, viewR, xy, w, visibleViews, true, onlyClickable);
        return visibleViews;
    }

    /**
     * Returns the {@link Collection} of {@link DataView}s that are not visible in current display area (i.e. that are
     * not traced at current zoom)
     * 
     * @param onlyClickable Whether you only want clickable {@link DataView}s
     * @return A {@link Collection}, never <code>null</code>.
     */
    public Collection<DataView> getInVisibleDataViews(boolean onlyClickable) {
        Collection<DataView> hiddenViews = new ArrayList<DataView>();
        int xOrg = viewR.x + y1AxisThickness;
        int yOrg = viewR.y + axisHeight + xAxisUpMargin;
        List<DataView>[] views = dataViewList.getDataViewsByAxis(false, IChartViewer.Y1, IChartViewer.Y2, -1,
                IChartViewer.X);
        List<DataView> y1Views = views[0];
        List<DataView> y2Views = views[1];
        List<DataView> noAxisViews = views[2];
        List<DataView> xViews = views[3];
        boolean xy;
        DataView w;
        if ((xViews == null) || xViews.isEmpty()) {
            xy = false;
            w = null;
        } else {
            xy = true;
            w = xViews.get(0);
        }
        fillViewsForVisibility(y1Views, xOrg, yOrg, viewR, xy, w, hiddenViews, false, onlyClickable);
        fillViewsForVisibility(y2Views, xOrg, yOrg, viewR, xy, w, hiddenViews, false, onlyClickable);
        for (DataView v : noAxisViews) {
            if ((!onlyClickable) || v.isClickable()) {
                hiddenViews.add(v);
            }
        }
        return hiddenViews;
    }

    /**
     * Extracts visible or invisible {@link DataView}s from a {@link Collection} of {@link DataView}s and puts them in
     * another one
     * 
     * @param yViews The {@link Collection} of {@link DataView}s from which to extract visible or invisible
     *            {@link DataView}s
     * @param xOrg x axis origin
     * @param yOrg y axis orgin
     * @param boundRect The display area surrounding rectangle
     * @param xy Whether y axis is in XY mode
     * @param w {@link DataView} potentially put on X axis
     * @param visibleOrInvisibleViews The {@link Collection} in which to put extracted {@link DataView}s
     * @param fillVisible Whether to extract visible {@link DataView}s.
     * @param onlyClickable Whether to extract only clickable {@link DataView}s
     */
    protected void fillViewsForVisibility(Collection<DataView> yViews, int xOrg, int yOrg, Rectangle boundRect,
            boolean xy, DataView w, Collection<DataView> visibleOrInvisibleViews, boolean fillVisible,
            boolean onlyClickable) {
        double[] previous;
        double[] coord = new double[2];
        for (DataView v : yViews) {
            previous = null;
            AbstractAxisView yAxis = getAxis(v.getAxis());
            if ((!onlyClickable) || v.isClickable()) {
                boolean visible = false;
                IDataListContainer e = xy ? new DataXY(v.getData(), w.getData()) : new DataListContainer(v.getData());
                if (e.isValid()) {
                    e.init();
                }
                while (e.isValid()) {
                    if (xy) {
                        coord[0] = w.getTransformedValue(((DataXY) e).getDataX().getY());
                        coord[1] = v.getTransformedValue(e.getDataY().getY());
                    } else {
                        coord[0] = e.getDataY().getX();
                        coord[1] = v.getTransformedValue(e.getDataY().getY());
                    }
                    transform(coord, xOrg, yOrg, yAxis);

                    if ((!Double.isNaN(coord[0])) && (!Double.isNaN(coord[1]))) {
                        if (boundRect.contains(coord[0], coord[1])) {
                            visible = true;
                            break;
                        } else if ((previous != null) && (!Double.isNaN(previous[0])) && (!Double.isNaN(previous[1]))
                                && boundRect.intersectsLine(previous[0], previous[1], coord[0], coord[1])) {
                            visible = true;
                            break;
                        }
                    }
                    previous = coord.clone();
                    e.next();
                }
                if (fillVisible) {
                    if (visible) {
                        visibleOrInvisibleViews.add(v);
                    }
                } else if (!visible) {
                    visibleOrInvisibleViews.add(v);
                }
            } // end if ((!onlyClickable) || v.isClickable())
        }
    }

    // Return the square distance
    protected int distance2(int x1, int y1, int x2, int y2) {
        return (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
    }

    public boolean isPaintAxisFirst() {
        return paintAxisFirst;
    }

    public void setPaintAxisFirst(boolean paintAxisFirst) {
        this.paintAxisFirst = paintAxisFirst;
        repaint();
    }

    public int getXAxisThickness() {
        return xAxisThickness;
    }

    public int getY1AxisThickness() {
        return y1AxisThickness;
    }

    public int getY2AxisThickness() {
        return y2AxisThickness;
    }

    /**
     * Determines whether the graph is zoomed.
     * 
     * @return true if the , false otherwise
     */
    public boolean isZoomed() {
        // isZoomed is in the case of an area Zoom
        return zoomed || xAxis.getScale().isZoomed() || y1Axis.getScale().isZoomed() || y2Axis.getScale().isZoomed();
    }

    /**
     * @return the limitDrawingZone
     */
    public boolean isLimitDrawingZone() {
        return limitDrawingZone;
    }

    /**
     * @param limitDrawingZone
     *            the limitDrawingZone to set
     */
    public void setLimitDrawingZone(boolean limitDrawingZone) {
        this.limitDrawingZone = limitDrawingZone;
    }

    /**
     * Returns whether [Ctrl] key is used to initialize zoom on {@link #mousePressed(MouseEvent)}. <code>true</code> by
     * default.
     * 
     * @return A <code>boolean</code>
     * @see #setUseControlKeyToZoom(boolean)
     */
    public boolean isUseControlKeyToZoom() {
        return useControlKeyToZoom;
    }

    /**
     * Sets whether [Ctrl] key shall be used to initialize zoom on {@link #mousePressed(MouseEvent)}.
     * 
     * @param useControlKeyToZoom Whether [Ctrl] key shall be used to initialize zoom on
     *            {@link #mousePressed(MouseEvent)}.
     *            <ul>
     *            <li>If <code>true</code>, then [Ctrl] key must be pressed on {@link #mousePressed(MouseEvent)} to
     *            create zoom rectangle</li>
     *            <li>If <code>false</code>, then [Ctrl] key must be pressed on {@link #mousePressed(MouseEvent)} to
     *            display information panel</li>
     *            </ul>
     */
    public void setUseControlKeyToZoom(boolean useControlKeyToZoom) {
        this.useControlKeyToZoom = useControlKeyToZoom;
    }

    /**
     * Returns whether a {@link ChartViewerEvent} with {@link Reason#SELECTION} should be fired at any click in the view
     * 
     * @return A <code>boolean</code> velue. <code>true</code> by default.
     */
    public boolean isFireSelectionEventOnAnyClick() {
        return fireSelectionEventOnAnyClick;
    }

    /**
     * Sets whether a {@link ChartViewerEvent} with {@link Reason#SELECTION} should be fired at any click in the view
     * 
     * @param fireSelectionEventOnAnyClick Whether a {@link ChartViewerEvent} with {@link Reason#SELECTION} should be
     *            fired at any click in the view
     */
    public void setFireSelectionEventOnAnyClick(boolean fireSelectionEventOnAnyClick) {
        this.fireSelectionEventOnAnyClick = fireSelectionEventOnAnyClick;
    }

    /**
     * Gets the display duration.
     * 
     * @return Display duration
     * @see #setDisplayDuration
     */
    public double getDisplayDuration() {
        return displayDuration;
    }

    /**
     * Sets the display duration.This will garbage old data in all displayed data views. Garbaging occurs when addData
     * is called.
     * 
     * @param v Display duration (milliseconds). Pass <code>Double.POSITIVE_INFINITY</code> to disable.
     * @return A <code>boolean</code>: whether display duration did change
     */
    public boolean setDisplayDuration(double v) {
        return setDisplayDuration(v, true);
    }

    protected boolean setDisplayDuration(double v, boolean fireChartEvent) {
        boolean changed = fireChartEvent;
        double duration = v;
        if (duration <= 0) {
            duration = maxDisplayDuration;
        }
        if (duration <= maxDisplayDuration) {
            // accept displayDuration
            displayDuration = duration;
            xAxis.getAttributes().setAxisDuration(duration);
        } else {
            // refuse displayDuration
            changed = false;
            StringBuilder messageBuilder = new StringBuilder("Duration refused : can not be greater than ");
            if (xAxis.getScale().isTimeScale()) {
                // in case of time, convert maxDisplayDuration to readable time
                double days, hours, minutes, seconds, milliseconds;
                int dayTime = 1000 * 60 * 60 * 24;
                int hourTime = 1000 * 60 * 60;
                int minuteTime = 1000 * 60;
                int secondTime = 1000;
                double totalTime = maxDisplayDuration;

                days = totalTime / dayTime;
                totalTime -= days * dayTime;

                hours = totalTime / hourTime;
                totalTime -= hours * hourTime;

                minutes = totalTime / minuteTime;
                totalTime -= minutes * minuteTime;

                seconds = totalTime / secondTime;

                totalTime -= seconds * secondTime;
                milliseconds = totalTime;

                if (days > 0) {
                    String dayString = Double.toString(days);
                    if (dayString.endsWith(".0")) {
                        dayString = dayString.substring(0, dayString.indexOf(ChartUtils.DOT));
                    }
                    messageBuilder.append(dayString).append("day(s) ");
                    dayString = null;
                }

                if (hours > 0) {
                    String hourString = Double.toString(hours);
                    if (hourString.endsWith(".0")) {
                        hourString = hourString.substring(0, hourString.indexOf(ChartUtils.DOT));
                    }
                    messageBuilder.append(hours).append("hr ");
                    hourString = null;
                }

                if (minutes > 0) {
                    String minuteString = Double.toString(minutes);
                    if (minuteString.endsWith(".0")) {
                        minuteString = minuteString.substring(0, minuteString.indexOf(ChartUtils.DOT));
                    }
                    messageBuilder.append(minuteString).append("mn ");
                    minuteString = null;
                }

                if (seconds > 0) {
                    String secondString = Double.toString(seconds);
                    if (secondString.endsWith(".0")) {
                        secondString = secondString.substring(0, secondString.indexOf(ChartUtils.DOT));
                    }
                    messageBuilder.append(secondString).append("s ");
                    secondString = null;
                }

                if (milliseconds > 0) {
                    String millisecondString = Double.toString(milliseconds);
                    if (millisecondString.endsWith(".0")) {
                        millisecondString = millisecondString.substring(0, millisecondString.indexOf(ChartUtils.DOT));
                    }
                    messageBuilder.append(millisecondString).append("ms ");
                    millisecondString = null;
                } else if (days == 0 && hours == 0 && minutes == 0 && seconds == 0 && milliseconds == 0) {
                    String millisecondString = Double.toString(milliseconds);
                    if (millisecondString.endsWith(".0")) {
                        millisecondString = millisecondString.substring(0, millisecondString.indexOf(ChartUtils.DOT));
                    }
                    messageBuilder.append(millisecondString).append("ms ");
                    millisecondString = null;
                }
            } else {
                // otherwise, maxDisplayDuration does not have to be converted
                // to readable time
                String maxString = Double.toString(maxDisplayDuration);
                if (maxString.endsWith(".0")) {
                    maxString = maxString.substring(0, maxString.indexOf(ChartUtils.DOT));
                }
                messageBuilder.append(maxString);
                maxString = null;
            }
            JOptionPane.showMessageDialog(this, messageBuilder.toString(), "Warning !", JOptionPane.WARNING_MESSAGE);
        }
        return changed;
    }

    /**
     * Gets the maximum allowed for a display duration
     * 
     * @return Maximum allowed for a display duration
     * @see #setMaxDisplayDuration
     * @see #setDisplayDuration
     */
    public double getMaxDisplayDuration() {
        return maxDisplayDuration;
    }

    /**
     * Sets the maximum allowed for a display duration
     * 
     * @param maxDisplayDuration
     *            The maximum allowed for a display duration
     * @see #getMaxDisplayDuration
     * @see #getDisplayDuration
     * @see #setDisplayDuration
     */
    public void setMaxDisplayDuration(double maxDisplayDuration) {
        this.maxDisplayDuration = maxDisplayDuration;
    }

    /**
     * Returns the chart title
     * 
     * @return A {@link String}
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the chart title
     * 
     * @param title The title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns the chart background
     * 
     * @return A {@link Color}
     */
    public Color getChartBackground() {
        return chartBackground;
    }

    /**
     * Sets the chart background
     * 
     * @param chartBackground the background to set
     */
    public void setChartBackground(Color chartBackground) {
        this.chartBackground = (chartBackground == null ? Color.WHITE : chartBackground);
    }

    /**
     * Returns whether title can be displayed
     * 
     * @return A <code>boolean</code>
     */
    public boolean isTitleVisible() {
        return titleVisible;
    }

    /**
     * Sets whether title can be displayed
     * 
     * @param titleVisible
     *            whether title can be displayed
     */
    public void setTitleVisible(boolean titleVisible) {
        this.titleVisible = titleVisible;
    }

    public Color getSelectionAreaColor() {
        return selectionAreaColor;
    }

    public void setSelectionAreaColor(Color selectionAreaColor) {
        this.selectionAreaColor = (selectionAreaColor == null ? DEFAULT_AREA_SELECTION_COLOR : selectionAreaColor);
    }

    public boolean isUseSelectionArea() {
        return useSelectionArea;
    }

    public void setUseSelectionArea(boolean allowAreaSelection) {
        this.useSelectionArea = allowAreaSelection;
    }

    public double[] getSelectedCoordinates() {
        double[] coordinates = selectedCoordinates;
        if (coordinates != null) {
            coordinates = coordinates.clone();
        }
        return coordinates;
    }

    public void selectCoordinates(double minX, double maxX, double minY1, double maxY1, double minY2, double maxY2) {
        selectedCoordinates = new double[] { minX, maxX, minY1, maxY1, minY2, maxY2 };
        if (useSelectionArea) {
            repaint();
        }
    }

    public void clearSelectedCoordinates() {
        selectedCoordinates = null;
        if (useSelectionArea) {
            repaint();
        }
    }

    /**
     * Returns the {@link AbstractAxisView} that corresponds to a given identifier
     * 
     * @param axis The identifier
     * @return An {@link AbstractAxisView}. <code>null</code> if there was no matching one.
     */
    public AbstractAxisView getAxis(int axis) {
        AbstractAxisView axisView;
        switch (axis) {
            case IChartViewer.X:
                axisView = xAxis;
                break;
            case IChartViewer.Y1:
                axisView = y1Axis;
                break;
            case IChartViewer.Y2:
                axisView = y2Axis;
                break;
            default:
                axisView = null;
                break;
        }
        return axisView;
    }

    /**
     * Extracts some {@link ChartProperties} from this {@link ChartView}
     * 
     * @return Some {@link ChartProperties}
     */
    public ChartProperties getChartProperties() {
        return new ChartProperties(true, IChartViewer.LABEL_ROW, ColorTool.getCometeColor(chartBackground),
                FontTool.getCometeFont(getFont()), FontTool.getCometeFont(xAxis.getAttributes().getLabelFont()),
                getTitle(), getDisplayDuration(), 0, ChartProperties.DEFAULT_NO_VALUE_STRING, true, true,
                isTransparencyAllowed(), getAxisProperties(IChartViewer.Y1), getAxisProperties(IChartViewer.Y2),
                getAxisProperties(IChartViewer.X), getSamplingProperties(), getDragProperties());
    }

    /**
     * Applies some {@link ChartProperties} to this {@link ChartView}
     * 
     * @param properties The {@link ChartProperties} to apply
     */
    public void setChartProperties(ChartProperties properties) {
        if (properties != null) {
            setChartBackground(ColorTool.getColor(properties.getBackgroundColor()));
            setFont(FontTool.getFont(properties.getHeaderFont()));
            setTitle(properties.getTitle());
            setDisplayDuration(properties.getDisplayDuration());
            setAxisProperties(IChartViewer.Y1, properties.getY1AxisProperties(), false);
            setAxisProperties(IChartViewer.Y2, properties.getY2AxisProperties(), false);
            setAxisProperties(IChartViewer.X, properties.getXAxisProperties(), false);
            setSamplingProperties(properties.getSamplingProperties(), false);
            setDragProperties(properties.getDragProperties());
            setTransparencyAllowed(properties.isTransparencyAllowed(), false);
            measureGraphItems();
            repaint();
        }
    }

    public boolean isSamplingAllowed() {
        return samplingProperties.isEnabled();
    }

    public SamplingProperties getSamplingProperties() {
        return samplingProperties.clone();
    }

    public void setSamplingProperties(SamplingProperties samplingProperties) {
        setSamplingProperties(samplingProperties, true);
    }

    protected void setSamplingProperties(SamplingProperties samplingProperties, boolean repaint) {
        this.samplingProperties = (samplingProperties == null ? new SamplingProperties() : samplingProperties.clone());
        if (repaint) {
            repaint();
        }
    }

    public int getDataViewOffsetDragType() {
        return dragProperties.getDataViewOffsetDragType();
    }

    public void setDataViewOffsetDragType(int dataViewOffsetDragType) {
        dragProperties.setDataViewOffsetDragType(dataViewOffsetDragType);
    }

    public int getScaleDragType() {
        return dragProperties.getScaleDragType();
    }

    public void setScaleDragType(int scaleDragType) {
        dragProperties.setScaleDragType(scaleDragType);
    }

    public int getScaleDragSensitivity() {
        return dragProperties.getScaleDragSensitivity();
    }

    public void setScaleDragSensitivity(int scaleDragSensitivity) {
        dragProperties.setScaleDragSensitivity(scaleDragSensitivity);
    }

    public DragProperties getDragProperties() {
        return dragProperties.clone();
    }

    public void setDragProperties(DragProperties dragProperties) {
        this.dragProperties = (dragProperties == null ? new DragProperties() : dragProperties.clone());
    }

    public boolean isTransparencyAllowed() {
        return transparencyAllowed;
    }

    public void setTransparencyAllowed(boolean transparencyAllowed) {
        setTransparencyAllowed(transparencyAllowed, true);
    }

    protected void setTransparencyAllowed(boolean transparencyAllowed, boolean repaint) {
        if (transparencyAllowed != this.transparencyAllowed) {
            this.transparencyAllowed = transparencyAllowed;
            if (repaint) {
                repaint();
            }
        }
    }

    /**
     * Extracts some {@link AxisProperties} from this {@link ChartView}
     * 
     * @param axis The axis identifier
     * @return Some {@link AxisProperties}
     */
    public AxisProperties getAxisProperties(int axis) {
        AxisProperties properties;
        AbstractAxisView view = getAxis(axis);
        if (view == null) {
            properties = null;
        } else {
            AxisAttributes attributes = view.getAttributes();
            properties = attributes.getAxisProperties();
            properties.setVisible(view.isVisible());
        }
        return properties;
    }

    /**
     * Applies some {@link AxisProperties} from this {@link ChartView}
     * 
     * @param axis The axis identifier
     * @param properties The {@link AxisProperties} to apply
     */
    public void setAxisProperties(int axis, AxisProperties properties) {
        setAxisProperties(axis, properties, true);
    }

    /**
     * Applies some {@link AxisProperties} from this {@link ChartView}
     * 
     * @param axis The axis identifier
     * @param properties The {@link AxisProperties} to apply
     * @param repaint Whether to repaint once properties applied
     */
    protected void setAxisProperties(int axis, AxisProperties properties, boolean repaint) {
        if (properties != null) {
            AbstractAxisView view = getAxis(axis);
            if (view != null) {
                exitZoom();
                AxisAttributes attributes = view.getAttributes();
                attributes.setAxisProperties(properties, chartBackground);
                switch (properties.getPosition()) {
                    case IChartViewer.HORIZONTAL_UP:
                        view.setTitlePosition(SwingConstants.TOP);
                        break;
                    case IChartViewer.HORIZONTAL_DOWN:
                        view.setTitlePosition(SwingConstants.BOTTOM);
                        break;
                    case IChartViewer.VERTICAL_LEFT:
                        view.setTitlePosition(SwingConstants.LEFT);
                        break;
                    case IChartViewer.VERTICAL_RIGHT:
                        view.setTitlePosition(SwingConstants.RIGHT);
                        break;
                    default:
                        if (view == xAxis) {
                            view.setTitlePosition(SwingConstants.BOTTOM);
                        } else if (view == y1Axis) {
                            view.setTitlePosition(SwingConstants.LEFT);
                        } else if (view == y2Axis) {
                            view.setTitlePosition(SwingConstants.RIGHT);
                        }
                        break;
                }
                Color color = ColorTool.getColor(properties.getColor());
                if (color == null) {
                    color = Color.BLACK;
                }
                view.setAxisColor(color);
                view.setVisible(properties.isVisible());
                updateScale(view);
                if (repaint) {
                    measureGraphItems();
                    repaint();
                }
            }
        }
    }

    /**
     * Updates an {@link AbstractAxisView} scale, depending on its {@link AxisAttributes}
     * 
     * @param view The {@link AbstractAxisView}
     */
    public void updateScale(AbstractAxisView view) {
        if (view != null) {
            AbstractAxisScale scale = (view.getAttributes().getScale() == IChartViewer.LOG_SCALE
                    ? new LogarithmicScale(view.getAttributes())
                    : new LinearScale(view.getAttributes()));
            view.setScale(scale);
        }
    }

    /**
     * Builds an axis configuration string that can be written into a file and
     * is compatible with CfFileReader.
     * 
     * @param prefix Axis settings prefix
     * @return A string containing param
     */
    public String getAxisConfiguration(String prefix, int axis) {
        return appendAxisConfiguration(null, prefix, axis).toString();
    }

    /**
     * Appends to a {@link StringBuilder} an axis configuration string that can be written into a file and is compatible
     * with CfFileReader.
     * 
     * @param configurationBuilder The {@link StringBuilder}. If <code>null</code>, a new one will be created
     * @param prefix Axis settings prefix.
     * @param axis The axis identifier.
     * @return A {@link StringBuilder} containing the configuration.
     */
    public StringBuilder appendAxisConfiguration(StringBuilder configurationBuilder, String prefix, int axis) {
        StringBuilder builder = configurationBuilder;
        if (builder == null) {
            builder = new StringBuilder();
        }
        AbstractAxisView view = getAxis(axis);
        if (view != null) {
            builder.append(prefix).append(VISIBLE).append(':').append(view.isVisible()).append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(GRID).append(':').append(view.getAttributes().isGridVisible())
                    .append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(SUBGRID).append(':').append(view.getAttributes().isSubGridVisible())
                    .append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(GRID_STYLE).append(':').append(view.getAttributes().getGridStyle())
                    .append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(MIN).append(':').append(view.getAttributes().getMinimum())
                    .append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(MAX).append(':').append(view.getAttributes().getMaximum())
                    .append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(AUTOSCALE).append(':').append(view.getAttributes().isAutoScale())
                    .append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(SCALE).append(':').append(view.getAttributes().getScale())
                    .append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(FORMAT).append(':').append(view.getAttributes().getLabelFormat())
                    .append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(TITLE).append(':').append('\'').append(view.getAttributes().getTitle())
                    .append('\'').append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(COLOR).append(':').append(OFormat.color(view.getAxisColor()))
                    .append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(LABEL_FONT).append(':')
                    .append(OFormat.font(view.getAttributes().getLabelFont())).append(ObjectUtils.NEW_LINE);
            builder.append(prefix).append(FIT_DISPLAY_DURATION).append(':')
                    .append(view.getAttributes().isFitXAxisToDisplayDuration()).append(ObjectUtils.NEW_LINE);
        }
        return builder;
    }

    /**
     * Appends to a {@link StringBuilder} a sampling configuration string that can be written into a file and is
     * compatible with CfFileReader.
     * 
     * @param configurationBuilder The {@link StringBuilder}. If <code>null</code>, a new one will be created.
     * @return A {@link StringBuilder} containing the configuration.
     */
    public StringBuilder appendSamplingConfiguration(StringBuilder configurationBuilder) {
        StringBuilder builder = configurationBuilder;
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append(SAMPLING_ENABLED).append(':').append(samplingProperties.isEnabled())
                .append(ObjectUtils.NEW_LINE);
        builder.append(SAMPLING_MEAN).append(':').append(samplingProperties.getMean()).append(ObjectUtils.NEW_LINE);
        builder.append(SAMPLING_PEAK_TYPE).append(':').append(samplingProperties.getPeakType())
                .append(ObjectUtils.NEW_LINE);
        return builder;
    }

    /**
     * Apply axis configuration.
     * 
     * @param prefix Axis settings prefix.
     * @param axis The axis identifier.
     * @param f CfFileReader object which contains axis parameters.
     */
    public void applyAxisConfiguration(String prefix, int axis, CfFileReader f) {
        AbstractAxisView view = getAxis(axis);
        if (view != null) {
            List<String> p;
            p = f.getParam(prefix + VISIBLE);
            if (p != null) {
                view.setVisible(OFormat.getBoolean(p.get(0).toString()));
            }
            p = f.getParam(prefix + GRID);
            if (p != null) {
                view.getAttributes().setGridVisible(OFormat.getBoolean(p.get(0).toString()));
            }
            p = f.getParam(prefix + SUBGRID);
            if (p != null) {
                view.getAttributes().setSubGridVisible(OFormat.getBoolean(p.get(0).toString()));
            }
            p = f.getParam(prefix + GRID_STYLE);
            if (p != null) {
                view.getAttributes().setGridStyle(OFormat.getInt(p.get(0).toString()));
            }
            p = f.getParam(prefix + MIN);
            if (p != null) {
                view.getAttributes().setMinimum(OFormat.getDouble(p.get(0).toString()));
            }
            p = f.getParam(prefix + MAX);
            if (p != null) {
                view.getAttributes().setMaximum(OFormat.getDouble(p.get(0).toString()));
            }
            p = f.getParam(prefix + AUTOSCALE);
            if (p != null) {
                view.getAttributes().setAutoScale(OFormat.getBoolean(p.get(0).toString()));
            }
            p = f.getParam(prefix + SCALE);
            if (p != null) {
                view.getAttributes().setScale(OFormat.getInt(p.get(0).toString()));
            } else {
                // To handle a bug in older version
                p = f.getParam(prefix + "cale");
                if (p != null) {
                    view.getAttributes().setScale(OFormat.getInt(p.get(0).toString()));
                }
            }
            p = f.getParam(prefix + FORMAT);
            if (p != null) {
                view.getAttributes().setLabelFormat(OFormat.getInt(p.get(0).toString()));
            }
            p = f.getParam(prefix + TITLE);
            if (p != null) {
                view.getAttributes().setTitle(OFormat.getName(p.get(0).toString()));
            }
            p = f.getParam(prefix + COLOR);
            if (p != null) {
                view.setAxisColor(OFormat.getColor(p));
            }
            p = f.getParam(prefix + LABEL_FONT);
            if (p != null) {
                view.getAttributes().setLabelFont(OFormat.getFont(p));
            }
            p = f.getParam(prefix + FIT_DISPLAY_DURATION);
            if (p != null) {
                view.getAttributes().setFitXAxisToDisplayDuration(OFormat.getBoolean(p.get(0).toString()));
            }
            updateScale(view);
        }
    }

    /**
     * Applies sampling configuration.
     * 
     * @param f CfFileReader object which contains axis parameters.
     */
    public void applySamplingConfiguration(CfFileReader f) {
        List<String> p;
        p = f.getParam(SAMPLING_ENABLED);
        if (p != null) {
            samplingProperties.setEnabled(OFormat.getBoolean(p.get(0)));
        }
        p = f.getParam(SAMPLING_MEAN);
        if (p != null) {
            samplingProperties.setMean(OFormat.getInt(p.get(0)));
        }
        p = f.getParam(SAMPLING_PEAK_TYPE);
        if (p != null) {
            samplingProperties.setPeakType(OFormat.getInt(p.get(0)));
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (e != null) {
            if (e.getSource() == zoomButton) {
                ignoreZoomButtonAction = true;
                zoomButtonMoved = true;
                zoomButtonX = zoomButton.getX() + e.getX() + zoomButtonDx;
                zoomButtonY = zoomButton.getY() + e.getY() + zoomButtonDy;
                zoomButton.setLocation(zoomButtonX, zoomButtonY);
            } else if (e.getSource() == this) {
                if (moveDataView && (lastSearch != null) && (!Double.isNaN(lastSearchXStart))) {
                    AbstractDataView view = lastSearch.getDataView();
                    if (view != null) {
                        if (getDataViewOffsetDragType() == IChartViewer.DRAG_X
                                || getDataViewOffsetDragType() == IChartViewer.DRAG_XY) {
                            double currentXAxisValue = getAxis(IChartViewer.X).getScale().getValueAt(e.getX(),
                                    getVisibleArea());
                            view.setA0X(lastSearchA0X + currentXAxisValue - lastSearchXStart);
                        }
                        if (getDataViewOffsetDragType() == IChartViewer.DRAG_Y
                                || getDataViewOffsetDragType() == IChartViewer.DRAG_XY) {
                            double currentAxisValue = getAxis(lastSearch.getAxis()).getScale().getValueAt(e.getY(),
                                    getVisibleArea());
                            view.setA0(lastSearchA0 + currentAxisValue - lastSearchYStart);
                        }
                        lastSearch.setX(e.getX());
                        lastSearch.setY(e.getY());
                        Graphics g = getGraphics();
                        cleanInfoPopup();
                        showPanel(g, lastSearch, true);
                        g.dispose();
                        repaint();
                    }
                } else if (zoomDrag || areaSelection) {
                    int repaintMaxX;
                    int repaintMinX = Math.min(e.getX(), Math.min(lastX, zoomX));
                    int repaintMinY = Math.min(e.getY(), Math.min(lastY, zoomY));
                    int repaintMaxY = Math.max(e.getY(), Math.max(lastY, zoomY));

                    repaintMaxX = Math.max(e.getX(), Math.max(lastX, zoomX));
                    lastX = e.getX();

                    // create repaint rectangle
                    final Rectangle repaintRect = buildRect(repaintMinX, repaintMinY, repaintMaxX, repaintMaxY);
                    repaintRect.width += 1;
                    repaintRect.height += 1;

                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            repaint(repaintRect);
                        }
                    });

                    // we save current coordinate
                    lastY = e.getY();
                } else {
                    // Concerning translation, we consider to really start translating
                    // when a movement > 5 pixels is done. That's why there are 2 booleans:
                    // potentialTranslateDrag and translateDrag.
                    // potentialTranslateDrag indicates there may be a will to start translation,
                    // but the movement is still not enough, and translateDrag indicates
                    // there is a will to do translation and the movement is enough.
                    if (potentialTranslateDrag) {
                        int xTrans, yTrans, distance, refDist = dragProperties.getScaleDragSensitivity();
                        refDist *= refDist; // distance is a square value for pythagorean theorem
                        switch (dragProperties.getScaleDragType()) {
                            case IChartViewer.DRAG_X:
                                xTrans = e.getX() - lastX;
                                yTrans = 0;
                                break;
                            case IChartViewer.DRAG_Y:
                                xTrans = 0;
                                yTrans = e.getY() - lastY;
                                break;
                            case IChartViewer.DRAG_XY:
                                xTrans = e.getX() - lastX;
                                yTrans = e.getY() - lastY;
                                break;
                            default:
                                xTrans = 0;
                                yTrans = 0;
                                translateDrag = false;
                                potentialTranslateDrag = false;
                                break;
                        }
                        // Use pythagorean theorem to compute cursor distance
                        distance = xTrans * xTrans + yTrans * yTrans;
                        if (distance > refDist) {
                            // We moved a bit, start translation
                            setCursor(new Cursor(Cursor.HAND_CURSOR));
                            translateDrag = true;
                            potentialTranslateDrag = false;
                        }
                    }
                    if (translateDrag) {
                        int tx, ty;
                        switch (dragProperties.getScaleDragType()) {
                            case IChartViewer.DRAG_X:
                                tx = e.getX() - lastX;
                                ty = 0;
                                lastX = e.getX();
                                applyTranslation(tx, ty);
                                repaint();
                                break;
                            case IChartViewer.DRAG_Y:
                                tx = 0;
                                ty = e.getY() - lastY;
                                lastY = e.getY();
                                applyTranslation(tx, ty);
                                repaint();
                                break;
                            case IChartViewer.DRAG_XY:
                                tx = e.getX() - lastX;
                                ty = e.getY() - lastY;
                                lastX = e.getX();
                                lastY = e.getY();
                                applyTranslation(tx, ty);
                                repaint();
                                break;
                            default:
                                translateDrag = false;
                                potentialTranslateDrag = false;
                                break;
                        }
                    }
                }
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // Nothing to do
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // Nothing to do
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e != null) {
            if (e.getSource() == this) {
                requestFocus();
                moveDataView = false;
                lastSearchXStart = Double.NaN;
                lastSearchYStart = Double.NaN;
                lastSearchA0X = 0;
                lastSearchA0 = 0;
                if (e.getButton() == MouseEvent.BUTTON1) {
                    // Left button click
                    SearchInfo search, axisSearch;
                    // Look for the nearest value on each dataView
                    search = searchNearest(e.getX(), e.getY(), IChartViewer.Y1);
                    axisSearch = searchNearest(e.getX(), e.getY(), IChartViewer.Y2);
                    if ((axisSearch != null) && axisSearch.isFound()
                            && ((search == null) || (!search.isFound()) || (axisSearch.getDist() < search.getDist()))) {
                        search = axisSearch;
                    }
                    lastSearch = search;
                    if (useSelectionArea && e.isShiftDown()) {
                        areaSelection = true;
                        zoomX = e.getX();
                        zoomY = e.getY();
                        lastX = e.getX();
                        lastY = e.getY();
                    } else if ((useControlKeyToZoom && macOSXAdapter.isAdaptedControlDown(e))
                            || ((!useControlKeyToZoom) && (!macOSXAdapter.isAdaptedControlDown(e)))) {
                        // Zoom management
                        zoomDrag = true;
                        zoomX = e.getX();
                        zoomY = e.getY();
                        lastX = e.getX();
                        lastY = e.getY();
                    } else if ((lastSearch != null) && lastSearch.isFound()) {
                        if (e.getClickCount() > 1) {
                            switch (getDataViewOffsetDragType()) {
                                case IChartViewer.DRAG_X:
                                case IChartViewer.DRAG_Y:
                                case IChartViewer.DRAG_XY:
                                    moveDataView = true;
                                    AbstractDataView view = search.getDataView();
                                    AbstractAxisScale xScale = getAxis(IChartViewer.X).getScale();
                                    AbstractAxisScale yScale = getAxis(search.getAxis()).getScale();
                                    lastSearchXStart = xScale.getValueAt(e.getX(), getVisibleArea());
                                    lastSearchYStart = yScale.getValueAt(e.getY(), getVisibleArea());
                                    lastSearchA0X = view == null ? 0 : view.getA0X();
                                    lastSearchA0 = view == null ? 0 : view.getA0();
                                    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                                    if (e.getClickCount() == 2) {
                                        if (view == null) {
                                            lastSearchHighlighted = false;
                                        } else {
                                            lastSearchHighlighted = view.isHighlighted();
                                            view.setHighlighted(true);
                                            repaint();
                                        }
                                    }
                                    break;
                                default:
                                    moveDataView = false;
                                    break;
                            }
                        }
                        if ((!moveDataView) && (dragProperties.getScaleDragType() != IChartViewer.DRAG_NONE)) {
                            potentialTranslateDrag = true;
                            lastX = e.getX();
                            lastY = e.getY();
                        }
                        if (displayableTooltipEvent == null) {
                            displayableTooltipEvent = new MouseEvent(this, e.getID(), e.getWhen(), e.getModifiers(),
                                    e.getX(), e.getY(), e.getClickCount(), e.isPopupTrigger());
                        }
                        Graphics g = getGraphics();
                        showPanel(g, lastSearch);
                        g.dispose();
                    }
                }
            } else if (e.getSource() == zoomButton) {
                zoomButtonDx = -e.getX();
                zoomButtonDy = -e.getY();
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e != null) {
            if (e.getSource() == this) {
                boolean measure = false;
                boolean alreadySelected = false;
                if (moveDataView && (lastSearch != null)) {
                    setCursor(Cursor.getDefaultCursor());
                    AbstractDataView view = lastSearch.getDataView();
                    moveDataView = false;
                    lastSearchXStart = Double.NaN;
                    lastSearchYStart = Double.NaN;
                    lastSearchA0X = 0;
                    lastSearchA0 = 0;
                    measure = true;
                    if (view != null) {
                        view.setHighlighted(lastSearchHighlighted);
                        IChartViewer chart = chartEventDelegate.getChart();
                        if (chart != null) {
                            chart.setDataViewXOffsetA0(view.getId(), view.getA0X());
                        }
                    }
                }
                if (areaSelection) {
                    areaSelection = false;
                    ChartEventDelegate chartEventDelegate = this.chartEventDelegate;
                    if (chartEventDelegate != null) {
                        Rectangle zoomRect = buildRect(zoomX, zoomY, e.getX(), e.getY());
                        int xOrg = viewR.x + y1AxisThickness;
                        int yOrg = viewR.y + xAxisUpMargin;
                        Rectangle visibleRect = new Rectangle(xOrg, yOrg, xAxis.getLength(), y1Axis.getLength());
                        if (zoomSelectionOK(zoomRect)) {
                            double[] xBounds = getNewBounds(xAxis.getScale(), zoomRect.x, zoomRect.x + zoomRect.width,
                                    visibleRect);
                            double[] y1Bounds = getNewBounds(y1Axis.getScale(), zoomRect.y,
                                    zoomRect.y + zoomRect.height, visibleRect);
                            double[] y2Bounds = getNewBounds(y2Axis.getScale(), zoomRect.y,
                                    zoomRect.y + zoomRect.height, visibleRect);
                            double[] coordinates = new double[] { xBounds[0], xBounds[1], y1Bounds[0], y1Bounds[1],
                                    y2Bounds[0], y2Bounds[1] };
                            selectedCoordinates = coordinates;
                            alreadySelected = true;
                            chartEventDelegate.fireChartEvent(new ChartViewerEvent(chartEventDelegate.getChart(),
                                    coordinates, -1, null, null, Reason.SELECTION));
                        }
                    }
                }
                if (zoomDrag) {
                    zoomDrag = false;
                    if (applyZoom(e.getX(), e.getY())) {
                        measure = true;
                    }
                }
                if (translateDrag) {
                    translateDrag = false;
                    setCursor(Cursor.getDefaultCursor());
                }
                potentialTranslateDrag = false;

                if (isFireSelectionEventOnAnyClick()
                        || ((!alreadySelected) && ((isUseControlKeyToZoom() && macOSXAdapter.isAdaptedControlDown(e))
                                || ((!isUseControlKeyToZoom()) && (!macOSXAdapter.isAdaptedControlDown(e)))))) {
                    // Notify listeners for selection change
                    SearchInfo search = lastSearch;
                    if (search != null) {
                        DataList data = search.getValue();
                        if (data != null) {
                            AbstractDataView view = search.getDataView();
                            if (chartEventDelegate != null) {
                                List<Integer> mask = new ArrayList<Integer>();
                                int[] maskTab;
                                if (e != null) {
                                    if (e.isAltDown()) {
                                        mask.add(Integer.valueOf(CometeEvent.KEY_ALT));
                                    }
                                    if (e.isShiftDown()) {
                                        mask.add(Integer.valueOf(CometeEvent.KEY_SHIFT));
                                    }
                                    if (macOSXAdapter.isAdaptedControlDown(e)) {
                                        mask.add(Integer.valueOf(CometeEvent.KEY_CONTROL));
                                    }
                                }
                                maskTab = new int[mask.size()];
                                for (int i = 0; i < maskTab.length; i++) {
                                    maskTab[i] = mask.get(i);
                                }
                                chartEventDelegate.fireChartEvent(new ChartViewerEvent(chartEventDelegate.getChart(),
                                        new double[] { data.getX(), data.getY() }, search.getClickIdx(),
                                        (view == null ? null : view.getId()), null, Reason.SELECTION, maskTab));
                            }
                        }
                    }
                }
                displayableTooltipText = null;
                if (displayableTooltipEvent != null) {
                    displayableTooltipEvent.consume();
                    displayableTooltipEvent = null;
                }
                cleanInfoPopup();
                ipanelVisible = false;
                displayableTooltipText = null;

                if (measure) {
                    measureGraphItems();
                }
                repaint();
            }
        }
    }

    protected double[] getNewBounds(AbstractAxisScale scale, int x1, int x2, Rectangle boundRect) {
        double[] bounds = scale.getNewBounds(x1, x2, boundRect);
        for (int i = 0; i < bounds.length; i++) {
            bounds[i] = scale.getUnScaledValue(bounds[i]);
        }
        return bounds;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Nothing to do
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Nothing to do
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == zoomButton) {
            if (ignoreZoomButtonAction) {
                ignoreZoomButtonAction = false;
            } else {
                exitZoom();
                measureGraphItems();
                repaint();
            }
        }
    }

    @Override
    public void componentResized(ComponentEvent e) {
        lastWidth = getWidth();
        lastHeight = getHeight();
        measureGraphItems();
        if (isVisible()) {
            repaint();
        }
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        // Nothing to do
    }

    @Override
    public void componentShown(ComponentEvent e) {
        // Nothing to do
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        // Nothing to do
    }

}
