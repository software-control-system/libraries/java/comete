/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.expression;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;

import fr.soleil.comete.swing.chart.JLChart;
import fr.soleil.comete.swing.chart.JLDataView;
import fr.soleil.comete.swing.chart.JLDataViewOption;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.ObjectUtils;

/**
 * @deprecated use {@link ExpressionOption} instead
 */
@Deprecated
public class ExpressionParserOption extends JLDataViewOption {

    private static final long serialVersionUID = 6680832108300102533L;

    protected final static Color SELECTION_COLOR = new Color(255, 160, 200);
    protected final static Color MOUSE_OVER_COLOR = new Color(200, 180, 255);
    protected final static Border LABEL_BORDER = new LineBorder(Color.BLACK, 1);
    protected final static Border SELECTION_BORDER = new LineBorder(SELECTION_COLOR, 1);
    protected final static Border TEXT_BORDER = new EtchedBorder();

    protected JButton cancelButton;
    protected JTextField expressionField;
    protected JLabel expressionLabel;
    protected JButton generateButton;
    protected JButton helpButton;
    protected VariablePanel variablePanel;
    protected JScrollPane variableScrollPane;

    public ExpressionParserOption() {
        super();
    }

    @Override
    public void setChart(JLChart chart) {
        super.setChart(chart);
        axisBox.removeItem("X");
        boolean canSetOnX = true;
        if (chart != null) {
            List<JLDataView> views;
            if (chart.isMathExpressionEnabled()) {
                views = chart.getViews();
                canSetOnX = chart.isCanPutExpressionOnX();
            } else {
                views = new ArrayList<JLDataView>();
                if (chart.getXAxis().isXY()) {
                    views.addAll(chart.getXAxis().getViewsCopy());
                }
                views.addAll(chart.getY1Axis().getViewsCopy());
                views.addAll(chart.getY2Axis().getViewsCopy());
            }
            String[] names = new String[views.size()];
            String[] ids = new String[views.size()];
            for (int i = 0; i < views.size(); i++) {
                ids[i] = (views.get(i)).getId();
                names[i] = (views.get(i)).getDisplayName();
            }
            views.clear();
            variablePanel.setNamesAndIds(names, ids);
            variablePanel.setX(false);
            views = null;
            names = null;
        }
        if (canSetOnX) {
            axisBox.addItem("X");
        }
    }

    protected VariablePanel createVariablePanel() {
        return new VariablePanel(this);
    }

    @Override
    protected void initComponents() {
        super.initComponents();
        setName("Expression Evaluation");
        JPanel newPanel = new JPanel(new GridBagLayout());
        variablePanel = createVariablePanel();
        expressionLabel = new JLabel("Enter your expression:");
        expressionLabel.setFont(CometeUtils.getLabelFont());
        expressionField = new JTextField(10);
        expressionField.setMargin(CometeUtils.getTextFieldInsets());
        cancelButton = new JButton("Cancel");
        helpButton = new JButton("?");
        helpButton.setToolTipText("About Expressions");
        helpButton.addActionListener(this);
        generateButton = new JButton("Generate Variables");
        generateButton.addActionListener(this);
        variableScrollPane = new JScrollPane(variablePanel);
        variableScrollPane.setPreferredSize(new Dimension(270, 215));
        variableScrollPane.setBorder(BorderFactory.createTitledBorder(new EtchedBorder(), "Variables"));
        closeButton.setText("Ok");
        closeButton.addActionListener(this);
        southPanel.remove(nameLabel);
        southPanel.add(cancelButton, BorderLayout.WEST);
        southPanel.add(Box.createGlue(), BorderLayout.CENTER);
        GridBagConstraints expressionLabelConstraints = new GridBagConstraints();
        expressionLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        expressionLabelConstraints.gridx = 0;
        expressionLabelConstraints.gridy = 0;
        expressionLabelConstraints.weightx = 0;
        expressionLabelConstraints.weighty = 0;
        expressionLabelConstraints.insets = new Insets(5, 5, 0, 0);
        newPanel.add(expressionLabel, expressionLabelConstraints);
        GridBagConstraints expressionFieldConstraints = new GridBagConstraints();
        expressionFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        expressionFieldConstraints.gridx = 1;
        expressionFieldConstraints.gridy = 0;
        expressionFieldConstraints.weightx = 1;
        expressionFieldConstraints.weighty = 0;
        expressionFieldConstraints.insets = new Insets(5, 5, 0, 5);
        newPanel.add(expressionField, expressionFieldConstraints);
        GridBagConstraints helpButtonConstraints = new GridBagConstraints();
        helpButtonConstraints.fill = GridBagConstraints.NONE;
        helpButtonConstraints.gridx = 0;
        helpButtonConstraints.gridy = 1;
        helpButtonConstraints.weightx = 0;
        helpButtonConstraints.weighty = 0;
        helpButtonConstraints.insets = new Insets(5, 5, 0, 0);
        helpButtonConstraints.anchor = GridBagConstraints.LINE_START;
        newPanel.add(helpButton, helpButtonConstraints);
        GridBagConstraints generateButtonConstraints = new GridBagConstraints();
        generateButtonConstraints.fill = GridBagConstraints.NONE;
        generateButtonConstraints.gridx = 1;
        generateButtonConstraints.gridy = 1;
        generateButtonConstraints.weightx = 0;
        generateButtonConstraints.weighty = 0;
        generateButtonConstraints.insets = new Insets(5, 5, 0, 5);
        generateButtonConstraints.anchor = GridBagConstraints.WEST;
        newPanel.add(generateButton, generateButtonConstraints);
        GridBagConstraints variableScrollPaneConstraints = new GridBagConstraints();
        variableScrollPaneConstraints.fill = GridBagConstraints.BOTH;
        variableScrollPaneConstraints.gridx = 0;
        variableScrollPaneConstraints.gridy = 2;
        variableScrollPaneConstraints.weightx = 1;
        variableScrollPaneConstraints.weighty = 1;
        variableScrollPaneConstraints.insets = new Insets(5, 5, 0, 5);
        variableScrollPaneConstraints.gridwidth = GridBagConstraints.REMAINDER;
        newPanel.add(variableScrollPane, variableScrollPaneConstraints);
        GridBagConstraints axisLabelConstraints = new GridBagConstraints();
        axisLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        axisLabelConstraints.gridx = 0;
        axisLabelConstraints.gridy = 3;
        axisLabelConstraints.weightx = 0;
        axisLabelConstraints.weighty = 0;
        axisLabelConstraints.insets = new Insets(5, 5, 5, 0);
        axisLabelConstraints.anchor = GridBagConstraints.EAST;
        newPanel.add(axisLabel, axisLabelConstraints);
        GridBagConstraints axisBoxConstraints = new GridBagConstraints();
        axisBoxConstraints.fill = GridBagConstraints.NONE;
        axisBoxConstraints.gridx = 1;
        axisBoxConstraints.gridy = 3;
        axisBoxConstraints.weightx = 0;
        axisBoxConstraints.weighty = 0;
        axisBoxConstraints.insets = new Insets(5, 5, 5, 5);
        axisBoxConstraints.anchor = GridBagConstraints.WEST;
        newPanel.add(axisBox, axisBoxConstraints);
        add(newPanel, BorderLayout.NORTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == closeButton) {
            properties.setAxisChoice(axisBox.getSelectedIndex());
            if (chart != null) {
                String displayName;
                if (lineNameText.getText() == null || lineNameText.getText().trim().isEmpty()) {
                    displayName = expressionField.getText().trim();
                } else {
                    displayName = lineNameText.getText().trim();
                }
                lineNameText.setText(displayName);
                properties.getCurve().setName(displayName);
            }
            commit();
        } else if (e.getSource() == generateButton) {
            variablePanel.setExpression(expressionField.getText());
            variablePanel.generateVariables();
        } else if (e.getSource() == helpButton) {
            new ParserHelpDialog(CometeUtils.getWindowForComponent(this)).setVisible(true);
        } else {
            super.actionPerformed(e);
        }
    }

    public boolean isX() {
        return variablePanel.isX();
    }

    public String[] getVariables() {
        return variablePanel.getVariables();
    }

    public void setVariableNamesAndIds(String[] names, String[] ids) {
        variablePanel.setNamesAndIds(names, ids);
    }

    // hack to set expression
    public void setExpression(String expression, String[] possibleVariableNames, String[] possibleVariableIds,
            String[] selectedIds) {
        HashMap<String, String> valueMap = new HashMap<String, String>();
        for (int i = 0; i < possibleVariableIds.length; i++) {
            valueMap.put(possibleVariableIds[i], possibleVariableNames[i]);
        }
        expressionField.setText(expression);
        variablePanel.setExpression(expression);
        variablePanel.setNamesAndIds(possibleVariableNames, possibleVariableIds);
        variablePanel.generateVariables();
        for (int i = 0; i < variablePanel.variableNames.length; i++) {
            variablePanel.variableNames[i].setName(selectedIds[i]);
            variablePanel.variableNames[i].setText(valueMap.get(selectedIds[i]));
            variablePanel.variableNames[i].setToolTipText(valueMap.get(selectedIds[i]));
        }
        valueMap.clear();
        valueMap = null;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public String getExpression() {
        return expressionField.getText().trim();
    }

    protected class PossibleNamesLister extends JDialog implements MouseListener {
        private static final long serialVersionUID = -1291956048071263432L;
        protected JLabel lastSelected = null;
        protected JLabel destination = null;
        protected JLabel[] names = null;
        protected String[] ids;
        protected JPanel mainPanel = null;
        protected JScrollPane scroller = null;
        protected final static int maxWidth = 400;
        protected final static int maxHeight = 400;
        protected VariablePanel parent;

        public PossibleNamesLister(String[] nameList, String[] idList, VariablePanel parent) {
            super(CometeUtils.getWindowForComponent(parent), "Choose variable:");
            this.parent = parent;
            setModal(true);
            mainPanel = new JPanel(new GridBagLayout());
            if ((nameList != null) && (idList != null)) {
                int dataCount = Math.min(nameList.length, idList.length);
                names = new JLabel[dataCount];
                ids = new String[dataCount];
                for (int i = 0; i < dataCount; i++) {
                    names[i] = new JLabel();
                    if (nameList[i] != null) {
                        names[i].setText(nameList[i]);
                        names[i].setToolTipText(nameList[i]);
                    }
                    names[i].setName(idList[i]);
                    ids[i] = idList[i];
                    names[i].setBorder(LABEL_BORDER);
                    GridBagConstraints nameConstraints = new GridBagConstraints();
                    nameConstraints.fill = GridBagConstraints.HORIZONTAL;
                    nameConstraints.gridx = 0;
                    nameConstraints.gridy = i;
                    nameConstraints.weightx = 1;
                    nameConstraints.weighty = 0;
                    mainPanel.add(names[i], nameConstraints);
                    names[i].addMouseListener(this);
                }
            }
            mainPanel.setBackground(Color.WHITE);
            scroller = new JScrollPane(mainPanel);
            scroller.setBackground(Color.WHITE);
            scroller.getViewport().setBackground(Color.WHITE);
            scroller.getHorizontalScrollBar().setBackground(Color.WHITE);
            scroller.getVerticalScrollBar().setBackground(Color.WHITE);
            scroller.setBorder(new EmptyBorder(0, 0, 0, 0));
            int barWidth = scroller.getVerticalScrollBar().getPreferredSize().width;
            int barHeight = scroller.getHorizontalScrollBar().getPreferredSize().height;
            int prefWidth = mainPanel.getPreferredSize().width;
            int prefHeight = mainPanel.getPreferredSize().height;
            // adapt size with bar size
            if (prefHeight > maxHeight) {
                prefHeight = maxHeight;
                prefWidth += barWidth;
            }
            if (prefWidth > maxWidth) {
                prefWidth = maxWidth;
                prefHeight += barHeight;
            }
            // restrict size
            if (prefHeight > maxHeight) {
                prefHeight = maxHeight;
            }
            if (prefWidth > maxWidth) {
                prefWidth = maxWidth;
            }
            scroller.setPreferredSize(new Dimension(prefWidth, prefHeight));
            scroller.setMinimumSize(new Dimension(Math.min(100, prefWidth), Math.min(100, prefHeight)));
            setContentPane(scroller);
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (lastSelected != null) {
                lastSelected.setBorder(LABEL_BORDER);
                lastSelected.setOpaque(false);
                lastSelected.setBackground(Color.WHITE);
                lastSelected.repaint();
                setVisible(false);
            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            ((JLabel) e.getSource()).setBackground(MOUSE_OVER_COLOR);
            ((JLabel) e.getSource()).setOpaque(true);
            ((JLabel) e.getSource()).repaint();
        }

        @Override
        public void mouseExited(MouseEvent e) {
            ((JLabel) e.getSource()).setOpaque(false);
            ((JLabel) e.getSource()).setBackground(Color.WHITE);
            ((JLabel) e.getSource()).repaint();
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (lastSelected != null && lastSelected != (JLabel) e.getSource()) {
                lastSelected.setBorder(LABEL_BORDER);
                lastSelected.setOpaque(false);
                lastSelected.setBackground(Color.WHITE);
                lastSelected.repaint();
            }
            ((JLabel) e.getSource()).setBackground(SELECTION_COLOR);
            ((JLabel) e.getSource()).setBorder(SELECTION_BORDER);
            ((JLabel) e.getSource()).setOpaque(true);
            ((JLabel) e.getSource()).repaint();
            lastSelected = (JLabel) e.getSource();
            if (destination != null) {
                parent.setVariable(destination, ((JLabel) e.getSource()).getName(), ((JLabel) e.getSource()).getText());
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            // nothing to do
        }

        @Override
        public void dispose() {
            if (names != null) {
                for (int i = 0; i < names.length; i++) {
                    names[i] = null;
                }
                names = null;
            }
            lastSelected = null;
        }

        public JLabel getDestination() {
            return destination;
        }

        public void setDestination(JLabel destination) {
            this.destination = destination;
        }
    }

    protected class VariablePanel extends JPanel implements MouseListener {
        private static final long serialVersionUID = -6178681329402760994L;
        protected String expression = null;
        protected JLabel[] variableTitles = null;
        protected JLabel[] variableNames = null;
        protected JLabel lastSelected = null;
        protected boolean x = false;
        protected Component parent;
        protected PossibleNamesLister lister = null;

        public VariablePanel(Component parent) {
            super(new GridBagLayout());
            this.parent = parent;
        }

        public void setNamesAndIds(String[] names, String ids[]) {
            if ((lister != null) && lister.isVisible()) {
                lister.setVisible(false);
                lister.getContentPane().removeAll();
                lister.dispose();
            }
            removeAll();
            lister = new PossibleNamesLister(names, ids, this);
        }

        public void setVariable(JLabel destination, String id, String name) {
            destination.setText(name);
            destination.setToolTipText(name);
            destination.setName(id);
        }

        public void generateVariables() {
            this.removeAll();
            int count = 0;
            if (expression.indexOf("x1") != -1) {
                x = false;
                while (true) {
                    if (expression.indexOf("x" + (count + 1)) != -1) {
                        count++;
                    } else {
                        break;
                    }
                }
                variableTitles = new JLabel[count];
                variableNames = new JLabel[count];
                for (int i = 0; i < count; i++) {
                    GridBagConstraints titleConstraints = new GridBagConstraints();
                    titleConstraints.fill = GridBagConstraints.HORIZONTAL;
                    titleConstraints.gridx = 0;
                    titleConstraints.gridy = i;
                    titleConstraints.weightx = 0;
                    titleConstraints.weighty = 0;
                    titleConstraints.insets = new Insets((i == 0 ? 5 : 0), 5, 5, 0);
                    variableTitles[i] = new JLabel("x" + (i + 1) + ":");
                    this.add(variableTitles[i], titleConstraints);
                    GridBagConstraints nameConstraints = new GridBagConstraints();
                    nameConstraints.fill = GridBagConstraints.BOTH;
                    nameConstraints.gridx = 1;
                    nameConstraints.gridy = i;
                    nameConstraints.weightx = 1;
                    nameConstraints.weighty = 0;
                    nameConstraints.insets = new Insets((i == 0 ? 5 : 0), 0, 5, 5);
                    variableNames[i] = new JLabel(ObjectUtils.EMPTY_STRING);
                    variableNames[i].setBackground(Color.WHITE);
                    variableNames[i].setBorder(TEXT_BORDER);
                    variableNames[i].setOpaque(true);
                    variableNames[i].addMouseListener(this);
                    variableNames[i].setPreferredSize(new Dimension(210, 25));
                    this.add(variableNames[i], nameConstraints);
                }
            } else {
                if (expression.replaceAll("x", "_x_").replaceAll("e_x_p", "exp").indexOf("_x_") != -1) {
                    count = 1;
                    x = true;
                    GridBagConstraints titleConstraints = new GridBagConstraints();
                    titleConstraints.fill = GridBagConstraints.HORIZONTAL;
                    titleConstraints.gridx = 0;
                    titleConstraints.gridy = 0;
                    titleConstraints.weightx = 0;
                    titleConstraints.weighty = 0;
                    titleConstraints.insets = new Insets(5, 5, 5, 0);
                    variableTitles = new JLabel[1];
                    variableNames = new JLabel[1];
                    variableTitles[0] = new JLabel("x:");
                    this.add(variableTitles[0], titleConstraints);
                    GridBagConstraints nameConstraints = new GridBagConstraints();
                    nameConstraints.fill = GridBagConstraints.BOTH;
                    nameConstraints.gridx = 1;
                    nameConstraints.gridy = 0;
                    nameConstraints.weightx = 1;
                    nameConstraints.weighty = 0;
                    nameConstraints.insets = new Insets(5, 0, 5, 5);
                    variableNames[0] = new JLabel(ObjectUtils.EMPTY_STRING);
                    variableNames[0].setBounds(40, 0, 210, 25);
                    variableNames[0].setBackground(Color.WHITE);
                    variableNames[0].setBorder(TEXT_BORDER);
                    variableNames[0].setOpaque(true);
                    variableNames[0].addMouseListener(this);
                    variableNames[0].setPreferredSize(new Dimension(210, 25));
                    this.add(variableNames[0], nameConstraints);
                } else {
                    x = false;
                    variableTitles = new JLabel[0];
                    variableNames = new JLabel[0];
                }
            }
            GridBagConstraints glueConstraints = new GridBagConstraints();
            glueConstraints.fill = GridBagConstraints.BOTH;
            glueConstraints.gridx = 0;
            glueConstraints.gridy = count;
            glueConstraints.weightx = 1;
            glueConstraints.weighty = 1;
            glueConstraints.gridwidth = GridBagConstraints.REMAINDER;
            this.add(Box.createGlue(), glueConstraints);

            revalidate();
            repaint();
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            // nothing to do
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            // nothing to do
        }

        @Override
        public void mouseExited(MouseEvent e) {
            // nothing to do
        }

        @Override
        public void mousePressed(MouseEvent e) {
            lister.pack();
            lister.setDestination((JLabel) e.getSource());
            lister.setLocation(e.getLocationOnScreen());
            lister.setVisible(true);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            // nothing to do
        }

        public String getExpression() {
            return expression;
        }

        public void setExpression(String expression) {
            this.expression = expression;
        }

        public void dispose() {
            expression = null;
            if (variableNames != null) {
                for (int i = 0; i < variableNames.length; i++) {
                    variableNames[i] = null;
                    variableTitles[i] = null;
                }
                variableNames = null;
                variableTitles = null;
            }
            lastSelected = null;
            parent = null;
        }

        public void setX(boolean x) {
            this.x = x;
        }

        public boolean isX() {
            return x;
        }

        public String[] getVariables() {
            String[] variables;
            if (variableNames == null) {
                variables = new String[0];
            } else {
                variables = new String[variableNames.length];
                for (int i = 0; i < variableNames.length; i++) {
                    if ((variableNames[i] == null) || (variableNames[i].getName() == null)) {
                        variables = new String[0];
                        break;
                    }
                    variables[i] = new String(variableNames[i].getName());
                }
            }
            return variables;
        }
    }

}
