/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util;

import java.awt.Dimension;
import java.awt.Point;

/**
 * A class that calculates data for image profiling
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ProfileDataCalculator {

    /**
     * Calculates the profile tables used for dual profiling (vertical+horizontal).
     * 
     * @param dimX The image width
     * @param dimY The image height
     * @param posX The cursor position in X
     * @param posY The cursor position in Y
     * @param stopAtCursorPosition Whether to stop profile at cursor position. If <code>false</code>, profiles will be
     *            done on whole row and column.
     * @param value The image flat table
     * @param calculator The {@link ImageScreenPositionCalculator} that is able to associate screen
     *            coordinates to image coordinates.
     * 
     * @return A <code>double[][]</code>, which is: <code>{ profileV, profileH, yValues, xValues }</code>, with:
     *         <ul>
     *         <li><code>profileV</code>: vertical profiling values</li>
     *         <li><code>profileH</code>: horizontal profiling values</li>
     *         <li><code>yValues</code>: y coordinates</li>
     *         <li><code>xValues</code>: x coordinates</li>
     *         </ul>
     */
    public static double[][] getProfileTables(int dimX, int dimY, int posX, int posY, boolean stopAtCursorPosition,
            Object value, ImageScreenPositionCalculator calculator) {
        posX = updatePosition(posX, dimX);
        posY = updatePosition(posY, dimY);
        int vLength, hLength;
        if (stopAtCursorPosition) {
            vLength = posY + 1;
            hLength = posX + 1;
        } else {
            vLength = dimY;
            hLength = dimX;
        }
        double[] profileV = new double[vLength];
        double[] profileH = new double[hLength];
        double[] yValues = new double[vLength];
        double[] xValues = new double[hLength];
        if (value instanceof byte[]) {
            byte[] array = (byte[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i);
                profileV[i] = array[(i * dimX) + posX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i);
                profileH[i] = array[(posY * dimX) + i];
            }
        } // end if (value instanceof byte[])
        else if (value instanceof short[]) {
            short[] array = (short[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i);
                profileV[i] = array[(i * dimX) + posX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i);
                profileH[i] = array[(posY * dimX) + i];
            }
        } // end if (value instanceof short[])
        else if (value instanceof int[]) {
            int[] array = (int[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i);
                profileV[i] = array[(i * dimX) + posX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i);
                profileH[i] = array[(posY * dimX) + i];
            }
        } // end if (value instanceof int[])
        else if (value instanceof long[]) {
            long[] array = (long[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i);
                profileV[i] = array[(i * dimX) + posX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i);
                profileH[i] = array[(posY * dimX) + i];
            }
        } // end if (value instanceof long[])
        else if (value instanceof float[]) {
            float[] array = (float[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i);
                profileV[i] = array[(i * dimX) + posX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i);
                profileH[i] = array[(posY * dimX) + i];
            }
        } // end if (value instanceof float[])
        else if (value instanceof double[]) {
            double[] array = (double[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i);
                profileV[i] = array[(i * dimX) + posX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i);
                profileH[i] = array[(posY * dimX) + i];
            }
        } // end if (value instanceof double[])
        else if (value instanceof Number[]) {
            Number[] array = (Number[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i);
                profileV[i] = NumberImageConverter.extractDouble(array[(i * dimX) + posX]);
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i);
                profileH[i] = NumberImageConverter.extractDouble(array[(posY * dimX) + i]);
            }
        } // end if (value instanceof Number[])
        return new double[][] { profileV, profileH, yValues, xValues };
    }

    /**
     * Calculates the profile tables used for dual profiling (vertical+horizontal).
     * 
     * @param dimX The image width
     * @param dimY The image height
     * @param posX The cursor position in X
     * @param posY The cursor position in Y
     * @param stopAtCursorPosition Whether to stop profile at cursor position. If <code>false</code>, profiles will be
     *            done on whole row and column.
     * @param value The image flat table
     * @param calculator The {@link ImageScreenPositionCalculator} that is able to associate screen
     *            coordinates to image coordinates.
     * 
     * @return A <code>double[][]</code>, which is: <code>{ profileV, profileH, yValues, xValues }</code>, with:
     *         <ul>
     *         <li><code>profileV</code>: vertical profiling values</li>
     *         <li><code>profileH</code>: horizontal profiling values</li>
     *         <li><code>yValues</code>: y coordinates</li>
     *         <li><code>xValues</code>: x coordinates</li>
     *         </ul>
     */
    public static double[][] getProfileTables(int dimX, int dimY, int hX, int vX, int hY, int vY, int hLength,
            int vLength, Object value, ImageScreenPositionCalculator calculator) {
        // TODO adapt positions and lengths
        hX = updatePosition(hX, dimX);
        vX = updatePosition(vX, dimX);
        hY = updatePosition(hY, dimY);
        vY = updatePosition(vY, dimY);
        hLength = updateLength(hX, hLength, dimX);
        vLength = updateLength(vY, vLength, dimY);
        double[] profileV = new double[vLength];
        double[] profileH = new double[hLength];
        double[] yValues = new double[vLength];
        double[] xValues = new double[hLength];
        if (value instanceof byte[]) {
            byte[] array = (byte[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i + vY);
                profileV[i] = array[((i + vY) * dimX) + vX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i + hX);
                profileH[i] = array[(hY * dimX) + i + hX];
            }
        } // end if (value instanceof byte[])
        else if (value instanceof short[]) {
            short[] array = (short[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i + vY);
                profileV[i] = array[((i + vY) * dimX) + vX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i + hX);
                profileH[i] = array[(hY * dimX) + i + hX];
            }
        } // end if (value instanceof short[])
        else if (value instanceof int[]) {
            int[] array = (int[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i + vY);
                profileV[i] = array[((i + vY) * dimX) + vX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i + hX);
                profileH[i] = array[(hY * dimX) + i + hX];
            }
        } // end if (value instanceof int[])
        else if (value instanceof long[]) {
            long[] array = (long[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i + vY);
                profileV[i] = array[((i + vY) * dimX) + vX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i + hX);
                profileH[i] = array[(hY * dimX) + i + hX];
            }
        } // end if (value instanceof long[])
        else if (value instanceof float[]) {
            float[] array = (float[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i + vY);
                profileV[i] = array[((i + vY) * dimX) + vX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i + hX);
                profileH[i] = array[(hY * dimX) + i + hX];
            }
        } // end if (value instanceof float[])
        else if (value instanceof double[]) {
            double[] array = (double[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i + vY);
                profileV[i] = array[((i + vY) * dimX) + vX];
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i + hX);
                profileH[i] = array[(hY * dimX) + i + hX];
            }
        } // end if (value instanceof double[])
        else if (value instanceof Number[]) {
            Number[] array = (Number[]) value;
            for (int i = 0; i < yValues.length; i++) {
                yValues[i] = calculator.imagePixelToAxisYD(i + vY);
                profileV[i] = NumberImageConverter.extractDouble(array[((i + vY) * dimX) + vX]);
            }
            for (int i = 0; i < xValues.length; i++) {
                xValues[i] = calculator.imagePixelToAxisXD(i + hX);
                profileH[i] = NumberImageConverter.extractDouble(array[(hY * dimX) + i + hX]);
            }
        } // end if (value instanceof Number[])
        return new double[][] { profileV, profileH, yValues, xValues };
    }

    /**
     * Calculates the profile table that corresponds to a line profile
     * 
     * @param dimX The image width
     * @param dimY The image height
     * @param value The image flat table
     * @param lineProfileRoiX1 X coordinate of the 1st point in line
     * @param lineProfileRoiY1 Y coordinate of the 1st point in line
     * @param lineProfileRoiX2 X coordinate of the 2nd point in line
     * @param lineProfileRoiY2 Y coordinate of the 2nd point in line
     * @return A <code>double[]</code>, that corresponds to the profile data.
     */
    public static double[] buildProfileData(int dimX, int dimY, Object value, int lineProfileRoiX1,
            int lineProfileRoiY1, int lineProfileRoiX2, int lineProfileRoiY2) {
        double[] profile;
        Dimension d = new Dimension(dimX, dimY);

        Point[] p = new Point[2];
        p[0] = new Point(lineProfileRoiX1, lineProfileRoiY1);
        p[1] = new Point(lineProfileRoiX2, lineProfileRoiY2);

        if ((p != null) && (value != null)) {
            int dx = p[1].x - p[0].x;
            int dy = p[1].y - p[0].y;
            int adx = Math.abs(dx);
            int ady = Math.abs(dy);
            double delta;
            int i, xe, ye;
            if (value instanceof byte[]) {
                byte[] array = (byte[]) value;
                if (adx > ady) {
                    delta = (double) dy / (double) adx;
                    profile = new double[adx + 1];
                    xe = p[0].x;
                    for (i = 0; i <= adx; i++) {
                        ye = p[0].y + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dx < 0) {
                            xe--;
                        } else {
                            xe++;
                        }
                    }
                } // end if (adx > ady)
                else {
                    delta = (double) dx / (double) ady;
                    profile = new double[ady + 1];
                    ye = p[0].y;
                    for (i = 0; i <= ady; i++) {
                        xe = p[0].x + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dy < 0) {
                            ye--;
                        } else {
                            ye++;
                        }
                    }
                } // end // end if (adx > ady) ... else
            } // end if (value instanceof byte[]) {
            else if (value instanceof short[]) {
                short[] array = (short[]) value;
                if (adx > ady) {
                    delta = (double) dy / (double) adx;
                    profile = new double[adx + 1];
                    xe = p[0].x;
                    for (i = 0; i <= adx; i++) {
                        ye = p[0].y + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dx < 0) {
                            xe--;
                        } else {
                            xe++;
                        }
                    }
                } // end if (adx > ady)
                else {
                    delta = (double) dx / (double) ady;
                    profile = new double[ady + 1];
                    ye = p[0].y;
                    for (i = 0; i <= ady; i++) {
                        xe = p[0].x + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dy < 0) {
                            ye--;
                        } else {
                            ye++;
                        }
                    }
                } // end // end if (adx > ady) ... else
            } // end if (value instanceof short[]) {
            else if (value instanceof int[]) {
                int[] array = (int[]) value;
                if (adx > ady) {
                    delta = (double) dy / (double) adx;
                    profile = new double[adx + 1];
                    xe = p[0].x;
                    for (i = 0; i <= adx; i++) {
                        ye = p[0].y + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dx < 0) {
                            xe--;
                        } else {
                            xe++;
                        }
                    }
                } // end if (adx > ady)
                else {
                    delta = (double) dx / (double) ady;
                    profile = new double[ady + 1];
                    ye = p[0].y;
                    for (i = 0; i <= ady; i++) {
                        xe = p[0].x + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dy < 0) {
                            ye--;
                        } else {
                            ye++;
                        }
                    }
                } // end // end if (adx > ady) ... else
            } // end if (value instanceof int[]) {
            else if (value instanceof long[]) {
                long[] array = (long[]) value;
                if (adx > ady) {
                    delta = (double) dy / (double) adx;
                    profile = new double[adx + 1];
                    xe = p[0].x;
                    for (i = 0; i <= adx; i++) {
                        ye = p[0].y + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dx < 0) {
                            xe--;
                        } else {
                            xe++;
                        }
                    }
                } // end if (adx > ady)
                else {
                    delta = (double) dx / (double) ady;
                    profile = new double[ady + 1];
                    ye = p[0].y;
                    for (i = 0; i <= ady; i++) {
                        xe = p[0].x + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dy < 0) {
                            ye--;
                        } else {
                            ye++;
                        }
                    }
                } // end // end if (adx > ady) ... else
            } // end if (value instanceof long[]) {
            else if (value instanceof float[]) {
                float[] array = (float[]) value;
                if (adx > ady) {
                    delta = (double) dy / (double) adx;
                    profile = new double[adx + 1];
                    xe = p[0].x;
                    for (i = 0; i <= adx; i++) {
                        ye = p[0].y + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dx < 0) {
                            xe--;
                        } else {
                            xe++;
                        }
                    }
                } // end if (adx > ady)
                else {
                    delta = (double) dx / (double) ady;
                    profile = new double[ady + 1];
                    ye = p[0].y;
                    for (i = 0; i <= ady; i++) {
                        xe = p[0].x + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dy < 0) {
                            ye--;
                        } else {
                            ye++;
                        }
                    }
                } // end // end if (adx > ady) ... else
            } // end if (value instanceof float[]) {
            else if (value instanceof double[]) {
                double[] array = (double[]) value;
                if (adx > ady) {
                    delta = (double) dy / (double) adx;
                    profile = new double[adx + 1];
                    xe = p[0].x;
                    for (i = 0; i <= adx; i++) {
                        ye = p[0].y + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dx < 0) {
                            xe--;
                        } else {
                            xe++;
                        }
                    }
                } // end if (adx > ady)
                else {
                    delta = (double) dx / (double) ady;
                    profile = new double[ady + 1];
                    ye = p[0].y;
                    for (i = 0; i <= ady; i++) {
                        xe = p[0].x + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = array[(ye * dimX) + xe];
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dy < 0) {
                            ye--;
                        } else {
                            ye++;
                        }
                    }
                } // end // end if (adx > ady) ... else
            } // end if (value instanceof double[]) {
            else if (value instanceof Number[]) {
                Number[] array = (Number[]) value;
                if (adx > ady) {
                    delta = (double) dy / (double) adx;
                    profile = new double[adx + 1];
                    xe = p[0].x;
                    for (i = 0; i <= adx; i++) {
                        ye = p[0].y + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = NumberImageConverter.extractDouble(array[(ye * dimX) + xe]);
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dx < 0) {
                            xe--;
                        } else {
                            xe++;
                        }
                    }
                } // end if (adx > ady)
                else {
                    delta = (double) dx / (double) ady;
                    profile = new double[ady + 1];
                    ye = p[0].y;
                    for (i = 0; i <= ady; i++) {
                        xe = p[0].x + (int) (delta * i);
                        if (isInImage(xe, ye, d)) {
                            profile[i] = NumberImageConverter.extractDouble(array[(ye * dimX) + xe]);
                        } else {
                            profile[i] = Double.NaN;
                        }
                        if (dy < 0) {
                            ye--;
                        } else {
                            ye++;
                        }
                    }
                } // end // end if (adx > ady) ... else
            } // end if (value instanceof Number[]) {
            else {
                profile = null;
            }
        } // end if ((p != null) && (value != null))
        else {
            profile = null;
        } // end if ((p != null) && (value != null)) ... else
        return profile;
    }

    /**
     * Makes sure a pixel maximum is lower than a dimension maximum and greater than 0
     * 
     * @param max The pixel maximum
     * @param dim The dimension maximum
     * @return The updated pixel maximum
     */
    protected static int updateMax(int max, int dim) {
        max = Math.max(0, max);
        max = Math.min(max, dim);
        return max;
    }

    /**
     * Makes sure a pixel position is strictly lower than a dimension maximum and greater than 0
     * 
     * @param pos The pixel position
     * @param dim The dimension maximum
     * @return The updated pixel position
     */
    protected static int updatePosition(int pos, int dim) {
        pos = Math.max(0, pos);
        pos = Math.min(pos, dim);
        if (pos == dim) {
            pos--;
        }
        return pos;
    }

    protected static int updateLength(int pos, int length, int dim) {
        length = Math.max(0, length);
        if (length + pos > dim) {
            length += dim - (length + pos);
        }
        return length;
    }

    /**
     * Calculates whether some coordinates are inside a {@link Dimension}
     * 
     * @param xe The x coordinate
     * @param ye The y coordinate
     * @param d The {@link Dimension}
     * @return A <code>boolean</code> value
     */
    protected static boolean isInImage(int xe, int ye, Dimension d) {
        return (xe >= 0 && xe < d.width && ye >= 0 && ye < d.height);
    }

}
