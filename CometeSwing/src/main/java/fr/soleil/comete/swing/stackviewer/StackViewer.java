/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.stackviewer;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.event.PlayerEvent;
import fr.soleil.comete.definition.listener.IPlayerListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IComponentPlayer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.IPlayerAnimationBehavior;
import fr.soleil.comete.swing.AbstractPanel;
import fr.soleil.comete.swing.Player;
import fr.soleil.comete.swing.Slider;

/**
 * This abstract class gives some basic implementations for {@link IComponentPLayer}
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <C> The type of {@link IComponent} managed by this {@link StackViewer}
 */
public abstract class StackViewer<C extends IComponent> extends AbstractPanel
        implements IComponentPlayer<C>, IPlayerListener {

    private static final long serialVersionUID = -6582185201035166629L;

    private Player player;
    private int index;
    private C viewer;
    private final List<IPlayerListener> listeners;

    public StackViewer() {
        listeners = new ArrayList<IPlayerListener>();
        index = 0;
        player = null;
        viewer = null;
        initComponents();
    }

    @Override
    public void setCometeBackground(CometeColor c) {
        super.setCometeBackground(c);
        getViewer().setCometeBackground(c);
        getSlider().setCometeBackground(c);
        if (c != null) {
            getRecordBarPanel().setBackground(ColorTool.getColor(c));
        }
    }

    protected void initComponents() {
        add(getViewer(), BorderLayout.CENTER);
        add((IComponent) getRecordBarPanel(), BorderLayout.SOUTH);
    }

    @Override
    public void addPlayerListener(IPlayerListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void fireIndexChanged(PlayerEvent event) {
        ListIterator<IPlayerListener> iterator = listeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().indexChanged(event);
        }
    }

    @Override
    public void removePlayerListener(IPlayerListener listener) {
        if (listeners.contains(listener)) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    @Override
    public int getPeriodInMs() {
        int periodInMs = 0;
        if (getRecordBarPanel() != null) {
            periodInMs = getRecordBarPanel().getPeriodInMs();
        }
        return periodInMs;
    }

    @Override
    public void setPeriodInMs(int periodInMs) {
        if (getRecordBarPanel() != null) {
            getRecordBarPanel().setPeriodInMs(periodInMs);
        }
    }

    @Override
    public void gotoFirst() {
        getRecordBarPanel().gotoFirst();
    }

    @Override
    public void gotoLast() {
        getRecordBarPanel().gotoLast();
    }

    @Override
    public boolean isRepeatActivated() {
        return getRecordBarPanel().isRepeatActivated();
    }

    @Override
    public void next() {
        getRecordBarPanel().next();
    }

    @Override
    public void play() {
        getRecordBarPanel().play();
    }

    @Override
    public void previous() {
        getRecordBarPanel().previous();
    }

    @Override
    public void setRepeatActivated(boolean activated) {
        getRecordBarPanel().setRepeatActivated(activated);
    }

    @Override
    public void stop() {
        getRecordBarPanel().stop();
    }

    // get Slider
    @Override
    public Slider getSlider() {
        return getRecordBarPanel().getSlider();
    }

    // get RecordBarPanel
    public Player getRecordBarPanel() {
        if (player == null) {
            player = new Player();
            player.setVideoComponent(this);
            player.addPlayerListener(this);
        }
        return player;
    }

    @Override
    public void initSlider(int stackSize) {
        getSlider().setMaximum(stackSize);
    }

    @Override
    public int getIndex() {
        return index;
    }

    // update image or spectrum in viewer in fonction of the selected indice in the stack
    protected void updateData(int iSelectedIndice) {
        index = iSelectedIndice;
    }

    @Override
    public boolean isEditingData() {
        return (getRecordBarPanel().isEditingData() || getSlider().isEditingData());
    }

    @Override
    public void indexChanged(PlayerEvent event) {
        if ((event != null) && (event.getSource() == getRecordBarPanel())) {
            updateData(index);
            fireIndexChanged(new PlayerEvent(this));
        }
    }

    @Override
    public void setIndex(int index) {
        getRecordBarPanel().setIndex(index);
    }

    @Override
    public void setPlayerAnimationBehavior(IPlayerAnimationBehavior behavior) {
        player.setPlayerAnimationBehavior(behavior);
    }

    @Override
    public IPlayerAnimationBehavior getPlayerAnimationBehavior() {
        return player.getPlayerAnimationBehavior();
    }

    @Override
    public void setAnimationButtonVisible(boolean visible) {
        player.setAnimationButtonVisible(visible);
    }

    @Override
    public boolean isAnimationButtonVisible() {
        return player.isAnimationButtonVisible();
    }

    @Override
    public int getMaximum() {
        return player.getMaximum();
    }

    @Override
    public C getViewer() {
        if (viewer == null) {
            viewer = generateComponent();
        }
        return viewer;
    }

    @Override
    public void addSnapshotListener(ISnapshotListener listener) {
        player.addSnapshotListener(listener);
    }

    @Override
    public void removeSnapshotListener(ISnapshotListener listener) {
        player.removeSnapshotListener(listener);
    }

    @Override
    public String getSnapshotDirectory() {
        return player.getSnapshotDirectory();
    }

    @Override
    public void setSnapshotDirectory(String snapshotDirectory) {
        player.setSnapshotDirectory(snapshotDirectory);
    }

    @Override
    public String getSnapshotFile() {
        return player.getSnapshotFile();
    }

    protected abstract C generateComponent();

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return viewer == null ? true : viewer.shouldReceiveDataInDrawingThread();
    }

}
