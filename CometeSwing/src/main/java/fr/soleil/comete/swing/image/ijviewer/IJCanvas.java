/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.swing.SwingConstants;

import org.jdesktop.swingx.action.ActionManager;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.ImageViewer.MouseZoomMode;
import fr.soleil.comete.swing.image.ijviewer.components.ArrowStroke;
import fr.soleil.comete.swing.image.ijviewer.events.IRoiManagerListener;
import fr.soleil.comete.swing.image.ijviewer.events.RoiEvent;
import fr.soleil.comete.swing.image.util.ij.roi.RoiGenerator;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.icons.Icons;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.gui.ImageCanvas;
import ij.gui.ImageWindow;
import ij.gui.Roi;
import ij.gui.Toolbar;

/**
 * This class is responsible for image drawing, ROIs and indicators drawing.
 * It also manages the mouse and the keyboard.
 * 
 * We can create different kind of ROI (rectangle, ellipse, polygon, line, point).
 * 
 * We introduce a selection mode, based on the rectangular mode. It allows selecting ROIs by clicking it or by drawing a
 * rectangle and considering the ROIs it contains. CTRL key can be used to add to the current selection, or to switch
 * selected state of designated ROIs. Selection can be either single-click mode or double-click mode. Handled ROI is
 * null in selection mode.
 * 
 * We can choose to highlight the ROI under the mouse. When creating a ROI or drawing a selection rectangle, highlight
 * is temporary disabled.
 * 
 * Zoom is available with the wheel.
 * 
 * To modify a ROI with the keyboard (arrows to move the whole ROI, ALT+arrows to move the down right handle) it has to
 * be handled. It means the mouse must be over it before one can start using the keys.
 * 
 * 
 * - We've lost the multi-points roi (maybe because of the Roi.previousRoi that is null: point roi looks for it on
 * shift+click).
 * 
 * @author MAINGUY, VIGUIER, Rapha&euml;l GIRARDOT
 */
public class IJCanvas extends ImageCanvas implements SwingConstants {

    private static final long serialVersionUID = 8609256700023389931L;

    protected static final Cursor HAND_CURSOR = Icons.generateCursorFromIcon(ImageViewer.PAN_MODE_ICON,
            ImageViewer.MODE_PAN_ACTION);

    public static final Color DEFAULT_ROI_COLOR = Color.YELLOW;
    public static final Color DEFAULT_HOVERED_ROI_COLOR = Color.CYAN;
    public static final Color DEFAULT_SELECTED_ROI_COLOR = Color.BLACK;
    public static final Color DEFAULT_SELECTION_ROI_COLOR = Color.WHITE;
    public static final Color DEFAULT_ZOOM_ROI_COLOR = Color.BLUE;
    public static final Color DEFAULT_FOCUS_ROI_COLOR = new Color(255, 150, 20);
    public static final Color DEFAULT_LINE_PROFILE_ROI_COLOR = new Color(0, 180, 90);
    public static final Color DEFAULT_CALIBRATION_ROI_COLOR = Color.ORANGE;
    public static final Color DEFAULT_HISTOGRAM_ROI_COLOR = Color.RED;
    public static final Color DEFAULT_INNER_ROI_COLOR = new Color(200, 0, 0);
    public static final Color DEFAULT_HOVERED_INNER_ROI_COLOR = new Color(255, 150, 150);
    public static final Color DEFAULT_SELECTED_INNER_ROI_COLOR = new Color(255, 0, 0);
    public static final Color DEFAULT_OUTER_ROI_COLOR = new Color(0, 200, 0);
    public static final Color DEFAULT_HOVERED_OUTER_ROI_COLOR = new Color(150, 255, 150);
    public static final Color DEFAULT_SELECTED_OUTER_ROI_COLOR = new Color(0, 255, 0);

    public static final Color DEFAULT_ROI_NAME_COLOR = Color.PINK;
    public static final Color DEFAULT_ROI_SELECTION_INDEX_COLOR = Color.WHITE;

    public static final BeamCrossType DEFAULT_BEAM_CROSS_TYPE = BeamCrossType.FIXED;
    public static final Color DEFAULT_BEAM_COLOR = Color.WHITE;
    public static final int BEAM_CROSS_MIN_SIZE = 2;
    public static final int BEAM_CROSS_MAX_SIZE = 15;

    public static final Color DEFAULT_CROSSHAIR_COLOR = Color.WHITE;

    public static final BasicStroke DEFAULT_ROI_STROKE = new BasicStroke();
    public static final BasicStroke DEFAULT_HOVERED_ROI_STROKE = DEFAULT_ROI_STROKE;
    public static final BasicStroke DEFAULT_SELECTED_ROI_STROKE = new BasicStroke(2f);
    public static final BasicStroke DEFAULT_SELECTION_ROI_STROKE = new BasicStroke(1f, BasicStroke.CAP_SQUARE,
            BasicStroke.JOIN_MITER, 10f, new float[] { 3f }, 0f);
    public static final BasicStroke DEFAULT_ZOOM_ROI_STROKE = new BasicStroke(2f, BasicStroke.CAP_ROUND,
            BasicStroke.JOIN_ROUND, 10f);
    public static final BasicStroke DEFAULT_FOCUS_ROI_STROKE = new BasicStroke(2f, BasicStroke.CAP_BUTT,
            BasicStroke.JOIN_BEVEL, 10f, new float[] { 3f }, 0f);
    public static final BasicStroke DEFAULT_LINE_PROFILE_ROI_STROKE = new BasicStroke(1f, BasicStroke.CAP_BUTT,
            BasicStroke.JOIN_ROUND, 10f);
    public static final BasicStroke DEFAULT_DUAL_PROFILE_ROI_STROKE = new BasicStroke(2f, BasicStroke.CAP_BUTT,
            BasicStroke.JOIN_ROUND, 10f);
    public static final BasicStroke DEFAULT_CALIBRATION_ROI_STROKE = new BasicStroke(1f, BasicStroke.CAP_BUTT,
            BasicStroke.JOIN_ROUND, 10f);

    public static final String MAGNIFICATION_PROPERTY = "magnification";

    protected static final String PAINT = "Paint";
    protected static final String PAINT_NOT_ALLOWED = "Component rendering disabled";

    public static enum BeamCrossType {
        FIXED, FULL
    }

    /**
     * This is the ImageJ application instance it is also used to manage events we don't care about, typically the ones
     * we don't override.
     */
    protected ImageJ hiddenImageJApp;

    protected boolean highlightHoveredRoi;

    protected MouseZoomMode mouseZoomMode;

    protected boolean drawSelectedRoiIndex;

    protected boolean drawRoiName;

    protected boolean roiNameColorAsRoi;

    // standard ROI
    protected Stroke roiStroke;
    protected Color roiColor;
    protected Color roiNameColor;

    // hovered ROI
    protected Roi hoveredRoi;
    protected Stroke hoveredRoiStroke;
    protected Color hoveredRoiColor;

    // selected ROI
    protected Stroke selectedRoiStroke;
    protected Color selectedRoiColor;
    protected Color roiSelectionIndexColor;

    // selection ROI
    protected Stroke selectionRoiStroke;
    protected Color selectionRoiColor;

    protected Stroke zoomIndicatorStroke;
    protected Color zoomIndicatorColor;

    // Inner and Outer ROI Colors
    protected Color innerRoiColor;
    protected Color hoveredInnerRoiColor;
    protected Color selectedInnerRoiColor;
    protected Color outerRoiColor;
    protected Color hoveredOuterRoiColor;
    protected Color selectedOuterRoiColor;

    protected boolean useShiftToKeepSelection;

    // zoom ROI
    protected Stroke zoomRoiStroke;
    protected Color zoomRoiColor;

    // focus ROI
    protected Stroke focusRoiStroke;
    protected Color focusRoiColor;

    // line profile ROI
    protected Stroke lineProfileRoiStroke;
    protected Stroke lineProfileSelectedRoiStroke;
    protected Color lineProfileRoiColor;

    // Calibration ROI
    protected Stroke calibrationRoiStroke;
    protected Stroke calibrationSelectedRoiStroke;
    protected Color calibrationRoiColor;

    // Beam position, defined as a real full image pixel position
    protected boolean drawBeamPosition;
    protected Point2D.Double beamPoint;
    protected Color beamColor;
    protected BasicStroke beamStroke;
    protected BeamCrossType beamCrossType;

    // crosshair
    protected Point crosshairPoint;
    protected Color crosshairColor;

    // roi drawing memory
    protected final Map<Roi, Color> strokeColorMap;
    protected final Map<Roi, Float> strokeWidthMap;

    protected Dimension maxDrawingSize;
    private WeakReference<ImageViewer> imageViewerRef;
    private boolean paintAllowed;
    private volatile int[] roiNameAlignment;

    /**
     * @param imgp
     */
    public IJCanvas(IJRoiManager imgp, ImageViewer imageViewer) {
        super(imgp);
        strokeColorMap = new WeakHashMap<>();
        strokeWidthMap = new WeakHashMap<>();
        highlightHoveredRoi = true;
        mouseZoomMode = MouseZoomMode.NONE;
        drawSelectedRoiIndex = false;
        drawRoiName = true;
        roiNameColorAsRoi = true;
        roiStroke = DEFAULT_ROI_STROKE;
        roiColor = DEFAULT_ROI_COLOR;
        roiNameColor = DEFAULT_ROI_NAME_COLOR;
        hoveredRoiStroke = DEFAULT_HOVERED_ROI_STROKE;
        hoveredRoiColor = DEFAULT_HOVERED_ROI_COLOR;
        selectedRoiStroke = DEFAULT_SELECTED_ROI_STROKE;
        selectedRoiColor = DEFAULT_SELECTED_ROI_COLOR;
        roiSelectionIndexColor = DEFAULT_ROI_SELECTION_INDEX_COLOR;
        selectionRoiStroke = DEFAULT_SELECTION_ROI_STROKE;
        selectionRoiColor = DEFAULT_SELECTION_ROI_COLOR;
        zoomIndicatorStroke = new BasicStroke();
        zoomIndicatorColor = new Color(128, 128, 255);
        innerRoiColor = DEFAULT_INNER_ROI_COLOR;
        hoveredInnerRoiColor = DEFAULT_HOVERED_INNER_ROI_COLOR;
        selectedInnerRoiColor = DEFAULT_SELECTED_INNER_ROI_COLOR;
        outerRoiColor = DEFAULT_OUTER_ROI_COLOR;
        hoveredOuterRoiColor = DEFAULT_HOVERED_OUTER_ROI_COLOR;
        selectedOuterRoiColor = DEFAULT_SELECTED_OUTER_ROI_COLOR;
        zoomRoiStroke = DEFAULT_ZOOM_ROI_STROKE;
        zoomRoiColor = DEFAULT_ZOOM_ROI_COLOR;
        focusRoiStroke = DEFAULT_FOCUS_ROI_STROKE;
        focusRoiColor = DEFAULT_FOCUS_ROI_COLOR;
        lineProfileRoiStroke = new ArrowStroke(DEFAULT_LINE_PROFILE_ROI_STROKE, ArrowStroke.END, 12, Math.PI / 5);
        lineProfileSelectedRoiStroke = new ArrowStroke(DEFAULT_SELECTED_ROI_STROKE, ArrowStroke.END, 12, Math.PI / 5);
        lineProfileRoiColor = DEFAULT_LINE_PROFILE_ROI_COLOR;
        calibrationRoiStroke = new ArrowStroke(DEFAULT_CALIBRATION_ROI_STROKE, ArrowStroke.BOTH, 8, Math.PI / 2);
        calibrationSelectedRoiStroke = new ArrowStroke(DEFAULT_SELECTED_ROI_STROKE, ArrowStroke.BOTH, 8, Math.PI / 2);
        calibrationRoiColor = DEFAULT_CALIBRATION_ROI_COLOR;
        drawBeamPosition = false;
        beamPoint = null;
        beamColor = DEFAULT_BEAM_COLOR;
        beamStroke = new BasicStroke();
        beamCrossType = DEFAULT_BEAM_CROSS_TYPE;
        crosshairColor = DEFAULT_CROSSHAIR_COLOR;
        maxDrawingSize = null;
        paintAllowed = true;
        if (imageViewer == null) {
            imageViewerRef = null;
        } else {
            imageViewerRef = new WeakReference<ImageViewer>(imageViewer);
        }
        roiNameAlignment = new int[] { LEFT, TOP };
        // otherwise, ROI point changes the font in draw()!
        Prefs.noPointLabels = true;
        // we don't care about the IJ's popup menu
        disablePopupMenu(true);
    }

    public ImageJ getHiddenIJ() {
        return hiddenImageJApp;
    }

    /**
     * Returns the roi name alignment
     * 
     * @return An <code>int[]</code>of length 2: <code>{horizontalAlignment, verticalAlignment}</code>
     * @see #setRoiNameAlignment(int, int)
     */
    public int[] getRoiNameAlignment() {
        return roiNameAlignment;
    }

    /**
     * Changes the roi name alignment
     * 
     * @param horizontalAlignment The horizontal alignment. Can be one of these values:
     *            <ul>
     *            <li>{@link SwingConstants#LEFT}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#RIGHT}</li>
     *            </ul>
     * @param verticalAlignment The vertical alignment. Can be one of these values:
     *            <ul>
     *            <li>{@link SwingConstants#TOP}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#BOTTOM}</li>
     *            </ul>
     */
    public void setRoiNameAlignment(int horizontalAlignment, int verticalAlignment) {
        roiNameAlignment = new int[] { horizontalAlignment, verticalAlignment };
        repaint();
    }

    /**
     * Returns whether paint() is allowed.
     * 
     * @return A <code>boolean</code> value.
     */
    public boolean isPaintAllowed() {
        return paintAllowed;
    }

    /**
     * Sets whether paint() is allowed.
     * 
     * @param paintAllowed Whether paint() is allowed.
     */
    public void setPaintAllowed(boolean paintAllowed) {
        if (this.paintAllowed != paintAllowed) {
            this.paintAllowed = paintAllowed;
            repaint();
        }
    }

    /**
     * Returns the {@link ImageViewer} associated with this {@link IJCanvas}.cThis is for advance use only in imagej
     * plugin development.
     * 
     * @return An {@link ImageViewer}.
     */
    public ImageViewer getImageViewer() {
        return ObjectUtils.recoverObject(imageViewerRef);
    }

    public void setHiddenIJ(ImageJ hiddenIJ) {
        if (hiddenIJ != null) {
            hiddenImageJApp = hiddenIJ;
        }
    }

    public IJRoiManager getImagePlus() {
        return (IJRoiManager) imp;
    }

    /**
     * Convenience method
     * 
     * @return true if current mode is classic, false otherwise
     */
    public boolean isClassicRoiMode() {
        return IJRoiManager.isClassicRoiMode();
    }

    public Cursor getDefaultCursor() {
        return defaultCursor;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension result;
        if (srcRect == null) {
            result = new Dimension(0, 0);
        } else {
            result = new Dimension((int) (srcRect.width * magnification), (int) (srcRect.height * magnification));
        }
        return result;
    }

    @Override
    public void paint(Graphics g) {
        if (g instanceof Graphics2D) {
            ImagePlus imp = this.imp;
            Graphics2D g2d = (Graphics2D) g;
            try {
                if (imageUpdated && (imp != null)) {
                    imageUpdated = false;
                    imp.updateImage();
                }
                Image img;
                try {
                    img = (imp == null ? null : imp.getImage());
                } catch (Exception e) {
                    // An exception is thrown when dimensions are not set yet: ignore it
                    img = null;
                }
                if (paintAllowed) {
                    if ((srcRect != null) && (img != null)) {
                        g.drawImage(img, 0, 0, (int) (srcRect.width * magnification),
                                (int) (srcRect.height * magnification), srcRect.x, srcRect.y, srcRect.x + srcRect.width,
                                srcRect.y + srcRect.height, null);

                    }
                    drawUnderRois(g2d);
                    IJRoiManager manager = getImagePlus();
                    if (manager != null) {
                        for (Roi roi : manager.getAllRois()) {
                            drawRoi(g2d, roi);
                        }
                    }
                    drawOverRois(g2d);
                } else {
                    Color formerColor = g.getColor();
                    g.setColor(Color.RED);
                    int width = (int) (srcRect.width * magnification);
                    int height = (int) (srcRect.height * magnification);
                    g.drawLine(0, 0, width, height);
                    g.drawLine(0, height, width, 0);
                    g.setColor(Color.BLACK);
                    Rectangle2D textBounds = CometeUtils.getStringBoundsForComponent(PAINT_NOT_ALLOWED, this);
                    int x = (int) ((srcRect.width * magnification - textBounds.getWidth()) / 2.0);
                    int y = (int) ((srcRect.height * magnification - textBounds.getHeight()) / 2.0);
                    Font formerFont = g.getFont();
                    g.setFont(getFont());
                    g.drawString(PAINT_NOT_ALLOWED, x, y);
                    if (formerColor != null) {
                        g.setColor(formerColor);
                    }
                    if (formerFont != null) {
                        g.setFont(formerFont);
                    }
                }
                drawMyZoomIndicator(g2d);
            } catch (OutOfMemoryError e) {
                IJ.outOfMemory(PAINT);
            }
        }
    }

    protected void drawUnderRois(Graphics2D g2d) {
        drawCrosshair(g2d);
    }

    protected void drawOverRois(Graphics2D g2d) {
        drawBeamPosition(g2d);
    }

    /**
     * Draws one ROI.
     * 
     * @param g The {@link Graphics2D} to draw on.
     * @param roi The {@link Roi}.
     */
    protected void drawRoi(Graphics2D g2d, Roi roi) {
        List<Roi> selectedRois = getImagePlus().getSelectedRois();
        int selectedIndex = selectedRois.indexOf(roi);
        boolean selected = (selectedIndex > -1);
        boolean hovered = (roi == hoveredRoi);
        // boolean handledRoi = (roi == getImagePlus().getRoi());

        setRoiColorAndStroke(g2d, roi, selected, hovered);

        roi.draw(g2d);

        drawRoiSelectionIndex(g2d, roi, selected, selectedIndex);

        drawRoiName(g2d, roi, hovered);
    }

    /**
     * @param g2d
     * @param roi
     * @param selected
     * @param hovered
     */
    protected void setRoiColorAndStroke(Graphics2D g2d, Roi roi, boolean selected, boolean hovered) {
        // special ROIs are specific
        RoiGenerator generator = getImagePlus().getRoiGenerator(roi);
        Color color = null;
        Stroke stroke = null;
        if (roi.getState() == Roi.CONSTRUCTING) {
            if (generator != null) {
                color = generator.getRoiColor();
                stroke = generator.getRoiStroke();
            }
            if (color == null) {
                color = roiColor;
            }
            if (stroke == null) {
                stroke = roiStroke;
            }
        } else {
            Float savedStrokeWidth = strokeWidthMap.get(roi);
            Color savedStrokeColor = strokeColorMap.get(roi);
            // set the ROI color
            // hover comes first so that all hovered ROIs including selected ones are highlighted
            if (hovered && highlightHoveredRoi) {
                if (generator != null) {
                    color = generator.getHoveredRoiColor();
                    stroke = generator.getHoveredRoiStroke();
                }
                if (color == null) {
                    if (getImagePlus() == null) {
                        color = hoveredRoiColor;
                    } else if (getImagePlus().isInner(roi)) {
                        color = hoveredInnerRoiColor;
                    } else if (getImagePlus().isOuter(roi)) {
                        color = hoveredOuterRoiColor;
                    } else {
                        color = hoveredRoiColor;
                    }
                }
                if (stroke == null) {
                    stroke = selected ? selectedRoiStroke : roiStroke;
                }
            } else if (selected) {
                if (generator != null) {
                    color = generator.getSelectedRoiColor();
                    stroke = generator.getSelectedRoiStroke();
                }
                if (color == null) {
                    if (getImagePlus() == null) {
                        color = selectedRoiColor;
                    } else if (getImagePlus().isInner(roi)) {
                        color = selectedInnerRoiColor;
                    } else if (getImagePlus().isOuter(roi)) {
                        color = selectedOuterRoiColor;
                    } else {
                        color = selectedRoiColor;
                    }
                }
                if (stroke == null) {
                    stroke = selectedRoiStroke;
                }
            } else {
                if (generator != null) {
                    color = generator.getRoiColor();
                    stroke = generator.getRoiStroke();
                }
                if (color == null) {
                    if (getImagePlus() == null) {
                        color = roiColor;
                    } else if (getImagePlus().isInner(roi)) {
                        color = innerRoiColor;
                    } else if (getImagePlus().isOuter(roi)) {
                        color = outerRoiColor;
                    } else {
                        color = roiColor;
                    }
                }
                if (stroke == null) {
                    stroke = roiStroke;
                }
            }
            if ((hovered && highlightHoveredRoi) || selected) {
                float strokeWidth = savedStrokeWidth == null ? roi.getStrokeWidth() : savedStrokeWidth.floatValue();
                Color strokeColor = roi.getStrokeColor();
                if (strokeWidth != 1) {
                    strokeWidthMap.put(roi, Float.valueOf(strokeWidth));
                    roi.setStrokeWidth(selected ? strokeWidth * 2 : strokeWidth);
                }
                if (strokeColor != null) {
                    strokeColorMap.put(roi, strokeColor);
                    roi.setStrokeColor(null);
                }
            } else {
                if (savedStrokeWidth != null) {
                    roi.setStrokeWidth(savedStrokeWidth.floatValue());
                    strokeWidthMap.remove(roi);
                }
                if (savedStrokeColor != null) {
                    roi.setStrokeColor(savedStrokeColor);
                    strokeColorMap.remove(roi);
                }
            }

        } // end if (roi.getState() == Roi.CONSTRUCTING) ... else
        Roi.setColor(color);
        g2d.setStroke(stroke);
    }

    /**
     * @param g2d
     * @param roi
     * @param selected
     * @param selectedIndex
     */
    protected void drawRoiSelectionIndex(Graphics2D g2d, Roi roi, boolean selected, int selectedIndex) {
        List<Roi> selectedRois = getImagePlus().getSelectedRois();
        boolean manySelected = (selectedRois.size() > 1);

        // displays a selection index on selected ROIs when there is
        // more than one selected
        if (selected && manySelected && drawSelectedRoiIndex) {
            // center the index in the ROI's bounding rectangle
            Rectangle bounds = roi.getBounds();
            int centerX = screenX(bounds.x + bounds.width / 2);
            int centerY = screenY(bounds.y + bounds.height / 2);

            String selectionIndex = Integer.toString(selectedIndex + 1);
            int length = g2d.getFontMetrics().stringWidth(selectionIndex);

            int posX = centerX - length / 2;
            int posY = centerY + g2d.getFontMetrics().getAscent() / 2;

            g2d.setColor(roiSelectionIndexColor);
            g2d.drawString(selectionIndex, posX, posY);
        }
    }

    /**
     * @param g2d
     * @param roi
     * @param hovered
     */
    protected void drawRoiName(Graphics2D g2d, Roi roi, boolean hovered) {
        // display the ROI's name in the top left corner of the ROI's
        // bounding rectangle
        if (drawRoiName) {
            if (roiNameColorAsRoi) {
                g2d.setColor(Roi.getColor());// same color as ROI
            } else {
                // change name color for the highlighted ROI
                if (hovered && highlightHoveredRoi) {
                    g2d.setColor(hoveredRoiColor);
                } else {
                    g2d.setColor(roiNameColor);
                }
            }
            Rectangle bounds = roi.getBounds();
            String name = roi.getName();
            if (name != null) {
                FontRenderContext frc = g2d.getFontRenderContext();
                Rectangle2D textBounds = g2d.getFont().getStringBounds(name, frc);
                int x, y;
                switch (roiNameAlignment[0]) {
                    case RIGHT:
                        x = screenX(bounds.x + bounds.width) - 1 - (int) Math.round(textBounds.getWidth());
                        break;
                    case CENTER:
                        x = screenX(bounds.x) + (int) Math.round((screenX(bounds.width) - textBounds.getWidth()) / 2)
                                + 1;
                        break;
                    default:
                        // LEFT
                        x = screenX(bounds.x) + 1;
                        break;
                }
                switch (roiNameAlignment[1]) {
                    case BOTTOM:
                        y = screenY(bounds.y + bounds.height) + g2d.getFontMetrics().getAscent()
                                - (int) Math.round(textBounds.getHeight());
                        break;
                    case CENTER:
                        y = screenY(bounds.y) + (int) Math.round((screenY(bounds.height) - textBounds.getHeight()) / 2)
                                + g2d.getFontMetrics().getAscent();
                        break;
                    default:
                        // TOP
                        y = screenY(bounds.y) + g2d.getFontMetrics().getAscent();
                        break;
                }
                g2d.drawString(name, x, y);
            }
        }
    }

    /**
     * Displays a zoom indicator in top left corner. We cannot override the original one, so let's duplicate it here
     * with some enhancement.
     * 
     * @param g The {@link Graphics2D} in which to draw zoom indicator.
     */
    protected void drawMyZoomIndicator(Graphics2D g2d) {
        if ((srcRect != null) && (srcRect.width < imageWidth || srcRect.height < imageHeight)) {
            final int indicatorWidth = 64;
            double aspectRatio = (double) imageHeight / imageWidth;
            // compute the full image rectangle
            int x1 = 10;
            int y1 = 10;
            int w1 = indicatorWidth;
            if (aspectRatio > 1.0) {
                w1 = (int) (w1 / aspectRatio);
            }
            int h1 = (int) (w1 * aspectRatio);
            if (w1 < 4) {
                w1 = 4;
            }
            if (h1 < 4) {
                h1 = 4;
            }
            // compute the window rectangle
            int x2 = (int) Math.round(w1 * ((double) srcRect.x / imageWidth));
            int y2 = (int) Math.round(h1 * ((double) srcRect.y / imageHeight));
            int w2 = (int) Math.round(w1 * ((double) srcRect.width / imageWidth));
            int h2 = (int) Math.round(h1 * ((double) srcRect.height / imageHeight));
            if (w2 < 1) {
                w2 = 1;
            }
            if (h2 < 1) {
                h2 = 1;
            }
            g2d.setColor(zoomIndicatorColor);
            g2d.setStroke(zoomIndicatorStroke);
            g2d.drawRect(x1, y1, w1, h1);
            if (w2 * h2 <= 200 || w2 < 10 || h2 < 10) {
                g2d.fillRect(x1 + x2, y1 + y2, w2 + 1, h2 + 1);
            } else {
                g2d.drawRect(x1 + x2, y1 + y2, w2, h2);
            }
        }
    }

    protected void drawBeamPosition(Graphics2D g2d) {
        if (drawBeamPosition && (beamPoint != null) && (srcRect != null)) {
            int x = screenXD(beamPoint.x);
            int y = screenYD(beamPoint.y);

            g2d.setStroke(beamStroke);
            g2d.setColor(beamColor);

            switch (beamCrossType) {
                case FIXED:
                    // cross size depends on zoom level, with a min and a max value
                    int crossSize = Math.min((int) Math.round(magnification * BEAM_CROSS_MAX_SIZE),
                            BEAM_CROSS_MAX_SIZE);
                    crossSize = Math.max(crossSize, BEAM_CROSS_MIN_SIZE);
                    // vertical
                    g2d.drawLine(x, y - crossSize, x, y + crossSize);
                    // horizontal
                    g2d.drawLine(x - crossSize, y, x + crossSize, y);
                    break;

                case FULL:
                    // vertical
                    g2d.drawLine(x, 0, x, (int) (srcRect.height * magnification));
                    // horizontal
                    g2d.drawLine(0, y, (int) (srcRect.width * magnification), y);
                    break;
            }
        }
    }

    protected void drawCrosshair(Graphics2D g2d) {
        if ((srcRect != null) && (crosshairPoint != null)) {
            int x = screenX(crosshairPoint.x);
            int y = screenY(crosshairPoint.y);
            int pixelSize = (int) Math.max(1, magnification);

            g2d.setColor(crosshairColor);
            // vertical line
            g2d.fillRect(x, 0, pixelSize, (int) (srcRect.height * magnification));
            // horizontal line
            g2d.fillRect(0, y, (int) (srcRect.width * magnification), pixelSize);
        }
    }

    /**
     * Notifies a {@link RoiGenerator} for some changes in a {@link Roi}.
     * 
     * @param roi The {@link Roi}.
     * @param generator The {@link RoiGenerator}.
     */
    protected void roiChanged(Roi roi, RoiGenerator generator) {
        if ((roi != null) && (generator instanceof IRoiManagerListener)) {
            ((IRoiManagerListener) generator)
                    .roiChanged(new RoiEvent(getImagePlus(), roi, getImagePlus().isSelectedRoi(roi)));
        }
    }

    /**
     * Notifies dedicated listeners for some changes in a {@link Roi}.
     * 
     * @param roi The {@link Roi}.
     */
    public void roiChanged(Roi roi) {
        roiChanged(roi, getImagePlus().getRoiGenerator(roi));
    }

    /**
     * IJ starts creating a new ROI on mouse pressed. It becomes the new handled ROI.
     * 
     * @see ij.gui.ImageCanvas#mousePressed(java.awt.event.MouseEvent)
     */
    @Override
    public void mousePressed(MouseEvent e) {
        RoiGenerator generator = getImagePlus().getCurrentRoiGenerator();
        if (generator != null) {
            e = generator.adaptMousePressedEvent(e);
        }
        if (e != null) {
            // //If Alt is down, stop handling the hovered ROI so that we can start
            // drawing another ROI
            // on top of another
            // int toolID = Toolbar.getToolId();
            // if (e.isAltDown()
            // && (toolID == Toolbar.RECTANGLE || toolID == Toolbar.OVAL
            // || toolID == Toolbar.POLYGON || toolID == Toolbar.FREEROI
            // || toolID == Toolbar.LINE || toolID == Toolbar.POINT || toolID ==
            // Toolbar.WAND)) {
            // getImagePlus().clearHandledRoi();
            // }

            // IJ changes ROI mod state on shift or alt down. We want it disabled
            // not to kill the
            // clicked ROI!
            int modifiers = e.getModifiers();
            modifiers &= ~InputEvent.SHIFT_MASK;
            modifiers &= ~InputEvent.ALT_MASK;
            MouseEvent newEvent = new MouseEvent((Component) e.getSource(), e.getID(), e.getWhen(), modifiers, e.getX(),
                    e.getY(), e.getClickCount(), e.isPopupTrigger());

            if (Toolbar.getToolId() == Toolbar.WAND) {
                // IJ workaround to avoid deleting the whole image traced ROI if it
                // exists
                Roi roi = getImagePlus().getRoi();
                if (roi != null) {
                    Rectangle r = roi.getBounds();
                    if (r.width == imageWidth && r.height == imageHeight) {
                        handleRoiMouseDown(e);
                    } else {
                        super.mousePressed(newEvent);
                    }
                } else {
                    super.mousePressed(newEvent);
                }
            } else {
                super.mousePressed(newEvent);
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        RoiGenerator generator = getImagePlus().getCurrentRoiGenerator();
        if (generator != null) {
            e = generator.adaptMouseReleasedEvent(e);
        }
        if (e != null) {
            // TODO manage special Roi cases
            if (Toolbar.getToolId() != Toolbar.HAND) {
                super.mouseReleased(e);

                // IJ workaround to update polygon infos when finishing
                // (PolygonRoi.finishPolygon)
                Roi roi = getImagePlus().getRoi();
                if ((roi != null) && (roi.getType() == Roi.POLYGON || roi.getType() == Roi.FREEROI)) {
                    getImagePlus().fireRoiChanged();
                }

                // IJ bug workaround: IJ should update the cursor
                // IJ bug: setCursor is not always the good one, which appears after a move
                int sx = e.getX();
                int sy = e.getY();
                int ox = offScreenX(sx);
                int oy = offScreenY(sy);
                setCursor(sx, sy, ox, oy);

                // let's highlight the ROI we may be over now (after a drag)
                highlightHoveredRoi(e);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        RoiGenerator generator = getImagePlus().getCurrentRoiGenerator();
        if (generator != null) {
            e = generator.adaptMouseClickedEvent(e);
        }
        if (e != null) {
            super.mouseClicked(e);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        RoiGenerator generator = getImagePlus().getCurrentRoiGenerator();
        if (generator != null) {
            e = generator.adaptMouseMovedEvent(e);
        }
        if ((e != null) && (Toolbar.getToolId() != Toolbar.HAND)) {
            super.mouseMoved(e);// update cursor, draw polygon rubber line...
            handleHoveredRoi(e.getX(), e.getY());
            // don't hover ROI in creation mode
            highlightHoveredRoi(e);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        RoiGenerator generator = getImagePlus().getCurrentRoiGenerator();
        if (generator != null) {
            e = generator.adaptMouseDraggedEvent(e);
        }
        if (e != null) {
            super.mouseDragged(e);
            // we don't fire if it concerns our special ROIs
            if ((generator == null) || (!generator.isSilentRoi())) {
                getImagePlus().fireRoiChanged();
            } else {
                roiChanged(getImagePlus().getRoi(), generator);
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        super.mouseEntered(e);
        int sx = e.getX();
        int sy = e.getY();
        int ox = offScreenX(sx);
        int oy = offScreenY(sy);
        setCursor(sx, sy, ox, oy);
        if (isClassicRoiMode()) {
            handleHoveredRoi(sx, sy);
        }
        highlightHoveredRoi(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        super.mouseExited(e);
        if (isClassicRoiMode()) {
            if (getImagePlus().getRoi() != null && getImagePlus().getRoi().getState() != Roi.CONSTRUCTING) {
                getImagePlus().clearHandledRoi();
                clearHighlightedRoi();
            }
        }
    }

    // Called by ImageViewer
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (srcRect != null) {
            int sx = e.getX();
            int sy = e.getY();
            if (mouseZoomMode == MouseZoomMode.NONE) {
                sx = screenXD(imageWidth / 2.0);
                sy = screenYD(imageHeight / 2.0);
            }
            double xd = offScreenXD(sx), yd = offScreenYD(sy);

            if (imp.getWindow() != null) {
                if (e.getWheelRotation() < 0) {
                    zoomIn(sx, sy);
                } else {
                    zoomOut(sx, sy);
                }
            }

            boolean changed = false;
            int dx = 0, dy = 0;
            // try to relocate srcRect according to what user expected (JAVAAPI-292)
            switch (mouseZoomMode) {
                case PIXEL:
                    dx = (int) Math.round(xd - offScreenXD(sx));
                    dy = (int) Math.round(yd - offScreenYD(sy));
                    break;
                case CENTERED:
                    int x = (int) xd;
                    int y = (int) yd;
                    dx = x - (srcRect.x + srcRect.width / 2);
                    dy = y - (srcRect.y + srcRect.height / 2);
                    break;
                case NONE:
                    dx = (imageWidth / 2) - (srcRect.x + srcRect.width / 2);
                    dy = (imageHeight / 2) - (srcRect.y + srcRect.height / 2);
                    break;
            }
            if (dx != 0) {
                srcRect.x += dx;
                changed = true;
            }
            if (dy != 0) {
                srcRect.y += dy;
                changed = true;
            }
            if (adaptRectangleLimit(srcRect) || changed) {
                ImagePlus imp = getImagePlus();
                if (imp != null) {
                    ImageWindow window = imp.getWindow();
                    if (window != null) {
                        window.pack();
                    }
                }
            }

            if (Toolbar.getToolId() != Toolbar.HAND) {
                // after zooming mouse may be over a ROI
                if (isClassicRoiMode()) {
                    handleHoveredRoi(sx, sy);

                    // sets the cursor according to the mouse position
                    int ox = offScreenX(sx);
                    int oy = offScreenY(sy);
                    setCursor(sx, sy, ox, oy);
                }
                highlightHoveredRoi(sx, sy);
            }
        }
    }

    // Adapts a rectangle to be inside image bounds
    protected boolean adaptRectangleLimit(Rectangle zone) {
        boolean changed = false;
        if (zone != null) {
            int xTooMuch = (zone.x + zone.width) - imageWidth;
            int yTooMuch = (zone.y + zone.height) - imageHeight;
            if (xTooMuch > 0) {
                zone.x -= xTooMuch;
                changed = true;
            }
            if (yTooMuch > 0) {
                zone.y -= yTooMuch;
                changed = true;
            }
            if (zone.x < 0) {
                zone.x = 0;
                changed = true;
            }
            if (zone.y < 0) {
                zone.y = 0;
                changed = true;
            }
        }
        return changed;
    }

    // Called by ImageViewer
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ALT:
                // TODO see if we can use some boolean variable to disable handled ROI and begin drawing over an
                // existing ROI
            case KeyEvent.VK_EQUALS:// initially zoom in cmd
            case KeyEvent.VK_6:// initially zoom out cmd
            case KeyEvent.VK_MINUS:// defined as for zoom out, but seems unused
            case KeyEvent.VK_BACK_SPACE:// initially clear cmd
            case KeyEvent.VK_A:// initially "Select All" macro
                // nop, disable some IJ default keys
                break;
            case KeyEvent.VK_ADD:
                zoomIn();
                break;
            case KeyEvent.VK_SUBTRACT:
                zoomOut();
                break;
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_UP:
            case KeyEvent.VK_DOWN:
                Roi roi = getImagePlus().getRoi();
                // disable zoom in and out functionality on up and down key
                if (roi != null) {
                    hiddenImageJApp.keyPressed(e);
                    RoiGenerator generator = getImagePlus().getRoiGenerator(roi);
                    boolean warned;
                    if ((generator == null) || (!generator.isSilentRoi())) {
                        warned = true;
                        getImagePlus().fireRoiChanged();
                    } else {
                        warned = false;
                    }
                    if (generator instanceof IRoiManagerListener) {
                        IRoiManagerListener listener = (IRoiManagerListener) generator;
                        if ((!warned) || (!getImagePlus().hasRoiManagerListener(listener))) {
                            roiChanged(roi, generator);
                        }
                    }
                }
                break;
            default:
                hiddenImageJApp.keyPressed(e);
        }
    }

    // Called by ImageViewer
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ALT:
                // TODO see if we can use some boolean variable to disable handled ROI and begin drawing over an
                // existing ROI
                break;
            default:
                hiddenImageJApp.keyReleased(e);
        }
    }

    // Called by ImageViewer
    public void keyTyped(KeyEvent e) {
        hiddenImageJApp.keyTyped(e);
    }

    /**
     * Looks for the ROI at the given coordinates. If there is more than one, we look for the "nearest".
     * 
     * @param sx X-coordinate (screen).
     * @param sy Y-coordinate (screen).
     * @param ox X-coordinate (image).
     * @param oy Y-coordinate (image).
     * @return the designated ROI if any, null otherwise.
     */
    protected Roi getRoiAtPoint(int sx, int sy, int ox, int oy) {
        Roi closestRoi = null;
        int minDistance = Integer.MAX_VALUE;
        for (Roi currentRoi : getImagePlus().getAllRois()) {
            // if we are on a handle, this is the one (careful: screen coordinates)
            if (currentRoi.isHandle(sx, sy) >= 0) {
                closestRoi = currentRoi;
                break;
            } else if (currentRoi.contains(ox, oy)) {
                Rectangle bounds = currentRoi.getBounds();
                // Look for the closest ROI:
                // This is the ROI whose bounds sides distance to point are the smallest
                int distanceX = Math.min(Math.abs(bounds.x - ox), Math.abs(bounds.x + bounds.width - ox));
                int distanceY = Math.min(Math.abs(bounds.y - oy), Math.abs(bounds.y + bounds.width - oy));
                int distance = Math.min(distanceX, distanceY);
                if (distance < minDistance) {
                    minDistance = distance;
                    closestRoi = currentRoi;
                }
            }
        }
        return closestRoi;
    }

    /**
     * Looks for the ROI at the given coordinates. If there is more than one, we look for the "nearest".
     * 
     * @param sx X-coordinate (screen).
     * @param sy Y-coordinate (screen).
     * @return the designated ROI if any, null otherwise.
     */
    public Roi getRoiAtPoint(int sx, int sy) {
        return getRoiAtPoint(sx, sy, offScreenX(sx), offScreenY(sy));
    }

    /**
     * Sets the ROI which can be moved/resized by mouse, corresponding to given coordinates.
     * 
     * @param sx X-coordinate (screen).
     * @param sy Y-coordinate (screen).
     */
    protected void handleHoveredRoi(int sx, int sy) {
        // we look for the current roi according to the position of the
        // cursor
        Roi roi = getImagePlus().getRoi();
        // roi is null when none is handled
        // we don't want to change the handled roi when constructing a new one
        if (roi == null || roi.getState() != Roi.CONSTRUCTING) {
            Roi closestRoi = getRoiAtPoint(sx, sy);
            if (!getImagePlus().shouldHaveNoHandledRoi()) {
                getImagePlus().setHandledRoi(closestRoi);
            }
        }
    }

    /**
     * Highlights a {@link Roi} at given mouse position.
     * 
     * @param e The {@link MouseEvent} that knows the mouse position.
     */
    public void highlightHoveredRoi(MouseEvent e) {
        Roi roi = getImagePlus().getRoi();
        if (roi == null || roi.getState() != Roi.CONSTRUCTING) {
            int sx = e.getX();
            int sy = e.getY();
            highlightHoveredRoi(sx, sy);
        }
    }

    /**
     * Highlights a {@link Roi} at given screen position.
     * 
     * @param sx x position on screen.
     * @param sy y position on screen.
     */
    public void highlightHoveredRoi(int sx, int sy) {
        if (highlightHoveredRoi) {
            RoiGenerator generator = getImagePlus().getCurrentRoiGenerator();
            int ox = offScreenX(sx);
            int oy = offScreenY(sy);
            if (generator == null) {
                hoveredRoi = getRoiAtPoint(sx, sy, ox, oy);
            } else if (generator.needsNoHandledRoi() && (!generator.canHoverOtherRois())) {
                Roi tmp = getRoiAtPoint(sx, sy, ox, oy);
                if (tmp == getImagePlus().getSpecialRoi(generator)) {
                    hoveredRoi = tmp;
                }
            } else {
                hoveredRoi = getRoiAtPoint(sx, sy, ox, oy);
            }
        }
    }

    protected void clearHighlightedRoi() {
        hoveredRoi = null;
    }

    /**
     * Returns whether selection should be kept according to a given {@link MouseEvent}.
     * 
     * @param e The {@link MouseEvent}.
     * @return <code>true</code> if we should deal with the actual selection, <code>false</code> to start a new one.
     */
    public boolean isKeepSelection(MouseEvent e) {
        // if CTRL key is down, modify the current selection, otherwise start a new one
        boolean result = false;
        if (e != null) {
            result = e.isControlDown();
            if (isUseShiftToKeepSelection()) {
                result = result || e.isShiftDown();
            }
        }
        return result;
        // int onMask = InputEvent.CTRL_DOWN_MASK;
        // boolean result = ((e.getModifiersEx() & onMask) == onMask);
        //
        // return result;
    }

    /**
     * Method used to find the ROI to select, corresponding to given coordinates.
     * 
     * @param sx X-coordinate (screen).
     * @param sy Y-coordinate (screen).
     * @param ox X-coordinate (image).
     * @param oy Y-coordinate (image).
     * @param keepSelection <code>false</code> to clear any previous selection, <code>true</code> to invert selected
     *            state for considered ROIs.
     */
    public void selectRoi(int sx, int sy, int ox, int oy, boolean keepSelection) {
        Roi closestRoi = getRoiAtPoint(sx, sy, ox, oy);

        // algo:
        // -if no roi under: clear the selection
        // -if roi under: clear the selection, select the roi
        // -if keep selection and roi is not selected: add roi to selection
        // -if keep selection and roi is selected: remove roi from selection
        if (closestRoi == null) {
            if (!keepSelection) {
                getImagePlus().clearRoiSelection();
            }
        } else {
            if (getImagePlus().isSelectedRoi(closestRoi) && keepSelection) {
                getImagePlus().deselectRoi(closestRoi);
            }
            // roi is not selected
            else if (keepSelection) {
                getImagePlus().selectRoi(closestRoi);
            } else {
                getImagePlus().clearRoiSelection();
                getImagePlus().selectRoi(closestRoi);
            }
        }
    }

    /**
     * Method used to find the ROI to select, corresponding to given coordinates.
     * 
     * @param sx X-coordinate (screen).
     * @param sy Y-coordinate (screen).
     * @param keepSelection <code>false</code> to clear any previous selection, <code>true</code> to invert selected
     *            state for considered ROIs.
     */
    public void selectRoi(int sx, int sy, boolean keepSelection) {
        selectRoi(sx, sy, offScreenX(sx), offScreenY(sy), keepSelection);
    }

    /**
     * Selects ROIs that are included in the one specified.
     * 
     * @param selectionRoi the rectangular ROI that contains ROIs to select.
     * @param keepSelection <code>false</code> to clear any previous selection, <code>true</code> to invert selected
     *            state for considered ROIs.
     */
    public void selectRois(Roi selectionRoi, boolean keepSelection) {
        if (selectionRoi != null) {
            if (!keepSelection) {
                getImagePlus().clearRoiSelection();
            }
            Rectangle selectionBounds = selectionRoi.getBounds();
            for (Roi aRoi : getImagePlus().getAllRois()) {
                Rectangle roiBounds = aRoi.getBounds();

                // special case for vertical and horizontal lines, whose
                // bounding rect has one 0
                // dimension (so that contains fail)
                if (aRoi.getType() == Roi.LINE && (roiBounds.width == 0 || roiBounds.height == 0)) {
                    // we artificially grow the 0-dimension to use contains
                    roiBounds.width = Math.max(1, roiBounds.width);
                    roiBounds.height = Math.max(1, roiBounds.height);
                }

                if (selectionBounds.contains(roiBounds)) {
                    // invert the selected state of the ROI
//                    if (getImagePlus().isSelectedRoi(aRoi)) {
//                        getImagePlus().deselectRoi(aRoi);
//                    } else {
                    getImagePlus().selectRoi(aRoi);
//                    }
                }
            }
        }
    }

    public void zoomIn() {
        // dstWidth and dstHeight do not reflect the real size in zoom mode (IJ
        // bug?)
        // so we can't use zoomIn(dstWidth / 2, dstHeight / 2);
        if ((srcRect != null) && (imp != null) && (imp.getWindow() != null)) {
            zoomIn((int) (srcRect.width * magnification) / 2, (int) (srcRect.height * magnification) / 2);
        }
    }

    public void zoomOut() {
        // dstWidth and dstHeight do not reflect the real size in zoom mode (IJ
        // bug?)
        // so we can't use zoomOut(dstWidth / 2, dstHeight / 2);
        if ((srcRect != null) && (imp != null) && (imp.getWindow() != null)) {
            zoomOut((int) (srcRect.width * magnification) / 2, (int) (srcRect.height * magnification) / 2);
        }
    }

    @Override
    public void setMagnification(double magnification) {
        double formerMagnification = getMagnification();
        try {
            super.setMagnification(magnification);
        } catch (NullPointerException npe) {
            // XXX NPE ignored as ImageJ is stupid enough to try to update a title we don't care about
        }
        double currentMagnification = getMagnification();
        firePropertyChange(MAGNIFICATION_PROPERTY, formerMagnification, currentMagnification);
    }

    /**
     * Returns the way mouse wheel interacts with image zoom.
     * 
     * @return A {@link MouseZoomMode}.
     */
    public MouseZoomMode getMouseZoomMode() {
        return mouseZoomMode;
    }

    /**
     * Sets the way mouse wheel interacts with image zoom.
     * 
     * @param mouseZoomMode The {@link MouseZoomMode} that represents the way mouse wheel interacts with image zoom.
     */
    public void setMouseZoomMode(MouseZoomMode mouseZoomMode) {
        this.mouseZoomMode = (mouseZoomMode == null ? MouseZoomMode.NONE : mouseZoomMode);
    }

//    /**
//     * Zooms in a particular zone in picture
//     * 
//     * @param zoomRect The {@link Rectangle} that represents the zone in which to zoom
//     */
//    public void zoomInRect(Rectangle zoomRect) {
//        if ((srcRect != null) && (zoomRect != null)) {
//            Dimension refSize = maxDrawingSize;
//            // This part seems to be necessary for small pictures in case of fit max size
//            int width, height;
//            if (refSize == null) {
//                refSize = getImagePlus().getWindow().getPreferredSize();
//                width = imageWidth;
//                height = imageHeight;
//            } else {
//                width = refSize.width;
//                height = refSize.height;
//            }
//            double newMag = Math.max(width / (double) zoomRect.width, height / (double) zoomRect.height);
//            newMag = getHigherZoomLevel(newMag - 0.0000000000000001);
//            if (newMag < magnification) {
//                newMag = getHigherZoomLevel(magnification);
//            }
//            // Workaround : unzoom to avoid srcRect interactions
//            unzoom();
//            width = Math.min((int) (imageWidth * newMag), refSize.width);
//            height = Math.min((int) (imageHeight * newMag), refSize.height);
//            double zoomRectCenterX = zoomRect.x + (zoomRect.width / 2.0d);
//            double zoomRectCenterY = zoomRect.y + (zoomRect.height / 2.0d);
//            double zoneX = zoomRect.x;
//            double zoneY = zoomRect.y;
//            double zoneWidth = width / newMag;
//            double zoneHeight = height / newMag;
//            // Workaround : avoiding limit case problems
//            if (zoneWidth <= imageWidth && zoneHeight <= imageHeight) {
//                double zoneCenterX = zoneX + (width / (2.0 * newMag));
//                double zoneCenterY = zoneY + (height / (2.0 * newMag));
//                // Center alignment
//                zoneX += (zoomRectCenterX - zoneCenterX);
//                zoneY += (zoomRectCenterY - zoneCenterY);
//                Rectangle zone = new Rectangle((int) Math.round(zoneX), (int) Math.round(zoneY),
//                        (int) Math.round(zoneWidth), (int) Math.round(zoneHeight));
//                // Limit case test: out of bounds
//                adaptRectangleLimit(zone);
//                // Border magnet:
//                borderMagnet(zone, zoomRect);
//                // Apply zoom
//                setDrawingSize(width, height);
//                setMagnification(newMag);
//                setSize(width, height);
//                setSourceRect(zone);
//                getImagePlus().getWindow().pack();
//                // Border magnet after pack()
//                borderMagnet(srcRect, zoomRect);
//            }
//            repaint();
//        }
//    }

    /**
     * Zooms in a particular zone in picture.
     * 
     * @param zoomRect The {@link Rectangle} that represents the zone in which to zoom. Its dimensions are expected to
     *            be expressed in image offscreen coordinates system.
     */
    public void zoomInRect(Rectangle zoomRect) {
        if ((srcRect != null) && (zoomRect != null)) {
            // Rectangle that will become the sub position rectangle (srcRect)
            Rectangle zone = new Rectangle(zoomRect.x, zoomRect.y, zoomRect.width, zoomRect.height);
            Dimension refSize = maxDrawingSize;
            // This part seems to be necessary for small pictures in case of fit max size
            int width, height;
            int refWidth, refHeight;
            if (refSize == null) {
                refSize = getImagePlus().getWindow().getPreferredSize();
            }
            refWidth = refSize.width - 10;
            refHeight = refSize.height - 10;
            // adapt zoom rectangle to image dimensions
            if (zone.x < 0) {
                int x = zone.x;
                zone.x = 0;
                zone.width += x;
            }
            int w = zone.x + zone.width;
            if (w > imageWidth) {
                zone.width += imageWidth - w;
            }
            if (zone.y < 0) {
                int y = zone.y;
                zone.y = 0;
                zone.height += y;
            }
            int h = zone.y + zone.height;
            if (h > imageHeight) {
                zone.height += imageHeight - h;
            }
            // width of the IJCanvas once zoom will be applied
            double drawWidth = zone.width * magnification;
            // height of the IJCanvas once zoom will be applied
            double drawHeight = zone.height * magnification;
            // Best zoom to match
            double newMag = Math.min(refWidth / drawWidth, refHeight / drawHeight) * magnification;

            // unzoom to avoid srcRect interactions
            unzoom();
            // Limit case test: out of bounds
            adaptRectangleLimit(zone);
            // Border magnet:
            borderMagnet(zone, zoomRect);
            // Apply zoom
            width = (int) Math.round(drawWidth);
            height = (int) Math.round(drawHeight);
            setDrawingSize(width, height);
            setMagnification(newMag);
            setSize(width, height);
            setSourceRect(zone);
            repaint();
        }
    }

    protected void borderMagnet(Rectangle zone, Rectangle zoomRect) {
        if (zoomRect.x == 0) {
            zone.x = 0;
        }
        if (zoomRect.x + zoomRect.width >= imageWidth) {
            zone.x += imageWidth - (zone.x + zone.width);
        }
        if (zoomRect.y == 0) {
            zone.y = 0;
        }
        if (zoomRect.y + zoomRect.height >= imageHeight) {
            zone.y += imageHeight - (zone.y + zone.height);
        }
    }

    public boolean isHighlightHoveredRoi() {
        return highlightHoveredRoi;
    }

    public void setHighlightHoveredRoi(boolean highlight) {
        highlightHoveredRoi = highlight;
    }

    /**
     * @return The roiColor
     */
    public Color getRoiColor() {
        return roiColor;
    }

    /**
     * @param color The roiColor to set
     */
    public void setRoiColor(Color color) {
        roiColor = (color == null ? DEFAULT_ROI_COLOR : color);
    }

    /**
     * @return The roiNameColor
     */
    public Color getRoiNameColor() {
        return roiNameColor;
    }

    /**
     * @param nameColor The roiNameColor to set
     */
    public void setRoiNameColor(Color nameColor) {
        roiNameColor = (nameColor == null ? DEFAULT_ROI_NAME_COLOR : nameColor);
    }

    /**
     * @return The hoveredRoiColor
     */
    public Color getHoveredRoiColor() {
        return hoveredRoiColor;
    }

    /**
     * @param hoveredColor The hoveredRoiColor to set
     */
    public void setHoveredRoiColor(Color hoveredColor) {
        hoveredRoiColor = (hoveredColor == null ? DEFAULT_HOVERED_ROI_COLOR : hoveredColor);
    }

    /**
     * @return The selectedRoiColor
     */
    public Color getSelectedRoiColor() {
        return selectedRoiColor;
    }

    /**
     * @param selectedColor The selectedRoiColor to set
     */
    public void setSelectedRoiColor(Color selectedColor) {
        selectedRoiColor = (selectedColor == null ? DEFAULT_SELECTED_ROI_COLOR : selectedColor);
    }

    /**
     * @return The roiSelectionIndexColor
     */
    public Color getRoiSelectionIndexColor() {
        return roiSelectionIndexColor;
    }

    /**
     * @param selectionIndexColor The roiSelectionIndexColor to set
     */
    public void setRoiSelectionIndexColor(Color selectionIndexColor) {
        roiSelectionIndexColor = (selectionIndexColor == null ? DEFAULT_ROI_SELECTION_INDEX_COLOR
                : selectionIndexColor);
    }

    /**
     * @return the selectionRoiColor
     */
    public Color getSelectionRoiColor() {
        return selectionRoiColor;
    }

    /**
     * @param selectionRoiColor The selectionRoiColor to set
     */
    public void setSelectionRoiColor(Color selectionRoiColor) {
        this.selectionRoiColor = (selectionRoiColor == null ? DEFAULT_SELECTION_ROI_COLOR : selectionRoiColor);
    }

    /**
     * @return the zoomRoiColor
     */
    public Color getZoomRoiColor() {
        return zoomRoiColor;
    }

    /**
     * @param zoomRoiColor The zoomRoiColor to set
     */
    public void setZoomRoiColor(Color zoomRoiColor) {
        this.zoomRoiColor = (zoomRoiColor == null ? DEFAULT_ZOOM_ROI_COLOR : zoomRoiColor);
    }

    /**
     * @return the focusRoiColor
     */
    public Color getFocusRoiColor() {
        return focusRoiColor;
    }

    /**
     * @param focusRoiColor The focusRoiColor to set
     */
    public void setFocusRoiColor(Color focusRoiColor) {
        this.focusRoiColor = (focusRoiColor == null ? DEFAULT_FOCUS_ROI_COLOR : focusRoiColor);
    }

    /**
     * Returns the Color for an inner ROI
     * 
     * @return The Color for an inner ROI
     */
    public Color getInnerRoiColor() {
        return innerRoiColor;
    }

    /**
     * Sets the Color for an inner ROI
     * 
     * @param innerRoiColor The Color for an inner ROI
     */
    public void setInnerRoiColor(Color innerRoiColor) {
        if (innerRoiColor == null) {
            this.innerRoiColor = DEFAULT_INNER_ROI_COLOR;
        } else {
            this.innerRoiColor = innerRoiColor;
        }
    }

    /**
     * Returns the Color for a hovered inner ROI
     * 
     * @return the Color for a hovered inner ROI
     */
    public Color getHoveredInnerRoiColor() {
        return hoveredInnerRoiColor;
    }

    /**
     * Sets the Color for a hovered inner ROI
     * 
     * @param hoveredInnerRoiColor The Color for a hovered inner ROI
     */
    public void setHoveredInnerRoiColor(Color hoveredInnerRoiColor) {
        if (hoveredInnerRoiColor == null) {
            this.hoveredInnerRoiColor = DEFAULT_HOVERED_INNER_ROI_COLOR;
        } else {
            this.hoveredInnerRoiColor = hoveredInnerRoiColor;
        }
    }

    /**
     * Returns the Color for a selected inner ROI
     * 
     * @return the Color for a selected inner ROI
     */
    public Color getSelectedInnerRoiColor() {
        return selectedInnerRoiColor;
    }

    /**
     * Sets the Color for a selected inner ROI
     * 
     * @param selectedInnerRoiColor The Color for a selected inner ROI
     */
    public void setSelectedInnerRoiColor(Color selectedInnerRoiColor) {
        if (selectedInnerRoiColor == null) {
            this.selectedInnerRoiColor = DEFAULT_SELECTED_INNER_ROI_COLOR;
        } else {
            this.selectedInnerRoiColor = selectedInnerRoiColor;
        }
    }

    /**
     * Returns the Color for an outer ROI
     * 
     * @return the Color for an outer ROI
     */
    public Color getOuterRoiColor() {
        return outerRoiColor;
    }

    /**
     * Sets the Color for an outer ROI
     * 
     * @param outerRoiColor The Color for an outer ROI
     */
    public void setOuterRoiColor(Color outerRoiColor) {
        if (outerRoiColor == null) {
            this.outerRoiColor = DEFAULT_OUTER_ROI_COLOR;
        } else {
            this.outerRoiColor = outerRoiColor;
        }
    }

    /**
     * Returns the Color for a hovered outer ROI
     * 
     * @return the Color for a hovered outer ROI
     */
    public Color getHoveredOuterRoiColor() {
        return hoveredOuterRoiColor;
    }

    /**
     * Sets the Color for a hovered outer ROI
     * 
     * @param hoveredOuterRoiColor The Color for a hovered outer ROI
     */
    public void setHoveredOuterRoiColor(Color hoveredOuterRoiColor) {
        if (hoveredOuterRoiColor == null) {
            this.hoveredOuterRoiColor = DEFAULT_HOVERED_OUTER_ROI_COLOR;
        } else {
            this.hoveredOuterRoiColor = hoveredOuterRoiColor;
        }
    }

    /**
     * Returns the Color for a selected outer ROI
     * 
     * @return the Color for a selected outer ROI
     */
    public Color getSelectedOuterRoiColor() {
        return selectedOuterRoiColor;
    }

    /**
     * Sets the Color for a selected outer ROI
     * 
     * @param selectedOuterRoiColor The Color for a selected outer ROI
     */
    public void setSelectedOuterRoiColor(Color selectedOuterRoiColor) {
        if (selectedOuterRoiColor == null) {
            this.selectedOuterRoiColor = DEFAULT_SELECTED_OUTER_ROI_COLOR;
        } else {
            this.selectedOuterRoiColor = selectedOuterRoiColor;
        }
    }

    /**
     * Returns whether [Shift] is used to keep selection (like [Ctrl])
     * 
     * @return a boolean value
     */
    public boolean isUseShiftToKeepSelection() {
        return useShiftToKeepSelection;
    }

    /**
     * Sets whether [Shift] is used to keep selection (like [Ctrl])
     * 
     * @param useShiftToKeepSelection A boolean
     */
    public void setUseShiftToKeepSelection(boolean useShiftToKeepSelection) {
        this.useShiftToKeepSelection = useShiftToKeepSelection;
    }

    public boolean isDrawSelectedRoiIndex() {
        return drawSelectedRoiIndex;
    }

    public void setDrawSelectedRoiIndex(boolean drawSelectedIndex) {
        drawSelectedRoiIndex = drawSelectedIndex;
    }

    public boolean isDrawRoiName() {
        return drawRoiName;
    }

    public void setDrawRoiName(boolean drawName) {
        drawRoiName = drawName;
    }

    /**
     * @return the roiNameColorAsRoi
     */
    public boolean isRoiNameColorAsRoi() {
        return roiNameColorAsRoi;
    }

    /**
     * @param roiNameColorAsRoi The roiNameColorAsRoi to set
     */
    public void setRoiNameColorAsRoi(boolean roiNameColorAsRoi) {
        this.roiNameColorAsRoi = roiNameColorAsRoi;
    }

    public boolean isDrawBeamPosition() {
        return drawBeamPosition;
    }

    public void setDrawBeamPosition(boolean drawBeam) {
        drawBeamPosition = drawBeam;
    }

    public Point2D.Double getBeamPoint() {
        return beamPoint;
    }

    public void setBeamPoint(Point2D.Double theBeamPoint) {
        beamPoint = theBeamPoint;
    }

    public void clearBeamPoint() {
        setBeamPoint(null);
    }

    public Color getBeamColor() {
        return beamColor;
    }

    public void setBeamColor(Color theBeamColor) {
        beamColor = (theBeamColor == null ? DEFAULT_BEAM_COLOR : theBeamColor);
    }

    /**
     * @return the beamCrossType
     */
    public BeamCrossType getBeamCrossType() {
        return beamCrossType;
    }

    /**
     * @param crossType The beamCrossType to set
     */
    public void setBeamCrossType(BeamCrossType crossType) {
        beamCrossType = (crossType == null ? DEFAULT_BEAM_CROSS_TYPE : crossType);
    }

    public Point getCrosshairPoint() {
        return crosshairPoint;
    }

    public void setCrosshairPoint(Point theCrosshairPoint) {
        crosshairPoint = theCrosshairPoint;
    }

    public void clearCrosshairPoint() {
        setCrosshairPoint(null);
    }

    /**
     * @return the crosshairColor
     */
    public Color getCrosshairColor() {
        return crosshairColor;
    }

    /**
     * @param theCrosshairColor The crosshairColor to set
     */
    public void setCrosshairColor(Color theCrosshairColor) {
        crosshairColor = (theCrosshairColor == null ? DEFAULT_CROSSHAIR_COLOR : theCrosshairColor);
    }

    /**
     * @return the rectangle that bounds canvas drawing
     */
    public Rectangle getCanvasRectangle() {
        int width = 0;
        int height = 0;
        if (srcRect != null) {
            width = (int) (srcRect.width * magnification);
            height = (int) (srcRect.height * magnification);
        }
        return new Rectangle(0, 0, width, height);
    }

    /**
     * Gives the pixel coordinates of the mouse in the whole image, taking care of the zoom level and the pan position.
     * 
     * @param screenPos The mouse position over the canvas (pixel)
     * @return The mouse position in image coordinates (pixel)
     */
    public Point getImagePosition(Point screenPos) {
        Point result = null;
        if (getCanvasRectangle().contains(screenPos)) {
            int ox = offScreenX(screenPos.x);
            int oy = offScreenY(screenPos.y);
            result = new Point(ox, oy);
        }
        return result;
    }

    /**
     * Gives the pixel coordinates of the mouse in the whole image, taking care of the zoom level and the pan position.
     * 
     * @param screenPos The mouse position over the canvas (pixel)
     * @return The mouse position in image coordinates (fraction of pixel)
     */
    public Point2D.Double getImagePositionD(Point screenPos) {
        Point2D.Double result = null;
        if (getCanvasRectangle().contains(screenPos)) {
            double ox = offScreenXD(screenPos.x);
            double oy = offScreenYD(screenPos.y);
            result = new Point2D.Double(ox, oy);
        }
        return result;
    }

    @Override
    protected Dimension canEnlarge(int newWidth, int newHeight) {
        // This is a hack. The preferred size was set to the window by the IJImage, because the IJImage knows the
        // available place.
        // So, we make sure the size returned is not bigger than the available place.
        Dimension refSize = maxDrawingSize;
        Dimension dim = super.canEnlarge(newWidth, newHeight);
        if (dim == null) {
            dim = refSize;
        }
        if (refSize != null) {
            dim = new Dimension(Math.min(dim.width, refSize.width), Math.min(dim.height, refSize.height));
        }
        return dim;
    }

    @Override
    public void setCursor(int sx, int sy, int ox, int oy) {
        super.setCursor(sx, sy, ox, oy);
        Cursor cursorToSet = getCursorToSet();
        if (cursorToSet != null) {
            doSetCursor(cursorToSet);
        }
    }

    protected Cursor getCursorToSet() {
        Cursor cursorToSet;
        RoiGenerator generator = getImagePlus().getCurrentRoiGenerator();
        if (generator == null) {
            if (ActionManager.getInstance().isSelected(ImageViewer.MODE_PAN_ACTION)) {
                cursorToSet = HAND_CURSOR;
            } else {
                cursorToSet = null;
            }
        } else {
            cursorToSet = generator.getCursor();
        }
        return cursorToSet;
    }

    @Override
    public void setCursor(Cursor cursor) {
        Cursor cursorToSet = getCursorToSet();
        if (cursorToSet == null) {
            cursorToSet = cursor;
        }
        doSetCursor(cursorToSet);
    }

    @Override
    public void fitToWindow() {
        try {
            super.fitToWindow();
        } catch (NullPointerException e) {
            // Ignore when parent is null due to Thread concurrency
            if (getParent() != null) {
                throw e;
            }
        }
    }

    public void doSetCursor(Cursor cursor) {
        super.setCursor(cursor);
    }

    public Dimension getMaxDrawingSize() {
        return maxDrawingSize;
    }

    public void setMaxDrawingSize(Dimension maxDrawingSize) {
        this.maxDrawingSize = maxDrawingSize;
    }

    public void clean() {
        imp = null;
    }

}
