/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.BorderLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeListener;

import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.swing.util.CometeUtils;

/**
 * Common part for DataViewOption and ChartOption
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AbstractChartDataOption extends JPanel
        implements ActionListener, MouseListener, ChangeListener, KeyListener, CometeConstants {

    private static final long serialVersionUID = -4883605270651312939L;

    protected JButton closeButton;
    protected Chart chart;
    protected JPanel southPanel;
    protected JTabbedPane tabPane;

    public AbstractChartDataOption() {
        super(new BorderLayout());
        chart = null;
        initComponents();
    }

    protected void initComponents() {
        closeButton = new JButton(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.close"));
        closeButton.addActionListener(this);
        southPanel = new JPanel(new BorderLayout());
        southPanel.add(closeButton, BorderLayout.EAST);
        add(southPanel, BorderLayout.SOUTH);
        initTabPane();
        add(tabPane, BorderLayout.CENTER);
        setName(getExpectedName());
    }

    protected void initTabPane() {
        tabPane = new JTabbedPane();
    }

    protected abstract String getExpectedName();

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == closeButton) {
            closeParentWindow();
            commit();
        }
    }

    protected void closeParentWindow() {
        Window parent = CometeUtils.getWindowForComponent(this);
        if (parent != null) {
            parent.setVisible(false);
        }
    }

    /**
     * Sets this {@link AbstractChartDataOption}'s {@link Chart}
     * 
     * @param chart The {@link Chart} to set
     */
    public void setChart(Chart chart) {
        this.chart = chart;
        updatePropertiesFromChart();
    }

    /**
     * Commit change. Repaint the graph.
     */
    protected abstract void commit();

    /**
     * Refresh content from chart
     */
    protected abstract void updatePropertiesFromChart();

    public JButton getCloseButton() {
        return closeButton;
    }

}
