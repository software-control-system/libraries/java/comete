/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import fr.soleil.lib.project.math.MathConst;

/**
 * A class that matches image values with colors
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class NumberImageConverter {

    private static final int COLOR_MAX = 65535;
    protected double bfa0;
    protected double bfa1;
    protected double bfMin;
    protected double bfMax;
    protected JGradientViewer gradientTool;
    protected boolean negative;
    protected int[] gColormap;
    protected int naNColor;
    protected int positiveInfinityColor;
    protected int negativeInfinityColor;

    private boolean imageFillingAllowed;

    public NumberImageConverter(JGradientViewer gradient, int[] colorMap) {
        imageFillingAllowed = true;
        this.gradientTool = gradient;
        this.gColormap = colorMap;
        negative = false;
        gColormap = null;
        bfMin = 0.0;
        bfMax = COLOR_MAX;
    }

    /**
     * @return the imageFillingAllowed
     */
    public boolean isImageFillingAllowed() {
        return imageFillingAllowed;
    }

    /**
     * @param imageFillingAllowed the imageFillingAllowed to set
     */
    public void setImageFillingAllowed(boolean imageFillingAllowed) {
        this.imageFillingAllowed = imageFillingAllowed;
    }

    public int getNaNColor() {
        return naNColor;
    }

    public void setNaNColor(int nanColor) {
        this.naNColor = nanColor;
    }

    public int getPositiveInfinityColor() {
        return positiveInfinityColor;
    }

    public void setPositiveInfinityColor(int positiveInfiniteColor) {
        this.positiveInfinityColor = positiveInfiniteColor;
    }

    public int getNegativeInfinityColor() {
        return negativeInfinityColor;
    }

    public void setNegativeInfinityColor(int negativeInfiniteColor) {
        this.negativeInfinityColor = negativeInfiniteColor;
    }

    /**
     * Sets the value of best fit min and max.
     * 
     * @param min Minimum value
     * @param max Maximum value
     */
    public void setBestFitMinMax(double min, double max) {
        if (min < max) {
            bfMin = min;
            bfMax = max;
        }
    }

    /**
     * Calculates the best zoom for an image to fit some bounds
     * 
     * @param imageWidth The image width
     * @param imageHeight The image height
     * @param prefWidth The desired width
     * @param prefHeight The desired height
     * @param minZoom The minimum zoom allowed
     * @param maxZoom The maximum zoom allowed
     * @return A <code>double</code> value that represents the calculated zoom
     */
    public double getBestZoom(int imageWidth, int imageHeight, int prefWidth, int prefHeight, double minZoom,
            double maxZoom) {
        double zoom = 1;
        double dimX = imageWidth;
        double dimY = imageHeight;
        double min = Math.abs(minZoom);
        double max = Math.abs(maxZoom);
        if (min > max) {
            double temp = min;
            min = max;
            max = temp;
        }
        if (dimX > prefWidth || dimY > prefHeight) {
            // Search smaller size
            while ((dimX > prefWidth || dimY > prefHeight) && zoom > min) {
                dimX /= 2;
                dimY /= 2;
                zoom /= 2;
            }
            if (zoom < min) {
                zoom = min;
            }
        } else {
            // Search bigger size
            while (dimX < prefWidth && dimY < prefHeight && zoom < max) {
                dimX *= 2;
                dimY *= 2;
                zoom *= 2;
            }
            if (zoom > max) {
                zoom = max;
            }
        }
        return zoom;
    }

    protected int bestFit(double v) {
        int nv = (int) ((bfa0 + v) * bfa1);
        if (nv < 0) {
            nv = 0;
        } else if (nv > COLOR_MAX) {
            nv = COLOR_MAX;
        }
        return nv;
    }

    public double getBfa0() {
        return bfa0;
    }

    public void setBfa0(double bfa0) {
        this.bfa0 = bfa0;
    }

    public double getBfa1() {
        return bfa1;
    }

    public void setBfa1(double bfa1) {
        this.bfa1 = bfa1;
    }

    public double getBfMin() {
        return bfMin;
    }

    public void setBfMin(double bfMin) {
        this.bfMin = bfMin;
    }

    public double getBfMax() {
        return bfMax;
    }

    public void setBfMax(double bfMax) {
        this.bfMax = bfMax;
    }

    /**
     * Returns the {@link JGradientViewer} used to recover Colors
     * 
     * @return a {@link JGradientViewer}
     */
    public JGradientViewer getGradientTool() {
        return gradientTool;
    }

    /**
     * Sets the {@link JGradientViewer} used to recover Colors
     * 
     * @param gradientTool The {@link JGradientViewer} to use
     */
    public void setGradientTool(JGradientViewer gradientTool) {
        this.gradientTool = gradientTool;
    }

    public boolean isNegative() {
        return negative;
    }

    public void setNegative(boolean negative) {
        this.negative = negative;
    }

    public int[] getGColormap() {
        return gColormap;
    }

    public void setGColormap(int[] colormap) {
        gColormap = colormap;
    }

    public BufferedImage convertToImage(Object values, int dimX, int dimY, BufferedImage source, boolean bestFit,
            boolean autoBestFit, boolean strictlyPositiveBounds) {
        return convertToImage(values, dimX, dimY, source, bestFit, autoBestFit, true, strictlyPositiveBounds);
    }

    public BufferedImage convertToImage(Object values, int dimX, int dimY, BufferedImage source, boolean bestFit,
            boolean autoBestFit, boolean needToUpdateGradient, boolean strictlyPositiveBounds) {
        BufferedImage image = null;
        if ((dimX > 0) && (dimY > 0) && (gColormap != null) && (values != null)) {
            image = source;
            if ((image == null) || (image.getWidth() != dimX) || (image.getHeight() != dimY)) {
                image = new BufferedImage(dimX, dimY, BufferedImage.TYPE_INT_RGB);
                if (!isImageFillingAllowed()) {
                    for (int j = 0; j < dimY; j++) {
                        int[] rgb = new int[dimX];
                        for (int i = 0; i < dimX; i++) {
                            rgb[i] = naNColor;
                        }
                        image.setRGB(0, j, dimX, 1, rgb, 0, dimX);
                    }
                }
            }
            if (isImageFillingAllowed()) {
                int[] rgb = new int[dimX];
                if (needToUpdateGradient) {
                    applyBestFitToGradient(values, dimX, dimY, bestFit, autoBestFit, strictlyPositiveBounds);
                }
                if (values instanceof byte[]) {
                    byte[] array = (byte[]) values;
                    for (int j = 0; j < dimY; j++) {
                        if (bestFit) {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~bestFit(val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[bestFit(val)];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        } else {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~(int) (val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[((int) val) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        }
                        image.setRGB(0, j, dimX, 1, rgb, 0, dimX);
                    }
                } // end if (values instanceof byte[])
                else if (values instanceof short[]) {
                    short[] array = (short[]) values;
                    for (int j = 0; j < dimY; j++) {
                        if (bestFit) {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~bestFit(val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[bestFit(val)];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        } else {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~(int) (val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[((int) val) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        }
                        image.setRGB(0, j, dimX, 1, rgb, 0, dimX);
                    }
                } // end if (values instanceof short[])
                else if (values instanceof int[]) {
                    int[] array = (int[]) values;
                    for (int j = 0; j < dimY; j++) {
                        if (bestFit) {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~bestFit(val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[bestFit(val)];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        } else {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~(int) (val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[((int) val) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        }
                        image.setRGB(0, j, dimX, 1, rgb, 0, dimX);
                    }
                } // end if (values instanceof int[])
                else if (values instanceof long[]) {
                    long[] array = (long[]) values;
                    for (int j = 0; j < dimY; j++) {
                        if (bestFit) {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~bestFit(val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[bestFit(val)];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        } else {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~(int) (val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[((int) val) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        }
                        image.setRGB(0, j, dimX, 1, rgb, 0, dimX);
                    }
                } // end if (values instanceof long[])
                else if (values instanceof float[]) {
                    float[] array = (float[]) values;
                    for (int j = 0; j < dimY; j++) {
                        if (bestFit) {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~bestFit(val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[bestFit(val)];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        } else {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~(int) (val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[((int) val) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        }
                        image.setRGB(0, j, dimX, 1, rgb, 0, dimX);
                    }
                } // end if (values instanceof float[])
                else if (values instanceof double[]) {
                    double[] array = (double[]) values;
                    for (int j = 0; j < dimY; j++) {
                        if (bestFit) {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~bestFit(val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[bestFit(val)];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        } else {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~(int) (val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = array[(j * dimX) + i];
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[((int) val) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        }
                        image.setRGB(0, j, dimX, 1, rgb, 0, dimX);
                    }
                } // end if (values instanceof double[])
                else if (values instanceof Number[]) {
                    Number[] array = (Number[]) values;
                    for (int j = 0; j < dimY; j++) {
                        if (bestFit) {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = extractDouble(array[(j * dimX) + i]);
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~bestFit(val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = extractDouble(array[(j * dimX) + i]);
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[bestFit(val)];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        } else {
                            if (negative) {
                                for (int i = 0; i < dimX; i++) {
                                    double val = extractDouble(array[(j * dimX) + i]);
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[(~(int) (val)) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            } else {
                                for (int i = 0; i < dimX; i++) {
                                    double val = extractDouble(array[(j * dimX) + i]);
                                    int c;
                                    if (Double.isNaN(val)) {
                                        c = naNColor;
                                    } else if (Double.isInfinite(val)) {
                                        if (val > 0) {
                                            c = positiveInfinityColor;
                                        } else {
                                            c = negativeInfinityColor;
                                        }
                                    } else {
                                        c = gColormap[((int) val) & COLOR_MAX];
                                    }
                                    rgb[i] = c;
                                }
                            }
                        }
                        image.setRGB(0, j, dimX, 1, rgb, 0, dimX);
                    }
                } // end if (values instanceof Number[])
            } // end if (isImageFillingAllowed())
        } // end if ((dimX > 0) && (dimY > 0) && (gColormap != null) && (values != null))
        return image;
    }

    public double[] preComputeBestFit(Object values, int dimX, int dimY, boolean bestFit, boolean autoBestFit,
            boolean strictlyPositiveBounds) {
        double[] result = null;
        double fitMin, fitMax;
        if ((values != null) && bestFit) {
            result = new double[2];
            int i, j;
            // DATAREDUC-856: recompute bounds when negative or zero and strictlyPositiveBounds is true
            if (autoBestFit || (strictlyPositiveBounds && (bfMin <= 0 || bfMax <= 0))) {
                // Start to opposite infinite values to avoid an undesired log scale bug (JAVAAPI-319)
                fitMin = Double.POSITIVE_INFINITY;
                fitMax = Double.NEGATIVE_INFINITY;
                if (values instanceof byte[]) {
                    byte[] array = (byte[]) values;
                    for (j = 0; j < dimY; j++) {
                        for (i = 0; i < dimX; i++) {
                            double v = array[(j * dimX) + i];
                            // DATAREDUC-856: in case of strictly positive bounds, exclude zero and negative values
                            if ((!Double.isNaN(v)) && (!Double.isInfinite(v))
                                    && ((!strictlyPositiveBounds) || (v > 0))) {
                                // ignore NaN and Infinite values
                                if (v > fitMax) {
                                    fitMax = v;
                                }
                                if (v < fitMin) {
                                    fitMin = v;
                                }
                            }
                        }
                    }
                } // end if (values instanceof byte[])
                else if (values instanceof short[]) {
                    short[] array = (short[]) values;
                    for (j = 0; j < dimY; j++) {
                        for (i = 0; i < dimX; i++) {
                            double v = array[(j * dimX) + i];
                            // DATAREDUC-856: in case of strictly positive bounds, exclude zero and negative values
                            if ((!Double.isNaN(v)) && (!Double.isInfinite(v))
                                    && ((!strictlyPositiveBounds) || (v > 0))) {
                                // ignore NaN and Infinite values
                                if (v > fitMax) {
                                    fitMax = v;
                                }
                                if (v < fitMin) {
                                    fitMin = v;
                                }
                            }
                        }
                    }
                } // end if (values instanceof short[])
                else if (values instanceof int[]) {
                    int[] array = (int[]) values;
                    for (j = 0; j < dimY; j++) {
                        for (i = 0; i < dimX; i++) {
                            double v = array[(j * dimX) + i];
                            // DATAREDUC-856: in case of strictly positive bounds, exclude zero and negative values
                            if ((!Double.isNaN(v)) && (!Double.isInfinite(v))
                                    && ((!strictlyPositiveBounds) || (v > 0))) {
                                // ignore NaN and Infinite values
                                if (v > fitMax) {
                                    fitMax = v;
                                }
                                if (v < fitMin) {
                                    fitMin = v;
                                }
                            }
                        }
                    }
                } // end if (values instanceof int[])
                else if (values instanceof long[]) {
                    long[] array = (long[]) values;
                    for (j = 0; j < dimY; j++) {
                        for (i = 0; i < dimX; i++) {
                            double v = array[(j * dimX) + i];
                            // DATAREDUC-856: in case of strictly positive bounds, exclude zero and negative values
                            if ((!Double.isNaN(v)) && (!Double.isInfinite(v))
                                    && ((!strictlyPositiveBounds) || (v > 0))) {
                                // ignore NaN and Infinite values
                                if (v > fitMax) {
                                    fitMax = v;
                                }
                                if (v < fitMin) {
                                    fitMin = v;
                                }
                            }
                        }
                    }
                } // end if (values instanceof long[])
                else if (values instanceof float[]) {
                    float[] array = (float[]) values;
                    for (j = 0; j < dimY; j++) {
                        for (i = 0; i < dimX; i++) {
                            double v = array[(j * dimX) + i];
                            // DATAREDUC-856: in case of strictly positive bounds, exclude zero and negative values
                            if ((!Double.isNaN(v)) && (!Double.isInfinite(v))
                                    && ((!strictlyPositiveBounds) || (v > 0))) {
                                // ignore NaN and Infinite values
                                if (v > fitMax) {
                                    fitMax = v;
                                }
                                if (v < fitMin) {
                                    fitMin = v;
                                }
                            }
                        }
                    }
                } // end if (values instanceof float[])
                else if (values instanceof double[]) {
                    double[] array = (double[]) values;
                    for (j = 0; j < dimY; j++) {
                        for (i = 0; i < dimX; i++) {
                            double v = array[(j * dimX) + i];
                            // DATAREDUC-856: in case of strictly positive bounds, exclude zero and negative values
                            if ((!Double.isNaN(v)) && (!Double.isInfinite(v))
                                    && ((!strictlyPositiveBounds) || (v > 0))) {
                                // ignore NaN and Infinite values
                                if (v > fitMax) {
                                    fitMax = v;
                                }
                                if (v < fitMin) {
                                    fitMin = v;
                                }
                            }
                        }
                    }
                } // end if (values instanceof double[])
                else if (values instanceof Number[]) {
                    Number[] array = (Number[]) values;
                    for (j = 0; j < dimY; j++) {
                        for (i = 0; i < dimX; i++) {
                            double v = extractDouble(array[(j * dimX) + i]);
                            // DATAREDUC-856: in case of strictly positive bounds, exclude zero and negative values
                            if ((!Double.isNaN(v)) && (!Double.isInfinite(v))
                                    && ((!strictlyPositiveBounds) || (v > 0))) {
                                // ignore NaN and Infinite values
                                if (v > fitMax) {
                                    fitMax = v;
                                }
                                if (v < fitMin) {
                                    fitMin = v;
                                }
                            }
                        }
                    }
                } // end if (values instanceof Number[])
                if (fitMin > fitMax) {
                    if (strictlyPositiveBounds) {
                        fitMin = 1;
                        fitMax = 1;
                    } else {
                        fitMin = 0;
                        fitMax = 0;
                    }
                }
            } // end if (autoBestFit)
            else {
                fitMin = bfMin;
                fitMax = bfMax;
            }

            // DATAREDUC-856: in case of strictly positive bounds but no autoBestFit,
            // recompute only negative or zero bound
            if (strictlyPositiveBounds && !autoBestFit) {
                if (bfMin > 0) {
                    fitMin = bfMin;
                }
                if (bfMax > 0) {
                    fitMax = bfMax;
                }
            }

            bfa0 = -fitMin;
            if (fitMax == fitMin) {
                // Uniform picture
                bfa1 = 0.0;
                result[0] = fitMin;
                result[1] = fitMax + 1.0;
            } else {
                bfa1 = (65536.0) / (fitMax - fitMin);
                result[0] = fitMin;
                result[1] = fitMax;
            }
            // Setting bfMin and bfMax seems to be mandatory to avoid an ImageViewer's auto best fit bug (DATAREDUC-315)
            bfMin = fitMin;
            bfMax = fitMax;
        } // end if ((values != null) && bestFit)
        return result;
    }

    public void applyBestFitToGradient(Object values, int dimX, int dimY, boolean bestFit, boolean autoBestFit,
            boolean strictlyPositiveBounds) {
        double[] minAndMax = preComputeBestFit(values, dimX, dimY, bestFit, autoBestFit, strictlyPositiveBounds);
        if (minAndMax != null) {
            if (gradientTool != null) {
                gradientTool.getAxis().getAttributes().setMinimum(minAndMax[0]);
                gradientTool.getAxis().getAttributes().setMaximum(minAndMax[1]);
                gradientTool.revalidate();// label size may change
                gradientTool.repaint();
            }
        }
    }

    public BufferedImage buildZoom(Object values, int dimX, int dimY, Rectangle zoomBounds, boolean bestFit,
            boolean autoBestFit, int zoomFactor, boolean strictlyPositiveBounds) {
        BufferedImage zoomImg = null;
        if ((values != null) && (zoomBounds != null) && (zoomBounds.width > 0) && (zoomBounds.height > 0)) {
            zoomImg = new BufferedImage(zoomBounds.width * zoomFactor, zoomBounds.height * zoomFactor,
                    BufferedImage.TYPE_INT_RGB);
            int[] rgb = new int[zoomBounds.width * zoomFactor];
            applyBestFitToGradient(values, dimX, dimY, bestFit, autoBestFit, strictlyPositiveBounds);
            // Fill the image
            int valIndex = 0;
            if (zoomBounds.y > 0) {
                valIndex += zoomBounds.y * dimX;
            }

            if (values instanceof byte[]) {
                byte[] array = (byte[]) values;
                for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++) {
                    if (zoomBounds.x > 0) {
                        valIndex += zoomBounds.x;
                    }
                    for (int i = zoomBounds.x; i < zoomBounds.x + zoomBounds.width; i++) {
                        if (negative) {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~bestFit(val)) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~(int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        } else {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[bestFit(val)];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[((int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        }
                    }
                    for (int k = 0; k < zoomFactor; k++) {
                        zoomImg.setRGB(0, (j - zoomBounds.y) * zoomFactor + k, zoomBounds.width * zoomFactor, 1, rgb, 0,
                                zoomBounds.width * zoomFactor);
                    }
                    if (zoomBounds.x + zoomBounds.width < dimX) {
                        valIndex += dimX - (zoomBounds.x + zoomBounds.width);
                    }
                } // end for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++)
            } // end if (values instanceof byte[])
            else if (values instanceof short[]) {
                short[] array = (short[]) values;
                for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++) {
                    if (zoomBounds.x > 0) {
                        valIndex += zoomBounds.x;
                    }
                    for (int i = zoomBounds.x; i < zoomBounds.x + zoomBounds.width; i++) {
                        if (negative) {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~bestFit(val)) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~(int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        } else {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[bestFit(val)];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[((int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        }
                    }
                    for (int k = 0; k < zoomFactor; k++) {
                        zoomImg.setRGB(0, (j - zoomBounds.y) * zoomFactor + k, zoomBounds.width * zoomFactor, 1, rgb, 0,
                                zoomBounds.width * zoomFactor);
                    }
                    if (zoomBounds.x + zoomBounds.width < dimX) {
                        valIndex += dimX - (zoomBounds.x + zoomBounds.width);
                    }
                } // end for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++)
            } // end if (values instanceof short[])
            else if (values instanceof int[]) {
                int[] array = (int[]) values;
                for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++) {
                    if (zoomBounds.x > 0) {
                        valIndex += zoomBounds.x;
                    }
                    for (int i = zoomBounds.x; i < zoomBounds.x + zoomBounds.width; i++) {
                        if (negative) {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~bestFit(val)) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~(int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        } else {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[bestFit(val)];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[((int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        }
                    }
                    for (int k = 0; k < zoomFactor; k++) {
                        zoomImg.setRGB(0, (j - zoomBounds.y) * zoomFactor + k, zoomBounds.width * zoomFactor, 1, rgb, 0,
                                zoomBounds.width * zoomFactor);
                    }
                    if (zoomBounds.x + zoomBounds.width < dimX) {
                        valIndex += dimX - (zoomBounds.x + zoomBounds.width);
                    }
                } // end for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++)
            } // end if (values instanceof int[])
            else if (values instanceof long[]) {
                long[] array = (long[]) values;
                for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++) {
                    if (zoomBounds.x > 0) {
                        valIndex += zoomBounds.x;
                    }
                    for (int i = zoomBounds.x; i < zoomBounds.x + zoomBounds.width; i++) {
                        if (negative) {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~bestFit(val)) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~(int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        } else {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[bestFit(val)];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[((int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        }
                    }
                    for (int k = 0; k < zoomFactor; k++) {
                        zoomImg.setRGB(0, (j - zoomBounds.y) * zoomFactor + k, zoomBounds.width * zoomFactor, 1, rgb, 0,
                                zoomBounds.width * zoomFactor);
                    }
                    if (zoomBounds.x + zoomBounds.width < dimX) {
                        valIndex += dimX - (zoomBounds.x + zoomBounds.width);
                    }
                } // end for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++)
            } // end if (values instanceof long[])
            else if (values instanceof float[]) {
                float[] array = (float[]) values;
                for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++) {
                    if (zoomBounds.x > 0) {
                        valIndex += zoomBounds.x;
                    }
                    for (int i = zoomBounds.x; i < zoomBounds.x + zoomBounds.width; i++) {
                        if (negative) {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~bestFit(val)) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~(int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        } else {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[bestFit(val)];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[((int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        }
                    }
                    for (int k = 0; k < zoomFactor; k++) {
                        zoomImg.setRGB(0, (j - zoomBounds.y) * zoomFactor + k, zoomBounds.width * zoomFactor, 1, rgb, 0,
                                zoomBounds.width * zoomFactor);
                    }
                    if (zoomBounds.x + zoomBounds.width < dimX) {
                        valIndex += dimX - (zoomBounds.x + zoomBounds.width);
                    }
                } // end for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++)
            } // end if (values instanceof float[])
            else if (values instanceof double[]) {
                double[] array = (double[]) values;
                for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++) {
                    if (zoomBounds.x > 0) {
                        valIndex += zoomBounds.x;
                    }
                    for (int i = zoomBounds.x; i < zoomBounds.x + zoomBounds.width; i++) {
                        if (negative) {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~bestFit(val)) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~(int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        } else {
                            if (bestFit) {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[bestFit(val)];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = array[valIndex++];
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[((int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        }
                    }
                    for (int k = 0; k < zoomFactor; k++) {
                        zoomImg.setRGB(0, (j - zoomBounds.y) * zoomFactor + k, zoomBounds.width * zoomFactor, 1, rgb, 0,
                                zoomBounds.width * zoomFactor);
                    }
                    if (zoomBounds.x + zoomBounds.width < dimX) {
                        valIndex += dimX - (zoomBounds.x + zoomBounds.width);
                    }
                } // end for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++)
            } // end if (values instanceof double[])
            else if (values instanceof Number[]) {
                Number[] array = (Number[]) values;
                for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++) {
                    if (zoomBounds.x > 0) {
                        valIndex += zoomBounds.x;
                    }
                    for (int i = zoomBounds.x; i < zoomBounds.x + zoomBounds.width; i++) {
                        if (negative) {
                            if (bestFit) {
                                double val = extractDouble(array[valIndex++]);
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~bestFit(val)) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = extractDouble(array[valIndex++]);
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[(~(int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        } else {
                            if (bestFit) {
                                double val = extractDouble(array[valIndex++]);
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[bestFit(val)];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            } else {
                                double val = extractDouble(array[valIndex++]);
                                int p;
                                if (Double.isNaN(val)) {
                                    p = naNColor;
                                } else if (Double.isInfinite(val)) {
                                    if (val > 0) {
                                        p = positiveInfinityColor;
                                    } else {
                                        p = negativeInfinityColor;
                                    }
                                } else {
                                    p = gColormap[((int) val) & COLOR_MAX];
                                }
                                for (int k = 0; k < zoomFactor; k++) {
                                    rgb[(i - zoomBounds.x) * zoomFactor + k] = p;
                                }
                            }
                        }
                    }
                    for (int k = 0; k < zoomFactor; k++) {
                        zoomImg.setRGB(0, (j - zoomBounds.y) * zoomFactor + k, zoomBounds.width * zoomFactor, 1, rgb, 0,
                                zoomBounds.width * zoomFactor);
                    }
                    if (zoomBounds.x + zoomBounds.width < dimX) {
                        valIndex += dimX - (zoomBounds.x + zoomBounds.width);
                    }
                } // end for (int j = zoomBounds.y; j < zoomBounds.y + zoomBounds.height; j++)
            } // end if (values instanceof Number[])

        } // end if ((values != null) && (zoomBounds != null) && (zoomBounds.width > 0) && (zoomBounds.height > 0))
        return zoomImg;
    }

    public static double extractDouble(Number nb) {
        return nb == null ? MathConst.NAN_FOR_NULL : nb.doubleValue();
    }

    public void clean() {
        gradientTool = null;
        gColormap = null;
    }

}
