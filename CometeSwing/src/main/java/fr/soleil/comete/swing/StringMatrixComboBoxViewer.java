/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.lang.reflect.Array;

import javax.activation.UnsupportedDataTypeException;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.data.target.matrix.ITextMatrixComponent;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.information.MatrixInformation;

/**
 * This is a ComboBox type widget that manages {@link StringMatrix} to update its content.
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 */
public class StringMatrixComboBoxViewer extends ComboBox implements ITextMatrixComponent {

    private static final long serialVersionUID = 1479264704771305107L;

    private final AbstractMatrix<String> matrix;

    public StringMatrixComboBoxViewer() {
        super();
        matrix = new StringMatrix();
    }

    @Override
    public String[][] getStringMatrix() {
        String[][] result = null;
        if (matrix != null) {
            result = (String[][]) matrix.getValue();
        }
        return result;
    }

    @Override
    public void setStringMatrix(String[][] array) {
        if (matrix != null) {
            try {
                matrix.setValue(array);
                updateValueList();
            } catch (UnsupportedDataTypeException e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to set string matrix", e);
            }
        }
    }

    @Override
    public String[] getFlatStringMatrix() {
        String[] result = null;
        if (matrix != null) {
            result = (String[]) matrix.getFlatValue();
        }
        return result;
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        if (matrix != null) {
            try {
                matrix.setFlatValue(value, width, height);
                updateValueList();
            } catch (UnsupportedDataTypeException e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to set flat string matrix", e);
            }
        }
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        int height = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if (value != null) {
                height = value.length;
            }
        }
        return height;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int width = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if ((value != null) && (value.length > 0)) {
                width = Array.getLength(value[0]);
            }
        }
        return width;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    private void updateValueList() {
        setValueList((Object[]) matrix.getFlatValue());
    }

    @Override
    protected void warnMediators() {
        warnMediators(new MatrixInformation(this, new String[][] { { String.valueOf(getSelectedValue()) } }));
    }

    /**
     * Main class, so you can have an example.
     */
    public static void main(String[] args) {
        JPanel panel = new JPanel();

        StringMatrixComboBoxViewer comboBox = new StringMatrixComboBoxViewer();
        panel.add(comboBox);

        JFrame frame = new JFrame();

        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setTitle(comboBox.getClass().getSimpleName() + " test");
    }

}
