/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.view.shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Paint;
import java.awt.geom.Line2D;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.chart.data.AbstractDataView;
import fr.soleil.comete.swing.chart.view.graphics.MarkerGraphics;

/**
 * A class used to generate shapes, including marker shapes.
 * 
 * @author Rapha&euml;l GIRARDOT
 * @see MarkerGraphics
 */
public class ShapeGenerator {

    protected static AbstractShape generateEquilateralTriangleMarker(int markerSize, int markerSize2, Color color) {
        int sqrt3Size3 = (int) (markerSize * Math.sqrt(3) / 3);
        int sqrt3Size6 = (int) (markerSize * Math.sqrt(3) / 6);
        return new Polygon(null, new int[] { -markerSize2, 0, markerSize2 },
                new int[] { sqrt3Size6, sqrt3Size3, sqrt3Size6 }, 3, true, color);
    }

    protected static AbstractShape generateIsoscelesTriangleMarker(int markerSize, int markerSize2, Color color) {
        return new Polygon(null, new int[] { -markerSize2, 0, markerSize2 },
                new int[] { markerSize2, -markerSize2, markerSize2 }, 3, true, color);
    }

    public static AbstractShape generateMarkerShape(int markerType, int markerSize, Color color) {
        AbstractShape axisShape;
        int markerSize2 = markerSize / 2;
        int markerSizeSqrt2Div2 = (int) (markerSize * Math.sqrt(2) / 2);
        switch (markerType) {
            case IChartViewer.MARKER_DOT:
                axisShape = new Oval(null, new java.awt.Rectangle(-markerSize2, -markerSize2, markerSize, markerSize),
                        true, color);
                break;
            case IChartViewer.MARKER_BOX:
                axisShape = new Rectangle(null,
                        new java.awt.Rectangle(-markerSize2, -markerSize2, markerSize, markerSize), true, color);
                break;
            case IChartViewer.MARKER_TRIANGLE:
                axisShape = generateIsoscelesTriangleMarker(markerSize, markerSize2, color);
                break;
            case IChartViewer.MARKER_DIAMOND:
                axisShape = new Polygon(null, new int[] { -markerSizeSqrt2Div2, 0, markerSizeSqrt2Div2, 0 },
                        new int[] { 0, -markerSizeSqrt2Div2, 0, markerSizeSqrt2Div2 }, 4, true, color);
                break;
            case IChartViewer.MARKER_STAR:
                ShapeList<Line> listStar = new ShapeList<>(null, Line.class);
                listStar.add(
                        new Line(null, new Line2D.Double(-markerSize2, markerSize2, markerSize2, -markerSize2), color));
                listStar.add(
                        new Line(null, new Line2D.Double(markerSize2, markerSize2, -markerSize2, -markerSize2), color));
                listStar.add(new Line(null, new Line2D.Double(0, -markerSize2, 0, markerSize2), color));
                listStar.add(new Line(null, new Line2D.Double(-markerSize2, 0, markerSize2, 0), color));
                axisShape = listStar;
                break;
            case IChartViewer.MARKER_VERT_LINE:
                axisShape = new Line(null, new Line2D.Double(0, -markerSize2, 0, markerSize2), color);
                break;
            case IChartViewer.MARKER_HORIZ_LINE:
                axisShape = new Line(null, new Line2D.Double(-markerSize2, 0, markerSize2, 0), color);
                break;
            case IChartViewer.MARKER_CROSS:
                ShapeList<Line> listCross = new ShapeList<>(null, Line.class);
                listCross.add(new Line(null, new Line2D.Double(0, -markerSize2, 0, markerSize2), color));
                listCross.add(new Line(null, new Line2D.Double(-markerSize2, 0, markerSize2, 0), color));
                axisShape = listCross;
                break;
            case IChartViewer.MARKER_CIRCLE:
                axisShape = new Oval(null,
                        new java.awt.Rectangle(-markerSize2, -markerSize2, markerSize + 1, markerSize + 1), false,
                        color);
                break;
            case IChartViewer.MARKER_SQUARE:
                axisShape = new Rectangle(null,
                        new java.awt.Rectangle(-markerSize2, -markerSize2, markerSize, markerSize), false, color);
                break;
            default:
                axisShape = null;
                break;
        }
        return axisShape;
    }

    public static AbstractShape generateMarkerShape(int markerType, int markerSize, int x, int y, Color color) {
        AbstractShape axisShape = generateMarkerShape(markerType, markerSize, color);
        if (axisShape != null) {
            axisShape.setX(x);
            axisShape.setY(y);
        }
        return axisShape;
    }

    public static ShapeList<AbstractShape> generateDataViewBarShape(AbstractDataView view, int barWidth,
            BasicStroke basicStroke, Paint fillPattern, int yOrigin, int x, int y) {
        ShapeList<AbstractShape> result = null;
        if (view.getViewType() == IChartViewer.TYPE_BAR) {
            result = new ShapeList<>(null, AbstractShape.class);
            Rectangle barRect = generateBarShape(fillPattern, barWidth, view.getFillColor(), view.getFillStyle(),
                    yOrigin, x, y);
            if (barRect != null) {
                result.add(barRect);
            }
            // Draw bar border
            if (view.getAdaptedLineWidth() > 0) {
                result.add(generateBarBorderShape(barWidth, yOrigin, x, y, view.getColor(), basicStroke));
            }
            if (result.isEmpty()) {
                result = null;
            }
        }
        return result;
    }

    public static Rectangle generateBarShape(Paint fillPattern, int barWidth, Color background, int fillStyle,
            int yOrigin, int x, int y) {
        Rectangle result = null;
        if (fillStyle != IChartViewer.FILL_STYLE_NONE) {
            if (y > yOrigin) {
                result = new Rectangle(null, new java.awt.Rectangle(x - barWidth / 2, yOrigin, barWidth, y - yOrigin),
                        true, background);
            } else {
                result = new Rectangle(null, new java.awt.Rectangle(x - barWidth / 2, y, barWidth, (yOrigin - y)), true,
                        background);
            }
            if (fillPattern != null) {
                result.setPattern(fillPattern);
            } else {
                result.setColor(background);
            }
        }
        return result;
    }

    public static ShapeList<Line> generateBarBorderShape(int barWidth, int y0, int x, int y, Color color,
            BasicStroke basicStroke) {
        ShapeList<Line> result = new ShapeList<>(null, Line.class);
        result.add(new Line(null, new Line2D.Double(x - barWidth / 2, y, x + barWidth / 2, y), color, basicStroke));
        result.add(new Line(null, new Line2D.Double(x + barWidth / 2, y, x + barWidth / 2, y0), color, basicStroke));
        result.add(new Line(null, new Line2D.Double(x + barWidth / 2, y0, x - barWidth / 2, y0), color, basicStroke));
        result.add(new Line(null, new Line2D.Double(x - barWidth / 2, y0, x - barWidth / 2, y), color, basicStroke));
        return result;
    }

    public static ShapeList<Polyline> generateDataViewPolylineShape(AbstractDataView view, BasicStroke basicStroke,
            Paint fillPattern, int nb, int yOrg, int[] pointX, int[] pointY) {
        ShapeList<Polyline> result = null;
        if (nb > 0) {
            if (view.getViewType() == IChartViewer.TYPE_LINE) {
                result = generateDataViewPolylineShapeNoCheck(view, basicStroke, fillPattern, nb, yOrg, pointX, pointY);
            } else if (view.getViewType() == IChartViewer.TYPE_STAIRS) {
                int nb2 = 2 * nb - 1;
                int[] pointX2, pointY2;
                pointX2 = new int[nb2];
                pointY2 = new int[nb2];
                for (int i = 0; i < nb; i++) {
                    pointX2[2 * i] = pointX[i];
                    pointY2[2 * i] = pointY[i];
                    if (i < nb - 1) {
                        pointX2[2 * i + 1] = pointX[i + 1];
                        pointY2[2 * i + 1] = pointY[i];
                    }
                }
                result = generateDataViewPolylineShapeNoCheck(view, basicStroke, fillPattern, nb2, yOrg, pointX2,
                        pointY2);
            }
        }
        return result;
    }

    public static ShapeList<Polyline> generateDataViewPolylineShapeNoCheck(AbstractDataView view,
            BasicStroke basicStroke, Paint fillPattern, int nb, int yOrg, int[] pointX, int[] pointY) {
        ShapeList<Polyline> result = new ShapeList<>(null, Polyline.class);
        // Ensure drawing at least a pixel
        int nb2 = nb;
        int[] pointX2 = pointX;
        int[] pointY2 = pointY;
        if (nb == 1) {
            nb2 = 2;
            pointX2 = new int[] { pointX[0], pointX[0] };
            pointY2 = new int[] { pointY[0], pointY[0] };
        }
        // Draw surface
        if (view.getFillStyle() != IChartViewer.FILL_STYLE_NONE) {
            int[] Xs = new int[nb2 + 2];
            int[] Ys = new int[nb2 + 2];
            for (int i = 0; i < nb2; i++) {
                Xs[i + 1] = pointX2[i];
                Ys[i + 1] = pointY2[i];
            }
            Xs[0] = Xs[1];
            Ys[0] = yOrg;
            Xs[nb2 + 1] = Xs[nb2];
            Ys[nb2 + 1] = yOrg;
            Polygon polygon = new Polygon(null, Xs, Ys, nb2 + 2, true, view.getColor());
            if (fillPattern != null) {
                polygon.setPattern(fillPattern);
            }
        }
        if (view.getAdaptedLineWidth() > 0) {
            Polyline polyline = new Polyline(null, pointX2, pointY2, nb2, view.getColor());
            polyline.setBasicStroke(basicStroke);
            result.add(polyline);
        }
        return result;
    }

}
