/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.image.BufferedImage;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.chart.util.ChartUtils;

public class LineStyleListCellRenderer extends ImageableTypeListCellRenderer {

    private static final long serialVersionUID = -8026528482065405296L;

    private static final String SOLID = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.line.style.solid");
    private static final String DOTS = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.line.style.dots");
    private static final String SHORT_DASHES = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.line.style.dashes.short");
    private static final String LONG_DASHES = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.line.style.dashes.long");
    private static final String DOTS_DASHES = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.line.style.dashes.dots");

    public LineStyleListCellRenderer() {
        super();
    }

    @Override
    public String getText(int id) {
        String text;
        switch (id) {
            case IChartViewer.STYLE_SOLID:
                text = SOLID;
                break;
            case IChartViewer.STYLE_DOT:
                text = DOTS;
                break;
            case IChartViewer.STYLE_DASH:
                text = SHORT_DASHES;
                break;
            case IChartViewer.STYLE_LONG_DASH:
                text = LONG_DASHES;
                break;
            case IChartViewer.STYLE_DASH_DOT:
                text = DOTS_DASHES;
                break;
            default:
                text = Integer.toString(id);
                break;
        }
        return text;
    }

    @Override
    protected BufferedImage getImage(int id, Color bg, Color fg) {
        return createImage(ChartUtils.generateStroke(id), fg, 30, 16);
    }

    /**
     * Given a Stroke, create an image that shows it.
     * 
     * @param stroke the Stroke to draw on the Icon.
     * @param color the stroke color
     * @param width the width of the icon.
     * @param height the height of the icon.
     */
    protected static BufferedImage createImage(Stroke stroke, Color color, int width, int height) {
        BufferedImage bigImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) bigImage.getGraphics();

        if (stroke != null) {
            g.setPaint(color);
            g.setStroke(stroke);
            // horizontal drawing
            g.drawLine(0, height / 2, width, height / 2);
        }
        return bigImage;
    }

}
