/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.util.EventObject;

public class JTableRowEvent extends EventObject {

    private static final long serialVersionUID = -7491027105572317394L;

    private Object[] value;
    private final long when;
    private int columnIndex;

    public JTableRowEvent(Object source, long timeStamp, Object[] value, int columnChanged) {
        super(source);
        when = timeStamp;
        setValues(value);
        setColumnChanged(columnChanged);
    }

    public void setColumnChanged(int columnChanged) {
        columnIndex = columnChanged;
    }

    public void setValues(Object[] value) {
        this.value = value;
    }

    public Object[] getColumnChanged() {
        return value;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public long getTimeStamp() {
        return when;
    }

    public String getVersion() {
        return "$Id: ATKEvent.java,v 1.2 2009/01/26 17:54:41 poncet Exp $";
    }

}
