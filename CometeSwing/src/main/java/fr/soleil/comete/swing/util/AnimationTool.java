/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IPlayer;
import fr.soleil.comete.swing.Player;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.awt.animation.AVIOutputStream;
import fr.soleil.lib.project.awt.animation.AnimatedGifEncoder;
import fr.soleil.lib.project.awt.animation.QuickTimeWriter;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.SnapshotUtil;

/**
 * A class that can be used to make some animations
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AnimationTool {

    private static final String AVI_EXTENSION = "avi";
    private static final String QUICKTIME_EXTENSION = "mov";
    private static final String GIF_EXTENSION = "gif";

    private static final FileFilter AVI_FILE_FILTER = new FileFilter() {
        @Override
        public String getDescription() {
            return "AVI movie Files";
        }

        @Override
        public boolean accept(File f) {
            boolean accept = false;
            if (f != null) {
                accept = (f.isDirectory() || AVI_EXTENSION.equals(FileUtils.getExtension(f)));
            }
            return accept;
        }
    };

    private static final FileFilter QUICKTIME_FILE_FILTER = new FileFilter() {
        @Override
        public String getDescription() {
            return "QuickTime movie Files";
        }

        @Override
        public boolean accept(File f) {
            boolean accept = false;
            if (f != null) {
                accept = (f.isDirectory() || QUICKTIME_EXTENSION.equals(FileUtils.getExtension(f)));
            }
            return accept;
        }
    };

    private static final FileFilter GIF_FILE_FILTER = new FileFilter() {
        @Override
        public String getDescription() {
            return "Animated Gif Files";
        }

        @Override
        public boolean accept(File f) {
            boolean accept = false;
            if (f != null) {
                accept = (f.isDirectory() || GIF_EXTENSION.equals(FileUtils.getExtension(f)));
            }
            return accept;
        }
    };

    /**
     * Creates an animated movie of an {@link IPlayer}, in a separated {@link Thread}. The snapshot
     * is based on coordinates parameters.
     * 
     * @param player The {@link IPlayer}. Will not work if the {@link IPlayer} is not a
     *            {@link Component}
     * @param targetDirectory The directory in which to put the target video file.
     * @return The {@link File} that represents the generated file
     * @throws IOException If a problem occurred during File writing
     */
    public static void createAnimation(final IPlayer player, final String targetDirectory, int x, int y, int width,
            int height) throws IOException {
        final Rectangle rectangle = new Rectangle(x, y, width, height);
        new Thread("create animation for " + player + " in " + targetDirectory) {
            @Override
            public void run() {
                try {
                    createAnimation(player, targetDirectory, rectangle);
                } catch (IOException e) {
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .error("Failed to create animation for " + player + " in " + targetDirectory, e);
                }
            }
        }.start();
    }

    /**
     * Creates an animated movie of an {@link IPlayer}, and returns the generated {@link File}. The
     * snapshot is based on the player's bounds.
     * 
     * @param player The {@link IPlayer}. Will not work if the {@link IPlayer} is not a
     *            {@link Component}
     * @param targetDirectory The directory in which to put the target video file.
     * @return The {@link File} that represents the generated file
     * @throws IOException If a problem occurred during File writing
     */
    public static File createAnimation(IPlayer player, String targetDirectory) throws IOException {
        File selectedFile = null;
        selectedFile = createAnimation(player, targetDirectory, null);
        return selectedFile;
    }

    /**
     * Creates an animated movie of an {@link IPlayer}, and returns the generated {@link File}. The
     * snapshot is based on the donate {@link Rectangle}, but if this one is <code>null</code>, the
     * bounds of the player are taken.
     * 
     * @param player The {@link IPlayer}. Will not work if the {@link IPlayer} is not a
     *            {@link Component}
     * @param targetDirectory The directory in which to put the target video file.
     * @param bounds the position and the size of the window where the snapshot must be taken
     * @return The {@link File} that represents the generated file
     * @throws IOException If a problem occurred during File writing
     */
    public static File createAnimation(final IPlayer player, String targetDirectory, final Rectangle bounds)
            throws IOException {
        File selectedFile = null;
        final Component playerAsComponent = getSnapshotableComponent(player);
        if ((targetDirectory == null) || targetDirectory.trim().isEmpty()) {
            targetDirectory = System.getProperty("user.home");
        }
        File targetDir = new File(targetDirectory);
        if (!targetDir.exists()) {
            targetDir.mkdirs();
        }
        if (!targetDir.isDirectory()) {
            targetDir = targetDir.getParentFile();
        }
        JFileChooser chooser = new JFileChooser(targetDir);
        chooser.addChoosableFileFilter(GIF_FILE_FILTER);
        chooser.addChoosableFileFilter(AVI_FILE_FILTER);
        chooser.addChoosableFileFilter(QUICKTIME_FILE_FILTER);
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setFileFilter(GIF_FILE_FILTER);
        int selection = chooser.showSaveDialog(playerAsComponent);
        if (selection == JFileChooser.APPROVE_OPTION) {
            final ExceptionContainer exceptionContainer = new ExceptionContainer();
            selectedFile = chooser.getSelectedFile();
            FileFilter filter = chooser.getFileFilter();
            if (filter == AVI_FILE_FILTER) {
                selectedFile = getAdaptedFile(selectedFile, AVI_EXTENSION);
            } else if (filter == QUICKTIME_FILE_FILTER) {
                selectedFile = getAdaptedFile(selectedFile, QUICKTIME_EXTENSION);
            } else {
                selectedFile = getAdaptedFile(selectedFile, GIF_EXTENSION);
            }
            selection = JOptionPane.YES_OPTION;
            if (selectedFile.exists()) {
                selection = JOptionPane.showConfirmDialog(playerAsComponent,
                        selectedFile.getName() + " exists. Overwrite?", "Warning", JOptionPane.YES_NO_OPTION);
            }
            if (selection == JOptionPane.YES_OPTION) {
                String extension = FileUtils.getExtension(selectedFile);
                int max = player.getMaximum();
                final int delay = player.getPeriodInMs();
                if (GIF_EXTENSION.equals(extension)) {
                    final AnimatedGifEncoder gifEncoder = new AnimatedGifEncoder();
                    gifEncoder.start(selectedFile.getAbsolutePath());
                    gifEncoder.setDelay(player.getPeriodInMs());
                    if (player.isRepeatActivated()) {
                        gifEncoder.setRepeat(100);
                    }
                    for (int i = 0; i <= max; i++) {
                        player.setIndex(i);
                        // Wait for redraw events...
                        Runnable toWait = new Runnable() {
                            @Override
                            public void run() {
                                BufferedImage image = getSnapshot(player, bounds);
                                gifEncoder.addFrame(image);
                            }
                        };
                        // ...if possible
                        if (SwingUtilities.isEventDispatchThread()) {
                            toWait.run();
                        } else {
                            try {
                                SwingUtilities.invokeAndWait(toWait);
                            } catch (Exception e) {
                                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(
                                        "Failed to create animation for " + player + " in " + targetDirectory, e);
                            }
                        }
                    }
                    gifEncoder.finish();
                } else if (AVI_EXTENSION.equals(extension)) {
                    int frameRate = 1;
                    double scale = delay / 1000d;
                    int intScale = (int) scale;
                    int maxScale = 100000;
                    while ((scale != intScale) && (scale <= maxScale)) {
                        frameRate *= 10;
                        scale *= 10;
                        intScale = (int) scale;
                    }
                    final AVIOutputStream outputStream = new AVIOutputStream(selectedFile,
                            AVIOutputStream.VideoFormat.JPG);
                    try {
                        outputStream.setVideoCompressionQuality(1f);
                        outputStream.setFrameRate(frameRate);
                        outputStream.setTimeScale(intScale);
                        for (int i = 0; i <= max; i++) {
                            player.setIndex(i);
                            // Wait for redraw events...
                            Runnable toWait = new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        outputStream.writeFrame(getSnapshot(player, bounds));
                                    } catch (IOException e) {
                                        exceptionContainer.setException(e);
                                    }
                                }
                            };
                            // ...if possible
                            if (SwingUtilities.isEventDispatchThread()) {
                                toWait.run();
                            } else {
                                try {
                                    SwingUtilities.invokeAndWait(toWait);
                                } catch (Exception e) {
                                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(
                                            "Failed to create animation for " + player + " in " + targetDirectory, e);
                                }
                            }
                            if (exceptionContainer.getException() != null) {
                                throw exceptionContainer.getException();
                            }

                        }
                    } finally {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    }
                } else if (QUICKTIME_EXTENSION.equals(extension)) {
                    Dimension size = (bounds == null) ? playerAsComponent.getSize() : bounds.getSize();
                    int width = size.width, height = size.height;

                    final QuickTimeWriter writer = new QuickTimeWriter(selectedFile);
                    BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                    Graphics2D g = img.createGraphics();
                    try {
                        writer.addVideoTrack(QuickTimeWriter.VideoFormat.PNG, 1000, width, height);
                        writer.setSyncInterval(0, delay);
                        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                        for (int i = 0; i <= max; i++) {
                            g.setColor(Color.BLACK);
                            g.fillRect(0, 0, width, height);
                            player.setIndex(i);
                            // Wait for redraw events...
                            Runnable toWait = new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        writer.writeFrame(0, getSnapshot(player, bounds), delay);
                                    } catch (IOException e) {
                                        exceptionContainer.setException(e);
                                    }
                                }
                            };
                            // ...if possible
                            if (SwingUtilities.isEventDispatchThread()) {
                                toWait.run();
                            } else {
                                try {
                                    SwingUtilities.invokeAndWait(toWait);
                                } catch (Exception e) {
                                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(
                                            "Failed to create animation for " + player + " in " + targetDirectory, e);
                                }
                            }
                            if (exceptionContainer.getException() != null) {
                                throw exceptionContainer.getException();
                            }
                        }
                    } finally {
                        if (writer != null) {
                            writer.close();
                        }
                        if (g != null) {
                            g.dispose();
                        }
                        img.flush();
                    }
                }
            }

        }
        return selectedFile;
    }

    /**
     * Return the snapshot of an image in function of the origin of the bounds object
     * 
     * @param player the player object
     * @param bounds the custom bounds for snapshot
     * @return the {@link BufferedImage} produced by the snapshot
     */
    private static BufferedImage getSnapshot(IPlayer player, Rectangle bounds) {
        BufferedImage result = null;
        Component playerComponent = getSnapshotableComponent(player);
        if (bounds != null) {
            result = SnapshotUtil.getRobotSnapshot(bounds);
        } else if (playerComponent != null) {
            result = SnapshotUtil.getSnapshot(playerComponent);
        }
        return result;
    }

    /**
     * Returns the {@link Component} that can be used to make snapshots/videos for an
     * {@link IPlayer}
     * 
     * @param player The {@link IPlayer}
     * @return A {@link Component}. <code>null</code> when there is no valid {@link Component} for
     *         the given {@link IPlayer}
     */
    private static Component getSnapshotableComponent(IPlayer player) {
        Component playerAsComponent;
        if (player instanceof Player) {
            playerAsComponent = ((Player) player).getVideoComponent();
        } else if (player instanceof Component) {
            playerAsComponent = (Component) player;
        } else {
            playerAsComponent = null;
        }
        return playerAsComponent;
    }

    private static File getAdaptedFile(File toCheck, String extension) {
        File result;
        if (extension.equals(FileUtils.getExtension(toCheck))) {
            result = toCheck;
        } else {
            result = new File(toCheck.getAbsolutePath() + ChartUtils.DOT + extension);
        }
        return result;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static class ExceptionContainer {
        private IOException exception = null;

        public IOException getException() {
            return exception;
        }

        public void setException(IOException exception) {
            this.exception = exception;
        }
    }

}
