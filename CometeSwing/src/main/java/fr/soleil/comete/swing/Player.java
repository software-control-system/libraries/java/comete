/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.event.PlayerEvent;
import fr.soleil.comete.definition.event.WheelSwitchEvent;
import fr.soleil.comete.definition.listener.IPlayerListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.listener.IWheelSwitchListener;
import fr.soleil.comete.definition.util.SnapshotEventDelegate;
import fr.soleil.comete.definition.widget.IPlayer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.IPlayerAnimationBehavior;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.stackviewer.PlayerAnimationComponentBehavior;
import fr.soleil.lib.project.awt.layout.VerticalFlowLayout;

/**
 * This class allows to manage a JSlider with buttons:
 * <ul>
 * <li>"previous step"</li>
 * <li>"next step"</li>
 * <li>"first step"</li>
 * <li>"last step"</li>
 * <li>"play"</li>
 * </ul>
 * 
 * @author MARECHAL
 * 
 */
public class Player extends AbstractPanel implements IPlayer, ChangeListener {

    private static final long serialVersionUID = 1425851873368624018L;

    protected static final ImageIcon SNAPSHOT_ICON = new ImageIcon(
            Player.class.getResource("/com/famfamfam/silk/film_save.png"));
    protected static final String SNAPSHOT_TOOLTIP = "Generate an animated file (.gif, .avi or .mov)";

    protected Slider slider;

    protected JPanel buttonPanel;
    protected JButton btFirst;
    protected JButton btLast;
    protected JButton btPrevious;
    protected JButton btNext;
    protected JButton btPeriodSetting;

    protected WheelSwitch wheelswitch;

    protected JButton btPlay;
    protected JButton btStop;
    protected JToggleButton tbtRepeat;

    protected JButton btGenerateAnimation;
    protected IPlayerAnimationBehavior animationBehavior;

    protected int periodInMs;

    // timer which contains timertask to increment selected value in slider
    protected Timer timerIncrementSlider;
    // boolean which set repeat mode
    // (the reading will continue until the user click on an another button)
    protected boolean bRepeat;

    protected final List<IPlayerListener> listeners;

    protected int index;

    protected java.awt.Component videoComponent;

    // Snapshot management
    protected final SnapshotEventDelegate snapshotEventDelegate;
    protected String snapshotDirectory;
    protected String snapshotFile;

    /**
     * Default constructor
     */
    public Player() {
        this(null);
    }

    /**
     * Constructor
     * 
     * @param slider The {@link Slider} to manage
     */
    public Player(Slider slider) {
        super();
        videoComponent = this;
        snapshotEventDelegate = new SnapshotEventDelegate(this, null);
        snapshotDirectory = ChartUtils.DOT;
        snapshotFile = null;
        index = -1;
        buttonPanel = null;
        btFirst = null;
        btLast = null;
        btPrevious = null;
        btNext = null;
        btPeriodSetting = null;
        btPlay = null;
        btStop = null;
        tbtRepeat = null;
        periodInMs = 40;
        timerIncrementSlider = null;
        bRepeat = false;
        btGenerateAnimation = null;
        wheelswitch = null;
        this.slider = slider;
        animationBehavior = new PlayerAnimationComponentBehavior();
        listeners = new ArrayList<IPlayerListener>();
        buildGUI();
        setSliderStep(getSlider().getMinorTickSpacing());
        getSlider().addChangeListener(this);
        getSlider().addPropertyChangeListener(new WheelSwitchPropertyListener());
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        java.awt.Component[] components = getComponents();
        if (components != null) {
            for (java.awt.Component comp : components) {
                setComponentsEnabled(comp, enabled);
            }
        }
    }

    protected void setComponentsEnabled(java.awt.Component comp, boolean enabled) {
        if ((comp != null) && (comp != this)) {
            comp.setEnabled(enabled);
            if (comp instanceof Container) {
                java.awt.Component[] components = ((Container) comp).getComponents();
                for (java.awt.Component child : components) {
                    setComponentsEnabled(child, enabled);
                }
            }
        }
    }

    /**
     * build all buttons to control slider and add it to the panel
     */
    public void buildGUI() {
        setLayout(new GridBagLayout());
        GridBagConstraints buttonsConstraints = new GridBagConstraints();
        buttonsConstraints.fill = GridBagConstraints.HORIZONTAL;
        buttonsConstraints.gridx = 0;
        buttonsConstraints.gridy = 0;
        buttonsConstraints.weightx = 1;
        buttonsConstraints.weighty = 0;
        add(getButtonPanel(), buttonsConstraints);
        GridBagConstraints sliderConstraints = new GridBagConstraints();
        sliderConstraints.fill = GridBagConstraints.HORIZONTAL;
        sliderConstraints.gridx = 0;
        sliderConstraints.gridy = 1;
        sliderConstraints.weightx = 1;
        sliderConstraints.weighty = 0;
        sliderConstraints.anchor = GridBagConstraints.NORTH;
        add((JComponent) getSlider(), sliderConstraints);
        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.BOTH;
        glueConstraints.gridx = 0;
        glueConstraints.gridy = 2;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 1;
        glueConstraints.anchor = GridBagConstraints.NORTH;
        add(Box.createGlue(), glueConstraints);
    }

    public WheelSwitch getWheelSwitch() {
        if (wheelswitch == null) {
            wheelswitch = new WheelSwitch();
            wheelswitch.setVisible(false);

            wheelswitch.setFormat("%d");
            // wheelswitch.setM
            wheelswitch.setNumberValue(0.0);
            wheelswitch.setMinValue(-1); // Defined the value min of the
                                         // WheelSwitch

            wheelswitch.addWheelSwitchListener(new IWheelSwitchListener() {

                @Override
                public void valueChange(WheelSwitchEvent arg0) {

                    // Slider slider= Player.getSlider();
                    if (slider != null) {
                        double value = arg0.getValue();
                        slider.setValue(Double.valueOf(value).intValue());
                    }
                }
            });

        }

        return wheelswitch;

    }

    public JPanel getButtonPanel() {
        if (buttonPanel == null) {
            buttonPanel = new JPanel(new VerticalFlowLayout());
            buttonPanel.add(getWheelSwitch());
            buttonPanel.add(getBtFirstStep());
            buttonPanel.add(getBtPreviousStep());
            buttonPanel.add(getBtNextStep());
            buttonPanel.add(getBtLastStep());
            buttonPanel.add(getBtSettingPeriod());

            // add extra space (actually 2 default spacing of flowlayout)
            buttonPanel.add(Box.createRigidArea(new Dimension(0, 0)));
            buttonPanel.add(getBtPlay());
            buttonPanel.add(getBtStop());
            buttonPanel.add(getTbtRepeat());
            buttonPanel.add(getBtGenerateAnimation());
        }
        return buttonPanel;
    }

    /**
     * Sets this {@link Player}'s {@link Slider}
     * 
     * @param slider
     *            the {@link Slider} to set
     */
    public void setSlider(Slider slider) {
        if (slider != null) {
            this.slider = slider;
        }
    }

    /**
     * Create and return button which allows toset the period
     * 
     * @return A {@link JButton}
     */
    public JButton getBtSettingPeriod() {
        if (btPeriodSetting == null) {
            btPeriodSetting = new JButton(String.valueOf(periodInMs + " ms"));
            btPeriodSetting.setToolTipText("Set the period");
            btPeriodSetting.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setPeriod();
                }
            });
        }
        return btPeriodSetting;
    }

    /**
     * Create and return button which allows to go to the first record
     * 
     * @return A {@link JButton}
     */
    public JButton getBtFirstStep() {
        if (btFirst == null) {
            btFirst = new JButton(new ImageIcon(this.getClass()
                    .getResource("/org/tango-project/tango-icon-theme/16x16/actions/media-skip-backward.png")));
            btFirst.setEnabled(false);
            btFirst.setToolTipText("Go to the first step");
            btFirst.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    gotoFirst();
                }
            });
        }
        return btFirst;
    }

    /**
     * Create and return button which allows to go to the last record
     * 
     * @return A {@link JButton}
     */
    public JButton getBtLastStep() {
        if (btLast == null) {
            btLast = new JButton(new ImageIcon(Player.class
                    .getResource("/org/tango-project/tango-icon-theme/16x16/actions/media-skip-forward.png")));
            btLast.setToolTipText("Go to the last step");
            btLast.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    gotoLast();
                }
            });
        }
        return btLast;
    }

    /**
     * Create and return button which allows to go to the previous record
     * 
     * @return A {@link JButton}
     */
    public JButton getBtPreviousStep() {
        if (btPrevious == null) {
            btPrevious = new JButton(new ImageIcon(Player.class
                    .getResource("/org/tango-project/tango-icon-theme/16x16/actions/media-seek-backward.png")));
            btPrevious.setEnabled(false);
            btPrevious.setToolTipText("Go to the previous step");
            btPrevious.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    previous();
                }
            });
        }
        return btPrevious;
    }

    /**
     * Create and return button which allows to go to the next record
     * 
     * @return A {@link JButton}
     */
    public JButton getBtNextStep() {
        if (btNext == null) {
            btNext = new JButton(new ImageIcon(Player.class
                    .getResource("/org/tango-project/tango-icon-theme/16x16/actions/media-seek-forward.png")));
            btNext.setToolTipText("Go to the next step");
            btNext.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    next();
                }
            });
        }
        return btNext;
    }

    /**
     * Create and return button which allows to read all values
     * 
     * @return A {@link JButton}
     */
    public JButton getBtPlay() {
        if (btPlay == null) {
            btPlay = new JButton(new ImageIcon(this.getClass()
                    .getResource("/org/tango-project/tango-icon-theme/16x16/actions/media-playback-start.png")));
            btPlay.setToolTipText("Play");
            btPlay.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    play();
                }
            });
        }
        return btPlay;
    }

    /**
     * Create timer with timertask which increments selected value in slider to
     * max value
     */
    public Timer getTimerIncremtSlider() {
        if (timerIncrementSlider == null) {
            timerIncrementSlider = new Timer(periodInMs, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // increment selected value to max value
                    if (slider.getValue() < slider.getMaximum()) {
                        slider.setValue((int) (slider.getValue() + getSliderStep()));
                    } else if (slider.getValue() == slider.getMaximum()) {
                        if (bRepeat) {
                            slider.setValue(slider.getMinimum());
                        } else {
                            timerIncrementSlider.stop();
                            btStop.setEnabled(false);
                        }
                    }
                }
            });

        }
        return timerIncrementSlider;
    }

    /**
     * Create and return button which allows to stop
     * "play action( read all values)"
     * 
     * @return a {@link JButton}
     */
    public JButton getBtStop() {
        if (btStop == null) {
            btStop = new JButton(new ImageIcon(this.getClass()
                    .getResource("/org/tango-project/tango-icon-theme/16x16/actions/media-playback-stop.png")));
            btStop.setEnabled(false);
            btStop.setToolTipText("Stop");
            btStop.addActionListener(getActionStop());
        }
        return btStop;
    }

    /**
     * Create actionlistener which stops reading and disable bt stop
     * 
     * @return ActionListener
     */
    public ActionListener getActionStop() {
        ActionListener listenerStop = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stop();
            }
        };
        return listenerStop;
    }

    public JButton getBtGenerateAnimation() {
        if (btGenerateAnimation == null) {
            btGenerateAnimation = new JButton(SNAPSHOT_ICON);
            btGenerateAnimation.setToolTipText(SNAPSHOT_TOOLTIP);
            final IPlayer player = this;
            btGenerateAnimation.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    // This must be done in a separated Thread to avoid UI
                    // display lock
                    new Thread("Generate animation for " + player) {
                        @Override
                        public void run() {
                            if (animationBehavior != null) {
                                snapshotEventDelegate.changeSnapshotLocation(animationBehavior.generateAnimation(player,
                                        snapshotEventDelegate.getSnapshotDirectory()));
                            }
                        };
                    }.start();
                }
            });
            btGenerateAnimation.setVisible(false);
        }
        return btGenerateAnimation;
    }

    /**
     * Create and return button which allows to repeat the reading
     * 
     * @return JToggleButton
     */
    public JToggleButton getTbtRepeat() {
        if (tbtRepeat == null) {
            tbtRepeat = new JToggleButton(
                    new ImageIcon(this.getClass().getResource("/fr/soleil/comete/icons/repeat.png")));
            tbtRepeat.setToolTipText("Repeat");
            tbtRepeat.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    bRepeat = tbtRepeat.isSelected();
                }
            });
        }
        return tbtRepeat;
    }

    /**
     * Setting period
     */
    public void setPeriod() {
        String newValue = JOptionPane.showInputDialog(this, "Set the new period in millisecond",
                String.valueOf(periodInMs));
        try {
            int newPeriod = Integer.parseInt(newValue);
            setPeriodInMs(newPeriod);
        } catch (Exception e) {
            // nothing to do: ignore exception
        }
    }

    /**
     * Returns the slider step
     * 
     * @return A <code>double</code>
     */
    public double getSliderStep() {
        return slider.getStep();
    }

    /**
     * Sets the slider step
     * 
     * @param slider_step
     *            The step to set
     */
    public void setSliderStep(double slider_step) {
        slider.setStep(slider_step);
    }

    public void setMinimum(int minimum) {
        getSlider().setMinimum(minimum);
    }

    public void setMaximum(int maximum) {
        getSlider().setMaximum(maximum);

    }

    /**
     * Update buttons (enable/disable) in function of the specified selected
     * index.
     * 
     * @param index
     *            The selected index
     */

    public void updateStateButtonsBar(int index) {
        if (slider.getMaximum() == slider.getMinimum()) {
            // disable all buttons if min = max
            getBtNextStep().setEnabled(false);
            getBtLastStep().setEnabled(false);
            getBtFirstStep().setEnabled(false);
            getBtPreviousStep().setEnabled(false);
        } else {
            if (index == slider.getMaximum()) {
                // max value selected
                getBtNextStep().setEnabled(false);
                getBtLastStep().setEnabled(false);
                getBtFirstStep().setEnabled(true);
                getBtPreviousStep().setEnabled(true);
            } else if (index == slider.getMinimum()) {
                // min value selected
                getBtNextStep().setEnabled(true);
                getBtLastStep().setEnabled(true);
                getBtFirstStep().setEnabled(false);
                getBtPreviousStep().setEnabled(false);
            } else {
                // another value selected
                getBtNextStep().setEnabled(true);
                getBtLastStep().setEnabled(true);
                getBtFirstStep().setEnabled(true);
                getBtPreviousStep().setEnabled(true);
            }
        }
    }

    @Override
    public void stateChanged(ChangeEvent event) {
        // selected value in slider
        int selectedIndex = getIndex();

        // update buttons in the associated record bar (previous, next, first,
        // last)
        updateStateButtonsBar(selectedIndex);

        // update the viewer only if the current slider's value has changed from
        // the previous one
        if ((index == -1) || (index != selectedIndex)) {
            index = selectedIndex;
            getWheelSwitch().setNumberValue(index);
            fireIndexChanged(new PlayerEvent(this));
        }
    }

    @Override
    public void initSlider(int stackSize) {
        getSlider().setMaximumDouble(stackSize);

    }

    @Override
    public int getPeriodInMs() {
        return periodInMs;
    }

    @Override
    public void setPeriodInMs(int periodInMs) {
        this.periodInMs = periodInMs;
        if (btPeriodSetting != null) {
            btPeriodSetting.setText(periodInMs + " ms");
        }
        // apply change immediately
        if (timerIncrementSlider != null) {
            timerIncrementSlider.setInitialDelay(periodInMs);
            timerIncrementSlider.setDelay(periodInMs);
        }
    }

    @Override
    public Slider getSlider() {
        if (slider == null) {
            this.slider = new Slider();
            this.slider.setPreferedMajorTickColor(Color.BLACK);
            this.slider.setMinimum(0);
            this.slider.setMinorTickSpacing(1);
            this.slider.setMajorTickSpacing(5);
            this.slider.setPaintTicks(true);
            this.slider.setPaintLabels(true);
            this.slider.setValue(0);
        }

        return slider;
    }

    @Override
    public void play() {
        slider.setValue(slider.getMinimum());
        btStop.setEnabled(true);
        getTimerIncremtSlider().start();
    }

    @Override
    public void stop() {
        getTimerIncremtSlider().stop();
        btStop.setEnabled(false);
    }

    @Override
    public void gotoFirst() {
        setIndex(slider.getMinimum());
    }

    @Override
    public void gotoLast() {
        setIndex(slider.getMaximum());
    }

    @Override
    public void previous() {
        setIndex((int) (slider.getValue() - getSliderStep()));
    }

    @Override
    public void next() {
        setIndex((int) (slider.getValue() + getSliderStep()));
    }

    @Override
    public void setIndex(int index) {
        stop();
        if ((slider.getValue() != index) && (index >= slider.getMinimum()) && (index <= slider.getMaximum())) {
            slider.setValue(index);
        }
    }

    @Override
    public int getIndex() {
        return slider.getValue();
    }

    @Override
    public boolean isRepeatActivated() {
        return bRepeat;
    }

    @Override
    public void setRepeatActivated(boolean activated) {
        bRepeat = activated;
        tbtRepeat.setSelected(activated);
    }

    @Override
    public boolean isEditingData() {
        return getBtStop().isEnabled();
    }

    @Override
    public void addPlayerListener(IPlayerListener listener) {
        synchronized (listeners) {
            if (!listeners.contains(listener)) {
                listeners.add(listener);
            }
        }
    }

    @Override
    public void fireIndexChanged(PlayerEvent event) {
        List<IPlayerListener> copy = new ArrayList<IPlayerListener>();
        synchronized (listeners) {
            copy.addAll(listeners);
        }
        for (IPlayerListener listener : copy) {
            listener.indexChanged(event);
        }
        copy.clear();
    }

    @Override
    public void removePlayerListener(IPlayerListener listener) {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }

    @Override
    public void setCometeBackground(final CometeColor color) {
        super.setCometeBackground(color);
        getButtonPanel().setBackground(ColorTool.getColor(color));
        getSlider().setCometeBackground(color);

    }

    @Override
    public void setPlayerAnimationBehavior(IPlayerAnimationBehavior behavior) {
        animationBehavior = behavior;
    }

    @Override
    public IPlayerAnimationBehavior getPlayerAnimationBehavior() {
        return animationBehavior;
    }

    @Override
    public int getMaximum() {
        return slider.getMaximum();
    }

    @Override
    public void setAnimationButtonVisible(boolean visible) {
        btGenerateAnimation.setVisible(visible);
    }

    @Override
    public boolean isAnimationButtonVisible() {
        return btGenerateAnimation.isVisible();
    }

    /**
     * Returns the {@link java.awt.Component Component} used to make videos and
     * snapshots
     * 
     * @return A {@link java.awt.Component Component}
     */
    public java.awt.Component getVideoComponent() {
        return videoComponent;
    }

    /**
     * Sets the {@link java.awt.Component Component} to use to make videos and
     * snapshots
     * 
     * @param videoComponent
     *            The {@link java.awt.Component Component} to use
     */
    public void setVideoComponent(java.awt.Component videoComponent) {
        this.videoComponent = (videoComponent == null ? this : videoComponent);
    }

    @Override
    public String getSnapshotDirectory() {
        return snapshotEventDelegate.getSnapshotDirectory();
    }

    @Override
    public void setSnapshotDirectory(String snapshotDirectory) {
        snapshotEventDelegate.setSnapshotDirectory(snapshotDirectory);
    }

    @Override
    public String getSnapshotFile() {
        return snapshotEventDelegate.getSnapshotFile();
    }

    @Override
    public void addSnapshotListener(ISnapshotListener listener) {
        snapshotEventDelegate.addSnapshotListener(listener);
    }

    @Override
    public void removeSnapshotListener(ISnapshotListener listener) {
        snapshotEventDelegate.removeSnapshotListener(listener);
    }

    public void setWheelSwitchVisible(boolean wheelswitchVisible) {
        getWheelSwitch().setVisible(wheelswitchVisible);

    }

    public boolean isWheelSwitchVisible() {
        return getWheelSwitch().isVisible();
    }

    public class WheelSwitchPropertyListener implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Object source = evt.getSource();
            if (getSlider().equals(source)) {
                String propertyName = evt.getPropertyName();

                if (propertyName.equals("minimum")) {
                    int minimum = (Integer) evt.getNewValue();
                    getWheelSwitch().setNumberValue(minimum);
                    getWheelSwitch().setMinValue(minimum - 1);

                } else if (propertyName.equals("maximum")) {
                    int maximum = (Integer) evt.getNewValue();
                    getWheelSwitch().setMaxValue(maximum + 1);
                }

            }
        }
    }

    public static void main(String[] args) {
        JFrame f = new JFrame();

        final Player player = new Player();
        player.getSlider().setMinimum(6);
        // player.getSlider().setMaximumDouble(3000);
        // player.setMinimum(5);
        player.setMaximum(3000);

        player.initSlider(3000);
        player.setSliderStep(1);
        player.setWheelSwitchVisible(true);
        // player.getSlider().setMinorTickSpacing(10);
        // player.getSlider().setMajorTickSpacing(100);
        f.getContentPane().add(player);
        f.pack();
        f.setVisible(true);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public double ValueLabelTable() {
        // TODO Auto-generated method stub
        return 0;
    }

}
