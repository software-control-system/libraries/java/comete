package fr.soleil.comete.swing.border.util;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;
import javax.swing.ToolTipManager;
import javax.swing.border.Border;

import fr.soleil.comete.swing.border.CometeTitledBorder;

/**
 * Utility class to manage tooltip text for a {@link JComponent} the {@link Border} of which is a
 * {@link CometeTitledBorder}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class CometeTitledBorderToolTipManager implements PropertyChangeListener {

    private static final String BORDER = "border";

    /**
     * Returns the tooltip text to display at given mouse position for a {@link JComponent} that used a
     * {@link CometeTitledBorder} as border..
     * 
     * @param e The {@link MouseEvent} that determines the mouse position and knows the {@link JComponent}.
     * @param defaultToolTipText The default tooltip that should have been displayed if the {@link JComponent} did not
     *            use a {@link CometeTitledBorder} as border.
     * @return A {@link String}: the tooltip text to display.
     */
    public static String getToolTipText(MouseEvent e, String defaultToolTipText) {
        String tooltip;
        if (e == null) {
            tooltip = defaultToolTipText;
        } else {
            Object src = e.getSource();
            if (src instanceof JComponent) {
                JComponent comp = (JComponent) src;
                Border tmp = ((JComponent) src).getBorder();
                if (tmp instanceof CometeTitledBorder) {
                    CometeTitledBorder border = (CometeTitledBorder) tmp;
                    Rectangle bounds = border.getLabelBounds(comp, 0, 0, comp.getWidth(), comp.getHeight());
                    tooltip = (bounds != null) && bounds.contains(e.getPoint()) ? border.getTitleToolTip()
                            : defaultToolTipText;
                } else {
                    tooltip = defaultToolTipText;
                }
            } else {
                tooltip = defaultToolTipText;
            }
        }
        return tooltip;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ((evt != null) && BORDER.equals(evt.getPropertyName())) {
            Object ov = evt.getOldValue(), nv = evt.getNewValue();
            if (ov instanceof CometeTitledBorder) {
                if (!(ov instanceof CometeTitledBorder)) {
                    Object src = evt.getSource();
                    if (src instanceof JComponent) {
                        JComponent c = (JComponent) src;
                        if (c.getToolTipText() == null) {
                            ToolTipManager.sharedInstance().unregisterComponent(c);
                        }
                    }
                }
            } else if (nv instanceof CometeTitledBorder) {
                Object src = evt.getSource();
                if (src instanceof JComponent) {
                    JComponent c = (JComponent) src;
                    if (c.getToolTipText() == null) {
                        ToolTipManager.sharedInstance().registerComponent(c);
                    }
                }
            }
        }
    }
}
