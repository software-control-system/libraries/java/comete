/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.stackviewer;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IPlayer;
import fr.soleil.comete.definition.widget.util.IPlayerAnimationBehavior;
import fr.soleil.comete.swing.util.AnimationTool;
import fr.soleil.data.mediator.Mediator;

/**
 * This class generates animations using a {@link IPlayer} {@link Component}. This kind of
 * behavioral class only work with Swing and AWT object.
 * 
 * @author huriez
 */
public class PlayerAnimationComponentBehavior implements IPlayerAnimationBehavior {

    @Override
    public String generateAnimation(IPlayer player, String targetDirectory) {
        String result;
        try {
            File temp = AnimationTool.createAnimation(player, targetDirectory);
            result = (temp == null ? null : temp.getAbsolutePath());
        } catch (IOException e) {
            result = null;
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to generate animation", e);
        }
        return result;
    }
}
