/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingWorker;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.lib.project.ij.HistogramTool;
import fr.soleil.lib.project.swing.icons.Icons;
import ij.gui.Roi;
import ij.gui.Toolbar;

public class HistogramRoiGenerator extends DataRoiGenerator implements PropertyChangeListener {

    protected static final Cursor CURSOR = Icons.generateCursorFromIcon(ImageViewer.HISTOGRAM_MODE_ICON,
            ImageViewer.MODE_HISTOGRAM_ACTION);
    protected static final Stroke STROKE = IJCanvas.DEFAULT_ROI_STROKE;
    protected static final Stroke SELECTED_STROKE = IJCanvas.DEFAULT_SELECTED_ROI_STROKE;

    public HistogramRoiGenerator(String text, String description, ImageViewer viewer) {
        super(text, description, ImageViewer.HISTOGRAM_MODE_ICON, viewer);
    }

    protected void updateData(IMaskedImageViewer context, boolean forceUpdate) {
        if (context instanceof ImageViewer) {
            ImageViewer viewer = (ImageViewer) context;
            Map<String, Object> data = new HashMap<>();
            if ((viewer.getHistogramViewer() != null) && (viewer.getHistogramViewer().isShowing() || forceUpdate)) {
                Roi roi = viewer.getRoiManager().getSpecialRoi(getReferentRoiGenerator());
                Object imageValue = viewer.getValue();
                int dimX = viewer.getDimX(), dimY = viewer.getDimY();
                double[][] histogram = HistogramTool.getHistogram(roi, imageValue, dimX, dimY, viewer.getHistogramMin(),
                        viewer.getHistogramMax(), viewer.isHistogramAutoScale(), viewer.getHistogramStep(),
                        viewer.isHistogramAutoStep(), IChartViewer.X_INDEX);
                if (histogram != null) {
                    data.put(ImageViewer.HISTOGRAM_NAME, histogram);
                }
                viewer.getHistogramViewer().setData(data);
            }
        }
    }

    @Override
    public String getActionCommand() {
        return ImageViewer.MODE_HISTOGRAM_ACTION;
    }

    @Override
    public int getRoiMode() {
        return IJRoiManager.HISTOGRAM_ROI_MODE;
    }

    @Override
    public Roi createNewRoi(int sx, int sy, IJRoiManager roiManager) {
        Roi roi = new Roi(sx, sy, roiManager);
        roi.setName(ImageViewer.HISTOGRAM_NAME);
        return roi;
    }

    @Override
    public boolean isCompatibleWithRoi(Roi roi) {
        return ((roi != null) && Roi.class.equals(roi.getClass()) && ImageViewer.HISTOGRAM_NAME.equals(roi.getName()));
    }

    @Override
    protected boolean isDialogVisible(ImageViewer viewer) {
        return viewer.getHistogramDialog().isVisible();
    }

    @Override
    protected void doShowDataDialog(ImageViewer viewer) {
        if (viewer != null) {
            checkWindowAdapters(viewer.getHistogramDialog(), viewer);
            SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    updateData(viewer, true);
                    return null;
                }
            };
            worker.execute();
            viewer.getHistogramDialog().setLocationRelativeTo(viewer);
            viewer.getHistogramDialog().setVisible(true);
        }
    }

    @Override
    protected void doHideDataDialog(ImageViewer viewer) {
        if (viewer != null) {
            viewer.getHistogramDialog().setVisible(false);
            if (viewer.getHistogramViewer() != null) {
                viewer.getHistogramViewer().setData(null);
            }
        }
    }

    @Override
    protected void updateDataBounds(ImageViewer viewer) {
        // Nothing to do here
    }

    @Override
    protected void updateData(IMaskedImageViewer context) {
        updateData(context, false);
    }

    @Override
    protected void treatData(ActionEvent e, ImageViewer viewer) {
        super.treatData(e, viewer);
        if (viewer != null) {
            viewer.addPropertyChangeListener(this);
        }
    }

    @Override
    protected void cleanData(ImageViewer viewer) {
        if (viewer != null) {
            viewer.removePropertyChangeListener(this);
        }
        super.cleanData(viewer);
    }

    @Override
    protected int getTool() {
        return Toolbar.RECTANGLE;
    }

    @Override
    protected Stroke generateRoiStroke() {
        return STROKE;
    }

    @Override
    protected Stroke generateSelectedRoiStroke() {
        return SELECTED_STROKE;
    }

    @Override
    protected Color generateRoiColor() {
        return IJCanvas.DEFAULT_HISTOGRAM_ROI_COLOR;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ((evt != null) && (evt.getSource() instanceof ImageViewer)) {
            ImageViewer viewer = (ImageViewer) evt.getSource();
            switch (evt.getPropertyName()) {
                case ImageViewer.HISTOGRAM_MIN:
                case ImageViewer.HISTOGRAM_MAX:
                case ImageViewer.HISTOGRAM_STEP:
                case ImageViewer.HISTOGRAM_AUTO_SCALE:
                case ImageViewer.HISTOGRAM_AUTO_STEP:
                    updateData(viewer);
                    break;
                default:
                    break;
            }
        }
    }

}
