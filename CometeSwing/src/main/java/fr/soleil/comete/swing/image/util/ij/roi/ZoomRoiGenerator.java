/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.lib.project.swing.icons.Icons;
import ij.gui.Roi;

public class ZoomRoiGenerator extends AutoDeleteSimpleRoiGenerator {

    protected static final Cursor CURSOR = Icons.generateCursorFromIcon(ImageViewer.ZOOM_MODE2_ICON,
            ImageViewer.MODE_ZOOM_ACTION);

    public ZoomRoiGenerator(String text, String description) {
        super(text, description, ImageViewer.ZOOM_MODE_ICON);
    }

    @Override
    public int getRoiMode() {
        return IJRoiManager.ZOOM_ROI_MODE;
    }

    @Override
    protected BasicStroke generateRoiStroke() {
        return IJCanvas.DEFAULT_ZOOM_ROI_STROKE;
    }

    @Override
    protected Color generateRoiColor() {
        return IJCanvas.DEFAULT_ZOOM_ROI_COLOR;
    }

    @Override
    public int getRoiModeMenu() {
        return ImageViewer.TYPE_SELECTION_MENU;
    }

    @Override
    public String getActionCommand() {
        return ImageViewer.MODE_ZOOM_ACTION;
    }

    @Override
    public Cursor getCursor() {
        return CURSOR;
    }

    @Override
    protected void doExpectedWork(MouseEvent e, ImageViewer viewer, Roi zoomRoi, IJCanvas canvas) {
        if ((zoomRoi != null) && (canvas != null)) {
            // we can now delete the temporary zoom ROI
            Rectangle zoomBounds = zoomRoi.getBounds();
            if ((zoomBounds.width > 0) && (zoomBounds.height > 0)) {
                // only zoom in a valid rectangle
                canvas.zoomInRect(zoomBounds);
                viewer.adjustToCanvas();
            }
        }
    }

}
