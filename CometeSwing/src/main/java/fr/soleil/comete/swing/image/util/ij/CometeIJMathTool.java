/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.activation.UnsupportedDataTypeException;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MathematicSector;
import fr.soleil.comete.swing.IMaskedImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.DoubleMatrix;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ij.FlatMatrixTool;
import fr.soleil.lib.project.ij.IJMathTool;
import fr.soleil.lib.project.ij.MatrixTool;
import fr.soleil.lib.project.ij.RoiTool;
import ij.gui.Roi;

public class CometeIJMathTool extends IJMathTool {

    /**
     * Calculates the masked matrix of an {@link IMaskedImageViewer}
     * 
     * @param imageViewer The {@link IMaskedImageViewer}
     * @return A <code>double[][]</code> value, with {@link Double#NaN} values
     *         in masked zones.
     */
    public static double[][] getMatriceValuesFromROI(IMaskedImageViewer imageViewer) {
        double[][] result = null;
        try {
            IJRoiManager roiManager = null;
            if (imageViewer != null) {
                roiManager = imageViewer.getRoiManager();
                if (roiManager != null) {
                    Object imageValue = imageViewer.getValue();
                    if (imageValue != null) {
                        Roi roi = roiManager.getValidatedRoi();
                        result = getDataAsDoubleMatrix(roi, imageValue, imageViewer.getDimX(), imageViewer.getDimY(),
                                isInnerRoi(roi, roiManager), imageViewer.getMask(),
                                getValidatedSectorMask(imageViewer));
                    }
                }
            }
        } catch (Exception e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to get matrix values from ROI", e);
            result = null;
        }
        return result;
    }

    /**
     * Calculates the masked matrix of an {@link IMaskedImageViewer}
     * 
     * @param imageViewer The {@link IMaskedImageViewer}
     * @return A <code>double[][]</code> value, with {@link Double#NaN} values
     *         in masked zones.
     */
    public static AbstractNumberMatrix<Double> getFlatMatrixValuesFromROI(IMaskedImageViewer imageViewer) {
        double[] flatData = null;
        int dimX = 0, dimY = 0;
        try {
            IJRoiManager roiManager = null;
            if (imageViewer != null) {
                roiManager = imageViewer.getRoiManager();
                if (roiManager != null) {
                    Object imageValue = imageViewer.getValue();
                    if (imageValue != null) {
                        Roi roi = roiManager.getValidatedRoi();
                        dimX = imageViewer.getDimX();
                        dimY = imageViewer.getDimY();
                        flatData = getDataAsFlatDoubleMatrix(roi, imageValue, dimX, dimY, isInnerRoi(roi, roiManager),
                                imageViewer.getMask(), getValidatedSectorMask(imageViewer));
                    }
                }
            }
        } catch (Exception e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to get flat matrix values from ROI", e);
            flatData = null;
            dimX = 0;
            dimY = 0;
        }
        AbstractNumberMatrix<Double> result;
        if (flatData == null) {
            result = null;
        } else {
            result = new DoubleMatrix(Double.TYPE);
            try {
                result.setFlatValue(flatData, dimY, dimX);
            } catch (UnsupportedDataTypeException e) {
                // Should not happen
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to extract image flat matrix", e);
                result = null;
            }
        }
        return result;
    }

    public static double[] getDataAsFlatDoubleMatrixFromROI(IMaskedImageViewer imageViewer, Roi roi,
            boolean keepRoiAspect) {
        double[] result;
        if (imageViewer != null) {
            int dimX = imageViewer.getDimX();
            int dimY = imageViewer.getDimY();
            Rectangle roiBounds;
            if (roi == null) {
                roiBounds = new Rectangle(dimX, dimY);
            } else if (keepRoiAspect) {
                roiBounds = roi.getBounds();
            } else {
                roiBounds = RoiTool.getRoiBounds(roi, dimX, dimY, true);
            }
            int[] usableBounds = RoiTool.extractUsableRoiBounds(roiBounds, dimX, dimY);
            Object imageData = imageViewer.getValue();
            if ((usableBounds != null) && (imageData != null)) {
                // Roi Y minimum, included, inside the image
                int minYroi = usableBounds[0];
                // Roi Y maximum, excluded, inside the image
                int maxYroi = usableBounds[1];
                // Roi X minimum, included, inside the image
                int minXroi = usableBounds[2];
                // Roi X maximum, excluded, inside the image
                int maxXroi = usableBounds[3];

                int xStart, yStart, xLength, yLength;
                if (keepRoiAspect) {
                    if (roiBounds.x < 0) {
                        xStart = -roiBounds.x;
                    } else {
                        xStart = 0;
                    }
                    if (roiBounds.y < 0) {
                        yStart = -roiBounds.y;
                    } else {
                        yStart = 0;
                    }
                    xLength = roiBounds.width;
                    yLength = roiBounds.height;
                } else {
                    xStart = 0;
                    yStart = 0;
                    xLength = maxXroi - minXroi;
                    yLength = maxYroi - minYroi;
                }
                int flatLength = xLength * yLength;
                if (flatLength > 0) {
                    result = new double[flatLength];
                    Arrays.fill(result, Double.NaN);
                    if (imageData instanceof byte[]) {
                        byte[] array = (byte[]) imageData;
                        for (int y = 0; y < yLength - yStart; y++) {
                            // treat roi rectangle
                            for (int x = 0; x < xLength - xStart; x++) {
                                result[(y + yStart) * xLength + x + xStart] = array[((y + minYroi) * dimX) + x
                                        + minXroi];
                            }
                        }
                    } // end if (imageData instanceof byte[])
                    else if (imageData instanceof short[]) {
                        short[] array = (short[]) imageData;
                        for (int y = 0; y < yLength - yStart; y++) {
                            // treat roi rectangle
                            for (int x = 0; x < xLength - xStart; x++) {
                                result[(y + yStart) * xLength + x + xStart] = array[((y + minYroi) * dimX) + x
                                        + minXroi];
                            }
                        }
                    } // end if (imageData instanceof short[])
                    else if (imageData instanceof int[]) {
                        int[] array = (int[]) imageData;
                        for (int y = 0; y < yLength - yStart; y++) {
                            // treat roi rectangle
                            for (int x = 0; x < xLength - xStart; x++) {
                                result[(y + yStart) * xLength + x + xStart] = array[((y + minYroi) * dimX) + x
                                        + minXroi];
                            }
                        }
                    } // end if (imageData instanceof int[])
                    else if (imageData instanceof long[]) {
                        long[] array = (long[]) imageData;
                        for (int y = 0; y < yLength - yStart; y++) {
                            // treat roi rectangle
                            for (int x = 0; x < xLength - xStart; x++) {
                                result[(y + yStart) * xLength + x + xStart] = array[((y + minYroi) * dimX) + x
                                        + minXroi];
                            }
                        }
                    } // end if (imageData instanceof long[])
                    else if (imageData instanceof float[]) {
                        float[] array = (float[]) imageData;
                        for (int y = 0; y < yLength - yStart; y++) {
                            // treat roi rectangle
                            for (int x = 0; x < xLength - xStart; x++) {
                                result[(y + yStart) * xLength + x + xStart] = array[((y + minYroi) * dimX) + x
                                        + minXroi];
                            }
                        }
                    } // end if (imageData instanceof float[])
                    else if (imageData instanceof double[]) {
                        double[] array = (double[]) imageData;
                        for (int y = 0; y < yLength - yStart; y++) {
                            // treat roi rectangle
                            for (int x = 0; x < xLength - xStart; x++) {
                                result[(y + yStart) * xLength + x + xStart] = array[((y + minYroi) * dimX) + x
                                        + minXroi];
                            }
                        }
                    } // end if (imageData instanceof double[])
                    else if (imageData instanceof Number[]) {
                        Number[] array = (Number[]) imageData;
                        for (int y = 0; y < yLength - yStart; y++) {
                            // treat roi rectangle
                            for (int x = 0; x < xLength - xStart; x++) {
                                result[(y + yStart) * xLength + x + xStart] = extractDouble(
                                        array[((y + minYroi) * dimX) + x + minXroi]);
                            }
                        }
                    } // end if (imageData instanceof Number[])
                } else { // empty roi
                    result = null;
                }
            } else { // no usableBounds or no imageData
                result = null;
            }
        } else { // no image viewer
            result = null;
        }
        return result;
    }

    /**
     * Returns whether a {@link Roi} is an inner one
     * 
     * @param roi The {@link Roi}
     * @param roiManager The {@link IJRoiManager} that knows the {@link Roi}
     * @return A <code>boolean</code> value
     */
    private static boolean isInnerRoi(Roi roi, IJRoiManager roiManager) {
        boolean innerRoi = false;
        if (roiManager != null) {
            if ((roi != null) && (!roiManager.isInner(roi)) && (!roiManager.isOuter(roi))) {
                roi = null;
            }
            if (roi == null) {
                innerRoi = true;
            } else if (roiManager.isInner(roi)) {
                innerRoi = true;
            } else if (roiManager.isOuter(roi)) {
                innerRoi = false;
            }
        }
        return innerRoi;
    }

    /**
     * Calculates the sector's mask if valid
     * 
     * @param imageViewer The {@link IMaskedImageViewer} that contains the sector
     * @return A {@link Mask}
     */
    public static Mask getValidatedSectorMask(IMaskedImageViewer imageViewer) {
        MathematicSector sector = imageViewer.getSector();
        Mask sectorMask = imageViewer.getSectorMask();
        if ((sector == null) || (sector.isConstructing())) {
            sectorMask = null;
        }
        return sectorMask;
    }

    /**
     * Calculates and returns the <code>double[][]</code> resulting from an original value, masked by a {@link Roi} and
     * some {@link Mask}s
     * 
     * @param roi The {@link Roi}
     * @param imageData The original value, represented as a single dimension array
     *            (example: <code>byte[]</code>, <code>float[]</code>, {@link Number}<code>[]</code>, etc...)
     * @param dimX The matrix width
     * @param dimY The matrix height
     * @param innerRoi A <code>boolean</code> whether the {@link Roi} is an inner {@link Roi} (i.e. exterior of this
     *            {@link Roi} should be considered as masked) or an outer {@link Roi} (i.e. interior of this {@link Roi}
     *            should be considered as masked). <code>TRUE</code> for an inner {@link Roi}
     * @param mainMask The 1st {@link Mask}
     * @param secondaryMask The 2nd {@link Mask}
     * @return A <code>double[][]</code>, with {@link Double#NaN} value in masked zones, and original value cast into
     *         <code>double</code> in the rest of the matrix.
     */
    public static double[][] getDataAsDoubleMatrix(Roi roi, Object imageData, int dimX, int dimY, boolean innerRoi,
            Mask... masks) {
        return MatrixTool.getDataAsDoubleMatrix(roi, imageData, dimX, dimY, innerRoi, getMaskValues(masks));
    }

    /**
     * Calculates and returns the <code>double[]</code> resulting from an
     * original value, masked by a {@link Roi} and some {@link Mask}s
     * 
     * @param roi The {@link Roi}
     * @param imageData The original value, represented as a single dimension array
     *            (example: <code>byte[]</code>, <code>float[]</code>, {@link Number}<code>[]</code>, etc...)
     * @param dimX The matrix width
     * @param dimY The matrix height
     * @param innerRoi A <code>boolean</code> whether the {@link Roi} is an inner {@link Roi} (i.e. exterior of this
     *            {@link Roi} should be considered as masked) or an outer {@link Roi} (i.e. interior of this {@link Roi}
     *            should be considered as masked). <code>TRUE</code> for an inner {@link Roi}
     * @param mainMask The 1st {@link Mask}
     * @param secondaryMask The 2nd {@link Mask}
     * @return A <code>double[]</code>, with {@link Double#NaN} value in masked zones, and original value cast into
     *         <code>double</code> in the rest of the flat matrix.
     */
    public static double[] getDataAsFlatDoubleMatrix(Roi roi, Object imageData, int dimX, int dimY, boolean innerRoi,
            Mask... masks) {
        return FlatMatrixTool.getDataAsFlatDoubleMatrix(roi, imageData, dimX, dimY, innerRoi, getMaskValues(masks));
    }

    protected static List<boolean[]> getMaskValues(Mask... masks) {
        List<boolean[]> maskValues;
        if (masks == null) {
            maskValues = null;
        } else {
            maskValues = new ArrayList<boolean[]>();
            for (Mask mask : masks) {
                if (mask != null) {
                    maskValues.add(mask.getValue());
                }
            }
        }
        return maskValues;
    }

}
