/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.axis.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.Map;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.swing.chart.axis.model.AxisAttributes;
import fr.soleil.comete.swing.chart.axis.model.AxisLabel;
import fr.soleil.comete.swing.chart.axis.scale.AbstractAxisScale.TickType;
import fr.soleil.lib.project.awt.ColorUtils;

/**
 * {@link AbstractAxisView} for vertical axis
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class VerticalAxisView extends AbstractAxisView {

    public VerticalAxisView(AxisAttributes attributes, int tickAlignment, int labelAlignment) {
        super(attributes, tickAlignment, labelAlignment);
        titlePosition = LEFT;
    }

    @Override
    protected Dimension computeAxisSize(FontRenderContext frc, int desiredLength, int maxLabelWidth,
            int maxLabelHeight) {
        int width;
        if ((attributes.getTitle() == null) || attributes.getTitle().isEmpty()) {
            width = maxLabelWidth;
        } else {
            width = maxLabelWidth + getLabelHeight(frc);
        }
        return new Dimension(width, desiredLength);
    }

    @Override
    public int getThickness() {
        return getAxisWidth();
    }

    @Override
    public int getLength() {
        return getAxisHeight();
    }

    protected double getTitleRotationAngle() {
        return titlePosition == RIGHT ? Math.PI / 2.0 : -Math.PI / 2.0;
    }

    @Override
    public void applyTitleBounds(FontRenderContext frc, int x, int y, Rectangle bounds) {
        int[] ticksAlignment = computeTicksX(x, tickAlignment);
        int tickX = ticksAlignment[0];
        int minLabelX = Math.min(x, tickX) - DEFAULT_MARGIN;
        int maxLabelX = Math.max(x, tickX + attributes.getTickLength()) + DEFAULT_MARGIN;
        applyTitleBounds(frc, x, y, bounds, minLabelX, maxLabelX);
    }

    protected void applyTitleBounds(FontRenderContext frc, int x, int y, Rectangle bounds, int minLabelX,
            int maxLabelX) {
        if (bounds != null) {
            Font labelFont = attributes.getLabelFont();
            if ((attributes.getTitle() == null) || attributes.getTitle().isEmpty() || (frc == null)
                    || (labelFont == null)) {
                bounds.x = 0;
                bounds.y = 0;
                bounds.width = 0;
                bounds.height = 0;
            } else {
                Rectangle2D titleBounds = labelFont.getStringBounds(attributes.getTitle(), frc);
                int fontAscent = (int) (labelFont.getLineMetrics(attributes.getTitle(), frc).getAscent() + 0.5f);
                // title coordinates in rotated graphics
                int rotatedTitleX, rotatedTitleY;
                if (titlePosition == RIGHT) {
                    switch (attributes.getTitleAlignment()) {
                        case IComponent.TOP:
                            rotatedTitleX = y;
                            break;
                        case IComponent.BOTTOM:
                            rotatedTitleX = y + getLength() - (int) titleBounds.getWidth();
                            break;
                        case IComponent.CENTER:
                        default:
                            rotatedTitleX = y + (getLength() - (int) (titleBounds.getWidth())) / 2;
                    }
                    rotatedTitleY = -maxLabelX - getThickness() + fontAscent;
                } else {
                    switch (attributes.getTitleAlignment()) {
                        case IComponent.TOP:
                            rotatedTitleX = -y - (int) (titleBounds.getWidth());
                            break;
                        case IComponent.BOTTOM:
                            rotatedTitleX = -y - getLength();
                            break;
                        // center aligned by default
                        case IComponent.CENTER:
                        default:
                            rotatedTitleX = -y - (getLength() + (int) (titleBounds.getWidth())) / 2;
                    }
                    rotatedTitleY = minLabelX - getThickness() + fontAscent + DEFAULT_MARGIN;
                }
                bounds.x = rotatedTitleX;
                bounds.y = rotatedTitleY;
                bounds.width = (int) Math.round(titleBounds.getWidth());
                bounds.height = (int) Math.round(titleBounds.getHeight());
            }
        }
    }

    @Override
    public void paintAxis(Graphics2D g, FontRenderContext frc, Color back, int x, int y, int xOpposite,
            int oppositeTickAlignment, boolean drawOpposite, int gridMin, int gridMax) {
        Font labelFont = attributes.getLabelFont();
        Color subTickColor;
        if (back == null) {
            subTickColor = axisColor;
        } else {
            subTickColor = ColorUtils.blend(axisColor, back, 0.25);
        }
        int[] ticksAlignment = computeTicksX(x, tickAlignment);
        int[] oppositeTicksAlignment = computeTicksX(xOpposite, oppositeTickAlignment);
        int tickX = ticksAlignment[0], subTickX = ticksAlignment[1];
        int oppositeTickX = oppositeTicksAlignment[0], oppositeSubTickX = oppositeTicksAlignment[1];
        AxisLabel[] labels = scale.getLabels();
        int minLabelX = Math.min(x, tickX) - DEFAULT_MARGIN;
        int maxLabelX = Math.max(x, tickX + attributes.getTickLength()) + DEFAULT_MARGIN;
        if (visible && (labels != null)) {
            Color labelColor = attributes.getLabelColor();
            if (labelColor == null) {
                labelColor = Color.BLACK;
            }
            g.setColor(labelColor);
            g.setFont(labelFont);
            // Draw labels
            for (AxisLabel label : labels) {
                int labelY = (int) Math.rint(label.getPos()) + y + label.getY() + label.getHeight() / 3;
                int labelX;
                if (labelAlignment == RIGHT) {
                    labelX = maxLabelX;
                } else {
                    // Left label alignment by default
                    labelX = minLabelX - label.getTotalWidth();
                }
                g.drawString(label.getValue(), labelX, labelY);
            }
        }
        Map<TickType, int[]> tickMap = scale.computeTicksPositions(y, getLength());
        int[] ticks = tickMap.get(TickType.TICK);
        int[] subTicks = tickMap.get(TickType.SUB_TICK);
        if (attributes.getGridStroke() != null) {
            // Draw grid
            g.setStroke(attributes.getGridStroke());
            if ((subTicks != null) && attributes.isSubGridVisible()) {
                g.setColor(subTickColor);
                for (int subTick : subTicks) {
                    g.drawLine(gridMin, subTick, gridMax, subTick);
                }
            }
            if ((ticks != null) && attributes.isGridVisible()) {
                g.setColor(axisColor);
                for (int tick : ticks) {
                    g.drawLine(gridMin, tick, gridMax, tick);
                }
            }
        }
        g.setStroke(DEFAULT_AXIS_STROKE);
        if ((subTicks != null) && attributes.isSubTickVisible()) {
            // Draw sub-ticks
            g.setColor(subTickColor);
            for (int subTick : subTicks) {
                if (visible) {
                    g.drawLine(subTickX, subTick, subTickX + attributes.getSubtickLength(), subTick);
                }
                if (drawOpposite) {
                    g.drawLine(oppositeSubTickX, subTick, oppositeSubTickX + attributes.getSubtickLength(), subTick);
                }
            }
        }
        if (ticks != null) {
            // Draw ticks
            g.setColor(axisColor);
            for (int tick : ticks) {
                if (visible) {
                    g.drawLine(tickX, tick, tickX + attributes.getTickLength(), tick);
                }
                if (drawOpposite) {
                    g.drawLine(oppositeTickX, tick, oppositeTickX + attributes.getTickLength(), tick);
                }
            }
        }
        // Draw Axe
        g.setColor(axisColor);
        if (drawOpposite) {
            g.drawLine(xOpposite, y, xOpposite, y + getLength());
        }
        if (visible) {
            g.drawLine(x, y, x, y + getLength());
            if ((attributes.getTitle() != null) && (!attributes.getTitle().isEmpty())) {
                double rotationAngle = getTitleRotationAngle();
                applyTitleBounds(frc, x, y, titleBounds, minLabelX, maxLabelX);
                g.rotate(rotationAngle);
                g.drawString(attributes.getTitle(), titleBounds.x, titleBounds.y);
                g.rotate(-rotationAngle);
            }
        }
    }

    /**
     * Computes ticks and sub-ticks x coordinate, depending on expected tick horizontal alignment
     * and axes x coordinate
     * 
     * @param x The axes x coordinate
     * @param alignment The expected tick horizontal alignment
     * @return An <code>int[]</code>: <code>{ tickX, subTickX }</code>
     */
    protected int[] computeTicksX(int x, int alignment) {
        int tickX, subTickX;
        switch (alignment) {
            case CENTER:
                tickX = x - attributes.getTickLength() / 2;
                subTickX = x - attributes.getSubtickLength() / 2;
                break;
            case LEFT:
                tickX = x - attributes.getTickLength();
                subTickX = x - attributes.getSubtickLength();
                break;
            // Right aligned by default
            case RIGHT:
            default:
                tickX = x;
                subTickX = x;
                break;
        }
        return new int[] { tickX, subTickX };
    }

    @Override
    public int[] computeLabelOffsetFromTickAlignment() {
        int offX = 0;
        int offY = 0;
        if (tickAlignment == LEFT) {
            offX = (attributes.getTickLength() < 0) ? -attributes.getTickLength() : 0;
        } else {
            offX = (attributes.getTickLength() < 0) ? attributes.getTickLength() : 0;
        }
        int[] offset = new int[2];
        offset[IChartViewer.X_INDEX] = offX;
        offset[IChartViewer.Y_INDEX] = offY;
        return offset;
    }

}
