/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;

import fr.soleil.comete.definition.listener.ITreeNodeListener;
import fr.soleil.comete.definition.widget.util.ITreeNode;

/**
 * A {@link TreeModel} that only manipulates {@link ITreeNode}s and {@link DefaultMutableTreeNode}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface CometeTreeModel extends TreeModel, ITreeNodeListener {

    @Override
    public DefaultMutableTreeNode getRoot();

    /**
     * Sets this model's root
     * 
     * @param root The {@link ITreeNode} that should be this model's root
     */
    public void setRoot(ITreeNode root);

    /**
     * Removes some nodes from this model
     * 
     * @param nodes The nodes to remove
     */
    public void removeNodes(DefaultMutableTreeNode... nodes);

    /**
     * Removes some {@link ITreeNode}s from this model
     * 
     * @param iTreeNodes The {@link ITreeNode}s to remove
     */
    public void removeTreeNodes(ITreeNode... iTreeNodes);

    /**
     * Adds an {@link ITreeNode} in a parent {@link DefaultMutableTreeNode}, constructing their
     * equivalent {@link DefaultMutableTreeNode}s if necessary. This method is recursive (i.e.
     * constructs and adds descendant {@link DefaultMutableTreeNode}s if necessary)
     * 
     * @param parent The parent {@link DefaultMutableTreeNode}
     * @param treeNode The {@link ITreeNode} to add
     * @param index The index at which to add the child
     */
    public void addTreeNode(DefaultMutableTreeNode parent, ITreeNode treeNode, int index);

    /**
     * Searches the {@link DefaultMutableTreeNode} that corresponds to a given {@link ITreeNode},
     * starting from a given {@link DefaultMutableTreeNode}
     * 
     * @param treeNode The {@link ITreeNode} to look for
     * @param startNode The {@link DefaultMutableTreeNode} from which to start the search
     * @return A {@link DefaultMutableTreeNode}. <code>null</code> if search failed
     */
    public DefaultMutableTreeNode recoverNode(ITreeNode treeNode, DefaultMutableTreeNode startNode);

    /**
     * Searches in current structure the {@link DefaultMutableTreeNode} that corresponds to a given
     * {@link ITreeNode}
     * 
     * @param treeNode The {@link ITreeNode} to look for
     * @return A {@link DefaultMutableTreeNode}. <code>null</code> if search failed
     */
    public DefaultMutableTreeNode recoverNode(ITreeNode treeNode);

    /**
     * Returns a {@link DefaultMutableTreeNode} that corresponds to a given {@link ITreeNode},
     * creating such a {@link DefaultMutableTreeNode} if necessary
     * 
     * @param node The {@link ITreeNode}
     * @return A {@link DefaultMutableTreeNode}
     */
    public ITreeNode getTreeNode(DefaultMutableTreeNode node);
}
