/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.undo;

import java.util.ResourceBundle;

import javax.swing.UIManager;
import javax.swing.undo.CompoundEdit;

import fr.soleil.comete.swing.image.ijviewer.IJRoiManager.RoiOperation;

/**
 * 
 * 
 * @author MAINGUY
 * 
 */
public class CombineRoiEdit extends CompoundEdit {

    private static final long serialVersionUID = -4361508661567396283L;

    protected static final ResourceBundle MESSAGES = ResourceBundle
            .getBundle("fr.soleil.comete.swing.image.ijviewer.messages");

    private RoiOperation roiOperation;

    /**
     * 
     */
    public CombineRoiEdit(RoiOperation operation) {
        super();
        roiOperation = operation;
    }

    /* (non-Javadoc)
     * @see javax.swing.undo.AbstractUndoableEdit#getPresentationName()
     */
    @Override
    public String getPresentationName() {
        String result = null;

        switch (roiOperation) {
            case OR:
                result = MESSAGES.getString("ImageJViewer.UndoManagement.OrRoi");
                break;

            case AND:
                result = MESSAGES.getString("ImageJViewer.UndoManagement.AndRoi");
                break;

            case NOT:
                result = MESSAGES.getString("ImageJViewer.UndoManagement.NotRoi");
                break;

            case XOR:
                result = MESSAGES.getString("ImageJViewer.UndoManagement.XorRoi");
                break;
        }

        return result;
    }

    /* (non-Javadoc)
     * @see javax.swing.undo.CompoundEdit#getUndoPresentationName()
     */
    @Override
    public String getUndoPresentationName() {
        String name = getPresentationName();
        if (name.isEmpty()) {
            name = UIManager.getString("AbstractUndoableEdit.undoText");
        } else {
            name = UIManager.getString("AbstractUndoableEdit.undoText") + " " + name;
        }

        return name;
    }

    /* (non-Javadoc)
     * @see javax.swing.undo.CompoundEdit#getRedoPresentationName()
     */
    @Override
    public String getRedoPresentationName() {
        String name = getPresentationName();
        if (name.isEmpty()) {
            name = UIManager.getString("AbstractUndoableEdit.redoText");
        } else {
            name = UIManager.getString("AbstractUndoableEdit.redoText") + " " + name;
        }

        return name;
    }

}
