/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import fr.soleil.comete.definition.data.target.matrix.ITextMatrixComponent;
import fr.soleil.data.container.matrix.StringMatrix;

public class StringMatrixTable extends AbstractMatrixTable<String> implements ITextMatrixComponent {

    private static final long serialVersionUID = -7696210604366807451L;

    @Override
    protected boolean isMenuTrimTableVisible() {
        return false;
    }

    @Override
    public String[][] getStringMatrix() {
        String[][] result = null;
        if (matrix != null) {
            result = (String[][]) matrix.getValue();
        }
        return result;
    }

    @Override
    public void setStringMatrix(String[][] array) {
        StringMatrix result = new StringMatrix();
        result.setValue(array);
        setData(result);
    }

    @Override
    public String[] getFlatStringMatrix() {
        String[] result = null;
        if (matrix != null) {
            result = (String[]) matrix.getFlatValue();
        }
        return result;
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        StringMatrix matrix = new StringMatrix();
        matrix.setFlatValue(value, height, width);
        setData(matrix);
    }

}
