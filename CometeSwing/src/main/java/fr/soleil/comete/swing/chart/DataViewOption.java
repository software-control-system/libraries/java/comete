/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.FillMethodListCellRenderer;
import fr.soleil.comete.swing.util.FillStyleListCellRenderer;
import fr.soleil.comete.swing.util.LineStyleListCellRenderer;
import fr.soleil.comete.swing.util.MarkerStyleListCellRenderer;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A class to display dataview settings dialog.
 * 
 * @author JL Pons
 */
public class DataViewOption extends AbstractChartDataOption {

    private static final long serialVersionUID = -3641332359054055206L;

    protected static final int ORIGIN_GLOBAL_PLOT = 0;
    protected static final int ORIGIN_BAR = 1;
    protected static final int ORIGIN_CURVE = 2;
    protected static final int ORIGIN_MARKER = 3;
    protected static final int ORIGIN_TRANSFORMATION = 4;
    protected static final int ORIGIN_INTERPOLATION = 5;
    protected static final int ORIGIN_SMOOTHING = 6;
    protected static final int ORIGIN_MATH = 7;
    protected static final int ORIGIN_ERROR = 8;
    protected static final int ORIGIN_X_OFFSET = 9;

    public static final String DERIVATIVE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.math.function.derivative");
    public static final String INTEGRAL = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.math.function.integral");
    public static final String FFT_MODULUS = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.math.function.fft.modulus");
    public static final String FFT_PHASE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.math.function.fft.phase");

    // Local declaration
    protected String dataViewId;
    protected JLabel nameLabel;
    protected JButton setAllButton;

    // Axis selection combo box (not displayed by default)
    protected JComboBox<String> axisBox;
    protected JLabel axisLabel;

    // DataView general option panel
    protected JPanel curvePanel;

    protected JLabel viewTypeLabel;
    protected JComboBox<String> viewTypeCombo;

    protected JLabel lineColorView;
    protected JButton lineColorBtn;
    protected JLabel lineColorLabel;

    protected JLabel fillColorView;
    protected JButton fillColorBtn;
    protected JLabel fillColorLabel;

    protected JLabel fillStyleLabel;
    protected JComboBox<Integer> fillStyleCombo;

    protected JLabel lineWidthLabel;
    protected JSpinner lineWidthSpinner;

    protected JLabel lineStyleLabel;
    protected JComboBox<Integer> lineStyleCombo;

    protected JLabel lineNameLabel;
    protected JTextField lineNameText;

    // Bar panel
    protected JPanel barPanel;

    protected JLabel barWidthLabel;
    protected JSpinner barWidthSpinner;

    protected JLabel fillMethodLabel;
    protected JComboBox<Integer> fillMethodCombo;

    // Marker option panel
    protected JPanel markerPanel;

    protected JLabel markerColorView;
    protected JButton markerColorBtn;
    protected JLabel markerColorLabel;

    protected JLabel markerSizeLabel;
    protected JSpinner markerSizeSpinner;

    protected JLabel markerStyleLabel;
    protected JComboBox<Integer> markerStyleCombo;

    protected JCheckBox legendVisibleCheck;
    protected JCheckBox samplingAllowedCheck;

    // Error option panel
    protected JPanel errorPanel;
    protected JLabel errorColorView;
    protected JButton errorColorBtn;
    protected JLabel errorColorLabel;
    protected JCheckBox errorVisibleCheck;

    // Transformation panel
    protected JPanel transformPanel;

    protected JTextArea transformHelpLabel;

    protected JLabel transformA0Label;
    protected JTextField transformA0Text;

    protected JLabel transformA1Label;
    protected JTextField transformA1Text;

    protected JLabel transformA2Label;
    protected JTextField transformA2Text;

    // X offset panel
    protected JPanel xOffsetPanel;

    protected JTextArea xOffsetHelpLabel;

    protected JLabel xOffsetA0Label;
    protected JTextField xOffsetA0Text;

    // Interpolation panel
    protected JPanel interpPanel;

    protected ButtonGroup methodIntBtnGrp;
    protected JRadioButton noInterpBtn;
    protected JRadioButton linearBtn;
    protected JRadioButton cosineBtn;
    protected JRadioButton cubicBtn;
    protected JRadioButton hermiteBtn;
    protected JSpinner stepSpinner;
    protected JTextField tensionText;
    protected JTextField biasText;

    // Smoothing panel
    protected JPanel smoothPanel;

    protected ButtonGroup methodSmBtnGrp;
    protected JRadioButton noSmoothBtn;
    protected JRadioButton flatSmoothBtn;
    protected JRadioButton triangularSmoothBtn;
    protected JRadioButton gaussianSmoothBtn;
    protected JSpinner neighborSpinner;
    protected JTextField sigmaText;
    protected ButtonGroup methodExtBtnGrp;
    protected JRadioButton noExtBtn;
    protected JRadioButton flatExtBtn;
    protected JRadioButton linearExtBtn;

    // Math panel
    protected JPanel mathPanel;

    protected ButtonGroup mathBtnGrp;
    protected JRadioButton noMathBtn;
    protected JRadioButton derivativeBtn;
    protected JRadioButton integralBtn;
    protected JRadioButton fftModBtn;
    protected JRadioButton fftPhaseBtn;

    protected PlotProperties properties;

    private boolean alwaysCommit = true;

    /**
     * Dialog constructor.
     * 
     * @param parent Parent dialog
     * @param chart Chart used to commit change (can be null)
     * @param v The id of DataView to edit
     */
    public DataViewOption() {
        super();
    }

    @Override
    protected String getExpectedName() {
        return ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.title");
    }

    @Override
    protected void initComponents() {
        dataViewId = null;
        super.initComponents();

        // Global frame construction
        nameLabel = new JLabel();
        nameLabel.setText(SPACE);
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        southPanel.add(nameLabel, BorderLayout.CENTER);

        updateControls();
    }

    @Override
    protected void initTabPane() {
        super.initTabPane();
        // Axis ComboBox
        axisBox = new JComboBox<>();
        axisBox.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.y1"));
        axisBox.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.y2"));
        axisBox.addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.x"));
        axisBox.addActionListener(this);
        axisLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.axis.title"), JLabel.RIGHT);

        initCurvePanel();
        initBarPanel();
        initMarkerPanel();
        initErrorPanel();
        initTransformPanel();
        initXOffsetPanel();
        initInterpolationPanel();
        initSmoothingPanel();
        initMathPanel();

        tabPane.setFont(CometeUtils.getLabelFont());
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.curve.title"),
                curvePanel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.bar.title"), barPanel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.marker.title"),
                markerPanel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.error.title"),
                errorPanel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.transform.title"),
                transformPanel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.offset.x.title"),
                xOffsetPanel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.interpolation.title"),
                interpPanel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.smoothing.title"),
                smoothPanel);
        tabPane.add(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.math.title"),
                mathPanel);
    }

    // Curve panel construction
    protected void initCurvePanel() {
        curvePanel = new JPanel(new GridBagLayout());

        viewTypeLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.view.type"));
        viewTypeLabel.setFont(CometeUtils.getLabelFont());
        viewTypeLabel.setForeground(CometeUtils.getfColor());

        viewTypeCombo = new JComboBox<>();
        viewTypeCombo.setFont(CometeUtils.getLabelFont());
        viewTypeCombo
                .addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.view.type.line"));
        viewTypeCombo
                .addItem(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.view.type.bar"));
        viewTypeCombo.addItem(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.view.type.stairs"));
        viewTypeCombo.addActionListener(this);

        lineColorView = new JLabel(ObjectUtils.EMPTY_STRING);
        lineColorView.setBackground(Color.RED);
        lineColorView.setOpaque(true);
        lineColorView.setBorder(BorderFactory.createLineBorder(Color.black));
        lineColorView.setPreferredSize(CometeUtils.getViewLabelSize());
        lineColorView.setMinimumSize(lineColorView.getPreferredSize());
        lineColorBtn = new JButton(SUSPENSION_POINTS);
        lineColorBtn.addMouseListener(this);
        lineColorBtn.setMargin(CometeUtils.getBtnInsets());
        lineColorLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.color.line"));
        lineColorLabel.setFont(CometeUtils.getLabelFont());
        lineColorLabel.setForeground(CometeUtils.getfColor());

        setAllButton = new JButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.color.set.all.text"));
        setAllButton.setToolTipText(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.color.set.all.tooltip"));
        setAllButton.addActionListener(this);
        setAllButton.setMargin(CometeUtils.getTextFieldInsets());

        fillColorView = new JLabel(ObjectUtils.EMPTY_STRING);
        fillColorView.setBackground(Color.LIGHT_GRAY);
        fillColorView.setOpaque(true);
        fillColorView.setBorder(BorderFactory.createLineBorder(Color.black));
        fillColorView.setPreferredSize(CometeUtils.getViewLabelSize());
        fillColorView.setMinimumSize(fillColorView.getPreferredSize());
        fillColorBtn = new JButton(SUSPENSION_POINTS);
        fillColorBtn.setMargin(CometeUtils.getBtnInsets());
        fillColorBtn.addMouseListener(this);
        fillColorLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.color.fill"));
        fillColorLabel.setFont(CometeUtils.getLabelFont());
        fillColorLabel.setForeground(CometeUtils.getfColor());

        lineNameLabel = new JLabel(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.name"));
        lineNameLabel.setFont(CometeUtils.getLabelFont());
        lineNameLabel.setForeground(CometeUtils.getfColor());
        lineNameText = new JTextField(8);
        lineNameText.setMargin(CometeUtils.getTextFieldInsets());
        lineNameText.setEditable(true);
        lineNameText.setText(SPACE);
        lineNameText.addKeyListener(this);

        lineWidthLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.line.width"));
        lineWidthLabel.setFont(CometeUtils.getLabelFont());
        lineWidthLabel.setForeground(CometeUtils.getfColor());
        lineWidthSpinner = new JSpinner();
        lineWidthSpinner.setPreferredSize(lineNameText.getPreferredSize());
        Integer value = Integer.valueOf(1);
        Integer min = Integer.valueOf(0);
        Integer max = Integer.valueOf(10);
        Integer step = Integer.valueOf(1);
        SpinnerNumberModel spModel = new SpinnerNumberModel(value, min, max, step);
        lineWidthSpinner.setModel(spModel);
        lineWidthSpinner.addChangeListener(this);

        lineStyleLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.line.style.text"));
        lineStyleLabel.setFont(CometeUtils.getLabelFont());
        lineStyleLabel.setForeground(CometeUtils.getfColor());
        lineStyleCombo = new JComboBox<>();
        lineStyleCombo.setForeground(lineColorView.getBackground());
        lineStyleCombo.setFont(CometeUtils.getLabelFont());
        lineStyleCombo.addItem(IChartViewer.STYLE_SOLID);
        lineStyleCombo.addItem(IChartViewer.STYLE_DOT);
        lineStyleCombo.addItem(IChartViewer.STYLE_DASH);
        lineStyleCombo.addItem(IChartViewer.STYLE_LONG_DASH);
        lineStyleCombo.addItem(IChartViewer.STYLE_DASH_DOT);
        lineStyleCombo.setSelectedIndex(0);
        lineStyleCombo.addActionListener(this);
        lineStyleCombo.setRenderer(new LineStyleListCellRenderer());

        fillStyleLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.text"));
        fillStyleLabel.setToolTipText(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.fill.style.tooltip"));
        fillStyleLabel.setFont(CometeUtils.getLabelFont());
        fillStyleLabel.setForeground(CometeUtils.getfColor());
        fillStyleCombo = new JComboBox<>();
        fillStyleCombo.setToolTipText(fillStyleLabel.getToolTipText());
        fillStyleCombo.setForeground(fillColorView.getBackground());
        fillStyleCombo.setFont(CometeUtils.getLabelFont());
        fillStyleCombo.addItem(IChartViewer.FILL_STYLE_NONE);
        fillStyleCombo.addItem(IChartViewer.FILL_STYLE_SOLID);
        fillStyleCombo.addItem(IChartViewer.FILL_STYLE_LARGE_LEFT_HATCH);
        fillStyleCombo.addItem(IChartViewer.FILL_STYLE_LARGE_RIGHT_HATCH);
        fillStyleCombo.addItem(IChartViewer.FILL_STYLE_LARGE_CROSS_HATCH);
        fillStyleCombo.addItem(IChartViewer.FILL_STYLE_SMALL_LEFT_HATCH);
        fillStyleCombo.addItem(IChartViewer.FILL_STYLE_SMALL_RIGHT_HATCH);
        fillStyleCombo.addItem(IChartViewer.FILL_STYLE_SMALL_CROSS_HATCH);
        fillStyleCombo.addItem(IChartViewer.FILL_STYLE_DOT_PATTERN_1);
        fillStyleCombo.addItem(IChartViewer.FILL_STYLE_DOT_PATTERN_2);
        fillStyleCombo.addItem(IChartViewer.FILL_STYLE_DOT_PATTERN_3);
        fillStyleCombo.setSelectedIndex(0);
        fillStyleCombo.addActionListener(this);
        fillStyleCombo.setRenderer(new FillStyleListCellRenderer());

        GridBagConstraints viewTypeLabelConstraints = new GridBagConstraints();
        viewTypeLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        viewTypeLabelConstraints.gridx = 0;
        viewTypeLabelConstraints.gridy = 0;
        viewTypeLabelConstraints.weightx = 0;
        viewTypeLabelConstraints.weighty = 0;
        viewTypeLabelConstraints.insets = new Insets(5, 5, 5, 10);
        curvePanel.add(viewTypeLabel, viewTypeLabelConstraints);
        GridBagConstraints viewTypeComboConstraints = new GridBagConstraints();
        viewTypeComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        viewTypeComboConstraints.gridx = 1;
        viewTypeComboConstraints.gridy = 0;
        viewTypeComboConstraints.weightx = 1;
        viewTypeComboConstraints.weighty = 0;
        viewTypeComboConstraints.insets = new Insets(5, 0, 5, 5);
        viewTypeComboConstraints.gridwidth = 2;
        curvePanel.add(viewTypeCombo, viewTypeComboConstraints);

        GridBagConstraints lineColorLabelConstraints = new GridBagConstraints();
        lineColorLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        lineColorLabelConstraints.gridx = 0;
        lineColorLabelConstraints.gridy = 1;
        lineColorLabelConstraints.weightx = 0;
        lineColorLabelConstraints.weighty = 0;
        lineColorLabelConstraints.insets = new Insets(0, 5, 5, 10);
        curvePanel.add(lineColorLabel, lineColorLabelConstraints);
        GridBagConstraints lineColorViewConstraints = new GridBagConstraints();
        lineColorViewConstraints.fill = GridBagConstraints.HORIZONTAL;
        lineColorViewConstraints.gridx = 1;
        lineColorViewConstraints.gridy = 1;
        lineColorViewConstraints.weightx = 1;
        lineColorViewConstraints.weighty = 0;
        lineColorViewConstraints.insets = new Insets(0, 0, 5, 5);
        curvePanel.add(lineColorView, lineColorViewConstraints);
        GridBagConstraints lineColorBtnConstraints = new GridBagConstraints();
        lineColorBtnConstraints.fill = GridBagConstraints.NONE;
        lineColorBtnConstraints.gridx = 2;
        lineColorBtnConstraints.gridy = 1;
        lineColorBtnConstraints.weightx = 0;
        lineColorBtnConstraints.weighty = 0;
        lineColorBtnConstraints.insets = new Insets(0, 0, 5, 5);
        curvePanel.add(lineColorBtn, lineColorBtnConstraints);
        GridBagConstraints setAllBtnConstraints = new GridBagConstraints();
        setAllBtnConstraints.fill = GridBagConstraints.NONE;
        setAllBtnConstraints.gridx = 3;
        setAllBtnConstraints.gridy = 1;
        setAllBtnConstraints.weightx = 0;
        setAllBtnConstraints.weighty = 0;
        setAllBtnConstraints.insets = new Insets(0, 0, 5, 5);
        curvePanel.add(setAllButton, setAllBtnConstraints);

        GridBagConstraints fillColorLabelConstraints = new GridBagConstraints();
        fillColorLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        fillColorLabelConstraints.gridx = 0;
        fillColorLabelConstraints.gridy = 2;
        fillColorLabelConstraints.weightx = 0;
        fillColorLabelConstraints.weighty = 0;
        fillColorLabelConstraints.insets = new Insets(0, 5, 5, 10);
        curvePanel.add(fillColorLabel, fillColorLabelConstraints);
        GridBagConstraints fillColorViewConstraints = new GridBagConstraints();
        fillColorViewConstraints.fill = GridBagConstraints.HORIZONTAL;
        fillColorViewConstraints.gridx = 1;
        fillColorViewConstraints.gridy = 2;
        fillColorViewConstraints.weightx = 1;
        fillColorViewConstraints.weighty = 0;
        fillColorViewConstraints.insets = new Insets(0, 0, 5, 5);
        curvePanel.add(fillColorView, fillColorViewConstraints);
        GridBagConstraints fillColorBtnConstraints = new GridBagConstraints();
        fillColorBtnConstraints.fill = GridBagConstraints.NONE;
        fillColorBtnConstraints.gridx = 2;
        fillColorBtnConstraints.gridy = 2;
        fillColorBtnConstraints.weightx = 0;
        fillColorBtnConstraints.weighty = 0;
        fillColorBtnConstraints.insets = new Insets(0, 0, 5, 5);
        curvePanel.add(fillColorBtn, fillColorBtnConstraints);

        GridBagConstraints fillStyleLabelConstraints = new GridBagConstraints();
        fillStyleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        fillStyleLabelConstraints.gridx = 0;
        fillStyleLabelConstraints.gridy = 3;
        fillStyleLabelConstraints.weightx = 0;
        fillStyleLabelConstraints.weighty = 0;
        fillStyleLabelConstraints.insets = new Insets(0, 5, 5, 10);
        curvePanel.add(fillStyleLabel, fillStyleLabelConstraints);
        GridBagConstraints fillStyleComboConstraints = new GridBagConstraints();
        fillStyleComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        fillStyleComboConstraints.gridx = 1;
        fillStyleComboConstraints.gridy = 3;
        fillStyleComboConstraints.weightx = 1;
        fillStyleComboConstraints.weighty = 0;
        fillStyleComboConstraints.insets = new Insets(0, 0, 5, 5);
        fillStyleComboConstraints.gridwidth = 2;
        curvePanel.add(fillStyleCombo, fillStyleComboConstraints);

        GridBagConstraints lineWidthLabelConstraints = new GridBagConstraints();
        lineWidthLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        lineWidthLabelConstraints.gridx = 0;
        lineWidthLabelConstraints.gridy = 4;
        lineWidthLabelConstraints.weightx = 0;
        lineWidthLabelConstraints.weighty = 0;
        lineWidthLabelConstraints.insets = new Insets(0, 5, 5, 10);
        curvePanel.add(lineWidthLabel, lineWidthLabelConstraints);
        GridBagConstraints lineWidthSpinnerConstraints = new GridBagConstraints();
        lineWidthSpinnerConstraints.fill = GridBagConstraints.HORIZONTAL;
        lineWidthSpinnerConstraints.gridx = 1;
        lineWidthSpinnerConstraints.gridy = 4;
        lineWidthSpinnerConstraints.weightx = 1;
        lineWidthSpinnerConstraints.weighty = 0;
        lineWidthSpinnerConstraints.insets = new Insets(0, 0, 5, 5);
        lineWidthSpinnerConstraints.gridwidth = 2;
        curvePanel.add(lineWidthSpinner, lineWidthSpinnerConstraints);

        GridBagConstraints lineDashLabelConstraints = new GridBagConstraints();
        lineDashLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        lineDashLabelConstraints.gridx = 0;
        lineDashLabelConstraints.gridy = 5;
        lineDashLabelConstraints.weightx = 0;
        lineDashLabelConstraints.weighty = 0;
        lineDashLabelConstraints.insets = new Insets(0, 5, 5, 10);
        curvePanel.add(lineStyleLabel, lineDashLabelConstraints);
        GridBagConstraints lineDashComboConstraints = new GridBagConstraints();
        lineDashComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        lineDashComboConstraints.gridx = 1;
        lineDashComboConstraints.gridy = 5;
        lineDashComboConstraints.weightx = 1;
        lineDashComboConstraints.weighty = 0;
        lineDashComboConstraints.insets = new Insets(0, 0, 5, 5);
        lineDashComboConstraints.gridwidth = 2;
        curvePanel.add(lineStyleCombo, lineDashComboConstraints);

        GridBagConstraints lineNameLabelConstraints = new GridBagConstraints();
        lineNameLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        lineNameLabelConstraints.gridx = 0;
        lineNameLabelConstraints.gridy = 6;
        lineNameLabelConstraints.weightx = 0;
        lineNameLabelConstraints.weighty = 0;
        lineNameLabelConstraints.insets = new Insets(0, 5, 5, 10);
        curvePanel.add(lineNameLabel, lineNameLabelConstraints);
        GridBagConstraints lineNameTextConstraints = new GridBagConstraints();
        lineNameTextConstraints.fill = GridBagConstraints.HORIZONTAL;
        lineNameTextConstraints.gridx = 1;
        lineNameTextConstraints.gridy = 6;
        lineNameTextConstraints.weightx = 1;
        lineNameTextConstraints.weighty = 0;
        lineNameTextConstraints.insets = new Insets(0, 0, 5, 5);
        lineNameTextConstraints.gridwidth = 2;
        curvePanel.add(lineNameText, lineNameTextConstraints);
    }

    protected void initBarPanel() {
        // Bar panel construction
        barPanel = new JPanel(new GridBagLayout());

        fillMethodLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.fill.method.text"));
        fillMethodLabel.setFont(CometeUtils.getLabelFont());
        fillMethodLabel.setForeground(CometeUtils.getfColor());
        fillMethodCombo = new JComboBox<>();
        fillMethodCombo.setFont(CometeUtils.getLabelFont());
        fillMethodCombo.addItem(IChartViewer.METHOD_FILL_FROM_TOP);
        fillMethodCombo.addItem(IChartViewer.METHOD_FILL_FROM_ZERO);
        fillMethodCombo.addItem(IChartViewer.METHOD_FILL_FROM_BOTTOM);
        fillMethodCombo.setSelectedIndex(0);
        fillMethodCombo.addActionListener(this);
        fillMethodCombo.setRenderer(new FillMethodListCellRenderer());

        barWidthLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.bar.width"));
        barWidthLabel.setFont(CometeUtils.getLabelFont());
        barWidthLabel.setForeground(CometeUtils.getfColor());
        barWidthSpinner = new JSpinner();
        barWidthSpinner.setPreferredSize(fillMethodCombo.getPreferredSize());
        Integer value = Integer.valueOf(10);
        Integer min = Integer.valueOf(0);
        Integer max = Integer.valueOf(100);
        Integer step = Integer.valueOf(1);
        SpinnerNumberModel spModel = new SpinnerNumberModel(value, min, max, step);
        barWidthSpinner.setModel(spModel);
        barWidthSpinner.addChangeListener(this);

        GridBagConstraints barWidthLabelConstraints = new GridBagConstraints();
        barWidthLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        barWidthLabelConstraints.gridx = 0;
        barWidthLabelConstraints.gridy = 0;
        barWidthLabelConstraints.weightx = 0;
        barWidthLabelConstraints.weighty = 0;
        barWidthLabelConstraints.insets = new Insets(5, 5, 0, 10);
        barPanel.add(barWidthLabel, barWidthLabelConstraints);
        GridBagConstraints barWidthSpinnerConstraints = new GridBagConstraints();
        barWidthSpinnerConstraints.fill = GridBagConstraints.HORIZONTAL;
        barWidthSpinnerConstraints.gridx = 1;
        barWidthSpinnerConstraints.gridy = 0;
        barWidthSpinnerConstraints.weightx = 1;
        barWidthSpinnerConstraints.weighty = 0;
        barWidthSpinnerConstraints.insets = new Insets(5, 0, 0, 5);
        barPanel.add(barWidthSpinner, barWidthSpinnerConstraints);

        GridBagConstraints fillMethodLabelConstraints = new GridBagConstraints();
        fillMethodLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        fillMethodLabelConstraints.gridx = 0;
        fillMethodLabelConstraints.gridy = 1;
        fillMethodLabelConstraints.weightx = 0;
        fillMethodLabelConstraints.weighty = 0;
        fillMethodLabelConstraints.insets = new Insets(5, 5, 5, 10);
        barPanel.add(fillMethodLabel, fillMethodLabelConstraints);
        barPanel.add(fillMethodLabel, fillMethodLabelConstraints);
        GridBagConstraints fillMethodComboConstraints = new GridBagConstraints();
        fillMethodComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        fillMethodComboConstraints.gridx = 1;
        fillMethodComboConstraints.gridy = 1;
        fillMethodComboConstraints.weightx = 1;
        fillMethodComboConstraints.weighty = 0;
        fillMethodComboConstraints.insets = new Insets(5, 0, 5, 5);
        barPanel.add(fillMethodCombo, fillMethodComboConstraints);
    }

    protected void initMarkerPanel() {
        // Marker panel construction
        markerPanel = new JPanel(new GridBagLayout());

        markerColorView = new JLabel(ObjectUtils.EMPTY_STRING);
        markerColorView.setBackground(Color.RED);
        markerColorView.setOpaque(true);
        markerColorView.setBorder(BorderFactory.createLineBorder(Color.black));
        markerColorView.setPreferredSize(CometeUtils.getViewLabelSize());
        markerColorView.setMinimumSize(markerColorView.getPreferredSize());

        markerColorBtn = new JButton(SUSPENSION_POINTS);
        markerColorBtn.setMargin(CometeUtils.getBtnInsets());
        markerColorBtn.addMouseListener(this);

        markerColorLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.color.marker"));
        markerColorLabel.setFont(CometeUtils.getLabelFont());
        markerColorLabel.setForeground(CometeUtils.getfColor());

        markerStyleLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.text"));
        markerStyleLabel.setFont(CometeUtils.getLabelFont());
        markerStyleLabel.setForeground(CometeUtils.getfColor());

        markerStyleCombo = new JComboBox<>();
        markerStyleCombo.setFont(CometeUtils.getLabelFont());
        markerStyleCombo.addItem(IChartViewer.MARKER_NONE);
        markerStyleCombo.addItem(IChartViewer.MARKER_DOT);
        markerStyleCombo.addItem(IChartViewer.MARKER_BOX);
        markerStyleCombo.addItem(IChartViewer.MARKER_TRIANGLE);
        markerStyleCombo.addItem(IChartViewer.MARKER_DIAMOND);
        markerStyleCombo.addItem(IChartViewer.MARKER_STAR);
        markerStyleCombo.addItem(IChartViewer.MARKER_VERT_LINE);
        markerStyleCombo.addItem(IChartViewer.MARKER_HORIZ_LINE);
        markerStyleCombo.addItem(IChartViewer.MARKER_CROSS);
        markerStyleCombo.addItem(IChartViewer.MARKER_CIRCLE);
        markerStyleCombo.addItem(IChartViewer.MARKER_SQUARE);
        markerStyleCombo.setRenderer(new MarkerStyleListCellRenderer());
        markerStyleCombo.setSelectedIndex(0);
        markerStyleCombo.addActionListener(this);
        markerStyleCombo.setForeground(markerColorView.getBackground());

        markerSizeLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.marker.size"));
        markerSizeLabel.setFont(CometeUtils.getLabelFont());
        markerSizeLabel.setForeground(CometeUtils.getfColor());

        markerSizeSpinner = new JSpinner();
        markerSizeSpinner.setPreferredSize(markerStyleCombo.getPreferredSize());
        Integer value = Integer.valueOf(6);
        Integer min = Integer.valueOf(2);
        Integer max = Integer.valueOf(99);
        Integer step = Integer.valueOf(2);
        SpinnerNumberModel spModel = new SpinnerNumberModel(value, min, max, step);
        markerSizeSpinner.setModel(spModel);
        markerSizeSpinner.addChangeListener(this);

        legendVisibleCheck = new JCheckBox();
        legendVisibleCheck.setFont(CometeUtils.getLabelFont());
        legendVisibleCheck.setForeground(CometeUtils.getfColor());
        legendVisibleCheck.setText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.legend.visible"));
        legendVisibleCheck.setSelected(true);
        legendVisibleCheck.addActionListener(this);

        samplingAllowedCheck = new JCheckBox();
        samplingAllowedCheck.setFont(CometeUtils.getLabelFont());
        samplingAllowedCheck.setForeground(CometeUtils.getfColor());
        samplingAllowedCheck
                .setText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.sampling.enabled.text"));
        samplingAllowedCheck
                .setToolTipText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.sampling.enabled.tooltip"));
        samplingAllowedCheck.setSelected(true);
        samplingAllowedCheck.addActionListener(this);

        int yIndex = 0;

        GridBagConstraints markerColorLabelConstraints = new GridBagConstraints();
        markerColorLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        markerColorLabelConstraints.gridx = 0;
        markerColorLabelConstraints.gridy = yIndex;
        markerColorLabelConstraints.weightx = 0;
        markerColorLabelConstraints.weighty = 0;
        markerColorLabelConstraints.insets = new Insets(5, 5, 0, 0);
        markerPanel.add(markerColorLabel, markerColorLabelConstraints);
        GridBagConstraints markerColorViewConstraints = new GridBagConstraints();
        markerColorViewConstraints.fill = GridBagConstraints.HORIZONTAL;
        markerColorViewConstraints.gridx = 1;
        markerColorViewConstraints.gridy = yIndex;
        markerColorViewConstraints.weightx = 1;
        markerColorViewConstraints.weighty = 0;
        markerColorViewConstraints.insets = new Insets(5, 0, 0, 5);
        markerPanel.add(markerColorView, markerColorViewConstraints);
        GridBagConstraints markerColorBtnConstraints = new GridBagConstraints();
        markerColorBtnConstraints.fill = GridBagConstraints.NONE;
        markerColorBtnConstraints.gridx = 2;
        markerColorBtnConstraints.gridy = yIndex++;
        markerColorBtnConstraints.weightx = 0;
        markerColorBtnConstraints.weighty = 0;
        markerColorBtnConstraints.insets = new Insets(5, 0, 0, 5);
        markerPanel.add(markerColorBtn, markerColorBtnConstraints);

        GridBagConstraints markerSizeLabelConstraints = new GridBagConstraints();
        markerSizeLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        markerSizeLabelConstraints.gridx = 0;
        markerSizeLabelConstraints.gridy = yIndex;
        markerSizeLabelConstraints.weightx = 0;
        markerSizeLabelConstraints.weighty = 0;
        markerSizeLabelConstraints.insets = new Insets(5, 5, 0, 0);
        markerPanel.add(markerSizeLabel, markerSizeLabelConstraints);
        GridBagConstraints markerSizeSpinnerConstraints = new GridBagConstraints();
        markerSizeSpinnerConstraints.fill = GridBagConstraints.HORIZONTAL;
        markerSizeSpinnerConstraints.gridx = 1;
        markerSizeSpinnerConstraints.gridy = yIndex++;
        markerSizeSpinnerConstraints.weightx = 1;
        markerSizeSpinnerConstraints.weighty = 0;
        markerSizeSpinnerConstraints.insets = new Insets(5, 0, 0, 5);
        markerPanel.add(markerSizeSpinner, markerSizeSpinnerConstraints);

        GridBagConstraints markerStyleLabelConstraints = new GridBagConstraints();
        markerStyleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        markerStyleLabelConstraints.gridx = 0;
        markerStyleLabelConstraints.gridy = yIndex;
        markerStyleLabelConstraints.weightx = 0;
        markerStyleLabelConstraints.weighty = 0;
        markerStyleLabelConstraints.insets = new Insets(5, 5, 0, 0);
        markerPanel.add(markerStyleLabel, markerStyleLabelConstraints);
        GridBagConstraints markerStyleComboConstraints = new GridBagConstraints();
        markerStyleComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        markerStyleComboConstraints.gridx = 1;
        markerStyleComboConstraints.gridy = yIndex++;
        markerStyleComboConstraints.weightx = 1;
        markerStyleComboConstraints.weighty = 0;
        markerStyleComboConstraints.insets = new Insets(5, 0, 0, 5);
        markerPanel.add(markerStyleCombo, markerStyleComboConstraints);

        GridBagConstraints labelVisibleCheckConstraints = new GridBagConstraints();
        labelVisibleCheckConstraints.fill = GridBagConstraints.HORIZONTAL;
        labelVisibleCheckConstraints.gridx = 0;
        labelVisibleCheckConstraints.gridy = yIndex++;
        labelVisibleCheckConstraints.weightx = 0;
        labelVisibleCheckConstraints.weighty = 0;
        labelVisibleCheckConstraints.insets = new Insets(5, 5, 0, 0);
        markerPanel.add(legendVisibleCheck, labelVisibleCheckConstraints);

        GridBagConstraints samplingAllowedCheckConstraints = new GridBagConstraints();
        samplingAllowedCheckConstraints.fill = GridBagConstraints.HORIZONTAL;
        samplingAllowedCheckConstraints.gridx = 0;
        samplingAllowedCheckConstraints.gridy = yIndex;
        samplingAllowedCheckConstraints.weightx = 0;
        samplingAllowedCheckConstraints.weighty = 0;
        samplingAllowedCheckConstraints.insets = new Insets(5, 5, 5, 0);
        markerPanel.add(samplingAllowedCheck, samplingAllowedCheckConstraints);
    }

    protected void initErrorPanel() {
        // Error panel construction
        errorPanel = new JPanel(new GridBagLayout());

        errorColorView = new JLabel(ObjectUtils.EMPTY_STRING);
        errorColorView.setBackground(Color.RED);
        errorColorView.setOpaque(true);
        errorColorView.setBorder(BorderFactory.createLineBorder(Color.black));
        errorColorView.setPreferredSize(CometeUtils.getViewLabelSize());
        errorColorView.setMinimumSize(errorColorView.getPreferredSize());

        errorColorBtn = new JButton(SUSPENSION_POINTS);
        errorColorBtn.setMargin(CometeUtils.getBtnInsets());
        errorColorBtn.addMouseListener(this);

        errorColorLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.color.error"));
        errorColorLabel.setFont(CometeUtils.getLabelFont());
        errorColorLabel.setForeground(CometeUtils.getfColor());

        errorVisibleCheck = new JCheckBox();
        errorVisibleCheck.setFont(CometeUtils.getLabelFont());
        errorVisibleCheck.setForeground(CometeUtils.getfColor());
        errorVisibleCheck.setText(ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.visible"));
        errorVisibleCheck.setSelected(true);
        errorVisibleCheck.addActionListener(this);

        int yIndex = 0;

        GridBagConstraints errorColorLabelConstraints = new GridBagConstraints();
        errorColorLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        errorColorLabelConstraints.gridx = 0;
        errorColorLabelConstraints.gridy = yIndex;
        errorColorLabelConstraints.weightx = 0;
        errorColorLabelConstraints.weighty = 0;
        errorColorLabelConstraints.insets = new Insets(5, 5, 0, 0);
        errorPanel.add(errorColorLabel, errorColorLabelConstraints);
        GridBagConstraints errorColorViewConstraints = new GridBagConstraints();
        errorColorViewConstraints.fill = GridBagConstraints.HORIZONTAL;
        errorColorViewConstraints.gridx = 1;
        errorColorViewConstraints.gridy = yIndex;
        errorColorViewConstraints.weightx = 1;
        errorColorViewConstraints.weighty = 0;
        errorColorViewConstraints.insets = new Insets(5, 0, 0, 5);
        errorPanel.add(errorColorView, errorColorViewConstraints);
        GridBagConstraints errorColorBtnConstraints = new GridBagConstraints();
        errorColorBtnConstraints.fill = GridBagConstraints.NONE;
        errorColorBtnConstraints.gridx = 2;
        errorColorBtnConstraints.gridy = yIndex++;
        errorColorBtnConstraints.weightx = 0;
        errorColorBtnConstraints.weighty = 0;
        errorColorBtnConstraints.insets = new Insets(5, 0, 0, 5);
        errorPanel.add(errorColorBtn, errorColorBtnConstraints);

        GridBagConstraints errorVisibleCheckConstraints = new GridBagConstraints();
        errorVisibleCheckConstraints.fill = GridBagConstraints.HORIZONTAL;
        errorVisibleCheckConstraints.gridx = 0;
        errorVisibleCheckConstraints.gridy = yIndex;
        errorVisibleCheckConstraints.weightx = 0;
        errorVisibleCheckConstraints.weighty = 0;
        errorVisibleCheckConstraints.insets = new Insets(5, 5, 5, 0);
        errorPanel.add(errorVisibleCheck, errorVisibleCheckConstraints);
    }

    protected void initTransformPanel() {
        // Transform panel construction
        transformPanel = new JPanel(new GridBagLayout());

        transformHelpLabel = new JTextArea(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.transform.tooltip"));
        transformHelpLabel.setLineWrap(true);
        transformHelpLabel.setWrapStyleWord(true);
        transformHelpLabel.setFont(CometeUtils.getLabelFont());
        transformHelpLabel.setForeground(CometeUtils.getfColor());
        transformHelpLabel.setFont(markerStyleLabel.getFont());
        transformHelpLabel.setEditable(false);
        transformHelpLabel.setBackground(markerStyleLabel.getBackground());

        transformA0Label = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.transform.a0"));
        transformA0Label.setFont(CometeUtils.getLabelFont());
        transformA0Label.setForeground(CometeUtils.getfColor());
        transformA0Text = new JTextField(8);
        transformA0Text.setMargin(CometeUtils.getTextFieldInsets());
        transformA0Text.setEditable(true);
        transformA0Text.setText(Double.toString(0));
        transformA0Text.addKeyListener(this);

        transformA1Label = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.transform.a1"));
        transformA1Label.setFont(CometeUtils.getLabelFont());
        transformA1Label.setForeground(CometeUtils.getfColor());
        transformA1Text = new JTextField(8);
        transformA1Text.setMargin(CometeUtils.getTextFieldInsets());
        transformA1Text.setEditable(true);
        transformA1Text.setText(Double.toString(1));
        transformA1Text.addKeyListener(this);

        transformA2Label = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.transform.a2"));
        transformA2Label.setFont(CometeUtils.getLabelFont());
        transformA2Label.setForeground(CometeUtils.getfColor());
        transformA2Text = new JTextField(8);
        transformA2Text.setMargin(CometeUtils.getTextFieldInsets());
        transformA2Text.setEditable(true);
        transformA2Text.setText(Double.toString(0));
        transformA2Text.addKeyListener(this);

        GridBagConstraints transformHelpLabelConstraints = new GridBagConstraints();
        transformHelpLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        transformHelpLabelConstraints.gridx = 0;
        transformHelpLabelConstraints.gridy = 0;
        transformHelpLabelConstraints.weightx = 1;
        transformHelpLabelConstraints.weighty = 0;
        transformHelpLabelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        transformHelpLabelConstraints.insets = new Insets(5, 5, 0, 5);
        transformPanel.add(transformHelpLabel, transformHelpLabelConstraints);

        GridBagConstraints transformA0LabelConstraints = new GridBagConstraints();
        transformA0LabelConstraints.fill = GridBagConstraints.NONE;
        transformA0LabelConstraints.gridx = 0;
        transformA0LabelConstraints.gridy = 1;
        transformA0LabelConstraints.weightx = 0;
        transformA0LabelConstraints.weighty = 0;
        transformA0LabelConstraints.insets = new Insets(5, 5, 0, 10);
        transformPanel.add(transformA0Label, transformA0LabelConstraints);
        GridBagConstraints transformA0TextConstraints = new GridBagConstraints();
        transformA0TextConstraints.fill = GridBagConstraints.HORIZONTAL;
        transformA0TextConstraints.gridx = 1;
        transformA0TextConstraints.gridy = 1;
        transformA0TextConstraints.weightx = 1;
        transformA0TextConstraints.weighty = 0;
        transformA0TextConstraints.insets = new Insets(5, 0, 0, 5);
        transformPanel.add(transformA0Text, transformA0TextConstraints);

        GridBagConstraints transformA1LabelConstraints = new GridBagConstraints();
        transformA1LabelConstraints.fill = GridBagConstraints.NONE;
        transformA1LabelConstraints.gridx = 0;
        transformA1LabelConstraints.gridy = 2;
        transformA1LabelConstraints.weightx = 0;
        transformA1LabelConstraints.weighty = 0;
        transformA1LabelConstraints.insets = new Insets(5, 5, 0, 10);
        transformPanel.add(transformA1Label, transformA1LabelConstraints);
        GridBagConstraints transformA1TextConstraints = new GridBagConstraints();
        transformA1TextConstraints.fill = GridBagConstraints.HORIZONTAL;
        transformA1TextConstraints.gridx = 1;
        transformA1TextConstraints.gridy = 2;
        transformA1TextConstraints.weightx = 1;
        transformA1TextConstraints.weighty = 0;
        transformA1TextConstraints.insets = new Insets(5, 0, 0, 5);
        transformPanel.add(transformA1Text, transformA1TextConstraints);

        GridBagConstraints transformA2LabelConstraints = new GridBagConstraints();
        transformA2LabelConstraints.fill = GridBagConstraints.NONE;
        transformA2LabelConstraints.gridx = 0;
        transformA2LabelConstraints.gridy = 3;
        transformA2LabelConstraints.weightx = 0;
        transformA2LabelConstraints.weighty = 0;
        transformA2LabelConstraints.insets = new Insets(5, 5, 5, 10);
        transformPanel.add(transformA2Label, transformA2LabelConstraints);
        GridBagConstraints transformA2TextConstraints = new GridBagConstraints();
        transformA2TextConstraints.fill = GridBagConstraints.HORIZONTAL;
        transformA2TextConstraints.gridx = 1;
        transformA2TextConstraints.gridy = 3;
        transformA2TextConstraints.weightx = 1;
        transformA2TextConstraints.weighty = 0;
        transformA2TextConstraints.insets = new Insets(5, 0, 5, 5);
        transformPanel.add(transformA2Text, transformA2TextConstraints);
    }

    protected void initXOffsetPanel() {
        // XOffset panel construction
        xOffsetPanel = new JPanel(new GridBagLayout());

        xOffsetHelpLabel = new JTextArea(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.offset.x.tooltip"));
        xOffsetHelpLabel.setLineWrap(true);
        xOffsetHelpLabel.setWrapStyleWord(true);
        xOffsetHelpLabel.setFont(CometeUtils.getLabelFont());
        xOffsetHelpLabel.setForeground(CometeUtils.getfColor());
        xOffsetHelpLabel.setFont(markerStyleLabel.getFont());
        xOffsetHelpLabel.setEditable(false);
        xOffsetHelpLabel.setBackground(markerStyleLabel.getBackground());

        xOffsetA0Label = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.transform.a0"));
        xOffsetA0Label.setFont(CometeUtils.getLabelFont());
        xOffsetA0Label.setForeground(CometeUtils.getfColor());
        xOffsetA0Text = new JTextField(8);
        xOffsetA0Text.setMargin(CometeUtils.getTextFieldInsets());
        xOffsetA0Text.setEditable(true);
        xOffsetA0Text.setText(Double.toString(0));
        xOffsetA0Text.addKeyListener(this);

        GridBagConstraints xOffsetHelpLabelConstraints = new GridBagConstraints();
        xOffsetHelpLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        xOffsetHelpLabelConstraints.gridx = 0;
        xOffsetHelpLabelConstraints.gridy = 0;
        xOffsetHelpLabelConstraints.weightx = 1;
        xOffsetHelpLabelConstraints.weighty = 0;
        xOffsetHelpLabelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        xOffsetHelpLabelConstraints.insets = new Insets(5, 5, 0, 5);
        xOffsetPanel.add(xOffsetHelpLabel, xOffsetHelpLabelConstraints);

        GridBagConstraints xOffsetA0LabelConstraints = new GridBagConstraints();
        xOffsetA0LabelConstraints.fill = GridBagConstraints.NONE;
        xOffsetA0LabelConstraints.gridx = 0;
        xOffsetA0LabelConstraints.gridy = 1;
        xOffsetA0LabelConstraints.weightx = 0;
        xOffsetA0LabelConstraints.weighty = 0;
        xOffsetA0LabelConstraints.insets = new Insets(5, 5, 0, 10);
        xOffsetPanel.add(xOffsetA0Label, xOffsetA0LabelConstraints);
        GridBagConstraints xOffsetA0TextConstraints = new GridBagConstraints();
        xOffsetA0TextConstraints.fill = GridBagConstraints.HORIZONTAL;
        xOffsetA0TextConstraints.gridx = 1;
        xOffsetA0TextConstraints.gridy = 1;
        xOffsetA0TextConstraints.weightx = 1;
        xOffsetA0TextConstraints.weighty = 0;
        xOffsetA0TextConstraints.insets = new Insets(5, 0, 0, 5);
        xOffsetPanel.add(xOffsetA0Text, xOffsetA0TextConstraints);

    }

    protected void initInterpolationPanel() {
        // Interpolation panel
        interpPanel = new JPanel(new GridBagLayout());

        methodIntBtnGrp = new ButtonGroup();

        noInterpBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.interpolation.none"));
        noInterpBtn.setForeground(CometeUtils.getfColor());
        noInterpBtn.setFont(CometeUtils.getLabelFont());
        methodIntBtnGrp.add(noInterpBtn);
        linearBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.interpolation.linear"));
        linearBtn.setForeground(CometeUtils.getfColor());
        linearBtn.setFont(CometeUtils.getLabelFont());
        methodIntBtnGrp.add(linearBtn);
        cosineBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.interpolation.cosine"));
        cosineBtn.setForeground(CometeUtils.getfColor());
        cosineBtn.setFont(CometeUtils.getLabelFont());
        methodIntBtnGrp.add(cosineBtn);
        cubicBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.interpolation.cubic"));
        cubicBtn.setForeground(CometeUtils.getfColor());
        cubicBtn.setFont(CometeUtils.getLabelFont());
        methodIntBtnGrp.add(cubicBtn);
        hermiteBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.interpolation.hermite"));
        hermiteBtn.setForeground(CometeUtils.getfColor());
        hermiteBtn.setFont(CometeUtils.getLabelFont());
        methodIntBtnGrp.add(hermiteBtn);

        JLabel tensionLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.interpolation.tension"));
        tensionLabel.setFont(CometeUtils.getLabelFont());
        tensionLabel.setForeground(CometeUtils.getfColor());
        tensionLabel.setHorizontalAlignment(JLabel.RIGHT);
        tensionText = new JTextField(4);
        tensionText.setMargin(CometeUtils.getTextFieldInsets());
        tensionText.setFont(CometeUtils.getLabelFont());
        tensionText.setEditable(true);
        tensionText.setEnabled(false);
        tensionText.addKeyListener(this);

        JLabel stepLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.interpolation.step"));
        stepLabel.setFont(CometeUtils.getLabelFont());
        stepLabel.setForeground(CometeUtils.getfColor());
        stepLabel.setHorizontalAlignment(JLabel.RIGHT);
        stepSpinner = new JSpinner();
        stepSpinner.setPreferredSize(tensionText.getPreferredSize());
        Integer value = Integer.valueOf(10);
        Integer min = Integer.valueOf(2);
        Integer max = Integer.valueOf(100);
        Integer step = Integer.valueOf(1);
        SpinnerNumberModel spModel = new SpinnerNumberModel(value, min, max, step);
        stepSpinner.setModel(spModel);
        stepSpinner.addChangeListener(this);

        JLabel biasLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.interpolation.bias"));
        biasLabel.setFont(CometeUtils.getLabelFont());
        biasLabel.setForeground(CometeUtils.getfColor());
        biasLabel.setHorizontalAlignment(JLabel.RIGHT);
        biasText = new JTextField(4);
        biasText.setMargin(CometeUtils.getTextFieldInsets());
        biasText.setFont(CometeUtils.getLabelFont());
        biasText.setEditable(true);
        biasText.setEnabled(false);
        biasText.addKeyListener(this);

        noInterpBtn.setSelected(true);

        noInterpBtn.addChangeListener(this);
        linearBtn.addChangeListener(this);
        cosineBtn.addChangeListener(this);
        cubicBtn.addChangeListener(this);
        hermiteBtn.addChangeListener(this);
        tensionText.setText(Double.toString(0));
        biasText.setText(Double.toString(0));

        GridBagConstraints noInterpBtnConstraints = new GridBagConstraints();
        noInterpBtnConstraints.fill = GridBagConstraints.NONE;
        noInterpBtnConstraints.gridx = 0;
        noInterpBtnConstraints.gridy = 0;
        noInterpBtnConstraints.weightx = 0;
        noInterpBtnConstraints.weighty = 0;
        noInterpBtnConstraints.insets = new Insets(5, 5, 0, 20);
        noInterpBtnConstraints.anchor = GridBagConstraints.WEST;
        interpPanel.add(noInterpBtn, noInterpBtnConstraints);
        GridBagConstraints stepLabelConstraints = new GridBagConstraints();
        stepLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        stepLabelConstraints.gridx = 1;
        stepLabelConstraints.gridy = 0;
        stepLabelConstraints.weightx = 1;
        stepLabelConstraints.weighty = 0;
        stepLabelConstraints.insets = new Insets(5, 0, 0, 5);
        interpPanel.add(stepLabel, stepLabelConstraints);
        GridBagConstraints stepSpinnerConstraints = new GridBagConstraints();
        stepSpinnerConstraints.fill = GridBagConstraints.NONE;
        stepSpinnerConstraints.gridx = 2;
        stepSpinnerConstraints.gridy = 0;
        stepSpinnerConstraints.weightx = 0;
        stepSpinnerConstraints.weighty = 0;
        stepSpinnerConstraints.insets = new Insets(5, 5, 0, 5);
        interpPanel.add(stepSpinner, stepSpinnerConstraints);

        GridBagConstraints linearBtnConstraints = new GridBagConstraints();
        linearBtnConstraints.fill = GridBagConstraints.NONE;
        linearBtnConstraints.gridx = 0;
        linearBtnConstraints.gridy = 1;
        linearBtnConstraints.weightx = 0;
        linearBtnConstraints.weighty = 0;
        linearBtnConstraints.insets = new Insets(5, 5, 0, 20);
        linearBtnConstraints.anchor = GridBagConstraints.WEST;
        interpPanel.add(linearBtn, linearBtnConstraints);

        GridBagConstraints cosineBtnConstraints = new GridBagConstraints();
        cosineBtnConstraints.fill = GridBagConstraints.NONE;
        cosineBtnConstraints.gridx = 0;
        cosineBtnConstraints.gridy = 2;
        cosineBtnConstraints.weightx = 0;
        cosineBtnConstraints.weighty = 0;
        cosineBtnConstraints.insets = new Insets(5, 5, 0, 20);
        cosineBtnConstraints.anchor = GridBagConstraints.WEST;
        interpPanel.add(cosineBtn, cosineBtnConstraints);

        GridBagConstraints cubicBtnConstraints = new GridBagConstraints();
        cubicBtnConstraints.fill = GridBagConstraints.NONE;
        cubicBtnConstraints.gridx = 0;
        cubicBtnConstraints.gridy = 3;
        cubicBtnConstraints.weightx = 0;
        cubicBtnConstraints.weighty = 0;
        cubicBtnConstraints.insets = new Insets(5, 5, 0, 20);
        cubicBtnConstraints.anchor = GridBagConstraints.WEST;
        interpPanel.add(cubicBtn, cubicBtnConstraints);

        GridBagConstraints hermiteBtnConstraints = new GridBagConstraints();
        hermiteBtnConstraints.fill = GridBagConstraints.NONE;
        hermiteBtnConstraints.gridx = 0;
        hermiteBtnConstraints.gridy = 4;
        hermiteBtnConstraints.weightx = 0;
        hermiteBtnConstraints.weighty = 0;
        hermiteBtnConstraints.insets = new Insets(5, 5, 5, 20);
        hermiteBtnConstraints.anchor = GridBagConstraints.WEST;
        interpPanel.add(hermiteBtn, hermiteBtnConstraints);
        GridBagConstraints tensionLabelConstraints = new GridBagConstraints();
        tensionLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        tensionLabelConstraints.gridx = 1;
        tensionLabelConstraints.gridy = 4;
        tensionLabelConstraints.weightx = 1;
        tensionLabelConstraints.weighty = 0;
        tensionLabelConstraints.insets = new Insets(5, 0, 0, 5);
        interpPanel.add(tensionLabel, tensionLabelConstraints);
        GridBagConstraints tensionTextConstraints = new GridBagConstraints();
        tensionTextConstraints.fill = GridBagConstraints.NONE;
        tensionTextConstraints.gridx = 2;
        tensionTextConstraints.gridy = 4;
        tensionTextConstraints.weightx = 0;
        tensionTextConstraints.weighty = 0;
        tensionTextConstraints.insets = new Insets(5, 5, 0, 5);
        interpPanel.add(tensionText, tensionTextConstraints);

        GridBagConstraints biasLabelConstraints = new GridBagConstraints();
        biasLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        biasLabelConstraints.gridx = 1;
        biasLabelConstraints.gridy = 5;
        biasLabelConstraints.weightx = 1;
        biasLabelConstraints.weighty = 0;
        biasLabelConstraints.insets = new Insets(5, 0, 0, 5);
        interpPanel.add(biasLabel, biasLabelConstraints);
        GridBagConstraints biasTextConstraints = new GridBagConstraints();
        biasTextConstraints.fill = GridBagConstraints.NONE;
        biasTextConstraints.gridx = 2;
        biasTextConstraints.gridy = 5;
        biasTextConstraints.weightx = 0;
        biasTextConstraints.weighty = 0;
        biasTextConstraints.insets = new Insets(5, 5, 0, 5);
        interpPanel.add(biasText, biasTextConstraints);
    }

    protected void initSmoothingPanel() {
        // Smoothing panel
        smoothPanel = new JPanel(new GridBagLayout());

        methodSmBtnGrp = new ButtonGroup();

        noSmoothBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.smoothing.none"));
        noSmoothBtn.setForeground(CometeUtils.getfColor());
        noSmoothBtn.setFont(CometeUtils.getLabelFont());
        methodSmBtnGrp.add(noSmoothBtn);

        flatSmoothBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.smoothing.flat"));
        flatSmoothBtn.setForeground(CometeUtils.getfColor());
        flatSmoothBtn.setFont(CometeUtils.getLabelFont());
        methodSmBtnGrp.add(flatSmoothBtn);

        triangularSmoothBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.smoothing.linear"));
        triangularSmoothBtn.setForeground(CometeUtils.getfColor());
        triangularSmoothBtn.setFont(CometeUtils.getLabelFont());
        methodSmBtnGrp.add(triangularSmoothBtn);

        gaussianSmoothBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.smoothing.gaussian"));
        gaussianSmoothBtn.setForeground(CometeUtils.getfColor());
        gaussianSmoothBtn.setFont(CometeUtils.getLabelFont());
        methodSmBtnGrp.add(gaussianSmoothBtn);

        JLabel sigmaLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.smoothing.sigma"));
        sigmaLabel.setForeground(CometeUtils.getfColor());
        sigmaLabel.setHorizontalAlignment(JLabel.RIGHT);
        sigmaText = new JTextField(4);
        sigmaText.setText(Double.toString(0));
        sigmaText.setMargin(CometeUtils.getTextFieldInsets());
        sigmaText.setFont(CometeUtils.getLabelFont());
        sigmaText.setEditable(true);
        sigmaText.setEnabled(false);
        sigmaText.addKeyListener(this);

        JLabel neighborLabel = new JLabel(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.smoothing.neighbors"));
        neighborLabel.setFont(CometeUtils.getLabelFont());
        neighborLabel.setForeground(CometeUtils.getfColor());
        neighborLabel.setHorizontalAlignment(JLabel.RIGHT);
        neighborSpinner = new JSpinner();
        neighborSpinner.setPreferredSize(sigmaText.getPreferredSize());
        Integer value = Integer.valueOf(2);
        Integer min = Integer.valueOf(2);
        Integer max = Integer.valueOf(99);
        Integer step = Integer.valueOf(2);
        SpinnerNumberModel spModel = new SpinnerNumberModel(value, min, max, step);
        neighborSpinner.setModel(spModel);
        neighborSpinner.addChangeListener(this);

        methodExtBtnGrp = new ButtonGroup();

        noExtBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.smoothing.none"));
        noExtBtn.setHorizontalAlignment(JRadioButton.LEFT);
        noExtBtn.setForeground(CometeUtils.getfColor());
        noExtBtn.setFont(CometeUtils.getLabelFont());
        methodExtBtnGrp.add(noExtBtn);

        flatExtBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.smoothing.flat"));
        flatExtBtn.setHorizontalAlignment(JRadioButton.LEFT);
        flatExtBtn.setForeground(CometeUtils.getfColor());
        flatExtBtn.setFont(CometeUtils.getLabelFont());
        methodExtBtnGrp.add(flatExtBtn);

        linearExtBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.smoothing.linear"));
        linearExtBtn.setHorizontalAlignment(JRadioButton.LEFT);
        linearExtBtn.setForeground(CometeUtils.getfColor());
        linearExtBtn.setFont(CometeUtils.getLabelFont());
        methodExtBtnGrp.add(linearExtBtn);

        GridBagConstraints noSmoothBtnConstraints = new GridBagConstraints();
        noSmoothBtnConstraints.fill = GridBagConstraints.NONE;
        noSmoothBtnConstraints.gridx = 0;
        noSmoothBtnConstraints.gridy = 0;
        noSmoothBtnConstraints.weightx = 0;
        noSmoothBtnConstraints.weighty = 0;
        noSmoothBtnConstraints.insets = new Insets(5, 5, 0, 20);
        noSmoothBtnConstraints.anchor = GridBagConstraints.WEST;
        smoothPanel.add(noSmoothBtn, noSmoothBtnConstraints);
        GridBagConstraints neighborLabelConstraints = new GridBagConstraints();
        neighborLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        neighborLabelConstraints.gridx = 1;
        neighborLabelConstraints.gridy = 0;
        neighborLabelConstraints.weightx = 1;
        neighborLabelConstraints.weighty = 0;
        neighborLabelConstraints.insets = new Insets(5, 0, 0, 5);
        smoothPanel.add(neighborLabel, neighborLabelConstraints);
        GridBagConstraints neighborSpinnerConstraints = new GridBagConstraints();
        neighborSpinnerConstraints.fill = GridBagConstraints.NONE;
        neighborSpinnerConstraints.gridx = 2;
        neighborSpinnerConstraints.gridy = 0;
        neighborSpinnerConstraints.weightx = 0;
        neighborSpinnerConstraints.weighty = 0;
        neighborSpinnerConstraints.insets = new Insets(5, 5, 0, 5);
        smoothPanel.add(neighborSpinner, neighborSpinnerConstraints);

        GridBagConstraints flatSmoothBtnConstraints = new GridBagConstraints();
        flatSmoothBtnConstraints.fill = GridBagConstraints.NONE;
        flatSmoothBtnConstraints.gridx = 0;
        flatSmoothBtnConstraints.gridy = 1;
        flatSmoothBtnConstraints.weightx = 0;
        flatSmoothBtnConstraints.weighty = 0;
        flatSmoothBtnConstraints.insets = new Insets(5, 5, 0, 20);
        flatSmoothBtnConstraints.anchor = GridBagConstraints.WEST;
        smoothPanel.add(flatSmoothBtn, flatSmoothBtnConstraints);

        GridBagConstraints triangularSmoothBtnConstraints = new GridBagConstraints();
        triangularSmoothBtnConstraints.fill = GridBagConstraints.NONE;
        triangularSmoothBtnConstraints.gridx = 0;
        triangularSmoothBtnConstraints.gridy = 2;
        triangularSmoothBtnConstraints.weightx = 0;
        triangularSmoothBtnConstraints.weighty = 0;
        triangularSmoothBtnConstraints.insets = new Insets(5, 5, 0, 20);
        triangularSmoothBtnConstraints.anchor = GridBagConstraints.WEST;
        smoothPanel.add(triangularSmoothBtn, triangularSmoothBtnConstraints);

        GridBagConstraints gaussianSmoothBtnConstraints = new GridBagConstraints();
        gaussianSmoothBtnConstraints.fill = GridBagConstraints.NONE;
        gaussianSmoothBtnConstraints.gridx = 0;
        gaussianSmoothBtnConstraints.gridy = 3;
        gaussianSmoothBtnConstraints.weightx = 0;
        gaussianSmoothBtnConstraints.weighty = 0;
        gaussianSmoothBtnConstraints.insets = new Insets(5, 5, 0, 20);
        gaussianSmoothBtnConstraints.anchor = GridBagConstraints.WEST;
        smoothPanel.add(gaussianSmoothBtn, gaussianSmoothBtnConstraints);
        GridBagConstraints sigmaLabelConstraints = new GridBagConstraints();
        sigmaLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        sigmaLabelConstraints.gridx = 1;
        sigmaLabelConstraints.gridy = 3;
        sigmaLabelConstraints.weightx = 1;
        sigmaLabelConstraints.weighty = 0;
        sigmaLabelConstraints.insets = new Insets(5, 0, 0, 5);
        smoothPanel.add(sigmaLabel, sigmaLabelConstraints);
        GridBagConstraints sigmaTextConstraints = new GridBagConstraints();
        sigmaTextConstraints.fill = GridBagConstraints.NONE;
        sigmaTextConstraints.gridx = 2;
        sigmaTextConstraints.gridy = 3;
        sigmaTextConstraints.weightx = 0;
        sigmaTextConstraints.weighty = 0;
        sigmaTextConstraints.insets = new Insets(5, 5, 0, 5);
        smoothPanel.add(sigmaText, sigmaTextConstraints);

        JPanel boundaryPanel = new JPanel(new GridBagLayout());
        boundaryPanel.setBorder(CometeUtils.createTitleBorder(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.smoothing.extrapolation")));
        GridBagConstraints noExtBtnConstraints = new GridBagConstraints();
        noExtBtnConstraints.fill = GridBagConstraints.HORIZONTAL;
        noExtBtnConstraints.gridx = 0;
        noExtBtnConstraints.gridy = 0;
        noExtBtnConstraints.weightx = 1;
        noExtBtnConstraints.weighty = 0;
        noExtBtnConstraints.insets = new Insets(5, 5, 0, 5);
        boundaryPanel.add(noExtBtn, noExtBtnConstraints);
        GridBagConstraints flatExtBtnConstraints = new GridBagConstraints();
        flatExtBtnConstraints.fill = GridBagConstraints.HORIZONTAL;
        flatExtBtnConstraints.gridx = 0;
        flatExtBtnConstraints.gridy = 1;
        flatExtBtnConstraints.weightx = 1;
        flatExtBtnConstraints.weighty = 0;
        flatExtBtnConstraints.insets = new Insets(5, 5, 0, 5);
        boundaryPanel.add(flatExtBtn, flatExtBtnConstraints);
        GridBagConstraints linearExtBtnConstraints = new GridBagConstraints();
        linearExtBtnConstraints.fill = GridBagConstraints.HORIZONTAL;
        linearExtBtnConstraints.gridx = 0;
        linearExtBtnConstraints.gridy = 2;
        linearExtBtnConstraints.weightx = 1;
        linearExtBtnConstraints.weighty = 0;
        linearExtBtnConstraints.insets = new Insets(5, 5, 0, 5);
        boundaryPanel.add(linearExtBtn, linearExtBtnConstraints);
        GridBagConstraints boundaryPanelConstraints = new GridBagConstraints();
        boundaryPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        boundaryPanelConstraints.gridx = 0;
        boundaryPanelConstraints.gridy = 4;
        boundaryPanelConstraints.weightx = 1;
        boundaryPanelConstraints.weighty = 0;
        boundaryPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        boundaryPanelConstraints.insets = new Insets(5, 5, 5, 5);
        smoothPanel.add(boundaryPanel, boundaryPanelConstraints);

        noExtBtn.setSelected(true);

        noSmoothBtn.setSelected(true);

        noExtBtn.addChangeListener(this);
        flatExtBtn.addChangeListener(this);
        linearExtBtn.addChangeListener(this);

        noSmoothBtn.addChangeListener(this);
        flatSmoothBtn.addChangeListener(this);
        triangularSmoothBtn.addChangeListener(this);
        gaussianSmoothBtn.addChangeListener(this);
    }

    protected void initMathPanel() {
        // Math panel
        mathPanel = new JPanel(new GridBagLayout());

        mathBtnGrp = new ButtonGroup();

        noMathBtn = new JRadioButton(
                ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.math.function.no"));
        noMathBtn.setHorizontalAlignment(JRadioButton.LEFT);
        noMathBtn.setForeground(CometeUtils.getfColor());
        noMathBtn.setFont(CometeUtils.getLabelFont());
        mathBtnGrp.add(noMathBtn);
        derivativeBtn = new JRadioButton(DataViewOption.DERIVATIVE);
        derivativeBtn.setForeground(CometeUtils.getfColor());
        derivativeBtn.setFont(CometeUtils.getLabelFont());
        mathBtnGrp.add(derivativeBtn);
        integralBtn = new JRadioButton(DataViewOption.INTEGRAL);
        integralBtn.setHorizontalAlignment(JRadioButton.LEFT);
        integralBtn.setForeground(CometeUtils.getfColor());
        integralBtn.setFont(CometeUtils.getLabelFont());
        mathBtnGrp.add(integralBtn);
        fftModBtn = new JRadioButton(DataViewOption.FFT_MODULUS);
        fftModBtn.setHorizontalAlignment(JRadioButton.LEFT);
        fftModBtn.setForeground(CometeUtils.getfColor());
        fftModBtn.setFont(CometeUtils.getLabelFont());
        mathBtnGrp.add(fftModBtn);
        fftPhaseBtn = new JRadioButton(DataViewOption.FFT_PHASE);
        fftPhaseBtn.setHorizontalAlignment(JRadioButton.LEFT);
        fftPhaseBtn.setForeground(CometeUtils.getfColor());
        fftPhaseBtn.setFont(CometeUtils.getLabelFont());
        mathBtnGrp.add(fftPhaseBtn);

        noMathBtn.setSelected(true);

        noMathBtn.addChangeListener(this);
        derivativeBtn.addChangeListener(this);
        integralBtn.addChangeListener(this);
        fftModBtn.addChangeListener(this);
        fftPhaseBtn.addChangeListener(this);

        GridBagConstraints noMathBtnConstraints = new GridBagConstraints();
        noMathBtnConstraints.fill = GridBagConstraints.HORIZONTAL;
        noMathBtnConstraints.gridx = 0;
        noMathBtnConstraints.gridy = 0;
        noMathBtnConstraints.weightx = 1;
        noMathBtnConstraints.weighty = 0;
        noMathBtnConstraints.insets = new Insets(5, 5, 0, 5);
        mathPanel.add(noMathBtn, noMathBtnConstraints);
        GridBagConstraints derivativeBtnConstraints = new GridBagConstraints();
        derivativeBtnConstraints.fill = GridBagConstraints.HORIZONTAL;
        derivativeBtnConstraints.gridx = 0;
        derivativeBtnConstraints.gridy = 1;
        derivativeBtnConstraints.weightx = 1;
        derivativeBtnConstraints.weighty = 0;
        derivativeBtnConstraints.insets = new Insets(5, 5, 0, 5);
        mathPanel.add(derivativeBtn, derivativeBtnConstraints);
        GridBagConstraints integralBtnConstraints = new GridBagConstraints();
        integralBtnConstraints.fill = GridBagConstraints.HORIZONTAL;
        integralBtnConstraints.gridx = 0;
        integralBtnConstraints.gridy = 2;
        integralBtnConstraints.weightx = 1;
        integralBtnConstraints.weighty = 0;
        integralBtnConstraints.insets = new Insets(5, 5, 0, 5);
        mathPanel.add(integralBtn, integralBtnConstraints);
        GridBagConstraints fftModBtnConstraints = new GridBagConstraints();
        fftModBtnConstraints.fill = GridBagConstraints.HORIZONTAL;
        fftModBtnConstraints.gridx = 0;
        fftModBtnConstraints.gridy = 3;
        fftModBtnConstraints.weightx = 1;
        fftModBtnConstraints.weighty = 0;
        fftModBtnConstraints.insets = new Insets(5, 5, 0, 5);
        mathPanel.add(fftModBtn, fftModBtnConstraints);
        GridBagConstraints fftPhaseBtnConstraints = new GridBagConstraints();
        fftPhaseBtnConstraints.fill = GridBagConstraints.HORIZONTAL;
        fftPhaseBtnConstraints.gridx = 0;
        fftPhaseBtnConstraints.gridy = 4;
        fftPhaseBtnConstraints.weightx = 1;
        fftPhaseBtnConstraints.weighty = 0;
        fftPhaseBtnConstraints.insets = new Insets(5, 5, 5, 5);
        mathPanel.add(fftPhaseBtn, fftPhaseBtnConstraints);
    }

    @Override
    protected void updatePropertiesFromChart() {
        initProperties();
        applyProperties();
    }

    public void setDataViewId(String dataViewId) {
        this.dataViewId = dataViewId;
        initProperties();
        applyProperties();
    }

    public void setPlotProperties(PlotProperties plotProperties) {
        setPlotProperties(plotProperties, false);
    }

    protected void setPlotProperties(PlotProperties plotProperties, boolean commit) {
        if (plotProperties == null) {
            properties = new PlotProperties();
        } else {
            properties = plotProperties.clone();
            if (properties.getBar() == null) {
                properties.setBar(new BarProperties());
            }
            if (properties.getCurve() == null) {
                properties.setCurve(new CurveProperties());
            }
            if (properties.getMarker() == null) {
                properties.setMarker(new MarkerProperties());
            }
            if (properties.getError() == null) {
                properties.setError(new ErrorProperties());
            }
            if (properties.getTransform() == null) {
                properties.setTransform(new TransformationProperties());
            }
            if (properties.getInterpolation() == null) {
                properties.setInterpolation(new InterpolationProperties());
            }
            if (properties.getSmoothing() == null) {
                properties.setSmoothing(new SmoothingProperties());
            }
            if (properties.getMath() == null) {
                properties.setMath(new MathProperties());
            }
        }
        if (commit) {
            commit(ORIGIN_GLOBAL_PLOT);
        }
        applyProperties();
    }

    public PlotProperties getPlotProperties() {
        if (properties == null) {
            properties = new PlotProperties();
        }
        return properties.clone();
    }

    protected void initProperties() {
        if (chart == null) {
            setPlotProperties(null, false);
        } else {
            setPlotProperties(chart.getDataViewPlotProperties(dataViewId), false);
        }
    }

    protected void applyProperties() {
        PlotProperties plotProperties = getPlotProperties();
        viewTypeCombo.setSelectedIndex(plotProperties.getViewType());
        axisBox.setSelectedIndex(plotProperties.getAxisChoice());
        applyBar();
        applyCurve();
        applyMarker();
        applyError();
        applyTransform();
        applyXOffset();
        applyInterpolation();
        applySmoothing();
        applyMath();
    }

    protected void applyBar() {
        PlotProperties plotProperties = getPlotProperties();
        fillColorView.setBackground(ColorTool.getColor(plotProperties.getBar().getFillColor()));
        fillStyleCombo.setSelectedItem(plotProperties.getBar().getFillStyle());
        fillStyleCombo.setForeground(fillColorView.getBackground());
        fillMethodCombo.setSelectedIndex(plotProperties.getBar().getFillingMethod());
        Integer value = Integer.valueOf(plotProperties.getBar().getWidth());
        Integer min = Integer.valueOf(0);
        int tempMax = 100;
        if (value.intValue() > tempMax) {
            tempMax = value.intValue();
        }
        Integer max = Integer.valueOf(tempMax);
        Integer step = Integer.valueOf(1);
        SpinnerNumberModel spModel = new SpinnerNumberModel(value, min, max, step);
        barWidthSpinner.removeChangeListener(this);
        barWidthSpinner.setModel(spModel);
        barWidthSpinner.addChangeListener(this);
    }

    protected void applyCurve() {
        PlotProperties plotProperties = getPlotProperties();
        lineColorView.setBackground(ColorTool.getColor(plotProperties.getCurve().getColor()));
        lineStyleCombo.setSelectedIndex(plotProperties.getCurve().getLineStyle());
        lineStyleCombo.setForeground(lineColorView.getBackground());
        Integer value = Integer.valueOf(plotProperties.getCurve().getWidth());
        Integer min = Integer.valueOf(0);
        int tempMax = 10;
        if (value.intValue() > tempMax) {
            tempMax = value.intValue();
        }
        Integer max = Integer.valueOf(tempMax);
        Integer step = Integer.valueOf(1);
        SpinnerNumberModel spModel = new SpinnerNumberModel(value, min, max, step);
        lineWidthSpinner.removeChangeListener(this);
        lineWidthSpinner.setModel(spModel);
        lineWidthSpinner.addChangeListener(this);
        String name = plotProperties.getCurve().getName();
        if (name == null) {
            name = ObjectUtils.EMPTY_STRING;
        }
        lineNameText.setText(name);
        nameLabel.setText(name);
    }

    protected void applyError() {
        PlotProperties plotProperties = getPlotProperties();
        errorColorView.setBackground(ColorTool.getColor(plotProperties.getError().getColor()));
        errorVisibleCheck.setSelected(plotProperties.getError().isVisible());
    }

    protected void applyMarker() {
        PlotProperties plotProperties = getPlotProperties();
        markerColorView.setBackground(ColorTool.getColor(plotProperties.getMarker().getColor()));
        markerStyleCombo.setSelectedIndex(plotProperties.getMarker().getStyle());
        markerStyleCombo.setForeground(markerColorView.getBackground());
        legendVisibleCheck.setSelected(plotProperties.getMarker().isLegendVisible());
        samplingAllowedCheck.setSelected(plotProperties.getMarker().isSamplingAllowed());
        Integer value = Integer.valueOf(plotProperties.getMarker().getSize());
        Integer min = Integer.valueOf(0);
        int tempMax = 100;
        if (value.intValue() > tempMax) {
            tempMax = value.intValue();
        }
        Integer max = Integer.valueOf(tempMax);
        Integer step = Integer.valueOf(1);
        SpinnerNumberModel spModel = new SpinnerNumberModel(value, min, max, step);
        markerSizeSpinner.removeChangeListener(this);
        markerSizeSpinner.setModel(spModel);
        markerSizeSpinner.addChangeListener(this);
    }

    protected void applyTransform() {
        PlotProperties plotProperties = getPlotProperties();
        transformA0Text.setText(Double.toString(plotProperties.getTransform().getA0()));
        transformA1Text.setText(Double.toString(plotProperties.getTransform().getA1()));
        transformA2Text.setText(Double.toString(plotProperties.getTransform().getA2()));
    }

    protected void applyXOffset() {
        PlotProperties plotProperties = getPlotProperties();
        xOffsetA0Text.setText(Double.toString(plotProperties.getXOffset().getA0()));
    }

    protected void applyInterpolation() {
        PlotProperties plotProperties = getPlotProperties();
        Integer value = Integer.valueOf(plotProperties.getInterpolation().getInterpolationStep());
        Integer min = Integer.valueOf(2);
        int tempMax = 100;
        if (value.intValue() > tempMax) {
            tempMax = value.intValue();
        }
        Integer max = Integer.valueOf(tempMax);
        Integer step = Integer.valueOf(1);
        SpinnerNumberModel spModel = new SpinnerNumberModel(value, min, max, step);
        stepSpinner.removeChangeListener(this);
        stepSpinner.setModel(spModel);
        stepSpinner.addChangeListener(this);
        switch (plotProperties.getInterpolation().getInterpolationMethod()) {
            case IChartViewer.INTERPOLATE_NONE:
                noInterpBtn.setSelected(true);
                break;
            case IChartViewer.INTERPOLATE_LINEAR:
                linearBtn.setSelected(true);
                break;
            case IChartViewer.INTERPOLATE_CUBIC:
                cubicBtn.setSelected(true);
                break;
            case IChartViewer.INTERPOLATE_COSINE:
                cosineBtn.setSelected(true);
                break;
            case IChartViewer.INTERPOLATE_HERMITE:
                hermiteBtn.setSelected(true);
                break;
        }
        tensionText.setText(Double.toString(plotProperties.getInterpolation().getHermiteTension()));
        biasText.setText(Double.toString(plotProperties.getInterpolation().getHermiteBias()));
        updateControls();
    }

    protected void applySmoothing() {
        PlotProperties plotProperties = getPlotProperties();
        Integer value = Integer.valueOf(plotProperties.getSmoothing().getNeighbors());
        Integer min = Integer.valueOf(2);
        int tempMax = 99;
        if (value.intValue() > tempMax) {
            tempMax = value.intValue();
        }
        Integer max = Integer.valueOf(tempMax);
        Integer step = Integer.valueOf(2);
        SpinnerNumberModel spModel = new SpinnerNumberModel(value, min, max, step);
        neighborSpinner.removeChangeListener(this);
        neighborSpinner.setModel(spModel);
        neighborSpinner.addChangeListener(this);
        switch (plotProperties.getSmoothing().getMethod()) {
            case IChartViewer.SMOOTH_NONE:
                noSmoothBtn.setSelected(true);
                break;
            case IChartViewer.SMOOTH_FLAT:
                flatSmoothBtn.setSelected(true);
                break;
            case IChartViewer.SMOOTH_GAUSSIAN:
                gaussianSmoothBtn.setSelected(true);
                break;
        }
        switch (plotProperties.getSmoothing().getExtrapolation()) {
            case IChartViewer.SMOOTH_EXT_NONE:
                noExtBtn.setSelected(true);
                break;
            case IChartViewer.SMOOTH_EXT_FLAT:
                flatExtBtn.setSelected(true);
                break;
            case IChartViewer.SMOOTH_EXT_LINEAR:
                linearExtBtn.setSelected(true);
                break;
        }
        sigmaText.setText(Double.toString(plotProperties.getSmoothing().getGaussSigma()));
        updateControls();
    }

    protected void applyMath() {
        PlotProperties plotProperties = getPlotProperties();
        switch (plotProperties.getMath().getFunction()) {
            case IChartViewer.MATH_NONE:
                noMathBtn.setSelected(true);
                break;
            case IChartViewer.MATH_DERIVATIVE:
                derivativeBtn.setSelected(true);
                break;
            case IChartViewer.MATH_INTEGRAL:
                integralBtn.setSelected(true);
                break;
            case IChartViewer.MATH_FFT_MODULUS:
                fftModBtn.setSelected(true);
                break;
            case IChartViewer.MATH_FFT_PHASE:
                fftPhaseBtn.setSelected(true);
                break;
        }
    }

    protected void updateControls() {
        biasText.setEnabled(false);
        tensionText.setEnabled(false);
        sigmaText.setEnabled(false);
        switch (getPlotProperties().getInterpolation().getInterpolationMethod()) {
            case IChartViewer.INTERPOLATE_HERMITE:
                biasText.setEnabled(true);
                tensionText.setEnabled(true);
                break;
        }
        switch (getPlotProperties().getSmoothing().getMethod()) {
            case IChartViewer.SMOOTH_GAUSSIAN:
                sigmaText.setEnabled(true);
                break;
        }
    }

    @Override
    public void commit() {
        boolean wasAlwaysCommit = isAlwaysCommit();
        setAlwaysCommit(true);
        commit(ORIGIN_GLOBAL_PLOT);
        setAlwaysCommit(wasAlwaysCommit);
    }

    /**
     * Commit change if allowed
     */
    protected void commit(int origin) {
        if (isAlwaysCommit() && chart != null) {
            switch (origin) {
                case ORIGIN_BAR:
                    chart.setDataViewBarProperties(dataViewId, getPlotProperties().getBar());
                    break;
                case ORIGIN_CURVE:
                    chart.setDataViewCurveProperties(dataViewId, getPlotProperties().getCurve());
                    break;
                case ORIGIN_MARKER:
                    chart.setDataViewMarkerProperties(dataViewId, getPlotProperties().getMarker());
                    break;
                case ORIGIN_TRANSFORMATION:
                    chart.setDataViewTransformationProperties(dataViewId, getPlotProperties().getTransform());
                    break;
                case ORIGIN_INTERPOLATION:
                    chart.setDataViewInterpolationProperties(dataViewId, getPlotProperties().getInterpolation());
                    break;
                case ORIGIN_SMOOTHING:
                    chart.setDataViewSmoothingProperties(dataViewId, getPlotProperties().getSmoothing());
                    break;
                case ORIGIN_MATH:
                    chart.setDataViewMathProperties(dataViewId, getPlotProperties().getMath());
                    break;
                case ORIGIN_ERROR:
                    chart.setDataViewErrorProperties(dataViewId, getPlotProperties().getError());
                    break;
                case ORIGIN_X_OFFSET:
                    chart.setDataViewXOffsetProperties(dataViewId, getPlotProperties().getXOffset());
                    break;
                case ORIGIN_GLOBAL_PLOT:
                    chart.setDataViewPlotProperties(dataViewId, getPlotProperties());
                    break;
            }
            if (chart instanceof Component) {
                ((Component) chart).repaint();
            }
        }
    }

    // Mouse Listener
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == lineColorBtn) {
            Color c = JColorChooser.showDialog(this,
                    ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.color.line.choose"),
                    ColorTool.getColor(properties.getCurve().getColor()));
            if (c != null) {
                properties.getCurve().setColor(ColorTool.getCometeColor(c));
                lineColorView.setBackground(c);
                lineStyleCombo.setForeground(c);
                commit(ORIGIN_CURVE);
            }
        } else if (e.getSource() == fillColorBtn) {
            Color c = JColorChooser.showDialog(this,
                    ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.color.fill.choose"),
                    ColorTool.getColor(properties.getBar().getFillColor()));
            if (c != null) {
                properties.getBar().setFillColor(ColorTool.getCometeColor(c));
                fillColorView.setBackground(c);
                fillStyleCombo.setForeground(c);
                commit(ORIGIN_BAR);
            }
        } else if (e.getSource() == markerColorBtn) {
            Color c = JColorChooser.showDialog(this,
                    ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.color.marker.choose"),
                    ColorTool.getColor(properties.getMarker().getColor()));
            if (c != null) {
                properties.getMarker().setColor(ColorTool.getCometeColor(c));
                markerStyleCombo.setForeground(c);
                markerColorView.setBackground(c);
                commit(ORIGIN_MARKER);
            }
        } else if (e.getSource() == errorColorBtn) {
            Color c = JColorChooser.showDialog(this,
                    ChartUtils.MESSAGES.getString("fr.soleil.comete.swing.chart.dataview.option.color.error.choose"),
                    ColorTool.getColor(properties.getError().getColor()));
            if (c != null) {
                properties.getError().setColor(ColorTool.getCometeColor(c));
                errorColorView.setBackground(c);
                commit(ORIGIN_ERROR);
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // nothing to do
    }

    // Action listener
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == lineStyleCombo) {
            properties.getCurve().setLineStyle(lineStyleCombo.getSelectedIndex());
            commit(ORIGIN_CURVE);
        } else if (e.getSource() == fillStyleCombo) {
            properties.getBar().setFillStyle((Integer) fillStyleCombo.getSelectedItem());
            commit(ORIGIN_BAR);
        } else if (e.getSource() == fillMethodCombo) {
            properties.getBar().setFillingMethod(fillMethodCombo.getSelectedIndex());
            commit(ORIGIN_BAR);
        } else if (e.getSource() == viewTypeCombo) {
            properties.setViewType(viewTypeCombo.getSelectedIndex());
            commit(ORIGIN_GLOBAL_PLOT);
        } else if (e.getSource() == markerStyleCombo) {
            properties.getMarker().setStyle(markerStyleCombo.getSelectedIndex());
            commit(ORIGIN_MARKER);
        } else if (e.getSource() == legendVisibleCheck) {
            properties.getMarker().setLegendVisible(legendVisibleCheck.isSelected());
            commit(ORIGIN_MARKER);
        } else if (e.getSource() == samplingAllowedCheck) {
            properties.getMarker().setSamplingAllowed(samplingAllowedCheck.isSelected());
            commit(ORIGIN_MARKER);
        } else if (e.getSource() == errorVisibleCheck) {
            properties.getError().setVisible(errorVisibleCheck.isSelected());
            commit(ORIGIN_ERROR);
        } else if (e.getSource() == axisBox) {
            properties.setAxisChoice(axisBox.getSelectedIndex());
            commit(ORIGIN_GLOBAL_PLOT);
        } else if (e.getSource() == setAllButton) {
            CometeColor color = getPlotProperties().getCurve().getColor();
            Color awtColor = ColorTool.getColor(color);
            lineColorView.setBackground(awtColor);
            lineStyleCombo.setForeground(awtColor);
            markerColorView.setBackground(awtColor);
            markerStyleCombo.setForeground(awtColor);
            errorColorView.setBackground(awtColor);
            fillColorView.setBackground(awtColor);
            fillStyleCombo.setForeground(awtColor);
            properties.getCurve().setColor(color);
            properties.getBar().setFillColor(color);
            properties.getMarker().setColor(color);
            properties.getError().setColor(color);
            commit(ORIGIN_GLOBAL_PLOT);
        } else {
            super.actionPerformed(e);
        }
    }

    // Change listener
    @Override
    public void stateChanged(ChangeEvent e) {
        Integer v;
        Object src = e.getSource();
        if (src == lineWidthSpinner) {
            v = (Integer) lineWidthSpinner.getValue();
            properties.getCurve().setWidth(v.intValue());
            commit(ORIGIN_CURVE);
        } else if (src == barWidthSpinner) {
            v = (Integer) barWidthSpinner.getValue();
            properties.getBar().setWidth(v.intValue());
            commit(ORIGIN_BAR);
        } else if (src == markerSizeSpinner) {
            v = (Integer) markerSizeSpinner.getValue();
            properties.getMarker().setSize(v.intValue());
            commit(ORIGIN_MARKER);
        } else if (src == stepSpinner) {
            v = (Integer) stepSpinner.getValue();
            properties.getInterpolation().setInterpolationStep(v.intValue());
            commit(ORIGIN_INTERPOLATION);
        } else if (src == neighborSpinner) {
            v = (Integer) neighborSpinner.getValue();
            properties.getSmoothing().setNeighbors(v.intValue());
            commit(ORIGIN_SMOOTHING);
        } else if (src == noInterpBtn) {
            if (noInterpBtn.isSelected()) {
                properties.getInterpolation().setInterpolationMethod(IChartViewer.INTERPOLATE_NONE);
                commit(ORIGIN_INTERPOLATION);
            }
        } else if (src == linearBtn) {
            if (linearBtn.isSelected()) {
                properties.getInterpolation().setInterpolationMethod(IChartViewer.INTERPOLATE_LINEAR);
                commit(ORIGIN_INTERPOLATION);
            }
        } else if (src == cosineBtn) {
            if (cosineBtn.isSelected()) {
                properties.getInterpolation().setInterpolationMethod(IChartViewer.INTERPOLATE_COSINE);
                commit(ORIGIN_INTERPOLATION);
            }
        } else if (src == cubicBtn) {
            if (cubicBtn.isSelected()) {
                properties.getInterpolation().setInterpolationMethod(IChartViewer.INTERPOLATE_CUBIC);
                commit(ORIGIN_INTERPOLATION);
            }
        } else if (src == hermiteBtn) {
            if (hermiteBtn.isSelected()) {
                properties.getInterpolation().setInterpolationMethod(IChartViewer.INTERPOLATE_HERMITE);
                commit(ORIGIN_INTERPOLATION);
            }
        } else if (src == noSmoothBtn) {
            if (noSmoothBtn.isSelected()) {
                properties.getSmoothing().setMethod(IChartViewer.SMOOTH_NONE);
                commit(ORIGIN_SMOOTHING);
            }
        } else if (src == flatSmoothBtn) {
            if (flatSmoothBtn.isSelected()) {
                properties.getSmoothing().setMethod(IChartViewer.SMOOTH_FLAT);
                commit(ORIGIN_SMOOTHING);
            }
        } else if (src == triangularSmoothBtn) {
            if (triangularSmoothBtn.isSelected()) {
                properties.getSmoothing().setMethod(IChartViewer.SMOOTH_TRIANGULAR);
                commit(ORIGIN_SMOOTHING);
            }
        } else if (src == gaussianSmoothBtn) {
            if (gaussianSmoothBtn.isSelected()) {
                properties.getSmoothing().setMethod(IChartViewer.SMOOTH_GAUSSIAN);
                commit(ORIGIN_SMOOTHING);
            }
        } else if (src == noExtBtn) {
            if (noExtBtn.isSelected()) {
                properties.getSmoothing().setExtrapolation(IChartViewer.SMOOTH_EXT_NONE);
                commit(ORIGIN_SMOOTHING);
            }
        } else if (src == flatExtBtn) {
            if (flatExtBtn.isSelected()) {
                properties.getSmoothing().setExtrapolation(IChartViewer.SMOOTH_EXT_FLAT);
                commit(ORIGIN_SMOOTHING);
            }
        } else if (src == linearExtBtn) {
            if (linearExtBtn.isSelected()) {
                properties.getSmoothing().setExtrapolation(IChartViewer.SMOOTH_EXT_LINEAR);
                commit(ORIGIN_SMOOTHING);
            }
        } else if (src == noMathBtn) {
            if (noMathBtn.isSelected()) {
                properties.getMath().setFunction(IChartViewer.MATH_NONE);
                commit(ORIGIN_MATH);
            }
        } else if (src == derivativeBtn) {
            if (derivativeBtn.isSelected()) {
                properties.getMath().setFunction(IChartViewer.MATH_DERIVATIVE);
                commit(ORIGIN_MATH);
            }
        } else if (src == integralBtn) {
            if (integralBtn.isSelected()) {
                properties.getMath().setFunction(IChartViewer.MATH_INTEGRAL);
                commit(ORIGIN_MATH);
            }
        } else if (src == fftModBtn) {
            if (fftModBtn.isSelected()) {
                properties.getMath().setFunction(IChartViewer.MATH_FFT_MODULUS);
                commit(ORIGIN_MATH);
            }
        } else if (src == fftPhaseBtn) {
            if (fftPhaseBtn.isSelected()) {
                properties.getMath().setFunction(IChartViewer.MATH_FFT_PHASE);
                commit(ORIGIN_MATH);
            }
        }
        updateControls();
    }

    // Key listener
    @Override
    public void keyPressed(KeyEvent e) {
        // nothing to do
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // nothing to do
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == transformA0Text) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                String s = transformA0Text.getText();
                try {
                    double d = Double.parseDouble(s);
                    properties.getTransform().setA0(d);
                    commit(ORIGIN_TRANSFORMATION);
                } catch (NumberFormatException err) {
                    transformA0Text.setText(Double.toString(getPlotProperties().getTransform().getA0()));
                }
            }
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                transformA0Text.setText(Double.toString(getPlotProperties().getTransform().getA0()));
            }
        } else if (e.getSource() == transformA1Text) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                String s = transformA1Text.getText();
                try {
                    double d = Double.parseDouble(s);
                    properties.getTransform().setA1(d);
                    commit(ORIGIN_TRANSFORMATION);
                } catch (NumberFormatException err) {
                    transformA1Text.setText(Double.toString(getPlotProperties().getTransform().getA1()));
                }
            }
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                transformA1Text.setText(Double.toString(getPlotProperties().getTransform().getA1()));
            }
        } else if (e.getSource() == transformA2Text) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                String s = transformA2Text.getText();
                try {
                    double d = Double.parseDouble(s);
                    properties.getTransform().setA2(d);
                    commit(ORIGIN_TRANSFORMATION);
                } catch (NumberFormatException err) {
                    transformA2Text.setText(Double.toString(getPlotProperties().getTransform().getA2()));
                }
            }
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                transformA2Text.setText(Double.toString(getPlotProperties().getTransform().getA2()));
            }
        } else if (e.getSource() == xOffsetA0Text) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                String s = xOffsetA0Text.getText();
                try {
                    double d = Double.parseDouble(s);
                    properties.getXOffset().setA0(d);
                    commit(ORIGIN_X_OFFSET);
                } catch (NumberFormatException err) {
                    xOffsetA0Text.setText(Double.toString(getPlotProperties().getXOffset().getA0()));
                }
            }
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                xOffsetA0Text.setText(Double.toString(getPlotProperties().getTransform().getA1()));
            }

        } else if (e.getSource() == lineNameText) {
            String name = lineNameText.getText().trim();
            properties.getCurve().setName(name);
            if (isAlwaysCommit()) {
                nameLabel.setText(name);
            }
            commit(ORIGIN_CURVE);

        } else if (e.getSource() == tensionText) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                String s = tensionText.getText();
                try {
                    double d = Double.parseDouble(s);
                    properties.getInterpolation().setHermiteTension(d);
                    commit(ORIGIN_INTERPOLATION);
                } catch (NumberFormatException err) {
                    tensionText.setText(Double.toString(getPlotProperties().getInterpolation().getHermiteTension()));
                }
            }
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                tensionText.setText(Double.toString(getPlotProperties().getInterpolation().getHermiteTension()));
            }
        } else if (e.getSource() == biasText) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                String s = biasText.getText();
                try {
                    double d = Double.parseDouble(s);
                    properties.getInterpolation().setHermiteBias(d);
                    commit(ORIGIN_INTERPOLATION);
                } catch (NumberFormatException err) {
                    tensionText.setText(Double.toString(getPlotProperties().getInterpolation().getHermiteBias()));
                }
            }
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                biasText.setText(Double.toString(getPlotProperties().getInterpolation().getHermiteBias()));
            }
        } else if (e.getSource() == sigmaText) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                String s = sigmaText.getText();
                try {
                    double d = Double.parseDouble(s);
                    properties.getSmoothing().setGaussSigma(d);
                    commit(ORIGIN_SMOOTHING);
                } catch (NumberFormatException err) {
                    sigmaText.setText(Double.toString(getPlotProperties().getSmoothing().getGaussSigma()));
                }
            }
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                sigmaText.setText(Double.toString(getPlotProperties().getSmoothing().getGaussSigma()));
            }
        }
    }

    public boolean isAlwaysCommit() {
        return alwaysCommit;
    }

    public void setAlwaysCommit(boolean alwaysCommit) {
        this.alwaysCommit = alwaysCommit;
    }

    public int getSelectedAxis() {
        return axisBox.getSelectedIndex();
    }

}
