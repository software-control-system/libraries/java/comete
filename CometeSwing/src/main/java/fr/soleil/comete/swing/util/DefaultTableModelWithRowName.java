/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public abstract class DefaultTableModelWithRowName extends DefaultTableModel {

    private static final long serialVersionUID = 6287699265843232565L;

    public DefaultTableModelWithRowName() {
        super();
    }

    public DefaultTableModelWithRowName(int rowCount, int columnCount) {
        super(rowCount, columnCount);
    }

    public DefaultTableModelWithRowName(Vector<?> columnNames, int rowCount) {
        super(columnNames, rowCount);
    }

    public DefaultTableModelWithRowName(Object[] columnNames, int rowCount) {
        super(columnNames, rowCount);
    }

    public DefaultTableModelWithRowName(Vector<?> data, Vector<?> columnNames) {
        super(data, columnNames);
    }

    public DefaultTableModelWithRowName(Object[][] data, Object[] columnNames) {
        super(data, columnNames);
    }

    public abstract String getRowName(int row);

}
