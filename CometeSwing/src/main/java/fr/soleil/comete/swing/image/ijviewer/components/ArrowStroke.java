/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;

/**
 * A stroke that draws an arrow at the start, the end or both of a line.
 */
public class ArrowStroke implements Stroke {
    private static final int LENGTH = 20;

    private static final double ANGLE = Math.PI / 6;

    public static final int NONE = 0;

    public static final int BEGIN = 1;

    public static final int END = 2;

    public static final int BOTH = 3;

    private int fLength = LENGTH;

    private double fAngle = ANGLE;

    private int fType = END;

    private Stroke fStroke;

    public ArrowStroke(Stroke aStroke) {
        fStroke = aStroke;
    }

    public Stroke getStroke() {
        return fStroke;
    }

    public ArrowStroke(Stroke aStroke, int aType) {
        fStroke = aStroke;
        fType = aType;
    }

    public ArrowStroke(Stroke aStroke, int aType, int aLength, double aAngle) {
        fStroke = aStroke;
        fType = aType;
        fLength = aLength;
        fAngle = aAngle;
    }

    public void setLength(int aLength) {
        fLength = aLength;
    }

    public void setAngle(double aAngle) {
        fAngle = aAngle;
    }

    public void setStroke(Stroke aStroke) {
        fStroke = aStroke;
    }

    public void setType(int aType) {
        fType = aType;
    }

    public int getType() {
        return fType;
    }

    @Override
    public Shape createStrokedShape(Shape p) {
        if (fType == NONE) {
            return fStroke.createStrokedShape(p);
        }
        GeneralPath strokedShape = new GeneralPath();
        strokedShape.append(p, false);
        float seg[] = new float[6];
        // int segtype = 0;
        float X1 = 0, Y1 = 0, X2 = 0, Y2 = 0;
        PathIterator pi = p.getPathIterator(null);
        if (!pi.isDone()) {
            /* segtype = */pi.currentSegment(seg);
            X1 = seg[0];
            Y1 = seg[1];
            pi.next();
            if (!pi.isDone()) {
                /* segtype = */pi.currentSegment(seg);
                X2 = seg[0];
                Y2 = seg[1];
            }
        }
        pi = p.getPathIterator(null);
        float prevX = 0, prevY = 0, curX = 0, curY = 0;
        while (!pi.isDone()) {
            /* segtype = */pi.currentSegment(seg);
            prevX = curX;
            prevY = curY;
            curX = seg[0];
            curY = seg[1];
            pi.next();
        }
        if ((X1 != X2 || Y1 != Y2) && (X1 != curX || Y1 != curY) && (fType == BEGIN || fType == BOTH)) {
            double angle = Math.atan2(Y2 - Y1, X2 - X1);
            double ang1 = angle - fAngle;
            double ang2 = angle + fAngle;
            GeneralPath arrow_head = new GeneralPath();
            arrow_head.moveTo(X1, Y1);
            arrow_head.lineTo((float) (X1 + fLength * Math.cos(ang1)), (float) (Y1 + fLength * Math.sin(ang1)));
            arrow_head.moveTo(X1, Y1);
            arrow_head.lineTo((float) (X1 + fLength * Math.cos(ang2)), (float) (Y1 + fLength * Math.sin(ang2)));
            strokedShape.append(arrow_head, false);
        }
        if ((curX != prevX || curY != prevY) && (fType == END || fType == BOTH)) {
            double angle = Math.atan2(prevY - curY, prevX - curX);
            double ang1 = angle - fAngle;
            double ang2 = angle + fAngle;
            GeneralPath arrow_head = new GeneralPath();
            arrow_head.moveTo(curX, curY);
            arrow_head.lineTo((float) (curX + fLength * Math.cos(ang1)), (float) (curY + fLength * Math.sin(ang1)));
            arrow_head.moveTo(curX, curY);
            arrow_head.lineTo((float) (curX + fLength * Math.cos(ang2)), (float) (curY + fLength * Math.sin(ang2)));
            strokedShape.append(arrow_head, false);
        }

        return fStroke.createStrokedShape(strokedShape);
    }
}
