/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.awt.Rectangle;

import fr.soleil.comete.swing.chart.data.AbstractDataView;

public class LabelRect {
    private final Rectangle rect;
    private final AbstractDataView view;

    public LabelRect(int x, int y, int w, int h, AbstractDataView v) {
        rect = new Rectangle(x, y, w, h);
        view = v;
    }

    public Rectangle getRect() {
        return rect;
    }

    public AbstractDataView getView() {
        return view;
    }

}
