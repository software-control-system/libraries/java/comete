/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.util.EventObject;

/**
 * Event sent when when the user select a user action from the contextual menu
 */
public class JLChartActionEvent extends EventObject implements Cloneable {

    private static final long serialVersionUID = 9081804192623329839L;
    private final String actionName;
    private final boolean state;

    public JLChartActionEvent(Object source, String name) {
        this(source, name, false);
    }

    public JLChartActionEvent(Object source, String name, boolean s) {
        super(source);
        actionName = name;
        state = s;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public String getVersion() {
        return "$Id: JLChartActionEvent.java,v 1.2 2009/01/26 17:54:56 poncet Exp $";
    }

    /**
     * Gets the action name
     */
    public String getName() {
        return actionName;
    }

    /**
     * Gets the action state
     */
    public boolean getState() {
        return state;
    }

}
