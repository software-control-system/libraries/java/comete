/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.chart.view.graphics.MarkerGraphics;

public class MarkerStyleListCellRenderer extends ImageableTypeListCellRenderer {

    private static final long serialVersionUID = 913151841930429355L;

    protected static final String MARKER_NONE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.none");
    protected static final String MARKER_DOT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.dot");
    protected static final String MARKER_BOX = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.box");
    protected static final String MARKER_TRIANGLE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.triangle");
    protected static final String MARKER_DIAMOND = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.diamond");
    protected static final String MARKER_STAR = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.star");
    protected static final String MARKER_VERT_LINE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.line.vertical");
    protected static final String MARKER_HORIZ_LINE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.line.horizontal");
    protected static final String MARKER_CROSS = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.cross");
    protected static final String MARKER_CIRCLE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.circle");
    protected static final String MARKER_SQUARE = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.dataview.option.marker.style.square");

    public MarkerStyleListCellRenderer() {
        super();
    }

    @Override
    public String getText(int value) {
        String result;
        switch (value) {
            case IChartViewer.MARKER_NONE:
                result = MARKER_NONE;
                break;
            case IChartViewer.MARKER_DOT:
                result = MARKER_DOT;
                break;
            case IChartViewer.MARKER_BOX:
                result = MARKER_BOX;
                break;
            case IChartViewer.MARKER_TRIANGLE:
                result = MARKER_TRIANGLE;
                break;
            case IChartViewer.MARKER_DIAMOND:
                result = MARKER_DIAMOND;
                break;
            case IChartViewer.MARKER_STAR:
                result = MARKER_STAR;
                break;
            case IChartViewer.MARKER_VERT_LINE:
                result = MARKER_VERT_LINE;
                break;
            case IChartViewer.MARKER_HORIZ_LINE:
                result = MARKER_HORIZ_LINE;
                break;
            case IChartViewer.MARKER_CROSS:
                result = MARKER_CROSS;
                break;
            case IChartViewer.MARKER_CIRCLE:
                result = MARKER_CIRCLE;
                break;
            case IChartViewer.MARKER_SQUARE:
                result = MARKER_SQUARE;
                break;
            default:
                result = Integer.toString(value);
                break;
        }
        return result;
    }

    @Override
    protected BufferedImage getImage(int id, Color bg, Color fg) {
        int width = 16;
        int height = 16;
        int mSize = 8;

        BufferedImage bigImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) bigImage.getGraphics();
        g.setPaint(fg);
        MarkerGraphics.paintMarker(g, id, mSize, width / 2, height / 2);

        return bigImage;
    }

}
