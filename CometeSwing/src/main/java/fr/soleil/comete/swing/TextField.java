/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.listener.ITextFieldListener;
import fr.soleil.comete.definition.widget.ITextField;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.ICancelableTarget;
import fr.soleil.data.target.IErrorNotifiableTarget;
import fr.soleil.data.target.IUnitTarget;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;

public class TextField extends JTextField
        implements ITextField, IUnitTarget, ICancelableTarget, IErrorNotifiableTarget, ActionListener, KeyListener {

    private static final long serialVersionUID = -1088055833867936830L;

    protected boolean actionPerformedOnTextChanged;
    protected final List<ITextFieldListener> textFieldListeners;
    protected final TargetDelegate delegate;
    protected final ErrorNotificationDelegate errorNotificationDelegate;
    protected volatile boolean isEditingData;
    protected volatile String lastText;
    protected volatile String unit;

    public TextField() {
        super(4); // This way, we are sure there is a minimum/preferred size
        setFocusable(true);
        actionPerformedOnTextChanged = false;
        textFieldListeners = new ArrayList<>();
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        isEditingData = false;
        lastText = null;
        unit = null;
        addKeyListener(this);
        addActionListener(this);
        setCometeBackground(null);
    }

    @Override
    public String getUnit() {
        return unit;
    }

    @Override
    public void setUnit(String unit) {
        this.unit = unit;
        if ((!isEditable()) || (!isEnabled())) {
            updateUnit();
        }
    }

    @Override
    public void setEditable(boolean editable) {
        if (editable != isEditable()) {
            super.setEditable(editable);
            if (!editable) {
                isEditingData = false;
            }
            updateUnit();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (enabled != isEnabled()) {
            super.setEnabled(enabled);
            if (!enabled) {
                isEditingData = false;
            }
            updateUnit();
        }
    }

    /**
     * Updates the unit of this {@link TextField}'s {@link UnitDocument}
     */
    protected void updateUnit() {
        // Update text in EDT
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            if (isEditable() && isEnabled()) {
                doSetText(lastText);
            } else if ((unit == null) || unit.trim().isEmpty()) {
                doSetText(lastText);
            } else {
                doSetText(lastText + " " + unit);
            }
            revalidate();
            repaint();
        });
    }

    /**
     * Sets this {@link TextField}'s text if possible, or else stores the value to set it later
     * 
     * @param text The text to set
     */
    @Override
    public void setText(String text) {
        lastText = text;
        if (!isEditingData) {
            updateUnit();
        }
    }

    /**
     * Does set this {@link TextField}'s text
     * 
     * @param text The text to set
     */
    protected void doSetText(String text) {
        String textToApply = (text == null ? ObjectUtils.EMPTY_STRING : text);
        if (getDocument() != null) {
            super.setText(textToApply);
            setCaretPosition(0);
        }
    }

    @Override
    public void setPreferredSize(final int width, final int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void addTextFieldListener(final ITextFieldListener listener) {
        if (!textFieldListeners.contains(listener)) {
            textFieldListeners.add(listener);
        }
    }

    @Override
    public void removeTextFieldListener(final ITextFieldListener listener) {
        if (textFieldListeners.contains(listener)) {
            synchronized (textFieldListeners) {
                textFieldListeners.remove(listener);
            }
        }
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        send();
    }

    @Override
    public void fireActionPerformed(final EventObject event) {
        final ListIterator<ITextFieldListener> iterator = textFieldListeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().actionPerformed(event);
        }
    }

    @Override
    public void fireTextChanged(final EventObject event) {
        final ListIterator<ITextFieldListener> iterator = textFieldListeners.listIterator();
        while (iterator.hasNext()) {
            final ITextFieldListener listener = iterator.next();
            listener.textChanged(event);
            if (isActionPerformedOnTextChanged()) {
                listener.actionPerformed(event);
            }
        }
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(final CometeColor color) {
        super.setBackground(ColorTool.getColor(color == null ? CometeColor.WHITE : color));
    }

    @Override
    public void setCometeFont(final CometeFont font) {
        super.setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(final CometeColor color) {
        super.setForeground(ColorTool.getColor(color));
    }

    @Override
    public boolean isActionPerformedOnTextChanged() {
        return actionPerformedOnTextChanged;
    }

    @Override
    public void setActionPerformedOnTextChanged(final boolean actionPerformedOnTextChanged) {
        this.actionPerformedOnTextChanged = actionPerformedOnTextChanged;

    }

    @Override
    public void keyPressed(final KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            cancel();
        } else if (isEditable() && isEnabled()) {
            this.isEditingData = true;
        }
    }

    @Override
    public void keyReleased(final KeyEvent e) {
        fireTextChanged(new EventObject(this));
    }

    @Override
    public void keyTyped(final KeyEvent e) {
    }

    @Override
    public void addMediator(final Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(final Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    @Override
    public void cancel() {
        this.isEditingData = false;
        updateUnit();
    }

    @Override
    public void send() {
        this.isEditingData = false;
        String text = getText();
        lastText = text;
        warnMediators(new TextInformation(this, text));
        fireActionPerformed(new EventObject(this));
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to {@link Mediator}s
     */
    public <I, V extends TargetInformation<I>> void warnMediators(final V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        // return false to be sure to keep trace of desired text through "setText"
        return false;
    }

    @Override
    public void setTitledBorder(final String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(final IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(final IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
