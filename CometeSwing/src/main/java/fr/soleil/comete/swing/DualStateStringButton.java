/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.event.ActionEvent;
import java.util.EventObject;

import javax.swing.Icon;

/**
 * A {@link StringButton} that is able to manage 2 different states, and to switch automatically
 * from one to another on {@link #actionPerformed(EventObject)}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DualStateStringButton extends StringButton {

    private static final long serialVersionUID = -7980084244815399825L;

    private String firstText;
    private String secondText;

    private String firstTooltipText;
    private String secondTooltipText;

    private String firstConfirmationMessage;
    private String secondConfirmationMessage;
    private String confirmationMessage;

    private Icon firstIcon;
    private Icon secondIcon;

    private boolean firstState;

    /**
     * Default constructor
     */
    public DualStateStringButton() {
        super();
        firstText = null;
        secondText = null;
        firstTooltipText = null;
        secondTooltipText = null;
        firstConfirmationMessage = null;
        secondConfirmationMessage = null;
        firstIcon = null;
        secondIcon = null;
        firstState = true;
        setButtonLook(true);
    }

    /**
     * Returns the text displayed in button when the 1st state is used
     * 
     * @return A {@link String}
     */
    public String getFirstText() {
        return firstText;
    }

    /**
     * Sets the text displayed in button when the 1st state is used
     * 
     * @param firstText The text to set
     */
    public void setFirstText(String firstText) {
        this.firstText = firstText;
    }

    /**
     * Returns the text displayed in button when the 2nd state is used
     * 
     * @return A {@link String}
     */
    public String getSecondText() {
        return secondText;
    }

    /**
     * Sets the text displayed in button when the 2nd state is used
     * 
     * @param secondText The text to set
     */
    public void setSecondText(String secondText) {
        this.secondText = secondText;
    }

    /**
     * Returns the {@link Icon} displayed in button when the 1st state is used
     * 
     * @return An {@link Icon}
     */
    public Icon getFirstIcon() {
        return firstIcon;
    }

    /**
     * Sets the {@link Icon} displayed in button when the 1st state is used
     * 
     * @param firstIcon The {@link Icon} to set
     */
    public void setFirstIcon(Icon firstIcon) {
        this.firstIcon = firstIcon;
    }

    /**
     * Returns the {@link Icon} displayed in button when the 2nd state is used
     * 
     * @return An {@link Icon}
     */
    public Icon getSecondIcon() {
        return secondIcon;
    }

    /**
     * Sets the {@link Icon} displayed in button when the 2nd state is used
     * 
     * @param secondIcon The {@link Icon} to set
     */
    public void setSecondIcon(Icon secondIcon) {
        this.secondIcon = secondIcon;
    }

    /**
     * Returns the tooltip text displayed when the 1st state is used
     * 
     * @return a {@link String}
     */
    public String getFirstTooltipText() {
        return firstTooltipText;
    }

    /**
     * Sets the tooltip text displayed when the 1st state is used
     * 
     * @param firstTooltipText The tooltip text to set
     */
    public void setFirstTooltipText(String firstTooltipText) {
        this.firstTooltipText = firstTooltipText;
    }

    /**
     * Returns the tooltip text displayed when the 2nd state is used
     * 
     * @return a {@link String}
     */
    public String getSecondTooltipText() {
        return secondTooltipText;
    }

    /**
     * Sets the tooltip text displayed when the 2nd state is used
     * 
     * @param secondTooltipText The tooltip text to set
     */
    public void setSecondTooltipText(String secondTooltipText) {
        this.secondTooltipText = secondTooltipText;
    }

    /**
     * Returns the confirmation message used when the 1st state is used
     * 
     * @return a {@link String}
     */
    public String getFirstConfirmationMessage() {
        return firstConfirmationMessage;
    }

    /**
     * Sets the confirmation message to use when the 1st state is used
     * 
     * @param firstConfirmationMessage The confirmation message to use
     */
    public void setFirstConfirmationMessage(String firstConfirmationMessage) {
        this.firstConfirmationMessage = firstConfirmationMessage;
    }

    /**
     * Returns the confirmation message used when the 2nd state is used
     * 
     * @return a {@link String}
     */
    public String getSecondConfirmationMessage() {
        return secondConfirmationMessage;
    }

    /**
     * Sets the confirmation message to use when the 2nd state is used
     * 
     * @param secondConfirmationMessage The confirmation message to use
     */
    public void setSecondConfirmationMessage(String secondConfirmationMessage) {
        this.secondConfirmationMessage = secondConfirmationMessage;
    }

    /**
     * @param confirmationMessage the confirmationMessage to set
     */
    public void setConfirmationMessage(String confirmationMessage) {
        this.confirmationMessage = confirmationMessage;
    }

    /**
     * @return the confirmationMessage
     */
    public String getConfirmationMessage() {
        return confirmationMessage;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        super.actionPerformed(event);
        switchState();
    }

    public void updateDualStringButton() {
        if (firstState) {
            super.setText(firstText);
            setToolTipText(firstTooltipText);
            setConfirmationMessage(firstConfirmationMessage);
            setIcon(firstIcon);
        } else {
            super.setText(secondText);
            setToolTipText(secondTooltipText);
            setConfirmationMessage(secondConfirmationMessage);
            setIcon(secondIcon);
        }

    }

    /**
     * Fully cleans this {@link DualKeyAutoSwitchStringButton}, disconnecting it from all its data
     * sources, and removing all registered states and their associated elements
     */
    public void deepClean() {
        firstText = null;
        firstTooltipText = null;
        firstConfirmationMessage = null;
        firstIcon = null;
        secondText = null;
        secondTooltipText = null;
        secondConfirmationMessage = null;
        secondIcon = null;
        setToolTipText(null);
        setIcon(null);
    }

    /**
     * Put the button into his first state
     */
    public void setFirstState() {
        firstState = true;
        updateDualStringButton();
    }

    /**
     * Put the button into his second state
     */
    public void setSecondState() {
        firstState = false;
        updateDualStringButton();
    }

    @Override
    public void setText(String text) {
        // This kind of object mustn't have text
    }

    /**
     * Switch the current state into the other one
     */
    public void switchState() {
        firstState = !firstState;
        updateDualStringButton();
    }

    public boolean isFirstState() {
        return firstState;
    }
}
