package fr.soleil.comete.swing.border.util;

import fr.soleil.comete.swing.border.CometeTitledBorder;

/**
 * An {@link Enum} that tells how a {@link CometeTitledBorder} should manage received text and tooltip.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public enum TextTargetMode {
    /**
     * {@link CometeTitledBorder#setText(String)} and {@link CometeTitledBorder#setToolTipText(String)} will do
     * nothing.
     */
    NO_TEXT_TARGET,
    /**
     * {@link CometeTitledBorder#setText(String)} will change {@link CometeTitledBorder}'s label text, and
     * {@link CometeTitledBorder#setToolTipText(String)} will change {@link CometeTitledBorder}'s label tooltip.
     */
    TEXT_AS_LABEL_TEXT_AND_TOOLTIP_AS_LABEL_TOOLTIP,
    /**
     * {@link CometeTitledBorder#setText(String)} will change {@link CometeTitledBorder}'s label tooltip,
     * and {@link CometeTitledBorder#setToolTipText(String)} will change {@link CometeTitledBorder}'s label
     * text.
     */
    TEXT_AS_LABEL_TOOLTIP_AND_TOOLTIP_AS_LABEL_TEXT,
    /**
     * {@link CometeTitledBorder#setText(String)} will change {@link CometeTitledBorder}'s label text,
     * and {@link CometeTitledBorder#setToolTipText(String)} will do nothing.
     */
    TEXT_AS_LABEL_TEXT,
    /**
     * {@link CometeTitledBorder#setText(String)} will change {@link CometeTitledBorder}'s label tooltip,
     * and {@link CometeTitledBorder#setToolTipText(String)} will do nothing.
     */
    TEXT_AS_LABEL_TOOLTIP,
    /**
     * {@link CometeTitledBorder#setText(String)} will do nothing,
     * and {@link CometeTitledBorder#setToolTipText(String)} will change {@link CometeTitledBorder}'s label
     * text.
     */
    TOOLTIP_AS_LABEL_TEXT,
    /**
     * {@link CometeTitledBorder#setText(String)} will do nothing,
     * and {@link CometeTitledBorder#setToolTipText(String)} will change {@link CometeTitledBorder}'s label
     * tooltip.
     */
    TOOLTIP_AS_LABEL_TOOLTIP
}
