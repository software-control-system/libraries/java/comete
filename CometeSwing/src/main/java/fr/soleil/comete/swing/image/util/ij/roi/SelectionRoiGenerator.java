/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import ij.gui.Roi;

public class SelectionRoiGenerator extends AutoDeleteSimpleRoiGenerator {
    /**
     * if false, allow ROI selection with a single click
     */
    protected boolean doubleClickSelection;

    public SelectionRoiGenerator(String text, String description) {
        super(text, description, ImageViewer.SELECTION_MODE_ICON);
        doubleClickSelection = false;
    }

    @Override
    public boolean canHoverOtherRois() {
        return true;
    }

    @Override
    public int getRoiMode() {
        return IJRoiManager.SELECTION_ROI_MODE;
    }

    @Override
    protected BasicStroke generateRoiStroke() {
        return IJCanvas.DEFAULT_SELECTION_ROI_STROKE;
    }

    @Override
    protected Color generateRoiColor() {
        return IJCanvas.DEFAULT_SELECTION_ROI_COLOR;
    }

    @Override
    public int getRoiModeMenu() {
        return ImageViewer.TYPE_SELECTION_MENU;
    }

    @Override
    public String getActionCommand() {
        return ImageViewer.MODE_SELECTION_ACTION;
    }

    @Override
    public Cursor getCursor() {
        return Cursor.getDefaultCursor();
    }

    @Override
    public MouseEvent adaptMouseDraggedEvent(MouseEvent event) {
        MouseEvent newEvent = event;
        if (event != null) {
            // we disable the control mask on selection mode to only allow drawing rectangles from the top left corner
            // we also disable the shift mask to let the user draw a real rectangle, not a square
            int modifiers = event.getModifiers();
            modifiers &= ~InputEvent.SHIFT_MASK;
            modifiers &= ~InputEvent.CTRL_MASK;
            newEvent = new MouseEvent((Component) event.getSource(), event.getID(), event.getWhen(), modifiers,
                    event.getX(), event.getY(), event.getClickCount(), event.isPopupTrigger());
        }
        return newEvent;
    }

    @Override
    public MouseEvent adaptMouseClickedEvent(MouseEvent event) {
        if (event != null) {
            treatClick(event);
        }
        return event;
    }

    @Override
    protected void doExpectedWork(MouseEvent e, ImageViewer viewer, Roi selectionRoi, IJCanvas canvas) {
        if ((selectionRoi != null) && (canvas != null)) {
            // we can now delete the selection zoom ROI
            if (!selectionRoi.getBounds().isEmpty()) {
                // look for ROIs included in the rectangle, and compute the new selection
                canvas.selectRois(selectionRoi, canvas.isKeepSelection(e));
            }
        }
    }

    @Override
    public boolean keyPressed(KeyEvent e) {
        boolean treated = super.keyPressed(e);
        if (e != null) {
            ImageViewer viewer = recoverViewer(e.getComponent());
            switch (e.getKeyCode()) {
                case KeyEvent.VK_DELETE:
                case KeyEvent.VK_BACK_SPACE:
                    if (viewer != null) {
                        // we allow deleting only on Selection mode
                        viewer.getRoiManager().deleteSelectedRois();
                        treated = true;
                    }
                    break;
                case KeyEvent.VK_A:
                    if (isAltKeyCodes(viewer)) {
                        viewer.getRoiManager().selectAllRois();
                        treated = true;
                    }
                    break;
            }
        }
        return treated;
    }

    public void selectRoi(MouseEvent e, IJCanvas canvas) {
        if ((canvas != null) && SwingUtilities.isLeftMouseButton(e)) {
            boolean keepSelection = canvas.isKeepSelection(e);
            int sx = e.getX();
            int sy = e.getY();
            canvas.selectRoi(sx, sy, keepSelection);
        }
    }

    protected void treatClick(MouseEvent e) {
        if (e != null) {
            ImageViewer viewer = recoverViewer(e.getComponent());
            if (viewer != null) {
                IJCanvas canvas = viewer.getImageCanvas();
                if ((!doubleClickSelection) || (e.getClickCount() == 2)) {
                    selectRoi(e, canvas);
                }
            }
        }
    }

    public boolean isDoubleClickSelection() {
        return doubleClickSelection;
    }

    public void setDoubleClickSelection(boolean doubleClick) {
        doubleClickSelection = doubleClick;
    }

}
