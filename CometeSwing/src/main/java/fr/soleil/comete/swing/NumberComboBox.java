/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.data.information.NumberInformation;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.array.INumberArrayTarget;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;

public class NumberComboBox extends ComboBox implements INumberTarget, INumberArrayTarget {

    private static final long serialVersionUID = 321569810406778155L;

    private Object valueList = new Number[0];

    public NumberComboBox() {
        super();
    }

    @Override
    public Number getNumberValue() {
        Number result = null;
        Object value = getSelectedValue();
        if (value != null) {
            try {
                result = recoverNumberValue(value);
            } catch (NumberFormatException e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to get number value", e);
                result = null;
            }
        }
        return result;
    }

    @Override
    public void setNumberValue(Number value) {
        setSelectedValue(value);
    }

    @Override
    public Object getNumberArray() {
        return valueList;
    }

    @Override
    public void setNumberArray(Object value) {
        if ((value == null)
                || (value.getClass().isArray() && ObjectUtils.isNumberClass(value.getClass().getComponentType()))) {
            Object[] effectiveValueList;
            if (value == null) {
                effectiveValueList = null;
            } else {
                effectiveValueList = NumberArrayUtils.extractNumberArray(value);
            }
            setValueList(effectiveValueList);
        }
    }

    @Override
    protected void warnMediators() {
        warnMediators(new NumberInformation(this, getNumberValue()));
    }

    /**
     * Main class, so you can have an example. You can monitor your own attribute by giving its full
     * path name in argument
     */
    public static void main(String[] args) {
        IPanel panel = new Panel();
        NumberComboBox widget = new NumberComboBox();
        widget.setNumberArray(new Number[] { 1, 2, 3 });
        widget.setDisplayedList(new String[] { "option 1", "option 2", "option 3" });

        panel.add(widget);

        IFrame frame = new Frame();
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setTitle("NumberComboBox test");
    }

}
