/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;
import java.util.WeakHashMap;

import javax.swing.ButtonModel;
import javax.swing.JToggleButton.ToggleButtonModel;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.data.information.BooleanInformation;
import fr.soleil.comete.definition.listener.ICheckBoxListener;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.ICheckBox;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;

public class CheckBox extends ConstrainedCheckBox implements ICheckBox {

    private static final long serialVersionUID = -1159936310755158575L;

    protected Collection<ICheckBoxListener> checkBoxListeners;
    private String falseLabel;
    private String trueLabel;
    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;
    private volatile boolean editable;

    public CheckBox() {
        super();
        checkBoxListeners = Collections.newSetFromMap(new WeakHashMap<ICheckBoxListener, Boolean>());
        falseLabel = ObjectUtils.EMPTY_STRING;
        trueLabel = ObjectUtils.EMPTY_STRING;
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        editable = true;
        super.setModel(new CheckBoxModel());
    }

    @Override
    public void setModel(ButtonModel newModel) {
        if (newModel instanceof CheckBoxModel) {
            super.setModel(newModel);
        } else {
            super.setModel(new CheckBoxModel());
        }
    }

    @Override
    protected void transmitItemEvent(ItemEvent evt) {
        if (isSelected()) {
            if (trueLabel != null) {
                setText(trueLabel);
            }
        } else if (falseLabel != null) {
            setText(falseLabel);
        }
        super.transmitItemEvent(evt);
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void addCheckBoxListener(final ICheckBoxListener listener) {
        if (listener != null) {
            synchronized (checkBoxListeners) {
                checkBoxListeners.add(listener);
            }
        }
    }

    @Override
    public void removeCheckBoxListener(final ICheckBoxListener listener) {
        if (listener != null) {
            synchronized (checkBoxListeners) {
                checkBoxListeners.remove(listener);
            }
        }
    }

    @Override
    public void fireSelectedChanged(EventObject event) {
        warnMediators(new BooleanInformation(this, isSelected()));
        List<ICheckBoxListener> listeners = new ArrayList<>();
        synchronized (checkBoxListeners) {
            listeners.addAll(checkBoxListeners);
        }
        for (ICheckBoxListener listener : listeners) {
            listener.selectedChanged(event);
        }
        listeners.clear();
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(CometeFont font) {
        super.setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        super.setForeground(ColorTool.getColor(color));
    }

    @Override
    protected void transmitActionEvent(ActionEvent evt) {
        if (editable) {
            fireSelectedChanged(new EventObject(this));
        } else {
            setSelected(!isSelected());
        }
        super.transmitActionEvent(evt);
    }

    @Override
    public String getFalseLabel() {
        return falseLabel;
    }

    @Override
    public String getTrueLabel() {
        return trueLabel;
    }

    @Override
    public void setFalseLabel(String falseLabel) {
        this.falseLabel = falseLabel;
        if (falseLabel != null && !isSelected()) {
            setText(falseLabel);
        }
    }

    @Override
    public void setTrueLabel(String trueLabel) {
        this.trueLabel = trueLabel;
        if (trueLabel != null && isSelected()) {
            setText(trueLabel);
        }
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void setEditable(boolean editable) {
        this.editable = editable;
        setFocusable(editable);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link ToggleButtonModel} that allows changes only when editable
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class CheckBoxModel extends ToggleButtonModel {

        private static final long serialVersionUID = -4745593877458264301L;

        @Override
        public void setPressed(boolean b) {
            if (editable) {
                super.setPressed(b);
            }
        }

        @Override
        public void setRollover(boolean b) {
            if (editable) {
                super.setRollover(b);
            }
        }
    }

}
