/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Position;

import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.awt.FontUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.text.JSmoothLabel;

/**
 * A font chooser dialog box.
 * <p>
 * 
 * <pre>
 *  Font defFont = new Font("Dialog", Font.BOLD, 12);
 *  Font newFont = ATKFontChooser.getNewFont(this,"Choose font",defFont);
 *  if (newFont != null) {
 *     ....
 *  }
 * </pre>
 */
public class FontChooser extends JDialog implements ActionListener, ListSelectionListener, ChangeListener {

    private static final long serialVersionUID = 6717935753400020702L;

    private static final Font LABEL_FONT = new Font(Font.DIALOG, Font.PLAIN, 12);
    private static final Font LABEL_FONT_BOLD = LABEL_FONT.deriveFont(Font.BOLD);
    private static final Font LABEL_FONT_ITALIC = LABEL_FONT.deriveFont(Font.ITALIC);
    private static final Font LABEL_FONT_BOLD_ITALIC = LABEL_FONT.deriveFont(Font.BOLD | Font.ITALIC);
    private static final Color LABEL_COLOR = new Color(85, 87, 140);
    // Get all available font families
    private static final Map<String, Font> FONT_MAP = new LinkedHashMap<>();
    static {
        String[] families = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        FontRenderContext frc = FontUtils.getDefaultRenderContext();
        // register available fonts
        int size = 14;
        for (String family : families) {
            Font font = new Font(family, LABEL_FONT.getStyle(), size);
            // only keep fonts that mean something on screen
            boolean canDisplay = true;
            for (int i = 0; i < family.length() && canDisplay; i++) {
                canDisplay = font.canDisplay(family.charAt(i));
            }
            if (canDisplay) {
                // adapt font size so that user may read font name on screen
                float ascent;
                LineMetrics lm = null;
                while (true) {
                    lm = font.getLineMetrics(family, frc);
                    ascent = lm.getAscent();
                    // Stupid ascent case that results in application freeze (CONTROLGUI-359)
                    // That case was detected with "Hershey" font on CentOS 6 64bits with openjdk 8.
                    if (ascent == 0) {
                        ascent = lm.getDescent();
                        if (ascent == 0) {
                            ascent = lm.getHeight();
                            if (ascent == 0) {
                                // stupid case: nothing can be done
                                break;
                            }
                        }
                    }
                    if (ascent < size) {
                        font = font.deriveFont((float) font.getSize() + 1);
                    } else {
                        break;
                    }
                }
                // register font
                FONT_MAP.put(family, font);
            }
        }
    }

    private JPanel innerPanel;
    private Font currentFont;
    private int fontSize;
    private String fontFamily;
    private Font result;

    private DefaultListModel<String> listModel;
    private JScrollPane familyView;
    private JList<String> familyList;

    private JPanel infoPanel;

    private JCheckBox plainCheck;
    private JCheckBox boldCheck;
    private JCheckBox italicCheck;
    private JCheckBox italicboldCheck;

    private JTextField sizeText;
    private JLabel sizeLabel;
    private JSlider sizeSlider;

    private JSmoothLabel sampleLabel;

    private JButton okBtn;
    private JButton cancelBtn;

    // -----------------------------------------------
    // Construction
    // -----------------------------------------------

    private FontChooser(Window parent, String title, Font initialFont) {
        super(parent);
        setModal(true);
        setFont(LABEL_FONT);// need to set a default font to avoid a null pointer when we use getFont()
        initComponents(title, initialFont);
    }

    private void initComponents(String title, Font initialFont) {

        innerPanel = new JPanel();
        innerPanel.setLayout(null);

        if (initialFont == null) {
            currentFont = getFont();
        } else {
            currentFont = initialFont;
        }

        fontFamily = currentFont.getFamily();
        fontSize = currentFont.getSize();
        result = currentFont;

        // Create Gui

        listModel = new DefaultListModel<>();
        for (String family : FONT_MAP.keySet()) {
            listModel.addElement(family);
        }
        familyList = new JList<>(listModel);
        familyList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        familyList.setFont(LABEL_FONT);
        familyList.setCellRenderer(new FontFamilyRenderer());

        familyView = new JScrollPane(familyList);
        familyView.setBorder(BorderFactory.createLoweredBevelBorder());

        infoPanel = new JPanel();
        infoPanel.setLayout(null);
        infoPanel.setBorder(createTitleBorder("Size and Style"));

        plainCheck = new JCheckBox("Plain");
        plainCheck.setForeground(LABEL_COLOR);
        plainCheck.setFont(LABEL_FONT);
        plainCheck.addActionListener(this);

        boldCheck = new JCheckBox("Bold");
        boldCheck.setForeground(LABEL_COLOR);
        boldCheck.setFont(LABEL_FONT_BOLD);
        boldCheck.addActionListener(this);

        italicCheck = new JCheckBox("Italic");
        italicCheck.setForeground(LABEL_COLOR);
        italicCheck.setFont(LABEL_FONT_ITALIC);
        italicCheck.addActionListener(this);

        italicboldCheck = new JCheckBox("Bold italic");
        italicboldCheck.setForeground(LABEL_COLOR);
        italicboldCheck.setFont(LABEL_FONT_BOLD_ITALIC);
        italicboldCheck.addActionListener(this);

        sizeText = new JTextField();
        sizeText.addActionListener(this);
        sizeText.setEditable(true);

        sizeLabel = new JLabel("Size");
        sizeLabel.setFont(LABEL_FONT);
        sizeLabel.setForeground(LABEL_COLOR);

        sizeSlider = new JSlider(5, 72, fontSize);
        sizeSlider.setMinorTickSpacing(1);
        sizeSlider.setMajorTickSpacing(5);
        sizeSlider.setPaintTicks(true);
        sizeSlider.setPaintLabels(true);

        sizeSlider.addChangeListener(this);

        infoPanel.add(plainCheck);
        infoPanel.add(italicCheck);
        infoPanel.add(italicboldCheck);
        infoPanel.add(boldCheck);
        infoPanel.add(sizeLabel);
        infoPanel.add(sizeText);
        infoPanel.add(sizeSlider);

        plainCheck.setBounds(5, 20, 100, 25);
        italicCheck.setBounds(5, 45, 100, 25);
        boldCheck.setBounds(5, 70, 100, 25);
        italicboldCheck.setBounds(5, 95, 100, 25);
        sizeLabel.setBounds(130, 35, 80, 25);
        sizeText.setBounds(130, 60, 80, 25);
        sizeSlider.setBounds(5, 125, 240, 45);

        okBtn = new JButton("Apply");
        okBtn.setFont(LABEL_FONT);
        okBtn.addActionListener(this);

        cancelBtn = new JButton("Cancel");
        cancelBtn.setFont(LABEL_FONT);
        cancelBtn.addActionListener(this);

        sampleLabel = new JSmoothLabel();
        sampleLabel.setText("Sample 12.34");
        sampleLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        sampleLabel.setBackground(new Color(220, 220, 220));

        // Add and size
        innerPanel.add(familyView);
        innerPanel.add(infoPanel);
        innerPanel.add(okBtn);
        innerPanel.add(cancelBtn);
        innerPanel.add(sampleLabel);

        infoPanel.setBounds(178, 5, 254, 175);
        familyView.setBounds(5, 5, 170, 300);
        okBtn.setBounds(180, 280, 80, 25);
        cancelBtn.setBounds(350, 280, 80, 25);
        sampleLabel.setBounds(180, 185, 250, 90);

        setTitle(title);
        updateControl();

        // Select the currentFont
        int selid = familyList.getNextMatch(currentFont.getFamily(), 0, Position.Bias.Forward);

        if (selid != -1) {
            familyList.setSelectedIndex(selid);
            familyList.ensureIndexIsVisible(selid);
        }

        familyList.addListSelectionListener(this);

        innerPanel.setPreferredSize(new Dimension(435, 312));
        setContentPane(innerPanel);
        setResizable(false);

    }

    // Update control according to the current font.
    private void updateControl() {
        plainCheck.setSelected(currentFont.isPlain());
        boldCheck.setSelected(currentFont.isBold() && !currentFont.isItalic());
        italicCheck.setSelected(currentFont.isItalic() && !currentFont.isBold());
        italicboldCheck.setSelected(currentFont.isBold() && currentFont.isItalic());

        sampleLabel.setFont(currentFont);
        sizeText.setText(Integer.toString(fontSize));
    }

    // -----------------------------------------------
    // ActionListener
    // -----------------------------------------------
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cancelBtn) {
            result = null;
            setVisible(false);
            dispose();
        } else if (e.getSource() == okBtn) {
            result = currentFont;
            setVisible(false);
            dispose();
        } else if (e.getSource() == plainCheck) {
            Font newFont = new Font(fontFamily, Font.PLAIN, fontSize);
            if (newFont != null) {
                currentFont = newFont;
                updateControl();
            }
        } else if (e.getSource() == boldCheck) {
            Font newFont = new Font(fontFamily, Font.BOLD, fontSize);
            if (newFont != null) {
                currentFont = newFont;
                updateControl();
            }

        } else if (e.getSource() == italicCheck) {
            Font newFont = new Font(fontFamily, Font.ITALIC, fontSize);
            if (newFont != null) {
                currentFont = newFont;
                updateControl();
            }
        } else if (e.getSource() == italicboldCheck) {
            Font newFont = new Font(fontFamily, Font.ITALIC + Font.BOLD, fontSize);
            if (newFont != null) {
                currentFont = newFont;
                updateControl();
            }
        } else if (e.getSource() == sizeText) {
            try {
                int nSize = Integer.parseInt(sizeText.getText());
                sizeSlider.setValue(nSize);
            } catch (NumberFormatException ex) {
                JOptionPane.showConfirmDialog(this, "Wrong integer format\n" + ex.getMessage());
            }
        }

    }

    // -----------------------------------------------
    // SelectionListListener
    // -----------------------------------------------
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == familyList) {
            String fName = listModel.get(familyList.getSelectedIndex());
            Font newFont = new Font(fName, Font.PLAIN, fontSize);
            fontFamily = fName;
            currentFont = newFont;
            updateControl();
        }
    }

    // -----------------------------------------------
    // Change listener
    // -----------------------------------------------
    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() == sizeSlider) {
            fontSize = sizeSlider.getValue();
            Font newFont = currentFont.deriveFont((float) fontSize);
            if (newFont != null) {
                currentFont = newFont;
                updateControl();
            }
        }
    }

    private Border createTitleBorder(String name) {
        return BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), name, TitledBorder.LEFT,
                TitledBorder.DEFAULT_POSITION, LABEL_FONT_BOLD, LABEL_COLOR);
    }

    /**
     * Display the Font chooser dialog.
     * 
     * @param parent Parent component
     * @param dlgTitle Dialog title
     * @param defaultFont Default font (can be null)
     * @return A handle to a new Font, null when canceled.
     */
    public static Font getNewFont(Component parent, String dlgTitle, Font defaultFont) {
        Window pWindow = WindowSwingUtils.getWindowForComponent(parent);
        FontChooser dlg;
        if (pWindow instanceof Dialog) {
            dlg = new FontChooser(pWindow, dlgTitle, defaultFont);
        } else if (pWindow instanceof Frame) {
            dlg = new FontChooser(pWindow, dlgTitle, defaultFont);
        } else {
            dlg = new FontChooser((Frame) null, dlgTitle, defaultFont);
        }

        CometeUtils.centerDialog(dlg);
        dlg.setVisible(true);
        return dlg.result;

    }

    public static void main(String[] args) {
        getNewFont(null, "Choose font", null);
        System.exit(0);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class FontFamilyRenderer extends DefaultListCellRenderer {

        private static final long serialVersionUID = 4784575529462478023L;

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
                boolean cellHasFocus) {
            Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof String) {
                Font font = FONT_MAP.get(value);
                if (font != null) {
                    comp.setFont(font);
                }
            }
            return comp;
        }
    }

}
