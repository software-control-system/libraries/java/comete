/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.util;

import javax.swing.ImageIcon;

public interface IconConstants {

    public static final ImageIcon ICON_BULB_OFF = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/bulbDisabled.gif"));
    public static final ImageIcon ICON_BULB_ON = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/bulbEnabled.gif"));
    public static final ImageIcon ICON_BULB_KO = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/bulbKO.gif"));
    public static final ImageIcon ICON_LED_BLUE = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledBlue.gif"));
    public static final ImageIcon ICON_LED_BROWNGRAY = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledBrownGray.gif"));
    public static final ImageIcon ICON_LED_DARKGRAY = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledDarkGray.gif"));
    public static final ImageIcon ICON_LED_ORANGE = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledDarkOrange.gif"));
    public static final ImageIcon ICON_LED_GRAY = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledGray.gif"));
    public static final ImageIcon ICON_LED_GREEN = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledGreen.gif"));
    public static final ImageIcon ICON_LED_DARKGREEN = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledDarkGreen.gif"));
    public static final ImageIcon ICON_LED_LIGHTORANGE = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledLightOrange.gif"));
    public static final ImageIcon ICON_LED_PINK = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledPink.gif"));
    public static final ImageIcon ICON_LED_RED = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledRed.gif"));
    public static final ImageIcon ICON_LED_WHITE = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledWhite.gif"));
    public static final ImageIcon ICON_LED_YELLOW = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledYellow.gif"));
    public static final ImageIcon ICON_LED_KO = new ImageIcon(
            IconConstants.class.getResource("/fr/soleil/comete/icons/ledKO.gif"));

}
