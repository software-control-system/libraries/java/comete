/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.util.ij.roi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.SwingConstants;

import ij.ImagePlus;
import ij.gui.Line;
import ij.gui.Roi;
import ij.process.ImageProcessor;

/**
 * A {@link Roi} used to draw a cross
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class CrossRoi extends Roi implements SwingConstants {

    private static final long serialVersionUID = 9095560054199860958L;

    public static final int TYPE = 12;

    protected int horizontalBarAlignment, verticalBarAlignment;
    protected volatile int leftX, rightX, topY, bottomY, width, height, centerX, centerY;
    protected volatile boolean canDraw;

    /**
     * Creates a new {@link CrossRoi} at given position on screen
     * 
     * @param sx x position on screen
     * @param sy y position on screen
     * @param imp {@link ImagePlus} that will manage this {@link CrossRoi}
     */
    public CrossRoi(int sx, int sy, ImagePlus imp) {
        this(sx, sy, CENTER, CENTER, imp);
    }

    /**
     * Creates a new {@link CrossRoi} at given position on screen, defining where intersection should happen
     * 
     * @param sx x position on screen
     * @param sy y position on screen
     * @param horizontalBarAlignment The vertical alignment of the horizontal bar.
     *            Can be any of these:
     *            <ul>
     *            <li>{@link SwingConstants#TOP}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#BOTTOM}</li>
     *            </ul>
     * @param verticalBarAlignment The horizontal alignment of the vertical bar.
     *            Can be any of these:
     *            <ul>
     *            <li>{@link SwingConstants#LEFT}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#RIGHT}</li>
     *            </ul>
     * @param imp {@link ImagePlus} that will manage this {@link CrossRoi}
     */
    public CrossRoi(int sx, int sy, int horizontalBarAlignment, int verticalBarAlignment, ImagePlus imp) {
        super(sx, sy, imp);
        type = TYPE;
        this.horizontalBarAlignment = horizontalBarAlignment;
        this.verticalBarAlignment = verticalBarAlignment;
        computePositions();
    }

    /**
     * Creates a new {@link CrossRoi} with given bounds, defining where intersection should happen
     * 
     * @param x x position in image (i.e. off-screen)
     * @param y y position in image (i.e. off-screen)
     * @param width expected width
     * @param height expected height
     * @param horizontalBarAlignment The vertical alignment of the horizontal bar.
     *            Can be any of these:
     *            <ul>
     *            <li>{@link SwingConstants#TOP}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#BOTTOM}</li>
     *            </ul>
     * @param verticalBarAlignment The horizontal alignment of the vertical bar.
     *            Can be any of these:
     *            <ul>
     *            <li>{@link SwingConstants#LEFT}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#RIGHT}</li>
     *            </ul>
     * @param imp {@link ImagePlus} that will manage this {@link CrossRoi}
     */
    public CrossRoi(int x, int y, int width, int height, int horizontalBarAlignment, int verticalBarAlignment,
            ImagePlus imp) {
        super(x, y, width, height);
        type = TYPE;
        setImage(imp);
        this.horizontalBarAlignment = horizontalBarAlignment;
        this.verticalBarAlignment = verticalBarAlignment;
        computePositions();
    }

    /**
     * Returns the x (left) position in image of the vertical bar.
     * 
     * @return An <code>int</code>.
     */
    public int getVerticalBarX() {
        int x;
        switch (verticalBarAlignment) {
            case RIGHT:
                x = rightX;
                break;
            case CENTER:
                x = centerX;
                break;
            default:
                x = leftX;
                break;
        }
        return x;
    }

    /**
     * Returns the y (top) position in image of the horizontal bar.
     * 
     * @return An <code>int</code>.
     */
    public int getHorizontalBarY() {
        int y;
        switch (horizontalBarAlignment) {
            case BOTTOM:
                y = bottomY;
                break;
            case CENTER:
                y = centerY;
                break;
            default:
                y = topY;
                break;
        }
        return y;
    }

    /**
     * Returns the vertical alignment of the horizontal bar.
     * 
     * @return An <code>int</code>
     * @see #setHorizontalBarAlignment(int)
     */
    public int getHorizontalBarAlignment() {
        return horizontalBarAlignment;
    }

    /**
     * Sets the vertical alignment of the horizontal bar.
     * 
     * @param horizontalBarAlignment The vertical alignment of the horizontal bar.
     *            Can be any of these:
     *            <ul>
     *            <li>{@link SwingConstants#TOP}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#BOTTOM}</li>
     *            </ul>
     */
    public void setHorizontalBarAlignment(int horizontalBarAlignment) {
        int align;
        switch (horizontalBarAlignment) {
            case TOP:
            case CENTER:
            case BOTTOM:
                align = horizontalBarAlignment;
                break;
            default:
                align = TOP;
                break;
        }
        if (this.horizontalBarAlignment != align) {
            this.horizontalBarAlignment = align;
            computePositions();
            imp.draw(clipX, clipY, clipWidth, clipHeight);
        }
    }

    /**
     * Returns the horizontal alignment of the vertical bar.
     * 
     * @return An <code>int</code>
     * @see #setVerticalBarAlignment(int)
     */
    public int getVerticalBarAlignment() {
        return verticalBarAlignment;
    }

    /**
     * Sets the horizontal alignment of the vertical bar.
     * 
     * @param verticalBarAlignment The horizontal alignment of the vertical bar.
     *            Can be any of these:
     *            <ul>
     *            <li>{@link SwingConstants#LEFT}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#RIGHT}</li>
     *            </ul>
     */
    public void setVerticalBarAlignment(int verticalBarAlignment) {
        int align;
        switch (verticalBarAlignment) {
            case LEFT:
            case CENTER:
            case RIGHT:
                align = verticalBarAlignment;
                break;
            default:
                align = LEFT;
                break;
        }
        if (this.verticalBarAlignment != align) {
            this.verticalBarAlignment = align;
            computePositions();
            imp.draw(clipX, clipY, clipWidth, clipHeight);
        }
    }

    /**
     * Returns the bounds of the horizontal bar.
     * 
     * @return A {@link Rectangle}
     */
    public Rectangle getHorizontalBarBounds() {
        Rectangle bounds = getBounds();
        bounds.height = 1;
        bounds.y = getHorizontalBarY();
        return bounds;
    }

    /**
     * Returns the bounds of the vertical bar.
     * 
     * @return A {@link Rectangle}
     */
    public Rectangle getVerticalBarBounds() {
        Rectangle bounds = getBounds();
        bounds.width = 1;
        bounds.x = getVerticalBarX();
        return bounds;
    }

    @Override
    protected void handleMouseDrag(int sx, int sy, int flags) {
        canDraw = false;
        super.handleMouseDrag(sx, sy, flags);
        computePositions();
        imp.draw(clipX, clipY, clipWidth, clipHeight);
    }

    /**
     * Computes all the positions this {@link CircleRoi} needs to work with.
     */
    protected void computePositions() {
        Rectangle bounds = getBounds();
        leftX = bounds.x;
        width = bounds.width;
        if (bounds.width == 0) {
            rightX = leftX;
        } else {
            rightX = leftX + bounds.width - 1;
        }
        topY = bounds.y;
        height = bounds.height;
        if (bounds.height == 0) {
            bottomY = topY;
        } else {
            bottomY = topY + bounds.height - 1;
        }
        centerX = bounds.x + bounds.width / 2;
        centerY = bounds.y + bounds.height / 2;
        canDraw = true;
    }

    @Override
    public void nudge(int key) {
        canDraw = false;
        super.nudge(key);
        computePositions();
        imp.draw(clipX, clipY, clipWidth, clipHeight);
    }

    @Override
    public void nudgeCorner(int key) {
        canDraw = false;
        super.nudgeCorner(key);
        computePositions();
        imp.draw(clipX, clipY, clipWidth, clipHeight);
    }

    @Override
    public void drawPixels(ImageProcessor ip) {
        if (canDraw) {
            int saveWidth = ip.getLineWidth();
            if (getStrokeWidth() > 1f) {
                ip.setLineWidth(Math.round(getStrokeWidth()));
            }
            int x = getVerticalBarX(), y = getHorizontalBarY();
            ip.moveTo(x, 0);
            ip.lineTo(x, bottomY);
            ip.moveTo(0, y);
            ip.lineTo(rightX, y);
            ip.setLineWidth(saveWidth);
            if (Line.getWidth() > 1 || getStrokeWidth() > 1) {
                updateFullWindow = true;
            }
        }
    }

    @Override
    public void draw(Graphics g) {
        if (canDraw) {
            Color color = strokeColor != null ? strokeColor : ROIColor;
            if (fillColor != null) {
                color = fillColor;
            }
            g.setColor(color);
            mag = getMagnification();

            int x = getVerticalBarX(), y = getHorizontalBarY();
            int slx = screenXD(leftX), srx = screenXD(rightX), sx = screenXD(x), cx = slx + (srx - slx) / 2;
            int sty = screenYD(topY), sby = screenYD(bottomY), sy = screenYD(y), cy = sty + (sby - sty) / 2;

            Graphics2D g2d = (Graphics2D) g;
            if (stroke != null) {
                g2d.setStroke(getScaledStroke());
            }

            g2d.drawLine(sx, sty, sx, sby); // vertical bar
            g2d.drawLine(slx, sy, srx, sy); // horizontal bar

            drawPoint(g2d, slx, sty); // top left point
            drawPoint(g2d, srx, sty); // top right point
            drawPoint(g2d, slx, sby); // bottom left point
            drawPoint(g2d, srx, sby); // bottom right point
            drawPoint(g2d, cx, sty); // top center point
            drawPoint(g2d, cx, sby); // bottom center point
            drawPoint(g2d, slx, cy); // center left point
            drawPoint(g2d, srx, cy); // center right point

            if (updateFullWindow) {
                updateFullWindow = false;
                imp.draw();
            }
        }
    }

    /**
     * Draws a little square point at given position.
     * 
     * @param g The {@link Graphics} in which to draw the point.
     * @param x x position.
     * @param y y position.
     */
    protected void drawPoint(Graphics g, int x, int y) {
        g.setColor(Color.BLACK);
        g.fillRect(x - 2, y - 2, 5, 5);
        g.setColor(handleColor);
        g.fillRect(x - 1, y - 1, 3, 3);
    }

}
