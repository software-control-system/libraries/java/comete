/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.chart;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.util.MeanTypeListCellRenderer;
import fr.soleil.comete.swing.util.PeakTypeListCellRenderer;

public class SamplingPanel extends JPanel implements ActionListener {

    private static final long serialVersionUID = -5253817239096620990L;

    private static final String ENABLE_TXT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.enabled.text");
    private static final String ENABLE_TOOLTIP = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.enabled.tooltip");
    private static final String MEAN_TYPE_TXT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.mean.text");
    private static final String PEAK_TYPE_TXT = ChartUtils.MESSAGES
            .getString("fr.soleil.comete.swing.chart.sampling.type.peak.text");

    private static final Integer[] MEAN_TYPE = { IChartViewer.GLOBAL_MEAN, IChartViewer.LOCAL_MEAN };
    private static final Integer[] PEAK_TYPE = { IChartViewer.PEAK_TYPE_GREATEST, IChartViewer.PEAK_TYPE_MIN,
            IChartViewer.PEAK_TYPE_MAX, IChartViewer.PEAK_TYPE_LOWEST, IChartViewer.PEAK_TYPE_CLOSEST };

    private SamplingProperties properties;
    private final Set<SamplingPanelListener> listeners;
    private final Object applyLock;

    private final JCheckBox enableCheckBox;

    private final JLabel meanTypeLabel;
    private final JComboBox<Integer> meanTypeCombo;
    private final JList<Integer> meanTypeList;

    private final JLabel peakTypeLabel;
    private final JComboBox<Integer> peakTypeCombo;
    private final JList<Integer> peakTypeList;

    public SamplingPanel() {
        super(new GridBagLayout());
        properties = new SamplingProperties();
        listeners = Collections.newSetFromMap(new WeakHashMap<>());
        applyLock = new Object();

        int gridy = 0;

        enableCheckBox = new JCheckBox(ENABLE_TXT);
        enableCheckBox.setToolTipText(ENABLE_TOOLTIP);
        enableCheckBox.addActionListener(this);
        GridBagConstraints enableCheckBoxConstraints = new GridBagConstraints();
        enableCheckBoxConstraints.fill = GridBagConstraints.HORIZONTAL;
        enableCheckBoxConstraints.gridx = 0;
        enableCheckBoxConstraints.gridy = gridy++;
        enableCheckBoxConstraints.weightx = 1;
        enableCheckBoxConstraints.weighty = 0;
        enableCheckBoxConstraints.gridwidth = GridBagConstraints.REMAINDER;
        enableCheckBoxConstraints.insets = new Insets(5, 5, 0, 5);
        add(enableCheckBox, enableCheckBoxConstraints);

        meanTypeLabel = new JLabel(MEAN_TYPE_TXT);
        GridBagConstraints meanTypeLabelConstraints = new GridBagConstraints();
        meanTypeLabelConstraints.fill = GridBagConstraints.BOTH;
        meanTypeLabelConstraints.gridx = 0;
        meanTypeLabelConstraints.gridy = gridy;
        meanTypeLabelConstraints.weightx = 0;
        meanTypeLabelConstraints.weighty = 0;
        meanTypeLabelConstraints.insets = new Insets(5, 5, 0, 5);
        add(meanTypeLabel, meanTypeLabelConstraints);
        meanTypeList = new JList<>(MEAN_TYPE);
        meanTypeCombo = new JComboBox<>(MEAN_TYPE);
        meanTypeCombo.setRenderer(new MeanTypeListCellRenderer());
        meanTypeCombo.addActionListener(this);
        GridBagConstraints meanTypeComboConstraints = new GridBagConstraints();
        meanTypeComboConstraints.fill = GridBagConstraints.BOTH;
        meanTypeComboConstraints.gridx = 1;
        meanTypeComboConstraints.gridy = gridy++;
        meanTypeComboConstraints.weightx = 1;
        meanTypeComboConstraints.weighty = 0;
        meanTypeComboConstraints.insets = new Insets(5, 0, 0, 5);
        add(meanTypeCombo, meanTypeComboConstraints);

        peakTypeLabel = new JLabel(PEAK_TYPE_TXT);
        GridBagConstraints peakTypeLabelConstraints = new GridBagConstraints();
        peakTypeLabelConstraints.fill = GridBagConstraints.BOTH;
        peakTypeLabelConstraints.gridx = 0;
        peakTypeLabelConstraints.gridy = gridy;
        peakTypeLabelConstraints.weightx = 0;
        peakTypeLabelConstraints.weighty = 0;
        peakTypeLabelConstraints.insets = new Insets(5, 5, 5, 5);
        add(peakTypeLabel, peakTypeLabelConstraints);
        peakTypeList = new JList<>(PEAK_TYPE);
        peakTypeCombo = new JComboBox<>(PEAK_TYPE);
        peakTypeCombo.setRenderer(new PeakTypeListCellRenderer());
        peakTypeCombo.addActionListener(this);
        GridBagConstraints peakTypeComboConstraints = new GridBagConstraints();
        peakTypeComboConstraints.fill = GridBagConstraints.BOTH;
        peakTypeComboConstraints.gridx = 1;
        peakTypeComboConstraints.gridy = gridy++;
        peakTypeComboConstraints.weightx = 1;
        peakTypeComboConstraints.weighty = 0;
        peakTypeComboConstraints.insets = new Insets(5, 0, 5, 5);
        add(peakTypeCombo, peakTypeComboConstraints);

    }

    public SamplingProperties getProperties() {
        return properties.clone();
    }

    public void setProperties(SamplingProperties properties) {
        boolean warn;
        SamplingProperties propertiesToApply;
        if (properties == null) {
            propertiesToApply = new SamplingProperties();
            warn = true;
        } else {
            propertiesToApply = properties.clone();
            warn = false;
        }
        this.properties = propertiesToApply;
        if (applyProperties(warn)) {
            warnListeners();
        }
    }

    private void updateTooltip(JComboBox<Integer> combo, JList<Integer> list) {
        JComponent comp = (JComponent) combo.getRenderer().getListCellRendererComponent(list,
                (Integer) combo.getSelectedItem(), combo.getSelectedIndex(), true, true);
        combo.setToolTipText(comp.getToolTipText());
    }

    private boolean applyProperties(boolean warn) {
        synchronized (applyLock) {
            enableCheckBox.removeActionListener(this);
            enableCheckBox.setSelected(this.properties.isEnabled());
            enableCheckBox.addActionListener(this);

            meanTypeCombo.removeActionListener(this);
            int mean = this.properties.getMean();
            meanTypeCombo.setSelectedItem(mean);
            Integer selected = (Integer) meanTypeCombo.getSelectedItem();
            if ((selected == null) || selected.intValue() != mean) {
                properties.setMean(0);
                meanTypeCombo.setSelectedIndex(0);
                warn = true;
            }
            updateTooltip(meanTypeCombo, meanTypeList);
            meanTypeCombo.addActionListener(this);

            peakTypeCombo.removeActionListener(this);
            int peak = properties.getPeakType();
            peakTypeCombo.setSelectedItem(peak);
            selected = (Integer) peakTypeCombo.getSelectedItem();
            if ((selected == null) || selected.intValue() != peak) {
                properties.setMean(0);
                peakTypeCombo.setSelectedIndex(0);
                warn = true;
            }
            updateTooltip(peakTypeCombo, peakTypeList);
            peakTypeCombo.addActionListener(this);
        }
        return warn;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e != null) {
            if (e.getSource() == enableCheckBox) {
                properties.setEnabled(enableCheckBox.isSelected());
                warnListeners();
            } else if (e.getSource() == meanTypeCombo) {
                updateTooltip(meanTypeCombo, meanTypeList);
                properties.setMean((Integer) meanTypeCombo.getSelectedItem());
                warnListeners();
            } else if (e.getSource() == peakTypeCombo) {
                updateTooltip(peakTypeCombo, peakTypeList);
                properties.setPeakType((Integer) peakTypeCombo.getSelectedItem());
                warnListeners();
            }
        }
    }

    public void addSamplingPanelListener(SamplingPanelListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    public void removeSamplingPanelListener(SamplingPanelListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    private void warnListeners() {
        SamplingPanelEvent event = new SamplingPanelEvent(this);
        List<SamplingPanelListener> copy = new ArrayList<>();
        synchronized (listeners) {
            copy.addAll(listeners);
        }
        for (SamplingPanelListener listener : copy) {
            listener.samplingPanelChanged(event);
        }
        copy.clear();
    }

}
