/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Arrays;
import java.util.Random;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.WindowConstants;

import fr.soleil.comete.definition.data.target.scalar.IEditableTextComponent;
import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.listener.IImageViewerListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.listener.MaskListener;
import fr.soleil.comete.definition.util.AbstractValueConvertor;
import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.comete.definition.widget.util.IRoi;
import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MathematicSector;
import fr.soleil.comete.swing.image.ImageTransformationPanel;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.data.target.IFormatableTarget;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * A {@link Panel} that encapsulates an {@link ImageViewer} to be able to apply some rotations and symmetry
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class RotaryImageViewer extends Panel implements IImageViewer, IEditableTextComponent, IFormatableTarget {

    private static final long serialVersionUID = 5076466091658427498L;

    protected static final String FILTER_SEPARATOR = ", ";

    protected fr.soleil.comete.swing.image.util.Transformation lastRotationAction;

    protected final IImageViewer imageViewer;
    protected final ImageViewer imageDisplayer;
    protected final ImageTransformationPanel transformationPanel;
    protected JSplitPane mainSplitPane;
    protected volatile boolean showImageTransformationActions;
    protected JScrollPane transformationScrollPane;

    public RotaryImageViewer() {
        this(null, null);
    }

    public RotaryImageViewer(ImageViewer imageViewer) {
        this(imageViewer, null);
    }

    public RotaryImageViewer(ImageViewer imageViewer, ImageTransformationPanel transformationPanel) {
        super();
        setLayout(new BorderLayout());
        lastRotationAction = fr.soleil.comete.swing.image.util.Transformation.NO_ROTATION;
        showImageTransformationActions = true;
        this.imageDisplayer = imageViewer == null ? generateImageViewer() : imageViewer;
        this.transformationPanel = transformationPanel == null ? generateImageTransformationPanel()
                : transformationPanel;
        this.imageViewer = this.transformationPanel.registerImageViewer(imageDisplayer);
        initComponents(imageViewer);
        layoutComponents();
    }

    protected ImageViewer generateImageViewer() {
        return new ImageViewer();
    }

    protected ImageTransformationPanel generateImageTransformationPanel() {
        return new ImageTransformationPanel();
    }

    protected void initComponents(ImageViewer imageViewer) {
        transformationScrollPane = new JScrollPane(transformationPanel);
        transformationScrollPane.setMinimumSize(new Dimension(0, 0));
        initMainSplitPane();
    }

    public String getNoRotationText() {
        return transformationPanel.getNoRotationText();
    }

    public void setNoRotationText(String text) {
        transformationPanel.setNoRotationText(text);
    }

    public String getPositiveRotationText() {
        return transformationPanel.getPositiveRotationText();
    }

    public void setPositiveRotationText(String text) {
        transformationPanel.setPositiveRotationText(text);
    }

    public String getNegativeRotationText() {
        return transformationPanel.getNegativeRotationText();
    }

    public void setNegativeRotationText(String text) {
        transformationPanel.setNegativeRotationText(text);
    }

    public String getRotationTitle() {
        return transformationPanel.getRotationTitle();
    }

    public void setRotationTitle(String title) {
        transformationPanel.setRotationTitle(title);
    }

    public String getSymmetryTitle() {
        return transformationPanel.getSymmetryTitle();
    }

    public void setSymmetryTitle(String title) {
        transformationPanel.setSymmetryTitle(title);
    }

    public String getTransformationText(fr.soleil.comete.swing.image.util.Transformation transformation) {
        return transformationPanel.getTransformationText(transformation);
    }

    public void setTransformationText(fr.soleil.comete.swing.image.util.Transformation transformation, String text) {
        transformationPanel.setTransformationText(transformation, text);
    }

    protected void initMainSplitPane() {
        if (showImageTransformationActions) {
            // initialize a separate JSplitPane for thread safety
            JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
            splitPane.setTopComponent(imageDisplayer);
            splitPane.setBottomComponent(transformationScrollPane);
            splitPane.setDividerLocation(0.8);
            splitPane.setDividerSize(8);
            splitPane.setResizeWeight(0.8);
            splitPane.setOneTouchExpandable(true);
            mainSplitPane = splitPane;
        } else {
            mainSplitPane = null;
        }
    }

    protected void layoutComponents() {
        JComponent comp = showImageTransformationActions ? mainSplitPane : imageDisplayer;
        if (comp != null) { // for thread safety
            add(comp, BorderLayout.CENTER);
        }
    }

    /**
     * Returns the encapsulated {@link ImageViewer}
     * 
     * @return An {@link ImageViewer}
     */
    public ImageViewer getImageViewer() {
        return imageDisplayer;
    }

    public boolean isShowImageTransformationActions() {
        return showImageTransformationActions;
    }

    public void setShowImageTransformationActions(boolean showPanel) {
        if (showPanel != showImageTransformationActions) {
            showImageTransformationActions = showPanel;
            removeAll();
            JSplitPane mainSplitPane = this.mainSplitPane; // for thread safety
            if (mainSplitPane != null) {
                mainSplitPane.removeAll();
            }
            initMainSplitPane();
            layoutComponents();
            revalidate();
            repaint();
        }
    }

    /**
     * Sets the x scale title
     * 
     * @param applyImmediately Whether to apply filters once scale set
     * @param name The x scale title
     */
    public void setXScale(boolean applyImmediately, String name) {
        transformationPanel.setXScale(applyImmediately, name);
    }

    /**
     * Sets the y scale title
     * 
     * @param applyImmediately Whether to apply filters once scale set
     * @param name The y scale title
     */
    public void setYScale(boolean applyImmediately, String name) {
        transformationPanel.setYScale(applyImmediately, name);
    }

    /**
     * Returns the x axis format
     * 
     * @return A {@link String}
     */
    @Override
    public String getXAxisFormat() {
        return transformationPanel.getXAxisFormat();
    }

    @Override
    public void setXAxisFormat(String format) {
        imageViewer.setXAxisFormat(format);
    }

    /**
     * Sets the x axis format
     * 
     * @param applyImmediately Whether to apply filters once format set
     * @param xAxisFormat The x axis format
     */
    public void setXAxisFormat(boolean applyImmediately, String xAxisFormat) {
        transformationPanel.setXAxisFormat(applyImmediately, xAxisFormat);
    }

    /**
     * Returns the y axis format
     * 
     * @return A {@link String}
     */
    @Override
    public String getYAxisFormat() {
        return transformationPanel.getYAxisFormat();
    }

    @Override
    public void setYAxisFormat(String format) {
        imageViewer.setYAxisFormat(format);
    }

    /**
     * Sets the y axis format
     * 
     * @param applyImmediately Whether to apply filters once format set
     * @param xAxisFormat They axis format
     */
    public void setYAxisFormat(boolean applyImmediately, String yAxisFormat) {
        transformationPanel.setYAxisFormat(applyImmediately, yAxisFormat);
    }

    @Override
    public Object[] getNumberMatrix() {
        return imageViewer.getNumberMatrix();
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        int[] shape = ArrayUtils.recoverShape(value);
        if ((shape == null) || (shape.length != 2)) {
            setFlatNumberMatrix(null, 0, 0);
        } else {
            setFlatNumberMatrix(ArrayUtils.convertArrayDimensionFromNTo1(value), shape[1], shape[0]);
        }
    }

    @Override
    public Object getFlatNumberMatrix() {
        return imageViewer.getFlatNumberMatrix();
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        imageViewer.setFlatNumberMatrix(value, width, height);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return false;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        return imageViewer.getMatrixDataWidth(concernedDataClass);
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return imageViewer.getMatrixDataHeight(concernedDataClass);
    }

    @Override
    public String getText() {
        return imageViewer.getText();
    }

    @Override
    public void setText(String text) {
        imageViewer.setText(text);
    }

    @Override
    public String getFormat() {
        return imageViewer.getFormat();
    }

    @Override
    public void setFormat(String format) {
        imageViewer.setFormat(format);
    }

    @Override
    public void setEditable(boolean b) {
        imageViewer.setEditable(b);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (imageViewer != null) {
            imageViewer.setEnabled(enabled);
        }
    }

    @Override
    public boolean isEditable() {
        return imageViewer.isEditable();
    }

    /**
     * Returns this {@link RotaryImageViewer}'s current transformations
     * 
     * @return A {@link fr.soleil.comete.swing.image.util.Transformation} array, never <code>null</code>.
     */
    public fr.soleil.comete.swing.image.util.Transformation[] getTransformations() {
        return transformationPanel.getTransformations();
    }

    /**
     * Applies transformations to this {@link RotaryImageViewer}
     * 
     * @param applyImmediately Whether to apply filters once scale set
     * @param transformations The transformations to apply
     */
    public void setTransformations(boolean applyImmediately,
            fr.soleil.comete.swing.image.util.Transformation... transformations) {
        transformationPanel.setTransformations(applyImmediately, transformations);
    }

    @Override
    public String getSnapshotDirectory() {
        return imageViewer.getSnapshotDirectory();
    }

    @Override
    public void setSnapshotDirectory(String snapshotDirectory) {
        imageViewer.setSnapshotDirectory(snapshotDirectory);
    }

    @Override
    public String getSnapshotFile() {
        return imageViewer.getSnapshotFile();
    }

    @Override
    public void addSnapshotListener(ISnapshotListener listener) {
        imageViewer.addSnapshotListener(listener);
    }

    @Override
    public void removeSnapshotListener(ISnapshotListener listener) {
        imageViewer.removeSnapshotListener(listener);
    }

    @Override
    public void loadDataFile(String fileName) {
        imageViewer.loadDataFile(fileName);
    }

    @Override
    public void saveDataFile(String path) {
        imageViewer.saveDataFile(path);
    }

    @Override
    public String getDataDirectory() {
        return imageViewer.getDataDirectory();
    }

    @Override
    public void setDataDirectory(String path) {
        imageViewer.setDataDirectory(path);
    }

    @Override
    public String getDataFile() {
        return imageViewer.getDataFile();
    }

    @Override
    public void addDataTransferListener(IDataTransferListener listener) {
        imageViewer.addDataTransferListener(listener);
    }

    @Override
    public void removeDataTransferListener(IDataTransferListener listener) {
        imageViewer.removeDataTransferListener(listener);
    }

    @Override
    public void addMaskListener(MaskListener listener) {
        imageViewer.addMaskListener(listener);
    }

    @Override
    public void removeMaskListener(MaskListener listener) {
        imageViewer.removeMaskListener(listener);
    }

    @Override
    public void removeAllMaskListeners() {
        imageViewer.removeAllMaskListeners();
    }

    @Override
    public boolean setMask(Mask mask) {
        return imageViewer.setMask(mask);
    }

    @Override
    public Mask getMask() {
        return imageViewer.getMask();
    }

    @Override
    public int getImageWidth() {
        return imageViewer.getImageWidth();
    }

    @Override
    public int getImageHeight() {
        return imageViewer.getImageHeight();
    }

    @Override
    public String getImageName() {
        return imageViewer.getImageName();
    }

    @Override
    public void setImageName(String name) {
        imageViewer.setImageName(name);
    }

    @Override
    public void addImageViewerListener(IImageViewerListener listener) {
        imageViewer.addImageViewerListener(listener);
    }

    @Override
    public void removeImageViewerListener(IImageViewerListener listener) {
        imageViewer.removeImageViewerListener(listener);
    }

    @Override
    public Gradient getGradient() {
        return imageViewer.getGradient();
    }

    @Override
    public void setGradient(Gradient gradient) {
        imageViewer.setGradient(gradient);
    }

    @Override
    public boolean isUseMaskManagement() {
        return imageViewer.isUseMaskManagement();
    }

    @Override
    public void setUseMaskManagement(boolean useMask) {
        imageViewer.setUseMaskManagement(useMask);
    }

    @Override
    public void loadMask(String path, boolean addToCurrentMask) {
        imageViewer.loadMask(path, addToCurrentMask);
    }

    @Override
    public void saveMask(String path) {
        imageViewer.saveMask(path);
    }

    @Override
    public boolean isAlwaysFitMaxSize() {
        return imageViewer.isAlwaysFitMaxSize();
    }

    @Override
    public void setAlwaysFitMaxSize(boolean alwaysFitMaxSize) {
        imageViewer.setAlwaysFitMaxSize(alwaysFitMaxSize);
    }

    @Override
    public void setShowRoiInformationTable(boolean showTable) {
        imageViewer.setShowRoiInformationTable(showTable);
    }

    @Override
    public boolean isShowRoiInformationTable() {
        return imageViewer.isShowRoiInformationTable();
    }

    @Override
    public boolean isDrawBeamPosition() {
        return imageViewer.isDrawBeamPosition();
    }

    @Override
    public void setDrawBeamPosition(boolean drawBeam) {
        imageViewer.setDrawBeamPosition(drawBeam);
    }

    @Override
    public double[] getBeamPoint() {
        return imageViewer.getBeamPoint();
    }

    @Override
    public void setBeamPoint(double... beamPoint) {
        imageViewer.setBeamPoint(beamPoint);
    }

    @Override
    public void clearBeamPoint() {
        imageViewer.clearBeamPoint();
    }

    @Override
    public void registerSectorClass(Class<?> sectorClass) {
        imageViewer.registerSectorClass(sectorClass);
    }

    @Override
    public boolean isUseSectorManagement() {
        return imageViewer.isUseSectorManagement();
    }

    @Override
    public void setUseSectorManagement(boolean useSector) {
        imageViewer.setUseSectorManagement(useSector);
    }

    @Override
    public MathematicSector getSector() {
        return imageViewer.getSector();
    }

    @Override
    public void setSector(MathematicSector sector) {
        imageViewer.setSector(sector);
    }

    @Override
    public IValueConvertor getXAxisConvertor() {
        return imageViewer.getXAxisConvertor();
    }

    @Override
    public void setXAxisConvertor(IValueConvertor xAxisConvertor) {
        imageViewer.setXAxisConvertor(xAxisConvertor);
    }

    @Override
    public IValueConvertor getYAxisConvertor() {
        return imageViewer.getYAxisConvertor();
    }

    @Override
    public void setYAxisConvertor(IValueConvertor yAxisConvertor) {
        imageViewer.setYAxisConvertor(yAxisConvertor);
    }

    @Override
    public boolean isSingleRoiMode() {
        return imageViewer.isSingleRoiMode();
    }

    @Override
    public void setSingleRoiMode(boolean singleRoiMode) {
        imageViewer.setSingleRoiMode(singleRoiMode);
    }

    @Override
    public boolean isCleanOnDataSetting() {
        return imageViewer.isCleanOnDataSetting();
    }

    @Override
    public void setCleanOnDataSetting(boolean clean) {
        imageViewer.setCleanOnDataSetting(clean);
    }

    @Override
    public void addRoi(IRoi roi) {
        imageViewer.addRoi(roi);
    }

    @Override
    public void addRoi(IRoi roi, boolean centered) {
        imageViewer.addRoi(roi, centered);
    }

    @Override
    public CometeColor getNanColor() {
        return imageViewer.getNanColor();
    }

    @Override
    public void setNanColor(CometeColor nanColor) {
        imageViewer.setNanColor(nanColor);
    }

    @Override
    public CometeColor getPositiveInfinityColor() {
        return imageViewer.getPositiveInfinityColor();
    }

    @Override
    public void setPositiveInfinityColor(CometeColor positiveInfinityColor) {
        imageViewer.setPositiveInfinityColor(positiveInfinityColor);
    }

    @Override
    public CometeColor getNegativeInfinityColor() {
        return imageViewer.getNegativeInfinityColor();
    }

    @Override
    public void setNegativeInfinityColor(CometeColor negativeInfinityColor) {
        imageViewer.setNegativeInfinityColor(negativeInfinityColor);
    }

    @Override
    public ImageProperties getImageProperties() {
        return imageViewer.getImageProperties();
    }

    @Override
    public void setImageProperties(ImageProperties properties) {
        imageViewer.setImageProperties(properties);
    }

    @Override
    public boolean[][] getBooleanMatrix() {
        return imageViewer.getBooleanMatrix();
    }

    @Override
    public void setBooleanMatrix(boolean[][] value) {
        imageViewer.setBooleanMatrix(value);
    }

    @Override
    public boolean[] getFlatBooleanMatrix() {
        return (boolean[]) imageViewer.getFlatNumberMatrix();
    }

    @Override
    public void setFlatBooleanMatrix(boolean[] value, int width, int height) {
        imageViewer.setFlatNumberMatrix(value, width, height);
    }

    public JPopupMenu getPopupMenu() {
        return imageDisplayer.getPopupMenu();
    }

    public String getApplicationId() {
        return imageDisplayer.getApplicationId();
    }

    public void setApplicationId(String applicationId) {
        imageDisplayer.setApplicationId(applicationId);
    }

    protected static IValueConvertor generateAxisConvertor(final double coef, final double offset) {
        IValueConvertor result = new AbstractValueConvertor() {
            @Override
            public boolean isValid() {
                return true;
            }

            @Override
            public double convertValue(double value) {
                return value * coef + offset;
            }
        };
        return result;
    }

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(RotaryImageViewer.class.getSimpleName() + " test");
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        final RotaryImageViewer imageViewer = new RotaryImageViewer();
        imageViewer.getImageViewer().setAlwaysFitMaxSize(true);
        imageViewer.setApplicationId(CometeUtils.generateIdForClass(RotaryImageViewer.class));
        boolean stress = false;
        if (args != null) {
            for (String arg : args) {
                if ("stress".equalsIgnoreCase(arg)) {
                    stress = true;
                    break;
                }
            }
        }
        final RotaryImageViewer imageViewer2 = new RotaryImageViewer();
        imageViewer2.getImageViewer().setAlwaysFitMaxSize(true);
        imageViewer2.setApplicationId(CometeUtils.generateIdForClass(RotaryImageViewer.class) + 1);

        IValueConvertor xconvertor = generateAxisConvertor(5, 100);
        IValueConvertor yconvertor = generateAxisConvertor(10, 1024);

        imageViewer2.setXAxisConvertor(xconvertor);
        imageViewer2.setYAxisConvertor(yconvertor);

        if (stress) {
            imageViewer.setXAxisConvertor(xconvertor);
            imageViewer.setYAxisConvertor(yconvertor);
            new Thread("Image data") {
                protected final Random random = new Random();

                @Override
                public void run() {
                    int width = 1024;
                    int height = 1024;
                    int runIndex = 0;
                    double[] attr_beam_image_read = null;
                    while (true) {
                        runIndex++;
                        if (runIndex % 10 == 0) {
                            width = 1024;
                            height = 1024;
                            attr_beam_image_read = null;
                        } else if (runIndex % 3 == 0) {
                            width = 1;
                            height = 1;
                            attr_beam_image_read = new double[1];
                            attr_beam_image_read[0] = 0;
                        } else if (runIndex % 5 == 0) {
                            width = 2048;
                            height = 2048;
                            attr_beam_image_read = new double[width * height];
                            Arrays.fill(attr_beam_image_read, Double.NaN);
                        } else {
                            width = 1024;
                            height = 1024;

                            attr_beam_image_read = new double[width * height];
                            int max_xy = width;

                            int bimg_center_x = random.nextInt() % (int) (max_xy * 0.050000000000000003D);
                            if (random.nextLong() % 2L == 0L) {
                                bimg_center_x *= -1;
                            }
                            int bimg_center_y = random.nextInt() % (int) (max_xy * 0.050000000000000003D);
                            if (random.nextInt() % 2 == 0) {
                                bimg_center_y *= -1;
                            }
                            int bimg_offset_to_zero = (max_xy - 1) / 2;
                            int bimg_x_offset_to_zero = bimg_offset_to_zero + bimg_center_x;
                            int bimg_y_offset_to_zero = bimg_offset_to_zero + bimg_center_y;
                            int limit = max_xy / 8;
                            int noise = random.nextInt() % (int) (limit * 0.20000000000000001D);
                            if (random.nextInt() % 2 == 0) {
                                noise *= -1;
                            }
                            limit += noise;
                            for (int cpt = 0; cpt < width * height; cpt++) {
                                attr_beam_image_read[cpt] = 0;
                            }

                            for (int i = -limit; i < limit; i++) {
                                int y = i + bimg_y_offset_to_zero;
                                if (y >= 0 && y < max_xy) {
                                    for (int j = -limit; j < limit; j++) {
                                        int x = j + bimg_x_offset_to_zero;
                                        if (x >= 0 && x < max_xy) {
                                            int value = (int) Math.sqrt(i * i + j * j);
                                            attr_beam_image_read[x * max_xy + y] = value >= limit ? 0 : limit - value;
                                        }
                                    }

                                }
                            }
                        }

                        imageViewer.setFlatNumberMatrix(attr_beam_image_read, width, height);
                        imageViewer2.setFlatNumberMatrix(attr_beam_image_read, width, height);
                        imageViewer.revalidate();
                        imageViewer2.revalidate();
                        try {
                            sleep(300);
                        } catch (InterruptedException e) {
                            // don't care about this exception: just quit test;
                        }
                    }
                }
            }.start();
        } else {
            int width = 3, height = 2;
            // Activate following code to test GBS-123
//          double[] array1 = new double[] { 4, 5, 6 };
//          double[] array2 = new double[] { 7, 8 };
//          ArrayPositionConvertor conv1 = new ArrayPositionConvertor();
//          conv1.setNumberArray(array1);
//          imageViewer.setXAxisConvertor(conv1);
//          ArrayPositionConvertor conv2 = new ArrayPositionConvertor();
//          conv2.setNumberArray(array2);
//          imageViewer.setYAxisConvertor(conv2);
            int[] data = new int[height * width];
            int value = 0;
            for (int i = 0; i < data.length; i++) {
                data[i] = value++;
            }
            imageViewer.setFlatNumberMatrix(data, width, height);
            imageViewer2.setFlatNumberMatrix(data, width, height);
        }
        JSplitPane mainSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        mainSplitPane.setLeftComponent(imageViewer);
        mainSplitPane.setRightComponent(imageViewer2);
        mainSplitPane.setDividerLocation(0.5);
        mainSplitPane.setDividerSize(8);
        mainSplitPane.setResizeWeight(0.5);
        mainSplitPane.setOneTouchExpandable(true);
        testFrame.setContentPane(mainSplitPane);
        testFrame.pack();
        testFrame.setSize(testFrame.getWidth(), 400);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A List of all possible transformations.
     * <p>
     * <big style="color:red;"><b><u>Warning! This enum was only restored due to Coox problems and should never be used.
     * </u></b></big> (JAVAAPI-612)
     * </p>
     * 
     * @deprecated Don't use this enum except for JAVAAPI-612. You should use
     *             {@link fr.soleil.comete.swing.image.util.Transformation} instead.
     */
    @Deprecated
    public static enum Transformation {
        POSITIVE_ROTATION("Rotation+"), NEGATIVE_ROTATION("Rotation-"), NO_ROTATION("None"),
        VERTICAL_SYMMETRY("Symmetry(V axis)"), HORIZONTAL_SYMMETRY("Symmetry(H axis)");

        protected String stringValue;

        private Transformation(String stringValue) {
            this.stringValue = stringValue;
        }

        @Override
        public String toString() {
            return stringValue;
        }

        /**
         * Parses a {@link String} value to return a {@link Transformation}
         * 
         * @param value The {@link String} value
         * @return A {@link Transformation}. May be <code>null</code>.
         */
        public static Transformation parseTransformation(String value) {
            Transformation transformation = null;
            for (Transformation tmp : values()) {
                if (tmp.toString().equals(value)) {
                    transformation = tmp;
                    break;
                }
            }
            return transformation;
        }

        /**
         * Converts a {@link Transformation} into a {@link fr.soleil.comete.swing.image.util.Transformation}.
         * 
         * @param t The {@link Transformation} to convert.
         * @return A {@link fr.soleil.comete.swing.image.util.Transformation}.
         */
        public static fr.soleil.comete.swing.image.util.Transformation toOfficialTransformation(Transformation t) {
            fr.soleil.comete.swing.image.util.Transformation result = null;
            if (t != null) {
                switch (t) {
                    case POSITIVE_ROTATION:
                        result = fr.soleil.comete.swing.image.util.Transformation.POSITIVE_ROTATION;
                        break;
                    case NEGATIVE_ROTATION:
                        result = fr.soleil.comete.swing.image.util.Transformation.NEGATIVE_ROTATION;
                        break;
                    case NO_ROTATION:
                        result = fr.soleil.comete.swing.image.util.Transformation.NO_ROTATION;
                        break;
                    case VERTICAL_SYMMETRY:
                        result = fr.soleil.comete.swing.image.util.Transformation.VERTICAL_SYMMETRY;
                        break;
                    case HORIZONTAL_SYMMETRY:
                        result = fr.soleil.comete.swing.image.util.Transformation.HORIZONTAL_SYMMETRY;
                        break;
                }
            }
            return result;
        }
    }

}
