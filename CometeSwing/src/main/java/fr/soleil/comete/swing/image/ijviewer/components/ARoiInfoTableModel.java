/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing.image.ijviewer.components;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.events.IIJViewerListener;
import fr.soleil.comete.swing.image.ijviewer.events.IRoiManagerListener;
import fr.soleil.comete.swing.image.ijviewer.events.RoiEvent;
import ij.gui.Roi;

/**
 * Abstract class for roi information table model.
 * 
 * @author MAINGUY, Rapha&euml;l GIRARDOT
 */
public abstract class ARoiInfoTableModel extends DefaultTableModel implements IRoiManagerListener, IIJViewerListener {

    private static final long serialVersionUID = 8517732698793326944L;

    protected final WeakReference<ImageViewer> viewerRef;
    protected final List<Roi> data;
    protected double pixelSize;

    public ARoiInfoTableModel(ImageViewer viewer) {
        super();
        viewerRef = viewer == null ? null : new WeakReference<>(viewer);
        data = new ArrayList<>();
        pixelSize = 1;
    }

    @Override
    public int getRowCount() {
        return data == null ? 0 : data.size();
    }

    public int getRoiIndex(Roi roi) {
        return data.indexOf(roi);
    }

    public Roi getRoi(int index) {
        return data.get(index);
    }

    /**
     * @return the pixelSize
     */
    public double getPixelSize() {
        return pixelSize;
    }

    @Override
    public void roiAdded(RoiEvent event) {
        data.add(event.getRoi());
        fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }

    @Override
    public void roiChanged(RoiEvent event) {
        int index = data.indexOf(event.getRoi());
        if (index > -1) {
            fireTableRowsUpdated(index, index);
        }
    }

    @Override
    public void roiDeleted(RoiEvent event) {
        int index = data.indexOf(event.getRoi());
        if (index > -1) {
            data.remove(index);
            fireTableRowsDeleted(index, index);
        }
    }
}
