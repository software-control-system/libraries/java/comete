/*
 * This file is part of CometeSwing.
 * 
 * CometeSwing is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeSwing is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeSwing. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import fr.soleil.comete.definition.data.information.BooleanInformation;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.swing.util.IIconComponent;
import fr.soleil.data.target.scalar.IBooleanTarget;

/**
 * A light to show the value of a signal attribute (this means an attribute representing a boolean
 * value, but of type BooleanScalar or NumberScalar)
 * 
 * @author ho
 */
public class IconButton extends AbstractButton implements IIconComponent, IBooleanTarget, ActionListener {

    private static final long serialVersionUID = -1963415554306222077L;

    private String falseLabel = "FALSE";
    private String trueLabel = "TRUE";

    private int trueIconId;
    private int falseIconId;

    private boolean viewIcon;
    private boolean viewLabel;
    private boolean state;

    private volatile boolean editable;

    public IconButton() {
        super();
        viewIcon = true;
        viewLabel = true;
        falseIconId = LED_GRAY;
        trueIconId = LED_GREEN;
        setIconId(LED_KO);
        editable = true;
        setSelected(false);
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (isEditable()) {
            long actualEvent = e.getWhen();
            if (oldEvent != actualEvent) {
                oldEvent = actualEvent;
                setSelected(!state);
                warnMediators(new BooleanInformation(this, isSelected()));
                fireActionPerformed(new EventObject(this));
            }
        }
    }

    public void setValue(boolean avalue) {
        if (avalue) {
            if (viewIcon) {
                setIconId(trueIconId);
            } else {
                setIcon(null);
            }
            if (viewLabel) {
                setText(trueLabel);
            } else {
                setText(null);
            }
        } else {
            if (viewIcon) {
                setIconId(falseIconId);
            } else {
                setIcon(null);
            }
            if (viewLabel) {
                setText(falseLabel);
            } else {
                setText(null);
            }
        }
    }

    @Override
    public void setSelected(boolean state) {
        this.state = state;
        setValue(state);
    }

    @Override
    public boolean isSelected() {
        return state;
    }

    @Override
    public void setIconId(int iconId) {
        setIcon(IIconComponent.getIconForId(iconId));
    }

    public String getFalseLabel() {
        return falseLabel;
    }

    public void setFalseLabel(String falseLabel) {
        this.falseLabel = falseLabel;
        setValue(isSelected());
    }

    public String getTrueLabel() {
        return trueLabel;
    }

    public void setTrueLabel(String trueLabel) {
        this.trueLabel = trueLabel;
        setValue(isSelected());
    }

    public void setTrueIconId(int iconId) {
        trueIconId = iconId;
        setValue(isSelected());
    }

    public int getTrueIconId() {
        return trueIconId;
    }

    public void setFalseIconId(int iconId) {
        falseIconId = iconId;
        setValue(isSelected());
    }

    public int getFalseIconId() {
        return falseIconId;
    }

    public void setViewLabel(boolean viewLabel) {
        this.viewLabel = viewLabel;
    }

    public boolean isViewLabel() {
        return viewLabel;
    }

    public void setViewIcon(boolean viewIcon) {
        this.viewIcon = viewIcon;
    }

    public boolean isViewIcon() {
        return viewIcon;
    }

    /**
     * 16uf Main class, so you can have an example. You can monitor your own attribute by giving its
     * full path name in argument
     */
    public static void main(String[] args) {
        IPanel panel = new Panel();
        IconButton widget = new IconButton();
        widget.setViewIcon(false);
        widget.setFalseIconId(IconButton.LED_KO);
        widget.setFalseLabel("FAUX");
        widget.setTrueLabel("VRAI");
        widget.setTrueIconId(IconButton.LED_BLUE);

        panel.add(widget);

        IFrame frame = new Frame();
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setTitle("BooleanIconButton test");
    }

}
