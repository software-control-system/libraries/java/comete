package fr.soleil.comete.swing.util;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.lang.reflect.Constructor;

import junit.extensions.jfcunit.TestHelper;
import junit.extensions.jfcunit.eventdata.AbstractMouseEventData;
import junit.extensions.jfcunit.keyboard.JFCKeyStroke;

/**
 * This Implementation of TestHelper is a copy of JFCTestHelper which is a final class. It's add a
 * method void click(final Component component, final int x, final int y) which allow to simulate a
 * click on component at the specified coordinates (x, y)
 * 
 * @author GRAMER
 * 
 */
public class CometeTestHelper extends TestHelper {
    // though this is a helper class, these "instance" variables are used
    // to "display" the difference between the previous "state" and the
    // current one.

    /**
     * The coordinate where the last event was fired.
     */
    private static Point s_last = new Point(0, 0);

    /**
     * The EventQueue on which all events are processed.
     */
    private final EventQueue m_queue;

    /**
     * The popup that appeared in the previous call.
     */
    private boolean m_lastPopup = false;

    /** Last number of clicks. */
    private int m_lastClicks;

    /**
     * The mouse buttons that were pressed in the previous call.
     */
    private int m_lastMouseModifiers = 0;

    /**
     * The mouse buttons that were pressed in the previous call.
     */
    private long m_lastPressed = 0;

    /**
     * Constructor.
     */
    public CometeTestHelper() {
        super();
        m_queue = Toolkit.getDefaultToolkit().getSystemEventQueue();
    }

    /**
     * Process a key press event on a component.
     * 
     * @param ultimate The ultimate parent Component
     * @param stroke The JFCKeyStroke to be performed.
     */
    @Override
    protected void keyPressed(final Component ultimate, final JFCKeyStroke stroke) {
        postEvent(new KeyEvent(ultimate, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), stroke.getModifiers(),
                stroke.getKeyCode(), stroke.getKeyChar()));
    }

    /**
     * Process a key release event on a component.
     * 
     * @param ultimate The ultimate parent Component
     * @param stroke The JFCKeyStroke to be performed.
     */
    @Override
    protected void keyReleased(final Component ultimate, final JFCKeyStroke stroke) {
        if (stroke.isTyped()) {
            postEvent(new KeyEvent(ultimate, KeyEvent.KEY_TYPED, System.currentTimeMillis(), stroke.getModifiers(),
                    KeyEvent.VK_UNDEFINED, stroke.getKeyChar()));
        }

        postEvent(new KeyEvent(ultimate, KeyEvent.KEY_RELEASED, System.currentTimeMillis(), stroke.getModifiers(),
                stroke.getKeyCode(), stroke.getKeyChar()));
    }

    /**
     * Process a mouse move event on a component.
     * 
     * @param ultimate The ultimate parent Component
     * @param x The x coordinate of the point where the mouse is being moved to.
     * @param y The y coordinate of the point where the mouse is being moved to.
     */
    @Override
    protected void mouseMoved(final Component ultimate, final int x, final int y) {
        final Point dest = new Point(x, y);
        dest.translate(-ultimate.getLocationOnScreen().x, -ultimate.getLocationOnScreen().y);

        int mouseEventType = MouseEvent.MOUSE_MOVED;

        if (m_lastPressed != 0) {
            mouseEventType = MouseEvent.MOUSE_DRAGGED;
        }

        while ((s_last.x != dest.x) || (s_last.y != dest.y)) {
            s_last = calcNextPoint(s_last, dest, getStep());
            postEvent(new MouseEvent(ultimate, mouseEventType, System.currentTimeMillis(), m_lastMouseModifiers,
                    s_last.x, s_last.y, 0, m_lastPopup));
        }
    }

    /**
     * Process a mouse press event on a component.
     * 
     * @param ultimate The ultimate parent Component
     * @param modifiers The modifiers associated with this mouse event.
     * @param click The number of clicks associated with this mouse event.
     * @param isPopupTrigger Whether this mouse event will generate a popup.
     */
    @Override
    protected void mousePressed(final Component ultimate, final int modifiers, final int click,
            final boolean isPopupTrigger) {
        m_lastPressed = System.currentTimeMillis();
        m_lastPopup = isPopupTrigger;
        m_lastMouseModifiers = modifiers;

        if (modifiers != 0) {
            postEvent(new MouseEvent(ultimate, MouseEvent.MOUSE_PRESSED, m_lastPressed, modifiers, s_last.x, s_last.y,
                    click, isPopupTrigger));
        }
    }

    /**
     * Process a mouse release event on a component.
     * 
     * @param ultimate The ultimate parent Component
     * @param modifiers The modifiers associated with this mouse event.
     * @param click The number of clicks associated with this mouse event.
     * @param isPopupTrigger Whether this mouse event will generate a popup.
     */
    @Override
    protected void mouseReleased(final Component ultimate, final int modifiers, final int click,
            final boolean isPopupTrigger) {
        m_lastMouseModifiers = modifiers;
        m_lastPopup = isPopupTrigger;
        m_lastClicks = click;

        if (modifiers != 0) {
            postEvent(new MouseEvent(ultimate, MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), modifiers,
                    s_last.x, s_last.y, click, isPopupTrigger));

            final long delta = m_lastPressed - System.currentTimeMillis();

            if ((click > 0) && (delta < 100)) {
                postEvent(new MouseEvent(ultimate, MouseEvent.MOUSE_CLICKED, System.currentTimeMillis(), modifiers,
                        s_last.x, s_last.y, click, isPopupTrigger));
            }
        }

        m_lastPressed = 0;
    }

    /**
     * Simulate rotating the mouseWheel Only supported in Java 1.4.
     * 
     * @param ultimate Component to fire the events on.
     * @param amount Amount to rotate the wheel.
     * @param wheelRotation amount to rotate the wheel. positive for rotation towards the user.
     *            Negative for rotation away from the user.
     */
    @Override
    protected void mouseWheel(final Component ultimate, final int amount, final int wheelRotation) {
        // This class is coded with reflection to allow the class
        // to compile on versions of the JDK less than 1.4.
        try {
            final Class<?> mouseWheelEventClass = Class.forName("java.awt.event.MouseWheelEvent");
            final Constructor<?> constructor = mouseWheelEventClass.getConstructor(new Class[] { Component.class,
                    int.class, long.class, int.class, int.class, int.class, int.class, boolean.class, int.class,
                    int.class, int.class });

            int scrollType = 0;

            final int eventId = mouseWheelEventClass.getField("MOUSE_WHEEL").getInt(null);

            if ((m_lastMouseModifiers & MouseEvent.SHIFT_MASK) > 1) {
                scrollType = mouseWheelEventClass.getField("WHEEL_BLOCK_SCROLL").getInt(null);
            } else {
                scrollType = mouseWheelEventClass.getField("WHEEL_UNIT_SCROLL").getInt(null);
            }

            postEvent((AWTEvent) constructor.newInstance(new Object[] { ultimate, new Integer(eventId),
                    new Long(System.currentTimeMillis()), new Integer(m_lastMouseModifiers), new Integer(s_last.x),
                    new Integer(s_last.y), new Integer(m_lastClicks), new Boolean(m_lastPopup),
                    new Integer(scrollType), new Integer(amount), new Integer(wheelRotation) }));
        } catch (final Exception ex) {
            throw new RuntimeException("Mouse Wheel not supported" + ex.toString());
        }

        // new MouseWheelEvent(Component source,
        // int id,
        // long when,
        // int modifiers,
        // int x,
        // int y,
        // int clickCount,
        // boolean popupTrigger,
        // int scrollType,
        // int scrollAmount,
        // int wheelRotation
        // throw new RuntimeException("Method is not supported by abstract class: mouseWheel");
    }

    /**
     * simutate a click on component at the specified coordinates (x,y)
     * 
     * @param evtData The event data container
     * @param x coordinate in component
     * @param y coordinate in component
     */
    public void enterClickAndLeave(final AbstractMouseEventData evtData, final int x, final int y) {
        if (evtData != null) {
            evtData.getTestCase().pauseAWT();
            if (!evtData.prepareComponent()) {
                evtData.getTestCase().resumeAWT();
            } else {

                evtData.getTestCase().resumeAWT();

                final int numberOfClicks = evtData.getNumberOfClicks();
                final int modifiers = evtData.getModifiers();
                final boolean isPopupTrigger = evtData.getPopupTrigger();

                final Component ultimate = evtData.getRoot();

                // try to clear the event queue
                evtData.getTestCase().flushAWT();

                // Translate the screen coordinates returned by the event to frame coordinates.
                final Point screen = evtData.getLocationOnScreen();
                pressModifiers(ultimate, mouseToKeyModifiers(modifiers));

                // Insure a move event.
                mouseMoved(ultimate, screen.x - 5, screen.y - 5);
                mouseMoved(ultimate, x, y);

                for (int click = 1; click <= numberOfClicks; click++) {
                    mousePressed(ultimate, modifiers, click, isPopupTrigger);
                    mouseReleased(ultimate, modifiers, click, isPopupTrigger);
                }

                releaseModifiers(ultimate, mouseToKeyModifiers(modifiers));

                // try to clear the event queue
                evtData.getTestCase().flushAWT();
            }
        }
    }

    /**
     * This method is just present so as to put debug statements in one central place, without
     * repeating everywhere.
     * 
     * @param evt The event to be posted.
     */
    private void postEvent(final AWTEvent evt) {
        m_queue.postEvent(evt);
    }
}