package fr.soleil.comete.swing.util;
/**
 * Utilitaire permettant de faire des pauses dans une unit� donn�e. 
 * Par exemple, pour faire une pause de 12 secondes, faire Sleeper.SECONDS.sleep(12)
 * 
 * @author rh
 */
public enum Sleeper
{
    MILLISECONDS(1), SECONDS(1000), MINUTES(60000);
    
    private long millis;
    
    private Sleeper(long millis)
    {
        this.millis = millis;
    }
    
    public void sleep(long value)
    {
        try
        {
            Thread.sleep(getValueInMillis(value));
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    
    public long getValueInMillis(long value)
    {
        return value*millis;
    }
}
