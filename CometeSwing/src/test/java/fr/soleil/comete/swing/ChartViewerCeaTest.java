package fr.soleil.comete.swing;

import javax.swing.JFrame;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;

public class ChartViewerCeaTest {

	public static void main(String[] args) {
		JFrame frame = new JFrame("ChartTest");
		frame.setSize(500, 500);
		frame.setPreferredSize(frame.getSize());
		Chart chartViewer = new Chart();

		// important to avoid reinit curve properties on each player step
		chartViewer.setCleanDataViewConfigurationOnRemoving(false);
		chartViewer.setFreezePanelVisible(true);
		chartViewer.setManagementPanelVisible(false);
		chartViewer.setDataViewRemovingEnabled(false);
		chartViewer.setAxisSelectionVisible(false);
		chartViewer.setCyclingCustomMap(true);
		chartViewer.setMathExpressionEnabled(true);
		chartViewer.setAutoHighlightOnLegend(true);
		chartViewer.setGridStyle(IChartViewer.STYLE_DASH, IChartViewer.Y1);
		chartViewer.setGridStyle(IChartViewer.STYLE_DASH, IChartViewer.Y2);
		chartViewer.setGridStyle(IChartViewer.STYLE_DASH, IChartViewer.X);

		frame.setContentPane(chartViewer);

		// Test issue on setScaleMode
		ChartProperties chartProperties = chartViewer.getChartProperties();
		AxisProperties xAxisProperties = chartProperties.getXAxisProperties();
		xAxisProperties.setScaleMode(28);
		chartViewer.setChartProperties(chartProperties);

		// Exception
		// Exception in thread "AWT-EventQueue-0" java.lang.IllegalArgumentException:
		// setSelectedIndex: 7 out of bounds
		// at javax.swing.JComboBox.setSelectedIndex(JComboBox.java:620)
		// at fr.soleil.comete.swing.chart.AxisPanel.applyProperties(AxisPanel.java:595)

		frame.setVisible(true);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

}
