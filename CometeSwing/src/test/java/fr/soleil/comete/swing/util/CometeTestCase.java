package fr.soleil.comete.swing.util;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

import org.junit.Assert;

import junit.extensions.jfcunit.JFCTestCase;
import junit.extensions.jfcunit.eventdata.KeyEventData;
import junit.extensions.jfcunit.eventdata.MouseEventData;
import junit.extensions.jfcunit.finder.DialogFinder;

public abstract class CometeTestCase extends JFCTestCase {

    protected CometeTestHelper helper;
    private Robot robot;

    public CometeTestCase() {
        super();
    }

    public CometeTestCase(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        helper = new CometeTestHelper();
        setHelper(helper);
        try {
            robot = new Robot();
        } catch (final AWTException e) {
            robot = null;
            e.printStackTrace();
        }
        Sleeper.SECONDS.sleep(2);
    }

    /**
     * Checks whether an {@link Object} is <code>null</code>
     * 
     * @param obj The {@link Object}
     * @return true if <code>null</code>
     */
    public boolean isNullObject(final Object obj) {
        assertNotNull(obj);
        return (obj == null);
    }

    /**
     * Checks that an {@link Object} is not an instance of a given class
     * 
     * @param obj The {@link Object}
     * @param testClass The class
     * @return true when the object is not an instance of the class
     */
    public boolean isNotInstance(final Object obj, final Class<?> testClass) {
        assertTrue(testClass.isInstance(obj));
        return (!testClass.isInstance(obj));
    }

    /**
     * Checks whether an {@link Object} is not <code>null</code>
     * 
     * @param obj The {@link Object}
     * @return true if not <code>null</code>
     */
    public boolean isNotNullObject(final Object obj) {
        assertNull(obj);
        return (obj != null);
    }

    /**
     * Checks an assertion validity
     * 
     * @param assertion The assertion
     * @return true if assertion is valid
     */
    public boolean isVerified(final boolean assertion) {
        assertTrue(assertion);
        return true;
    }

    /**
     * Mouse click on a {@link Component}
     * 
     * @param component The {@link Component}
     */
    public void click(final Component component) {
        component.requestFocus();
        helper.enterClickAndLeave(new MouseEventData(this, component));
        Sleeper.MILLISECONDS.sleep(100);
    }

    public void click(final Component component, final int x, final int y) {
        component.requestFocus();
        helper.enterClickAndLeave(new MouseEventData(this, component), x, y);
        Sleeper.MILLISECONDS.sleep(100);
    }

    public void pressEnterKey(final Component textComponent) {
        pressKey(textComponent, KeyEvent.VK_ENTER);
    }

    public void pressKey(final Component textComponent, final int keyEventCode) {
        textComponent.requestFocus();
        helper.sendKeyAction(new KeyEventData(this, textComponent, keyEventCode));
        Sleeper.MILLISECONDS.sleep(100);
    }

    /**
     * text input
     * 
     * @param textComponent The text field
     * @param text The text to write
     */
    public void enterText(final Component textComponent, final String text) {
        click(textComponent);
        if (textComponent instanceof JComboBox) {
            click(textComponent);
        }

        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    if (textComponent instanceof JTextComponent) {
                        ((JTextComponent) textComponent).setText(text);
                    } else if (textComponent instanceof JLabel) {
                        ((JLabel) textComponent).setText(text);
                    } else if (textComponent instanceof JComboBox<?>) {
                        ((JComboBox<?>) textComponent).setSelectedItem(text);
                    }
                }
            });
        } catch (final InterruptedException e) {
            e.printStackTrace();
        } catch (final InvocationTargetException e) {
            e.printStackTrace();
        }
        Sleeper.MILLISECONDS.sleep(400);
    }

    public void enterIndexForCombo(final JComboBox<?> jcb, final int index) {
        if (index != jcb.getSelectedIndex()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    jcb.setSelectedIndex(index);

                    final ActionEvent e = new ActionEvent(jcb, 0, "comboBoxChanged", InputEvent.BUTTON1_MASK);
                    jcb.actionPerformed(e);
                }
            });
            Sleeper.MILLISECONDS.sleep(400);
        }
    }

    /**
     * Selects a tab by its title
     * 
     * @param title The tab title
     * @return Whether the tab was selected
     */
    public boolean selectTitledTab(final String title) {
        boolean result = false;
        final CometeComponentFinder tabbedPaneFinder = new CometeComponentFinder(JTabbedPane.class);
        for (final Component component : tabbedPaneFinder.findAll()) {
            if (result) {
                break;
            }
            final JTabbedPane tabbedPane = (JTabbedPane) component;
            for (int tab = 0; tab < tabbedPane.getTabCount(); tab++) {
                if (!title.equals(tabbedPane.getTitleAt(tab))) {
                    continue;
                }

                tabbedPane.setSelectedIndex(tab);
                Sleeper.SECONDS.sleep(1);
                result = true;
                break;
            }
        }
        assertTrue(result);
        return result;
    }

    /**
     * Selects a tab by its tooltip text
     * 
     * @param tipText The tab tooltip text
     * @return Whether the tab was selected
     */
    public boolean selectTippedTab(final String tipText) {
        boolean result = false;
        final CometeComponentFinder tabbedPaneFinder = new CometeComponentFinder(JTabbedPane.class);
        for (final Component component : tabbedPaneFinder.findAll()) {
            if (result) {
                break;
            }
            final JTabbedPane tabbedPane = (JTabbedPane) component;
            for (int tab = 0; tab < tabbedPane.getTabCount(); tab++) {
                if (!tipText.equals(tabbedPane.getToolTipTextAt(tab))) {
                    continue;
                }

                tabbedPane.setSelectedIndex(tab);
                result = true;
                break;
            }
        }
        assertTrue(result);
        return result;
    }

    /**
     * Looks for {@link Component}s of a given class contained in a a parent {@link Component}
     * 
     * @param parentComponent The parent {@link Component}
     * @param componentClass The component class
     * @param componentCount Expected {@link Component} count (-1 when unknown)
     * @return The {@link Component} list, or <code>null</code> when not found or incorrect count
     */
    private Component[] findAndSort(final Component parentComponent, final Class<?> componentClass,
            final int componentCount) {
        Component[] componentList = new ComponentFinderSorter(componentClass).findAndSort(parentComponent);
        if ((componentList != null) && (componentCount != -1) && (componentList.length != componentCount)) {
            componentList = null;
        }
        return componentList;
    }

    /**
     * Clicks on a button identified by its text
     * 
     * @param label The button text
     * @return whether the button was clicked
     */
    public boolean clickLabeledButton(final String label, final Component parentComponent) {
        boolean result = false;
        final Component[] buttonList = findAndSort(parentComponent, JButton.class, -1);
        if (!isNullObject(buttonList)) {
            for (final Component component : buttonList) {
                final JButton button = (JButton) component;
                if ((label != null) && (label.equals(button.getText()))) {
                    click(button);
                    result = true;
                    break;
                }
            }
        }
        assertTrue(result);
        return result;
    }

    /**
     * Looks for a {@link JButton} identified by its tooltip text
     * 
     * @param container The parent container in which to search the button
     * @param tipText The button tooltip text
     * @return The {@link JButton}
     */
    public JButton findButtonByTip(final Container container, final String tipText) {
        JButton result = null;
        final CometeComponentFinder buttonFinder = new CometeComponentFinder(JButton.class);
        final List<Component> components = ((container == null) ? buttonFinder.findAll()
                : buttonFinder.findAll(container));
        for (final Component component : components) {
            final JButton button = (JButton) component;
            if (tipText.equals(button.getToolTipText())) {
                result = button;
                break;
            }
        }
        return result;
    }

    /**
     * Looks for a {@link JButton} identified by its text
     * 
     * @param container The parent container in which to search the button
     * @param text The button text
     * @return The {@link JButton}
     */
    public JButton findButtonByText(final Container container, final String text) {
        JButton result = null;
        final CometeComponentFinder buttonFinder = new CometeComponentFinder(JButton.class);
        final List<Component> components = ((container == null) ? buttonFinder.findAll()
                : buttonFinder.findAll(container));
        for (final Component component : components) {
            final JButton button = (JButton) component;
            if (text.equalsIgnoreCase(button.getText())) {
                result = button;
                break;
            }
        }
        return result;
    }

    /**
     * Looks for a {@link JDialog} identified by its title
     * 
     * @param title The {@link JDialog} title
     * @return The {@link JDialog}
     */
    public JDialog findDialog(final String title) {
        JDialog result = null;
        final DialogFinder dialogFinder = new DialogFinder(title);
        final List<?> list = dialogFinder.findAll();
        for (final Object o : list) {
            if (o instanceof JDialog) {
                JDialog dlg = (JDialog) o;
                if (dlg.getTitle().equalsIgnoreCase(title)) {
                    result = dlg;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Looks for a {@link Component} of a given class in another {@link Component}
     * 
     * @param parentComponent The parent {@link Component} in which to search
     * @return A {@link Component}
     */
    public Component findFirstComponent(final Class<?> componentClass, final Component parentComponent) {
        return findComponent(componentClass, parentComponent, -1);
    }

    /**
     * Looks for a {@link Component} of a given class in another {@link Component}
     * 
     * @param parentComponent The parent {@link Component} in which to search
     * @param index The component index
     * @return A {@link Component}
     */
    public Component findComponent(final Class<?> componentClass, final Component parentComponent, final int index) {
        Component component = null;
        final Component[] componentList = findAndSort(parentComponent, componentClass, -1);
        if (componentList != null) {
            int i = index;
            if (i < 0) {
                i = 0;
            }
            if (i < componentList.length) {
                component = componentList[i];
            }
        }
        return component;
    }

    /**
     * Looks for a {@link JMenu} identified by its text in a {@link JMenuBar}
     * 
     * @param menuBar The {@link JMenuBar}
     * @param label The text
     * @return A {@link JMenu}
     */
    public JMenu getMenuFromMenuBar(final JMenuBar menuBar, final String label) {
        JMenu result = null;
        for (int pos = 0; pos < menuBar.getMenuCount(); pos++) {
            final JMenu menu = menuBar.getMenu(pos);
            if ((menu != null) && label.equals(menu.getText())) {
                result = menu;
                break;
            }
        }
        return result;
    }

    /**
     * Looks for a {@link JMenuItem} identified by its text in a {@link JMenu}
     * 
     * @param menu The {@link JMenu}
     * @param label The text
     * @return A {@link JMenuItem}
     */
    public JMenuItem getMenuItemFromMenu(final JMenu menu, final String label) {
        JMenuItem result = null;
        for (int pos = 0; pos < menu.getItemCount(); pos++) {
            final JMenuItem menuItem = menu.getItem(pos);
            if ((menuItem != null) && label.equals(menuItem.getText())) {
                result = menuItem;
                break;
            }
        }
        return result;
    }

    /**
     * Deletes a directory
     * 
     * @param directory The directory path
     */
    public void deleteDirectory(final File directory) {
        if (directory.isDirectory()) {
            final File[] files = directory.listFiles();
            for (final File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                } else {
                    file.delete();
                }
            }
            directory.delete();
        }
    }

    /**
     * Clicks on "OK" button in a {@link JDialog}
     * 
     * @param dlg The {@link JDialog}
     * 
     */
    public void validDialog(final JDialog dlg) {
        JButton btn = findButtonByText(dlg, "OK");
        if (btn == null) {
            btn = findButtonByText(dlg, "oui");
        }
        if (btn == null) {
            btn = findButtonByText(dlg, "yes");
        }
        if (btn == null) {
            btn = findButtonByText(dlg, "ok");
        }
        click(btn);
        Sleeper.SECONDS.sleep(1);
    }

    /**
     * Clicks on "NON" button in a {@link JDialog}
     * 
     * @param dlg The {@link JDialog}
     */
    public void rejectDialog(final JDialog dlg) {
        JButton btn = findButtonByText(dlg, "non");
        if (btn == null) {
            btn = findButtonByText(dlg, "no");
        }
        click(btn);
        Sleeper.SECONDS.sleep(1);
    }

    /**
     * Clicks on "CANCEL" button in a {@link JDialog}
     * 
     * @param dlg The {@link JDialog}
     * 
     */
    public void cancelDialog(final JDialog dlg) {
        JButton btn = findButtonByText(dlg, "annuler");
        if (btn == null) {
            btn = findButtonByText(dlg, "cancel");
        }
        click(btn);
        Sleeper.SECONDS.sleep(1);
    }

    /**
     * Waits for the text display of a {@link JTextComponent} until time out
     * 
     * @param jtc The {@link JTextComponent}
     * @param text The text to wait for
     * @param timeout The maximum time (in milliseconds) to wait
     */
    public boolean waitText(final JTextComponent jtc, final String text, final long timeout) {
        final JTextComponentSpy jtcSpy = new JTextComponentSpy(jtc);
        final boolean result = jtcSpy.isLaterDisplaying(text, timeout);
        jtcSpy.close();
        return result;
    }

    /**
     * Puts the mouse at the center of a {@link Component}
     * 
     * @param component The {@link Component}
     */
    private void mouseMoveOnComponent(final Component component) {
        final Point pos = component.getLocationOnScreen();
        final int xCenter = pos.x + component.getWidth() / 2;
        final int yCenter = pos.y + component.getHeight() / 2;
        robot.mouseMove(xCenter, yCenter);
    }

    /**
     * Clicks at the center of a {@link Component}
     * 
     * @param component The {@link Component}
     */
    public void mouseClickOnComponent(final Component component) {
        mouseMoveOnComponent(component);
        robot.mousePress(InputEvent.BUTTON1_MASK);
        Sleeper.MILLISECONDS.sleep(200);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    /**
     * Input key simulation
     * 
     * @param character The character key
     */
    public void simulateInput(final int character) {
        int keyCode = 0;
        switch (character) {
            case '0':
                keyCode = KeyEvent.VK_NUMPAD0;
                break;
            case '1':
                keyCode = KeyEvent.VK_NUMPAD1;
                break;
            case '2':
                keyCode = KeyEvent.VK_NUMPAD2;
                break;
            case '3':
                keyCode = KeyEvent.VK_NUMPAD3;
                break;
            case '4':
                keyCode = KeyEvent.VK_NUMPAD4;
                break;
            case '5':
                keyCode = KeyEvent.VK_NUMPAD5;
                break;
            case '6':
                keyCode = KeyEvent.VK_NUMPAD6;
                break;
            case '7':
                keyCode = KeyEvent.VK_NUMPAD7;
                break;
            case '8':
                keyCode = KeyEvent.VK_NUMPAD8;
                break;
            case '9':
                keyCode = KeyEvent.VK_NUMPAD9;
                break;
            case '.':
                keyCode = KeyEvent.VK_DECIMAL;
                break;
            default:
                Assert.assertTrue(false);
        }
        enterKey(keyCode);
    }

    /**
     * [Enter] Key simulation
     */
    public void simulateEnter() {
        enterKey(KeyEvent.VK_ENTER);
    }

    /**
     * Keyboard key pressed simulation
     * 
     * @param keyCode The key code
     */
    private void enterKey(final int keyCode) {
        assertTrue(robot != null);
        robot.keyPress(keyCode);
        Sleeper.MILLISECONDS.sleep(100);
        robot.keyRelease(keyCode);
        Sleeper.MILLISECONDS.sleep(100);
    }

    /**
     * send a KeyEvent per character in sequence to the listener. The function
     * blocks the thread until action trigered by event is completed.<br/>
     * <br/>
     * 
     * <b>note:</b> event are raised with modifier =0. ie without mask (ctr +
     * sequence, alt+ sequence ,...)
     * 
     * @param listener
     *            target of events
     * @param source
     *            the component which raise the event
     * @param sequence
     *            char sequence of event to raise. Sequence can only containt
     *            number and dot
     */
    public void sendEventsToKeyListener(final KeyListener listener, final Component source, final String sequence) {

        for (int i = 0; i < sequence.length(); i++) {
            final char charCode = sequence.charAt(i);
            int keyCode = 0;

            switch (charCode) {
                case '0':
                    keyCode = KeyEvent.VK_NUMPAD0;
                    break;
                case '1':
                    keyCode = KeyEvent.VK_NUMPAD1;
                    break;
                case '2':
                    keyCode = KeyEvent.VK_NUMPAD2;
                    break;
                case '3':
                    keyCode = KeyEvent.VK_NUMPAD3;
                    break;
                case '4':
                    keyCode = KeyEvent.VK_NUMPAD4;
                    break;
                case '5':
                    keyCode = KeyEvent.VK_NUMPAD5;
                    break;
                case '6':
                    keyCode = KeyEvent.VK_NUMPAD6;
                    break;
                case '7':
                    keyCode = KeyEvent.VK_NUMPAD7;
                    break;
                case '8':
                    keyCode = KeyEvent.VK_NUMPAD8;
                    break;
                case '9':
                    keyCode = KeyEvent.VK_NUMPAD9;
                    break;
                case '.':
                    keyCode = KeyEvent.VK_DECIMAL;
                    break;
                default:
                    fail("charCode '" + charCode + "' is not manage");
            }

            listener.keyPressed(
                    new KeyEvent(source, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, keyCode, charCode));
        }
    }

    /**
     * send Enter press keyEvent to a Listener. If the execution of Event
     * triggered a blocking action, like confirmation dialog, you must run it a
     * thread(set inAThread flag to true)
     * 
     * @param listener
     *            target of events
     * @param source
     *            the component which raise the event
     * @param inAThread
     *            flag which indicate if the event is runned in a thread
     */
    public void sendEnterToKeyListener(final KeyListener listener, final Component source, final boolean inAThread) {
        if (inAThread) {
            new Thread() {
                @Override
                public void run() {
                    listener.keyPressed(new KeyEvent(source, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0,
                            KeyEvent.VK_ENTER, '\n'));

                }
            }.start();

        } else {
            listener.keyPressed(
                    new KeyEvent(source, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_ENTER, '\n'));
        }

    }

}
