package fr.soleil.comete.swing.util;

import java.awt.Component;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;

public class ComponentFinderSorter extends CometeComponentFinder {
    public ComponentFinderSorter(final Class<?> cls) {
        super(cls);
    }

    /**
     * Searches and sorts by ascending coordinates {@link Component}s of given class contained in a given
     * {@link Component}
     * 
     * @param parentComponent The parent {@link Component} in which to search for {@link Component}s
     * 
     * @return A {@link Component} array. <code>null</code> when nothing was found.
     */

    public Component[] findAndSort(final Component parentComponent) {
        Component[] componentList = null;
        final ArrayList<SortableComponent> componentSortList = new ArrayList<SortableComponent>();
        for (final Component component : findAll()) {
            if (!component.isVisible()) {
                continue;
            }

            if (areParentAndChild(parentComponent, component)) {
                componentSortList.add(new SortableComponent(component));
            }
        }

        final int size = componentSortList.size();
        if (size >= 0) {
            if (size > 1) {
                Collections.sort(componentSortList);
            }
            componentList = new Component[size];
            int index = 0;
            for (SortableComponent component : componentSortList) {
                componentList[index++] = component.getComponent();
            }
        }

        return componentList;
    }

    /**
     * Tests a parental link between 2 {@link Component}s
     * 
     * @param parentComponent The supposed parent
     * @param childComponent The supposed child
     * @return <code>true</code> when a parental link exists
     */
    private boolean areParentAndChild(final Component parentComponent, final Component childComponent) {
        boolean result;
        // null component contains all components
        if (parentComponent == null) {
            result = true;
        } else {
            result = false;
            Component component = childComponent;
            while (component != null) {
                component = component.getParent();
                if (component == parentComponent) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * Class allowing {@link Component}s sorting by ascending ordinate then by ascending abscissa
     * 
     * @author DJ
     */
    private class SortableComponent implements Comparable<SortableComponent> {
        private final Component component;
        private final Point point;

        public SortableComponent(final Component component) {
            this.component = component;
            point = component.getLocationOnScreen();
        }

        public Component getComponent() {
            return component;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        @Override
        public int compareTo(final SortableComponent sortableComponent) {
            if (this.point.y == sortableComponent.point.y) {
                return this.point.x - sortableComponent.point.x;
            } else {
                return this.point.y - sortableComponent.point.y;
            }
        }
    }
}
