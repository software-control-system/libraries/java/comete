/*******************************************************************************
 * Copyright (c) 2008-2023 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.swing;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.junit.Assert;
import org.junit.Test;

import fr.soleil.comete.swing.util.CometeComponentFinder;
import fr.soleil.comete.swing.util.CometeTestCase;

public class CheckBoxTest extends CometeTestCase {

    public static final String TRUE_LABEL = "GOOD";
    public static final String FALSE_LABEL = "BAD";
    public static final ImageIcon OK_ICON = new ImageIcon(
            CheckBoxTest.class.getResource("/fr/soleil/comete/icons/ledGreen.gif"));
    public static final ImageIcon KO_ICON = new ImageIcon(
            CheckBoxTest.class.getResource("/fr/soleil/comete/icons/ledRed.gif"));

    private CheckBoxFrame checkBoxFrame;

    public CheckBoxTest() {
        super(CheckBox.class.getSimpleName());
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        checkBoxFrame = new CheckBoxFrame();
        checkBoxFrame.pack();
        checkBoxFrame.setVisible(true);
    }

    @Override
    protected void tearDown() throws Exception {
        if (checkBoxFrame != null) {
            checkBoxFrame.setVisible(false);
            checkBoxFrame = null;
        }
        super.tearDown();
    }

    @Test
    public void testSetEditable() {
        CometeComponentFinder checkBoxFinder = new CometeComponentFinder(CheckBox.class);
        List<Component> checkBoxes = checkBoxFinder.findAll(checkBoxFrame);
        Assert.assertNotNull("CheckBox not found", checkBoxes);
        Assert.assertNotEquals("CheckBox not found", 0, checkBoxes.size());
        CheckBox checkBox = (CheckBox) checkBoxes.get(0);
        CometeComponentFinder labelFinder = new CometeComponentFinder(JLabel.class);
        List<Component> labels = labelFinder.findAll(checkBoxFrame);
        Assert.assertNotNull("Labels not found", labels);
        JLabel valueLabel = null, editableLabel = null;
        for (Component comp : labels) {
            if ("valueLabel".equals(comp.getName())) {
                valueLabel = (JLabel) comp;
            } else if ("editableLabel".equals(comp.getName())) {
                editableLabel = (JLabel) comp;
            }
        }
        Assert.assertNotNull("valueLabel not found", valueLabel);
        Assert.assertNotNull("editableLabel not found", editableLabel);
        Assert.assertEquals("Checkbox should start as not selected", FALSE_LABEL, checkBox.getText());
        click(checkBox);
        Assert.assertEquals("Checkbox, once clicked, should be selected and display 'true' label", TRUE_LABEL,
                checkBox.getText());
        click(editableLabel);
        click(checkBox);
        Assert.assertEquals("Checkbox, once not editable, can't be selected/deselected through user click", TRUE_LABEL,
                checkBox.getText());
        click(valueLabel);
        Assert.assertEquals("Checkbox can have its selection changed through programmation even if not editable",
                FALSE_LABEL, checkBox.getText());
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link JFrame} used to test {@link CheckBox} class
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class CheckBoxFrame extends JFrame {
        private static final long serialVersionUID = -7205697104750354495L;

        private final JLabel valueLabel;
        private final JLabel editableLabel;
        private final CheckBox checkBox;

        /**
         * Constructs a new {@link CheckBoxFrame}
         */
        public CheckBoxFrame() {
            super("CheckBox test");
            checkBox = new CheckBox();
            checkBox.setTrueLabel(TRUE_LABEL);
            checkBox.setFalseLabel(FALSE_LABEL);
            valueLabel = new JLabel("Click to change value");
            valueLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    checkBox.setSelected(!checkBox.isSelected());
                }
            });
            valueLabel.setName("valueLabel");
            editableLabel = new JLabel("Click to switch editable/not editable");
            editableLabel.setName("editableLabel");
            editableLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    checkBox.setEditable(!checkBox.isEditable());
                    editableLabel.setIcon(checkBox.isEditable() ? OK_ICON : KO_ICON);
                }
            });
            editableLabel.setIcon(checkBox.isEditable() ? OK_ICON : KO_ICON);
            JPanel testPanel = new JPanel(new BorderLayout());
            testPanel.add(editableLabel, BorderLayout.NORTH);
            testPanel.add(checkBox, BorderLayout.CENTER);
            testPanel.add(valueLabel, BorderLayout.SOUTH);
            setContentPane(testPanel);
        }
    }

}
