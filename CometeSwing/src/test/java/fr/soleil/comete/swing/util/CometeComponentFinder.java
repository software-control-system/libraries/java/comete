package fr.soleil.comete.swing.util;

import java.awt.Component;
import java.awt.Container;
import java.util.ArrayList;
import java.util.List;

import junit.extensions.jfcunit.WindowMonitor;
import junit.extensions.jfcunit.finder.ComponentFinder;

public class CometeComponentFinder extends ComponentFinder {

    public CometeComponentFinder(Class<?> cls) {
        super(cls);
    }

    /* (non-Javadoc)
     * @see junit.extensions.jfcunit.finder.Finder#findAll()
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Component> findAll() {
        List<Component> componentList = super.findAll();
        if (!componentList.isEmpty() && componentList.get(0) instanceof Container) {
            ArrayList<Component> completeList = new ArrayList<Component>(componentList);
            for (Component component : componentList)
                completeList.addAll(findAll((Container) component));
            return completeList;
        }
        return componentList;
    }

    /* (non-Javadoc)
     * @see junit.extensions.jfcunit.finder.Finder#findAll(java.awt.Container)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Component> findAll(Container container) {
        List<Component> componentList = super.findAll(container);
        ArrayList<Component> completeList = new ArrayList<Component>(componentList);
        for (Component component : componentList) {
            if (component instanceof Container)
                completeList.addAll(findAll((Container) component));
        }
        return completeList;
    }

    @Override
    protected Component find(Container[] conts, int index) {
        Component result = null;
        Container[] search = null;
        int idx = 0;

        if (conts == null)
            search = WindowMonitor.getWindows();
        else
            search = conts;

        for (int window = 0; window < search.length; window++) {
            List<?> comps = findComponentList(this, search[window], new ArrayList<Component>(), index - idx);
            int size = comps.size();

            if ((size > 0) && (size > (index - idx))) {
                Component comp = (Component) comps.get(index - idx);
                result = comp;
                break;
            }

            idx += comps.size();
        }
        return result;
    }
}
