package fr.soleil.comete.swing.util;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

/**
 * @author DJ
 * espion de l'affichage d'un JTextComponent
 */
public class JTextComponentSpy implements DocumentListener
{
	JTextComponent jtc;

	public JTextComponentSpy(JTextComponent jtc)
	{
		this.jtc = jtc;
		jtc.getDocument().addDocumentListener(this);
	}

	/**
	 * doit �tre appel� lorsque l'objet n'est plus utilis�
	 */
	public void close()
	{
		jtc.getDocument().removeDocumentListener(this);
	}

	/**
	 * Test si un message donn� est affich� avant l'issue du timeout
	 * @param text
	 * @param timeout
	 * @return
	 */
	public synchronized boolean isLaterDisplaying(String text,long timeout)
	{
		if(jtc.getText().trim().equals(text))
			return true;

		try
		{
			wait(timeout);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		return jtc.getText().trim().equals(text);
	}

	@Override
	public synchronized void changedUpdate(DocumentEvent e)
	{
		notify();
	}

	@Override
	public synchronized void insertUpdate(DocumentEvent e)
	{
		notify();
	}

	@Override
	public synchronized void removeUpdate(DocumentEvent e)
	{
		notify();
	}
}
