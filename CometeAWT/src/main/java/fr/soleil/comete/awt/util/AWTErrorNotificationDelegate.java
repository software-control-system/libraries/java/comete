/*
 * This file is part of CometeAWT.
 * 
 * CometeAWT is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeAWT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeAWT. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.awt.util;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.WindowUtils;

public class AWTErrorNotificationDelegate {

    private static final Font LABEL_FONT = new Font(Font.DIALOG, Font.PLAIN, 12);
    private static final Font AREA_FONT = new Font(Font.DIALOG, Font.ITALIC, 11);
    private static final String TITLE = "Error!";
    private static final String OK = "OK";
    private static final int MAX_WIDTH = 750, MAX_HEIGHT = 350;

    private Component source;
    private Dialog messageDialog;
    private final Label messageLabel;
    private final TextArea stackTraceArea;
    private final Object messageLock;

    public AWTErrorNotificationDelegate(Component source) {
        this.source = source;
        messageLabel = new Label();
        messageLabel.setForeground(Color.RED);
        messageLabel.setFont(LABEL_FONT);
        stackTraceArea = new TextArea(10, 50);
        stackTraceArea.setFont(AREA_FONT);
        stackTraceArea.setEditable(false);
        messageLock = new Object();
    }

    protected void hideMessageDialog() {
        messageDialog.setVisible(false);
        messageLabel.setText(ObjectUtils.EMPTY_STRING);
        stackTraceArea.setText(ObjectUtils.EMPTY_STRING);
    }

    public void notifyForError(final String message, final Throwable error) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (messageDialog == null) {
                    synchronized (messageLock) {
                        if (messageDialog == null) {
                            messageDialog = new Dialog(WindowUtils.getWindowForComponent(source), TITLE,
                                    Dialog.DEFAULT_MODALITY_TYPE);
                            messageDialog.addWindowListener(new WindowAdapter() {
                                @Override
                                public void windowClosing(java.awt.event.WindowEvent e) {
                                    hideMessageDialog();
                                }
                            });
                            Panel mainPanel = new Panel(new GridBagLayout());
                            Button okButton = new Button(OK);
                            okButton.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    hideMessageDialog();
                                }
                            });
                            GridBagConstraints messageLabelConstraints = new GridBagConstraints();
                            messageLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
                            messageLabelConstraints.gridx = 0;
                            messageLabelConstraints.gridy = 0;
                            messageLabelConstraints.weightx = 1;
                            messageLabelConstraints.weighty = 0;
                            messageLabelConstraints.insets = new Insets(10, 5, 10, 5);
                            messageLabelConstraints.anchor = GridBagConstraints.LINE_START;
                            mainPanel.add(messageLabel, messageLabelConstraints);
                            GridBagConstraints stackTraceAreaConstraints = new GridBagConstraints();
                            stackTraceAreaConstraints.fill = GridBagConstraints.BOTH;
                            stackTraceAreaConstraints.gridx = 0;
                            stackTraceAreaConstraints.gridy = 1;
                            stackTraceAreaConstraints.weightx = 1;
                            stackTraceAreaConstraints.weighty = 1;
                            mainPanel.add(stackTraceArea, stackTraceAreaConstraints);
                            GridBagConstraints okButtonConstraints = new GridBagConstraints();
                            okButtonConstraints.fill = GridBagConstraints.NONE;
                            okButtonConstraints.gridx = 0;
                            okButtonConstraints.gridy = 2;
                            okButtonConstraints.weightx = 1;
                            okButtonConstraints.weighty = 0;
                            okButtonConstraints.insets = new Insets(5, 5, 5, 5);
                            okButtonConstraints.anchor = GridBagConstraints.CENTER;
                            mainPanel.add(okButton, okButtonConstraints);

                            messageDialog.add(mainPanel);
                        }
                    }
                } // end if (messageDialog == null)
                messageLabel.setText(message == null ? ObjectUtils.EMPTY_STRING : message);
                String stackTrace = (error == null ? ObjectUtils.EMPTY_STRING : ObjectUtils.printStackTrace(error));
                stackTraceArea.setText(stackTrace);
                stackTraceArea.invalidate();
                stackTraceArea.validate();
                if (!stackTrace.isEmpty()) {
                    stackTraceArea.setCaretPosition(0);
                }
                messageDialog.pack();
                int width = messageDialog.getWidth(), height = messageDialog.getHeight();
                boolean changed = false;
                if (width > MAX_WIDTH) {
                    Dimension prefSize = messageLabel.getPreferredSize();
                    width = Math.min(width, Math.max(prefSize.width + 15, MAX_WIDTH));
                    changed = (width != messageDialog.getWidth());
                }
                if (height > MAX_HEIGHT) {
                    height = MAX_HEIGHT;
                    changed = true;
                }
                if (changed) {
                    messageDialog.setSize(width, height);
                }
                messageDialog.setLocationRelativeTo(source);
                messageDialog.setVisible(true);
            }
        };
        if (EventQueue.isDispatchThread()) {
            runnable.run();
        } else {
            EventQueue.invokeLater(runnable);
        }
    }

}
