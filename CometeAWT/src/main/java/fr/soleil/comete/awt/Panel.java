/*
 * This file is part of CometeAWT.
 * 
 * CometeAWT is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeAWT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeAWT. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.awt;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;

import fr.soleil.comete.awt.util.AWTDrawingThreadManager;
import fr.soleil.comete.awt.util.AWTErrorNotificationDelegate;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;

public class Panel extends java.awt.Panel implements IPanel {

    private static final long serialVersionUID = -7862982617256285992L;

    private final TargetDelegate delegate;
    private final AWTErrorNotificationDelegate errorNotificationDelegate;

    public Panel() {
        super();
        delegate = new TargetDelegate();
        errorNotificationDelegate = new AWTErrorNotificationDelegate(this);
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void add(IComponent component) {
        if (component instanceof Component) {
            add(((Component) component));
        }
    }

    @Override
    public void addCenter(IComponent component) {
        if (component instanceof Component) {
            add(((Component) component), BorderLayout.CENTER);
        }

    }

    @Override
    public void add(IComponent component, Object constraints) {
        if (constraints != null && (component instanceof Component)) {
            LayoutManager layout = getLayout();
            if (layout == null || !(layout instanceof BorderLayout)) {
                setLayout(new BorderLayout());
            }
            add(((Component) component), constraints);
        }
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(CometeFont font) {
        super.setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        super.setForeground(ColorTool.getColor(color));
    }

    @Override
    public void setLayout(Object layout) {
        if (layout != null && layout instanceof LayoutManager) {
            super.setLayout((LayoutManager) layout);
        }
    }

    @Override
    public int getHorizontalAlignment() {
        return 0;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
    }

    @Override
    public String getToolTipText() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setOpaque(boolean opaque) {
    }

    @Override
    public void setToolTipText(String text) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setTitledBorder(String title) {
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to
     *            {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return AWTDrawingThreadManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
