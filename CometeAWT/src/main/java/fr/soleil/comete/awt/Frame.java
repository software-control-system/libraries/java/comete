/*
 * This file is part of CometeAWT.
 * 
 * CometeAWT is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeAWT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeAWT. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.awt;

import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import fr.soleil.comete.awt.util.AWTDrawingThreadManager;
import fr.soleil.comete.awt.util.AWTErrorNotificationDelegate;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;

public class Frame extends java.awt.Frame implements IFrame, WindowListener {

    private static final long serialVersionUID = -6485622320576691393L;

    private boolean closeFrame;
    private final TargetDelegate delegate;
    private final AWTErrorNotificationDelegate errorNotificationDelegate;

    public Frame() {
        super();
        closeFrame = false;
        delegate = new TargetDelegate();
        errorNotificationDelegate = new AWTErrorNotificationDelegate(this);
        addWindowListener(this);
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void setContentPane(Object panel) {
        if (panel != null && panel instanceof Panel) {
            super.add((Panel) panel);
        }
        super.pack();
    }

    @Override
    public void setDefaultCloseOperation(int operation) {
        if (operation == IFrame.EXIT_ON_CLOSE) {
            closeFrame = true;
        }
    }

    @Override
    public CometeColor getCometeBackground() {
        // not managed
        return null;
    }

    @Override
    public CometeFont getCometeFont() {
        // not managed
        return null;
    }

    @Override
    public CometeColor getCometeForeground() {
        // not managed
        return null;
    }

    @Override
    public int getHorizontalAlignment() {
        // not managed
        return 0;
    }

    @Override
    public String getToolTipText() {
        // not managed
        return null;
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        // not managed
    }

    @Override
    public void setCometeFont(CometeFont font) {
        // not managed
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        // not managed
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // not managed
    }

    @Override
    public void setOpaque(boolean opaque) {
        // not managed
    }

    @Override
    public void setToolTipText(String text) {
        // not managed
    }

    @Override
    public void windowActivated(WindowEvent e) {
        // not managed
    }

    @Override
    public void windowClosed(WindowEvent e) {
        // not managed
    }

    @Override
    public void windowClosing(WindowEvent e) {
        if (closeFrame) {
            System.exit(0);
        } else {
            this.setVisible(false);
        }
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        // not managed
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        // not managed
    }

    @Override
    public void windowIconified(WindowEvent e) {
        // not managed
    }

    @Override
    public void windowOpened(WindowEvent e) {
        // not managed
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to
     *            {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return AWTDrawingThreadManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
