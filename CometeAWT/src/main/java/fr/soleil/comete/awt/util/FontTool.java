/*
 * This file is part of CometeAWT.
 * 
 * CometeAWT is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeAWT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeAWT. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.awt.util;

import java.awt.Font;

import fr.soleil.comete.definition.widget.util.CometeFont;

/**
 * Common Font utilities
 * 
 * @author saintin
 */
public class FontTool {

    public static Font getFont(CometeFont cometeFont) {
        if (cometeFont == null) {
            return null;
        }
        return getFont(cometeFont.getName(), cometeFont.getSize(), cometeFont.getStyle());
    }

    public static Font getFont(String name, int size, int style) {
        return new Font(name, style, size);
    }

    public static CometeFont getCometeFont(Font srcFont) {
        if (srcFont == null) {
            return null;
        }
        return new CometeFont(srcFont.getName(), srcFont.getStyle(), srcFont.getSize());
    }

}
