/*
 * This file is part of CometeAWT.
 * 
 * CometeAWT is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeAWT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeAWT. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.awt;

import java.awt.Choice;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.LinkedHashSet;

import fr.soleil.comete.awt.util.AWTDrawingThreadManager;
import fr.soleil.comete.awt.util.AWTErrorNotificationDelegate;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.IComboBoxListener;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IComboBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;

public class ComboBox extends Choice implements IComboBox, ItemListener {

    private static final long serialVersionUID = -352044818347082244L;

    private final Collection<IComboBoxListener> comboBoxListeners;
    private String[] optionList;
    private Object[] valueList;
    private final TargetDelegate delegate;
    private final AWTErrorNotificationDelegate errorNotificationDelegate;

    public ComboBox() {
        super();
        comboBoxListeners = new LinkedHashSet<>();
        optionList = new String[0];
        valueList = new Object[0];
        delegate = new TargetDelegate();
        errorNotificationDelegate = new AWTErrorNotificationDelegate(this);
    }

    @Override
    public void addComboBoxListener(final IComboBoxListener listener) {
        synchronized (comboBoxListeners) {
            comboBoxListeners.add(listener);
        }
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void removeComboBoxListener(final IComboBoxListener listener) {
        synchronized (comboBoxListeners) {
            comboBoxListeners.remove(listener);
        }
    }

    @Override
    public void fireSelectedItemChanged(final EventObject event) {
        Collection<IComboBoxListener> listeners;
        synchronized (comboBoxListeners) {
            listeners = new ArrayList<>(comboBoxListeners);
        }
        try {
            for (IComboBoxListener listener : listeners) {
                listener.selectedItemChanged(event);
            }
        } finally {
            listeners.clear();
        }
    }

    @Override
    public void setSelectedIndex(int index) {
        super.select(index);
    }

    @Override
    public void setSelectedValue(Object value) {
        int index = getIndexItemForValue(value);
        int itemCount = getItemCount();
        // System.out.println("index found=" + index);
        // System.out.println("itemCount=" + itemCount);
        // System.out.println("valueList=" + valueList.length);
        if (itemCount > 0 && index > -1 && index < itemCount) {
            setSelectedIndex(index);
        }
    }

    @Override
    public String[] getDisplayedList() {
        return optionList;
    }

    @Override
    public Object[] getValueList() {
        return valueList;
    }

    @Override
    public void setValueList(final Object... valueList) {
        this.valueList = valueList;
        if (valueList != null && valueList.length > 0) {
            if (optionList == null || optionList.length == 0) {
                String[] displayedList = new String[valueList.length];
                for (int i = 0; i < valueList.length; i++) {
                    displayedList[i] = (valueList[i] == null ? null : valueList[i].toString());
                }
                setDisplayedList(displayedList);
            }
        }
    }

    @Override
    public String[] getStringArray() {
        return getDisplayedList();
    }

    @Override
    public void setStringArray(String[] value) {
        setDisplayedList(value);
    }

    @Override
    public Object[] getObjectArray() {
        return getValueList();
    }

    @Override
    public void setObjectArray(Object[] value) {
        setValueList(value);
    }

    @Override
    public void setDisplayedList(final String... optionList) {
        this.optionList = optionList;
        removeAll();

        if (optionList != null && optionList.length > 0) {
            for (String element : optionList) {
                addItem(element);
            }
        }

    }

    @Override
    public String getSelectedItem() {
        return super.getSelectedItem().toString();
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(CometeFont font) {
        super.setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        super.setForeground(ColorTool.getColor(color));
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        fireSelectedItemChanged(new EventObject(this));
    }

    @Override
    public String getToolTipText() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setOpaque(boolean opaque) {
        // TODO Auto-generated method stub
    }

    @Override
    public void setToolTipText(String text) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getHorizontalAlignment() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isEditable() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEditable(boolean b) {
        // TODO Auto-generated method stub

    }

    public int getIndexItemForValue(Object value) {
        int index = -1;
        if ((valueList != null) && (valueList.length > 0)) {
            for (int i = 0; (i < valueList.length) && (index == -1); i++) {
                if (ObjectUtils.sameObject(valueList[i], value)) {
                    index = i;
                }
            }
        }
        return index;
    }

    public String getDisplayedValueForIndex(int index) {
        String value = null;
        if ((optionList != null) && (optionList.length > 0) && (index > -1) && (index < optionList.length)) {
            value = optionList[index];
        }
        return value;
    }

    public Object getValueForIndex(int index) {
        Object value = null;
        if ((valueList != null) && (valueList.length > 0) && (index > -1) && (index < valueList.length)) {
            value = valueList[index];
        }
        return value;
    }

    public String getOptionForIndex(int index) {
        if (optionList == null || optionList.length == 0 || index < 0 || index >= optionList.length) {
            return null;
        }

        return optionList[index];
    }

    @Override
    public Object getSelectedValue() {
        int index = getSelectedIndex();
        return getValueForIndex(index);
    }

    @Override
    public String getText() {
        int index = getSelectedIndex();
        return getDisplayedValueForIndex(index);
    }

    @Override
    public void setText(String text) {
        setSelectedValue(text);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to
     *            {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return AWTDrawingThreadManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

    /**
     * Main class, so you can have an example. You can monitor your own attribute by giving its full
     * path name in argument
     */
    public static void main(String[] args) {
        IPanel panel = new Panel();
        ComboBox component = new ComboBox();
        component.setDisplayedList(new String[] { "option 1", "option 2", "option 3" });
        panel.add(component);
        IFrame frame = new Frame();
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setTitle("ComboBox test");
    }

}
