/*
 * This file is part of CometeAWT.
 * 
 * CometeAWT is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeAWT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeAWT. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.awt.util;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.soleil.comete.definition.event.CometeMouseEvent;
import fr.soleil.comete.definition.event.CometeMouseEvent.MouseReason;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IComponent;

/**
 * This class handles Comete mouse event forwarding for {@link IComponent}s that also are
 * {@link Component}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ComponentMouseDelegate implements MouseListener, MouseWheelListener, MouseMotionListener {

    private static final ComponentMouseDelegate instance = new ComponentMouseDelegate();

    private final Map<IComponent, List<WeakReference<IMouseListener>>> listenerMap;

    private ComponentMouseDelegate() {
        listenerMap = new HashMap<IComponent, List<WeakReference<IMouseListener>>>();
    }

    /**
     * Adds an {@link IMouseListener} to an {@link IComponent}, if this {@link IComponent} is a
     * {@link Component}
     * 
     * @param comp The concerned {@link IComponent}
     * @param listener The {@link IMouseListener} to add
     */
    public static void addMouseListenerToComponent(IComponent comp, IMouseListener listener) {
        instance.registerMouseListenerToComponent(comp, listener);
    }

    /**
     * Removes an {@link IMouseListener} from an {@link IComponent}, if this {@link IComponent} is a
     * {@link Component}
     * 
     * @param comp The concerned {@link IComponent}
     * @param listener The {@link IMouseListener} to remove
     */
    public static void removeMouseListenerFromComponent(IComponent comp, IMouseListener listener) {
        instance.unregisterMouseListenerFromComponent(comp, listener);
    }

    /**
     * Removes all {@link IMouseListener}s from an {@link IComponent}, if this {@link IComponent} is
     * a {@link Component}
     * 
     * @param comp The concerned {@link IComponent}
     */
    public static void removeAllMouseListenersFromComponent(IComponent comp) {
        instance.unregisterAllMouseListenersFromComponent(comp);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if ((e != null) && (e.getSource() instanceof IComponent)) {
            IComponent comp = (IComponent) e.getSource();
            warnMouseListeners(comp,
                    new CometeMouseEvent(comp, MouseReason.DRAGGED, e.getX(), e.getY(), e.getButton(),
                            e.getClickCount(), 0, e.getModifiersEx(), e.getWhen(), e.isAltDown(), e.isAltGraphDown(),
                            e.isControlDown(), e.isShiftDown(), e.isMetaDown()));
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if ((e != null) && (e.getSource() instanceof IComponent)) {
            IComponent comp = (IComponent) e.getSource();
            warnMouseListeners(comp,
                    new CometeMouseEvent(comp, MouseReason.MOVED, e.getX(), e.getY(), e.getButton(), e.getClickCount(),
                            0, e.getModifiersEx(), e.getWhen(), e.isAltDown(), e.isAltGraphDown(), e.isControlDown(),
                            e.isShiftDown(), e.isMetaDown()));
        }
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if ((e != null) && (e.getSource() instanceof IComponent)) {
            IComponent comp = (IComponent) e.getSource();
            warnMouseListeners(comp,
                    new CometeMouseEvent(comp, MouseReason.WHEEL, e.getX(), e.getY(), e.getButton(), e.getClickCount(),
                            e.getWheelRotation(), e.getModifiersEx(), e.getWhen(), e.isAltDown(), e.isAltGraphDown(),
                            e.isControlDown(), e.isShiftDown(), e.isMetaDown()));
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if ((e != null) && (e.getSource() instanceof IComponent)) {
            IComponent comp = (IComponent) e.getSource();
            warnMouseListeners(comp,
                    new CometeMouseEvent(comp, MouseReason.CLICKED, e.getX(), e.getY(), e.getButton(),
                            e.getClickCount(), 0, e.getModifiersEx(), e.getWhen(), e.isAltDown(), e.isAltGraphDown(),
                            e.isControlDown(), e.isShiftDown(), e.isMetaDown()));
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if ((e != null) && (e.getSource() instanceof IComponent)) {
            IComponent comp = (IComponent) e.getSource();
            warnMouseListeners(comp,
                    new CometeMouseEvent(comp, MouseReason.PRESSED, e.getX(), e.getY(), e.getButton(),
                            e.getClickCount(), 0, e.getModifiersEx(), e.getWhen(), e.isAltDown(), e.isAltGraphDown(),
                            e.isControlDown(), e.isShiftDown(), e.isMetaDown()));
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if ((e != null) && (e.getSource() instanceof IComponent)) {
            IComponent comp = (IComponent) e.getSource();
            warnMouseListeners(comp,
                    new CometeMouseEvent(comp, MouseReason.RELEASED, e.getX(), e.getY(), e.getButton(),
                            e.getClickCount(), 0, e.getModifiersEx(), e.getWhen(), e.isAltDown(), e.isAltGraphDown(),
                            e.isControlDown(), e.isShiftDown(), e.isMetaDown()));
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if ((e != null) && (e.getSource() instanceof IComponent)) {
            IComponent comp = (IComponent) e.getSource();
            warnMouseListeners(comp,
                    new CometeMouseEvent(comp, MouseReason.ENTERED, e.getX(), e.getY(), e.getButton(),
                            e.getClickCount(), 0, e.getModifiersEx(), e.getWhen(), e.isAltDown(), e.isAltGraphDown(),
                            e.isControlDown(), e.isShiftDown(), e.isMetaDown()));
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if ((e != null) && (e.getSource() instanceof IComponent)) {
            IComponent comp = (IComponent) e.getSource();
            warnMouseListeners(comp,
                    new CometeMouseEvent(comp, MouseReason.EXITED, e.getX(), e.getY(), e.getButton(), e.getClickCount(),
                            0, e.getModifiersEx(), e.getWhen(), e.isAltDown(), e.isAltGraphDown(), e.isControlDown(),
                            e.isShiftDown(), e.isMetaDown()));
        }
    }

    /**
     * Adds an {@link IMouseListener} to an {@link IComponent}, if this {@link IComponent} is a
     * {@link Component}
     * 
     * @param comp The concerned {@link IComponent}
     * @param listener The {@link IMouseListener} to add
     */
    private void registerMouseListenerToComponent(IComponent comp, IMouseListener listener) {
        if ((listener != null) && (comp instanceof Component)) {
            List<WeakReference<IMouseListener>> listeners;
            Component component = (Component) comp;
            synchronized (listenerMap) {
                listeners = listenerMap.get(comp);
                if (listeners == null) {
                    component.removeMouseListener(this);
                    component.removeMouseWheelListener(this);
                    component.removeMouseMotionListener(this);
                    listeners = new ArrayList<WeakReference<IMouseListener>>();
                    listenerMap.put(comp, listeners);
                    component.addMouseListener(this);
                    component.addMouseWheelListener(this);
                    component.addMouseMotionListener(this);
                }
            }
            synchronized (listeners) {
                List<WeakReference<IMouseListener>> toRemove = new ArrayList<WeakReference<IMouseListener>>();
                boolean canAdd = true;
                for (WeakReference<IMouseListener> ref : listeners) {
                    IMouseListener temp = ref.get();
                    if (temp == null) {
                        toRemove.add(ref);
                    } else if (temp.equals(listener)) {
                        canAdd = false;
                    }
                }
                listeners.removeAll(toRemove);
                toRemove.clear();
                if (canAdd) {
                    listeners.add(new WeakReference<IMouseListener>(listener));
                }
            }
        }
    }

    /**
     * Removes an {@link IMouseListener} from an {@link IComponent}, if this {@link IComponent} is a
     * {@link Component}
     * 
     * @param comp The concerned {@link IComponent}
     * @param listener The {@link IMouseListener} to remove
     */
    private void unregisterMouseListenerFromComponent(IComponent comp, IMouseListener listener) {
        if ((listener != null) && (comp instanceof Component)) {
            List<WeakReference<IMouseListener>> listeners;
            Component component = (Component) comp;
            synchronized (listenerMap) {
                listeners = listenerMap.get(comp);
                if (listeners == null) {
                    component.removeMouseListener(this);
                    component.removeMouseWheelListener(this);
                    component.removeMouseMotionListener(this);
                }
            }
            if (listeners != null) {
                synchronized (listeners) {
                    List<WeakReference<IMouseListener>> toRemove = new ArrayList<WeakReference<IMouseListener>>();
                    for (WeakReference<IMouseListener> ref : listeners) {
                        IMouseListener temp = ref.get();
                        if ((temp == null) || temp.equals(listener)) {
                            toRemove.add(ref);
                        }
                    }
                    listeners.removeAll(toRemove);
                    toRemove.clear();
                    if (listeners.isEmpty()) {
                        synchronized (listenerMap) {
                            listenerMap.remove(comp);
                            component.removeMouseListener(this);
                            component.removeMouseWheelListener(this);
                            component.removeMouseMotionListener(this);
                        }
                    }
                }
            }
        }
    }

    /**
     * Removes all {@link IMouseListener}s from an {@link IComponent}, if this {@link IComponent} is
     * a {@link Component}
     * 
     * @param comp The concerned {@link IComponent}
     */
    private void unregisterAllMouseListenersFromComponent(IComponent comp) {
        if (comp instanceof Component) {
            Component component = (Component) comp;
            synchronized (listenerMap) {
                component.removeMouseListener(this);
                component.removeMouseWheelListener(this);
                component.removeMouseMotionListener(this);
                List<WeakReference<IMouseListener>> listeners = listenerMap.get(comp);
                if (listeners != null) {
                    synchronized (listeners) {
                        listeners.clear();
                    }
                }
                listenerMap.remove(comp);
            }
        }
    }

    /**
     * Warns all mouse listeners of an {@link IComponent} about a mouse event
     * 
     * @param comp The concerned {@link IComponent}
     * @param event The mouse event
     */
    protected void warnMouseListeners(IComponent comp, CometeMouseEvent event) {
        if ((comp != null) && (event != null)) {
            List<WeakReference<IMouseListener>> listeners;
            synchronized (listenerMap) {
                listeners = listenerMap.get(comp);
            }
            if (listeners != null) {
                synchronized (listeners) {
                    List<WeakReference<IMouseListener>> toRemove = new ArrayList<WeakReference<IMouseListener>>();
                    for (WeakReference<IMouseListener> ref : listeners) {
                        IMouseListener listener = ref.get();
                        if (listener == null) {
                            toRemove.add(ref);
                        } else if (!event.isCanceled()) {
                            listener.mouseChanged(event);
                        }
                    }
                    listeners.removeAll(toRemove);
                    toRemove.clear();
                }
            }
        }
    }

}
