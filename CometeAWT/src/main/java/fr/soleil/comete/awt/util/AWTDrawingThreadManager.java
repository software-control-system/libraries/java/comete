/*
 * This file is part of CometeAWT.
 * 
 * CometeAWT is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeAWT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeAWT. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.awt.util;

import java.awt.EventQueue;

import fr.soleil.data.service.thread.IDrawingThreadManager;

public class AWTDrawingThreadManager implements IDrawingThreadManager {

    public static final AWTDrawingThreadManager INSTANCE = new AWTDrawingThreadManager();

    private AWTDrawingThreadManager() {
        // nothing to do
    }

    @Override
    public void runInDrawingThread(Runnable runnable) {
        if (runnable != null) {
            if (EventQueue.isDispatchThread()) {
                runnable.run();
            } else {
                EventQueue.invokeLater(runnable);
            }
        }
    }

}
