/*
 * This file is part of CometeAWT.
 * 
 * CometeAWT is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeAWT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeAWT. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.awt.util;

import java.awt.Color;

import fr.soleil.comete.definition.widget.util.CometeColor;

/**
 * Common color utilities. Some of the code comes from no.geosoft.cc.color.ui.ColorUtil
 * 
 * @author saintin
 * @author <a href="mailto:jacob.dreyer@geosoft.no">Jacob Dreyer</a>
 */
public class ColorTool {

    public static Color getColor(CometeColor cometeColor) {
        if (cometeColor == null) {
            return null;
        }
        return getColor(cometeColor.getRed(), cometeColor.getGreen(), cometeColor.getBlue());
    }

    public static Color getColor(int r, int g, int b) {
        return new Color(r, g, b);
    }

    public static Color getColor(int r, int g, int b, int a) {
        return new Color(r, g, b, a);
    }

    public static CometeColor getCometeColor(Color srcColor) {
        if (srcColor == null) {
            return null;
        }
        return new CometeColor(srcColor.getRed(), srcColor.getGreen(), srcColor.getBlue(), srcColor.getAlpha());
    }

    public static CometeColor[] getCometeColors(Color[] colors) {
        CometeColor[] result;
        if (colors == null) {
            result = null;
        } else {
            result = new CometeColor[colors.length];
            for (int i = 0; i < colors.length; i++) {
                result[i] = getCometeColor(colors[i]);
            }
        }
        return result;
    }

    public static Color[] getColors(CometeColor[] colors) {
        Color[] result;
        if (colors == null) {
            result = null;
        } else {
            result = new Color[colors.length];
            for (int i = 0; i < colors.length; i++) {
                result[i] = getColor(colors[i]);
            }
        }
        return result;
    }

}
