/*
 * This file is part of CometeAWT.
 * 
 * CometeAWT is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeAWT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeAWT. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.awt;

import java.awt.Checkbox;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.LinkedHashSet;

import fr.soleil.comete.awt.util.AWTDrawingThreadManager;
import fr.soleil.comete.awt.util.AWTErrorNotificationDelegate;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.ICheckBoxListener;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.ICheckBox;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;

public class CheckBox extends Checkbox implements ICheckBox, ItemListener {

    private static final long serialVersionUID = 1737704630422314474L;

    protected Collection<ICheckBoxListener> checkBoxListeners;
    private String falseLabel;
    private String trueLabel;
    private final TargetDelegate delegate;
    private final AWTErrorNotificationDelegate errorNotificationDelegate;
    private volatile boolean editable;

    public CheckBox() {
        super();
        checkBoxListeners = new LinkedHashSet<>();
        falseLabel = ObjectUtils.EMPTY_STRING;
        trueLabel = ObjectUtils.EMPTY_STRING;
        delegate = new TargetDelegate();
        errorNotificationDelegate = new AWTErrorNotificationDelegate(this);
        editable = true;
        addItemListener(this);
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void setState(boolean state) {
        if (editable) {
            doSetState(state);
        }
    }

    /**
     * Changes the checkbox' state and label
     * 
     * @param state The state to set
     */
    protected void doSetState(boolean state) {
        super.setState(state);
        updateLabel();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        updateLabel();
        fireSelectedChanged(new EventObject(this));
    }

    /**
     * Updates the checkbox label according to its selection
     */
    protected void updateLabel() {
        if (isSelected()) {
            if (trueLabel != null) {
                setText(trueLabel);
            }
        } else if (falseLabel != null) {
            setText(falseLabel);
        }
    }

    @Override
    public void addCheckBoxListener(final ICheckBoxListener listener) {
        synchronized (checkBoxListeners) {
            checkBoxListeners.add(listener);
        }
    }

    @Override
    public void removeCheckBoxListener(final ICheckBoxListener listener) {
        synchronized (checkBoxListeners) {
            checkBoxListeners.remove(listener);
        }
    }

    @Override
    public void fireSelectedChanged(EventObject event) {
        Collection<ICheckBoxListener> listeners;
        synchronized (checkBoxListeners) {
            listeners = new ArrayList<>(checkBoxListeners);
        }
        try {
            for (ICheckBoxListener listener : listeners) {
                listener.selectedChanged(event);
            }
        } finally {
            listeners.clear();
        }
    }

    public String getText() {
        return super.getLabel();
    }

    @Override
    public boolean isSelected() {
        return super.getState();
    }

    @Override
    public void setSelected(boolean selected) {
        doSetState(selected);
    }

    public void setText(String text) {
        super.setLabel(text);
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(CometeFont font) {
        super.setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        super.setForeground(ColorTool.getColor(color));
    }

    @Override
    public void setOpaque(boolean opaque) {
    }

    @Override
    public void setToolTipText(String text) {
    }

    @Override
    public String getToolTipText() {
        return null;
    }

    @Override
    public int getHorizontalAlignment() {
        return 0;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
    }

    @Override
    public String getFalseLabel() {
        return falseLabel;
    }

    @Override
    public String getTrueLabel() {
        return trueLabel;
    }

    @Override
    public void setFalseLabel(String falseLabel) {
        this.falseLabel = falseLabel;
        if (falseLabel != null && !isSelected()) {
            setText(falseLabel);
        }

    }

    @Override
    public void setTrueLabel(String trueLabel) {
        this.trueLabel = trueLabel;
        if (trueLabel != null && isSelected()) {
            setText(trueLabel);
        }
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to
     *            {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    @Override
    public void setEditable(boolean editable) {
        this.editable = editable;
        setFocusable(editable);
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return AWTDrawingThreadManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
