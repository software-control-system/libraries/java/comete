/*
 * This file is part of CometeAWT.
 * 
 * CometeAWT is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * CometeAWT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with CometeAWT. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.comete.awt;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.LinkedHashSet;

import fr.soleil.comete.awt.util.AWTDrawingThreadManager;
import fr.soleil.comete.awt.util.AWTErrorNotificationDelegate;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.IButtonListener;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IButton;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;

public class Button extends java.awt.Button implements IButton, ActionListener {

    private static final long serialVersionUID = -2331685482555999529L;

    private final Collection<IButtonListener> buttonListeners;
    private long oldEvent;
    private String actionName;
    private final TargetDelegate delegate;
    private final AWTErrorNotificationDelegate errorNotificationDelegate;

    public Button() {
        super();
        buttonListeners = new LinkedHashSet<>();
        oldEvent = 0;
        actionName = ObjectUtils.EMPTY_STRING;
        delegate = new TargetDelegate();
        errorNotificationDelegate = new AWTErrorNotificationDelegate(this);
        addActionListener(this);
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void addButtonListener(IButtonListener listener) {
        synchronized (buttonListeners) {
            buttonListeners.add(listener);
        }
    }

    @Override
    public void removeButtonListener(IButtonListener listener) {
        synchronized (buttonListeners) {
            buttonListeners.remove(listener);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        long actualEvent = e.getWhen();
        if (oldEvent != actualEvent) {
            oldEvent = actualEvent;
            fireActionPerformed(new EventObject(this));
        }
    }

    @Override
    public void fireActionPerformed(EventObject event) {
        Collection<IButtonListener> listeners;
        synchronized (buttonListeners) {
            listeners = new ArrayList<>(buttonListeners);
        }
        try {
            for (IButtonListener listener : listeners) {
                listener.actionPerformed(event);
            }
        } finally {
            listeners.clear();
        }
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(super.getBackground());
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(super.getFont());
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(super.getForeground());
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setBackground(ColorTool.getColor(color));
    }

    @Override
    public void setCometeFont(CometeFont font) {
        super.setFont(FontTool.getFont(font));
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        super.setForeground(ColorTool.getColor(color));
    }

    @Override
    public CometeImage getCometeImage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getText() {
        return super.getLabel();
    }

    @Override
    public boolean isBorderPainted() {
        return true;
    }

    @Override
    public boolean isButtonLook() {
        return true;
    }

    @Override
    public void setBorderPainted(boolean painted) {
    }

    @Override
    public void setButtonLook(boolean buttonLook) {
    }

    @Override
    public void setCometeImage(CometeImage image) {
    }

    @Override
    public void setText(String text) {
        super.setLabel(text);
    }

    @Override
    public int getHorizontalAlignment() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getToolTipText() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
    }

    @Override
    public void setOpaque(boolean opaque) {
    }

    @Override
    public void setToolTipText(String text) {
    }

    @Override
    public void execute() {
        ActionEvent event = new ActionEvent(this, this.hashCode(), actionName, System.currentTimeMillis(), 0);
        actionPerformed(event);
    }

    @Override
    public String getActionName() {
        return actionName;
    }

    @Override
    public void setActionName(String actionName) {
        this.actionName = actionName;
        setActionCommand(actionName);
    }

    @Override
    public boolean isSelected() {
        return false;
    }

    @Override
    public void setSelected(boolean state) {
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to
     *            {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    @Override
    public void setEditable(boolean editable) {
        // Nothing to do: a button is not editable
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return AWTDrawingThreadManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
