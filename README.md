![Comete](doc/images/CometeLogo-small.png) ![comete-icon](doc/images/comet-small.png)

---
# What is Comete?  
Comete is a Java framework that aims to make easier programming of scientific data plotting applications.  
It uses a MVC paradigm and other design patterns.  
# Organization and licenses  
Comete project is organized into maven modules.  
Each module has its own license, though for now all of these are LGPL V3.
1. **DataConnectionManagement**
    * DataConnectionManagement is the module that describes everything needed to connect something to a datasource.  
    * It is licensed under LGPL V3.
1. **CometeDefinition**
    * CometeDefinition describes what is a widget and how to connect it.  
    * It is licensed under LGPL V3.
1. **CometeAWT**
    * CometeAWT proposes pure java AWT widgets.  
    * It is licensed under LGPL V3.
1. **CometeSwing**
    * CometeSwing proposes java swing widgets.  
    * It is licensed under LGPL V3.
1. **CometeBox**
    * CometeBox is a project that proposes some tools to simplify the way to connect widgets to datasources and their metadata.  
    * It is licensed under LGPL V3.
